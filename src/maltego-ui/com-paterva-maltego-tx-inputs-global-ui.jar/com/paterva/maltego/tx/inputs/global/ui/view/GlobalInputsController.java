/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.tx.inputs.global.GlobalTransformInputProvider
 *  com.paterva.maltego.tx.inputs.global.GlobalTransformInputs
 *  com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInputsRepository
 */
package com.paterva.maltego.tx.inputs.global.ui.view;

import com.paterva.maltego.tx.inputs.global.GlobalTransformInputProvider;
import com.paterva.maltego.tx.inputs.global.GlobalTransformInputs;
import com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInputsRepository;
import com.paterva.maltego.tx.inputs.global.ui.view.GlobalInputsPanel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class GlobalInputsController {
    private GlobalTransformInputProvider _provider;
    private GlobalInputsPanel _panel;

    public synchronized JPanel getComponent() {
        if (this._panel == null) {
            this._panel = new GlobalInputsPanel();
            this.updatePanel();
            this.getProvider().addChangeListener(new ChangeListener(){

                @Override
                public void stateChanged(ChangeEvent changeEvent) {
                    GlobalInputsController.this.updatePanel();
                }
            });
            GlobalTransformInputsRepository.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                    GlobalInputsController.this.updatePanel();
                }
            });
        }
        return this._panel;
    }

    private void updatePanel() {
        GlobalTransformInputProvider globalTransformInputProvider = this.getProvider();
        GlobalTransformInputs globalTransformInputs = globalTransformInputProvider.getGlobalInputs();
        this._panel.setGlobalInputs(globalTransformInputs);
    }

    private synchronized GlobalTransformInputProvider getProvider() {
        if (this._provider == null) {
            this._provider = GlobalTransformInputProvider.getDefault();
        }
        return this._provider;
    }

}

