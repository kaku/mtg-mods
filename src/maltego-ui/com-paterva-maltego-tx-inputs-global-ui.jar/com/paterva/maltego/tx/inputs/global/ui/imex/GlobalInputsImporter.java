/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.EntryFactory
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput
 *  com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInputsRepository
 */
package com.paterva.maltego.tx.inputs.global.ui.imex;

import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput;
import com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInputsRepository;
import com.paterva.maltego.tx.inputs.global.ui.imex.GlobalInputEntryFactory;
import com.paterva.maltego.tx.inputs.global.ui.imex.GlobalInputWrapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class GlobalInputsImporter {
    public List<GlobalTransformInput> read(MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        List list = maltegoArchiveReader.readAll((EntryFactory)new GlobalInputEntryFactory(), "Graph1");
        if (list.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        List<GlobalTransformInput> list2 = this.getGlobalInputs(list);
        return list2;
    }

    public int apply(Collection<GlobalTransformInput> collection) {
        GlobalTransformInputsRepository globalTransformInputsRepository = GlobalTransformInputsRepository.getDefault();
        int n = 0;
        for (GlobalTransformInput globalTransformInput : collection) {
            globalTransformInputsRepository.put(globalTransformInput);
            ++n;
        }
        return n;
    }

    private List<GlobalTransformInput> getGlobalInputs(List<GlobalInputWrapper> list) {
        ArrayList<GlobalTransformInput> arrayList = new ArrayList<GlobalTransformInput>();
        for (GlobalInputWrapper globalInputWrapper : list) {
            GlobalTransformInput globalTransformInput = globalInputWrapper.getGlobalInput();
            arrayList.add(globalTransformInput);
        }
        return arrayList;
    }
}

