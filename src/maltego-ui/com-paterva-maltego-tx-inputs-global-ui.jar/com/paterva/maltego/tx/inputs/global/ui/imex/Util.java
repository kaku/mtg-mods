/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput
 */
package com.paterva.maltego.tx.inputs.global.ui.imex;

import com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput;
import com.paterva.maltego.tx.inputs.global.ui.imex.SelectableGlobalInput;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

class Util {
    Util() {
    }

    public static List<SelectableGlobalInput> createSelectables(Collection<? extends GlobalTransformInput> collection) {
        ArrayList<SelectableGlobalInput> arrayList = new ArrayList<SelectableGlobalInput>();
        for (GlobalTransformInput globalTransformInput : collection) {
            arrayList.add(new SelectableGlobalInput(globalTransformInput, true));
        }
        Collections.sort(arrayList);
        return arrayList;
    }
}

