/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.utils;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.UIManager;

public class ButtonSizingUtils {
    private static ButtonSizingUtils instance;
    private Insets outsets;
    private Insets toggleOutsets;

    public static synchronized ButtonSizingUtils getInstance() {
        if (instance == null) {
            instance = new ButtonSizingUtils();
        }
        return instance;
    }

    private ButtonSizingUtils() {
        this.outsets = this.syncOutsets(new JButton(""));
        this.toggleOutsets = this.syncOutsets(new JToggleButton(""));
        UIManager.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("lookAndFeel".equals(propertyChangeEvent.getPropertyName())) {
                    ButtonSizingUtils.this.outsets = ButtonSizingUtils.this.syncOutsets(new JButton(""));
                    ButtonSizingUtils.this.toggleOutsets = ButtonSizingUtils.this.syncOutsets(new JToggleButton(""));
                }
            }
        });
    }

    private Insets syncOutsets(AbstractButton abstractButton) {
        int n;
        int n2;
        int n3;
        int n4;
        int n5;
        JPanel jPanel = new JPanel(null);
        abstractButton.putClientProperty("JButton.buttonStyle", "square");
        abstractButton.setFocusable(false);
        abstractButton.setOpaque(false);
        jPanel.add(abstractButton);
        abstractButton.setBounds(0, 0, 100, 50);
        GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice graphicsDevice = graphicsEnvironment.getDefaultScreenDevice();
        GraphicsConfiguration graphicsConfiguration = graphicsDevice.getDefaultConfiguration();
        BufferedImage bufferedImage = graphicsConfiguration.createCompatibleImage(100, 50, 3);
        abstractButton.paint(bufferedImage.getGraphics());
        int n6 = 0;
        for (n3 = 0; n3 < 25; ++n3) {
            n4 = bufferedImage.getRGB(50, n3);
            n2 = n4 >>> 24 & 255;
            if (n2 != 255) continue;
            n6 = n3;
            break;
        }
        n3 = 0;
        for (n4 = 49; n4 > 25; --n4) {
            n2 = bufferedImage.getRGB(50, n4);
            n5 = n2 >>> 24 & 255;
            if (n5 != 255) continue;
            n3 = 49 - n4;
            break;
        }
        n4 = 0;
        for (n2 = 0; n2 < 50; ++n2) {
            n5 = bufferedImage.getRGB(n2, 25);
            n = n5 >>> 24 & 255;
            if (n != 255) continue;
            n4 = n2;
            break;
        }
        n2 = 0;
        for (n5 = 99; n5 > 50; --n5) {
            n = bufferedImage.getRGB(n5, 25);
            int n7 = n >>> 24 & 255;
            if (n7 != 255) continue;
            n2 = 99 - n5;
            break;
        }
        return new Insets(n6, n4, n3, n2);
    }

    public Insets getOutsets() {
        return new Insets(this.outsets.top, this.outsets.left, this.outsets.bottom, this.outsets.right);
    }

    public Insets getToggleOutsets() {
        return new Insets(this.toggleOutsets.top, this.toggleOutsets.left, this.toggleOutsets.bottom, this.toggleOutsets.right);
    }

}

