/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.utils;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Container;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import javax.swing.AbstractButton;
import javax.swing.FocusManager;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SwingUtilities;
import javax.swing.event.EventListenerList;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;
import org.pushingpixels.flamingo.api.common.model.PopupButtonModel;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelManager;
import org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;
import org.pushingpixels.flamingo.api.ribbon.JRibbonComponent;
import org.pushingpixels.flamingo.api.ribbon.JRibbonFrame;
import org.pushingpixels.flamingo.api.ribbon.RibbonTask;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.BasicRibbonUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.JRibbonTaskToggleButton;
import org.pushingpixels.flamingo.internal.ui.ribbon.RibbonComponentUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.RibbonUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuButton;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuPopupPanel;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public class KeyTipManager {
    boolean isShowingKeyTips = false;
    List<KeyTipChain> keyTipChains = new ArrayList<KeyTipChain>();
    protected EventListenerList listenerList = new EventListenerList();
    protected BlockingQueue<Character> processingQueue = new LinkedBlockingQueue<Character>();
    protected ProcessingThread processingThread;
    private JRibbonFrame rootOwner;
    private Component focusOwner;
    private static final KeyTipManager instance = new KeyTipManager();

    public static KeyTipManager defaultManager() {
        return instance;
    }

    private KeyTipManager() {
        this.processingThread = new ProcessingThread();
        this.processingThread.start();
    }

    public boolean isShowingKeyTips() {
        return !this.keyTipChains.isEmpty();
    }

    public void hideAllKeyTips() {
        if (this.keyTipChains.isEmpty()) {
            return;
        }
        this.keyTipChains.clear();
        this.fireKeyTipsHidden(this.rootOwner);
        this.repaintWindows();
        this.tryRestoringFocusOwner();
    }

    private void tryRestoringFocusOwner() {
        if (this.focusOwner != null && this.focusOwner.isDisplayable() && this.focusOwner.isShowing()) {
            this.focusOwner.requestFocus();
        }
    }

    public void showRootKeyTipChain(JRibbonFrame jRibbonFrame) {
        Object object2;
        Object object;
        Object object3;
        JComponent jComponent;
        if (!this.keyTipChains.isEmpty()) {
            throw new IllegalStateException("Can't call this method when key tip chains are present");
        }
        this.focusOwner = FocusManager.getCurrentManager().getFocusOwner();
        jRibbonFrame.requestFocus();
        this.rootOwner = jRibbonFrame;
        JRibbon jRibbon = jRibbonFrame.getRibbon();
        KeyTipChain keyTipChain = new KeyTipChain(jRibbon);
        final JRibbonApplicationMenuButton jRibbonApplicationMenuButton = FlamingoUtilities.getApplicationMenuButton(jRibbonFrame);
        if (jRibbonApplicationMenuButton != null && jRibbon.getApplicationMenuKeyTip() != null) {
            object2 = new KeyTipLink();
            object2.comp = jRibbonApplicationMenuButton;
            object2.keyTipString = jRibbon.getApplicationMenuKeyTip();
            object2.prefAnchorPoint = jRibbonApplicationMenuButton.getUI().getKeyTipAnchorCenterPoint();
            object2.onActivated = new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    jRibbonApplicationMenuButton.doPopupClick();
                }
            };
            object2.enabled = true;
            object2.traversal = new KeyTipLinkTraversal((KeyTipLink)object2){
                final /* synthetic */ KeyTipLink val$appMenuButtonLink;

                @Override
                public KeyTipChain getNextChain() {
                    PopupPanelManager.PopupInfo popupInfo;
                    List<PopupPanelManager.PopupInfo> list = PopupPanelManager.defaultManager().getShownPath();
                    if (list.size() > 0 && (popupInfo = list.get(list.size() - 1)).getPopupOriginator() == jRibbonApplicationMenuButton) {
                        JPopupPanel jPopupPanel = popupInfo.getPopupPanel();
                        KeyTipChain keyTipChain = new KeyTipChain(jPopupPanel);
                        keyTipChain.parent = this.val$appMenuButtonLink.traversal;
                        KeyTipManager.this.populateChain(popupInfo.getPopupPanel(), keyTipChain);
                        return keyTipChain;
                    }
                    return null;
                }
            };
            keyTipChain.addLink((KeyTipLink)object2);
        }
        for (Component object4 : jRibbon.getTaskbarComponents()) {
            if (!(object4 instanceof AbstractCommandButton)) continue;
            AbstractCommandButton abstractCommandButton = (AbstractCommandButton)object4;
            object = this.getCommandButtonActionLink(abstractCommandButton);
            if (object != null) {
                keyTipChain.addLink((KeyTipLink)object);
            }
            if (!(object4 instanceof JCommandButton) || (object3 = this.getCommandButtonPopupLink((JCommandButton)(jComponent = (JCommandButton)object4))) == null) continue;
            keyTipChain.addLink((KeyTipLink)object3);
        }
        object2 = jRibbon.getUI();
        if (object2 instanceof BasicRibbonUI) {
            for (Map.Entry<RibbonTask, JRibbonTaskToggleButton> entry : ((BasicRibbonUI)object2).getTaskToggleButtons().entrySet()) {
                object = entry.getKey();
                jComponent = entry.getValue();
                object3 = object.getKeyTip();
                if (object3 == null) continue;
                KeyTipLink keyTipLink = new KeyTipLink();
                keyTipLink.comp = jComponent;
                keyTipLink.keyTipString = object3;
                keyTipLink.prefAnchorPoint = new Point(jComponent.getWidth() / 2, jComponent.getHeight());
                keyTipLink.onActivated = new ActionListener((JRibbonTaskToggleButton)jComponent){
                    final /* synthetic */ JRibbonTaskToggleButton val$taskToggleButton;

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        this.val$taskToggleButton.doActionClick();
                    }
                };
                keyTipLink.enabled = true;
                keyTipLink.traversal = new KeyTipLinkTraversal((JRibbonTaskToggleButton)jComponent, (RibbonTask)object, keyTipLink){
                    final /* synthetic */ JRibbonTaskToggleButton val$taskToggleButton;
                    final /* synthetic */ RibbonTask val$task;
                    final /* synthetic */ KeyTipLink val$taskToggleButtonLink;

                    @Override
                    public KeyTipChain getNextChain() {
                        KeyTipChain keyTipChain = new KeyTipChain(this.val$taskToggleButton);
                        for (AbstractRibbonBand abstractRibbonBand : this.val$task.getBands()) {
                            KeyTipManager.this.populateChain(abstractRibbonBand, keyTipChain);
                        }
                        keyTipChain.parent = this.val$taskToggleButtonLink.traversal;
                        return keyTipChain;
                    }
                };
                keyTipChain.addLink(keyTipLink);
            }
        }
        this.keyTipChains.add(keyTipChain);
        this.fireKeyTipsShown(jRibbonFrame);
        jRibbonFrame.repaint();
    }

    public Collection<KeyTipLink> getCurrentlyShownKeyTips() {
        if (this.keyTipChains.isEmpty()) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableCollection(this.keyTipChains.get(this.keyTipChains.size() - 1).links);
    }

    public KeyTipChain getCurrentlyShownKeyTipChain() {
        if (this.keyTipChains.isEmpty()) {
            return null;
        }
        return this.keyTipChains.get(this.keyTipChains.size() - 1);
    }

    public void showPreviousChain() {
        if (this.keyTipChains.isEmpty()) {
            return;
        }
        this.keyTipChains.remove(this.keyTipChains.size() - 1);
        if (!this.isShowingKeyTips()) {
            this.tryRestoringFocusOwner();
        }
        this.repaintWindows();
    }

    private void addCommandButtonLinks(Component component, KeyTipChain keyTipChain) {
        JCommandButton jCommandButton;
        KeyTipLink keyTipLink;
        AbstractCommandButton abstractCommandButton = (AbstractCommandButton)component;
        KeyTipLink keyTipLink2 = this.getCommandButtonActionLink(abstractCommandButton);
        if (keyTipLink2 != null) {
            keyTipChain.addLink(keyTipLink2);
        }
        if (component instanceof JCommandButton && (keyTipLink = this.getCommandButtonPopupLink(jCommandButton = (JCommandButton)component)) != null) {
            keyTipChain.addLink(keyTipLink);
        }
    }

    private void populateChain(final Component component, final KeyTipChain keyTipChain) {
        Serializable serializable2;
        KeyTipLink keyTipLink;
        if (component instanceof AbstractCommandButton) {
            Serializable serializable2 = component.getBounds();
            if (component.isVisible() && component.isShowing()) {
                if (serializable2.height > 0 && serializable2.width > 0) {
                    this.addCommandButtonLinks(component, keyTipChain);
                } else {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            Rectangle rectangle = component.getBounds();
                            if (rectangle.height > 0 && rectangle.width > 0) {
                                KeyTipManager.this.addCommandButtonLinks(component, keyTipChain);
                            }
                        }
                    });
                }
            }
        }
        if (component instanceof JRibbonComponent && (keyTipLink = this.getRibbonComponentLink((JRibbonComponent)(serializable2 = (JRibbonComponent)component))) != null) {
            keyTipChain.addLink(keyTipLink);
        }
        if (component instanceof Container) {
            serializable2 = (Container)component;
            for (int i = 0; i < serializable2.getComponentCount(); ++i) {
                this.populateChain(serializable2.getComponent(i), keyTipChain);
            }
        }
    }

    private KeyTipLink getCommandButtonActionLink(final AbstractCommandButton abstractCommandButton) {
        if (abstractCommandButton.getActionKeyTip() != null) {
            final KeyTipLink keyTipLink = new KeyTipLink();
            keyTipLink.comp = abstractCommandButton;
            keyTipLink.keyTipString = abstractCommandButton.getActionKeyTip();
            keyTipLink.prefAnchorPoint = abstractCommandButton.getUI().getKeyTipAnchorCenterPoint();
            keyTipLink.onActivated = new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    abstractCommandButton.doActionClick();
                }
            };
            keyTipLink.enabled = abstractCommandButton.getActionModel().isEnabled();
            keyTipLink.traversal = abstractCommandButton.getClass().isAnnotationPresent(HasNextKeyTipChain.class) ? new KeyTipLinkTraversal(){

                @Override
                public KeyTipChain getNextChain() {
                    List<PopupPanelManager.PopupInfo> list = PopupPanelManager.defaultManager().getShownPath();
                    if (list.size() > 0) {
                        PopupPanelManager.PopupInfo popupInfo = list.get(list.size() - 1);
                        JPopupPanel jPopupPanel = popupInfo.getPopupPanel();
                        KeyTipChain keyTipChain = new KeyTipChain(jPopupPanel);
                        KeyTipManager.this.populateChain(popupInfo.getPopupPanel(), keyTipChain);
                        keyTipChain.parent = keyTipLink.traversal;
                        return keyTipChain;
                    }
                    return null;
                }
            } : null;
            return keyTipLink;
        }
        return null;
    }

    private KeyTipLink getRibbonComponentLink(final JRibbonComponent jRibbonComponent) {
        if (jRibbonComponent.getKeyTip() != null) {
            KeyTipLink keyTipLink = new KeyTipLink();
            keyTipLink.comp = jRibbonComponent;
            keyTipLink.keyTipString = jRibbonComponent.getKeyTip();
            keyTipLink.prefAnchorPoint = jRibbonComponent.getUI().getKeyTipAnchorCenterPoint();
            keyTipLink.onActivated = new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    JComponent jComponent = jRibbonComponent.getMainComponent();
                    if (jComponent instanceof AbstractButton) {
                        ((AbstractButton)jComponent).doClick();
                    } else if (jComponent instanceof JComboBox) {
                        ((JComboBox)jComponent).showPopup();
                    } else if (jComponent instanceof JSpinner) {
                        JComponent jComponent2 = ((JSpinner)jComponent).getEditor();
                        jComponent2.requestFocusInWindow();
                    } else {
                        jComponent.requestFocusInWindow();
                    }
                }
            };
            keyTipLink.enabled = jRibbonComponent.getMainComponent().isEnabled();
            keyTipLink.traversal = null;
            return keyTipLink;
        }
        return null;
    }

    private KeyTipLink getCommandButtonPopupLink(final JCommandButton jCommandButton) {
        if (jCommandButton.getPopupKeyTip() != null) {
            final KeyTipLink keyTipLink = new KeyTipLink();
            keyTipLink.comp = jCommandButton;
            keyTipLink.keyTipString = jCommandButton.getPopupKeyTip();
            keyTipLink.prefAnchorPoint = jCommandButton.getUI().getKeyTipAnchorCenterPoint();
            keyTipLink.onActivated = new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    if (jCommandButton instanceof JCommandMenuButton) {
                        ((JCommandMenuButton)jCommandButton).doActionRollover();
                    }
                    jCommandButton.doPopupClick();
                }
            };
            keyTipLink.enabled = jCommandButton.getPopupModel().isEnabled();
            keyTipLink.traversal = new KeyTipLinkTraversal(){

                @Override
                public KeyTipChain getNextChain() {
                    List<PopupPanelManager.PopupInfo> list = PopupPanelManager.defaultManager().getShownPath();
                    if (list.size() > 0) {
                        PopupPanelManager.PopupInfo popupInfo = list.get(list.size() - 1);
                        JPopupPanel jPopupPanel = popupInfo.getPopupPanel();
                        if (jPopupPanel instanceof JRibbonApplicationMenuPopupPanel) {
                            JRibbonApplicationMenuPopupPanel jRibbonApplicationMenuPopupPanel = (JRibbonApplicationMenuPopupPanel)jPopupPanel;
                            JPanel jPanel = jRibbonApplicationMenuPopupPanel.getPanelLevel1();
                            JPanel jPanel2 = jRibbonApplicationMenuPopupPanel.getPanelLevel2();
                            if (jPanel2.getComponentCount() > 0) {
                                KeyTipChain keyTipChain = new KeyTipChain(jPanel2);
                                KeyTipManager.this.populateChain(jPanel2, keyTipChain);
                                keyTipChain.parent = keyTipLink.traversal;
                                return keyTipChain;
                            }
                            KeyTipChain keyTipChain = new KeyTipChain(jPanel);
                            KeyTipManager.this.populateChain(jPanel, keyTipChain);
                            keyTipChain.parent = keyTipLink.traversal;
                            return keyTipChain;
                        }
                        KeyTipChain keyTipChain = new KeyTipChain(jPopupPanel);
                        KeyTipManager.this.populateChain(popupInfo.getPopupPanel(), keyTipChain);
                        keyTipChain.parent = keyTipLink.traversal;
                        return keyTipChain;
                    }
                    return null;
                }
            };
            return keyTipLink;
        }
        return null;
    }

    public void handleKeyPress(char c) {
        this.processingQueue.add(Character.valueOf(c));
    }

    private void processNextKeyPress(char c) {
        if (this.keyTipChains.isEmpty()) {
            return;
        }
        KeyTipChain keyTipChain = this.keyTipChains.get(this.keyTipChains.size() - 1);
        for (Object object : keyTipChain.links) {
            Object object2 = object.keyTipString;
            if (Character.toLowerCase(object2.charAt(keyTipChain.keyTipLookupIndex)) != Character.toLowerCase(c) || object2.length() != keyTipChain.keyTipLookupIndex + 1) continue;
            if (object.enabled) {
                object.onActivated.actionPerformed(new ActionEvent(object.comp, 1001, "keyTipActivated"));
                if (object.traversal != null) {
                    SwingUtilities.invokeLater(new Runnable((KeyTipLink)object){
                        final /* synthetic */ KeyTipLink val$link;

                        @Override
                        public void run() {
                            KeyTipChain keyTipChain = this.val$link.traversal.getNextChain();
                            if (keyTipChain != null) {
                                KeyTipChain keyTipChain2 = KeyTipManager.this.keyTipChains.isEmpty() ? null : KeyTipManager.this.keyTipChains.get(KeyTipManager.this.keyTipChains.size() - 1);
                                KeyTipManager.this.keyTipChains.add(keyTipChain);
                                KeyTipManager.this.repaintWindows();
                                if (keyTipChain2 != null) {
                                    for (KeyTipLink keyTipLink : keyTipChain2.links) {
                                        if (!(keyTipLink.comp instanceof JCommandMenuButton)) continue;
                                        keyTipLink.comp.repaint();
                                    }
                                }
                            }
                        }
                    });
                } else {
                    this.hideAllKeyTips();
                }
            }
            return;
        }
        if (keyTipChain.keyTipLookupIndex == 0) {
            KeyTipChain keyTipChain2 = new KeyTipChain(keyTipChain.chainParentComponent);
            keyTipChain2.keyTipLookupIndex = 1;
            for (Object object2 : keyTipChain.links) {
                String string = object2.keyTipString;
                if (Character.toLowerCase(string.charAt(keyTipChain.keyTipLookupIndex)) != Character.toLowerCase(c) || string.length() != 2) continue;
                KeyTipLink keyTipLink = new KeyTipLink();
                keyTipLink.comp = object2.comp;
                keyTipLink.enabled = object2.enabled;
                keyTipLink.keyTipString = object2.keyTipString;
                keyTipLink.onActivated = object2.onActivated;
                keyTipLink.prefAnchorPoint = object2.prefAnchorPoint;
                keyTipLink.traversal = object2.traversal;
                keyTipChain2.addLink(keyTipLink);
            }
            if (keyTipChain2.links.size() > 0) {
                this.keyTipChains.add(keyTipChain2);
            }
            this.repaintWindows();
            return;
        }
    }

    private void repaintWindows() {
        for (Window window2 : Window.getWindows()) {
            window2.repaint();
        }
        List<PopupPanelManager.PopupInfo> list = PopupPanelManager.defaultManager().getShownPath();
        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            PopupPanelManager.PopupInfo popupInfo = (PopupPanelManager.PopupInfo)iterator.next();
            JPopupPanel jPopupPanel = popupInfo.getPopupPanel();
            jPopupPanel.paintImmediately(new Rectangle(0, 0, jPopupPanel.getWidth(), jPopupPanel.getHeight()));
        }
    }

    public void addKeyTipListener(KeyTipListener keyTipListener) {
        this.listenerList.add(KeyTipListener.class, keyTipListener);
    }

    public void removeKeyTipListener(KeyTipListener keyTipListener) {
        this.listenerList.remove(KeyTipListener.class, keyTipListener);
    }

    protected void fireKeyTipsShown(JRibbonFrame jRibbonFrame) {
        Object[] arrobject = this.listenerList.getListenerList();
        KeyTipEvent keyTipEvent = new KeyTipEvent(jRibbonFrame, 0);
        for (int i = arrobject.length - 2; i >= 0; i -= 2) {
            if (arrobject[i] != KeyTipListener.class) continue;
            ((KeyTipListener)arrobject[i + 1]).keyTipsShown(keyTipEvent);
        }
    }

    protected void fireKeyTipsHidden(JRibbonFrame jRibbonFrame) {
        Object[] arrobject = this.listenerList.getListenerList();
        KeyTipEvent keyTipEvent = new KeyTipEvent(jRibbonFrame, 0);
        for (int i = arrobject.length - 2; i >= 0; i -= 2) {
            if (arrobject[i] != KeyTipListener.class) continue;
            ((KeyTipListener)arrobject[i + 1]).keyTipsHidden(keyTipEvent);
        }
    }

    public void refreshCurrentChain() {
        KeyTipChain keyTipChain = this.keyTipChains.get(this.keyTipChains.size() - 1);
        if (keyTipChain.parent == null) {
            return;
        }
        KeyTipChain keyTipChain2 = keyTipChain.parent.getNextChain();
        this.keyTipChains.remove(this.keyTipChains.size() - 1);
        this.keyTipChains.add(keyTipChain2);
        this.repaintWindows();
    }

    private class ProcessingThread
    extends Thread {
        public ProcessingThread() {
            this.setName("KeyTipManager processing thread");
            this.setDaemon(true);
        }

        @Override
        public void run() {
            do {
                try {
                    do {
                        final char c = KeyTipManager.this.processingQueue.take().charValue();
                        SwingUtilities.invokeAndWait(new Runnable(){

                            @Override
                            public void run() {
                                KeyTipManager.this.processNextKeyPress(c);
                            }
                        });
                    } while (true);
                }
                catch (Throwable var1_2) {
                    var1_2.printStackTrace();
                    continue;
                }
                break;
            } while (true);
        }

    }

    public class KeyTipChain {
        private List<KeyTipLink> links;
        public int keyTipLookupIndex;
        public JComponent chainParentComponent;
        private KeyTipLinkTraversal parent;

        public KeyTipChain(JComponent jComponent) {
            this.chainParentComponent = jComponent;
            this.links = new ArrayList<KeyTipLink>();
            this.keyTipLookupIndex = 0;
        }

        public void addLink(KeyTipLink keyTipLink) {
            this.links.add(keyTipLink);
        }
    }

    public class KeyTipLink {
        public String keyTipString;
        public JComponent comp;
        public Point prefAnchorPoint;
        public ActionListener onActivated;
        public KeyTipLinkTraversal traversal;
        public boolean enabled;
    }

    @Target(value={ElementType.TYPE})
    @Retention(value=RetentionPolicy.RUNTIME)
    public static @interface HasNextKeyTipChain {
    }

    public static class KeyTipEvent
    extends AWTEvent {
        public KeyTipEvent(Object object, int n) {
            super(object, n);
        }
    }

    public static interface KeyTipListener
    extends EventListener {
        public void keyTipsShown(KeyTipEvent var1);

        public void keyTipsHidden(KeyTipEvent var1);
    }

    public static interface KeyTipLinkTraversal {
        public KeyTipChain getNextChain();
    }

}

