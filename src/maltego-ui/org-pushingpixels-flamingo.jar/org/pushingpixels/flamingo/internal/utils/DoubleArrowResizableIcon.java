/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.utils;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.GeneralPath;
import javax.swing.UIManager;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

public class DoubleArrowResizableIcon
implements ResizableIcon {
    private Dimension initialDim;
    protected int width;
    protected int height;
    protected int direction;

    public DoubleArrowResizableIcon(Dimension dimension, int n) {
        this.initialDim = dimension;
        this.width = dimension.width;
        this.height = dimension.height;
        this.direction = n;
    }

    public DoubleArrowResizableIcon(int n, int n2) {
        this(new Dimension(n, n), n2);
    }

    public void revertToOriginalDimension() {
        this.width = this.initialDim.width;
        this.height = this.initialDim.height;
    }

    @Override
    public void setDimension(Dimension dimension) {
        this.width = dimension.width;
        this.height = dimension.height;
    }

    @Override
    public int getIconHeight() {
        return this.height;
    }

    @Override
    public int getIconWidth() {
        return this.width;
    }

    protected boolean toPaintEnabled(Component component) {
        return component.isEnabled();
    }

    @Override
    public void paintIcon(Component component, Graphics graphics, int n, int n2) {
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        Color color = this.toPaintEnabled(component) ? UIManager.getLookAndFeelDefaults().getColor("7-white") : UIManager.getLookAndFeelDefaults().getColor("ribbon-disabled-icon-color");
        graphics2D.setColor(color);
        BasicStroke basicStroke = new BasicStroke((float)this.width / 8.0f, 0, 0);
        graphics2D.setStroke(basicStroke);
        graphics2D.translate(n, n2);
        GeneralPath generalPath = new GeneralPath();
        int n3 = this.height / 2;
        int n4 = this.width / 2;
        int n5 = (this.height + 2) / 3;
        int n6 = (this.width + 2) / 3;
        switch (this.direction) {
            case 1: {
                generalPath.moveTo(0.0f, this.height - 1);
                generalPath.lineTo(0.5f * (float)(this.width - 1), this.height - 1 - n3);
                generalPath.lineTo(this.width - 1, this.height - 1);
                generalPath.moveTo(0.0f, this.height - 1 - n5);
                generalPath.lineTo(0.5f * (float)(this.width - 1), this.height - 1 - n3 - n5);
                generalPath.lineTo(this.width - 1, this.height - 1 - n5);
                break;
            }
            case 5: {
                generalPath.moveTo(0.0f, 0.0f);
                generalPath.lineTo(0.5f * (float)(this.width - 1), n3);
                generalPath.lineTo(this.width - 1, 0.0f);
                generalPath.moveTo(0.0f, n5);
                generalPath.lineTo(0.5f * (float)(this.width - 1), n3 + n5);
                generalPath.lineTo(this.width - 1, n5);
                break;
            }
            case 3: {
                generalPath.moveTo(0.0f, 0.0f);
                generalPath.lineTo(n4, 0.5f * (float)(this.height - 1));
                generalPath.lineTo(0.0f, this.height - 1);
                generalPath.moveTo(n6, 0.0f);
                generalPath.lineTo(n4 + n6, 0.5f * (float)(this.height - 1));
                generalPath.lineTo(n6, this.height - 1);
                break;
            }
            case 7: {
                generalPath.moveTo(this.width - 1, 0.0f);
                generalPath.lineTo(this.width - 1 - n4, 0.5f * (float)(this.height - 1));
                generalPath.lineTo(this.width - 1, this.height - 1);
                generalPath.moveTo(this.width - 1 - n6, 0.0f);
                generalPath.lineTo(this.width - 1 - n4 - n6, 0.5f * (float)(this.height - 1));
                generalPath.lineTo(this.width - 1 - n6, this.height - 1);
            }
        }
        graphics2D.draw(generalPath);
        graphics2D.dispose();
    }
}

