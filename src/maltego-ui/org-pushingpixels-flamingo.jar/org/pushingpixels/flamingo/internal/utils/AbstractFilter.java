/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.utils;

import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.util.Hashtable;

public abstract class AbstractFilter
implements BufferedImageOp {
    @Override
    public Rectangle2D getBounds2D(BufferedImage bufferedImage) {
        return new Rectangle(0, 0, bufferedImage.getWidth(), bufferedImage.getHeight());
    }

    @Override
    public BufferedImage createCompatibleDestImage(BufferedImage bufferedImage, ColorModel colorModel) {
        if (colorModel == null) {
            colorModel = bufferedImage.getColorModel();
        }
        return new BufferedImage(colorModel, colorModel.createCompatibleWritableRaster(bufferedImage.getWidth(), bufferedImage.getHeight()), colorModel.isAlphaPremultiplied(), null);
    }

    @Override
    public Point2D getPoint2D(Point2D point2D, Point2D point2D2) {
        return (Point2D)point2D.clone();
    }

    @Override
    public RenderingHints getRenderingHints() {
        return null;
    }

    protected int[] getPixels(BufferedImage bufferedImage, int n, int n2, int n3, int n4, int[] arrn) {
        if (n3 == 0 || n4 == 0) {
            return new int[0];
        }
        if (arrn == null) {
            arrn = new int[n3 * n4];
        } else if (arrn.length < n3 * n4) {
            throw new IllegalArgumentException("pixels array must have a length >= w*h");
        }
        int n5 = bufferedImage.getType();
        if (n5 == 2 || n5 == 1) {
            WritableRaster writableRaster = bufferedImage.getRaster();
            return (int[])writableRaster.getDataElements(n, n2, n3, n4, arrn);
        }
        return bufferedImage.getRGB(n, n2, n3, n4, arrn, 0, n3);
    }

    protected void setPixels(BufferedImage bufferedImage, int n, int n2, int n3, int n4, int[] arrn) {
        if (arrn == null || n3 == 0 || n4 == 0) {
            return;
        }
        if (arrn.length < n3 * n4) {
            throw new IllegalArgumentException("pixels array must have a length >= w*h");
        }
        int n5 = bufferedImage.getType();
        if (n5 == 2 || n5 == 1) {
            WritableRaster writableRaster = bufferedImage.getRaster();
            writableRaster.setDataElements(n, n2, n3, n4, arrn);
        } else {
            bufferedImage.setRGB(n, n2, n3, n4, arrn, 0, n3);
        }
    }
}

