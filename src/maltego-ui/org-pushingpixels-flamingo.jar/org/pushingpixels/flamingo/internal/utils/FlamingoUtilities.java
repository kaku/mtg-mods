/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.utils;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.GeneralPath;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.util.List;
import javax.swing.ButtonModel;
import javax.swing.CellRendererPane;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.UIResource;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelManager;
import org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;
import org.pushingpixels.flamingo.api.ribbon.JRibbonFrame;
import org.pushingpixels.flamingo.api.ribbon.resize.IconRibbonBandResizePolicy;
import org.pushingpixels.flamingo.api.ribbon.resize.RibbonBandResizePolicy;
import org.pushingpixels.flamingo.internal.ui.ribbon.JRibbonTaskToggleButton;
import org.pushingpixels.flamingo.internal.ui.ribbon.RibbonBandUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuButton;
import org.pushingpixels.flamingo.internal.utils.ArrowResizableIcon;

public class FlamingoUtilities {
    public static /* varargs */ FontUIResource getFont(Component component, String ... arrstring) {
        Object object;
        if (component != null && (object = component.getFont()) != null) {
            if (object instanceof UIResource) {
                return (FontUIResource)object;
            }
            return new FontUIResource((Font)object);
        }
        for (String string : arrstring) {
            Font font = UIManager.getLookAndFeelDefaults().getFont(string);
            if (font == null) continue;
            if (font instanceof UIResource) {
                return (FontUIResource)font;
            }
            return new FontUIResource(font);
        }
        return null;
    }

    public static /* varargs */ Color getColor(Color color, String ... arrstring) {
        for (String string : arrstring) {
            Color color2 = UIManager.getColor(string);
            if (color2 == null) continue;
            return color2;
        }
        return new ColorUIResource(color);
    }

    public static ResizableIcon getRibbonBandExpandIcon(AbstractRibbonBand abstractRibbonBand) {
        boolean bl = abstractRibbonBand.getComponentOrientation().isLeftToRight();
        return new ArrowResizableIcon(9, bl ? 3 : 7);
    }

    public static ResizableIcon getCommandButtonPopupActionIcon(JCommandButton jCommandButton) {
        JCommandButton.CommandButtonPopupOrientationKind commandButtonPopupOrientationKind = jCommandButton.getPopupOrientationKind();
        switch (commandButtonPopupOrientationKind) {
            case DOWNWARD: {
                return new ArrowResizableIcon.CommandButtonPopupIcon(9, 5);
            }
            case SIDEWARD: {
                return new ArrowResizableIcon.CommandButtonPopupIcon(9, jCommandButton.getComponentOrientation().isLeftToRight() ? 3 : 7);
            }
        }
        return null;
    }

    public static BufferedImage createThumbnail(BufferedImage bufferedImage, int n) {
        float f = (float)bufferedImage.getWidth() / (float)bufferedImage.getHeight();
        int n2 = bufferedImage.getWidth();
        BufferedImage bufferedImage2 = bufferedImage;
        do {
            if ((n2 /= 2) < n) {
                n2 = n;
            }
            BufferedImage bufferedImage3 = new BufferedImage(n2, (int)((float)n2 / f), 2);
            Graphics2D graphics2D = bufferedImage3.createGraphics();
            graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            graphics2D.drawImage(bufferedImage2, 0, 0, bufferedImage3.getWidth(), bufferedImage3.getHeight(), null);
            graphics2D.dispose();
            bufferedImage2 = bufferedImage3;
        } while (n2 != n);
        return bufferedImage2;
    }

    public static GeneralPath getRibbonBorderOutline(int n, int n2, int n3, int n4, int n5, int n6, int n7, float f) {
        int n8 = n7 - n5;
        GeneralPath generalPath = new GeneralPath();
        float f2 = (float)((double)f / (1.5 * Math.pow(n8, 0.5)));
        generalPath.moveTo((float)n + f, n6);
        generalPath.lineTo((float)n3 - f, n6);
        generalPath.moveTo((float)n4 + f - 1.0f, n6);
        generalPath.lineTo((float)n2 - f - 1.0f, n6);
        generalPath.quadTo((float)n2 - f2 - 1.0f, (float)n6 + f2, n2 - 1, (float)n6 + f);
        generalPath.lineTo(n2 - 1, (float)n7 - f - 1.0f);
        generalPath.quadTo((float)n2 - f2 - 1.0f, (float)(n7 - 1) - f2, (float)n2 - f - 1.0f, n7 - 1);
        generalPath.lineTo((float)n + f, n7 - 1);
        generalPath.quadTo((float)n + f2, (float)(n7 - 1) - f2, n, (float)n7 - f - 1.0f);
        generalPath.lineTo(n, (float)n6 + f);
        generalPath.quadTo((float)n + f2, (float)n6 + f2, (float)n + f, n6);
        return generalPath;
    }

    public static GeneralPath getRibbonTaskToggleButtonOutline(int n, int n2, float f) {
        GeneralPath generalPath = new GeneralPath();
        float f2 = (float)((double)f / (1.5 * Math.pow(n2, 0.5)));
        generalPath.moveTo(0.0f, n2);
        generalPath.lineTo(0.0f, f);
        generalPath.quadTo(f2, f2, f, 0.0f);
        generalPath.lineTo((float)n - f - 1.0f, 0.0f);
        generalPath.quadTo((float)n + f2 - 1.0f, f2, n - 1, f);
        generalPath.lineTo(n - 1, n2);
        generalPath.lineTo(0.0f, n2);
        return generalPath;
    }

    public static GeneralPath getRibbonGalleryOutline(int n, int n2, int n3, int n4, float f) {
        int n5 = n4 - n3;
        GeneralPath generalPath = new GeneralPath();
        float f2 = (float)((double)f / (1.5 * Math.pow(n5, 0.5)));
        generalPath.moveTo((float)n + f, n3);
        generalPath.lineTo((float)n2 - f - 1.0f, n3);
        generalPath.quadTo((float)n2 - f2 - 1.0f, (float)n3 + f2, n2 - 1, (float)n3 + f);
        generalPath.lineTo(n2 - 1, (float)n4 - f - 1.0f);
        generalPath.quadTo((float)n2 - f2 - 1.0f, (float)(n4 - 1) - f2, (float)n2 - f - 1.0f, n4 - 1);
        generalPath.lineTo((float)n + f, n4 - 1);
        generalPath.quadTo((float)n + f2, (float)(n4 - 1) - f2, n, (float)n4 - f - 1.0f);
        generalPath.lineTo(n, (float)n3 + f);
        generalPath.quadTo((float)n + f2, (float)n3 + f2, (float)n + f, n3);
        return generalPath;
    }

    public static String clipString(FontMetrics fontMetrics, int n, String string) {
        if (fontMetrics.stringWidth(string) <= n) {
            return string;
        }
        String string2 = "...";
        int n2 = fontMetrics.stringWidth(string2);
        if (n2 > n) {
            return "";
        }
        String string3 = "";
        int n3 = string.length();
        String string4 = "";
        for (int i = 0; i < n3; ++i) {
            String string5 = string3 + string.charAt(i);
            String string6 = string5 + string2;
            if (fontMetrics.stringWidth(string6) > n) {
                return string4;
            }
            string3 = string5;
            string4 = string6;
        }
        return string;
    }

    public static BufferedImage getBlankImage(int n, int n2) {
        GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice graphicsDevice = graphicsEnvironment.getDefaultScreenDevice();
        GraphicsConfiguration graphicsConfiguration = graphicsDevice.getDefaultConfiguration();
        BufferedImage bufferedImage = graphicsConfiguration.createCompatibleImage(n, n2, 3);
        return bufferedImage;
    }

    public static Color getAlphaColor(Color color, int n) {
        return new Color(color.getRed(), color.getGreen(), color.getBlue(), n);
    }

    public static int getHLayoutGap(AbstractCommandButton abstractCommandButton) {
        Font font = abstractCommandButton.getFont();
        if (font == null) {
            font = UIManager.getFont("Button.font");
        }
        return (int)Math.ceil(abstractCommandButton.getHGapScaleFactor() * (double)(font.getSize() - 4) / 4.0);
    }

    public static int getVLayoutGap(AbstractCommandButton abstractCommandButton) {
        Font font = abstractCommandButton.getFont();
        if (font == null) {
            font = UIManager.getFont("Button.font");
        }
        return (int)Math.ceil(abstractCommandButton.getVGapScaleFactor() * (double)(font.getSize() - 4) / 4.0);
    }

    public static boolean hasPopupAction(AbstractCommandButton abstractCommandButton) {
        if (abstractCommandButton instanceof JCommandButton) {
            JCommandButton jCommandButton = (JCommandButton)abstractCommandButton;
            return jCommandButton.getCommandButtonKind().hasPopup();
        }
        return false;
    }

    public static void updateRibbonFrameIconImages(JRibbonFrame jRibbonFrame) {
        JRibbonApplicationMenuButton jRibbonApplicationMenuButton = FlamingoUtilities.getApplicationMenuButton(jRibbonFrame);
        if (jRibbonApplicationMenuButton == null) {
            return;
        }
        ResizableIcon resizableIcon = jRibbonFrame.getApplicationIcon();
        if (resizableIcon != null) {
            jRibbonApplicationMenuButton.setIcon(resizableIcon);
        }
    }

    public static JRibbonApplicationMenuButton getApplicationMenuButton(Component component) {
        if (component instanceof JRibbonApplicationMenuButton) {
            return (JRibbonApplicationMenuButton)component;
        }
        if (component instanceof Container) {
            Container container = (Container)component;
            for (int i = 0; i < container.getComponentCount(); ++i) {
                JRibbonApplicationMenuButton jRibbonApplicationMenuButton = FlamingoUtilities.getApplicationMenuButton(container.getComponent(i));
                if (jRibbonApplicationMenuButton == null || !jRibbonApplicationMenuButton.isVisible()) continue;
                return jRibbonApplicationMenuButton;
            }
        }
        return null;
    }

    public static void renderSurface(Graphics graphics, Container container, Rectangle rectangle, boolean bl, boolean bl2, boolean bl3) {
        CellRendererPane cellRendererPane = new CellRendererPane();
        JButton jButton = new JButton("");
        jButton.getModel().setRollover(bl);
        cellRendererPane.setBounds(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        graphics2D.clipRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        cellRendererPane.paintComponent(graphics2D, jButton, container, rectangle.x - rectangle.width / 2, rectangle.y - rectangle.height / 2, 2 * rectangle.width, 2 * rectangle.height, true);
        graphics2D.setColor(FlamingoUtilities.getBorderColor());
        if (bl2) {
            graphics2D.drawLine(rectangle.x, rectangle.y, rectangle.x + rectangle.width - 1, rectangle.y);
        }
        if (bl3) {
            graphics2D.drawLine(rectangle.x, rectangle.y + rectangle.height - 1, rectangle.x + rectangle.width - 1, rectangle.y + rectangle.height - 1);
        }
        graphics2D.dispose();
    }

    public static Color getLighterColor(Color color, double d) {
        int n = color.getRed() + (int)(d * (double)(255 - color.getRed()));
        int n2 = color.getGreen() + (int)(d * (double)(255 - color.getGreen()));
        int n3 = color.getBlue() + (int)(d * (double)(255 - color.getBlue()));
        return new Color(n, n2, n3);
    }

    public static Color getBorderColor() {
        return FlamingoUtilities.getColor(Color.gray, "TextField.inactiveForeground", "Button.disabledText", "ComboBox.disabledForeground");
    }

    public static boolean isShowingMinimizedRibbonInPopup(JRibbon jRibbon) {
        List<PopupPanelManager.PopupInfo> list = PopupPanelManager.defaultManager().getShownPath();
        if (list.size() == 0) {
            return false;
        }
        for (PopupPanelManager.PopupInfo popupInfo : list) {
            JComponent jComponent = popupInfo.getPopupOriginator();
            if (!(jComponent instanceof JRibbonTaskToggleButton)) continue;
            return jRibbon == SwingUtilities.getAncestorOfClass(JRibbon.class, jComponent);
        }
        return false;
    }

    public static boolean isShowingMinimizedRibbonInPopup(JRibbonTaskToggleButton jRibbonTaskToggleButton) {
        List<PopupPanelManager.PopupInfo> list = PopupPanelManager.defaultManager().getShownPath();
        if (list.size() == 0) {
            return false;
        }
        for (PopupPanelManager.PopupInfo popupInfo : list) {
            JComponent jComponent = popupInfo.getPopupOriginator();
            if (jComponent != jRibbonTaskToggleButton) continue;
            return true;
        }
        return false;
    }

    public static void checkResizePoliciesConsistency(AbstractRibbonBand abstractRibbonBand) {
        Insets insets = abstractRibbonBand.getInsets();
        Object t = abstractRibbonBand.getControlPanel();
        if (t == null) {
            return;
        }
        int n = t.getPreferredSize().height + abstractRibbonBand.getUI().getBandTitleHeight() + insets.top + insets.bottom;
        List<RibbonBandResizePolicy> list = abstractRibbonBand.getResizePolicies();
        FlamingoUtilities.checkResizePoliciesConsistencyBase(abstractRibbonBand);
        for (int i = 0; i < list.size() - 1; ++i) {
            int n2;
            RibbonBandResizePolicy ribbonBandResizePolicy = list.get(i);
            RibbonBandResizePolicy ribbonBandResizePolicy2 = list.get(i + 1);
            int n3 = ribbonBandResizePolicy.getPreferredWidth(n, 4);
            if (n3 >= (n2 = ribbonBandResizePolicy2.getPreferredWidth(n, 4))) continue;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Inconsistent preferred widths\n");
            stringBuilder.append("Ribbon band '" + abstractRibbonBand.getTitle() + "' has the following resize policies\n");
            for (int j = 0; j < list.size(); ++j) {
                RibbonBandResizePolicy ribbonBandResizePolicy3 = list.get(j);
                int n4 = ribbonBandResizePolicy3.getPreferredWidth(n, 4);
                stringBuilder.append("\t" + ribbonBandResizePolicy3.getClass().getName() + " with preferred width " + n4 + "\n");
            }
            stringBuilder.append(ribbonBandResizePolicy.getClass().getName() + " with pref width " + n3 + " is followed by resize policy " + ribbonBandResizePolicy2.getClass().getName() + " with larger pref width\n");
            throw new IllegalStateException(stringBuilder.toString());
        }
    }

    public static void checkResizePoliciesConsistencyBase(AbstractRibbonBand abstractRibbonBand) {
        List<RibbonBandResizePolicy> list = abstractRibbonBand.getResizePolicies();
        if (list.size() == 0) {
            throw new IllegalStateException("Resize policy list is empty");
        }
        for (int i = 0; i < list.size(); ++i) {
            RibbonBandResizePolicy ribbonBandResizePolicy = list.get(i);
            boolean bl = ribbonBandResizePolicy instanceof IconRibbonBandResizePolicy;
            if (!bl || i >= list.size() - 1) continue;
            throw new IllegalStateException("Icon resize policy must be the last in the list");
        }
    }

}

