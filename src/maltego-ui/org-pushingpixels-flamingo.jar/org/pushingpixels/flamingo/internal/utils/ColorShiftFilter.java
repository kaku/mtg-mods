/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.utils;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import org.pushingpixels.flamingo.internal.utils.AbstractFilter;

public class ColorShiftFilter
extends AbstractFilter {
    int rShift;
    int gShift;
    int bShift;
    double hueShiftAmount;

    public ColorShiftFilter(Color color, double d) {
        this.rShift = color.getRed();
        this.gShift = color.getGreen();
        this.bShift = color.getBlue();
        this.hueShiftAmount = d;
    }

    @Override
    public BufferedImage filter(BufferedImage bufferedImage, BufferedImage bufferedImage2) {
        if (bufferedImage2 == null) {
            bufferedImage2 = this.createCompatibleDestImage(bufferedImage, null);
        }
        int n = bufferedImage.getWidth();
        int n2 = bufferedImage.getHeight();
        int[] arrn = new int[n * n2];
        this.getPixels(bufferedImage, 0, 0, n, n2, arrn);
        this.shiftColor(arrn);
        this.setPixels(bufferedImage2, 0, 0, n, n2, arrn);
        return bufferedImage2;
    }

    private void shiftColor(int[] arrn) {
        for (int i = 0; i < arrn.length; ++i) {
            int n = arrn[i];
            int n2 = n >>> 16 & 255;
            int n3 = n >>> 8 & 255;
            int n4 = n >>> 0 & 255;
            int n5 = (int)(this.hueShiftAmount * (double)this.rShift + (1.0 - this.hueShiftAmount) * (double)n2);
            int n6 = (int)(this.hueShiftAmount * (double)this.gShift + (1.0 - this.hueShiftAmount) * (double)n3);
            int n7 = (int)(this.hueShiftAmount * (double)this.bShift + (1.0 - this.hueShiftAmount) * (double)n4);
            arrn[i] = n & -16777216 | n5 << 16 | n6 << 8 | n7;
        }
    }
}

