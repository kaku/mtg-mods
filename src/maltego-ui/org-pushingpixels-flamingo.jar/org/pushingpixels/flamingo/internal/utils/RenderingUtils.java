/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.utils;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.PrintGraphics;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.print.PrinterGraphics;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.swing.JComponent;
import sun.swing.SwingUtilities2;

public class RenderingUtils {
    private static final String PROP_DESKTOPHINTS = "awt.font.desktophints";

    public static Map installDesktopHints(Graphics2D graphics2D) {
        HashMap<RenderingHints.Key, Object> hashMap = null;
        Map map = RenderingUtils.desktopHints(graphics2D);
        if (map != null && !map.isEmpty()) {
            hashMap = new HashMap<RenderingHints.Key, Object>(map.size());
            for (RenderingHints.Key key : map.keySet()) {
                hashMap.put(key, graphics2D.getRenderingHint(key));
            }
            graphics2D.addRenderingHints(map);
        }
        return hashMap;
    }

    private static Map desktopHints(Graphics2D graphics2D) {
        Object v;
        if (RenderingUtils.isPrinting(graphics2D)) {
            return null;
        }
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        GraphicsDevice graphicsDevice = graphics2D.getDeviceConfiguration().getDevice();
        Map map = (Map)toolkit.getDesktopProperty("awt.font.desktophints." + graphicsDevice.getIDstring());
        if (map == null) {
            map = (Map)toolkit.getDesktopProperty("awt.font.desktophints");
        }
        if (map != null && ((v = map.get(RenderingHints.KEY_TEXT_ANTIALIASING)) == RenderingHints.VALUE_TEXT_ANTIALIAS_OFF || v == RenderingHints.VALUE_TEXT_ANTIALIAS_DEFAULT)) {
            map = null;
        }
        return map;
    }

    private static boolean isPrinting(Graphics graphics) {
        return graphics instanceof PrintGraphics || graphics instanceof PrinterGraphics;
    }

    public static void setupTextAntialiasing(Graphics graphics, JComponent jComponent) {
        RenderingUtils.setupTextAntialiasing(graphics, jComponent, false);
    }

    public static void setupTextAntialiasing(Graphics graphics, JComponent jComponent, boolean bl) {
        if (graphics instanceof Graphics2D) {
            Graphics2D graphics2D = (Graphics2D)graphics;
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Map map = (Map)toolkit.getDesktopProperty("awt.font.desktophints");
            if (map != null && !bl) {
                graphics2D.addRenderingHints(map);
            }
            if (jComponent != null) {
                jComponent.putClientProperty(SwingUtilities2.AA_TEXT_PROPERTY_KEY, null);
            }
        }
    }
}

