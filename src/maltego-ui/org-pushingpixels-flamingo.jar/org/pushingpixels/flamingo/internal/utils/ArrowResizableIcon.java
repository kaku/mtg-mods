/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.utils;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.GeneralPath;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.model.PopupButtonModel;

public class ArrowResizableIcon
implements ResizableIcon {
    private Dimension initialDim;
    protected int width;
    protected int height;
    protected int direction;

    public ArrowResizableIcon(Dimension dimension, int n) {
        this.initialDim = dimension;
        this.width = dimension.width;
        this.height = dimension.height;
        this.direction = n;
    }

    public ArrowResizableIcon(int n, int n2) {
        this(new Dimension(n, n), n2);
    }

    public void revertToOriginalDimension() {
        this.width = this.initialDim.width;
        this.height = this.initialDim.height;
    }

    @Override
    public void setDimension(Dimension dimension) {
        this.width = dimension.width;
        this.height = dimension.height;
    }

    @Override
    public int getIconHeight() {
        return this.height;
    }

    @Override
    public int getIconWidth() {
        return this.width;
    }

    protected boolean toPaintEnabled(Component component) {
        return component.isEnabled();
    }

    @Override
    public void paintIcon(Component component, Graphics graphics, int n, int n2) {
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        float f = (float)this.width / 7.0f;
        if (f < 1.0f) {
            f = 1.0f;
        }
        BasicStroke basicStroke = new BasicStroke(f, 0, 0);
        graphics2D.setStroke(basicStroke);
        GeneralPath generalPath = new GeneralPath();
        switch (this.direction) {
            case 5: {
                generalPath.moveTo(0.0f, 2.0f);
                generalPath.lineTo(0.5f * (float)(this.width - 1), this.height - 2);
                generalPath.lineTo(this.width - 1, 2.0f);
                break;
            }
            case 1: {
                generalPath.moveTo(0.0f, this.height - 2);
                generalPath.lineTo(0.5f * (float)(this.width - 1), 2.0f);
                generalPath.lineTo(this.width - 1, this.height - 2);
                break;
            }
            case 3: {
                generalPath.moveTo(2.0f, 0.0f);
                generalPath.lineTo(this.width - 2, 0.5f * (float)(this.height - 1));
                generalPath.lineTo(2.0f, this.height - 1);
                break;
            }
            case 7: {
                generalPath.moveTo(this.width - 2, 0.0f);
                generalPath.lineTo(2.0f, 0.5f * (float)(this.height - 1));
                generalPath.lineTo(this.width - 2, this.height - 1);
            }
        }
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.translate(n, n2 + 1);
        Color color = this.toPaintEnabled(component) ? new Color(255, 255, 255, 196) : new Color(255, 255, 255, 32);
        graphics2D.setColor(color);
        graphics2D.draw(generalPath);
        graphics2D.translate(0, -1);
        Color color2 = this.toPaintEnabled(component) ? Color.black : Color.gray;
        graphics2D.setColor(color2);
        if (this.width < 9) {
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
            graphics2D.draw(generalPath);
        }
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.draw(generalPath);
        graphics2D.dispose();
    }

    public static class CommandButtonPopupIcon
    extends ArrowResizableIcon {
        public CommandButtonPopupIcon(int n, int n2) {
            super(n, n2);
        }

        @Override
        protected boolean toPaintEnabled(Component component) {
            JCommandButton jCommandButton = (JCommandButton)component;
            return jCommandButton.isEnabled() && jCommandButton.getPopupModel().isEnabled();
        }
    }

}

