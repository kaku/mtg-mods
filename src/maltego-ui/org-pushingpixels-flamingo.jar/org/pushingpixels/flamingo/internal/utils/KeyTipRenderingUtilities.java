/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.utils;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.font.LineMetrics;
import java.awt.geom.RoundRectangle2D;
import java.util.Collection;
import javax.swing.CellRendererPane;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.UIManager;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;
import org.pushingpixels.flamingo.api.common.model.PopupButtonModel;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonUI;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;
import org.pushingpixels.flamingo.internal.utils.KeyTipManager;

public class KeyTipRenderingUtilities {
    private static int INSETS = 3;

    public static Dimension getPrefSize(FontMetrics fontMetrics, String string) {
        int n = fontMetrics.stringWidth(string) + 2 * INSETS + 1;
        int n2 = fontMetrics.getHeight() + INSETS - 1;
        return new Dimension(n, n2);
    }

    public static void renderKeyTip(Graphics graphics, Container container, Rectangle rectangle, String string, boolean bl) {
        CellRendererPane cellRendererPane = new CellRendererPane();
        JButton jButton = new JButton("");
        jButton.setEnabled(bl);
        cellRendererPane.setBounds(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        graphics2D.setComposite(AlphaComposite.SrcOver.derive(bl ? 1.0f : 0.5f));
        Shape shape = graphics2D.getClip();
        RoundRectangle2D.Double double_ = new RoundRectangle2D.Double(rectangle.x, rectangle.y, rectangle.width - 1, rectangle.height - 1, 0.0, 0.0);
        graphics2D.clip(double_);
        cellRendererPane.paintComponent(graphics2D, jButton, container, rectangle.x - rectangle.width / 2, rectangle.y - rectangle.height / 2, 2 * rectangle.width, 2 * rectangle.height, true);
        graphics2D.setClip(shape);
        graphics2D.setColor(FlamingoUtilities.getBorderColor());
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.draw(double_);
        graphics2D.setColor(FlamingoUtilities.getColor(Color.black, "Button.foreground"));
        Font font = UIManager.getFont("Button.font");
        font = font.deriveFont((float)font.getSize() + 1.0f);
        graphics2D.setFont(font);
        int n = graphics2D.getFontMetrics().stringWidth(string);
        graphics2D.translate(rectangle.x, rectangle.y);
        LineMetrics lineMetrics = graphics2D.getFontMetrics().getLineMetrics(string, graphics2D);
        int n2 = (int)lineMetrics.getHeight();
        graphics2D.drawString(string, (rectangle.width - n + 1) / 2, (rectangle.height + n2) / 2 - graphics2D.getFontMetrics().getDescent() + 1);
        graphics2D.dispose();
    }

    public static void renderMenuButtonKeyTips(Graphics graphics, JCommandMenuButton jCommandMenuButton, CommandButtonLayoutManager commandButtonLayoutManager) {
        Dimension dimension;
        Collection<KeyTipManager.KeyTipLink> collection = KeyTipManager.defaultManager().getCurrentlyShownKeyTips();
        if (collection == null) {
            return;
        }
        boolean bl = false;
        for (KeyTipManager.KeyTipLink object2 : collection) {
            bl = object2.comp == jCommandMenuButton;
            if (!bl) continue;
            break;
        }
        if (!bl) {
            return;
        }
        String string = jCommandMenuButton.getActionKeyTip();
        String string2 = jCommandMenuButton.getPopupKeyTip();
        CommandButtonLayoutManager.CommandButtonLayoutInfo commandButtonLayoutInfo = commandButtonLayoutManager.getLayoutInfo(jCommandMenuButton, graphics);
        Point point = jCommandMenuButton.getUI().getKeyTipAnchorCenterPoint();
        if (commandButtonLayoutInfo.iconRect.width > 0 && string != null) {
            dimension = KeyTipRenderingUtilities.getPrefSize(graphics.getFontMetrics(), string);
            KeyTipRenderingUtilities.renderKeyTip(graphics, jCommandMenuButton, new Rectangle(point.x - dimension.width / 2, Math.min(point.y - dimension.height / 2, commandButtonLayoutInfo.actionClickArea.y + commandButtonLayoutInfo.actionClickArea.height - dimension.height), dimension.width, dimension.height), string, jCommandMenuButton.getActionModel().isEnabled());
        }
        if (commandButtonLayoutInfo.popupClickArea.width > 0 && string2 != null) {
            dimension = KeyTipRenderingUtilities.getPrefSize(graphics.getFontMetrics(), string2);
            if (jCommandMenuButton.getPopupOrientationKind() == JCommandButton.CommandButtonPopupOrientationKind.SIDEWARD) {
                if (jCommandMenuButton.getCommandButtonKind() != JCommandButton.CommandButtonKind.POPUP_ONLY) {
                    KeyTipRenderingUtilities.renderKeyTip(graphics, jCommandMenuButton, new Rectangle(commandButtonLayoutInfo.popupClickArea.x + commandButtonLayoutInfo.popupClickArea.width - dimension.width - 4, Math.min(point.y - dimension.height / 2, commandButtonLayoutInfo.actionClickArea.y + commandButtonLayoutInfo.actionClickArea.height - dimension.height), dimension.width, dimension.height), string2, jCommandMenuButton.getPopupModel().isEnabled());
                } else {
                    KeyTipRenderingUtilities.renderKeyTip(graphics, jCommandMenuButton, new Rectangle(point.x - dimension.width / 2, Math.min(point.y - dimension.height / 2, commandButtonLayoutInfo.popupClickArea.y + commandButtonLayoutInfo.popupClickArea.height - dimension.height), dimension.width, dimension.height), string2, jCommandMenuButton.getPopupModel().isEnabled());
                }
            } else {
                KeyTipRenderingUtilities.renderKeyTip(graphics, jCommandMenuButton, new Rectangle((commandButtonLayoutInfo.popupClickArea.x + commandButtonLayoutInfo.popupClickArea.width - dimension.width) / 2, commandButtonLayoutInfo.popupClickArea.y + commandButtonLayoutInfo.popupClickArea.height - dimension.height, dimension.width, dimension.height), string2, jCommandMenuButton.getPopupModel().isEnabled());
            }
        }
    }
}

