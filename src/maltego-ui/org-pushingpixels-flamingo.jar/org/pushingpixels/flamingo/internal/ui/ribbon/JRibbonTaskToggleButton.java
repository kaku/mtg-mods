/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon;

import java.awt.Color;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.JCommandToggleButton;
import org.pushingpixels.flamingo.api.ribbon.RibbonTask;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.BasicRibbonTaskToggleButtonUI;

public class JRibbonTaskToggleButton
extends JCommandToggleButton {
    public static final String uiClassID = "RibbonTaskToggleButtonUI";
    private Color contextualGroupHueColor;
    private String keyTip;
    private RibbonTask ribbonTask;

    public JRibbonTaskToggleButton(RibbonTask ribbonTask) {
        super(ribbonTask.getTitle());
    }

    @Override
    public void updateUI() {
        if (UIManager.get(this.getUIClassID()) != null) {
            this.setUI(UIManager.getUI(this));
        } else {
            this.setUI(new BasicRibbonTaskToggleButtonUI());
        }
    }

    @Override
    public String getUIClassID() {
        return "RibbonTaskToggleButtonUI";
    }

    public Color getContextualGroupHueColor() {
        return this.contextualGroupHueColor;
    }

    public RibbonTask getRibbonTask() {
        return this.ribbonTask;
    }

    public void setContextualGroupHueColor(Color color) {
        Color color2 = this.contextualGroupHueColor;
        this.contextualGroupHueColor = color;
        this.firePropertyChange("contextualGroupHueColor", color2, this.contextualGroupHueColor);
    }

    public void setKeyTip(String string) {
        this.keyTip = string;
    }

    public String getKeyTip() {
        return this.keyTip;
    }
}

