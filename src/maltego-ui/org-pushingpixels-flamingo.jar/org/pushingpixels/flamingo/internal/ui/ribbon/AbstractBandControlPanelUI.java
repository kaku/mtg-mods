/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import javax.swing.JComponent;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.UIResource;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.icon.EmptyResizableIcon;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.BandControlPanelUI;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

abstract class AbstractBandControlPanelUI
extends BandControlPanelUI {
    protected AbstractBandControlPanel controlPanel;
    protected JCommandButton dummy;
    public static final String TOP_ROW = "flamingo.internal.ribbonBandControlPanel.topRow";
    public static final String MID_ROW = "flamingo.internal.ribbonBandControlPanel.midRow";
    public static final String BOTTOM_ROW = "flamingo.internal.ribbonBandControlPanel.bottomRow";

    AbstractBandControlPanelUI() {
    }

    @Override
    public void installUI(JComponent jComponent) {
        this.controlPanel = (AbstractBandControlPanel)jComponent;
        this.dummy = new JCommandButton("Dummy", new EmptyResizableIcon(16));
        this.dummy.setDisplayState(CommandButtonDisplayState.BIG);
        this.dummy.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_ACTION);
        this.installDefaults();
        this.installComponents();
        this.installListeners();
        jComponent.setLayout(this.createLayoutManager());
    }

    @Override
    public void uninstallUI(JComponent jComponent) {
        this.uninstallListeners();
        this.uninstallComponents();
        this.uninstallDefaults();
        jComponent.setLayout(null);
        this.controlPanel = null;
    }

    protected void installListeners() {
    }

    protected void uninstallListeners() {
    }

    protected void installComponents() {
    }

    protected void uninstallComponents() {
    }

    protected void installDefaults() {
        Border border;
        Color color = this.controlPanel.getBackground();
        if (color == null || color instanceof UIResource) {
            this.controlPanel.setBackground(FlamingoUtilities.getColor(Color.lightGray, "ControlPanel.background", "Panel.background"));
        }
        if ((border = this.controlPanel.getBorder()) == null || border instanceof UIResource) {
            Border border2 = UIManager.getBorder("ControlPanel.border");
            if (border2 == null) {
                new BorderUIResource.EmptyBorderUIResource(1, 2, 1, 2);
            }
            this.controlPanel.setBorder(border2);
        }
    }

    protected void uninstallDefaults() {
        LookAndFeel.uninstallBorder(this.controlPanel);
    }

    protected abstract LayoutManager createLayoutManager();

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        this.paintBandBackground(graphics2D, new Rectangle(0, 0, jComponent.getWidth(), jComponent.getHeight()));
        graphics2D.dispose();
    }

    protected void paintBandBackground(Graphics graphics, Rectangle rectangle) {
        graphics.setColor(this.controlPanel.getBackground());
        graphics.fillRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
    }

    @Override
    public int getLayoutGap() {
        return 4;
    }
}

