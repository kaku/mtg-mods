/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.GeneralPath;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.Popup;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.UIResource;
import org.pushingpixels.flamingo.BlankPopupFixPopupFactory;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButtonPanel;
import org.pushingpixels.flamingo.api.common.JCommandButtonStrip;
import org.pushingpixels.flamingo.api.common.JCommandToggleButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;
import org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelManager;
import org.pushingpixels.flamingo.api.ribbon.JRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.RibbonElementPriority;
import org.pushingpixels.flamingo.internal.ui.ribbon.JRibbonGallery;
import org.pushingpixels.flamingo.internal.ui.ribbon.RibbonGalleryUI;
import org.pushingpixels.flamingo.internal.utils.ArrowResizableIcon;
import org.pushingpixels.flamingo.internal.utils.DoubleArrowResizableIcon;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;
import org.pushingpixels.flamingo.internal.utils.KeyTipManager;

public class BasicRibbonGalleryUI
extends RibbonGalleryUI {
    protected JRibbonGallery ribbonGallery;
    protected int firstVisibleButtonIndex;
    protected int visibleButtonsInEachRow;
    protected int visibleButtonRowNumber;
    protected JCommandButton scrollDownButton;
    protected JCommandButton scrollUpButton;
    protected ExpandCommandButton expandActionButton;
    protected JCommandButtonStrip buttonStrip;
    protected ActionListener scrollDownListener;
    protected ActionListener scrollUpListener;
    protected ActionListener expandListener;
    protected PopupPanelManager.PopupListener popupListener;
    protected PropertyChangeListener propertyChangeListener;
    protected Insets margin;

    public static ComponentUI createUI(JComponent jComponent) {
        return new BasicRibbonGalleryUI();
    }

    @Override
    public void installUI(JComponent jComponent) {
        this.ribbonGallery = (JRibbonGallery)jComponent;
        this.firstVisibleButtonIndex = 0;
        this.installDefaults();
        this.installComponents();
        this.installListeners();
        jComponent.setLayout(this.createLayoutManager());
    }

    protected void installComponents() {
        this.buttonStrip = new JButtonStripUIResource(JCommandButtonStrip.StripOrientation.VERTICAL);
        this.buttonStrip.setDisplayState(CommandButtonDisplayState.FIT_TO_ICON);
        this.ribbonGallery.add(this.buttonStrip);
        this.scrollUpButton = this.createScrollUpButton();
        this.scrollDownButton = this.createScrollDownButton();
        this.expandActionButton = this.createExpandButton();
        this.syncExpandKeyTip();
        this.buttonStrip.add(this.scrollUpButton);
        this.buttonStrip.add(this.scrollDownButton);
        this.buttonStrip.add(this.expandActionButton);
    }

    protected JCommandButton createScrollDownButton() {
        JCommandButton jCommandButton = new JCommandButton(new ArrowResizableIcon(9, 5));
        jCommandButton.setFocusable(false);
        jCommandButton.setName("RibbonGallery.scrollDownButton");
        jCommandButton.setFlat(false);
        jCommandButton.putClientProperty("flamingo.internal.commandButton.ui.dontDisposePopups", Boolean.TRUE);
        jCommandButton.setAutoRepeatAction(true);
        return jCommandButton;
    }

    protected JCommandButton createScrollUpButton() {
        JCommandButton jCommandButton = new JCommandButton(new ArrowResizableIcon(9, 1));
        jCommandButton.setFocusable(false);
        jCommandButton.setName("RibbonGallery.scrollUpButton");
        jCommandButton.setFlat(false);
        jCommandButton.putClientProperty("flamingo.internal.commandButton.ui.dontDisposePopups", Boolean.TRUE);
        jCommandButton.setAutoRepeatAction(true);
        return jCommandButton;
    }

    protected ExpandCommandButton createExpandButton() {
        ExpandCommandButton expandCommandButton = new ExpandCommandButton(new DoubleArrowResizableIcon(9, 5));
        expandCommandButton.getActionModel().setFireActionOnPress(true);
        expandCommandButton.setFocusable(false);
        expandCommandButton.setName("RibbonGallery.expandButton");
        expandCommandButton.setFlat(false);
        expandCommandButton.putClientProperty("flamingo.internal.commandButton.ui.dontDisposePopups", Boolean.TRUE);
        return expandCommandButton;
    }

    protected void uninstallComponents() {
        this.buttonStrip.remove(this.scrollUpButton);
        this.buttonStrip.remove(this.scrollDownButton);
        this.buttonStrip.remove(this.expandActionButton);
        this.ribbonGallery.remove(this.buttonStrip);
    }

    protected void installDefaults() {
        Border border;
        this.margin = UIManager.getInsets("RibbonGallery.margin");
        if (this.margin == null) {
            this.margin = new Insets(3, 3, 3, 3);
        }
        if ((border = this.ribbonGallery.getBorder()) == null || border instanceof UIResource) {
            Border border2 = UIManager.getBorder("RibbonGallery.border");
            if (border2 == null) {
                border2 = new BorderUIResource.EmptyBorderUIResource(2, 2, 2, 2);
            }
            this.ribbonGallery.setBorder(border2);
        }
        this.ribbonGallery.setOpaque(false);
    }

    protected void uninstallDefaults() {
    }

    protected void installListeners() {
        this.scrollDownListener = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                BasicRibbonGalleryUI.this.scrollOneRowDown();
                BasicRibbonGalleryUI.this.ribbonGallery.revalidate();
            }
        };
        this.scrollDownButton.addActionListener(this.scrollDownListener);
        this.scrollUpListener = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                BasicRibbonGalleryUI.this.scrollOneRowUp();
                BasicRibbonGalleryUI.this.ribbonGallery.revalidate();
            }
        };
        this.scrollUpButton.addActionListener(this.scrollUpListener);
        this.expandListener = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                PopupPanelManager.defaultManager().hidePopups(BasicRibbonGalleryUI.this.ribbonGallery);
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        JCommandButtonPanel jCommandButtonPanel = BasicRibbonGalleryUI.this.ribbonGallery.getPopupButtonPanel();
                        final Point point = BasicRibbonGalleryUI.this.ribbonGallery.getLocationOnScreen();
                        final JCommandPopupMenu jCommandPopupMenu = new JCommandPopupMenu(jCommandButtonPanel, BasicRibbonGalleryUI.this.ribbonGallery.getPreferredPopupMaxButtonColumns(), BasicRibbonGalleryUI.this.ribbonGallery.getPreferredPopupMaxVisibleButtonRows());
                        if (BasicRibbonGalleryUI.this.ribbonGallery.getPopupCallback() != null) {
                            BasicRibbonGalleryUI.this.ribbonGallery.getPopupCallback().popupToBeShown(jCommandPopupMenu);
                        }
                        jCommandPopupMenu.applyComponentOrientation(BasicRibbonGalleryUI.this.ribbonGallery.getComponentOrientation());
                        jCommandPopupMenu.setCustomizer(new JPopupPanel.PopupPanelCustomizer(){

                            @Override
                            public Rectangle getScreenBounds() {
                                Rectangle rectangle = BasicRibbonGalleryUI.this.ribbonGallery.getGraphicsConfiguration().getBounds();
                                boolean bl = jCommandPopupMenu.getComponentOrientation().isLeftToRight();
                                Dimension dimension = jCommandPopupMenu.getPreferredSize();
                                int n = Math.max(dimension.width, BasicRibbonGalleryUI.this.ribbonGallery.getWidth());
                                int n2 = dimension.height;
                                int n3 = bl ? point.x : point.x + n - dimension.width;
                                int n4 = point.y;
                                if (n3 + n > rectangle.x + rectangle.width) {
                                    n3 = rectangle.x + rectangle.width - n;
                                }
                                if (n4 + n2 > rectangle.y + rectangle.height) {
                                    n4 = rectangle.y + rectangle.height - n2;
                                }
                                return new Rectangle(n3, n4, n, n2);
                            }
                        });
                        BasicRibbonGalleryUI.this.ribbonGallery.setShowingPopupPanel(true);
                        Dimension dimension = jCommandPopupMenu.getPreferredSize();
                        int n = Math.max(dimension.width, BasicRibbonGalleryUI.this.ribbonGallery.getWidth());
                        boolean bl = BasicRibbonGalleryUI.this.ribbonGallery.getComponentOrientation().isLeftToRight();
                        int n2 = bl ? point.x : point.x + BasicRibbonGalleryUI.this.ribbonGallery.getWidth() - n;
                        Popup popup = BlankPopupFixPopupFactory.getPopup(jCommandPopupMenu, n2, point.y);
                        BasicRibbonGalleryUI.this.ribbonGallery.repaint();
                        PopupPanelManager.defaultManager().addPopup(BasicRibbonGalleryUI.this.ribbonGallery, popup, jCommandPopupMenu);
                        if (jCommandButtonPanel.getSelectedButton() != null) {
                            Rectangle rectangle = jCommandButtonPanel.getSelectedButton().getBounds();
                            jCommandButtonPanel.scrollRectToVisible(rectangle);
                        }
                    }

                });
            }

        };
        this.expandActionButton.addActionListener(this.expandListener);
        this.popupListener = new PopupPanelManager.PopupListener(){

            @Override
            public void popupHidden(PopupPanelManager.PopupEvent popupEvent) {
                if (popupEvent.getPopupOriginator() == BasicRibbonGalleryUI.this.ribbonGallery) {
                    for (int i = 0; i < BasicRibbonGalleryUI.this.ribbonGallery.getButtonCount(); ++i) {
                        BasicRibbonGalleryUI.this.ribbonGallery.getButtonAt(i).getActionModel().setRollover(false);
                    }
                    BasicRibbonGalleryUI.this.ribbonGallery.setShowingPopupPanel(false);
                }
            }

            @Override
            public void popupShown(PopupPanelManager.PopupEvent popupEvent) {
            }
        };
        PopupPanelManager.defaultManager().addPopupListener(this.popupListener);
        this.propertyChangeListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("selectedButton".equals(propertyChangeEvent.getPropertyName())) {
                    BasicRibbonGalleryUI.this.scrollToSelected();
                    BasicRibbonGalleryUI.this.ribbonGallery.revalidate();
                }
                if ("expandKeyTip".equals(propertyChangeEvent.getPropertyName())) {
                    BasicRibbonGalleryUI.this.syncExpandKeyTip();
                }
                if ("buttonDisplayState".equals(propertyChangeEvent.getPropertyName())) {
                    BasicRibbonGalleryUI.this.firstVisibleButtonIndex = 0;
                    BasicRibbonGalleryUI.this.ribbonGallery.revalidate();
                }
            }
        };
        this.ribbonGallery.addPropertyChangeListener(this.propertyChangeListener);
    }

    protected void uninstallListeners() {
        this.scrollDownButton.removeActionListener(this.scrollDownListener);
        this.scrollDownListener = null;
        this.scrollUpButton.removeActionListener(this.scrollUpListener);
        this.scrollUpListener = null;
        this.expandActionButton.removeActionListener(this.expandListener);
        this.expandListener = null;
        PopupPanelManager.defaultManager().removePopupListener(this.popupListener);
        this.popupListener = null;
        this.ribbonGallery.removePropertyChangeListener(this.propertyChangeListener);
        this.propertyChangeListener = null;
    }

    @Override
    public void uninstallUI(JComponent jComponent) {
        jComponent.setLayout(null);
        this.uninstallListeners();
        this.uninstallDefaults();
        this.uninstallComponents();
        this.ribbonGallery = null;
    }

    protected LayoutManager createLayoutManager() {
        return new RibbonGalleryLayout();
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        this.paintRibbonGalleryBorder(graphics2D);
        graphics2D.dispose();
    }

    protected void paintRibbonGalleryBorder(Graphics graphics) {
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        graphics2D.setColor(FlamingoUtilities.getBorderColor());
        GeneralPath generalPath = FlamingoUtilities.getRibbonGalleryOutline(this.margin.left, this.ribbonGallery.getWidth() - this.margin.right, this.margin.top, this.ribbonGallery.getHeight() - this.margin.bottom, 2.0f);
        if (this.ribbonGallery.getComponentOrientation().isLeftToRight()) {
            graphics2D.clipRect(0, 0, this.ribbonGallery.getWidth() - this.margin.right - this.buttonStrip.getWidth() / 2, this.ribbonGallery.getHeight());
        } else {
            graphics2D.clipRect(this.margin.left + this.buttonStrip.getWidth() / 2, 0, this.ribbonGallery.getWidth() - this.margin.left - this.buttonStrip.getWidth() / 2, this.ribbonGallery.getHeight());
        }
        graphics2D.draw(generalPath);
        graphics2D.dispose();
    }

    protected int getLayoutGap() {
        return 4;
    }

    public int getPreferredWidth(int n, int n2) {
        Insets insets = this.ribbonGallery.getInsets();
        int n3 = n2 - this.margin.top - this.margin.bottom;
        int n4 = n3 - insets.top - insets.bottom;
        int n5 = this.margin.left;
        CommandButtonDisplayState commandButtonDisplayState = this.ribbonGallery.getButtonDisplayState();
        if (commandButtonDisplayState == CommandButtonDisplayState.SMALL) {
            n5 += n * n4 / 3;
        }
        if (commandButtonDisplayState == JRibbonBand.BIG_FIXED) {
            n5 += n * n4;
        }
        if (commandButtonDisplayState == JRibbonBand.BIG_FIXED_LANDSCAPE) {
            n5 += n * n4 * 5 / 4;
        }
        n5 += (n + 1) * this.getLayoutGap();
        n5 += 15;
        return n5 += this.margin.right;
    }

    protected void scrollOneRowDown() {
        this.firstVisibleButtonIndex += this.visibleButtonsInEachRow;
    }

    protected void scrollOneRowUp() {
        this.firstVisibleButtonIndex -= this.visibleButtonsInEachRow;
    }

    protected void scrollToSelected() {
        JCommandToggleButton jCommandToggleButton = this.ribbonGallery.getSelectedButton();
        if (jCommandToggleButton == null) {
            return;
        }
        int n = -1;
        for (int i = 0; i < this.ribbonGallery.getButtonCount(); ++i) {
            if (this.ribbonGallery.getButtonAt(i) != jCommandToggleButton) continue;
            n = i;
            break;
        }
        if (n < 0) {
            return;
        }
        if (n >= this.firstVisibleButtonIndex && n < this.firstVisibleButtonIndex + this.visibleButtonRowNumber * this.visibleButtonsInEachRow) {
            return;
        }
        if (this.visibleButtonsInEachRow <= 0) {
            return;
        }
        do {
            if (n < this.firstVisibleButtonIndex) {
                this.scrollOneRowUp();
                continue;
            }
            this.scrollOneRowDown();
        } while (n < this.firstVisibleButtonIndex || n >= this.firstVisibleButtonIndex + this.visibleButtonRowNumber * this.visibleButtonsInEachRow);
    }

    protected void syncExpandKeyTip() {
        this.expandActionButton.setActionKeyTip(this.ribbonGallery.getExpandKeyTip());
    }

    @KeyTipManager.HasNextKeyTipChain
    protected static class ExpandCommandButton
    extends JCommandButton {
        public ExpandCommandButton(ResizableIcon resizableIcon) {
            super(resizableIcon);
        }
    }

    private class RibbonGalleryLayout
    implements LayoutManager {
        private RibbonGalleryLayout() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            return new Dimension(BasicRibbonGalleryUI.this.ribbonGallery.getPreferredWidth(BasicRibbonGalleryUI.this.ribbonGallery.getDisplayPriority(), container.getHeight()), container.getHeight());
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            return this.preferredLayoutSize(container);
        }

        @Override
        public void layoutContainer(Container container) {
            int n = container.getWidth();
            int n2 = container.getHeight();
            Insets insets = BasicRibbonGalleryUI.this.ribbonGallery.getInsets();
            int n3 = n2 - BasicRibbonGalleryUI.this.margin.top - BasicRibbonGalleryUI.this.margin.bottom;
            int n4 = n3 - insets.top - insets.bottom;
            BasicRibbonGalleryUI.this.visibleButtonRowNumber = 1;
            CommandButtonDisplayState commandButtonDisplayState = BasicRibbonGalleryUI.this.ribbonGallery.getButtonDisplayState();
            if (commandButtonDisplayState == CommandButtonDisplayState.SMALL) {
                n4 /= 3;
                BasicRibbonGalleryUI.this.visibleButtonRowNumber = 3;
            }
            boolean bl = container.getComponentOrientation().isLeftToRight();
            int n5 = n3 / 3;
            int n6 = 15;
            int n7 = bl ? n - n6 - BasicRibbonGalleryUI.this.margin.right : BasicRibbonGalleryUI.this.margin.left;
            BasicRibbonGalleryUI.this.scrollDownButton.setPreferredSize(new Dimension(n6, n5));
            BasicRibbonGalleryUI.this.scrollUpButton.setPreferredSize(new Dimension(n6, n5));
            BasicRibbonGalleryUI.this.expandActionButton.setPreferredSize(new Dimension(n6, n3 - 2 * n5));
            BasicRibbonGalleryUI.this.buttonStrip.setBounds(n7, BasicRibbonGalleryUI.this.margin.top, n6, n3);
            BasicRibbonGalleryUI.this.buttonStrip.doLayout();
            if (!BasicRibbonGalleryUI.this.ribbonGallery.isShowingPopupPanel()) {
                int n8;
                int n9;
                int n10;
                int n11 = n4;
                if (commandButtonDisplayState == JRibbonBand.BIG_FIXED_LANDSCAPE) {
                    n11 = n11 * 5 / 4;
                }
                for (n10 = 0; n10 < BasicRibbonGalleryUI.this.ribbonGallery.getButtonCount(); ++n10) {
                    JCommandToggleButton jCommandToggleButton = BasicRibbonGalleryUI.this.ribbonGallery.getButtonAt(n10);
                    jCommandToggleButton.setVisible(false);
                }
                n10 = BasicRibbonGalleryUI.this.getLayoutGap();
                BasicRibbonGalleryUI.this.visibleButtonsInEachRow = 0;
                int n12 = n8 = bl ? n7 - BasicRibbonGalleryUI.this.margin.left : n - n7 - n6 - BasicRibbonGalleryUI.this.margin.right;
                do {
                    if ((n9 = BasicRibbonGalleryUI.this.visibleButtonsInEachRow * n11 + (BasicRibbonGalleryUI.this.visibleButtonsInEachRow + 1) * n10) > n8) {
                        --BasicRibbonGalleryUI.this.visibleButtonsInEachRow;
                        break;
                    }
                    ++BasicRibbonGalleryUI.this.visibleButtonsInEachRow;
                } while (true);
                n9 = BasicRibbonGalleryUI.this.visibleButtonsInEachRow * n11 + (BasicRibbonGalleryUI.this.visibleButtonsInEachRow + 1) * n10;
                int n13 = bl ? BasicRibbonGalleryUI.this.margin.left + n10 : n - BasicRibbonGalleryUI.this.margin.right - n10;
                int n14 = bl ? n7 - BasicRibbonGalleryUI.this.margin.right : n - n7 - n6 - BasicRibbonGalleryUI.this.margin.left;
                int n15 = (n14 - n9) / BasicRibbonGalleryUI.this.visibleButtonsInEachRow;
                int n16 = BasicRibbonGalleryUI.this.firstVisibleButtonIndex + BasicRibbonGalleryUI.this.visibleButtonRowNumber * BasicRibbonGalleryUI.this.visibleButtonsInEachRow - 1;
                n16 = Math.min(n16, BasicRibbonGalleryUI.this.ribbonGallery.getButtonCount() - 1);
                int n17 = 0;
                int n18 = BasicRibbonGalleryUI.this.margin.top + insets.top;
                int n19 = n11 + n15;
                for (int i = BasicRibbonGalleryUI.this.firstVisibleButtonIndex; i <= n16; ++i) {
                    JCommandToggleButton jCommandToggleButton = BasicRibbonGalleryUI.this.ribbonGallery.getButtonAt(i);
                    jCommandToggleButton.setVisible(true);
                    if (bl) {
                        jCommandToggleButton.setBounds(n13, n18, n19, n4);
                        n13 += n19 + n10;
                    } else {
                        jCommandToggleButton.setBounds(n13 - n19, n18, n19, n4);
                        n13 -= n19 + n10;
                    }
                    if (++n17 != BasicRibbonGalleryUI.this.visibleButtonsInEachRow) continue;
                    n17 = 0;
                    n13 = bl ? BasicRibbonGalleryUI.this.margin.left + n10 : n - BasicRibbonGalleryUI.this.margin.right - n10;
                    n18 += n4;
                }
                if (BasicRibbonGalleryUI.this.ribbonGallery.getButtonCount() == 0) {
                    BasicRibbonGalleryUI.this.scrollDownButton.setEnabled(false);
                    BasicRibbonGalleryUI.this.scrollUpButton.setEnabled(false);
                    BasicRibbonGalleryUI.this.expandActionButton.setEnabled(false);
                } else {
                    BasicRibbonGalleryUI.this.scrollDownButton.setEnabled(!BasicRibbonGalleryUI.this.ribbonGallery.getButtonAt(BasicRibbonGalleryUI.this.ribbonGallery.getButtonCount() - 1).isVisible());
                    BasicRibbonGalleryUI.this.scrollUpButton.setEnabled(!BasicRibbonGalleryUI.this.ribbonGallery.getButtonAt(0).isVisible());
                    BasicRibbonGalleryUI.this.expandActionButton.setEnabled(true);
                }
            }
        }
    }

    protected static class JButtonStripUIResource
    extends JCommandButtonStrip
    implements UIResource {
        public JButtonStripUIResource() {
        }

        public JButtonStripUIResource(JCommandButtonStrip.StripOrientation stripOrientation) {
            super(stripOrientation);
        }
    }

}

