/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common;

import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JSeparator;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public class CommandButtonLayoutManagerTile
implements CommandButtonLayoutManager {
    @Override
    public int getPreferredIconSize() {
        return 32;
    }

    @Override
    public Dimension getPreferredSize(AbstractCommandButton abstractCommandButton) {
        Insets insets = abstractCommandButton.getInsets();
        int n = insets.top + insets.bottom;
        FontMetrics fontMetrics = abstractCommandButton.getFontMetrics(abstractCommandButton.getFont());
        String string = abstractCommandButton.getText();
        int n2 = string == null ? 0 : fontMetrics.stringWidth(abstractCommandButton.getText());
        String string2 = abstractCommandButton.getExtraText();
        int n3 = string2 == null ? 0 : fontMetrics.stringWidth(string2);
        double d = Math.max(n2, n3);
        int n4 = FlamingoUtilities.getHLayoutGap(abstractCommandButton);
        boolean bl = abstractCommandButton.getIcon() != null;
        boolean bl2 = d > 0.0;
        boolean bl3 = FlamingoUtilities.hasPopupAction(abstractCommandButton);
        int n5 = bl ? this.getPreferredIconSize() : 0;
        int n6 = insets.left;
        if (bl) {
            n6 += n4;
            n6 += n5;
            n6 += n4;
        }
        if (bl2) {
            n6 += n4;
            n6 = (int)((double)n6 + d);
            n6 += n4;
        }
        if (bl3) {
            n6 += 2 * n4;
            n6 += 1 + fontMetrics.getHeight() / 2;
            n6 += 2 * n4;
        }
        if (abstractCommandButton instanceof JCommandButton) {
            JCommandButton jCommandButton = (JCommandButton)abstractCommandButton;
            JCommandButton.CommandButtonKind commandButtonKind = jCommandButton.getCommandButtonKind();
            boolean bl4 = false;
            if (commandButtonKind == JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_ACTION && (bl || bl2)) {
                bl4 = true;
            }
            if (commandButtonKind == JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_POPUP && bl) {
                bl4 = true;
            }
            if (bl4) {
                n6 += new JSeparator((int)1).getPreferredSize().width;
            }
        }
        n6 += insets.right;
        return new Dimension(n6 -= 2 * n4, n + Math.max(n5, 2 * (fontMetrics.getAscent() + fontMetrics.getDescent())));
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
    }

    @Override
    public Point getKeyTipAnchorCenterPoint(AbstractCommandButton abstractCommandButton) {
        Insets insets = abstractCommandButton.getInsets();
        int n = abstractCommandButton.getHeight();
        ResizableIcon resizableIcon = abstractCommandButton.getIcon();
        if (resizableIcon != null) {
            return new Point(insets.left + resizableIcon.getIconWidth(), (n + resizableIcon.getIconHeight()) / 2);
        }
        return new Point(insets.left, 3 * n / 4);
    }

    @Override
    public CommandButtonLayoutManager.CommandButtonLayoutInfo getLayoutInfo(AbstractCommandButton abstractCommandButton, Graphics graphics) {
        int n;
        int n2;
        CommandButtonLayoutManager.CommandButtonLayoutInfo commandButtonLayoutInfo = new CommandButtonLayoutManager.CommandButtonLayoutInfo();
        commandButtonLayoutInfo.actionClickArea = new Rectangle(0, 0, 0, 0);
        commandButtonLayoutInfo.popupClickArea = new Rectangle(0, 0, 0, 0);
        Insets insets = abstractCommandButton.getInsets();
        commandButtonLayoutInfo.iconRect = new Rectangle();
        commandButtonLayoutInfo.popupActionRect = new Rectangle();
        int n3 = abstractCommandButton.getWidth();
        int n4 = abstractCommandButton.getHeight();
        int n5 = this.getPreferredSize((AbstractCommandButton)abstractCommandButton).width;
        int n6 = 0;
        if (abstractCommandButton.getHorizontalAlignment() == 0 && n3 > n5) {
            n6 = (n3 - n5) / 2;
        }
        ResizableIcon resizableIcon = abstractCommandButton.getIcon();
        String string = abstractCommandButton.getText();
        String string2 = abstractCommandButton.getExtraText();
        boolean bl = resizableIcon != null;
        boolean bl2 = string != null || string2 != null;
        boolean bl3 = FlamingoUtilities.hasPopupAction(abstractCommandButton);
        boolean bl4 = abstractCommandButton.getComponentOrientation().isLeftToRight();
        FontMetrics fontMetrics = graphics.getFontMetrics();
        int n7 = fontMetrics.getAscent() + fontMetrics.getDescent();
        JCommandButton.CommandButtonKind commandButtonKind = abstractCommandButton instanceof JCommandButton ? ((JCommandButton)abstractCommandButton).getCommandButtonKind() : JCommandButton.CommandButtonKind.ACTION_ONLY;
        int n8 = FlamingoUtilities.getHLayoutGap(abstractCommandButton);
        if (bl4) {
            int n9 = insets.left + n6 - n8;
            if (bl) {
                n2 = resizableIcon.getIconHeight();
                n = resizableIcon.getIconWidth();
                commandButtonLayoutInfo.iconRect.x = n9 += n8;
                commandButtonLayoutInfo.iconRect.y = (n4 - n2) / 2;
                commandButtonLayoutInfo.iconRect.width = n;
                commandButtonLayoutInfo.iconRect.height = n2;
                n9 += n + n8;
            }
            if (bl2) {
                CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo = new CommandButtonLayoutManager.TextLayoutInfo();
                textLayoutInfo.text = abstractCommandButton.getText();
                textLayoutInfo.textRect = new Rectangle();
                textLayoutInfo.textRect.x = n9 += n8;
                textLayoutInfo.textRect.y = (n4 - 2 * n7) / 2;
                textLayoutInfo.textRect.width = string == null ? 0 : (int)fontMetrics.getStringBounds(string, graphics).getWidth();
                textLayoutInfo.textRect.height = n7;
                commandButtonLayoutInfo.textLayoutInfoList = new ArrayList<CommandButtonLayoutManager.TextLayoutInfo>();
                commandButtonLayoutInfo.textLayoutInfoList.add(textLayoutInfo);
                String string3 = abstractCommandButton.getExtraText();
                CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo2 = new CommandButtonLayoutManager.TextLayoutInfo();
                textLayoutInfo2.text = string3;
                textLayoutInfo2.textRect = new Rectangle();
                textLayoutInfo2.textRect.x = n9;
                textLayoutInfo2.textRect.y = textLayoutInfo.textRect.y + n7;
                textLayoutInfo2.textRect.width = string3 == null ? 0 : (int)fontMetrics.getStringBounds(string3, graphics).getWidth();
                textLayoutInfo2.textRect.height = n7;
                commandButtonLayoutInfo.extraTextLayoutInfoList = new ArrayList<CommandButtonLayoutManager.TextLayoutInfo>();
                commandButtonLayoutInfo.extraTextLayoutInfoList.add(textLayoutInfo2);
                n9 += Math.max(textLayoutInfo.textRect.width, textLayoutInfo2.textRect.width);
                n9 += n8;
            }
            if (bl3) {
                commandButtonLayoutInfo.popupActionRect.x = n9 += 2 * n8;
                commandButtonLayoutInfo.popupActionRect.y = (n4 - n7) / 2 - 1;
                commandButtonLayoutInfo.popupActionRect.width = 1 + n7 / 2;
                commandButtonLayoutInfo.popupActionRect.height = n7 + 2;
                n9 += commandButtonLayoutInfo.popupActionRect.width;
                n9 += 2 * n8;
            }
            n2 = 0;
            n = new JSeparator((int)1).getPreferredSize().width;
            switch (commandButtonKind) {
                case ACTION_ONLY: {
                    commandButtonLayoutInfo.actionClickArea.x = 0;
                    commandButtonLayoutInfo.actionClickArea.y = 0;
                    commandButtonLayoutInfo.actionClickArea.width = n3;
                    commandButtonLayoutInfo.actionClickArea.height = n4;
                    commandButtonLayoutInfo.isTextInActionArea = true;
                    break;
                }
                case POPUP_ONLY: {
                    commandButtonLayoutInfo.popupClickArea.x = 0;
                    commandButtonLayoutInfo.popupClickArea.y = 0;
                    commandButtonLayoutInfo.popupClickArea.width = n3;
                    commandButtonLayoutInfo.popupClickArea.height = n4;
                    commandButtonLayoutInfo.isTextInActionArea = false;
                    break;
                }
                case ACTION_AND_POPUP_MAIN_ACTION: {
                    if (bl2 || bl) {
                        commandButtonLayoutInfo.popupActionRect.x += n;
                        n2 = commandButtonLayoutInfo.popupActionRect.x - 2 * n8;
                        commandButtonLayoutInfo.actionClickArea.x = 0;
                        commandButtonLayoutInfo.actionClickArea.y = 0;
                        commandButtonLayoutInfo.actionClickArea.width = n2;
                        commandButtonLayoutInfo.actionClickArea.height = n4;
                        commandButtonLayoutInfo.popupClickArea.x = n2;
                        commandButtonLayoutInfo.popupClickArea.y = 0;
                        commandButtonLayoutInfo.popupClickArea.width = n3 - n2;
                        commandButtonLayoutInfo.popupClickArea.height = n4;
                        commandButtonLayoutInfo.separatorOrientation = CommandButtonLayoutManager.CommandButtonSeparatorOrientation.VERTICAL;
                        commandButtonLayoutInfo.separatorArea = new Rectangle();
                        commandButtonLayoutInfo.separatorArea.x = n2;
                        commandButtonLayoutInfo.separatorArea.y = 0;
                        commandButtonLayoutInfo.separatorArea.width = n;
                        commandButtonLayoutInfo.separatorArea.height = n4;
                        commandButtonLayoutInfo.isTextInActionArea = true;
                        break;
                    }
                    commandButtonLayoutInfo.popupClickArea.x = 0;
                    commandButtonLayoutInfo.popupClickArea.y = 0;
                    commandButtonLayoutInfo.popupClickArea.width = n3;
                    commandButtonLayoutInfo.popupClickArea.height = n4;
                    commandButtonLayoutInfo.isTextInActionArea = true;
                    break;
                }
                case ACTION_AND_POPUP_MAIN_POPUP: {
                    if (bl) {
                        if (commandButtonLayoutInfo.textLayoutInfoList != null) {
                            for (CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo : commandButtonLayoutInfo.textLayoutInfoList) {
                                textLayoutInfo.textRect.x += n;
                            }
                        }
                        if (commandButtonLayoutInfo.extraTextLayoutInfoList != null) {
                            for (CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo : commandButtonLayoutInfo.extraTextLayoutInfoList) {
                                textLayoutInfo.textRect.x += n;
                            }
                        }
                        commandButtonLayoutInfo.popupActionRect.x += n;
                        n2 = commandButtonLayoutInfo.iconRect.x + commandButtonLayoutInfo.iconRect.width + n8;
                        commandButtonLayoutInfo.actionClickArea.x = 0;
                        commandButtonLayoutInfo.actionClickArea.y = 0;
                        commandButtonLayoutInfo.actionClickArea.width = n2;
                        commandButtonLayoutInfo.actionClickArea.height = n4;
                        commandButtonLayoutInfo.popupClickArea.x = n2;
                        commandButtonLayoutInfo.popupClickArea.y = 0;
                        commandButtonLayoutInfo.popupClickArea.width = n3 - n2;
                        commandButtonLayoutInfo.popupClickArea.height = n4;
                        commandButtonLayoutInfo.separatorOrientation = CommandButtonLayoutManager.CommandButtonSeparatorOrientation.VERTICAL;
                        commandButtonLayoutInfo.separatorArea = new Rectangle();
                        commandButtonLayoutInfo.separatorArea.x = n2;
                        commandButtonLayoutInfo.separatorArea.y = 0;
                        commandButtonLayoutInfo.separatorArea.width = n;
                        commandButtonLayoutInfo.separatorArea.height = n4;
                        commandButtonLayoutInfo.isTextInActionArea = true;
                        break;
                    }
                    commandButtonLayoutInfo.popupClickArea.x = 0;
                    commandButtonLayoutInfo.popupClickArea.y = 0;
                    commandButtonLayoutInfo.popupClickArea.width = n3;
                    commandButtonLayoutInfo.popupClickArea.height = n4;
                    commandButtonLayoutInfo.isTextInActionArea = true;
                }
            }
        } else {
            int n10 = n3 - insets.right - n6 + n8;
            if (bl) {
                int n11 = resizableIcon.getIconHeight();
                int n12 = resizableIcon.getIconWidth();
                commandButtonLayoutInfo.iconRect.x = (n10 -= n8) - n12;
                commandButtonLayoutInfo.iconRect.y = (n4 - n11) / 2;
                commandButtonLayoutInfo.iconRect.width = n12;
                commandButtonLayoutInfo.iconRect.height = n11;
                n10 -= n12 + n8;
            }
            if (bl2) {
                CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo = new CommandButtonLayoutManager.TextLayoutInfo();
                textLayoutInfo.text = abstractCommandButton.getText();
                textLayoutInfo.textRect = new Rectangle();
                textLayoutInfo.textRect.width = string == null ? 0 : (int)fontMetrics.getStringBounds(string, graphics).getWidth();
                textLayoutInfo.textRect.x = (n10 -= n8) - textLayoutInfo.textRect.width;
                textLayoutInfo.textRect.y = (n4 - 2 * n7) / 2;
                textLayoutInfo.textRect.height = n7;
                commandButtonLayoutInfo.textLayoutInfoList = new ArrayList<CommandButtonLayoutManager.TextLayoutInfo>();
                commandButtonLayoutInfo.textLayoutInfoList.add(textLayoutInfo);
                String string4 = abstractCommandButton.getExtraText();
                CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo3 = new CommandButtonLayoutManager.TextLayoutInfo();
                textLayoutInfo3.text = string4;
                textLayoutInfo3.textRect = new Rectangle();
                textLayoutInfo3.textRect.width = string4 == null ? 0 : (int)fontMetrics.getStringBounds(string4, graphics).getWidth();
                textLayoutInfo3.textRect.x = n10 - textLayoutInfo3.textRect.width;
                textLayoutInfo3.textRect.y = textLayoutInfo.textRect.y + n7;
                textLayoutInfo3.textRect.height = n7;
                commandButtonLayoutInfo.extraTextLayoutInfoList = new ArrayList<CommandButtonLayoutManager.TextLayoutInfo>();
                commandButtonLayoutInfo.extraTextLayoutInfoList.add(textLayoutInfo3);
                n10 -= Math.max(textLayoutInfo.textRect.width, textLayoutInfo3.textRect.width);
                n10 -= n8;
            }
            if (bl3) {
                commandButtonLayoutInfo.popupActionRect.width = 1 + n7 / 2;
                commandButtonLayoutInfo.popupActionRect.x = (n10 -= 2 * n8) - commandButtonLayoutInfo.popupActionRect.width;
                commandButtonLayoutInfo.popupActionRect.y = (n4 - n7) / 2 - 1;
                commandButtonLayoutInfo.popupActionRect.height = n7 + 2;
                n10 -= commandButtonLayoutInfo.popupActionRect.width;
                n10 -= 2 * n8;
            }
            n2 = 0;
            n = new JSeparator((int)1).getPreferredSize().width;
            switch (commandButtonKind) {
                case ACTION_ONLY: {
                    commandButtonLayoutInfo.actionClickArea.x = 0;
                    commandButtonLayoutInfo.actionClickArea.y = 0;
                    commandButtonLayoutInfo.actionClickArea.width = n3;
                    commandButtonLayoutInfo.actionClickArea.height = n4;
                    commandButtonLayoutInfo.isTextInActionArea = true;
                    break;
                }
                case POPUP_ONLY: {
                    commandButtonLayoutInfo.popupClickArea.x = 0;
                    commandButtonLayoutInfo.popupClickArea.y = 0;
                    commandButtonLayoutInfo.popupClickArea.width = n3;
                    commandButtonLayoutInfo.popupClickArea.height = n4;
                    commandButtonLayoutInfo.isTextInActionArea = false;
                    break;
                }
                case ACTION_AND_POPUP_MAIN_ACTION: {
                    if (bl2 || bl) {
                        commandButtonLayoutInfo.popupActionRect.x -= n;
                        commandButtonLayoutInfo.actionClickArea.x = n2 = commandButtonLayoutInfo.popupActionRect.x + commandButtonLayoutInfo.popupActionRect.width + 2 * n8;
                        commandButtonLayoutInfo.actionClickArea.y = 0;
                        commandButtonLayoutInfo.actionClickArea.width = n3 - n2;
                        commandButtonLayoutInfo.actionClickArea.height = n4;
                        commandButtonLayoutInfo.popupClickArea.x = 0;
                        commandButtonLayoutInfo.popupClickArea.y = 0;
                        commandButtonLayoutInfo.popupClickArea.width = n2;
                        commandButtonLayoutInfo.popupClickArea.height = n4;
                        commandButtonLayoutInfo.separatorOrientation = CommandButtonLayoutManager.CommandButtonSeparatorOrientation.VERTICAL;
                        commandButtonLayoutInfo.separatorArea = new Rectangle();
                        commandButtonLayoutInfo.separatorArea.x = n2;
                        commandButtonLayoutInfo.separatorArea.y = 0;
                        commandButtonLayoutInfo.separatorArea.width = n;
                        commandButtonLayoutInfo.separatorArea.height = n4;
                        commandButtonLayoutInfo.isTextInActionArea = true;
                        break;
                    }
                    commandButtonLayoutInfo.popupClickArea.x = 0;
                    commandButtonLayoutInfo.popupClickArea.y = 0;
                    commandButtonLayoutInfo.popupClickArea.width = n3;
                    commandButtonLayoutInfo.popupClickArea.height = n4;
                    commandButtonLayoutInfo.isTextInActionArea = true;
                    break;
                }
                case ACTION_AND_POPUP_MAIN_POPUP: {
                    if (bl) {
                        if (commandButtonLayoutInfo.textLayoutInfoList != null) {
                            for (CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo : commandButtonLayoutInfo.textLayoutInfoList) {
                                textLayoutInfo.textRect.x -= n;
                            }
                        }
                        if (commandButtonLayoutInfo.extraTextLayoutInfoList != null) {
                            for (CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo : commandButtonLayoutInfo.extraTextLayoutInfoList) {
                                textLayoutInfo.textRect.x -= n;
                            }
                        }
                        commandButtonLayoutInfo.popupActionRect.x -= n;
                        commandButtonLayoutInfo.actionClickArea.x = n2 = commandButtonLayoutInfo.iconRect.x - n8;
                        commandButtonLayoutInfo.actionClickArea.y = 0;
                        commandButtonLayoutInfo.actionClickArea.width = n3 - n2;
                        commandButtonLayoutInfo.actionClickArea.height = n4;
                        commandButtonLayoutInfo.popupClickArea.x = 0;
                        commandButtonLayoutInfo.popupClickArea.y = 0;
                        commandButtonLayoutInfo.popupClickArea.width = n2;
                        commandButtonLayoutInfo.popupClickArea.height = n4;
                        commandButtonLayoutInfo.separatorOrientation = CommandButtonLayoutManager.CommandButtonSeparatorOrientation.VERTICAL;
                        commandButtonLayoutInfo.separatorArea = new Rectangle();
                        commandButtonLayoutInfo.separatorArea.x = n2;
                        commandButtonLayoutInfo.separatorArea.y = 0;
                        commandButtonLayoutInfo.separatorArea.width = n;
                        commandButtonLayoutInfo.separatorArea.height = n4;
                        commandButtonLayoutInfo.isTextInActionArea = true;
                        break;
                    }
                    commandButtonLayoutInfo.popupClickArea.x = 0;
                    commandButtonLayoutInfo.popupClickArea.y = 0;
                    commandButtonLayoutInfo.popupClickArea.width = n3;
                    commandButtonLayoutInfo.popupClickArea.height = n4;
                    commandButtonLayoutInfo.isTextInActionArea = true;
                }
            }
        }
        return commandButtonLayoutInfo;
    }

}

