/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common;

import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.PanelUI;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.internal.ui.common.BasicRichTooltipPanelUI;
import org.pushingpixels.flamingo.internal.ui.common.RichTooltipPanelUI;

public class JRichTooltipPanel
extends JPanel {
    protected RichTooltip tooltipInfo;
    public static final String uiClassID = "RichTooltipPanelUI";

    public JRichTooltipPanel(RichTooltip richTooltip) {
        this.tooltipInfo = richTooltip;
    }

    @Override
    public RichTooltipPanelUI getUI() {
        return (RichTooltipPanelUI)this.ui;
    }

    protected void setUI(RichTooltipPanelUI richTooltipPanelUI) {
        super.setUI(richTooltipPanelUI);
    }

    @Override
    public String getUIClassID() {
        return "RichTooltipPanelUI";
    }

    @Override
    public void updateUI() {
        if (UIManager.get(this.getUIClassID()) != null) {
            this.setUI((RichTooltipPanelUI)UIManager.getUI(this));
        } else {
            this.setUI(BasicRichTooltipPanelUI.createUI(this));
        }
    }

    public RichTooltip getTooltipInfo() {
        return this.tooltipInfo;
    }
}

