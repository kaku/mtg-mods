/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon;

import javax.swing.JPanel;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.PanelUI;
import javax.swing.plaf.UIResource;
import org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand;
import org.pushingpixels.flamingo.internal.ui.ribbon.BandControlPanelUI;

public class AbstractBandControlPanel
extends JPanel
implements UIResource {
    private AbstractRibbonBand ribbonBand;

    @Override
    public BandControlPanelUI getUI() {
        return (BandControlPanelUI)this.ui;
    }

    public void setRibbonBand(AbstractRibbonBand abstractRibbonBand) {
        this.ribbonBand = abstractRibbonBand;
    }

    public AbstractRibbonBand getRibbonBand() {
        return this.ribbonBand;
    }
}

