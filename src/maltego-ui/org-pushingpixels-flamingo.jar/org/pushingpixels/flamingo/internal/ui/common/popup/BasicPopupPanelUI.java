/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common.popup;

import java.applet.Applet;
import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.plaf.ActionMapUIResource;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.ComponentInputMapUIResource;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.ComboPopup;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.model.PopupButtonModel;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelManager;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;
import org.pushingpixels.flamingo.internal.ui.common.popup.PopupPanelUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.JRibbonTaskToggleButton;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuPopupPanel;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;
import org.pushingpixels.flamingo.internal.utils.KeyTipManager;

public class BasicPopupPanelUI
extends PopupPanelUI {
    protected JPopupPanel popupPanel;
    static PopupPanelManager.PopupListener popupPanelManagerListener;

    public static ComponentUI createUI(JComponent jComponent) {
        return new BasicPopupPanelUI();
    }

    @Override
    public void installUI(JComponent jComponent) {
        this.popupPanel = (JPopupPanel)jComponent;
        super.installUI(this.popupPanel);
        this.installDefaults();
        this.installComponents();
        this.installListeners();
    }

    @Override
    public void uninstallUI(JComponent jComponent) {
        this.uninstallListeners();
        this.uninstallComponents();
        this.uninstallDefaults();
        super.uninstallUI(this.popupPanel);
    }

    protected void installDefaults() {
        Border border;
        Color color = this.popupPanel.getBackground();
        if (color == null || color instanceof UIResource) {
            this.popupPanel.setBackground(FlamingoUtilities.getColor(Color.lightGray, "PopupPanel.background", "Panel.background"));
        }
        if ((border = this.popupPanel.getBorder()) == null || border instanceof UIResource) {
            Border border2 = UIManager.getBorder("PopupPanel.border");
            if (border2 == null) {
                border2 = new BorderUIResource.CompoundBorderUIResource(new LineBorder(FlamingoUtilities.getBorderColor()), new EmptyBorder(1, 1, 1, 1));
            }
            this.popupPanel.setBorder(border2);
        }
        LookAndFeel.installProperty(this.popupPanel, "opaque", Boolean.TRUE);
    }

    protected void installListeners() {
        BasicPopupPanelUI.initiliazeGlobalListeners();
    }

    protected void installComponents() {
    }

    protected void uninstallDefaults() {
        LookAndFeel.uninstallBorder(this.popupPanel);
    }

    protected void uninstallListeners() {
    }

    protected void uninstallComponents() {
    }

    protected static synchronized void initiliazeGlobalListeners() {
        if (popupPanelManagerListener != null) {
            return;
        }
        popupPanelManagerListener = new PopupPanelEscapeDismisser();
        PopupPanelManager.defaultManager().addPopupListener(popupPanelManagerListener);
        new WindowTracker();
    }

    protected static class WindowTracker
    implements PopupPanelManager.PopupListener,
    AWTEventListener,
    ComponentListener,
    WindowListener {
        Window grabbedWindow;
        List<PopupPanelManager.PopupInfo> lastPathSelected;

        public WindowTracker() {
            PopupPanelManager popupPanelManager = PopupPanelManager.defaultManager();
            popupPanelManager.addPopupListener(this);
            this.lastPathSelected = popupPanelManager.getShownPath();
            if (this.lastPathSelected.size() != 0) {
                this.grabWindow(this.lastPathSelected);
            }
        }

        void grabWindow(List<PopupPanelManager.PopupInfo> list) {
            final Toolkit toolkit = Toolkit.getDefaultToolkit();
            AccessController.doPrivileged(new PrivilegedAction(){

                public Object run() {
                    toolkit.addAWTEventListener(WindowTracker.this, 131184);
                    return null;
                }
            });
            JComponent jComponent = list.get(0).getPopupOriginator();
            Window window = this.grabbedWindow = jComponent instanceof Window ? (Window)((Object)jComponent) : SwingUtilities.getWindowAncestor(jComponent);
            if (this.grabbedWindow != null) {
                this.grabbedWindow.addComponentListener(this);
                this.grabbedWindow.addWindowListener(this);
            }
        }

        void ungrabWindow() {
            final Toolkit toolkit = Toolkit.getDefaultToolkit();
            AccessController.doPrivileged(new PrivilegedAction(){

                public Object run() {
                    toolkit.removeAWTEventListener(WindowTracker.this);
                    return null;
                }
            });
            if (this.grabbedWindow != null) {
                this.grabbedWindow.removeComponentListener(this);
                this.grabbedWindow.removeWindowListener(this);
                this.grabbedWindow = null;
            }
        }

        @Override
        public void popupShown(PopupPanelManager.PopupEvent popupEvent) {
            PopupPanelManager popupPanelManager = PopupPanelManager.defaultManager();
            List<PopupPanelManager.PopupInfo> list = popupPanelManager.getShownPath();
            if (this.lastPathSelected.size() == 0 && list.size() != 0) {
                this.grabWindow(list);
            }
            this.lastPathSelected = list;
        }

        @Override
        public void popupHidden(PopupPanelManager.PopupEvent popupEvent) {
            PopupPanelManager popupPanelManager = PopupPanelManager.defaultManager();
            List<PopupPanelManager.PopupInfo> list = popupPanelManager.getShownPath();
            if (this.lastPathSelected.size() != 0 && list.size() == 0) {
                this.ungrabWindow();
            }
            this.lastPathSelected = list;
        }

        @Override
        public void eventDispatched(AWTEvent aWTEvent) {
            if (!(aWTEvent instanceof MouseEvent)) {
                return;
            }
            MouseEvent mouseEvent = (MouseEvent)aWTEvent;
            final Component component = mouseEvent.getComponent();
            JPopupPanel jPopupPanel = (JPopupPanel)SwingUtilities.getAncestorOfClass(JPopupPanel.class, component);
            switch (mouseEvent.getID()) {
                case 501: {
                    JRibbon jRibbon;
                    boolean bl = false;
                    if (component instanceof JCommandButton) {
                        bl = ((JCommandButton)component).getPopupModel().isPopupShowing();
                    }
                    if (!bl && jPopupPanel != null) {
                        PopupPanelManager.defaultManager().hidePopups(jPopupPanel);
                        return;
                    }
                    if (component instanceof JRibbonTaskToggleButton && (jRibbon = (JRibbon)SwingUtilities.getAncestorOfClass(JRibbon.class, component)) != null && FlamingoUtilities.isShowingMinimizedRibbonInPopup(jRibbon)) {
                        return;
                    }
                    if (bl || SwingUtilities.getAncestorOfClass(ComboPopup.class, component) != null) break;
                    PopupPanelManager.defaultManager().hidePopups(component);
                    break;
                }
                case 502: {
                    if (SwingUtilities.getAncestorOfClass(ComboPopup.class, component) == null) break;
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            PopupPanelManager.defaultManager().hidePopups(component);
                        }
                    });
                    break;
                }
                case 507: {
                    if (jPopupPanel != null) {
                        PopupPanelManager.defaultManager().hidePopups(jPopupPanel);
                        return;
                    }
                    PopupPanelManager.defaultManager().hidePopups(component);
                }
            }
        }

        boolean isInPopupPanel(Component component) {
            for (Component component2 = component; component2 != null && !(component2 instanceof Applet) && !(component2 instanceof Window); component2 = component2.getParent()) {
                if (!(component2 instanceof JPopupPanel)) continue;
                return true;
            }
            return false;
        }

        @Override
        public void componentResized(ComponentEvent componentEvent) {
            PopupPanelManager.defaultManager().hidePopups(null);
        }

        @Override
        public void componentMoved(ComponentEvent componentEvent) {
            PopupPanelManager.defaultManager().hidePopups(null);
        }

        @Override
        public void componentShown(ComponentEvent componentEvent) {
            PopupPanelManager.defaultManager().hidePopups(null);
        }

        @Override
        public void componentHidden(ComponentEvent componentEvent) {
            PopupPanelManager.defaultManager().hidePopups(null);
        }

        @Override
        public void windowClosing(WindowEvent windowEvent) {
            PopupPanelManager.defaultManager().hidePopups(null);
        }

        @Override
        public void windowClosed(WindowEvent windowEvent) {
            PopupPanelManager.defaultManager().hidePopups(null);
        }

        @Override
        public void windowIconified(WindowEvent windowEvent) {
            PopupPanelManager.defaultManager().hidePopups(null);
        }

        @Override
        public void windowDeactivated(WindowEvent windowEvent) {
            PopupPanelManager.defaultManager().hidePopups(null);
        }

        @Override
        public void windowOpened(WindowEvent windowEvent) {
        }

        @Override
        public void windowDeiconified(WindowEvent windowEvent) {
        }

        @Override
        public void windowActivated(WindowEvent windowEvent) {
        }

    }

    protected static class PopupPanelEscapeDismisser
    implements PopupPanelManager.PopupListener {
        private ActionMap newActionMap;
        private InputMap newInputMap;
        List<PopupPanelManager.PopupInfo> lastPathSelected;
        private JRootPane tracedRootPane;

        public PopupPanelEscapeDismisser() {
            PopupPanelManager popupPanelManager = PopupPanelManager.defaultManager();
            this.lastPathSelected = popupPanelManager.getShownPath();
            if (this.lastPathSelected.size() != 0) {
                this.traceRootPane(this.lastPathSelected);
            }
        }

        @Override
        public void popupHidden(PopupPanelManager.PopupEvent popupEvent) {
            PopupPanelManager popupPanelManager = PopupPanelManager.defaultManager();
            List<PopupPanelManager.PopupInfo> list = popupPanelManager.getShownPath();
            if (this.lastPathSelected.size() != 0 && list.size() == 0) {
                this.untraceRootPane();
            }
            this.lastPathSelected = list;
        }

        private void untraceRootPane() {
            if (this.tracedRootPane != null) {
                this.removeUIActionMap(this.tracedRootPane, this.newActionMap);
                this.removeUIInputMap(this.tracedRootPane, this.newInputMap);
            }
        }

        @Override
        public void popupShown(PopupPanelManager.PopupEvent popupEvent) {
            PopupPanelManager popupPanelManager = PopupPanelManager.defaultManager();
            List<PopupPanelManager.PopupInfo> list = popupPanelManager.getShownPath();
            if (this.lastPathSelected.size() == 0 && list.size() != 0) {
                this.traceRootPane(list);
            }
            this.lastPathSelected = list;
        }

        private void traceRootPane(List<PopupPanelManager.PopupInfo> list) {
            JComponent jComponent = list.get(0).getPopupOriginator();
            this.tracedRootPane = SwingUtilities.getRootPane(jComponent);
            if (this.tracedRootPane != null) {
                this.newInputMap = new ComponentInputMapUIResource(this.tracedRootPane);
                this.newInputMap.put(KeyStroke.getKeyStroke(27, 0), "hidePopupPanel");
                this.newActionMap = new ActionMapUIResource();
                this.newActionMap.put("hidePopupPanel", new AbstractAction(){

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        PopupPanelManager.PopupInfo popupInfo;
                        List<PopupPanelManager.PopupInfo> list = PopupPanelManager.defaultManager().getShownPath();
                        if (list.size() > 0 && (popupInfo = list.get(list.size() - 1)).getPopupPanel() instanceof JRibbonApplicationMenuPopupPanel) {
                            JRibbonApplicationMenuPopupPanel jRibbonApplicationMenuPopupPanel = (JRibbonApplicationMenuPopupPanel)popupInfo.getPopupPanel();
                            KeyTipManager.KeyTipChain keyTipChain = KeyTipManager.defaultManager().getCurrentlyShownKeyTipChain();
                            if (keyTipChain != null && keyTipChain.chainParentComponent == jRibbonApplicationMenuPopupPanel.getPanelLevel2()) {
                                return;
                            }
                        }
                        PopupPanelManager.defaultManager().hideLastPopup();
                    }
                });
                this.addUIInputMap(this.tracedRootPane, this.newInputMap);
                this.addUIActionMap(this.tracedRootPane, this.newActionMap);
            }
        }

        void addUIInputMap(JComponent jComponent, InputMap inputMap) {
            InputMap inputMap2;
            InputMap inputMap3 = null;
            for (inputMap2 = jComponent.getInputMap((int)2); inputMap2 != null && !(inputMap2 instanceof UIResource); inputMap2 = inputMap2.getParent()) {
                inputMap3 = inputMap2;
            }
            if (inputMap3 == null) {
                jComponent.setInputMap(2, inputMap);
            } else {
                inputMap3.setParent(inputMap);
            }
            inputMap.setParent(inputMap2);
        }

        void addUIActionMap(JComponent jComponent, ActionMap actionMap) {
            ActionMap actionMap2;
            ActionMap actionMap3 = null;
            for (actionMap2 = jComponent.getActionMap(); actionMap2 != null && !(actionMap2 instanceof UIResource); actionMap2 = actionMap2.getParent()) {
                actionMap3 = actionMap2;
            }
            if (actionMap3 == null) {
                jComponent.setActionMap(actionMap);
            } else {
                actionMap3.setParent(actionMap);
            }
            actionMap.setParent(actionMap2);
        }

        void removeUIInputMap(JComponent jComponent, InputMap inputMap) {
            InputMap inputMap2 = null;
            for (InputMap inputMap3 = jComponent.getInputMap((int)2); inputMap3 != null; inputMap3 = inputMap3.getParent()) {
                if (inputMap3 == inputMap) {
                    if (inputMap2 == null) {
                        jComponent.setInputMap(2, inputMap.getParent());
                        break;
                    }
                    inputMap2.setParent(inputMap.getParent());
                    break;
                }
                inputMap2 = inputMap3;
            }
        }

        void removeUIActionMap(JComponent jComponent, ActionMap actionMap) {
            ActionMap actionMap2 = null;
            for (ActionMap actionMap3 = jComponent.getActionMap(); actionMap3 != null; actionMap3 = actionMap3.getParent()) {
                if (actionMap3 == actionMap) {
                    if (actionMap2 == null) {
                        jComponent.setActionMap(actionMap.getParent());
                        break;
                    }
                    actionMap2.setParent(actionMap.getParent());
                    break;
                }
                actionMap2 = actionMap3;
            }
        }

    }

}

