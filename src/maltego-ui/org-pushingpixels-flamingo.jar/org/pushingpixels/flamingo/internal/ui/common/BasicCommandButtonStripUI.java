/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common;

import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.JComponent;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButtonStrip;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonStripUI;

public class BasicCommandButtonStripUI
extends CommandButtonStripUI {
    protected JCommandButtonStrip buttonStrip;
    protected ChangeListener changeListener;

    public static ComponentUI createUI(JComponent jComponent) {
        return new BasicCommandButtonStripUI();
    }

    @Override
    public void installUI(JComponent jComponent) {
        this.buttonStrip = (JCommandButtonStrip)jComponent;
        this.installDefaults();
        this.installComponents();
        this.installListeners();
    }

    @Override
    public void uninstallUI(JComponent jComponent) {
        this.uninstallListeners();
        this.uninstallComponents();
        this.uninstallDefaults();
        jComponent.setLayout(null);
        this.buttonStrip = null;
    }

    protected void installListeners() {
        this.changeListener = new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                if (BasicCommandButtonStripUI.this.buttonStrip.getButtonCount() == 1) {
                    BasicCommandButtonStripUI.this.buttonStrip.getButton(0).setLocationOrderKind(AbstractCommandButton.CommandButtonLocationOrderKind.ONLY);
                } else {
                    BasicCommandButtonStripUI.this.buttonStrip.getButton(0).setLocationOrderKind(AbstractCommandButton.CommandButtonLocationOrderKind.FIRST);
                    for (int i = 1; i < BasicCommandButtonStripUI.this.buttonStrip.getButtonCount() - 1; ++i) {
                        BasicCommandButtonStripUI.this.buttonStrip.getButton(i).setLocationOrderKind(AbstractCommandButton.CommandButtonLocationOrderKind.MIDDLE);
                    }
                    BasicCommandButtonStripUI.this.buttonStrip.getButton(BasicCommandButtonStripUI.this.buttonStrip.getButtonCount() - 1).setLocationOrderKind(AbstractCommandButton.CommandButtonLocationOrderKind.LAST);
                }
            }
        };
        this.buttonStrip.addChangeListener(this.changeListener);
    }

    protected void uninstallListeners() {
        this.buttonStrip.removeChangeListener(this.changeListener);
        this.changeListener = null;
    }

    protected void installDefaults() {
        this.buttonStrip.setBorder(new EmptyBorder(0, 0, 0, 0));
    }

    protected void uninstallDefaults() {
    }

    protected void installComponents() {
        this.buttonStrip.setLayout(this.createLayoutManager());
    }

    protected void uninstallComponents() {
    }

    protected LayoutManager createLayoutManager() {
        return new ButtonStripLayout();
    }

    private class ButtonStripLayout
    implements LayoutManager {
        private ButtonStripLayout() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            int n;
            int n2 = 0;
            int n3 = 0;
            if (BasicCommandButtonStripUI.this.buttonStrip.getOrientation() == JCommandButtonStrip.StripOrientation.HORIZONTAL) {
                for (n = 0; n < BasicCommandButtonStripUI.this.buttonStrip.getButtonCount(); ++n) {
                    n2 += BasicCommandButtonStripUI.this.buttonStrip.getButton((int)n).getPreferredSize().width;
                    n3 = Math.max(n3, BasicCommandButtonStripUI.this.buttonStrip.getButton((int)n).getPreferredSize().height);
                }
            } else {
                for (n = 0; n < BasicCommandButtonStripUI.this.buttonStrip.getButtonCount(); ++n) {
                    n3 += BasicCommandButtonStripUI.this.buttonStrip.getButton((int)n).getPreferredSize().height;
                    n2 = Math.max(n2, BasicCommandButtonStripUI.this.buttonStrip.getButton((int)n).getPreferredSize().width);
                }
            }
            Insets insets = container.getInsets();
            return new Dimension(n2 + insets.left + insets.right, n3 + insets.top + insets.bottom);
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            return this.preferredLayoutSize(container);
        }

        @Override
        public void layoutContainer(Container container) {
            if (BasicCommandButtonStripUI.this.buttonStrip.getButtonCount() == 0) {
                return;
            }
            Insets insets = container.getInsets();
            int n = container.getHeight() - insets.top - insets.bottom;
            int n2 = container.getWidth() - insets.left - insets.right;
            if (BasicCommandButtonStripUI.this.buttonStrip.getOrientation() == JCommandButtonStrip.StripOrientation.HORIZONTAL) {
                int n3;
                int n4 = 0;
                for (n3 = 0; n3 < BasicCommandButtonStripUI.this.buttonStrip.getButtonCount(); ++n3) {
                    AbstractCommandButton abstractCommandButton = BasicCommandButtonStripUI.this.buttonStrip.getButton(n3);
                    n4 += abstractCommandButton.getPreferredSize().width;
                }
                n3 = (n2 - n4) / BasicCommandButtonStripUI.this.buttonStrip.getButtonCount();
                if (BasicCommandButtonStripUI.this.buttonStrip.getComponentOrientation().isLeftToRight()) {
                    int n5 = insets.left;
                    for (int i = 0; i < BasicCommandButtonStripUI.this.buttonStrip.getButtonCount(); ++i) {
                        AbstractCommandButton abstractCommandButton = BasicCommandButtonStripUI.this.buttonStrip.getButton(i);
                        abstractCommandButton.setBounds(n5, insets.top, abstractCommandButton.getPreferredSize().width + n3, n);
                        n5 += abstractCommandButton.getPreferredSize().width + n3;
                    }
                } else {
                    int n6 = container.getWidth() - insets.right;
                    for (int i = 0; i < BasicCommandButtonStripUI.this.buttonStrip.getButtonCount(); ++i) {
                        AbstractCommandButton abstractCommandButton = BasicCommandButtonStripUI.this.buttonStrip.getButton(i);
                        int n7 = abstractCommandButton.getPreferredSize().width + n3;
                        abstractCommandButton.setBounds(n6 - n7, insets.top, n7, n);
                        n6 -= n7;
                    }
                }
            } else {
                int n8 = 0;
                for (int i = 0; i < BasicCommandButtonStripUI.this.buttonStrip.getButtonCount(); ++i) {
                    AbstractCommandButton abstractCommandButton = BasicCommandButtonStripUI.this.buttonStrip.getButton(i);
                    n8 += abstractCommandButton.getPreferredSize().height;
                }
                float f = (float)(n - n8) / (float)BasicCommandButtonStripUI.this.buttonStrip.getButtonCount();
                float f2 = insets.top;
                for (int j = 0; j < BasicCommandButtonStripUI.this.buttonStrip.getButtonCount(); ++j) {
                    AbstractCommandButton abstractCommandButton = BasicCommandButtonStripUI.this.buttonStrip.getButton(j);
                    float f3 = (float)abstractCommandButton.getPreferredSize().height + f;
                    abstractCommandButton.setBounds(insets.left, (int)f2, n2, (int)Math.ceil(f3));
                    f2 += f3;
                }
            }
        }
    }

}

