/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon;

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Composite;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.Arc2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.Popup;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicGraphicsUtils;
import org.pushingpixels.flamingo.BlankPopupFixPopupFactory;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.CommandToggleButtonGroup;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandToggleButton;
import org.pushingpixels.flamingo.api.common.JScrollablePanel;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelManager;
import org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;
import org.pushingpixels.flamingo.api.ribbon.JRibbonFrame;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu;
import org.pushingpixels.flamingo.api.ribbon.RibbonContextualTaskGroup;
import org.pushingpixels.flamingo.api.ribbon.RibbonTask;
import org.pushingpixels.flamingo.api.ribbon.resize.RibbonBandResizePolicy;
import org.pushingpixels.flamingo.api.ribbon.resize.RibbonBandResizeSequencingPolicy;
import org.pushingpixels.flamingo.internal.ui.ribbon.BandControlPanelUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.JRibbonTaskToggleButton;
import org.pushingpixels.flamingo.internal.ui.ribbon.RibbonBandUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.RibbonUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuButton;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;
import org.pushingpixels.flamingo.internal.utils.KeyTipManager;
import org.pushingpixels.flamingo.internal.utils.RenderingUtils;

public class BasicRibbonUI
extends RibbonUI {
    public static final String IS_USING_TITLE_PANE = "ribbon.internal.isUsingTitlePane";
    private static final String JUST_MINIMIZED = "ribbon.internal.justMinimized";
    protected JRibbon ribbon;
    protected JPanel taskBarPanel;
    protected JScrollablePanel<BandHostPanel> bandScrollablePanel;
    protected JScrollablePanel<TaskToggleButtonsHostPanel> taskToggleButtonsScrollablePanel;
    protected JRibbonApplicationMenuButton applicationMenuButton;
    protected JCommandButton helpButton;
    protected Map<RibbonTask, JRibbonTaskToggleButton> taskToggleButtons = new HashMap<RibbonTask, JRibbonTaskToggleButton>();
    protected CommandToggleButtonGroup taskToggleButtonGroup = new CommandToggleButtonGroup();
    protected ChangeListener ribbonChangeListener;
    protected PropertyChangeListener propertyChangeListener;
    protected ContainerListener ribbonContainerListener;
    protected ComponentListener ribbonComponentListener;

    public static ComponentUI createUI(JComponent jComponent) {
        return new BasicRibbonUI();
    }

    public BasicRibbonUI() {
        this.taskToggleButtonGroup.setAllowsClearingSelection(false);
    }

    @Override
    public void installUI(JComponent jComponent) {
        this.ribbon = (JRibbon)jComponent;
        this.installDefaults();
        this.installComponents();
        this.installListeners();
    }

    @Override
    public void uninstallUI(JComponent jComponent) {
        this.uninstallListeners();
        this.uninstallComponents();
        this.uninstallDefaults();
        this.ribbon = null;
    }

    protected void installListeners() {
        this.ribbonChangeListener = new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                BasicRibbonUI.this.syncRibbonState();
            }
        };
        this.ribbon.addChangeListener(this.ribbonChangeListener);
        this.propertyChangeListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                Object object;
                if ("selectedTask".equals(propertyChangeEvent.getPropertyName())) {
                    JRibbonTaskToggleButton jRibbonTaskToggleButton;
                    object = (RibbonTask)propertyChangeEvent.getOldValue();
                    final RibbonTask ribbonTask = (RibbonTask)propertyChangeEvent.getNewValue();
                    if (object != null && BasicRibbonUI.this.taskToggleButtons.get(object) != null) {
                        BasicRibbonUI.this.taskToggleButtons.get(object).getActionModel().setSelected(false);
                    }
                    if (ribbonTask != null && BasicRibbonUI.this.taskToggleButtons.get(ribbonTask) != null) {
                        BasicRibbonUI.this.taskToggleButtons.get(ribbonTask).getActionModel().setSelected(true);
                    }
                    if (BasicRibbonUI.this.isShowingScrollsForTaskToggleButtons() && ribbonTask != null && (jRibbonTaskToggleButton = BasicRibbonUI.this.taskToggleButtons.get(ribbonTask)) != null) {
                        BasicRibbonUI.this.scrollAndRevealTaskToggleButton(jRibbonTaskToggleButton);
                    }
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            KeyTipManager keyTipManager = KeyTipManager.defaultManager();
                            if (keyTipManager.isShowingKeyTips()) {
                                KeyTipManager.KeyTipChain keyTipChain = keyTipManager.getCurrentlyShownKeyTipChain();
                                if (keyTipChain.chainParentComponent == BasicRibbonUI.this.taskToggleButtons.get(ribbonTask)) {
                                    keyTipManager.refreshCurrentChain();
                                }
                            }
                        }
                    });
                }
                if ("applicationMenuRichTooltip".equals(propertyChangeEvent.getPropertyName())) {
                    BasicRibbonUI.this.syncApplicationMenuTips();
                }
                if ("applicationMenuKeyTip".equals(propertyChangeEvent.getPropertyName())) {
                    BasicRibbonUI.this.syncApplicationMenuTips();
                }
                if ("applicationMenu".equals(propertyChangeEvent.getPropertyName())) {
                    BasicRibbonUI.this.ribbon.revalidate();
                    BasicRibbonUI.this.ribbon.doLayout();
                    BasicRibbonUI.this.ribbon.repaint();
                    object = SwingUtilities.getWindowAncestor(BasicRibbonUI.this.ribbon);
                    if (object instanceof JRibbonFrame) {
                        FlamingoUtilities.updateRibbonFrameIconImages((JRibbonFrame)object);
                    }
                }
                if ("minimized".equals(propertyChangeEvent.getPropertyName())) {
                    PopupPanelManager.defaultManager().hidePopups(null);
                    BasicRibbonUI.this.ribbon.revalidate();
                    BasicRibbonUI.this.ribbon.doLayout();
                    BasicRibbonUI.this.ribbon.repaint();
                }
            }

        };
        this.ribbon.addPropertyChangeListener(this.propertyChangeListener);
        this.ribbonContainerListener = new ContainerAdapter(){

            @Override
            public void componentAdded(ContainerEvent containerEvent) {
                if (BasicRibbonUI.this.isUsingTitlePane()) {
                    return;
                }
                Component component = containerEvent.getComponent();
                if (component != BasicRibbonUI.this.applicationMenuButton) {
                    BasicRibbonUI.this.ribbon.setComponentZOrder(BasicRibbonUI.this.applicationMenuButton, BasicRibbonUI.this.ribbon.getComponentCount() - 1);
                }
            }
        };
        this.ribbon.addContainerListener(this.ribbonContainerListener);
        this.ribbonComponentListener = new ComponentAdapter(){

            @Override
            public void componentResized(ComponentEvent componentEvent) {
                KeyTipManager.defaultManager().hideAllKeyTips();
            }
        };
        this.ribbon.addComponentListener(this.ribbonComponentListener);
    }

    protected void uninstallListeners() {
        this.ribbon.removeChangeListener(this.ribbonChangeListener);
        this.ribbonChangeListener = null;
        this.ribbon.removePropertyChangeListener(this.propertyChangeListener);
        this.propertyChangeListener = null;
        this.ribbon.removeContainerListener(this.ribbonContainerListener);
        this.ribbonContainerListener = null;
        this.ribbon.removeComponentListener(this.ribbonComponentListener);
        this.ribbonComponentListener = null;
    }

    protected void installDefaults() {
        Border border = this.ribbon.getBorder();
        if (border == null || border instanceof UIResource) {
            Border border2 = UIManager.getBorder("Ribbon.border");
            if (border2 == null) {
                border2 = new BorderUIResource.EmptyBorderUIResource(1, 2, 1, 2);
            }
            this.ribbon.setBorder(border2);
        }
    }

    protected void uninstallDefaults() {
    }

    protected void installComponents() {
        this.taskBarPanel = new TaskbarPanel();
        this.taskBarPanel.setName("JRibbon Task Bar");
        this.taskBarPanel.setLayout(this.createTaskbarLayoutManager());
        this.ribbon.add(this.taskBarPanel);
        BandHostPanel bandHostPanel = this.createBandHostPanel();
        bandHostPanel.setLayout(this.createBandHostPanelLayoutManager());
        this.bandScrollablePanel = new JScrollablePanel<BandHostPanel>(bandHostPanel, JScrollablePanel.ScrollType.HORIZONTALLY);
        this.bandScrollablePanel.setScrollOnRollover(false);
        this.bandScrollablePanel.setOpaque(false);
        this.ribbon.add(this.bandScrollablePanel);
        TaskToggleButtonsHostPanel taskToggleButtonsHostPanel = this.createTaskToggleButtonsHostPanel();
        taskToggleButtonsHostPanel.setLayout(this.createTaskToggleButtonsHostPanelLayoutManager());
        this.taskToggleButtonsScrollablePanel = new JScrollablePanel<TaskToggleButtonsHostPanel>(taskToggleButtonsHostPanel, JScrollablePanel.ScrollType.HORIZONTALLY);
        this.taskToggleButtonsScrollablePanel.setScrollOnRollover(false);
        this.taskToggleButtonsScrollablePanel.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                BasicRibbonUI.this.ribbon.repaint();
            }
        });
        this.ribbon.add(this.taskToggleButtonsScrollablePanel);
        this.ribbon.setLayout(this.createLayoutManager());
        this.syncRibbonState();
        this.applicationMenuButton = new JRibbonApplicationMenuButton(this.ribbon);
        this.syncApplicationMenuTips();
        this.ribbon.add(this.applicationMenuButton);
        Window window = SwingUtilities.getWindowAncestor(this.ribbon);
        if (window instanceof JRibbonFrame) {
            FlamingoUtilities.updateRibbonFrameIconImages((JRibbonFrame)window);
        }
    }

    protected LayoutManager createTaskToggleButtonsHostPanelLayoutManager() {
        return new TaskToggleButtonsHostPanelLayout();
    }

    protected TaskToggleButtonsHostPanel createTaskToggleButtonsHostPanel() {
        return new TaskToggleButtonsHostPanel();
    }

    protected BandHostPanel createBandHostPanel() {
        return new BandHostPanel();
    }

    protected LayoutManager createBandHostPanelLayoutManager() {
        return new BandHostPanelLayout();
    }

    protected void uninstallComponents() {
        this.taskBarPanel.removeAll();
        this.taskBarPanel.setLayout(null);
        this.ribbon.remove(this.taskBarPanel);
        BandHostPanel bandHostPanel = this.bandScrollablePanel.getView();
        bandHostPanel.removeAll();
        bandHostPanel.setLayout(null);
        this.ribbon.remove(this.bandScrollablePanel);
        TaskToggleButtonsHostPanel taskToggleButtonsHostPanel = this.taskToggleButtonsScrollablePanel.getView();
        taskToggleButtonsHostPanel.removeAll();
        taskToggleButtonsHostPanel.setLayout(null);
        this.ribbon.remove(this.taskToggleButtonsScrollablePanel);
        this.ribbon.remove(this.applicationMenuButton);
        if (this.helpButton != null) {
            this.ribbon.remove(this.helpButton);
        }
        this.ribbon.setLayout(null);
    }

    @Override
    public void update(Graphics graphics, JComponent jComponent) {
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        RenderingUtils.installDesktopHints(graphics2D);
        RenderingUtils.setupTextAntialiasing(graphics2D, jComponent);
        super.update(graphics2D, jComponent);
        graphics2D.dispose();
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        this.paintBackground(graphics);
        if (!this.ribbon.isMinimized()) {
            Insets insets = jComponent.getInsets();
            int n = this.getTaskToggleButtonHeight();
            if (!this.isUsingTitlePane()) {
                n += this.getTaskbarHeight();
            }
            this.paintTaskArea(graphics, 0, insets.top + n, jComponent.getWidth(), jComponent.getHeight() - n - insets.top - insets.bottom);
        } else {
            this.paintMinimizedRibbonSeparator(graphics);
        }
    }

    protected void paintMinimizedRibbonSeparator(Graphics graphics) {
        Color color = FlamingoUtilities.getBorderColor();
        graphics.setColor(color);
        Insets insets = this.ribbon.getInsets();
        graphics.drawLine(0, this.ribbon.getHeight() - insets.bottom, this.ribbon.getWidth(), this.ribbon.getHeight() - insets.bottom);
    }

    protected void paintBackground(Graphics graphics) {
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        graphics2D.setColor(FlamingoUtilities.getColor(Color.lightGray, "Panel.background"));
        graphics2D.fillRect(0, 0, this.ribbon.getWidth(), this.ribbon.getHeight());
        graphics2D.dispose();
    }

    protected void paintTaskArea(Graphics graphics, int n, int n2, int n3, int n4) {
        if (this.ribbon.getTaskCount() == 0) {
            return;
        }
        JRibbonTaskToggleButton jRibbonTaskToggleButton = this.taskToggleButtons.get(this.ribbon.getSelectedTask());
        Rectangle rectangle = jRibbonTaskToggleButton.getBounds();
        Point point = SwingUtilities.convertPoint(jRibbonTaskToggleButton.getParent(), rectangle.getLocation(), this.ribbon);
        Rectangle rectangle2 = this.taskToggleButtonsScrollablePanel.getView().getParent().getBounds();
        rectangle2.setLocation(SwingUtilities.convertPoint(this.taskToggleButtonsScrollablePanel, rectangle2.getLocation(), this.ribbon));
        int n5 = Math.max(point.x + 1, (int)rectangle2.getMinX());
        n5 = Math.min(n5, (int)rectangle2.getMaxX());
        int n6 = Math.min(point.x + rectangle.width - 1, (int)rectangle2.getMaxX());
        n6 = Math.max(n6, (int)rectangle2.getMinX());
        GeneralPath generalPath = FlamingoUtilities.getRibbonBorderOutline(n + 1, n + n3 - 3, n5, n6, point.y, n2, n2 + n4, 2.0f);
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        graphics2D.setColor(FlamingoUtilities.getBorderColor());
        graphics2D.draw(generalPath);
        RibbonTask ribbonTask = this.ribbon.getSelectedTask();
        RibbonContextualTaskGroup ribbonContextualTaskGroup = ribbonTask.getContextualGroup();
        if (ribbonContextualTaskGroup != null) {
            Insets insets = this.ribbon.getInsets();
            int n7 = insets.top + this.getTaskbarHeight();
            int n8 = n7 + 5;
            Color color = ribbonContextualTaskGroup.getHueColor();
            GradientPaint gradientPaint = new GradientPaint(0.0f, n7, FlamingoUtilities.getAlphaColor(color, 63), 0.0f, n8, FlamingoUtilities.getAlphaColor(color, 0));
            graphics2D.setPaint(gradientPaint);
            graphics2D.clip(generalPath);
            graphics2D.fillRect(0, n7, n3, n8 - n7 + 1);
        }
        graphics2D.dispose();
    }

    @Override
    public Rectangle getContextualTaskGroupBounds(RibbonContextualTaskGroup ribbonContextualTaskGroup) {
        int n;
        Rectangle rectangle = null;
        for (n = 0; n < ribbonContextualTaskGroup.getTaskCount(); ++n) {
            JRibbonTaskToggleButton jRibbonTaskToggleButton = this.taskToggleButtons.get(ribbonContextualTaskGroup.getTask(n));
            rectangle = rectangle == null ? jRibbonTaskToggleButton.getBounds() : rectangle.union(jRibbonTaskToggleButton.getBounds());
        }
        n = this.getTabButtonGap();
        Point point = SwingUtilities.convertPoint(this.taskToggleButtonsScrollablePanel.getView(), rectangle.getLocation(), this.ribbon);
        return new Rectangle(point.x - n / 3, point.y - 1, rectangle.width + n * 2 / 3 - 1, rectangle.height + 1);
    }

    protected int getBandGap() {
        return 2;
    }

    protected int getTabButtonGap() {
        return 0;
    }

    protected LayoutManager createLayoutManager() {
        return new RibbonLayout();
    }

    protected LayoutManager createTaskbarLayoutManager() {
        return new TaskbarLayout();
    }

    public int getTaskbarHeight() {
        return 24;
    }

    public int getTaskToggleButtonHeight() {
        return 22;
    }

    protected void syncRibbonState() {
        int n;
        Object object;
        BandHostPanel bandHostPanel = this.bandScrollablePanel.getView();
        bandHostPanel.removeAll();
        TaskToggleButtonsHostPanel taskToggleButtonsHostPanel = this.taskToggleButtonsScrollablePanel.getView();
        taskToggleButtonsHostPanel.removeAll();
        if (this.helpButton != null) {
            this.ribbon.remove(this.helpButton);
            this.helpButton = null;
        }
        List<RibbonTask> list = this.getCurrentlyShownRibbonTasks();
        RibbonTask ribbonTask = this.ribbon.getSelectedTask();
        for (final RibbonTask ribbonTask2 : list) {
            object = new JRibbonTaskToggleButton(ribbonTask2);
            RichTooltip richTooltip = ribbonTask2.getRichTooltip();
            if (richTooltip != null) {
                object.setActionRichTooltip(richTooltip);
            }
            object.setKeyTip(ribbonTask2.getKeyTip());
            object.addActionListener(new ActionListener((JRibbonTaskToggleButton)object, ribbonTask2){
                final /* synthetic */ JRibbonTaskToggleButton val$taskToggleButton;
                final /* synthetic */ RibbonTask val$task;

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            BasicRibbonUI.this.scrollAndRevealTaskToggleButton(6.this.val$taskToggleButton);
                            BasicRibbonUI.this.ribbon.setSelectedTask(6.this.val$task);
                            if (BasicRibbonUI.this.ribbon.isMinimized()) {
                                int n;
                                AbstractRibbonBand abstractRibbonBand;
                                if (Boolean.TRUE.equals(BasicRibbonUI.this.ribbon.getClientProperty("ribbon.internal.justMinimized"))) {
                                    BasicRibbonUI.this.ribbon.putClientProperty("ribbon.internal.justMinimized", null);
                                    return;
                                }
                                List<PopupPanelManager.PopupInfo> list = PopupPanelManager.defaultManager().getShownPath();
                                if (list.size() > 0) {
                                    for (PopupPanelManager.PopupInfo object2 : list) {
                                        if (object2.getPopupOriginator() != 6.this.val$taskToggleButton) continue;
                                        PopupPanelManager.defaultManager().hidePopups(null);
                                        return;
                                    }
                                }
                                PopupPanelManager.defaultManager().hidePopups(null);
                                BasicRibbonUI.this.ribbon.remove(BasicRibbonUI.this.bandScrollablePanel);
                                int n2 = BasicRibbonUI.this.bandScrollablePanel.getView().getPreferredSize().height;
                                Insets insets = BasicRibbonUI.this.ribbon.getInsets();
                                n2 += insets.top + insets.bottom;
                                AbstractRibbonBand abstractRibbonBand2 = abstractRibbonBand = BasicRibbonUI.this.ribbon.getSelectedTask().getBandCount() > 0 ? BasicRibbonUI.this.ribbon.getSelectedTask().getBand(0) : null;
                                if (abstractRibbonBand != null) {
                                    Insets insets2 = abstractRibbonBand.getInsets();
                                    n2 += insets2.top + insets2.bottom;
                                }
                                BandHostPopupPanel bandHostPopupPanel = new BandHostPopupPanel(BasicRibbonUI.this.bandScrollablePanel, new Dimension(BasicRibbonUI.this.ribbon.getWidth(), n2));
                                int n3 = BasicRibbonUI.this.ribbon.getLocationOnScreen().x;
                                int n4 = BasicRibbonUI.this.ribbon.getLocationOnScreen().y + BasicRibbonUI.this.ribbon.getHeight();
                                Rectangle rectangle = BasicRibbonUI.this.ribbon.getGraphicsConfiguration().getBounds();
                                int n5 = bandHostPopupPanel.getPreferredSize().width;
                                if (n3 + n5 > rectangle.x + rectangle.width) {
                                    n3 = rectangle.x + rectangle.width - n5;
                                }
                                if (n4 + (n = bandHostPopupPanel.getPreferredSize().height) > rectangle.y + rectangle.height) {
                                    n4 = rectangle.y + rectangle.height - n;
                                }
                                bandHostPopupPanel.setPreferredSize(new Dimension(BasicRibbonUI.this.ribbon.getWidth(), n2));
                                Popup popup = BlankPopupFixPopupFactory.getPopup(bandHostPopupPanel, n3, n4);
                                PopupPanelManager.PopupListener popupListener = new PopupPanelManager.PopupListener(){

                                    @Override
                                    public void popupShown(PopupPanelManager.PopupEvent popupEvent) {
                                        JComponent jComponent = popupEvent.getPopupOriginator();
                                        if (jComponent instanceof JRibbonTaskToggleButton) {
                                            BasicRibbonUI.this.bandScrollablePanel.doLayout();
                                            BasicRibbonUI.this.bandScrollablePanel.repaint();
                                        }
                                    }

                                    @Override
                                    public void popupHidden(PopupPanelManager.PopupEvent popupEvent) {
                                        JComponent jComponent = popupEvent.getPopupOriginator();
                                        if (jComponent instanceof JRibbonTaskToggleButton) {
                                            BasicRibbonUI.this.ribbon.add(BasicRibbonUI.this.bandScrollablePanel);
                                            PopupPanelManager.defaultManager().removePopupListener(this);
                                            BasicRibbonUI.this.ribbon.revalidate();
                                            BasicRibbonUI.this.ribbon.doLayout();
                                            BasicRibbonUI.this.ribbon.repaint();
                                        }
                                    }
                                };
                                PopupPanelManager.defaultManager().addPopupListener(popupListener);
                                PopupPanelManager.defaultManager().addPopup(6.this.val$taskToggleButton, popup, bandHostPopupPanel);
                            }
                        }

                    });
                }

            });
            object.addMouseListener(new MouseAdapter(){

                @Override
                public void mouseClicked(MouseEvent mouseEvent) {
                    if (BasicRibbonUI.this.ribbon.getSelectedTask() == ribbonTask2 && mouseEvent.getClickCount() == 2) {
                        boolean bl = BasicRibbonUI.this.ribbon.isMinimized();
                        BasicRibbonUI.this.ribbon.setMinimized(!bl);
                        if (!bl) {
                            BasicRibbonUI.this.ribbon.putClientProperty("ribbon.internal.justMinimized", Boolean.TRUE);
                        }
                    }
                }
            });
            if (ribbonTask2.getContextualGroup() != null) {
                object.setContextualGroupHueColor(ribbonTask2.getContextualGroup().getHueColor());
            }
            object.putClientProperty("flamingo.internal.commandButton.ui.dontDisposePopups", Boolean.TRUE);
            this.taskToggleButtonGroup.add((JCommandToggleButton)object);
            taskToggleButtonsHostPanel.add((Component)object);
            this.taskToggleButtons.put(ribbonTask2, (JRibbonTaskToggleButton)object);
        }
        JRibbonTaskToggleButton jRibbonTaskToggleButton = this.taskToggleButtons.get(ribbonTask);
        if (jRibbonTaskToggleButton != null) {
            jRibbonTaskToggleButton.getActionModel().setSelected(true);
        }
        for (n = 0; n < this.ribbon.getTaskCount(); ++n) {
            object = this.ribbon.getTask(n);
            for (AbstractRibbonBand object2 : object.getBands()) {
                bandHostPanel.add(object2);
                object2.setVisible(ribbonTask == object);
            }
        }
        for (n = 0; n < this.ribbon.getContextualTaskGroupCount(); ++n) {
            object = this.ribbon.getContextualTaskGroup(n);
            for (int i = 0; i < object.getTaskCount(); ++i) {
                RibbonTask ribbonTask3 = object.getTask(i);
                for (AbstractRibbonBand abstractRibbonBand : ribbonTask3.getBands()) {
                    bandHostPanel.add(abstractRibbonBand);
                    abstractRibbonBand.setVisible(ribbonTask == ribbonTask3);
                }
            }
        }
        ActionListener actionListener = this.ribbon.getHelpActionListener();
        if (actionListener != null) {
            this.helpButton = new JCommandButton("", this.ribbon.getHelpIcon());
            this.helpButton.setToolTipText("Read the User Guide");
            this.helpButton.setDisplayState(CommandButtonDisplayState.SMALL);
            this.helpButton.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_ONLY);
            this.helpButton.getActionModel().addActionListener(actionListener);
            this.ribbon.add(this.helpButton);
        }
        this.ribbon.revalidate();
        this.ribbon.repaint();
    }

    protected List<RibbonTask> getCurrentlyShownRibbonTasks() {
        Object object;
        int n;
        ArrayList<RibbonTask> arrayList = new ArrayList<RibbonTask>();
        for (n = 0; n < this.ribbon.getTaskCount(); ++n) {
            object = this.ribbon.getTask(n);
            arrayList.add((RibbonTask)object);
        }
        for (n = 0; n < this.ribbon.getContextualTaskGroupCount(); ++n) {
            object = this.ribbon.getContextualTaskGroup(n);
            if (!this.ribbon.isVisible((RibbonContextualTaskGroup)object)) continue;
            for (int i = 0; i < object.getTaskCount(); ++i) {
                RibbonTask ribbonTask = object.getTask(i);
                arrayList.add(ribbonTask);
            }
        }
        return arrayList;
    }

    protected boolean isUsingTitlePane() {
        return Boolean.TRUE.equals(this.ribbon.getClientProperty("ribbon.internal.isUsingTitlePane"));
    }

    protected void syncApplicationMenuTips() {
        this.applicationMenuButton.setPopupRichTooltip(this.ribbon.getApplicationMenuRichTooltip());
        this.applicationMenuButton.setPopupKeyTip(this.ribbon.getApplicationMenuKeyTip());
    }

    @Override
    public boolean isShowingScrollsForTaskToggleButtons() {
        return this.taskToggleButtonsScrollablePanel.isShowingScrollButtons();
    }

    @Override
    public boolean isShowingScrollsForBands() {
        return this.bandScrollablePanel.isShowingScrollButtons();
    }

    public Map<RibbonTask, JRibbonTaskToggleButton> getTaskToggleButtons() {
        return Collections.unmodifiableMap(this.taskToggleButtons);
    }

    @Override
    public void handleMouseWheelEvent(MouseWheelEvent mouseWheelEvent) {
        int n;
        if (this.ribbon.isMinimized()) {
            return;
        }
        final List<RibbonTask> list = this.getCurrentlyShownRibbonTasks();
        if (list.size() == 0) {
            return;
        }
        int n2 = mouseWheelEvent.getWheelRotation();
        if (n2 == 0) {
            return;
        }
        int n3 = list.indexOf(this.ribbon.getSelectedTask());
        if (!this.ribbon.getComponentOrientation().isLeftToRight()) {
            n2 = - n2;
        }
        if ((n = n3 + (n2 > 0 ? 1 : -1)) < 0) {
            return;
        }
        if (n >= list.size()) {
            return;
        }
        final int n4 = n;
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                BasicRibbonUI.this.ribbon.setCursor(Cursor.getPredefinedCursor(3));
                BasicRibbonUI.this.ribbon.setSelectedTask((RibbonTask)list.get(n4));
                BasicRibbonUI.this.ribbon.setCursor(Cursor.getPredefinedCursor(0));
            }
        });
    }

    protected void scrollAndRevealTaskToggleButton(JRibbonTaskToggleButton jRibbonTaskToggleButton) {
        Point point = SwingUtilities.convertPoint(jRibbonTaskToggleButton.getParent(), jRibbonTaskToggleButton.getLocation(), this.taskToggleButtonsScrollablePanel.getView());
        this.taskToggleButtonsScrollablePanel.scrollToIfNecessary(point.x, jRibbonTaskToggleButton.getWidth());
    }

    protected static class BandHostPopupPanel
    extends JPopupPanel {
        public BandHostPopupPanel(Component component, Dimension dimension) {
            this.setLayout(new BorderLayout());
            this.add(component, "Center");
            this.setPreferredSize(dimension);
            this.setSize(dimension);
        }
    }

    private class TaskToggleButtonsHostPanelLayout
    implements LayoutManager {
        private TaskToggleButtonsHostPanelLayout() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            int n = BasicRibbonUI.this.getTabButtonGap();
            int n2 = BasicRibbonUI.this.getTaskToggleButtonHeight();
            int n3 = 0;
            List<RibbonTask> list = BasicRibbonUI.this.getCurrentlyShownRibbonTasks();
            for (RibbonTask ribbonTask : list) {
                JRibbonTaskToggleButton jRibbonTaskToggleButton = BasicRibbonUI.this.taskToggleButtons.get(ribbonTask);
                int n4 = jRibbonTaskToggleButton.getPreferredSize().width;
                n3 += n4 + n;
            }
            return new Dimension(n3, n2);
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            int n = BasicRibbonUI.this.getTabButtonGap();
            int n2 = BasicRibbonUI.this.getTaskToggleButtonHeight();
            int n3 = 0;
            List<RibbonTask> list = BasicRibbonUI.this.getCurrentlyShownRibbonTasks();
            for (RibbonTask ribbonTask : list) {
                JRibbonTaskToggleButton jRibbonTaskToggleButton = BasicRibbonUI.this.taskToggleButtons.get(ribbonTask);
                int n4 = jRibbonTaskToggleButton.getMinimumSize().width;
                n3 += n4 + n;
            }
            return new Dimension(n3, n2);
        }

        @Override
        public void layoutContainer(Container container) {
            int n = 0;
            int n2 = BasicRibbonUI.this.getTabButtonGap();
            int n3 = BasicRibbonUI.this.getTaskToggleButtonHeight();
            int n4 = 0;
            int n5 = 0;
            List<RibbonTask> list = BasicRibbonUI.this.getCurrentlyShownRibbonTasks();
            HashMap<JRibbonTaskToggleButton, Integer> hashMap = new HashMap<JRibbonTaskToggleButton, Integer>();
            int n6 = 0;
            for (RibbonTask ribbonTask : list) {
                JRibbonTaskToggleButton jRibbonTaskToggleButton = BasicRibbonUI.this.taskToggleButtons.get(ribbonTask);
                int n7 = jRibbonTaskToggleButton.getPreferredSize().width;
                int n8 = jRibbonTaskToggleButton.getMinimumSize().width;
                hashMap.put(jRibbonTaskToggleButton, n7 - n8);
                n6 += n7 - n8;
                n4 += n7;
                n5 += n8;
            }
            n5 += n2 * list.size();
            boolean bl = container.getComponentOrientation().isLeftToRight();
            if ((n4 += n2 * list.size()) <= container.getWidth()) {
                int n9 = bl ? 0 : container.getWidth();
                for (RibbonTask ribbonTask2 : list) {
                    JRibbonTaskToggleButton jRibbonTaskToggleButton = BasicRibbonUI.this.taskToggleButtons.get(ribbonTask2);
                    int n10 = jRibbonTaskToggleButton.getPreferredSize().width;
                    if (bl) {
                        jRibbonTaskToggleButton.setBounds(n9, n + 1, n10, n3 - 1);
                        n9 += n10 + n2;
                        continue;
                    }
                    jRibbonTaskToggleButton.setBounds(n9 - n10, n + 1, n10, n3 - 1);
                    n9 -= n10 + n2;
                }
                ((JComponent)container).putClientProperty("flamingo.internal.ribbon.taskToggleButtonsHostPanel.isSquished", null);
            } else {
                if (n5 > container.getWidth()) {
                    throw new IllegalStateException("Available width not enough to host minimized task tab buttons");
                }
                int n11 = bl ? 0 : container.getWidth();
                int n12 = n4 - container.getWidth() + 2;
                for (RibbonTask ribbonTask3 : list) {
                    JRibbonTaskToggleButton jRibbonTaskToggleButton = BasicRibbonUI.this.taskToggleButtons.get(ribbonTask3);
                    int n13 = jRibbonTaskToggleButton.getPreferredSize().width;
                    int n14 = n12 * (Integer)hashMap.get(jRibbonTaskToggleButton) / n6;
                    int n15 = n13 - n14;
                    if (bl) {
                        jRibbonTaskToggleButton.setBounds(n11, n + 1, n15, n3 - 1);
                        n11 += n15 + n2;
                    } else {
                        jRibbonTaskToggleButton.setBounds(n11 - n15, n + 1, n15, n3 - 1);
                        n11 -= n15 + n2;
                    }
                    RichTooltip richTooltip = new RichTooltip();
                    richTooltip.setTitle(ribbonTask3.getTitle());
                    jRibbonTaskToggleButton.setActionRichTooltip(richTooltip);
                }
                ((JComponent)container).putClientProperty("flamingo.internal.ribbon.taskToggleButtonsHostPanel.isSquished", Boolean.TRUE);
            }
        }
    }

    protected class TaskToggleButtonsHostPanel
    extends JPanel {
        public static final String IS_SQUISHED = "flamingo.internal.ribbon.taskToggleButtonsHostPanel.isSquished";

        protected TaskToggleButtonsHostPanel() {
        }

        @Override
        protected void paintComponent(Graphics graphics) {
            super.paintComponent(graphics);
            this.paintContextualTaskGroupsOutlines(graphics);
            if (Boolean.TRUE.equals(this.getClientProperty("flamingo.internal.ribbon.taskToggleButtonsHostPanel.isSquished"))) {
                this.paintTaskOutlines(graphics);
            }
        }

        protected void paintTaskOutlines(Graphics graphics) {
            Object object;
            int n;
            Object object22;
            Graphics2D graphics2D = (Graphics2D)graphics.create();
            Color color = FlamingoUtilities.getBorderColor();
            GradientPaint gradientPaint = new GradientPaint(0.0f, 0.0f, FlamingoUtilities.getAlphaColor(color, 0), 0.0f, this.getHeight(), color);
            graphics2D.setPaint(gradientPaint);
            HashSet<Object> hashSet = new HashSet<Object>();
            for (n = 0; n < BasicRibbonUI.this.ribbon.getTaskCount() - 1; ++n) {
                object22 = BasicRibbonUI.this.ribbon.getTask(n);
                hashSet.add((RibbonTask)object22);
            }
            for (n = 0; n < BasicRibbonUI.this.ribbon.getContextualTaskGroupCount(); ++n) {
                object22 = BasicRibbonUI.this.ribbon.getContextualTaskGroup(n);
                if (!BasicRibbonUI.this.ribbon.isVisible((RibbonContextualTaskGroup)object22)) continue;
                for (int i = 0; i < object22.getTaskCount() - 1; ++i) {
                    object = object22.getTask(i);
                    hashSet.add(object);
                }
            }
            for (Object object22 : hashSet) {
                JRibbonTaskToggleButton jRibbonTaskToggleButton = BasicRibbonUI.this.taskToggleButtons.get(object22);
                object = jRibbonTaskToggleButton.getBounds();
                int n2 = object.x + object.width + BasicRibbonUI.this.getTabButtonGap() / 2 - 1;
                graphics2D.drawLine(n2, 0, n2, this.getHeight());
            }
            graphics2D.dispose();
        }

        protected void paintContextualTaskGroupsOutlines(Graphics graphics) {
            for (int i = 0; i < BasicRibbonUI.this.ribbon.getContextualTaskGroupCount(); ++i) {
                RibbonContextualTaskGroup ribbonContextualTaskGroup = BasicRibbonUI.this.ribbon.getContextualTaskGroup(i);
                if (!BasicRibbonUI.this.ribbon.isVisible(ribbonContextualTaskGroup)) continue;
                Rectangle rectangle = BasicRibbonUI.this.getContextualTaskGroupBounds(ribbonContextualTaskGroup);
                rectangle.setLocation(SwingUtilities.convertPoint(BasicRibbonUI.this.ribbon, rectangle.getLocation(), BasicRibbonUI.this.taskToggleButtonsScrollablePanel.getView()));
                this.paintContextualTaskGroupOutlines(graphics, ribbonContextualTaskGroup, rectangle);
            }
        }

        protected void paintContextualTaskGroupOutlines(Graphics graphics, RibbonContextualTaskGroup ribbonContextualTaskGroup, Rectangle rectangle) {
            Graphics2D graphics2D = (Graphics2D)graphics.create();
            Color color = FlamingoUtilities.getBorderColor();
            GradientPaint gradientPaint = new GradientPaint(0.0f, rectangle.y, color, 0.0f, rectangle.y + rectangle.height, FlamingoUtilities.getAlphaColor(color, 0));
            graphics2D.setPaint(gradientPaint);
            int n = rectangle.x;
            graphics2D.drawLine(n, rectangle.y, n, rectangle.y + rectangle.height);
            n = rectangle.x + rectangle.width;
            graphics2D.drawLine(n, rectangle.y, n, rectangle.y + rectangle.height);
            graphics2D.dispose();
        }
    }

    private class BandHostPanelLayout
    implements LayoutManager {
        private BandHostPanelLayout() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            int n = 0;
            if (BasicRibbonUI.this.ribbon.getTaskCount() > 0) {
                RibbonTask ribbonTask = BasicRibbonUI.this.ribbon.getSelectedTask();
                for (AbstractRibbonBand abstractRibbonBand : ribbonTask.getBands()) {
                    int n2 = abstractRibbonBand.getPreferredSize().height;
                    Insets insets = abstractRibbonBand.getInsets();
                    n = Math.max(n, n2 + insets.top + insets.bottom);
                }
            }
            return new Dimension(container.getWidth(), n);
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            int n = 0;
            int n2 = 0;
            int n3 = BasicRibbonUI.this.getBandGap();
            RibbonTask ribbonTask = BasicRibbonUI.this.ribbon.getSelectedTask();
            for (AbstractRibbonBand abstractRibbonBand : ribbonTask.getBands()) {
                int n4 = abstractRibbonBand.getMinimumSize().height;
                Insets insets = abstractRibbonBand.getInsets();
                RibbonBandUI ribbonBandUI = abstractRibbonBand.getUI();
                int n5 = ribbonBandUI.getPreferredCollapsedWidth() + insets.left + insets.right;
                n += n5;
                n2 = Math.max(n2, n4);
            }
            return new Dimension(n += n3 * (ribbonTask.getBandCount() + 1), n2);
        }

        @Override
        public void layoutContainer(Container container) {
            Object object5;
            Object object4;
            Object object2;
            int n;
            boolean bl;
            Object object3;
            int n2 = BasicRibbonUI.this.getBandGap();
            int n3 = 0;
            int n4 = 0;
            RibbonTask ribbonTask = BasicRibbonUI.this.ribbon.getSelectedTask();
            if (ribbonTask == null) {
                return;
            }
            for (AbstractRibbonBand abstractRibbonBand22 : ribbonTask.getBands()) {
                FlamingoUtilities.checkResizePoliciesConsistency(abstractRibbonBand22);
            }
            for (AbstractRibbonBand abstractRibbonBand22 : ribbonTask.getBands()) {
                object2 = abstractRibbonBand22.getResizePolicies();
                object4 = object2.get(0);
                abstractRibbonBand22.setCurrentResizePolicy((RibbonBandResizePolicy)object4);
            }
            int n5 = container.getHeight();
            int n6 = container.getWidth();
            if (ribbonTask.getBandCount() > 0) {
                object2 = ribbonTask.getResizeSequencingPolicy();
                object2.reset();
                object4 = object2.next();
                do {
                    boolean bl2 = true;
                    for (AbstractRibbonBand abstractRibbonBand : ribbonTask.getBands()) {
                        object5 = abstractRibbonBand.getCurrentResizePolicy();
                        if (object5 == (object3 = abstractRibbonBand.getResizePolicies()).get(object3.size() - 1)) continue;
                        bl2 = false;
                        break;
                    }
                    if (bl2) break;
                    int n7 = 0;
                    for (Object object5 : ribbonTask.getBands()) {
                        object3 = object5.getCurrentResizePolicy();
                        Insets insets = object5.getInsets();
                        Object t = object5.getControlPanel();
                        if (t == null) {
                            t = object5.getPopupRibbonBand().getControlPanel();
                        }
                        Insets insets2 = t.getInsets();
                        n = t.getUI().getLayoutGap();
                        int n8 = n5 - insets.top - insets.bottom;
                        int n9 = n8 - object5.getUI().getBandTitleHeight();
                        if (t != null) {
                            n9 = n9 - insets2.top - insets2.bottom;
                        }
                        int n10 = object3.getPreferredWidth(n9, n) + insets.left + insets.right;
                        n7 += n10 + n2;
                    }
                    if (n7 < n6) break;
                    List<RibbonBandResizePolicy> list2 = object4.getResizePolicies();
                    int n11 = list2.indexOf(object4.getCurrentResizePolicy());
                    if (n11 != list2.size() - 1) {
                        object4.setCurrentResizePolicy(list2.get(n11 + 1));
                    }
                    object4 = object2.next();
                } while (true);
            }
            n3 = (bl = container.getComponentOrientation().isLeftToRight()) ? 1 : container.getWidth() - 1;
            for (AbstractRibbonBand abstractRibbonBand3 : ribbonTask.getBands()) {
                Insets insets = abstractRibbonBand3.getInsets();
                RibbonBandResizePolicy ribbonBandResizePolicy = abstractRibbonBand3.getCurrentResizePolicy();
                object5 = abstractRibbonBand3.getControlPanel();
                if (object5 == null) {
                    object5 = abstractRibbonBand3.getPopupRibbonBand().getControlPanel();
                }
                object3 = object5.getInsets();
                int n12 = object5.getUI().getLayoutGap();
                int n13 = n5;
                int n14 = n13 - insets.top - insets.bottom - abstractRibbonBand3.getUI().getBandTitleHeight();
                if (object3 != null) {
                    n14 = n14 - object3.top - object3.bottom;
                }
                n = ribbonBandResizePolicy.getPreferredWidth(n14, n12) + insets.left + insets.right;
                if (bl) {
                    abstractRibbonBand3.setBounds(n3, n4, n, n13);
                } else {
                    abstractRibbonBand3.setBounds(n3 - n, n4, n, n13);
                }
                if (abstractRibbonBand3.getHeight() > 0) {
                    abstractRibbonBand3.doLayout();
                }
                if (bl) {
                    n3 += n + n2;
                    continue;
                }
                n3 -= n + n2;
            }
        }
    }

    protected static class BandHostPanel
    extends JPanel {
        protected BandHostPanel() {
        }
    }

    private class TaskbarPanel
    extends JPanel {
        public TaskbarPanel() {
            this.setOpaque(false);
            this.setBorder(new EmptyBorder(1, 0, 1, 0));
        }

        @Override
        protected void paintComponent(Graphics graphics) {
            int n;
            int n2;
            Shape shape = this.getOutline(this);
            Graphics2D graphics2D = (Graphics2D)graphics.create();
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            RenderingUtils.installDesktopHints(graphics2D);
            RenderingUtils.setupTextAntialiasing(graphics2D, null);
            if (shape != null) {
                graphics2D.setComposite(AlphaComposite.SrcOver.derive(0.6f));
                graphics2D.setColor(FlamingoUtilities.getColor(Color.lightGray.brighter(), "Panel.background"));
                graphics2D.fill(shape);
                graphics2D.setColor(FlamingoUtilities.getBorderColor().darker());
                graphics2D.draw(shape);
            }
            boolean bl = this.getComponentOrientation().isLeftToRight();
            int n3 = 0;
            int n4 = this.getWidth();
            if (this.getComponentCount() == 0) {
                n3 = 1;
                n4 = this.getWidth() - 1;
                if (BasicRibbonUI.this.applicationMenuButton.isVisible()) {
                    n3 += BasicRibbonUI.this.applicationMenuButton.getX() + BasicRibbonUI.this.applicationMenuButton.getWidth();
                    n4 = BasicRibbonUI.this.applicationMenuButton.getX() - 1;
                }
            } else {
                for (n2 = 0; n2 < this.getComponentCount(); ++n2) {
                    Component component = this.getComponent(n2);
                    n3 = Math.max(n3, component.getX() + component.getWidth());
                    n4 = Math.min(n4, component.getX());
                }
            }
            n2 = this.getHeight();
            if (bl) {
                graphics2D.drawLine(n3, n2 - 1, this.getWidth(), n2 - 1);
            } else {
                graphics2D.drawLine(0, n2 - 1, n4, n2 - 1);
            }
            int n5 = shape != null ? (int)shape.getBounds2D().getMaxX() + 6 : 6;
            int n6 = n = shape != null ? (int)shape.getBounds2D().getMinX() - 6 : 6;
            if (!BasicRibbonUI.this.isShowingScrollsForTaskToggleButtons()) {
                graphics2D.setComposite(AlphaComposite.SrcOver);
                graphics2D.translate(- this.getBounds().x, 0);
                for (int i = 0; i < BasicRibbonUI.this.ribbon.getContextualTaskGroupCount(); ++i) {
                    int n7;
                    RibbonContextualTaskGroup ribbonContextualTaskGroup = BasicRibbonUI.this.ribbon.getContextualTaskGroup(i);
                    if (!BasicRibbonUI.this.ribbon.isVisible(ribbonContextualTaskGroup)) continue;
                    Rectangle rectangle = BasicRibbonUI.this.getContextualTaskGroupBounds(ribbonContextualTaskGroup);
                    Color color = ribbonContextualTaskGroup.getHueColor();
                    GradientPaint gradientPaint = new GradientPaint(0.0f, 0.0f, FlamingoUtilities.getAlphaColor(color, 0), 0.0f, n2, FlamingoUtilities.getAlphaColor(color, 63));
                    graphics2D.setPaint(gradientPaint);
                    int n8 = bl ? rectangle.x : Math.min(n, rectangle.x);
                    int n9 = n7 = bl ? rectangle.x + rectangle.width - n8 : Math.min(rectangle.x + rectangle.width, n) - n8;
                    if (n7 <= 0) continue;
                    graphics2D.fillRect(n8, 0, n7, n2);
                    graphics2D.setColor(color);
                    graphics2D.drawLine(n8 + 1, n2 - 1, n8 + n7, n2 - 1);
                    graphics2D.setColor(FlamingoUtilities.getColor(Color.black, "Button.foreground"));
                    FontMetrics fontMetrics = this.getFontMetrics(BasicRibbonUI.this.ribbon.getFont());
                    int n10 = (n2 + fontMetrics.getHeight()) / 2 - fontMetrics.getDescent();
                    int n11 = n7 - 10;
                    String string = ribbonContextualTaskGroup.getTitle();
                    if (fontMetrics.stringWidth(string) > n11) {
                        while (string.length() != 0 && fontMetrics.stringWidth(string + "...") > n11) {
                            string = string.substring(0, string.length() - 1);
                        }
                        string = string + "...";
                    }
                    if (bl) {
                        BasicGraphicsUtils.drawString(graphics2D, string, -1, n8 + 5, n10);
                    } else {
                        BasicGraphicsUtils.drawString(graphics2D, string, -1, n8 + n7 - 5 - fontMetrics.stringWidth(string), n10);
                    }
                    Color color2 = FlamingoUtilities.getBorderColor();
                    graphics2D.setPaint(new GradientPaint(0.0f, 0.0f, FlamingoUtilities.getAlphaColor(color2, 0), 0.0f, n2, color2));
                    graphics2D.drawLine(n8, 0, n8, n2);
                    graphics2D.drawLine(n8 + n7, 0, n8 + n7, n2);
                }
            }
            graphics2D.dispose();
        }

        protected Shape getOutline(TaskbarPanel taskbarPanel) {
            Component component;
            double d = this.getHeight() - 1;
            boolean bl = taskbarPanel.getComponentOrientation().isLeftToRight();
            if (this.getComponentCount() == 0) {
                if (BasicRibbonUI.this.applicationMenuButton.isVisible()) {
                    if (bl) {
                        int n = 1;
                        if (BasicRibbonUI.this.applicationMenuButton.isVisible()) {
                            n += BasicRibbonUI.this.applicationMenuButton.getX() + BasicRibbonUI.this.applicationMenuButton.getWidth();
                        }
                        return new Arc2D.Double((double)(n - 1) - 2.0 * d, 0.0, 2.0 * d, 2.0 * d, 0.0, 90.0, 0);
                    }
                    int n = taskbarPanel.getWidth() - 1;
                    if (BasicRibbonUI.this.applicationMenuButton.isVisible()) {
                        n = BasicRibbonUI.this.applicationMenuButton.getX() - 1;
                    }
                    return new Arc2D.Double(n + 1, 0.0, 2.0 * d, 2.0 * d, 90.0, 90.0, 0);
                }
                return null;
            }
            int n = this.getWidth();
            int n2 = 0;
            for (int i = 0; i < this.getComponentCount(); ++i) {
                component = this.getComponent(i);
                n = Math.min(n, component.getX());
                n2 = Math.max(n2, component.getX() + component.getWidth());
            }
            float f = (float)d / 2.0f;
            component = new GeneralPath();
            if (bl) {
                if (BasicRibbonUI.this.applicationMenuButton.isVisible()) {
                    component.moveTo((float)(n + 5) - 2.0f * f, 0.0f);
                } else {
                    component.moveTo(n - 1, 0.0f);
                }
                component.lineTo(n2, 0.0f);
                component.append(new Arc2D.Double((float)n2 - f, 0.0, d, d, 90.0, -180.0, 0), true);
                component.lineTo((double)(n - 1), d);
                if (BasicRibbonUI.this.applicationMenuButton.isVisible()) {
                    component.append(new Arc2D.Double((double)(n - 1) - 2.0 * d, 0.0, 2.0 * d, 2.0 * d, 0.0, 90.0, 0), true);
                } else {
                    component.lineTo(n - 1, 0.0f);
                }
            } else {
                if (BasicRibbonUI.this.applicationMenuButton.isVisible()) {
                    component.moveTo((float)(n2 - 5) + 2.0f * f, 0.0f);
                } else {
                    component.moveTo(n2 - 1, 0.0f);
                }
                component.lineTo(n, 0.0f);
                component.append(new Arc2D.Double((float)n - f, 0.0, d, d, 90.0, 180.0, 0), true);
                component.lineTo((double)(n2 - 1), d);
                if (BasicRibbonUI.this.applicationMenuButton.isVisible()) {
                    component.append(new Arc2D.Double(n2 - 1, 0.0, 2.0 * d, 2.0 * d, 180.0, -90.0, 0), true);
                } else {
                    component.lineTo(n2 + 1, 0.0f);
                }
            }
            return component;
        }

        @Override
        public Dimension getPreferredSize() {
            Dimension dimension = super.getPreferredSize();
            return new Dimension(dimension.width + dimension.height / 2, dimension.height);
        }
    }

    private class TaskbarLayout
    implements LayoutManager {
        private TaskbarLayout() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            Insets insets = container.getInsets();
            int n = 0;
            int n2 = BasicRibbonUI.this.getBandGap();
            for (Component component : BasicRibbonUI.this.ribbon.getTaskbarComponents()) {
                n += component.getPreferredSize().width;
                n += n2;
            }
            return new Dimension(n + insets.left + insets.right, BasicRibbonUI.this.getTaskbarHeight() + insets.top + insets.bottom);
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            return this.preferredLayoutSize(container);
        }

        @Override
        public void layoutContainer(Container container) {
            Insets insets = container.getInsets();
            int n = BasicRibbonUI.this.getBandGap();
            boolean bl = container.getComponentOrientation().isLeftToRight();
            if (bl) {
                int n2 = insets.left + 1;
                if (BasicRibbonUI.this.applicationMenuButton.isVisible()) {
                    n2 += BasicRibbonUI.this.applicationMenuButton.getX() + BasicRibbonUI.this.applicationMenuButton.getWidth();
                }
                for (Component component : BasicRibbonUI.this.ribbon.getTaskbarComponents()) {
                    int n3 = component.getPreferredSize().width;
                    component.setBounds(n2, insets.top + 1, n3, container.getHeight() - insets.top - insets.bottom - 2);
                    n2 += n3 + n;
                }
            } else {
                int n4 = container.getWidth() - insets.right - 1;
                if (BasicRibbonUI.this.applicationMenuButton.isVisible()) {
                    n4 = BasicRibbonUI.this.applicationMenuButton.getX() - 1;
                }
                for (Component component : BasicRibbonUI.this.ribbon.getTaskbarComponents()) {
                    int n5 = component.getPreferredSize().width;
                    component.setBounds(n4 - n5, insets.top + 1, n5, container.getHeight() - insets.top - insets.bottom - 2);
                    n4 -= n5 + n;
                }
            }
        }
    }

    private class RibbonLayout
    implements LayoutManager {
        private RibbonLayout() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            Insets insets = container.getInsets();
            int n = 0;
            boolean bl = BasicRibbonUI.this.ribbon.isMinimized();
            if (!bl && BasicRibbonUI.this.ribbon.getTaskCount() > 0) {
                RibbonTask ribbonTask = BasicRibbonUI.this.ribbon.getSelectedTask();
                for (AbstractRibbonBand abstractRibbonBand : ribbonTask.getBands()) {
                    int n2 = abstractRibbonBand.getPreferredSize().height;
                    Insets insets2 = abstractRibbonBand.getInsets();
                    n = Math.max(n, n2 + insets2.top + insets2.bottom);
                }
            }
            int n3 = BasicRibbonUI.this.getTaskToggleButtonHeight();
            if (!BasicRibbonUI.this.isUsingTitlePane()) {
                n3 += BasicRibbonUI.this.getTaskbarHeight();
            }
            int n4 = n + n3 + insets.top + insets.bottom;
            return new Dimension(container.getWidth(), n4);
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            Insets insets = container.getInsets();
            int n = 0;
            int n2 = 0;
            int n3 = BasicRibbonUI.this.getBandGap();
            int n4 = BasicRibbonUI.this.getTaskToggleButtonHeight();
            if (!BasicRibbonUI.this.isUsingTitlePane()) {
                n4 += BasicRibbonUI.this.getTaskbarHeight();
            }
            if (BasicRibbonUI.this.ribbon.getTaskCount() > 0) {
                boolean bl = BasicRibbonUI.this.ribbon.isMinimized();
                RibbonTask ribbonTask = BasicRibbonUI.this.ribbon.getSelectedTask();
                for (AbstractRibbonBand abstractRibbonBand : ribbonTask.getBands()) {
                    int n5 = abstractRibbonBand.getMinimumSize().height;
                    Insets insets2 = abstractRibbonBand.getInsets();
                    RibbonBandUI ribbonBandUI = abstractRibbonBand.getUI();
                    n += ribbonBandUI.getPreferredCollapsedWidth();
                    if (bl) continue;
                    n2 = Math.max(n2, n5 + insets2.top + insets2.bottom);
                }
                n += n3 * (ribbonTask.getBandCount() - 1);
            } else {
                n = 50;
            }
            return new Dimension(n, n2 + n4 + insets.top + insets.bottom);
        }

        @Override
        public void layoutContainer(Container container) {
            int n;
            Insets insets = container.getInsets();
            int n2 = BasicRibbonUI.this.getTabButtonGap();
            boolean bl = BasicRibbonUI.this.ribbon.getComponentOrientation().isLeftToRight();
            int n3 = container.getWidth();
            int n4 = BasicRibbonUI.this.getTaskbarHeight();
            int n5 = insets.top;
            boolean bl2 = BasicRibbonUI.this.isUsingTitlePane();
            if (!bl2) {
                BasicRibbonUI.this.taskBarPanel.removeAll();
                for (Component component : BasicRibbonUI.this.ribbon.getTaskbarComponents()) {
                    BasicRibbonUI.this.taskBarPanel.add(component);
                }
                BasicRibbonUI.this.taskBarPanel.setBounds(insets.left, insets.top, n3 - insets.left - insets.right, n4);
                n5 += n4;
            } else {
                BasicRibbonUI.this.taskBarPanel.setBounds(0, 0, 0, 0);
            }
            int n6 = BasicRibbonUI.this.getTaskToggleButtonHeight();
            int n7 = bl ? insets.left : n3 - insets.right;
            int n8 = n4 + n6;
            if (!bl2) {
                BasicRibbonUI.this.applicationMenuButton.setVisible(BasicRibbonUI.this.ribbon.getApplicationMenu() != null);
                if (BasicRibbonUI.this.ribbon.getApplicationMenu() != null) {
                    if (bl) {
                        BasicRibbonUI.this.applicationMenuButton.setBounds(n7, insets.top, n8, n8);
                    } else {
                        BasicRibbonUI.this.applicationMenuButton.setBounds(n7 - n8, insets.top, n8, n8);
                    }
                }
            } else {
                BasicRibbonUI.this.applicationMenuButton.setVisible(false);
            }
            int n9 = n7 = bl ? n7 + 2 : n7 - 2;
            if (FlamingoUtilities.getApplicationMenuButton(SwingUtilities.getWindowAncestor(BasicRibbonUI.this.ribbon)) != null) {
                int n10 = n7 = bl ? n7 + n8 : n7 - n8;
            }
            if (BasicRibbonUI.this.helpButton != null) {
                Dimension dimension = BasicRibbonUI.this.helpButton.getPreferredSize();
                if (bl) {
                    BasicRibbonUI.this.helpButton.setBounds(n3 - insets.right - dimension.width, n5, dimension.width, dimension.height);
                } else {
                    BasicRibbonUI.this.helpButton.setBounds(insets.left, n5, dimension.width, dimension.height);
                }
            }
            if (bl) {
                n = BasicRibbonUI.this.helpButton != null ? BasicRibbonUI.this.helpButton.getX() - n2 - n7 : container.getWidth() - insets.right - n7;
                BasicRibbonUI.this.taskToggleButtonsScrollablePanel.setBounds(n7, n5, n, n6);
            } else {
                n = BasicRibbonUI.this.helpButton != null ? n7 - n2 - BasicRibbonUI.this.helpButton.getX() - BasicRibbonUI.this.helpButton.getWidth() : n7 - insets.left;
                BasicRibbonUI.this.taskToggleButtonsScrollablePanel.setBounds(n7 - n, n5, n, n6);
            }
            TaskToggleButtonsHostPanel taskToggleButtonsHostPanel = BasicRibbonUI.this.taskToggleButtonsScrollablePanel.getView();
            int n11 = taskToggleButtonsHostPanel.getMinimumSize().width;
            taskToggleButtonsHostPanel.setPreferredSize(new Dimension(n11, BasicRibbonUI.this.taskToggleButtonsScrollablePanel.getBounds().height));
            BasicRibbonUI.this.taskToggleButtonsScrollablePanel.doLayout();
            n5 += n6;
            int n12 = n6;
            if (!bl2) {
                n12 += n4;
            }
            if (BasicRibbonUI.this.bandScrollablePanel.getParent() == BasicRibbonUI.this.ribbon) {
                if (!BasicRibbonUI.this.ribbon.isMinimized() && BasicRibbonUI.this.ribbon.getTaskCount() > 0) {
                    Insets insets2 = BasicRibbonUI.this.ribbon.getSelectedTask().getBandCount() == 0 ? new Insets(0, 0, 0, 0) : BasicRibbonUI.this.ribbon.getSelectedTask().getBand(0).getInsets();
                    BasicRibbonUI.this.bandScrollablePanel.setBounds(1 + insets.left, n5 + insets2.top, container.getWidth() - 2 * insets.left - 2 * insets.right - 1, container.getHeight() - n12 - insets.top - insets.bottom - insets2.top - insets2.bottom);
                    BandHostPanel bandHostPanel = BasicRibbonUI.this.bandScrollablePanel.getView();
                    int n13 = bandHostPanel.getMinimumSize().width;
                    bandHostPanel.setPreferredSize(new Dimension(n13, BasicRibbonUI.this.bandScrollablePanel.getBounds().height));
                    BasicRibbonUI.this.bandScrollablePanel.doLayout();
                    bandHostPanel.doLayout();
                } else {
                    BasicRibbonUI.this.bandScrollablePanel.setBounds(0, 0, 0, 0);
                }
            }
        }
    }

}

