/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.PanelUI;
import javax.swing.plaf.UIResource;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.ribbon.JRibbonComponent;
import org.pushingpixels.flamingo.api.ribbon.RibbonElementPriority;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.BandControlPanelUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.BasicBandControlPanelUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.JRibbonGallery;

public class JBandControlPanel
extends AbstractBandControlPanel
implements UIResource {
    private Map<String, JRibbonGallery> galleryNameMap = new HashMap<String, JRibbonGallery>();
    private LinkedList<ControlPanelGroup> controlPanelGroups = new LinkedList();
    public static final List<AbstractCommandButton> EMPTY_GALLERY_BUTTONS_LIST = new LinkedList<AbstractCommandButton>();
    public static final List<JRibbonGallery> EMPTY_RIBBON_GALLERIES_LIST = new LinkedList<JRibbonGallery>();
    public static final String uiClassID = "BandControlPanelUI";

    public void setUI(BandControlPanelUI bandControlPanelUI) {
        super.setUI(bandControlPanelUI);
    }

    @Override
    public void updateUI() {
        if (UIManager.get(this.getUIClassID()) != null) {
            this.setUI((BandControlPanelUI)UIManager.getUI(this));
        } else {
            this.setUI(new BasicBandControlPanelUI());
        }
    }

    @Override
    public BandControlPanelUI getUI() {
        return (BandControlPanelUI)this.ui;
    }

    @Override
    public String getUIClassID() {
        return "BandControlPanelUI";
    }

    public synchronized void addCommandButton(AbstractCommandButton abstractCommandButton, RibbonElementPriority ribbonElementPriority) {
        if (this.controlPanelGroups.size() == 0) {
            this.startGroup();
        }
        this.controlPanelGroups.getLast().addCommandButton(abstractCommandButton, ribbonElementPriority);
        super.add(abstractCommandButton);
    }

    public synchronized void addRibbonGallery(JRibbonGallery jRibbonGallery, RibbonElementPriority ribbonElementPriority) {
        String string = jRibbonGallery.getName();
        if (string == null || string.isEmpty()) {
            throw new IllegalArgumentException("Ribbon gallery name null or empty");
        }
        if (this.galleryNameMap.containsKey(string)) {
            throw new IllegalArgumentException("Another riboon gallery with the same name already exists");
        }
        if (this.controlPanelGroups.size() == 0) {
            this.startGroup();
        }
        this.controlPanelGroups.getLast().addRibbonGallery(jRibbonGallery, ribbonElementPriority);
        this.galleryNameMap.put(string, jRibbonGallery);
        super.add(jRibbonGallery);
    }

    public synchronized void setPriority(JCommandButton jCommandButton, RibbonElementPriority ribbonElementPriority) {
        if (this.controlPanelGroups.size() == 0) {
            this.startGroup();
        }
        this.controlPanelGroups.getLast().setPriority(jCommandButton, ribbonElementPriority);
    }

    public synchronized void setPriority(JRibbonGallery jRibbonGallery, RibbonElementPriority ribbonElementPriority) {
        if (this.controlPanelGroups.size() == 0) {
            this.startGroup();
        }
        this.controlPanelGroups.getLast().setPriority(jRibbonGallery, ribbonElementPriority);
    }

    public void addRibbonComponent(JRibbonComponent jRibbonComponent) {
        this.addRibbonComponent(jRibbonComponent, 1);
    }

    public void addRibbonComponent(JRibbonComponent jRibbonComponent, int n) {
        if (this.controlPanelGroups.size() == 0) {
            this.startGroup();
        }
        this.controlPanelGroups.getLast().addRibbonComponent(jRibbonComponent, n);
        super.add(jRibbonComponent);
    }

    public List<ControlPanelGroup> getControlPanelGroups() {
        return Collections.unmodifiableList(this.controlPanelGroups);
    }

    public int getControlPanelGroupCount() {
        if (this.controlPanelGroups == null) {
            return 1;
        }
        return this.controlPanelGroups.size();
    }

    public String getControlPanelGroupTitle(int n) {
        if (this.controlPanelGroups == null) {
            return null;
        }
        return this.controlPanelGroups.get((int)n).groupTitle;
    }

    public int startGroup() {
        return this.startGroup(null);
    }

    public int startGroup(String string) {
        ControlPanelGroup controlPanelGroup = new ControlPanelGroup(string);
        this.controlPanelGroups.addLast(controlPanelGroup);
        this.fireChanged();
        return this.controlPanelGroups.size() - 1;
    }

    public void setGroupTitle(int n, String string) {
        this.controlPanelGroups.get(n).setGroupTitle(string);
        this.fireChanged();
    }

    public JRibbonGallery getRibbonGallery(String string) {
        return this.galleryNameMap.get(string);
    }

    public void addChangeListener(ChangeListener changeListener) {
        this.listenerList.add(ChangeListener.class, changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this.listenerList.remove(ChangeListener.class, changeListener);
    }

    protected void fireChanged() {
        Object[] arrobject = this.listenerList.getListenerList();
        ChangeEvent changeEvent = new ChangeEvent(this);
        for (int i = arrobject.length - 2; i >= 0; i -= 2) {
            if (arrobject[i] != ChangeListener.class) continue;
            ((ChangeListener)arrobject[i + 1]).stateChanged(changeEvent);
        }
    }

    public List<JRibbonComponent> getRibbonComponents(int n) {
        return Collections.unmodifiableList(this.controlPanelGroups.get(n).getRibbonComps());
    }

    public static class ControlPanelGroup {
        public String groupTitle;
        private boolean hasGalleries;
        private int galleryCount;
        private Map<RibbonElementPriority, List<JRibbonGallery>> ribbonGalleries;
        private Map<JRibbonGallery, RibbonElementPriority> ribbonGalleriesPriorities;
        private Map<RibbonElementPriority, List<AbstractCommandButton>> ribbonButtons;
        private Map<AbstractCommandButton, RibbonElementPriority> ribbonButtonsPriorities;
        private List<JRibbonComponent> coreComps;
        private Map<JRibbonComponent, Integer> coreCompRowSpans;

        public ControlPanelGroup(String string) {
            this.groupTitle = string;
            this.ribbonButtons = new HashMap<RibbonElementPriority, List<AbstractCommandButton>>();
            this.ribbonButtonsPriorities = new HashMap<AbstractCommandButton, RibbonElementPriority>();
            this.ribbonGalleries = new HashMap<RibbonElementPriority, List<JRibbonGallery>>();
            this.ribbonGalleriesPriorities = new HashMap<JRibbonGallery, RibbonElementPriority>();
            this.hasGalleries = false;
            this.galleryCount = 0;
            this.coreComps = new ArrayList<JRibbonComponent>();
            this.coreCompRowSpans = new HashMap<JRibbonComponent, Integer>();
        }

        public String getGroupTitle() {
            return this.groupTitle;
        }

        public void setGroupTitle(String string) {
            if (this.groupTitle == null && string != null) {
                throw new IllegalArgumentException("Cannot set a title for an unnamed group");
            }
            if (this.groupTitle != null && string == null) {
                throw new IllegalArgumentException("Cannot remove a title from a named group");
            }
            this.groupTitle = string;
        }

        public boolean isCoreContent() {
            return !this.coreComps.isEmpty();
        }

        public synchronized void addCommandButton(AbstractCommandButton abstractCommandButton, RibbonElementPriority ribbonElementPriority) {
            if (this.groupTitle != null) {
                throw new UnsupportedOperationException("Can't add command buttons to ribbon band group with non-null title");
            }
            if (this.isCoreContent()) {
                throw new UnsupportedOperationException("Ribbon band groups do not support mixing JRibbonComponents and custom Flamingo components");
            }
            if (!this.ribbonButtons.containsKey((Object)ribbonElementPriority)) {
                this.ribbonButtons.put(ribbonElementPriority, new LinkedList());
            }
            List<AbstractCommandButton> list = this.ribbonButtons.get((Object)ribbonElementPriority);
            list.add(abstractCommandButton);
            this.ribbonButtonsPriorities.put(abstractCommandButton, ribbonElementPriority);
            abstractCommandButton.setDisplayState(CommandButtonDisplayState.BIG);
        }

        public synchronized void addRibbonGallery(JRibbonGallery jRibbonGallery, RibbonElementPriority ribbonElementPriority) {
            if (this.groupTitle != null) {
                throw new UnsupportedOperationException("Can't add galleries to ribbon band group with non-null title");
            }
            if (this.isCoreContent()) {
                throw new UnsupportedOperationException("Ribbon band groups do not support mixing JRibbonComponents and custom Flamingo components");
            }
            if (!this.ribbonGalleries.containsKey((Object)ribbonElementPriority)) {
                this.ribbonGalleries.put(ribbonElementPriority, new LinkedList());
            }
            List<JRibbonGallery> list = this.ribbonGalleries.get((Object)ribbonElementPriority);
            list.add(jRibbonGallery);
            this.ribbonGalleriesPriorities.put(jRibbonGallery, ribbonElementPriority);
            jRibbonGallery.setDisplayPriority(RibbonElementPriority.TOP);
            this.hasGalleries = true;
            ++this.galleryCount;
        }

        public synchronized void setPriority(JCommandButton jCommandButton, RibbonElementPriority ribbonElementPriority) {
            RibbonElementPriority ribbonElementPriority2 = this.ribbonButtonsPriorities.get(jCommandButton);
            if (ribbonElementPriority == ribbonElementPriority2) {
                return;
            }
            this.ribbonButtons.get((Object)ribbonElementPriority2).remove(jCommandButton);
            if (!this.ribbonButtons.containsKey((Object)ribbonElementPriority)) {
                this.ribbonButtons.put(ribbonElementPriority, new ArrayList());
            }
            this.ribbonButtons.get((Object)ribbonElementPriority).add(jCommandButton);
        }

        public synchronized void setPriority(JRibbonGallery jRibbonGallery, RibbonElementPriority ribbonElementPriority) {
            RibbonElementPriority ribbonElementPriority2 = this.ribbonGalleriesPriorities.get(jRibbonGallery);
            if (ribbonElementPriority == ribbonElementPriority2) {
                return;
            }
            this.ribbonGalleries.get((Object)ribbonElementPriority2).remove(jRibbonGallery);
            if (!this.ribbonGalleries.containsKey((Object)ribbonElementPriority)) {
                this.ribbonGalleries.put(ribbonElementPriority, new ArrayList());
            }
            this.ribbonGalleries.get((Object)ribbonElementPriority).add(jRibbonGallery);
        }

        public void addRibbonComponent(JRibbonComponent jRibbonComponent, int n) {
            if (!this.ribbonButtonsPriorities.isEmpty() || !this.ribbonGalleries.isEmpty()) {
                throw new UnsupportedOperationException("Ribbon band groups do not support mixing JRibbonComponents and custom Flamingo components");
            }
            jRibbonComponent.setOpaque(false);
            this.coreComps.add(jRibbonComponent);
            this.coreCompRowSpans.put(jRibbonComponent, n);
        }

        public List<AbstractCommandButton> getRibbonButtons(RibbonElementPriority ribbonElementPriority) {
            List<AbstractCommandButton> list = this.ribbonButtons.get((Object)ribbonElementPriority);
            if (list == null) {
                return JBandControlPanel.EMPTY_GALLERY_BUTTONS_LIST;
            }
            return list;
        }

        public List<JRibbonGallery> getRibbonGalleries(RibbonElementPriority ribbonElementPriority) {
            List<JRibbonGallery> list = this.ribbonGalleries.get((Object)ribbonElementPriority);
            if (list == null) {
                return JBandControlPanel.EMPTY_RIBBON_GALLERIES_LIST;
            }
            return list;
        }

        public boolean hasRibbonGalleries() {
            return this.hasGalleries;
        }

        public int getRibbonGalleriesCount() {
            return this.galleryCount;
        }

        public List<JRibbonComponent> getRibbonComps() {
            return this.coreComps;
        }

        public Map<JRibbonComponent, Integer> getRibbonCompsRowSpans() {
            return this.coreCompRowSpans;
        }
    }

}

