/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon.appmenu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.CellRendererPane;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.RolloverActionListener;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.model.PopupButtonModel;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryFooter;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary;
import org.pushingpixels.flamingo.internal.ui.common.popup.BasicPopupPanelUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.CommandButtonLayoutManagerMenuTileLevel1;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuButton;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuPopupPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuPopupPanelSecondary;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public class BasicRibbonApplicationMenuPopupPanelUI
extends BasicPopupPanelUI {
    protected JPanel panelLevel1;
    protected JPanel panelLevel2;
    protected JPanel footerPanel;
    protected static final CommandButtonDisplayState MENU_TILE_LEVEL_1 = new CommandButtonDisplayState("Ribbon application menu tile level 1", 32){

        @Override
        public CommandButtonLayoutManager createLayoutManager(AbstractCommandButton abstractCommandButton) {
            return new CommandButtonLayoutManagerMenuTileLevel1();
        }
    };
    protected JRibbonApplicationMenuPopupPanel applicationMenuPopupPanel;
    protected JPanel mainPanel;

    public static ComponentUI createUI(JComponent jComponent) {
        return new BasicRibbonApplicationMenuPopupPanelUI();
    }

    @Override
    public void installUI(JComponent jComponent) {
        this.applicationMenuPopupPanel = (JRibbonApplicationMenuPopupPanel)jComponent;
        this.popupPanel = (JPopupPanel)jComponent;
        this.applicationMenuPopupPanel.setLayout(new BorderLayout());
        this.installDefaults();
        this.installComponents();
        this.installListeners();
    }

    @Override
    public void uninstallUI(JComponent jComponent) {
        this.uninstallListeners();
        this.uninstallComponents();
        this.uninstallDefaults();
        this.applicationMenuPopupPanel = null;
    }

    @Override
    protected void installDefaults() {
        super.installDefaults();
    }

    @Override
    protected void installComponents() {
        super.installComponents();
        this.mainPanel = this.createMainPanel();
        this.panelLevel1 = new JPanel();
        this.panelLevel1.setLayout(new LayoutManager(){

            @Override
            public void addLayoutComponent(String string, Component component) {
            }

            @Override
            public void removeLayoutComponent(Component component) {
            }

            @Override
            public Dimension preferredLayoutSize(Container container) {
                int n = 0;
                int n2 = 0;
                for (int i = 0; i < container.getComponentCount(); ++i) {
                    Dimension dimension = container.getComponent(i).getPreferredSize();
                    n += dimension.height;
                    n2 = Math.max(n2, dimension.width);
                }
                Insets insets = container.getInsets();
                return new Dimension(n2 + insets.left + insets.right, n + insets.top + insets.bottom);
            }

            @Override
            public Dimension minimumLayoutSize(Container container) {
                return this.preferredLayoutSize(container);
            }

            @Override
            public void layoutContainer(Container container) {
                Insets insets = container.getInsets();
                int n = insets.top;
                for (int i = 0; i < container.getComponentCount(); ++i) {
                    Component component = container.getComponent(i);
                    Dimension dimension = component.getPreferredSize();
                    component.setBounds(insets.left, n, container.getWidth() - insets.left - insets.right, dimension.height);
                    n += dimension.height;
                }
            }
        });
        final RibbonApplicationMenu ribbonApplicationMenu = this.applicationMenuPopupPanel.getRibbonAppMenu();
        if (ribbonApplicationMenu != null) {
            List<List<RibbonApplicationMenuEntryPrimary>> list = ribbonApplicationMenu.getPrimaryEntries();
            int n = list.size();
            for (int i = 0; i < n; ++i) {
                for (final RibbonApplicationMenuEntryPrimary ribbonApplicationMenuEntryPrimary : list.get(i)) {
                    final JCommandMenuButton jCommandMenuButton = new JCommandMenuButton(ribbonApplicationMenuEntryPrimary.getText(), ribbonApplicationMenuEntryPrimary.getIcon());
                    jCommandMenuButton.setCommandButtonKind(ribbonApplicationMenuEntryPrimary.getEntryKind());
                    jCommandMenuButton.addActionListener(ribbonApplicationMenuEntryPrimary.getMainActionListener());
                    jCommandMenuButton.setActionKeyTip(ribbonApplicationMenuEntryPrimary.getActionKeyTip());
                    jCommandMenuButton.setPopupKeyTip(ribbonApplicationMenuEntryPrimary.getPopupKeyTip());
                    if (ribbonApplicationMenuEntryPrimary.getDisabledIcon() != null) {
                        jCommandMenuButton.setDisabledIcon(ribbonApplicationMenuEntryPrimary.getDisabledIcon());
                    }
                    if (ribbonApplicationMenuEntryPrimary.getSecondaryGroupCount() == 0) {
                        jCommandMenuButton.addRolloverActionListener(new RolloverActionListener(){

                            @Override
                            public void actionPerformed(ActionEvent actionEvent) {
                                RibbonApplicationMenuEntryPrimary.PrimaryRolloverCallback primaryRolloverCallback = ribbonApplicationMenuEntryPrimary.getRolloverCallback();
                                if (primaryRolloverCallback != null) {
                                    primaryRolloverCallback.menuEntryActivated(BasicRibbonApplicationMenuPopupPanelUI.this.panelLevel2);
                                } else {
                                    RibbonApplicationMenuEntryPrimary.PrimaryRolloverCallback primaryRolloverCallback2 = ribbonApplicationMenu.getDefaultCallback();
                                    if (primaryRolloverCallback2 != null) {
                                        primaryRolloverCallback2.menuEntryActivated(BasicRibbonApplicationMenuPopupPanelUI.this.panelLevel2);
                                    } else {
                                        BasicRibbonApplicationMenuPopupPanelUI.this.panelLevel2.removeAll();
                                        BasicRibbonApplicationMenuPopupPanelUI.this.panelLevel2.revalidate();
                                        BasicRibbonApplicationMenuPopupPanelUI.this.panelLevel2.repaint();
                                    }
                                }
                                BasicRibbonApplicationMenuPopupPanelUI.this.panelLevel2.applyComponentOrientation(BasicRibbonApplicationMenuPopupPanelUI.this.applicationMenuPopupPanel.getComponentOrientation());
                            }
                        });
                    } else {
                        final RibbonApplicationMenuEntryPrimary.PrimaryRolloverCallback primaryRolloverCallback = new RibbonApplicationMenuEntryPrimary.PrimaryRolloverCallback(){

                            @Override
                            public void menuEntryActivated(JPanel jPanel) {
                                jPanel.removeAll();
                                jPanel.setLayout(new BorderLayout());
                                JRibbonApplicationMenuPopupPanelSecondary jRibbonApplicationMenuPopupPanelSecondary = new JRibbonApplicationMenuPopupPanelSecondary(ribbonApplicationMenuEntryPrimary){

                                    @Override
                                    public void removeNotify() {
                                        super.removeNotify();
                                        jCommandMenuButton.getPopupModel().setPopupShowing(false);
                                    }
                                };
                                jRibbonApplicationMenuPopupPanelSecondary.applyComponentOrientation(BasicRibbonApplicationMenuPopupPanelUI.this.applicationMenuPopupPanel.getComponentOrientation());
                                jPanel.add((Component)jRibbonApplicationMenuPopupPanelSecondary, "Center");
                            }

                        };
                        jCommandMenuButton.addRolloverActionListener(new RolloverActionListener(){

                            @Override
                            public void actionPerformed(ActionEvent actionEvent) {
                                primaryRolloverCallback.menuEntryActivated(BasicRibbonApplicationMenuPopupPanelUI.this.panelLevel2);
                                jCommandMenuButton.getPopupModel().setPopupShowing(true);
                            }
                        });
                    }
                    jCommandMenuButton.setDisplayState(MENU_TILE_LEVEL_1);
                    jCommandMenuButton.setHorizontalAlignment(10);
                    jCommandMenuButton.setPopupOrientationKind(JCommandButton.CommandButtonPopupOrientationKind.SIDEWARD);
                    jCommandMenuButton.setEnabled(ribbonApplicationMenuEntryPrimary.isEnabled());
                    this.panelLevel1.add(jCommandMenuButton);
                }
                if (i >= n - 1) continue;
                this.panelLevel1.add(new JPopupMenu.Separator());
            }
        }
        this.mainPanel.add((Component)this.panelLevel1, "Before");
        this.panelLevel2 = new JPanel();
        this.panelLevel2.setBorder(new Border(){

            @Override
            public Insets getBorderInsets(Component component) {
                boolean bl = component.getComponentOrientation().isLeftToRight();
                return new Insets(0, bl ? 1 : 0, 0, bl ? 0 : 1);
            }

            @Override
            public boolean isBorderOpaque() {
                return true;
            }

            @Override
            public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
                graphics.setColor(FlamingoUtilities.getColor(Color.gray, "Label.disabledForeground"));
                boolean bl = component.getComponentOrientation().isLeftToRight();
                int n5 = bl ? n : n + n3 - 1;
                graphics.drawLine(n5, n2, n5, n2 + n4);
            }
        });
        this.panelLevel2.setPreferredSize(new Dimension(30 * FlamingoUtilities.getFont(this.panelLevel1, "Ribbon.font", "Button.font", "Panel.font").getSize() - 30, 10));
        this.mainPanel.add((Component)this.panelLevel2, "Center");
        if (ribbonApplicationMenu != null && ribbonApplicationMenu.getDefaultCallback() != null) {
            ribbonApplicationMenu.getDefaultCallback().menuEntryActivated(this.panelLevel2);
        }
        this.applicationMenuPopupPanel.add((Component)this.mainPanel, "Center");
        this.footerPanel = new JPanel(new FlowLayout(4)){

            @Override
            protected void paintComponent(Graphics graphics) {
                FlamingoUtilities.renderSurface(graphics, BasicRibbonApplicationMenuPopupPanelUI.this.footerPanel, new Rectangle(0, 0, BasicRibbonApplicationMenuPopupPanelUI.this.footerPanel.getWidth(), BasicRibbonApplicationMenuPopupPanelUI.this.footerPanel.getHeight()), false, false, false);
            }
        };
        if (ribbonApplicationMenu != null) {
            for (RibbonApplicationMenuEntryFooter ribbonApplicationMenuEntryFooter : ribbonApplicationMenu.getFooterEntries()) {
                JCommandButton jCommandButton = new JCommandButton(ribbonApplicationMenuEntryFooter.getText(), ribbonApplicationMenuEntryFooter.getIcon());
                if (ribbonApplicationMenuEntryFooter.getDisabledIcon() != null) {
                    jCommandButton.setDisabledIcon(ribbonApplicationMenuEntryFooter.getDisabledIcon());
                }
                jCommandButton.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_ONLY);
                jCommandButton.addActionListener(ribbonApplicationMenuEntryFooter.getMainActionListener());
                jCommandButton.setDisplayState(CommandButtonDisplayState.MEDIUM);
                jCommandButton.setFlat(false);
                jCommandButton.setEnabled(ribbonApplicationMenuEntryFooter.isEnabled());
                this.footerPanel.add(jCommandButton);
            }
        }
        this.applicationMenuPopupPanel.add((Component)this.footerPanel, "South");
        this.applicationMenuPopupPanel.setBorder(new Border(){

            @Override
            public Insets getBorderInsets(Component component) {
                return new Insets(20, 2, 2, 2);
            }

            @Override
            public boolean isBorderOpaque() {
                return true;
            }

            @Override
            public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
                graphics.setColor(FlamingoUtilities.getColor(Color.gray, "Label.disabledForeground"));
                graphics.drawRect(n, n2, n3 - 1, n4 - 1);
                graphics.setColor(FlamingoUtilities.getColor(Color.gray, "Label.disabledForeground").brighter().brighter());
                graphics.drawRect(n + 1, n2 + 1, n3 - 3, n4 - 3);
                FlamingoUtilities.renderSurface(graphics, BasicRibbonApplicationMenuPopupPanelUI.this.applicationMenuPopupPanel, new Rectangle(n + 2, n2 + 2, n3 - 4, 24), false, false, false);
                JRibbonApplicationMenuButton jRibbonApplicationMenuButton = BasicRibbonApplicationMenuPopupPanelUI.this.applicationMenuPopupPanel.getAppMenuButton();
                JRibbonApplicationMenuButton jRibbonApplicationMenuButton2 = new JRibbonApplicationMenuButton(BasicRibbonApplicationMenuPopupPanelUI.this.applicationMenuPopupPanel.getAppMenuButton().getRibbon());
                jRibbonApplicationMenuButton2.setPopupKeyTip(jRibbonApplicationMenuButton.getPopupKeyTip());
                jRibbonApplicationMenuButton2.setIcon(jRibbonApplicationMenuButton.getIcon());
                jRibbonApplicationMenuButton2.getPopupModel().setRollover(false);
                jRibbonApplicationMenuButton2.getPopupModel().setPressed(true);
                jRibbonApplicationMenuButton2.getPopupModel().setArmed(true);
                jRibbonApplicationMenuButton2.getPopupModel().setPopupShowing(true);
                CellRendererPane cellRendererPane = new CellRendererPane();
                Point point = jRibbonApplicationMenuButton.getLocationOnScreen();
                Point point2 = component.getLocationOnScreen();
                cellRendererPane.setBounds(point2.x - point.x, point2.y - point.y, jRibbonApplicationMenuButton.getWidth(), jRibbonApplicationMenuButton.getHeight());
                cellRendererPane.paintComponent(graphics, jRibbonApplicationMenuButton2, (Container)component, - point2.x + point.x, - point2.y + point.y, jRibbonApplicationMenuButton.getWidth(), jRibbonApplicationMenuButton.getHeight(), true);
            }
        });
    }

    protected JPanel createMainPanel() {
        JPanel jPanel = new JPanel(new BorderLayout());
        jPanel.setBorder(new Border(){

            @Override
            public Insets getBorderInsets(Component component) {
                return new Insets(2, 2, 2, 2);
            }

            @Override
            public boolean isBorderOpaque() {
                return true;
            }

            @Override
            public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
                graphics.setColor(FlamingoUtilities.getColor(Color.gray, "Label.disabledForeground").brighter().brighter());
                graphics.drawRect(n, n2, n3 - 1, n4 - 1);
                graphics.setColor(FlamingoUtilities.getColor(Color.gray, "Label.disabledForeground"));
                graphics.drawRect(n + 1, n2 + 1, n3 - 3, n4 - 3);
            }
        });
        return jPanel;
    }

    @Override
    protected void installListeners() {
        super.installListeners();
    }

    @Override
    protected void uninstallDefaults() {
        super.uninstallDefaults();
    }

    @Override
    protected void uninstallComponents() {
        super.uninstallComponents();
    }

    @Override
    protected void uninstallListeners() {
        super.uninstallListeners();
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        graphics2D.dispose();
    }

    public JPanel getPanelLevel1() {
        return this.panelLevel1;
    }

    public JPanel getPanelLevel2() {
        return this.panelLevel2;
    }

}

