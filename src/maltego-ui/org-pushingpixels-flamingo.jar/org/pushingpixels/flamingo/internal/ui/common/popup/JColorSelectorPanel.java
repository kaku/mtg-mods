/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common.popup;

import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.internal.ui.common.popup.BasicColorSelectorPanelUI;
import org.pushingpixels.flamingo.internal.ui.common.popup.ColorSelectorPanelUI;

public class JColorSelectorPanel
extends JPanel {
    private String caption;
    private JPanel colorSelectionContainer;
    private boolean isLastPanel;
    public static final String uiClassID = "ColorSelectorPanelUI";

    public JColorSelectorPanel(String string, JPanel jPanel) {
        this.caption = string;
        this.colorSelectionContainer = jPanel;
        this.updateUI();
    }

    @Override
    public void updateUI() {
        if (UIManager.get(this.getUIClassID()) != null) {
            this.setUI((ColorSelectorPanelUI)UIManager.getUI(this));
        } else {
            this.setUI(BasicColorSelectorPanelUI.createUI(this));
        }
    }

    @Override
    public String getUIClassID() {
        return "ColorSelectorPanelUI";
    }

    public String getCaption() {
        return this.caption;
    }

    public JPanel getColorSelectionContainer() {
        return this.colorSelectionContainer;
    }

    public void setLastPanel(boolean bl) {
        this.isLastPanel = bl;
    }

    public boolean isLastPanel() {
        return this.isLastPanel;
    }
}

