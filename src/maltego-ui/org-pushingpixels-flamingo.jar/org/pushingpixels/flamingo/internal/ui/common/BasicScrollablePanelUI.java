/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common;

import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JScrollablePanel;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.internal.ui.common.ScrollablePanelUI;
import org.pushingpixels.flamingo.internal.utils.DoubleArrowResizableIcon;

public class BasicScrollablePanelUI
extends ScrollablePanelUI {
    protected JScrollablePanel scrollablePanel;
    private JPanel viewport;
    private JCommandButton leadingScroller;
    private JCommandButton trailingScroller;
    private int viewOffset;
    private MouseWheelListener mouseWheelListener;
    private PropertyChangeListener propertyChangeListener;
    private ComponentListener componentListener;

    public static ComponentUI createUI(JComponent jComponent) {
        return new BasicScrollablePanelUI();
    }

    @Override
    public void installUI(JComponent jComponent) {
        this.scrollablePanel = (JScrollablePanel)jComponent;
        super.installUI(this.scrollablePanel);
        this.installDefaults();
        this.installComponents();
        this.installListeners();
    }

    protected void installListeners() {
        this.mouseWheelListener = new MouseWheelListener(){

            @Override
            public void mouseWheelMoved(MouseWheelEvent mouseWheelEvent) {
                if (BasicScrollablePanelUI.this.scrollablePanel.getScrollType() != JScrollablePanel.ScrollType.VERTICALLY) {
                    return;
                }
                int n = 8 * mouseWheelEvent.getScrollAmount() * mouseWheelEvent.getWheelRotation();
                BasicScrollablePanelUI.this.viewOffset = BasicScrollablePanelUI.this.viewOffset + n;
                BasicScrollablePanelUI.this.syncScrolling();
            }
        };
        this.scrollablePanel.addMouseWheelListener(this.mouseWheelListener);
        this.propertyChangeListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("scrollOnRollover".equals(propertyChangeEvent.getPropertyName())) {
                    boolean bl = (Boolean)propertyChangeEvent.getNewValue();
                    BasicScrollablePanelUI.this.leadingScroller.setFireActionOnRollover(bl);
                    BasicScrollablePanelUI.this.trailingScroller.setFireActionOnRollover(bl);
                }
            }
        };
        this.scrollablePanel.addPropertyChangeListener(this.propertyChangeListener);
        if (this.scrollablePanel.getView() != null) {
            this.componentListener = new ComponentAdapter(){

                @Override
                public void componentResized(ComponentEvent componentEvent) {
                    BasicScrollablePanelUI.this.scrollablePanel.doLayout();
                }
            };
            this.scrollablePanel.getView().addComponentListener(this.componentListener);
        }
    }

    protected void installComponents() {
        this.viewport = new JPanel(new LayoutManager(){

            @Override
            public void addLayoutComponent(String string, Component component) {
            }

            @Override
            public void removeLayoutComponent(Component component) {
            }

            @Override
            public Dimension preferredLayoutSize(Container container) {
                return new Dimension(10, 10);
            }

            @Override
            public Dimension minimumLayoutSize(Container container) {
                return this.preferredLayoutSize(container);
            }

            @Override
            public void layoutContainer(Container container) {
                Object t = BasicScrollablePanelUI.this.scrollablePanel.getView();
                if (BasicScrollablePanelUI.this.scrollablePanel.getScrollType() == JScrollablePanel.ScrollType.HORIZONTALLY) {
                    int n = t.getPreferredSize().width;
                    int n2 = container.getWidth();
                    int n3 = - BasicScrollablePanelUI.this.viewOffset;
                    t.setBounds(n3, 0, Math.max(n, n2), container.getHeight());
                } else {
                    int n = t.getPreferredSize().height;
                    int n4 = container.getHeight();
                    int n5 = - BasicScrollablePanelUI.this.viewOffset;
                    t.setBounds(0, n5, container.getWidth(), Math.max(n, n4));
                }
            }
        }){

            @Override
            public void updateUI() {
                super.updateUI();
                this.setOpaque(false);
            }
        };
        Object t = this.scrollablePanel.getView();
        if (t != null) {
            this.viewport.add((Component)t);
        }
        this.scrollablePanel.add(this.viewport);
        this.leadingScroller = this.createLeadingScroller();
        this.configureLeftScrollerButtonAction();
        this.scrollablePanel.add(this.leadingScroller);
        this.trailingScroller = this.createTrailingScroller();
        this.configureRightScrollerButtonAction();
        this.scrollablePanel.add(this.trailingScroller);
    }

    protected void installDefaults() {
        this.scrollablePanel.setLayout(new ScrollablePanelLayout());
    }

    @Override
    public void uninstallUI(JComponent jComponent) {
        this.uninstallListeners();
        this.uninstallComponents();
        this.uninstallDefaults();
        super.uninstallUI(this.scrollablePanel);
    }

    protected void uninstallDefaults() {
    }

    protected void uninstallComponents() {
        this.scrollablePanel.remove(this.viewport);
        this.scrollablePanel.remove(this.leadingScroller);
        this.scrollablePanel.remove(this.trailingScroller);
    }

    protected void uninstallListeners() {
        this.scrollablePanel.removePropertyChangeListener(this.propertyChangeListener);
        this.propertyChangeListener = null;
        this.scrollablePanel.removeMouseWheelListener(this.mouseWheelListener);
        this.mouseWheelListener = null;
        if (this.scrollablePanel.getView() != null) {
            this.scrollablePanel.getView().removeComponentListener(this.componentListener);
            this.componentListener = null;
        }
    }

    protected JCommandButton createLeadingScroller() {
        JCommandButton jCommandButton = new JCommandButton(null, new DoubleArrowResizableIcon(new Dimension(9, 9), this.scrollablePanel.getScrollType() == JScrollablePanel.ScrollType.HORIZONTALLY ? 7 : 1));
        jCommandButton.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        jCommandButton.setFocusable(false);
        jCommandButton.setCursor(Cursor.getPredefinedCursor(12));
        jCommandButton.putClientProperty("flamingo.internal.commandButton.ui.emulateSquare", Boolean.TRUE);
        jCommandButton.putClientProperty("flamingo.internal.commandButton.ui.dontDisposePopups", Boolean.TRUE);
        return jCommandButton;
    }

    protected JCommandButton createTrailingScroller() {
        JCommandButton jCommandButton = new JCommandButton(null, new DoubleArrowResizableIcon(new Dimension(9, 9), this.scrollablePanel.getScrollType() == JScrollablePanel.ScrollType.HORIZONTALLY ? 3 : 5));
        jCommandButton.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        jCommandButton.setFocusable(false);
        jCommandButton.setCursor(Cursor.getPredefinedCursor(12));
        jCommandButton.putClientProperty("flamingo.internal.commandButton.ui.emulateSquare", Boolean.TRUE);
        jCommandButton.putClientProperty("flamingo.internal.commandButton.ui.dontDisposePopups", Boolean.TRUE);
        return jCommandButton;
    }

    private void syncScrolling() {
        this.scrollablePanel.doLayout();
    }

    public void removeScrollers() {
        if (this.leadingScroller.getParent() == this.scrollablePanel) {
            this.scrollablePanel.remove(this.leadingScroller);
            this.scrollablePanel.remove(this.trailingScroller);
            this.syncScrolling();
            this.scrollablePanel.revalidate();
            this.scrollablePanel.repaint();
        }
    }

    private void addScrollers() {
        this.scrollablePanel.add(this.leadingScroller);
        this.scrollablePanel.add(this.trailingScroller);
        this.scrollablePanel.revalidate();
        Object t = this.scrollablePanel.getView();
        t.setPreferredSize(t.getMinimumSize());
        t.setSize(t.getMinimumSize());
        this.scrollablePanel.doLayout();
        this.scrollablePanel.repaint();
    }

    protected void configureLeftScrollerButtonAction() {
        this.leadingScroller.setAutoRepeatAction(true);
        this.leadingScroller.setAutoRepeatActionIntervals(200, 50);
        this.leadingScroller.setFireActionOnRollover(this.scrollablePanel.isScrollOnRollover());
        this.leadingScroller.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                BasicScrollablePanelUI.this.viewOffset = BasicScrollablePanelUI.this.viewOffset - 12;
                BasicScrollablePanelUI.this.syncScrolling();
            }
        });
    }

    protected void configureRightScrollerButtonAction() {
        this.trailingScroller.setAutoRepeatAction(true);
        this.trailingScroller.setAutoRepeatActionIntervals(200, 50);
        this.trailingScroller.setFireActionOnRollover(this.scrollablePanel.isScrollOnRollover());
        this.trailingScroller.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                BasicScrollablePanelUI.this.viewOffset = BasicScrollablePanelUI.this.viewOffset + 12;
                BasicScrollablePanelUI.this.syncScrolling();
            }
        });
    }

    @Override
    public void scrollToIfNecessary(int n, int n2) {
        if (this.scrollablePanel.getScrollType() == JScrollablePanel.ScrollType.HORIZONTALLY) {
            if (this.scrollablePanel.getComponentOrientation().isLeftToRight()) {
                this.revealRightEdge(n, n2);
                this.revealLeftEdge(n);
            } else {
                this.revealLeftEdge(n);
                this.revealRightEdge(n, n2);
            }
        } else {
            this.revealBottomEdge(n, n2);
            this.revealTopEdge(n);
        }
    }

    private void revealLeftEdge(int n) {
        if (n < this.viewOffset) {
            this.viewOffset = n - 5;
            this.syncScrolling();
        }
    }

    private void revealRightEdge(int n, int n2) {
        if (n + n2 > this.viewOffset + this.viewport.getWidth()) {
            this.viewOffset = n + n2 - this.viewport.getWidth() + 5;
            this.syncScrolling();
        }
    }

    private void revealTopEdge(int n) {
        if (n < this.viewOffset) {
            this.viewOffset = n - 5;
            this.syncScrolling();
        }
    }

    private void revealBottomEdge(int n, int n2) {
        if (n + n2 > this.viewOffset + this.viewport.getHeight()) {
            this.viewOffset = n + n2 - this.viewport.getHeight() + 5;
            this.syncScrolling();
        }
    }

    @Override
    public boolean isShowingScrollButtons() {
        return this.leadingScroller.isVisible();
    }

    protected class ScrollablePanelLayout
    implements LayoutManager {
        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            if (BasicScrollablePanelUI.this.scrollablePanel.getScrollType() == JScrollablePanel.ScrollType.HORIZONTALLY) {
                return new Dimension(container.getWidth(), 21);
            }
            return new Dimension(21, container.getHeight());
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            if (BasicScrollablePanelUI.this.scrollablePanel.getScrollType() == JScrollablePanel.ScrollType.HORIZONTALLY) {
                return new Dimension(10, 21);
            }
            return new Dimension(21, 10);
        }

        @Override
        public void layoutContainer(Container container) {
            int n = container.getWidth();
            int n2 = container.getHeight();
            Insets insets = container.getInsets();
            Object t = BasicScrollablePanelUI.this.scrollablePanel.getView();
            Dimension dimension = t.getPreferredSize();
            if (BasicScrollablePanelUI.this.scrollablePanel.getScrollType() == JScrollablePanel.ScrollType.HORIZONTALLY) {
                int n3;
                boolean bl = dimension.width > n;
                BasicScrollablePanelUI.this.leadingScroller.setVisible(bl);
                BasicScrollablePanelUI.this.trailingScroller.setVisible(bl);
                int n4 = bl ? n - insets.left - insets.right - BasicScrollablePanelUI.access$200((BasicScrollablePanelUI)BasicScrollablePanelUI.this).getPreferredSize().width - BasicScrollablePanelUI.access$300((BasicScrollablePanelUI)BasicScrollablePanelUI.this).getPreferredSize().width - 4 : n - insets.left - insets.right;
                int n5 = insets.left;
                if (bl) {
                    n3 = BasicScrollablePanelUI.access$200((BasicScrollablePanelUI)BasicScrollablePanelUI.this).getPreferredSize().width;
                    BasicScrollablePanelUI.this.leadingScroller.setBounds(n5, insets.top, n3, n2 - insets.top - insets.bottom);
                    n5 += n3 + 2;
                }
                BasicScrollablePanelUI.this.viewport.setBounds(n5, insets.top, n4, n2 - insets.top - insets.bottom);
                n3 = t.getPreferredSize().width;
                if (BasicScrollablePanelUI.this.viewOffset < 0) {
                    BasicScrollablePanelUI.this.viewOffset = 0;
                }
                if (n3 > 0 && BasicScrollablePanelUI.this.viewOffset + n4 > n3) {
                    BasicScrollablePanelUI.this.viewOffset = Math.max(0, n3 - n4);
                }
                BasicScrollablePanelUI.this.viewport.doLayout();
                n5 += n4 + 2;
                if (bl) {
                    int n6 = BasicScrollablePanelUI.access$300((BasicScrollablePanelUI)BasicScrollablePanelUI.this).getPreferredSize().width;
                    BasicScrollablePanelUI.this.trailingScroller.setBounds(n5, insets.top, n6, n2 - insets.top - insets.bottom);
                }
            } else {
                int n7;
                boolean bl = dimension.height > n2;
                BasicScrollablePanelUI.this.leadingScroller.setVisible(bl);
                BasicScrollablePanelUI.this.trailingScroller.setVisible(bl);
                int n8 = bl ? n2 - insets.top - insets.bottom - BasicScrollablePanelUI.access$200((BasicScrollablePanelUI)BasicScrollablePanelUI.this).getPreferredSize().height - BasicScrollablePanelUI.access$300((BasicScrollablePanelUI)BasicScrollablePanelUI.this).getPreferredSize().height - 4 : n2 - insets.top - insets.bottom;
                int n9 = insets.top;
                if (bl) {
                    n7 = BasicScrollablePanelUI.access$200((BasicScrollablePanelUI)BasicScrollablePanelUI.this).getPreferredSize().height;
                    BasicScrollablePanelUI.this.leadingScroller.setBounds(insets.left, n9, n - insets.left - insets.right, n7);
                    n9 += n7 + 2;
                }
                BasicScrollablePanelUI.this.viewport.setBounds(insets.left, n9, n - insets.left - insets.right, n8);
                n7 = t.getPreferredSize().height;
                if (BasicScrollablePanelUI.this.viewOffset < 0) {
                    BasicScrollablePanelUI.this.viewOffset = 0;
                }
                if (n7 > 0 && BasicScrollablePanelUI.this.viewOffset + n8 > n7) {
                    BasicScrollablePanelUI.this.viewOffset = Math.max(0, n7 - n8);
                }
                BasicScrollablePanelUI.this.viewport.doLayout();
                n9 += n8 + 2;
                if (bl) {
                    int n10 = BasicScrollablePanelUI.access$300((BasicScrollablePanelUI)BasicScrollablePanelUI.this).getPreferredSize().height;
                    BasicScrollablePanelUI.this.trailingScroller.setBounds(insets.left, n9, n - insets.left - insets.right, n10);
                }
            }
            if (BasicScrollablePanelUI.this.scrollablePanel.getScrollType() == JScrollablePanel.ScrollType.HORIZONTALLY) {
                BasicScrollablePanelUI.this.trailingScroller.setEnabled(BasicScrollablePanelUI.this.viewOffset + BasicScrollablePanelUI.this.viewport.getWidth() < t.getWidth());
            } else {
                BasicScrollablePanelUI.this.trailingScroller.setEnabled(BasicScrollablePanelUI.this.viewOffset + BasicScrollablePanelUI.this.viewport.getHeight() < t.getHeight());
            }
            BasicScrollablePanelUI.this.leadingScroller.setEnabled(BasicScrollablePanelUI.this.viewOffset > 0);
        }
    }

}

