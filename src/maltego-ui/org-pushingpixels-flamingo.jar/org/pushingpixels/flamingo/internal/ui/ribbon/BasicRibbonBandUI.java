/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.trident.Timeline
 *  org.pushingpixels.trident.callback.TimelineCallback
 *  org.pushingpixels.trident.swing.SwingRepaintCallback
 */
package org.pushingpixels.flamingo.internal.ui.ribbon;

import java.awt.AWTEvent;
import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Composite;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.UIResource;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelManager;
import org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;
import org.pushingpixels.flamingo.api.ribbon.resize.IconRibbonBandResizePolicy;
import org.pushingpixels.flamingo.api.ribbon.resize.RibbonBandResizePolicy;
import org.pushingpixels.flamingo.internal.ui.ribbon.BandControlPanelUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.RibbonBandUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.RibbonUI;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;
import org.pushingpixels.flamingo.internal.utils.RenderingUtils;
import org.pushingpixels.trident.Timeline;
import org.pushingpixels.trident.callback.TimelineCallback;
import org.pushingpixels.trident.swing.SwingRepaintCallback;

public class BasicRibbonBandUI
extends RibbonBandUI {
    protected AbstractRibbonBand ribbonBand;
    protected JCommandButton collapsedButton;
    protected AbstractCommandButton expandButton;
    protected float rolloverAmount;
    protected Timeline rolloverTimeline;
    protected MouseListener mouseListener;
    protected PropertyChangeListener propertyChangeListener;
    protected ActionListener expandButtonActionListener;

    public static ComponentUI createUI(JComponent jComponent) {
        return new BasicRibbonBandUI();
    }

    @Override
    public void installUI(JComponent jComponent) {
        this.ribbonBand = (AbstractRibbonBand)jComponent;
        this.rolloverTimeline = new Timeline((Object)this);
        this.rolloverTimeline.addPropertyToInterpolate("rolloverAmount", (Object)Float.valueOf(0.0f), (Object)Float.valueOf(1.0f));
        this.rolloverTimeline.addCallback((TimelineCallback)new SwingRepaintCallback((Component)this.ribbonBand));
        this.rolloverTimeline.setDuration(250);
        this.installDefaults();
        this.installComponents();
        this.installListeners();
        jComponent.setLayout(this.createLayoutManager());
        AWTRibbonEventListener.install();
    }

    @Override
    public void uninstallUI(JComponent jComponent) {
        jComponent.setLayout(null);
        this.uninstallListeners();
        this.uninstallDefaults();
        this.uninstallComponents();
        if (!AWTRibbonEventListener.uninstall()) {
            // empty if block
        }
    }

    protected void installDefaults() {
        Border border;
        Color color = this.ribbonBand.getBackground();
        if (color == null || color instanceof UIResource) {
            this.ribbonBand.setBackground(FlamingoUtilities.getColor(Color.lightGray, "RibbonBand.background", "Panel.background"));
        }
        if ((border = this.ribbonBand.getBorder()) == null || border instanceof UIResource) {
            Border border2 = UIManager.getBorder("RibbonBand.border");
            if (border2 == null) {
                border2 = new BorderUIResource(new RoundBorder(FlamingoUtilities.getBorderColor(), new Insets(2, 2, 2, 2)));
            }
            this.ribbonBand.setBorder(border2);
        }
    }

    protected void installComponents() {
        this.collapsedButton = new JCommandButton(this.ribbonBand.getTitle(), this.ribbonBand.getIcon());
        this.collapsedButton.setDisplayState(CommandButtonDisplayState.BIG);
        this.collapsedButton.setCommandButtonKind(JCommandButton.CommandButtonKind.POPUP_ONLY);
        this.collapsedButton.setPopupKeyTip(this.ribbonBand.getCollapsedStateKeyTip());
        this.ribbonBand.add(this.collapsedButton);
        if (this.ribbonBand.getExpandActionListener() != null) {
            this.expandButton = this.createExpandButton();
            this.ribbonBand.add(this.expandButton);
        }
    }

    protected JCommandButton createExpandButton() {
        ResizableIcon resizableIcon = FlamingoUtilities.getRibbonBandExpandIcon(this.ribbonBand);
        JCommandButton jCommandButton = new JCommandButton(null, resizableIcon);
        jCommandButton.setFlat(true);
        jCommandButton.putClientProperty("flamingo.internal.commandButton.ui.emulateSquare", Boolean.TRUE);
        jCommandButton.setBorder(new EmptyBorder(3, 2, 3, 2));
        jCommandButton.setActionKeyTip(this.ribbonBand.getExpandButtonKeyTip());
        jCommandButton.setActionRichTooltip(this.ribbonBand.getExpandButtonRichTooltip());
        return jCommandButton;
    }

    protected void syncExpandButtonIcon() {
        this.expandButton.setIcon(FlamingoUtilities.getRibbonBandExpandIcon(this.ribbonBand));
    }

    protected void installListeners() {
        this.mouseListener = new MouseAdapter(){};
        this.ribbonBand.addMouseListener(this.mouseListener);
        this.configureExpandButton();
        this.propertyChangeListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("title".equals(propertyChangeEvent.getPropertyName())) {
                    BasicRibbonBandUI.this.ribbonBand.repaint();
                }
                if ("expandButtonKeyTip".equals(propertyChangeEvent.getPropertyName()) && BasicRibbonBandUI.this.expandButton != null) {
                    BasicRibbonBandUI.this.expandButton.setActionKeyTip((String)propertyChangeEvent.getNewValue());
                }
                if ("expandButtonRichTooltip".equals(propertyChangeEvent.getPropertyName()) && BasicRibbonBandUI.this.expandButton != null) {
                    BasicRibbonBandUI.this.expandButton.setActionRichTooltip((RichTooltip)propertyChangeEvent.getNewValue());
                }
                if ("collapsedStateKeyTip".equals(propertyChangeEvent.getPropertyName()) && BasicRibbonBandUI.this.collapsedButton != null) {
                    BasicRibbonBandUI.this.collapsedButton.setPopupKeyTip((String)propertyChangeEvent.getNewValue());
                }
                if ("expandActionListener".equals(propertyChangeEvent.getPropertyName())) {
                    ActionListener actionListener = (ActionListener)propertyChangeEvent.getOldValue();
                    ActionListener actionListener2 = (ActionListener)propertyChangeEvent.getNewValue();
                    if (actionListener != null && actionListener2 == null) {
                        BasicRibbonBandUI.this.unconfigureExpandButton();
                        BasicRibbonBandUI.this.ribbonBand.remove(BasicRibbonBandUI.this.expandButton);
                        BasicRibbonBandUI.this.expandButton = null;
                        BasicRibbonBandUI.this.ribbonBand.revalidate();
                    }
                    if (actionListener == null && actionListener2 != null) {
                        BasicRibbonBandUI.this.expandButton = BasicRibbonBandUI.this.createExpandButton();
                        BasicRibbonBandUI.this.configureExpandButton();
                        BasicRibbonBandUI.this.ribbonBand.add(BasicRibbonBandUI.this.expandButton);
                        BasicRibbonBandUI.this.ribbonBand.revalidate();
                    }
                    if (actionListener != null && actionListener2 != null) {
                        BasicRibbonBandUI.this.expandButton.removeActionListener(actionListener);
                        BasicRibbonBandUI.this.expandButton.addActionListener(actionListener2);
                    }
                }
                if ("componentOrientation".equals(propertyChangeEvent.getPropertyName()) && BasicRibbonBandUI.this.expandButton != null) {
                    BasicRibbonBandUI.this.syncExpandButtonIcon();
                }
            }
        };
        this.ribbonBand.addPropertyChangeListener(this.propertyChangeListener);
    }

    protected void configureExpandButton() {
        if (this.expandButton != null) {
            this.expandButton.addActionListener(this.ribbonBand.getExpandActionListener());
            this.expandButtonActionListener = new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            BasicRibbonBandUI.this.trackMouseCrossing(false);
                        }
                    });
                }

            };
            this.expandButton.addActionListener(this.expandButtonActionListener);
        }
    }

    protected void uninstallDefaults() {
        LookAndFeel.uninstallBorder(this.ribbonBand);
    }

    protected void uninstallComponents() {
        if (this.collapsedButton.isVisible()) {
            CollapsedButtonPopupPanel collapsedButtonPopupPanel;
            CollapsedButtonPopupPanel collapsedButtonPopupPanel2 = collapsedButtonPopupPanel = this.collapsedButton.getPopupCallback() == null ? null : (CollapsedButtonPopupPanel)this.collapsedButton.getPopupCallback().getPopupPanel(this.collapsedButton);
            if (collapsedButtonPopupPanel != null) {
                AbstractRibbonBand abstractRibbonBand = (AbstractRibbonBand)collapsedButtonPopupPanel.removeComponent();
                this.ribbonBand.setControlPanel(abstractRibbonBand.getControlPanel());
                this.ribbonBand.setPopupRibbonBand(null);
                this.collapsedButton.setPopupCallback(null);
            }
        }
        this.ribbonBand.remove(this.collapsedButton);
        this.collapsedButton = null;
        if (this.expandButton != null) {
            this.ribbonBand.remove(this.expandButton);
        }
        this.expandButton = null;
        this.ribbonBand = null;
    }

    protected void uninstallListeners() {
        this.ribbonBand.removePropertyChangeListener(this.propertyChangeListener);
        this.propertyChangeListener = null;
        this.ribbonBand.removeMouseListener(this.mouseListener);
        this.mouseListener = null;
        this.unconfigureExpandButton();
    }

    protected void unconfigureExpandButton() {
        if (this.expandButton != null) {
            this.expandButton.removeActionListener(this.expandButtonActionListener);
            this.expandButtonActionListener = null;
            this.expandButton.removeActionListener(this.ribbonBand.getExpandActionListener());
        }
    }

    protected LayoutManager createLayoutManager() {
        return new RibbonBandLayout();
    }

    @Override
    public void update(Graphics graphics, JComponent jComponent) {
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        RenderingUtils.installDesktopHints(graphics2D);
        RenderingUtils.setupTextAntialiasing(graphics2D, jComponent);
        super.update(graphics2D, jComponent);
        graphics2D.dispose();
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        Insets insets = this.ribbonBand.getInsets();
        this.paintBandBackground(graphics2D, new Rectangle(0, 0, jComponent.getWidth(), jComponent.getHeight()));
        if (!(this.ribbonBand.getCurrentResizePolicy() instanceof IconRibbonBandResizePolicy)) {
            String string = this.ribbonBand.getTitle();
            int n = this.getBandTitleHeight();
            int n2 = jComponent.getHeight() - n;
            this.paintBandTitleBackground(graphics2D, new Rectangle(0, n2, jComponent.getWidth(), n), string);
            boolean bl = this.ribbonBand.getComponentOrientation().isLeftToRight();
            int n3 = jComponent.getWidth() - 2 * insets.left - 2 * insets.right;
            int n4 = 2 * insets.left;
            if (this.expandButton != null) {
                if (bl) {
                    n3 = this.expandButton.getX() - 2 * insets.right - 2 * insets.left;
                } else {
                    n3 = this.ribbonBand.getWidth() - this.expandButton.getX() - this.expandButton.getWidth() - 2 * insets.right - 2 * insets.left;
                    n4 = this.expandButton.getX() + this.expandButton.getWidth() + 2 * insets.left;
                }
            }
            this.paintBandTitle(graphics2D, new Rectangle(n4, n2, n3, n), string);
        }
        graphics2D.dispose();
    }

    protected void paintBandTitle(Graphics graphics, Rectangle rectangle, String string) {
        if (rectangle.width <= 0) {
            return;
        }
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        graphics2D.setFont(FlamingoUtilities.getFont(this.ribbonBand, "Ribbon.font", "Button.font", "Panel.font"));
        FontMetrics fontMetrics = graphics2D.getFontMetrics();
        int n = rectangle.y - 2 + (rectangle.height + fontMetrics.getAscent()) / 2;
        int n2 = (int)fontMetrics.getStringBounds(string, graphics).getWidth();
        String string2 = string;
        while (n2 > rectangle.width) {
            string = string.substring(0, string.length() - 1);
            string2 = string + "...";
            n2 = (int)fontMetrics.getStringBounds(string2, graphics).getWidth();
        }
        int n3 = rectangle.x + (rectangle.width - n2) / 2;
        graphics2D.setColor(this.ribbonBand.getForeground());
        graphics2D.drawString(string2, n3, n);
        graphics2D.dispose();
    }

    protected void paintBandTitleBackground(Graphics graphics, Rectangle rectangle, String string) {
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        graphics2D.setComposite(AlphaComposite.SrcOver.derive(0.7f + 0.3f * this.rolloverAmount));
        FlamingoUtilities.renderSurface(graphics2D, this.ribbonBand, rectangle, this.rolloverAmount > 0.0f, true, false);
        graphics2D.dispose();
    }

    public void setRolloverAmount(float f) {
        this.rolloverAmount = f;
    }

    protected void paintBandBackground(Graphics graphics, Rectangle rectangle) {
        graphics.setColor(this.ribbonBand.getBackground());
        graphics.fillRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
    }

    @Override
    public float getRolloverAmount() {
        return this.rolloverAmount;
    }

    @Override
    public int getBandTitleHeight() {
        int n;
        Font font = FlamingoUtilities.getFont(this.ribbonBand, "Ribbon.font", "Button.font", "Panel.font");
        if (font == null) {
            font = new JLabel().getFont();
        }
        if ((n = font.getSize() + 5) % 2 == 0) {
            ++n;
        }
        return n;
    }

    @Override
    public int getPreferredCollapsedWidth() {
        return this.collapsedButton.getPreferredSize().width + 2;
    }

    @Override
    public void trackMouseCrossing(boolean bl) {
        if (bl) {
            this.rolloverTimeline.play();
        } else {
            this.rolloverTimeline.playReverse();
        }
        this.ribbonBand.repaint();
    }

    public AbstractCommandButton getExpandButton() {
        return this.expandButton;
    }

    protected static class RoundBorder
    implements Border {
        protected Color color;
        protected Insets insets;

        public RoundBorder(Color color, Insets insets) {
            this.color = color;
            this.insets = insets;
        }

        @Override
        public Insets getBorderInsets(Component component) {
            return this.insets;
        }

        @Override
        public boolean isBorderOpaque() {
            return false;
        }

        @Override
        public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
            Graphics2D graphics2D = (Graphics2D)graphics.create();
            graphics2D.setColor(this.color);
            graphics2D.drawRoundRect(n, n2, n3 - 1, n4 - 1, 3, 3);
            graphics2D.dispose();
        }
    }

    private static class AWTRibbonEventListener
    implements AWTEventListener {
        private static AWTRibbonEventListener instance;
        private int installCount = 0;
        private AbstractRibbonBand lastHovered;

        private AWTRibbonEventListener() {
        }

        public static void install() {
            if (instance == null) {
                instance = new AWTRibbonEventListener();
                AccessController.doPrivileged(new PrivilegedAction<Object>(){

                    @Override
                    public Object run() {
                        Toolkit.getDefaultToolkit().addAWTEventListener(instance, 131088);
                        return null;
                    }
                });
            }
            ++AWTRibbonEventListener.instance.installCount;
        }

        public static boolean uninstall() {
            if (instance != null) {
                --AWTRibbonEventListener.instance.installCount;
                if (AWTRibbonEventListener.instance.installCount == 0) {
                    Toolkit.getDefaultToolkit().removeAWTEventListener(instance);
                    instance = null;
                }
                return true;
            }
            return false;
        }

        @Override
        public void eventDispatched(AWTEvent aWTEvent) {
            Component component;
            Object object;
            MouseEvent mouseEvent = (MouseEvent)aWTEvent;
            if (mouseEvent.getID() == 504) {
                object = aWTEvent.getSource();
                if (!(object instanceof Component)) {
                    return;
                }
                component = (Component)object;
                AbstractRibbonBand abstractRibbonBand = component instanceof AbstractRibbonBand ? (AbstractRibbonBand)component : (AbstractRibbonBand)SwingUtilities.getAncestorOfClass(AbstractRibbonBand.class, component);
                this.setHoveredBand(abstractRibbonBand);
            }
            if (mouseEvent.getID() == 507) {
                if (PopupPanelManager.defaultManager().getShownPath().size() > 0) {
                    return;
                }
                object = aWTEvent.getSource();
                if (!(object instanceof Component)) {
                    return;
                }
                component = (Component)object;
                MouseWheelEvent mouseWheelEvent = (MouseWheelEvent)mouseEvent;
                Component component2 = SwingUtilities.getDeepestComponentAt(component, mouseWheelEvent.getX(), mouseWheelEvent.getY());
                JRibbon jRibbon = (JRibbon)SwingUtilities.getAncestorOfClass(JRibbon.class, component2);
                if (jRibbon != null) {
                    jRibbon.getUI().handleMouseWheelEvent((MouseWheelEvent)mouseEvent);
                }
            }
        }

        private void setHoveredBand(AbstractRibbonBand abstractRibbonBand) {
            if (this.lastHovered == abstractRibbonBand) {
                return;
            }
            if (this.lastHovered != null) {
                this.lastHovered.getUI().trackMouseCrossing(false);
            }
            this.lastHovered = abstractRibbonBand;
            if (abstractRibbonBand != null) {
                abstractRibbonBand.getUI().trackMouseCrossing(true);
            }
        }

    }

    private class RibbonBandLayout
    implements LayoutManager {
        private RibbonBandLayout() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            Insets insets = container.getInsets();
            Object t = BasicRibbonBandUI.this.ribbonBand.getControlPanel();
            boolean bl = t == null || !t.isVisible();
            int n = bl ? BasicRibbonBandUI.this.collapsedButton.getPreferredSize().width : t.getPreferredSize().width;
            int n2 = (bl ? BasicRibbonBandUI.this.collapsedButton.getPreferredSize().height : t.getPreferredSize().height) + BasicRibbonBandUI.this.getBandTitleHeight();
            return new Dimension(n + 2 + insets.left + insets.right, n2 + insets.top + insets.bottom);
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            Insets insets = container.getInsets();
            Object t = BasicRibbonBandUI.this.ribbonBand.getControlPanel();
            boolean bl = t == null || !t.isVisible();
            int n = bl ? BasicRibbonBandUI.this.collapsedButton.getMinimumSize().width : t.getMinimumSize().width;
            int n2 = bl ? BasicRibbonBandUI.this.collapsedButton.getMinimumSize().height + BasicRibbonBandUI.this.getBandTitleHeight() : t.getMinimumSize().height + BasicRibbonBandUI.this.getBandTitleHeight();
            return new Dimension(n + 2 + insets.left + insets.right, n2 + insets.top + insets.bottom);
        }

        @Override
        public void layoutContainer(Container container) {
            CollapsedButtonPopupPanel collapsedButtonPopupPanel;
            if (!container.isVisible()) {
                return;
            }
            Insets insets = container.getInsets();
            int n = container.getHeight() - insets.top - insets.bottom;
            RibbonBandResizePolicy ribbonBandResizePolicy = ((AbstractRibbonBand)container).getCurrentResizePolicy();
            if (ribbonBandResizePolicy instanceof IconRibbonBandResizePolicy) {
                BasicRibbonBandUI.this.collapsedButton.setVisible(true);
                int n2 = BasicRibbonBandUI.this.collapsedButton.getPreferredSize().width;
                BasicRibbonBandUI.this.collapsedButton.setBounds((container.getWidth() - n2) / 2, insets.top, n2, container.getHeight() - insets.top - insets.bottom);
                if (BasicRibbonBandUI.this.collapsedButton.getPopupCallback() == null) {
                    final AbstractRibbonBand abstractRibbonBand = BasicRibbonBandUI.this.ribbonBand.cloneBand();
                    abstractRibbonBand.setControlPanel(BasicRibbonBandUI.this.ribbonBand.getControlPanel());
                    List<RibbonBandResizePolicy> list = BasicRibbonBandUI.this.ribbonBand.getResizePolicies();
                    abstractRibbonBand.setResizePolicies(list);
                    RibbonBandResizePolicy ribbonBandResizePolicy2 = list.get(0);
                    abstractRibbonBand.setCurrentResizePolicy(ribbonBandResizePolicy2);
                    int n3 = abstractRibbonBand.getControlPanel().getUI().getLayoutGap();
                    final Dimension dimension = new Dimension(insets.left + insets.right + n3 + ribbonBandResizePolicy2.getPreferredWidth(n, n3), insets.top + insets.bottom + Math.max(container.getHeight(), BasicRibbonBandUI.this.ribbonBand.getControlPanel().getPreferredSize().height + BasicRibbonBandUI.this.getBandTitleHeight()));
                    BasicRibbonBandUI.this.collapsedButton.setPopupCallback(new PopupPanelCallback(){

                        @Override
                        public JPopupPanel getPopupPanel(JCommandButton jCommandButton) {
                            return new CollapsedButtonPopupPanel(abstractRibbonBand, dimension);
                        }
                    });
                    BasicRibbonBandUI.this.ribbonBand.setControlPanel(null);
                    BasicRibbonBandUI.this.ribbonBand.setPopupRibbonBand(abstractRibbonBand);
                }
                if (BasicRibbonBandUI.this.expandButton != null) {
                    BasicRibbonBandUI.this.expandButton.setBounds(0, 0, 0, 0);
                }
                return;
            }
            if (BasicRibbonBandUI.this.collapsedButton.isVisible()) {
                CollapsedButtonPopupPanel collapsedButtonPopupPanel2 = collapsedButtonPopupPanel = BasicRibbonBandUI.this.collapsedButton.getPopupCallback() != null ? (CollapsedButtonPopupPanel)BasicRibbonBandUI.this.collapsedButton.getPopupCallback().getPopupPanel(BasicRibbonBandUI.this.collapsedButton) : null;
                if (collapsedButtonPopupPanel != null) {
                    AbstractRibbonBand abstractRibbonBand = (AbstractRibbonBand)collapsedButtonPopupPanel.removeComponent();
                    BasicRibbonBandUI.this.ribbonBand.setControlPanel(abstractRibbonBand.getControlPanel());
                    BasicRibbonBandUI.this.ribbonBand.setPopupRibbonBand(null);
                    BasicRibbonBandUI.this.collapsedButton.setPopupCallback(null);
                }
            }
            BasicRibbonBandUI.this.collapsedButton.setVisible(false);
            collapsedButtonPopupPanel = (CollapsedButtonPopupPanel)BasicRibbonBandUI.this.ribbonBand.getControlPanel();
            collapsedButtonPopupPanel.setVisible(true);
            collapsedButtonPopupPanel.setBounds(insets.left, insets.top, container.getWidth() - insets.left - insets.right, container.getHeight() - BasicRibbonBandUI.this.getBandTitleHeight() - insets.top - insets.bottom);
            collapsedButtonPopupPanel.doLayout();
            if (BasicRibbonBandUI.this.expandButton != null) {
                int n4 = BasicRibbonBandUI.this.expandButton.getPreferredSize().width;
                int n5 = BasicRibbonBandUI.this.expandButton.getPreferredSize().height;
                int n6 = BasicRibbonBandUI.this.getBandTitleHeight() - 4;
                if (n5 > n6) {
                    n5 = n6;
                }
                int n7 = container.getHeight() - (BasicRibbonBandUI.this.getBandTitleHeight() - n5) / 2;
                boolean bl = BasicRibbonBandUI.this.ribbonBand.getComponentOrientation().isLeftToRight();
                if (bl) {
                    BasicRibbonBandUI.this.expandButton.setBounds(container.getWidth() - insets.right - n4, n7 - n5, n4, n5);
                } else {
                    BasicRibbonBandUI.this.expandButton.setBounds(insets.left, n7 - n5, n4, n5);
                }
            }
        }

    }

    protected static class CollapsedButtonPopupPanel
    extends JPopupPanel {
        protected Component component;

        public CollapsedButtonPopupPanel(Component component, Dimension dimension) {
            this.component = component;
            this.setLayout(new BorderLayout());
            this.add(component, "Center");
            this.setPreferredSize(dimension);
            this.setSize(dimension);
        }

        public Component removeComponent() {
            this.remove(this.component);
            return this.component;
        }

        public Component getComponent() {
            return this.component;
        }
    }

}

