/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.trident.Timeline
 *  org.pushingpixels.trident.callback.TimelineCallback
 *  org.pushingpixels.trident.swing.SwingRepaintCallback
 */
package org.pushingpixels.flamingo.internal.ui.common.popup;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.ButtonModel;
import javax.swing.DefaultButtonModel;
import javax.swing.JComponent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelManager;
import org.pushingpixels.flamingo.internal.ui.common.popup.ColorSelectorComponentUI;
import org.pushingpixels.flamingo.internal.ui.common.popup.JColorSelectorComponent;
import org.pushingpixels.trident.Timeline;
import org.pushingpixels.trident.callback.TimelineCallback;
import org.pushingpixels.trident.swing.SwingRepaintCallback;

public class BasicColorSelectorComponentUI
extends ColorSelectorComponentUI {
    protected JColorSelectorComponent colorSelectorComponent;
    protected ButtonModel buttonModel;
    protected MouseListener mouseListener;
    protected ChangeListener modelChangeListener;
    protected ActionListener actionListener;
    protected Timeline rolloverTimeline;
    protected float rollover;

    public static ComponentUI createUI(JComponent jComponent) {
        return new BasicColorSelectorComponentUI();
    }

    @Override
    public void installUI(JComponent jComponent) {
        this.colorSelectorComponent = (JColorSelectorComponent)jComponent;
        this.buttonModel = new DefaultButtonModel();
        this.installDefaults();
        this.installComponents();
        this.installListeners();
    }

    @Override
    public void uninstallUI(JComponent jComponent) {
        this.uninstallListeners();
        this.uninstallComponents();
        this.uninstallDefaults();
        jComponent.setLayout(null);
        this.colorSelectorComponent = null;
    }

    protected void installListeners() {
        this.mouseListener = new MouseAdapter(){

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {
                if (!BasicColorSelectorComponentUI.this.buttonModel.isRollover()) {
                    BasicColorSelectorComponentUI.this.colorSelectorComponent.onColorRollover(BasicColorSelectorComponentUI.this.colorSelectorComponent.getColor());
                    BasicColorSelectorComponentUI.this.rolloverTimeline.play();
                }
                BasicColorSelectorComponentUI.this.buttonModel.setRollover(true);
            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {
                if (BasicColorSelectorComponentUI.this.buttonModel.isRollover()) {
                    BasicColorSelectorComponentUI.this.colorSelectorComponent.onColorRollover(null);
                    BasicColorSelectorComponentUI.this.rolloverTimeline.playReverse();
                }
                BasicColorSelectorComponentUI.this.buttonModel.setRollover(false);
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                BasicColorSelectorComponentUI.this.buttonModel.setArmed(true);
                BasicColorSelectorComponentUI.this.buttonModel.setPressed(true);
            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
                BasicColorSelectorComponentUI.this.buttonModel.setPressed(false);
                BasicColorSelectorComponentUI.this.buttonModel.setArmed(false);
            }
        };
        this.colorSelectorComponent.addMouseListener(this.mouseListener);
        this.modelChangeListener = new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                BasicColorSelectorComponentUI.this.colorSelectorComponent.repaint();
            }
        };
        this.buttonModel.addChangeListener(this.modelChangeListener);
        this.actionListener = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                BasicColorSelectorComponentUI.this.colorSelectorComponent.onColorSelected(BasicColorSelectorComponentUI.this.colorSelectorComponent.getColor());
                PopupPanelManager.defaultManager().hidePopups(null);
            }
        };
        this.buttonModel.addActionListener(this.actionListener);
    }

    protected void uninstallListeners() {
        this.buttonModel.removeActionListener(this.actionListener);
        this.actionListener = null;
        this.buttonModel.removeChangeListener(this.modelChangeListener);
        this.modelChangeListener = null;
        this.colorSelectorComponent.removeMouseListener(this.mouseListener);
        this.mouseListener = null;
    }

    protected void installDefaults() {
        this.rolloverTimeline = new Timeline((Object)this);
        this.rolloverTimeline.addPropertyToInterpolate("rollover", (Object)Float.valueOf(0.0f), (Object)Float.valueOf(1.0f));
        this.rolloverTimeline.addCallback((TimelineCallback)new SwingRepaintCallback((Component)this.colorSelectorComponent));
        this.rolloverTimeline.setDuration(150);
    }

    protected void uninstallDefaults() {
    }

    protected void installComponents() {
    }

    protected void uninstallComponents() {
    }

    public void setRollover(float f) {
        this.rollover = f;
    }

    @Override
    public void update(Graphics graphics, JComponent jComponent) {
        int n = this.colorSelectorComponent.getWidth();
        int n2 = this.colorSelectorComponent.getHeight();
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        Color color = this.colorSelectorComponent.getColor();
        graphics2D.setColor(color);
        graphics2D.fillRect(0, 0, n, n2);
        float[] arrf = new float[3];
        Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), arrf);
        float f = arrf[2] * 0.7f;
        graphics2D.setColor(new Color(f, f, f));
        int n3 = this.colorSelectorComponent.isTopOpen() ? 1 : 0;
        int n4 = this.colorSelectorComponent.isBottomOpen() ? 1 : 0;
        graphics2D.drawRect(0, - n3, n - 1, n2 - 1 + n3 + n4);
        if (this.rollover > 0.0f) {
            graphics2D.setComposite(AlphaComposite.SrcOver.derive(this.rollover));
            graphics2D.setColor(new Color(207, 186, 115));
            graphics2D.drawRect(0, 0, n - 1, n2 - 1);
            graphics2D.setColor(new Color(230, 212, 150));
            graphics2D.drawRect(1, 1, n - 3, n2 - 3);
        }
        graphics2D.dispose();
    }

}

