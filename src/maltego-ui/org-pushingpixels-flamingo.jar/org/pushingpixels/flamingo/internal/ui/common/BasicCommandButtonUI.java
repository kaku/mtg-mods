/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Composite;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.color.ColorSpace;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImageOp;
import java.awt.image.ColorConvertOp;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.CellRendererPane;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;
import javax.swing.Popup;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicGraphicsUtils;
import org.pushingpixels.flamingo.BlankPopupFixPopupFactory;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.AsynchronousLoadListener;
import org.pushingpixels.flamingo.api.common.AsynchronousLoading;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButtonStrip;
import org.pushingpixels.flamingo.api.common.PopupActionListener;
import org.pushingpixels.flamingo.api.common.icon.FilteredResizableIcon;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;
import org.pushingpixels.flamingo.api.common.model.PopupButtonModel;
import org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelManager;
import org.pushingpixels.flamingo.internal.ui.common.BasicCommandButtonListener;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonUI;
import org.pushingpixels.flamingo.internal.ui.common.ResizableIconUIResource;
import org.pushingpixels.flamingo.internal.utils.ButtonSizingUtils;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;
import org.pushingpixels.flamingo.internal.utils.RenderingUtils;

public class BasicCommandButtonUI
extends CommandButtonUI {
    protected AbstractCommandButton commandButton;
    protected boolean isUnderMouse;
    protected PropertyChangeListener propertyChangeListener;
    protected BasicCommandButtonListener basicPopupButtonListener;
    public static final String EMULATE_SQUARE_BUTTON = "flamingo.internal.commandButton.ui.emulateSquare";
    public static final String DONT_DISPOSE_POPUPS = "flamingo.internal.commandButton.ui.dontDisposePopups";
    protected ActionListener disposePopupsActionListener;
    protected PopupActionListener popupActionListener;
    protected ResizableIcon popupActionIcon;
    protected CommandButtonLayoutManager layoutManager;
    protected CellRendererPane buttonRendererPane;
    protected AbstractButton rendererButton;
    protected JSeparator rendererSeparator;

    public static ComponentUI createUI(JComponent jComponent) {
        return new BasicCommandButtonUI();
    }

    @Override
    public void installUI(JComponent jComponent) {
        this.commandButton = (AbstractCommandButton)jComponent;
        this.installDefaults();
        this.installComponents();
        this.installListeners();
        this.installKeyboardActions();
        this.layoutManager = this.commandButton.getDisplayState().createLayoutManager(this.commandButton);
        this.updateCustomDimension();
    }

    @Override
    public void uninstallUI(JComponent jComponent) {
        jComponent.setLayout(null);
        this.uninstallKeyboardActions();
        this.uninstallListeners();
        this.uninstallComponents();
        this.uninstallDefaults();
        this.commandButton = null;
    }

    protected void installDefaults() {
        this.configureRenderer();
        this.updateBorder();
        this.syncDisabledIcon();
    }

    protected void configureRenderer() {
        this.buttonRendererPane = new CellRendererPane();
        this.commandButton.add(this.buttonRendererPane);
        this.rendererButton = this.createRendererButton();
        this.rendererButton.setOpaque(false);
        this.rendererSeparator = new JSeparator();
        Font font = this.commandButton.getFont();
        if (font == null) {
            this.commandButton.setFont(this.rendererButton.getFont());
        }
        this.rendererButton.putClientProperty("JButton.buttonType", "square");
    }

    protected void updateBorder() {
        Border border = this.commandButton.getBorder();
        if (border == null || border instanceof UIResource) {
            int n = (int)(this.commandButton.getVGapScaleFactor() * 4.0);
            int n2 = (int)(this.commandButton.getHGapScaleFactor() * 6.0);
            this.commandButton.setBorder(new BorderUIResource.EmptyBorderUIResource(n, n2, n, n2));
        }
    }

    protected AbstractButton createRendererButton() {
        return new JButton("");
    }

    protected void installComponents() {
        this.updatePopupActionIcon();
        ResizableIcon resizableIcon = this.commandButton.getIcon();
        if (resizableIcon instanceof AsynchronousLoading) {
            ((AsynchronousLoading)((Object)resizableIcon)).addAsynchronousLoadListener(new AsynchronousLoadListener(){

                @Override
                public void completed(boolean bl) {
                    if (bl && BasicCommandButtonUI.this.commandButton != null) {
                        BasicCommandButtonUI.this.commandButton.repaint();
                    }
                }
            });
        }
        if (this.commandButton instanceof JCommandButton) {
            this.popupActionIcon = this.createPopupActionIcon();
        }
    }

    protected void installListeners() {
        this.basicPopupButtonListener = this.createButtonListener(this.commandButton);
        if (this.basicPopupButtonListener != null) {
            this.commandButton.addMouseListener(this.basicPopupButtonListener);
            this.commandButton.addMouseMotionListener(this.basicPopupButtonListener);
            this.commandButton.addFocusListener(this.basicPopupButtonListener);
            this.commandButton.addChangeListener(this.basicPopupButtonListener);
        }
        this.propertyChangeListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                Object object;
                Object object2;
                if ("icon".equals(propertyChangeEvent.getPropertyName())) {
                    object = (Icon)propertyChangeEvent.getNewValue();
                    if (object instanceof AsynchronousLoading) {
                        object2 = (AsynchronousLoading)object;
                        object2.addAsynchronousLoadListener(new AsynchronousLoadListener(){

                            @Override
                            public void completed(boolean bl) {
                                if (bl && BasicCommandButtonUI.this.commandButton != null) {
                                    BasicCommandButtonUI.this.syncIconDimension();
                                    BasicCommandButtonUI.this.syncDisabledIcon();
                                    BasicCommandButtonUI.this.commandButton.repaint();
                                }
                            }
                        });
                        if (!object2.isLoading()) {
                            BasicCommandButtonUI.this.syncIconDimension();
                            BasicCommandButtonUI.this.syncDisabledIcon();
                            BasicCommandButtonUI.this.commandButton.repaint();
                        }
                    } else {
                        BasicCommandButtonUI.this.syncIconDimension();
                        BasicCommandButtonUI.this.syncDisabledIcon();
                        BasicCommandButtonUI.this.commandButton.revalidate();
                        BasicCommandButtonUI.this.commandButton.repaint();
                    }
                }
                if ("commandButtonKind".equals(propertyChangeEvent.getPropertyName())) {
                    BasicCommandButtonUI.this.updatePopupActionIcon();
                }
                if ("popupOrientationKind".equals(propertyChangeEvent.getPropertyName())) {
                    BasicCommandButtonUI.this.updatePopupActionIcon();
                }
                if ("customDimension".equals(propertyChangeEvent.getPropertyName())) {
                    BasicCommandButtonUI.this.updateCustomDimension();
                }
                if ("hgapScaleFactor".equals(propertyChangeEvent.getPropertyName())) {
                    BasicCommandButtonUI.this.updateBorder();
                }
                if ("vgapScaleFactor".equals(propertyChangeEvent.getPropertyName())) {
                    BasicCommandButtonUI.this.updateBorder();
                }
                if ("popupModel".equals(propertyChangeEvent.getPropertyName())) {
                    object = (PopupButtonModel)propertyChangeEvent.getOldValue();
                    object2 = (PopupButtonModel)propertyChangeEvent.getNewValue();
                    if (object != null) {
                        object.removePopupActionListener(BasicCommandButtonUI.this.popupActionListener);
                        BasicCommandButtonUI.this.popupActionListener = null;
                    }
                    if (object2 != null) {
                        BasicCommandButtonUI.this.popupActionListener = BasicCommandButtonUI.this.createPopupActionListener();
                        object2.addPopupActionListener(BasicCommandButtonUI.this.popupActionListener);
                    }
                }
                if ("displayState".equals(propertyChangeEvent.getPropertyName())) {
                    BasicCommandButtonUI.this.syncIconDimension();
                    BasicCommandButtonUI.this.syncDisabledIcon();
                    BasicCommandButtonUI.this.commandButton.invalidate();
                    BasicCommandButtonUI.this.commandButton.revalidate();
                    BasicCommandButtonUI.this.commandButton.doLayout();
                }
                if (BasicCommandButtonUI.this.layoutManager != null) {
                    BasicCommandButtonUI.this.layoutManager.propertyChange(propertyChangeEvent);
                }
                if ("componentOrientation".equals(propertyChangeEvent.getPropertyName())) {
                    BasicCommandButtonUI.this.updatePopupActionIcon();
                    BasicCommandButtonUI.this.commandButton.repaint();
                }
            }

        };
        this.commandButton.addPropertyChangeListener(this.propertyChangeListener);
        this.disposePopupsActionListener = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JCommandPopupMenu jCommandPopupMenu;
                boolean bl;
                boolean bl2 = bl = !Boolean.TRUE.equals(BasicCommandButtonUI.this.commandButton.getClientProperty("flamingo.internal.commandButton.ui.dontDisposePopups"));
                if (bl && (jCommandPopupMenu = (JCommandPopupMenu)SwingUtilities.getAncestorOfClass(JCommandPopupMenu.class, BasicCommandButtonUI.this.commandButton)) != null) {
                    bl = jCommandPopupMenu.isToDismissOnChildClick();
                }
                if (bl) {
                    if (SwingUtilities.getAncestorOfClass(JPopupPanel.class, BasicCommandButtonUI.this.commandButton) != null) {
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                if (BasicCommandButtonUI.this.commandButton != null) {
                                    BasicCommandButtonUI.this.commandButton.getActionModel().setPressed(false);
                                    BasicCommandButtonUI.this.commandButton.getActionModel().setRollover(false);
                                    BasicCommandButtonUI.this.commandButton.getActionModel().setArmed(false);
                                }
                            }
                        });
                    }
                    PopupPanelManager.defaultManager().hidePopups(null);
                }
            }

        };
        this.commandButton.addActionListener(this.disposePopupsActionListener);
        if (this.commandButton instanceof JCommandButton) {
            this.popupActionListener = this.createPopupActionListener();
            ((JCommandButton)this.commandButton).getPopupModel().addPopupActionListener(this.popupActionListener);
        }
    }

    protected ResizableIcon createPopupActionIcon() {
        return FlamingoUtilities.getCommandButtonPopupActionIcon((JCommandButton)this.commandButton);
    }

    protected BasicCommandButtonListener createButtonListener(AbstractCommandButton abstractCommandButton) {
        return new BasicCommandButtonListener();
    }

    protected void installKeyboardActions() {
        if (this.basicPopupButtonListener != null) {
            this.basicPopupButtonListener.installKeyboardActions(this.commandButton);
        }
    }

    protected void uninstallDefaults() {
        this.unconfigureRenderer();
    }

    protected void unconfigureRenderer() {
        if (this.buttonRendererPane != null) {
            this.commandButton.remove(this.buttonRendererPane);
        }
        this.buttonRendererPane = null;
    }

    protected void uninstallComponents() {
    }

    protected void uninstallListeners() {
        if (this.basicPopupButtonListener != null) {
            this.commandButton.removeMouseListener(this.basicPopupButtonListener);
            this.commandButton.removeMouseListener(this.basicPopupButtonListener);
            this.commandButton.removeMouseMotionListener(this.basicPopupButtonListener);
            this.commandButton.removeFocusListener(this.basicPopupButtonListener);
            this.commandButton.removeChangeListener(this.basicPopupButtonListener);
        }
        this.commandButton.removePropertyChangeListener(this.propertyChangeListener);
        this.propertyChangeListener = null;
        this.commandButton.removeActionListener(this.disposePopupsActionListener);
        this.disposePopupsActionListener = null;
        if (this.commandButton instanceof JCommandButton) {
            ((JCommandButton)this.commandButton).getPopupModel().removePopupActionListener(this.popupActionListener);
            this.popupActionListener = null;
        }
    }

    protected void uninstallKeyboardActions() {
        if (this.basicPopupButtonListener != null) {
            this.basicPopupButtonListener.uninstallKeyboardActions(this.commandButton);
        }
    }

    @Override
    public void update(Graphics graphics, JComponent jComponent) {
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        RenderingUtils.installDesktopHints(graphics2D);
        RenderingUtils.setupTextAntialiasing(graphics2D, jComponent);
        super.update(graphics2D, jComponent);
        graphics2D.dispose();
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        graphics.setFont(FlamingoUtilities.getFont(this.commandButton, "Ribbon.font", "Button.font", "Panel.font"));
        CommandButtonLayoutManager.CommandButtonLayoutInfo commandButtonLayoutInfo = this.getLayoutInfo();
        this.commandButton.putClientProperty("icon.bounds", commandButtonLayoutInfo.iconRect);
        if (this.isPaintingBackground()) {
            this.paintButtonBackground(graphics, new Rectangle(0, 0, this.commandButton.getWidth(), this.commandButton.getHeight()));
        }
        if (commandButtonLayoutInfo.iconRect != null) {
            this.paintButtonIcon(graphics, commandButtonLayoutInfo.iconRect);
        }
        if (commandButtonLayoutInfo.popupActionRect.getWidth() > 0.0) {
            this.paintPopupActionIcon(graphics, commandButtonLayoutInfo.popupActionRect);
        }
        FontMetrics fontMetrics = graphics.getFontMetrics();
        boolean bl = this.commandButton.isEnabled();
        if (this.commandButton instanceof JCommandButton) {
            Iterator<CommandButtonLayoutManager.TextLayoutInfo> iterator = (JCommandButton)this.commandButton;
            bl = commandButtonLayoutInfo.isTextInActionArea ? iterator.getActionModel().isEnabled() : iterator.getPopupModel().isEnabled();
        }
        graphics.setColor(this.getForegroundColor(bl));
        if (commandButtonLayoutInfo.textLayoutInfoList != null) {
            for (CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo : commandButtonLayoutInfo.textLayoutInfoList) {
                if (textLayoutInfo.text == null) continue;
                BasicGraphicsUtils.drawString(graphics, textLayoutInfo.text, -1, textLayoutInfo.textRect.x, textLayoutInfo.textRect.y + fontMetrics.getAscent());
            }
        }
        if (bl) {
            graphics.setColor(FlamingoUtilities.getColor(Color.gray, "Label.disabledForeground"));
        } else {
            graphics.setColor(FlamingoUtilities.getColor(Color.gray, "Label.disabledForeground").brighter());
        }
        if (commandButtonLayoutInfo.extraTextLayoutInfoList != null) {
            for (CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo : commandButtonLayoutInfo.extraTextLayoutInfoList) {
                if (textLayoutInfo.text == null) continue;
                BasicGraphicsUtils.drawString(graphics, textLayoutInfo.text, -1, textLayoutInfo.textRect.x, textLayoutInfo.textRect.y + fontMetrics.getAscent());
            }
        }
        if (this.isPaintingSeparators() && commandButtonLayoutInfo.separatorArea != null) {
            if (commandButtonLayoutInfo.separatorOrientation == CommandButtonLayoutManager.CommandButtonSeparatorOrientation.HORIZONTAL) {
                this.paintButtonHorizontalSeparator(graphics, commandButtonLayoutInfo.separatorArea);
            } else {
                this.paintButtonVerticalSeparator(graphics, commandButtonLayoutInfo.separatorArea);
            }
        }
    }

    protected Color getForegroundColor(boolean bl) {
        if (bl) {
            return FlamingoUtilities.getColor(Color.DARK_GRAY, "Button.foreground");
        }
        return FlamingoUtilities.getColor(Color.gray, "Label.disabledForeground");
    }

    protected void paintPopupActionIcon(Graphics graphics, Rectangle rectangle) {
        int n = Math.max(rectangle.width - 2, 7);
        if (n % 2 == 0) {
            --n;
        }
        this.popupActionIcon.setDimension(new Dimension(n, n));
        this.popupActionIcon.paintIcon(this.commandButton, graphics, rectangle.x + (rectangle.width - n) / 2, rectangle.y + (rectangle.height - n) / 2);
    }

    protected Icon getIconToPaint() {
        return this.toUseDisabledIcon() && this.commandButton.getDisabledIcon() != null ? this.commandButton.getDisabledIcon() : this.commandButton.getIcon();
    }

    protected boolean toUseDisabledIcon() {
        boolean bl = this.commandButton instanceof JCommandButton && ((JCommandButton)this.commandButton).getCommandButtonKind() == JCommandButton.CommandButtonKind.POPUP_ONLY ? !((JCommandButton)this.commandButton).getPopupModel().isEnabled() : !this.commandButton.getActionModel().isEnabled();
        return bl;
    }

    protected void paintButtonVerticalSeparator(Graphics graphics, Rectangle rectangle) {
        this.buttonRendererPane.setBounds(0, 0, this.commandButton.getWidth(), this.commandButton.getHeight());
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        this.rendererSeparator.setOrientation(1);
        this.buttonRendererPane.paintComponent(graphics2D, this.rendererSeparator, this.commandButton, rectangle.x, 2, 2, this.commandButton.getHeight() - 4, true);
        graphics2D.dispose();
    }

    protected void paintButtonHorizontalSeparator(Graphics graphics, Rectangle rectangle) {
        this.buttonRendererPane.setBounds(0, 0, this.commandButton.getWidth(), this.commandButton.getHeight());
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        this.rendererSeparator.setOrientation(0);
        this.buttonRendererPane.paintComponent(graphics2D, this.rendererSeparator, this.commandButton, 2, rectangle.y, this.commandButton.getWidth() - 4, 2, true);
        graphics2D.dispose();
    }

    protected void paintButtonBackground(Graphics graphics, Rectangle rectangle) {
        Graphics2D graphics2D;
        float f;
        ActionButtonModel actionButtonModel = this.commandButton.getActionModel();
        PopupButtonModel popupButtonModel = this.commandButton instanceof JCommandButton ? ((JCommandButton)this.commandButton).getPopupModel() : null;
        this.paintButtonBackground(graphics, rectangle, actionButtonModel, popupButtonModel);
        Rectangle rectangle2 = this.getLayoutInfo().actionClickArea;
        Rectangle rectangle3 = this.getLayoutInfo().popupClickArea;
        if (rectangle2 != null && !rectangle2.isEmpty()) {
            graphics2D = (Graphics2D)graphics.create();
            graphics2D.clip(rectangle2);
            f = 0.4f;
            if (popupButtonModel != null && !popupButtonModel.isEnabled()) {
                f = 1.0f;
            }
            graphics2D.setComposite(AlphaComposite.SrcOver.derive(f));
            this.paintButtonBackground(graphics2D, rectangle, actionButtonModel);
            graphics2D.dispose();
        }
        if (rectangle3 != null && !rectangle3.isEmpty()) {
            graphics2D = (Graphics2D)graphics.create();
            graphics2D.clip(rectangle3);
            f = 0.4f;
            if (!actionButtonModel.isEnabled()) {
                f = 1.0f;
            }
            graphics2D.setComposite(AlphaComposite.SrcOver.derive(f));
            this.paintButtonBackground(graphics2D, rectangle, popupButtonModel);
            graphics2D.dispose();
        }
    }

    protected /* varargs */ void paintButtonBackground(Graphics graphics, Rectangle rectangle, ButtonModel ... arrbuttonModel) {
        if (arrbuttonModel.length == 0) {
            return;
        }
        if (arrbuttonModel.length == 1 && arrbuttonModel[0] == null) {
            return;
        }
        this.buttonRendererPane.setBounds(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        this.rendererButton.setRolloverEnabled(true);
        boolean bl = true;
        boolean bl2 = false;
        boolean bl3 = true;
        boolean bl4 = true;
        boolean bl5 = true;
        for (ButtonModel object2 : arrbuttonModel) {
            if (object2 == null) continue;
            bl = bl && object2.isEnabled();
            bl2 = bl2 || object2.isRollover();
            bl3 = bl3 && object2.isPressed();
            bl4 = bl4 && object2.isArmed();
            boolean bl6 = bl5 = bl5 && object2.isSelected();
            if (!(object2 instanceof PopupButtonModel)) continue;
            bl2 = bl2 || ((PopupButtonModel)object2).isPopupShowing();
        }
        this.rendererButton.getModel().setEnabled(bl);
        this.rendererButton.getModel().setRollover(bl2);
        this.rendererButton.getModel().setPressed(bl3);
        this.rendererButton.getModel().setArmed(bl4);
        this.rendererButton.getModel().setSelected(bl5);
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        Color color = FlamingoUtilities.getBorderColor();
        if (Boolean.TRUE.equals(this.commandButton.getClientProperty("flamingo.internal.commandButton.ui.emulateSquare"))) {
            this.buttonRendererPane.paintComponent(graphics2D, this.rendererButton, this.commandButton, rectangle.x - rectangle.width / 2, rectangle.y - rectangle.height / 2, 2 * rectangle.width, 2 * rectangle.height, true);
            graphics2D.setColor(color);
            graphics2D.drawRect(rectangle.x, rectangle.y, rectangle.width - 1, rectangle.height - 1);
        } else {
            Insets insets;
            AbstractCommandButton.CommandButtonLocationOrderKind commandButtonLocationOrderKind = this.commandButton.getLocationOrderKind();
            Insets insets2 = insets = this.rendererButton instanceof JToggleButton ? ButtonSizingUtils.getInstance().getToggleOutsets() : ButtonSizingUtils.getInstance().getOutsets();
            if (commandButtonLocationOrderKind != null) {
                if (commandButtonLocationOrderKind == AbstractCommandButton.CommandButtonLocationOrderKind.ONLY) {
                    this.buttonRendererPane.paintComponent(graphics2D, this.rendererButton, this.commandButton, rectangle.x - insets.left, rectangle.y - insets.top, rectangle.width + insets.left + insets.right, rectangle.height + insets.top + insets.bottom, true);
                } else {
                    Container container = this.commandButton.getParent();
                    if (container instanceof JCommandButtonStrip && ((JCommandButtonStrip)container).getOrientation() == JCommandButtonStrip.StripOrientation.VERTICAL) {
                        switch (commandButtonLocationOrderKind) {
                            case FIRST: {
                                this.buttonRendererPane.paintComponent(graphics2D, this.rendererButton, this.commandButton, rectangle.x - insets.left, rectangle.y - insets.top, rectangle.width + insets.left + insets.right, 2 * rectangle.height, true);
                                graphics2D.setColor(color);
                                graphics2D.drawLine(rectangle.x + 1, rectangle.y + rectangle.height - 1, rectangle.x + rectangle.width - 2, rectangle.y + rectangle.height - 1);
                                break;
                            }
                            case LAST: {
                                this.buttonRendererPane.paintComponent(graphics2D, this.rendererButton, this.commandButton, rectangle.x - insets.left, rectangle.y - rectangle.height, rectangle.width + insets.left + insets.right, 2 * rectangle.height + insets.bottom, true);
                                break;
                            }
                            case MIDDLE: {
                                this.buttonRendererPane.paintComponent(graphics2D, this.rendererButton, this.commandButton, rectangle.x - insets.left, rectangle.y - rectangle.height, rectangle.width + insets.left + insets.right, 3 * rectangle.height, true);
                                graphics2D.setColor(color);
                                graphics2D.drawLine(rectangle.x + 1, rectangle.y + rectangle.height - 1, rectangle.x + rectangle.width - 2, rectangle.y + rectangle.height - 1);
                            }
                        }
                    } else {
                        boolean bl7 = this.commandButton.getComponentOrientation().isLeftToRight();
                        if (commandButtonLocationOrderKind == AbstractCommandButton.CommandButtonLocationOrderKind.MIDDLE) {
                            this.buttonRendererPane.paintComponent(graphics2D, this.rendererButton, this.commandButton, rectangle.x - rectangle.width, rectangle.y - insets.top, 3 * rectangle.width, rectangle.height + insets.top + insets.bottom, true);
                            graphics2D.setColor(color);
                            graphics2D.drawLine(rectangle.x + rectangle.width - 1, rectangle.y + 1, rectangle.x + rectangle.width - 1, rectangle.y + rectangle.height - 2);
                        } else {
                            boolean bl8;
                            boolean bl9 = bl8 = bl7 && commandButtonLocationOrderKind == AbstractCommandButton.CommandButtonLocationOrderKind.FIRST || !bl7 && commandButtonLocationOrderKind == AbstractCommandButton.CommandButtonLocationOrderKind.LAST;
                            if (bl8) {
                                this.buttonRendererPane.paintComponent(graphics2D, this.rendererButton, this.commandButton, rectangle.x - insets.left, rectangle.y - insets.top, 2 * rectangle.width, rectangle.height + insets.top + insets.bottom, true);
                                graphics2D.setColor(color);
                                graphics2D.drawLine(rectangle.x + rectangle.width - 1, rectangle.y + 1, rectangle.x + rectangle.width - 1, rectangle.y + rectangle.height - 2);
                            } else {
                                this.buttonRendererPane.paintComponent(graphics2D, this.rendererButton, this.commandButton, rectangle.x - rectangle.width, rectangle.y - insets.top, 2 * rectangle.width + insets.right, rectangle.height + insets.top + insets.bottom, true);
                            }
                        }
                    }
                }
            } else {
                this.buttonRendererPane.paintComponent(graphics2D, this.rendererButton, this.commandButton, rectangle.x - insets.left, rectangle.y - insets.top, rectangle.width + insets.left + insets.right, rectangle.height + insets.top + insets.bottom, true);
            }
        }
        graphics2D.dispose();
    }

    protected void updateCustomDimension() {
        int n = this.commandButton.getCustomDimension();
        if (n > 0) {
            this.commandButton.getIcon().setDimension(new Dimension(n, n));
            this.commandButton.setDisplayState(CommandButtonDisplayState.FIT_TO_ICON);
            this.commandButton.invalidate();
            this.commandButton.revalidate();
            this.commandButton.doLayout();
            this.commandButton.repaint();
        }
    }

    protected void updatePopupActionIcon() {
        JCommandButton jCommandButton = (JCommandButton)this.commandButton;
        this.popupActionIcon = jCommandButton.getCommandButtonKind().hasPopup() ? this.createPopupActionIcon() : null;
    }

    protected void paintButtonIcon(Graphics graphics, Rectangle rectangle) {
        Icon icon = this.getIconToPaint();
        if (rectangle == null || icon == null || rectangle.width == 0 || rectangle.height == 0) {
            return;
        }
        icon.paintIcon(this.commandButton, graphics, rectangle.x, rectangle.y);
    }

    @Override
    public Dimension getPreferredSize(JComponent jComponent) {
        AbstractCommandButton abstractCommandButton = (AbstractCommandButton)jComponent;
        return this.layoutManager.getPreferredSize(abstractCommandButton);
    }

    @Override
    public CommandButtonLayoutManager.CommandButtonLayoutInfo getLayoutInfo() {
        return this.layoutManager.getLayoutInfo(this.commandButton, this.commandButton.getGraphics());
    }

    protected int getLayoutGap() {
        Font font = this.commandButton.getFont();
        if (font == null) {
            font = UIManager.getFont("Button.font");
        }
        return (font.getSize() - 4) / 4;
    }

    protected boolean isPaintingSeparators() {
        PopupButtonModel popupButtonModel = this.commandButton instanceof JCommandButton ? ((JCommandButton)this.commandButton).getPopupModel() : null;
        boolean bl = this.commandButton.getActionModel().isRollover();
        boolean bl2 = popupButtonModel != null && popupButtonModel.isRollover();
        return bl || bl2;
    }

    protected boolean isPaintingBackground() {
        PopupButtonModel popupButtonModel = this.commandButton instanceof JCommandButton ? ((JCommandButton)this.commandButton).getPopupModel() : null;
        boolean bl = this.commandButton.getActionModel().isSelected();
        boolean bl2 = popupButtonModel != null && popupButtonModel.isSelected();
        boolean bl3 = this.commandButton.getActionModel().isRollover();
        boolean bl4 = popupButtonModel != null && popupButtonModel.isRollover();
        boolean bl5 = popupButtonModel != null && popupButtonModel.isPopupShowing();
        boolean bl6 = this.commandButton.getActionModel().isArmed();
        boolean bl7 = popupButtonModel != null && popupButtonModel.isArmed();
        return bl || bl2 || bl3 || bl4 || bl5 || bl6 || bl7 || !this.commandButton.isFlat();
    }

    protected PopupActionListener createPopupActionListener() {
        return new PopupActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                BasicCommandButtonUI.this.processPopupAction();
            }
        };
    }

    protected void processPopupAction() {
        JPopupPanel jPopupPanel;
        boolean bl = false;
        if (this.commandButton instanceof JCommandButton) {
            bl = ((JCommandButton)this.commandButton).getPopupModel().isPopupShowing();
        }
        PopupPanelManager.defaultManager().hidePopups(this.commandButton);
        if (!(this.commandButton instanceof JCommandButton)) {
            return;
        }
        if (bl) {
            return;
        }
        JCommandButton jCommandButton = (JCommandButton)this.commandButton;
        PopupPanelCallback popupPanelCallback = jCommandButton.getPopupCallback();
        JPopupPanel jPopupPanel2 = jPopupPanel = popupPanelCallback != null ? popupPanelCallback.getPopupPanel(jCommandButton) : null;
        if (jPopupPanel != null) {
            jPopupPanel.applyComponentOrientation(jCommandButton.getComponentOrientation());
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    int n;
                    Rectangle rectangle;
                    Object object;
                    if (BasicCommandButtonUI.this.commandButton == null || jPopupPanel == null) {
                        return;
                    }
                    if (!BasicCommandButtonUI.this.commandButton.isShowing()) {
                        return;
                    }
                    jPopupPanel.doLayout();
                    int n2 = 0;
                    int n3 = 0;
                    JPopupPanel.PopupPanelCustomizer popupPanelCustomizer = jPopupPanel.getCustomizer();
                    boolean bl = BasicCommandButtonUI.this.commandButton.getComponentOrientation().isLeftToRight();
                    if (popupPanelCustomizer == null) {
                        switch (((JCommandButton)BasicCommandButtonUI.this.commandButton).getPopupOrientationKind()) {
                            case DOWNWARD: {
                                n2 = bl ? BasicCommandButtonUI.this.commandButton.getLocationOnScreen().x : BasicCommandButtonUI.this.commandButton.getLocationOnScreen().x + BasicCommandButtonUI.this.commandButton.getWidth() - jPopupPanel.getPreferredSize().width;
                                n3 = BasicCommandButtonUI.this.commandButton.getLocationOnScreen().y + BasicCommandButtonUI.this.commandButton.getSize().height;
                                break;
                            }
                            case SIDEWARD: {
                                n2 = bl ? BasicCommandButtonUI.this.commandButton.getLocationOnScreen().x + BasicCommandButtonUI.this.commandButton.getWidth() : BasicCommandButtonUI.this.commandButton.getLocationOnScreen().x - jPopupPanel.getPreferredSize().width;
                                n3 = BasicCommandButtonUI.this.commandButton.getLocationOnScreen().y + BasicCommandButtonUI.this.getLayoutInfo().popupClickArea.y;
                            }
                        }
                    } else {
                        rectangle = popupPanelCustomizer.getScreenBounds();
                        n2 = rectangle.x;
                        n3 = rectangle.y;
                    }
                    rectangle = BasicCommandButtonUI.this.commandButton.getGraphicsConfiguration().getBounds();
                    int n4 = jPopupPanel.getPreferredSize().width;
                    if (n2 + n4 > rectangle.x + rectangle.width) {
                        n2 = rectangle.x + rectangle.width - n4;
                    }
                    if (n3 + (n = jPopupPanel.getPreferredSize().height) > rectangle.y + rectangle.height) {
                        n3 = rectangle.y + rectangle.height - n;
                    }
                    if (popupPanelCustomizer != null) {
                        object = popupPanelCustomizer.getScreenBounds();
                        jPopupPanel.setPreferredSize(new Dimension(object.width, object.height));
                    }
                    object = BlankPopupFixPopupFactory.getPopup(jPopupPanel, n2, n3);
                    PopupPanelManager.defaultManager().addPopup(BasicCommandButtonUI.this.commandButton, (Popup)object, jPopupPanel);
                }
            });
            return;
        }
    }

    protected void syncDisabledIcon() {
        ResizableIcon resizableIcon = this.commandButton.getDisabledIcon();
        ResizableIcon resizableIcon2 = this.commandButton.getIcon();
        if (resizableIcon == null || resizableIcon instanceof UIResource) {
            if (resizableIcon2 != null) {
                this.commandButton.setDisabledIcon(new ResizableIconUIResource(new FilteredResizableIcon(resizableIcon2, new ColorConvertOp(ColorSpace.getInstance(1003), null))));
            } else {
                this.commandButton.setDisabledIcon(null);
            }
        } else if (resizableIcon2 != null) {
            this.commandButton.getDisabledIcon().setDimension(new Dimension(resizableIcon2.getIconWidth(), resizableIcon2.getIconHeight()));
        }
    }

    protected void syncIconDimension() {
        ResizableIcon resizableIcon = this.commandButton.getIcon();
        CommandButtonDisplayState commandButtonDisplayState = this.commandButton.getDisplayState();
        this.layoutManager = commandButtonDisplayState.createLayoutManager(this.commandButton);
        if (resizableIcon == null) {
            return;
        }
        int n = this.layoutManager.getPreferredIconSize();
        if (n < 0) {
            n = this.commandButton.getIcon().getIconHeight();
        }
        if (commandButtonDisplayState != CommandButtonDisplayState.FIT_TO_ICON) {
            Dimension dimension = new Dimension(n, n);
            resizableIcon.setDimension(dimension);
        }
    }

    @Override
    public Point getKeyTipAnchorCenterPoint() {
        return this.layoutManager.getKeyTipAnchorCenterPoint(this.commandButton);
    }

}

