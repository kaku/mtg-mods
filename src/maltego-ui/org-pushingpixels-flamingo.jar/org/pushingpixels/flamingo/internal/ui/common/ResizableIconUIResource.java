/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.plaf.UIResource;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

public class ResizableIconUIResource
implements ResizableIcon,
UIResource {
    private ResizableIcon delegate;

    public ResizableIconUIResource(ResizableIcon resizableIcon) {
        this.delegate = resizableIcon;
    }

    @Override
    public int getIconHeight() {
        return this.delegate.getIconHeight();
    }

    @Override
    public int getIconWidth() {
        return this.delegate.getIconWidth();
    }

    @Override
    public void paintIcon(Component component, Graphics graphics, int n, int n2) {
        this.delegate.paintIcon(component, graphics, n, n2);
    }

    @Override
    public void setDimension(Dimension dimension) {
        this.delegate.setDimension(dimension);
    }
}

