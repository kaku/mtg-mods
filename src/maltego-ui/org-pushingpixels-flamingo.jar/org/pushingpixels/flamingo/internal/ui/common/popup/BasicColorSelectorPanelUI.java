/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common.popup;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.internal.ui.common.popup.ColorSelectorPanelUI;
import org.pushingpixels.flamingo.internal.ui.common.popup.JColorSelectorPanel;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public class BasicColorSelectorPanelUI
extends ColorSelectorPanelUI {
    protected JColorSelectorPanel colorSelectorPanel;
    protected JLabel captionLabel;
    protected JPanel colorSelectorContainer;

    public static ComponentUI createUI(JComponent jComponent) {
        return new BasicColorSelectorPanelUI();
    }

    @Override
    public void installUI(JComponent jComponent) {
        this.colorSelectorPanel = (JColorSelectorPanel)jComponent;
        this.installDefaults();
        this.installComponents();
        this.installListeners();
    }

    @Override
    public void uninstallUI(JComponent jComponent) {
        this.uninstallListeners();
        this.uninstallComponents();
        this.uninstallDefaults();
        jComponent.setLayout(null);
        this.colorSelectorPanel = null;
    }

    protected void installListeners() {
    }

    protected void uninstallListeners() {
    }

    protected void installDefaults() {
    }

    protected void uninstallDefaults() {
    }

    protected void installComponents() {
        this.captionLabel = new JLabel(this.colorSelectorPanel.getCaption());
        this.captionLabel.setFont(this.captionLabel.getFont().deriveFont(1));
        this.colorSelectorContainer = this.colorSelectorPanel.getColorSelectionContainer();
        this.colorSelectorPanel.add(this.captionLabel);
        if (this.colorSelectorContainer != null) {
            this.colorSelectorPanel.add(this.colorSelectorContainer);
        }
        this.colorSelectorPanel.setLayout(new PanelLayout());
    }

    protected void uninstallComponents() {
        this.colorSelectorPanel.remove(this.captionLabel);
        if (this.colorSelectorContainer != null) {
            this.colorSelectorPanel.remove(this.colorSelectorContainer);
        }
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        Color color = this.colorSelectorPanel.getBackground();
        graphics.setColor(color);
        int n = jComponent.getWidth();
        int n2 = jComponent.getHeight();
        graphics.fillRect(0, 0, n, n2);
        Rectangle rectangle = this.captionLabel.getBounds();
        this.paintCaptionBackground(graphics, 0, 0, n, rectangle.height + 2 * this.getLayoutGap());
        if (this.colorSelectorPanel.isLastPanel()) {
            this.paintBottomDivider(graphics, 0, 0, n, n2);
        }
    }

    protected void paintBottomDivider(Graphics graphics, int n, int n2, int n3, int n4) {
        graphics.setColor(FlamingoUtilities.getBorderColor());
        graphics.drawLine(n, n2 + n4 - 1, n + n3 - 1, n2 + n4 - 1);
    }

    protected void paintCaptionBackground(Graphics graphics, int n, int n2, int n3, int n4) {
        FlamingoUtilities.renderSurface(graphics, this.colorSelectorPanel, new Rectangle(n, n2, n3, n4), false, true, true);
    }

    protected int getLayoutGap() {
        return 4;
    }

    protected class PanelLayout
    implements LayoutManager {
        protected PanelLayout() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            return new Dimension(20, 20);
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            int n = BasicColorSelectorPanelUI.this.getLayoutGap();
            Dimension dimension = BasicColorSelectorPanelUI.this.captionLabel.getPreferredSize();
            Dimension dimension2 = BasicColorSelectorPanelUI.this.colorSelectorContainer.getPreferredSize();
            return new Dimension(Math.max(dimension.width, dimension2.width), 2 * n + dimension.height + dimension2.height + (BasicColorSelectorPanelUI.this.colorSelectorPanel.isLastPanel() ? 1 : 0));
        }

        @Override
        public void layoutContainer(Container container) {
            int n = BasicColorSelectorPanelUI.this.getLayoutGap();
            Dimension dimension = BasicColorSelectorPanelUI.this.captionLabel.getPreferredSize();
            int n2 = dimension.width;
            int n3 = dimension.height;
            int n4 = n;
            if (BasicColorSelectorPanelUI.this.captionLabel.getComponentOrientation().isLeftToRight()) {
                BasicColorSelectorPanelUI.this.captionLabel.setBounds(n, n4, n2, n3);
            } else {
                BasicColorSelectorPanelUI.this.captionLabel.setBounds(container.getWidth() - n - n2, n4, n2, n3);
            }
            BasicColorSelectorPanelUI.this.colorSelectorContainer.setBounds(0, n4, container.getWidth(), container.getHeight() - (n4 += n3 + n) - (BasicColorSelectorPanelUI.this.colorSelectorPanel.isLastPanel() ? 1 : 0));
        }
    }

}

