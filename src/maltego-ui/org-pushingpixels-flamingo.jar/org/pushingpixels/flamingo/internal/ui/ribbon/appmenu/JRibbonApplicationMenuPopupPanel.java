/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon.appmenu;

import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu;
import org.pushingpixels.flamingo.internal.ui.common.popup.BasicPopupPanelUI;
import org.pushingpixels.flamingo.internal.ui.common.popup.PopupPanelUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.BasicRibbonApplicationMenuPopupPanelUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuButton;

public class JRibbonApplicationMenuPopupPanel
extends JPopupPanel {
    public static final String uiClassID = "RibbonApplicationMenuPopupPanelUI";
    protected JRibbonApplicationMenuButton appMenuButton;
    protected RibbonApplicationMenu ribbonAppMenu;

    public JRibbonApplicationMenuPopupPanel(JRibbonApplicationMenuButton jRibbonApplicationMenuButton, RibbonApplicationMenu ribbonApplicationMenu) {
        this.appMenuButton = jRibbonApplicationMenuButton;
        this.ribbonAppMenu = ribbonApplicationMenu;
        this.updateUI();
    }

    public JPanel getPanelLevel1() {
        return ((BasicRibbonApplicationMenuPopupPanelUI)this.getUI()).getPanelLevel1();
    }

    public JPanel getPanelLevel2() {
        return ((BasicRibbonApplicationMenuPopupPanelUI)this.getUI()).getPanelLevel2();
    }

    @Override
    public void updateUI() {
        if (UIManager.get(this.getUIClassID()) != null) {
            this.setUI((BasicPopupPanelUI)UIManager.getUI(this));
        } else {
            this.setUI(BasicRibbonApplicationMenuPopupPanelUI.createUI(this));
        }
    }

    @Override
    public String getUIClassID() {
        return "RibbonApplicationMenuPopupPanelUI";
    }

    public JRibbonApplicationMenuButton getAppMenuButton() {
        return this.appMenuButton;
    }

    public RibbonApplicationMenu getRibbonAppMenu() {
        return this.ribbonAppMenu;
    }
}

