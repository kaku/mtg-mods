/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.CellRendererPane;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicGraphicsUtils;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelManager;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;
import org.pushingpixels.flamingo.api.ribbon.RibbonTask;
import org.pushingpixels.flamingo.internal.ui.common.BasicCommandToggleButtonUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.JRibbonTaskToggleButton;
import org.pushingpixels.flamingo.internal.utils.ColorShiftFilter;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;
import org.pushingpixels.flamingo.internal.utils.RenderingUtils;

public class BasicRibbonTaskToggleButtonUI
extends BasicCommandToggleButtonUI {
    protected PopupPanelManager.PopupListener popupListener;

    public static ComponentUI createUI(JComponent jComponent) {
        return new BasicRibbonTaskToggleButtonUI();
    }

    @Override
    protected void installDefaults() {
        Border border;
        super.installDefaults();
        Font font = this.commandButton.getFont();
        if (font == null || font instanceof UIResource) {
            this.commandButton.setFont(FlamingoUtilities.getFont(null, "Ribbon.font", "Button.font", "Panel.font"));
        }
        if ((border = this.commandButton.getBorder()) == null || border instanceof UIResource) {
            Border border2 = UIManager.getBorder("RibbonTaskToggleButton.border");
            if (border2 == null) {
                border2 = new BorderUIResource.EmptyBorderUIResource(1, 12, 1, 12);
            }
            this.commandButton.setBorder(border2);
        }
        this.commandButton.setFlat(true);
        this.commandButton.setOpaque(false);
    }

    @Override
    protected void installListeners() {
        super.installListeners();
        this.popupListener = new PopupPanelManager.PopupListener(){

            @Override
            public void popupShown(PopupPanelManager.PopupEvent popupEvent) {
                if (popupEvent.getSource() == BasicRibbonTaskToggleButtonUI.this.commandButton) {
                    BasicRibbonTaskToggleButtonUI.this.commandButton.getActionModel().setSelected(this.isTaskSelected());
                }
            }

            @Override
            public void popupHidden(PopupPanelManager.PopupEvent popupEvent) {
                if (popupEvent.getSource() == BasicRibbonTaskToggleButtonUI.this.commandButton) {
                    BasicRibbonTaskToggleButtonUI.this.commandButton.getActionModel().setSelected(this.isTaskSelected());
                }
            }

            private boolean isTaskSelected() {
                JRibbon jRibbon = (JRibbon)SwingUtilities.getAncestorOfClass(JRibbon.class, BasicRibbonTaskToggleButtonUI.this.commandButton);
                if (jRibbon == null) {
                    return false;
                }
                return jRibbon.getSelectedTask() == ((JRibbonTaskToggleButton)BasicRibbonTaskToggleButtonUI.this.commandButton).getRibbonTask();
            }
        };
        PopupPanelManager.defaultManager().addPopupListener(this.popupListener);
    }

    @Override
    protected void uninstallListeners() {
        PopupPanelManager.defaultManager().removePopupListener(this.popupListener);
        this.popupListener = null;
        super.uninstallListeners();
    }

    @Override
    public void update(Graphics graphics, JComponent jComponent) {
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        RenderingUtils.installDesktopHints(graphics2D);
        RenderingUtils.setupTextAntialiasing(graphics2D, jComponent);
        this.paintButtonBackground(graphics2D, new Rectangle(0, 0, jComponent.getWidth(), jComponent.getHeight() + 10));
        this.paintText(graphics2D);
        graphics2D.dispose();
    }

    protected void paintText(Graphics graphics) {
        int n;
        FontMetrics fontMetrics = graphics.getFontMetrics();
        String string = this.commandButton.getText();
        int n2 = this.commandButton.getWidth();
        int n3 = this.commandButton.getHeight();
        int n4 = 0;
        Rectangle rectangle = new Rectangle(n4, 1 + (n3 - fontMetrics.getHeight()) / 2, n2 - 2 * n4, fontMetrics.getHeight());
        int n5 = rectangle.x;
        int n6 = fontMetrics.stringWidth(string);
        int n7 = n5 = (n5 += (rectangle.width - n6) / 2) > rectangle.x ? n5 : rectangle.x;
        while (string.length() != 0 && (n = fontMetrics.stringWidth(string)) > rectangle.width) {
            string = string.substring(0, string.length() - 1);
        }
        BasicGraphicsUtils.drawString(graphics, string, -1, n5, rectangle.y + fontMetrics.getAscent());
    }

    @Override
    protected void paintButtonBackground(Graphics graphics, Rectangle rectangle) {
        JRibbon jRibbon = (JRibbon)SwingUtilities.getAncestorOfClass(JRibbon.class, this.commandButton);
        this.buttonRendererPane.setBounds(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        ButtonModel buttonModel = this.rendererButton.getModel();
        buttonModel.setEnabled(this.commandButton.isEnabled());
        buttonModel.setSelected(false);
        boolean bl = this.commandButton.getActionModel().isSelected();
        buttonModel.setRollover(bl || this.commandButton.getActionModel().isRollover());
        buttonModel.setPressed(false);
        if (buttonModel.isRollover()) {
            boolean bl2;
            Graphics2D graphics2D = (Graphics2D)graphics.create();
            if (!this.commandButton.getActionModel().isSelected()) {
                graphics2D.setComposite(AlphaComposite.SrcOver.derive(0.4f));
            }
            graphics2D.translate(rectangle.x, rectangle.y);
            Color color = ((JRibbonTaskToggleButton)this.commandButton).getContextualGroupHueColor();
            boolean bl3 = bl2 = color != null;
            if (!bl2) {
                Shape shape = graphics2D.getClip();
                graphics2D.clip(FlamingoUtilities.getRibbonTaskToggleButtonOutline(rectangle.width, rectangle.height, 2.0f));
                this.buttonRendererPane.paintComponent(graphics2D, this.rendererButton, this.commandButton, rectangle.x - rectangle.width / 2, rectangle.y - rectangle.height / 2, 2 * rectangle.width, 2 * rectangle.height, true);
                graphics2D.setColor(FlamingoUtilities.getBorderColor().darker());
                graphics2D.setClip(shape);
                graphics2D.draw(FlamingoUtilities.getRibbonTaskToggleButtonOutline(rectangle.width, rectangle.height + 1, 2.0f));
            } else {
                BufferedImage bufferedImage = FlamingoUtilities.getBlankImage(rectangle.width, rectangle.height);
                Graphics2D graphics2D2 = bufferedImage.createGraphics();
                Shape shape = graphics2D.getClip();
                graphics2D2.clip(FlamingoUtilities.getRibbonTaskToggleButtonOutline(rectangle.width, rectangle.height, 2.0f));
                this.buttonRendererPane.paintComponent(graphics2D2, this.rendererButton, this.commandButton, rectangle.x - rectangle.width / 2, rectangle.y - rectangle.height / 2, 2 * rectangle.width, 2 * rectangle.height, true);
                graphics2D2.setColor(FlamingoUtilities.getBorderColor().darker());
                graphics2D2.setClip(shape);
                graphics2D2.draw(FlamingoUtilities.getRibbonTaskToggleButtonOutline(rectangle.width, rectangle.height + 1, 2.0f));
                graphics2D2.dispose();
                ColorShiftFilter colorShiftFilter = new ColorShiftFilter(color, 0.25);
                BufferedImage bufferedImage2 = colorShiftFilter.filter(bufferedImage, null);
                graphics2D.drawImage(bufferedImage2, 0, 0, null);
            }
            graphics2D.dispose();
        }
    }

    @Override
    public Dimension getPreferredSize(JComponent jComponent) {
        JRibbonTaskToggleButton jRibbonTaskToggleButton = (JRibbonTaskToggleButton)jComponent;
        ResizableIcon resizableIcon = jRibbonTaskToggleButton.getIcon();
        String string = jRibbonTaskToggleButton.getText();
        Font font = jRibbonTaskToggleButton.getFont();
        FontMetrics fontMetrics = jRibbonTaskToggleButton.getFontMetrics(font);
        Rectangle rectangle = new Rectangle();
        Rectangle rectangle2 = new Rectangle();
        Rectangle rectangle3 = new Rectangle(32767, 32767);
        SwingUtilities.layoutCompoundLabel(jRibbonTaskToggleButton, fontMetrics, string, resizableIcon, 0, jRibbonTaskToggleButton.getHorizontalAlignment(), 0, 0, rectangle3, rectangle, rectangle2, string == null ? 0 : 6);
        Rectangle rectangle4 = rectangle.union(rectangle2);
        Insets insets = jRibbonTaskToggleButton.getInsets();
        rectangle4.width += insets.left + insets.right;
        rectangle4.height += insets.top + insets.bottom;
        return rectangle4.getSize();
    }

    @Override
    public Dimension getMinimumSize(JComponent jComponent) {
        JRibbonTaskToggleButton jRibbonTaskToggleButton = (JRibbonTaskToggleButton)jComponent;
        ResizableIcon resizableIcon = jRibbonTaskToggleButton.getIcon();
        String string = "Www";
        Font font = jRibbonTaskToggleButton.getFont();
        FontMetrics fontMetrics = jRibbonTaskToggleButton.getFontMetrics(font);
        Rectangle rectangle = new Rectangle();
        Rectangle rectangle2 = new Rectangle();
        Rectangle rectangle3 = new Rectangle(32767, 32767);
        SwingUtilities.layoutCompoundLabel(jRibbonTaskToggleButton, fontMetrics, string, resizableIcon, 0, jRibbonTaskToggleButton.getHorizontalAlignment(), 0, 0, rectangle3, rectangle, rectangle2, string == null ? 0 : 6);
        Rectangle rectangle4 = rectangle.union(rectangle2);
        Insets insets = jRibbonTaskToggleButton.getInsets();
        rectangle4.width += 4;
        rectangle4.height += insets.top + insets.bottom;
        return rectangle4.getSize();
    }

}

