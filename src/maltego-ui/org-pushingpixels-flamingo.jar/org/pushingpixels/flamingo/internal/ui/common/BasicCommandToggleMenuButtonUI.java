/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.GeneralPath;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;
import org.pushingpixels.flamingo.internal.ui.common.BasicCommandToggleButtonUI;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public class BasicCommandToggleMenuButtonUI
extends BasicCommandToggleButtonUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new BasicCommandToggleMenuButtonUI();
    }

    @Override
    protected void paintButtonIcon(Graphics graphics, Rectangle rectangle) {
        Object object;
        Object object2;
        boolean bl = this.commandButton.getActionModel().isSelected();
        if (bl) {
            object = FlamingoUtilities.getColor(Color.blue.darker(), "Table.selectionBackground", "textHighlight");
            object2 = new Rectangle(rectangle.x - 1, rectangle.y - 1, rectangle.width + 1, rectangle.height + 1);
            graphics.setColor((Color)object);
            graphics.fillRect(object2.x, object2.y, object2.width, object2.height);
            graphics.setColor(object.darker());
            graphics.drawRect(object2.x, object2.y, object2.width, object2.height);
        }
        super.paintButtonIcon(graphics, rectangle);
        object = this.getIconToPaint();
        if (bl && object == null) {
            object2 = (Graphics2D)graphics.create();
            object2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            object2.setColor(this.getForegroundColor(this.commandButton.getActionModel().isEnabled()));
            int n = rectangle.width;
            int n2 = rectangle.height;
            GeneralPath generalPath = new GeneralPath();
            generalPath.moveTo(0.2f * (float)n, 0.5f * (float)n2);
            generalPath.lineTo(0.42f * (float)n, 0.8f * (float)n2);
            generalPath.lineTo(0.8f * (float)n, 0.2f * (float)n2);
            object2.translate(rectangle.x, rectangle.y);
            BasicStroke basicStroke = new BasicStroke(0.1f * (float)n, 1, 1);
            object2.setStroke(basicStroke);
            object2.draw(generalPath);
            object2.dispose();
        }
    }

    @Override
    protected boolean isPaintingBackground() {
        boolean bl = this.commandButton.getActionModel().isRollover();
        return bl || !this.commandButton.isFlat();
    }
}

