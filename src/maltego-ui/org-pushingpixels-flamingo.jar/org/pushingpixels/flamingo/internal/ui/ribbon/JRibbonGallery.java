/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.CommandToggleButtonGroup;
import org.pushingpixels.flamingo.api.common.JCommandButtonPanel;
import org.pushingpixels.flamingo.api.common.JCommandToggleButton;
import org.pushingpixels.flamingo.api.common.StringValuePair;
import org.pushingpixels.flamingo.api.ribbon.JRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.RibbonElementPriority;
import org.pushingpixels.flamingo.internal.ui.ribbon.BasicRibbonGalleryUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.RibbonGalleryUI;

public class JRibbonGallery
extends JComponent {
    protected List<JCommandToggleButton> buttons = new ArrayList<JCommandToggleButton>();
    protected CommandToggleButtonGroup buttonSelectionGroup = new CommandToggleButtonGroup();
    protected RibbonElementPriority displayPriority;
    protected Map<RibbonElementPriority, Integer> preferredVisibleIconCount;
    protected List<StringValuePair<List<JCommandToggleButton>>> buttonGroups;
    protected int preferredPopupMaxButtonColumns;
    protected int preferredPopupMaxVisibleButtonRows;
    protected boolean isShowingPopupPanel;
    protected JRibbonBand.RibbonGalleryPopupCallback popupCallback;
    public static final String uiClassID = "RibbonGalleryUI";
    protected ActionListener dismissActionListener;
    private String expandKeyTip;
    private CommandButtonDisplayState buttonDisplayState;

    public JRibbonGallery() {
        this.buttonSelectionGroup.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(final PropertyChangeEvent propertyChangeEvent) {
                if ("selected".equals(propertyChangeEvent.getPropertyName())) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            JRibbonGallery.this.firePropertyChange("selectedButton", propertyChangeEvent.getOldValue(), propertyChangeEvent.getNewValue());
                        }
                    });
                }
            }

        });
        this.preferredVisibleIconCount = new HashMap<RibbonElementPriority, Integer>();
        for (RibbonElementPriority ribbonElementPriority : RibbonElementPriority.values()) {
            this.preferredVisibleIconCount.put(ribbonElementPriority, 100);
        }
        this.isShowingPopupPanel = false;
        this.buttonDisplayState = JRibbonBand.BIG_FIXED_LANDSCAPE;
        this.updateUI();
    }

    public void setUI(RibbonGalleryUI ribbonGalleryUI) {
        super.setUI(ribbonGalleryUI);
    }

    @Override
    public void updateUI() {
        if (UIManager.get(this.getUIClassID()) != null) {
            this.setUI((RibbonGalleryUI)UIManager.getUI(this));
        } else {
            this.setUI(new BasicRibbonGalleryUI());
        }
    }

    public RibbonGalleryUI getUI() {
        return (RibbonGalleryUI)this.ui;
    }

    @Override
    public String getUIClassID() {
        return "RibbonGalleryUI";
    }

    private void addGalleryButton(StringValuePair<List<JCommandToggleButton>> stringValuePair, JCommandToggleButton jCommandToggleButton) {
        String string = (String)stringValuePair.getKey();
        int n = 0;
        for (int i = 0; i < this.buttonGroups.size(); ++i) {
            StringValuePair<List<JCommandToggleButton>> stringValuePair2 = this.buttonGroups.get(i);
            String string2 = (String)stringValuePair2.getKey();
            n += ((List)stringValuePair2.getValue()).size();
            if (string2 == null && string == null || string2.compareTo(string) == 0) break;
        }
        this.buttons.add(n, jCommandToggleButton);
        this.buttonSelectionGroup.add(jCommandToggleButton);
        ((List)stringValuePair.getValue()).add(jCommandToggleButton);
        jCommandToggleButton.setDisplayState(this.buttonDisplayState);
        jCommandToggleButton.addActionListener(this.dismissActionListener);
        super.add(jCommandToggleButton);
    }

    private void removeGalleryButton(JCommandToggleButton jCommandToggleButton) {
        this.buttons.remove(jCommandToggleButton);
        this.buttonSelectionGroup.remove(jCommandToggleButton);
        jCommandToggleButton.removeActionListener(this.dismissActionListener);
        super.remove(jCommandToggleButton);
    }

    public void setPreferredVisibleButtonCount(RibbonElementPriority ribbonElementPriority, int n) {
        this.preferredVisibleIconCount.put(ribbonElementPriority, n);
    }

    public int getPreferredWidth(RibbonElementPriority ribbonElementPriority, int n) {
        int n2 = this.preferredVisibleIconCount.get((Object)ribbonElementPriority);
        BasicRibbonGalleryUI basicRibbonGalleryUI = (BasicRibbonGalleryUI)this.getUI();
        return basicRibbonGalleryUI.getPreferredWidth(n2, n);
    }

    public void setDisplayPriority(RibbonElementPriority ribbonElementPriority) {
        this.displayPriority = ribbonElementPriority;
    }

    public RibbonElementPriority getDisplayPriority() {
        return this.displayPriority;
    }

    public int getButtonGroupCount() {
        return this.buttonGroups.size();
    }

    public List<JCommandToggleButton> getButtonGroup(String string) {
        for (StringValuePair<List<JCommandToggleButton>> stringValuePair : this.buttonGroups) {
            if (((String)stringValuePair.getKey()).compareTo(string) != 0) continue;
            return (List)stringValuePair.getValue();
        }
        return null;
    }

    public int getButtonCount() {
        return this.buttons.size();
    }

    public JCommandToggleButton getButtonAt(int n) {
        return this.buttons.get(n);
    }

    public JCommandToggleButton getSelectedButton() {
        return this.buttonSelectionGroup.getSelected();
    }

    public void setSelectedButton(JCommandToggleButton jCommandToggleButton) {
        this.buttonSelectionGroup.setSelected(jCommandToggleButton, true);
    }

    public JCommandButtonPanel getPopupButtonPanel() {
        JCommandButtonPanel jCommandButtonPanel = new JCommandButtonPanel(this.buttonDisplayState);
        jCommandButtonPanel.setMaxButtonColumns(this.preferredPopupMaxButtonColumns);
        jCommandButtonPanel.setToShowGroupLabels(true);
        for (StringValuePair<List<JCommandToggleButton>> stringValuePair : this.buttonGroups) {
            String string = (String)stringValuePair.getKey();
            if (string == null) {
                jCommandButtonPanel.setToShowGroupLabels(false);
            }
            jCommandButtonPanel.addButtonGroup(string);
            for (JCommandToggleButton jCommandToggleButton : (List)stringValuePair.getValue()) {
                jCommandToggleButton.setVisible(true);
                jCommandButtonPanel.addButtonToLastGroup(jCommandToggleButton);
            }
        }
        jCommandButtonPanel.setSingleSelectionMode(true);
        return jCommandButtonPanel;
    }

    public void setShowingPopupPanel(boolean bl) {
        this.isShowingPopupPanel = bl;
        if (!bl) {
            for (StringValuePair<List<JCommandToggleButton>> stringValuePair : this.buttonGroups) {
                for (JCommandToggleButton jCommandToggleButton : (List)stringValuePair.getValue()) {
                    jCommandToggleButton.setDisplayState(this.buttonDisplayState);
                    this.add(jCommandToggleButton);
                }
            }
            this.doLayout();
        }
    }

    public boolean isShowingPopupPanel() {
        return this.isShowingPopupPanel;
    }

    public void setGroupMapping(List<StringValuePair<List<JCommandToggleButton>>> list) {
        this.buttonGroups = new ArrayList<StringValuePair<List<JCommandToggleButton>>>();
        boolean bl = false;
        for (StringValuePair<List<JCommandToggleButton>> stringValuePair : list) {
            if (stringValuePair.getKey() == null) {
                if (bl) {
                    throw new IllegalArgumentException("Can't have more than one ribbon gallery group with null name");
                }
                bl = true;
            }
            ArrayList arrayList = new ArrayList();
            StringValuePair<List<JCommandToggleButton>> stringValuePair2 = new StringValuePair<List<JCommandToggleButton>>((String)stringValuePair.getKey(), arrayList);
            this.buttonGroups.add(stringValuePair2);
            for (JCommandToggleButton jCommandToggleButton : (List)stringValuePair.getValue()) {
                this.addGalleryButton(stringValuePair2, jCommandToggleButton);
            }
        }
    }

    public /* varargs */ void addRibbonGalleryButtons(String string, JCommandToggleButton ... arrjCommandToggleButton) {
        for (StringValuePair<List<JCommandToggleButton>> stringValuePair : this.buttonGroups) {
            if (((String)stringValuePair.getKey()).compareTo(string) != 0) continue;
            for (JCommandToggleButton jCommandToggleButton : arrjCommandToggleButton) {
                this.addGalleryButton(stringValuePair, jCommandToggleButton);
            }
            return;
        }
        this.revalidate();
        this.doLayout();
    }

    public /* varargs */ void removeRibbonGalleryButtons(JCommandToggleButton ... arrjCommandToggleButton) {
        for (StringValuePair<List<JCommandToggleButton>> stringValuePair : this.buttonGroups) {
            Iterator iterator = ((List)stringValuePair.getValue()).iterator();
            while (iterator.hasNext()) {
                JCommandToggleButton jCommandToggleButton = (JCommandToggleButton)iterator.next();
                for (JCommandToggleButton jCommandToggleButton2 : arrjCommandToggleButton) {
                    if (jCommandToggleButton2 != jCommandToggleButton) continue;
                    iterator.remove();
                    this.removeGalleryButton(jCommandToggleButton2);
                }
            }
        }
        this.revalidate();
        this.doLayout();
    }

    public void setPreferredPopupPanelDimension(int n, int n2) {
        this.preferredPopupMaxButtonColumns = n;
        this.preferredPopupMaxVisibleButtonRows = n2;
    }

    public void setPopupCallback(JRibbonBand.RibbonGalleryPopupCallback ribbonGalleryPopupCallback) {
        this.popupCallback = ribbonGalleryPopupCallback;
    }

    public JRibbonBand.RibbonGalleryPopupCallback getPopupCallback() {
        return this.popupCallback;
    }

    public int getPreferredPopupMaxButtonColumns() {
        return this.preferredPopupMaxButtonColumns;
    }

    public int getPreferredPopupMaxVisibleButtonRows() {
        return this.preferredPopupMaxVisibleButtonRows;
    }

    public void setExpandKeyTip(String string) {
        String string2 = this.expandKeyTip;
        this.expandKeyTip = string;
        this.firePropertyChange("expandKeyTip", string2, this.expandKeyTip);
    }

    public String getExpandKeyTip() {
        return this.expandKeyTip;
    }

    public CommandButtonDisplayState getButtonDisplayState() {
        return this.buttonDisplayState;
    }

    public void setButtonDisplayState(CommandButtonDisplayState commandButtonDisplayState) {
        boolean bl;
        if (this.getButtonCount() > 0) {
            throw new IllegalStateException("Cannot change button display state on ribbon gallery with existing buttons");
        }
        boolean bl2 = bl = commandButtonDisplayState == JRibbonBand.BIG_FIXED || commandButtonDisplayState == CommandButtonDisplayState.SMALL || commandButtonDisplayState == JRibbonBand.BIG_FIXED_LANDSCAPE;
        if (!bl) {
            throw new IllegalArgumentException("Display state " + commandButtonDisplayState.getDisplayName() + " is not supported in ribbon galleries");
        }
        if (!commandButtonDisplayState.equals(this.buttonDisplayState)) {
            CommandButtonDisplayState commandButtonDisplayState2 = this.buttonDisplayState;
            this.buttonDisplayState = commandButtonDisplayState;
            for (JCommandToggleButton jCommandToggleButton : this.buttons) {
                jCommandToggleButton.setDisplayState(commandButtonDisplayState);
            }
            this.firePropertyChange("buttonDisplayState", commandButtonDisplayState2, this.buttonDisplayState);
        }
    }

}

