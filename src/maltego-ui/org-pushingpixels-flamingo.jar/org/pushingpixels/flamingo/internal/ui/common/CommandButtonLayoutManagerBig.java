/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common;

import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public class CommandButtonLayoutManagerBig
implements CommandButtonLayoutManager {
    protected AbstractCommandButton commandButton;
    protected String titlePart1;
    protected String titlePart2;

    public CommandButtonLayoutManagerBig(AbstractCommandButton abstractCommandButton) {
        this.commandButton = abstractCommandButton;
        this.updateTitleStrings();
    }

    @Override
    public int getPreferredIconSize() {
        return 32;
    }

    @Override
    public Dimension getPreferredSize(AbstractCommandButton abstractCommandButton) {
        Insets insets = abstractCommandButton == null ? new Insets(0, 0, 0, 0) : abstractCommandButton.getInsets();
        int n = insets.left + insets.right;
        FontMetrics fontMetrics = abstractCommandButton.getFontMetrics(abstractCommandButton.getFont());
        int n2 = FlamingoUtilities.getHLayoutGap(abstractCommandButton);
        int n3 = FlamingoUtilities.getVLayoutGap(abstractCommandButton);
        boolean bl = abstractCommandButton.getIcon() != null;
        boolean bl2 = this.titlePart1 != null;
        boolean bl3 = FlamingoUtilities.hasPopupAction(abstractCommandButton);
        int n4 = this.titlePart1 == null ? 0 : fontMetrics.stringWidth(this.titlePart1);
        int n5 = this.titlePart2 == null ? 0 : fontMetrics.stringWidth(this.titlePart2);
        int n6 = bl ? this.getPreferredIconSize() : 0;
        int n7 = Math.max(n6, Math.max(n4, n5 + 4 * n2 + 2 + (FlamingoUtilities.hasPopupAction(abstractCommandButton) ? 1 + fontMetrics.getHeight() / 2 : 0)));
        int n8 = insets.top;
        if (bl) {
            n8 += n3;
            n8 += n6;
            n8 += n3;
        }
        if (bl2) {
            n8 += n3;
            n8 += 2 * (fontMetrics.getAscent() + fontMetrics.getDescent());
            n8 += n3;
        }
        if (!bl2 && bl3) {
            n8 += n3;
            n8 += fontMetrics.getHeight();
            n8 += n3;
        }
        if (abstractCommandButton instanceof JCommandButton) {
            JCommandButton jCommandButton = (JCommandButton)abstractCommandButton;
            JCommandButton.CommandButtonKind commandButtonKind = jCommandButton.getCommandButtonKind();
            if (bl && commandButtonKind.hasAction() && commandButtonKind.hasPopup()) {
                n8 += 2;
            }
        }
        n8 += insets.bottom;
        return new Dimension(n + n7, n8 -= 2 * n3);
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if ("text".equals(propertyChangeEvent.getPropertyName()) || "font".equals(propertyChangeEvent.getPropertyName())) {
            this.updateTitleStrings();
        }
    }

    protected void updateTitleStrings() {
        String string;
        BufferedImage bufferedImage = new BufferedImage(30, 30, 2);
        Graphics2D graphics2D = (Graphics2D)bufferedImage.getGraphics();
        graphics2D.setFont(FlamingoUtilities.getFont(this.commandButton, "Ribbon.font", "Button.font", "Panel.font"));
        FontMetrics fontMetrics = graphics2D.getFontMetrics();
        String string2 = string = this.commandButton == null ? null : this.commandButton.getText();
        if (string != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(string, " _-", true);
            if (stringTokenizer.countTokens() <= 1) {
                this.titlePart1 = string;
                this.titlePart2 = null;
            } else {
                int n = (int)fontMetrics.getStringBounds(this.commandButton.getText(), graphics2D).getWidth();
                int n2 = FlamingoUtilities.hasPopupAction(this.commandButton) ? 0 : 2 * FlamingoUtilities.getHLayoutGap(this.commandButton) + (fontMetrics.getAscent() + fontMetrics.getDescent()) / 2;
                String string3 = "";
                while (stringTokenizer.hasMoreTokens()) {
                    int n3;
                    String string4 = string3 = string3 + stringTokenizer.nextToken();
                    String string5 = string.substring(string3.length());
                    int n4 = (int)fontMetrics.getStringBounds(string4, graphics2D).getWidth();
                    int n5 = Math.max(n4, n3 = (int)fontMetrics.getStringBounds(string5, graphics2D).getWidth() + n2);
                    if (n <= n5) continue;
                    n = n5;
                    this.titlePart1 = string4;
                    this.titlePart2 = string5;
                }
            }
        } else {
            this.titlePart1 = null;
            this.titlePart2 = null;
        }
    }

    @Override
    public Point getKeyTipAnchorCenterPoint(AbstractCommandButton abstractCommandButton) {
        return new Point(abstractCommandButton.getWidth() / 2, abstractCommandButton.getHeight());
    }

    @Override
    public CommandButtonLayoutManager.CommandButtonLayoutInfo getLayoutInfo(AbstractCommandButton abstractCommandButton, Graphics graphics) {
        int n;
        int n2;
        CommandButtonLayoutManager.CommandButtonLayoutInfo commandButtonLayoutInfo = new CommandButtonLayoutManager.CommandButtonLayoutInfo();
        commandButtonLayoutInfo.actionClickArea = new Rectangle(0, 0, 0, 0);
        commandButtonLayoutInfo.popupClickArea = new Rectangle(0, 0, 0, 0);
        Insets insets = abstractCommandButton.getInsets();
        commandButtonLayoutInfo.iconRect = new Rectangle();
        commandButtonLayoutInfo.popupActionRect = new Rectangle();
        int n3 = abstractCommandButton.getWidth();
        int n4 = abstractCommandButton.getHeight();
        int n5 = insets.left;
        boolean bl = abstractCommandButton.getComponentOrientation().isLeftToRight();
        FontMetrics fontMetrics = graphics.getFontMetrics();
        int n6 = fontMetrics.getAscent() + fontMetrics.getDescent();
        JCommandButton.CommandButtonKind commandButtonKind = abstractCommandButton instanceof JCommandButton ? ((JCommandButton)abstractCommandButton).getCommandButtonKind() : JCommandButton.CommandButtonKind.ACTION_ONLY;
        ResizableIcon resizableIcon = abstractCommandButton.getIcon();
        boolean bl2 = abstractCommandButton.getIcon() != null;
        boolean bl3 = this.titlePart1 != null;
        boolean bl4 = FlamingoUtilities.hasPopupAction(abstractCommandButton);
        int n7 = FlamingoUtilities.getHLayoutGap(abstractCommandButton);
        int n8 = FlamingoUtilities.getVLayoutGap(abstractCommandButton);
        int n9 = this.getPreferredSize((AbstractCommandButton)abstractCommandButton).height;
        int n10 = 0;
        if (n4 > n9) {
            n10 = (n4 - n9) / 2;
        }
        int n11 = insets.top + n10 - n8;
        if (bl2) {
            n = resizableIcon.getIconHeight();
            n2 = resizableIcon.getIconWidth();
            commandButtonLayoutInfo.iconRect.x = (n3 - n2) / 2;
            commandButtonLayoutInfo.iconRect.y = n11 += n8;
            commandButtonLayoutInfo.iconRect.width = n2;
            commandButtonLayoutInfo.iconRect.height = n;
            n11 += n + n8;
        }
        if (abstractCommandButton instanceof JCommandButton && bl2 && commandButtonKind.hasAction() && commandButtonKind.hasPopup()) {
            commandButtonLayoutInfo.separatorOrientation = CommandButtonLayoutManager.CommandButtonSeparatorOrientation.HORIZONTAL;
            commandButtonLayoutInfo.separatorArea = new Rectangle(0, 0, 0, 0);
            commandButtonLayoutInfo.separatorArea.x = 0;
            commandButtonLayoutInfo.separatorArea.y = n11;
            commandButtonLayoutInfo.separatorArea.width = n3;
            commandButtonLayoutInfo.separatorArea.height = 2;
            n11 += commandButtonLayoutInfo.separatorArea.height;
        }
        n = 0;
        if (bl3) {
            int n12;
            n = this.titlePart1 != null ? (int)fontMetrics.getStringBounds(this.titlePart1, graphics).getWidth() : 0;
            CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo = new CommandButtonLayoutManager.TextLayoutInfo();
            textLayoutInfo.text = this.titlePart1;
            textLayoutInfo.textRect = new Rectangle();
            textLayoutInfo.textRect.x = insets.left + (n3 - n - insets.left - insets.right) / 2;
            textLayoutInfo.textRect.y = n11 += n8;
            textLayoutInfo.textRect.width = n;
            textLayoutInfo.textRect.height = n6;
            if (this.titlePart1 != null) {
                n11 += n6;
            }
            n = this.titlePart2 != null ? (int)fontMetrics.getStringBounds(this.titlePart2, graphics).getWidth() : 0;
            int n13 = n12 = bl4 ? 4 * n7 + n6 / 2 : 0;
            if (bl) {
                n5 = insets.left + (n3 - n - n12 - insets.left - insets.right) / 2;
            }
            if (!bl) {
                n5 = n3 - insets.right - n - (n3 - n - n12 - insets.left - insets.right) / 2;
            }
            CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo2 = new CommandButtonLayoutManager.TextLayoutInfo();
            textLayoutInfo2.text = this.titlePart2;
            textLayoutInfo2.textRect = new Rectangle();
            textLayoutInfo2.textRect.x = n5;
            textLayoutInfo2.textRect.y = n11;
            textLayoutInfo2.textRect.width = n;
            textLayoutInfo2.textRect.height = n6;
            commandButtonLayoutInfo.textLayoutInfoList = new ArrayList<CommandButtonLayoutManager.TextLayoutInfo>();
            commandButtonLayoutInfo.textLayoutInfoList.add(textLayoutInfo);
            commandButtonLayoutInfo.textLayoutInfoList.add(textLayoutInfo2);
        }
        if (bl4) {
            if (n > 0) {
                if (bl) {
                    n5 += 2 * n7;
                    n5 += n;
                } else {
                    n5 -= 2 * n7;
                    n5 -= n6 / 2;
                }
            } else {
                n5 = (n3 - 1 - n6 / 2) / 2;
            }
            commandButtonLayoutInfo.popupActionRect.x = n5;
            commandButtonLayoutInfo.popupActionRect.y = n11 + 1;
            commandButtonLayoutInfo.popupActionRect.width = 1 + n6 / 2;
            commandButtonLayoutInfo.popupActionRect.height = n6 + 2;
        }
        switch (commandButtonKind) {
            case ACTION_ONLY: {
                commandButtonLayoutInfo.actionClickArea.x = 0;
                commandButtonLayoutInfo.actionClickArea.y = 0;
                commandButtonLayoutInfo.actionClickArea.width = n3;
                commandButtonLayoutInfo.actionClickArea.height = n4;
                commandButtonLayoutInfo.isTextInActionArea = true;
                break;
            }
            case POPUP_ONLY: {
                commandButtonLayoutInfo.popupClickArea.x = 0;
                commandButtonLayoutInfo.popupClickArea.y = 0;
                commandButtonLayoutInfo.popupClickArea.width = n3;
                commandButtonLayoutInfo.popupClickArea.height = n4;
                commandButtonLayoutInfo.isTextInActionArea = false;
                break;
            }
            case ACTION_AND_POPUP_MAIN_ACTION: 
            case ACTION_AND_POPUP_MAIN_POPUP: {
                if (bl2) {
                    n2 = commandButtonLayoutInfo.iconRect.y + commandButtonLayoutInfo.iconRect.height + n8;
                    commandButtonLayoutInfo.actionClickArea.x = 0;
                    commandButtonLayoutInfo.actionClickArea.y = 0;
                    commandButtonLayoutInfo.actionClickArea.width = n3;
                    commandButtonLayoutInfo.actionClickArea.height = n2;
                    commandButtonLayoutInfo.popupClickArea.x = 0;
                    commandButtonLayoutInfo.popupClickArea.y = n2;
                    commandButtonLayoutInfo.popupClickArea.width = n3;
                    commandButtonLayoutInfo.popupClickArea.height = n4 - n2;
                    commandButtonLayoutInfo.isTextInActionArea = false;
                    break;
                }
                commandButtonLayoutInfo.popupClickArea.x = 0;
                commandButtonLayoutInfo.popupClickArea.y = 0;
                commandButtonLayoutInfo.popupClickArea.width = n3;
                commandButtonLayoutInfo.popupClickArea.height = n4;
                commandButtonLayoutInfo.isTextInActionArea = false;
            }
        }
        return commandButtonLayoutInfo;
    }

}

