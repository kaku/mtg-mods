/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common;

import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public class CommandButtonLayoutManagerMedium
implements CommandButtonLayoutManager {
    @Override
    public int getPreferredIconSize() {
        return 16;
    }

    protected float getIconTextGapFactor() {
        return 1.0f;
    }

    private boolean hasIcon(AbstractCommandButton abstractCommandButton) {
        if (abstractCommandButton.getIcon() != null) {
            return true;
        }
        if (Boolean.TRUE.equals(abstractCommandButton.getClientProperty("flamingo.internal.commandButtonLayoutManagerMedium.forceIcon"))) {
            return true;
        }
        return false;
    }

    @Override
    public Dimension getPreferredSize(AbstractCommandButton abstractCommandButton) {
        Insets insets = abstractCommandButton.getInsets();
        int n = insets.top + insets.bottom;
        FontMetrics fontMetrics = abstractCommandButton.getFontMetrics(abstractCommandButton.getFont());
        String string = abstractCommandButton.getText();
        int n2 = FlamingoUtilities.getHLayoutGap(abstractCommandButton);
        boolean bl = this.hasIcon(abstractCommandButton);
        boolean bl2 = string != null;
        boolean bl3 = FlamingoUtilities.hasPopupAction(abstractCommandButton);
        int n3 = bl ? this.getPreferredIconSize() : 0;
        int n4 = insets.left;
        if (bl) {
            n4 += n2;
            n4 += n3;
            n4 += n2;
        }
        if (bl2) {
            n4 = bl ? (n4 += (int)((float)n2 * this.getIconTextGapFactor())) : (n4 += n2);
            n4 += fontMetrics.stringWidth(string);
            n4 += n2;
        }
        if (bl3) {
            if (bl2 && bl) {
                n4 += 2 * n2;
            }
            n4 += 1 + fontMetrics.getHeight() / 2;
            n4 += 2 * n2;
        }
        if (abstractCommandButton instanceof JCommandButton) {
            JCommandButton jCommandButton = (JCommandButton)abstractCommandButton;
            JCommandButton.CommandButtonKind commandButtonKind = jCommandButton.getCommandButtonKind();
            boolean bl4 = false;
            if (commandButtonKind == JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_ACTION && (bl || bl2)) {
                bl4 = true;
            }
            if (commandButtonKind == JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_POPUP && bl) {
                bl4 = true;
            }
            if (bl4) {
                n4 += 2;
            }
        }
        n4 += insets.right;
        return new Dimension(n4 -= 2 * n2, n + Math.max(n3, fontMetrics.getAscent() + fontMetrics.getDescent()));
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
    }

    @Override
    public Point getKeyTipAnchorCenterPoint(AbstractCommandButton abstractCommandButton) {
        Insets insets = abstractCommandButton.getInsets();
        int n = abstractCommandButton.getHeight();
        boolean bl = this.hasIcon(abstractCommandButton);
        int n2 = this.getPreferredIconSize();
        if (bl) {
            return new Point(insets.left + n2, (n + n2) / 2);
        }
        return new Point(insets.left, 3 * n / 4);
    }

    @Override
    public CommandButtonLayoutManager.CommandButtonLayoutInfo getLayoutInfo(AbstractCommandButton abstractCommandButton, Graphics graphics) {
        CommandButtonLayoutManager.CommandButtonLayoutInfo commandButtonLayoutInfo = new CommandButtonLayoutManager.CommandButtonLayoutInfo();
        commandButtonLayoutInfo.actionClickArea = new Rectangle(0, 0, 0, 0);
        commandButtonLayoutInfo.popupClickArea = new Rectangle(0, 0, 0, 0);
        Insets insets = abstractCommandButton.getInsets();
        commandButtonLayoutInfo.iconRect = new Rectangle();
        commandButtonLayoutInfo.popupActionRect = new Rectangle();
        int n = abstractCommandButton.getWidth();
        int n2 = abstractCommandButton.getHeight();
        String string = abstractCommandButton.getText();
        int n3 = this.getPreferredIconSize();
        boolean bl = this.hasIcon(abstractCommandButton);
        boolean bl2 = string != null;
        boolean bl3 = FlamingoUtilities.hasPopupAction(abstractCommandButton);
        boolean bl4 = abstractCommandButton.getComponentOrientation().isLeftToRight();
        int n4 = this.getPreferredSize((AbstractCommandButton)abstractCommandButton).width;
        int n5 = 0;
        if (abstractCommandButton.getHorizontalAlignment() == 0 && n > n4) {
            n5 = (n - n4) / 2;
        }
        FontMetrics fontMetrics = graphics.getFontMetrics();
        int n6 = fontMetrics.getAscent() + fontMetrics.getDescent();
        JCommandButton.CommandButtonKind commandButtonKind = abstractCommandButton instanceof JCommandButton ? ((JCommandButton)abstractCommandButton).getCommandButtonKind() : JCommandButton.CommandButtonKind.ACTION_ONLY;
        int n7 = FlamingoUtilities.getHLayoutGap(abstractCommandButton);
        if (bl4) {
            int n8 = insets.left + n5 - n7;
            if (bl) {
                commandButtonLayoutInfo.iconRect.x = n8 += n7;
                commandButtonLayoutInfo.iconRect.y = (n2 - n3) / 2;
                commandButtonLayoutInfo.iconRect.width = n3;
                commandButtonLayoutInfo.iconRect.height = n3;
                n8 += n3 + n7;
            }
            if (bl2) {
                n8 = bl ? (n8 += (int)((float)n7 * this.getIconTextGapFactor())) : (n8 += n7);
                CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo = new CommandButtonLayoutManager.TextLayoutInfo();
                textLayoutInfo.text = abstractCommandButton.getText();
                textLayoutInfo.textRect = new Rectangle();
                commandButtonLayoutInfo.textLayoutInfoList = new ArrayList<CommandButtonLayoutManager.TextLayoutInfo>();
                commandButtonLayoutInfo.textLayoutInfoList.add(textLayoutInfo);
                textLayoutInfo.textRect.x = n8;
                textLayoutInfo.textRect.y = (n2 - n6) / 2;
                textLayoutInfo.textRect.width = (int)fontMetrics.getStringBounds(string, graphics).getWidth();
                textLayoutInfo.textRect.height = n6;
                n8 += textLayoutInfo.textRect.width;
                n8 += n7;
            }
            if (bl3) {
                if (bl2 && bl) {
                    n8 += 2 * n7;
                }
                commandButtonLayoutInfo.popupActionRect.x = n8;
                commandButtonLayoutInfo.popupActionRect.y = (n2 - n6) / 2 - 1;
                commandButtonLayoutInfo.popupActionRect.width = 1 + n6 / 2;
                commandButtonLayoutInfo.popupActionRect.height = n6 + 2;
                n8 += commandButtonLayoutInfo.popupActionRect.width;
                n8 += 2 * n7;
            }
            int n9 = 2;
            switch (commandButtonKind) {
                case ACTION_ONLY: {
                    commandButtonLayoutInfo.actionClickArea.x = 0;
                    commandButtonLayoutInfo.actionClickArea.y = 0;
                    commandButtonLayoutInfo.actionClickArea.width = n;
                    commandButtonLayoutInfo.actionClickArea.height = n2;
                    commandButtonLayoutInfo.isTextInActionArea = true;
                    break;
                }
                case POPUP_ONLY: {
                    commandButtonLayoutInfo.popupClickArea.x = 0;
                    commandButtonLayoutInfo.popupClickArea.y = 0;
                    commandButtonLayoutInfo.popupClickArea.width = n;
                    commandButtonLayoutInfo.popupClickArea.height = n2;
                    commandButtonLayoutInfo.isTextInActionArea = false;
                    break;
                }
                case ACTION_AND_POPUP_MAIN_ACTION: {
                    if (bl2 || bl) {
                        commandButtonLayoutInfo.popupActionRect.x += n9;
                        int n10 = commandButtonLayoutInfo.popupActionRect.x - 2 * n7;
                        commandButtonLayoutInfo.actionClickArea.x = 0;
                        commandButtonLayoutInfo.actionClickArea.y = 0;
                        commandButtonLayoutInfo.actionClickArea.width = n10;
                        commandButtonLayoutInfo.actionClickArea.height = n2;
                        commandButtonLayoutInfo.popupClickArea.x = n10;
                        commandButtonLayoutInfo.popupClickArea.y = 0;
                        commandButtonLayoutInfo.popupClickArea.width = n - n10;
                        commandButtonLayoutInfo.popupClickArea.height = n2;
                        commandButtonLayoutInfo.separatorOrientation = CommandButtonLayoutManager.CommandButtonSeparatorOrientation.VERTICAL;
                        commandButtonLayoutInfo.separatorArea = new Rectangle();
                        commandButtonLayoutInfo.separatorArea.x = n10;
                        commandButtonLayoutInfo.separatorArea.y = 0;
                        commandButtonLayoutInfo.separatorArea.width = n9;
                        commandButtonLayoutInfo.separatorArea.height = n2;
                        commandButtonLayoutInfo.isTextInActionArea = true;
                        break;
                    }
                    commandButtonLayoutInfo.popupClickArea.x = 0;
                    commandButtonLayoutInfo.popupClickArea.y = 0;
                    commandButtonLayoutInfo.popupClickArea.width = n;
                    commandButtonLayoutInfo.popupClickArea.height = n2;
                    commandButtonLayoutInfo.isTextInActionArea = false;
                    break;
                }
                case ACTION_AND_POPUP_MAIN_POPUP: {
                    if (bl) {
                        if (commandButtonLayoutInfo.textLayoutInfoList != null) {
                            for (CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo : commandButtonLayoutInfo.textLayoutInfoList) {
                                textLayoutInfo.textRect.x += n9;
                            }
                        }
                        commandButtonLayoutInfo.popupActionRect.x += n9;
                        int n11 = commandButtonLayoutInfo.iconRect.x + commandButtonLayoutInfo.iconRect.width + n7;
                        commandButtonLayoutInfo.actionClickArea.x = 0;
                        commandButtonLayoutInfo.actionClickArea.y = 0;
                        commandButtonLayoutInfo.actionClickArea.width = n11;
                        commandButtonLayoutInfo.actionClickArea.height = n2;
                        commandButtonLayoutInfo.popupClickArea.x = n11;
                        commandButtonLayoutInfo.popupClickArea.y = 0;
                        commandButtonLayoutInfo.popupClickArea.width = n - n11;
                        commandButtonLayoutInfo.popupClickArea.height = n2;
                        commandButtonLayoutInfo.separatorOrientation = CommandButtonLayoutManager.CommandButtonSeparatorOrientation.VERTICAL;
                        commandButtonLayoutInfo.separatorArea = new Rectangle();
                        commandButtonLayoutInfo.separatorArea.x = n11;
                        commandButtonLayoutInfo.separatorArea.y = 0;
                        commandButtonLayoutInfo.separatorArea.width = n9;
                        commandButtonLayoutInfo.separatorArea.height = n2;
                        commandButtonLayoutInfo.isTextInActionArea = false;
                        break;
                    }
                    commandButtonLayoutInfo.popupClickArea.x = 0;
                    commandButtonLayoutInfo.popupClickArea.y = 0;
                    commandButtonLayoutInfo.popupClickArea.width = n;
                    commandButtonLayoutInfo.popupClickArea.height = n2;
                    commandButtonLayoutInfo.isTextInActionArea = true;
                }
            }
        } else {
            int n12 = n - insets.right - n5 + n7;
            if (bl) {
                commandButtonLayoutInfo.iconRect.x = (n12 -= n7) - n3;
                commandButtonLayoutInfo.iconRect.y = (n2 - n3) / 2;
                commandButtonLayoutInfo.iconRect.width = n3;
                commandButtonLayoutInfo.iconRect.height = n3;
                n12 -= n3 + n7;
            }
            if (bl2) {
                n12 = bl ? (n12 -= (int)((float)n7 * this.getIconTextGapFactor())) : (n12 -= n7);
                CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo = new CommandButtonLayoutManager.TextLayoutInfo();
                textLayoutInfo.text = abstractCommandButton.getText();
                textLayoutInfo.textRect = new Rectangle();
                commandButtonLayoutInfo.textLayoutInfoList = new ArrayList<CommandButtonLayoutManager.TextLayoutInfo>();
                commandButtonLayoutInfo.textLayoutInfoList.add(textLayoutInfo);
                textLayoutInfo.textRect.width = (int)fontMetrics.getStringBounds(string, graphics).getWidth();
                textLayoutInfo.textRect.x = n12 - textLayoutInfo.textRect.width;
                textLayoutInfo.textRect.y = (n2 - n6) / 2;
                textLayoutInfo.textRect.height = n6;
                n12 -= textLayoutInfo.textRect.width;
                n12 -= n7;
            }
            if (bl3) {
                if (bl2 && bl) {
                    n12 -= 2 * n7;
                }
                commandButtonLayoutInfo.popupActionRect.width = 1 + n6 / 2;
                commandButtonLayoutInfo.popupActionRect.x = n12 - commandButtonLayoutInfo.popupActionRect.width;
                commandButtonLayoutInfo.popupActionRect.y = (n2 - n6) / 2 - 1;
                commandButtonLayoutInfo.popupActionRect.height = n6 + 2;
                n12 -= commandButtonLayoutInfo.popupActionRect.width;
                n12 -= 2 * n7;
            }
            int n13 = 0;
            int n14 = 2;
            switch (commandButtonKind) {
                case ACTION_ONLY: {
                    commandButtonLayoutInfo.actionClickArea.x = 0;
                    commandButtonLayoutInfo.actionClickArea.y = 0;
                    commandButtonLayoutInfo.actionClickArea.width = n;
                    commandButtonLayoutInfo.actionClickArea.height = n2;
                    commandButtonLayoutInfo.isTextInActionArea = true;
                    break;
                }
                case POPUP_ONLY: {
                    commandButtonLayoutInfo.popupClickArea.x = 0;
                    commandButtonLayoutInfo.popupClickArea.y = 0;
                    commandButtonLayoutInfo.popupClickArea.width = n;
                    commandButtonLayoutInfo.popupClickArea.height = n2;
                    commandButtonLayoutInfo.isTextInActionArea = false;
                    break;
                }
                case ACTION_AND_POPUP_MAIN_ACTION: {
                    if (bl2 || bl) {
                        commandButtonLayoutInfo.popupActionRect.x -= n14;
                        commandButtonLayoutInfo.actionClickArea.x = n13 = commandButtonLayoutInfo.popupActionRect.x + commandButtonLayoutInfo.popupActionRect.width + 2 * n7;
                        commandButtonLayoutInfo.actionClickArea.y = 0;
                        commandButtonLayoutInfo.actionClickArea.width = n - n13;
                        commandButtonLayoutInfo.actionClickArea.height = n2;
                        commandButtonLayoutInfo.popupClickArea.x = 0;
                        commandButtonLayoutInfo.popupClickArea.y = 0;
                        commandButtonLayoutInfo.popupClickArea.width = n13;
                        commandButtonLayoutInfo.popupClickArea.height = n2;
                        commandButtonLayoutInfo.separatorOrientation = CommandButtonLayoutManager.CommandButtonSeparatorOrientation.VERTICAL;
                        commandButtonLayoutInfo.separatorArea = new Rectangle();
                        commandButtonLayoutInfo.separatorArea.x = n13;
                        commandButtonLayoutInfo.separatorArea.y = 0;
                        commandButtonLayoutInfo.separatorArea.width = n14;
                        commandButtonLayoutInfo.separatorArea.height = n2;
                        commandButtonLayoutInfo.isTextInActionArea = true;
                        break;
                    }
                    commandButtonLayoutInfo.popupClickArea.x = 0;
                    commandButtonLayoutInfo.popupClickArea.y = 0;
                    commandButtonLayoutInfo.popupClickArea.width = n;
                    commandButtonLayoutInfo.popupClickArea.height = n2;
                    commandButtonLayoutInfo.isTextInActionArea = false;
                    break;
                }
                case ACTION_AND_POPUP_MAIN_POPUP: {
                    if (bl) {
                        if (commandButtonLayoutInfo.textLayoutInfoList != null) {
                            for (CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo : commandButtonLayoutInfo.textLayoutInfoList) {
                                textLayoutInfo.textRect.x -= n14;
                            }
                        }
                        commandButtonLayoutInfo.popupActionRect.x -= n14;
                        commandButtonLayoutInfo.actionClickArea.x = n13 = commandButtonLayoutInfo.iconRect.x - n7;
                        commandButtonLayoutInfo.actionClickArea.y = 0;
                        commandButtonLayoutInfo.actionClickArea.width = n - n13;
                        commandButtonLayoutInfo.actionClickArea.height = n2;
                        commandButtonLayoutInfo.popupClickArea.x = 0;
                        commandButtonLayoutInfo.popupClickArea.y = 0;
                        commandButtonLayoutInfo.popupClickArea.width = n13;
                        commandButtonLayoutInfo.popupClickArea.height = n2;
                        commandButtonLayoutInfo.separatorOrientation = CommandButtonLayoutManager.CommandButtonSeparatorOrientation.VERTICAL;
                        commandButtonLayoutInfo.separatorArea = new Rectangle();
                        commandButtonLayoutInfo.separatorArea.x = n13;
                        commandButtonLayoutInfo.separatorArea.y = 0;
                        commandButtonLayoutInfo.separatorArea.width = n14;
                        commandButtonLayoutInfo.separatorArea.height = n2;
                        commandButtonLayoutInfo.isTextInActionArea = false;
                        break;
                    }
                    commandButtonLayoutInfo.popupClickArea.x = 0;
                    commandButtonLayoutInfo.popupClickArea.y = 0;
                    commandButtonLayoutInfo.popupClickArea.width = n;
                    commandButtonLayoutInfo.popupClickArea.height = n2;
                    commandButtonLayoutInfo.isTextInActionArea = true;
                }
            }
        }
        return commandButtonLayoutInfo;
    }

}

