/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JSeparator;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public class CommandButtonLayoutManagerBigFixedLandscape
implements CommandButtonLayoutManager {
    @Override
    public int getPreferredIconSize() {
        return 32;
    }

    @Override
    public Dimension getPreferredSize(AbstractCommandButton abstractCommandButton) {
        Insets insets = abstractCommandButton.getInsets();
        int n = insets.left + insets.right;
        int n2 = insets.top + insets.bottom;
        FontMetrics fontMetrics = abstractCommandButton.getFontMetrics(abstractCommandButton.getFont());
        JSeparator jSeparator = new JSeparator(1);
        int n3 = FlamingoUtilities.getVLayoutGap(abstractCommandButton);
        int n4 = fontMetrics.stringWidth(abstractCommandButton.getText());
        int n5 = Math.max(this.getPreferredIconSize(), n4);
        int n6 = n2 + this.getPreferredIconSize() + n3 + jSeparator.getPreferredSize().width;
        if (abstractCommandButton.getText() != null) {
            n6 += fontMetrics.getHeight();
        }
        n5 = Math.max(n5, n6 * 5 / 4);
        return new Dimension(n + n5, n6);
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
    }

    @Override
    public Point getKeyTipAnchorCenterPoint(AbstractCommandButton abstractCommandButton) {
        return new Point(abstractCommandButton.getWidth() / 2, abstractCommandButton.getHeight());
    }

    @Override
    public CommandButtonLayoutManager.CommandButtonLayoutInfo getLayoutInfo(AbstractCommandButton abstractCommandButton, Graphics graphics) {
        CommandButtonLayoutManager.CommandButtonLayoutInfo commandButtonLayoutInfo = new CommandButtonLayoutManager.CommandButtonLayoutInfo();
        commandButtonLayoutInfo.actionClickArea = new Rectangle(0, 0, 0, 0);
        commandButtonLayoutInfo.popupClickArea = new Rectangle(0, 0, 0, 0);
        Insets insets = abstractCommandButton.getInsets();
        commandButtonLayoutInfo.iconRect = new Rectangle();
        commandButtonLayoutInfo.popupActionRect = new Rectangle();
        int n = abstractCommandButton.getWidth();
        int n2 = abstractCommandButton.getHeight();
        int n3 = insets.left;
        int n4 = insets.top;
        FontMetrics fontMetrics = graphics.getFontMetrics();
        int n5 = fontMetrics.getAscent() + fontMetrics.getDescent();
        JCommandButton.CommandButtonKind commandButtonKind = abstractCommandButton instanceof JCommandButton ? ((JCommandButton)abstractCommandButton).getCommandButtonKind() : JCommandButton.CommandButtonKind.ACTION_ONLY;
        commandButtonLayoutInfo.isTextInActionArea = false;
        if (commandButtonKind == JCommandButton.CommandButtonKind.ACTION_ONLY) {
            commandButtonLayoutInfo.actionClickArea.x = 0;
            commandButtonLayoutInfo.actionClickArea.y = 0;
            commandButtonLayoutInfo.actionClickArea.width = n;
            commandButtonLayoutInfo.actionClickArea.height = n2;
            commandButtonLayoutInfo.isTextInActionArea = true;
        }
        if (commandButtonKind == JCommandButton.CommandButtonKind.POPUP_ONLY) {
            commandButtonLayoutInfo.popupClickArea.x = 0;
            commandButtonLayoutInfo.popupClickArea.y = 0;
            commandButtonLayoutInfo.popupClickArea.width = n;
            commandButtonLayoutInfo.popupClickArea.height = n2;
        }
        JSeparator jSeparator = new JSeparator(1);
        ResizableIcon resizableIcon = abstractCommandButton.getIcon();
        if (abstractCommandButton.getText() == null) {
            n4 = insets.top + (n2 - insets.top - insets.bottom - resizableIcon.getIconHeight()) / 2;
        }
        commandButtonLayoutInfo.iconRect.x = (n - resizableIcon.getIconWidth()) / 2;
        commandButtonLayoutInfo.iconRect.y = n4;
        commandButtonLayoutInfo.iconRect.width = resizableIcon.getIconWidth();
        commandButtonLayoutInfo.iconRect.height = resizableIcon.getIconHeight();
        n4 += resizableIcon.getIconHeight();
        CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo = new CommandButtonLayoutManager.TextLayoutInfo();
        textLayoutInfo.text = abstractCommandButton.getText();
        textLayoutInfo.textRect = new Rectangle();
        int n6 = (int)fontMetrics.getStringBounds(abstractCommandButton.getText(), graphics).getWidth();
        textLayoutInfo.textRect.x = insets.left + (n - n6 - insets.left - insets.right) / 2;
        textLayoutInfo.textRect.y = n4 += jSeparator.getPreferredSize().width;
        textLayoutInfo.textRect.width = n6;
        textLayoutInfo.textRect.height = n5;
        commandButtonLayoutInfo.textLayoutInfoList = new ArrayList<CommandButtonLayoutManager.TextLayoutInfo>();
        commandButtonLayoutInfo.textLayoutInfoList.add(textLayoutInfo);
        return commandButtonLayoutInfo;
    }
}

