/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon.appmenu;

import java.awt.event.ActionListener;
import java.util.List;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButtonPanel;
import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntrySecondary;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.CommandButtonLayoutManagerMenuTileLevel2;

public class JRibbonApplicationMenuPopupPanelSecondary
extends JCommandButtonPanel {
    protected static final CommandButtonDisplayState MENU_TILE_LEVEL_2 = new CommandButtonDisplayState("Ribbon application menu tile level 2", 32){

        @Override
        public CommandButtonLayoutManager createLayoutManager(AbstractCommandButton abstractCommandButton) {
            return new CommandButtonLayoutManagerMenuTileLevel2();
        }
    };

    public JRibbonApplicationMenuPopupPanelSecondary(RibbonApplicationMenuEntryPrimary ribbonApplicationMenuEntryPrimary) {
        super(MENU_TILE_LEVEL_2);
        this.setMaxButtonColumns(1);
        int n = ribbonApplicationMenuEntryPrimary.getSecondaryGroupCount();
        for (int i = 0; i < n; ++i) {
            String string = ribbonApplicationMenuEntryPrimary.getSecondaryGroupTitleAt(i);
            this.addButtonGroup(string);
            for (RibbonApplicationMenuEntrySecondary ribbonApplicationMenuEntrySecondary : ribbonApplicationMenuEntryPrimary.getSecondaryGroupEntries(i)) {
                JCommandMenuButton jCommandMenuButton = new JCommandMenuButton(ribbonApplicationMenuEntrySecondary.getText(), ribbonApplicationMenuEntrySecondary.getIcon());
                jCommandMenuButton.setExtraText(ribbonApplicationMenuEntrySecondary.getDescriptionText());
                jCommandMenuButton.setCommandButtonKind(ribbonApplicationMenuEntrySecondary.getEntryKind());
                jCommandMenuButton.addActionListener(ribbonApplicationMenuEntrySecondary.getMainActionListener());
                jCommandMenuButton.setDisplayState(MENU_TILE_LEVEL_2);
                jCommandMenuButton.setHorizontalAlignment(10);
                jCommandMenuButton.setPopupOrientationKind(JCommandButton.CommandButtonPopupOrientationKind.SIDEWARD);
                jCommandMenuButton.setEnabled(ribbonApplicationMenuEntrySecondary.isEnabled());
                jCommandMenuButton.setPopupCallback(ribbonApplicationMenuEntrySecondary.getPopupCallback());
                jCommandMenuButton.setActionKeyTip(ribbonApplicationMenuEntrySecondary.getActionKeyTip());
                jCommandMenuButton.setPopupKeyTip(ribbonApplicationMenuEntrySecondary.getPopupKeyTip());
                if (ribbonApplicationMenuEntrySecondary.getDisabledIcon() != null) {
                    jCommandMenuButton.setDisabledIcon(ribbonApplicationMenuEntrySecondary.getDisabledIcon());
                }
                this.addButtonToLastGroup(jCommandMenuButton);
            }
        }
    }

}

