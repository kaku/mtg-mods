/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon.appmenu;

import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.text.AttributedCharacterIterator;
import java.text.AttributedString;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JSeparator;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public class CommandButtonLayoutManagerMenuTileLevel2
implements CommandButtonLayoutManager {
    @Override
    public int getPreferredIconSize() {
        return 32;
    }

    @Override
    public Dimension getPreferredSize(AbstractCommandButton abstractCommandButton) {
        Insets insets = abstractCommandButton.getInsets();
        int n = insets.left + insets.right;
        int n2 = insets.top + insets.bottom;
        FontMetrics fontMetrics = abstractCommandButton.getFontMetrics(abstractCommandButton.getFont());
        JSeparator jSeparator = new JSeparator(1);
        int n3 = fontMetrics.stringWidth(abstractCommandButton.getText());
        int n4 = 2 * FlamingoUtilities.getHLayoutGap(abstractCommandButton);
        int n5 = 2 * FlamingoUtilities.getVLayoutGap(abstractCommandButton);
        int n6 = this.getPreferredIconSize() + 2 * n4 + jSeparator.getPreferredSize().width + n3 + (FlamingoUtilities.hasPopupAction(abstractCommandButton) ? 1 + fontMetrics.getHeight() / 2 + 4 * n4 + jSeparator.getPreferredSize().width : 0);
        int n7 = fontMetrics.getAscent() + fontMetrics.getDescent();
        int n8 = n7 + n5;
        String string = abstractCommandButton.getExtraText();
        if (string != null && string.length() > 0) {
            n8 += 2 * n7;
        }
        return new Dimension(n + n6, n2 + Math.max(this.getPreferredIconSize(), n8));
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
    }

    @Override
    public Point getKeyTipAnchorCenterPoint(AbstractCommandButton abstractCommandButton) {
        Insets insets = abstractCommandButton.getInsets();
        int n = abstractCommandButton.getHeight();
        ResizableIcon resizableIcon = abstractCommandButton.getIcon();
        return new Point(insets.left + resizableIcon.getIconWidth(), (n + resizableIcon.getIconHeight()) / 2);
    }

    @Override
    public CommandButtonLayoutManager.CommandButtonLayoutInfo getLayoutInfo(AbstractCommandButton abstractCommandButton, Graphics graphics) {
        JCommandButton.CommandButtonKind commandButtonKind;
        CommandButtonLayoutManager.CommandButtonLayoutInfo commandButtonLayoutInfo = new CommandButtonLayoutManager.CommandButtonLayoutInfo();
        commandButtonLayoutInfo.actionClickArea = new Rectangle(0, 0, 0, 0);
        commandButtonLayoutInfo.popupClickArea = new Rectangle(0, 0, 0, 0);
        Insets insets = abstractCommandButton.getInsets();
        commandButtonLayoutInfo.iconRect = new Rectangle();
        commandButtonLayoutInfo.popupActionRect = new Rectangle();
        int n = abstractCommandButton.getWidth();
        int n2 = abstractCommandButton.getHeight();
        FontMetrics fontMetrics = graphics.getFontMetrics();
        int n3 = fontMetrics.getAscent() + fontMetrics.getDescent();
        JCommandButton.CommandButtonKind commandButtonKind2 = commandButtonKind = abstractCommandButton instanceof JCommandButton ? ((JCommandButton)abstractCommandButton).getCommandButtonKind() : JCommandButton.CommandButtonKind.ACTION_ONLY;
        if (commandButtonKind == JCommandButton.CommandButtonKind.ACTION_ONLY) {
            commandButtonLayoutInfo.actionClickArea.x = 0;
            commandButtonLayoutInfo.actionClickArea.y = 0;
            commandButtonLayoutInfo.actionClickArea.width = n;
            commandButtonLayoutInfo.actionClickArea.height = n2;
            commandButtonLayoutInfo.isTextInActionArea = true;
        }
        if (commandButtonKind == JCommandButton.CommandButtonKind.POPUP_ONLY) {
            commandButtonLayoutInfo.popupClickArea.x = 0;
            commandButtonLayoutInfo.popupClickArea.y = 0;
            commandButtonLayoutInfo.popupClickArea.width = n;
            commandButtonLayoutInfo.popupClickArea.height = n2;
            commandButtonLayoutInfo.isTextInActionArea = false;
        }
        JSeparator jSeparator = new JSeparator(1);
        int n4 = 2 * FlamingoUtilities.getHLayoutGap(abstractCommandButton);
        int n5 = 2 * FlamingoUtilities.getVLayoutGap(abstractCommandButton);
        ResizableIcon resizableIcon = abstractCommandButton.getIcon();
        boolean bl = abstractCommandButton.getComponentOrientation().isLeftToRight();
        if (bl) {
            int n6;
            commandButtonLayoutInfo.iconRect.x = n6 = insets.left;
            commandButtonLayoutInfo.iconRect.y = insets.top + n5;
            commandButtonLayoutInfo.iconRect.width = resizableIcon.getIconWidth();
            commandButtonLayoutInfo.iconRect.height = resizableIcon.getIconHeight();
            n6 += resizableIcon.getIconWidth();
            if (commandButtonKind == JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_POPUP) {
                commandButtonLayoutInfo.actionClickArea.x = 0;
                commandButtonLayoutInfo.actionClickArea.y = 0;
                commandButtonLayoutInfo.actionClickArea.width = n6 + n4;
                commandButtonLayoutInfo.actionClickArea.height = n2;
                commandButtonLayoutInfo.popupClickArea.x = n6 + n4;
                commandButtonLayoutInfo.popupClickArea.y = 0;
                commandButtonLayoutInfo.popupClickArea.width = n - n6 - n4;
                commandButtonLayoutInfo.popupClickArea.height = n2;
                commandButtonLayoutInfo.separatorOrientation = CommandButtonLayoutManager.CommandButtonSeparatorOrientation.VERTICAL;
                commandButtonLayoutInfo.separatorArea = new Rectangle();
                commandButtonLayoutInfo.separatorArea.x = n6 + n4;
                commandButtonLayoutInfo.separatorArea.y = 0;
                commandButtonLayoutInfo.separatorArea.width = new JSeparator((int)1).getPreferredSize().width;
                commandButtonLayoutInfo.separatorArea.height = n2;
                commandButtonLayoutInfo.isTextInActionArea = false;
            }
            CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo = new CommandButtonLayoutManager.TextLayoutInfo();
            textLayoutInfo.text = abstractCommandButton.getText();
            textLayoutInfo.textRect = new Rectangle();
            textLayoutInfo.textRect.x = n6 += 2 * n4 + jSeparator.getPreferredSize().width;
            textLayoutInfo.textRect.y = insets.top + n5 / 2;
            textLayoutInfo.textRect.width = fontMetrics.stringWidth(abstractCommandButton.getText());
            textLayoutInfo.textRect.height = n3;
            commandButtonLayoutInfo.textLayoutInfoList = new ArrayList<CommandButtonLayoutManager.TextLayoutInfo>();
            commandButtonLayoutInfo.textLayoutInfoList.add(textLayoutInfo);
            String string = abstractCommandButton.getExtraText();
            if (string == null || string.length() == 0) {
                textLayoutInfo.textRect.y = (n2 - n3) / 2;
            } else {
                AttributedString attributedString = new AttributedString(abstractCommandButton.getExtraText());
                attributedString.addAttribute(TextAttribute.FONT, graphics.getFont());
                LineBreakMeasurer lineBreakMeasurer = new LineBreakMeasurer(attributedString.getIterator(), ((Graphics2D)graphics).getFontRenderContext());
                int n7 = n - n6 - insets.right - n4 - n3;
                int n8 = lineBreakMeasurer.nextOffset(n7);
                CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo2 = new CommandButtonLayoutManager.TextLayoutInfo();
                textLayoutInfo2.text = abstractCommandButton.getExtraText().substring(0, n8);
                textLayoutInfo2.textRect = new Rectangle();
                textLayoutInfo2.textRect.x = n6;
                textLayoutInfo2.textRect.y = insets.top + n5 + n3;
                textLayoutInfo2.textRect.width = fontMetrics.stringWidth(textLayoutInfo2.text);
                textLayoutInfo2.textRect.height = n3;
                CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo3 = new CommandButtonLayoutManager.TextLayoutInfo();
                textLayoutInfo3.text = abstractCommandButton.getExtraText().substring(n8);
                textLayoutInfo3.textRect = new Rectangle();
                textLayoutInfo3.textRect.x = n6;
                textLayoutInfo3.textRect.y = insets.top + n5 + 2 * n3;
                textLayoutInfo3.textRect.width = fontMetrics.stringWidth(textLayoutInfo3.text);
                textLayoutInfo3.textRect.height = n3;
                commandButtonLayoutInfo.extraTextLayoutInfoList = new ArrayList<CommandButtonLayoutManager.TextLayoutInfo>();
                commandButtonLayoutInfo.extraTextLayoutInfoList.add(textLayoutInfo2);
                commandButtonLayoutInfo.extraTextLayoutInfoList.add(textLayoutInfo3);
            }
            n6 = (int)((double)n6 + fontMetrics.getStringBounds(abstractCommandButton.getText(), graphics).getWidth());
            if (commandButtonKind == JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_ACTION) {
                commandButtonLayoutInfo.actionClickArea.x = 0;
                commandButtonLayoutInfo.actionClickArea.y = 0;
                commandButtonLayoutInfo.actionClickArea.width = n - insets.right - n3;
                commandButtonLayoutInfo.actionClickArea.height = n2;
                commandButtonLayoutInfo.popupClickArea.x = n - insets.right - n3;
                commandButtonLayoutInfo.popupClickArea.y = 0;
                commandButtonLayoutInfo.popupClickArea.width = n3 + insets.right;
                commandButtonLayoutInfo.popupClickArea.height = n2;
                commandButtonLayoutInfo.separatorOrientation = CommandButtonLayoutManager.CommandButtonSeparatorOrientation.VERTICAL;
                commandButtonLayoutInfo.separatorArea = new Rectangle();
                commandButtonLayoutInfo.separatorArea.x = n - insets.right - n3;
                commandButtonLayoutInfo.separatorArea.y = 0;
                commandButtonLayoutInfo.separatorArea.width = new JSeparator((int)1).getPreferredSize().width;
                commandButtonLayoutInfo.separatorArea.height = n2;
                commandButtonLayoutInfo.isTextInActionArea = true;
            }
            if (FlamingoUtilities.hasPopupAction(abstractCommandButton)) {
                commandButtonLayoutInfo.popupActionRect.x = n - insets.right - n3 * 3 / 4;
                commandButtonLayoutInfo.popupActionRect.y = (n2 - n3) / 2 - 1;
                commandButtonLayoutInfo.popupActionRect.width = 1 + n3 / 2;
                commandButtonLayoutInfo.popupActionRect.height = n3 + 2;
            }
        } else {
            int n9 = abstractCommandButton.getWidth() - insets.right;
            commandButtonLayoutInfo.iconRect.x = n9 - resizableIcon.getIconWidth();
            commandButtonLayoutInfo.iconRect.y = insets.top + n5;
            commandButtonLayoutInfo.iconRect.width = resizableIcon.getIconWidth();
            commandButtonLayoutInfo.iconRect.height = resizableIcon.getIconHeight();
            n9 -= resizableIcon.getIconWidth();
            if (commandButtonKind == JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_POPUP) {
                commandButtonLayoutInfo.actionClickArea.x = n9 + n4;
                commandButtonLayoutInfo.actionClickArea.y = 0;
                commandButtonLayoutInfo.actionClickArea.width = n - n9 - n4;
                commandButtonLayoutInfo.actionClickArea.height = n2;
                commandButtonLayoutInfo.popupClickArea.x = 0;
                commandButtonLayoutInfo.popupClickArea.y = 0;
                commandButtonLayoutInfo.popupClickArea.width = n9 + n4;
                commandButtonLayoutInfo.popupClickArea.height = n2;
                commandButtonLayoutInfo.separatorOrientation = CommandButtonLayoutManager.CommandButtonSeparatorOrientation.VERTICAL;
                commandButtonLayoutInfo.separatorArea = new Rectangle();
                commandButtonLayoutInfo.separatorArea.x = n9 + n4;
                commandButtonLayoutInfo.separatorArea.y = 0;
                commandButtonLayoutInfo.separatorArea.width = new JSeparator((int)1).getPreferredSize().width;
                commandButtonLayoutInfo.separatorArea.height = n2;
                commandButtonLayoutInfo.isTextInActionArea = false;
            }
            CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo = new CommandButtonLayoutManager.TextLayoutInfo();
            textLayoutInfo.text = abstractCommandButton.getText();
            textLayoutInfo.textRect = new Rectangle();
            textLayoutInfo.textRect.width = fontMetrics.stringWidth(abstractCommandButton.getText());
            textLayoutInfo.textRect.x = (n9 -= 2 * n4 + jSeparator.getPreferredSize().width) - textLayoutInfo.textRect.width;
            textLayoutInfo.textRect.y = insets.top + n5 / 2;
            textLayoutInfo.textRect.height = n3;
            commandButtonLayoutInfo.textLayoutInfoList = new ArrayList<CommandButtonLayoutManager.TextLayoutInfo>();
            commandButtonLayoutInfo.textLayoutInfoList.add(textLayoutInfo);
            String string = abstractCommandButton.getExtraText();
            if (string == null || string.length() == 0) {
                textLayoutInfo.textRect.y = (n2 - n3) / 2;
            } else {
                AttributedString attributedString = new AttributedString(abstractCommandButton.getExtraText());
                attributedString.addAttribute(TextAttribute.FONT, graphics.getFont());
                LineBreakMeasurer lineBreakMeasurer = new LineBreakMeasurer(attributedString.getIterator(), ((Graphics2D)graphics).getFontRenderContext());
                int n10 = n9 - insets.left - n4 - n3;
                int n11 = lineBreakMeasurer.nextOffset(n10);
                CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo4 = new CommandButtonLayoutManager.TextLayoutInfo();
                textLayoutInfo4.text = abstractCommandButton.getExtraText().substring(0, n11);
                textLayoutInfo4.textRect = new Rectangle();
                textLayoutInfo4.textRect.width = fontMetrics.stringWidth(textLayoutInfo4.text);
                textLayoutInfo4.textRect.x = n9 - textLayoutInfo4.textRect.width;
                textLayoutInfo4.textRect.y = insets.top + n5 + n3;
                textLayoutInfo4.textRect.height = n3;
                CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo5 = new CommandButtonLayoutManager.TextLayoutInfo();
                textLayoutInfo5.text = abstractCommandButton.getExtraText().substring(n11);
                textLayoutInfo5.textRect = new Rectangle();
                textLayoutInfo5.textRect.width = fontMetrics.stringWidth(textLayoutInfo5.text);
                textLayoutInfo5.textRect.x = n9 - textLayoutInfo5.textRect.width;
                textLayoutInfo5.textRect.y = insets.top + n5 + 2 * n3;
                textLayoutInfo5.textRect.height = n3;
                commandButtonLayoutInfo.extraTextLayoutInfoList = new ArrayList<CommandButtonLayoutManager.TextLayoutInfo>();
                commandButtonLayoutInfo.extraTextLayoutInfoList.add(textLayoutInfo4);
                commandButtonLayoutInfo.extraTextLayoutInfoList.add(textLayoutInfo5);
            }
            if (commandButtonKind == JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_ACTION) {
                commandButtonLayoutInfo.actionClickArea.x = n3 + insets.left;
                commandButtonLayoutInfo.actionClickArea.y = 0;
                commandButtonLayoutInfo.actionClickArea.width = n - insets.right - n3;
                commandButtonLayoutInfo.actionClickArea.height = n2;
                commandButtonLayoutInfo.popupClickArea.x = 0;
                commandButtonLayoutInfo.popupClickArea.y = 0;
                commandButtonLayoutInfo.popupClickArea.width = insets.left + n3;
                commandButtonLayoutInfo.popupClickArea.height = n2;
                commandButtonLayoutInfo.separatorOrientation = CommandButtonLayoutManager.CommandButtonSeparatorOrientation.VERTICAL;
                commandButtonLayoutInfo.separatorArea = new Rectangle();
                commandButtonLayoutInfo.separatorArea.x = n3 + insets.left;
                commandButtonLayoutInfo.separatorArea.y = 0;
                commandButtonLayoutInfo.separatorArea.width = new JSeparator((int)1).getPreferredSize().width;
                commandButtonLayoutInfo.separatorArea.height = n2;
                commandButtonLayoutInfo.isTextInActionArea = true;
            }
            if (FlamingoUtilities.hasPopupAction(abstractCommandButton)) {
                commandButtonLayoutInfo.popupActionRect.x = insets.left + n3 / 4;
                commandButtonLayoutInfo.popupActionRect.y = (n2 - n3) / 2 - 1;
                commandButtonLayoutInfo.popupActionRect.width = 1 + n3 / 2;
                commandButtonLayoutInfo.popupActionRect.height = n3 + 2;
            }
        }
        return commandButtonLayoutInfo;
    }
}

