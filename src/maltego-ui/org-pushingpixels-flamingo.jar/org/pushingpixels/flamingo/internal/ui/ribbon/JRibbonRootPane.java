/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JLayeredPane;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.plaf.RootPaneUI;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;
import org.pushingpixels.flamingo.api.ribbon.JRibbonFrame;

public class JRibbonRootPane
extends JRootPane {
    public static final String uiClassID = "RibbonRootPaneUI";
    public static final int RIBBON_SPECIAL_LAYER = JLayeredPane.DEFAULT_LAYER + 50;

    public JRibbonRootPane() {
        InputMap inputMap = this.getInputMap(2);
        ActionMap actionMap = this.getActionMap();
        actionMap.put("toggleMinimized", new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JRibbon jRibbon;
                JRibbonFrame jRibbonFrame = (JRibbonFrame)SwingUtilities.getWindowAncestor(JRibbonRootPane.this);
                jRibbon.setMinimized(!(jRibbon = jRibbonFrame.getRibbon()).isMinimized());
            }
        });
        inputMap.put(KeyStroke.getKeyStroke(112, 2), "toggleMinimized");
    }

    @Override
    public void updateUI() {
        this.setUI((RootPaneUI)UIManager.getUI(this));
    }

    @Override
    public String getUIClassID() {
        if (UIManager.get("RibbonRootPaneUI") != null) {
            return "RibbonRootPaneUI";
        }
        return "RootPaneUI";
    }

}

