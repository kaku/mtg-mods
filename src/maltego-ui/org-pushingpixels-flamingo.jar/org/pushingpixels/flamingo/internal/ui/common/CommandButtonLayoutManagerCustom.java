/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Insets;
import javax.swing.JSeparator;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonLayoutManagerBig;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public class CommandButtonLayoutManagerCustom
extends CommandButtonLayoutManagerBig {
    public CommandButtonLayoutManagerCustom(AbstractCommandButton abstractCommandButton) {
        super(abstractCommandButton);
    }

    @Override
    public int getPreferredIconSize() {
        return -1;
    }

    @Override
    public Dimension getPreferredSize(AbstractCommandButton abstractCommandButton) {
        Insets insets = abstractCommandButton.getInsets();
        int n = insets.left + insets.right;
        int n2 = insets.top + insets.bottom;
        FontMetrics fontMetrics = abstractCommandButton.getFontMetrics(abstractCommandButton.getFont());
        JSeparator jSeparator = new JSeparator(0);
        int n3 = FlamingoUtilities.getHLayoutGap(abstractCommandButton);
        int n4 = FlamingoUtilities.getVLayoutGap(abstractCommandButton);
        int n5 = this.titlePart1 == null ? 0 : fontMetrics.stringWidth(this.titlePart1);
        int n6 = this.titlePart2 == null ? 0 : fontMetrics.stringWidth(this.titlePart2);
        ResizableIcon resizableIcon = abstractCommandButton.getIcon();
        int n7 = resizableIcon == null ? 0 : resizableIcon.getIconWidth();
        int n8 = Math.max(n7, Math.max(n5, n6 + 4 * n3 + jSeparator.getPreferredSize().width + (FlamingoUtilities.hasPopupAction(abstractCommandButton) ? 1 + fontMetrics.getHeight() / 2 : 0)));
        boolean bl = abstractCommandButton.getIcon() != null;
        boolean bl2 = this.titlePart1 != null;
        boolean bl3 = FlamingoUtilities.hasPopupAction(abstractCommandButton);
        int n9 = insets.top;
        if (bl) {
            n9 += n4;
            n9 += resizableIcon.getIconHeight();
            n9 += n4;
        }
        if (bl2) {
            n9 += n4;
            n9 += 2 * (fontMetrics.getAscent() + fontMetrics.getDescent());
            n9 += n4;
        }
        if (!bl2 && bl3) {
            n9 += n4;
            n9 += fontMetrics.getHeight();
            n9 += n4;
        }
        if (bl3) {
            n9 += new JSeparator((int)0).getPreferredSize().height;
        }
        n9 += insets.bottom;
        return new Dimension(n + n8, n9 -= 2 * n4);
    }
}

