/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon;

import java.awt.Dimension;
import java.awt.Point;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.ribbon.RibbonElementPriority;

public abstract class RibbonComponentUI
extends ComponentUI {
    public abstract Point getKeyTipAnchorCenterPoint();

    public abstract Dimension getPreferredSize(RibbonElementPriority var1);
}

