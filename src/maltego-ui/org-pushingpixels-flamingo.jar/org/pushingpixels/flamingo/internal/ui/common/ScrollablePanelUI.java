/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common;

import javax.swing.plaf.PanelUI;

public abstract class ScrollablePanelUI
extends PanelUI {
    public abstract void scrollToIfNecessary(int var1, int var2);

    public abstract boolean isShowingScrollButtons();
}

