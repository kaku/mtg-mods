/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common;

import java.awt.Container;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JScrollablePanel;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;
import org.pushingpixels.flamingo.api.common.model.PopupButtonModel;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonUI;

public class BasicCommandButtonListener
implements MouseListener,
MouseMotionListener,
FocusListener,
ChangeListener {
    @Override
    public void focusLost(FocusEvent focusEvent) {
        AbstractCommandButton abstractCommandButton = (AbstractCommandButton)focusEvent.getSource();
        abstractCommandButton.getActionModel().setArmed(false);
        abstractCommandButton.getActionModel().setPressed(false);
        if (abstractCommandButton instanceof JCommandButton) {
            PopupButtonModel popupButtonModel = ((JCommandButton)abstractCommandButton).getPopupModel();
            popupButtonModel.setPressed(false);
            popupButtonModel.setArmed(false);
        }
    }

    @Override
    public void focusGained(FocusEvent focusEvent) {
        AbstractCommandButton abstractCommandButton = (AbstractCommandButton)focusEvent.getSource();
        abstractCommandButton.repaint();
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        if (SwingUtilities.isLeftMouseButton(mouseEvent)) {
            Object object;
            AbstractCommandButton abstractCommandButton = (AbstractCommandButton)mouseEvent.getSource();
            JScrollablePanel jScrollablePanel = (JScrollablePanel)SwingUtilities.getAncestorOfClass(JScrollablePanel.class, abstractCommandButton);
            if (jScrollablePanel != null) {
                object = SwingUtilities.convertPoint(abstractCommandButton.getParent(), abstractCommandButton.getLocation(), jScrollablePanel.getView());
                if (jScrollablePanel.getScrollType() == JScrollablePanel.ScrollType.HORIZONTALLY) {
                    jScrollablePanel.scrollToIfNecessary(object.x, abstractCommandButton.getWidth());
                } else {
                    jScrollablePanel.scrollToIfNecessary(object.y, abstractCommandButton.getHeight());
                }
            }
            if (abstractCommandButton.contains(mouseEvent.getX(), mouseEvent.getY())) {
                PopupButtonModel popupButtonModel;
                object = abstractCommandButton.getUI();
                Rectangle rectangle = object.getLayoutInfo().actionClickArea;
                Rectangle rectangle2 = object.getLayoutInfo().popupClickArea;
                if (rectangle != null && rectangle.contains(mouseEvent.getPoint())) {
                    ActionButtonModel actionButtonModel = abstractCommandButton.getActionModel();
                    if (actionButtonModel.isEnabled()) {
                        actionButtonModel.setArmed(true);
                        actionButtonModel.setPressed(true);
                    }
                } else if (rectangle2 != null && rectangle2.contains(mouseEvent.getPoint()) && (popupButtonModel = ((JCommandButton)abstractCommandButton).getPopupModel()).isEnabled()) {
                    popupButtonModel.setArmed(true);
                    popupButtonModel.setPressed(true);
                }
                if (!abstractCommandButton.hasFocus() && abstractCommandButton.isRequestFocusEnabled()) {
                    abstractCommandButton.requestFocusInWindow();
                }
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        if (SwingUtilities.isLeftMouseButton(mouseEvent)) {
            AbstractCommandButton abstractCommandButton = (AbstractCommandButton)mouseEvent.getSource();
            abstractCommandButton.getActionModel().setPressed(false);
            if (abstractCommandButton instanceof JCommandButton) {
                ((JCommandButton)abstractCommandButton).getPopupModel().setPressed(false);
            }
            abstractCommandButton.getActionModel().setArmed(false);
            if (abstractCommandButton instanceof JCommandButton) {
                ((JCommandButton)abstractCommandButton).getPopupModel().setArmed(false);
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
        this.syncMouseMovement(mouseEvent);
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {
        this.syncMouseMovement(mouseEvent);
    }

    private void syncMouseMovement(MouseEvent mouseEvent) {
        AbstractCommandButton abstractCommandButton = (AbstractCommandButton)mouseEvent.getSource();
        ActionButtonModel actionButtonModel = abstractCommandButton.getActionModel();
        PopupButtonModel popupButtonModel = abstractCommandButton instanceof JCommandButton ? ((JCommandButton)abstractCommandButton).getPopupModel() : null;
        CommandButtonUI commandButtonUI = abstractCommandButton.getUI();
        Rectangle rectangle = commandButtonUI.getLayoutInfo().actionClickArea;
        Rectangle rectangle2 = commandButtonUI.getLayoutInfo().popupClickArea;
        if (rectangle != null && rectangle.contains(mouseEvent.getPoint())) {
            if (actionButtonModel.isEnabled()) {
                if (!SwingUtilities.isLeftMouseButton(mouseEvent)) {
                    actionButtonModel.setRollover(true);
                }
                if (actionButtonModel.isPressed()) {
                    actionButtonModel.setArmed(true);
                }
            }
            if (popupButtonModel != null && !SwingUtilities.isLeftMouseButton(mouseEvent)) {
                popupButtonModel.setRollover(false);
            }
        } else if (rectangle2 != null && rectangle2.contains(mouseEvent.getPoint())) {
            if (popupButtonModel != null && popupButtonModel.isEnabled()) {
                if (!SwingUtilities.isLeftMouseButton(mouseEvent)) {
                    popupButtonModel.setRollover(true);
                }
                if (popupButtonModel.isPressed()) {
                    popupButtonModel.setArmed(true);
                }
            }
            if (!SwingUtilities.isLeftMouseButton(mouseEvent)) {
                actionButtonModel.setRollover(false);
            }
        }
    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {
        AbstractCommandButton abstractCommandButton = (AbstractCommandButton)mouseEvent.getSource();
        ActionButtonModel actionButtonModel = abstractCommandButton.getActionModel();
        PopupButtonModel popupButtonModel = abstractCommandButton instanceof JCommandButton ? ((JCommandButton)abstractCommandButton).getPopupModel() : null;
        actionButtonModel.setRollover(false);
        actionButtonModel.setArmed(false);
        if (popupButtonModel != null) {
            popupButtonModel.setRollover(false);
            popupButtonModel.setArmed(false);
        }
    }

    @Override
    public void stateChanged(ChangeEvent changeEvent) {
        AbstractCommandButton abstractCommandButton = (AbstractCommandButton)changeEvent.getSource();
        abstractCommandButton.repaint();
    }

    public void installKeyboardActions(AbstractCommandButton abstractCommandButton) {
        ActionMap actionMap = new ActionMap();
        actionMap.put("pressed", new PressAction(abstractCommandButton));
        actionMap.put("released", new ReleaseAction(abstractCommandButton));
        SwingUtilities.replaceUIActionMap(abstractCommandButton, actionMap);
        InputMap inputMap = LookAndFeel.makeInputMap(new Object[]{"SPACE", "pressed", "released SPACE", "released", "ENTER", "pressed", "released ENTER", "released"});
        SwingUtilities.replaceUIInputMap(abstractCommandButton, 0, inputMap);
    }

    public void uninstallKeyboardActions(AbstractCommandButton abstractCommandButton) {
        SwingUtilities.replaceUIInputMap(abstractCommandButton, 2, null);
        SwingUtilities.replaceUIInputMap(abstractCommandButton, 0, null);
        SwingUtilities.replaceUIActionMap(abstractCommandButton, null);
    }

    private static class ReleaseAction
    extends AbstractAction {
        private static final String RELEASE = "released";
        AbstractCommandButton button;

        ReleaseAction(AbstractCommandButton abstractCommandButton) {
            super("released");
            this.button = abstractCommandButton;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            ActionButtonModel actionButtonModel = this.button.getActionModel();
            actionButtonModel.setPressed(false);
            actionButtonModel.setArmed(false);
        }

        @Override
        public boolean isEnabled() {
            return this.button.getActionModel().isEnabled();
        }
    }

    private static class PressAction
    extends AbstractAction {
        private static final String PRESS = "pressed";
        AbstractCommandButton button;

        PressAction(AbstractCommandButton abstractCommandButton) {
            super("pressed");
            this.button = abstractCommandButton;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            ActionButtonModel actionButtonModel = this.button.getActionModel();
            actionButtonModel.setArmed(true);
            actionButtonModel.setPressed(true);
            if (!this.button.hasFocus()) {
                this.button.requestFocus();
            }
        }

        @Override
        public boolean isEnabled() {
            return this.button.getActionModel().isEnabled();
        }
    }

}

