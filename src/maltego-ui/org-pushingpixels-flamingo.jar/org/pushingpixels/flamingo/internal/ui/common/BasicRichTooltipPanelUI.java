/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Paint;
import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.ImageObserver;
import java.text.AttributedCharacterIterator;
import java.text.AttributedString;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.UIResource;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.internal.ui.common.JRichTooltipPanel;
import org.pushingpixels.flamingo.internal.ui.common.RichTooltipPanelUI;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public class BasicRichTooltipPanelUI
extends RichTooltipPanelUI {
    protected JRichTooltipPanel richTooltipPanel;
    protected List<JLabel> titleLabels = new ArrayList<JLabel>();
    protected List<JLabel> descriptionLabels = new ArrayList<JLabel>();
    protected JLabel mainImageLabel;
    protected JSeparator footerSeparator;
    protected JLabel footerImageLabel;
    protected List<JLabel> footerLabels = new ArrayList<JLabel>();

    public static ComponentUI createUI(JComponent jComponent) {
        return new BasicRichTooltipPanelUI();
    }

    @Override
    public void installUI(JComponent jComponent) {
        this.richTooltipPanel = (JRichTooltipPanel)jComponent;
        super.installUI(this.richTooltipPanel);
        this.installDefaults();
        this.installComponents();
        this.installListeners();
        this.richTooltipPanel.setLayout(this.createLayoutManager());
    }

    @Override
    public void uninstallUI(JComponent jComponent) {
        this.uninstallListeners();
        this.uninstallComponents();
        this.uninstallDefaults();
        super.uninstallUI(this.richTooltipPanel);
    }

    protected void installDefaults() {
        Object object;
        Border border = this.richTooltipPanel.getBorder();
        if (border == null || border instanceof UIResource) {
            object = UIManager.getBorder("RichTooltipPanel.border");
            if (object == null) {
                object = new BorderUIResource.CompoundBorderUIResource(new LineBorder(FlamingoUtilities.getBorderColor()), new EmptyBorder(2, 4, 3, 4));
            }
            this.richTooltipPanel.setBorder((Border)object);
        }
        LookAndFeel.installProperty(this.richTooltipPanel, "opaque", Boolean.TRUE);
        object = this.richTooltipPanel.getFont();
        if (object == null || object instanceof UIResource) {
            Font font = UIManager.getFont("RichTooltipPanel.font");
            if (font == null) {
                font = new JLabel().getFont();
            }
            this.richTooltipPanel.setFont(font);
        }
    }

    protected void installListeners() {
    }

    protected void installComponents() {
    }

    protected void uninstallDefaults() {
        LookAndFeel.uninstallBorder(this.richTooltipPanel);
    }

    protected void uninstallListeners() {
    }

    protected void uninstallComponents() {
        this.removeExistingComponents();
    }

    @Override
    public void update(Graphics graphics, JComponent jComponent) {
        this.paintBackground(graphics);
        this.paint(graphics, jComponent);
    }

    protected void paintBackground(Graphics graphics) {
        Color color = FlamingoUtilities.getColor(Color.gray, "Label.disabledForeground").brighter();
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        graphics2D.setPaint(new GradientPaint(0.0f, 0.0f, FlamingoUtilities.getLighterColor(color, 0.9), 0.0f, this.richTooltipPanel.getHeight(), FlamingoUtilities.getLighterColor(color, 0.4)));
        graphics2D.fillRect(0, 0, this.richTooltipPanel.getWidth(), this.richTooltipPanel.getHeight());
        graphics2D.setFont(FlamingoUtilities.getFont(this.richTooltipPanel, "Ribbon.font", "Button.font", "Panel.font"));
        graphics2D.dispose();
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
    }

    protected LayoutManager createLayoutManager() {
        return new RichTooltipPanelLayout();
    }

    protected int getDescriptionTextWidth() {
        return 200;
    }

    protected int getLayoutGap() {
        return 4;
    }

    protected void removeExistingComponents() {
        for (JLabel jLabel22 : this.titleLabels) {
            this.richTooltipPanel.remove(jLabel22);
        }
        if (this.mainImageLabel != null) {
            this.richTooltipPanel.remove(this.mainImageLabel);
        }
        for (JLabel jLabel22 : this.descriptionLabels) {
            this.richTooltipPanel.remove(jLabel22);
        }
        if (this.footerSeparator != null) {
            this.richTooltipPanel.remove(this.footerSeparator);
        }
        if (this.footerImageLabel != null) {
            this.richTooltipPanel.remove(this.footerImageLabel);
        }
        for (JLabel jLabel22 : this.footerLabels) {
            this.richTooltipPanel.remove(jLabel22);
        }
    }

    protected class RichTooltipPanelLayout
    implements LayoutManager {
        protected RichTooltipPanelLayout() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            return this.preferredLayoutSize(container);
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            TextLayout textLayout;
            Object object;
            Insets insets = container.getInsets();
            int n = BasicRichTooltipPanelUI.this.getLayoutGap();
            FontUIResource fontUIResource = FlamingoUtilities.getFont(container, "Ribbon.font", "Button.font", "Panel.font");
            Font font = fontUIResource.deriveFont(1);
            int n2 = BasicRichTooltipPanelUI.this.getDescriptionTextWidth();
            int n3 = insets.left + 2 * n + n2 + insets.right;
            RichTooltip richTooltip = BasicRichTooltipPanelUI.this.richTooltipPanel.getTooltipInfo();
            FontRenderContext fontRenderContext = new FontRenderContext(new AffineTransform(), true, false);
            if (richTooltip.getMainImage() != null) {
                n3 += richTooltip.getMainImage().getWidth(null);
            }
            int n4 = container.getFontMetrics(fontUIResource).getHeight();
            int n5 = insets.top;
            int n6 = 0;
            AttributedString attributedString = new AttributedString(richTooltip.getTitle());
            attributedString.addAttribute(TextAttribute.FONT, font);
            LineBreakMeasurer lineBreakMeasurer = new LineBreakMeasurer(attributedString.getIterator(), fontRenderContext);
            int n7 = 0;
            while ((textLayout = lineBreakMeasurer.nextLayout(n2)) != null) {
                n6 += n4;
                int n8 = (int)Math.ceil(textLayout.getBounds().getWidth());
                n7 = Math.max(n7, n8);
            }
            n5 += n6;
            int n9 = 0;
            for (String string : richTooltip.getDescriptionSections()) {
                AttributedString attributedString2 = new AttributedString(string);
                attributedString2.addAttribute(TextAttribute.FONT, fontUIResource);
                LineBreakMeasurer object2 = new LineBreakMeasurer(attributedString2.getIterator(), fontRenderContext);
                while ((object = object2.nextLayout(n2)) != null) {
                    n9 += n4;
                }
                n9 += n4;
            }
            if (!richTooltip.getDescriptionSections().isEmpty()) {
                n9 -= n4;
                n9 += n;
            }
            n5 = richTooltip.getMainImage() != null ? (n5 += Math.max(n9, new JLabel((Icon)new ImageIcon((Image)richTooltip.getMainImage())).getPreferredSize().height + n)) : (n5 += n9);
            if (richTooltip.getFooterImage() != null || richTooltip.getFooterSections().size() > 0) {
                n5 += n;
                n5 += new JSeparator((int)0).getPreferredSize().height;
                n5 += n;
                int n10 = 0;
                int n11 = n2;
                if (richTooltip.getFooterImage() != null) {
                    n11 -= richTooltip.getFooterImage().getWidth(null);
                }
                if (richTooltip.getMainImage() != null) {
                    n11 += richTooltip.getMainImage().getWidth(null);
                }
                for (String string2 : richTooltip.getFooterSections()) {
                    TextLayout textLayout2;
                    object = new AttributedString(string2);
                    object.addAttribute(TextAttribute.FONT, fontUIResource);
                    LineBreakMeasurer lineBreakMeasurer2 = new LineBreakMeasurer(object.getIterator(), fontRenderContext);
                    while ((textLayout2 = lineBreakMeasurer2.nextLayout(n11)) != null) {
                        n10 += n4;
                    }
                    n10 += n4;
                }
                n5 = richTooltip.getFooterImage() != null ? (n5 += Math.max(n10, new JLabel((Icon)new ImageIcon((Image)richTooltip.getFooterImage())).getPreferredSize().height)) : (n5 += (n10 -= n4));
            }
            n5 += insets.bottom;
            if (richTooltip.getDescriptionSections().isEmpty() && richTooltip.getMainImage() == null && richTooltip.getFooterSections().isEmpty() && richTooltip.getFooterImage() == null) {
                n3 = n7 + 1 + insets.left + insets.right;
            }
            return new Dimension(n3, n5);
        }

        @Override
        public void layoutContainer(Container container) {
            Object object;
            Object object2;
            int n;
            Object object3;
            int n2;
            Object object4;
            TextLayout textLayout;
            String string /* !! */ ;
            BasicRichTooltipPanelUI.this.removeExistingComponents();
            FontUIResource fontUIResource = FlamingoUtilities.getFont(container, "Ribbon.font", "Button.font", "Panel.font");
            Insets insets = BasicRichTooltipPanelUI.this.richTooltipPanel.getInsets();
            int n3 = insets.top;
            RichTooltip richTooltip = BasicRichTooltipPanelUI.this.richTooltipPanel.getTooltipInfo();
            FontRenderContext fontRenderContext = new FontRenderContext(new AffineTransform(), true, false);
            int n4 = BasicRichTooltipPanelUI.this.getLayoutGap();
            int n5 = container.getFontMetrics(fontUIResource).getHeight();
            Font font = fontUIResource.deriveFont(1);
            boolean bl = BasicRichTooltipPanelUI.this.richTooltipPanel.getComponentOrientation().isLeftToRight();
            int n6 = container.getWidth() - insets.left - insets.right;
            AttributedString attributedString = new AttributedString(richTooltip.getTitle());
            attributedString.addAttribute(TextAttribute.FONT, font);
            LineBreakMeasurer lineBreakMeasurer = new LineBreakMeasurer(attributedString.getIterator(), fontRenderContext);
            int n7 = 0;
            while ((textLayout = lineBreakMeasurer.nextLayout(n6)) != null) {
                n = textLayout.getCharacterCount();
                String string2 = richTooltip.getTitle().substring(n7, n7 + n);
                JLabel iterator = new JLabel(string2);
                iterator.setFont(font);
                BasicRichTooltipPanelUI.this.titleLabels.add(iterator);
                BasicRichTooltipPanelUI.this.richTooltipPanel.add(iterator);
                int n8 = iterator.getPreferredSize().width;
                if (bl) {
                    iterator.setBounds(insets.left, n3, n8, n5);
                } else {
                    iterator.setBounds(container.getWidth() - insets.right - n8, n3, n8, n5);
                }
                n3 += iterator.getHeight();
                n7 += n;
            }
            n3 += n4;
            int n9 = n2 = bl ? insets.left : container.getWidth() - insets.right;
            if (richTooltip.getMainImage() != null) {
                BasicRichTooltipPanelUI.this.mainImageLabel = new JLabel(new ImageIcon(richTooltip.getMainImage()));
                BasicRichTooltipPanelUI.this.richTooltipPanel.add(BasicRichTooltipPanelUI.this.mainImageLabel);
                n = BasicRichTooltipPanelUI.this.mainImageLabel.getPreferredSize().width;
                if (bl) {
                    BasicRichTooltipPanelUI.this.mainImageLabel.setBounds(n2, n3, n, BasicRichTooltipPanelUI.this.mainImageLabel.getPreferredSize().height);
                    n2 += n;
                } else {
                    BasicRichTooltipPanelUI.this.mainImageLabel.setBounds(n2 - n, n3, n, BasicRichTooltipPanelUI.this.mainImageLabel.getPreferredSize().height);
                    n2 -= n;
                }
            }
            n2 = bl ? (n2 += 2 * n4) : (n2 -= 2 * n4);
            n = bl ? container.getWidth() - n2 - insets.right : n2 - insets.left;
            for (String string2 : richTooltip.getDescriptionSections()) {
                TextLayout textLayout2;
                AttributedString attributedString2 = new AttributedString(string2);
                attributedString2.addAttribute(TextAttribute.FONT, fontUIResource);
                object2 = new LineBreakMeasurer(attributedString2.getIterator(), fontRenderContext);
                int n10 = 0;
                while ((textLayout2 = object2.nextLayout(n)) != null) {
                    object3 = textLayout2.getCharacterCount();
                    string /* !! */  = string2.substring(n10, n10 + object3);
                    object = new JLabel(string /* !! */ );
                    object.setFont(fontUIResource);
                    BasicRichTooltipPanelUI.this.descriptionLabels.add((JLabel)object);
                    BasicRichTooltipPanelUI.this.richTooltipPanel.add((Component)object);
                    object4 = object.getPreferredSize().width;
                    if (bl) {
                        object.setBounds(n2, n3, (int)object4, n5);
                    } else {
                        object.setBounds(n2 - object4, n3, (int)object4, n5);
                    }
                    n3 += object.getHeight();
                    n10 += object3;
                }
                n3 += n5;
            }
            n3 -= n5;
            if (BasicRichTooltipPanelUI.this.mainImageLabel != null) {
                n3 = Math.max(n3, BasicRichTooltipPanelUI.this.mainImageLabel.getY() + BasicRichTooltipPanelUI.this.mainImageLabel.getHeight());
            }
            if (richTooltip.getFooterImage() != null || richTooltip.getFooterSections().size() > 0) {
                BasicRichTooltipPanelUI.this.footerSeparator = new JSeparator(0);
                BasicRichTooltipPanelUI.this.richTooltipPanel.add(BasicRichTooltipPanelUI.this.footerSeparator);
                BasicRichTooltipPanelUI.this.footerSeparator.setBounds(insets.left, n3 += n4, container.getWidth() - insets.left - insets.right, BasicRichTooltipPanelUI.this.footerSeparator.getPreferredSize().height);
                n3 += BasicRichTooltipPanelUI.this.footerSeparator.getHeight() + n4;
                int n11 = n2 = bl ? insets.left : container.getWidth() - insets.right;
                if (richTooltip.getFooterImage() != null) {
                    BasicRichTooltipPanelUI.this.footerImageLabel = new JLabel(new ImageIcon(richTooltip.getFooterImage()));
                    BasicRichTooltipPanelUI.this.richTooltipPanel.add(BasicRichTooltipPanelUI.this.footerImageLabel);
                    int n12 = BasicRichTooltipPanelUI.this.footerImageLabel.getPreferredSize().width;
                    if (bl) {
                        BasicRichTooltipPanelUI.this.footerImageLabel.setBounds(n2, n3, n12, BasicRichTooltipPanelUI.this.footerImageLabel.getPreferredSize().height);
                        n2 += n12 + 2 * n4;
                    } else {
                        BasicRichTooltipPanelUI.this.footerImageLabel.setBounds(n2 - n12, n3, n12, BasicRichTooltipPanelUI.this.footerImageLabel.getPreferredSize().height);
                        n2 -= n12 + 2 * n4;
                    }
                }
                int n13 = bl ? container.getWidth() - n2 - insets.right : n2 - insets.left;
                for (String string3 : richTooltip.getFooterSections()) {
                    object2 = new AttributedString(string3);
                    object2.addAttribute(TextAttribute.FONT, fontUIResource);
                    LineBreakMeasurer lineBreakMeasurer2 = new LineBreakMeasurer(object2.getIterator(), fontRenderContext);
                    int n12 = 0;
                    while ((object3 = (Object)lineBreakMeasurer2.nextLayout(n13)) != null) {
                        string /* !! */  = (String)object3.getCharacterCount();
                        object = string3.substring(n12, n12 + string /* !! */ );
                        object4 = new JLabel((String)object);
                        object4.setFont(fontUIResource);
                        BasicRichTooltipPanelUI.this.footerLabels.add((JLabel)object4);
                        BasicRichTooltipPanelUI.this.richTooltipPanel.add((Component)object4);
                        int n14 = object4.getPreferredSize().width;
                        if (bl) {
                            object4.setBounds(n2, n3, n14, n5);
                        } else {
                            object4.setBounds(n2 - n14, n3, n14, n5);
                        }
                        n3 += object4.getHeight();
                        n12 += string /* !! */ ;
                    }
                    n3 += n5;
                }
                n3 -= n5;
            }
        }
    }

}

