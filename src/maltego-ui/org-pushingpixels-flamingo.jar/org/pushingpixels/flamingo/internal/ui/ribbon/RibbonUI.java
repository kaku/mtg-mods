/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon;

import java.awt.Rectangle;
import java.awt.event.MouseWheelEvent;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.ribbon.RibbonContextualTaskGroup;

public abstract class RibbonUI
extends ComponentUI {
    public abstract Rectangle getContextualTaskGroupBounds(RibbonContextualTaskGroup var1);

    public abstract boolean isShowingScrollsForTaskToggleButtons();

    public abstract boolean isShowingScrollsForBands();

    public abstract void handleMouseWheelEvent(MouseWheelEvent var1);
}

