/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common.popup;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.popup.JColorSelectorPopupMenu;
import org.pushingpixels.flamingo.internal.ui.common.popup.BasicColorSelectorComponentUI;
import org.pushingpixels.flamingo.internal.ui.common.popup.ColorSelectorComponentUI;

public class JColorSelectorComponent
extends JComponent {
    private Color color;
    private List<JColorSelectorPopupMenu.ColorSelectorCallback> colorChooserCallbacks;
    private boolean isTopOpen;
    private boolean isBottomOpen;
    public static final String uiClassID = "ColorSelectorComponentUI";

    public JColorSelectorComponent(Color color, JColorSelectorPopupMenu.ColorSelectorCallback colorSelectorCallback) {
        this.setOpaque(true);
        this.color = color;
        this.colorChooserCallbacks = new ArrayList<JColorSelectorPopupMenu.ColorSelectorCallback>();
        this.colorChooserCallbacks.add(colorSelectorCallback);
        this.updateUI();
    }

    @Override
    public void updateUI() {
        if (UIManager.get(this.getUIClassID()) != null) {
            this.setUI((ColorSelectorComponentUI)UIManager.getUI(this));
        } else {
            this.setUI(BasicColorSelectorComponentUI.createUI(this));
        }
    }

    @Override
    public String getUIClassID() {
        return "ColorSelectorComponentUI";
    }

    public Color getColor() {
        return this.color;
    }

    public synchronized void addColorSelectorCallback(JColorSelectorPopupMenu.ColorSelectorCallback colorSelectorCallback) {
        this.colorChooserCallbacks.add(colorSelectorCallback);
    }

    public synchronized void onColorSelected(Color color) {
        for (JColorSelectorPopupMenu.ColorSelectorCallback colorSelectorCallback : this.colorChooserCallbacks) {
            colorSelectorCallback.onColorSelected(color);
        }
    }

    public synchronized void onColorRollover(Color color) {
        for (JColorSelectorPopupMenu.ColorSelectorCallback colorSelectorCallback : this.colorChooserCallbacks) {
            colorSelectorCallback.onColorRollover(color);
        }
    }

    public void setTopOpen(boolean bl) {
        this.isTopOpen = bl;
    }

    public void setBottomOpen(boolean bl) {
        this.isBottomOpen = bl;
    }

    public boolean isTopOpen() {
        return this.isTopOpen;
    }

    public boolean isBottomOpen() {
        return this.isBottomOpen;
    }
}

