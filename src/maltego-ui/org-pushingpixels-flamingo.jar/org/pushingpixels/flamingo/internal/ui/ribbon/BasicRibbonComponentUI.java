/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon;

import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImageOp;
import java.awt.image.ColorConvertOp;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.HorizontalAlignment;
import org.pushingpixels.flamingo.api.common.icon.FilteredResizableIcon;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.ribbon.JRibbonComponent;
import org.pushingpixels.flamingo.api.ribbon.RibbonElementPriority;
import org.pushingpixels.flamingo.internal.ui.ribbon.RibbonComponentUI;

public class BasicRibbonComponentUI
extends RibbonComponentUI {
    protected JRibbonComponent ribbonComponent;
    protected JLabel captionLabel;
    protected PropertyChangeListener propertyChangeListener;
    protected ResizableIcon disabledIcon;

    public static ComponentUI createUI(JComponent jComponent) {
        return new BasicRibbonComponentUI();
    }

    @Override
    public void installUI(JComponent jComponent) {
        this.ribbonComponent = (JRibbonComponent)jComponent;
        this.installDefaults();
        this.installComponents();
        this.installListeners();
        jComponent.setLayout(this.createLayoutManager());
    }

    @Override
    public void uninstallUI(JComponent jComponent) {
        jComponent.setLayout(null);
        this.uninstallListeners();
        this.uninstallDefaults();
        this.uninstallComponents();
    }

    protected void installDefaults() {
        ResizableIcon resizableIcon;
        if (!this.ribbonComponent.isSimpleWrapper() && (resizableIcon = this.ribbonComponent.getIcon()) != null) {
            resizableIcon.setDimension(new Dimension(16, 16));
            this.disabledIcon = this.createDisabledIcon();
        }
        this.ribbonComponent.getMainComponent().setOpaque(false);
        this.ribbonComponent.setOpaque(false);
    }

    protected void installComponents() {
        this.captionLabel = new JLabel(this.ribbonComponent.getCaption());
        this.captionLabel.setEnabled(this.ribbonComponent.isEnabled());
        this.ribbonComponent.add(this.captionLabel);
        JComponent jComponent = this.ribbonComponent.getMainComponent();
        this.ribbonComponent.add(jComponent);
    }

    protected void installListeners() {
        this.propertyChangeListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("enabled".equals(propertyChangeEvent.getPropertyName())) {
                    boolean bl = (Boolean)propertyChangeEvent.getNewValue();
                    BasicRibbonComponentUI.this.ribbonComponent.getMainComponent().setEnabled(bl);
                    if (!BasicRibbonComponentUI.this.ribbonComponent.isSimpleWrapper()) {
                        BasicRibbonComponentUI.this.captionLabel.setEnabled(bl);
                    }
                    BasicRibbonComponentUI.this.ribbonComponent.repaint();
                }
                if ("caption".equals(propertyChangeEvent.getPropertyName())) {
                    BasicRibbonComponentUI.this.captionLabel.setText((String)propertyChangeEvent.getNewValue());
                }
                if ("displayPriority".equals(propertyChangeEvent.getPropertyName())) {
                    BasicRibbonComponentUI.this.ribbonComponent.revalidate();
                    BasicRibbonComponentUI.this.ribbonComponent.doLayout();
                }
            }
        };
        this.ribbonComponent.addPropertyChangeListener(this.propertyChangeListener);
    }

    protected void uninstallDefaults() {
    }

    protected void uninstallComponents() {
        JComponent jComponent = this.ribbonComponent.getMainComponent();
        this.ribbonComponent.remove(jComponent);
        this.ribbonComponent.remove(this.captionLabel);
        this.captionLabel = null;
    }

    protected void uninstallListeners() {
        this.ribbonComponent.removePropertyChangeListener(this.propertyChangeListener);
        this.propertyChangeListener = null;
    }

    @Override
    public Point getKeyTipAnchorCenterPoint() {
        if (this.ribbonComponent.isSimpleWrapper()) {
            return new Point(this.ribbonComponent.getMainComponent().getX() + 10, this.ribbonComponent.getHeight());
        }
        return new Point(this.captionLabel.getX(), this.ribbonComponent.getHeight());
    }

    protected LayoutManager createLayoutManager() {
        return new ExtComponentLayout();
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        JRibbonComponent jRibbonComponent = (JRibbonComponent)jComponent;
        if (this.isIconVisible(this.ribbonComponent.getDisplayPriority())) {
            ResizableIcon resizableIcon;
            Insets insets = jRibbonComponent.getInsets();
            ResizableIcon resizableIcon2 = resizableIcon = jRibbonComponent.isEnabled() ? jRibbonComponent.getIcon() : this.disabledIcon;
            if (resizableIcon != null) {
                int n = jRibbonComponent.getHeight() - insets.top - insets.bottom;
                int n2 = Math.max(0, insets.top + (n - resizableIcon.getIconHeight()) / 2);
                if (jRibbonComponent.getComponentOrientation().isLeftToRight()) {
                    this.paintIcon(graphics, jRibbonComponent, resizableIcon, insets.left, n2);
                } else {
                    this.paintIcon(graphics, jRibbonComponent, resizableIcon, jRibbonComponent.getWidth() - insets.right - resizableIcon.getIconWidth(), n2);
                }
            }
        }
    }

    protected void paintIcon(Graphics graphics, JRibbonComponent jRibbonComponent, Icon icon, int n, int n2) {
        icon.paintIcon(jRibbonComponent, graphics, n, n2);
    }

    protected int getLayoutGap() {
        return 4;
    }

    protected ResizableIcon createDisabledIcon() {
        return new FilteredResizableIcon(this.ribbonComponent.getIcon(), new ColorConvertOp(ColorSpace.getInstance(1003), null));
    }

    protected boolean isIconVisible(RibbonElementPriority ribbonElementPriority) {
        if (this.ribbonComponent.isSimpleWrapper()) {
            return false;
        }
        return ribbonElementPriority == RibbonElementPriority.TOP || ribbonElementPriority == RibbonElementPriority.MEDIUM;
    }

    protected boolean isCaptionVisible(RibbonElementPriority ribbonElementPriority) {
        if (this.ribbonComponent.isSimpleWrapper()) {
            return false;
        }
        return ribbonElementPriority == RibbonElementPriority.TOP;
    }

    @Override
    public Dimension getPreferredSize(RibbonElementPriority ribbonElementPriority) {
        Object object;
        Insets insets = this.ribbonComponent.getInsets();
        JComponent jComponent = this.ribbonComponent.getMainComponent();
        Dimension dimension = jComponent.getPreferredSize();
        int n = insets.left;
        int n2 = dimension.height;
        if (this.isIconVisible(ribbonElementPriority) && (object = this.ribbonComponent.getIcon()) != null) {
            n += object.getIconWidth() + this.getLayoutGap();
            n2 = Math.max(n2, object.getIconHeight());
        }
        if (this.isCaptionVisible(ribbonElementPriority)) {
            object = this.captionLabel.getPreferredSize();
            n += object.width + this.getLayoutGap();
            n2 = Math.max(n2, object.height);
        }
        n += dimension.width;
        return new Dimension(n += insets.right, n2 += insets.top + insets.bottom);
    }

    protected class ExtComponentLayout
    implements LayoutManager {
        protected ExtComponentLayout() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            Object object;
            Insets insets = BasicRibbonComponentUI.this.ribbonComponent.getInsets();
            JComponent jComponent = BasicRibbonComponentUI.this.ribbonComponent.getMainComponent();
            Dimension dimension = jComponent.getMinimumSize();
            int n = insets.left;
            int n2 = dimension.height;
            if (BasicRibbonComponentUI.this.isIconVisible(BasicRibbonComponentUI.this.ribbonComponent.getDisplayPriority()) && (object = BasicRibbonComponentUI.this.ribbonComponent.getIcon()) != null) {
                n += object.getIconWidth() + BasicRibbonComponentUI.this.getLayoutGap();
                n2 = Math.max(n2, object.getIconHeight());
            }
            if (BasicRibbonComponentUI.this.isCaptionVisible(BasicRibbonComponentUI.this.ribbonComponent.getDisplayPriority())) {
                object = BasicRibbonComponentUI.this.captionLabel.getMinimumSize();
                n += object.width + BasicRibbonComponentUI.this.getLayoutGap();
                n2 = Math.max(n2, object.height);
            }
            n += dimension.width;
            return new Dimension(n += insets.right, n2 += insets.top + insets.bottom);
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            return BasicRibbonComponentUI.this.getPreferredSize(BasicRibbonComponentUI.this.ribbonComponent.getDisplayPriority());
        }

        @Override
        public void layoutContainer(Container container) {
            JRibbonComponent jRibbonComponent = (JRibbonComponent)container;
            Insets insets = jRibbonComponent.getInsets();
            int n = BasicRibbonComponentUI.this.getLayoutGap();
            int n2 = jRibbonComponent.getHeight() - insets.top - insets.bottom;
            int n3 = jRibbonComponent.getWidth() - insets.left - insets.right;
            HorizontalAlignment horizontalAlignment = jRibbonComponent.getHorizontalAlignment();
            JComponent jComponent = jRibbonComponent.getMainComponent();
            Dimension dimension = jComponent.getPreferredSize();
            int n4 = dimension.width;
            int n5 = Math.min(dimension.height, n2);
            boolean bl = jRibbonComponent.getComponentOrientation().isLeftToRight();
            if (jRibbonComponent.isSimpleWrapper()) {
                int n6 = Math.min(n3, n4);
                int n7 = n3 - n4;
                int n8 = insets.top + (n2 - n5) / 2;
                int n9 = bl ? insets.left : jRibbonComponent.getWidth() - insets.right;
                switch (horizontalAlignment) {
                    case LEADING: {
                        if (bl) {
                            jComponent.setBounds(n9, n8, n6, n5);
                            break;
                        }
                        jComponent.setBounds(n9 - n6, n8, n6, n5);
                        break;
                    }
                    case TRAILING: {
                        if (bl) {
                            jComponent.setBounds(n9 + n7, n8, n6, n5);
                            break;
                        }
                        jComponent.setBounds(n9 - n6 - n7, n8, n6, n5);
                        break;
                    }
                    case CENTER: {
                        if (bl) {
                            jComponent.setBounds(n9 + n7 / 2, n8, n6, n5);
                            break;
                        }
                        jComponent.setBounds(n9 - n6 - n7 / 2, n8, n6, n5);
                        break;
                    }
                    case FILL: {
                        if (bl) {
                            jComponent.setBounds(n9, n8, n3, n5);
                            break;
                        }
                        jComponent.setBounds(n9 - n3, n8, n3, n5);
                    }
                }
                jComponent.doLayout();
            } else {
                int n10;
                int n11 = n10 = bl ? insets.left : jRibbonComponent.getWidth() - insets.right;
                if (BasicRibbonComponentUI.this.isIconVisible(BasicRibbonComponentUI.this.ribbonComponent.getDisplayPriority()) && jRibbonComponent.getIcon() != null) {
                    int n12 = jRibbonComponent.getIcon().getIconWidth();
                    int n13 = n10 = bl ? n10 + n12 + n : n10 - n12 - n;
                }
                if (BasicRibbonComponentUI.this.isCaptionVisible(BasicRibbonComponentUI.this.ribbonComponent.getDisplayPriority())) {
                    BasicRibbonComponentUI.this.captionLabel.setVisible(true);
                    Dimension dimension2 = BasicRibbonComponentUI.this.captionLabel.getPreferredSize();
                    if (bl) {
                        BasicRibbonComponentUI.this.captionLabel.setBounds(n10, insets.top + (n2 - dimension2.height) / 2, dimension2.width, dimension2.height);
                        n10 += dimension2.width + n;
                    } else {
                        BasicRibbonComponentUI.this.captionLabel.setBounds(n10 - dimension2.width, insets.top + (n2 - dimension2.height) / 2, dimension2.width, dimension2.height);
                        n10 -= dimension2.width + n;
                    }
                } else {
                    BasicRibbonComponentUI.this.captionLabel.setVisible(false);
                }
                int n14 = insets.top + (n2 - n5) / 2;
                int n15 = bl ? Math.min(jRibbonComponent.getWidth() - insets.right - n10, n4) : Math.min(n10 - insets.left, n4);
                int n16 = bl ? jRibbonComponent.getWidth() - insets.right - n10 - n4 : n10 - n4 - insets.left;
                switch (horizontalAlignment) {
                    case LEADING: {
                        if (bl) {
                            jComponent.setBounds(n10, n14, n15, n5);
                            break;
                        }
                        jComponent.setBounds(n10 - n15, n14, n15, n5);
                        break;
                    }
                    case TRAILING: {
                        if (bl) {
                            jComponent.setBounds(n10 + n16, n14, n15, n5);
                            break;
                        }
                        jComponent.setBounds(n10 - n15 - n16, n14, n15, n5);
                        break;
                    }
                    case CENTER: {
                        if (bl) {
                            jComponent.setBounds(n10 + n16 / 2, n14, n15, n5);
                            break;
                        }
                        jComponent.setBounds(n10 - n15 - n16 / 2, n14, n15, n5);
                        break;
                    }
                    case FILL: {
                        if (bl) {
                            jComponent.setBounds(n10, n14, jRibbonComponent.getWidth() - insets.right - n10, n5);
                            break;
                        }
                        jComponent.setBounds(insets.left, n14, n10 - insets.left, n5);
                    }
                }
                jComponent.doLayout();
            }
        }
    }

}

