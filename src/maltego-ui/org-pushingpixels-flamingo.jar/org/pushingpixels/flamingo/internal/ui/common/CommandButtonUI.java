/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common;

import java.awt.Point;
import javax.swing.plaf.ButtonUI;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;

public abstract class CommandButtonUI
extends ButtonUI {
    public abstract CommandButtonLayoutManager.CommandButtonLayoutInfo getLayoutInfo();

    public abstract Point getKeyTipAnchorCenterPoint();
}

