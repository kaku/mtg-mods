/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon;

import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.JRibbonComponent;
import org.pushingpixels.flamingo.api.ribbon.RibbonElementPriority;
import org.pushingpixels.flamingo.api.ribbon.resize.IconRibbonBandResizePolicy;
import org.pushingpixels.flamingo.api.ribbon.resize.RibbonBandResizePolicy;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanelUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.BasicRibbonBandUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.JBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.JRibbonGallery;

public class BasicBandControlPanelUI
extends AbstractBandControlPanelUI {
    private JSeparator[] groupSeparators;
    private JLabel[] groupLabels;
    protected ChangeListener changeListener;

    public static ComponentUI createUI(JComponent jComponent) {
        return new BasicBandControlPanelUI();
    }

    @Override
    protected LayoutManager createLayoutManager() {
        return new ControlPanelLayout();
    }

    @Override
    protected void installListeners() {
        super.installListeners();
        this.changeListener = new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                BasicBandControlPanelUI.this.syncGroupHeaders();
                BasicBandControlPanelUI.this.controlPanel.revalidate();
            }
        };
        ((JBandControlPanel)this.controlPanel).addChangeListener(this.changeListener);
    }

    @Override
    protected void uninstallListeners() {
        ((JBandControlPanel)this.controlPanel).removeChangeListener(this.changeListener);
        this.changeListener = null;
        super.uninstallListeners();
    }

    @Override
    protected void installComponents() {
        super.installComponents();
        this.syncGroupHeaders();
    }

    @Override
    protected void uninstallComponents() {
        if (this.groupSeparators != null) {
            for (JSeparator jSeparator : this.groupSeparators) {
                this.controlPanel.remove(jSeparator);
            }
        }
        if (this.groupLabels != null) {
            for (JSeparator jSeparator : this.groupLabels) {
                if (jSeparator == null) continue;
                this.controlPanel.remove(jSeparator);
            }
        }
        super.uninstallComponents();
    }

    protected void syncGroupHeaders() {
        int n;
        int n2;
        JSeparator jSeparator;
        int n3;
        JSeparator[] arrjSeparator;
        if (this.groupSeparators != null) {
            arrjSeparator = this.groupSeparators;
            n3 = arrjSeparator.length;
            for (n2 = 0; n2 < n3; ++n2) {
                jSeparator = arrjSeparator[n2];
                this.controlPanel.remove(jSeparator);
            }
        }
        if (this.groupLabels != null) {
            arrjSeparator = this.groupLabels;
            n3 = arrjSeparator.length;
            for (n2 = 0; n2 < n3; ++n2) {
                jSeparator = arrjSeparator[n2];
                if (jSeparator == null) continue;
                this.controlPanel.remove(jSeparator);
            }
        }
        if ((n = ((JBandControlPanel)this.controlPanel).getControlPanelGroupCount()) > 1) {
            this.groupSeparators = new JSeparator[n - 1];
            for (n3 = 0; n3 < n - 1; ++n3) {
                this.groupSeparators[n3] = new JSeparator(1);
                this.controlPanel.add(this.groupSeparators[n3]);
            }
        }
        if (n > 0) {
            this.groupLabels = new JLabel[n];
            for (n3 = 0; n3 < n; ++n3) {
                String string = ((JBandControlPanel)this.controlPanel).getControlPanelGroupTitle(n3);
                if (string == null) continue;
                this.groupLabels[n3] = new JLabel(string);
                this.controlPanel.add(this.groupLabels[n3]);
            }
        }
    }

    private class ControlPanelLayout
    implements LayoutManager {
        private ControlPanelLayout() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            int n = BasicBandControlPanelUI.this.dummy.getPreferredSize().height;
            int n2 = BasicBandControlPanelUI.this.getLayoutGap() * 3 / 4;
            int n3 = n - 2 * n2;
            switch (n3 % 3) {
                case 1: {
                    n += 2;
                    break;
                }
                case 2: {
                    ++n;
                }
            }
            Insets insets = container.getInsets();
            return new Dimension(container.getWidth(), n + insets.top + insets.bottom);
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            return this.preferredLayoutSize(container);
        }

        @Override
        public void layoutContainer(Container container) {
            AbstractRibbonBand abstractRibbonBand = ((JBandControlPanel)container).getRibbonBand();
            RibbonBandResizePolicy ribbonBandResizePolicy = abstractRibbonBand.getCurrentResizePolicy();
            if (ribbonBandResizePolicy == null) {
                return;
            }
            boolean bl = container.getComponentOrientation().isLeftToRight();
            Insets insets = container.getInsets();
            int n = BasicBandControlPanelUI.this.getLayoutGap();
            int n2 = bl ? insets.left + n / 2 : container.getWidth() - insets.right - n / 2;
            int n3 = container.getHeight() - insets.top - insets.bottom;
            if (SwingUtilities.getAncestorOfClass(BasicRibbonBandUI.CollapsedButtonPopupPanel.class, container) != null) {
                List<RibbonBandResizePolicy> list = abstractRibbonBand.getResizePolicies();
                list.get(0).install(n3, n);
            } else {
                if (ribbonBandResizePolicy instanceof IconRibbonBandResizePolicy) {
                    return;
                }
                ribbonBandResizePolicy.install(n3, n);
            }
            int n4 = 0;
            for (JBandControlPanel.ControlPanelGroup controlPanelGroup : ((JBandControlPanel)BasicBandControlPanelUI.this.controlPanel).getControlPanelGroups()) {
                JLabel object32 /* !! */ ;
                int n8;
                Object object;
                Component component;
                Object object2;
                Object object33;
                int n5;
                Object object4;
                Object object5;
                int n7;
                int n6;
                if (n4 > 0) {
                    n6 = BasicBandControlPanelUI.access$100((BasicBandControlPanelUI)BasicBandControlPanelUI.this)[n4 - 1].getPreferredSize().width;
                    n8 = bl ? n2 - n + (n - n6) / 2 : n2 + n / 2 - (n - n6) / 2;
                    BasicBandControlPanelUI.this.groupSeparators[n4 - 1].setBounds(n8, insets.top, n6, n3);
                }
                n6 = 0;
                n8 = controlPanelGroup.isCoreContent();
                if (n8 != 0) {
                    int n9 = n3 / 3;
                    int n10 = controlPanelGroup.getGroupTitle() != null ? 1 : 0;
                    n7 = 0;
                    if (n10 != 0) {
                        object32 /* !! */  = BasicBandControlPanelUI.this.groupLabels[n4];
                        int n14 = object32 /* !! */ .getPreferredSize().width;
                        object33 = Math.min(n9 - n / 4, object32 /* !! */ .getPreferredSize().height);
                        n5 = n9 - object33;
                        object = object33 > 0 ? object32 /* !! */ .getBaseline(n14, (int)object33) : 0;
                        Object object6 = object;
                        if (bl) {
                            object32 /* !! */ .setBounds(n2 + n, insets.top + n5 - object33 + object, n14, (int)object33);
                        } else {
                            object32 /* !! */ .setBounds(n2 - n - n14, insets.top + n5 - object33 + object, n14, (int)object33);
                        }
                        n7 = n + n14;
                    }
                    List<JRibbonComponent> list = controlPanelGroup.getRibbonComps();
                    Map<JRibbonComponent, Integer> map = controlPanelGroup.getRibbonCompsRowSpans();
                    ArrayList<Component> arrayList = new ArrayList<Component>();
                    n5 = n10 != 0 ? 1 : 0;
                    object = n5;
                    for (int i = 0; i < list.size(); ++i) {
                        component = list.get(i);
                        object5 = component.getPreferredSize().width;
                        object2 = map.get(component);
                        object4 = object + object2;
                        if (object4 > 3) {
                            if (bl) {
                                if (n6 != 0) {
                                    n2 += n;
                                }
                                n2 += n7;
                            } else {
                                if (n6 != 0) {
                                    n2 -= n;
                                }
                                n2 -= n7;
                            }
                            n6 = 1;
                            n7 = 0;
                            object = n5;
                            arrayList.clear();
                        }
                        int n11 = Math.min(object2 * n9 - n / 4, component.getPreferredSize().height);
                        int n12 = object2 * n9 - n11;
                        reference var29_55 = object * n9 + insets.top;
                        if (bl) {
                            component.setBounds(n2, (int)(var29_55 + n12), (int)object5, n11);
                        } else {
                            component.setBounds(n2 - object5, (int)(var29_55 + n12), (int)object5, n11);
                        }
                        n7 = Math.max(n7, (int)object5);
                        arrayList.add(component);
                        component.putClientProperty("flamingo.internal.ribbonBandControlPanel.topRow", object == false);
                        component.putClientProperty("flamingo.internal.ribbonBandControlPanel.midRow", object > 0 && object < 2);
                        component.putClientProperty("flamingo.internal.ribbonBandControlPanel.bottomRow", object == 2);
                        for (JRibbonComponent jRibbonComponent : arrayList) {
                            Rectangle rectangle = jRibbonComponent.getBounds();
                            if (bl) {
                                jRibbonComponent.setBounds(rectangle.x, rectangle.y, n7, rectangle.height);
                            } else {
                                jRibbonComponent.setBounds(rectangle.x + rectangle.width - n7, rectangle.y, n7, rectangle.height);
                            }
                            jRibbonComponent.doLayout();
                        }
                        object += object2;
                    }
                    if (object > 0 && object <= 3) {
                        if (bl) {
                            if (n6 != 0) {
                                n2 += n;
                            }
                            n2 += n7;
                        } else {
                            if (n6 != 0) {
                                n2 -= n;
                            }
                            n2 -= n7;
                        }
                        n6 = 1;
                    }
                } else {
                    int n13;
                    void var17_25;
                    for (RibbonElementPriority abstractCommandButton : RibbonElementPriority.values()) {
                        for (Object object33 : controlPanelGroup.getRibbonGalleries(abstractCommandButton)) {
                            n5 = object33.getPreferredWidth(object33.getDisplayPriority(), n3);
                            if (bl) {
                                object33.setBounds(n2, insets.top, n5, n3);
                                if (n6 != 0) {
                                    n2 += n;
                                }
                                n2 += n5;
                            } else {
                                object33.setBounds(n2 - n5, insets.top, n5, n3);
                                if (n6 != 0) {
                                    n2 -= n;
                                }
                                n2 -= n5;
                            }
                            n6 = 1;
                        }
                    }
                    HashMap hashMap = new HashMap();
                    Object list = RibbonElementPriority.values();
                    n7 = list.length;
                    boolean bl2 = false;
                    while (++var17_25 < n7) {
                        RibbonElementPriority ribbonElementPriority = list[var17_25];
                        object33 = controlPanelGroup.getRibbonButtons(ribbonElementPriority).iterator();
                        while (object33.hasNext()) {
                            AbstractCommandButton abstractCommandButton2 = (AbstractCommandButton)object33.next();
                            CommandButtonDisplayState commandButtonDisplayState = abstractCommandButton2.getDisplayState();
                            if (hashMap.get(commandButtonDisplayState) == null) {
                                hashMap.put(commandButtonDisplayState, new ArrayList());
                            }
                            ((List)hashMap.get(commandButtonDisplayState)).add(abstractCommandButton2);
                        }
                    }
                    list = (List)hashMap.get(CommandButtonDisplayState.BIG);
                    if (list != null) {
                        Iterator iterator = list.iterator();
                        while (iterator.hasNext()) {
                            AbstractCommandButton abstractCommandButton3 = (AbstractCommandButton)iterator.next();
                            int n14 = abstractCommandButton3.getPreferredSize().width;
                            if (n6 != 0) {
                                n2 = bl ? (n2 += n) : (n2 -= n);
                            }
                            if (bl) {
                                abstractCommandButton3.setBounds(n2, insets.top, n14, n3);
                            } else {
                                abstractCommandButton3.setBounds(n2 - n14, insets.top, n14, n3);
                            }
                            abstractCommandButton3.putClientProperty("flamingo.internal.ribbonBandControlPanel.topRow", Boolean.FALSE);
                            abstractCommandButton3.putClientProperty("flamingo.internal.ribbonBandControlPanel.midRow", Boolean.FALSE);
                            abstractCommandButton3.putClientProperty("flamingo.internal.ribbonBandControlPanel.bottomRow", Boolean.FALSE);
                            n2 = bl ? (n2 += n14) : (n2 -= n14);
                            n6 = 1;
                        }
                    }
                    n7 = n * 3 / 4;
                    object32 /* !! */  = (JLabel)((n3 - 2 * n7) / 3);
                    boolean bl3 = false;
                    int n15 = 0;
                    List list2 = (List)hashMap.get(CommandButtonDisplayState.MEDIUM);
                    if (list2 != null) {
                        for (AbstractCommandButton abstractCommandButton4 : list2) {
                            int n16;
                            component = (Component)abstractCommandButton4.getPreferredSize().width;
                            n15 = Math.max(n15, (int)component);
                            if (n6 != 0 && n16 == 0) {
                                n2 = bl ? (n2 += n) : (n2 -= n);
                            }
                            object5 = (object32 /* !! */  + n7) * n16;
                            object2 = (object32 /* !! */  + n7) * (n16 + true) - n7;
                            if (bl) {
                                abstractCommandButton4.setBounds(n2, insets.top + object5, (int)component, (int)(object2 - object5));
                            } else {
                                abstractCommandButton4.setBounds(n2 - component, insets.top + object5, (int)component, (int)(object2 - object5));
                            }
                            abstractCommandButton4.putClientProperty("flamingo.internal.ribbonBandControlPanel.topRow", n16 == 0);
                            abstractCommandButton4.putClientProperty("flamingo.internal.ribbonBandControlPanel.midRow", n16 == 1);
                            abstractCommandButton4.putClientProperty("flamingo.internal.ribbonBandControlPanel.bottomRow", n16 == 2);
                            if ((n16 += 1) != 3) continue;
                            n16 = 0;
                            n2 = bl ? (n2 += n15) : (n2 -= n15);
                            n6 = 1;
                            n15 = 0;
                        }
                    }
                    if (n15 > 0) {
                        n2 = bl ? (n2 += n15) : (n2 -= n15);
                        n6 = 1;
                    }
                    boolean bl4 = false;
                    n15 = 0;
                    object = (List)hashMap.get(CommandButtonDisplayState.SMALL);
                    if (object != null) {
                        Iterator iterator = object.iterator();
                        while (iterator.hasNext()) {
                            AbstractCommandButton abstractCommandButton5 = (AbstractCommandButton)iterator.next();
                            object5 = abstractCommandButton5.getPreferredSize().width;
                            n15 = Math.max(n15, (int)object5);
                            if (n6 != 0 && n13 == 0) {
                                n2 = bl ? (n2 += n) : (n2 -= n);
                            }
                            object2 = (object32 /* !! */  + n7) * n13;
                            object4 = (object32 /* !! */  + n7) * (n13 + true) - n7;
                            if (bl) {
                                abstractCommandButton5.setBounds(n2, insets.top + object2, (int)object5, (int)(object4 - object2));
                            } else {
                                abstractCommandButton5.setBounds(n2 - object5, insets.top + object2, (int)object5, (int)(object4 - object2));
                            }
                            abstractCommandButton5.putClientProperty("flamingo.internal.ribbonBandControlPanel.topRow", n13 == 0);
                            abstractCommandButton5.putClientProperty("flamingo.internal.ribbonBandControlPanel.midRow", n13 == 1);
                            abstractCommandButton5.putClientProperty("flamingo.internal.ribbonBandControlPanel.bottomRow", n13 == 2);
                            if ((n13 += 1) != 3) continue;
                            n13 = 0;
                            n2 = bl ? (n2 += n15) : (n2 -= n15);
                            n6 = 1;
                            n15 = 0;
                        }
                    }
                    if (n13 < 3 && n15 > 0) {
                        n2 = bl ? (n2 += n15) : (n2 -= n15);
                        n6 = 1;
                    }
                }
                n2 = bl ? (n2 += n * 3 / 2) : (n2 -= n * 3 / 2);
                ++n4;
            }
        }
    }

}

