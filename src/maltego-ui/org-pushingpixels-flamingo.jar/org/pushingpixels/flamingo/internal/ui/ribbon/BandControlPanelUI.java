/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon;

import javax.swing.plaf.PanelUI;

public abstract class BandControlPanelUI
extends PanelUI {
    public abstract int getLayoutGap();
}

