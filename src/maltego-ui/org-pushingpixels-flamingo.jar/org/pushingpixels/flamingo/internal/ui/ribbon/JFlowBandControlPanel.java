/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon;

import java.awt.Component;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.PanelUI;
import javax.swing.plaf.UIResource;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.BandControlPanelUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.BasicFlowBandControlPanelUI;

public class JFlowBandControlPanel
extends AbstractBandControlPanel
implements UIResource {
    private List<JComponent> comps = new LinkedList<JComponent>();
    public static final String uiClassID = "FlowBandControlPanelUI";

    public void setUI(BandControlPanelUI bandControlPanelUI) {
        super.setUI(bandControlPanelUI);
    }

    @Override
    public void updateUI() {
        if (UIManager.get(this.getUIClassID()) != null) {
            this.setUI((BandControlPanelUI)UIManager.getUI(this));
        } else {
            this.setUI(new BasicFlowBandControlPanelUI());
        }
    }

    @Override
    public BandControlPanelUI getUI() {
        return (BandControlPanelUI)this.ui;
    }

    @Override
    public String getUIClassID() {
        return "FlowBandControlPanelUI";
    }

    public void addFlowComponent(JComponent jComponent) {
        this.comps.add(jComponent);
        super.add(jComponent);
    }

    public List<JComponent> getFlowComponents() {
        return this.comps;
    }
}

