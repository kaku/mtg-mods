/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon.appmenu;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.icon.EmptyResizableIcon;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;
import org.pushingpixels.flamingo.internal.ui.common.BasicCommandButtonUI;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.BasicRibbonApplicationMenuButtonUI;

public class JRibbonApplicationMenuButton
extends JCommandButton {
    private JRibbon ribbon;
    public static final String uiClassID = "RibbonApplicationMenuButtonUI";
    private static final CommandButtonDisplayState APP_MENU_BUTTON_STATE = new CommandButtonDisplayState("Ribbon Application Menu Button", 24){

        @Override
        public CommandButtonLayoutManager createLayoutManager(AbstractCommandButton abstractCommandButton) {
            return new CommandButtonLayoutManager(){

                @Override
                public int getPreferredIconSize() {
                    return 24;
                }

                @Override
                public CommandButtonLayoutManager.CommandButtonLayoutInfo getLayoutInfo(AbstractCommandButton abstractCommandButton, Graphics graphics) {
                    CommandButtonLayoutManager.CommandButtonLayoutInfo commandButtonLayoutInfo = new CommandButtonLayoutManager.CommandButtonLayoutInfo();
                    commandButtonLayoutInfo.actionClickArea = new Rectangle(0, 0, 0, 0);
                    commandButtonLayoutInfo.popupClickArea = new Rectangle(0, 0, abstractCommandButton.getWidth(), abstractCommandButton.getHeight());
                    commandButtonLayoutInfo.popupActionRect = new Rectangle(0, 0, 0, 0);
                    ResizableIcon resizableIcon = abstractCommandButton.getIcon();
                    commandButtonLayoutInfo.iconRect = new Rectangle((abstractCommandButton.getWidth() - resizableIcon.getIconWidth()) / 2, (abstractCommandButton.getHeight() - resizableIcon.getIconHeight()) / 2, resizableIcon.getIconWidth(), resizableIcon.getIconHeight());
                    commandButtonLayoutInfo.isTextInActionArea = false;
                    return commandButtonLayoutInfo;
                }

                @Override
                public Dimension getPreferredSize(AbstractCommandButton abstractCommandButton) {
                    return new Dimension(40, 40);
                }

                @Override
                public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                }

                @Override
                public Point getKeyTipAnchorCenterPoint(AbstractCommandButton abstractCommandButton) {
                    return new Point(abstractCommandButton.getWidth() / 2, abstractCommandButton.getHeight() / 2);
                }
            };
        }

    };

    public JRibbonApplicationMenuButton(JRibbon jRibbon) {
        super("", new EmptyResizableIcon(16));
        this.setCommandButtonKind(JCommandButton.CommandButtonKind.POPUP_ONLY);
        this.setDisplayState(APP_MENU_BUTTON_STATE);
        this.ribbon = jRibbon;
    }

    @Override
    public void updateUI() {
        if (UIManager.get(this.getUIClassID()) != null) {
            this.setUI((BasicCommandButtonUI)UIManager.getUI(this));
        } else {
            this.setUI(BasicRibbonApplicationMenuButtonUI.createUI(this));
        }
    }

    @Override
    public String getUIClassID() {
        return "RibbonApplicationMenuButtonUI";
    }

    public JRibbon getRibbon() {
        return this.ribbon;
    }

}

