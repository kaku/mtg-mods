/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common.popup;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Composite;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.List;
import javax.swing.CellRendererPane;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.PanelUI;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.common.JCommandButtonPanel;
import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.JCommandToggleMenuButton;
import org.pushingpixels.flamingo.api.common.JScrollablePanel;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.popup.JColorSelectorPopupMenu;
import org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelManager;
import org.pushingpixels.flamingo.internal.ui.common.BasicCommandButtonPanelUI;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonLayoutManagerMedium;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonUI;
import org.pushingpixels.flamingo.internal.ui.common.popup.BasicPopupPanelUI;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public class BasicCommandPopupMenuUI
extends BasicPopupPanelUI {
    protected JCommandPopupMenu popupMenu;
    protected ChangeListener popupMenuChangeListener;
    protected PopupPanelManager.PopupListener popupListener;
    protected ScrollableCommandButtonPanel commandButtonPanel;
    protected JScrollablePanel<JPanel> menuItemsPanel;
    public static final String FORCE_ICON = "flamingo.internal.commandButtonLayoutManagerMedium.forceIcon";
    protected static final CommandButtonDisplayState POPUP_MENU = new CommandButtonDisplayState("Popup menu", 16){

        @Override
        public CommandButtonLayoutManager createLayoutManager(AbstractCommandButton abstractCommandButton) {
            return new CommandButtonLayoutManagerMedium(){

                @Override
                protected float getIconTextGapFactor() {
                    return 2.0f;
                }
            };
        }

    };

    public static ComponentUI createUI(JComponent jComponent) {
        return new BasicCommandPopupMenuUI();
    }

    @Override
    public void installUI(JComponent jComponent) {
        this.popupMenu = (JCommandPopupMenu)jComponent;
        super.installUI(this.popupMenu);
        this.popupMenu.setLayout(this.createLayoutManager());
    }

    @Override
    protected void installComponents() {
        super.installComponents();
        this.syncComponents();
    }

    protected void syncComponents() {
        if (this.popupMenu.hasCommandButtonPanel()) {
            this.commandButtonPanel = this.createScrollableButtonPanel();
            this.popupMenu.add(this.commandButtonPanel);
        }
        final JPanel jPanel = this.createMenuPanel();
        jPanel.setLayout(new LayoutManager(){

            @Override
            public void addLayoutComponent(String string, Component component) {
            }

            @Override
            public void removeLayoutComponent(Component component) {
            }

            @Override
            public Dimension preferredLayoutSize(Container container) {
                int n = 0;
                int n2 = 0;
                for (int i = 0; i < container.getComponentCount(); ++i) {
                    Dimension dimension = container.getComponent(i).getPreferredSize();
                    n += dimension.height;
                    n2 = Math.max(n2, dimension.width);
                }
                Insets insets = container.getInsets();
                return new Dimension(n2 + insets.left + insets.right, n + insets.top + insets.bottom);
            }

            @Override
            public Dimension minimumLayoutSize(Container container) {
                return this.preferredLayoutSize(container);
            }

            @Override
            public void layoutContainer(Container container) {
                Insets insets = container.getInsets();
                int n = insets.top;
                for (int i = 0; i < container.getComponentCount(); ++i) {
                    Component component = container.getComponent(i);
                    Dimension dimension = component.getPreferredSize();
                    component.setBounds(insets.left, n, container.getWidth() - insets.left - insets.right, dimension.height);
                    n += dimension.height;
                }
            }
        });
        this.popupMenu.putClientProperty("flamingo.internal.commandButtonLayoutManagerMedium.forceIcon", null);
        List<Component> list = this.popupMenu.getMenuComponents();
        if (list != null) {
            JComponent jComponent;
            for (Component iterator : list) {
                jPanel.add(iterator);
            }
            boolean bl = false;
            for (Component component2 : list) {
                if (component2 instanceof JCommandMenuButton && (jComponent = (JCommandMenuButton)component2).getIcon() != null) {
                    bl = true;
                }
                if (!(component2 instanceof JCommandToggleMenuButton)) continue;
                bl = true;
            }
            this.popupMenu.putClientProperty("flamingo.internal.commandButtonLayoutManagerMedium.forceIcon", bl ? Boolean.TRUE : null);
            for (Component component2 : list) {
                if (component2 instanceof JCommandMenuButton) {
                    jComponent = (JCommandMenuButton)component2;
                    jComponent.putClientProperty("flamingo.internal.commandButtonLayoutManagerMedium.forceIcon", bl ? Boolean.TRUE : null);
                    jComponent.setDisplayState(POPUP_MENU);
                }
                if (!(component2 instanceof JCommandToggleMenuButton)) continue;
                jComponent = (JCommandToggleMenuButton)component2;
                jComponent.putClientProperty("flamingo.internal.commandButtonLayoutManagerMedium.forceIcon", Boolean.TRUE);
                jComponent.setDisplayState(POPUP_MENU);
            }
        }
        this.menuItemsPanel = new JScrollablePanel<JPanel>(jPanel, JScrollablePanel.ScrollType.VERTICALLY);
        final LayoutManager layoutManager = this.menuItemsPanel.getLayout();
        this.menuItemsPanel.setLayout(new LayoutManager(){

            @Override
            public void addLayoutComponent(String string, Component component) {
                layoutManager.addLayoutComponent(string, component);
            }

            @Override
            public void removeLayoutComponent(Component component) {
                layoutManager.removeLayoutComponent(component);
            }

            @Override
            public Dimension preferredLayoutSize(Container container) {
                Dimension dimension = jPanel.getPreferredSize();
                int n = BasicCommandPopupMenuUI.this.popupMenu.getMaxVisibleMenuButtons();
                if (n < 0 || n >= jPanel.getComponentCount()) {
                    return dimension;
                }
                int n2 = jPanel.getComponent((int)0).getPreferredSize().height;
                int n3 = 0;
                for (int i = 0; i < jPanel.getComponentCount(); ++i) {
                    n3 = Math.max(n3, jPanel.getComponent((int)i).getPreferredSize().width);
                }
                Insets insets = container.getInsets();
                return new Dimension(n3 + insets.left + insets.right, n2 * (n + 2) + insets.top + insets.bottom);
            }

            @Override
            public Dimension minimumLayoutSize(Container container) {
                return this.preferredLayoutSize(container);
            }

            @Override
            public void layoutContainer(Container container) {
                layoutManager.layoutContainer(container);
            }
        });
        this.popupMenu.add(this.menuItemsPanel);
    }

    protected ScrollableCommandButtonPanel createScrollableButtonPanel() {
        return new ScrollableCommandButtonPanel(this.popupMenu.getMainButtonPanel(), this.popupMenu.getMaxButtonColumns(), this.popupMenu.getMaxVisibleButtonRows());
    }

    @Override
    protected void uninstallComponents() {
        this.popupMenu.removeAll();
        super.uninstallComponents();
    }

    @Override
    protected void installListeners() {
        super.installListeners();
        this.popupMenuChangeListener = new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                BasicCommandPopupMenuUI.this.popupMenu.removeAll();
                BasicCommandPopupMenuUI.this.syncComponents();
            }
        };
        this.popupMenu.addChangeListener(this.popupMenuChangeListener);
        this.popupListener = new PopupPanelManager.PopupListener(){

            @Override
            public void popupShown(PopupPanelManager.PopupEvent popupEvent) {
            }

            @Override
            public void popupHidden(PopupPanelManager.PopupEvent popupEvent) {
                if (popupEvent.getSource() instanceof JColorSelectorPopupMenu) {
                    ((JColorSelectorPopupMenu)popupEvent.getSource()).getColorSelectorCallback().onColorRollover(null);
                }
            }
        };
        PopupPanelManager.defaultManager().addPopupListener(this.popupListener);
    }

    @Override
    protected void uninstallListeners() {
        this.popupMenu.removeChangeListener(this.popupMenuChangeListener);
        this.popupMenuChangeListener = null;
        PopupPanelManager.defaultManager().addPopupListener(this.popupListener);
        this.popupListener = null;
        super.uninstallListeners();
    }

    protected JPanel createMenuPanel() {
        return new MenuPanel();
    }

    protected LayoutManager createLayoutManager() {
        return new PopupMenuLayoutManager();
    }

    protected static class MenuPanel
    extends JPanel {
        protected MenuPanel() {
        }

        @Override
        public void paintComponent(Graphics graphics) {
            super.paintComponent(graphics);
            JCommandPopupMenu jCommandPopupMenu = (JCommandPopupMenu)SwingUtilities.getAncestorOfClass(JCommandPopupMenu.class, this);
            if (Boolean.TRUE.equals(jCommandPopupMenu.getClientProperty("flamingo.internal.commandButtonLayoutManagerMedium.forceIcon"))) {
                // empty if block
            }
        }

        protected int getSeparatorX() {
            JCommandPopupMenu jCommandPopupMenu = (JCommandPopupMenu)SwingUtilities.getAncestorOfClass(JCommandPopupMenu.class, this);
            if (!Boolean.TRUE.equals(jCommandPopupMenu.getClientProperty("flamingo.internal.commandButtonLayoutManagerMedium.forceIcon"))) {
                return -1;
            }
            List<Component> list = jCommandPopupMenu.getMenuComponents();
            if (list != null) {
                for (Component component : list) {
                    AbstractCommandButton abstractCommandButton;
                    if (!(component instanceof JCommandMenuButton) && !(component instanceof JCommandToggleMenuButton) || !Boolean.TRUE.equals((abstractCommandButton = (AbstractCommandButton)component).getClientProperty("flamingo.internal.commandButtonLayoutManagerMedium.forceIcon"))) continue;
                    boolean bl = abstractCommandButton.getComponentOrientation().isLeftToRight();
                    CommandButtonLayoutManager.CommandButtonLayoutInfo commandButtonLayoutInfo = abstractCommandButton.getUI().getLayoutInfo();
                    if (commandButtonLayoutInfo.textLayoutInfoList == null) {
                        commandButtonLayoutInfo.textLayoutInfoList = new ArrayList<CommandButtonLayoutManager.TextLayoutInfo>();
                    }
                    if (bl) {
                        int n = commandButtonLayoutInfo.iconRect.x + commandButtonLayoutInfo.iconRect.width;
                        int n2 = abstractCommandButton.getWidth();
                        for (CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo : commandButtonLayoutInfo.textLayoutInfoList) {
                            n2 = Math.min(n2, textLayoutInfo.textRect.x);
                        }
                        return (n + n2) / 2;
                    }
                    int n = commandButtonLayoutInfo.iconRect.x;
                    int n3 = 0;
                    for (CommandButtonLayoutManager.TextLayoutInfo textLayoutInfo : commandButtonLayoutInfo.textLayoutInfoList) {
                        n3 = Math.max(n3, textLayoutInfo.textRect.x + textLayoutInfo.textRect.width);
                    }
                    return (n + n3) / 2;
                }
            }
            throw new IllegalStateException("Menu marked to show icons but no menu buttons in it");
        }

        protected void paintIconGutterSeparator(Graphics graphics) {
            CellRendererPane cellRendererPane = new CellRendererPane();
            JSeparator jSeparator = new JSeparator(1);
            cellRendererPane.setBounds(0, 0, this.getWidth(), this.getHeight());
            int n = this.getSeparatorX();
            if (this.getComponentOrientation().isLeftToRight()) {
                cellRendererPane.paintComponent(graphics, jSeparator, this, n, 2, 2, this.getHeight() - 4, true);
            } else {
                cellRendererPane.paintComponent(graphics, jSeparator, this, n, 2, 2, this.getHeight() - 4, true);
            }
        }

        protected void paintIconGutterBackground(Graphics graphics) {
            Graphics2D graphics2D = (Graphics2D)graphics.create();
            graphics2D.setComposite(AlphaComposite.SrcOver.derive(0.7f));
            int n = this.getSeparatorX();
            if (this.getComponentOrientation().isLeftToRight()) {
                graphics2D.clipRect(0, 0, n + 2, this.getHeight());
                AffineTransform affineTransform = AffineTransform.getTranslateInstance(0.0, this.getHeight());
                affineTransform.rotate(-1.5707963267948966);
                graphics2D.transform(affineTransform);
                FlamingoUtilities.renderSurface(graphics2D, this, new Rectangle(0, 0, this.getHeight(), 50), false, false, false);
            } else {
                graphics2D.clipRect(this.getWidth() - n, 0, n + 2, this.getHeight());
                AffineTransform affineTransform = AffineTransform.getTranslateInstance(0.0, this.getHeight());
                affineTransform.rotate(-1.5707963267948966);
                graphics2D.transform(affineTransform);
                FlamingoUtilities.renderSurface(graphics2D, this, new Rectangle(0, n, this.getHeight(), this.getWidth() - n), false, false, false);
            }
            graphics2D.dispose();
        }
    }

    protected class PopupMenuLayoutManager
    implements LayoutManager {
        protected PopupMenuLayoutManager() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            return null;
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            int n = 0;
            int n2 = 0;
            if (BasicCommandPopupMenuUI.this.commandButtonPanel != null) {
                n2 = BasicCommandPopupMenuUI.this.commandButtonPanel.getPreferredSize().width;
                n = BasicCommandPopupMenuUI.this.commandButtonPanel.getPreferredSize().height;
            }
            Dimension dimension = BasicCommandPopupMenuUI.this.popupMenu.getMaxVisibleMenuButtons() > 0 ? BasicCommandPopupMenuUI.this.menuItemsPanel.getPreferredSize() : BasicCommandPopupMenuUI.this.menuItemsPanel.getView().getPreferredSize();
            n2 = Math.max(dimension.width, n2);
            Insets insets = container.getInsets();
            return new Dimension(n2 + insets.left + insets.right, (n += dimension.height) + insets.top + insets.bottom);
        }

        @Override
        public void layoutContainer(Container container) {
            Insets insets = container.getInsets();
            int n = container.getHeight() - insets.bottom;
            Dimension dimension = BasicCommandPopupMenuUI.this.popupMenu.getMaxVisibleMenuButtons() > 0 ? BasicCommandPopupMenuUI.this.menuItemsPanel.getPreferredSize() : BasicCommandPopupMenuUI.this.menuItemsPanel.getView().getPreferredSize();
            BasicCommandPopupMenuUI.this.menuItemsPanel.setBounds(insets.left, n - dimension.height, container.getWidth() - insets.left - insets.right, dimension.height);
            BasicCommandPopupMenuUI.this.menuItemsPanel.doLayout();
            n -= dimension.height;
            if (BasicCommandPopupMenuUI.this.commandButtonPanel != null) {
                BasicCommandPopupMenuUI.this.commandButtonPanel.setBounds(insets.left, insets.top, container.getWidth() - insets.left - insets.right, n - insets.top);
                BasicCommandPopupMenuUI.this.commandButtonPanel.invalidate();
                BasicCommandPopupMenuUI.this.commandButtonPanel.validate();
                BasicCommandPopupMenuUI.this.commandButtonPanel.doLayout();
            }
        }
    }

    protected static class ScrollableCommandButtonPanel
    extends JComponent {
        protected Dimension maxDimension;
        protected JCommandButtonPanel buttonPanel;
        protected int maxVisibleButtonRows;
        protected JScrollPane scroll;

        public ScrollableCommandButtonPanel(JCommandButtonPanel jCommandButtonPanel, int n, int n2) {
            this.buttonPanel = jCommandButtonPanel;
            this.buttonPanel.setMaxButtonColumns(n);
            this.maxVisibleButtonRows = n2;
            int n3 = 0;
            int n4 = 0;
            int n5 = jCommandButtonPanel.getGroupCount();
            for (int i = 0; i < n5; ++i) {
                for (AbstractCommandButton abstractCommandButton : jCommandButtonPanel.getGroupButtons(i)) {
                    n3 = Math.max(n3, abstractCommandButton.getPreferredSize().width);
                    n4 = Math.max(n4, abstractCommandButton.getPreferredSize().height);
                }
            }
            this.updateMaxDimension();
            this.scroll = new JScrollPane(this.buttonPanel, 22, 31);
            this.scroll.setBorder(new EmptyBorder(0, 0, 0, 0));
            this.buttonPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
            this.scroll.setOpaque(false);
            this.scroll.getViewport().setOpaque(false);
            this.setLayout(new IconPopupLayout());
            this.add(this.scroll);
            this.setBorder(new Border(){

                @Override
                public Insets getBorderInsets(Component component) {
                    return new Insets(0, 0, 1, 0);
                }

                @Override
                public boolean isBorderOpaque() {
                    return true;
                }

                @Override
                public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
                    graphics.setColor(FlamingoUtilities.getBorderColor());
                    graphics.drawLine(n, n2 + n4 - 1, n + n3, n2 + n4 - 1);
                }
            });
        }

        public void updateMaxDimension() {
            if (this.buttonPanel == null) {
                return;
            }
            this.buttonPanel.setPreferredSize(null);
            Dimension dimension = this.buttonPanel.getPreferredSize();
            BasicCommandButtonPanelUI basicCommandButtonPanelUI = (BasicCommandButtonPanelUI)this.buttonPanel.getUI();
            int n = this.buttonPanel.isToShowGroupLabels() ? 1 : 0;
            this.maxDimension = new Dimension(dimension.width, basicCommandButtonPanelUI.getPreferredHeight(this.maxVisibleButtonRows, n));
            this.setPreferredSize(null);
        }

        protected class IconPopupLayout
        implements LayoutManager {
            protected IconPopupLayout() {
            }

            @Override
            public void addLayoutComponent(String string, Component component) {
            }

            @Override
            public void removeLayoutComponent(Component component) {
            }

            @Override
            public void layoutContainer(Container container) {
                Insets insets = container.getInsets();
                int n = insets.left;
                int n2 = insets.right;
                int n3 = insets.top;
                int n4 = insets.bottom;
                ScrollableCommandButtonPanel.this.scroll.setBounds(n, n3, container.getWidth() - n - n2, container.getHeight() - n3 - n4);
            }

            @Override
            public Dimension minimumLayoutSize(Container container) {
                return this.preferredLayoutSize(container);
            }

            @Override
            public Dimension preferredLayoutSize(Container container) {
                Insets insets = container.getInsets();
                int n = insets.left;
                int n2 = insets.right;
                int n3 = insets.top;
                int n4 = insets.bottom;
                Dimension dimension = ScrollableCommandButtonPanel.this.buttonPanel.getPreferredSize();
                if (dimension == null) {
                    dimension = new Dimension(0, 0);
                }
                int n5 = Math.min(dimension.width, ScrollableCommandButtonPanel.this.maxDimension.width) + n + n2;
                int n6 = Math.min(dimension.height, ScrollableCommandButtonPanel.this.maxDimension.height) + n3 + n4;
                if (n6 == ScrollableCommandButtonPanel.this.maxDimension.height + n3 + n4) {
                    int n7 = UIManager.getInt("ScrollBar.width");
                    if (n7 == 0) {
                        n7 = new JScrollBar((int)1).getPreferredSize().width;
                    }
                    n5 += n7;
                }
                return new Dimension(n5, n6);
            }
        }

    }

}

