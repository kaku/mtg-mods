/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon;

import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.resize.IconRibbonBandResizePolicy;
import org.pushingpixels.flamingo.api.ribbon.resize.RibbonBandResizePolicy;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanelUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.BasicRibbonBandUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.JFlowBandControlPanel;

public class BasicFlowBandControlPanelUI
extends AbstractBandControlPanelUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new BasicFlowBandControlPanelUI();
    }

    @Override
    protected LayoutManager createLayoutManager() {
        return new FlowControlPanelLayout();
    }

    private class FlowControlPanelLayout
    implements LayoutManager {
        private FlowControlPanelLayout() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            int n = BasicFlowBandControlPanelUI.this.dummy.getPreferredSize().height;
            int n2 = BasicFlowBandControlPanelUI.this.getLayoutGap() * 3 / 4;
            int n3 = n - 2 * n2;
            switch (n3 % 3) {
                case 1: {
                    n += 2;
                    break;
                }
                case 2: {
                    ++n;
                }
            }
            Insets insets = container.getInsets();
            return new Dimension(container.getWidth(), n + insets.top + insets.bottom);
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            return this.preferredLayoutSize(container);
        }

        @Override
        public void layoutContainer(Container container) {
            JFlowBandControlPanel jFlowBandControlPanel = (JFlowBandControlPanel)container;
            AbstractRibbonBand abstractRibbonBand = jFlowBandControlPanel.getRibbonBand();
            RibbonBandResizePolicy ribbonBandResizePolicy = abstractRibbonBand.getCurrentResizePolicy();
            if (ribbonBandResizePolicy == null) {
                return;
            }
            boolean bl = container.getComponentOrientation().isLeftToRight();
            Insets insets = container.getInsets();
            int n = insets.left;
            int n2 = BasicFlowBandControlPanelUI.this.getLayoutGap();
            int n3 = container.getHeight() - insets.top - insets.bottom;
            if (SwingUtilities.getAncestorOfClass(BasicRibbonBandUI.CollapsedButtonPopupPanel.class, container) != null) {
                List<RibbonBandResizePolicy> list = abstractRibbonBand.getResizePolicies();
                list.get(0).install(n3, n2);
            } else {
                if (ribbonBandResizePolicy instanceof IconRibbonBandResizePolicy) {
                    return;
                }
                ribbonBandResizePolicy.install(n3, n2);
            }
            int n4 = 0;
            int n5 = 1;
            for (JComponent jComponent : jFlowBandControlPanel.getFlowComponents()) {
                Dimension dimension = jComponent.getPreferredSize();
                if (n + dimension.width > container.getWidth() - insets.right) {
                    n = insets.left;
                    ++n5;
                }
                n += dimension.width + n2;
                n4 = Math.max(n4, dimension.height);
            }
            int n6 = (n3 - n5 * n4) / n5;
            if (n6 < 0) {
                n6 = 2;
                n4 = (n3 - n6 * (n5 - 1)) / n5;
            }
            int n7 = insets.top + n6 / 2;
            n = bl ? insets.left : container.getWidth() - insets.right;
            int n8 = 0;
            for (JComponent jComponent2 : jFlowBandControlPanel.getFlowComponents()) {
                Dimension dimension = jComponent2.getPreferredSize();
                if (bl) {
                    if (n + dimension.width > container.getWidth() - insets.right) {
                        n = insets.left;
                        n7 += n4 + n6;
                        ++n8;
                    }
                } else if (n - dimension.width < insets.left) {
                    n = container.getWidth() - insets.right;
                    n7 += n4 + n6;
                    ++n8;
                }
                int n9 = Math.min(n4, dimension.height);
                if (bl) {
                    jComponent2.setBounds(n, n7 + (n4 - n9) / 2, dimension.width, n9);
                } else {
                    jComponent2.setBounds(n - dimension.width, n7 + (n4 - n9) / 2, dimension.width, n9);
                }
                jComponent2.putClientProperty("flamingo.internal.ribbonBandControlPanel.topRow", n8 == 0);
                jComponent2.putClientProperty("flamingo.internal.ribbonBandControlPanel.midRow", n8 > 0 && n8 < n5 - 1);
                jComponent2.putClientProperty("flamingo.internal.ribbonBandControlPanel.bottomRow", n8 == n5 - 1);
                if (bl) {
                    n += dimension.width + n2;
                    continue;
                }
                n -= dimension.width + n2;
            }
        }
    }

}

