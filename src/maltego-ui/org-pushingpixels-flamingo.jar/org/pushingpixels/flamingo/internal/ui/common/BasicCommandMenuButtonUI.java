/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common;

import java.awt.AWTEvent;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.EventListener;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.RolloverActionListener;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;
import org.pushingpixels.flamingo.internal.ui.common.BasicCommandButtonUI;
import org.pushingpixels.flamingo.internal.utils.KeyTipRenderingUtilities;

public class BasicCommandMenuButtonUI
extends BasicCommandButtonUI {
    protected MouseListener rolloverMenuMouseListener;

    public static ComponentUI createUI(JComponent jComponent) {
        return new BasicCommandMenuButtonUI();
    }

    @Override
    protected void installListeners() {
        super.installListeners();
        this.rolloverMenuMouseListener = new MouseAdapter(){

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {
                if (BasicCommandMenuButtonUI.this.commandButton.isEnabled()) {
                    int n = 0;
                    AWTEvent aWTEvent = EventQueue.getCurrentEvent();
                    if (aWTEvent instanceof InputEvent) {
                        n = ((InputEvent)aWTEvent).getModifiers();
                    } else if (aWTEvent instanceof ActionEvent) {
                        n = ((ActionEvent)aWTEvent).getModifiers();
                    }
                    BasicCommandMenuButtonUI.this.fireRolloverActionPerformed(new ActionEvent(this, 1001, BasicCommandMenuButtonUI.this.commandButton.getActionModel().getActionCommand(), EventQueue.getMostRecentEventTime(), n));
                    BasicCommandMenuButtonUI.this.processPopupAction();
                }
            }
        };
        this.commandButton.addMouseListener(this.rolloverMenuMouseListener);
    }

    @Override
    protected void uninstallListeners() {
        this.commandButton.removeMouseListener(this.rolloverMenuMouseListener);
        this.rolloverMenuMouseListener = null;
        super.uninstallListeners();
    }

    protected void fireRolloverActionPerformed(ActionEvent actionEvent) {
        RolloverActionListener[] arrrolloverActionListener = (RolloverActionListener[])this.commandButton.getListeners(RolloverActionListener.class);
        for (int i = arrrolloverActionListener.length - 1; i >= 0; --i) {
            arrrolloverActionListener[i].actionPerformed(actionEvent);
        }
    }

    @Override
    public void update(Graphics graphics, JComponent jComponent) {
        JCommandMenuButton jCommandMenuButton = (JCommandMenuButton)jComponent;
        super.update(graphics, jComponent);
        KeyTipRenderingUtilities.renderMenuButtonKeyTips(graphics, jCommandMenuButton, this.layoutManager);
    }

}

