/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon;

import javax.swing.plaf.ComponentUI;

public abstract class RibbonBandUI
extends ComponentUI {
    public abstract int getPreferredCollapsedWidth();

    public abstract int getBandTitleHeight();

    public abstract void trackMouseCrossing(boolean var1);

    public abstract float getRolloverAmount();
}

