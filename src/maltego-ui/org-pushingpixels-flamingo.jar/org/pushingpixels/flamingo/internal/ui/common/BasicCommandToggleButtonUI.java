/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common;

import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.JToggleButton;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.internal.ui.common.BasicCommandButtonUI;

public class BasicCommandToggleButtonUI
extends BasicCommandButtonUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new BasicCommandToggleButtonUI();
    }

    @Override
    protected void updatePopupActionIcon() {
    }

    @Override
    protected boolean isPaintingSeparators() {
        return false;
    }

    @Override
    protected AbstractButton createRendererButton() {
        return new JToggleButton("");
    }
}

