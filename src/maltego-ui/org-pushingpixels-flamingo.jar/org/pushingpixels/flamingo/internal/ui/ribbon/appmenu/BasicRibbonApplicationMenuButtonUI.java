/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.ribbon.appmenu;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.CellRendererPane;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.UIResource;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;
import org.pushingpixels.flamingo.api.common.model.PopupButtonModel;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;
import org.pushingpixels.flamingo.api.ribbon.JRibbonFrame;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu;
import org.pushingpixels.flamingo.internal.ui.common.BasicCommandButtonUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuButton;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuPopupPanel;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public class BasicRibbonApplicationMenuButtonUI
extends BasicCommandButtonUI {
    protected JRibbonApplicationMenuButton applicationMenuButton;

    public static ComponentUI createUI(JComponent jComponent) {
        return new BasicRibbonApplicationMenuButtonUI();
    }

    @Override
    public void installUI(JComponent jComponent) {
        this.applicationMenuButton = (JRibbonApplicationMenuButton)jComponent;
        super.installUI(jComponent);
    }

    @Override
    protected void installDefaults() {
        super.installDefaults();
        Border border = this.commandButton.getBorder();
        if (border == null || border instanceof UIResource) {
            Border border2 = UIManager.getBorder("RibbonApplicationMenuButton.border");
            if (border2 == null) {
                border2 = new BorderUIResource.EmptyBorderUIResource(4, 4, 4, 4);
            }
            this.commandButton.setBorder(border2);
        }
        this.commandButton.setOpaque(false);
    }

    @Override
    protected void configureRenderer() {
        this.buttonRendererPane = new CellRendererPane();
        this.commandButton.add(this.buttonRendererPane);
        this.rendererButton = new JButton("");
    }

    @Override
    protected void unconfigureRenderer() {
        this.commandButton.remove(this.buttonRendererPane);
        this.buttonRendererPane = null;
        this.rendererButton = null;
    }

    @Override
    protected void installComponents() {
        super.installComponents();
        final JRibbonApplicationMenuButton jRibbonApplicationMenuButton = (JRibbonApplicationMenuButton)this.commandButton;
        jRibbonApplicationMenuButton.setPopupCallback(new PopupPanelCallback(){

            @Override
            public JPopupPanel getPopupPanel(final JCommandButton jCommandButton) {
                JRibbonFrame jRibbonFrame = (JRibbonFrame)SwingUtilities.getWindowAncestor(jCommandButton);
                final JRibbon jRibbon = jRibbonFrame.getRibbon();
                RibbonApplicationMenu ribbonApplicationMenu = jRibbon.getApplicationMenu();
                final JRibbonApplicationMenuPopupPanel jRibbonApplicationMenuPopupPanel = new JRibbonApplicationMenuPopupPanel(jRibbonApplicationMenuButton, ribbonApplicationMenu);
                jRibbonApplicationMenuPopupPanel.applyComponentOrientation(jRibbonApplicationMenuButton.getComponentOrientation());
                jRibbonApplicationMenuPopupPanel.setCustomizer(new JPopupPanel.PopupPanelCustomizer(){

                    @Override
                    public Rectangle getScreenBounds() {
                        int n;
                        boolean bl = jCommandButton.getComponentOrientation().isLeftToRight();
                        int n2 = jRibbonApplicationMenuPopupPanel.getPreferredSize().width;
                        int n3 = bl ? jRibbon.getLocationOnScreen().x : jRibbon.getLocationOnScreen().x + jRibbon.getWidth() - n2;
                        int n4 = jCommandButton.getLocationOnScreen().y + jCommandButton.getSize().height / 2 + 2;
                        Rectangle rectangle = jCommandButton.getGraphicsConfiguration().getBounds();
                        if (n3 + n2 > rectangle.x + rectangle.width) {
                            n3 = rectangle.x + rectangle.width - n2;
                        }
                        if (n4 + (n = jRibbonApplicationMenuPopupPanel.getPreferredSize().height) > rectangle.y + rectangle.height) {
                            n4 = rectangle.y + rectangle.height - n;
                        }
                        return new Rectangle(n3, n4, jRibbonApplicationMenuPopupPanel.getPreferredSize().width, jRibbonApplicationMenuPopupPanel.getPreferredSize().height);
                    }
                });
                return jRibbonApplicationMenuPopupPanel;
            }

        });
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        Insets insets = jComponent.getInsets();
        this.paintButtonBackground(graphics2D, new Rectangle(insets.left, insets.top, jComponent.getWidth() - insets.left - insets.right, jComponent.getHeight() - insets.top - insets.bottom));
        CommandButtonLayoutManager.CommandButtonLayoutInfo commandButtonLayoutInfo = this.getLayoutInfo();
        this.commandButton.putClientProperty("icon.bounds", commandButtonLayoutInfo.iconRect);
        this.paintButtonIcon(graphics2D, commandButtonLayoutInfo.iconRect);
        graphics2D.dispose();
    }

    @Override
    protected void paintButtonBackground(Graphics graphics, Rectangle rectangle) {
        this.buttonRendererPane.setBounds(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        ButtonModel buttonModel = this.rendererButton.getModel();
        buttonModel.setEnabled(true);
        buttonModel.setSelected(this.applicationMenuButton.getPopupModel().isSelected());
        buttonModel.setRollover(this.applicationMenuButton.getPopupModel().isRollover());
        buttonModel.setPressed(this.applicationMenuButton.getPopupModel().isPressed() || this.applicationMenuButton.getPopupModel().isPopupShowing());
        buttonModel.setArmed(this.applicationMenuButton.getActionModel().isArmed());
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        graphics2D.translate(rectangle.x, rectangle.y);
        Shape shape = graphics2D.getClip();
        graphics2D.clip(new Ellipse2D.Double(0.0, 0.0, rectangle.width, rectangle.height));
        this.rendererButton.setBorderPainted(false);
        this.buttonRendererPane.paintComponent(graphics2D, this.rendererButton, this.applicationMenuButton, (- rectangle.width) / 2, (- rectangle.height) / 2, 2 * rectangle.width, 2 * rectangle.height, true);
        graphics2D.setColor(FlamingoUtilities.getBorderColor().darker());
        graphics2D.setClip(shape);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.draw(new Ellipse2D.Double(0.0, 0.0, rectangle.width, rectangle.height));
        graphics2D.dispose();
    }

}

