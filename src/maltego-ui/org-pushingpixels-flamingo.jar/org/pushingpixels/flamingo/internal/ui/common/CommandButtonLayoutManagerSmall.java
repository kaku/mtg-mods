/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common;

import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import javax.swing.JSeparator;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public class CommandButtonLayoutManagerSmall
implements CommandButtonLayoutManager {
    @Override
    public int getPreferredIconSize() {
        return 16;
    }

    @Override
    public Dimension getPreferredSize(AbstractCommandButton abstractCommandButton) {
        Insets insets = abstractCommandButton.getInsets();
        int n = insets.top + insets.bottom;
        FontMetrics fontMetrics = abstractCommandButton.getFontMetrics(abstractCommandButton.getFont());
        int n2 = FlamingoUtilities.getHLayoutGap(abstractCommandButton);
        boolean bl = abstractCommandButton.getIcon() != null;
        boolean bl2 = FlamingoUtilities.hasPopupAction(abstractCommandButton);
        int n3 = bl ? this.getPreferredIconSize() : 0;
        int n4 = insets.left;
        if (bl) {
            n4 += n2;
            n4 += n3;
            n4 += n2;
        }
        if (bl2) {
            n4 += 2 * n2;
            n4 += 1 + fontMetrics.getHeight() / 2;
            n4 += 2 * n2;
        }
        if (abstractCommandButton instanceof JCommandButton) {
            JCommandButton jCommandButton = (JCommandButton)abstractCommandButton;
            JCommandButton.CommandButtonKind commandButtonKind = jCommandButton.getCommandButtonKind();
            if (bl && commandButtonKind.hasAction() && commandButtonKind.hasPopup()) {
                n4 += new JSeparator((int)1).getPreferredSize().width;
            }
        }
        n4 += insets.right;
        return new Dimension(n4 -= 2 * n2, n + Math.max(n3, fontMetrics.getAscent() + fontMetrics.getDescent()));
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
    }

    @Override
    public Point getKeyTipAnchorCenterPoint(AbstractCommandButton abstractCommandButton) {
        Insets insets = abstractCommandButton.getInsets();
        int n = abstractCommandButton.getHeight();
        ResizableIcon resizableIcon = abstractCommandButton.getIcon();
        if (resizableIcon != null) {
            return new Point(insets.left + resizableIcon.getIconWidth(), (n + resizableIcon.getIconHeight()) / 2);
        }
        return new Point(insets.left, 3 * n / 4);
    }

    @Override
    public CommandButtonLayoutManager.CommandButtonLayoutInfo getLayoutInfo(AbstractCommandButton abstractCommandButton, Graphics graphics) {
        ResizableIcon resizableIcon;
        CommandButtonLayoutManager.CommandButtonLayoutInfo commandButtonLayoutInfo = new CommandButtonLayoutManager.CommandButtonLayoutInfo();
        commandButtonLayoutInfo.actionClickArea = new Rectangle(0, 0, 0, 0);
        commandButtonLayoutInfo.popupClickArea = new Rectangle(0, 0, 0, 0);
        Insets insets = abstractCommandButton.getInsets();
        commandButtonLayoutInfo.iconRect = new Rectangle();
        commandButtonLayoutInfo.popupActionRect = new Rectangle();
        int n = abstractCommandButton.getWidth();
        int n2 = abstractCommandButton.getHeight();
        int n3 = this.getPreferredSize((AbstractCommandButton)abstractCommandButton).width;
        int n4 = 0;
        if (abstractCommandButton.getHorizontalAlignment() == 0 && n > n3) {
            n4 = (n - n3) / 2;
        }
        boolean bl = (resizableIcon = abstractCommandButton.getIcon()) != null;
        boolean bl2 = FlamingoUtilities.hasPopupAction(abstractCommandButton);
        boolean bl3 = abstractCommandButton.getComponentOrientation().isLeftToRight();
        FontMetrics fontMetrics = graphics.getFontMetrics();
        int n5 = fontMetrics.getAscent() + fontMetrics.getDescent();
        JCommandButton.CommandButtonKind commandButtonKind = abstractCommandButton instanceof JCommandButton ? ((JCommandButton)abstractCommandButton).getCommandButtonKind() : JCommandButton.CommandButtonKind.ACTION_ONLY;
        int n6 = FlamingoUtilities.getHLayoutGap(abstractCommandButton);
        if (bl3) {
            int n7;
            int n8;
            int n9 = insets.left + n4 - n6;
            if (bl) {
                n7 = resizableIcon.getIconHeight();
                n8 = resizableIcon.getIconWidth();
                commandButtonLayoutInfo.iconRect.x = n9 += n6;
                commandButtonLayoutInfo.iconRect.y = (n2 - n7) / 2;
                commandButtonLayoutInfo.iconRect.width = n8;
                commandButtonLayoutInfo.iconRect.height = n7;
                n9 += n8 + n6;
            }
            if (bl2) {
                commandButtonLayoutInfo.popupActionRect.x = n9 += 2 * n6;
                commandButtonLayoutInfo.popupActionRect.y = (n2 - n5) / 2 - 1;
                commandButtonLayoutInfo.popupActionRect.width = 1 + n5 / 2;
                commandButtonLayoutInfo.popupActionRect.height = n5 + 2;
                n9 += commandButtonLayoutInfo.popupActionRect.width;
                n9 += 2 * n6;
            }
            n7 = 0;
            n8 = new JSeparator((int)1).getPreferredSize().width;
            switch (commandButtonKind) {
                case ACTION_ONLY: {
                    commandButtonLayoutInfo.actionClickArea.x = 0;
                    commandButtonLayoutInfo.actionClickArea.y = 0;
                    commandButtonLayoutInfo.actionClickArea.width = n;
                    commandButtonLayoutInfo.actionClickArea.height = n2;
                    commandButtonLayoutInfo.isTextInActionArea = true;
                    break;
                }
                case POPUP_ONLY: {
                    commandButtonLayoutInfo.popupClickArea.x = 0;
                    commandButtonLayoutInfo.popupClickArea.y = 0;
                    commandButtonLayoutInfo.popupClickArea.width = n;
                    commandButtonLayoutInfo.popupClickArea.height = n2;
                    commandButtonLayoutInfo.isTextInActionArea = false;
                    break;
                }
                case ACTION_AND_POPUP_MAIN_ACTION: 
                case ACTION_AND_POPUP_MAIN_POPUP: {
                    if (bl) {
                        commandButtonLayoutInfo.popupActionRect.x += n8;
                        n7 = commandButtonLayoutInfo.iconRect.x + commandButtonLayoutInfo.iconRect.width + n6;
                        commandButtonLayoutInfo.actionClickArea.x = 0;
                        commandButtonLayoutInfo.actionClickArea.y = 0;
                        commandButtonLayoutInfo.actionClickArea.width = n7;
                        commandButtonLayoutInfo.actionClickArea.height = n2;
                        commandButtonLayoutInfo.popupClickArea.x = n7;
                        commandButtonLayoutInfo.popupClickArea.y = 0;
                        commandButtonLayoutInfo.popupClickArea.width = n - n7;
                        commandButtonLayoutInfo.popupClickArea.height = n2;
                        commandButtonLayoutInfo.separatorOrientation = CommandButtonLayoutManager.CommandButtonSeparatorOrientation.VERTICAL;
                        commandButtonLayoutInfo.separatorArea = new Rectangle();
                        commandButtonLayoutInfo.separatorArea.x = n7;
                        commandButtonLayoutInfo.separatorArea.y = 0;
                        commandButtonLayoutInfo.separatorArea.width = n8;
                        commandButtonLayoutInfo.separatorArea.height = n2;
                        commandButtonLayoutInfo.isTextInActionArea = true;
                        break;
                    }
                    commandButtonLayoutInfo.popupClickArea.x = 0;
                    commandButtonLayoutInfo.popupClickArea.y = 0;
                    commandButtonLayoutInfo.popupClickArea.width = n;
                    commandButtonLayoutInfo.popupClickArea.height = n2;
                    commandButtonLayoutInfo.isTextInActionArea = true;
                }
            }
        } else {
            int n10;
            int n11;
            int n12 = n - insets.right - n4 + n6;
            if (bl) {
                n10 = resizableIcon.getIconHeight();
                n11 = resizableIcon.getIconWidth();
                commandButtonLayoutInfo.iconRect.x = (n12 -= n6) - n11;
                commandButtonLayoutInfo.iconRect.y = (n2 - n10) / 2;
                commandButtonLayoutInfo.iconRect.width = n11;
                commandButtonLayoutInfo.iconRect.height = n10;
                n12 -= n11 + n6;
            }
            if (bl2) {
                commandButtonLayoutInfo.popupActionRect.width = 1 + n5 / 2;
                commandButtonLayoutInfo.popupActionRect.x = (n12 -= 2 * n6) - commandButtonLayoutInfo.popupActionRect.width;
                commandButtonLayoutInfo.popupActionRect.y = (n2 - n5) / 2 - 1;
                commandButtonLayoutInfo.popupActionRect.height = n5 + 2;
                n12 -= commandButtonLayoutInfo.popupActionRect.width;
                n12 -= 2 * n6;
            }
            n10 = 0;
            n11 = new JSeparator((int)1).getPreferredSize().width;
            switch (commandButtonKind) {
                case ACTION_ONLY: {
                    commandButtonLayoutInfo.actionClickArea.x = 0;
                    commandButtonLayoutInfo.actionClickArea.y = 0;
                    commandButtonLayoutInfo.actionClickArea.width = n;
                    commandButtonLayoutInfo.actionClickArea.height = n2;
                    commandButtonLayoutInfo.isTextInActionArea = true;
                    break;
                }
                case POPUP_ONLY: {
                    commandButtonLayoutInfo.popupClickArea.x = 0;
                    commandButtonLayoutInfo.popupClickArea.y = 0;
                    commandButtonLayoutInfo.popupClickArea.width = n;
                    commandButtonLayoutInfo.popupClickArea.height = n2;
                    commandButtonLayoutInfo.isTextInActionArea = false;
                    break;
                }
                case ACTION_AND_POPUP_MAIN_ACTION: 
                case ACTION_AND_POPUP_MAIN_POPUP: {
                    if (bl) {
                        commandButtonLayoutInfo.popupActionRect.x -= n11;
                        commandButtonLayoutInfo.actionClickArea.x = n10 = commandButtonLayoutInfo.iconRect.x - n6;
                        commandButtonLayoutInfo.actionClickArea.y = 0;
                        commandButtonLayoutInfo.actionClickArea.width = n - n10;
                        commandButtonLayoutInfo.actionClickArea.height = n2;
                        commandButtonLayoutInfo.popupClickArea.x = 0;
                        commandButtonLayoutInfo.popupClickArea.y = 0;
                        commandButtonLayoutInfo.popupClickArea.width = n10;
                        commandButtonLayoutInfo.popupClickArea.height = n2;
                        commandButtonLayoutInfo.separatorOrientation = CommandButtonLayoutManager.CommandButtonSeparatorOrientation.VERTICAL;
                        commandButtonLayoutInfo.separatorArea = new Rectangle();
                        commandButtonLayoutInfo.separatorArea.x = n10;
                        commandButtonLayoutInfo.separatorArea.y = 0;
                        commandButtonLayoutInfo.separatorArea.width = n11;
                        commandButtonLayoutInfo.separatorArea.height = n2;
                        commandButtonLayoutInfo.isTextInActionArea = true;
                        break;
                    }
                    commandButtonLayoutInfo.popupClickArea.x = 0;
                    commandButtonLayoutInfo.popupClickArea.y = 0;
                    commandButtonLayoutInfo.popupClickArea.width = n;
                    commandButtonLayoutInfo.popupClickArea.height = n2;
                    commandButtonLayoutInfo.isTextInActionArea = true;
                }
            }
        }
        return commandButtonLayoutInfo;
    }

}

