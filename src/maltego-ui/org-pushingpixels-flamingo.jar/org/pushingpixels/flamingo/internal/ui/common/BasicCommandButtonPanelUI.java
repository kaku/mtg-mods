/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.internal.ui.common;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.UIResource;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButtonPanel;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonPanelUI;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public class BasicCommandButtonPanelUI
extends CommandButtonPanelUI {
    protected JCommandButtonPanel buttonPanel;
    protected JLabel[] groupLabels;
    protected Rectangle[] groupRects;
    protected PropertyChangeListener propertyChangeListener;
    protected ChangeListener changeListener;
    protected static final Insets GROUP_INSETS = new Insets(4, 4, 4, 4);

    public static ComponentUI createUI(JComponent jComponent) {
        return new BasicCommandButtonPanelUI();
    }

    @Override
    public void installUI(JComponent jComponent) {
        this.buttonPanel = (JCommandButtonPanel)jComponent;
        this.installDefaults();
        this.installComponents();
        this.installListeners();
    }

    protected void installDefaults() {
        this.buttonPanel.setLayout(this.createLayoutManager());
        Font font = this.buttonPanel.getFont();
        if (font == null || font instanceof UIResource) {
            FontUIResource fontUIResource = FlamingoUtilities.getFont(null, "CommandButtonPanel.font", "Button.font", "Panel.font");
            this.buttonPanel.setFont(fontUIResource);
        }
    }

    protected void installComponents() {
        this.recomputeGroupHeaders();
    }

    protected void installListeners() {
        this.propertyChangeListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("maxButtonColumns".equals(propertyChangeEvent.getPropertyName()) || "maxButtonRows".equals(propertyChangeEvent.getPropertyName()) || "toShowGroupLabels".equals(propertyChangeEvent.getPropertyName())) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            if (BasicCommandButtonPanelUI.this.buttonPanel != null) {
                                BasicCommandButtonPanelUI.this.recomputeGroupHeaders();
                                BasicCommandButtonPanelUI.this.buttonPanel.revalidate();
                                BasicCommandButtonPanelUI.this.buttonPanel.doLayout();
                            }
                        }
                    });
                }
                if ("layoutKind".equals(propertyChangeEvent.getPropertyName())) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            if (BasicCommandButtonPanelUI.this.buttonPanel != null) {
                                BasicCommandButtonPanelUI.this.buttonPanel.setLayout(BasicCommandButtonPanelUI.this.createLayoutManager());
                                BasicCommandButtonPanelUI.this.buttonPanel.revalidate();
                                BasicCommandButtonPanelUI.this.buttonPanel.doLayout();
                            }
                        }
                    });
                }
            }

        };
        this.buttonPanel.addPropertyChangeListener(this.propertyChangeListener);
        this.changeListener = new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                BasicCommandButtonPanelUI.this.recomputeGroupHeaders();
                BasicCommandButtonPanelUI.this.buttonPanel.revalidate();
                BasicCommandButtonPanelUI.this.buttonPanel.doLayout();
            }
        };
        this.buttonPanel.addChangeListener(this.changeListener);
    }

    @Override
    public void uninstallUI(JComponent jComponent) {
        jComponent.setLayout(null);
        this.uninstallListeners();
        this.uninstallComponents();
        this.uninstallDefaults();
        this.buttonPanel = null;
    }

    protected void uninstallDefaults() {
    }

    protected void uninstallComponents() {
        if (this.groupLabels != null) {
            for (JLabel jLabel : this.groupLabels) {
                this.buttonPanel.remove(jLabel);
            }
        }
    }

    protected void uninstallListeners() {
        this.buttonPanel.removePropertyChangeListener(this.propertyChangeListener);
        this.propertyChangeListener = null;
        this.buttonPanel.removeChangeListener(this.changeListener);
        this.changeListener = null;
    }

    protected LayoutManager createLayoutManager() {
        if (this.buttonPanel.getLayoutKind() == JCommandButtonPanel.LayoutKind.ROW_FILL) {
            return new RowFillLayout();
        }
        return new ColumnFillLayout();
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        Color color = this.buttonPanel.getBackground();
        graphics.setColor(color);
        graphics.fillRect(0, 0, jComponent.getWidth(), jComponent.getHeight());
        for (int i = 0; i < this.buttonPanel.getGroupCount(); ++i) {
            Rectangle rectangle = this.groupRects[i];
            this.paintGroupBackground(graphics, i, rectangle.x, rectangle.y, rectangle.width, rectangle.height);
            if (!this.groupLabels[i].isVisible()) continue;
            Rectangle rectangle2 = this.groupLabels[i].getBounds();
            this.paintGroupTitleBackground(graphics, i, rectangle.x, rectangle2.y - this.getGroupInsets().top, rectangle.width, rectangle2.height + this.getGroupInsets().top + this.getLayoutGap());
        }
    }

    protected void paintGroupBackground(Graphics graphics, int n, int n2, int n3, int n4, int n5) {
        Color color = this.buttonPanel.getBackground();
        if (color == null || color instanceof UIResource) {
            color = UIManager.getColor("Panel.background");
            if (color == null) {
                color = new Color(190, 190, 190);
            }
            if (n % 2 == 1) {
                double d = 0.95;
                color = new Color((int)((double)color.getRed() * d), (int)((double)color.getGreen() * d), (int)((double)color.getBlue() * d));
            }
        }
        graphics.setColor(color);
        graphics.fillRect(n2, n3, n4, n5);
    }

    protected void paintGroupTitleBackground(Graphics graphics, int n, int n2, int n3, int n4, int n5) {
        FlamingoUtilities.renderSurface(graphics, this.buttonPanel, new Rectangle(n2, n3, n4, n5), false, n > 0, true);
    }

    protected int getGroupTitleHeight(int n) {
        return this.groupLabels[n].getPreferredSize().height;
    }

    protected Insets getGroupInsets() {
        return GROUP_INSETS;
    }

    protected int getLayoutGap() {
        return 4;
    }

    protected void recomputeGroupHeaders() {
        int n;
        if (this.groupLabels != null) {
            JLabel[] arrjLabel = this.groupLabels;
            n = arrjLabel.length;
            for (int i = 0; i < n; ++i) {
                JLabel jLabel = arrjLabel[i];
                this.buttonPanel.remove(jLabel);
            }
        }
        int n2 = this.buttonPanel.getGroupCount();
        this.groupLabels = new JLabel[n2];
        for (n = 0; n < n2; ++n) {
            this.groupLabels[n] = new JLabel(this.buttonPanel.getGroupTitleAt(n));
            this.groupLabels[n].setComponentOrientation(this.buttonPanel.getComponentOrientation());
            this.buttonPanel.add(this.groupLabels[n]);
            this.groupLabels[n].setVisible(this.buttonPanel.isToShowGroupLabels());
        }
    }

    public int getPreferredHeight(int n, int n2) {
        int n3;
        Insets insets = this.buttonPanel.getInsets();
        Insets insets2 = this.getGroupInsets();
        int n4 = 0;
        int n5 = this.buttonPanel.getGroupCount();
        for (n3 = 0; n3 < n5; ++n3) {
            for (AbstractCommandButton abstractCommandButton : this.buttonPanel.getGroupButtons(n3)) {
                n4 = Math.max(n4, abstractCommandButton.getPreferredSize().height);
            }
        }
        n3 = this.getLayoutGap();
        int n6 = insets.top + insets.bottom;
        n6 += n * n4;
        n6 += (n - 1) * n3;
        n6 += n2 * this.getGroupTitleHeight(0);
        return n6 += (n2 - 1) * (insets2.top + insets2.bottom);
    }

    protected class ColumnFillLayout
    implements LayoutManager {
        protected ColumnFillLayout() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public void layoutContainer(Container container) {
            int n;
            int n2;
            Insets insets = container.getInsets();
            Insets insets2 = BasicCommandButtonPanelUI.this.getGroupInsets();
            int n3 = insets.top;
            int n4 = insets.bottom;
            JCommandButtonPanel jCommandButtonPanel = (JCommandButtonPanel)container;
            boolean bl = jCommandButtonPanel.getComponentOrientation().isLeftToRight();
            int n5 = 0;
            int n6 = 0;
            int n7 = jCommandButtonPanel.getGroupCount();
            for (n = 0; n < n7; ++n) {
                for (AbstractCommandButton abstractCommandButton : jCommandButtonPanel.getGroupButtons(n)) {
                    n5 = Math.max(n5, abstractCommandButton.getPreferredSize().width);
                    n6 = Math.max(n6, abstractCommandButton.getPreferredSize().height);
                }
            }
            BasicCommandButtonPanelUI.this.groupRects = new Rectangle[n7];
            n = BasicCommandButtonPanelUI.this.getLayoutGap();
            int n8 = container.getHeight() - insets.top - insets.bottom - insets2.top - insets2.bottom;
            int n9 = n2 = n6 == 0 ? 0 : (n8 + n) / (n6 + n);
            if (bl) {
                int n10 = insets.left + insets2.left;
                for (int i = 0; i < n7; ++i) {
                    int n11 = n10;
                    n10 += insets2.left;
                    int n12 = n3 + insets2.top;
                    int n13 = n2 == 0 ? 0 : (int)Math.ceil((double)jCommandButtonPanel.getGroupButtons(i).size() / (double)n2);
                    int n14 = n13 > 1 ? (n8 - (n2 - 1) * n) / n2 : n5;
                    for (AbstractCommandButton abstractCommandButton : jCommandButtonPanel.getGroupButtons(i)) {
                        int n15 = n12 + n14;
                        if (n15 > container.getHeight() - n4 - insets2.bottom) {
                            n12 = n3 + insets2.top;
                            n10 += n5;
                            n10 += n;
                        }
                        abstractCommandButton.setBounds(n10, n12, n5, n14);
                        n12 += n14;
                        n12 += n;
                    }
                    int n16 = n10 += n5 + insets2.bottom;
                    BasicCommandButtonPanelUI.this.groupRects[i] = new Rectangle(n11, n3, n16 - n11, container.getHeight() - n3 - n4);
                }
            } else {
                int n17 = jCommandButtonPanel.getWidth() - insets.right - insets2.right;
                for (int i = 0; i < n7; ++i) {
                    int n18 = n17;
                    n17 -= insets2.left;
                    int n19 = n3 + insets2.top;
                    int n20 = n2 == 0 ? 0 : (int)Math.ceil((double)jCommandButtonPanel.getGroupButtons(i).size() / (double)n2);
                    int n21 = n20 > 1 ? (n8 - (n2 - 1) * n) / n2 : n5;
                    for (AbstractCommandButton abstractCommandButton : jCommandButtonPanel.getGroupButtons(i)) {
                        int n22 = n19 + n21;
                        if (n22 > container.getHeight() - n4 - insets2.bottom) {
                            n19 = n3 + insets2.top;
                            n17 -= n5;
                            n17 -= n;
                        }
                        abstractCommandButton.setBounds(n17 - n5, n19, n5, n21);
                        n19 += n21;
                        n19 += n;
                    }
                    int n23 = n17 -= n5 + insets2.bottom;
                    BasicCommandButtonPanelUI.this.groupRects[i] = new Rectangle(n23, n3, n18 - n23, container.getHeight() - n3 - n4);
                }
            }
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            return new Dimension(20, 20);
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            int n;
            int n2;
            JCommandButtonPanel jCommandButtonPanel = (JCommandButtonPanel)container;
            int n3 = jCommandButtonPanel.getMaxButtonRows();
            Insets insets = container.getInsets();
            Insets insets2 = BasicCommandButtonPanelUI.this.getGroupInsets();
            int n4 = insets.top + insets2.top + insets.bottom + insets2.bottom;
            int n5 = 0;
            int n6 = 0;
            int n7 = jCommandButtonPanel.getGroupCount();
            for (n = 0; n < n7; ++n) {
                for (AbstractCommandButton abstractCommandButton : jCommandButtonPanel.getGroupButtons(n)) {
                    n5 = Math.max(n5, abstractCommandButton.getPreferredSize().width);
                    n6 = Math.max(n6, abstractCommandButton.getPreferredSize().height);
                }
            }
            n = BasicCommandButtonPanelUI.this.getLayoutGap();
            boolean bl = n3 <= 0;
            int n8 = jCommandButtonPanel.getHeight();
            n8 -= n4;
            if (bl) {
                n3 = (n8 + n) / (n6 + n);
            }
            int n9 = insets.left + insets.right;
            for (n2 = 0; n2 < n7; ++n2) {
                n9 += insets2.left + insets2.right;
                int n10 = (int)Math.ceil((double)jCommandButtonPanel.getGroupButtons(n2).size() / (double)n3);
                n9 += n10 * n5 + (n10 - 1) * n;
            }
            n2 = bl ? n8 : n3 * n5 + (n3 - 1) * n + insets.top + insets.bottom + insets2.top + insets2.bottom;
            return new Dimension(Math.max(10, n9), Math.max(10, n2));
        }
    }

    protected class RowFillLayout
    implements LayoutManager {
        protected RowFillLayout() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public void layoutContainer(Container container) {
            int n;
            Insets insets = container.getInsets();
            Insets insets2 = BasicCommandButtonPanelUI.this.getGroupInsets();
            int n2 = insets.left;
            int n3 = insets.right;
            int n4 = insets.top;
            JCommandButtonPanel jCommandButtonPanel = (JCommandButtonPanel)container;
            boolean bl = jCommandButtonPanel.getComponentOrientation().isLeftToRight();
            int n5 = 0;
            int n6 = 0;
            int n7 = jCommandButtonPanel.getGroupCount();
            for (n = 0; n < n7; ++n) {
                for (AbstractCommandButton abstractCommandButton : jCommandButtonPanel.getGroupButtons(n)) {
                    n5 = Math.max(n5, abstractCommandButton.getPreferredSize().width);
                    n6 = Math.max(n6, abstractCommandButton.getPreferredSize().height);
                }
            }
            BasicCommandButtonPanelUI.this.groupRects = new Rectangle[n7];
            n = BasicCommandButtonPanelUI.this.getLayoutGap();
            int n8 = container.getWidth() - insets.left - insets.right - insets2.left - insets2.right;
            int n9 = n5 == 0 ? 0 : (n8 + n) / (n5 + n);
            int n10 = jCommandButtonPanel.getMaxButtonColumns();
            if (n10 > 0) {
                n9 = Math.min(n9, n10);
            }
            for (int i = 0; i < n7; ++i) {
                int n11;
                int n12;
                int n13;
                int n14;
                int n15 = n4;
                n4 += insets2.top;
                JLabel jLabel = BasicCommandButtonPanelUI.this.groupLabels[i];
                if (BasicCommandButtonPanelUI.this.buttonPanel.isToShowGroupLabels()) {
                    n12 = jLabel.getPreferredSize().width;
                    n11 = BasicCommandButtonPanelUI.this.getGroupTitleHeight(i);
                    if (jLabel.getComponentOrientation().isLeftToRight()) {
                        jLabel.setBounds(n2 + insets2.left, n4, n12, n11);
                    } else {
                        jLabel.setBounds(container.getWidth() - n3 - insets2.right - n12, n4, n12, n11);
                    }
                    n4 += n11 + n;
                }
                int n16 = n12 = n9 == 0 ? 0 : (int)Math.ceil((double)jCommandButtonPanel.getGroupButtons(i).size() / (double)n9);
                if (n10 > 0) {
                    n9 = Math.min(n9, n10);
                }
                int n17 = n11 = n12 > 1 ? (n8 - (n9 - 1) * n) / n9 : n5;
                if (n10 == 1) {
                    n11 = n8;
                }
                if (bl) {
                    n13 = n2 + insets2.left;
                    for (AbstractCommandButton abstractCommandButton : jCommandButtonPanel.getGroupButtons(i)) {
                        n14 = n13 + n11;
                        if (n14 > container.getWidth() - n3 - insets2.right) {
                            n13 = n2 + insets2.left;
                            n4 += n6;
                            n4 += n;
                        }
                        abstractCommandButton.setBounds(n13, n4, n11, n6);
                        n13 += n11;
                        n13 += n;
                    }
                } else {
                    n13 = container.getWidth() - n3 - insets2.right;
                    for (AbstractCommandButton abstractCommandButton : jCommandButtonPanel.getGroupButtons(i)) {
                        n14 = n13 - n11;
                        if (n14 < n2 + insets2.left) {
                            n13 = container.getWidth() - n3 - insets2.right;
                            n4 += n6;
                            n4 += n;
                        }
                        abstractCommandButton.setBounds(n13 - n11, n4, n11, n6);
                        n13 -= n11;
                        n13 -= n;
                    }
                }
                n13 = n4 += n6 + insets2.bottom;
                BasicCommandButtonPanelUI.this.groupRects[i] = new Rectangle(n2, n15, container.getWidth() - n2 - n3, n13 - n15);
            }
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            return new Dimension(20, 20);
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            int n;
            int n2;
            JCommandButtonPanel jCommandButtonPanel = (JCommandButtonPanel)container;
            int n3 = jCommandButtonPanel.getMaxButtonColumns();
            Insets insets = container.getInsets();
            Insets insets2 = BasicCommandButtonPanelUI.this.getGroupInsets();
            int n4 = insets.left + insets2.left + insets.right + insets2.right;
            int n5 = 0;
            int n6 = 0;
            int n7 = jCommandButtonPanel.getGroupCount();
            for (n = 0; n < n7; ++n) {
                for (AbstractCommandButton abstractCommandButton : jCommandButtonPanel.getGroupButtons(n)) {
                    n5 = Math.max(n5, abstractCommandButton.getPreferredSize().width);
                    n6 = Math.max(n6, abstractCommandButton.getPreferredSize().height);
                }
            }
            n = BasicCommandButtonPanelUI.this.getLayoutGap();
            boolean bl = n3 <= 0;
            int n8 = jCommandButtonPanel.getWidth();
            n8 -= n4;
            if (bl) {
                n3 = (n8 + n) / (n5 + n);
            }
            int n9 = insets.top + insets.bottom;
            for (n2 = 0; n2 < n7; ++n2) {
                if (BasicCommandButtonPanelUI.this.groupLabels[n2].isVisible()) {
                    n9 += BasicCommandButtonPanelUI.this.getGroupTitleHeight(n2) + n;
                }
                n9 += insets2.top + insets2.bottom;
                int n10 = (int)Math.ceil((double)jCommandButtonPanel.getGroupButtons(n2).size() / (double)n3);
                n9 += n10 * n6 + (n10 - 1) * n;
            }
            n2 = bl ? n8 : n3 * n5 + (n3 - 1) * n + insets.left + insets.right + insets2.left + insets2.right;
            return new Dimension(Math.max(10, n2), Math.max(10, n9));
        }
    }

}

