/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo;

import java.awt.Component;
import javax.swing.Popup;
import javax.swing.PopupFactory;

public class BlankPopupFixPopupFactory {
    public static Popup getPopup(Component component, int n, int n2) {
        return PopupFactory.getSharedInstance().getPopup(null, component, n, n2);
    }
}

