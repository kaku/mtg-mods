/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.ribbon.resize;

import org.pushingpixels.flamingo.api.ribbon.RibbonTask;
import org.pushingpixels.flamingo.api.ribbon.resize.RibbonBandResizeSequencingPolicy;

public abstract class BaseRibbonBandResizeSequencingPolicy
implements RibbonBandResizeSequencingPolicy {
    protected RibbonTask ribbonTask;

    protected BaseRibbonBandResizeSequencingPolicy(RibbonTask ribbonTask) {
        this.ribbonTask = ribbonTask;
    }
}

