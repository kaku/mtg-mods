/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.ribbon;

import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.ribbon.RibbonTask;
import org.pushingpixels.flamingo.api.ribbon.resize.RibbonBandResizePolicy;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.BasicRibbonBandUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.RibbonBandUI;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public abstract class AbstractRibbonBand<T extends AbstractBandControlPanel>
extends JComponent {
    public static final String uiClassID = "RibbonBandUI";
    RibbonTask ribbonTask;
    private String title;
    private ActionListener expandActionListener;
    protected T controlPanel;
    private AbstractRibbonBand popupRibbonBand;
    private ResizableIcon icon;
    private RibbonBandResizePolicy currResizePolicy;
    protected List<RibbonBandResizePolicy> resizePolicies;
    private String expandButtonKeyTip;
    private RichTooltip expandButtonRichTooltip;
    private String collapsedStateKeyTip;

    public AbstractRibbonBand(String string, ResizableIcon resizableIcon, ActionListener actionListener, T t) {
        this.title = string;
        this.icon = resizableIcon;
        this.expandActionListener = actionListener;
        this.controlPanel = t;
        this.controlPanel.setRibbonBand(this);
        this.add((Component)this.controlPanel);
        this.updateUI();
    }

    public abstract AbstractRibbonBand<T> cloneBand();

    public RibbonBandUI getUI() {
        return (RibbonBandUI)this.ui;
    }

    public void setUI(RibbonBandUI ribbonBandUI) {
        super.setUI(ribbonBandUI);
    }

    @Override
    public void updateUI() {
        if (UIManager.get(this.getUIClassID()) != null) {
            this.setUI((RibbonBandUI)UIManager.getUI(this));
        } else {
            this.setUI(new BasicRibbonBandUI());
        }
    }

    @Override
    public String getUIClassID() {
        return "RibbonBandUI";
    }

    public String getTitle() {
        return this.title;
    }

    public ResizableIcon getIcon() {
        return this.icon;
    }

    public void setTitle(String string) {
        String string2 = this.title;
        this.title = string;
        this.firePropertyChange("title", string2, this.title);
    }

    public ActionListener getExpandActionListener() {
        return this.expandActionListener;
    }

    public void setExpandActionListener(ActionListener actionListener) {
        ActionListener actionListener2 = this.expandActionListener;
        this.expandActionListener = actionListener;
        this.firePropertyChange("expandActionListener", actionListener2, this.expandActionListener);
    }

    public T getControlPanel() {
        return this.controlPanel;
    }

    public void setControlPanel(T t) {
        if (t == null) {
            this.remove((Component)this.controlPanel);
        } else {
            this.add((Component)t);
            t.applyComponentOrientation(this.getComponentOrientation());
        }
        this.controlPanel = t;
    }

    public AbstractRibbonBand getPopupRibbonBand() {
        return this.popupRibbonBand;
    }

    public void setPopupRibbonBand(AbstractRibbonBand abstractRibbonBand) {
        this.popupRibbonBand = abstractRibbonBand;
        if (this.popupRibbonBand != null) {
            abstractRibbonBand.applyComponentOrientation(this.getComponentOrientation());
        }
    }

    public RibbonBandResizePolicy getCurrentResizePolicy() {
        return this.currResizePolicy;
    }

    public void setCurrentResizePolicy(RibbonBandResizePolicy ribbonBandResizePolicy) {
        this.currResizePolicy = ribbonBandResizePolicy;
    }

    public List<RibbonBandResizePolicy> getResizePolicies() {
        return Collections.unmodifiableList(this.resizePolicies);
    }

    public void setResizePolicies(List<RibbonBandResizePolicy> list) {
        this.resizePolicies = Collections.unmodifiableList(list);
        if (this.ribbonTask != null) {
            FlamingoUtilities.checkResizePoliciesConsistency(this);
        }
    }

    public String getExpandButtonKeyTip() {
        return this.expandButtonKeyTip;
    }

    public void setExpandButtonKeyTip(String string) {
        String string2 = this.expandButtonKeyTip;
        this.expandButtonKeyTip = string;
        this.firePropertyChange("expandButtonKeyTip", string2, this.expandButtonKeyTip);
    }

    public RichTooltip getExpandButtonRichTooltip() {
        return this.expandButtonRichTooltip;
    }

    public void setExpandButtonRichTooltip(RichTooltip richTooltip) {
        RichTooltip richTooltip2 = this.expandButtonRichTooltip;
        this.expandButtonRichTooltip = richTooltip;
        this.firePropertyChange("expandButtonRichTooltip", richTooltip2, this.expandButtonRichTooltip);
    }

    public String getCollapsedStateKeyTip() {
        return this.collapsedStateKeyTip;
    }

    public void setCollapsedStateKeyTip(String string) {
        String string2 = this.collapsedStateKeyTip;
        this.collapsedStateKeyTip = string;
        this.firePropertyChange("collapsedStateKeyTip", string2, this.collapsedStateKeyTip);
    }

    void setRibbonTask(RibbonTask ribbonTask) {
        if (this.ribbonTask != null) {
            throw new IllegalArgumentException("Ribbon band cannot be added to more than one ribbon task");
        }
        this.ribbonTask = ribbonTask;
        FlamingoUtilities.checkResizePoliciesConsistency(this);
    }
}

