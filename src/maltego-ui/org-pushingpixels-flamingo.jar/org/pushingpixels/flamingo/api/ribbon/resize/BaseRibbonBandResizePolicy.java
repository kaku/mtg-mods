/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.ribbon.resize;

import org.pushingpixels.flamingo.api.ribbon.resize.RibbonBandResizePolicy;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel;

public abstract class BaseRibbonBandResizePolicy<T extends AbstractBandControlPanel>
implements RibbonBandResizePolicy {
    protected T controlPanel;

    protected BaseRibbonBandResizePolicy(T t) {
        this.controlPanel = t;
    }
}

