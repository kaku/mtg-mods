/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.ribbon;

import java.awt.ComponentOrientation;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JComponent;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies;
import org.pushingpixels.flamingo.api.ribbon.resize.RibbonBandResizePolicy;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.JFlowBandControlPanel;

public class JFlowRibbonBand
extends AbstractRibbonBand<JFlowBandControlPanel> {
    public JFlowRibbonBand(String string, ResizableIcon resizableIcon) {
        this(string, resizableIcon, null);
    }

    public JFlowRibbonBand(String string, ResizableIcon resizableIcon, ActionListener actionListener) {
        super(string, resizableIcon, actionListener, new JFlowBandControlPanel());
        this.resizePolicies = CoreRibbonResizePolicies.getCoreFlowPoliciesRestrictive(this, 3);
        this.updateUI();
    }

    public void addFlowComponent(JComponent jComponent) {
        ((JFlowBandControlPanel)this.controlPanel).addFlowComponent(jComponent);
    }

    @Override
    public AbstractRibbonBand<JFlowBandControlPanel> cloneBand() {
        JFlowRibbonBand jFlowRibbonBand = new JFlowRibbonBand(this.getTitle(), this.getIcon(), this.getExpandActionListener());
        jFlowRibbonBand.applyComponentOrientation(this.getComponentOrientation());
        return jFlowRibbonBand;
    }
}

