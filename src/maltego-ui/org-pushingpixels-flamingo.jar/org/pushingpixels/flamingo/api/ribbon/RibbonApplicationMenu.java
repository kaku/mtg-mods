/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.ribbon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryFooter;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary;

public class RibbonApplicationMenu {
    private boolean isFrozen;
    private List<List<RibbonApplicationMenuEntryPrimary>> primaryEntries = new ArrayList<List<RibbonApplicationMenuEntryPrimary>>();
    private List<RibbonApplicationMenuEntryFooter> footerEntries;
    private RibbonApplicationMenuEntryPrimary.PrimaryRolloverCallback defaultCallback;

    public RibbonApplicationMenu() {
        this.primaryEntries.add(new ArrayList());
        this.footerEntries = new ArrayList<RibbonApplicationMenuEntryFooter>();
    }

    public synchronized void addMenuEntry(RibbonApplicationMenuEntryPrimary ribbonApplicationMenuEntryPrimary) {
        if (this.isFrozen) {
            throw new IllegalStateException("Cannot add menu entries after the menu has been set on the ribbon");
        }
        this.primaryEntries.get(this.primaryEntries.size() - 1).add(ribbonApplicationMenuEntryPrimary);
    }

    public synchronized void addMenuSeparator() {
        if (this.isFrozen) {
            throw new IllegalStateException("Cannot add menu entries after the menu has been set on the ribbon");
        }
        this.primaryEntries.add(new ArrayList());
    }

    public List<List<RibbonApplicationMenuEntryPrimary>> getPrimaryEntries() {
        return Collections.unmodifiableList(this.primaryEntries);
    }

    public synchronized void addFooterEntry(RibbonApplicationMenuEntryFooter ribbonApplicationMenuEntryFooter) {
        if (this.isFrozen) {
            throw new IllegalStateException("Cannot add footer entries after the menu has been set on the ribbon");
        }
        this.footerEntries.add(ribbonApplicationMenuEntryFooter);
    }

    public List<RibbonApplicationMenuEntryFooter> getFooterEntries() {
        return Collections.unmodifiableList(this.footerEntries);
    }

    public void setDefaultCallback(RibbonApplicationMenuEntryPrimary.PrimaryRolloverCallback primaryRolloverCallback) {
        this.defaultCallback = primaryRolloverCallback;
    }

    public RibbonApplicationMenuEntryPrimary.PrimaryRolloverCallback getDefaultCallback() {
        return this.defaultCallback;
    }

    synchronized void setFrozen() {
        this.isFrozen = true;
        if (this.primaryEntries.get(this.primaryEntries.size() - 1).isEmpty()) {
            this.primaryEntries.remove(this.primaryEntries.size() - 1);
        }
    }
}

