/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.ribbon.resize;

import org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand;

public interface RibbonBandResizeSequencingPolicy {
    public void reset();

    public AbstractRibbonBand next();
}

