/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.ribbon.resize;

public interface RibbonBandResizePolicy {
    public int getPreferredWidth(int var1, int var2);

    public void install(int var1, int var2);
}

