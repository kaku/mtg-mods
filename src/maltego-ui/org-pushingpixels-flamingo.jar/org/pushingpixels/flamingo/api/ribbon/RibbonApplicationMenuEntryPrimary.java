/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.ribbon;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JPanel;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntry;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntrySecondary;

public class RibbonApplicationMenuEntryPrimary
extends RibbonApplicationMenuEntry {
    protected PrimaryRolloverCallback rolloverCallback;
    protected List<String> groupTitles = new ArrayList<String>();
    protected List<List<RibbonApplicationMenuEntrySecondary>> groupEntries = new ArrayList<List<RibbonApplicationMenuEntrySecondary>>();

    public RibbonApplicationMenuEntryPrimary(ResizableIcon resizableIcon, String string, ActionListener actionListener, JCommandButton.CommandButtonKind commandButtonKind) {
        super(resizableIcon, string, actionListener, commandButtonKind);
    }

    public synchronized /* varargs */ int addSecondaryMenuGroup(String string, RibbonApplicationMenuEntrySecondary ... arrribbonApplicationMenuEntrySecondary) {
        this.groupTitles.add(string);
        ArrayList<RibbonApplicationMenuEntrySecondary> arrayList = new ArrayList<RibbonApplicationMenuEntrySecondary>();
        this.groupEntries.add(arrayList);
        for (RibbonApplicationMenuEntrySecondary ribbonApplicationMenuEntrySecondary : arrribbonApplicationMenuEntrySecondary) {
            arrayList.add(ribbonApplicationMenuEntrySecondary);
        }
        return this.groupTitles.size() - 1;
    }

    public int getSecondaryGroupCount() {
        return this.groupTitles.size();
    }

    public String getSecondaryGroupTitleAt(int n) {
        return this.groupTitles.get(n);
    }

    public List<RibbonApplicationMenuEntrySecondary> getSecondaryGroupEntries(int n) {
        return Collections.unmodifiableList(this.groupEntries.get(n));
    }

    public void setRolloverCallback(PrimaryRolloverCallback primaryRolloverCallback) {
        this.rolloverCallback = primaryRolloverCallback;
    }

    public PrimaryRolloverCallback getRolloverCallback() {
        return this.rolloverCallback;
    }

    public synchronized void setSecondaryGroupTitle(int n, String string) {
        this.groupTitles.set(n, string);
    }

    public static interface PrimaryRolloverCallback {
        public void menuEntryActivated(JPanel var1);
    }

}

