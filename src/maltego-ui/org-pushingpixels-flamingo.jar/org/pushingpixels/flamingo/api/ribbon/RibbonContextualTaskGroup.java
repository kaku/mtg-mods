/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.ribbon;

import java.awt.Color;
import java.util.ArrayList;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;
import org.pushingpixels.flamingo.api.ribbon.RibbonTask;

public class RibbonContextualTaskGroup {
    private JRibbon ribbon;
    private ArrayList<RibbonTask> tasks;
    private String title;
    private Color hueColor;
    public static final double HUE_ALPHA = 0.25;

    public /* varargs */ RibbonContextualTaskGroup(String string, Color color, RibbonTask ... arrribbonTask) {
        this.title = string;
        this.hueColor = color;
        this.tasks = new ArrayList();
        for (RibbonTask ribbonTask : arrribbonTask) {
            ribbonTask.setContextualGroup(this);
            this.tasks.add(ribbonTask);
        }
    }

    public int getTaskCount() {
        return this.tasks.size();
    }

    public RibbonTask getTask(int n) {
        return this.tasks.get(n);
    }

    public String getTitle() {
        return this.title;
    }

    public Color getHueColor() {
        return this.hueColor;
    }

    public void setTitle(String string) {
        this.title = string;
        if (this.ribbon != null) {
            this.ribbon.fireStateChanged();
        }
    }

    public String toString() {
        return this.getTitle() + " (" + this.getTaskCount() + " tasks)";
    }

    void setRibbon(JRibbon jRibbon) {
        if (this.ribbon != null) {
            throw new IllegalStateException("The contextual task group already belongs to another ribbon");
        }
        this.ribbon = jRibbon;
    }
}

