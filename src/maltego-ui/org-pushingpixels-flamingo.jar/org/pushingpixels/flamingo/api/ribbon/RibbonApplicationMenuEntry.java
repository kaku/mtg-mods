/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.ribbon;

import java.awt.event.ActionListener;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

abstract class RibbonApplicationMenuEntry {
    protected ResizableIcon icon;
    protected ResizableIcon disabledIcon;
    protected String text;
    protected ActionListener mainActionListener;
    protected JCommandButton.CommandButtonKind entryKind;
    protected boolean isEnabled;
    protected String actionKeyTip;
    protected String popupKeyTip;

    public RibbonApplicationMenuEntry(ResizableIcon resizableIcon, String string, ActionListener actionListener, JCommandButton.CommandButtonKind commandButtonKind) {
        this.icon = resizableIcon;
        this.text = string;
        this.mainActionListener = actionListener;
        this.entryKind = commandButtonKind;
        this.isEnabled = true;
    }

    public ResizableIcon getIcon() {
        return this.icon;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String string) {
        this.text = string;
    }

    public ActionListener getMainActionListener() {
        return this.mainActionListener;
    }

    public JCommandButton.CommandButtonKind getEntryKind() {
        return this.entryKind;
    }

    public void setEnabled(boolean bl) {
        this.isEnabled = bl;
    }

    public boolean isEnabled() {
        return this.isEnabled;
    }

    public String getActionKeyTip() {
        return this.actionKeyTip;
    }

    public void setActionKeyTip(String string) {
        this.actionKeyTip = string;
    }

    public String getPopupKeyTip() {
        return this.popupKeyTip;
    }

    public void setPopupKeyTip(String string) {
        this.popupKeyTip = string;
    }

    public ResizableIcon getDisabledIcon() {
        return this.disabledIcon;
    }

    public void setDisabledIcon(ResizableIcon resizableIcon) {
        this.disabledIcon = resizableIcon;
    }
}

