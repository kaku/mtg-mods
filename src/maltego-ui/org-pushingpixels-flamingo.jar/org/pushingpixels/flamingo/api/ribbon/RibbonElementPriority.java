/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.ribbon;

public enum RibbonElementPriority {
    TOP,
    MEDIUM,
    LOW;
    

    private RibbonElementPriority() {
    }
}

