/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.ribbon;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.JRibbonFrame;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu;
import org.pushingpixels.flamingo.api.ribbon.RibbonContextualTaskGroup;
import org.pushingpixels.flamingo.api.ribbon.RibbonTask;
import org.pushingpixels.flamingo.internal.ui.ribbon.BasicRibbonUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.RibbonUI;

public class JRibbon
extends JComponent {
    private ArrayList<RibbonTask> tasks = new ArrayList();
    private ArrayList<RibbonContextualTaskGroup> contextualTaskGroups = new ArrayList();
    private ArrayList<Component> taskbarComponents = new ArrayList();
    private ArrayList<AbstractRibbonBand> bands = new ArrayList();
    private RibbonTask currentlySelectedTask = null;
    private ResizableIcon helpIcon;
    private ActionListener helpActionListener;
    private Map<RibbonContextualTaskGroup, Boolean> groupVisibilityMap = new HashMap<RibbonContextualTaskGroup, Boolean>();
    private RibbonApplicationMenu applicationMenu;
    private RichTooltip applicationMenuRichTooltip;
    private String applicationMenuKeyTip;
    private boolean isMinimized;
    private JRibbonFrame ribbonFrame;
    public static final String uiClassID = "RibbonUI";

    public JRibbon() {
        this.updateUI();
    }

    JRibbon(JRibbonFrame jRibbonFrame) {
        this();
        this.ribbonFrame = jRibbonFrame;
    }

    public synchronized void addTaskbarComponent(Component component) {
        if (component instanceof AbstractCommandButton) {
            AbstractCommandButton abstractCommandButton = (AbstractCommandButton)component;
            abstractCommandButton.setDisplayState(CommandButtonDisplayState.SMALL);
            abstractCommandButton.setGapScaleFactor(0.5);
            abstractCommandButton.setFocusable(false);
        }
        this.taskbarComponents.add(component);
        this.fireStateChanged();
    }

    public synchronized void removeTaskbarComponent(Component component) {
        this.taskbarComponents.remove(component);
        this.fireStateChanged();
    }

    public synchronized void addTask(RibbonTask ribbonTask) {
        ribbonTask.setRibbon(this);
        this.tasks.add(ribbonTask);
        if (this.tasks.size() == 1) {
            this.setSelectedTask(ribbonTask);
        }
        this.fireStateChanged();
    }

    public synchronized void configureHelp(ResizableIcon resizableIcon, ActionListener actionListener) {
        this.helpIcon = resizableIcon;
        this.helpActionListener = actionListener;
        this.fireStateChanged();
    }

    public ResizableIcon getHelpIcon() {
        return this.helpIcon;
    }

    public ActionListener getHelpActionListener() {
        return this.helpActionListener;
    }

    public synchronized void addContextualTaskGroup(RibbonContextualTaskGroup ribbonContextualTaskGroup) {
        ribbonContextualTaskGroup.setRibbon(this);
        this.contextualTaskGroups.add(ribbonContextualTaskGroup);
        this.groupVisibilityMap.put(ribbonContextualTaskGroup, false);
        this.fireStateChanged();
    }

    public synchronized int getTaskCount() {
        return this.tasks.size();
    }

    public synchronized RibbonTask getTask(int n) {
        return this.tasks.get(n);
    }

    public synchronized int getContextualTaskGroupCount() {
        return this.contextualTaskGroups.size();
    }

    public synchronized RibbonContextualTaskGroup getContextualTaskGroup(int n) {
        return this.contextualTaskGroups.get(n);
    }

    public synchronized void setSelectedTask(RibbonTask ribbonTask) {
        boolean bl = this.tasks.contains(ribbonTask);
        if (!bl) {
            for (int i = 0; i < this.getContextualTaskGroupCount(); ++i) {
                RibbonContextualTaskGroup object2 = this.getContextualTaskGroup(i);
                if (!this.isVisible(object2)) continue;
                for (int j = 0; j < object2.getTaskCount(); ++j) {
                    if (object2.getTask(j) != ribbonTask) continue;
                    bl = true;
                    break;
                }
                if (bl) break;
            }
        }
        if (!bl) {
            throw new IllegalArgumentException("The specified task to be selected is either not part of this ribbon or not marked as visible");
        }
        for (AbstractRibbonBand abstractRibbonBand : this.bands) {
            abstractRibbonBand.setVisible(false);
        }
        this.bands.clear();
        for (int i = 0; i < ribbonTask.getBandCount(); ++i) {
            AbstractRibbonBand abstractRibbonBand2 = ribbonTask.getBand(i);
            abstractRibbonBand2.setVisible(true);
            this.bands.add(abstractRibbonBand2);
        }
        RibbonTask ribbonTask2 = this.currentlySelectedTask;
        this.currentlySelectedTask = ribbonTask;
        this.revalidate();
        this.repaint();
        this.firePropertyChange("selectedTask", ribbonTask2, this.currentlySelectedTask);
    }

    public synchronized RibbonTask getSelectedTask() {
        return this.currentlySelectedTask;
    }

    @Override
    public void updateUI() {
        if (UIManager.get(this.getUIClassID()) != null) {
            this.setUI(UIManager.getUI(this));
        } else {
            this.setUI(new BasicRibbonUI());
        }
        for (Component component : this.taskbarComponents) {
            SwingUtilities.updateComponentTreeUI(component);
        }
    }

    public RibbonUI getUI() {
        return (RibbonUI)this.ui;
    }

    @Override
    public String getUIClassID() {
        return "RibbonUI";
    }

    public synchronized List<Component> getTaskbarComponents() {
        return Collections.unmodifiableList(this.taskbarComponents);
    }

    public void addChangeListener(ChangeListener changeListener) {
        this.listenerList.add(ChangeListener.class, changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this.listenerList.remove(ChangeListener.class, changeListener);
    }

    protected void fireStateChanged() {
        Object[] arrobject = this.listenerList.getListenerList();
        ChangeEvent changeEvent = new ChangeEvent(this);
        for (int i = arrobject.length - 2; i >= 0; i -= 2) {
            if (arrobject[i] != ChangeListener.class) continue;
            ((ChangeListener)arrobject[i + 1]).stateChanged(changeEvent);
        }
    }

    public synchronized void setVisible(RibbonContextualTaskGroup ribbonContextualTaskGroup, boolean bl) {
        this.groupVisibilityMap.put(ribbonContextualTaskGroup, bl);
        if (!bl) {
            boolean bl2 = false;
            for (int i = 0; i < ribbonContextualTaskGroup.getTaskCount(); ++i) {
                if (this.getSelectedTask() != ribbonContextualTaskGroup.getTask(i)) continue;
                bl2 = true;
                break;
            }
            if (bl2) {
                this.setSelectedTask(this.getTask(0));
            }
        }
        this.fireStateChanged();
        this.revalidate();
        SwingUtilities.getWindowAncestor(this).repaint();
    }

    public synchronized boolean isVisible(RibbonContextualTaskGroup ribbonContextualTaskGroup) {
        return this.groupVisibilityMap.get(ribbonContextualTaskGroup);
    }

    public synchronized void setApplicationMenu(RibbonApplicationMenu ribbonApplicationMenu) {
        RibbonApplicationMenu ribbonApplicationMenu2 = this.applicationMenu;
        if (ribbonApplicationMenu2 != ribbonApplicationMenu) {
            this.applicationMenu = ribbonApplicationMenu;
            if (this.applicationMenu != null) {
                this.applicationMenu.setFrozen();
            }
            this.firePropertyChange("applicationMenu", ribbonApplicationMenu2, this.applicationMenu);
        }
    }

    public synchronized RibbonApplicationMenu getApplicationMenu() {
        return this.applicationMenu;
    }

    public synchronized void setApplicationMenuRichTooltip(RichTooltip richTooltip) {
        RichTooltip richTooltip2 = this.applicationMenuRichTooltip;
        this.applicationMenuRichTooltip = richTooltip;
        this.firePropertyChange("applicationMenuRichTooltip", richTooltip2, this.applicationMenuRichTooltip);
    }

    public synchronized RichTooltip getApplicationMenuRichTooltip() {
        return this.applicationMenuRichTooltip;
    }

    public synchronized void setApplicationMenuKeyTip(String string) {
        String string2 = this.applicationMenuKeyTip;
        this.applicationMenuKeyTip = string;
        this.firePropertyChange("applicationMenuKeyTip", string2, this.applicationMenuKeyTip);
    }

    public synchronized String getApplicationMenuKeyTip() {
        return this.applicationMenuKeyTip;
    }

    public synchronized boolean isMinimized() {
        return this.isMinimized;
    }

    public synchronized void setMinimized(boolean bl) {
        boolean bl2 = this.isMinimized;
        if (bl2 != bl) {
            this.isMinimized = bl;
            this.firePropertyChange("minimized", bl2, this.isMinimized);
        }
    }

    public JRibbonFrame getRibbonFrame() {
        return this.ribbonFrame;
    }

    @Override
    public void setVisible(boolean bl) {
        if (!bl && this.getRibbonFrame() != null) {
            throw new IllegalArgumentException("Can't hide ribbon on JRibbonFrame");
        }
        super.setVisible(bl);
    }
}

