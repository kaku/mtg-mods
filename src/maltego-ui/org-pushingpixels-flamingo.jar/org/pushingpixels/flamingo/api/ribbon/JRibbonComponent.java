/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.ribbon;

import java.awt.event.MouseEvent;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.HorizontalAlignment;
import org.pushingpixels.flamingo.api.common.RichToolTipManager;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.ribbon.RibbonElementPriority;
import org.pushingpixels.flamingo.internal.ui.ribbon.BasicRibbonComponentUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.RibbonComponentUI;

public class JRibbonComponent
extends RichToolTipManager.JTrackableComponent {
    private ResizableIcon icon;
    private String caption;
    private JComponent mainComponent;
    private boolean isSimpleWrapper;
    private String keyTip;
    private RichTooltip richTooltip;
    private HorizontalAlignment horizontalAlignment;
    private RibbonElementPriority displayPriority;
    private boolean isResizingAware;
    public static final String uiClassID = "RibbonComponentUI";

    public JRibbonComponent(JComponent jComponent) {
        if (jComponent == null) {
            throw new IllegalArgumentException("All parameters must be non-null");
        }
        this.mainComponent = jComponent;
        this.isSimpleWrapper = true;
        this.horizontalAlignment = HorizontalAlignment.LEADING;
        this.isResizingAware = false;
        this.displayPriority = RibbonElementPriority.TOP;
        this.updateUI();
    }

    public JRibbonComponent(ResizableIcon resizableIcon, String string, JComponent jComponent) {
        if (string == null) {
            throw new IllegalArgumentException("Caption must be non-null");
        }
        if (jComponent == null) {
            throw new IllegalArgumentException("Main component must be non-null");
        }
        this.icon = resizableIcon;
        this.caption = string;
        this.mainComponent = jComponent;
        this.isSimpleWrapper = false;
        this.horizontalAlignment = HorizontalAlignment.TRAILING;
        this.updateUI();
    }

    @Override
    public void updateUI() {
        if (UIManager.get(this.getUIClassID()) != null) {
            this.setUI(UIManager.getUI(this));
        } else {
            this.setUI(BasicRibbonComponentUI.createUI(this));
        }
    }

    @Override
    public String getUIClassID() {
        return "RibbonComponentUI";
    }

    public RibbonComponentUI getUI() {
        return (RibbonComponentUI)this.ui;
    }

    public ResizableIcon getIcon() {
        return this.icon;
    }

    public String getCaption() {
        return this.caption;
    }

    public void setCaption(String string) {
        if (this.isSimpleWrapper) {
            throw new IllegalArgumentException("Cannot set caption on a simple component");
        }
        if (string == null) {
            throw new IllegalArgumentException("Caption must be non-null");
        }
        String string2 = this.caption;
        this.caption = string;
        this.firePropertyChange("caption", string2, this.caption);
    }

    public JComponent getMainComponent() {
        return this.mainComponent;
    }

    public boolean isSimpleWrapper() {
        return this.isSimpleWrapper;
    }

    public String getKeyTip() {
        return this.keyTip;
    }

    public void setKeyTip(String string) {
        String string2 = this.keyTip;
        this.keyTip = string;
        this.firePropertyChange("keyTip", string2, this.keyTip);
    }

    @Override
    public RichTooltip getRichTooltip(MouseEvent mouseEvent) {
        return this.richTooltip;
    }

    public void setRichTooltip(RichTooltip richTooltip) {
        this.richTooltip = richTooltip;
        RichToolTipManager richToolTipManager = RichToolTipManager.sharedInstance();
        if (richTooltip != null) {
            richToolTipManager.registerComponent(this);
        } else {
            richToolTipManager.unregisterComponent(this);
        }
    }

    public HorizontalAlignment getHorizontalAlignment() {
        return this.horizontalAlignment;
    }

    public void setHorizontalAlignment(HorizontalAlignment horizontalAlignment) {
        this.horizontalAlignment = horizontalAlignment;
    }

    public RibbonElementPriority getDisplayPriority() {
        return this.displayPriority;
    }

    public void setDisplayPriority(RibbonElementPriority ribbonElementPriority) {
        RibbonElementPriority ribbonElementPriority2 = this.displayPriority;
        this.displayPriority = ribbonElementPriority;
        if (ribbonElementPriority2 != ribbonElementPriority) {
            this.firePropertyChange("displayPriority", (Object)ribbonElementPriority2, (Object)this.displayPriority);
        }
    }

    public boolean isResizingAware() {
        return this.isResizingAware;
    }

    public void setResizingAware(boolean bl) {
        this.isResizingAware = bl;
    }
}

