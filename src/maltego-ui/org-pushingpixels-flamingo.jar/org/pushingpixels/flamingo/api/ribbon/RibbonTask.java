/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.ribbon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;
import org.pushingpixels.flamingo.api.ribbon.RibbonContextualTaskGroup;
import org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizeSequencingPolicies;
import org.pushingpixels.flamingo.api.ribbon.resize.RibbonBandResizeSequencingPolicy;

public class RibbonTask {
    private JRibbon ribbon;
    private ArrayList<AbstractRibbonBand<?>> bands;
    private String title;
    private RibbonContextualTaskGroup contextualGroup;
    private RibbonBandResizeSequencingPolicy resizeSequencingPolicy;
    private String keyTip;
    private RichTooltip richTooltip;

    public /* varargs */ RibbonTask(String string, AbstractRibbonBand<?> ... arrabstractRibbonBand) {
        if (arrabstractRibbonBand == null || arrabstractRibbonBand.length == 0) {
            throw new IllegalArgumentException("Cannot have empty ribbon task");
        }
        this.title = string;
        this.bands = new ArrayList();
        for (AbstractRibbonBand abstractRibbonBand : arrabstractRibbonBand) {
            abstractRibbonBand.setRibbonTask(this);
            this.bands.add(abstractRibbonBand);
        }
        this.resizeSequencingPolicy = new CoreRibbonResizeSequencingPolicies.RoundRobin(this);
    }

    public int getBandCount() {
        return this.bands.size();
    }

    public AbstractRibbonBand<?> getBand(int n) {
        return this.bands.get(n);
    }

    public String getTitle() {
        return this.title;
    }

    void setContextualGroup(RibbonContextualTaskGroup ribbonContextualTaskGroup) {
        if (this.contextualGroup != null) {
            throw new IllegalStateException("The task already belongs to another contextual task group");
        }
        this.contextualGroup = ribbonContextualTaskGroup;
    }

    public RibbonContextualTaskGroup getContextualGroup() {
        return this.contextualGroup;
    }

    public List<AbstractRibbonBand<?>> getBands() {
        return Collections.unmodifiableList(this.bands);
    }

    public void setTitle(String string) {
        this.title = string;
        if (this.ribbon != null) {
            this.ribbon.fireStateChanged();
        }
    }

    void setRibbon(JRibbon jRibbon) {
        if (this.ribbon != null) {
            throw new IllegalStateException("The task already belongs to another ribbon");
        }
        this.ribbon = jRibbon;
    }

    public RibbonBandResizeSequencingPolicy getResizeSequencingPolicy() {
        return this.resizeSequencingPolicy;
    }

    public void setResizeSequencingPolicy(RibbonBandResizeSequencingPolicy ribbonBandResizeSequencingPolicy) {
        this.resizeSequencingPolicy = ribbonBandResizeSequencingPolicy;
    }

    public String getKeyTip() {
        return this.keyTip;
    }

    public void setKeyTip(String string) {
        this.keyTip = string;
    }

    public void setRichTooltip(RichTooltip richTooltip) {
        this.richTooltip = richTooltip;
    }

    public RichTooltip getRichTooltip() {
        return this.richTooltip;
    }
}

