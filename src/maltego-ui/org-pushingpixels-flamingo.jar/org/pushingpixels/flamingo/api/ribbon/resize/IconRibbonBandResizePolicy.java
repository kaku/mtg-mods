/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.ribbon.resize;

import org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.resize.BaseRibbonBandResizePolicy;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.RibbonBandUI;

public class IconRibbonBandResizePolicy
extends BaseRibbonBandResizePolicy<AbstractBandControlPanel> {
    public IconRibbonBandResizePolicy(AbstractBandControlPanel abstractBandControlPanel) {
        super(abstractBandControlPanel);
    }

    @Override
    public int getPreferredWidth(int n, int n2) {
        AbstractRibbonBand abstractRibbonBand = this.controlPanel.getRibbonBand();
        RibbonBandUI ribbonBandUI = abstractRibbonBand.getUI();
        return ribbonBandUI.getPreferredCollapsedWidth();
    }

    @Override
    public void install(int n, int n2) {
    }
}

