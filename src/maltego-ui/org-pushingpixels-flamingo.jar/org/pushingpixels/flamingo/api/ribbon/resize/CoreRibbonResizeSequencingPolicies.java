/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.ribbon.resize;

import java.util.List;
import org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.RibbonTask;
import org.pushingpixels.flamingo.api.ribbon.resize.BaseRibbonBandResizeSequencingPolicy;
import org.pushingpixels.flamingo.api.ribbon.resize.RibbonBandResizePolicy;

public class CoreRibbonResizeSequencingPolicies {

    public static class CollapseFromLast
    extends BaseRibbonBandResizeSequencingPolicy {
        int nextIndex;

        public CollapseFromLast(RibbonTask ribbonTask) {
            super(ribbonTask);
        }

        @Override
        public void reset() {
            this.nextIndex = this.ribbonTask.getBandCount() - 1;
        }

        @Override
        public AbstractRibbonBand next() {
            AbstractRibbonBand abstractRibbonBand = this.ribbonTask.getBand(this.nextIndex);
            List<RibbonBandResizePolicy> list = abstractRibbonBand.getResizePolicies();
            if (abstractRibbonBand.getCurrentResizePolicy() == list.get(list.size() - 1)) {
                --this.nextIndex;
                if (this.nextIndex < 0) {
                    this.nextIndex = 0;
                }
            }
            return abstractRibbonBand;
        }
    }

    public static class RoundRobin
    extends BaseRibbonBandResizeSequencingPolicy {
        int nextIndex;

        public RoundRobin(RibbonTask ribbonTask) {
            super(ribbonTask);
        }

        @Override
        public void reset() {
            this.nextIndex = this.ribbonTask.getBandCount() - 1;
        }

        @Override
        public AbstractRibbonBand next() {
            AbstractRibbonBand abstractRibbonBand = this.ribbonTask.getBand(this.nextIndex);
            --this.nextIndex;
            if (this.nextIndex < 0) {
                this.nextIndex = this.ribbonTask.getBandCount() - 1;
            }
            return abstractRibbonBand;
        }
    }

}

