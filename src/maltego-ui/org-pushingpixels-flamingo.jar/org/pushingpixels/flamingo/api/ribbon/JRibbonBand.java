/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.ribbon;

import java.awt.ComponentOrientation;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.common.JCommandToggleButton;
import org.pushingpixels.flamingo.api.common.StringValuePair;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu;
import org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.JRibbonComponent;
import org.pushingpixels.flamingo.api.ribbon.RibbonElementPriority;
import org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies;
import org.pushingpixels.flamingo.api.ribbon.resize.RibbonBandResizePolicy;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.CommandButtonLayoutManagerBigFixed;
import org.pushingpixels.flamingo.internal.ui.ribbon.CommandButtonLayoutManagerBigFixedLandscape;
import org.pushingpixels.flamingo.internal.ui.ribbon.JBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.JRibbonGallery;

public class JRibbonBand
extends AbstractRibbonBand<JBandControlPanel> {
    public static final CommandButtonDisplayState BIG_FIXED_LANDSCAPE = new CommandButtonDisplayState("Big Fixed Landscape", 32){

        @Override
        public CommandButtonLayoutManager createLayoutManager(AbstractCommandButton abstractCommandButton) {
            return new CommandButtonLayoutManagerBigFixedLandscape();
        }
    };
    public static final CommandButtonDisplayState BIG_FIXED = new CommandButtonDisplayState("Big Fixed", 32){

        @Override
        public CommandButtonLayoutManager createLayoutManager(AbstractCommandButton abstractCommandButton) {
            return new CommandButtonLayoutManagerBigFixed();
        }
    };

    public JRibbonBand(String string, ResizableIcon resizableIcon) {
        this(string, resizableIcon, null);
    }

    public JRibbonBand(String string, ResizableIcon resizableIcon, ActionListener actionListener) {
        super(string, resizableIcon, actionListener, new JBandControlPanel());
        this.resizePolicies = Collections.unmodifiableList(CoreRibbonResizePolicies.getCorePoliciesPermissive(this));
        this.updateUI();
    }

    public void addCommandButton(AbstractCommandButton abstractCommandButton, RibbonElementPriority ribbonElementPriority) {
        abstractCommandButton.setHorizontalAlignment(2);
        ((JBandControlPanel)this.controlPanel).addCommandButton(abstractCommandButton, ribbonElementPriority);
    }

    public void addRibbonGallery(String string, List<StringValuePair<List<JCommandToggleButton>>> list, Map<RibbonElementPriority, Integer> map, int n, int n2, RibbonElementPriority ribbonElementPriority) {
        this.addRibbonGallery(string, list, map, n, n2, BIG_FIXED_LANDSCAPE, ribbonElementPriority);
    }

    public void addRibbonGallery(String string, List<StringValuePair<List<JCommandToggleButton>>> list, Map<RibbonElementPriority, Integer> map, int n, int n2, CommandButtonDisplayState commandButtonDisplayState, RibbonElementPriority ribbonElementPriority) {
        JRibbonGallery jRibbonGallery = new JRibbonGallery();
        jRibbonGallery.setButtonDisplayState(commandButtonDisplayState);
        jRibbonGallery.setName(string);
        for (Map.Entry<RibbonElementPriority, Integer> entry : map.entrySet()) {
            jRibbonGallery.setPreferredVisibleButtonCount(entry.getKey(), entry.getValue());
        }
        jRibbonGallery.setGroupMapping(list);
        jRibbonGallery.setPreferredPopupPanelDimension(n, n2);
        ((JBandControlPanel)this.controlPanel).addRibbonGallery(jRibbonGallery, ribbonElementPriority);
    }

    public /* varargs */ void addRibbonGalleryButtons(String string, String string2, JCommandToggleButton ... arrjCommandToggleButton) {
        JRibbonGallery jRibbonGallery = ((JBandControlPanel)this.controlPanel).getRibbonGallery(string);
        if (jRibbonGallery == null) {
            return;
        }
        jRibbonGallery.addRibbonGalleryButtons(string2, arrjCommandToggleButton);
    }

    public /* varargs */ void removeRibbonGalleryButtons(String string, JCommandToggleButton ... arrjCommandToggleButton) {
        JRibbonGallery jRibbonGallery = ((JBandControlPanel)this.controlPanel).getRibbonGallery(string);
        if (jRibbonGallery == null) {
            return;
        }
        jRibbonGallery.removeRibbonGalleryButtons(arrjCommandToggleButton);
    }

    public void setSelectedRibbonGalleryButton(String string, JCommandToggleButton jCommandToggleButton) {
        JRibbonGallery jRibbonGallery = ((JBandControlPanel)this.controlPanel).getRibbonGallery(string);
        if (jRibbonGallery == null) {
            return;
        }
        jRibbonGallery.setSelectedButton(jCommandToggleButton);
    }

    public void setRibbonGalleryButtonDisplayState(String string, CommandButtonDisplayState commandButtonDisplayState) {
        JRibbonGallery jRibbonGallery = ((JBandControlPanel)this.controlPanel).getRibbonGallery(string);
        if (jRibbonGallery == null) {
            return;
        }
        jRibbonGallery.setButtonDisplayState(commandButtonDisplayState);
    }

    public void setRibbonGalleryPopupCallback(String string, RibbonGalleryPopupCallback ribbonGalleryPopupCallback) {
        JRibbonGallery jRibbonGallery = ((JBandControlPanel)this.controlPanel).getRibbonGallery(string);
        if (jRibbonGallery == null) {
            return;
        }
        jRibbonGallery.setPopupCallback(ribbonGalleryPopupCallback);
    }

    public void setRibbonGalleryExpandKeyTip(String string, String string2) {
        JRibbonGallery jRibbonGallery = ((JBandControlPanel)this.controlPanel).getRibbonGallery(string);
        if (jRibbonGallery == null) {
            return;
        }
        jRibbonGallery.setExpandKeyTip(string2);
    }

    public void addRibbonComponent(JRibbonComponent jRibbonComponent) {
        ((JBandControlPanel)this.controlPanel).addRibbonComponent(jRibbonComponent);
    }

    public void addRibbonComponent(JRibbonComponent jRibbonComponent, int n) {
        int n2;
        int n3 = ((JBandControlPanel)this.controlPanel).getControlPanelGroupCount();
        String string = n3 > 0 ? ((JBandControlPanel)this.controlPanel).getControlPanelGroupTitle(n3 - 1) : null;
        int n4 = n2 = string == null ? 3 : 2;
        if (n <= 0 || n > n2) {
            throw new IllegalArgumentException("Row span value not supported. Should be in 1.." + n2 + " range");
        }
        ((JBandControlPanel)this.controlPanel).addRibbonComponent(jRibbonComponent, n);
    }

    public int startGroup() {
        return ((JBandControlPanel)this.controlPanel).startGroup();
    }

    public int startGroup(String string) {
        return ((JBandControlPanel)this.controlPanel).startGroup(string);
    }

    public void setGroupTitle(int n, String string) {
        ((JBandControlPanel)this.controlPanel).setGroupTitle(n, string);
    }

    public List<JRibbonComponent> getRibbonComponents(int n) {
        return ((JBandControlPanel)this.controlPanel).getRibbonComponents(n);
    }

    @Override
    public AbstractRibbonBand<JBandControlPanel> cloneBand() {
        JRibbonBand jRibbonBand = new JRibbonBand(this.getTitle(), this.getIcon(), this.getExpandActionListener());
        jRibbonBand.applyComponentOrientation(this.getComponentOrientation());
        return jRibbonBand;
    }

    public static interface RibbonGalleryPopupCallback {
        public void popupToBeShown(JCommandPopupMenu var1);
    }

}

