/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.ribbon;

import java.awt.event.ActionListener;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntry;

public class RibbonApplicationMenuEntrySecondary
extends RibbonApplicationMenuEntry {
    protected String descriptionText;
    protected PopupPanelCallback popupCallback;

    public RibbonApplicationMenuEntrySecondary(ResizableIcon resizableIcon, String string, ActionListener actionListener, JCommandButton.CommandButtonKind commandButtonKind) {
        super(resizableIcon, string, actionListener, commandButtonKind);
    }

    public String getDescriptionText() {
        return this.descriptionText;
    }

    public void setDescriptionText(String string) {
        this.descriptionText = string;
    }

    public void setPopupCallback(PopupPanelCallback popupPanelCallback) {
        this.popupCallback = popupPanelCallback;
    }

    public PopupPanelCallback getPopupCallback() {
        return this.popupCallback;
    }
}

