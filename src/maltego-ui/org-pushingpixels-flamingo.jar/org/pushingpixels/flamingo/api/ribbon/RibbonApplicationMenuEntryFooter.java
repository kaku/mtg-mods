/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.ribbon;

import java.awt.event.ActionListener;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntry;

public class RibbonApplicationMenuEntryFooter
extends RibbonApplicationMenuEntry {
    public RibbonApplicationMenuEntryFooter(ResizableIcon resizableIcon, String string, ActionListener actionListener) {
        super(resizableIcon, string, actionListener, JCommandButton.CommandButtonKind.ACTION_ONLY);
    }
}

