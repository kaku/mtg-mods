/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.ribbon;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JMenuBar;
import javax.swing.JPopupMenu;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import org.pushingpixels.flamingo.api.common.AsynchronousLoadListener;
import org.pushingpixels.flamingo.api.common.AsynchronousLoading;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelManager;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.JRibbonRootPane;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;
import org.pushingpixels.flamingo.internal.utils.KeyTipManager;
import org.pushingpixels.flamingo.internal.utils.KeyTipRenderingUtilities;
import org.pushingpixels.flamingo.internal.utils.RenderingUtils;

public class JRibbonFrame
extends JFrame {
    private JRibbon ribbon;
    private ResizableIcon appIcon;
    private boolean wasSetIconImagesCalled;

    public JRibbonFrame() throws HeadlessException {
        this.initRibbon();
    }

    public JRibbonFrame(GraphicsConfiguration graphicsConfiguration) {
        super(graphicsConfiguration);
        this.initRibbon();
    }

    public JRibbonFrame(String string) throws HeadlessException {
        super(string);
        this.initRibbon();
    }

    public JRibbonFrame(String string, GraphicsConfiguration graphicsConfiguration) {
        super(string, graphicsConfiguration);
        this.initRibbon();
    }

    @Override
    public void setLayout(LayoutManager layoutManager) {
        LayoutManager layoutManager2;
        if (layoutManager.getClass() != RibbonFrameLayout.class && (layoutManager2 = this.getLayout()) != null) {
            throw new IllegalArgumentException("Can't set a custom layout manager on JRibbonFrame");
        }
        super.setLayout(layoutManager);
    }

    @Override
    public void setJMenuBar(JMenuBar jMenuBar) {
        throw new IllegalArgumentException("Can't set a menu bar on JRibbonFrame");
    }

    @Override
    public void setContentPane(Container container) {
        throw new IllegalArgumentException("Can't set the content pane on JRibbonFrame");
    }

    private void initRibbon() {
        this.setLayout(new RibbonFrameLayout());
        this.ribbon = new JRibbon(this);
        this.add((Component)this.ribbon, "North");
        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener(){
            private boolean prevAltModif;

            @Override
            public void eventDispatched(AWTEvent aWTEvent) {
                Component component;
                Object object = aWTEvent.getSource();
                if (object instanceof Component && ((component = (Component)object) == JRibbonFrame.this || SwingUtilities.getWindowAncestor(component) == JRibbonFrame.this)) {
                    AWTEvent aWTEvent2;
                    if (aWTEvent instanceof KeyEvent) {
                        aWTEvent2 = (KeyEvent)aWTEvent;
                        switch (aWTEvent2.getID()) {
                            case 401: {
                                break;
                            }
                            case 402: {
                                boolean bl = this.prevAltModif;
                                boolean bl2 = this.prevAltModif = aWTEvent2.getModifiersEx() == 512;
                                if (bl && aWTEvent2.getKeyCode() == 18) break;
                                char c = aWTEvent2.getKeyChar();
                                if (Character.isLetter(c) || Character.isDigit(c)) {
                                    KeyTipManager.defaultManager().handleKeyPress(c);
                                }
                                if (aWTEvent2.getKeyCode() == 18 || aWTEvent2.getKeyCode() == 121) {
                                    if (aWTEvent2.getModifiers() != 0 || aWTEvent2.getModifiersEx() != 0) break;
                                    boolean bl3 = !PopupPanelManager.defaultManager().getShownPath().isEmpty();
                                    PopupPanelManager.defaultManager().hidePopups(null);
                                    if (bl3 || KeyTipManager.defaultManager().isShowingKeyTips()) {
                                        KeyTipManager.defaultManager().hideAllKeyTips();
                                    } else {
                                        KeyTipManager.defaultManager().showRootKeyTipChain(JRibbonFrame.this);
                                    }
                                }
                                if (aWTEvent2.getKeyCode() != 27) break;
                                KeyTipManager.defaultManager().showPreviousChain();
                            }
                        }
                    }
                    if (aWTEvent instanceof MouseEvent) {
                        aWTEvent2 = (MouseEvent)aWTEvent;
                        switch (aWTEvent2.getID()) {
                            case 500: 
                            case 501: 
                            case 502: 
                            case 506: {
                                KeyTipManager.defaultManager().hideAllKeyTips();
                            }
                        }
                    }
                }
            }
        }, 24);
        final KeyTipLayer keyTipLayer = new KeyTipLayer();
        JRootPane jRootPane = this.getRootPane();
        JLayeredPane jLayeredPane = jRootPane.getLayeredPane();
        final LayoutManager layoutManager = jRootPane.getLayout();
        jRootPane.setLayout(new LayoutManager(){

            @Override
            public void addLayoutComponent(String string, Component component) {
                layoutManager.addLayoutComponent(string, component);
            }

            @Override
            public void layoutContainer(Container container) {
                layoutManager.layoutContainer(container);
                JRibbonFrame jRibbonFrame = JRibbonFrame.this;
                if (jRibbonFrame.getRootPane().getWindowDecorationStyle() != 0) {
                    keyTipLayer.setBounds(jRibbonFrame.getRootPane().getBounds());
                } else {
                    keyTipLayer.setBounds(jRibbonFrame.getRootPane().getContentPane().getBounds());
                }
            }

            @Override
            public Dimension minimumLayoutSize(Container container) {
                return layoutManager.minimumLayoutSize(container);
            }

            @Override
            public Dimension preferredLayoutSize(Container container) {
                return layoutManager.preferredLayoutSize(container);
            }

            @Override
            public void removeLayoutComponent(Component component) {
                layoutManager.removeLayoutComponent(component);
            }
        });
        jLayeredPane.add((Component)keyTipLayer, (Object)(JLayeredPane.DEFAULT_LAYER + 60));
        this.addWindowListener(new WindowAdapter(){

            @Override
            public void windowDeactivated(WindowEvent windowEvent) {
                KeyTipManager keyTipManager = KeyTipManager.defaultManager();
                if (keyTipManager.isShowingKeyTips()) {
                    keyTipManager.hideAllKeyTips();
                }
            }
        });
        KeyTipManager.defaultManager().addKeyTipListener(new KeyTipManager.KeyTipListener(){

            @Override
            public void keyTipsHidden(KeyTipManager.KeyTipEvent keyTipEvent) {
                if (keyTipEvent.getSource() == JRibbonFrame.this) {
                    keyTipLayer.setVisible(false);
                }
            }

            @Override
            public void keyTipsShown(KeyTipManager.KeyTipEvent keyTipEvent) {
                if (keyTipEvent.getSource() == JRibbonFrame.this) {
                    keyTipLayer.setVisible(true);
                }
            }
        });
        ToolTipManager.sharedInstance().setLightWeightPopupEnabled(false);
        JPopupMenu.setDefaultLightWeightPopupEnabled(false);
        super.setIconImages(Arrays.asList(FlamingoUtilities.getBlankImage(16, 16)));
    }

    public JRibbon getRibbon() {
        return this.ribbon;
    }

    @Override
    protected JRootPane createRootPane() {
        JRibbonRootPane jRibbonRootPane = new JRibbonRootPane();
        jRibbonRootPane.setOpaque(true);
        return jRibbonRootPane;
    }

    @Override
    public synchronized void setIconImages(List<? extends Image> list) {
        super.setIconImages(list);
        this.wasSetIconImagesCalled = true;
    }

    public synchronized void setApplicationIcon(final ResizableIcon resizableIcon) {
        new Thread("Set App Icon"){

            @Override
            public void run() {
                AsynchronousLoading asynchronousLoading;
                if (resizableIcon instanceof AsynchronousLoading && (asynchronousLoading = (AsynchronousLoading)((Object)resizableIcon)).isLoading()) {
                    final CountDownLatch countDownLatch = new CountDownLatch(1);
                    final boolean[] arrbl = new boolean[1];
                    AsynchronousLoadListener asynchronousLoadListener = new AsynchronousLoadListener(){

                        @Override
                        public void completed(boolean bl) {
                            arrbl[0] = bl;
                            countDownLatch.countDown();
                        }
                    };
                    asynchronousLoading.addAsynchronousLoadListener(asynchronousLoadListener);
                    try {
                        countDownLatch.await();
                    }
                    catch (InterruptedException var5_5) {
                        // empty catch block
                    }
                    asynchronousLoading.removeAsynchronousLoadListener(asynchronousLoadListener);
                }
                JRibbonFrame.this.setApplicationAndMenuButtonIcon(resizableIcon);
            }

        }.start();
    }

    private void setApplicationAndMenuButtonIcon(final ResizableIcon resizableIcon) {
        if (System.getProperty("os.name").startsWith("Mac")) {
            final Image image = JRibbonFrame.getImage(resizableIcon, 16);
            final Image image2 = JRibbonFrame.getImage(resizableIcon, 128);
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    if (image != null) {
                        JRibbonFrame.this.setLegacyIconImages(Arrays.asList(image));
                    }
                    if (image2 != null) {
                        try {
                            Class class_ = Class.forName("com.apple.eawt.Application");
                            if (class_ != null) {
                                Object obj = class_.newInstance();
                                Method method = class_.getDeclaredMethod("setDockIconImage", Image.class);
                                if (method != null) {
                                    method.invoke(obj, image2);
                                }
                            }
                        }
                        catch (Throwable var1_2) {
                            var1_2.printStackTrace();
                        }
                    }
                    JRibbonFrame.this.setMainAppIcon(resizableIcon);
                }
            });
        } else {
            Image image;
            Image image3;
            final ArrayList<Image> arrayList = new ArrayList<Image>();
            Image image4 = JRibbonFrame.getImage(resizableIcon, 16);
            if (image4 != null) {
                arrayList.add(image4);
            }
            if ((image3 = JRibbonFrame.getImage(resizableIcon, 32)) != null) {
                arrayList.add(image3);
            }
            if ((image = JRibbonFrame.getImage(resizableIcon, 64)) != null) {
                arrayList.add(image);
            }
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    if (!arrayList.isEmpty()) {
                        JRibbonFrame.this.setLegacyIconImages(arrayList);
                    }
                    JRibbonFrame.this.setMainAppIcon(resizableIcon);
                }
            });
        }
    }

    private void setMainAppIcon(ResizableIcon resizableIcon) {
        this.appIcon = resizableIcon;
        FlamingoUtilities.updateRibbonFrameIconImages(this);
    }

    private void setLegacyIconImages(List<Image> list) {
        if (this.wasSetIconImagesCalled) {
            return;
        }
        super.setIconImages(list);
    }

    private static Image getImage(ResizableIcon resizableIcon, int n) {
        Object object;
        Object object2;
        resizableIcon.setDimension(new Dimension(n, n));
        if (resizableIcon instanceof AsynchronousLoading && (object = (AsynchronousLoading)((Object)resizableIcon)).isLoading()) {
            object2 = new CountDownLatch(1);
            final boolean[] arrbl = new boolean[1];
            AsynchronousLoadListener asynchronousLoadListener = new AsynchronousLoadListener((CountDownLatch)object2){
                final /* synthetic */ CountDownLatch val$latch;

                @Override
                public void completed(boolean bl) {
                    arrbl[0] = bl;
                    this.val$latch.countDown();
                }
            };
            object.addAsynchronousLoadListener(asynchronousLoadListener);
            try {
                object2.await();
            }
            catch (InterruptedException var6_6) {
                // empty catch block
            }
            object.removeAsynchronousLoadListener(asynchronousLoadListener);
            if (!arrbl[0]) {
                return null;
            }
            if (object.isLoading()) {
                return null;
            }
        }
        object = FlamingoUtilities.getBlankImage(n, n);
        object2 = (Graphics2D)object.getGraphics().create();
        resizableIcon.paintIcon(null, (Graphics)object2, 0, 0);
        object2.dispose();
        return object;
    }

    public synchronized ResizableIcon getApplicationIcon() {
        return this.appIcon;
    }

    public boolean isShowingKeyTips() {
        return KeyTipManager.defaultManager().isShowingKeyTips();
    }

    private class KeyTipLayer
    extends JComponent {
        public KeyTipLayer() {
            this.setOpaque(false);
            try {
                Class class_ = Class.forName("com.sun.awt.AWTUtilities");
                Method method = class_.getMethod("setComponentMixingCutoutShape", Component.class, Shape.class);
                method.invoke(null, this, new Rectangle());
            }
            catch (Throwable var2_3) {
                // empty catch block
            }
        }

        @Override
        public synchronized void addMouseListener(MouseListener mouseListener) {
        }

        @Override
        public synchronized void addMouseMotionListener(MouseMotionListener mouseMotionListener) {
        }

        @Override
        public synchronized void addMouseWheelListener(MouseWheelListener mouseWheelListener) {
        }

        @Override
        public synchronized void addKeyListener(KeyListener keyListener) {
        }

        @Override
        protected void paintComponent(Graphics graphics) {
            JRibbonFrame jRibbonFrame = (JRibbonFrame)SwingUtilities.getWindowAncestor(this);
            if (!jRibbonFrame.isShowingKeyTips()) {
                return;
            }
            if (!jRibbonFrame.isActive()) {
                return;
            }
            Collection<KeyTipManager.KeyTipLink> collection = KeyTipManager.defaultManager().getCurrentlyShownKeyTips();
            if (collection != null) {
                Graphics2D graphics2D = (Graphics2D)graphics.create();
                RenderingUtils.installDesktopHints(graphics2D);
                RenderingUtils.setupTextAntialiasing(graphics2D, null);
                for (KeyTipManager.KeyTipLink keyTipLink : collection) {
                    if (SwingUtilities.getAncestorOfClass(JPopupPanel.class, keyTipLink.comp) != null) continue;
                    Rectangle rectangle = keyTipLink.comp.getBounds();
                    if (!keyTipLink.comp.isShowing() || rectangle.getWidth() == 0.0 || rectangle.getHeight() == 0.0) continue;
                    Dimension dimension = KeyTipRenderingUtilities.getPrefSize(graphics2D.getFontMetrics(), keyTipLink.keyTipString);
                    Point point = keyTipLink.prefAnchorPoint;
                    Point point2 = SwingUtilities.convertPoint(keyTipLink.comp, point, this);
                    Container container = SwingUtilities.getAncestorOfClass(AbstractBandControlPanel.class, keyTipLink.comp);
                    if (container != null) {
                        if (this.hasClientPropertySetToTrue(keyTipLink.comp, "flamingo.internal.ribbonBandControlPanel.topRow")) {
                            point2 = SwingUtilities.convertPoint(keyTipLink.comp, point, container);
                            point2.y = 0;
                            point2 = SwingUtilities.convertPoint(container, point2, this);
                        }
                        if (this.hasClientPropertySetToTrue(keyTipLink.comp, "flamingo.internal.ribbonBandControlPanel.midRow")) {
                            point2 = SwingUtilities.convertPoint(keyTipLink.comp, point, container);
                            point2.y = container.getHeight() / 2;
                            point2 = SwingUtilities.convertPoint(container, point2, this);
                        }
                        if (this.hasClientPropertySetToTrue(keyTipLink.comp, "flamingo.internal.ribbonBandControlPanel.bottomRow")) {
                            point2 = SwingUtilities.convertPoint(keyTipLink.comp, point, container);
                            point2.y = container.getHeight();
                            point2 = SwingUtilities.convertPoint(container, point2, this);
                        }
                    }
                    KeyTipRenderingUtilities.renderKeyTip(graphics2D, this, new Rectangle(point2.x - dimension.width / 2, point2.y - dimension.height / 2, dimension.width, dimension.height), keyTipLink.keyTipString, keyTipLink.enabled);
                }
                graphics2D.dispose();
            }
        }

        private boolean hasClientPropertySetToTrue(Component component, String string) {
            while (component != null) {
                JComponent jComponent;
                if (component instanceof JComponent && Boolean.TRUE.equals((jComponent = (JComponent)component).getClientProperty(string))) {
                    return true;
                }
                component = component.getParent();
            }
            return false;
        }

        @Override
        public boolean contains(int n, int n2) {
            return false;
        }
    }

    private class RibbonFrameLayout
    extends BorderLayout {
        private RibbonFrameLayout() {
        }

        @Override
        public void addLayoutComponent(Component component, Object object) {
            if (object != null && object.equals("North")) {
                if (this.getLayoutComponent("North") != null) {
                    throw new IllegalArgumentException("Already has a NORTH JRibbon component");
                }
                if (!(component instanceof JRibbon)) {
                    throw new IllegalArgumentException("Can't add non-JRibbon component to NORTH location");
                }
            }
            super.addLayoutComponent(component, object);
        }

        @Override
        public void removeLayoutComponent(Component component) {
            if (component instanceof JRibbon) {
                throw new IllegalArgumentException("Can't remove JRibbon component");
            }
            super.removeLayoutComponent(component);
        }
    }

}

