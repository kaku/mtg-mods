/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.ribbon.resize;

import java.awt.Dimension;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.ribbon.JFlowRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.JRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.JRibbonComponent;
import org.pushingpixels.flamingo.api.ribbon.RibbonElementPriority;
import org.pushingpixels.flamingo.api.ribbon.resize.BaseRibbonBandResizePolicy;
import org.pushingpixels.flamingo.api.ribbon.resize.IconRibbonBandResizePolicy;
import org.pushingpixels.flamingo.api.ribbon.resize.RibbonBandResizePolicy;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.JBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.JFlowBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.JRibbonGallery;
import org.pushingpixels.flamingo.internal.ui.ribbon.RibbonComponentUI;

public class CoreRibbonResizePolicies {
    public static List<RibbonBandResizePolicy> getCorePoliciesPermissive(JRibbonBand jRibbonBand) {
        ArrayList<RibbonBandResizePolicy> arrayList = new ArrayList<RibbonBandResizePolicy>();
        arrayList.add(new None((JBandControlPanel)jRibbonBand.getControlPanel()));
        arrayList.add(new Low2Mid((JBandControlPanel)jRibbonBand.getControlPanel()));
        arrayList.add(new Mid2Mid((JBandControlPanel)jRibbonBand.getControlPanel()));
        arrayList.add(new Mirror((JBandControlPanel)jRibbonBand.getControlPanel()));
        arrayList.add(new Mid2Low((JBandControlPanel)jRibbonBand.getControlPanel()));
        arrayList.add(new High2Mid((JBandControlPanel)jRibbonBand.getControlPanel()));
        arrayList.add(new High2Low((JBandControlPanel)jRibbonBand.getControlPanel()));
        arrayList.add(new IconRibbonBandResizePolicy((AbstractBandControlPanel)jRibbonBand.getControlPanel()));
        return arrayList;
    }

    public static List<RibbonBandResizePolicy> getCorePoliciesRestrictive(JRibbonBand jRibbonBand) {
        ArrayList<RibbonBandResizePolicy> arrayList = new ArrayList<RibbonBandResizePolicy>();
        arrayList.add(new Mirror((JBandControlPanel)jRibbonBand.getControlPanel()));
        arrayList.add(new Mid2Low((JBandControlPanel)jRibbonBand.getControlPanel()));
        arrayList.add(new High2Mid((JBandControlPanel)jRibbonBand.getControlPanel()));
        arrayList.add(new High2Low((JBandControlPanel)jRibbonBand.getControlPanel()));
        arrayList.add(new IconRibbonBandResizePolicy((AbstractBandControlPanel)jRibbonBand.getControlPanel()));
        return arrayList;
    }

    public static List<RibbonBandResizePolicy> getCorePoliciesNone(JRibbonBand jRibbonBand) {
        ArrayList<RibbonBandResizePolicy> arrayList = new ArrayList<RibbonBandResizePolicy>();
        arrayList.add(new Mirror((JBandControlPanel)jRibbonBand.getControlPanel()));
        arrayList.add(new IconRibbonBandResizePolicy((AbstractBandControlPanel)jRibbonBand.getControlPanel()));
        return arrayList;
    }

    public static List<RibbonBandResizePolicy> getCoreFlowPoliciesRestrictive(JFlowRibbonBand jFlowRibbonBand, int n) {
        int n2;
        ArrayList<RibbonBandResizePolicy> arrayList = new ArrayList<RibbonBandResizePolicy>();
        for (n2 = 0; n2 < n; ++n2) {
            arrayList.add(new FlowTwoRows((JFlowBandControlPanel)jFlowRibbonBand.getControlPanel()));
        }
        for (n2 = 0; n2 < n; ++n2) {
            arrayList.add(new FlowThreeRows((JFlowBandControlPanel)jFlowRibbonBand.getControlPanel()));
        }
        arrayList.add(new IconRibbonBandResizePolicy((AbstractBandControlPanel)jFlowRibbonBand.getControlPanel()));
        return arrayList;
    }

    public static class FlowThreeRows
    extends BaseRibbonBandResizePolicy<JFlowBandControlPanel> {
        public FlowThreeRows(JFlowBandControlPanel jFlowBandControlPanel) {
            super(jFlowBandControlPanel);
        }

        @Override
        public int getPreferredWidth(int n, int n2) {
            int n3;
            int n4 = ((JFlowBandControlPanel)this.controlPanel).getFlowComponents().size();
            int[] arrn = new int[n4];
            int n5 = 0;
            int n6 = 0;
            for (JComponent jComponent : ((JFlowBandControlPanel)this.controlPanel).getFlowComponents()) {
                n3 = jComponent.getPreferredSize().width;
                arrn[n5++] = n3;
                n6 += n3 + n2;
            }
            for (int i = 0; i < n4 - 2; ++i) {
                for (int j = i + 1; j < n4 - 1; ++j) {
                    int n7;
                    int n8;
                    int n9;
                    n3 = 0;
                    for (n9 = 0; n9 <= i; ++n9) {
                        n3 += arrn[n9] + n2;
                    }
                    n9 = 0;
                    for (n8 = i + 1; n8 <= j; ++n8) {
                        n9 += arrn[n8] + n2;
                    }
                    n8 = 0;
                    for (n7 = j + 1; n7 < n4; ++n7) {
                        n8 += arrn[n7] + n2;
                    }
                    n7 = Math.max(Math.max(n3, n9), n8);
                    if (n7 >= n6) continue;
                    n6 = n7;
                }
            }
            return n6;
        }

        @Override
        public void install(int n, int n2) {
        }
    }

    public static class FlowTwoRows
    extends BaseRibbonBandResizePolicy<JFlowBandControlPanel> {
        public FlowTwoRows(JFlowBandControlPanel jFlowBandControlPanel) {
            super(jFlowBandControlPanel);
        }

        @Override
        public int getPreferredWidth(int n, int n2) {
            int n3;
            int n4 = ((JFlowBandControlPanel)this.controlPanel).getFlowComponents().size();
            int[] arrn = new int[n4];
            int n5 = 0;
            int n6 = 0;
            for (JComponent jComponent : ((JFlowBandControlPanel)this.controlPanel).getFlowComponents()) {
                n3 = jComponent.getPreferredSize().width;
                arrn[n5++] = n3;
                n6 += n3 + n2;
            }
            for (int i = 0; i < n4 - 1; ++i) {
                int n7;
                int n8 = 0;
                for (n3 = 0; n3 <= i; ++n3) {
                    n8 += arrn[n3] + n2;
                }
                n3 = 0;
                for (n7 = i + 1; n7 < n4; ++n7) {
                    n3 += arrn[n7] + n2;
                }
                n7 = Math.max(n8, n3);
                if (n7 >= n6) continue;
                n6 = n7;
            }
            return n6;
        }

        @Override
        public void install(int n, int n2) {
        }
    }

    public static final class High2Low
    extends BaseCoreRibbonBandResizePolicy {
        public High2Low(JBandControlPanel jBandControlPanel) {
            super(jBandControlPanel, new Mapping(){

                @Override
                public RibbonElementPriority map(RibbonElementPriority ribbonElementPriority) {
                    return RibbonElementPriority.LOW;
                }
            });
        }

    }

    public static final class High2Mid
    extends BaseCoreRibbonBandResizePolicy {
        public High2Mid(JBandControlPanel jBandControlPanel) {
            super(jBandControlPanel, new Mapping(){

                @Override
                public RibbonElementPriority map(RibbonElementPriority ribbonElementPriority) {
                    switch (ribbonElementPriority) {
                        case TOP: {
                            return RibbonElementPriority.MEDIUM;
                        }
                        case MEDIUM: {
                            return RibbonElementPriority.LOW;
                        }
                        case LOW: {
                            return RibbonElementPriority.LOW;
                        }
                    }
                    return null;
                }
            });
        }

    }

    public static final class Mid2Low
    extends BaseCoreRibbonBandResizePolicy {
        public Mid2Low(JBandControlPanel jBandControlPanel) {
            super(jBandControlPanel, new Mapping(){

                @Override
                public RibbonElementPriority map(RibbonElementPriority ribbonElementPriority) {
                    switch (ribbonElementPriority) {
                        case TOP: {
                            return RibbonElementPriority.TOP;
                        }
                        case MEDIUM: {
                            return RibbonElementPriority.LOW;
                        }
                        case LOW: {
                            return RibbonElementPriority.LOW;
                        }
                    }
                    return null;
                }
            });
        }

    }

    public static final class Mirror
    extends BaseCoreRibbonBandResizePolicy {
        public Mirror(JBandControlPanel jBandControlPanel) {
            super(jBandControlPanel, new Mapping(){

                @Override
                public RibbonElementPriority map(RibbonElementPriority ribbonElementPriority) {
                    return ribbonElementPriority;
                }
            });
        }

    }

    public static final class Mid2Mid
    extends BaseCoreRibbonBandResizePolicy {
        public Mid2Mid(JBandControlPanel jBandControlPanel) {
            super(jBandControlPanel, new Mapping(){

                @Override
                public RibbonElementPriority map(RibbonElementPriority ribbonElementPriority) {
                    switch (ribbonElementPriority) {
                        case TOP: {
                            return RibbonElementPriority.TOP;
                        }
                        case MEDIUM: {
                            return RibbonElementPriority.MEDIUM;
                        }
                        case LOW: {
                            return RibbonElementPriority.MEDIUM;
                        }
                    }
                    return null;
                }
            });
        }

    }

    public static final class Low2Mid
    extends BaseCoreRibbonBandResizePolicy {
        public Low2Mid(JBandControlPanel jBandControlPanel) {
            super(jBandControlPanel, new Mapping(){

                @Override
                public RibbonElementPriority map(RibbonElementPriority ribbonElementPriority) {
                    switch (ribbonElementPriority) {
                        case TOP: {
                            return RibbonElementPriority.TOP;
                        }
                        case MEDIUM: {
                            return RibbonElementPriority.TOP;
                        }
                        case LOW: {
                            return RibbonElementPriority.MEDIUM;
                        }
                    }
                    return null;
                }
            });
        }

    }

    public static final class None
    extends BaseCoreRibbonBandResizePolicy {
        public None(JBandControlPanel jBandControlPanel) {
            super(jBandControlPanel, new Mapping(){

                @Override
                public RibbonElementPriority map(RibbonElementPriority ribbonElementPriority) {
                    return RibbonElementPriority.TOP;
                }
            });
        }

    }

    protected static abstract class BaseCoreRibbonBandResizePolicy
    extends BaseRibbonBandResizePolicy<JBandControlPanel> {
        protected Mapping mapping;

        protected BaseCoreRibbonBandResizePolicy(JBandControlPanel jBandControlPanel, Mapping mapping) {
            super(jBandControlPanel);
            this.mapping = mapping;
        }

        protected int getWidth(int n, List<AbstractCommandButton> list, List<AbstractCommandButton> list2, List<AbstractCommandButton> list3) {
            int n2;
            int n3 = 0;
            boolean bl = false;
            for (AbstractCommandButton abstractCommandButton2 : list) {
                if (bl) {
                    n3 += n;
                }
                n3 += this.getPreferredWidth(abstractCommandButton2, RibbonElementPriority.TOP);
                bl = true;
            }
            int n4 = list2.size();
            if (n4 > 0) {
                while (list2.size() % 3 != 0 && list3.size() > 0) {
                    AbstractCommandButton abstractCommandButton2;
                    abstractCommandButton2 = list3.remove(0);
                    list2.add(abstractCommandButton2);
                }
            }
            int n5 = 0;
            int n6 = 0;
            for (AbstractCommandButton abstractCommandButton32 : list2) {
                n2 = this.getPreferredWidth(abstractCommandButton32, RibbonElementPriority.MEDIUM);
                n6 = Math.max(n6, n2);
                if (++n5 != 3) continue;
                n5 = 0;
                if (bl) {
                    n3 += n;
                }
                n3 += n6;
                bl = true;
                n6 = 0;
            }
            if (n6 > 0) {
                if (bl) {
                    n3 += n;
                }
                n3 += n6;
                bl = true;
            }
            n5 = 0;
            n6 = 0;
            for (AbstractCommandButton abstractCommandButton32 : list3) {
                n2 = this.getPreferredWidth(abstractCommandButton32, RibbonElementPriority.LOW);
                n6 = Math.max(n6, n2);
                if (++n5 != 3) continue;
                n5 = 0;
                if (bl) {
                    n3 += n;
                }
                n3 += n6;
                bl = true;
                n6 = 0;
            }
            if (n6 > 0) {
                if (bl) {
                    n3 += n;
                }
                n3 += n6;
                bl = true;
            }
            return n3;
        }

        private int getPreferredWidth(AbstractCommandButton abstractCommandButton, RibbonElementPriority ribbonElementPriority) {
            CommandButtonDisplayState commandButtonDisplayState = null;
            switch (ribbonElementPriority) {
                case TOP: {
                    commandButtonDisplayState = CommandButtonDisplayState.BIG;
                    break;
                }
                case MEDIUM: {
                    commandButtonDisplayState = CommandButtonDisplayState.MEDIUM;
                    break;
                }
                case LOW: {
                    commandButtonDisplayState = CommandButtonDisplayState.SMALL;
                }
            }
            return commandButtonDisplayState.createLayoutManager((AbstractCommandButton)abstractCommandButton).getPreferredSize((AbstractCommandButton)abstractCommandButton).width;
        }

        @Override
        public int getPreferredWidth(int n, int n2) {
            int n3 = 0;
            Insets insets = ((JBandControlPanel)this.controlPanel).getInsets();
            for (JBandControlPanel.ControlPanelGroup controlPanelGroup : ((JBandControlPanel)this.controlPanel).getControlPanelGroups()) {
                boolean bl = controlPanelGroup.isCoreContent();
                if (bl) {
                    int n4;
                    List<JRibbonComponent> list = controlPanelGroup.getRibbonComps();
                    Map<JRibbonComponent, Integer> map = controlPanelGroup.getRibbonCompsRowSpans();
                    int n5 = n4 = controlPanelGroup.getGroupTitle() == null ? 0 : 1;
                    int n6 = 0;
                    for (int i = 0; i < list.size(); ++i) {
                        JRibbonComponent jRibbonComponent = list.get(i);
                        int n7 = map.get(jRibbonComponent);
                        int n8 = n5 + n7;
                        if (n8 > 3) {
                            n3 += n6;
                            n3 += n2;
                            n6 = 0;
                            n5 = n4;
                        }
                        RibbonElementPriority ribbonElementPriority = RibbonElementPriority.TOP;
                        if (jRibbonComponent.isResizingAware()) {
                            ribbonElementPriority = this.mapping.map(RibbonElementPriority.TOP);
                        }
                        int n9 = jRibbonComponent.getUI().getPreferredSize((RibbonElementPriority)ribbonElementPriority).width;
                        n6 = Math.max(n6, n9);
                        n5 += n7;
                    }
                    if (n5 > 0 && n5 <= 3) {
                        n3 += n6;
                        n3 += n2;
                    }
                } else {
                    int n10 = n - insets.top - insets.bottom;
                    n3 += this.getPreferredGalleryWidth(controlPanelGroup, n10, n2);
                    n3 += this.getPreferredButtonWidth(controlPanelGroup, n2);
                }
                n3 += n2 * 3 / 2;
            }
            n3 -= n2 * 3 / 2;
            n3 += insets.left + insets.right;
            return n3 += n2;
        }

        protected int getPreferredButtonWidth(JBandControlPanel.ControlPanelGroup controlPanelGroup, int n) {
            HashMap hashMap = new HashMap();
            for (RibbonElementPriority ribbonElementPriority2 : RibbonElementPriority.values()) {
                hashMap.put(ribbonElementPriority2, new ArrayList());
            }
            for (RibbonElementPriority ribbonElementPriority2 : RibbonElementPriority.values()) {
                RibbonElementPriority ribbonElementPriority3 = this.mapping.map(ribbonElementPriority2);
                for (AbstractCommandButton abstractCommandButton : controlPanelGroup.getRibbonButtons(ribbonElementPriority2)) {
                    ((List)hashMap.get((Object)ribbonElementPriority3)).add(abstractCommandButton);
                }
            }
            return this.getWidth(n, (List)hashMap.get((Object)RibbonElementPriority.TOP), (List)hashMap.get((Object)RibbonElementPriority.MEDIUM), (List)hashMap.get((Object)RibbonElementPriority.LOW));
        }

        private int getPreferredGalleryWidth(JBandControlPanel.ControlPanelGroup controlPanelGroup, int n, int n2) {
            int n3 = 0;
            for (RibbonElementPriority ribbonElementPriority : RibbonElementPriority.values()) {
                RibbonElementPriority ribbonElementPriority2 = this.mapping.map(ribbonElementPriority);
                for (JRibbonGallery jRibbonGallery : controlPanelGroup.getRibbonGalleries(ribbonElementPriority)) {
                    n3 += jRibbonGallery.getPreferredWidth(ribbonElementPriority2, n) + n2;
                }
            }
            return n3;
        }

        @Override
        public void install(int n, int n2) {
            for (JBandControlPanel.ControlPanelGroup controlPanelGroup : ((JBandControlPanel)this.controlPanel).getControlPanelGroups()) {
                reference var9_13;
                RibbonElementPriority ribbonElementPriority;
                Object object;
                HashMap hashMap;
                boolean bl = controlPanelGroup.isCoreContent();
                if (bl) {
                    hashMap = controlPanelGroup.getRibbonComps();
                    for (int i = 0; i < hashMap.size(); ++i) {
                        object = (JRibbonComponent)hashMap.get(i);
                        var9_13 = RibbonElementPriority.TOP;
                        if (object.isResizingAware()) {
                            var9_13 = this.mapping.map(RibbonElementPriority.TOP);
                        }
                        object.setDisplayPriority(var9_13);
                    }
                    continue;
                }
                for (RibbonElementPriority ribbonElementPriority2 : RibbonElementPriority.values()) {
                    ribbonElementPriority = this.mapping.map(ribbonElementPriority2);
                    for (JRibbonGallery jRibbonGallery : controlPanelGroup.getRibbonGalleries(ribbonElementPriority2)) {
                        jRibbonGallery.setDisplayPriority(ribbonElementPriority);
                    }
                }
                hashMap = new HashMap();
                Object object3 = RibbonElementPriority.values();
                object = object3.length;
                for (var9_13 = (reference)false ? 1 : 0; var9_13 < object; ++var9_13) {
                    ribbonElementPriority = object3[var9_13];
                    hashMap.put(ribbonElementPriority, new ArrayList());
                }
                object3 = RibbonElementPriority.values();
                object = object3.length;
                for (var9_13 = (reference)false ? 1 : 0; var9_13 < object; ++var9_13) {
                    ribbonElementPriority = object3[var9_13];
                    RibbonElementPriority ribbonElementPriority3 = this.mapping.map(ribbonElementPriority);
                    for (AbstractCommandButton abstractCommandButton : controlPanelGroup.getRibbonButtons(ribbonElementPriority)) {
                        ((List)hashMap.get((Object)ribbonElementPriority3)).add(abstractCommandButton);
                    }
                }
                for (AbstractCommandButton abstractCommandButton : (List)hashMap.get((Object)RibbonElementPriority.TOP)) {
                    abstractCommandButton.setDisplayState(CommandButtonDisplayState.BIG);
                }
                if (((List)hashMap.get((Object)RibbonElementPriority.MEDIUM)).size() > 0) {
                    while (((List)hashMap.get((Object)RibbonElementPriority.MEDIUM)).size() % 3 != 0 && ((List)hashMap.get((Object)RibbonElementPriority.LOW)).size() > 0) {
                        object3 = (AbstractCommandButton)((List)hashMap.get((Object)RibbonElementPriority.LOW)).get(0);
                        ((List)hashMap.get((Object)RibbonElementPriority.LOW)).remove(object3);
                        ((List)hashMap.get((Object)RibbonElementPriority.MEDIUM)).add(object3);
                    }
                }
                for (AbstractCommandButton abstractCommandButton2 : (List)hashMap.get((Object)RibbonElementPriority.MEDIUM)) {
                    abstractCommandButton2.setDisplayState(CommandButtonDisplayState.MEDIUM);
                }
                for (AbstractCommandButton abstractCommandButton3 : (List)hashMap.get((Object)RibbonElementPriority.LOW)) {
                    abstractCommandButton3.setDisplayState(CommandButtonDisplayState.SMALL);
                }
            }
        }
    }

    static interface Mapping {
        public RibbonElementPriority map(RibbonElementPriority var1);
    }

}

