/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common;

import org.pushingpixels.flamingo.api.common.KeyValuePair;

public class StringValuePair<T>
extends KeyValuePair<String, T> {
    public StringValuePair(String string, T t) {
        super(string, t);
    }
}

