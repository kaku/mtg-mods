/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common.popup;

import java.awt.Rectangle;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.PanelUI;
import org.pushingpixels.flamingo.internal.ui.common.popup.BasicPopupPanelUI;
import org.pushingpixels.flamingo.internal.ui.common.popup.PopupPanelUI;

public abstract class JPopupPanel
extends JPanel {
    public static final String uiClassID = "PopupPanelUI";
    protected PopupPanelCustomizer customizer;

    protected JPopupPanel() {
    }

    @Override
    public PopupPanelUI getUI() {
        return (PopupPanelUI)this.ui;
    }

    protected void setUI(PopupPanelUI popupPanelUI) {
        super.setUI(popupPanelUI);
    }

    @Override
    public String getUIClassID() {
        return "PopupPanelUI";
    }

    @Override
    public void updateUI() {
        if (UIManager.get(this.getUIClassID()) != null) {
            this.setUI((PopupPanelUI)UIManager.getUI(this));
        } else {
            this.setUI(BasicPopupPanelUI.createUI(this));
        }
    }

    public void setCustomizer(PopupPanelCustomizer popupPanelCustomizer) {
        this.customizer = popupPanelCustomizer;
    }

    public PopupPanelCustomizer getCustomizer() {
        return this.customizer;
    }

    public static interface PopupPanelCustomizer {
        public Rectangle getScreenBounds();
    }

}

