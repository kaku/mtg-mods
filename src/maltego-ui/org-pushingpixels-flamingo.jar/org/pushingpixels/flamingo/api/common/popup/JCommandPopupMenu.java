/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common.popup;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EventListener;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.JCommandButtonPanel;
import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.JCommandToggleMenuButton;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.internal.ui.common.popup.BasicCommandPopupMenuUI;
import org.pushingpixels.flamingo.internal.ui.common.popup.PopupPanelUI;

public class JCommandPopupMenu
extends JPopupPanel {
    public static final String uiClassID = "CommandPopupMenuUI";
    protected JCommandButtonPanel mainButtonPanel;
    protected List<Component> menuComponents = new ArrayList<Component>();
    protected int maxButtonColumns;
    protected int maxVisibleButtonRows;
    protected int maxVisibleMenuButtons = -1;
    private boolean toDismissOnChildClick = true;

    public JCommandPopupMenu() {
    }

    public JCommandPopupMenu(JCommandButtonPanel jCommandButtonPanel, int n, int n2) {
        this();
        this.mainButtonPanel = jCommandButtonPanel;
        this.maxButtonColumns = n;
        this.maxVisibleButtonRows = n2;
        this.updateUI();
    }

    public void addMenuButton(JCommandMenuButton jCommandMenuButton) {
        jCommandMenuButton.setHorizontalAlignment(2);
        this.menuComponents.add(jCommandMenuButton);
        this.fireStateChanged();
    }

    public void addMenuButton(JCommandToggleMenuButton jCommandToggleMenuButton) {
        jCommandToggleMenuButton.setHorizontalAlignment(2);
        this.menuComponents.add(jCommandToggleMenuButton);
        this.fireStateChanged();
    }

    public void addMenuSeparator() {
        this.menuComponents.add(new JPopupMenu.Separator());
        this.fireStateChanged();
    }

    protected void addMenuPanel(JPanel jPanel) {
        if (this.maxVisibleMenuButtons > 0) {
            throw new IllegalStateException("This method is not supported on menu that contains a command button panel");
        }
        this.menuComponents.add(jPanel);
        this.fireStateChanged();
    }

    public boolean hasCommandButtonPanel() {
        return this.mainButtonPanel != null;
    }

    public JCommandButtonPanel getMainButtonPanel() {
        return this.mainButtonPanel;
    }

    public List<Component> getMenuComponents() {
        if (this.menuComponents == null) {
            return null;
        }
        return Collections.unmodifiableList(this.menuComponents);
    }

    public int getMaxButtonColumns() {
        return this.maxButtonColumns;
    }

    public int getMaxVisibleButtonRows() {
        return this.maxVisibleButtonRows;
    }

    public int getMaxVisibleMenuButtons() {
        return this.maxVisibleMenuButtons;
    }

    public void setMaxVisibleMenuButtons(int n) {
        for (Component component : this.menuComponents) {
            if (!(component instanceof JPanel)) continue;
            throw new IllegalStateException("This method is not supported on menus with panels");
        }
        int n2 = this.maxVisibleMenuButtons;
        this.maxVisibleMenuButtons = n;
        if (n2 != this.maxVisibleMenuButtons) {
            this.firePropertyChange("maxVisibleMenuButtons", n2, this.maxVisibleMenuButtons);
        }
    }

    @Override
    public String getUIClassID() {
        return "CommandPopupMenuUI";
    }

    @Override
    public void updateUI() {
        if (UIManager.get(this.getUIClassID()) != null) {
            this.setUI((PopupPanelUI)UIManager.getUI(this));
        } else {
            this.setUI(BasicCommandPopupMenuUI.createUI(this));
        }
    }

    public void addChangeListener(ChangeListener changeListener) {
        this.listenerList.add(ChangeListener.class, changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this.listenerList.remove(ChangeListener.class, changeListener);
    }

    protected void fireStateChanged() {
        Object[] arrobject = this.listenerList.getListenerList();
        ChangeEvent changeEvent = new ChangeEvent(this);
        for (int i = arrobject.length - 2; i >= 0; i -= 2) {
            if (arrobject[i] != ChangeListener.class) continue;
            ((ChangeListener)arrobject[i + 1]).stateChanged(changeEvent);
        }
    }

    public boolean isToDismissOnChildClick() {
        return this.toDismissOnChildClick;
    }

    public void setToDismissOnChildClick(boolean bl) {
        this.toDismissOnChildClick = bl;
    }
}

