/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common.model;

import javax.swing.ButtonModel;
import org.pushingpixels.flamingo.api.common.PopupActionListener;

public interface PopupButtonModel
extends ButtonModel {
    public void addPopupActionListener(PopupActionListener var1);

    public void removePopupActionListener(PopupActionListener var1);

    public void setPopupShowing(boolean var1);

    public boolean isPopupShowing();
}

