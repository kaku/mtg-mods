/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EventListener;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.Scrollable;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.PanelUI;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.CommandToggleButtonGroup;
import org.pushingpixels.flamingo.api.common.JCommandToggleButton;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;
import org.pushingpixels.flamingo.internal.ui.common.BasicCommandButtonPanelUI;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonPanelUI;

public class JCommandButtonPanel
extends JPanel
implements Scrollable {
    public static final String uiClassID = "CommandButtonPanelUI";
    protected List<String> groupTitles = new ArrayList<String>();
    protected List<List<AbstractCommandButton>> buttons = new ArrayList<List<AbstractCommandButton>>();
    protected int maxButtonColumns = -1;
    protected int maxButtonRows = -1;
    protected boolean isSingleSelectionMode = false;
    protected boolean toShowGroupLabels = true;
    protected CommandToggleButtonGroup buttonGroup;
    protected int currDimension;
    protected CommandButtonDisplayState currState;
    protected LayoutKind layoutKind;

    protected JCommandButtonPanel() {
        this.setLayoutKind(LayoutKind.ROW_FILL);
    }

    public JCommandButtonPanel(int n) {
        this();
        this.currDimension = n;
        this.currState = CommandButtonDisplayState.FIT_TO_ICON;
        this.updateUI();
    }

    public JCommandButtonPanel(CommandButtonDisplayState commandButtonDisplayState) {
        this();
        this.currDimension = -1;
        this.currState = commandButtonDisplayState;
        this.updateUI();
    }

    public void addButtonGroup(String string, int n) {
        this.groupTitles.add(n, string);
        ArrayList arrayList = new ArrayList();
        this.buttons.add(n, arrayList);
        this.fireStateChanged();
    }

    public void addButtonGroup(String string) {
        this.addButtonGroup(string, this.groupTitles.size());
    }

    public void removeButtonGroup(String string) {
        int n = this.groupTitles.indexOf(string);
        if (n < 0) {
            return;
        }
        this.groupTitles.remove(n);
        List<AbstractCommandButton> list = this.buttons.get(n);
        if (list != null) {
            for (AbstractCommandButton abstractCommandButton : list) {
                this.remove(abstractCommandButton);
                if (!this.isSingleSelectionMode || !(abstractCommandButton instanceof JCommandToggleButton)) continue;
                this.buttonGroup.remove((JCommandToggleButton)abstractCommandButton);
            }
        }
        this.buttons.remove(n);
        this.fireStateChanged();
    }

    public int addButtonToLastGroup(AbstractCommandButton abstractCommandButton) {
        if (this.groupTitles.size() == 0) {
            return -1;
        }
        int n = this.groupTitles.size() - 1;
        abstractCommandButton.setDisplayState(this.currState);
        return this.addButtonToGroup(this.groupTitles.get(n), this.buttons.get(n).size(), abstractCommandButton);
    }

    public int addButtonToGroup(String string, AbstractCommandButton abstractCommandButton) {
        int n = this.groupTitles.indexOf(string);
        if (n < 0) {
            return -1;
        }
        abstractCommandButton.setDisplayState(this.currState);
        return this.addButtonToGroup(string, this.buttons.get(n).size(), abstractCommandButton);
    }

    public int addButtonToGroup(String string, int n, AbstractCommandButton abstractCommandButton) {
        int n2 = this.groupTitles.indexOf(string);
        if (n2 < 0) {
            return -1;
        }
        this.add(abstractCommandButton);
        this.buttons.get(n2).add(n, abstractCommandButton);
        if (this.isSingleSelectionMode && abstractCommandButton instanceof JCommandToggleButton) {
            this.buttonGroup.add((JCommandToggleButton)abstractCommandButton);
        }
        this.fireStateChanged();
        return n;
    }

    public void removeButtonFromGroup(String string, int n) {
        int n2 = this.groupTitles.indexOf(string);
        if (n2 < 0) {
            return;
        }
        AbstractCommandButton abstractCommandButton = this.buttons.get(n2).remove(n);
        this.remove(abstractCommandButton);
        if (this.isSingleSelectionMode && abstractCommandButton instanceof JCommandToggleButton) {
            this.buttonGroup.remove((JCommandToggleButton)abstractCommandButton);
        }
        this.fireStateChanged();
    }

    public void removeAllGroups() {
        for (List<AbstractCommandButton> list : this.buttons) {
            for (AbstractCommandButton abstractCommandButton : list) {
                if (this.isSingleSelectionMode && abstractCommandButton instanceof JCommandToggleButton) {
                    this.buttonGroup.remove((JCommandToggleButton)abstractCommandButton);
                }
                this.remove(abstractCommandButton);
            }
        }
        this.buttons.clear();
        this.groupTitles.clear();
        this.fireStateChanged();
    }

    public int getGroupCount() {
        if (this.groupTitles == null) {
            return 0;
        }
        return this.groupTitles.size();
    }

    public int getButtonCount() {
        int n = 0;
        for (List<AbstractCommandButton> list : this.buttons) {
            n += list.size();
        }
        return n;
    }

    public String getGroupTitleAt(int n) {
        return this.groupTitles.get(n);
    }

    @Override
    public void updateUI() {
        if (UIManager.get(this.getUIClassID()) != null) {
            this.setUI((CommandButtonPanelUI)UIManager.getUI(this));
        } else {
            this.setUI(BasicCommandButtonPanelUI.createUI(this));
        }
    }

    @Override
    public String getUIClassID() {
        return "CommandButtonPanelUI";
    }

    public void setMaxButtonColumns(int n) {
        if (n != this.maxButtonColumns) {
            int n2 = this.maxButtonColumns;
            this.maxButtonColumns = n;
            this.firePropertyChange("maxButtonColumns", n2, this.maxButtonColumns);
        }
    }

    public int getMaxButtonColumns() {
        return this.maxButtonColumns;
    }

    public void setMaxButtonRows(int n) {
        if (n != this.maxButtonRows) {
            int n2 = this.maxButtonRows;
            this.maxButtonRows = n;
            this.firePropertyChange("maxButtonRows", n2, this.maxButtonRows);
        }
    }

    public int getMaxButtonRows() {
        return this.maxButtonRows;
    }

    public List<AbstractCommandButton> getGroupButtons(int n) {
        return Collections.unmodifiableList(this.buttons.get(n));
    }

    public void setSingleSelectionMode(boolean bl) {
        if (this.isSingleSelectionMode == bl) {
            return;
        }
        this.isSingleSelectionMode = bl;
        if (this.isSingleSelectionMode) {
            this.buttonGroup = new CommandToggleButtonGroup();
            for (List<AbstractCommandButton> list : this.buttons) {
                for (AbstractCommandButton abstractCommandButton : list) {
                    if (!(abstractCommandButton instanceof JCommandToggleButton)) continue;
                    this.buttonGroup.add((JCommandToggleButton)abstractCommandButton);
                }
            }
        } else {
            for (List<AbstractCommandButton> list : this.buttons) {
                for (AbstractCommandButton abstractCommandButton : list) {
                    if (!(abstractCommandButton instanceof JCommandToggleButton)) continue;
                    this.buttonGroup.remove((JCommandToggleButton)abstractCommandButton);
                }
            }
            this.buttonGroup = null;
        }
    }

    public void setToShowGroupLabels(boolean bl) {
        if (this.layoutKind == LayoutKind.COLUMN_FILL && bl) {
            throw new IllegalArgumentException("Column fill layout is not supported when group labels are shown");
        }
        if (this.toShowGroupLabels != bl) {
            boolean bl2 = this.toShowGroupLabels;
            this.toShowGroupLabels = bl;
            this.firePropertyChange("toShowGroupLabels", bl2, this.toShowGroupLabels);
        }
    }

    public boolean isToShowGroupLabels() {
        return this.toShowGroupLabels;
    }

    public void setIconDimension(int n) {
        this.currDimension = n;
        this.currState = CommandButtonDisplayState.FIT_TO_ICON;
        for (List<AbstractCommandButton> list : this.buttons) {
            for (AbstractCommandButton abstractCommandButton : list) {
                abstractCommandButton.updateCustomDimension(n);
            }
        }
        this.revalidate();
        this.doLayout();
        this.repaint();
    }

    public void setIconState(CommandButtonDisplayState commandButtonDisplayState) {
        this.currDimension = -1;
        this.currState = commandButtonDisplayState;
        for (List<AbstractCommandButton> list : this.buttons) {
            for (AbstractCommandButton abstractCommandButton : list) {
                abstractCommandButton.setDisplayState(commandButtonDisplayState);
                abstractCommandButton.revalidate();
                abstractCommandButton.doLayout();
            }
        }
        this.revalidate();
        this.doLayout();
        this.repaint();
    }

    public JCommandToggleButton getSelectedButton() {
        if (this.isSingleSelectionMode) {
            for (List<AbstractCommandButton> list : this.buttons) {
                for (AbstractCommandButton abstractCommandButton : list) {
                    JCommandToggleButton jCommandToggleButton;
                    if (!(abstractCommandButton instanceof JCommandToggleButton) || !(jCommandToggleButton = (JCommandToggleButton)abstractCommandButton).getActionModel().isSelected()) continue;
                    return jCommandToggleButton;
                }
            }
        }
        return null;
    }

    public LayoutKind getLayoutKind() {
        return this.layoutKind;
    }

    public void setLayoutKind(LayoutKind layoutKind) {
        if (layoutKind == null) {
            throw new IllegalArgumentException("Layout kind cannot be null");
        }
        if (layoutKind == LayoutKind.COLUMN_FILL && this.isToShowGroupLabels()) {
            throw new IllegalArgumentException("Column fill layout is not supported when group labels are shown");
        }
        if (layoutKind != this.layoutKind) {
            LayoutKind layoutKind2 = this.layoutKind;
            this.layoutKind = layoutKind;
            this.firePropertyChange("layoutKind", (Object)layoutKind2, (Object)this.layoutKind);
        }
    }

    public void addChangeListener(ChangeListener changeListener) {
        this.listenerList.add(ChangeListener.class, changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this.listenerList.remove(ChangeListener.class, changeListener);
    }

    protected void fireStateChanged() {
        Object[] arrobject = this.listenerList.getListenerList();
        ChangeEvent changeEvent = new ChangeEvent(this);
        for (int i = arrobject.length - 2; i >= 0; i -= 2) {
            if (arrobject[i] != ChangeListener.class) continue;
            ((ChangeListener)arrobject[i + 1]).stateChanged(changeEvent);
        }
    }

    @Override
    public Dimension getPreferredScrollableViewportSize() {
        return this.getPreferredSize();
    }

    @Override
    public int getScrollableBlockIncrement(Rectangle rectangle, int n, int n2) {
        return 30;
    }

    @Override
    public boolean getScrollableTracksViewportHeight() {
        return this.layoutKind == LayoutKind.COLUMN_FILL;
    }

    @Override
    public boolean getScrollableTracksViewportWidth() {
        return this.layoutKind == LayoutKind.ROW_FILL;
    }

    @Override
    public int getScrollableUnitIncrement(Rectangle rectangle, int n, int n2) {
        return 10;
    }

    public static enum LayoutKind {
        ROW_FILL,
        COLUMN_FILL;
        

        private LayoutKind() {
        }
    }

}

