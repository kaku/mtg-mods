/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common;

import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonLayoutManagerBig;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonLayoutManagerCustom;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonLayoutManagerMedium;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonLayoutManagerSmall;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonLayoutManagerTile;

public abstract class CommandButtonDisplayState {
    public static final CommandButtonDisplayState FIT_TO_ICON = new CommandButtonDisplayState("Fit to icon", -1){

        @Override
        public CommandButtonLayoutManager createLayoutManager(AbstractCommandButton abstractCommandButton) {
            return new CommandButtonLayoutManagerCustom(abstractCommandButton);
        }
    };
    public static final CommandButtonDisplayState BIG = new CommandButtonDisplayState("Big", 32){

        @Override
        public CommandButtonLayoutManager createLayoutManager(AbstractCommandButton abstractCommandButton) {
            return new CommandButtonLayoutManagerBig(abstractCommandButton);
        }
    };
    public static final CommandButtonDisplayState TILE = new CommandButtonDisplayState("Tile", 32){

        @Override
        public CommandButtonLayoutManager createLayoutManager(AbstractCommandButton abstractCommandButton) {
            return new CommandButtonLayoutManagerTile();
        }
    };
    public static final CommandButtonDisplayState MEDIUM = new CommandButtonDisplayState("Medium", 16){

        @Override
        public CommandButtonLayoutManager createLayoutManager(AbstractCommandButton abstractCommandButton) {
            return new CommandButtonLayoutManagerMedium();
        }
    };
    public static final CommandButtonDisplayState SMALL = new CommandButtonDisplayState("Small", 16){

        @Override
        public CommandButtonLayoutManager createLayoutManager(AbstractCommandButton abstractCommandButton) {
            return new CommandButtonLayoutManagerSmall();
        }
    };
    int preferredIconSize;
    String displayName;

    protected CommandButtonDisplayState(String string, int n) {
        this.displayName = string;
        this.preferredIconSize = n;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public int getPreferredIconSize() {
        return this.preferredIconSize;
    }

    public abstract CommandButtonLayoutManager createLayoutManager(AbstractCommandButton var1);

    public String toString() {
        return this.getDisplayName();
    }

}

