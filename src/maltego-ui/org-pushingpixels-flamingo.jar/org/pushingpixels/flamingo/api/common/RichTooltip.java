/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common;

import java.awt.Image;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class RichTooltip {
    protected String title;
    protected Image mainImage;
    protected List<String> descriptionSections;
    protected Image footerImage;
    protected List<String> footerSections;

    public RichTooltip() {
    }

    public RichTooltip(String string, String string2) {
        this.setTitle(string);
        this.addDescriptionSection(string2);
    }

    public void setTitle(String string) {
        this.title = string;
    }

    public void setMainImage(Image image) {
        this.mainImage = image;
    }

    public void addDescriptionSection(String string) {
        if (this.descriptionSections == null) {
            this.descriptionSections = new LinkedList<String>();
        }
        this.descriptionSections.add(string);
    }

    public void setFooterImage(Image image) {
        this.footerImage = image;
    }

    public void addFooterSection(String string) {
        if (this.footerSections == null) {
            this.footerSections = new LinkedList<String>();
        }
        this.footerSections.add(string);
    }

    public String getTitle() {
        return this.title;
    }

    public Image getMainImage() {
        return this.mainImage;
    }

    public List<String> getDescriptionSections() {
        if (this.descriptionSections == null) {
            return Collections.EMPTY_LIST;
        }
        return Collections.unmodifiableList(this.descriptionSections);
    }

    public Image getFooterImage() {
        return this.footerImage;
    }

    public List<String> getFooterSections() {
        if (this.footerSections == null) {
            return Collections.EMPTY_LIST;
        }
        return Collections.unmodifiableList(this.footerSections);
    }
}

