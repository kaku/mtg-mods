/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common;

import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.JCommandToggleButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.internal.ui.common.BasicCommandToggleMenuButtonUI;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonUI;

public class JCommandToggleMenuButton
extends JCommandToggleButton {
    public static final String uiClassID = "CommandToggleMenuButtonUI";

    public JCommandToggleMenuButton(String string, ResizableIcon resizableIcon) {
        super(string, resizableIcon);
    }

    @Override
    public String getUIClassID() {
        return "CommandToggleMenuButtonUI";
    }

    @Override
    public void updateUI() {
        if (UIManager.get(this.getUIClassID()) != null) {
            this.setUI((CommandButtonUI)UIManager.getUI(this));
        } else {
            this.setUI(BasicCommandToggleMenuButtonUI.createUI(this));
        }
    }
}

