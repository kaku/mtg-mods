/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common.icon;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

public class LayeredIcon
implements ResizableIcon {
    protected ResizableIcon[] layers;

    public /* varargs */ LayeredIcon(ResizableIcon ... arrresizableIcon) {
        this.layers = arrresizableIcon;
    }

    @Override
    public void setDimension(Dimension dimension) {
        for (ResizableIcon resizableIcon : this.layers) {
            resizableIcon.setDimension(dimension);
        }
    }

    @Override
    public int getIconHeight() {
        return this.layers[0].getIconHeight();
    }

    @Override
    public int getIconWidth() {
        return this.layers[0].getIconWidth();
    }

    @Override
    public void paintIcon(Component component, Graphics graphics, int n, int n2) {
        for (ResizableIcon resizableIcon : this.layers) {
            resizableIcon.paintIcon(component, graphics, n, n2);
        }
    }
}

