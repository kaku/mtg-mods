/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common;

import java.awt.Component;
import java.util.EventListener;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.internal.ui.common.BasicCommandButtonStripUI;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonStripUI;

public class JCommandButtonStrip
extends JComponent {
    public static final String uiClassID = "CommandButtonStripUI";
    protected CommandButtonDisplayState displayState;
    protected double hgapScaleFactor;
    protected double vgapScaleFactor;
    private StripOrientation orientation;

    public JCommandButtonStrip() {
        this(StripOrientation.HORIZONTAL);
    }

    public JCommandButtonStrip(StripOrientation stripOrientation) {
        this.orientation = stripOrientation;
        this.displayState = CommandButtonDisplayState.SMALL;
        switch (stripOrientation) {
            case HORIZONTAL: {
                this.hgapScaleFactor = 0.75;
                this.vgapScaleFactor = 1.0;
                break;
            }
            case VERTICAL: {
                this.hgapScaleFactor = 1.0;
                this.vgapScaleFactor = 0.75;
            }
        }
        this.setOpaque(false);
        this.updateUI();
    }

    public void setDisplayState(CommandButtonDisplayState commandButtonDisplayState) {
        if (this.getComponentCount() > 0) {
            throw new IllegalStateException("Can't call this method after buttons have been already added");
        }
        this.displayState = commandButtonDisplayState;
    }

    public void setHGapScaleFactor(double d) {
        if (this.getComponentCount() > 0) {
            throw new IllegalStateException("Can't call this method after buttons have been already added");
        }
        this.hgapScaleFactor = d;
    }

    public void setVGapScaleFactor(double d) {
        if (this.getComponentCount() > 0) {
            throw new IllegalStateException("Can't call this method after buttons have been already added");
        }
        this.vgapScaleFactor = d;
    }

    @Override
    public void add(Component component, Object object, int n) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(Component component, Object object) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Component add(Component component, int n) {
        if (!(component instanceof AbstractCommandButton)) {
            throw new UnsupportedOperationException();
        }
        this.configureCommandButton((AbstractCommandButton)component);
        return super.add(component, n);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Component add(Component component) {
        if (!(component instanceof AbstractCommandButton)) {
            throw new UnsupportedOperationException();
        }
        try {
            Component component2;
            this.configureCommandButton((AbstractCommandButton)component);
            Component component3 = component2 = super.add(component);
            return component3;
        }
        finally {
            this.fireStateChanged();
        }
    }

    private void configureCommandButton(AbstractCommandButton abstractCommandButton) {
        abstractCommandButton.setDisplayState(this.displayState);
        abstractCommandButton.setHGapScaleFactor(this.hgapScaleFactor);
        abstractCommandButton.setVGapScaleFactor(this.vgapScaleFactor);
        abstractCommandButton.setFlat(false);
    }

    @Override
    public Component add(String string, Component component) {
        throw new UnsupportedOperationException();
    }

    public void setUI(CommandButtonStripUI commandButtonStripUI) {
        super.setUI(commandButtonStripUI);
    }

    @Override
    public void updateUI() {
        if (UIManager.get(this.getUIClassID()) != null) {
            this.setUI((CommandButtonStripUI)UIManager.getUI(this));
        } else {
            this.setUI(BasicCommandButtonStripUI.createUI(this));
        }
    }

    public CommandButtonStripUI getUI() {
        return (CommandButtonStripUI)this.ui;
    }

    @Override
    public String getUIClassID() {
        return "CommandButtonStripUI";
    }

    public int getButtonCount() {
        return this.getComponentCount();
    }

    public AbstractCommandButton getButton(int n) {
        return (AbstractCommandButton)this.getComponent(n);
    }

    public boolean isFirst(AbstractCommandButton abstractCommandButton) {
        return abstractCommandButton == this.getButton(0);
    }

    public boolean isLast(AbstractCommandButton abstractCommandButton) {
        return abstractCommandButton == this.getButton(this.getButtonCount() - 1);
    }

    public StripOrientation getOrientation() {
        return this.orientation;
    }

    public void addChangeListener(ChangeListener changeListener) {
        this.listenerList.add(ChangeListener.class, changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this.listenerList.remove(ChangeListener.class, changeListener);
    }

    protected void fireStateChanged() {
        Object[] arrobject = this.listenerList.getListenerList();
        ChangeEvent changeEvent = new ChangeEvent(this);
        for (int i = arrobject.length - 2; i >= 0; i -= 2) {
            if (arrobject[i] != ChangeListener.class) continue;
            ((ChangeListener)arrobject[i + 1]).stateChanged(changeEvent);
        }
    }

    public static enum StripOrientation {
        HORIZONTAL,
        VERTICAL;
        

        private StripOrientation() {
        }
    }

}

