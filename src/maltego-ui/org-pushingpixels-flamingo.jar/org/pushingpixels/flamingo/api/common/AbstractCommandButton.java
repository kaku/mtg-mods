/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.EventListener;
import javax.accessibility.AccessibleContext;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.RichToolTipManager;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonUI;

public abstract class AbstractCommandButton
extends RichToolTipManager.JTrackableComponent {
    protected ResizableIcon icon;
    protected ResizableIcon disabledIcon;
    private String text;
    protected ActionButtonModel actionModel;
    protected String extraText;
    protected CommandButtonDisplayState displayState;
    protected int customDimension;
    protected boolean isFlat;
    private int horizontalAlignment;
    private double hgapScaleFactor;
    private double vgapScaleFactor;
    private RichTooltip actionRichTooltip;
    private CommandButtonLocationOrderKind locationOrderKind;
    protected ActionHandler actionHandler;
    protected String actionKeyTip;

    public AbstractCommandButton(String string, ResizableIcon resizableIcon) {
        this.icon = resizableIcon;
        this.customDimension = -1;
        this.displayState = CommandButtonDisplayState.FIT_TO_ICON;
        this.horizontalAlignment = 0;
        this.actionHandler = new ActionHandler();
        this.isFlat = true;
        this.hgapScaleFactor = 1.0;
        this.vgapScaleFactor = 1.0;
        this.setText(string);
        this.setOpaque(false);
    }

    public void setUI(CommandButtonUI commandButtonUI) {
        super.setUI(commandButtonUI);
    }

    public CommandButtonUI getUI() {
        return (CommandButtonUI)this.ui;
    }

    public void setDisplayState(CommandButtonDisplayState commandButtonDisplayState) {
        CommandButtonDisplayState commandButtonDisplayState2 = this.displayState;
        this.displayState = commandButtonDisplayState;
        this.firePropertyChange("displayState", commandButtonDisplayState2, this.displayState);
    }

    public ResizableIcon getIcon() {
        return this.icon;
    }

    public void setIcon(ResizableIcon resizableIcon) {
        ResizableIcon resizableIcon2 = this.icon;
        this.icon = resizableIcon;
        this.firePropertyChange("icon", resizableIcon2, resizableIcon);
        if (resizableIcon != resizableIcon2) {
            if (resizableIcon == null || resizableIcon2 == null || resizableIcon.getIconWidth() != resizableIcon2.getIconWidth() || resizableIcon.getIconHeight() != resizableIcon2.getIconHeight()) {
                this.revalidate();
            }
            this.repaint();
        }
    }

    public void setDisabledIcon(ResizableIcon resizableIcon) {
        this.disabledIcon = resizableIcon;
    }

    public ResizableIcon getDisabledIcon() {
        return this.disabledIcon;
    }

    public CommandButtonDisplayState getDisplayState() {
        return this.displayState;
    }

    public String getExtraText() {
        return this.extraText;
    }

    public void setExtraText(String string) {
        String string2 = this.extraText;
        this.extraText = string;
        this.firePropertyChange("extraText", string2, string);
        if (this.accessibleContext != null) {
            this.accessibleContext.firePropertyChange("AccessibleVisibleData", string2, string);
        }
        if (string == null || string2 == null || !string.equals(string2)) {
            this.revalidate();
            this.repaint();
        }
    }

    public String getText() {
        return this.text;
    }

    public void setText(String string) {
        String string2 = this.text;
        this.text = string;
        this.firePropertyChange("text", string2, string);
        if (this.accessibleContext != null) {
            this.accessibleContext.firePropertyChange("AccessibleVisibleData", string2, string);
        }
        if (string == null || string2 == null || !string.equals(string2)) {
            this.revalidate();
            this.repaint();
        }
    }

    public void updateCustomDimension(int n) {
        if (this.customDimension != n) {
            int n2 = this.customDimension;
            this.customDimension = n;
            this.firePropertyChange("customDimension", n2, this.customDimension);
        }
    }

    public int getCustomDimension() {
        return this.customDimension;
    }

    public boolean isFlat() {
        return this.isFlat;
    }

    public void setFlat(boolean bl) {
        boolean bl2 = this.isFlat;
        this.isFlat = bl;
        if (bl2 != this.isFlat) {
            this.firePropertyChange("flat", bl2, this.isFlat);
        }
        if (bl2 != bl) {
            this.repaint();
        }
    }

    public ActionButtonModel getActionModel() {
        return this.actionModel;
    }

    public void setActionModel(ActionButtonModel actionButtonModel) {
        ActionButtonModel actionButtonModel2 = this.getActionModel();
        if (actionButtonModel2 != null) {
            actionButtonModel2.removeChangeListener(this.actionHandler);
            actionButtonModel2.removeActionListener(this.actionHandler);
        }
        this.actionModel = actionButtonModel;
        if (actionButtonModel != null) {
            actionButtonModel.addChangeListener(this.actionHandler);
            actionButtonModel.addActionListener(this.actionHandler);
        }
        this.firePropertyChange("actionModel", actionButtonModel2, actionButtonModel);
        if (actionButtonModel != actionButtonModel2) {
            this.revalidate();
            this.repaint();
        }
    }

    public void addActionListener(ActionListener actionListener) {
        this.listenerList.add(ActionListener.class, actionListener);
    }

    public void removeActionListener(ActionListener actionListener) {
        this.listenerList.remove(ActionListener.class, actionListener);
    }

    public void addChangeListener(ChangeListener changeListener) {
        this.listenerList.add(ChangeListener.class, changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this.listenerList.remove(ChangeListener.class, changeListener);
    }

    @Override
    public void setEnabled(boolean bl) {
        if (!bl && this.actionModel.isRollover()) {
            this.actionModel.setRollover(false);
        }
        super.setEnabled(bl);
        this.actionModel.setEnabled(bl);
    }

    protected void fireStateChanged() {
        Object[] arrobject = this.listenerList.getListenerList();
        ChangeEvent changeEvent = new ChangeEvent(this);
        for (int i = arrobject.length - 2; i >= 0; i -= 2) {
            if (arrobject[i] != ChangeListener.class) continue;
            ((ChangeListener)arrobject[i + 1]).stateChanged(changeEvent);
        }
    }

    protected void fireActionPerformed(ActionEvent actionEvent) {
        Object[] arrobject = this.listenerList.getListenerList();
        ActionEvent actionEvent2 = null;
        for (int i = arrobject.length - 2; i >= 0; i -= 2) {
            if (arrobject[i] != ActionListener.class) continue;
            if (actionEvent2 == null) {
                String string = actionEvent.getActionCommand();
                actionEvent2 = new ActionEvent(this, 1001, string, actionEvent.getWhen(), actionEvent.getModifiers());
            }
            ((ActionListener)arrobject[i + 1]).actionPerformed(actionEvent2);
        }
    }

    public void setHorizontalAlignment(int n) {
        if (n == this.horizontalAlignment) {
            return;
        }
        int n2 = this.horizontalAlignment;
        this.horizontalAlignment = n;
        this.firePropertyChange("horizontalAlignment", n2, this.horizontalAlignment);
        this.repaint();
    }

    public int getHorizontalAlignment() {
        return this.horizontalAlignment;
    }

    public void setHGapScaleFactor(double d) {
        if (d == this.hgapScaleFactor) {
            return;
        }
        double d2 = this.hgapScaleFactor;
        this.hgapScaleFactor = d;
        this.firePropertyChange("hgapScaleFactor", d2, this.hgapScaleFactor);
        if (this.hgapScaleFactor != d2) {
            this.revalidate();
            this.repaint();
        }
    }

    public void setVGapScaleFactor(double d) {
        if (d == this.vgapScaleFactor) {
            return;
        }
        double d2 = this.vgapScaleFactor;
        this.vgapScaleFactor = d;
        this.firePropertyChange("vgapScaleFactor", d2, this.vgapScaleFactor);
        if (this.vgapScaleFactor != d2) {
            this.revalidate();
            this.repaint();
        }
    }

    public void setGapScaleFactor(double d) {
        this.setHGapScaleFactor(d);
        this.setVGapScaleFactor(d);
    }

    public double getHGapScaleFactor() {
        return this.hgapScaleFactor;
    }

    public double getVGapScaleFactor() {
        return this.vgapScaleFactor;
    }

    public void doActionClick() {
        Dimension dimension = this.getSize();
        ActionButtonModel actionButtonModel = this.getActionModel();
        actionButtonModel.setArmed(true);
        actionButtonModel.setPressed(true);
        this.paintImmediately(new Rectangle(0, 0, dimension.width, dimension.height));
        try {
            Thread.sleep(100);
        }
        catch (InterruptedException var3_3) {
            // empty catch block
        }
        actionButtonModel.setPressed(false);
        actionButtonModel.setArmed(false);
    }

    boolean hasRichTooltips() {
        return this.actionRichTooltip != null;
    }

    public void setActionRichTooltip(RichTooltip richTooltip) {
        this.actionRichTooltip = richTooltip;
        RichToolTipManager richToolTipManager = RichToolTipManager.sharedInstance();
        if (this.hasRichTooltips()) {
            richToolTipManager.registerComponent(this);
        } else {
            richToolTipManager.unregisterComponent(this);
        }
    }

    @Override
    public RichTooltip getRichTooltip(MouseEvent mouseEvent) {
        return this.actionRichTooltip;
    }

    @Override
    public void setToolTipText(String string) {
        super.setToolTipText(string);
    }

    public CommandButtonLocationOrderKind getLocationOrderKind() {
        return this.locationOrderKind;
    }

    public void setLocationOrderKind(CommandButtonLocationOrderKind commandButtonLocationOrderKind) {
        CommandButtonLocationOrderKind commandButtonLocationOrderKind2 = this.locationOrderKind;
        if (commandButtonLocationOrderKind2 != commandButtonLocationOrderKind) {
            this.locationOrderKind = commandButtonLocationOrderKind;
            this.firePropertyChange("locationOrderKind", (Object)commandButtonLocationOrderKind2, (Object)this.locationOrderKind);
        }
    }

    public String getActionKeyTip() {
        return this.actionKeyTip;
    }

    public void setActionKeyTip(String string) {
        String string2 = this.actionKeyTip;
        this.actionKeyTip = string;
        this.firePropertyChange("actionKeyTip", string2, this.actionKeyTip);
    }

    class ActionHandler
    implements ActionListener,
    ChangeListener {
        ActionHandler() {
        }

        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            AbstractCommandButton.this.fireStateChanged();
            AbstractCommandButton.this.repaint();
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            AbstractCommandButton.this.fireActionPerformed(actionEvent);
        }
    }

    public static enum CommandButtonLocationOrderKind {
        ONLY,
        FIRST,
        MIDDLE,
        LAST;
        

        private CommandButtonLocationOrderKind() {
        }
    }

}

