/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common.icon;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.InputStream;
import java.util.EventListener;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.SwingWorker;
import javax.swing.event.EventListenerList;
import org.pushingpixels.flamingo.api.common.AsynchronousLoadListener;
import org.pushingpixels.flamingo.api.common.AsynchronousLoading;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

abstract class ImageWrapperIcon
implements Icon,
AsynchronousLoading {
    protected BufferedImage originalImage;
    protected InputStream imageInputStream;
    protected Image image;
    protected Map<String, BufferedImage> cachedImages;
    protected int width;
    protected int height;
    protected EventListenerList listenerList = new EventListenerList();

    public ImageWrapperIcon(InputStream inputStream, int n, int n2) {
        this.imageInputStream = inputStream;
        this.width = n;
        this.height = n2;
        this.listenerList = new EventListenerList();
        this.cachedImages = new LinkedHashMap<String, BufferedImage>(){

            @Override
            protected boolean removeEldestEntry(Map.Entry<String, BufferedImage> entry) {
                return this.size() > 5;
            }
        };
        this.renderImage(this.width, this.height);
    }

    public ImageWrapperIcon(Image image, int n, int n2) {
        this.imageInputStream = null;
        this.image = image;
        this.width = n;
        this.height = n2;
        this.listenerList = new EventListenerList();
        this.cachedImages = new LinkedHashMap<String, BufferedImage>(){

            @Override
            protected boolean removeEldestEntry(Map.Entry<String, BufferedImage> entry) {
                return this.size() > 5;
            }
        };
        this.renderImage(this.width, this.height);
    }

    @Override
    public void addAsynchronousLoadListener(AsynchronousLoadListener asynchronousLoadListener) {
        this.listenerList.add(AsynchronousLoadListener.class, asynchronousLoadListener);
    }

    @Override
    public void removeAsynchronousLoadListener(AsynchronousLoadListener asynchronousLoadListener) {
        this.listenerList.remove(AsynchronousLoadListener.class, asynchronousLoadListener);
    }

    @Override
    public int getIconWidth() {
        return this.width;
    }

    @Override
    public int getIconHeight() {
        return this.height;
    }

    @Override
    public void paintIcon(Component component, Graphics graphics, int n, int n2) {
        BufferedImage bufferedImage = this.cachedImages.get("" + this.getIconWidth() + ":" + this.getIconHeight());
        if (bufferedImage != null) {
            int n3 = (this.width - bufferedImage.getWidth()) / 2;
            int n4 = (this.height - bufferedImage.getHeight()) / 2;
            graphics.drawImage(bufferedImage, n + n3, n2 + n4, null);
        }
    }

    public synchronized void setPreferredSize(Dimension dimension) {
        if (dimension.width == this.width && dimension.height == this.height) {
            return;
        }
        this.width = dimension.width;
        this.height = dimension.height;
        this.renderImage(this.width, this.height);
    }

    protected synchronized void renderImage(final int n, final int n2) {
        String string = "" + n + ":" + n2;
        if (this.cachedImages.containsKey(string)) {
            this.fireAsyncCompleted(true);
            return;
        }
        SwingWorker<BufferedImage, Void> swingWorker = new SwingWorker<BufferedImage, Void>(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            protected BufferedImage doInBackground() throws Exception {
                Object object;
                if (ImageWrapperIcon.this.imageInputStream != null) {
                    object = ImageWrapperIcon.this.imageInputStream;
                    synchronized (object) {
                        if (ImageWrapperIcon.this.originalImage == null) {
                            ImageWrapperIcon.this.originalImage = ImageIO.read(ImageWrapperIcon.this.imageInputStream);
                        }
                    }
                } else {
                    object = GraphicsEnvironment.getLocalGraphicsEnvironment();
                    GraphicsDevice graphicsDevice = object.getDefaultScreenDevice();
                    GraphicsConfiguration graphicsConfiguration = graphicsDevice.getDefaultConfiguration();
                    ImageWrapperIcon.this.originalImage = graphicsConfiguration.createCompatibleImage(ImageWrapperIcon.this.image.getWidth(null), ImageWrapperIcon.this.image.getHeight(null), 3);
                    Graphics graphics = ImageWrapperIcon.this.originalImage.getGraphics();
                    graphics.drawImage(ImageWrapperIcon.this.image, 0, 0, null);
                    graphics.dispose();
                }
                object = ImageWrapperIcon.this.originalImage;
                float f = (float)ImageWrapperIcon.this.originalImage.getWidth() / (float)n;
                float f2 = (float)ImageWrapperIcon.this.originalImage.getHeight() / (float)ImageWrapperIcon.this.height;
                float f3 = Math.max(f, f2);
                if (f3 > 1.0f) {
                    int n3 = (int)((float)ImageWrapperIcon.this.originalImage.getWidth() / f3);
                    object = FlamingoUtilities.createThumbnail(ImageWrapperIcon.this.originalImage, n3);
                }
                return object;
            }

            @Override
            protected void done() {
                try {
                    BufferedImage bufferedImage = (BufferedImage)this.get();
                    ImageWrapperIcon.this.cachedImages.put("" + n + ":" + n2, bufferedImage);
                    ImageWrapperIcon.this.fireAsyncCompleted(true);
                }
                catch (Exception var1_2) {
                    ImageWrapperIcon.this.fireAsyncCompleted(false);
                }
            }
        };
        swingWorker.execute();
    }

    protected void fireAsyncCompleted(Boolean bl) {
        Object[] arrobject = this.listenerList.getListenerList();
        for (int i = arrobject.length - 2; i >= 0; i -= 2) {
            if (arrobject[i] != AsynchronousLoadListener.class) continue;
            ((AsynchronousLoadListener)arrobject[i + 1]).completed(bl);
        }
    }

    @Override
    public synchronized boolean isLoading() {
        BufferedImage bufferedImage = this.cachedImages.get("" + this.getIconWidth() + ":" + this.getIconHeight());
        return bufferedImage == null;
    }

}

