/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common.icon;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import javax.imageio.ImageIO;
import org.pushingpixels.flamingo.api.common.icon.BadIcoResException;

class Ico {
    private static final int FDE_OFFSET = 6;
    private static final int DE_LENGTH = 16;
    private static final int BMIH_LENGTH = 40;
    private byte[] icoimage = new byte[0];
    private int numImages;
    private BufferedImage[] bi;
    private int[] colorCount;

    public Ico(File file) throws BadIcoResException, IOException {
        this(file.getAbsolutePath());
    }

    public Ico(InputStream inputStream) throws BadIcoResException, IOException {
        try {
            this.read(inputStream);
            this.parseICOImage();
        }
        finally {
            try {
                inputStream.close();
            }
            catch (IOException var2_2) {}
        }
    }

    public Ico(String string) throws BadIcoResException, IOException {
        this(new FileInputStream(string));
    }

    public Ico(URL uRL) throws BadIcoResException, IOException {
        this(uRL.openStream());
    }

    public BufferedImage getImage(int n) {
        if (n < 0 || n >= this.numImages) {
            throw new IllegalArgumentException("index out of range");
        }
        return this.bi[n];
    }

    public int getNumColors(int n) {
        if (n < 0 || n >= this.numImages) {
            throw new IllegalArgumentException("index out of range");
        }
        return this.colorCount[n];
    }

    public int getNumImages() {
        return this.numImages;
    }

    private int calcScanlineBytes(int n, int n2) {
        return (n * n2 + 31) / 32 * 4;
    }

    private void parseICOImage() throws BadIcoResException, IOException {
        if (this.icoimage[2] != 1 || this.icoimage[3] != 0) {
            throw new BadIcoResException("Not an ICO resource");
        }
        this.numImages = this.ubyte(this.icoimage[5]);
        this.numImages <<= 8;
        this.numImages |= this.icoimage[4];
        this.bi = new BufferedImage[this.numImages];
        this.colorCount = new int[this.numImages];
        for (int i = 0; i < this.numImages; ++i) {
            int n = this.ubyte(this.icoimage[6 + i * 16]);
            int n2 = this.ubyte(this.icoimage[6 + i * 16 + 1]);
            this.colorCount[i] = this.ubyte(this.icoimage[6 + i * 16 + 2]);
            int n3 = this.ubyte(this.icoimage[6 + i * 16 + 11]);
            n3 <<= 8;
            n3 |= this.ubyte(this.icoimage[6 + i * 16 + 10]);
            n3 <<= 8;
            n3 |= this.ubyte(this.icoimage[6 + i * 16 + 9]);
            n3 <<= 8;
            n3 |= this.ubyte(this.icoimage[6 + i * 16 + 8]);
            int n4 = this.ubyte(this.icoimage[6 + i * 16 + 15]);
            n4 <<= 8;
            n4 |= this.ubyte(this.icoimage[6 + i * 16 + 14]);
            n4 <<= 8;
            n4 |= this.ubyte(this.icoimage[6 + i * 16 + 13]);
            n4 <<= 8;
            if (this.icoimage[n4 |= this.ubyte(this.icoimage[6 + i * 16 + 12])] == 40 && this.icoimage[n4 + 1] == 0 && this.icoimage[n4 + 2] == 0 && this.icoimage[n4 + 3] == 0) {
                int[] arrn;
                int n5;
                int n6;
                int n7;
                int n8;
                int n9;
                int n10;
                int n11;
                int n12 = this.ubyte(this.icoimage[n4 + 7]);
                n12 <<= 8;
                n12 |= this.ubyte(this.icoimage[n4 + 6]);
                n12 <<= 8;
                n12 |= this.ubyte(this.icoimage[n4 + 5]);
                n12 <<= 8;
                n12 |= this.ubyte(this.icoimage[n4 + 4]);
                if (n == 0) {
                    n = n12;
                }
                int n13 = this.ubyte(this.icoimage[n4 + 11]);
                n13 <<= 8;
                n13 |= this.ubyte(this.icoimage[n4 + 10]);
                n13 <<= 8;
                n13 |= this.ubyte(this.icoimage[n4 + 9]);
                n13 <<= 8;
                n13 |= this.ubyte(this.icoimage[n4 + 8]);
                if (n2 == 0) {
                    n2 = n13 >> 1;
                }
                int n14 = this.ubyte(this.icoimage[n4 + 13]);
                n14 <<= 8;
                n14 |= this.ubyte(this.icoimage[n4 + 12]);
                int n15 = this.ubyte(this.icoimage[n4 + 15]);
                n15 <<= 8;
                n15 |= this.ubyte(this.icoimage[n4 + 14]);
                if (this.colorCount[i] == 0) {
                    if (n14 == 1) {
                        if (n15 == 1) {
                            this.colorCount[i] = 2;
                        } else if (n15 == 4) {
                            this.colorCount[i] = 16;
                        } else if (n15 == 8) {
                            this.colorCount[i] = 256;
                        } else if (n15 != 32) {
                            this.colorCount[i] = (int)Math.pow(2.0, n15);
                        }
                    } else {
                        this.colorCount[i] = (int)Math.pow(2.0, n15 * n14);
                    }
                }
                this.bi[i] = new BufferedImage(n, n2, 2);
                int n16 = n4 + 40;
                if (this.colorCount[i] == 2) {
                    n5 = n16 + 8;
                    n8 = this.calcScanlineBytes(n, 1);
                    n11 = n5 + n8 * n2;
                    arrn = new int[]{128, 64, 32, 16, 8, 4, 2, 1};
                    for (n6 = 0; n6 < n2; ++n6) {
                        for (n7 = 0; n7 < n; ++n7) {
                            n10 = (this.ubyte(this.icoimage[n5 + n6 * n8 + n7 / 8]) & arrn[n7 % 8]) != 0 ? 1 : 0;
                            n9 = 0;
                            n9 |= this.ubyte(this.icoimage[n16 + n10 * 4 + 2]);
                            n9 <<= 8;
                            n9 |= this.ubyte(this.icoimage[n16 + n10 * 4 + 1]);
                            n9 <<= 8;
                            n9 |= this.ubyte(this.icoimage[n16 + n10 * 4]);
                            if ((this.ubyte(this.icoimage[n11 + n6 * n8 + n7 / 8]) & arrn[n7 % 8]) != 0) {
                                this.bi[i].setRGB(n7, n2 - 1 - n6, n9);
                                continue;
                            }
                            this.bi[i].setRGB(n7, n2 - 1 - n6, -16777216 | n9);
                        }
                    }
                    continue;
                }
                if (this.colorCount[i] == 16) {
                    n5 = n16 + 64;
                    n8 = this.calcScanlineBytes(n, 4);
                    n11 = n5 + n8 * n2;
                    arrn = new int[]{128, 64, 32, 16, 8, 4, 2, 1};
                    for (n6 = 0; n6 < n2; ++n6) {
                        for (n7 = 0; n7 < n; ++n7) {
                            if ((n7 & 1) == 0) {
                                n10 = this.ubyte(this.icoimage[n5 + n6 * n8 + n7 / 2]);
                                n10 >>= 4;
                            } else {
                                n10 = this.ubyte(this.icoimage[n5 + n6 * n8 + n7 / 2]) & 15;
                            }
                            n9 = 0;
                            n9 |= this.ubyte(this.icoimage[n16 + n10 * 4 + 2]);
                            n9 <<= 8;
                            n9 |= this.ubyte(this.icoimage[n16 + n10 * 4 + 1]);
                            n9 <<= 8;
                            n9 |= this.ubyte(this.icoimage[n16 + n10 * 4]);
                            if ((this.ubyte(this.icoimage[n11 + n6 * this.calcScanlineBytes(n, 1) + n7 / 8]) & arrn[n7 % 8]) != 0) {
                                this.bi[i].setRGB(n7, n2 - 1 - n6, n9);
                                continue;
                            }
                            this.bi[i].setRGB(n7, n2 - 1 - n6, -16777216 | n9);
                        }
                    }
                    continue;
                }
                if (this.colorCount[i] == 256) {
                    n5 = n16 + 1024;
                    n8 = this.calcScanlineBytes(n, 8);
                    n11 = n5 + n8 * n2;
                    arrn = new int[]{128, 64, 32, 16, 8, 4, 2, 1};
                    for (n6 = 0; n6 < n2; ++n6) {
                        for (n7 = 0; n7 < n; ++n7) {
                            n10 = this.ubyte(this.icoimage[n5 + n6 * n8 + n7]);
                            n9 = 0;
                            n9 |= this.ubyte(this.icoimage[n16 + n10 * 4 + 2]);
                            n9 <<= 8;
                            n9 |= this.ubyte(this.icoimage[n16 + n10 * 4 + 1]);
                            n9 <<= 8;
                            n9 |= this.ubyte(this.icoimage[n16 + n10 * 4]);
                            if ((this.ubyte(this.icoimage[n11 + n6 * this.calcScanlineBytes(n, 1) + n7 / 8]) & arrn[n7 % 8]) != 0) {
                                this.bi[i].setRGB(n7, n2 - 1 - n6, n9);
                                continue;
                            }
                            this.bi[i].setRGB(n7, n2 - 1 - n6, -16777216 | n9);
                        }
                    }
                    continue;
                }
                if (this.colorCount[i] != 0) continue;
                n5 = this.calcScanlineBytes(n, 32);
                for (n8 = 0; n8 < n2; ++n8) {
                    for (n11 = 0; n11 < n; ++n11) {
                        int n17 = this.ubyte(this.icoimage[n16 + n8 * n5 + n11 * 4 + 3]);
                        n17 <<= 8;
                        n17 |= this.ubyte(this.icoimage[n16 + n8 * n5 + n11 * 4 + 2]);
                        n17 <<= 8;
                        n17 |= this.ubyte(this.icoimage[n16 + n8 * n5 + n11 * 4 + 1]);
                        n17 <<= 8;
                        this.bi[i].setRGB(n11, n2 - 1 - n8, n17 |= this.ubyte(this.icoimage[n16 + n8 * n5 + n11 * 4]));
                    }
                }
                continue;
            }
            if (this.ubyte(this.icoimage[n4]) == 137 && this.icoimage[n4 + 1] == 80 && this.icoimage[n4 + 2] == 78 && this.icoimage[n4 + 3] == 71 && this.icoimage[n4 + 4] == 13 && this.icoimage[n4 + 5] == 10 && this.icoimage[n4 + 6] == 26 && this.icoimage[n4 + 7] == 10) {
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(this.icoimage, n4, n3);
                this.bi[i] = ImageIO.read(byteArrayInputStream);
                continue;
            }
            throw new BadIcoResException("BITMAPINFOHEADER or PNG expected");
        }
        this.icoimage = null;
    }

    private void read(InputStream inputStream) throws IOException {
        int n;
        while ((n = inputStream.available()) != 0) {
            byte[] arrby = new byte[this.icoimage.length + n];
            System.arraycopy(this.icoimage, 0, arrby, 0, this.icoimage.length);
            inputStream.read(arrby, this.icoimage.length, n);
            this.icoimage = arrby;
        }
    }

    private int ubyte(byte n) {
        return n < 0 ? 256 + n : n;
    }
}

