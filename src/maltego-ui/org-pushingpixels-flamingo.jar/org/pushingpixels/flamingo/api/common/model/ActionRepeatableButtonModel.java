/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common.model;

import java.awt.AWTEvent;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import javax.swing.DefaultButtonModel;
import javax.swing.Timer;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;

public class ActionRepeatableButtonModel
extends DefaultButtonModel
implements ActionButtonModel {
    private JCommandButton commandButton;
    protected Timer autoRepeatTimer;
    protected boolean toFireActionOnPress;

    public ActionRepeatableButtonModel(JCommandButton jCommandButton) {
        this.commandButton = jCommandButton;
        this.toFireActionOnPress = false;
    }

    @Override
    public void setPressed(boolean bl) {
        if (this.isPressed() == bl || !this.isEnabled()) {
            return;
        }
        this.stateMask = bl ? (this.stateMask |= 4) : (this.stateMask &= -5);
        boolean bl2 = this.isArmed();
        if (this.commandButton.isAutoRepeatAction() || this.isFireActionOnPress()) {
            bl2 = this.isPressed() && bl2;
        } else {
            boolean bl3 = bl2 = !this.isPressed() && bl2;
        }
        if (this.commandButton.getCommandButtonKind() == JCommandButton.CommandButtonKind.POPUP_ONLY) {
            bl2 = false;
        }
        if (this.commandButton.isFireActionOnRollover()) {
            bl2 = false;
        }
        int n = 0;
        AWTEvent aWTEvent = EventQueue.getCurrentEvent();
        if (aWTEvent instanceof InputEvent) {
            n = ((InputEvent)aWTEvent).getModifiers();
        } else if (aWTEvent instanceof ActionEvent) {
            n = ((ActionEvent)aWTEvent).getModifiers();
        }
        if (bl2) {
            this.fireActionPerformed(new ActionEvent(this, 1001, this.getActionCommand(), EventQueue.getMostRecentEventTime(), n));
            if (this.commandButton.isAutoRepeatAction()) {
                this.startActionTimer(n);
            }
        }
        if (!this.commandButton.isFireActionOnRollover() && this.commandButton.isAutoRepeatAction() && !bl) {
            this.stopActionTimer();
        }
        this.fireStateChanged();
    }

    @Override
    public void setRollover(boolean bl) {
        if (this.isRollover() == bl || !this.isEnabled()) {
            return;
        }
        this.stateMask = bl ? (this.stateMask |= 16) : (this.stateMask &= -17);
        if (this.commandButton.isFireActionOnRollover()) {
            if (bl && !this.isActionTimerRunning() && this.commandButton.getCommandButtonKind() != JCommandButton.CommandButtonKind.POPUP_ONLY) {
                int n = 0;
                AWTEvent aWTEvent = EventQueue.getCurrentEvent();
                if (aWTEvent instanceof InputEvent) {
                    n = ((InputEvent)aWTEvent).getModifiers();
                } else if (aWTEvent instanceof ActionEvent) {
                    n = ((ActionEvent)aWTEvent).getModifiers();
                }
                this.fireActionPerformed(new ActionEvent(this, 1001, this.getActionCommand(), EventQueue.getMostRecentEventTime(), n));
                if (this.commandButton.isAutoRepeatAction()) {
                    this.startActionTimer(n);
                }
            }
            if (!bl) {
                this.stopActionTimer();
            }
        }
        this.fireStateChanged();
    }

    private void stopActionTimer() {
        if (this.autoRepeatTimer != null) {
            this.autoRepeatTimer.stop();
        }
    }

    private boolean isActionTimerRunning() {
        if (this.autoRepeatTimer == null) {
            return false;
        }
        return this.autoRepeatTimer.isRunning();
    }

    private void startActionTimer(final int n) {
        this.autoRepeatTimer = new Timer(this.commandButton.getAutoRepeatSubsequentInterval(), new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (!(ActionRepeatableButtonModel.this.isEnabled() && ActionRepeatableButtonModel.this.commandButton.isVisible() && ActionRepeatableButtonModel.this.commandButton.isDisplayable())) {
                    ActionRepeatableButtonModel.this.autoRepeatTimer.stop();
                    return;
                }
                ActionRepeatableButtonModel.this.fireActionPerformed(new ActionEvent(this, 1001, ActionRepeatableButtonModel.this.getActionCommand(), EventQueue.getMostRecentEventTime(), n));
            }
        });
        this.autoRepeatTimer.setInitialDelay(this.commandButton.getAutoRepeatInitialInterval());
        this.autoRepeatTimer.start();
    }

    @Override
    public boolean isFireActionOnPress() {
        return this.toFireActionOnPress;
    }

    @Override
    public void setFireActionOnPress(boolean bl) {
        this.toFireActionOnPress = bl;
    }

}

