/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common;

import org.pushingpixels.flamingo.api.common.AsynchronousLoadListener;

public interface AsynchronousLoading {
    public void addAsynchronousLoadListener(AsynchronousLoadListener var1);

    public void removeAsynchronousLoadListener(AsynchronousLoadListener var1);

    public boolean isLoading();
}

