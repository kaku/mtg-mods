/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.beans.PropertyChangeListener;
import java.util.List;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;

public interface CommandButtonLayoutManager
extends PropertyChangeListener {
    public Dimension getPreferredSize(AbstractCommandButton var1);

    public int getPreferredIconSize();

    public Point getKeyTipAnchorCenterPoint(AbstractCommandButton var1);

    public CommandButtonLayoutInfo getLayoutInfo(AbstractCommandButton var1, Graphics var2);

    public static class CommandButtonLayoutInfo {
        public Rectangle actionClickArea;
        public Rectangle popupClickArea;
        public Rectangle separatorArea;
        public CommandButtonSeparatorOrientation separatorOrientation;
        public Rectangle iconRect;
        public List<TextLayoutInfo> textLayoutInfoList;
        public List<TextLayoutInfo> extraTextLayoutInfoList;
        public Rectangle popupActionRect;
        public boolean isTextInActionArea;
    }

    public static class TextLayoutInfo {
        public String text;
        public Rectangle textRect;
    }

    public static enum CommandButtonSeparatorOrientation {
        VERTICAL,
        HORIZONTAL;
        

        private CommandButtonSeparatorOrientation() {
        }
    }

}

