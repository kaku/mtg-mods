/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common.icon;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

public class EmptyResizableIcon
implements ResizableIcon {
    protected int width;
    protected int height;

    public EmptyResizableIcon(Dimension dimension) {
        this.width = dimension.width;
        this.height = dimension.height;
    }

    public EmptyResizableIcon(int n) {
        this(new Dimension(n, n));
    }

    @Override
    public void setDimension(Dimension dimension) {
        this.width = dimension.width;
        this.height = dimension.height;
    }

    @Override
    public int getIconHeight() {
        return this.height;
    }

    @Override
    public int getIconWidth() {
        return this.width;
    }

    @Override
    public void paintIcon(Component component, Graphics graphics, int n, int n2) {
    }
}

