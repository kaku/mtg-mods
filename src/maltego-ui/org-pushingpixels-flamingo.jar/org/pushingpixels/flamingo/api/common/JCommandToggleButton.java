/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common;

import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;
import org.pushingpixels.flamingo.api.common.model.ActionToggleButtonModel;
import org.pushingpixels.flamingo.internal.ui.common.BasicCommandToggleButtonUI;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonUI;

public class JCommandToggleButton
extends AbstractCommandButton {
    public static final String uiClassID = "CommandToggleButtonUI";

    public JCommandToggleButton(ResizableIcon resizableIcon) {
        this(null, resizableIcon);
    }

    public JCommandToggleButton(String string) {
        this(string, null);
    }

    public JCommandToggleButton(String string, ResizableIcon resizableIcon) {
        super(string, resizableIcon);
        this.setActionModel(new ActionToggleButtonModel(false));
        this.updateUI();
    }

    @Override
    public void updateUI() {
        if (UIManager.get(this.getUIClassID()) != null) {
            this.setUI((CommandButtonUI)UIManager.getUI(this));
        } else {
            this.setUI(BasicCommandToggleButtonUI.createUI(this));
        }
    }

    @Override
    public String getUIClassID() {
        return "CommandToggleButtonUI";
    }

    @Override
    public String toString() {
        return "Command toggle button[" + this.getText() + "]";
    }
}

