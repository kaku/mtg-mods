/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common.icon;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.pushingpixels.flamingo.api.common.AsynchronousLoadListener;
import org.pushingpixels.flamingo.api.common.icon.ImageWrapperIcon;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

public class ImageWrapperResizableIcon
extends ImageWrapperIcon
implements ResizableIcon {
    public static ImageWrapperResizableIcon getIcon(Image image, Dimension dimension) {
        return new ImageWrapperResizableIcon(image, dimension);
    }

    public static ImageWrapperResizableIcon getIcon(URL uRL, Dimension dimension) {
        try {
            return new ImageWrapperResizableIcon(uRL.openStream(), dimension);
        }
        catch (IOException var2_2) {
            var2_2.printStackTrace();
            return null;
        }
    }

    public static ImageWrapperResizableIcon getIcon(InputStream inputStream, Dimension dimension) {
        return new ImageWrapperResizableIcon(inputStream, dimension);
    }

    private ImageWrapperResizableIcon(Image image, Dimension dimension) {
        super(image, dimension.width, dimension.height);
    }

    private ImageWrapperResizableIcon(InputStream inputStream, Dimension dimension) {
        super(inputStream, dimension.width, dimension.height);
    }

    @Override
    public void setDimension(Dimension dimension) {
        this.setPreferredSize(dimension);
    }
}

