/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common.icon;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.InputStream;
import java.util.EventListener;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.swing.Icon;
import javax.swing.SwingWorker;
import javax.swing.event.EventListenerList;
import org.pushingpixels.flamingo.api.common.AsynchronousLoadListener;
import org.pushingpixels.flamingo.api.common.AsynchronousLoading;
import org.pushingpixels.flamingo.api.common.icon.Ico;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

abstract class IcoWrapperIcon
implements Icon,
AsynchronousLoading {
    protected InputStream icoInputStream;
    protected Map<Integer, BufferedImage> icoPlaneMap;
    protected Map<String, BufferedImage> cachedImages;
    protected int width;
    protected int height;
    protected EventListenerList listenerList = new EventListenerList();

    public IcoWrapperIcon(InputStream inputStream, int n, int n2) {
        this.icoInputStream = inputStream;
        this.width = n;
        this.height = n2;
        this.listenerList = new EventListenerList();
        this.cachedImages = new LinkedHashMap<String, BufferedImage>(){

            @Override
            protected boolean removeEldestEntry(Map.Entry<String, BufferedImage> entry) {
                return this.size() > 5;
            }
        };
        this.renderImage(this.width, this.height);
    }

    @Override
    public void addAsynchronousLoadListener(AsynchronousLoadListener asynchronousLoadListener) {
        this.listenerList.add(AsynchronousLoadListener.class, asynchronousLoadListener);
    }

    @Override
    public void removeAsynchronousLoadListener(AsynchronousLoadListener asynchronousLoadListener) {
        this.listenerList.remove(AsynchronousLoadListener.class, asynchronousLoadListener);
    }

    @Override
    public int getIconWidth() {
        return this.width;
    }

    @Override
    public int getIconHeight() {
        return this.height;
    }

    @Override
    public void paintIcon(Component component, Graphics graphics, int n, int n2) {
        BufferedImage bufferedImage = this.cachedImages.get("" + this.getIconWidth() + ":" + this.getIconHeight());
        if (bufferedImage != null) {
            int n3 = (this.width - bufferedImage.getWidth()) / 2;
            int n4 = (this.height - bufferedImage.getHeight()) / 2;
            graphics.drawImage(bufferedImage, n + n3, n2 + n4, null);
        }
    }

    public synchronized void setPreferredSize(Dimension dimension) {
        if (dimension.width == this.width && dimension.height == this.height) {
            return;
        }
        this.width = dimension.width;
        this.height = dimension.height;
        this.renderImage(this.width, this.height);
    }

    protected synchronized void renderImage(final int n, final int n2) {
        String string = "" + n + ":" + n2;
        if (this.cachedImages.containsKey(string)) {
            this.fireAsyncCompleted(true);
            return;
        }
        SwingWorker<BufferedImage, Void> swingWorker = new SwingWorker<BufferedImage, Void>(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            protected BufferedImage doInBackground() throws Exception {
                BufferedImage bufferedImage;
                Object object;
                float f;
                Object object2;
                int n9;
                InputStream inputStream = IcoWrapperIcon.this.icoInputStream;
                synchronized (inputStream) {
                    if (IcoWrapperIcon.this.icoPlaneMap == null) {
                        Ico ico = new Ico(IcoWrapperIcon.this.icoInputStream);
                        IcoWrapperIcon.this.icoPlaneMap = new TreeMap<Integer, BufferedImage>();
                        object = new HashSet();
                        for (int i = 0; i < ico.getNumImages(); ++i) {
                            bufferedImage = ico.getImage(i);
                            object.add(bufferedImage.getWidth());
                        }
                        object2 = object.iterator();
                        while (object2.hasNext()) {
                            int n22 = (Integer)object2.next();
                            BufferedImage bufferedImage2 = null;
                            int n3 = -1;
                            for (n9 = 0; n9 < ico.getNumImages(); ++n9) {
                                BufferedImage bufferedImage3 = ico.getImage(n9);
                                if (bufferedImage3.getWidth() != n22) continue;
                                int n4 = ico.getNumColors(n9);
                                if (n4 == 0) {
                                    bufferedImage2 = bufferedImage3;
                                    n3 = 0;
                                    continue;
                                }
                                if (n3 == 0 || n4 <= n3) continue;
                                bufferedImage2 = bufferedImage3;
                                n3 = n4;
                            }
                            IcoWrapperIcon.this.icoPlaneMap.put(n22, bufferedImage2);
                        }
                    }
                }
                int n5 = -1;
                int n6 = -1;
                object = IcoWrapperIcon.this.icoPlaneMap.entrySet().iterator();
                while (object.hasNext()) {
                    object2 = (Map.Entry)object.next();
                    bufferedImage = (BufferedImage)object2.getValue();
                    int n7 = bufferedImage.getWidth();
                    if (n7 <= n) continue;
                    if (n6 < 0) {
                        n6 = n7;
                        continue;
                    }
                    if (n6 <= n7) continue;
                    n6 = n7;
                }
                if (n5 < 0) {
                    object = IcoWrapperIcon.this.icoPlaneMap.entrySet().iterator();
                    while (object.hasNext()) {
                        object2 = (Map.Entry)object.next();
                        bufferedImage = (BufferedImage)object2.getValue();
                        int n8 = bufferedImage.getWidth();
                        if (n8 <= n6) continue;
                        n6 = n8;
                    }
                }
                object2 = object = IcoWrapperIcon.this.icoPlaneMap.get(n6);
                float f2 = (float)object.getWidth() / (float)n;
                float f3 = Math.max(f2, f = (float)object.getHeight() / (float)n2);
                if (f3 > 1.0f) {
                    n9 = (int)((float)object.getWidth() / f3);
                    object2 = FlamingoUtilities.createThumbnail((BufferedImage)object, n9);
                }
                return object2;
            }

            @Override
            protected void done() {
                try {
                    BufferedImage bufferedImage = (BufferedImage)this.get();
                    IcoWrapperIcon.this.cachedImages.put("" + n + ":" + n2, bufferedImage);
                    IcoWrapperIcon.this.fireAsyncCompleted(true);
                }
                catch (Exception var1_2) {
                    IcoWrapperIcon.this.fireAsyncCompleted(false);
                }
            }
        };
        swingWorker.execute();
    }

    protected void fireAsyncCompleted(Boolean bl) {
        Object[] arrobject = this.listenerList.getListenerList();
        for (int i = arrobject.length - 2; i >= 0; i -= 2) {
            if (arrobject[i] != AsynchronousLoadListener.class) continue;
            ((AsynchronousLoadListener)arrobject[i + 1]).completed(bl);
        }
    }

    @Override
    public synchronized boolean isLoading() {
        BufferedImage bufferedImage = this.cachedImages.get("" + this.getIconWidth() + ":" + this.getIconHeight());
        return bufferedImage == null;
    }

}

