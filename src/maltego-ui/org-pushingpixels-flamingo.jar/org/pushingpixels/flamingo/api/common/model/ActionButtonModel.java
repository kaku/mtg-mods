/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common.model;

import javax.swing.ButtonModel;

public interface ActionButtonModel
extends ButtonModel {
    public void setFireActionOnPress(boolean var1);

    public boolean isFireActionOnPress();
}

