/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common;

import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.Popup;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import org.pushingpixels.flamingo.BlankPopupFixPopupFactory;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelManager;
import org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand;
import org.pushingpixels.flamingo.internal.ui.common.JRichTooltipPanel;

public class RichToolTipManager
extends MouseAdapter
implements MouseMotionListener {
    private Timer initialDelayTimer;
    private Timer dismissTimer;
    private RichTooltip richTooltip;
    private JTrackableComponent insideComponent;
    private MouseEvent mouseEvent;
    static final RichToolTipManager sharedInstance = new RichToolTipManager();
    private Popup tipWindow;
    private JRichTooltipPanel tip;
    private boolean tipShowing = false;
    private static final String TRACKED_FOR_RICH_TOOLTIP = "flamingo.internal.trackedForRichTooltip";

    RichToolTipManager() {
        this.initialDelayTimer = new Timer(750, new InitialDelayTimerAction());
        this.initialDelayTimer.setRepeats(false);
        this.dismissTimer = new Timer(20000, new DismissTimerAction());
        this.dismissTimer.setRepeats(false);
    }

    public void setInitialDelay(int n) {
        this.initialDelayTimer.setInitialDelay(n);
    }

    public int getInitialDelay() {
        return this.initialDelayTimer.getInitialDelay();
    }

    public void setDismissDelay(int n) {
        this.dismissTimer.setInitialDelay(n);
    }

    public int getDismissDelay() {
        return this.dismissTimer.getInitialDelay();
    }

    void showTipWindow(MouseEvent mouseEvent) {
        boolean bl;
        if (this.insideComponent == null || !this.insideComponent.isShowing()) {
            return;
        }
        Point point = this.insideComponent.getLocationOnScreen();
        Point point2 = new Point();
        GraphicsConfiguration graphicsConfiguration = this.insideComponent.getGraphicsConfiguration();
        Rectangle rectangle = graphicsConfiguration.getBounds();
        Insets insets = Toolkit.getDefaultToolkit().getScreenInsets(graphicsConfiguration);
        rectangle.x += insets.left;
        rectangle.y += insets.top;
        rectangle.width -= insets.left + insets.right;
        rectangle.height -= insets.top + insets.bottom;
        this.hideTipWindow();
        this.tip = new JRichTooltipPanel(this.insideComponent.getRichTooltip(mouseEvent));
        this.tip.applyComponentOrientation(this.insideComponent.getComponentOrientation());
        Dimension dimension = this.tip.getPreferredSize();
        AbstractRibbonBand abstractRibbonBand = (AbstractRibbonBand)SwingUtilities.getAncestorOfClass(AbstractRibbonBand.class, this.insideComponent);
        boolean bl2 = this.tip.getComponentOrientation().isLeftToRight();
        boolean bl3 = bl = abstractRibbonBand != null;
        if (bl) {
            point2.x = bl2 ? point.x : point.x + this.insideComponent.getWidth() - dimension.width;
            Point point3 = abstractRibbonBand.getLocationOnScreen();
            point2.y = point3.y + abstractRibbonBand.getHeight() + 4;
            if (point2.y + dimension.height > rectangle.y + rectangle.height) {
                point2.y = point3.y - dimension.height;
            }
        } else {
            point2.x = bl2 ? point.x : point.x + this.insideComponent.getWidth() - dimension.width;
            point2.y = point.y + this.insideComponent.getHeight();
            if (point2.y + dimension.height > rectangle.y + rectangle.height) {
                point2.y = point.y - dimension.height;
            }
        }
        if (point2.x < rectangle.x) {
            point2.x = rectangle.x;
        } else if (point2.x - rectangle.x + dimension.width > rectangle.width) {
            point2.x = rectangle.x + Math.max(0, rectangle.width - dimension.width);
        }
        this.tipWindow = BlankPopupFixPopupFactory.getPopup(this.tip, point2.x, point2.y);
        this.tipWindow.show();
        this.dismissTimer.start();
        this.tipShowing = true;
    }

    void hideTipWindow() {
        if (this.tipWindow != null) {
            this.tipWindow.hide();
            this.tipWindow = null;
            this.tipShowing = false;
            this.tip = null;
            this.dismissTimer.stop();
        }
    }

    public static RichToolTipManager sharedInstance() {
        return sharedInstance;
    }

    public void registerComponent(JTrackableComponent jTrackableComponent) {
        if (Boolean.TRUE.equals(jTrackableComponent.getClientProperty("flamingo.internal.trackedForRichTooltip"))) {
            return;
        }
        jTrackableComponent.addMouseListener(this);
        jTrackableComponent.putClientProperty("flamingo.internal.trackedForRichTooltip", Boolean.TRUE);
    }

    public void unregisterComponent(JTrackableComponent jTrackableComponent) {
        jTrackableComponent.removeMouseListener(this);
        jTrackableComponent.putClientProperty("flamingo.internal.trackedForRichTooltip", null);
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {
        this.initiateToolTip(mouseEvent);
    }

    private void initiateToolTip(MouseEvent mouseEvent) {
        JTrackableComponent jTrackableComponent = (JTrackableComponent)mouseEvent.getSource();
        Point point = mouseEvent.getPoint();
        if (point.x < 0 || point.x >= jTrackableComponent.getWidth() || point.y < 0 || point.y >= jTrackableComponent.getHeight()) {
            return;
        }
        List<PopupPanelManager.PopupInfo> list = PopupPanelManager.defaultManager().getShownPath();
        if (list.size() > 0) {
            JPopupPanel jPopupPanel = list.get(list.size() - 1).getPopupPanel();
            boolean bl = true;
            for (Container container = jTrackableComponent; container != null; container = container.getParent()) {
                if (container != jPopupPanel) continue;
                bl = false;
                break;
            }
            if (bl) {
                return;
            }
        }
        if (this.insideComponent != null) {
            this.initialDelayTimer.stop();
        }
        jTrackableComponent.removeMouseMotionListener(this);
        jTrackableComponent.addMouseMotionListener(this);
        this.insideComponent = jTrackableComponent;
        this.mouseEvent = mouseEvent;
        this.initialDelayTimer.start();
    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {
        this.initialDelayTimer.stop();
        if (this.insideComponent != null) {
            this.insideComponent.removeMouseMotionListener(this);
        }
        this.insideComponent = null;
        this.richTooltip = null;
        this.mouseEvent = null;
        this.hideTipWindow();
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        this.hideTipWindow();
        this.initialDelayTimer.stop();
        this.insideComponent = null;
        this.mouseEvent = null;
    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
        if (this.tipShowing) {
            this.checkForTipChange(mouseEvent);
        } else {
            this.insideComponent = (JTrackableComponent)mouseEvent.getSource();
            this.mouseEvent = mouseEvent;
            this.richTooltip = null;
            this.initialDelayTimer.restart();
        }
    }

    private void checkForTipChange(MouseEvent mouseEvent) {
        boolean bl;
        JTrackableComponent jTrackableComponent = (JTrackableComponent)mouseEvent.getSource();
        RichTooltip richTooltip = jTrackableComponent.getRichTooltip(mouseEvent);
        boolean bl2 = bl = this.richTooltip != richTooltip;
        if (bl) {
            this.hideTipWindow();
            if (richTooltip != null) {
                this.richTooltip = richTooltip;
                this.initialDelayTimer.restart();
            }
        }
    }

    protected class DismissTimerAction
    implements ActionListener {
        protected DismissTimerAction() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            RichToolTipManager.this.hideTipWindow();
            RichToolTipManager.this.initialDelayTimer.stop();
            RichToolTipManager.this.insideComponent = null;
            RichToolTipManager.this.mouseEvent = null;
        }
    }

    protected class InitialDelayTimerAction
    implements ActionListener {
        protected InitialDelayTimerAction() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (RichToolTipManager.this.insideComponent != null && RichToolTipManager.this.insideComponent.isShowing()) {
                if (RichToolTipManager.this.richTooltip == null && RichToolTipManager.this.mouseEvent != null) {
                    RichToolTipManager.this.richTooltip = RichToolTipManager.this.insideComponent.getRichTooltip(RichToolTipManager.this.mouseEvent);
                }
                if (RichToolTipManager.this.richTooltip != null) {
                    boolean bl = true;
                    for (PopupPanelManager.PopupInfo popupInfo : PopupPanelManager.defaultManager().getShownPath()) {
                        if (popupInfo.getPopupOriginator() != RichToolTipManager.this.insideComponent) continue;
                        bl = false;
                        break;
                    }
                    if (bl) {
                        RichToolTipManager.this.showTipWindow(RichToolTipManager.this.mouseEvent);
                    }
                } else {
                    RichToolTipManager.this.insideComponent = null;
                    RichToolTipManager.this.richTooltip = null;
                    RichToolTipManager.this.mouseEvent = null;
                    RichToolTipManager.this.hideTipWindow();
                }
            }
        }
    }

    public static abstract class JTrackableComponent
    extends JComponent {
        public abstract RichTooltip getRichTooltip(MouseEvent var1);
    }

}

