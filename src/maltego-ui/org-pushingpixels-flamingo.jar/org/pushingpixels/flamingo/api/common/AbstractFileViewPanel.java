/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common;

import java.awt.Dimension;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.SwingWorker;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.AsynchronousLoadListener;
import org.pushingpixels.flamingo.api.common.AsynchronousLoading;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButtonPanel;
import org.pushingpixels.flamingo.api.common.ProgressEvent;
import org.pushingpixels.flamingo.api.common.ProgressListener;
import org.pushingpixels.flamingo.api.common.StringValuePair;
import org.pushingpixels.flamingo.api.common.icon.EmptyResizableIcon;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

public abstract class AbstractFileViewPanel<T>
extends JCommandButtonPanel {
    protected Map<String, JCommandButton> buttonMap = new HashMap<String, JCommandButton>();
    protected ProgressListener progressListener;
    protected Set<JCommandButton> loadedSet;
    private SwingWorker<Void, Leaf> mainWorker;

    public AbstractFileViewPanel(int n, ProgressListener progressListener) {
        super(n);
        this.progressListener = progressListener;
        this.loadedSet = new HashSet<JCommandButton>();
        this.setToShowGroupLabels(false);
    }

    public AbstractFileViewPanel(CommandButtonDisplayState commandButtonDisplayState, ProgressListener progressListener) {
        super(commandButtonDisplayState);
        this.progressListener = progressListener;
        this.loadedSet = new HashSet<JCommandButton>();
        this.setToShowGroupLabels(false);
    }

    public void setFolder(final List<StringValuePair<T>> list) {
        this.removeAllGroups();
        this.addButtonGroup("");
        this.buttonMap.clear();
        int n = 0;
        final HashMap<String, JCommandButton> hashMap = new HashMap<String, JCommandButton>();
        for (StringValuePair<T> stringValuePair : list) {
            String string = (String)stringValuePair.getKey();
            if (!this.toShowFile(stringValuePair)) continue;
            int n2 = this.currDimension;
            if (n2 < 0) {
                n2 = this.currState.getPreferredIconSize();
            }
            JCommandButton jCommandButton = new JCommandButton(string, new EmptyResizableIcon(n2));
            jCommandButton.setHorizontalAlignment(2);
            jCommandButton.setDisplayState(this.currState);
            if (this.currState == CommandButtonDisplayState.FIT_TO_ICON) {
                jCommandButton.updateCustomDimension(this.currDimension);
            }
            this.addButtonToLastGroup(jCommandButton);
            hashMap.put(string, jCommandButton);
            this.buttonMap.put(string, jCommandButton);
            ++n;
        }
        this.doLayout();
        this.repaint();
        final int n3 = n;
        this.mainWorker = new SwingWorker<Void, Leaf>(){

            @Override
            protected Void doInBackground() throws Exception {
                if (n3 > 0 && AbstractFileViewPanel.this.progressListener != null) {
                    AbstractFileViewPanel.this.progressListener.onProgress(new ProgressEvent(AbstractFileViewPanel.this, 0, n3, 0));
                }
                for (StringValuePair stringValuePair : list) {
                    if (this.isCancelled()) break;
                    String string = (String)stringValuePair.getKey();
                    if (!AbstractFileViewPanel.this.toShowFile(stringValuePair)) continue;
                    InputStream inputStream = AbstractFileViewPanel.this.getLeafContent(stringValuePair.getValue());
                    Leaf leaf = new Leaf(string, inputStream);
                    leaf.setLeafProp("source", stringValuePair.getValue());
                    for (Map.Entry<String, Object> entry : stringValuePair.getProps().entrySet()) {
                        leaf.setLeafProp(entry.getKey(), entry.getValue());
                    }
                    this.publish(leaf);
                }
                return null;
            }

            @Override
            protected void process(List<Leaf> list2) {
                for (Leaf leaf : list2) {
                    Dimension dimension;
                    String string = leaf.getLeafName();
                    InputStream inputStream = leaf.getLeafStream();
                    ResizableIcon resizableIcon = AbstractFileViewPanel.this.getResizableIcon(leaf, inputStream, AbstractFileViewPanel.this.currState, dimension = new Dimension(AbstractFileViewPanel.this.currDimension, AbstractFileViewPanel.this.currDimension));
                    if (resizableIcon == null) continue;
                    final JCommandButton jCommandButton = (JCommandButton)hashMap.get(string);
                    jCommandButton.setIcon(resizableIcon);
                    if (resizableIcon instanceof AsynchronousLoading) {
                        ((AsynchronousLoading)((Object)resizableIcon)).addAsynchronousLoadListener(new AsynchronousLoadListener(){

                            /*
                             * WARNING - Removed try catching itself - possible behaviour change.
                             */
                            @Override
                            public void completed(boolean bl) {
                                AbstractFileViewPanel abstractFileViewPanel = AbstractFileViewPanel.this;
                                synchronized (abstractFileViewPanel) {
                                    if (AbstractFileViewPanel.this.loadedSet.contains(jCommandButton)) {
                                        return;
                                    }
                                    AbstractFileViewPanel.this.loadedSet.add(jCommandButton);
                                    if (AbstractFileViewPanel.this.progressListener != null) {
                                        AbstractFileViewPanel.this.progressListener.onProgress(new ProgressEvent(AbstractFileViewPanel.this, 0, n3, AbstractFileViewPanel.this.loadedSet.size()));
                                        if (AbstractFileViewPanel.this.loadedSet.size() == n3) {
                                            AbstractFileViewPanel.this.progressListener.onProgress(new ProgressEvent(AbstractFileViewPanel.this, 0, n3, n3));
                                        }
                                    }
                                }
                            }
                        });
                    }
                    AbstractFileViewPanel.this.configureCommandButton(leaf, jCommandButton, resizableIcon);
                    jCommandButton.setDisplayState(AbstractFileViewPanel.this.currState);
                    if (AbstractFileViewPanel.this.currState != CommandButtonDisplayState.FIT_TO_ICON) continue;
                    jCommandButton.updateCustomDimension(AbstractFileViewPanel.this.currDimension);
                }
            }

        };
        this.mainWorker.execute();
    }

    public int getLoadedIconCount() {
        return this.loadedSet.size();
    }

    public void cancelMainWorker() {
        if (this.mainWorker == null) {
            return;
        }
        if (this.mainWorker.isDone() || this.mainWorker.isCancelled()) {
            return;
        }
        this.mainWorker.cancel(false);
    }

    public Map<String, JCommandButton> getButtonMap() {
        return Collections.unmodifiableMap(this.buttonMap);
    }

    protected abstract boolean toShowFile(StringValuePair<T> var1);

    protected abstract ResizableIcon getResizableIcon(Leaf var1, InputStream var2, CommandButtonDisplayState var3, Dimension var4);

    protected abstract void configureCommandButton(Leaf var1, JCommandButton var2, ResizableIcon var3);

    protected abstract InputStream getLeafContent(T var1);

    public static class Leaf {
        protected String leafName;
        protected InputStream leafStream;
        protected Map<String, Object> leafProps;

        public Leaf(String string, InputStream inputStream) {
            this.leafName = string;
            this.leafStream = inputStream;
            this.leafProps = new HashMap<String, Object>();
        }

        public String getLeafName() {
            return this.leafName;
        }

        public InputStream getLeafStream() {
            return this.leafStream;
        }

        public Object getLeafProp(String string) {
            return this.leafProps.get(string);
        }

        public void setLeafProp(String string, Object object) {
            this.leafProps.put(string, object);
        }

        public Map<String, Object> getLeafProps() {
            return Collections.unmodifiableMap(this.leafProps);
        }
    }

}

