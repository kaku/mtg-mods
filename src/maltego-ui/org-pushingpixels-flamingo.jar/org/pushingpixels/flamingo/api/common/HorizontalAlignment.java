/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common;

public enum HorizontalAlignment {
    LEADING,
    CENTER,
    TRAILING,
    FILL;
    

    private HorizontalAlignment() {
    }
}

