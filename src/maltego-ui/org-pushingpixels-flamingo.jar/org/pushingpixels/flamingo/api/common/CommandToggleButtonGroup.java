/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.pushingpixels.flamingo.api.common.JCommandToggleButton;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;

public class CommandToggleButtonGroup
implements Serializable {
    protected Vector<JCommandToggleButton> buttons = new Vector();
    protected Map<JCommandToggleButton, ChangeListener> modelChangeListeners = new HashMap<JCommandToggleButton, ChangeListener>();
    private PropertyChangeSupport changeSupport;
    public static final String SELECTED_PROPERTY = "selected";
    protected JCommandToggleButton selection;
    protected boolean allowsClearingSelection = true;

    public void setAllowsClearingSelection(boolean bl) {
        this.allowsClearingSelection = bl;
    }

    public boolean isAllowsClearingSelection() {
        return this.allowsClearingSelection;
    }

    public void add(final JCommandToggleButton jCommandToggleButton) {
        boolean bl;
        if (jCommandToggleButton == null) {
            return;
        }
        this.buttons.addElement(jCommandToggleButton);
        boolean bl2 = bl = this.selection == null;
        if (jCommandToggleButton.getActionModel().isSelected()) {
            if (bl) {
                this.selection = jCommandToggleButton;
            } else {
                jCommandToggleButton.getActionModel().setSelected(false);
            }
        }
        ChangeListener changeListener = new ChangeListener(){
            boolean wasSelected;

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                boolean bl = jCommandToggleButton.getActionModel().isSelected();
                if (this.wasSelected != bl) {
                    CommandToggleButtonGroup.this.setSelected(jCommandToggleButton, bl);
                }
                this.wasSelected = bl;
            }
        };
        jCommandToggleButton.getActionModel().addChangeListener(changeListener);
        this.modelChangeListeners.put(jCommandToggleButton, ()changeListener);
        if (bl) {
            this.firePropertyChange("selected", null, jCommandToggleButton);
        }
    }

    public void remove(JCommandToggleButton jCommandToggleButton) {
        boolean bl;
        if (jCommandToggleButton == null) {
            return;
        }
        this.buttons.removeElement(jCommandToggleButton);
        boolean bl2 = bl = jCommandToggleButton == this.selection;
        if (bl) {
            this.selection = null;
        }
        jCommandToggleButton.getActionModel().removeChangeListener(this.modelChangeListeners.get(jCommandToggleButton));
        this.modelChangeListeners.remove(jCommandToggleButton);
        if (bl) {
            this.firePropertyChange("selected", jCommandToggleButton, null);
        }
    }

    public void setSelected(JCommandToggleButton jCommandToggleButton, boolean bl) {
        if (bl && jCommandToggleButton != null && jCommandToggleButton != this.selection) {
            JCommandToggleButton jCommandToggleButton2 = this.selection;
            this.selection = jCommandToggleButton;
            if (jCommandToggleButton2 != null) {
                jCommandToggleButton2.getActionModel().setSelected(false);
            }
            jCommandToggleButton.getActionModel().setSelected(true);
            this.firePropertyChange("selected", jCommandToggleButton2, jCommandToggleButton);
        }
        if (!bl && jCommandToggleButton != null && jCommandToggleButton == this.selection) {
            if (this.allowsClearingSelection) {
                this.selection = null;
                jCommandToggleButton.getActionModel().setSelected(false);
                this.firePropertyChange("selected", jCommandToggleButton, null);
            } else {
                jCommandToggleButton.getActionModel().setSelected(true);
            }
        }
    }

    public JCommandToggleButton getSelected() {
        return this.selection;
    }

    public void clearSelection() {
        if (this.allowsClearingSelection && this.selection != null) {
            this.selection.getActionModel().setSelected(false);
        }
    }

    public synchronized void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        if (propertyChangeListener == null) {
            return;
        }
        if (this.changeSupport == null) {
            this.changeSupport = new PropertyChangeSupport(this);
        }
        this.changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public synchronized void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        if (propertyChangeListener == null || this.changeSupport == null) {
            return;
        }
        this.changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    protected void firePropertyChange(String string, Object object, Object object2) {
        PropertyChangeSupport propertyChangeSupport = this.changeSupport;
        if (propertyChangeSupport == null || object == object2) {
            return;
        }
        propertyChangeSupport.firePropertyChange(string, object, object2);
    }

}

