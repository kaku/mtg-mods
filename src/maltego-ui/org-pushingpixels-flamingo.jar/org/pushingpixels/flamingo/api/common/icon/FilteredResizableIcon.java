/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common.icon;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ImageObserver;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.swing.UIManager;
import org.pushingpixels.flamingo.api.common.AsynchronousLoading;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public class FilteredResizableIcon
implements ResizableIcon {
    protected Map<String, BufferedImage> cachedImages;
    protected ResizableIcon delegate;
    protected BufferedImageOp operation;

    public FilteredResizableIcon(ResizableIcon resizableIcon, BufferedImageOp bufferedImageOp) {
        this.delegate = resizableIcon;
        this.operation = bufferedImageOp;
        this.cachedImages = new LinkedHashMap<String, BufferedImage>(){

            @Override
            protected boolean removeEldestEntry(Map.Entry<String, BufferedImage> entry) {
                return this.size() > 5;
            }
        };
    }

    @Override
    public int getIconHeight() {
        return this.delegate.getIconHeight();
    }

    @Override
    public int getIconWidth() {
        return this.delegate.getIconWidth();
    }

    @Override
    public void setDimension(Dimension dimension) {
        this.delegate.setDimension(dimension);
    }

    @Override
    public void paintIcon(Component component, Graphics graphics, int n, int n2) {
        String string = "" + this.getIconWidth() + ":" + this.getIconHeight();
        if (!this.cachedImages.containsKey(string)) {
            Object object;
            if (this.delegate instanceof AsynchronousLoading && (object = (AsynchronousLoading)((Object)this.delegate)).isLoading()) {
                return;
            }
            object = FlamingoUtilities.getBlankImage(this.getIconWidth(), this.getIconHeight());
            Graphics2D graphics2D = object.createGraphics();
            this.delegate.paintIcon(component, graphics2D, 0, 0);
            graphics2D.dispose();
            BufferedImage bufferedImage = this.operation.filter((BufferedImage)object, null);
            bufferedImage = this.toOneColor(bufferedImage);
            this.cachedImages.put(string, bufferedImage);
        }
        graphics.drawImage(this.cachedImages.get(string), n, n2, null);
    }

    private BufferedImage toOneColor(BufferedImage bufferedImage) {
        int n = bufferedImage.getWidth();
        int n2 = bufferedImage.getHeight();
        BufferedImage bufferedImage2 = new BufferedImage(n, n2, 2);
        Graphics2D graphics2D = (Graphics2D)bufferedImage2.getGraphics();
        Color color = UIManager.getLookAndFeelDefaults().getColor("ribbon-disabled-icon-color");
        color = color == null ? Color.LIGHT_GRAY : color;
        graphics2D.setColor(color);
        graphics2D.fillRect(0, 0, n, n2);
        graphics2D.setComposite(AlphaComposite.DstIn);
        graphics2D.drawImage(bufferedImage, 0, 0, n, n2, 0, 0, n, n2, null);
        return bufferedImage2;
    }

}

