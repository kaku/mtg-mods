/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common;

import java.awt.event.ActionEvent;
import java.util.EventListener;
import javax.swing.UIManager;
import javax.swing.event.EventListenerList;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.RolloverActionListener;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;
import org.pushingpixels.flamingo.internal.ui.common.BasicCommandMenuButtonUI;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonUI;

public class JCommandMenuButton
extends JCommandButton {
    public static final String uiClassID = "CommandMenuButtonUI";

    public JCommandMenuButton(String string, ResizableIcon resizableIcon) {
        super(string, resizableIcon);
    }

    public void addRolloverActionListener(RolloverActionListener rolloverActionListener) {
        this.listenerList.add(RolloverActionListener.class, rolloverActionListener);
    }

    public void removeRolloverActionListener(RolloverActionListener rolloverActionListener) {
        this.listenerList.remove(RolloverActionListener.class, rolloverActionListener);
    }

    @Override
    public String getUIClassID() {
        return "CommandMenuButtonUI";
    }

    @Override
    public void updateUI() {
        if (UIManager.get(this.getUIClassID()) != null) {
            this.setUI((CommandButtonUI)UIManager.getUI(this));
        } else {
            this.setUI(BasicCommandMenuButtonUI.createUI(this));
        }
    }

    @Override
    boolean canHaveBothKeyTips() {
        return true;
    }

    public void doActionRollover() {
        ActionEvent actionEvent = new ActionEvent(this, 1001, this.getActionModel().getActionCommand());
        RolloverActionListener[] arrrolloverActionListener = (RolloverActionListener[])this.getListeners(RolloverActionListener.class);
        for (int i = arrrolloverActionListener.length - 1; i >= 0; --i) {
            arrrolloverActionListener[i].actionPerformed(actionEvent);
        }
    }
}

