/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common;

import java.util.EventListener;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.PanelUI;
import org.pushingpixels.flamingo.internal.ui.common.BasicScrollablePanelUI;
import org.pushingpixels.flamingo.internal.ui.common.ScrollablePanelUI;

public class JScrollablePanel<T extends JComponent>
extends JPanel {
    public static final String uiClassID = "ScrollablePanelUI";
    private T view;
    private ScrollType scrollType;
    private boolean isScrollOnRollover;

    public JScrollablePanel(T t, ScrollType scrollType) {
        this.view = t;
        this.scrollType = scrollType;
        this.isScrollOnRollover = true;
        this.updateUI();
    }

    @Override
    public ScrollablePanelUI getUI() {
        return (ScrollablePanelUI)this.ui;
    }

    @Override
    public String getUIClassID() {
        return "ScrollablePanelUI";
    }

    @Override
    public void updateUI() {
        if (UIManager.get(this.getUIClassID()) != null) {
            this.setUI((ScrollablePanelUI)UIManager.getUI(this));
        } else {
            this.setUI(BasicScrollablePanelUI.createUI(this));
        }
    }

    public void setScrollOnRollover(boolean bl) {
        boolean bl2 = this.isScrollOnRollover;
        this.isScrollOnRollover = bl;
        if (bl2 != this.isScrollOnRollover) {
            this.firePropertyChange("scrollOnRollover", bl2, this.isScrollOnRollover);
        }
    }

    public void scrollToIfNecessary(int n, int n2) {
        this.getUI().scrollToIfNecessary(n, n2);
    }

    public T getView() {
        return this.view;
    }

    public void addChangeListener(ChangeListener changeListener) {
        this.listenerList.add(ChangeListener.class, changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this.listenerList.remove(ChangeListener.class, changeListener);
    }

    protected void fireStateChanged() {
        Object[] arrobject = this.listenerList.getListenerList();
        ChangeEvent changeEvent = new ChangeEvent(this);
        for (int i = arrobject.length - 2; i >= 0; i -= 2) {
            if (arrobject[i] != ChangeListener.class) continue;
            ((ChangeListener)arrobject[i + 1]).stateChanged(changeEvent);
        }
    }

    @Override
    public void doLayout() {
        super.doLayout();
        this.fireStateChanged();
    }

    public ScrollType getScrollType() {
        return this.scrollType;
    }

    public boolean isScrollOnRollover() {
        return this.isScrollOnRollover;
    }

    public boolean isShowingScrollButtons() {
        return this.getUI().isShowingScrollButtons();
    }

    public static enum ScrollType {
        VERTICALLY,
        HORIZONTALLY;
        

        private ScrollType() {
        }
    }

}

