/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common.icon;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import org.pushingpixels.flamingo.api.common.AsynchronousLoadListener;
import org.pushingpixels.flamingo.api.common.AsynchronousLoading;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

public class IconDeckResizableIcon<T>
implements ResizableIcon,
AsynchronousLoading {
    private ResizableIcon currentIcon;
    private final Map<T, ? extends ResizableIcon> iconDeck;

    public IconDeckResizableIcon(Map<T, ? extends ResizableIcon> map) {
        if (map.isEmpty()) {
            throw new IllegalArgumentException("Icon deck is empty; must have at least one icon");
        }
        this.iconDeck = map;
        this.currentIcon = map.values().iterator().next();
    }

    public void setIcon(T t) {
        this.currentIcon = this.iconDeck.get(t);
    }

    @Override
    public void setDimension(Dimension dimension) {
        for (ResizableIcon resizableIcon : this.iconDeck.values()) {
            int n = resizableIcon.getIconHeight();
            int n2 = resizableIcon.getIconWidth();
            if (n == dimension.height && n2 == dimension.width) continue;
            resizableIcon.setDimension(dimension);
        }
    }

    @Override
    public int getIconHeight() {
        return this.currentIcon.getIconHeight();
    }

    @Override
    public int getIconWidth() {
        return this.currentIcon.getIconWidth();
    }

    @Override
    public void paintIcon(Component component, Graphics graphics, int n, int n2) {
        this.currentIcon.paintIcon(component, graphics, n, n2);
    }

    @Override
    public void addAsynchronousLoadListener(AsynchronousLoadListener asynchronousLoadListener) {
        for (ResizableIcon resizableIcon : this.iconDeck.values()) {
            if (!(resizableIcon instanceof AsynchronousLoading)) continue;
            ((AsynchronousLoading)((Object)resizableIcon)).addAsynchronousLoadListener(asynchronousLoadListener);
        }
    }

    @Override
    public boolean isLoading() {
        for (ResizableIcon resizableIcon : this.iconDeck.values()) {
            if (!(resizableIcon instanceof AsynchronousLoading) || !((AsynchronousLoading)((Object)resizableIcon)).isLoading()) continue;
            return true;
        }
        return false;
    }

    @Override
    public void removeAsynchronousLoadListener(AsynchronousLoadListener asynchronousLoadListener) {
        for (ResizableIcon resizableIcon : this.iconDeck.values()) {
            if (!(resizableIcon instanceof AsynchronousLoading)) continue;
            ((AsynchronousLoading)((Object)resizableIcon)).removeAsynchronousLoadListener(asynchronousLoadListener);
        }
    }
}

