/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common.icon;

import java.awt.Dimension;
import javax.swing.Icon;

public interface ResizableIcon
extends Icon {
    public void setDimension(Dimension var1);
}

