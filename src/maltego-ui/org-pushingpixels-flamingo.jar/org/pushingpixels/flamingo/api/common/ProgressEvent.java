/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common;

import java.util.EventObject;

public class ProgressEvent
extends EventObject {
    private int minimum;
    private int maximum;
    private int progress;

    public ProgressEvent(Object object, int n, int n2, int n3) {
        super(object);
        this.maximum = n2;
        this.minimum = n;
        this.progress = n3;
    }

    public int getMaximum() {
        return this.maximum;
    }

    public int getMinimum() {
        return this.minimum;
    }

    public int getProgress() {
        return this.progress;
    }
}

