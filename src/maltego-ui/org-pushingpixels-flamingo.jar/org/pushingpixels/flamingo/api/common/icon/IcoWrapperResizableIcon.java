/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common.icon;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.pushingpixels.flamingo.api.common.AsynchronousLoadListener;
import org.pushingpixels.flamingo.api.common.icon.IcoWrapperIcon;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

public class IcoWrapperResizableIcon
extends IcoWrapperIcon
implements ResizableIcon {
    public static IcoWrapperResizableIcon getIcon(URL uRL, Dimension dimension) {
        try {
            return new IcoWrapperResizableIcon(uRL.openStream(), dimension);
        }
        catch (IOException var2_2) {
            var2_2.printStackTrace();
            return null;
        }
    }

    public static IcoWrapperResizableIcon getIcon(InputStream inputStream, Dimension dimension) {
        return new IcoWrapperResizableIcon(inputStream, dimension);
    }

    private IcoWrapperResizableIcon(InputStream inputStream, Dimension dimension) {
        super(inputStream, dimension.width, dimension.height);
    }

    @Override
    public void setDimension(Dimension dimension) {
        this.setPreferredSize(dimension);
    }
}

