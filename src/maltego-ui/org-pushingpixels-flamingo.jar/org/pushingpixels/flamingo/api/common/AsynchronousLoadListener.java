/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common;

import java.util.EventListener;

public interface AsynchronousLoadListener
extends EventListener {
    public void completed(boolean var1);
}

