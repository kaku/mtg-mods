/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common.popup;

import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;

public interface PopupPanelCallback {
    public JPopupPanel getPopupPanel(JCommandButton var1);
}

