/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common.icon;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import org.pushingpixels.flamingo.api.common.AsynchronousLoadListener;
import org.pushingpixels.flamingo.api.common.AsynchronousLoading;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

public class DecoratedResizableIcon
implements ResizableIcon,
AsynchronousLoading {
    protected ResizableIcon delegate;
    protected List<IconDecorator> decorators;

    public /* varargs */ DecoratedResizableIcon(ResizableIcon resizableIcon, IconDecorator ... arriconDecorator) {
        this.delegate = resizableIcon;
        this.decorators = new ArrayList<IconDecorator>();
        if (arriconDecorator != null) {
            for (IconDecorator iconDecorator : arriconDecorator) {
                this.decorators.add(iconDecorator);
            }
        }
    }

    public DecoratedResizableIcon(ResizableIcon resizableIcon) {
        this(resizableIcon, new IconDecorator[]{null});
    }

    @Override
    public int getIconHeight() {
        return this.delegate.getIconHeight();
    }

    @Override
    public int getIconWidth() {
        return this.delegate.getIconWidth();
    }

    @Override
    public void paintIcon(Component component, Graphics graphics, int n, int n2) {
        this.delegate.paintIcon(component, graphics, n, n2);
        for (IconDecorator iconDecorator : this.decorators) {
            iconDecorator.paintIconDecoration(component, graphics, n, n2, this.delegate.getIconWidth(), this.delegate.getIconHeight());
        }
    }

    @Override
    public void setDimension(Dimension dimension) {
        this.delegate.setDimension(dimension);
    }

    public void addIconDecorator(IconDecorator iconDecorator) {
        if (this.decorators.contains(iconDecorator)) {
            return;
        }
        this.decorators.add(iconDecorator);
    }

    public void removeIconDecorator(IconDecorator iconDecorator) {
        this.decorators.remove(iconDecorator);
    }

    @Override
    public void addAsynchronousLoadListener(AsynchronousLoadListener asynchronousLoadListener) {
        if (this.delegate instanceof AsynchronousLoading) {
            ((AsynchronousLoading)((Object)this.delegate)).addAsynchronousLoadListener(asynchronousLoadListener);
        }
    }

    @Override
    public void removeAsynchronousLoadListener(AsynchronousLoadListener asynchronousLoadListener) {
        if (this.delegate instanceof AsynchronousLoading) {
            ((AsynchronousLoading)((Object)this.delegate)).removeAsynchronousLoadListener(asynchronousLoadListener);
        }
    }

    @Override
    public synchronized boolean isLoading() {
        if (this.delegate instanceof AsynchronousLoading && ((AsynchronousLoading)((Object)this.delegate)).isLoading()) {
            return true;
        }
        for (IconDecorator iconDecorator : this.decorators) {
            if (!(iconDecorator instanceof AsynchronousLoading) || !((AsynchronousLoading)((Object)iconDecorator)).isLoading()) continue;
            return true;
        }
        return false;
    }

    public static interface IconDecorator {
        public void paintIconDecoration(Component var1, Graphics var2, int var3, int var4, int var5, int var6);
    }

}

