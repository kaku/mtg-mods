/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common.popup;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.Popup;
import javax.swing.event.EventListenerList;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.model.PopupButtonModel;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;

public class PopupPanelManager {
    protected EventListenerList listenerList = new EventListenerList();
    private static final PopupPanelManager instance = new PopupPanelManager();
    protected LinkedList<PopupInfo> shownPath = new LinkedList();
    protected Map<JPopupPanel, Popup> popupPanels = new HashMap<JPopupPanel, Popup>();

    public static PopupPanelManager defaultManager() {
        return instance;
    }

    public void addPopup(JComponent jComponent, Popup popup, JPopupPanel jPopupPanel) {
        this.popupPanels.put(jPopupPanel, popup);
        this.shownPath.addLast(new PopupInfo(jComponent, jPopupPanel));
        popup.show();
        if (jComponent instanceof JCommandButton) {
            ((JCommandButton)jComponent).getPopupModel().setPopupShowing(true);
        }
        this.firePopupShown(jPopupPanel, jComponent);
    }

    public void hideLastPopup() {
        if (this.shownPath.size() == 0) {
            return;
        }
        PopupInfo popupInfo = this.shownPath.removeLast();
        Popup popup = this.popupPanels.get(popupInfo.popupPanel);
        popup.hide();
        this.popupPanels.remove(popupInfo.popupPanel);
        if (popupInfo.popupOriginator instanceof JCommandButton) {
            ((JCommandButton)popupInfo.popupOriginator).getPopupModel().setPopupShowing(false);
        }
        this.firePopupHidden(popupInfo.popupPanel, popupInfo.popupOriginator);
    }

    public void hidePopups(Component component) {
        Object object;
        Object object2;
        boolean bl = false;
        if (component != null) {
            for (object2 = component; object2 != null; object2 = object2.getParent()) {
                if (!(object2 instanceof JPopupPanel)) continue;
                bl = true;
                while (this.shownPath.size() > 0) {
                    if (this.shownPath.getLast().popupPanel == object2) {
                        return;
                    }
                    object = this.shownPath.removeLast();
                    Popup popup = this.popupPanels.get(((PopupInfo)object).popupPanel);
                    popup.hide();
                    if (((PopupInfo)object).popupOriginator instanceof JCommandButton) {
                        ((JCommandButton)((PopupInfo)object).popupOriginator).getPopupModel().setPopupShowing(false);
                    }
                    this.firePopupHidden(((PopupInfo)object).popupPanel, ((PopupInfo)object).popupOriginator);
                    this.popupPanels.remove(((PopupInfo)object).popupPanel);
                }
            }
        }
        if (!bl || component == null) {
            while (this.shownPath.size() > 0) {
                object2 = this.shownPath.removeLast();
                object = this.popupPanels.get(((PopupInfo)object2).popupPanel);
                object.hide();
                if (((PopupInfo)object2).popupOriginator instanceof JCommandButton) {
                    ((JCommandButton)((PopupInfo)object2).popupOriginator).getPopupModel().setPopupShowing(false);
                }
                this.firePopupHidden(((PopupInfo)object2).popupPanel, ((PopupInfo)object2).popupOriginator);
                this.popupPanels.remove(((PopupInfo)object2).popupPanel);
            }
        }
    }

    public List<PopupInfo> getShownPath() {
        ArrayList<PopupInfo> arrayList = new ArrayList<PopupInfo>();
        for (PopupInfo popupInfo : this.shownPath) {
            arrayList.add(popupInfo);
        }
        return arrayList;
    }

    public void addPopupListener(PopupListener popupListener) {
        this.listenerList.add(PopupListener.class, popupListener);
    }

    public void removePopupListener(PopupListener popupListener) {
        this.listenerList.remove(PopupListener.class, popupListener);
    }

    protected void firePopupShown(JPopupPanel jPopupPanel, JComponent jComponent) {
        Object[] arrobject = this.listenerList.getListenerList();
        PopupEvent popupEvent = new PopupEvent(jPopupPanel, 100, jComponent);
        for (int i = arrobject.length - 2; i >= 0; i -= 2) {
            if (arrobject[i] != PopupListener.class) continue;
            ((PopupListener)arrobject[i + 1]).popupShown(popupEvent);
        }
    }

    protected void firePopupHidden(JPopupPanel jPopupPanel, JComponent jComponent) {
        Object[] arrobject = this.listenerList.getListenerList();
        PopupEvent popupEvent = new PopupEvent(jPopupPanel, 101, jComponent);
        for (int i = arrobject.length - 2; i >= 0; i -= 2) {
            if (arrobject[i] != PopupListener.class) continue;
            ((PopupListener)arrobject[i + 1]).popupHidden(popupEvent);
        }
    }

    public static class PopupInfo {
        private JPopupPanel popupPanel;
        private JComponent popupOriginator;

        public PopupInfo(JComponent jComponent, JPopupPanel jPopupPanel) {
            this.popupOriginator = jComponent;
            this.popupPanel = jPopupPanel;
        }

        public JPopupPanel getPopupPanel() {
            return this.popupPanel;
        }

        public JComponent getPopupOriginator() {
            return this.popupOriginator;
        }
    }

    public static class PopupEvent
    extends ComponentEvent {
        public static final int POPUP_SHOWN = 100;
        public static final int POPUP_HIDDEN = 101;
        private JComponent popupOriginator;

        public PopupEvent(JPopupPanel jPopupPanel, int n, JComponent jComponent) {
            super(jPopupPanel, n);
            this.popupOriginator = jComponent;
        }

        public JComponent getPopupOriginator() {
            return this.popupOriginator;
        }
    }

    public static interface PopupListener
    extends EventListener {
        public void popupShown(PopupEvent var1);

        public void popupHidden(PopupEvent var1);
    }

}

