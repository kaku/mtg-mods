/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common;

import java.awt.AWTEvent;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.EventListener;
import javax.swing.DefaultButtonModel;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.common.PopupActionListener;
import org.pushingpixels.flamingo.api.common.RichToolTipManager;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;
import org.pushingpixels.flamingo.api.common.model.ActionRepeatableButtonModel;
import org.pushingpixels.flamingo.api.common.model.PopupButtonModel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback;
import org.pushingpixels.flamingo.internal.ui.common.BasicCommandButtonUI;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonUI;

public class JCommandButton
extends AbstractCommandButton {
    public static final String uiClassID = "CommandButtonUI";
    protected PopupPanelCallback popupCallback;
    protected CommandButtonKind commandButtonKind;
    protected CommandButtonPopupOrientationKind popupOrientationKind;
    protected boolean isAutoRepeatAction;
    protected int autoRepeatInitialInterval;
    protected int autoRepeatSubsequentInterval;
    protected boolean isFireActionOnRollover;
    protected PopupButtonModel popupModel;
    protected PopupHandler popupHandler;
    private RichTooltip popupRichTooltip;
    protected String popupKeyTip;

    public JCommandButton(ResizableIcon resizableIcon) {
        this(null, resizableIcon);
    }

    public JCommandButton(String string) {
        this(string, null);
    }

    public JCommandButton(String string, ResizableIcon resizableIcon) {
        super(string, resizableIcon);
        this.setActionModel(new ActionRepeatableButtonModel(this));
        this.popupHandler = new PopupHandler();
        this.setPopupModel(new DefaultPopupButtonModel());
        this.commandButtonKind = CommandButtonKind.ACTION_ONLY;
        this.popupOrientationKind = CommandButtonPopupOrientationKind.DOWNWARD;
        this.isAutoRepeatAction = false;
        this.autoRepeatInitialInterval = 500;
        this.autoRepeatSubsequentInterval = 100;
        this.updateUI();
    }

    public CommandButtonKind getCommandButtonKind() {
        return this.commandButtonKind;
    }

    public void setCommandButtonKind(CommandButtonKind commandButtonKind) {
        CommandButtonKind commandButtonKind2 = this.commandButtonKind;
        this.commandButtonKind = commandButtonKind;
        if (commandButtonKind2 != this.commandButtonKind) {
            this.firePropertyChange("commandButtonKind", (Object)commandButtonKind2, (Object)this.commandButtonKind);
        }
    }

    public CommandButtonPopupOrientationKind getPopupOrientationKind() {
        return this.popupOrientationKind;
    }

    public void setPopupOrientationKind(CommandButtonPopupOrientationKind commandButtonPopupOrientationKind) {
        CommandButtonPopupOrientationKind commandButtonPopupOrientationKind2 = this.popupOrientationKind;
        this.popupOrientationKind = commandButtonPopupOrientationKind;
        if (commandButtonPopupOrientationKind2 != this.popupOrientationKind) {
            this.firePropertyChange("popupOrientationKind", (Object)commandButtonPopupOrientationKind2, (Object)this.popupOrientationKind);
        }
    }

    @Override
    public void updateUI() {
        if (UIManager.get(this.getUIClassID()) != null) {
            this.setUI((CommandButtonUI)UIManager.getUI(this));
        } else {
            this.setUI(BasicCommandButtonUI.createUI(this));
        }
    }

    @Override
    public String getUIClassID() {
        return "CommandButtonUI";
    }

    public PopupPanelCallback getPopupCallback() {
        return this.popupCallback;
    }

    public void setPopupCallback(PopupPanelCallback popupPanelCallback) {
        this.popupCallback = popupPanelCallback;
    }

    public void setAutoRepeatAction(boolean bl) {
        this.isAutoRepeatAction = bl;
    }

    public void setAutoRepeatActionIntervals(int n, int n2) {
        this.autoRepeatInitialInterval = n;
        this.autoRepeatSubsequentInterval = n2;
    }

    public boolean isAutoRepeatAction() {
        return this.isAutoRepeatAction;
    }

    public int getAutoRepeatInitialInterval() {
        return this.autoRepeatInitialInterval;
    }

    public int getAutoRepeatSubsequentInterval() {
        return this.autoRepeatSubsequentInterval;
    }

    public void setFireActionOnRollover(boolean bl) {
        this.isFireActionOnRollover = bl;
    }

    public boolean isFireActionOnRollover() {
        return this.isFireActionOnRollover;
    }

    public PopupButtonModel getPopupModel() {
        return this.popupModel;
    }

    public void setPopupModel(PopupButtonModel popupButtonModel) {
        PopupButtonModel popupButtonModel2 = this.getPopupModel();
        if (popupButtonModel2 != null) {
            popupButtonModel2.removeChangeListener(this.popupHandler);
            popupButtonModel2.removeActionListener(this.popupHandler);
        }
        this.popupModel = popupButtonModel;
        if (popupButtonModel != null) {
            popupButtonModel.addChangeListener(this.popupHandler);
            popupButtonModel.addActionListener(this.popupHandler);
        }
        this.firePropertyChange("popupModel", popupButtonModel2, popupButtonModel);
        if (popupButtonModel != popupButtonModel2) {
            this.revalidate();
            this.repaint();
        }
    }

    @Override
    public void setEnabled(boolean bl) {
        if (!bl && this.popupModel.isRollover()) {
            this.popupModel.setRollover(false);
        }
        super.setEnabled(bl);
        this.popupModel.setEnabled(bl);
    }

    protected void firePopupActionPerformed(ActionEvent actionEvent) {
        Object[] arrobject = this.listenerList.getListenerList();
        ActionEvent actionEvent2 = null;
        for (int i = arrobject.length - 2; i >= 0; i -= 2) {
            if (arrobject[i] != PopupActionListener.class) continue;
            if (actionEvent2 == null) {
                String string = actionEvent.getActionCommand();
                actionEvent2 = new ActionEvent(this, 1001, string, actionEvent.getWhen(), actionEvent.getModifiers());
            }
            ((PopupActionListener)arrobject[i + 1]).actionPerformed(actionEvent2);
        }
    }

    @Override
    boolean hasRichTooltips() {
        return super.hasRichTooltips() || this.popupRichTooltip != null;
    }

    public void setPopupRichTooltip(RichTooltip richTooltip) {
        this.popupRichTooltip = richTooltip;
        RichToolTipManager richToolTipManager = RichToolTipManager.sharedInstance();
        if (this.hasRichTooltips()) {
            richToolTipManager.registerComponent(this);
        } else {
            richToolTipManager.unregisterComponent(this);
        }
    }

    @Override
    public RichTooltip getRichTooltip(MouseEvent mouseEvent) {
        CommandButtonUI commandButtonUI = this.getUI();
        if (commandButtonUI.getLayoutInfo().actionClickArea.contains(mouseEvent.getPoint())) {
            return super.getRichTooltip(mouseEvent);
        }
        if (commandButtonUI.getLayoutInfo().popupClickArea.contains(mouseEvent.getPoint())) {
            return this.popupRichTooltip;
        }
        return null;
    }

    public String getPopupKeyTip() {
        return this.popupKeyTip;
    }

    public void setPopupKeyTip(String string) {
        if (!this.canHaveBothKeyTips() && string != null && this.actionKeyTip != null) {
            throw new IllegalArgumentException("Action *and* popup keytips are not supported at the same time");
        }
        String string2 = this.popupKeyTip;
        this.popupKeyTip = string;
        this.firePropertyChange("popupKeyTip", string2, this.popupKeyTip);
    }

    @Override
    public void setActionKeyTip(String string) {
        if (!this.canHaveBothKeyTips() && this.popupKeyTip != null && this.actionKeyTip != null) {
            throw new IllegalArgumentException("Action *and* popup keytips are not supported at the same time");
        }
        super.setActionKeyTip(string);
    }

    boolean canHaveBothKeyTips() {
        return false;
    }

    public void doPopupClick() {
        Dimension dimension = this.getSize();
        PopupButtonModel popupButtonModel = this.getPopupModel();
        popupButtonModel.setArmed(true);
        popupButtonModel.setPressed(true);
        this.paintImmediately(new Rectangle(0, 0, dimension.width, dimension.height));
        try {
            Thread.sleep(100);
        }
        catch (InterruptedException var3_3) {
            // empty catch block
        }
        popupButtonModel.setPressed(false);
        popupButtonModel.setArmed(false);
        popupButtonModel.setPopupShowing(true);
        this.paintImmediately(new Rectangle(0, 0, dimension.width, dimension.height));
    }

    class PopupHandler
    implements PopupActionListener,
    ChangeListener {
        PopupHandler() {
        }

        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            JCommandButton.this.fireStateChanged();
            JCommandButton.this.repaint();
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            JCommandButton.this.firePopupActionPerformed(actionEvent);
        }
    }

    private static class DefaultPopupButtonModel
    extends DefaultButtonModel
    implements PopupButtonModel {
        protected Timer autoRepeatTimer;
        public static final int POPUP_SHOWING = 256;

        @Override
        public void addPopupActionListener(PopupActionListener popupActionListener) {
            this.listenerList.add(PopupActionListener.class, popupActionListener);
        }

        @Override
        public void removePopupActionListener(PopupActionListener popupActionListener) {
            this.listenerList.remove(PopupActionListener.class, popupActionListener);
        }

        protected void firePopupActionPerformed(ActionEvent actionEvent) {
            Object[] arrobject = this.listenerList.getListenerList();
            for (int i = arrobject.length - 2; i >= 0; i -= 2) {
                if (arrobject[i] != PopupActionListener.class) continue;
                ((PopupActionListener)arrobject[i + 1]).actionPerformed(actionEvent);
            }
        }

        @Override
        public void setPressed(boolean bl) {
            if (this.isPressed() == bl || !this.isEnabled()) {
                return;
            }
            this.stateMask = bl ? (this.stateMask |= 4) : (this.stateMask &= -5);
            if (this.isPressed() && this.isArmed()) {
                int n = 0;
                AWTEvent aWTEvent = EventQueue.getCurrentEvent();
                if (aWTEvent instanceof InputEvent) {
                    n = ((InputEvent)aWTEvent).getModifiers();
                } else if (aWTEvent instanceof ActionEvent) {
                    n = ((ActionEvent)aWTEvent).getModifiers();
                }
                this.firePopupActionPerformed(new ActionEvent(this, 1001, this.getActionCommand(), EventQueue.getMostRecentEventTime(), n));
            }
            this.fireStateChanged();
        }

        @Override
        public boolean isPopupShowing() {
            return (this.stateMask & 256) != 0;
        }

        @Override
        public void setPopupShowing(boolean bl) {
            if (this.isPopupShowing() == bl) {
                return;
            }
            this.stateMask = bl ? (this.stateMask |= 256) : (this.stateMask &= -257);
            this.fireStateChanged();
        }
    }

    public static enum CommandButtonPopupOrientationKind {
        DOWNWARD,
        SIDEWARD;
        

        private CommandButtonPopupOrientationKind() {
        }
    }

    public static enum CommandButtonKind {
        ACTION_ONLY(true, false),
        POPUP_ONLY(false, true),
        ACTION_AND_POPUP_MAIN_ACTION(true, true),
        ACTION_AND_POPUP_MAIN_POPUP(true, true);
        
        private boolean hasAction;
        private boolean hasPopup;

        private CommandButtonKind(boolean bl, boolean bl2) {
            this.hasAction = bl;
            this.hasPopup = bl2;
        }

        public boolean hasAction() {
            return this.hasAction;
        }

        public boolean hasPopup() {
            return this.hasPopup;
        }
    }

}

