/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common;

import java.util.EventListener;
import org.pushingpixels.flamingo.api.common.ProgressEvent;

public interface ProgressListener
extends EventListener {
    public void onProgress(ProgressEvent var1);
}

