/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common.model;

import java.awt.AWTEvent;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import javax.swing.JToggleButton;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;

public class ActionToggleButtonModel
extends JToggleButton.ToggleButtonModel
implements ActionButtonModel {
    protected boolean toFireActionOnPress;

    public ActionToggleButtonModel(boolean bl) {
        this.toFireActionOnPress = bl;
    }

    @Override
    public boolean isFireActionOnPress() {
        return this.toFireActionOnPress;
    }

    @Override
    public void setFireActionOnPress(boolean bl) {
        this.toFireActionOnPress = bl;
    }

    @Override
    public void setPressed(boolean bl) {
        if (this.isPressed() == bl || !this.isEnabled()) {
            return;
        }
        if (this.isArmed()) {
            if (!this.isFireActionOnPress()) {
                if (!bl) {
                    this.setSelected(!this.isSelected());
                }
            } else if (bl) {
                this.setSelected(!this.isSelected());
            }
        }
        this.stateMask = bl ? (this.stateMask |= 4) : (this.stateMask &= -5);
        this.fireStateChanged();
        boolean bl2 = false;
        if (this.isFireActionOnPress()) {
            bl2 = this.isPressed() && this.isArmed();
        } else {
            boolean bl3 = bl2 = !this.isPressed() && this.isArmed();
        }
        if (bl2) {
            int n = 0;
            AWTEvent aWTEvent = EventQueue.getCurrentEvent();
            if (aWTEvent instanceof InputEvent) {
                n = ((InputEvent)aWTEvent).getModifiers();
            } else if (aWTEvent instanceof ActionEvent) {
                n = ((ActionEvent)aWTEvent).getModifiers();
            }
            this.fireActionPerformed(new ActionEvent(this, 1001, this.getActionCommand(), EventQueue.getMostRecentEventTime(), n));
        }
    }
}

