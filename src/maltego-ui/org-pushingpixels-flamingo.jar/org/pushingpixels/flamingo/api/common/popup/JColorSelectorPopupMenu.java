/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common.popup;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JPanel;
import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.JCommandToggleMenuButton;
import org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu;
import org.pushingpixels.flamingo.internal.ui.common.popup.JColorSelectorComponent;
import org.pushingpixels.flamingo.internal.ui.common.popup.JColorSelectorPanel;

public class JColorSelectorPopupMenu
extends JCommandPopupMenu {
    private ColorSelectorCallback colorSelectorCallback;
    private JColorSelectorPanel lastColorSelectorPanel;
    private static LinkedList<Color> recentlySelected = new LinkedList();

    public JColorSelectorPopupMenu(ColorSelectorCallback colorSelectorCallback) {
        this.colorSelectorCallback = colorSelectorCallback;
    }

    public void addColorSectionWithDerived(String string, Color[] arrcolor) {
        if (arrcolor == null || arrcolor.length != 10) {
            throw new IllegalArgumentException("Must pass exactly 10 colors");
        }
        MultiRowSelector multiRowSelector = new MultiRowSelector(arrcolor);
        JColorSelectorPanel jColorSelectorPanel = new JColorSelectorPanel(string, multiRowSelector);
        this.addMenuPanel(jColorSelectorPanel);
        this.lastColorSelectorPanel = jColorSelectorPanel;
    }

    public void addColorSection(String string, Color[] arrcolor) {
        if (arrcolor == null || arrcolor.length != 10) {
            throw new IllegalArgumentException("Must pass exactly 10 colors");
        }
        SingleRowSelector singleRowSelector = new SingleRowSelector(arrcolor);
        JColorSelectorPanel jColorSelectorPanel = new JColorSelectorPanel(string, singleRowSelector);
        this.addMenuPanel(jColorSelectorPanel);
        this.lastColorSelectorPanel = jColorSelectorPanel;
    }

    public void addRecentSection(String string) {
        SingleRowSelector singleRowSelector = new SingleRowSelector(recentlySelected.toArray(new Color[0]));
        JColorSelectorPanel jColorSelectorPanel = new JColorSelectorPanel(string, singleRowSelector);
        jColorSelectorPanel.setLastPanel(true);
        this.addMenuPanel(jColorSelectorPanel);
        this.lastColorSelectorPanel = jColorSelectorPanel;
    }

    @Override
    public void addMenuButton(JCommandMenuButton jCommandMenuButton) {
        super.addMenuButton(jCommandMenuButton);
        this.updateLastColorSelectorPanel();
    }

    @Override
    public void addMenuButton(JCommandToggleMenuButton jCommandToggleMenuButton) {
        super.addMenuButton(jCommandToggleMenuButton);
        this.updateLastColorSelectorPanel();
    }

    @Override
    public void addMenuSeparator() {
        super.addMenuSeparator();
        this.updateLastColorSelectorPanel();
    }

    private void updateLastColorSelectorPanel() {
        if (this.lastColorSelectorPanel != null) {
            this.lastColorSelectorPanel.setLastPanel(true);
            this.lastColorSelectorPanel = null;
        }
    }

    public ColorSelectorCallback getColorSelectorCallback() {
        return this.colorSelectorCallback;
    }

    private static void wireToLRU(JColorSelectorComponent jColorSelectorComponent) {
        jColorSelectorComponent.addColorSelectorCallback(new ColorSelectorCallback(){

            @Override
            public void onColorSelected(Color color) {
                JColorSelectorPopupMenu.addColorToRecentlyUsed(color);
            }

            @Override
            public void onColorRollover(Color color) {
            }
        });
    }

    public static synchronized List<Color> getRecentlyUsedColors() {
        return Collections.unmodifiableList(recentlySelected);
    }

    public static synchronized void addColorToRecentlyUsed(Color color) {
        if (recentlySelected.contains(color)) {
            recentlySelected.remove(color);
            recentlySelected.addLast(color);
            return;
        }
        if (recentlySelected.size() == 10) {
            recentlySelected.removeFirst();
        }
        recentlySelected.addLast(color);
    }

    private class MultiRowSelector
    extends JPanel {
        static final int SECONDARY_ROWS = 5;

        public /* varargs */ MultiRowSelector(Color ... arrcolor) {
            JColorSelectorComponent[][] arrjColorSelectorComponent = new JColorSelectorComponent[arrcolor.length][6];
            for (int i = 0; i < arrcolor.length; ++i) {
                Color color = arrcolor[i];
                arrjColorSelectorComponent[i][0] = new JColorSelectorComponent(color, JColorSelectorPopupMenu.this.colorSelectorCallback);
                JColorSelectorPopupMenu.wireToLRU(arrjColorSelectorComponent[i][0]);
                this.add(arrjColorSelectorComponent[i][0]);
                float[] arrf = new float[3];
                Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), arrf);
                for (int j = 1; j <= 5; ++j) {
                    float f = (float)(j - 1) / 5.0f;
                    f = (float)Math.pow(f, 1.399999976158142);
                    float f2 = 1.0f - f;
                    if (arrf[1] == 0.0f) {
                        float f3 = 0.5f + 0.5f * arrf[2];
                        f2 = f3 * (float)(5 - j + 1) / 5.0f;
                    }
                    Color color2 = new Color(Color.HSBtoRGB(arrf[0], arrf[1] * (float)(j + 1) / 6.0f, f2));
                    arrjColorSelectorComponent[i][j] = new JColorSelectorComponent(color2, JColorSelectorPopupMenu.this.colorSelectorCallback);
                    arrjColorSelectorComponent[i][j].setTopOpen(j > 1);
                    arrjColorSelectorComponent[i][j].setBottomOpen(j < 5);
                    JColorSelectorPopupMenu.wireToLRU(arrjColorSelectorComponent[i][j]);
                    this.add(arrjColorSelectorComponent[i][j]);
                }
            }
            this.setLayout(new LayoutManager(JColorSelectorPopupMenu.this, arrcolor, arrjColorSelectorComponent){
                final /* synthetic */ JColorSelectorPopupMenu val$this$0;
                final /* synthetic */ Color[] val$colors;
                final /* synthetic */ JColorSelectorComponent[][] val$comps;

                @Override
                public void addLayoutComponent(String string, Component component) {
                }

                @Override
                public void removeLayoutComponent(Component component) {
                }

                @Override
                public Dimension minimumLayoutSize(Container container) {
                    return new Dimension(10, 10);
                }

                @Override
                public Dimension preferredLayoutSize(Container container) {
                    int n = this.getGap();
                    int n2 = this.getSize();
                    return new Dimension(this.val$colors.length * n2 + (this.val$colors.length + 1) * n, n + n2 + n + 5 * n2 + n);
                }

                @Override
                public void layoutContainer(Container container) {
                    int n = this.getGap();
                    int n2 = this.getSize();
                    if (container.getComponentOrientation().isLeftToRight()) {
                        int n3 = n;
                        for (int i = 0; i <= 5; ++i) {
                            int n4 = n;
                            for (int j = 0; j < this.val$colors.length; ++j) {
                                this.val$comps[j][i].setBounds(n4, n3, n2, n2);
                                n4 += n2 + n;
                            }
                            n3 += n2;
                            if (i != 0) continue;
                            n3 += n;
                        }
                    } else {
                        int n5 = n;
                        for (int i = 0; i <= 5; ++i) {
                            int n6 = MultiRowSelector.this.getWidth() - n;
                            for (int j = 0; j < this.val$colors.length; ++j) {
                                this.val$comps[j][i].setBounds(n6 - n2, n5, n2, n2);
                                n6 -= n2 + n;
                            }
                            n5 += n2;
                            if (i != 0) continue;
                            n5 += n;
                        }
                    }
                }

                private int getGap() {
                    return 4;
                }

                private int getSize() {
                    return 13;
                }
            });
        }

    }

    private class SingleRowSelector
    extends JPanel {
        public /* varargs */ SingleRowSelector(Color ... arrcolor) {
            JColorSelectorComponent[] arrjColorSelectorComponent = new JColorSelectorComponent[arrcolor.length];
            for (int i = 0; i < arrcolor.length; ++i) {
                arrjColorSelectorComponent[i] = new JColorSelectorComponent(arrcolor[i], JColorSelectorPopupMenu.this.colorSelectorCallback);
                JColorSelectorPopupMenu.wireToLRU(arrjColorSelectorComponent[i]);
                this.add(arrjColorSelectorComponent[i]);
            }
            this.setLayout(new LayoutManager(JColorSelectorPopupMenu.this, arrcolor, arrjColorSelectorComponent){
                final /* synthetic */ JColorSelectorPopupMenu val$this$0;
                final /* synthetic */ Color[] val$colors;
                final /* synthetic */ JColorSelectorComponent[] val$comps;

                @Override
                public void addLayoutComponent(String string, Component component) {
                }

                @Override
                public void removeLayoutComponent(Component component) {
                }

                @Override
                public Dimension minimumLayoutSize(Container container) {
                    return new Dimension(10, 10);
                }

                @Override
                public Dimension preferredLayoutSize(Container container) {
                    int n = this.getGap();
                    int n2 = this.getSize();
                    return new Dimension(this.val$colors.length * n2 + (this.val$colors.length + 1) * n, n2 + 2 * n);
                }

                @Override
                public void layoutContainer(Container container) {
                    int n = this.getGap();
                    int n2 = this.getSize();
                    if (container.getComponentOrientation().isLeftToRight()) {
                        int n3 = n;
                        int n4 = n;
                        for (int i = 0; i < this.val$colors.length; ++i) {
                            this.val$comps[i].setBounds(n3, n4, n2, n2);
                            n3 += n2 + n;
                        }
                    } else {
                        int n5 = SingleRowSelector.this.getWidth() - n;
                        int n6 = n;
                        for (int i = 0; i < this.val$colors.length; ++i) {
                            this.val$comps[i].setBounds(n5 - n2, n6, n2, n2);
                            n5 -= n2 + n;
                        }
                    }
                }

                private int getGap() {
                    return 4;
                }

                private int getSize() {
                    return 13;
                }
            });
        }

    }

    public static interface ColorSelectorCallback {
        public void onColorRollover(Color var1);

        public void onColorSelected(Color var1);
    }

}

