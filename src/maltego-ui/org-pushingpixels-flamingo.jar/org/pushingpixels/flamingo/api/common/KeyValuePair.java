/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.flamingo.api.common;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class KeyValuePair<S, T> {
    protected S key;
    protected T value;
    protected Map<String, Object> propMap;

    public KeyValuePair(S s, T t) {
        this.key = s;
        this.value = t;
        this.propMap = new HashMap<String, Object>();
    }

    public T getValue() {
        return this.value;
    }

    public S getKey() {
        return this.key;
    }

    public Object get(String string) {
        return this.propMap.get(string);
    }

    public void set(String string, Object object) {
        this.propMap.put(string, object);
    }

    public Map<String, Object> getProps() {
        return Collections.unmodifiableMap(this.propMap);
    }
}

