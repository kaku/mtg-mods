/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityConverter
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.registry.converter.AllEntityConverter
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.entity.api.EntityConverter;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.manager.forms.AdvancedSettingsController;
import com.paterva.maltego.entity.manager.forms.CategoryFormController;
import com.paterva.maltego.entity.manager.forms.DisplayDescriptorEditController;
import com.paterva.maltego.entity.manager.forms.DisplayInfoFormController;
import com.paterva.maltego.entity.manager.forms.EntityInfoFormController;
import com.paterva.maltego.entity.manager.forms.EntityRegistryAction;
import com.paterva.maltego.entity.manager.forms.MainPropertyFormController;
import com.paterva.maltego.entity.registry.converter.AllEntityConverter;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.text.MessageFormat;
import org.openide.WizardDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Lookup;

public class NewEntityTypeAction
extends EntityRegistryAction {
    public static final String ICON_RESOURCE = "com/paterva/maltego/entity/manager/resources/AddEntity.png";
    private static final String NULL_CATEGORY = "My Entities";

    public String getName() {
        return "New Entity Type (Advanced)";
    }

    public void performAction() {
        StringBuffer stringBuffer = new StringBuffer();
        MaltegoEntitySpec maltegoEntitySpec = this.createEntity(stringBuffer);
        if (maltegoEntitySpec != null) {
            maltegoEntitySpec.setDefaultCategory(stringBuffer.toString());
            this.registry().put((TypeSpec)maltegoEntitySpec);
        }
    }

    protected String getWizardTitle() {
        return "New Entity Wizard (Advanced)";
    }

    private MaltegoEntitySpec createEntity(StringBuffer stringBuffer) {
        WizardDescriptor.Panel[] arrpanel = this.getPanels();
        WizardDescriptor wizardDescriptor = new WizardDescriptor(arrpanel);
        wizardDescriptor.putProperty("WizardPanel_helpDisplayed", (Object)Boolean.FALSE);
        wizardDescriptor.putProperty("entitySpec", (Object)this.getNewEntitySpec());
        wizardDescriptor.putProperty("editMode", (Object)Boolean.FALSE);
        wizardDescriptor.putProperty("entityRegistry", (Object)this.registry());
        wizardDescriptor.putProperty("categories", (Object)this.registry().allCategories());
        wizardDescriptor.putProperty("entityCategory", (Object)this.getDefaultCategory());
        wizardDescriptor.setTitleFormat(new MessageFormat("New Entity - {0}"));
        wizardDescriptor.setTitle(this.getWizardTitle());
        if (WizardUtilities.runWizard((WizardDescriptor)wizardDescriptor)) {
            String string = (String)wizardDescriptor.getProperty("entityCategory");
            if (StringUtilities.isNullOrEmpty((String)string)) {
                string = "My Entities";
            }
            stringBuffer.append(string);
            return (MaltegoEntitySpec)wizardDescriptor.getProperty("entitySpec");
        }
        return null;
    }

    protected WizardDescriptor.Panel[] getPanels() {
        WizardDescriptor.Panel[] arrpanel = this.getDefaultCategory() == null ? new WizardDescriptor.Panel[]{new EntityInfoFormController(), new MainPropertyFormController(), new CategoryFormController(), new DisplayDescriptorEditController(), new DisplayInfoFormController(false), new AdvancedSettingsController()} : new WizardDescriptor.Panel[]{new EntityInfoFormController(), new MainPropertyFormController(), new DisplayDescriptorEditController(), new DisplayInfoFormController(false), new AdvancedSettingsController()};
        return this.initPanels(arrpanel);
    }

    private MaltegoEntitySpec getNewEntitySpec() {
        MaltegoEntitySpec maltegoEntitySpec = null;
        try {
            FileObject fileObject = FileUtil.getConfigRoot().getFileObject("Templates/Entity/EntityTemplate.entity");
            DataObject dataObject = DataObject.find((FileObject)fileObject);
            maltegoEntitySpec = (MaltegoEntitySpec)dataObject.getLookup().lookup(MaltegoEntitySpec.class);
        }
        catch (DataObjectNotFoundException var2_3) {
        }
        catch (IOException var2_4) {
            // empty catch block
        }
        if (maltegoEntitySpec == null) {
            maltegoEntitySpec = new MaltegoEntitySpec();
        }
        maltegoEntitySpec.setConverter((EntityConverter)AllEntityConverter.instance());
        return maltegoEntitySpec;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        this.performAction();
    }
}

