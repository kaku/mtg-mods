/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ExportAction
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.entity.manager.imex;

import com.paterva.maltego.entity.manager.imex.EntityExporter;
import com.paterva.maltego.importexport.ExportAction;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class ExportEntitiesAction
extends SystemAction {
    public String getName() {
        return "Export Entities";
    }

    protected String iconResource() {
        return "com/paterva/maltego/entity/manager/resources/ExportEntity.png";
    }

    public void perform() {
        InstanceContent instanceContent = new InstanceContent();
        instanceContent.add((Object)new EntityExporter());
        AbstractLookup abstractLookup = new AbstractLookup((AbstractLookup.Content)instanceContent);
        ExportAction exportAction = (ExportAction)SystemAction.get(ExportAction.class);
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("item_selection_description", "Select the items you which to export to a Maltego archive file.");
        hashMap.put("item_selection_icon", "com/paterva/maltego/entity/manager/resources/ExportEntity.png");
        hashMap.put("progress_description", "The summary of the progress to export entity items to a Maltego archive file is shown below.");
        hashMap.put("progress_icon", "com/paterva/maltego/entity/manager/resources/ExportEntity.png");
        hashMap.put("file_description", "Choose the name and location on your file system to save the Maltego archive file. The file can optionally be encrypted by selecting the \"Encrypt (AES-128)\" checkbox.");
        hashMap.put("file_icon", "com/paterva/maltego/entity/manager/resources/ExportEntity.png");
        hashMap.put("main_selection_description", "Choose whether to export all the entity items or a custom selection thereof.");
        hashMap.put("main_selection_icon", "com/paterva/maltego/entity/manager/resources/ExportEntity.png");
        hashMap.put("password_description", "Enter the password with which to encrypt the Maltego archive file.");
        hashMap.put("password_icon", "com/paterva/maltego/entity/manager/resources/ExportEntity.png");
        exportAction.perform((Lookup)abstractLookup, hashMap);
    }

    public void actionPerformed(ActionEvent actionEvent) {
        this.perform();
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }
}

