/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.editing.UnsupportedEditorException
 *  com.paterva.maltego.typing.editing.controls.ObjectEditor
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  com.paterva.maltego.util.ui.dialog.ChangeEventPropagator
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.editing.UnsupportedEditorException;
import com.paterva.maltego.typing.editing.controls.ObjectEditor;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import com.paterva.maltego.util.ui.dialog.ChangeEventPropagator;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.openide.util.NbBundle;

public class MainPropertyForm
extends JPanel {
    private final ChangeEventPropagator _changeSupport;
    private Color _descriptionFg;
    private JRadioButton _customPropertyRadioButton;
    private ObjectEditor _defaultValueControl;
    private JLabel _defaultValueHint;
    private JLabel _defaultValueLabel;
    private JTextField _description;
    private JTextField _displayName;
    private JTextField _id;
    private JRadioButton _inheritPropertyRadioButton;
    private JPanel _mainPanel;
    private ObjectEditor _sampleValueControl;
    private JLabel _sampleValueLabel;
    private JComboBox _types;
    private ButtonGroup buttonGroup1;
    private JLabel jLabel1;
    private JLabel jLabel10;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JLabel jLabel8;
    private JLabel jLabel9;
    private JPanel jPanel1;
    private JPanel jPanel2;

    public MainPropertyForm() {
        this._changeSupport = new ChangeEventPropagator((Object)this);
        this._descriptionFg = UIManager.getLookAndFeelDefaults().getColor("7-description-foreground");
        this.initComponents();
        this.setName("Main Property");
        this._description.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        this._id.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        this._displayName.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        this._types.addActionListener((ActionListener)this._changeSupport);
        this._sampleValueControl.addChangeListener((ChangeListener)this._changeSupport);
        this._sampleValueControl.addActionListener((ActionListener)this._changeSupport);
        this._defaultValueControl.addChangeListener((ChangeListener)this._changeSupport);
        this._defaultValueControl.addActionListener((ActionListener)this._changeSupport);
        this._types.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MainPropertyForm.this.updateValueControls();
            }
        });
        this._inheritPropertyRadioButton.addChangeListener((ChangeListener)this._changeSupport);
        this._customPropertyRadioButton.addChangeListener((ChangeListener)this._changeSupport);
        ChangeListener changeListener = new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                MainPropertyForm.this.updateCustomPropertyControls();
            }
        };
        this._inheritPropertyRadioButton.addChangeListener(changeListener);
        this._customPropertyRadioButton.addChangeListener(changeListener);
        this._defaultValueLabel.setVisible(false);
        this._defaultValueControl.setVisible(false);
        this._defaultValueHint.setVisible(false);
    }

    public void addDisplayNameFocusListener(FocusListener focusListener) {
        this._displayName.addFocusListener(focusListener);
    }

    public void removeDisplayNameFocusListener(FocusListener focusListener) {
        this._displayName.removeFocusListener(focusListener);
    }

    private void updateValueControls() {
        TypeDescriptor typeDescriptor = this.getDataType();
        if (typeDescriptor != null) {
            try {
                this._sampleValueControl.setDataType(typeDescriptor.getType());
                this._defaultValueControl.setDataType(typeDescriptor.getType());
            }
            catch (UnsupportedEditorException var2_2) {
                // empty catch block
            }
        }
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    private void initComponents() {
        this.buttonGroup1 = new ButtonGroup();
        this.jPanel2 = new JPanel();
        this._inheritPropertyRadioButton = new JRadioButton();
        this._customPropertyRadioButton = new JRadioButton();
        this._mainPanel = new JPanel();
        this.jLabel2 = new LabelWithBackground();
        this._description = new JTextField();
        this.jLabel1 = new LabelWithBackground();
        this._displayName = new JTextField();
        this._id = new JTextField();
        this.jLabel4 = new LabelWithBackground();
        this.jLabel6 = new JLabel();
        this.jLabel7 = new JLabel();
        this.jLabel8 = new JLabel();
        this.jLabel3 = new LabelWithBackground();
        this.jLabel9 = new JLabel();
        this._types = new JComboBox();
        this._sampleValueControl = new ObjectEditor();
        this.jLabel10 = new JLabel();
        this._sampleValueLabel = new LabelWithBackground();
        this._defaultValueHint = new JLabel();
        this._defaultValueControl = new ObjectEditor();
        this._defaultValueLabel = new LabelWithBackground();
        this.jPanel1 = new JPanel();
        this.setMinimumSize(new Dimension(561, 470));
        this.setName("");
        this.setLayout(new GridBagLayout());
        this.jPanel2.setLayout(new GridBagLayout());
        this.buttonGroup1.add(this._inheritPropertyRadioButton);
        this._inheritPropertyRadioButton.setText(NbBundle.getMessage(MainPropertyForm.class, (String)"MainPropertyForm._inheritPropertyRadioButton.text"));
        this._inheritPropertyRadioButton.setNextFocusableComponent(this._customPropertyRadioButton);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 6, 0, 6);
        this.jPanel2.add((Component)this._inheritPropertyRadioButton, gridBagConstraints);
        this.buttonGroup1.add(this._customPropertyRadioButton);
        this._customPropertyRadioButton.setText(NbBundle.getMessage(MainPropertyForm.class, (String)"MainPropertyForm._customPropertyRadioButton.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 10, 6);
        this.jPanel2.add((Component)this._customPropertyRadioButton, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        this.add((Component)this.jPanel2, gridBagConstraints);
        this._mainPanel.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(MainPropertyForm.class, (String)"MainPropertyForm._mainPanel.border.title")));
        this._mainPanel.setLayout(new GridBagLayout());
        this.jLabel2.setText(NbBundle.getMessage(MainPropertyForm.class, (String)"MainPropertyForm.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 12;
        gridBagConstraints.insets = new Insets(6, 6, 0, 0);
        this._mainPanel.add((Component)this.jLabel2, gridBagConstraints);
        this._description.setText(NbBundle.getMessage(MainPropertyForm.class, (String)"MainPropertyForm._description.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 0, 0, 6);
        this._mainPanel.add((Component)this._description, gridBagConstraints);
        this.jLabel1.setText(NbBundle.getMessage(MainPropertyForm.class, (String)"MainPropertyForm.jLabel1.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 12;
        gridBagConstraints.insets = new Insets(6, 6, 0, 0);
        this._mainPanel.add((Component)this.jLabel1, gridBagConstraints);
        this._displayName.setText(NbBundle.getMessage(MainPropertyForm.class, (String)"MainPropertyForm._displayName.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 0, 0, 6);
        this._mainPanel.add((Component)this._displayName, gridBagConstraints);
        this._id.setText(NbBundle.getMessage(MainPropertyForm.class, (String)"MainPropertyForm._id.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 0, 0, 6);
        this._mainPanel.add((Component)this._id, gridBagConstraints);
        this.jLabel4.setText(NbBundle.getMessage(MainPropertyForm.class, (String)"MainPropertyForm.jLabel4.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 12;
        gridBagConstraints.insets = new Insets(6, 6, 0, 0);
        this._mainPanel.add((Component)this.jLabel4, gridBagConstraints);
        this.jLabel6.setForeground(this._descriptionFg);
        this.jLabel6.setText(NbBundle.getMessage(MainPropertyForm.class, (String)"MainPropertyForm.jLabel6.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 6, 6, 6);
        this._mainPanel.add((Component)this.jLabel6, gridBagConstraints);
        this.jLabel7.setForeground(this._descriptionFg);
        this.jLabel7.setText(NbBundle.getMessage(MainPropertyForm.class, (String)"MainPropertyForm.jLabel7.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 6, 6, 6);
        this._mainPanel.add((Component)this.jLabel7, gridBagConstraints);
        this.jLabel8.setForeground(this._descriptionFg);
        this.jLabel8.setText(NbBundle.getMessage(MainPropertyForm.class, (String)"MainPropertyForm.jLabel8.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 6, 6, 6);
        this._mainPanel.add((Component)this.jLabel8, gridBagConstraints);
        this.jLabel3.setText(NbBundle.getMessage(MainPropertyForm.class, (String)"MainPropertyForm.jLabel3.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 12;
        gridBagConstraints.insets = new Insets(6, 6, 0, 0);
        this._mainPanel.add((Component)this.jLabel3, gridBagConstraints);
        this.jLabel9.setForeground(this._descriptionFg);
        this.jLabel9.setText(NbBundle.getMessage(MainPropertyForm.class, (String)"MainPropertyForm.jLabel9.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 6, 6, 6);
        this._mainPanel.add((Component)this.jLabel9, gridBagConstraints);
        this._types.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 0, 0, 6);
        this._mainPanel.add((Component)this._types, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 0, 0, 6);
        this._mainPanel.add((Component)this._sampleValueControl, gridBagConstraints);
        this.jLabel10.setForeground(this._descriptionFg);
        this.jLabel10.setText(NbBundle.getMessage(MainPropertyForm.class, (String)"MainPropertyForm.jLabel10.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 6, 6, 6);
        this._mainPanel.add((Component)this.jLabel10, gridBagConstraints);
        this._sampleValueLabel.setText(NbBundle.getMessage(MainPropertyForm.class, (String)"MainPropertyForm._sampleValueLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 12;
        gridBagConstraints.insets = new Insets(6, 6, 0, 0);
        this._mainPanel.add((Component)this._sampleValueLabel, gridBagConstraints);
        this._defaultValueHint.setForeground(this._descriptionFg);
        this._defaultValueHint.setText(NbBundle.getMessage(MainPropertyForm.class, (String)"MainPropertyForm._defaultValueHint.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 6, 6, 6);
        this._mainPanel.add((Component)this._defaultValueHint, gridBagConstraints);
        this._defaultValueControl.setNextFocusableComponent((Component)this._inheritPropertyRadioButton);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipady = 24;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(6, 0, 0, 6);
        this._mainPanel.add((Component)this._defaultValueControl, gridBagConstraints);
        this._defaultValueLabel.setText(NbBundle.getMessage(MainPropertyForm.class, (String)"MainPropertyForm._defaultValueLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 12;
        gridBagConstraints.insets = new Insets(6, 6, 0, 0);
        this._mainPanel.add((Component)this._defaultValueLabel, gridBagConstraints);
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 436, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 35, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this._mainPanel.add((Component)this.jPanel1, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this._mainPanel, gridBagConstraints);
    }

    public void setDataTypes(TypeDescriptor[] arrtypeDescriptor) {
        this._types.setModel(new DefaultComboBoxModel<TypeDescriptor>((E[])arrtypeDescriptor));
        this._types.setSelectedItem(null);
    }

    public void setDataType(TypeDescriptor typeDescriptor) {
        this._types.setSelectedItem((Object)typeDescriptor);
    }

    public TypeDescriptor getDataType() {
        return (TypeDescriptor)this._types.getSelectedItem();
    }

    public String getDisplayName() {
        return this._displayName.getText();
    }

    public String getUniqueName() {
        return this._id.getText();
    }

    public String getDescription() {
        return this._description.getText();
    }

    public void setDisplayName(String string) {
        this._displayName.setText(string);
        this._displayName.requestFocusInWindow();
    }

    public void setUniqueName(String string) {
        this._id.setText(string);
    }

    public void setDescription(String string) {
        this._description.setText(string);
    }

    public void setDefaultValue(Object object) {
        this._defaultValueControl.setValue(object);
    }

    public Object getDefaultValue() {
        return this._defaultValueControl.getValue();
    }

    public boolean isDefaultValueEmpty() {
        return this._defaultValueControl.isEmpty();
    }

    public void setSampleValue(Object object) {
        this._sampleValueControl.setValue(object);
    }

    public Object getSampleValue() {
        return this._sampleValueControl.getValue();
    }

    public boolean isSampleValueEmpty() {
        return this._sampleValueControl.isEmpty();
    }

    public void setCreateCustomProperty(boolean bl) {
        if (bl) {
            this._customPropertyRadioButton.setSelected(true);
        } else {
            this._inheritPropertyRadioButton.setSelected(true);
        }
        this.updateCustomPropertyControls();
    }

    public boolean isCreateCustomProperty() {
        return this._customPropertyRadioButton.isSelected();
    }

    public void setInheritPropertyEnabled(boolean bl) {
        this._inheritPropertyRadioButton.setEnabled(bl);
        if (bl) {
            this._customPropertyRadioButton.setSelected(true);
        }
    }

    private void updateCustomPropertyControls() {
        boolean bl = this._customPropertyRadioButton.isSelected();
        this._displayName.setEnabled(bl);
        this._description.setEnabled(bl);
        this._id.setEnabled(bl);
        this._types.setEnabled(bl);
        this._sampleValueControl.setEnabled(bl);
        this._defaultValueControl.setEnabled(bl);
    }

}

