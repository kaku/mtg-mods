/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.Evaluator
 *  com.paterva.maltego.typing.Evaluator$Replacement
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.entity.manager.actions;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.Evaluator;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.openide.util.Exceptions;

public class UrlReplaceEvaluator {
    private static final Pattern _valuePattern = Pattern.compile("\\$value\\(\\)", 2);
    private static final Pattern _encodePattern = Pattern.compile("(\\$urlencode\\((.*?)\\))", 2);
    private Evaluator.Replacement _delegate = new Evaluator.Replacement();

    public String evaluate(String string, DataSource dataSource, PropertyDescriptor propertyDescriptor) {
        String string2 = this.substituteValue(string, dataSource, propertyDescriptor);
        string2 = (String)this._delegate.evaluate((Object)string2, (Object)null, dataSource);
        string2 = this.encode(string2);
        return string2;
    }

    private String substituteValue(String string, DataSource dataSource, PropertyDescriptor propertyDescriptor) {
        Object object;
        String string2 = "";
        if (propertyDescriptor != null && (object = dataSource.getValue(propertyDescriptor)) != null) {
            string2 = TypeRegistry.getDefault().getType(propertyDescriptor.getType()).convert(object);
        }
        return _valuePattern.matcher(string).replaceAll(string2);
    }

    private String encode(String string) {
        String string2;
        int n = 0;
        Matcher matcher = _encodePattern.matcher(string);
        StringBuilder stringBuilder = new StringBuilder();
        while (matcher.find()) {
            string2 = string.substring(n, matcher.start());
            stringBuilder.append(string2);
            try {
                stringBuilder.append(URLEncoder.encode(matcher.group(2), "UTF-8"));
            }
            catch (UnsupportedEncodingException var6_6) {
                Exceptions.printStackTrace((Throwable)var6_6);
            }
            n = matcher.end();
        }
        string2 = string.substring(n, string.length());
        stringBuilder.append(string2);
        return stringBuilder.toString();
    }
}

