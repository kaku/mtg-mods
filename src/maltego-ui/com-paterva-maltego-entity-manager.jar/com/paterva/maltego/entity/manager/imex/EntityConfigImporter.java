/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules$EntityRule
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.registry.DefaultEntityRegistry
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigImporter
 *  org.openide.filesystems.FileObject
 */
package com.paterva.maltego.entity.manager.imex;

import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.manager.imex.EntityConfig;
import com.paterva.maltego.entity.manager.imex.EntityExistInfo;
import com.paterva.maltego.entity.manager.imex.EntityImporter;
import com.paterva.maltego.entity.manager.imex.SelectableCategory;
import com.paterva.maltego.entity.manager.imex.SelectableEntitySpec;
import com.paterva.maltego.entity.registry.DefaultEntityRegistry;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigImporter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import org.openide.filesystems.FileObject;

public class EntityConfigImporter
extends ConfigImporter {
    private final String DEFAULT_FOLDER = "Miscellaneous";

    public Config loadConfig(MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        EntityImporter entityImporter = new EntityImporter();
        List<String> list = entityImporter.readCategories(maltegoArchiveReader);
        List<MaltegoEntitySpec> list2 = entityImporter.readEntities(maltegoArchiveReader);
        return this.createConfig(list, list2);
    }

    public Config loadPreviousConfig(FileObject fileObject) throws IOException {
        DefaultEntityRegistry defaultEntityRegistry = new DefaultEntityRegistry(fileObject);
        List<String> list = Arrays.asList(defaultEntityRegistry.allCategories());
        ArrayList<MaltegoEntitySpec> arrayList = new ArrayList<MaltegoEntitySpec>(defaultEntityRegistry.getAll());
        return this.createConfig(list, arrayList);
    }

    private Config createConfig(List<String> list, List<MaltegoEntitySpec> list2) {
        EntityExistInfo entityExistInfo = new EntityExistInfo();
        ArrayList<SelectableCategory> arrayList = new ArrayList<SelectableCategory>();
        HashSet<String> hashSet = new HashSet<String>(list);
        for (MaltegoEntitySpec object2 : list2) {
            hashSet.add(object2.getDefaultCategory());
        }
        for (String string : hashSet) {
            ArrayList<SelectableEntitySpec> arrayList2 = new ArrayList<SelectableEntitySpec>();
            boolean bl = false;
            for (MaltegoEntitySpec maltegoEntitySpec : list2) {
                if ((string == null || !string.equals(maltegoEntitySpec.getDefaultCategory())) && (string != null || maltegoEntitySpec.getDefaultCategory() != null)) continue;
                boolean bl2 = !entityExistInfo.exist(maltegoEntitySpec);
                arrayList2.add(new SelectableEntitySpec(maltegoEntitySpec, bl2));
                bl = bl || bl2;
            }
            bl = arrayList2.isEmpty() ? true : bl;
            arrayList.add(new SelectableCategory(arrayList2, string != null ? string : "Miscellaneous", bl));
        }
        if (arrayList.size() > 0) {
            return new EntityConfig(arrayList);
        }
        return null;
    }

    public int applyConfig(Config config) {
        EntityConfig entityConfig = (EntityConfig)config;
        List<MaltegoEntitySpec> list = entityConfig.getSelectedEntities();
        EntityImporter entityImporter = new EntityImporter();
        entityImporter.applyEntities(list, DiscoveryMergingRules.EntityRule.REPLACE);
        return list.size();
    }
}

