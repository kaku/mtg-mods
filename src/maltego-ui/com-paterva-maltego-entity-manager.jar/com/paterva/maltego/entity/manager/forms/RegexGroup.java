/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DisplayDescriptor
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.typing.DisplayDescriptor;

public class RegexGroup {
    private DisplayDescriptor _property;

    public RegexGroup(DisplayDescriptor displayDescriptor) {
        this._property = displayDescriptor;
    }

    public DisplayDescriptor getProperty() {
        return this._property;
    }

    public void setProperty(DisplayDescriptor displayDescriptor) {
        this._property = displayDescriptor;
    }
}

