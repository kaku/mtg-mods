/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.customicons.imex.IconConfigImporter
 *  com.paterva.maltego.importexport.ImportAction
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.entity.manager.imex;

import com.paterva.maltego.customicons.imex.IconConfigImporter;
import com.paterva.maltego.entity.manager.imex.EntityConfigImporter;
import com.paterva.maltego.importexport.ImportAction;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public final class ImportEntitiesAction
extends SystemAction {
    public String getName() {
        return "Import Entities";
    }

    protected String iconResource() {
        return "com/paterva/maltego/entity/manager/resources/ImportEntity.png";
    }

    public void perform() {
        InstanceContent instanceContent = new InstanceContent();
        instanceContent.add((Object)new EntityConfigImporter());
        instanceContent.add((Object)new IconConfigImporter());
        AbstractLookup abstractLookup = new AbstractLookup((AbstractLookup.Content)instanceContent);
        ImportAction importAction = (ImportAction)SystemAction.get(ImportAction.class);
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("item_selection_description", "Select the items you which to import from the Maltego archive file.");
        hashMap.put("item_selection_icon", "com/paterva/maltego/entity/manager/resources/ImportEntity.png");
        hashMap.put("progress_description", "The summary of the progress to import entity and icon items from a Maltego archive file is shown below.");
        hashMap.put("progress_icon", "com/paterva/maltego/entity/manager/resources/ImportEntity.png");
        hashMap.put("file_description", "Choose the Maltego archive file (containing entity and icon items) to import from your file system.");
        hashMap.put("file_icon", "com/paterva/maltego/entity/manager/resources/ImportEntity.png");
        importAction.perform((Lookup)abstractLookup, hashMap);
    }

    public void actionPerformed(ActionEvent actionEvent) {
        this.perform();
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }
}

