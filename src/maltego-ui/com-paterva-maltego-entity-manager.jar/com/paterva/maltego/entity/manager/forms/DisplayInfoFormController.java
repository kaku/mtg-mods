/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.util.ui.dialog.DataEditController
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$FinishablePanel
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.manager.forms.DisplayInfoForm;
import com.paterva.maltego.entity.manager.forms.PropertyTypes;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.util.ui.dialog.DataEditController;
import java.awt.Component;
import java.util.List;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;

public class DisplayInfoFormController
extends DataEditController<DisplayInfoForm, MaltegoEntitySpec>
implements WizardDescriptor.FinishablePanel {
    private boolean _displayDescriptionInPanel = false;

    public DisplayInfoFormController(boolean bl) {
        super("entitySpec");
        this.setName("Display Settings");
        this.setDescription("Select which properties to display in the graph view.");
        this._displayDescriptionInPanel = bl;
    }

    protected String getFirstError(DisplayInfoForm displayInfoForm) {
        return null;
    }

    protected DisplayInfoForm createComponent() {
        DisplayInfoForm displayInfoForm = new DisplayInfoForm(this._displayDescriptionInPanel);
        displayInfoForm.addChangeListener(this.changeListener());
        return displayInfoForm;
    }

    protected void setData(DisplayInfoForm displayInfoForm, MaltegoEntitySpec maltegoEntitySpec) {
        boolean bl = this.isInheritingSpec(maltegoEntitySpec);
        DisplayDescriptor[] arrdisplayDescriptor = null;
        DisplayDescriptor displayDescriptor = new DisplayDescriptor(DisplayInfoFormController.class, "<default>", "<Get from inherited type>");
        DisplayDescriptor displayDescriptor2 = new DisplayDescriptor(DisplayInfoFormController.class, "<default>", "<Use entity type icon>");
        arrdisplayDescriptor = PropertyTypes.getValueProperties((TypeSpec)maltegoEntitySpec);
        if (bl) {
            arrdisplayDescriptor = this.prependDummyProperty(arrdisplayDescriptor, displayDescriptor);
        }
        displayInfoForm.setValueProperties(arrdisplayDescriptor);
        if (maltegoEntitySpec.getValueProperty() != null) {
            displayInfoForm.setValueProperty(maltegoEntitySpec.getValueProperty());
        } else {
            displayInfoForm.setValueProperty(arrdisplayDescriptor[0]);
        }
        arrdisplayDescriptor = PropertyTypes.getDisplayValueProperties((TypeSpec)maltegoEntitySpec);
        if (bl) {
            arrdisplayDescriptor = this.prependDummyProperty(arrdisplayDescriptor, displayDescriptor);
        }
        displayInfoForm.setDisplayValueProperties(arrdisplayDescriptor);
        if (maltegoEntitySpec.getDisplayValueProperty() != null) {
            displayInfoForm.setDisplayValueProperty(maltegoEntitySpec.getDisplayValueProperty());
        } else {
            displayInfoForm.setDisplayValueProperty(arrdisplayDescriptor[0]);
        }
        arrdisplayDescriptor = PropertyTypes.getImageProperties((TypeSpec)maltegoEntitySpec);
        arrdisplayDescriptor = this.prependDummyProperty(arrdisplayDescriptor, displayDescriptor2);
        displayInfoForm.setImageProperties(arrdisplayDescriptor);
        if (maltegoEntitySpec.getImageProperty() == null) {
            displayInfoForm.setImageProperty(arrdisplayDescriptor[0]);
        } else {
            displayInfoForm.setImageProperty(maltegoEntitySpec.getImageProperty());
        }
    }

    private boolean isInheritingSpec(MaltegoEntitySpec maltegoEntitySpec) {
        List list = maltegoEntitySpec.getBaseEntitySpecs();
        return list != null && list.size() > 0 && (list.size() > 1 || !((String)list.get(0)).equals("maltego.Unknown"));
    }

    private DisplayDescriptor[] prependDummyProperty(DisplayDescriptor[] arrdisplayDescriptor, DisplayDescriptor displayDescriptor) {
        DisplayDescriptor[] arrdisplayDescriptor2 = new DisplayDescriptor[arrdisplayDescriptor.length + 1];
        System.arraycopy(arrdisplayDescriptor, 0, arrdisplayDescriptor2, 1, arrdisplayDescriptor.length);
        arrdisplayDescriptor2[0] = displayDescriptor;
        return arrdisplayDescriptor2;
    }

    protected void updateData(DisplayInfoForm displayInfoForm, MaltegoEntitySpec maltegoEntitySpec) {
        DisplayDescriptor displayDescriptor = displayInfoForm.getValueProperty();
        if (displayDescriptor.getType() == DisplayInfoFormController.class) {
            maltegoEntitySpec.setValueProperty(null);
        } else {
            maltegoEntitySpec.setValueProperty(displayDescriptor);
        }
        DisplayDescriptor displayDescriptor2 = displayInfoForm.getDisplayValueProperty();
        if (displayDescriptor2.getType() == DisplayInfoFormController.class) {
            maltegoEntitySpec.setDisplayValueProperty(null);
        } else {
            maltegoEntitySpec.setDisplayValueProperty(displayDescriptor2);
        }
        DisplayDescriptor displayDescriptor3 = displayInfoForm.getImageProperty();
        if (displayDescriptor3.getType() == DisplayInfoFormController.class) {
            maltegoEntitySpec.setImageProperty(null);
        } else {
            maltegoEntitySpec.setImageProperty(displayDescriptor3);
        }
    }

    public boolean isFinishPanel() {
        return true;
    }
}

