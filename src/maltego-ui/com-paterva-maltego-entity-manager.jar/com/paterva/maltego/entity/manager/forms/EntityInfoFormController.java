/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.typing.TypeNameValidator
 *  com.paterva.maltego.util.ui.dialog.DataEditController
 *  org.openide.WizardDescriptor
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.manager.forms.EntityInfoForm;
import com.paterva.maltego.typing.TypeNameValidator;
import com.paterva.maltego.util.ui.dialog.DataEditController;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;
import org.openide.util.ImageUtilities;

public class EntityInfoFormController
extends DataEditController<EntityInfoForm, MaltegoEntitySpec> {
    private static final String PROP_FULLNAME = "maltego.registeredto.fullname";
    private static final String PROP_NAMESPACE = "maltego.registeredto.namespace";

    public EntityInfoFormController() {
        super("entitySpec");
        this.setName("Basic Information");
        this.setDescription("Enter the details for your new entity below.");
        this.setImage(ImageUtilities.loadImage((String)"com/paterva/maltego/entity/manager/resources/AddEntity.png".replace(".png", "48.png")));
    }

    protected String getFirstError(EntityInfoForm entityInfoForm) {
        Object object;
        String string = entityInfoForm.getDisplayName();
        if (string.trim().length() == 0) {
            return "Display name is required";
        }
        String string2 = entityInfoForm.getID();
        if ((string2 = string2.trim()).length() == 0) {
            return "Unique type name is required";
        }
        String string3 = TypeNameValidator.checkName((String)string2);
        if (string3 != null) {
            return string3;
        }
        if (!this.isEditMode() && (object = (EntityRegistry)this.getDescriptor().getProperty("entityRegistry")).contains(string2)) {
            return "The type '" + string2 + "' is already registered";
        }
        if (entityInfoForm.hasBaseEntity() && ((object = entityInfoForm.getBaseEntity()) == null || object.trim().isEmpty())) {
            return "Base entity not specified";
        }
        if (entityInfoForm.getLargeIcon() == null && entityInfoForm.getLargeIconResource() == null) {
            return "At least a 48x48 icon is required";
        }
        return null;
    }

    protected EntityInfoForm createComponent() {
        final EntityInfoForm entityInfoForm = new EntityInfoForm();
        entityInfoForm.addChangeListener(this.changeListener());
        entityInfoForm.addDisplayNameFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent focusEvent) {
            }

            @Override
            public void focusLost(FocusEvent focusEvent) {
                String string;
                if (!EntityInfoFormController.this.isEditMode() && (string = entityInfoForm.getID()).trim().length() == 0) {
                    entityInfoForm.setID(EntityInfoFormController.this.createID(entityInfoForm.getDisplayName()));
                }
            }
        });
        return entityInfoForm;
    }

    private String createID(String string) {
        String string2 = System.getProperty("maltego.registeredto.namespace", null);
        string2 = string2 != null ? string2.toLowerCase() : System.getProperty("maltego.registeredto.fullname", "yourorganization").toLowerCase();
        return (string2 + "." + string).replaceAll(" ", "");
    }

    protected boolean isEditMode() {
        return this.getDescriptor().getProperty("editMode") == Boolean.TRUE;
    }

    protected void setData(EntityInfoForm entityInfoForm, MaltegoEntitySpec maltegoEntitySpec) {
        if (this.isEditMode()) {
            entityInfoForm.setIDEnabled(false);
            if (maltegoEntitySpec.getLargeIconResource() != null) {
                entityInfoForm.setLargeIconResource(maltegoEntitySpec.getLargeIconResource());
                entityInfoForm.setSmallIconResource(maltegoEntitySpec.getSmallIconResource());
            } else {
                entityInfoForm.setLargeIcon(maltegoEntitySpec.getLargeIcon());
                entityInfoForm.setSmallIcon(maltegoEntitySpec.getSmallIcon());
            }
        } else {
            entityInfoForm.setIDEnabled(true);
        }
        entityInfoForm.setDisplayName(maltegoEntitySpec.getDisplayName());
        entityInfoForm.setDescription(maltegoEntitySpec.getDescription());
        entityInfoForm.setID(maltegoEntitySpec.getTypeName());
        entityInfoForm.setEntitySpecs(this.getEntitySpecNames());
        entityInfoForm.setBaseEntity(this.getBaseEntity(maltegoEntitySpec));
    }

    private List<String> getEntitySpecNames() {
        EntityRegistry entityRegistry = (EntityRegistry)this.getDescriptor().getProperty("entityRegistry");
        Collection collection = entityRegistry.getAllVisible();
        ArrayList<String> arrayList = new ArrayList<String>();
        for (MaltegoEntitySpec maltegoEntitySpec : collection) {
            arrayList.add(maltegoEntitySpec.getTypeName());
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    private String getBaseEntity(MaltegoEntitySpec maltegoEntitySpec) {
        ArrayList arrayList = new ArrayList(maltegoEntitySpec.getBaseEntitySpecs());
        arrayList.remove("maltego.Unknown");
        String string = null;
        if (!arrayList.isEmpty()) {
            string = (String)arrayList.get(0);
        }
        return string;
    }

    protected void updateData(EntityInfoForm entityInfoForm, MaltegoEntitySpec maltegoEntitySpec) {
        maltegoEntitySpec.setDisplayName(entityInfoForm.getDisplayName());
        maltegoEntitySpec.setDescription(entityInfoForm.getDescription());
        if (entityInfoForm.getLargeIconResource() != null) {
            maltegoEntitySpec.setLargeIconResource(entityInfoForm.getLargeIconResource());
            maltegoEntitySpec.setSmallIconResource(entityInfoForm.getSmallIconResource());
        } else {
            maltegoEntitySpec.setLargeIcon(entityInfoForm.getLargeIcon());
            maltegoEntitySpec.setSmallIcon(entityInfoForm.getSmallIcon());
        }
        if (!this.isEditMode()) {
            maltegoEntitySpec.setTypeName(entityInfoForm.getID().trim());
        }
        String string = "maltego.Unknown";
        if (entityInfoForm.hasBaseEntity()) {
            string = entityInfoForm.getBaseEntity();
        }
        maltegoEntitySpec.setBaseEntitySpecs(Arrays.asList(string));
    }

}

