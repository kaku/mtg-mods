/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.SpecAction
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.FileUtilities
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.entity.manager.actions;

import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.SpecAction;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.FileUtilities;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.openide.awt.HtmlBrowser;
import org.openide.util.Exceptions;

public class BrowseUrlsAction
extends SpecAction {
    public String getTypeName() {
        return "maltego.spec.action.type.browse-urls";
    }

    public boolean isEnabledFor(SpecRegistry specRegistry, MaltegoPart maltegoPart, String string) {
        return !this.generatePartURLs(maltegoPart).isEmpty();
    }

    public void perform(SpecRegistry specRegistry, MaltegoPart maltegoPart, String string) {
        List<URL> list = this.generatePartURLs(maltegoPart);
        for (URL uRL : list) {
            if (FileUtilities.isRemoteFileURL((URL)uRL)) continue;
            HtmlBrowser.URLDisplayer.getDefault().showURL(uRL);
        }
    }

    private List<URL> generatePartURLs(MaltegoPart maltegoPart) {
        ArrayList<URL> arrayList = new ArrayList<URL>();
        for (PropertyDescriptor propertyDescriptor : maltegoPart.getProperties()) {
            Object object;
            if (FastURL.class.equals((Object)propertyDescriptor.getType())) {
                object = (FastURL)maltegoPart.getValue(propertyDescriptor);
                if (object == null) continue;
                try {
                    arrayList.add(object.getURL());
                }
                catch (MalformedURLException var6_7) {
                    Exceptions.printStackTrace((Throwable)var6_7);
                }
                continue;
            }
            if (!String.class.equals((Object)propertyDescriptor.getType()) || (object = (String)maltegoPart.getValue(propertyDescriptor)) == null) continue;
            if (object.startsWith("www.")) {
                object = "http://" + (String)object;
            }
            try {
                URL uRL = new URL((String)object);
                arrayList.add(uRL);
            }
            catch (MalformedURLException var6_8) {}
        }
        return arrayList;
    }
}

