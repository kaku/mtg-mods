/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  com.paterva.maltego.util.ui.dialog.ChangeEventPropagator
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import com.paterva.maltego.util.ui.dialog.ChangeEventPropagator;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import javax.swing.ComboBoxEditor;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.openide.util.NbBundle;

public class CategoryForm
extends JPanel {
    private final ChangeEventPropagator _changeSupport;
    private JComboBox _items;
    private JLabel jLabel1;
    private JPanel jPanel1;

    public CategoryForm() {
        this._changeSupport = new ChangeEventPropagator((Object)this);
        this.initComponents();
        this.setName("Category");
        this._items.addActionListener((ActionListener)this._changeSupport);
        JTextField jTextField = (JTextField)this._items.getEditor().getEditorComponent();
        jTextField.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.jLabel1 = new LabelWithBackground();
        this._items = new JComboBox();
        this.setMinimumSize(new Dimension(540, 428));
        this.setPreferredSize(new Dimension(540, 428));
        this.setLayout(new GridBagLayout());
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.jPanel1, gridBagConstraints);
        this.jLabel1.setText(NbBundle.getMessage(CategoryForm.class, (String)"CategoryForm.jLabel1.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(15, 15, 0, 0);
        this.add((Component)this.jLabel1, gridBagConstraints);
        this._items.setEditable(true);
        this._items.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 170;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(15, 0, 0, 0);
        this.add((Component)this._items, gridBagConstraints);
    }

    public void setCategories(String[] arrstring) {
        this._items.setModel(new DefaultComboBoxModel<String>(arrstring));
    }

    public void setSelectedCategory(String string) {
        this._items.setSelectedItem(string);
    }

    public String getSelectedCategory() {
        JTextField jTextField = (JTextField)this._items.getEditor().getEditorComponent();
        if (!StringUtilities.isNullOrEmpty((String)jTextField.getText())) {
            return jTextField.getText();
        }
        return (String)this._items.getSelectedItem();
    }
}

