/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.descriptor.RegistryEvent
 *  com.paterva.maltego.typing.descriptor.RegistryListener
 *  com.paterva.maltego.typing.descriptor.TypeSpecDisplayNameComparator
 *  com.paterva.maltego.util.ui.table.RowTableModel
 *  org.openide.util.ImageUtilities
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.entity.manager;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.descriptor.RegistryEvent;
import com.paterva.maltego.typing.descriptor.RegistryListener;
import com.paterva.maltego.typing.descriptor.TypeSpecDisplayNameComparator;
import com.paterva.maltego.util.ui.table.RowTableModel;
import java.awt.Image;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.List;
import javax.swing.SwingUtilities;
import org.openide.util.ImageUtilities;
import org.openide.util.WeakListeners;

class EntityRegistryTableModel
extends RowTableModel<MaltegoEntitySpec> {
    public static final String[] Columns = new String[]{"", "Display name", "Description", "Type", "Value property", "Default value"};
    private final EntityRegistry _registry;
    private final RegistryListener _listener;

    public EntityRegistryTableModel(EntityRegistry entityRegistry) {
        super(Columns, EntityRegistryTableModel.getSortedSpecs(entityRegistry));
        this._registry = entityRegistry;
        this._listener = new RegistryListener(){

            public void typeAdded(RegistryEvent registryEvent) {
                EntityRegistryTableModel.this.refresh();
            }

            public void typeRemoved(RegistryEvent registryEvent) {
                EntityRegistryTableModel.this.refresh();
            }

            public void typeUpdated(RegistryEvent registryEvent) {
                EntityRegistryTableModel.this.refresh();
            }
        };
        this._registry.addRegistryListener((RegistryListener)WeakListeners.create(RegistryListener.class, (EventListener)this._listener, (Object)this._registry));
    }

    private static List<MaltegoEntitySpec> getSortedSpecs(EntityRegistry entityRegistry) {
        ArrayList<MaltegoEntitySpec> arrayList = new ArrayList<MaltegoEntitySpec>(entityRegistry.getAllVisible());
        Collections.sort(arrayList, new TypeSpecDisplayNameComparator());
        return arrayList;
    }

    public Class<?> getColumnClass(int n) {
        if (n == 0) {
            return Image.class;
        }
        return String.class;
    }

    public void refresh() {
        try {
            Runnable runnable = new Runnable(){

                @Override
                public void run() {
                    EntityRegistryTableModel.this.setRows(EntityRegistryTableModel.getSortedSpecs(EntityRegistryTableModel.this._registry));
                }
            };
            if (!SwingUtilities.isEventDispatchThread()) {
                SwingUtilities.invokeAndWait(runnable);
            } else {
                runnable.run();
            }
        }
        catch (InterruptedException var1_2) {
        }
        catch (InvocationTargetException var1_3) {
            // empty catch block
        }
    }

    public Object getValueFor(MaltegoEntitySpec maltegoEntitySpec, int n) {
        switch (n) {
            case 0: {
                Image image = maltegoEntitySpec.getIcon(16);
                if (image == null) {
                    image = ImageUtilities.loadImage((String)"com/paterva/maltego/entity/manager/resources/Entity.png");
                }
                return image;
            }
            case 1: {
                return maltegoEntitySpec.getDisplayName();
            }
            case 2: {
                return maltegoEntitySpec.getDescription();
            }
            case 3: {
                return maltegoEntitySpec.getTypeName();
            }
            case 4: {
                DisplayDescriptor displayDescriptor = maltegoEntitySpec.getValueProperty();
                return displayDescriptor == null ? "<none>" : displayDescriptor.getDisplayName();
            }
            case 5: {
                DisplayDescriptor displayDescriptor = maltegoEntitySpec.getValueProperty();
                return displayDescriptor == null ? "<none>" : displayDescriptor.getDefaultValue();
            }
        }
        return null;
    }

}

