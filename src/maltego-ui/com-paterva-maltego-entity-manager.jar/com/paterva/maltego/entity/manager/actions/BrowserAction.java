/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.SpecAction
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.util.FileUtilities
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.entity.manager.actions;

import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.SpecAction;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.entity.manager.actions.UrlReplaceEvaluator;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.util.FileUtilities;
import java.io.IOException;
import java.net.URL;
import org.openide.awt.HtmlBrowser;
import org.openide.util.Exceptions;

public class BrowserAction
extends SpecAction {
    public String getTypeName() {
        return "maltego.spec.action.type.browser";
    }

    public boolean isEnabledFor(SpecRegistry specRegistry, MaltegoPart maltegoPart, String string) {
        return true;
    }

    public void perform(SpecRegistry specRegistry, MaltegoPart maltegoPart, String string) {
        try {
            UrlReplaceEvaluator urlReplaceEvaluator = new UrlReplaceEvaluator();
            PropertyDescriptor propertyDescriptor = InheritanceHelper.getValueProperty((SpecRegistry)specRegistry, (TypedPropertyBag)maltegoPart, (boolean)true);
            string = urlReplaceEvaluator.evaluate(string, (DataSource)maltegoPart, propertyDescriptor);
            URL uRL = new URL(string);
            if (!FileUtilities.isRemoteFileURL((URL)uRL)) {
                HtmlBrowser.URLDisplayer.getDefault().showURL(uRL);
            }
        }
        catch (IOException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
    }
}

