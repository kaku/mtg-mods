/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.entity.manager.forms.NewDisplayDescriptorControl;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;

public class NewDisplayDescriptorController
extends ValidatingController<NewDisplayDescriptorControl> {
    public static final String PROP_NAME = "name";
    public static final String PROP_DISPLAY_NAME = "displayName";
    public static final String PROP_TYPE = "type";

    protected NewDisplayDescriptorControl createComponent() {
        NewDisplayDescriptorControl newDisplayDescriptorControl = new NewDisplayDescriptorControl((Object[])TypeRegistry.getDefault().getTypes());
        newDisplayDescriptorControl.addChangeListener(this.changeListener());
        return newDisplayDescriptorControl;
    }

    protected String getFirstError(NewDisplayDescriptorControl newDisplayDescriptorControl) {
        String string = newDisplayDescriptorControl.getPropertyName();
        String string2 = newDisplayDescriptorControl.getDisplayName();
        Object object = newDisplayDescriptorControl.getType();
        if (StringUtilities.isNullOrEmpty((String)string)) {
            return "Name is required";
        }
        if (StringUtilities.isNullOrEmpty((String)string2)) {
            return "Display name is required";
        }
        if (object == null) {
            return "Type is required";
        }
        return null;
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        ((NewDisplayDescriptorControl)this.component()).setPropertyName((String)wizardDescriptor.getProperty("name"));
        ((NewDisplayDescriptorControl)this.component()).setDisplayName((String)wizardDescriptor.getProperty("displayName"));
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        wizardDescriptor.putProperty("type", ((NewDisplayDescriptorControl)this.component()).getType());
        wizardDescriptor.putProperty("name", (Object)((NewDisplayDescriptorControl)this.component()).getPropertyName());
        wizardDescriptor.putProperty("displayName", (Object)((NewDisplayDescriptorControl)this.component()).getDisplayName());
    }
}

