/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.entity.serializer.EntityCategoryEntry
 *  com.paterva.entity.serializer.MaltegoEntityEntry
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveWriter
 *  com.paterva.maltego.customicons.imex.IconDescriptor
 *  com.paterva.maltego.customicons.imex.IconFileEntry
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.imgfactory.icons.IconLayerRegistry
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigExporter
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.IconSize
 *  org.openide.filesystems.FileObject
 */
package com.paterva.maltego.entity.manager.imex;

import com.paterva.entity.serializer.EntityCategoryEntry;
import com.paterva.entity.serializer.MaltegoEntityEntry;
import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.MaltegoArchiveWriter;
import com.paterva.maltego.customicons.imex.IconDescriptor;
import com.paterva.maltego.customicons.imex.IconFileEntry;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.manager.imex.EntityConfig;
import com.paterva.maltego.imgfactory.icons.IconLayerRegistry;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigExporter;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.IconSize;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.openide.filesystems.FileObject;

public class EntityExporter
extends ConfigExporter {
    public Config getCurrentConfig() {
        String[] arrstring = EntityRegistry.getDefault().allCategories();
        Collection collection = EntityRegistry.getDefault().getAllVisible();
        if (arrstring.length > 0) {
            return new EntityConfig(arrstring, collection);
        }
        return null;
    }

    public int saveConfig(MaltegoArchiveWriter maltegoArchiveWriter, Config config) throws IOException {
        String string;
        Iterator iterator;
        String string2;
        Object object;
        EntityConfig entityConfig = (EntityConfig)config;
        List<String> list = entityConfig.getSelectedCategories();
        ArrayList<String> arrayList = new ArrayList<String>(list.size());
        for (String object3 : list) {
            object = FileUtilities.createUniqueLegalFilename(arrayList, (String)object3);
            arrayList.add((String)object);
            maltegoArchiveWriter.write((Entry)new EntityCategoryEntry(object3, (String)object));
        }
        List<MaltegoEntitySpec> list2 = entityConfig.getSelectedEntities();
        Iterator iterator2 = list2.iterator();
        while (iterator2.hasNext()) {
            object = (MaltegoEntitySpec)iterator2.next();
            maltegoArchiveWriter.write((Entry)new MaltegoEntityEntry((MaltegoEntitySpec)object));
        }
        IconLayerRegistry iconLayerRegistry = IconLayerRegistry.getDefault();
        object = new HashSet();
        Object object2 = list2.iterator();
        while (object2.hasNext()) {
            iterator = (MaltegoEntitySpec)object2.next();
            object.add(iterator.getSmallIconResource());
            object.add(iterator.getLargeIconResource());
        }
        object2 = new ArrayList();
        iterator = object.iterator();
        while (iterator.hasNext()) {
            string2 = (String)iterator.next();
            if (string2 == null || (string = iconLayerRegistry.getCategory(string2)) == null) continue;
            for (IconSize iconSize : IconSize.values()) {
                object2.add(this.createDescriptor(iconLayerRegistry, string2, string, iconSize));
            }
        }
        iterator = object2.iterator();
        while (iterator.hasNext()) {
            string2 = (IconDescriptor)iterator.next();
            string = new IconFileEntry((IconDescriptor)string2);
            maltegoArchiveWriter.write((Entry)string);
        }
        return list2.size();
    }

    private IconDescriptor createDescriptor(IconLayerRegistry iconLayerRegistry, String string, String string2, IconSize iconSize) {
        FileObject fileObject = iconLayerRegistry.getFileObject(string, iconSize);
        return new IconDescriptor(fileObject, string2, string, iconSize);
    }
}

