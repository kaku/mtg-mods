/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityConverter
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.registry.converter.AllEntityConverter
 *  com.paterva.maltego.entity.registry.converter.RegexEntityConverter
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.util.ui.dialog.DataEditController
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.entity.api.EntityConverter;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.manager.forms.AdvancedSettingsControl;
import com.paterva.maltego.entity.registry.converter.AllEntityConverter;
import com.paterva.maltego.entity.registry.converter.RegexEntityConverter;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.util.ui.dialog.DataEditController;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

public class AdvancedSettingsController
extends DataEditController<AdvancedSettingsControl, MaltegoEntitySpec> {
    public AdvancedSettingsController() {
        super("entitySpec");
        this.setName("Advanced Settings");
        this.setDescription("Enter advanced settings for the entity. \"Text Conversion\" relates to how plaintext pasted onto a Malteog graph is mapped to an enity by the \"Regular expression\". The regular expression of entities are tested in order of ascending \"Conversion order\".");
    }

    protected AdvancedSettingsControl createComponent() {
        AdvancedSettingsControl advancedSettingsControl = new AdvancedSettingsControl();
        return advancedSettingsControl;
    }

    protected void setData(AdvancedSettingsControl advancedSettingsControl, MaltegoEntitySpec maltegoEntitySpec) {
        EntityConverter entityConverter = maltegoEntitySpec.getConverter();
        boolean bl = entityConverter instanceof RegexEntityConverter;
        int n = maltegoEntitySpec.getConversionOrder();
        advancedSettingsControl.setProperties(maltegoEntitySpec.getProperties());
        advancedSettingsControl.setRegex(bl ? entityConverter.toString() : "");
        advancedSettingsControl.setConversionOrder(n != Integer.MAX_VALUE ? n : 1000);
        List list = null;
        if (bl) {
            RegexEntityConverter regexEntityConverter = (RegexEntityConverter)entityConverter;
            list = regexEntityConverter.getProperties();
        }
        advancedSettingsControl.setRegexGroups(list != null ? list : new ArrayList());
        advancedSettingsControl.setIsRoot(maltegoEntitySpec.isToolboxItem());
        advancedSettingsControl.setPluralName(maltegoEntitySpec.getDisplayNamePlural());
    }

    protected void updateData(AdvancedSettingsControl advancedSettingsControl, MaltegoEntitySpec maltegoEntitySpec) {
        boolean bl = advancedSettingsControl.hasRegex();
        if (bl) {
            String string = advancedSettingsControl.getRegex();
            RegexEntityConverter regexEntityConverter = new RegexEntityConverter();
            regexEntityConverter.setMainProperty(maltegoEntitySpec.getValueProperty().getName());
            regexEntityConverter.setValue(string);
            regexEntityConverter.setProperties(advancedSettingsControl.getRegexGroups());
            maltegoEntitySpec.setConverter((EntityConverter)regexEntityConverter);
            maltegoEntitySpec.setConversionOrder(advancedSettingsControl.getConversionOrder());
            maltegoEntitySpec.getConverter().init(string);
        } else {
            maltegoEntitySpec.setConverter((EntityConverter)AllEntityConverter.instance());
        }
        maltegoEntitySpec.setToolboxItem(advancedSettingsControl.getIsRoot());
        maltegoEntitySpec.setDisplayNamePlural(advancedSettingsControl.getPluralName());
    }
}

