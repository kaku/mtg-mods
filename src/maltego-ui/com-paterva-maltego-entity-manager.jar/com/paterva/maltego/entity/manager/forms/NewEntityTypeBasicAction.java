/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.entity.manager.forms.CategoryFormController;
import com.paterva.maltego.entity.manager.forms.EntityInfoFormController;
import com.paterva.maltego.entity.manager.forms.MainPropertyFormController;
import com.paterva.maltego.entity.manager.forms.NewEntityTypeAction;
import org.openide.WizardDescriptor;

public final class NewEntityTypeBasicAction
extends NewEntityTypeAction {
    @Override
    public String getName() {
        return "New Entity Type";
    }

    @Override
    protected String getWizardTitle() {
        return "New Entity Wizard (Basic)";
    }

    @Override
    protected WizardDescriptor.Panel[] getPanels() {
        WizardDescriptor.Panel[] arrpanel = this.getDefaultCategory() == null ? new WizardDescriptor.Panel[]{new EntityInfoFormController(), new MainPropertyFormController(), new CategoryFormController()} : new WizardDescriptor.Panel[]{new EntityInfoFormController(), new MainPropertyFormController()};
        return this.initPanels(arrpanel);
    }
}

