/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 */
package com.paterva.maltego.entity.manager.imex;

import com.paterva.maltego.entity.api.MaltegoEntitySpec;

public class SelectableEntitySpec {
    private MaltegoEntitySpec _entitySpec;
    private boolean _selected;

    public SelectableEntitySpec(MaltegoEntitySpec maltegoEntitySpec, boolean bl) {
        this._entitySpec = maltegoEntitySpec;
        this._selected = bl;
    }

    public MaltegoEntitySpec getEntitySpec() {
        return this._entitySpec;
    }

    public boolean isSelected() {
        return this._selected;
    }

    public void setSelected(boolean bl) {
        this._selected = bl;
    }
}

