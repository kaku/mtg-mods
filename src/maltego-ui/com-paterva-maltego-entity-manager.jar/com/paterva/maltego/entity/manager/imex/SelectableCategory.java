/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.entity.manager.imex;

import com.paterva.maltego.entity.manager.imex.SelectableEntitySpec;
import java.util.ArrayList;
import java.util.Collection;

public class SelectableCategory
extends ArrayList<SelectableEntitySpec> {
    private String _name;
    private boolean _selected;

    public SelectableCategory(Collection<SelectableEntitySpec> collection, String string, boolean bl) {
        super(collection);
        this._name = string;
        this._selected = bl;
    }

    public String getName() {
        return this._name;
    }

    public boolean isSelected() {
        return this._selected;
    }

    public void setSelected(boolean bl) {
        this._selected = bl;
    }
}

