/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.palette.PaletteActions
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.entity.manager.palette;

import com.paterva.maltego.entity.manager.ShowEnitityManagerAction;
import com.paterva.maltego.entity.manager.forms.NewEntityTypeBasicAction;
import com.paterva.maltego.entity.manager.imex.ExportEntitiesAction;
import com.paterva.maltego.entity.manager.imex.ImportEntitiesAction;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.netbeans.spi.palette.PaletteActions;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;

class EntityPaletteActions
extends PaletteActions {
    EntityPaletteActions() {
    }

    public Action[] getImportActions() {
        return new Action[]{SystemAction.get(ShowEnitityManagerAction.class), SystemAction.get(ImportEntitiesAction.class), SystemAction.get(ExportEntitiesAction.class)};
    }

    public Action[] getCustomPaletteActions() {
        NewEntityTypeBasicAction newEntityTypeBasicAction = (NewEntityTypeBasicAction)SystemAction.get(NewEntityTypeBasicAction.class);
        return new Action[]{newEntityTypeBasicAction};
    }

    public Action[] getCustomCategoryActions(Lookup lookup) {
        Node node = (Node)lookup.lookup(Node.class);
        NewEntityTypeBasicAction newEntityTypeBasicAction = (NewEntityTypeBasicAction)SystemAction.get(NewEntityTypeBasicAction.class);
        newEntityTypeBasicAction.setDefaultCategory(node.getDisplayName());
        return new Action[]{newEntityTypeBasicAction};
    }

    public Action[] getCustomItemActions(Lookup lookup) {
        return new Action[0];
    }

    public Action getPreferredAction(Lookup lookup) {
        return null;
    }

    public Action getResetAction() {
        return new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
            }
        };
    }

}

