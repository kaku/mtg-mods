/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectExistsException
 *  org.openide.loaders.MultiDataObject
 *  org.openide.loaders.MultiDataObject$Entry
 *  org.openide.loaders.MultiFileLoader
 *  org.openide.nodes.CookieSet
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.DataEditorSupport
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 *  org.openide.util.lookup.ProxyLookup
 */
package com.paterva.maltego.entity.manager.data;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.manager.data.CookieToSpecConverter;
import com.paterva.maltego.entity.manager.data.EntityDataNode;
import com.paterva.maltego.entity.manager.data.EntitySpecCookie;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import java.io.IOException;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;
import org.openide.text.DataEditorSupport;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ProxyLookup;

public class EntityDataObject
extends MultiDataObject
implements EntitySpecCookie {
    private MaltegoEntitySpec _spec;
    private Lookup _lookup;

    public EntityDataObject(FileObject fileObject, MultiFileLoader multiFileLoader) throws DataObjectExistsException, IOException {
        super(fileObject, multiFileLoader);
        CookieSet cookieSet = this.getCookieSet();
        cookieSet.add((Node.Cookie)DataEditorSupport.create((DataObject)this, (MultiDataObject.Entry)this.getPrimaryEntry(), (CookieSet)cookieSet));
        InstanceContent instanceContent = new InstanceContent();
        instanceContent.add((Object)this, (InstanceContent.Convertor)CookieToSpecConverter.instance());
        this._lookup = new ProxyLookup(new Lookup[]{this.getCookieSet().getLookup(), new AbstractLookup((AbstractLookup.Content)instanceContent)});
    }

    protected Node createNodeDelegate() {
        return new EntityDataNode((DataObject)this, this.getLookup());
    }

    public Lookup getLookup() {
        return this._lookup != null ? this._lookup : Lookup.EMPTY;
    }

    @Override
    public MaltegoEntitySpec getEntitySpec() {
        if (this._spec == null) {
            String string = this.getPrimaryFile().getName();
            this._spec = (MaltegoEntitySpec)EntityRegistry.getDefault().get(string);
        }
        return this._spec;
    }
}

