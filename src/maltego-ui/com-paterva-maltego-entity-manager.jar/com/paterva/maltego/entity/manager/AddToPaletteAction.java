/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.NodeAction
 */
package com.paterva.maltego.entity.manager;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import java.util.ArrayList;
import java.util.Collection;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.NodeAction;

public class AddToPaletteAction
extends NodeAction {
    protected void performAction(Node[] arrnode) {
        this.getNewDescriptors(this.getActivatedNodes());
    }

    protected boolean enable(Node[] arrnode) {
        Node[] arrnode2 = this.getActivatedNodes();
        if (arrnode2.length > 0) {
            return this.getNewDescriptors(arrnode2).size() > 0;
        }
        return false;
    }

    protected void addToRegistry(Collection<MaltegoEntitySpec> collection) {
        EntityRegistry entityRegistry = EntityRegistry.getDefault();
        for (MaltegoEntitySpec maltegoEntitySpec : collection) {
            entityRegistry.put((TypeSpec)maltegoEntitySpec);
        }
    }

    protected Collection<MaltegoEntitySpec> getNewDescriptors(Node[] arrnode) {
        EntityRegistry entityRegistry = EntityRegistry.getDefault();
        ArrayList<MaltegoEntitySpec> arrayList = new ArrayList<MaltegoEntitySpec>();
        for (Node node : arrnode) {
            MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)node.getLookup().lookup(MaltegoEntitySpec.class);
            if (maltegoEntitySpec == null || entityRegistry.contains(maltegoEntitySpec.getTypeName())) continue;
            arrayList.add(maltegoEntitySpec);
        }
        return arrayList;
    }

    public String getName() {
        return "Add to palette";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }
}

