/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.util.ColorUtilities
 *  org.openide.loaders.DataNode
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Lookup
 *  org.openide.util.datatransfer.PasteType
 */
package com.paterva.maltego.entity.manager.data;

import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.manager.data.EntitySpecCookie;
import com.paterva.maltego.util.ColorUtilities;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.datatransfer.Transferable;
import java.io.PrintStream;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.datatransfer.PasteType;

public class EntityDataNode
extends DataNode {
    public EntityDataNode(DataObject dataObject, Lookup lookup) {
        super(dataObject, Children.LEAF, lookup);
    }

    public String getDisplayName() {
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        String string = ColorUtilities.encode((Color)uIDefaults.getColor("palette-item-name-fg"));
        String string2 = ColorUtilities.encode((Color)uIDefaults.getColor("palette-item-description-fg"));
        int n = new JLabel().getFont().getSize();
        String string3 = "<html><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"font-size:" + ((double)n - 2.5) + "px;\">&nbsp;<font color=\"" + string + "\"><b>" + this.getEntitySpec().getDisplayName() + "</b></font></td></tr><tr><td style=\"font-size:" + ((double)n - 2.5) + "px; color:" + string2 + "\">&nbsp;" + this.getEntitySpec().getDescription() + "</font></td></tr></table></html>";
        return string3;
    }

    public String getHtmlDisplayName() {
        return this.getEntitySpec().getDisplayName();
    }

    public String getShortDescription() {
        return this.getEntitySpec().getDescription();
    }

    public String getName() {
        return this.getEntitySpec().getTypeName();
    }

    public Image getIcon(int n) {
        int n2 = 48;
        if (n == 1 || n == 3) {
            n2 = 16;
        } else if (n == 2 || n == 4) {
            n2 = 32;
        }
        Image image = this.getEntitySpec().getIcon(n2);
        return image != null ? image : super.getIcon(n);
    }

    protected void createPasteTypes(Transferable transferable, List<PasteType> list) {
        super.createPasteTypes(transferable, list);
        System.out.println("create pasteTypes " + transferable);
    }

    public PasteType getDropType(Transferable transferable, int n, int n2) {
        System.out.println("getDropType " + transferable);
        return super.getDropType(transferable, n, n2);
    }

    private MaltegoEntitySpec getEntitySpec() {
        return ((EntitySpecCookie)this.getDataObject().getCookie(EntitySpecCookie.class)).getEntitySpec();
    }
}

