/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntitySpecSerializer
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.util.XmlSerializationException
 */
package com.paterva.maltego.entity.manager;

import com.paterva.maltego.entity.api.EntitySpecSerializer;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.util.XmlSerializationException;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

public class EntitySpecTransferable
implements Transferable {
    public static final DataFlavor DataFlavour = new DataFlavor(MaltegoEntitySpec.class, "Entity Specification");
    private static final DataFlavor[] SupportedFlavors = new DataFlavor[]{DataFlavour, DataFlavor.stringFlavor};
    private MaltegoEntitySpec _data;

    public EntitySpecTransferable(MaltegoEntitySpec maltegoEntitySpec) {
        this._data = maltegoEntitySpec;
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        return SupportedFlavors;
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor dataFlavor) {
        return dataFlavor.equals(DataFlavour) || dataFlavor.equals(DataFlavor.stringFlavor);
    }

    @Override
    public Object getTransferData(DataFlavor dataFlavor) throws UnsupportedFlavorException, IOException {
        if (dataFlavor.equals(DataFlavour)) {
            return this._data;
        }
        if (dataFlavor.equals(DataFlavor.stringFlavor)) {
            return EntitySpecTransferable.toString(this._data);
        }
        throw new UnsupportedFlavorException(dataFlavor);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    private static String toString(MaltegoEntitySpec maltegoEntitySpec) {
        EntitySpecSerializer entitySpecSerializer = EntitySpecSerializer.getDefault();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            entitySpecSerializer.write(maltegoEntitySpec, (OutputStream)byteArrayOutputStream);
            String string = byteArrayOutputStream.toString("UTF-8");
            return string;
        }
        catch (UnsupportedEncodingException var3_4) {
            String string = "";
            return string;
        }
        catch (XmlSerializationException var3_5) {
            try {
                String string = "";
                return string;
            }
            catch (Throwable var6_11) {}
            throw var6_11;
            finally {
                try {
                    byteArrayOutputStream.close();
                }
                catch (IOException var5_9) {}
            }
        }
    }
}

