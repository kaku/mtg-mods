/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 */
package com.paterva.maltego.entity.manager.data;

import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.manager.data.EntitySpecCookie;
import org.openide.util.lookup.InstanceContent;

class CookieToSpecConverter
implements InstanceContent.Convertor<EntitySpecCookie, MaltegoEntitySpec> {
    private static CookieToSpecConverter _instance;

    private CookieToSpecConverter() {
    }

    public static CookieToSpecConverter instance() {
        if (_instance == null) {
            _instance = new CookieToSpecConverter();
        }
        return _instance;
    }

    public MaltegoEntitySpec convert(EntitySpecCookie entitySpecCookie) {
        return entitySpecCookie.getEntitySpec();
    }

    public Class<? extends MaltegoEntitySpec> type(EntitySpecCookie entitySpecCookie) {
        return MaltegoEntitySpec.class;
    }

    public String id(EntitySpecCookie entitySpecCookie) {
        return entitySpecCookie.toString();
    }

    public String displayName(EntitySpecCookie entitySpecCookie) {
        return "EntitySpecCookie";
    }
}

