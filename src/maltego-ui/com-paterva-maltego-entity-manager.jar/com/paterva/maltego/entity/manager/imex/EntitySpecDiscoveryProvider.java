/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryContext
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryEntityMerger
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules$EntityRule
 *  com.paterva.maltego.archive.mtz.discover.MtzDiscoveryItems
 *  com.paterva.maltego.archive.mtz.discover.MtzDiscoveryProvider
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 */
package com.paterva.maltego.entity.manager.imex;

import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.archive.mtz.discover.DiscoveryContext;
import com.paterva.maltego.archive.mtz.discover.DiscoveryEntityMerger;
import com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules;
import com.paterva.maltego.archive.mtz.discover.MtzDiscoveryItems;
import com.paterva.maltego.archive.mtz.discover.MtzDiscoveryProvider;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.manager.imex.EntityImporter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class EntitySpecDiscoveryProvider
extends MtzDiscoveryProvider<MtzEntitySpecs> {
    public MtzEntitySpecs read(DiscoveryContext discoveryContext, MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        EntityImporter entityImporter = new EntityImporter();
        return new MtzEntitySpecs(entityImporter.readEntities(maltegoArchiveReader), discoveryContext);
    }

    public void apply(MtzEntitySpecs mtzEntitySpecs) {
        DiscoveryMergingRules.EntityRule entityRule = mtzEntitySpecs.getContext().getMergingRules().getEntityRule();
        EntityImporter entityImporter = new EntityImporter();
        entityImporter.applyEntities(mtzEntitySpecs.getEntitySpecs(), entityRule);
    }

    public MtzEntitySpecs getNewAndMerged(MtzEntitySpecs mtzEntitySpecs) {
        List<MaltegoEntitySpec> list = mtzEntitySpecs.getEntitySpecs();
        DiscoveryMergingRules.EntityRule entityRule = mtzEntitySpecs.getContext().getMergingRules().getEntityRule();
        EntityRegistry entityRegistry = EntityRegistry.getDefault();
        ArrayList<MaltegoEntitySpec> arrayList = new ArrayList<MaltegoEntitySpec>();
        for (MaltegoEntitySpec maltegoEntitySpec : list) {
            MaltegoEntitySpec maltegoEntitySpec2 = DiscoveryEntityMerger.getNewEntity((EntityRegistry)entityRegistry, (DiscoveryMergingRules.EntityRule)entityRule, (MaltegoEntitySpec)maltegoEntitySpec);
            if (maltegoEntitySpec2 == null) continue;
            arrayList.add(maltegoEntitySpec);
        }
        return new MtzEntitySpecs(arrayList, mtzEntitySpecs.getContext());
    }

    public class MtzEntitySpecs
    extends MtzDiscoveryItems {
        private final List<MaltegoEntitySpec> _entitySpecs;

        public MtzEntitySpecs(List<MaltegoEntitySpec> list, DiscoveryContext discoveryContext) {
            super((MtzDiscoveryProvider)EntitySpecDiscoveryProvider.this, discoveryContext);
            this._entitySpecs = list;
        }

        public String getDescription() {
            return "Entities";
        }

        public List<MaltegoEntitySpec> getEntitySpecs() {
            return this._entitySpecs;
        }

        public int size() {
            return this._entitySpecs.size();
        }
    }

}

