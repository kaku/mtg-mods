/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.entity.manager.forms;

class Constants {
    public static final String ENTITY_SPEC = "entitySpec";
    public static final String IS_EDIT_MODE = "editMode";
    public static final String ENTITY_REGISTRY = "entityRegistry";
    public static final String ENTITY_CATEGORY = "entityCategory";
    public static final String CATEGORIES = "categories";
    public static final String INHERIT_MAIN_PROPERTY = "inheritMainProperty";
    public static final String MAIN_PROPERTY = "mainProperty";

    Constants() {
    }
}

