/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  com.paterva.maltego.util.ui.dialog.ChangeEventPropagator
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.util.ui.components.LabelWithBackground;
import com.paterva.maltego.util.ui.dialog.ChangeEventPropagator;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.openide.util.NbBundle;

public class NewDisplayDescriptorControl
extends JPanel {
    private ChangeEventPropagator _changeSupport;
    private JTextField _displayName;
    private JTextField _name;
    private JComboBox _type;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JPanel jPanel1;

    public NewDisplayDescriptorControl(Object[] arrobject) {
        this._changeSupport = new ChangeEventPropagator((Object)this);
        this.initComponents();
        this._type.setModel(new DefaultComboBoxModel<Object>(arrobject));
        this._displayName.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        this._name.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        this._type.addActionListener((ActionListener)this._changeSupport);
    }

    public NewDisplayDescriptorControl() {
        this(new Object[0]);
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this._changeSupport.removeChangeListener(changeListener);
    }

    private void initComponents() {
        this.jLabel1 = new LabelWithBackground();
        this._name = new JTextField();
        this.jLabel2 = new LabelWithBackground();
        this.jLabel3 = new LabelWithBackground();
        this._displayName = new JTextField();
        this._type = new JComboBox();
        this.jPanel1 = new JPanel();
        this.setLayout(new GridBagLayout());
        this.jLabel1.setText(NbBundle.getMessage(NewDisplayDescriptorControl.class, (String)"NewDisplayDescriptorControl.jLabel1.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 6, 6, 0);
        this.add((Component)this.jLabel1, gridBagConstraints);
        this._name.setText(NbBundle.getMessage(NewDisplayDescriptorControl.class, (String)"NewDisplayDescriptorControl._name.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 164;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 0, 6, 10);
        this.add((Component)this._name, gridBagConstraints);
        this.jLabel2.setText(NbBundle.getMessage(NewDisplayDescriptorControl.class, (String)"NewDisplayDescriptorControl.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 6, 0);
        this.add((Component)this.jLabel2, gridBagConstraints);
        this.jLabel3.setText(NbBundle.getMessage(NewDisplayDescriptorControl.class, (String)"NewDisplayDescriptorControl.jLabel3.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 6, 0);
        this.add((Component)this.jLabel3, gridBagConstraints);
        this._displayName.setText(NbBundle.getMessage(NewDisplayDescriptorControl.class, (String)"NewDisplayDescriptorControl._displayName.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 164;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 0, 6, 10);
        this.add((Component)this._displayName, gridBagConstraints);
        this._type.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 114;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 0, 6, 10);
        this.add((Component)this._type, gridBagConstraints);
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.jPanel1, gridBagConstraints);
    }

    public String getPropertyName() {
        return this._name.getText();
    }

    public void setPropertyName(String string) {
        this._name.setText(string);
    }

    public String getDisplayName() {
        return this._displayName.getText();
    }

    public void setDisplayName(String string) {
        this._displayName.setText(string);
    }

    public Object getType() {
        return this._type.getSelectedItem();
    }

    public void setType(Object object) {
        this._type.setSelectedItem(object);
    }
}

