/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.importexport.Config
 *  org.openide.nodes.Node
 */
package com.paterva.maltego.entity.manager.imex;

import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.manager.imex.EntityConfigNode;
import com.paterva.maltego.entity.manager.imex.EntityExistInfo;
import com.paterva.maltego.entity.manager.imex.SelectableCategory;
import com.paterva.maltego.entity.manager.imex.SelectableEntitySpec;
import com.paterva.maltego.importexport.Config;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.openide.nodes.Node;

class EntityConfig
implements Config {
    private ArrayList<SelectableCategory> _categories;

    public EntityConfig(String[] arrstring, Collection<MaltegoEntitySpec> collection) {
        ArrayList<SelectableCategory> arrayList = new ArrayList<SelectableCategory>(arrstring.length);
        for (String string : arrstring) {
            ArrayList<SelectableEntitySpec> arrayList2 = new ArrayList<SelectableEntitySpec>();
            for (MaltegoEntitySpec maltegoEntitySpec : collection) {
                if (!string.equals(maltegoEntitySpec.getDefaultCategory())) continue;
                SelectableEntitySpec selectableEntitySpec = new SelectableEntitySpec(maltegoEntitySpec, true);
                arrayList2.add(selectableEntitySpec);
            }
            arrayList.add(new SelectableCategory(arrayList2, string, true));
        }
        this._categories = arrayList;
    }

    public EntityConfig(ArrayList<SelectableCategory> arrayList) {
        this._categories = arrayList;
    }

    public ArrayList<SelectableCategory> getSelectables() {
        return this._categories;
    }

    public List<SelectableEntitySpec> getSelectableEntities() {
        ArrayList<SelectableEntitySpec> arrayList = new ArrayList<SelectableEntitySpec>();
        for (SelectableCategory selectableCategory : this._categories) {
            arrayList.addAll(selectableCategory);
        }
        return arrayList;
    }

    public List<String> getSelectedCategories() {
        ArrayList<String> arrayList = new ArrayList<String>();
        for (SelectableCategory selectableCategory : this._categories) {
            if (!selectableCategory.isSelected()) continue;
            arrayList.add(selectableCategory.getName());
        }
        return arrayList;
    }

    public List<MaltegoEntitySpec> getSelectedEntities() {
        ArrayList<MaltegoEntitySpec> arrayList = new ArrayList<MaltegoEntitySpec>();
        for (SelectableCategory selectableCategory : this._categories) {
            if (!selectableCategory.isSelected()) continue;
            for (SelectableEntitySpec selectableEntitySpec : selectableCategory) {
                if (!selectableEntitySpec.isSelected()) continue;
                arrayList.add(selectableEntitySpec.getEntitySpec());
            }
        }
        return arrayList;
    }

    public Node getConfigNode(boolean bl) {
        EntityExistInfo entityExistInfo = bl ? new EntityExistInfo() : null;
        return new EntityConfigNode(this, entityExistInfo);
    }

    public int getPriority() {
        return 20;
    }
}

