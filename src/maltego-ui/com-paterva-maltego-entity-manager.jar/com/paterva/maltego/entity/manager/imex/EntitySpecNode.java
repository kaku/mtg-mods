/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.importexport.ConfigNode
 *  org.openide.nodes.Children
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.entity.manager.imex;

import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.manager.imex.EntityExistInfo;
import com.paterva.maltego.entity.manager.imex.SelectableEntitySpec;
import com.paterva.maltego.importexport.ConfigNode;
import java.awt.Image;
import org.openide.nodes.Children;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class EntitySpecNode
extends ConfigNode {
    public EntitySpecNode(SelectableEntitySpec selectableEntitySpec, EntityExistInfo entityExistInfo) {
        this(selectableEntitySpec, new InstanceContent(), entityExistInfo);
    }

    private EntitySpecNode(SelectableEntitySpec selectableEntitySpec, InstanceContent instanceContent, EntityExistInfo entityExistInfo) {
        super(Children.LEAF, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        instanceContent.add((Object)selectableEntitySpec);
        instanceContent.add((Object)this);
        String string = selectableEntitySpec.getEntitySpec().getDisplayName();
        if (entityExistInfo != null && entityExistInfo.exist(selectableEntitySpec.getEntitySpec())) {
            string = "<exist> " + string;
        }
        this.setDisplayName(string);
        this.setShortDescription(selectableEntitySpec.getEntitySpec().getDescription());
        this.setSelectedNonRecursive(selectableEntitySpec.isSelected());
    }

    public void setSelectedNonRecursive(Boolean bl) {
        if (!this.isSelected().equals(bl)) {
            super.setSelectedNonRecursive(bl);
            SelectableEntitySpec selectableEntitySpec = (SelectableEntitySpec)this.getLookup().lookup(SelectableEntitySpec.class);
            selectableEntitySpec.setSelected(bl);
        }
    }

    public Image getIcon(int n) {
        SelectableEntitySpec selectableEntitySpec = (SelectableEntitySpec)this.getLookup().lookup(SelectableEntitySpec.class);
        int n2 = 48;
        if (n == 1 || n == 3) {
            n2 = 16;
        } else if (n == 2 || n == 4) {
            n2 = 32;
        }
        Image image = selectableEntitySpec.getEntitySpec().getIcon(n2);
        if (image == null) {
            image = ImageUtilities.loadImage((String)"com/paterva/maltego/entity/manager/resources/Entity.png");
        }
        return image;
    }
}

