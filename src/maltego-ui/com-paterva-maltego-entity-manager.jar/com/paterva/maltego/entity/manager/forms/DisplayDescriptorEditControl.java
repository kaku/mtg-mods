/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.util.ui.ListUtil
 *  com.paterva.maltego.util.ui.dialog.EditDialogDescriptor
 *  com.paterva.maltego.util.ui.table.EditableTableDecorator
 *  com.paterva.maltego.util.ui.table.TableButtonCallback
 *  org.netbeans.swing.etable.ETable
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.explorer.propertysheet.PropertySheet
 *  org.openide.nodes.Node
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.entity.manager.forms.DisplayDescriptorNode;
import com.paterva.maltego.entity.manager.forms.DisplayDescriptorTableModel;
import com.paterva.maltego.entity.manager.forms.NewDisplayDescriptorController;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.util.ui.ListUtil;
import com.paterva.maltego.util.ui.dialog.EditDialogDescriptor;
import com.paterva.maltego.util.ui.table.EditableTableDecorator;
import com.paterva.maltego.util.ui.table.TableButtonCallback;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.netbeans.swing.etable.ETable;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.explorer.propertysheet.PropertySheet;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

class DisplayDescriptorEditControl
extends JPanel {
    private DisplayDescriptorTableModel _tableModel;
    private PropertySheet _properties;
    private DisplayDescriptor _valueDescriptor;
    private JButton _addButton;
    private JPanel _buttonPanel;
    private JButton _moveDownButton;
    private JButton _moveUpButton;
    private JPanel _propertiesPanel;
    private JSplitPane _splitPane;
    private ETable _table;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JScrollPane jScrollPane1;

    public DisplayDescriptorEditControl() {
        this.initComponents();
        this.setName("Additional Properties");
        this._properties = new PropertySheet();
        this._properties.setBorder(null);
        this._splitPane.setRightComponent((Component)this._properties);
        this._tableModel = new DisplayDescriptorTableModel();
        this._table.setModel((TableModel)((Object)this._tableModel));
        HighlightRenderer highlightRenderer = new HighlightRenderer();
        this._table.setDefaultRenderer(String.class, (TableCellRenderer)highlightRenderer);
        EditableTableDecorator editableTableDecorator = new EditableTableDecorator();
        editableTableDecorator.addDelete((JTable)this._table, new TableButtonCallback(){

            public boolean isButtonEnabled(JTable jTable, String string, int n) {
                return DisplayDescriptorEditControl.this._valueDescriptor != DisplayDescriptorEditControl.this._tableModel.getRow(n);
            }
        });
        this._table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                DisplayDescriptorEditControl.this.updateSelection();
            }
        });
        int n = this._table.getPreferredScrollableViewportSize().width;
        TableColumnModel tableColumnModel = this._table.getColumnModel();
        tableColumnModel.getColumn(0).setPreferredWidth(n / 2);
        tableColumnModel.getColumn(0).setPreferredWidth(n / 10 * 4);
    }

    private void updateSelection() {
        List list = this._tableModel.getRows(DisplayDescriptorEditControl.getSelectedRowIndicestoModel(this._table));
        DisplayDescriptorNode[] arrdisplayDescriptorNode = new DisplayDescriptorNode[list.size()];
        for (int i = 0; i < list.size(); ++i) {
            arrdisplayDescriptorNode[i] = new DisplayDescriptorNode((DisplayDescriptor)list.get(i));
        }
        this._properties.setNodes((Node[])arrdisplayDescriptorNode);
    }

    private static int[] getSelectedRowIndicestoModel(ETable eTable) {
        int[] arrn = eTable.getSelectedRows();
        for (int i = 0; i < arrn.length; ++i) {
            arrn[i] = eTable.convertRowIndexToModel(arrn[i]);
        }
        return arrn;
    }

    private void setSelection(List<DisplayDescriptor> list) {
        ListSelectionModel listSelectionModel = this._table.getSelectionModel();
        listSelectionModel.setValueIsAdjusting(true);
        listSelectionModel.clearSelection();
        for (int i = 0; i < this._table.getRowCount(); ++i) {
            if (!list.contains(this._tableModel.getRow(i))) continue;
            int n = this._table.convertRowIndexToView(i);
            listSelectionModel.addSelectionInterval(n, n);
        }
        listSelectionModel.setValueIsAdjusting(false);
    }

    private void initComponents() {
        this._buttonPanel = new JPanel();
        this._addButton = new JButton();
        this.jPanel3 = new JPanel();
        this._splitPane = new JSplitPane();
        this._propertiesPanel = new JPanel();
        this.jPanel2 = new JPanel();
        this._moveUpButton = new JButton();
        this._moveDownButton = new JButton();
        this.jPanel1 = new JPanel();
        this.jScrollPane1 = new JScrollPane();
        this._table = new ETable();
        this.setMinimumSize(new Dimension(480, 300));
        this.setPreferredSize(new Dimension(480, 300));
        this.setLayout(new BorderLayout());
        this._addButton.setText(NbBundle.getMessage(DisplayDescriptorEditControl.class, (String)"DisplayDescriptorEditControl._addButton.text"));
        this._addButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DisplayDescriptorEditControl.this._addButtonActionPerformed(actionEvent);
            }
        });
        GroupLayout groupLayout = new GroupLayout(this._buttonPanel);
        this._buttonPanel.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, groupLayout.createSequentialGroup().addContainerGap(383, 32767).addComponent(this._addButton).addContainerGap()));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, groupLayout.createSequentialGroup().addContainerGap(-1, 32767).addComponent(this._addButton).addContainerGap()));
        this.add((Component)this._buttonPanel, "First");
        this.jPanel3.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5), BorderFactory.createLineBorder(UIManager.getLookAndFeelDefaults().getColor("darculaMod.borderColor"))));
        this.jPanel3.setLayout(new BorderLayout());
        this._splitPane.setDividerLocation(250);
        this._propertiesPanel.setBorder(BorderFactory.createEmptyBorder(3, 0, 0, 0));
        this._propertiesPanel.setLayout(new BorderLayout());
        this.jPanel2.setLayout(new GridBagLayout());
        this._moveUpButton.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/entity/manager/resources/Up.png")));
        this._moveUpButton.setText(NbBundle.getMessage(DisplayDescriptorEditControl.class, (String)"DisplayDescriptorEditControl._moveUpButton.text"));
        this._moveUpButton.setMaximumSize(new Dimension(30, 25));
        this._moveUpButton.setMinimumSize(new Dimension(30, 25));
        this._moveUpButton.setPreferredSize(new Dimension(30, 25));
        this._moveUpButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DisplayDescriptorEditControl.this._moveUpButtonActionPerformed(actionEvent);
            }
        });
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 2;
        gridBagConstraints.insets = new Insets(0, 3, 6, 6);
        this.jPanel2.add((Component)this._moveUpButton, gridBagConstraints);
        this._moveDownButton.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/entity/manager/resources/Down.png")));
        this._moveDownButton.setText(NbBundle.getMessage(DisplayDescriptorEditControl.class, (String)"DisplayDescriptorEditControl._moveDownButton.text"));
        this._moveDownButton.setMaximumSize(new Dimension(30, 25));
        this._moveDownButton.setMinimumSize(new Dimension(30, 25));
        this._moveDownButton.setPreferredSize(new Dimension(30, 25));
        this._moveDownButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DisplayDescriptorEditControl.this._moveDownButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.insets = new Insets(0, 3, 0, 6);
        this.jPanel2.add((Component)this._moveDownButton, gridBagConstraints);
        this.jPanel1.setName("");
        this.jPanel1.setPreferredSize(new Dimension(0, 0));
        GroupLayout groupLayout2 = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout2);
        groupLayout2.setHorizontalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 39, 32767));
        groupLayout2.setVerticalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 239, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weighty = 1.0;
        this.jPanel2.add((Component)this.jPanel1, gridBagConstraints);
        this._propertiesPanel.add((Component)this.jPanel2, "West");
        this.jScrollPane1.setMinimumSize(new Dimension(252, 302));
        this._table.setModel((TableModel)new DefaultTableModel(new Object[][]{{null, null, null, null}, {null, null, null, null}, {null, null, null, null}, {null, null, null, null}}, new String[]{"Title 1", "Title 2", "Title 3", "Title 4"}));
        this._table.setPreferredScrollableViewportSize(new Dimension(250, 300));
        this.jScrollPane1.setViewportView((Component)this._table);
        this._propertiesPanel.add((Component)this.jScrollPane1, "Center");
        this._splitPane.setLeftComponent(this._propertiesPanel);
        this.jPanel3.add((Component)this._splitPane, "Center");
        this.add((Component)this.jPanel3, "Center");
    }

    private void _addButtonActionPerformed(ActionEvent actionEvent) {
        EditDialogDescriptor editDialogDescriptor = new EditDialogDescriptor("Add New Property", (WizardDescriptor.Panel)new NewDisplayDescriptorController());
        if (DialogDisplayer.getDefault().notify((NotifyDescriptor)editDialogDescriptor) == EditDialogDescriptor.OK_OPTION) {
            String string = (String)editDialogDescriptor.getProperty("name");
            TypeDescriptor typeDescriptor = (TypeDescriptor)editDialogDescriptor.getProperty("type");
            String string2 = (String)editDialogDescriptor.getProperty("displayName");
            DisplayDescriptor displayDescriptor = new DisplayDescriptor(typeDescriptor.getType(), string, string2);
            this._tableModel.addRow((Object)displayDescriptor);
        }
    }

    private void _moveUpButtonActionPerformed(ActionEvent actionEvent) {
        int[] arrn = this._table.getSelectedRows();
        List list = this._tableModel.getRows();
        List list2 = this._tableModel.getRows(arrn);
        ListUtil.moveSelectedUp((List)list, (List)list2);
        this._tableModel.fireTableDataChanged();
        this.setSelection(list2);
    }

    private void _moveDownButtonActionPerformed(ActionEvent actionEvent) {
        int[] arrn = this._table.getSelectedRows();
        List list = this._tableModel.getRows();
        List list2 = this._tableModel.getRows(arrn);
        ListUtil.moveSelectedDown((List)list, (List)list2);
        this._tableModel.fireTableDataChanged();
        this.setSelection(list2);
    }

    public void setDescriptors(DisplayDescriptorCollection displayDescriptorCollection) {
        this._tableModel.setRows((List)displayDescriptorCollection);
    }

    public void setValueDescriptor(DisplayDescriptor displayDescriptor) {
        this._valueDescriptor = displayDescriptor;
    }

    private class HighlightRenderer
    extends DefaultTableCellRenderer
    implements TableCellRenderer {
        private HighlightRenderer() {
        }

        @Override
        public Component getTableCellRendererComponent(JTable jTable, Object object, boolean bl, boolean bl2, int n, int n2) {
            Component component = super.getTableCellRendererComponent(jTable, object, bl, bl2, n, n2);
            Font font = component.getFont();
            if (DisplayDescriptorEditControl.this._tableModel.getRow(n) == DisplayDescriptorEditControl.this._valueDescriptor) {
                component.setFont(font.deriveFont(1));
            } else {
                component.setFont(font.deriveFont(0));
            }
            return component;
        }
    }

}

