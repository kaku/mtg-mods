/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  org.netbeans.spi.palette.PaletteFilter
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.entity.manager.palette;

import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.manager.data.EntityDataObject;
import org.netbeans.spi.palette.PaletteFilter;
import org.openide.util.Lookup;

class EntityPaletteFilter
extends PaletteFilter {
    EntityPaletteFilter() {
    }

    public boolean isValidItem(Lookup lookup) {
        EntityDataObject entityDataObject = (EntityDataObject)lookup.lookup(EntityDataObject.class);
        return entityDataObject.getEntitySpec().isToolboxItem() && entityDataObject.getEntitySpec().isVisible();
    }

    public boolean isValidCategory(Lookup lookup) {
        return true;
    }
}

