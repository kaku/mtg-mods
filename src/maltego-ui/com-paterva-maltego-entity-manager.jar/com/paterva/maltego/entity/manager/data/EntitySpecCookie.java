/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 */
package com.paterva.maltego.entity.manager.data;

import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import org.openide.nodes.Node;

interface EntitySpecCookie
extends Node.Cookie {
    public MaltegoEntitySpec getEntitySpec();
}

