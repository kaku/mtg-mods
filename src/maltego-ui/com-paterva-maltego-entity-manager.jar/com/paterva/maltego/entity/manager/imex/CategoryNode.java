/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigFolderNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.entity.manager.imex;

import com.paterva.maltego.entity.manager.imex.EntityExistInfo;
import com.paterva.maltego.entity.manager.imex.EntitySpecNode;
import com.paterva.maltego.entity.manager.imex.SelectableCategory;
import com.paterva.maltego.entity.manager.imex.SelectableEntitySpec;
import com.paterva.maltego.importexport.ConfigFolderNode;
import java.util.Collection;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class CategoryNode
extends ConfigFolderNode {
    CategoryNode(SelectableCategory selectableCategory, EntityExistInfo entityExistInfo) {
        this(selectableCategory, new InstanceContent(), entityExistInfo);
    }

    private CategoryNode(SelectableCategory selectableCategory, InstanceContent instanceContent, EntityExistInfo entityExistInfo) {
        super((Children)new EntitySpecChildren(selectableCategory, entityExistInfo), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        instanceContent.add((Object)selectableCategory);
        instanceContent.add((Object)this);
        this.setName(selectableCategory.getName());
        this.setShortDescription("" + selectableCategory.size() + " Entities");
        this.setSelectedNonRecursive(selectableCategory.isSelected());
    }

    public void setSelectedNonRecursive(Boolean bl) {
        if (!this.isSelected().equals(bl)) {
            super.setSelectedNonRecursive(bl);
            SelectableCategory selectableCategory = (SelectableCategory)this.getLookup().lookup(SelectableCategory.class);
            selectableCategory.setSelected(bl);
        }
    }

    protected boolean unselectWhenNoSelectedChildren() {
        return false;
    }

    private static class EntitySpecChildren
    extends Children.Keys<SelectableEntitySpec> {
        private EntityExistInfo _existInfo;

        public EntitySpecChildren(SelectableCategory selectableCategory, EntityExistInfo entityExistInfo) {
            this._existInfo = entityExistInfo;
            this.setKeys((Collection)selectableCategory);
        }

        protected Node[] createNodes(SelectableEntitySpec selectableEntitySpec) {
            EntitySpecNode entitySpecNode = new EntitySpecNode(selectableEntitySpec, this._existInfo);
            return new Node[]{entitySpecNode};
        }
    }

}

