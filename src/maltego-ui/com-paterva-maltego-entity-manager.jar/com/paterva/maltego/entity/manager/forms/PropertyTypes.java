/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import java.util.ArrayList;
import java.util.Arrays;

public class PropertyTypes {
    private static String[] _valueTypes = new String[]{"string", "date", "int", "double"};
    private static String[] _imageTypes = new String[]{"image", "url", "string", "attachments"};

    private PropertyTypes() {
    }

    public static TypeDescriptor[] valueTypes() {
        return PropertyTypes.getTypes(_valueTypes);
    }

    private static TypeDescriptor[] getTypes(String[] arrstring) {
        TypeRegistry typeRegistry = TypeRegistry.getDefault();
        ArrayList<TypeDescriptor> arrayList = new ArrayList<TypeDescriptor>();
        for (String string : arrstring) {
            arrayList.add(typeRegistry.getType(string));
        }
        return arrayList.toArray((T[])new TypeDescriptor[arrayList.size()]);
    }

    public static TypeDescriptor[] allTypes() {
        return new TypeDescriptor[0];
    }

    public static DisplayDescriptor[] getValueProperties(TypeSpec typeSpec) {
        return PropertyTypes.getPropertiesOfType(typeSpec.getProperties(), _valueTypes);
    }

    public static DisplayDescriptor[] getImageProperties(TypeSpec typeSpec) {
        return PropertyTypes.getPropertiesOfType(typeSpec.getProperties(), _imageTypes);
    }

    public static DisplayDescriptor[] getDisplayValueProperties(TypeSpec typeSpec) {
        return PropertyTypes.getValueProperties(typeSpec);
    }

    private static DisplayDescriptor[] getPropertiesOfType(DisplayDescriptorCollection displayDescriptorCollection, String[] arrstring) {
        Object[] arrobject = Arrays.copyOf(arrstring, arrstring.length);
        Arrays.sort(arrobject);
        ArrayList<DisplayDescriptor> arrayList = new ArrayList<DisplayDescriptor>();
        for (DisplayDescriptor displayDescriptor : displayDescriptorCollection) {
            if (Arrays.binarySearch(arrobject, displayDescriptor.getTypeDescriptor().getTypeName()) < 0) continue;
            arrayList.add(displayDescriptor);
        }
        return arrayList.toArray((T[])new DisplayDescriptor[arrayList.size()]);
    }
}

