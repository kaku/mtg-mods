/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.components.PanelWithMatteBorderAllSides
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.CallableSystemAction
 */
package com.paterva.maltego.entity.manager;

import com.paterva.maltego.entity.manager.EntityManagerForm;
import com.paterva.maltego.entity.manager.palette.PaletteSupport;
import com.paterva.maltego.util.ui.components.PanelWithMatteBorderAllSides;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import javax.swing.JButton;
import javax.swing.SwingUtilities;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.actions.CallableSystemAction;

public final class ShowEnitityManagerAction
extends CallableSystemAction {
    public void performAction() {
        if (SwingUtilities.isEventDispatchThread()) {
            this.performImpl();
        } else {
            try {
                SwingUtilities.invokeAndWait(new Runnable(){

                    @Override
                    public void run() {
                        ShowEnitityManagerAction.this.performImpl();
                    }
                });
            }
            catch (InterruptedException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
            catch (InvocationTargetException var1_2) {
                Exceptions.printStackTrace((Throwable)var1_2);
            }
        }
    }

    private void performImpl() {
        EntityManagerForm entityManagerForm = new EntityManagerForm();
        PanelWithMatteBorderAllSides panelWithMatteBorderAllSides = new PanelWithMatteBorderAllSides((LayoutManager)new BorderLayout());
        panelWithMatteBorderAllSides.add(entityManagerForm);
        JButton jButton = new JButton("Close");
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)panelWithMatteBorderAllSides, "Entity Manager", true, (Object[])new JButton[]{jButton}, (Object)jButton, 0, HelpCtx.DEFAULT_HELP, null);
        DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor);
        PaletteSupport.getPalette().refresh();
    }

    public String getName() {
        return "Manage Entities";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected String iconResource() {
        return "com/paterva/maltego/entity/manager/resources/EntityManager.png";
    }

    protected boolean asynchronous() {
        return false;
    }

}

