/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactory.icons.ImageBrowserPanel
 *  com.paterva.maltego.util.IconSize
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  com.paterva.maltego.util.ui.dialog.ChangeEventPropagator
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.imgfactory.icons.ImageBrowserPanel;
import com.paterva.maltego.util.IconSize;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import com.paterva.maltego.util.ui.dialog.ChangeEventPropagator;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxEditor;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.openide.util.NbBundle;

public class EntityInfoForm
extends JPanel {
    private final ChangeEventPropagator _changeSupport;
    private ImageBrowserPanel _smallIcon;
    private ImageBrowserPanel _largeIcon;
    private Color _descriptionFg;
    private JCheckBox _baseEntityCheckBox;
    private JComboBox _baseEntityComboBox;
    private JTextField _description;
    private JTextField _displayName;
    private JTextField _id;
    private JPanel _largeIconPanel;
    private JPanel _smallIconPanel;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JLabel jLabel8;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JPanel jPanel4;
    private JPanel jPanel6;

    public EntityInfoForm() {
        this._changeSupport = new ChangeEventPropagator((Object)this);
        this._descriptionFg = UIManager.getLookAndFeelDefaults().getColor("7-description-foreground");
        this.initComponents();
        this.setName("Basic Information");
        this._displayName.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        this._id.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        this._largeIcon = (ImageBrowserPanel)this._largeIconPanel;
        this._smallIcon = (ImageBrowserPanel)this._smallIconPanel;
        this._smallIcon.setClipSize(IconSize.TINY);
        this._largeIcon.setClipSize(IconSize.LARGE);
        this._smallIcon.addActionListener((ActionListener)this._changeSupport);
        this._largeIcon.addActionListener((ActionListener)this._changeSupport);
        this._largeIcon.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                EntityInfoForm.this.updateSmallIcon();
            }
        });
        this._baseEntityCheckBox.addChangeListener((ChangeListener)this._changeSupport);
        ((JTextComponent)this._baseEntityComboBox.getEditor().getEditorComponent()).getDocument().addDocumentListener((DocumentListener)this._changeSupport);
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    public void addDisplayNameFocusListener(FocusListener focusListener) {
        this._displayName.addFocusListener(focusListener);
    }

    public void removeDisplayNameFocusListener(FocusListener focusListener) {
        this._displayName.removeFocusListener(focusListener);
    }

    private void updateSmallIcon() {
        String string = this._largeIcon.getIconResource();
        if (string != null) {
            this._smallIcon.setIconResource(string);
        } else {
            File file = this._largeIcon.getImageFile();
            if (file != null) {
                try {
                    this._smallIcon.setImageFile(file);
                }
                catch (IOException var3_3) {
                    // empty catch block
                }
            }
        }
    }

    private void updateBaseEntityComboBox() {
        this._baseEntityComboBox.setEnabled(this._baseEntityCheckBox.isSelected());
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.jLabel3 = new JLabel();
        this._largeIconPanel = new ImageBrowserPanel();
        this.jLabel5 = new JLabel();
        this._smallIconPanel = new ImageBrowserPanel();
        this.jPanel6 = new JPanel();
        this.jPanel2 = new JPanel();
        this.jLabel2 = new LabelWithBackground();
        this._description = new JTextField();
        this.jLabel1 = new LabelWithBackground();
        this._displayName = new JTextField();
        this._id = new JTextField();
        this.jLabel4 = new LabelWithBackground();
        this.jLabel6 = new JLabel();
        this.jLabel7 = new JLabel();
        this.jLabel8 = new JLabel();
        this.jPanel3 = new JPanel();
        this._baseEntityComboBox = new JComboBox();
        this._baseEntityCheckBox = new JCheckBox();
        this.jPanel4 = new JPanel();
        this.setName("");
        this.jPanel1.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(EntityInfoForm.class, (String)"EntityInfoForm.jPanel1.border.title")));
        this.jPanel1.setLayout(new GridBagLayout());
        this.jLabel3.setText(NbBundle.getMessage(EntityInfoForm.class, (String)"EntityInfoForm.jLabel3.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 3;
        gridBagConstraints.insets = new Insets(10, 10, 10, 10);
        this.jPanel1.add((Component)this.jLabel3, gridBagConstraints);
        this._largeIconPanel.setLayout(new FlowLayout(0, 0, 0));
        this.jPanel1.add((Component)this._largeIconPanel, new GridBagConstraints());
        this.jLabel5.setText(NbBundle.getMessage(EntityInfoForm.class, (String)"EntityInfoForm.jLabel5.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 3;
        gridBagConstraints.insets = new Insets(10, 10, 10, 10);
        this.jPanel1.add((Component)this.jLabel5, gridBagConstraints);
        this._smallIconPanel.setLayout(new FlowLayout(0, 0, 0));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new Insets(0, 0, 0, 10);
        this.jPanel1.add((Component)this._smallIconPanel, gridBagConstraints);
        GroupLayout groupLayout = new GroupLayout(this.jPanel6);
        this.jPanel6.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.jPanel1.add((Component)this.jPanel6, gridBagConstraints);
        this.jPanel2.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(EntityInfoForm.class, (String)"EntityInfoForm.jPanel2.border.title")));
        this.jPanel2.setMinimumSize(new Dimension(520, 178));
        this.jPanel2.setVerifyInputWhenFocusTarget(false);
        this.jPanel2.setLayout(new GridBagLayout());
        this.jLabel2.setText(NbBundle.getMessage(EntityInfoForm.class, (String)"EntityInfoForm.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 10, 0, 0);
        this.jPanel2.add((Component)this.jLabel2, gridBagConstraints);
        this._description.setText(NbBundle.getMessage(EntityInfoForm.class, (String)"EntityInfoForm._description.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 410;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 0, 0, 10);
        this.jPanel2.add((Component)this._description, gridBagConstraints);
        this.jLabel1.setText(NbBundle.getMessage(EntityInfoForm.class, (String)"EntityInfoForm.jLabel1.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 24;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 10, 0, 0);
        this.jPanel2.add((Component)this.jLabel1, gridBagConstraints);
        this._displayName.setText(NbBundle.getMessage(EntityInfoForm.class, (String)"EntityInfoForm._displayName.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 410;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(10, 0, 0, 10);
        this.jPanel2.add((Component)this._displayName, gridBagConstraints);
        this._id.setText(NbBundle.getMessage(EntityInfoForm.class, (String)"EntityInfoForm._id.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 410;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 0, 0, 10);
        this.jPanel2.add((Component)this._id, gridBagConstraints);
        this.jLabel4.setText(NbBundle.getMessage(EntityInfoForm.class, (String)"EntityInfoForm.jLabel4.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 10, 0, 0);
        this.jPanel2.add((Component)this.jLabel4, gridBagConstraints);
        this.jLabel6.setForeground(this._descriptionFg);
        this.jLabel6.setText(NbBundle.getMessage(EntityInfoForm.class, (String)"EntityInfoForm.jLabel6.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 6, 10, 0);
        this.jPanel2.add((Component)this.jLabel6, gridBagConstraints);
        this.jLabel7.setForeground(this._descriptionFg);
        this.jLabel7.setText(NbBundle.getMessage(EntityInfoForm.class, (String)"EntityInfoForm.jLabel7.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 6, 6, 0);
        this.jPanel2.add((Component)this.jLabel7, gridBagConstraints);
        this.jLabel8.setForeground(this._descriptionFg);
        this.jLabel8.setText(NbBundle.getMessage(EntityInfoForm.class, (String)"EntityInfoForm.jLabel8.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 6, 6, 0);
        this.jPanel2.add((Component)this.jLabel8, gridBagConstraints);
        this.jPanel3.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(EntityInfoForm.class, (String)"EntityInfoForm.jPanel3.border.title")));
        this.jPanel3.setLayout(new GridBagLayout());
        this._baseEntityComboBox.setEditable(true);
        this._baseEntityComboBox.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 76;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 10, 10, 10);
        this.jPanel3.add((Component)this._baseEntityComboBox, gridBagConstraints);
        this._baseEntityCheckBox.setText(NbBundle.getMessage(EntityInfoForm.class, (String)"EntityInfoForm._baseEntityCheckBox.text"));
        this._baseEntityCheckBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                EntityInfoForm.this._baseEntityCheckBoxActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 10, 10, 6);
        this.jPanel3.add((Component)this._baseEntityCheckBox, gridBagConstraints);
        this.jPanel4.setMinimumSize(new Dimension(1, 1));
        this.jPanel4.setPreferredSize(new Dimension(1, 1));
        GroupLayout groupLayout2 = new GroupLayout(this.jPanel4);
        this.jPanel4.setLayout(groupLayout2);
        groupLayout2.setHorizontalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 241, 32767));
        groupLayout2.setVerticalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 43, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        this.jPanel3.add((Component)this.jPanel4, gridBagConstraints);
        GroupLayout groupLayout3 = new GroupLayout(this);
        this.setLayout(groupLayout3);
        groupLayout3.setHorizontalGroup(groupLayout3.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout3.createSequentialGroup().addContainerGap().addGroup(groupLayout3.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jPanel3, GroupLayout.Alignment.TRAILING, -1, -1, 32767).addComponent(this.jPanel2, -1, -1, 32767).addComponent(this.jPanel1, GroupLayout.Alignment.TRAILING, -1, 542, 32767)).addContainerGap()));
        groupLayout3.setVerticalGroup(groupLayout3.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout3.createSequentialGroup().addContainerGap().addComponent(this.jPanel2, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jPanel3, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jPanel1, -2, -1, -2).addContainerGap(-1, 32767)));
    }

    private void _baseEntityCheckBoxActionPerformed(ActionEvent actionEvent) {
        this.updateBaseEntityComboBox();
    }

    public String getDisplayName() {
        return this._displayName.getText();
    }

    public String getDescription() {
        return this._description.getText();
    }

    public void setDescription(String string) {
        this._description.setText(string);
    }

    public void setDisplayName(String string) {
        this._displayName.setText(string);
    }

    public String getID() {
        return this._id.getText();
    }

    public void setID(String string) {
        this._id.setText(string);
    }

    public void setIDEnabled(boolean bl) {
        this._id.setEnabled(bl);
    }

    public void setSmallIconResource(String string) {
        this._smallIcon.setIconResource(string);
    }

    public void setLargeIconResource(String string) {
        this._largeIcon.setIconResource(string);
    }

    public String getLargeIconResource() {
        return this._largeIcon.getIconResource();
    }

    public String getSmallIconResource() {
        return this._smallIcon.getIconResource();
    }

    public void setSmallIcon(Image image) {
        this._smallIcon.setImage(image);
    }

    public void setLargeIcon(Image image) {
        this._largeIcon.setImage(image);
    }

    public Image getLargeIcon() {
        return this._largeIcon.getImage();
    }

    public Image getSmallIcon() {
        return this._smallIcon.getImage();
    }

    public void setEntitySpecs(List<String> list) {
        this._baseEntityComboBox.removeAllItems();
        for (String string : list) {
            this._baseEntityComboBox.addItem(string);
        }
    }

    public void setBaseEntity(String string) {
        this._baseEntityCheckBox.setSelected(string != null);
        this._baseEntityComboBox.setSelectedItem(string);
        this.updateBaseEntityComboBox();
    }

    public boolean hasBaseEntity() {
        return this._baseEntityCheckBox.isSelected();
    }

    public String getBaseEntity() {
        if (this._baseEntityCheckBox.isSelected()) {
            Object object = this._baseEntityComboBox.getSelectedItem();
            if (object != null) {
                return object.toString();
            }
            return ((JTextComponent)this._baseEntityComboBox.getEditor().getEditorComponent()).getText();
        }
        return null;
    }

}

