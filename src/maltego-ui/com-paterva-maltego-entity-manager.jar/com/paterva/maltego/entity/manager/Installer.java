/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.registry.Level1EntityRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.util.ui.dialog.UIRunQueue
 *  org.openide.modules.ModuleInstall
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.entity.manager;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.registry.Level1EntityRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.util.ui.dialog.UIRunQueue;
import java.util.prefs.Preferences;
import org.openide.modules.ModuleInstall;
import org.openide.util.NbPreferences;

public class Installer
extends ModuleInstall {
    private static final String PREF_RESET_GPS_ENTITY = "resetGPSEntity";
    private static final String GPS_ENTITY = "maltego.GPS";

    public void restored() {
        Preferences preferences = NbPreferences.forModule(Installer.class);
        if (preferences.getBoolean("resetGPSEntity", true)) {
            preferences.putBoolean("resetGPSEntity", false);
            UIRunQueue uIRunQueue = UIRunQueue.instance();
            uIRunQueue.queue(new Runnable(){

                @Override
                public void run() {
                    Level1EntityRegistry level1EntityRegistry;
                    EntityRegistry entityRegistry = EntityRegistry.getDefault();
                    MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get("maltego.GPS");
                    if (maltegoEntitySpec != null && (maltegoEntitySpec = (level1EntityRegistry = new Level1EntityRegistry()).get("maltego.GPS")) != null) {
                        entityRegistry.remove("maltego.GPS");
                        entityRegistry.put((TypeSpec)maltegoEntitySpec);
                    }
                }
            }, 96);
        }
    }

}

