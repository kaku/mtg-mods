/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.entity.serializer.EntityCategoryEntryFactory
 *  com.paterva.entity.serializer.MaltegoEntityEntryFactory
 *  com.paterva.maltego.archive.mtz.EntryFactory
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryEntityMerger
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules$EntityRule
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.registry.EmbeddedIconExtractor
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 */
package com.paterva.maltego.entity.manager.imex;

import com.paterva.entity.serializer.EntityCategoryEntryFactory;
import com.paterva.entity.serializer.MaltegoEntityEntryFactory;
import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.archive.mtz.discover.DiscoveryEntityMerger;
import com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.registry.EmbeddedIconExtractor;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

class EntityImporter {
    EntityImporter() {
    }

    public List<String> readCategories(MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        return maltegoArchiveReader.readAll((EntryFactory)new EntityCategoryEntryFactory(), "Graph1");
    }

    public List<MaltegoEntitySpec> readEntities(MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        return maltegoArchiveReader.readAll((EntryFactory)new MaltegoEntityEntryFactory(), "Graph1");
    }

    public void applyEntities(Collection<MaltegoEntitySpec> collection, DiscoveryMergingRules.EntityRule entityRule) {
        EntityRegistry entityRegistry = EntityRegistry.getDefault();
        IconRegistry iconRegistry = IconRegistry.getDefault();
        for (MaltegoEntitySpec maltegoEntitySpec : collection) {
            EmbeddedIconExtractor.extract((MaltegoEntitySpec)maltegoEntitySpec, (IconRegistry)iconRegistry);
            DiscoveryEntityMerger.merge((EntityRegistry)entityRegistry, (MaltegoEntitySpec)maltegoEntitySpec, (DiscoveryMergingRules.EntityRule)entityRule);
        }
    }
}

