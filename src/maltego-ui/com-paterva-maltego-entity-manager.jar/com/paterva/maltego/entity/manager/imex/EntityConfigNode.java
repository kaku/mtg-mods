/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigFolderNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.entity.manager.imex;

import com.paterva.maltego.entity.manager.imex.CategoryNode;
import com.paterva.maltego.entity.manager.imex.EntityConfig;
import com.paterva.maltego.entity.manager.imex.EntityExistInfo;
import com.paterva.maltego.entity.manager.imex.SelectableCategory;
import com.paterva.maltego.entity.manager.imex.SelectableEntitySpec;
import com.paterva.maltego.importexport.ConfigFolderNode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class EntityConfigNode
extends ConfigFolderNode {
    EntityConfigNode(EntityConfig entityConfig, EntityExistInfo entityExistInfo) {
        this(entityConfig, new InstanceContent(), entityExistInfo);
    }

    private EntityConfigNode(EntityConfig entityConfig, InstanceContent instanceContent, EntityExistInfo entityExistInfo) {
        super((Children)new EntitySpecChildren(entityConfig, entityExistInfo), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        instanceContent.add((Object)entityConfig);
        instanceContent.add((Object)this);
        this.setName("Entities");
        this.setShortDescription("" + entityConfig.getSelectables().size() + " Categories (" + entityConfig.getSelectableEntities().size() + " Entities)");
        this.setSelectedNonRecursive(Boolean.valueOf(entityConfig.getSelectedCategories().size() != 0));
    }

    private static class EntitySpecChildren
    extends Children.Keys<SelectableCategory> {
        private EntityExistInfo _existInfo;

        public EntitySpecChildren(EntityConfig entityConfig, EntityExistInfo entityExistInfo) {
            this._existInfo = entityExistInfo;
            this.setKeys(entityConfig.getSelectables());
        }

        protected Node[] createNodes(SelectableCategory selectableCategory) {
            CategoryNode categoryNode = new CategoryNode(selectableCategory, this._existInfo);
            return new Node[]{categoryNode};
        }
    }

}

