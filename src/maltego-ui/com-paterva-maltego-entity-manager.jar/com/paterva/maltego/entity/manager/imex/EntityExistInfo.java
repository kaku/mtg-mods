/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.util.ExistInfo
 */
package com.paterva.maltego.entity.manager.imex;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.util.ExistInfo;

class EntityExistInfo
implements ExistInfo<MaltegoEntitySpec> {
    EntityExistInfo() {
    }

    public boolean exist(MaltegoEntitySpec maltegoEntitySpec) {
        return EntityRegistry.getDefault().contains(maltegoEntitySpec.getTypeName());
    }
}

