/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.util.ui.table.RowTableModel
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.util.ui.table.RowTableModel;
import java.util.List;

class DisplayDescriptorTableModel
extends RowTableModel<DisplayDescriptor> {
    private static final String[] _columns = new String[]{"Display name", "Type"};

    public DisplayDescriptorTableModel() {
        super(_columns);
    }

    public DisplayDescriptorTableModel(List<DisplayDescriptor> list) {
        super(_columns, list);
    }

    public Object getValueFor(DisplayDescriptor displayDescriptor, int n) {
        switch (n) {
            case 0: {
                return displayDescriptor.getDisplayName();
            }
            case 1: {
                return displayDescriptor.getTypeDescriptor();
            }
        }
        return null;
    }
}

