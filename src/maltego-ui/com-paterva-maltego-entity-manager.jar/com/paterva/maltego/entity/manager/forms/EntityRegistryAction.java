/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import org.openide.WizardDescriptor;
import org.openide.util.HelpCtx;
import org.openide.util.actions.SystemAction;

public abstract class EntityRegistryAction
extends SystemAction {
    private EntityRegistry _registry;
    private String _defaultCategory;

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected String iconResource() {
        return "com/paterva/maltego/entity/manager/resources/EntityManager.png";
    }

    protected EntityRegistry registry() {
        if (this._registry == null) {
            return EntityRegistry.getDefault();
        }
        return this._registry;
    }

    protected WizardDescriptor.Panel[] initPanels(WizardDescriptor.Panel[] arrpanel) {
        WizardUtilities.updatePanels((WizardDescriptor.Panel[])arrpanel);
        return arrpanel;
    }

    public String getDefaultCategory() {
        return this._defaultCategory;
    }

    public void setDefaultCategory(String string) {
        this._defaultCategory = string;
    }
}

