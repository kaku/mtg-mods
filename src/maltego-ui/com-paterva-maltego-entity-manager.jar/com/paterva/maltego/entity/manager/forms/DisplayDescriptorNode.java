/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.Converter
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.Evaluator
 *  com.paterva.maltego.typing.FormatAdapter
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.serializer.EvaluatorFactory
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.PropertySupport
 *  org.openide.nodes.PropertySupport$ReadOnly
 *  org.openide.nodes.PropertySupport$ReadWrite
 *  org.openide.nodes.PropertySupport$Reflection
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.Lookups
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.Evaluator;
import com.paterva.maltego.typing.FormatAdapter;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.serializer.EvaluatorFactory;
import com.paterva.maltego.util.StringUtilities;
import java.lang.reflect.InvocationTargetException;
import java.text.Format;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

class DisplayDescriptorNode
extends AbstractNode {
    public DisplayDescriptorNode(DisplayDescriptor displayDescriptor) {
        super(Children.LEAF, Lookups.singleton((Object)displayDescriptor));
    }

    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        Sheet.Set set2 = Sheet.createPropertiesSet();
        Sheet.Set set3 = Sheet.createPropertiesSet();
        set2.setName("main");
        set.setName("display");
        set3.setName("defaults");
        set2.setDisplayName("Basic information");
        set.setDisplayName("Display information");
        set3.setDisplayName("Defaults");
        DisplayDescriptor displayDescriptor = (DisplayDescriptor)this.getLookup().lookup(DisplayDescriptor.class);
        try {
            Node.Property property = this.createReflectionProperty((Object)displayDescriptor, String.class, "DisplayName", "Display name", "The name displayed to the user.");
            Node.Property property2 = this.createReflectionProperty((Object)displayDescriptor, String.class, "Description", "Description", "Short description of this property.");
            Node.Property property3 = this.createReflectionProperty((Object)displayDescriptor, String.class, "getName", null, "Name", "The unique name that references this property.");
            Node.Property property4 = this.createReadOnlyProperty(String.class, displayDescriptor.getTypeDescriptor().toString(), "Type", "The data type of this property.");
            Node.Property property5 = this.createReflectionProperty((Object)displayDescriptor, String.class, "GroupName", "Group", "Optional group name under which this property is visually ordered.");
            RequiredProperty requiredProperty = new RequiredProperty(displayDescriptor);
            ReadonlyProperty readonlyProperty = new ReadonlyProperty(displayDescriptor);
            DefaultValueProperty defaultValueProperty = new DefaultValueProperty(displayDescriptor);
            SampleValueProperty sampleValueProperty = new SampleValueProperty(displayDescriptor);
            FormatProperty formatProperty = null;
            if (displayDescriptor.getTypeDescriptor().getFormatAdapter() != null) {
                formatProperty = new FormatProperty(displayDescriptor);
            }
            set2.put(property3);
            set2.put(property4);
            set2.put((Node.Property)requiredProperty);
            set2.put((Node.Property)readonlyProperty);
            set.put(property);
            set.put(property2);
            if (formatProperty != null) {
                set.put((Node.Property)formatProperty);
            }
            set3.put((Node.Property)defaultValueProperty);
            set3.put((Node.Property)sampleValueProperty);
        }
        catch (NoSuchMethodException var6_7) {
            Exceptions.printStackTrace((Throwable)var6_7);
        }
        sheet.put(set2);
        sheet.put(set);
        sheet.put(set3);
        return sheet;
    }

    private Node.Property createReadOnlyProperty(Class<?> class_, final Object object, String string, String string2) {
        PropertySupport.ReadOnly readOnly = new PropertySupport.ReadOnly(string, class_, string, string2){

            public Object getValue() throws IllegalAccessException, InvocationTargetException {
                return object;
            }
        };
        return readOnly;
    }

    private Node.Property createReflectionProperty(Object object, Class<?> class_, String string, String string2, String string3) throws NoSuchMethodException {
        PropertySupport.Reflection reflection = new PropertySupport.Reflection(object, class_, string);
        reflection.setName(string2);
        reflection.setDisplayName(string2);
        reflection.setShortDescription(string3);
        return reflection;
    }

    private Node.Property createReflectionProperty(Object object, Class<?> class_, String string, String string2, String string3, String string4) throws NoSuchMethodException {
        PropertySupport.Reflection reflection = new PropertySupport.Reflection(object, class_, string, string2);
        reflection.setName(string3);
        reflection.setDisplayName(string3);
        reflection.setShortDescription(string4);
        return reflection;
    }

    public String getDisplayName() {
        DisplayDescriptor displayDescriptor = (DisplayDescriptor)this.getLookup().lookup(DisplayDescriptor.class);
        return displayDescriptor.getDisplayName();
    }

    public String getShortDescription() {
        DisplayDescriptor displayDescriptor = (DisplayDescriptor)this.getLookup().lookup(DisplayDescriptor.class);
        return displayDescriptor.getDescription();
    }

    private static class ReadonlyProperty
    extends PropertySupport.ReadWrite {
        private DisplayDescriptor _descriptor;

        public ReadonlyProperty(DisplayDescriptor displayDescriptor) {
            super("reaonly", Boolean.class, "Read only", "Indicates that this property cannot be set by the user.");
            this._descriptor = displayDescriptor;
        }

        public Object getValue() throws IllegalAccessException, InvocationTargetException {
            return new Boolean(this._descriptor.isReadonly());
        }

        public void setValue(Object object) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            this._descriptor.setReadonly(((Boolean)object).booleanValue());
        }
    }

    private static class RequiredProperty
    extends PropertySupport.ReadWrite {
        private DisplayDescriptor _descriptor;

        public RequiredProperty(DisplayDescriptor displayDescriptor) {
            super("required", Boolean.class, "Required", "Indicates that this property cannot be left blank.");
            this._descriptor = displayDescriptor;
        }

        public Object getValue() throws IllegalAccessException, InvocationTargetException {
            return new Boolean(!this._descriptor.isNullable());
        }

        public void setValue(Object object) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            this._descriptor.setNullable((Boolean)object == false);
        }
    }

    private static class FormatProperty
    extends PropertySupport.ReadWrite {
        private DisplayDescriptor _descriptor;

        public FormatProperty(DisplayDescriptor displayDescriptor) {
            super("format", String.class, "Format", "Optional format specifier used when rendering values (highly dependant on data type).");
            this._descriptor = displayDescriptor;
        }

        public Object getValue() throws IllegalAccessException, InvocationTargetException {
            Format format = this._descriptor.getFormat();
            String string = this._descriptor.getTypeDescriptor().getFormatString(format);
            return string == null ? "" : string;
        }

        public void setValue(Object object) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            this._descriptor.setFormat(this._descriptor.getTypeDescriptor().getFormat((String)object));
        }
    }

    private static class SampleValueProperty
    extends PropertySupport.ReadWrite {
        private DisplayDescriptor _descriptor;

        public SampleValueProperty(DisplayDescriptor displayDescriptor) {
            super("sampleValue", String.class, "Sample value", "The value of the property when dragged from the palette.");
            this._descriptor = displayDescriptor;
        }

        public Object getValue() throws IllegalAccessException, InvocationTargetException {
            String string = "";
            if (this._descriptor.getSampleValue() != null) {
                string = Converter.convertTo((Object)this._descriptor.getSampleValue(), (Class)this._descriptor.getType());
            }
            return string;
        }

        public void setValue(Object object) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            try {
                String string = (String)object;
                Class class_ = this._descriptor.getType();
                if (String.class.equals((Object)class_)) {
                    if (string != null) {
                        if (string.isEmpty()) {
                            string = " ";
                        }
                        this._descriptor.setSampleValue((Object)string);
                    }
                } else if (!StringUtilities.isNullOrEmpty((String)string)) {
                    this._descriptor.setSampleValue(Converter.convertFrom((String)string, (Class)class_));
                }
            }
            catch (IllegalArgumentException var2_3) {
                // empty catch block
            }
        }
    }

    private static class DefaultValueProperty
    extends PropertySupport.ReadWrite {
        private DisplayDescriptor _descriptor;

        public DefaultValueProperty(DisplayDescriptor displayDescriptor) {
            super("defaultValue", String.class, "Default value", "The default value of the property.");
            this._descriptor = displayDescriptor;
        }

        public Object getValue() throws IllegalAccessException, InvocationTargetException {
            String string = "";
            if (this._descriptor.getDefaultValue() != null) {
                string = Converter.convertTo((Object)this._descriptor.getDefaultValue(), (Class)this._descriptor.getType());
            }
            return string;
        }

        public void setValue(Object object) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            try {
                String string = (String)object;
                Class class_ = this._descriptor.getType();
                if (String.class.equals((Object)class_)) {
                    if (string != null) {
                        if (string.isEmpty()) {
                            string = " ";
                        }
                        Evaluator evaluator = EvaluatorFactory.createForDefaultValue((String)string);
                        this._descriptor.setEvaluator(evaluator);
                        this._descriptor.setDefaultValue((Object)string);
                    }
                } else if (!StringUtilities.isNullOrEmpty((String)string)) {
                    this._descriptor.setDefaultValue(Converter.convertFrom((String)string, (Class)class_));
                }
            }
            catch (IllegalArgumentException var2_3) {
                // empty catch block
            }
        }
    }

}

