/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.util.ui.dialog.EditDialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.manager.forms.AdvancedSettingsController;
import com.paterva.maltego.entity.manager.forms.DisplayDescriptorEditController;
import com.paterva.maltego.entity.manager.forms.DisplayInfoFormController;
import com.paterva.maltego.entity.manager.forms.EntityInfoFormController;
import com.paterva.maltego.entity.manager.forms.EntityRegistryAction;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.util.ui.dialog.EditDialogDescriptor;
import java.awt.event.ActionEvent;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;

public final class EditEntityTypeAction
extends EntityRegistryAction {
    public String getName() {
        return "Edit Entity Type...";
    }

    public boolean edit(MaltegoEntitySpec maltegoEntitySpec) {
        EditDialogDescriptor editDialogDescriptor = new EditDialogDescriptor(new WizardDescriptor.Panel[]{new EntityInfoFormController(), new DisplayDescriptorEditController(), new DisplayInfoFormController(true), new AdvancedSettingsController()});
        editDialogDescriptor.putProperty("entitySpec", (Object)maltegoEntitySpec);
        editDialogDescriptor.putProperty("editMode", (Object)Boolean.TRUE);
        editDialogDescriptor.putProperty("entityRegistry", (Object)this.registry());
        editDialogDescriptor.setTitle("Edit Entity - " + maltegoEntitySpec.getDisplayName());
        if (DialogDisplayer.getDefault().notify((NotifyDescriptor)editDialogDescriptor) == EditDialogDescriptor.FINISH_OPTION) {
            this.registry().put((TypeSpec)maltegoEntitySpec);
            return true;
        }
        return false;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}

