/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.palette.DragAndDropHandler
 *  org.netbeans.spi.palette.PaletteActions
 *  org.netbeans.spi.palette.PaletteController
 *  org.netbeans.spi.palette.PaletteFactory
 *  org.netbeans.spi.palette.PaletteFilter
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Children
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.FilterNode$Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeTransfer
 *  org.openide.util.Lookup
 *  org.openide.util.datatransfer.ExTransferable
 *  org.openide.util.datatransfer.PasteType
 */
package com.paterva.maltego.entity.manager.palette;

import com.paterva.maltego.entity.manager.palette.EntityPaletteActions;
import com.paterva.maltego.entity.manager.palette.EntityPaletteFilter;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.io.IOException;
import org.netbeans.spi.palette.DragAndDropHandler;
import org.netbeans.spi.palette.PaletteActions;
import org.netbeans.spi.palette.PaletteController;
import org.netbeans.spi.palette.PaletteFactory;
import org.netbeans.spi.palette.PaletteFilter;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.nodes.NodeTransfer;
import org.openide.util.Lookup;
import org.openide.util.datatransfer.ExTransferable;
import org.openide.util.datatransfer.PasteType;

public class PaletteSupport {
    public static PaletteController _controller;

    public static synchronized PaletteController getPalette() {
        if (_controller == null) {
            try {
                DataObject dataObject = DataObject.find((FileObject)PaletteSupport.getEntitiesRoot());
                NodeProxy nodeProxy = new NodeProxy(dataObject.getNodeDelegate());
                _controller = PaletteFactory.createPalette((Node)nodeProxy, (PaletteActions)new EntityPaletteActions(), (PaletteFilter)new EntityPaletteFilter(), (DragAndDropHandler)new MyDragDropHandler());
                Lookup lookup = _controller.getRoot();
                Node node = (Node)lookup.lookup(Node.class);
                node.setDisplayName("Entities");
            }
            catch (IOException var0_1) {
                var0_1.printStackTrace();
            }
        }
        return _controller;
    }

    private static FileObject getEntitiesRoot() throws IOException {
        FileObject fileObject;
        FileObject fileObject2 = FileUtil.getConfigRoot().getFileObject("Maltego");
        if (fileObject2 == null) {
            fileObject2 = FileUtil.getConfigRoot().createFolder("Maltego");
        }
        if ((fileObject = fileObject2.getFileObject("Entities")) == null) {
            fileObject = fileObject2.createFolder("Entities");
        }
        return fileObject;
    }

    static class ProxyChildren
    extends FilterNode.Children {
        public ProxyChildren(Node node) {
            super(node);
        }

        protected Node copyNode(Node node) {
            return new NodeProxy(node);
        }
    }

    static class NodeProxy
    extends FilterNode {
        public NodeProxy(Node node) {
            super(node, (Children)new ProxyChildren(node));
        }

        public PasteType[] getPasteTypes(Transferable transferable) {
            return super.getPasteTypes(transferable);
        }
    }

    private static class MyDragDropHandler
    extends DragAndDropHandler {
        private MyDragDropHandler() {
        }

        public void customize(ExTransferable exTransferable, Lookup lookup) {
            DataFlavor[] arrdataFlavor = exTransferable.getTransferDataFlavors();
            Node node = (Node)lookup.lookup(Node.class);
        }

        public boolean canDrop(Lookup lookup, DataFlavor[] arrdataFlavor, int n) {
            return true;
        }

        public boolean doDrop(Lookup lookup, Transferable transferable, int n, int n2) {
            Node node = NodeTransfer.node((Transferable)transferable, (int)3);
            if (node != null) {
                NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)"Hello...", 1);
                DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)message);
            }
            return super.doDrop(lookup, transferable, n, n2);
        }
    }

}

