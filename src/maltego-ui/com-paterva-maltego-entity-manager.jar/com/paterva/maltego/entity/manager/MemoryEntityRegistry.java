/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 */
package com.paterva.maltego.entity.manager;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class MemoryEntityRegistry
extends EntityRegistry {
    private Map<String, MaltegoEntitySpec> _map = new HashMap<String, MaltegoEntitySpec>();

    public MaltegoEntitySpec get(String string) {
        return this._map.get(string);
    }

    public Collection<MaltegoEntitySpec> getAll() {
        return this._map.values();
    }

    public void remove(String string) {
        MaltegoEntitySpec maltegoEntitySpec = this.get(string);
        if (maltegoEntitySpec != null) {
            this._map.remove(string);
            this.fireTypeRemoved((TypeSpec)maltegoEntitySpec);
        }
    }

    public boolean contains(String string) {
        return this._map.containsKey(string);
    }

    public String[] allCategories() {
        return new String[0];
    }

    public void put(MaltegoEntitySpec maltegoEntitySpec) {
        this.put(maltegoEntitySpec, "Default");
    }

    public void put(MaltegoEntitySpec maltegoEntitySpec, String string) {
        if (this._map.containsKey(maltegoEntitySpec.getTypeName())) {
            this._map.put(maltegoEntitySpec.getTypeName(), maltegoEntitySpec);
            this.fireTypeUpdated((TypeSpec)maltegoEntitySpec);
        } else {
            this._map.put(maltegoEntitySpec.getTypeName(), maltegoEntitySpec);
            this.fireTypeAdded((TypeSpec)maltegoEntitySpec);
        }
    }
}

