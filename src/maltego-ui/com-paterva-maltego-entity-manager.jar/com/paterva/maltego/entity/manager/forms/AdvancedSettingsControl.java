/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.ListUtil
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  com.paterva.maltego.util.ui.table.EditableTableDecorator
 *  com.paterva.maltego.util.ui.table.PaddedTableCellRenderer
 *  org.netbeans.swing.etable.ETable
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.entity.manager.forms.RegexGroup;
import com.paterva.maltego.entity.manager.forms.RegexGroupsTableModel;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.ListUtil;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import com.paterva.maltego.util.ui.table.EditableTableDecorator;
import com.paterva.maltego.util.ui.table.PaddedTableCellRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.netbeans.swing.etable.ETable;
import org.openide.util.NbBundle;

public class AdvancedSettingsControl
extends JPanel {
    private RegexGroupsTableModel _tableModel;
    private DisplayDescriptorCollection _properties;
    private JButton _addButton;
    private JSpinner _conversionOrder;
    private JPanel _conversionPanel;
    private JButton _downButton;
    private JTextField _plural;
    private JTextField _regex;
    private JCheckBox _regexCheckBox;
    private JCheckBox _root;
    private ETable _table;
    private JButton _upButton;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JPanel jPanel1;
    private JScrollPane jScrollPane4;

    public AdvancedSettingsControl() {
        this.initComponents();
        this.setName("Advanced Settings");
        this._tableModel = new RegexGroupsTableModel();
        this._table.setModel((TableModel)((Object)this._tableModel));
        EditableTableDecorator editableTableDecorator = new EditableTableDecorator();
        editableTableDecorator.addDelete((JTable)this._table, true, true);
        PaddedTableCellRenderer paddedTableCellRenderer = new PaddedTableCellRenderer();
        this._table.setDefaultRenderer(Integer.class, (TableCellRenderer)paddedTableCellRenderer);
        this._table.setDefaultRenderer(String.class, (TableCellRenderer)paddedTableCellRenderer);
        this._table.setAutoCreateColumnsFromModel(false);
        this._table.getSelectionModel().addListSelectionListener(new TableSelectionListener());
        TableColumnModel tableColumnModel = this._table.getColumnModel();
        tableColumnModel.getColumn(0).setPreferredWidth(50);
        tableColumnModel.getColumn(0).setMaxWidth(50);
        tableColumnModel.getColumn(1).setPreferredWidth(100);
        tableColumnModel.getColumn(1).setCellEditor(new PropertyComboCellEditor());
        this._tableModel.addTableModelListener((TableModelListener)new MappingTableModelListener());
    }

    public String getRegex() {
        return this._regex.getText().trim();
    }

    public String getPluralName() {
        return this._plural.getText();
    }

    public boolean getIsRoot() {
        return this._root.isSelected();
    }

    public int getConversionOrder() {
        return (Integer)this._conversionOrder.getValue();
    }

    public void setIsRoot(boolean bl) {
        this._root.setSelected(bl);
    }

    public void setRegex(String string) {
        this._regex.setText(string);
        this._regexCheckBox.setSelected(!StringUtilities.isNullOrEmpty((String)string));
        this.updateStates();
    }

    public void setRegexGroups(List<String> list) {
        ArrayList<RegexGroup> arrayList = new ArrayList<RegexGroup>();
        for (String string : list) {
            arrayList.add(new RegexGroup(this._properties.get(string)));
        }
        this._tableModel.setRows(arrayList);
    }

    public List<String> getRegexGroups() {
        ArrayList<String> arrayList = new ArrayList<String>();
        List list = this._tableModel.getRows();
        for (RegexGroup regexGroup : list) {
            DisplayDescriptor displayDescriptor = regexGroup.getProperty();
            arrayList.add(displayDescriptor != null ? displayDescriptor.getName() : "");
        }
        return arrayList;
    }

    public void setProperties(DisplayDescriptorCollection displayDescriptorCollection) {
        this._properties = displayDescriptorCollection;
    }

    private void populateCombo(JComboBox jComboBox, DisplayDescriptor displayDescriptor) {
        jComboBox.removeAllItems();
        jComboBox.addItem("<Ignore>");
        List list = this._tableModel.getRows();
        for (DisplayDescriptor displayDescriptor2 : this._properties) {
            boolean bl;
            if (displayDescriptor2.equals((PropertyDescriptor)displayDescriptor)) {
                bl = true;
            } else {
                bl = true;
                for (RegexGroup regexGroup : list) {
                    if (!displayDescriptor2.equals((PropertyDescriptor)regexGroup.getProperty())) continue;
                    bl = false;
                    break;
                }
            }
            if (!bl) continue;
            jComboBox.addItem(displayDescriptor2);
        }
    }

    public boolean hasRegex() {
        return this._regexCheckBox.isSelected() && !this.getRegex().isEmpty();
    }

    public void setPluralName(String string) {
        this._plural.setText(string);
    }

    public void setConversionOrder(int n) {
        this._conversionOrder.setValue(n);
    }

    private void updateStates() {
        boolean bl = this._regexCheckBox.isSelected();
        this._regex.setEnabled(bl);
        this._conversionOrder.setEnabled(bl);
        this._table.setEnabled(bl);
        this._addButton.setEnabled(bl);
        this._upButton.setEnabled(bl);
        this._downButton.setEnabled(bl);
    }

    private void onSelectedGroupsChanged() {
        boolean bl;
        boolean bl2;
        if (this._table.getSelectionModel().getValueIsAdjusting()) {
            return;
        }
        int n = this._table.getSelectedRowCount();
        if (n == 1) {
            int n2 = this._table.convertRowIndexToModel(this._table.getSelectedRow());
            bl2 = n2 != 0;
            bl = n2 != this._tableModel.getRowCount() - 1;
        } else {
            bl = bl2 = n > 0;
        }
        this._upButton.setEnabled(bl2);
        this._downButton.setEnabled(bl);
    }

    private List<RegexGroup> getSelectedGroups() {
        int[] arrn;
        ArrayList<RegexGroup> arrayList = new ArrayList<RegexGroup>();
        for (int n : arrn = this._table.getSelectedRows()) {
            arrayList.add((RegexGroup)this._tableModel.getRow(this._table.convertRowIndexToModel(n)));
        }
        return arrayList;
    }

    private void setSelection(List<RegexGroup> list) {
        ListSelectionModel listSelectionModel = this._table.getSelectionModel();
        listSelectionModel.setValueIsAdjusting(true);
        listSelectionModel.clearSelection();
        for (int i = 0; i < this._table.getRowCount(); ++i) {
            if (!list.contains(this._tableModel.getRow(i))) continue;
            int n = this._table.convertRowIndexToView(i);
            listSelectionModel.addSelectionInterval(n, n);
        }
        listSelectionModel.setValueIsAdjusting(false);
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        LabelWithBackground labelWithBackground = new LabelWithBackground();
        this._plural = new JTextField();
        LabelWithBackground labelWithBackground2 = new LabelWithBackground();
        this._root = new JCheckBox();
        this._conversionPanel = new JPanel();
        LabelWithBackground labelWithBackground3 = new LabelWithBackground();
        this._regex = new JTextField();
        LabelWithBackground labelWithBackground4 = new LabelWithBackground();
        this._conversionOrder = new JSpinner();
        this._regexCheckBox = new JCheckBox();
        LabelWithBackground labelWithBackground5 = new LabelWithBackground();
        this.jScrollPane4 = new JScrollPane();
        this._table = new ETable();
        this.jLabel5 = new LabelWithBackground();
        this._addButton = new JButton();
        this._upButton = new JButton();
        this._downButton = new JButton();
        this.jLabel6 = new JLabel();
        this.setPreferredSize(new Dimension(561, 396));
        this.jPanel1.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(AdvancedSettingsControl.class, (String)"AdvancedSettingsControl.jPanel1.border.title")));
        this.jPanel1.setLayout(new GridBagLayout());
        labelWithBackground.setText(NbBundle.getMessage(AdvancedSettingsControl.class, (String)"AdvancedSettingsControl.jLabel1.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(6, 6, 6, 0);
        this.jPanel1.add((Component)labelWithBackground, gridBagConstraints);
        this._plural.setText(NbBundle.getMessage(AdvancedSettingsControl.class, (String)"AdvancedSettingsControl._plural.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 300;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 0, 6, 6);
        this.jPanel1.add((Component)this._plural, gridBagConstraints);
        labelWithBackground2.setText(NbBundle.getMessage(AdvancedSettingsControl.class, (String)"AdvancedSettingsControl.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(6, 6, 6, 0);
        this.jPanel1.add((Component)labelWithBackground2, gridBagConstraints);
        this._root.setText(NbBundle.getMessage(AdvancedSettingsControl.class, (String)"AdvancedSettingsControl._root.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 3;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(6, 0, 6, 6);
        this.jPanel1.add((Component)this._root, gridBagConstraints);
        this._conversionPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(AdvancedSettingsControl.class, (String)"AdvancedSettingsControl._conversionPanel.border.border.title"))));
        this._conversionPanel.setMinimumSize(new Dimension(540, 214));
        this._conversionPanel.setPreferredSize(new Dimension(540, 434));
        this._conversionPanel.setLayout(new GridBagLayout());
        labelWithBackground3.setText(NbBundle.getMessage(AdvancedSettingsControl.class, (String)"AdvancedSettingsControl.jLabel3.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(6, 6, 6, 0);
        this._conversionPanel.add((Component)labelWithBackground3, gridBagConstraints);
        this._regex.setText(NbBundle.getMessage(AdvancedSettingsControl.class, (String)"AdvancedSettingsControl._regex.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 100;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 0, 6, 6);
        this._conversionPanel.add((Component)this._regex, gridBagConstraints);
        labelWithBackground4.setText(NbBundle.getMessage(AdvancedSettingsControl.class, (String)"AdvancedSettingsControl.jLabel7.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(6, 6, 6, 0);
        this._conversionPanel.add((Component)labelWithBackground4, gridBagConstraints);
        this._conversionOrder.setModel(new SpinnerNumberModel((Number)1000, Integer.valueOf(1), null, (Number)1));
        this._conversionOrder.setEditor(new JSpinner.NumberEditor(this._conversionOrder, "#"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 100;
        gridBagConstraints.anchor = 19;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 0, 6, 6);
        this._conversionPanel.add((Component)this._conversionOrder, gridBagConstraints);
        this._regexCheckBox.setText(NbBundle.getMessage(AdvancedSettingsControl.class, (String)"AdvancedSettingsControl._regexCheckBox.text"));
        this._regexCheckBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                AdvancedSettingsControl.this._regexCheckBoxActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 3;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(6, 0, 6, 6);
        this._conversionPanel.add((Component)this._regexCheckBox, gridBagConstraints);
        labelWithBackground5.setText(NbBundle.getMessage(AdvancedSettingsControl.class, (String)"AdvancedSettingsControl.jLabel4.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(6, 6, 6, 0);
        this._conversionPanel.add((Component)labelWithBackground5, gridBagConstraints);
        this._table.setModel((TableModel)new DefaultTableModel(new Object[][]{{null, null, null, null}, {null, null, null, null}, {null, null, null, null}, {null, null, null, null}}, new String[]{"Title 1", "Title 2", "Title 3", "Title 4"}));
        this._table.setFillsViewportHeight(true);
        this._table.setPreferredScrollableViewportSize(new Dimension(250, 300));
        this.jScrollPane4.setViewportView((Component)this._table);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridheight = 5;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 100;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(6, 0, 6, 6);
        this._conversionPanel.add((Component)this.jScrollPane4, gridBagConstraints);
        this.jLabel5.setText(NbBundle.getMessage(AdvancedSettingsControl.class, (String)"AdvancedSettingsControl.jLabel5.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipady = 6;
        gridBagConstraints.anchor = 24;
        gridBagConstraints.insets = new Insets(6, 6, 6, 0);
        this._conversionPanel.add((Component)this.jLabel5, gridBagConstraints);
        this._addButton.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/entity/manager/resources/Add.png")));
        this._addButton.setText(NbBundle.getMessage(AdvancedSettingsControl.class, (String)"AdvancedSettingsControl._addButton.text"));
        this._addButton.setToolTipText(NbBundle.getMessage(AdvancedSettingsControl.class, (String)"AdvancedSettingsControl._addButton.toolTipText"));
        this._addButton.setFocusPainted(false);
        this._addButton.setMaximumSize(new Dimension(30, 25));
        this._addButton.setMinimumSize(new Dimension(30, 25));
        this._addButton.setPreferredSize(new Dimension(30, 25));
        this._addButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                AdvancedSettingsControl.this._addButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new Insets(6, 0, 0, 6);
        this._conversionPanel.add((Component)this._addButton, gridBagConstraints);
        this._upButton.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/entity/manager/resources/Up.png")));
        this._upButton.setText(NbBundle.getMessage(AdvancedSettingsControl.class, (String)"AdvancedSettingsControl._upButton.text"));
        this._upButton.setToolTipText(NbBundle.getMessage(AdvancedSettingsControl.class, (String)"AdvancedSettingsControl._upButton.toolTipText"));
        this._upButton.setFocusPainted(false);
        this._upButton.setMaximumSize(new Dimension(30, 25));
        this._upButton.setMinimumSize(new Dimension(30, 25));
        this._upButton.setPreferredSize(new Dimension(30, 25));
        this._upButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                AdvancedSettingsControl.this._upButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.insets = new Insets(6, 0, 0, 6);
        this._conversionPanel.add((Component)this._upButton, gridBagConstraints);
        this._downButton.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/entity/manager/resources/Down.png")));
        this._downButton.setText(NbBundle.getMessage(AdvancedSettingsControl.class, (String)"AdvancedSettingsControl.text"));
        this._downButton.setToolTipText(NbBundle.getMessage(AdvancedSettingsControl.class, (String)"AdvancedSettingsControl.toolTipText"));
        this._downButton.setFocusPainted(false);
        this._downButton.setMaximumSize(new Dimension(30, 25));
        this._downButton.setMinimumSize(new Dimension(30, 25));
        this._downButton.setName("");
        this._downButton.setPreferredSize(new Dimension(30, 25));
        this._downButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                AdvancedSettingsControl.this._downButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.insets = new Insets(6, 0, 0, 6);
        this._conversionPanel.add((Component)this._downButton, gridBagConstraints);
        this.jLabel6.setForeground(UIManager.getLookAndFeelDefaults().getColor("7-description-foreground"));
        this.jLabel6.setText(NbBundle.getMessage(AdvancedSettingsControl.class, (String)"AdvancedSettingsControl.jLabel6.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 3;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(6, 30, 6, 6);
        this._conversionPanel.add((Component)this.jLabel6, gridBagConstraints);
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jPanel1, -1, -1, 32767).addComponent(this._conversionPanel, -1, -1, 32767)).addContainerGap()));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(this.jPanel1, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._conversionPanel, -1, 280, 32767).addContainerGap()));
    }

    private void _addButtonActionPerformed(ActionEvent actionEvent) {
        this._tableModel.addRow((Object)new RegexGroup(null));
    }

    private void _regexCheckBoxActionPerformed(ActionEvent actionEvent) {
        this.updateStates();
    }

    private void _upButtonActionPerformed(ActionEvent actionEvent) {
        List list = this._tableModel.getRows();
        List<RegexGroup> list2 = this.getSelectedGroups();
        ListUtil.moveSelectedUp((List)list, list2);
        this._tableModel.fireTableDataChanged();
        this.setSelection(list2);
    }

    private void _downButtonActionPerformed(ActionEvent actionEvent) {
        List list = this._tableModel.getRows();
        List<RegexGroup> list2 = this.getSelectedGroups();
        ListUtil.moveSelectedDown((List)list, list2);
        this._tableModel.fireTableDataChanged();
        this.setSelection(list2);
    }

    private class MappingTableModelListener
    implements TableModelListener {
        private MappingTableModelListener() {
        }

        @Override
        public void tableChanged(TableModelEvent tableModelEvent) {
            if (AdvancedSettingsControl.this._properties != null) {
                AdvancedSettingsControl.this.onSelectedGroupsChanged();
            }
        }
    }

    private class PropertyComboCellEditor
    extends DefaultCellEditor {
        public PropertyComboCellEditor() {
            super(new JComboBox());
        }

        @Override
        public Component getTableCellEditorComponent(JTable jTable, Object object, boolean bl, int n, int n2) {
            Component component = super.getTableCellEditorComponent(jTable, object, bl, n, n2);
            if (component instanceof JComboBox) {
                JComboBox jComboBox = (JComboBox)component;
                DisplayDescriptor displayDescriptor = object instanceof DisplayDescriptor ? (DisplayDescriptor)object : null;
                AdvancedSettingsControl.this.populateCombo(jComboBox, displayDescriptor);
                if (displayDescriptor != null) {
                    jComboBox.setSelectedItem((Object)displayDescriptor);
                }
            }
            return component;
        }
    }

    private class TableSelectionListener
    implements ListSelectionListener {
        private TableSelectionListener() {
        }

        @Override
        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            AdvancedSettingsControl.this.onSelectedGroupsChanged();
        }
    }

}

