/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.util.ui.table.RowTableModel
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.entity.manager.forms.RegexGroup;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.util.ui.table.RowTableModel;
import java.util.List;

public class RegexGroupsTableModel
extends RowTableModel<RegexGroup> {
    private static final String[] COLUMNS = new String[]{"Group", "Property"};

    public RegexGroupsTableModel() {
        super(COLUMNS);
    }

    public Class<?> getColumnClass(int n) {
        return this.getValueAt(0, n).getClass();
    }

    public boolean isCellEditable(int n, int n2) {
        return n2 == 1;
    }

    public Object getValueFor(RegexGroup regexGroup, int n) {
        switch (n) {
            case 0: {
                return this.getRows().indexOf(regexGroup) + 1;
            }
            case 1: {
                DisplayDescriptor displayDescriptor = regexGroup.getProperty();
                return displayDescriptor == null ? "<Ignore>" : displayDescriptor;
            }
        }
        return null;
    }

    public void setValueAt(Object object, int n, int n2) {
        RegexGroup regexGroup = (RegexGroup)this.getRow(n);
        if (object instanceof DisplayDescriptor) {
            regexGroup.setProperty((DisplayDescriptor)object);
        } else {
            regexGroup.setProperty(null);
        }
        this.fireTableCellUpdated(n, n2);
    }
}

