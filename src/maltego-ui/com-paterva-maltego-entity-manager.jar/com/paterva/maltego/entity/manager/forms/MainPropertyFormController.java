/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeNameValidator
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.typing.descriptor.TypeSpecDisplayNameComparator
 *  com.paterva.maltego.util.ui.dialog.DataEditController
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$FinishablePanel
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.entity.manager.forms.MainPropertyForm;
import com.paterva.maltego.entity.manager.forms.PropertyTypes;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeNameValidator;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.typing.descriptor.TypeSpecDisplayNameComparator;
import com.paterva.maltego.util.ui.dialog.DataEditController;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;
import org.openide.util.ImageUtilities;

public class MainPropertyFormController
extends DataEditController<MainPropertyForm, MaltegoEntitySpec>
implements WizardDescriptor.FinishablePanel {
    public MainPropertyFormController() {
        super("entitySpec");
        this.setName("Main Property");
        this.setDescription("Enter the main property details of the new entity in the fields below. By default the main property is displayed in the graph view (this can be changed later at Manage Entities > [...] > Display Settings).");
        this.setImage(ImageUtilities.loadImage((String)"com/paterva/maltego/entity/manager/resources/AddEntity.png".replace(".png", "48.png")));
    }

    protected String getFirstError(MainPropertyForm mainPropertyForm) {
        if (mainPropertyForm.isCreateCustomProperty()) {
            String string = mainPropertyForm.getDisplayName();
            if (string.trim().length() == 0) {
                return "Display name is required";
            }
            String string2 = mainPropertyForm.getUniqueName();
            if ((string2 = string2.trim()).length() == 0) {
                return "Unique type name is required";
            }
            String string3 = TypeNameValidator.checkName((String)string2);
            if (string3 != null) {
                return string3;
            }
            if (mainPropertyForm.getDataType() == null) {
                return "Data type is required";
            }
            if (mainPropertyForm.isSampleValueEmpty()) {
                return "Sample value is required";
            }
        }
        return null;
    }

    protected MainPropertyForm createComponent() {
        final MainPropertyForm mainPropertyForm = new MainPropertyForm();
        mainPropertyForm.addChangeListener(this.changeListener());
        mainPropertyForm.addDisplayNameFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent focusEvent) {
            }

            @Override
            public void focusLost(FocusEvent focusEvent) {
                String string;
                if (!MainPropertyFormController.this.isEditMode() && (string = mainPropertyForm.getUniqueName()).trim().length() == 0) {
                    mainPropertyForm.setUniqueName(MainPropertyFormController.this.createID(mainPropertyForm.getDisplayName()));
                }
            }
        });
        return mainPropertyForm;
    }

    protected boolean isEditMode() {
        return this.getDescriptor().getProperty("editMode") == Boolean.TRUE;
    }

    private String createID(String string) {
        string = string.replaceAll(" ", "");
        string = string.toLowerCase();
        return "properties." + string;
    }

    protected void setData(MainPropertyForm mainPropertyForm, MaltegoEntitySpec maltegoEntitySpec) {
        mainPropertyForm.setDataTypes(PropertyTypes.valueTypes());
        List<MaltegoEntitySpec> list = this.getInheritedSpecs(maltegoEntitySpec);
        mainPropertyForm.setInheritPropertyEnabled(!list.isEmpty());
        Boolean bl = (Boolean)this.getDescriptor().getProperty("inheritMainProperty");
        if (bl == null) {
            bl = !list.isEmpty();
        } else if (list.isEmpty()) {
            bl = Boolean.FALSE;
        }
        mainPropertyForm.setCreateCustomProperty(bl == false);
        DisplayDescriptor displayDescriptor = (DisplayDescriptor)this.getDescriptor().getProperty("mainProperty");
        if (displayDescriptor != null) {
            this.setData(mainPropertyForm, displayDescriptor);
        } else {
            mainPropertyForm.setDisplayName(maltegoEntitySpec.getDisplayName());
            mainPropertyForm.setDataType(TypeRegistry.getDefault().getType(String.class));
            mainPropertyForm.setSampleValue("-");
            mainPropertyForm.setDefaultValue(" ");
        }
    }

    private void setData(MainPropertyForm mainPropertyForm, DisplayDescriptor displayDescriptor) {
        mainPropertyForm.setDataType(displayDescriptor.getTypeDescriptor());
        mainPropertyForm.setSampleValue(displayDescriptor.getSampleValue());
        mainPropertyForm.setDefaultValue(displayDescriptor.getDefaultValue());
        mainPropertyForm.setDisplayName(displayDescriptor.getDisplayName());
        mainPropertyForm.setDescription(displayDescriptor.getDescription());
        mainPropertyForm.setUniqueName(displayDescriptor.getName());
    }

    private List<MaltegoEntitySpec> getInheritedSpecs(MaltegoEntitySpec maltegoEntitySpec) {
        HashSet<MaltegoEntitySpec> hashSet = new HashSet<MaltegoEntitySpec>();
        EntityRegistry entityRegistry = (EntityRegistry)this.getDescriptor().getProperty("entityRegistry");
        Set<MaltegoEntitySpec> set = this.getValidVisibleSpecs(entityRegistry, maltegoEntitySpec.getBaseEntitySpecs());
        for (MaltegoEntitySpec maltegoEntitySpec2 : set) {
            List list = InheritanceHelper.getInheritanceList((SpecRegistry)entityRegistry, (String)maltegoEntitySpec2.getTypeName());
            Set<MaltegoEntitySpec> set2 = this.getValidVisibleSpecs(entityRegistry, list);
            hashSet.addAll(set2);
        }
        ArrayList arrayList = new ArrayList(hashSet);
        Collections.sort(arrayList, new TypeSpecDisplayNameComparator());
        return arrayList;
    }

    private Set<MaltegoEntitySpec> getValidVisibleSpecs(EntityRegistry entityRegistry, List<String> list) {
        HashSet<MaltegoEntitySpec> hashSet = new HashSet<MaltegoEntitySpec>();
        for (String string : list) {
            MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get(string);
            if (maltegoEntitySpec == null || !maltegoEntitySpec.isVisible()) continue;
            hashSet.add(maltegoEntitySpec);
        }
        return hashSet;
    }

    protected void updateData(MainPropertyForm mainPropertyForm, MaltegoEntitySpec maltegoEntitySpec) {
        DisplayDescriptorCollection displayDescriptorCollection = maltegoEntitySpec.getProperties();
        displayDescriptorCollection.clear();
        DisplayDescriptor displayDescriptor = null;
        if (mainPropertyForm.getDataType() != null) {
            displayDescriptor = MainPropertyFormController.createProperty(mainPropertyForm.getUniqueName(), mainPropertyForm.getDataType());
            displayDescriptor.setDisplayName(mainPropertyForm.getDisplayName());
            displayDescriptor.setDescription(mainPropertyForm.getDescription());
            displayDescriptor.setSampleValue(mainPropertyForm.getSampleValue());
            displayDescriptor.setDefaultValue(mainPropertyForm.getDefaultValue());
        }
        this.getDescriptor().putProperty("mainProperty", (Object)displayDescriptor);
        this.getDescriptor().putProperty("inheritMainProperty", (Object)(!mainPropertyForm.isCreateCustomProperty()));
        if (mainPropertyForm.isCreateCustomProperty() && displayDescriptor != null) {
            displayDescriptorCollection.add(displayDescriptor);
            maltegoEntitySpec.setValueProperty(displayDescriptor);
        }
    }

    private static DisplayDescriptor createProperty(String string, TypeDescriptor typeDescriptor) {
        DisplayDescriptor displayDescriptor = new DisplayDescriptor(typeDescriptor.getType(), string);
        return displayDescriptor;
    }

    public boolean isFinishPanel() {
        return true;
    }

    private boolean checkChars(String string) {
        for (char c : string.toCharArray()) {
            if (Character.isLetter(c) || c == '.' || Character.isDigit(c)) continue;
            return false;
        }
        return true;
    }

}

