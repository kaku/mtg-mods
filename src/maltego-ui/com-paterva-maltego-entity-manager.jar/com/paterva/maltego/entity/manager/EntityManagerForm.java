/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.registry.RestoreEntitiesAction
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.table.ETableColumnSelectionDecorator
 *  com.paterva.maltego.util.ui.table.EditableTableDecorator
 *  com.paterva.maltego.util.ui.table.ImageTableCellRenderer
 *  com.paterva.maltego.util.ui.table.PaddedTableCellRenderer
 *  com.paterva.maltego.util.ui.table.TableButtonEvent
 *  com.paterva.maltego.util.ui.table.TableButtonListener
 *  org.netbeans.swing.etable.ETable
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.entity.manager;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.manager.EntityRegistryTableModel;
import com.paterva.maltego.entity.manager.forms.EditEntityTypeAction;
import com.paterva.maltego.entity.manager.forms.NewEntityTypeBasicAction;
import com.paterva.maltego.entity.manager.imex.ExportEntitiesAction;
import com.paterva.maltego.entity.manager.imex.ImportEntitiesAction;
import com.paterva.maltego.entity.registry.RestoreEntitiesAction;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.table.ETableColumnSelectionDecorator;
import com.paterva.maltego.util.ui.table.EditableTableDecorator;
import com.paterva.maltego.util.ui.table.ImageTableCellRenderer;
import com.paterva.maltego.util.ui.table.PaddedTableCellRenderer;
import com.paterva.maltego.util.ui.table.TableButtonEvent;
import com.paterva.maltego.util.ui.table.TableButtonListener;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.netbeans.swing.etable.ETable;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;

public class EntityManagerForm
extends JPanel {
    private EntityRegistry _registry;
    private final EntityRegistryTableModel _model;
    private JButton _exportButton;
    private JButton _importButton;
    private JButton _newButton;
    private JButton _restoreButton;
    private ETable _table;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JScrollPane jScrollPane1;

    public EntityManagerForm() {
        this(null);
    }

    public EntityManagerForm(EntityRegistry entityRegistry) {
        this._registry = entityRegistry;
        this.initComponents();
        this._restoreButton.setVisible("true".equals(System.getProperty("maltego.entities.can_restore", "false")));
        this._model = new EntityRegistryTableModel(this.registry());
        this._table.setModel((TableModel)((Object)this._model));
        EditableTableDecorator editableTableDecorator = new EditableTableDecorator();
        editableTableDecorator.addEditDelete((JTable)this._table, new TableButtonListener(){

            public void actionPerformed(TableButtonEvent tableButtonEvent) {
                int[] arrn = tableButtonEvent.getSelectedRows();
                ArrayList<Object> arrayList = new ArrayList<Object>(arrn.length);
                for (int n : arrn) {
                    arrayList.add(EntityManagerForm.this._model.getRow(EntityManagerForm.this._table.convertRowIndexToModel(n)));
                }
                String string = arrayList.size() > 1 ? "Would you like to delete these " + arrayList.size() + " selected entities?" + StringUtilities.newLine() + "This might affect the way your graphs are displayed." : "Would you like to delete the " + ((MaltegoEntitySpec)arrayList.get(0)).getDisplayName() + " entity?" + StringUtilities.newLine() + "This might affect the way your graphs are displayed.";
                if (DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Confirmation((Object)string, "Delete entity?", 1)) == NotifyDescriptor.YES_OPTION) {
                    for (MaltegoEntitySpec maltegoEntitySpec : arrayList) {
                        EntityManagerForm.this.registry().remove(maltegoEntitySpec.getTypeName());
                    }
                }
            }
        }, new TableButtonListener(){

            public void actionPerformed(TableButtonEvent tableButtonEvent) {
                MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)EntityManagerForm.this._model.getRow(EntityManagerForm.this._table.convertRowIndexToModel(tableButtonEvent.getSelectedRows()[0]));
                ((EditEntityTypeAction)SystemAction.get(EditEntityTypeAction.class)).edit(maltegoEntitySpec);
            }
        });
        TableColumnModel tableColumnModel = this._table.getColumnModel();
        TableColumn tableColumn = tableColumnModel.getColumn(0);
        tableColumn.setPreferredWidth(20);
        ETableColumnSelectionDecorator eTableColumnSelectionDecorator = new ETableColumnSelectionDecorator();
        eTableColumnSelectionDecorator.makeSelectable(this._table, EntityRegistryTableModel.Columns, new boolean[]{true, true, true, false, false, false}, new String[]{"Image", null, null, null, null, null});
        this._table.setDefaultRenderer(Image.class, (TableCellRenderer)new ImageTableCellRenderer());
        PaddedTableCellRenderer paddedTableCellRenderer = new PaddedTableCellRenderer();
        this._table.setDefaultRenderer(String.class, (TableCellRenderer)paddedTableCellRenderer);
        this._table.setAutoCreateColumnsFromModel(false);
    }

    private EntityRegistry registry() {
        if (this._registry == null) {
            return EntityRegistry.getDefault();
        }
        return this._registry;
    }

    public ETable getEntityTable() {
        return this._table;
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this._newButton = new JButton();
        this._importButton = new JButton();
        this._exportButton = new JButton();
        this._restoreButton = new JButton();
        this.jPanel3 = new JPanel();
        this.jPanel2 = new JPanel();
        this.jScrollPane1 = new JScrollPane();
        this._table = new ETable();
        this.setLayout(new GridBagLayout());
        this.jPanel1.setLayout(new GridBagLayout());
        this._newButton.setText(NbBundle.getMessage(EntityManagerForm.class, (String)"EntityManagerForm._newButton.text"));
        this._newButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                EntityManagerForm.this._newButtonActionPerformed(actionEvent);
            }
        });
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 6, 10, 10);
        this.jPanel1.add((Component)this._newButton, gridBagConstraints);
        this._importButton.setText(NbBundle.getMessage(EntityManagerForm.class, (String)"EntityManagerForm._importButton.text"));
        this._importButton.setMaximumSize(new Dimension(84, 23));
        this._importButton.setMinimumSize(new Dimension(84, 23));
        this._importButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                EntityManagerForm.this._importButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 10, 10, 0);
        this.jPanel1.add((Component)this._importButton, gridBagConstraints);
        this._exportButton.setText(NbBundle.getMessage(EntityManagerForm.class, (String)"EntityManagerForm._exportButton.text"));
        this._exportButton.setMaximumSize(new Dimension(84, 23));
        this._exportButton.setMinimumSize(new Dimension(84, 23));
        this._exportButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                EntityManagerForm.this._exportButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 6, 10, 0);
        this.jPanel1.add((Component)this._exportButton, gridBagConstraints);
        this._restoreButton.setText(NbBundle.getMessage(EntityManagerForm.class, (String)"EntityManagerForm._restoreButton.text"));
        this._restoreButton.setMaximumSize(new Dimension(84, 23));
        this._restoreButton.setMinimumSize(new Dimension(84, 23));
        this._restoreButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                EntityManagerForm.this._restoreButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 6, 10, 0);
        this.jPanel1.add((Component)this._restoreButton, gridBagConstraints);
        GroupLayout groupLayout = new GroupLayout(this.jPanel3);
        this.jPanel3.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        this.jPanel1.add((Component)this.jPanel3, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        this.add((Component)this.jPanel1, gridBagConstraints);
        this.jPanel2.setLayout(new BorderLayout());
        this.jScrollPane1.setBorder(null);
        this._table.setModel((TableModel)new DefaultTableModel(new Object[][]{{null, null, null, null}, {null, null, null, null}, {null, null, null, null}, {null, null, null, null}}, new String[]{"Title 1", "Title 2", "Title 3", "Title 4"}));
        this._table.setRowHeight(20);
        this.jScrollPane1.setViewportView((Component)this._table);
        this.jPanel2.add((Component)this.jScrollPane1, "Center");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 10, 10, 10);
        this.add((Component)this.jPanel2, gridBagConstraints);
    }

    private void _newButtonActionPerformed(ActionEvent actionEvent) {
        ((NewEntityTypeBasicAction)SystemAction.get(NewEntityTypeBasicAction.class)).performAction();
    }

    private void _importButtonActionPerformed(ActionEvent actionEvent) {
        ((ImportEntitiesAction)SystemAction.get(ImportEntitiesAction.class)).perform();
    }

    private void _exportButtonActionPerformed(ActionEvent actionEvent) {
        ((ExportEntitiesAction)SystemAction.get(ExportEntitiesAction.class)).perform();
    }

    private void _restoreButtonActionPerformed(ActionEvent actionEvent) {
        String string = "Restore Default Entities?";
        String string2 = "Are you sure you want to restore all the default entities? Note that custom entities will not be affected but modification to the default entities will be reverted.";
        NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)string2, string, 2);
        if (NotifyDescriptor.OK_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation))) {
            ((RestoreEntitiesAction)SystemAction.get(RestoreEntitiesAction.class)).perform(true);
        }
    }

}

