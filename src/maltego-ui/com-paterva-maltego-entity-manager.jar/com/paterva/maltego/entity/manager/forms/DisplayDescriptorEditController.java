/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.util.ui.dialog.DataEditController
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$FinishablePanel
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.manager.forms.DisplayDescriptorEditControl;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.util.ui.dialog.DataEditController;
import java.awt.Component;
import org.openide.WizardDescriptor;

class DisplayDescriptorEditController
extends DataEditController<DisplayDescriptorEditControl, MaltegoEntitySpec>
implements WizardDescriptor.FinishablePanel {
    public DisplayDescriptorEditController() {
        super("entitySpec");
        this.setName("Additional Properties");
        this.setDescription("Add additional properties to the entity which will be visibile in the Property View.");
    }

    protected DisplayDescriptorEditControl createComponent() {
        DisplayDescriptorEditControl displayDescriptorEditControl = new DisplayDescriptorEditControl();
        return displayDescriptorEditControl;
    }

    protected void setData(DisplayDescriptorEditControl displayDescriptorEditControl, MaltegoEntitySpec maltegoEntitySpec) {
        displayDescriptorEditControl.setDescriptors(maltegoEntitySpec.getProperties());
        displayDescriptorEditControl.setValueDescriptor(maltegoEntitySpec.getValueProperty());
    }

    protected void updateData(DisplayDescriptorEditControl displayDescriptorEditControl, MaltegoEntitySpec maltegoEntitySpec) {
    }

    public boolean isFinishPanel() {
        return true;
    }
}

