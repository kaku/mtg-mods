/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  com.paterva.maltego.util.ui.dialog.ChangeEventPropagator
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import com.paterva.maltego.util.ui.dialog.ChangeEventPropagator;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeListener;
import org.openide.util.NbBundle;

public class DisplayInfoForm
extends JPanel {
    private final ChangeEventPropagator _changeSupport;
    private final Color _descriptionFg;
    private JPanel _descriptionInPanel;
    private JComboBox _displayValue;
    private JComboBox _image;
    private JComboBox _value;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel4;

    public DisplayInfoForm(boolean bl) {
        this._changeSupport = new ChangeEventPropagator((Object)this);
        this._descriptionFg = UIManager.getLookAndFeelDefaults().getColor("7-description-foreground");
        this.initComponents();
        this.setName("Display Settings");
        this._value.addActionListener((ActionListener)this._changeSupport);
        this._displayValue.addActionListener((ActionListener)this._changeSupport);
        this._image.addActionListener((ActionListener)this._changeSupport);
        this._descriptionInPanel.setVisible(bl);
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.jLabel1 = new LabelWithBackground();
        this._value = new JComboBox();
        this.jLabel2 = new LabelWithBackground();
        this._displayValue = new JComboBox();
        this.jLabel3 = new LabelWithBackground();
        this._image = new JComboBox();
        this.jLabel5 = new JLabel();
        this.jLabel6 = new JLabel();
        this.jLabel7 = new JLabel();
        this.jPanel2 = new JPanel();
        this.jPanel4 = new JPanel();
        this._descriptionInPanel = new JPanel();
        this.jLabel4 = new JLabel();
        this.setLayout(new GridBagLayout());
        this.jPanel1.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(DisplayInfoForm.class, (String)"DisplayInfoForm.jPanel1.border.title")));
        this.jPanel1.setLayout(new GridBagLayout());
        this.jLabel1.setText(NbBundle.getMessage(DisplayInfoForm.class, (String)"DisplayInfoForm.jLabel1.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 10, 0, 0);
        this.jPanel1.add((Component)this.jLabel1, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 267;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 0, 0, 10);
        this.jPanel1.add((Component)this._value, gridBagConstraints);
        this.jLabel2.setText(NbBundle.getMessage(DisplayInfoForm.class, (String)"DisplayInfoForm.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 10, 0, 0);
        this.jPanel1.add((Component)this.jLabel2, gridBagConstraints);
        this._displayValue.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 239;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 0, 0, 10);
        this.jPanel1.add((Component)this._displayValue, gridBagConstraints);
        this.jLabel3.setText(NbBundle.getMessage(DisplayInfoForm.class, (String)"DisplayInfoForm.jLabel3.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 10, 0, 0);
        this.jPanel1.add((Component)this.jLabel3, gridBagConstraints);
        this._image.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 239;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 0, 0, 10);
        this.jPanel1.add((Component)this._image, gridBagConstraints);
        this.jLabel5.setForeground(this._descriptionFg);
        this.jLabel5.setText(NbBundle.getMessage(DisplayInfoForm.class, (String)"DisplayInfoForm.jLabel5.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 6, 6, 0);
        this.jPanel1.add((Component)this.jLabel5, gridBagConstraints);
        this.jLabel6.setForeground(this._descriptionFg);
        this.jLabel6.setText(NbBundle.getMessage(DisplayInfoForm.class, (String)"DisplayInfoForm.jLabel6.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 6, 6, 0);
        this.jPanel1.add((Component)this.jLabel6, gridBagConstraints);
        this.jLabel7.setForeground(this._descriptionFg);
        this.jLabel7.setText(NbBundle.getMessage(DisplayInfoForm.class, (String)"DisplayInfoForm.jLabel7.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 6, 6, 0);
        this.jPanel1.add((Component)this.jLabel7, gridBagConstraints);
        this.jPanel2.setName("");
        this.jPanel2.setPreferredSize(new Dimension(0, 0));
        GroupLayout groupLayout = new GroupLayout(this.jPanel2);
        this.jPanel2.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.jPanel1.add((Component)this.jPanel2, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.ipadx = 26;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 10, 10, 10);
        this.add((Component)this.jPanel1, gridBagConstraints);
        GroupLayout groupLayout2 = new GroupLayout(this.jPanel4);
        this.jPanel4.setLayout(groupLayout2);
        groupLayout2.setHorizontalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        groupLayout2.setVerticalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.jPanel4, gridBagConstraints);
        this._descriptionInPanel.setPreferredSize(new Dimension(300, 20));
        this._descriptionInPanel.setLayout(new BorderLayout());
        this.jLabel4.setText(NbBundle.getMessage(DisplayInfoForm.class, (String)"DisplayInfoForm.jLabel4.text"));
        this._descriptionInPanel.add((Component)this.jLabel4, "Center");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.insets = new Insets(15, 10, 0, 0);
        this.add((Component)this._descriptionInPanel, gridBagConstraints);
    }

    public DisplayDescriptor getValueProperty() {
        return (DisplayDescriptor)this._value.getSelectedItem();
    }

    public DisplayDescriptor getDisplayValueProperty() {
        return (DisplayDescriptor)this._displayValue.getSelectedItem();
    }

    public DisplayDescriptor getImageProperty() {
        return (DisplayDescriptor)this._image.getSelectedItem();
    }

    public void setValueProperty(DisplayDescriptor displayDescriptor) {
        this._value.setSelectedItem((Object)displayDescriptor);
    }

    public void setDisplayValueProperty(DisplayDescriptor displayDescriptor) {
        this._displayValue.setSelectedItem((Object)displayDescriptor);
    }

    public void setImageProperty(DisplayDescriptor displayDescriptor) {
        this._image.setSelectedItem((Object)displayDescriptor);
    }

    public void setValueProperties(DisplayDescriptor[] arrdisplayDescriptor) {
        this._value.setModel(new DefaultComboBoxModel<DisplayDescriptor>((E[])arrdisplayDescriptor));
    }

    public void setDisplayValueProperties(DisplayDescriptor[] arrdisplayDescriptor) {
        this._displayValue.setModel(new DefaultComboBoxModel<DisplayDescriptor>((E[])arrdisplayDescriptor));
    }

    public void setImageProperties(DisplayDescriptor[] arrdisplayDescriptor) {
        this._image.setModel(new DefaultComboBoxModel<DisplayDescriptor>((E[])arrdisplayDescriptor));
    }
}

