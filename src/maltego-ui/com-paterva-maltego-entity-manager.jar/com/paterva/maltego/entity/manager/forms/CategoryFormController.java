/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$FinishablePanel
 */
package com.paterva.maltego.entity.manager.forms;

import com.paterva.maltego.entity.manager.forms.CategoryForm;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;

public class CategoryFormController
extends ValidatingController<CategoryForm>
implements WizardDescriptor.FinishablePanel {
    public CategoryFormController() {
        this.setName("Select Category");
        this.setDescription("Add the entity(ies) to one of the following categories.");
    }

    protected String getFirstError(CategoryForm categoryForm) {
        return null;
    }

    protected CategoryForm createComponent() {
        CategoryForm categoryForm = new CategoryForm();
        categoryForm.addChangeListener(this.changeListener());
        return categoryForm;
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        ((CategoryForm)this.component()).setCategories((String[])wizardDescriptor.getProperty("categories"));
        ((CategoryForm)this.component()).setSelectedCategory((String)wizardDescriptor.getProperty("entityCategory"));
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        wizardDescriptor.putProperty("entityCategory", (Object)((CategoryForm)this.component()).getSelectedCategory());
    }

    public boolean isFinishPanel() {
        return true;
    }
}

