/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FastURL
 */
package com.paterva.maltego.transform.discovery;

import com.paterva.maltego.util.FastURL;

public class TransformServerReference {
    private final String _seedUrl;
    private final FastURL _baseUrl;

    public TransformServerReference(String string, FastURL fastURL) {
        this._seedUrl = string;
        this._baseUrl = fastURL;
    }

    public String getSeedUrl() {
        return this._seedUrl;
    }

    public FastURL getBaseUrl() {
        return this._baseUrl;
    }
}

