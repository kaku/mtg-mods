/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.discovery;

public class DiscoveryResult<T> {
    private T[] _data;
    private Exception[] _errors;

    public DiscoveryResult(T[] arrT, Exception[] arrexception) {
        this._data = arrT;
        this._errors = arrexception;
    }

    public DiscoveryResult(T[] arrT) {
        this(arrT, new Exception[0]);
    }

    public T[] getData() {
        return this._data;
    }

    public Exception[] getErrors() {
        return this._errors;
    }
}

