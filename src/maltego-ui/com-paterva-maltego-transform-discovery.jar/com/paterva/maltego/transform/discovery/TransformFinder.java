/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.discovery;

import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.discovery.DiscoveryException;
import com.paterva.maltego.transform.discovery.DiscoveryResult;
import com.paterva.maltego.transform.discovery.ProgressCallback;
import com.paterva.maltego.transform.discovery.TransformServerDetail;
import com.paterva.maltego.transform.discovery.TransformServerListing;
import com.paterva.maltego.transform.discovery.TransformServerReference;
import org.openide.util.Lookup;

public abstract class TransformFinder {
    public static TransformFinder getDefault() {
        TransformFinder transformFinder = (TransformFinder)Lookup.getDefault().lookup(TransformFinder.class);
        if (transformFinder == null) {
            return new TrivialFinder();
        }
        return transformFinder;
    }

    public DiscoveryResult<TransformServerReference> findServers(TransformSeed[] arrtransformSeed) throws DiscoveryException {
        return this.findServers(arrtransformSeed, null);
    }

    public abstract DiscoveryResult<TransformServerReference> findServers(TransformSeed[] var1, ProgressCallback var2) throws DiscoveryException;

    public DiscoveryResult<TransformServerDetail> getDetails(TransformServerReference[] arrtransformServerReference) throws DiscoveryException {
        return this.getDetails(arrtransformServerReference, null);
    }

    public abstract DiscoveryResult<TransformServerDetail> getDetails(TransformServerReference[] var1, ProgressCallback var2) throws DiscoveryException;

    public DiscoveryResult<TransformServerListing> listTransforms(TransformServerDetail[] arrtransformServerDetail) throws DiscoveryException {
        return this.listTransforms(arrtransformServerDetail, null);
    }

    public abstract DiscoveryResult<TransformServerListing> listTransforms(TransformServerDetail[] var1, ProgressCallback var2) throws DiscoveryException;

    private static class TrivialFinder
    extends TransformFinder {
        private TrivialFinder() {
        }

        @Override
        public DiscoveryResult<TransformServerReference> findServers(TransformSeed[] arrtransformSeed, ProgressCallback progressCallback) {
            throw new UnsupportedOperationException("No TransformFinder registered.");
        }

        @Override
        public DiscoveryResult<TransformServerDetail> getDetails(TransformServerReference[] arrtransformServerReference, ProgressCallback progressCallback) {
            throw new UnsupportedOperationException("No TransformFinder registered.");
        }

        @Override
        public DiscoveryResult<TransformServerListing> listTransforms(TransformServerDetail[] arrtransformServerDetail, ProgressCallback progressCallback) {
            throw new UnsupportedOperationException("No TransformFinder registered.");
        }
    }

}

