/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.discovery;

public class DiscoveryException
extends Exception {
    public DiscoveryException() {
    }

    public DiscoveryException(String string) {
        super(string);
    }

    public DiscoveryException(String string, Throwable throwable) {
        super(string, throwable);
    }

    public DiscoveryException(Throwable throwable) {
        super(throwable.getMessage(), throwable);
    }
}

