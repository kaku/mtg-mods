/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryEntityMerger
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules$EntityRule
 *  com.paterva.maltego.archive.mtz.discover.MtzDiscoveryItems
 *  com.paterva.maltego.archive.mtz.discover.MtzDiscoveryProvider
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.imgfactoryapi.IconUtils
 *  com.paterva.maltego.pws.api.PublicWebService
 *  com.paterva.maltego.pws.api.PublicWebServices
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.registry.HubSeedRegistry
 *  com.paterva.maltego.transform.descriptor.ProtocolVersion
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformRepository
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.descriptor.TransformServerAuthentication
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.transform.descriptor.TransformSetRepository
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.ImageUtils
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.transform.discovery.impl;

import com.paterva.maltego.archive.mtz.discover.DiscoveryEntityMerger;
import com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules;
import com.paterva.maltego.archive.mtz.discover.MtzDiscoveryItems;
import com.paterva.maltego.archive.mtz.discover.MtzDiscoveryProvider;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.imgfactoryapi.IconUtils;
import com.paterva.maltego.pws.api.PublicWebService;
import com.paterva.maltego.pws.api.PublicWebServices;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.registry.HubSeedRegistry;
import com.paterva.maltego.transform.descriptor.ProtocolVersion;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformServerAuthentication;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.descriptor.TransformSetRepository;
import com.paterva.maltego.transform.discovery.TransformServerListing;
import com.paterva.maltego.transform.discovery.TransformUpdater;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.ImageUtils;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DefaultTransformUpdater
extends TransformUpdater {
    private static final Logger LOGGER = Logger.getLogger(DefaultTransformUpdater.class.getName());

    @Override
    public void removeMissing(TransformServerListing[] arrtransformServerListing) {
        TransformServerRegistry transformServerRegistry = TransformServerRegistry.getDefault();
        for (TransformServerListing transformServerListing : arrtransformServerListing) {
            TransformServerInfo transformServerInfo = transformServerRegistry.get(transformServerListing.getBaseUrl());
            if (transformServerInfo == null) continue;
            HashSet<String> hashSet = new HashSet<String>();
            for (String string : transformServerInfo.getTransforms()) {
                boolean bl = false;
                for (TransformDescriptor transformDescriptor : transformServerListing.getTransforms()) {
                    if (!string.equals(transformDescriptor.getName())) continue;
                    bl = true;
                    break;
                }
                if (bl) continue;
                hashSet.add(string);
            }
            transformServerInfo.getTransforms().removeAll(hashSet);
        }
    }

    @Override
    public void updateServers(TransformServerListing[] arrtransformServerListing) {
        TransformServerRegistry transformServerRegistry = TransformServerRegistry.getDefault();
        for (TransformServerListing transformServerListing : arrtransformServerListing) {
            TransformServerInfo transformServerInfo = transformServerRegistry.get(transformServerListing.getBaseUrl());
            if (transformServerInfo == null) {
                transformServerInfo = new TransformServerInfo(transformServerListing.getSeedUrl(), transformServerListing.getBaseUrl(), transformServerListing.getName());
            } else {
                transformServerInfo.addSeedUrl(transformServerListing.getSeedUrl());
            }
            transformServerRegistry.put(transformServerInfo);
        }
    }

    @Override
    public void update(HubSeedDescriptor hubSeedDescriptor, TransformServerListing[] arrtransformServerListing, DiscoveryMergingRules discoveryMergingRules) throws IOException {
        TransformServerRegistry transformServerRegistry = TransformServerRegistry.getDefault();
        for (TransformServerListing transformServerListing : arrtransformServerListing) {
            TransformServerInfo transformServerInfo = transformServerRegistry.get(transformServerListing.getBaseUrl());
            DefaultTransformUpdater.updateTas(hubSeedDescriptor, transformServerInfo, transformServerListing);
            transformServerRegistry.put(transformServerInfo);
            this.updateDescriptors(transformServerListing.getTransforms());
            this.createNewSets(transformServerListing.getSets());
            this.updateSets(transformServerListing.getTransforms());
            this.updateEntities(transformServerListing.getEntities(), discoveryMergingRules);
            this.updateMtzItems(transformServerListing.getMtzDiscoveryItems());
            this.updateAuthenticators(transformServerListing.getTransforms(), transformServerListing.getPublicWebServices());
        }
    }

    private static void updateTas(HubSeedDescriptor hubSeedDescriptor, TransformServerInfo transformServerInfo, TransformServerListing transformServerListing) {
        transformServerInfo.setAuthentication(transformServerListing.getAuthentication());
        transformServerInfo.setDescription(transformServerListing.getDescription());
        transformServerInfo.setDisplayName(transformServerListing.getName());
        transformServerInfo.setLastSync(new Date(System.currentTimeMillis()));
        transformServerInfo.setProtocolVersion(transformServerListing.getProtocolVersion());
        for (TransformDescriptor transformDescriptor : transformServerListing.getTransforms()) {
            if (hubSeedDescriptor != null) {
                List list = HubSeedRegistry.getDefault().getHubSeeds(transformDescriptor);
                for (HubSeedDescriptor hubSeedDescriptor2 : list) {
                    if (hubSeedDescriptor.equals((Object)hubSeedDescriptor2)) continue;
                    transformServerListing.getDuplicateTransforms().put(transformDescriptor, hubSeedDescriptor2);
                }
            }
            transformServerInfo.getTransforms().add(transformDescriptor.getName());
        }
    }

    private void updateEntities(Set<MaltegoEntitySpec> set, DiscoveryMergingRules discoveryMergingRules) {
        EntityRegistry entityRegistry = EntityRegistry.getDefault();
        Iterator<MaltegoEntitySpec> iterator = set.iterator();
        while (iterator.hasNext()) {
            MaltegoEntitySpec maltegoEntitySpec = iterator.next();
            boolean bl = DiscoveryEntityMerger.merge((EntityRegistry)entityRegistry, (MaltegoEntitySpec)maltegoEntitySpec, (DiscoveryMergingRules.EntityRule)discoveryMergingRules.getEntityRule());
            if (bl) continue;
            iterator.remove();
        }
    }

    private void updateMtzItems(Set<MtzDiscoveryItems> set) {
        Iterator<MtzDiscoveryItems> iterator = set.iterator();
        while (iterator.hasNext()) {
            MtzDiscoveryItems mtzDiscoveryItems = iterator.next();
            MtzDiscoveryProvider mtzDiscoveryProvider = mtzDiscoveryItems.getProvider();
            mtzDiscoveryProvider.apply(mtzDiscoveryItems);
            if (!mtzDiscoveryItems.isEmpty()) continue;
            iterator.remove();
        }
    }

    private void updateDescriptors(Set<TransformDescriptor> set) throws IOException {
        TransformRepository transformRepository = TransformRepositoryRegistry.getDefault().getOrCreateRepository("Remote");
        for (TransformDescriptor transformDescriptor : set) {
            transformRepository.put(transformDescriptor);
        }
    }

    private void createNewSets(Set<TransformSet> set) throws IOException {
        TransformSetRepository transformSetRepository = TransformSetRepository.getDefault();
        for (TransformSet transformSet : set) {
            if (transformSetRepository.contains(transformSet.getName())) continue;
            transformSetRepository.put(transformSet);
        }
    }

    private void updateSets(Set<TransformDescriptor> set) throws IOException {
        TransformSetRepository transformSetRepository = TransformSetRepository.getDefault();
        for (TransformDescriptor transformDescriptor : set) {
            for (String string : transformDescriptor.getDefaultSets()) {
                TransformSet transformSet = transformSetRepository.get(string);
                if (transformSet == null) {
                    transformSet = new TransformSet(string);
                }
                transformSet.addTransform(transformDescriptor.getName());
                transformSetRepository.put(transformSet);
            }
        }
    }

    @Override
    public TransformServerListing[] getNew(TransformServerListing[] arrtransformServerListing, DiscoveryMergingRules discoveryMergingRules) {
        ArrayList<TransformServerListing> arrayList = new ArrayList<TransformServerListing>();
        TransformServerRegistry transformServerRegistry = TransformServerRegistry.getDefault();
        TransformRepositoryRegistry transformRepositoryRegistry = TransformRepositoryRegistry.getDefault();
        for (TransformServerListing transformServerListing : arrtransformServerListing) {
            FastURL fastURL = transformServerListing.getBaseUrl();
            LOGGER.log(Level.INFO, "server: {0}", (Object)fastURL);
            TransformServerInfo transformServerInfo = transformServerRegistry.get(fastURL);
            Set<TransformDescriptor> set = transformServerListing.getTransforms();
            Set<MtzDiscoveryItems> set2 = transformServerListing.getMtzDiscoveryItems();
            Map<PublicWebService, String> map = transformServerListing.getPublicWebServices();
            if (transformServerInfo != null) {
                Object object;
                TransformDescriptor transformDescriptor22;
                int n = set.size();
                TransformDescriptor[] arrtransformDescriptor = set.toArray((T[])new TransformDescriptor[set.size()]);
                set.clear();
                for (TransformDescriptor transformDescriptor22 : arrtransformDescriptor) {
                    object = transformRepositoryRegistry.findTransform(transformDescriptor22.getName());
                    if (transformDescriptor22.isCopy((TransformDescriptor)object)) continue;
                    set.add(transformDescriptor22);
                }
                int n2 = set.size();
                LOGGER.log(Level.INFO, "  {0}/{1} Transforms", new Object[]{n2, n});
                HashSet<MtzDiscoveryItems> hashSet = new HashSet<MtzDiscoveryItems>(set2);
                set2.clear();
                Object object2 = hashSet.iterator();
                while (object2.hasNext()) {
                    transformDescriptor22 = object2.next();
                    object = transformDescriptor22.getProvider();
                    int n3 = transformDescriptor22.size();
                    transformDescriptor22 = object.getNewAndMerged((MtzDiscoveryItems)transformDescriptor22);
                    int n4 = transformDescriptor22.size();
                    LOGGER.log(Level.INFO, "  {0}/{1} {2}", new Object[]{n4, n3, transformDescriptor22.getDescription()});
                    if (transformDescriptor22.isEmpty()) continue;
                    set2.add((MtzDiscoveryItems)transformDescriptor22);
                }
                object2 = new HashMap<PublicWebService, String>(map);
                map.clear();
                transformDescriptor22 = PublicWebServices.getDefault();
                for (Map.Entry entry : object2.entrySet()) {
                    if (transformDescriptor22.get(((PublicWebService)entry.getKey()).getName()) != null) continue;
                    map.put((PublicWebService)entry.getKey(), (String)entry.getValue());
                }
                LOGGER.log(Level.INFO, "  {0}/{1} Services", new Object[]{map.size(), object2.size()});
            }
            if (set.isEmpty() && set2.isEmpty() && map.isEmpty()) continue;
            arrayList.add(transformServerListing);
        }
        return arrayList.toArray(new TransformServerListing[arrayList.size()]);
    }

    private void updateAuthenticators(Set<TransformDescriptor> set, Map<PublicWebService, String> map) throws IOException {
        Set<String> set2 = this.getAuthenticatorNames(set);
        this.removeMissing(map, set2);
        this.importAuthenticators("OAuth", map);
    }

    private Set<String> getAuthenticatorNames(Set<TransformDescriptor> set) {
        HashSet<String> hashSet = new HashSet<String>();
        for (TransformDescriptor transformDescriptor : set) {
            String string = transformDescriptor.getAuthenticator();
            if (StringUtilities.isNullOrEmpty((String)string)) continue;
            hashSet.add(string);
        }
        TransformRepository transformRepository = TransformRepositoryRegistry.getDefault().getRepository("Remote");
        hashSet.addAll(this.getAuthenticators(transformRepository));
        transformRepository = TransformRepositoryRegistry.getDefault().getRepository("Local");
        hashSet.addAll(this.getAuthenticators(transformRepository));
        return hashSet;
    }

    private Set<String> getAuthenticators(TransformRepository transformRepository) {
        HashSet<String> hashSet = new HashSet<String>();
        if (transformRepository != null) {
            for (TransformDefinition transformDefinition : transformRepository.getAll()) {
                String string = transformDefinition.getAuthenticator();
                if (StringUtilities.isNullOrEmpty((String)string)) continue;
                hashSet.add(string);
            }
        }
        return hashSet;
    }

    private void removeMissing(Map<PublicWebService, String> map, Set<String> set) {
        Set<Map.Entry<PublicWebService, String>> set2 = map.entrySet();
        Iterator<Map.Entry<PublicWebService, String>> iterator = set2.iterator();
        while (iterator.hasNext()) {
            PublicWebService publicWebService = iterator.next().getKey();
            String string = publicWebService.getName();
            if (set.contains(string)) continue;
            iterator.remove();
        }
    }

    private void importAuthenticators(String string, Map<PublicWebService, String> map) throws IOException {
        PublicWebServices publicWebServices = PublicWebServices.getDefault();
        Iterator<Map.Entry<PublicWebService, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<PublicWebService, String> entry = iterator.next();
            PublicWebService publicWebService = entry.getKey();
            String string2 = this.getIconName(publicWebService);
            publicWebService.setIconName(string2);
            String string3 = entry.getValue();
            this.importIcon(string2, string3);
            if (publicWebServices.replace(string, publicWebService.getName(), publicWebService)) continue;
            iterator.remove();
        }
    }

    private void importIcon(String string, String string2) throws IOException {
        IconRegistry iconRegistry = IconRegistry.getDefault();
        if (iconRegistry.exists(string)) {
            iconRegistry.remove(string);
        }
        BufferedImage bufferedImage = ImageUtils.base64Decode((String)string2);
        Map map = IconUtils.createSizedIcons((Image)bufferedImage);
        iconRegistry.add("Accounts", string, map);
    }

    private String getIconName(PublicWebService publicWebService) {
        String string = publicWebService.getName() + ".discovered.icon";
        return IconUtils.makeValidIconName((String)string);
    }
}

