/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.discover.MtzDiscoveryItems
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.pws.api.PublicWebService
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.transform.descriptor.ProtocolVersion
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformServerAuthentication
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.util.FastURL
 */
package com.paterva.maltego.transform.discovery;

import com.paterva.maltego.archive.mtz.discover.MtzDiscoveryItems;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.pws.api.PublicWebService;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.transform.descriptor.ProtocolVersion;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformServerAuthentication;
import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.discovery.TransformServerDetail;
import com.paterva.maltego.util.FastURL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TransformServerListing
extends TransformServerDetail {
    private String _description;
    private Map<PublicWebService, String> _webService;
    private Set<TransformDescriptor> _transforms;
    private Set<MaltegoEntitySpec> _entities;
    private Set<TransformSet> _sets;
    private Set<MtzDiscoveryItems> _items;
    private Map<TransformDescriptor, HubSeedDescriptor> _duplicateTransforms;

    public TransformServerListing(String string, FastURL fastURL) {
        super(string, fastURL);
    }

    public TransformServerListing(String string, FastURL fastURL, String string2, ProtocolVersion protocolVersion) {
        super(string, fastURL, string2, protocolVersion);
    }

    public TransformServerListing(TransformServerDetail transformServerDetail) {
        super(transformServerDetail.getSeedUrl(), transformServerDetail.getBaseUrl(), transformServerDetail.getName(), transformServerDetail.getProtocolVersion());
        this.setAuthentication(transformServerDetail.getAuthentication());
    }

    public String getDescription() {
        return this._description;
    }

    public void setDescription(String string) {
        this._description = string;
    }

    public Map<PublicWebService, String> getPublicWebServices() {
        if (this._webService == null) {
            this._webService = new HashMap<PublicWebService, String>();
        }
        return this._webService;
    }

    public Set<TransformDescriptor> getTransforms() {
        if (this._transforms == null) {
            this._transforms = new HashSet<TransformDescriptor>();
        }
        return this._transforms;
    }

    public Set<MaltegoEntitySpec> getEntities() {
        if (this._entities == null) {
            this._entities = new HashSet<MaltegoEntitySpec>();
        }
        return this._entities;
    }

    public Set<TransformSet> getSets() {
        if (this._sets == null) {
            this._sets = new HashSet<TransformSet>();
        }
        return this._sets;
    }

    public Set<MtzDiscoveryItems> getMtzDiscoveryItems() {
        if (this._items == null) {
            this._items = new HashSet<MtzDiscoveryItems>();
        }
        return this._items;
    }

    public Map<TransformDescriptor, HubSeedDescriptor> getDuplicateTransforms() {
        if (this._duplicateTransforms == null) {
            this._duplicateTransforms = new HashMap<TransformDescriptor, HubSeedDescriptor>();
        }
        return this._duplicateTransforms;
    }
}

