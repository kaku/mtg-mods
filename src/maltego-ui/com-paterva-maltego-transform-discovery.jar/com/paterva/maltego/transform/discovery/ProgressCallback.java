/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.discovery;

public interface ProgressCallback {
    public void progress(String var1);

    public void progress(int var1, String var2);
}

