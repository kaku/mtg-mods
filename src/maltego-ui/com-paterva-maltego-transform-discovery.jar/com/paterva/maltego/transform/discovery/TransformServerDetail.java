/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.ProtocolVersion
 *  com.paterva.maltego.transform.descriptor.TransformServerAuthentication
 *  com.paterva.maltego.util.FastURL
 */
package com.paterva.maltego.transform.discovery;

import com.paterva.maltego.transform.descriptor.ProtocolVersion;
import com.paterva.maltego.transform.descriptor.TransformServerAuthentication;
import com.paterva.maltego.transform.discovery.TransformServerReference;
import com.paterva.maltego.util.FastURL;

public class TransformServerDetail
extends TransformServerReference {
    private ProtocolVersion _protocolVersion;
    private String _name;
    private TransformServerAuthentication _authentication = TransformServerAuthentication.None;

    public TransformServerDetail(String string, FastURL fastURL) {
        this(string, fastURL, null, null);
    }

    public TransformServerDetail(String string, FastURL fastURL, String string2, ProtocolVersion protocolVersion) {
        super(string, fastURL);
        this._name = string2;
        this._protocolVersion = protocolVersion;
    }

    public ProtocolVersion getProtocolVersion() {
        return this._protocolVersion;
    }

    public void setProtocolVersion(ProtocolVersion protocolVersion) {
        this._protocolVersion = protocolVersion;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public TransformServerAuthentication getAuthentication() {
        return this._authentication;
    }

    public void setAuthentication(TransformServerAuthentication transformServerAuthentication) {
        this._authentication = transformServerAuthentication;
    }
}

