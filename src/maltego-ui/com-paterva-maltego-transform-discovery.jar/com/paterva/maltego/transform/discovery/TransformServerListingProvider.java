/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.discovery;

import com.paterva.maltego.transform.discovery.TransformServerDetail;
import com.paterva.maltego.transform.discovery.TransformServerListing;
import java.util.ArrayList;
import java.util.Collection;
import org.openide.util.Lookup;

public abstract class TransformServerListingProvider {
    public static synchronized Collection<TransformServerListingProvider> getAll() {
        Collection collection = Lookup.getDefault().lookupAll(TransformServerListingProvider.class);
        return new ArrayList<TransformServerListingProvider>(collection);
    }

    public abstract void update(TransformServerDetail var1, TransformServerListing var2) throws Exception;
}

