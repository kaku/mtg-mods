/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.discovery;

import com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.transform.discovery.TransformServerListing;
import com.paterva.maltego.transform.discovery.impl.DefaultTransformUpdater;
import java.io.IOException;
import org.openide.util.Lookup;

public abstract class TransformUpdater {
    public static TransformUpdater getDefault() {
        TransformUpdater transformUpdater = (TransformUpdater)Lookup.getDefault().lookup(TransformUpdater.class);
        if (transformUpdater == null) {
            return new DefaultTransformUpdater();
        }
        return transformUpdater;
    }

    public abstract void removeMissing(TransformServerListing[] var1);

    public abstract void updateServers(TransformServerListing[] var1);

    public abstract void update(HubSeedDescriptor var1, TransformServerListing[] var2, DiscoveryMergingRules var3) throws IOException;

    public abstract TransformServerListing[] getNew(TransformServerListing[] var1, DiscoveryMergingRules var2);
}

