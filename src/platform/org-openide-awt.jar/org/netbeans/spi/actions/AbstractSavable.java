/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 */
package org.netbeans.spi.actions;

import java.io.IOException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.actions.Savable;
import org.netbeans.modules.openide.awt.SavableRegistry;
import org.openide.util.Lookup;

public abstract class AbstractSavable
implements Savable {
    private static final Logger LOG = Logger.getLogger(Savable.class.getName());

    protected AbstractSavable() {
    }

    @Override
    public final void save() throws IOException {
        Lookup.Template t = new Lookup.Template(AbstractSavable.class, null, (Object)this);
        for (Savable s : Savable.REGISTRY.lookup(t).allInstances()) {
            if (s != this) continue;
            this.handleSave();
            this.unregister();
            return;
        }
        LOG.log(Level.WARNING, "Savable {0} is not in Savable.REGISTRY! Have not you forgotten to call register() in constructor?", this.getClass());
    }

    protected abstract String findDisplayName();

    protected abstract void handleSave() throws IOException;

    public abstract boolean equals(Object var1);

    public abstract int hashCode();

    protected final void register() {
        SavableRegistry.register(this);
    }

    protected final void unregister() {
        SavableRegistry.unregister(this);
    }

    @Override
    public final String toString() {
        return this.findDisplayName();
    }
}

