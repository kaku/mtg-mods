/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.api.actions;

import java.io.IOException;
import org.netbeans.modules.openide.awt.SavableRegistry;
import org.openide.util.Lookup;

public interface Savable {
    public static final Lookup REGISTRY = SavableRegistry.getRegistry();

    public void save() throws IOException;

    public String toString();
}

