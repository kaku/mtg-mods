/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.actions.ActionPresenterProvider
 *  org.openide.util.actions.BooleanStateAction
 *  org.openide.util.actions.SystemAction
 */
package org.netbeans.modules.openide.awt;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;
import org.openide.awt.Actions;
import org.openide.awt.DynamicMenuContent;
import org.openide.util.actions.ActionPresenterProvider;
import org.openide.util.actions.BooleanStateAction;
import org.openide.util.actions.SystemAction;

public final class DefaultAWTBridge
extends ActionPresenterProvider {
    public JMenuItem createMenuPresenter(Action action) {
        if (action instanceof BooleanStateAction) {
            BooleanStateAction b = (BooleanStateAction)action;
            return new Actions.CheckboxMenuItem(b, true);
        }
        if (action instanceof SystemAction) {
            SystemAction s = (SystemAction)action;
            return new Actions.MenuItem(s, true);
        }
        return new Actions.MenuItem(action, true);
    }

    public JMenuItem createPopupPresenter(Action action) {
        JMenuItem item2;
        JMenuItem item2;
        if (action instanceof BooleanStateAction) {
            BooleanStateAction b = (BooleanStateAction)action;
            item2 = new Actions.CheckboxMenuItem(b, false);
        } else if (action instanceof SystemAction) {
            SystemAction s = (SystemAction)action;
            item2 = new Actions.MenuItem(s, false);
        } else {
            item2 = new Actions.MenuItem(action, false);
        }
        return item2;
    }

    public Component createToolbarPresenter(Action action) {
        AbstractButton btn2;
        AbstractButton btn2;
        if (action instanceof BooleanStateAction) {
            btn2 = new JToggleButton();
            Actions.connect(btn2, (BooleanStateAction)action);
        } else {
            btn2 = new JButton();
            Actions.connect(btn2, action);
        }
        return btn2;
    }

    public JPopupMenu createEmptyPopup() {
        return new JPopupMenu();
    }

    public Component[] convertComponents(Component comp) {
        JMenuItem item;
        if (comp instanceof JMenuItem && Boolean.TRUE.equals((item = (JMenuItem)comp).getClientProperty("hideWhenDisabled")) && !item.isEnabled()) {
            return new Component[0];
        }
        if (comp instanceof DynamicMenuContent) {
            Component[] toRet = ((DynamicMenuContent)((Object)comp)).getMenuPresenters();
            boolean atLeastOne = false;
            ArrayList<Component> col = new ArrayList<Component>();
            for (int i = 0; i < toRet.length; ++i) {
                if (toRet[i] instanceof DynamicMenuContent && toRet[i] != comp) {
                    col.addAll(Arrays.asList(this.convertComponents(toRet[i])));
                    atLeastOne = true;
                    continue;
                }
                if (toRet[i] == null) {
                    toRet[i] = new JSeparator();
                }
                col.add(toRet[i]);
            }
            if (atLeastOne) {
                return col.toArray(new Component[col.size()]);
            }
            return toRet;
        }
        return new Component[]{comp};
    }
}

