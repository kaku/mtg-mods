/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package org.netbeans.modules.openide.awt;

import java.util.concurrent.Executor;
import org.netbeans.spi.actions.AbstractSavable;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public final class SavableRegistry {
    private static final RequestProcessor RP = new RequestProcessor("Savable Registry");
    private static final InstanceContent IC = new InstanceContent((Executor)RP);
    private static final Lookup LOOKUP = new AbstractLookup((AbstractLookup.Content)IC);

    public static Lookup getRegistry() {
        return LOOKUP;
    }

    public static void register(AbstractSavable as) {
        IC.add((Object)as);
    }

    public static void unregister(AbstractSavable as) {
        IC.remove((Object)as);
    }
}

