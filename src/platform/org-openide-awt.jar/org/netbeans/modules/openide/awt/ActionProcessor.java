/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.util.actions.Presenter$Toolbar
 */
package org.netbeans.modules.openide.awt;

import java.awt.event.ActionListener;
import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.processing.Completion;
import javax.annotation.processing.Completions;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.swing.Action;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.awt.DynamicMenuContent;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;
import org.openide.util.ContextAwareAction;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.util.actions.Presenter;

@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
public final class ActionProcessor
extends LayerGeneratingProcessor {
    private static final String IDENTIFIER = "(?:\\p{javaJavaIdentifierStart}\\p{javaJavaIdentifierPart}*)";
    private static final Pattern FQN = Pattern.compile("(?:\\p{javaJavaIdentifierStart}\\p{javaJavaIdentifierPart}*)(?:[.](?:\\p{javaJavaIdentifierStart}\\p{javaJavaIdentifierPart}*))*");
    private static final String[] DEFAULT_COMPLETIONS = new String[]{"Menu", "Toolbars", "Shortcuts", "Loaders"};
    private Processor COMPLETIONS;

    public Set<String> getSupportedAnnotationTypes() {
        HashSet<String> hash = new HashSet<String>();
        hash.add(ActionRegistration.class.getCanonicalName());
        hash.add(ActionID.class.getCanonicalName());
        hash.add(ActionReference.class.getCanonicalName());
        hash.add(ActionReferences.class.getCanonicalName());
        return hash;
    }

    public Iterable<? extends Completion> getCompletions(Element element, AnnotationMirror annotation, ExecutableElement member, String userText) {
        if (annotation.getAnnotationType().asElement().getSimpleName().toString().contains(ActionReference.class.getSimpleName()) && member.getSimpleName().contentEquals("path")) {
            if (userText == null) {
                userText = "";
            }
            if (userText.startsWith("\"")) {
                userText = userText.substring(1);
            }
            HashSet<Completion> res = new HashSet<Completion>();
            for (String c : DEFAULT_COMPLETIONS) {
                if (!c.startsWith(userText)) continue;
                res.add(Completions.of("\"" + c + '/', NbBundle.getMessage(ActionProcessor.class, (String)("HINT_" + c))));
            }
            if (!res.isEmpty()) {
                return res;
            }
            if (this.COMPLETIONS == null) {
                String pathCompletions = System.getProperty(ActionReference.class.getName() + ".completion");
                if (pathCompletions != null) {
                    ClassLoader l = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
                    if (l == null) {
                        l = Thread.currentThread().getContextClassLoader();
                    }
                    if (l == null) {
                        l = ActionProcessor.class.getClassLoader();
                    }
                    try {
                        this.COMPLETIONS = (Processor)Class.forName(pathCompletions, true, l).newInstance();
                    }
                    catch (Exception ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                        this.COMPLETIONS = this;
                    }
                } else {
                    return res;
                }
            }
            if (this.COMPLETIONS != null && this.COMPLETIONS != this) {
                this.COMPLETIONS.init(this.processingEnv);
                for (Completion completion : this.COMPLETIONS.getCompletions(element, annotation, member, userText)) {
                    res.add(completion);
                }
            }
            return res;
        }
        return Collections.emptyList();
    }

    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        ActionID id;
        TypeMirror actionListener = this.type(ActionListener.class);
        TypeMirror p1 = this.type(Presenter.Menu.class);
        TypeMirror p2 = this.type(Presenter.Toolbar.class);
        TypeMirror p3 = this.type(Presenter.Popup.class);
        TypeMirror caa = this.type(ContextAwareAction.class);
        TypeMirror dmc = this.type(DynamicMenuContent.class);
        for (Element e22 : roundEnv.getElementsAnnotatedWith(ActionRegistration.class)) {
            String popupText;
            ActionReferences refs;
            String key;
            ActionRegistration ar = e22.getAnnotation(ActionRegistration.class);
            if (ar == null) continue;
            ActionID aid = e22.getAnnotation(ActionID.class);
            if (aid == null) {
                throw new LayerGenerationException("@ActionRegistration can only be used together with @ActionID annotation", e22, this.processingEnv, (Annotation)ar);
            }
            if (aid.id() == null) continue;
            if (aid.category().startsWith("Actions/")) {
                throw new LayerGenerationException("@ActionID category() should not start with Actions/", e22, this.processingEnv, (Annotation)aid, "category");
            }
            if (!FQN.matcher(aid.id()).matches()) {
                throw new LayerGenerationException("@ActionID id() must be valid fully qualified name", e22, this.processingEnv, (Annotation)aid, "id");
            }
            String id2 = aid.id().replace('.', '-');
            LayerBuilder builder = this.layer(new Element[]{e22});
            LayerBuilder.File f = builder.file("Actions/" + aid.category() + "/" + id2 + ".instance");
            f.bundlevalue("displayName", ar.displayName(), (Annotation)ar, "displayName");
            String menuText = ar.menuText();
            if (!menuText.isEmpty()) {
                f.bundlevalue("menuText", menuText, (Annotation)ar, "menuText");
            }
            if (!(popupText = ar.popupText()).isEmpty()) {
                f.bundlevalue("popupText", popupText, (Annotation)ar, "popupText");
            }
            boolean createDelegate = true;
            if (e22.getKind() == ElementKind.FIELD) {
                VariableElement var = (VariableElement)e22;
                TypeMirror stringType = this.type(String.class);
                if (!(e22.asType() == stringType && e22.getModifiers().contains((Object)Modifier.PUBLIC) && e22.getModifiers().contains((Object)Modifier.STATIC) && e22.getModifiers().contains((Object)Modifier.FINAL))) {
                    throw new LayerGenerationException("Only static string constant fields can be annotated", e22, this.processingEnv, (Annotation)ar);
                }
                if (ar.key().length() != 0) {
                    throw new LayerGenerationException("When annotating field, one cannot define key()", e22, this.processingEnv, (Annotation)ar, "key");
                }
                createDelegate = false;
                key = var.getConstantValue().toString();
            } else if (e22.getKind() == ElementKind.CLASS) {
                if (!this.isAssignable(e22.asType(), actionListener)) {
                    throw new LayerGenerationException("Class annotated with @ActionRegistration must implement java.awt.event.ActionListener!", e22, this.processingEnv, (Annotation)ar);
                }
                if (!e22.getModifiers().contains((Object)Modifier.PUBLIC)) {
                    throw new LayerGenerationException("Class has to be public", e22, this.processingEnv, (Annotation)ar);
                }
                if (e22.getEnclosingElement().getKind() == ElementKind.CLASS && !e22.getModifiers().contains((Object)Modifier.STATIC)) {
                    throw new LayerGenerationException("Inner class annotated with @ActionRegistration has to be static", e22);
                }
                key = ar.key();
            } else {
                assert (e22.getKind() == ElementKind.METHOD);
                builder.instanceFile("dummy", null, ActionListener.class, (Annotation)ar, null);
                key = ar.key();
            }
            Boolean direct = null;
            AnnotationMirror arMirror = null;
            block3 : for (AnnotationMirror m : e22.getAnnotationMirrors()) {
                if (!m.getAnnotationType().toString().equals(ActionRegistration.class.getCanonicalName())) continue;
                arMirror = m;
                for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : m.getElementValues().entrySet()) {
                    if (!entry.getKey().getSimpleName().contentEquals("lazy")) continue;
                    direct = (Boolean)entry.getValue().getValue() == false;
                    assert (direct == !ar.lazy());
                    continue block3;
                }
            }
            if (direct == null) {
                if (e22.getKind() == ElementKind.FIELD) {
                    direct = false;
                } else {
                    TypeMirror type = e22.getKind() == ElementKind.CLASS ? e22.asType() : ((ExecutableElement)e22).getReturnType();
                    direct = this.isAssignable(type, p1) || this.isAssignable(type, p2) || this.isAssignable(type, p3) || this.isAssignable(type, caa) || this.isAssignable(type, dmc);
                    if (direct.booleanValue()) {
                        this.processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING, "Should explicitly specify lazy attribute", e22);
                    }
                }
            }
            if (direct.booleanValue()) {
                if (key.length() != 0) {
                    throw new LayerGenerationException("Cannot specify key and use eager registration", e22, this.processingEnv, (Annotation)ar, "key");
                }
                if (!ar.iconBase().isEmpty()) {
                    this.processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING, "iconBase unused on eager registrations", e22, arMirror);
                }
                f.instanceAttribute("instanceCreate", Action.class);
            } else {
                if (key.length() == 0) {
                    f.methodvalue("instanceCreate", "org.openide.awt.Actions", "alwaysEnabled");
                } else {
                    f.methodvalue("instanceCreate", "org.openide.awt.Actions", "callback");
                    if (createDelegate) {
                        f.methodvalue("fallback", "org.openide.awt.Actions", "alwaysEnabled");
                    }
                    f.stringvalue("key", key);
                }
                if (createDelegate) {
                    try {
                        f.instanceAttribute("delegate", ActionListener.class, (Annotation)ar, null);
                    }
                    catch (LayerGenerationException ex) {
                        this.generateContext(e22, f, ar);
                    }
                }
                if (ar.iconBase().length() > 0) {
                    builder.validateResource(ar.iconBase(), e22, (Annotation)ar, "iconBase", true);
                    f.stringvalue("iconBase", ar.iconBase());
                }
                f.boolvalue("noIconInMenu", !ar.iconInMenu());
                if (ar.asynchronous()) {
                    f.boolvalue("asynchronous", true);
                }
                if (ar.surviveFocusChange()) {
                    f.boolvalue("surviveFocusChange", true);
                }
            }
            f.write();
            ActionReference aref = e22.getAnnotation(ActionReference.class);
            if (aref != null) {
                this.processReferences(e22, aref, aid);
            }
            if ((refs = e22.getAnnotation(ActionReferences.class)) == null) continue;
            for (ActionReference actionReference : refs.value()) {
                this.processReferences(e22, actionReference, aid);
            }
        }
        for (Element e : roundEnv.getElementsAnnotatedWith(ActionReference.class)) {
            ActionReference ref;
            if (e.getAnnotation(ActionRegistration.class) != null || (ref = e.getAnnotation(ActionReference.class)) == null) continue;
            id = e.getAnnotation(ActionID.class);
            if (id != null) {
                this.processReferences(e, ref, id);
                continue;
            }
            throw new LayerGenerationException("Don't use @ActionReference without @ActionID", e, this.processingEnv, (Annotation)ref);
        }
        for (Element e2 : roundEnv.getElementsAnnotatedWith(ActionReferences.class)) {
            ActionReferences refs;
            if (e2.getAnnotation(ActionRegistration.class) != null || (refs = e2.getAnnotation(ActionReferences.class)) == null) continue;
            id = e2.getAnnotation(ActionID.class);
            if (id != null) {
                for (ActionReference actionReference : refs.value()) {
                    if (!actionReference.id().id().isEmpty() || !actionReference.id().category().isEmpty()) {
                        throw new LayerGenerationException("Don't specify additional id=@ActionID(...) when using @ActionID on the element", e2, this.processingEnv, (Annotation)actionReference.id());
                    }
                    this.processReferences(e2, actionReference, id);
                }
                continue;
            }
            for (ActionReference actionReference : refs.value()) {
                if (actionReference.id().id().isEmpty() || actionReference.id().category().isEmpty()) {
                    throw new LayerGenerationException("Specify real id=@ActionID(...)", e2, this.processingEnv, (Annotation)actionReference.id());
                }
                this.processReferences(e2, actionReference, actionReference.id());
            }
        }
        return true;
    }

    private TypeMirror type(Class<?> type) {
        TypeElement e = this.processingEnv.getElementUtils().getTypeElement(type.getCanonicalName());
        return e == null ? null : e.asType();
    }

    private void generateContext(Element e, LayerBuilder.File f, ActionRegistration ar) throws LayerGenerationException {
        ExecutableElement ee = null;
        ExecutableElement candidate = null;
        for (ExecutableElement element : ElementFilter.constructorsIn(e.getEnclosedElements())) {
            if (element.getKind() != ElementKind.CONSTRUCTOR) continue;
            candidate = element;
            if (!element.getModifiers().contains((Object)Modifier.PUBLIC)) continue;
            if (ee != null) {
                throw new LayerGenerationException("Only one public constructor allowed", e, this.processingEnv, (Annotation)ar);
            }
            ee = element;
        }
        if (ee == null || ee.getParameters().size() != 1) {
            if (candidate != null) {
                throw new LayerGenerationException("Constructor has to be public with one argument", (Element)candidate);
            }
            throw new LayerGenerationException("Constructor must have one argument", (Element)ee);
        }
        VariableElement ve = ee.getParameters().get(0);
        TypeMirror ctorType = ve.asType();
        switch (ctorType.getKind()) {
            case ARRAY: {
                String elemType = ((ArrayType)ctorType).getComponentType().toString();
                throw new LayerGenerationException("Use List<" + elemType + "> rather than " + elemType + "[] in constructor", e, this.processingEnv, (Annotation)ar);
            }
            case DECLARED: {
                break;
            }
            default: {
                throw new LayerGenerationException("Must use SomeType (or List<SomeType>) in constructor, not " + (Object)((Object)ctorType.getKind()));
            }
        }
        DeclaredType dt = (DeclaredType)ctorType;
        String dtName = this.processingEnv.getElementUtils().getBinaryName((TypeElement)dt.asElement()).toString();
        if ("java.util.List".equals(dtName)) {
            if (dt.getTypeArguments().isEmpty()) {
                throw new LayerGenerationException("Use List<SomeType>", (Element)ee);
            }
            f.stringvalue("type", this.binaryName(dt.getTypeArguments().get(0)));
            f.methodvalue("delegate", "org.openide.awt.Actions", "inject");
            f.stringvalue("injectable", this.processingEnv.getElementUtils().getBinaryName((TypeElement)e).toString());
            f.stringvalue("selectionType", "ANY");
            f.methodvalue("instanceCreate", "org.openide.awt.Actions", "context");
            return;
        }
        if (!dt.getTypeArguments().isEmpty()) {
            throw new LayerGenerationException("No type parameters allowed in ", (Element)ee);
        }
        f.stringvalue("type", this.binaryName(ctorType));
        f.methodvalue("delegate", "org.openide.awt.Actions", "inject");
        f.stringvalue("injectable", this.processingEnv.getElementUtils().getBinaryName((TypeElement)e).toString());
        f.stringvalue("selectionType", "EXACTLY_ONE");
        f.methodvalue("instanceCreate", "org.openide.awt.Actions", "context");
    }

    private String binaryName(TypeMirror t) {
        Element e = this.processingEnv.getTypeUtils().asElement(t);
        if (e != null && (e.getKind().isClass() || e.getKind().isInterface())) {
            return this.processingEnv.getElementUtils().getBinaryName((TypeElement)e).toString();
        }
        return t.toString();
    }

    private boolean isAssignable(TypeMirror first, TypeMirror snd) {
        if (snd == null) {
            return false;
        }
        return this.processingEnv.getTypeUtils().isAssignable(first, snd);
    }

    private void processReferences(Element e, ActionReference ref, ActionID aid) throws LayerGenerationException {
        KeyStroke[] stroke;
        if (!(ref.id().category().isEmpty() || ref.id().id().isEmpty() || aid.id().equals(ref.id().id()) && aid.category().equals(ref.id().category()))) {
            throw new LayerGenerationException("Can't specify id() attribute when @ActionID provided on the element", e, this.processingEnv, (Annotation)aid);
        }
        String name = ref.name();
        if (name.isEmpty()) {
            name = aid.id().replace('.', '-');
        }
        if (ref.path().startsWith("Shortcuts") && (stroke = Utilities.stringToKeys((String)name)) == null) {
            throw new LayerGenerationException("Registrations in Shortcuts folder need to represent a key. Specify value for 'name' attribute.\nSee org.openide.util.Utilities.stringToKeys for possible values. Current name=\"" + name + "\" is not valid.\n", e, this.processingEnv, (Annotation)ref, "path");
        }
        LayerBuilder.File f = this.layer(new Element[]{e}).file(ref.path() + "/" + name + ".shadow");
        f.stringvalue("originalFile", "Actions/" + aid.category() + "/" + aid.id().replace('.', '-') + ".instance");
        f.position(ref.position());
        f.write();
        if (ref.separatorAfter() != Integer.MAX_VALUE) {
            if (ref.position() == Integer.MAX_VALUE || ref.position() >= ref.separatorAfter()) {
                throw new LayerGenerationException("separatorAfter() must be greater than position()", e, this.processingEnv, (Annotation)ref);
            }
            LayerBuilder.File after = this.layer(new Element[]{e}).file(ref.path() + "/" + name + "-separatorAfter.instance");
            after.newvalue("instanceCreate", JSeparator.class.getName());
            after.position(ref.separatorAfter());
            after.write();
        }
        if (ref.separatorBefore() != Integer.MAX_VALUE) {
            if (ref.position() == Integer.MAX_VALUE || ref.position() <= ref.separatorBefore()) {
                throw new LayerGenerationException("separatorBefore() must be lower than position()", e, this.processingEnv, (Annotation)ref);
            }
            LayerBuilder.File before = this.layer(new Element[]{e}).file(ref.path() + "/" + name + "-separatorBefore.instance");
            before.newvalue("instanceCreate", JSeparator.class.getName());
            before.position(ref.separatorBefore());
            before.write();
        }
    }

}

