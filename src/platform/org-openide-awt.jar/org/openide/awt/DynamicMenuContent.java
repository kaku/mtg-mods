/*
 * Decompiled with CFR 0_118.
 */
package org.openide.awt;

import javax.swing.JComponent;

public interface DynamicMenuContent {
    public static final String HIDE_WHEN_DISABLED = "hideWhenDisabled";

    public JComponent[] getMenuPresenters();

    public JComponent[] synchMenuPresenters(JComponent[] var1);
}

