/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.openide.awt;

import java.io.PrintStream;
import javax.swing.event.ChangeListener;
import org.openide.util.ChangeSupport;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;

public abstract class StatusDisplayer {
    public static final int IMPORTANCE_ANNOTATION = 1000;
    public static final int IMPORTANCE_INCREMENTAL_FIND = 900;
    public static final int IMPORTANCE_FIND_OR_REPLACE = 800;
    public static final int IMPORTANCE_ERROR_HIGHLIGHT = 700;
    private static StatusDisplayer INSTANCE = null;

    protected StatusDisplayer() {
    }

    public static synchronized StatusDisplayer getDefault() {
        if (INSTANCE == null && (StatusDisplayer.INSTANCE = (StatusDisplayer)Lookup.getDefault().lookup(StatusDisplayer.class)) == null) {
            INSTANCE = new Trivial();
        }
        return INSTANCE;
    }

    public abstract String getStatusText();

    public abstract void setStatusText(String var1);

    public Message setStatusText(final String text, int importance) {
        if (importance <= 0) {
            throw new IllegalArgumentException("Invalid importance value: " + importance);
        }
        this.setStatusText(text);
        return new Message(){

            @Override
            public void clear(int timeInMillis) {
                RequestProcessor.getDefault().post(new Runnable(){

                    @Override
                    public void run() {
                        if (text == StatusDisplayer.this.getStatusText()) {
                            StatusDisplayer.this.setStatusText("");
                        }
                    }
                }, timeInMillis);
            }

            protected void finalize() throws Throwable {
                if (text == StatusDisplayer.this.getStatusText()) {
                    StatusDisplayer.this.setStatusText("");
                }
            }

        };
    }

    public abstract void addChangeListener(ChangeListener var1);

    public abstract void removeChangeListener(ChangeListener var1);

    private static final class Trivial
    extends StatusDisplayer {
        private final ChangeSupport cs;
        private String text;

        private Trivial() {
            this.cs = new ChangeSupport((Object)this);
            this.text = "";
        }

        @Override
        public synchronized String getStatusText() {
            return this.text;
        }

        @Override
        public synchronized void setStatusText(String text) {
            if (text.equals(this.text)) {
                return;
            }
            this.text = text;
            if (text.length() > 0) {
                System.err.println("(" + text + ")");
            }
            this.cs.fireChange();
        }

        @Override
        public void addChangeListener(ChangeListener l) {
            this.cs.addChangeListener(l);
        }

        @Override
        public void removeChangeListener(ChangeListener l) {
            this.cs.removeChangeListener(l);
        }
    }

    public static interface Message {
        public void clear(int var1);
    }

}

