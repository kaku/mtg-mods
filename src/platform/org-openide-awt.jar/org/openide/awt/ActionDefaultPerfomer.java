/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 */
package org.openide.awt;

import java.awt.event.ActionEvent;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.netbeans.api.actions.Closable;
import org.netbeans.api.actions.Editable;
import org.netbeans.api.actions.Openable;
import org.netbeans.api.actions.Printable;
import org.netbeans.api.actions.Viewable;
import org.openide.awt.ContextAction;
import org.openide.util.Lookup;

final class ActionDefaultPerfomer
extends ContextAction.Performer<Object> {
    final int type;

    public ActionDefaultPerfomer(int type) {
        super(Collections.emptyMap());
        this.type = type;
    }

    @Override
    public void actionPerformed(ActionEvent ev, List<? extends Object> data, Lookup.Provider everything) {
        block7 : for (Object o : data) {
            switch (this.type) {
                case 0: {
                    ((Openable)o).open();
                    continue block7;
                }
                case 1: {
                    ((Viewable)o).view();
                    continue block7;
                }
                case 2: {
                    ((Editable)o).edit();
                    continue block7;
                }
                case 3: {
                    ((Closable)o).close();
                    continue block7;
                }
                case 4: {
                    ((Printable)o).print();
                    continue block7;
                }
            }
            assert (false);
        }
    }
}

