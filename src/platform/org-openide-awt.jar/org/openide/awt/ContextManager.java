/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Mutex
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.WeakSet
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 */
package org.openide.awt;

import java.awt.event.ActionEvent;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.awt.ContextAction;
import org.openide.awt.ContextSelection;
import org.openide.awt.GeneralAction;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Mutex;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.WeakSet;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

class ContextManager {
    private static final Logger LOG = GeneralAction.LOG;
    private static final Map<LookupRef, Reference<ContextManager>> CACHE = new HashMap<LookupRef, Reference<ContextManager>>();
    private static final Map<LookupRef, Reference<ContextManager>> SURVIVE = new HashMap<LookupRef, Reference<ContextManager>>();
    private Map<Class, LSet> listeners = new HashMap<Class, LSet>();
    private Lookup lookup;
    private LSet<Lookup.Provider> selectionAll;

    private ContextManager(Lookup lookup) {
        this.lookup = lookup;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static ContextManager findManager(Lookup context, boolean survive) {
        Map<LookupRef, Reference<ContextManager>> map = CACHE;
        synchronized (map) {
            ContextManager g;
            Map<LookupRef, Reference<ContextManager>> map2 = survive ? SURVIVE : CACHE;
            LookupRef lr = new LookupRef(context);
            GMReference ref = map2.get(lr);
            ContextManager contextManager = g = ref == null ? null : ref.get();
            if (g == null) {
                g = survive ? new SurviveManager(context) : new ContextManager(context);
                ref = new GMReference(g, lr, survive);
                map2.put(lr, ref);
            }
            return g;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void clearCache(LookupRef lr, GMReference ref, boolean survive) {
        Map<LookupRef, Reference<ContextManager>> map = CACHE;
        synchronized (map) {
            Map<LookupRef, Reference<ContextManager>> map2;
            Map<LookupRef, Reference<ContextManager>> map3 = map2 = survive ? SURVIVE : CACHE;
            if (map2.get(lr) == ref) {
                map2.remove(lr);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public <T> void registerListener(Class<T> type, ContextAction<T> a) {
        Map<LookupRef, Reference<ContextManager>> map = CACHE;
        synchronized (map) {
            LSet<T> existing = this.findLSet(type);
            if (existing == null) {
                Lookup.Result<T> result = this.createResult(this.lookup.lookupResult(type));
                existing = new LSet<T>(result, type);
                this.listeners.put(type, existing);
            }
            existing.add(a);
            if (a.selectMode == ContextSelection.ALL) {
                this.initSelectionAll();
                this.selectionAll.add(a);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public <T> void unregisterListener(Class<T> type, ContextAction<T> a) {
        Map<LookupRef, Reference<ContextManager>> map = CACHE;
        synchronized (map) {
            LSet<T> existing = this.findLSet(type);
            if (existing != null) {
                existing.remove(a);
                if (existing.isEmpty()) {
                    this.listeners.remove(type);
                    existing.cleanup();
                }
            }
            if (a.selectMode == ContextSelection.ALL && this.selectionAll != null) {
                this.selectionAll.remove(a);
                if (this.selectionAll.isEmpty() && !this.isSurvive()) {
                    this.selectionAll = null;
                }
            }
        }
    }

    public boolean isSurvive() {
        return false;
    }

    public <T> boolean isEnabled(Class<T> type, ContextSelection selectMode, ContextAction.Performer<? super T> enabler) {
        Lookup.Result<T> result = this.findResult(type);
        boolean e = this.isEnabledOnData(result, type, selectMode);
        if (e && enabler != null) {
            e = enabler.enabled(this.listFromResult(result));
        }
        return e;
    }

    private <T> boolean isEnabledOnData(Lookup.Result<T> result, Class<T> type, ContextSelection selectMode) {
        boolean res = this.isEnabledOnDataImpl(result, type, selectMode);
        LOG.log(Level.FINE, "isEnabledOnData(result, {0}, {1}) = {2}", new Object[]{type, selectMode, res});
        return res;
    }

    private <T> boolean isEnabledOnDataImpl(Lookup.Result<T> result, Class<T> type, ContextSelection selectMode) {
        switch (selectMode) {
            case EXACTLY_ONE: {
                HashSet instances = new HashSet(result.allItems());
                return instances.size() == 1;
            }
            case ANY: {
                return !result.allItems().isEmpty();
            }
            case EACH: {
                if (result.allItems().isEmpty()) {
                    return false;
                }
                Lookup.Result items = this.lookup.lookupResult(Lookup.Provider.class);
                if (result.allItems().size() != items.allItems().size()) {
                    return false;
                }
                Lookup.Template template = new Lookup.Template(type);
                for (Lookup.Provider prov : items.allInstances()) {
                    if (prov.getLookup().lookupItem(template) != null) continue;
                    return false;
                }
                return true;
            }
            case ALL: {
                if (result.allItems().isEmpty()) {
                    return false;
                }
                Lookup.Result items = this.lookup.lookupResult(Lookup.Provider.class);
                if (result.allItems().size() < items.allItems().size()) {
                    return false;
                }
                Lookup.Template template = new Lookup.Template(type);
                for (Lookup.Provider prov : items.allInstances()) {
                    if (prov.getLookup().lookupItem(template) != null) continue;
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    <T> LSet<T> findLSet(Class<T> type) {
        Map<LookupRef, Reference<ContextManager>> map = CACHE;
        synchronized (map) {
            return this.listeners.get(type);
        }
    }

    private <T> Lookup.Result<T> findResult(Class<T> type) {
        LSet<T> lset = this.findLSet(type);
        Lookup.Result result = lset != null ? lset.result : this.lookup.lookupResult(type);
        return result;
    }

    protected <T> Lookup.Result<T> createResult(Lookup.Result<T> res) {
        return res;
    }

    public <T> void actionPerformed(ActionEvent e, ContextAction.Performer<? super T> perf, Class<T> type, ContextSelection selectMode) {
        class LkpAE
        implements Lookup.Provider {
            private Lookup lookup;
            final /* synthetic */ List val$all;
            final /* synthetic */ Class val$type;

            LkpAE() {
                this.val$all = var2_2;
                this.val$type = var3_3;
            }

            public Lookup getLookup() {
                if (this.lookup == null) {
                    this.lookup = new ProxyLookup(new Lookup[]{Lookups.fixed((Object[])this.val$all.toArray()), Lookups.exclude((Lookup)this$0.lookup, (Class[])new Class[]{this.val$type})});
                }
                return this.lookup;
            }
        }
        Lookup.Result<T> result = this.findResult(type);
        List<T> all = this.listFromResult(result);
        perf.actionPerformed(e, Collections.unmodifiableList(all), new LkpAE(this, all, type));
    }

    private <T> List<? extends T> listFromResult(Lookup.Result<T> result) {
        ArrayList all;
        ArrayList col = result.allInstances();
        LinkedHashSet tmp = new LinkedHashSet(col);
        if (tmp.size() != col.size()) {
            ArrayList nt = new ArrayList(tmp.size());
            nt.addAll(tmp);
            col = nt;
        }
        if (col instanceof List) {
            all = col;
        } else {
            ArrayList arr = new ArrayList();
            arr.addAll(col);
            all = arr;
        }
        return all;
    }

    private Lookup.Result<Lookup.Provider> initSelectionAll() {
        assert (Thread.holdsLock(CACHE));
        if (this.selectionAll == null) {
            Lookup.Result result = this.lookup.lookupResult(Lookup.Provider.class);
            this.selectionAll = new LSet<Lookup.Provider>(result, Lookup.Provider.class);
        }
        return this.selectionAll.result;
    }

    static class LookupRef
    extends WeakReference<Lookup> {
        private final int hashCode;

        public LookupRef(Lookup referent) {
            super(referent);
            this.hashCode = System.identityHashCode((Object)referent);
        }

        public boolean equals(Object obj) {
            if (obj instanceof LookupRef) {
                LookupRef lr = (LookupRef)obj;
                return this.get() == lr.get();
            }
            return false;
        }

        public int hashCode() {
            return this.hashCode;
        }
    }

    static final class LSet<T>
    extends WeakSet<ContextAction>
    implements LookupListener,
    Runnable {
        final Lookup.Result<T> result;

        public LSet(Lookup.Result<T> context, Class<T> type) {
            this.result = context;
            this.result.addLookupListener((LookupListener)this);
            this.result.allItems();
        }

        public boolean add(ContextAction e) {
            assert (e != null);
            return WeakSet.super.add((Object)e);
        }

        public void resultChanged(LookupEvent ev) {
            Mutex.EVENT.readAccess((Runnable)this);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            ContextAction[] arr;
            Map map = CACHE;
            synchronized (map) {
                arr = (ContextAction[])this.toArray((Object[])new ContextAction[0]);
            }
            long now = 0;
            assert ((now = System.currentTimeMillis()) >= 0);
            for (ContextAction a : arr) {
                if (a == null) continue;
                a.updateState();
            }
            long took = 0;
            assert ((took = System.currentTimeMillis() - now) >= 0);
            if (took > 2000) {
                LOG.log(Level.WARNING, "Updating state of {1} actions took {0} ms. here is the action list:", new Object[]{took, arr.length});
                for (ContextAction a2 : arr) {
                    LOG.log(Level.INFO, "  {0}", a2);
                }
            }
        }

        private void cleanup() {
            this.result.removeLookupListener((LookupListener)this);
        }
    }

    private static final class NeverEmptyResult<T>
    extends Lookup.Result<T>
    implements LookupListener {
        private final Lookup.Result<T> delegate;
        private final Lookup.Result<Lookup.Provider> nodes;
        private final Collection<LookupListener> listeners;
        private Collection<? extends Lookup.Item<T>> allItems;
        private Collection<? extends T> allInstances;
        private Set<Class<? extends T>> allClasses;

        public NeverEmptyResult(Lookup.Result<T> delegate, Lookup.Result<Lookup.Provider> nodes) {
            this.delegate = delegate;
            this.nodes = nodes;
            this.listeners = new CopyOnWriteArrayList<LookupListener>();
            this.delegate.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)((Object)this), this.delegate));
            this.nodes.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)((Object)this), this.nodes));
            this.initValues();
        }

        public void addLookupListener(LookupListener l) {
            this.listeners.add(l);
        }

        public void removeLookupListener(LookupListener l) {
            this.listeners.remove((Object)l);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Collection<? extends Lookup.Item<T>> allItems() {
            Collection res = this.delegate.allItems();
            NeverEmptyResult neverEmptyResult = this;
            synchronized (neverEmptyResult) {
                if (!res.isEmpty()) {
                    this.allItems = res;
                }
                return this.allItems;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Collection<? extends T> allInstances() {
            Collection res = this.delegate.allInstances();
            NeverEmptyResult neverEmptyResult = this;
            synchronized (neverEmptyResult) {
                if (!res.isEmpty()) {
                    this.allInstances = res;
                }
                return this.allInstances;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Set<Class<? extends T>> allClasses() {
            Set res = this.delegate.allClasses();
            NeverEmptyResult neverEmptyResult = this;
            synchronized (neverEmptyResult) {
                if (!res.isEmpty()) {
                    this.allClasses = res;
                }
                return this.allClasses;
            }
        }

        public void resultChanged(LookupEvent ev) {
            if (ev.getSource() == this.nodes) {
                Collection arr = this.nodes.allItems();
                if (arr.size() == 1 && ((Lookup.Item)arr.iterator().next()).getInstance() == null) {
                    return;
                }
                this.initValues();
                return;
            }
            LookupEvent mev = new LookupEvent((Lookup.Result)this);
            for (LookupListener ll : this.listeners) {
                ll.resultChanged(mev);
            }
        }

        private synchronized void initValues() {
            this.allItems = Collections.emptyList();
            this.allInstances = Collections.emptyList();
            this.allClasses = Collections.emptySet();
        }
    }

    private static final class SurviveManager
    extends ContextManager {
        private SurviveManager(Lookup context) {
            super(context);
        }

        @Override
        public boolean isSurvive() {
            return true;
        }

        @Override
        protected <T> Lookup.Result<T> createResult(Lookup.Result<T> res) {
            return new NeverEmptyResult<T>(res, this.initSelectionAll());
        }
    }

    private static final class GMReference
    extends WeakReference<ContextManager>
    implements Runnable {
        private LookupRef context;
        private boolean survive;

        public GMReference(ContextManager m, LookupRef context, boolean survive) {
            super(m, Utilities.activeReferenceQueue());
            this.context = context;
            this.survive = survive;
        }

        @Override
        public void run() {
            ContextManager.clearCache(this.context, this, this.survive);
        }
    }

}

