/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package org.openide.awt;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.font.LineMetrics;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import org.openide.awt.HtmlLabelUI;
import org.openide.awt.HtmlRendererImpl;
import org.openide.util.Utilities;

public final class HtmlRenderer {
    private static Stack<Color> colorStack = new Stack();
    public static final int STYLE_CLIP = 0;
    public static final int STYLE_TRUNCATE = 1;
    private static final int STYLE_WORDWRAP = 2;
    private static final boolean STRICT_HTML = Boolean.getBoolean("netbeans.lwhtml.strict");
    private static Set<String> badStrings = null;
    private static Logger LOG = Logger.getLogger(HtmlRenderer.class.getName());
    private static final Object[] entities = new Object[]{new char[]{'g', 't'}, new char[]{'l', 't'}, new char[]{'q', 'u', 'o', 't'}, new char[]{'a', 'm', 'p'}, new char[]{'l', 's', 'q', 'u', 'o'}, new char[]{'r', 's', 'q', 'u', 'o'}, new char[]{'l', 'd', 'q', 'u', 'o'}, new char[]{'r', 'd', 'q', 'u', 'o'}, new char[]{'n', 'd', 'a', 's', 'h'}, new char[]{'m', 'd', 'a', 's', 'h'}, new char[]{'n', 'e'}, new char[]{'l', 'e'}, new char[]{'g', 'e'}, new char[]{'c', 'o', 'p', 'y'}, new char[]{'r', 'e', 'g'}, new char[]{'t', 'r', 'a', 'd', 'e'}, new char[]{'n', 'b', 's', 'p'}};
    private static final char[] entitySubstitutions = new char[]{'>', '<', '\"', '&', '\u2018', '\u2019', '\u201c', '\u201d', '\u2013', '\u2014', '\u2260', '\u2264', '\u2265', '\u00a9', '\u00ae', '\u2122', ' '};

    private HtmlRenderer() {
    }

    public static final Renderer createRenderer() {
        return new HtmlRendererImpl();
    }

    public static final JLabel createLabel() {
        return new HtmlRendererImpl();
    }

    public static double renderPlainString(String s, Graphics g, int x, int y, int w, int h, Font f, Color defaultColor, int style, boolean paint) {
        if (style < 0 || style > 1) {
            throw new IllegalArgumentException("Unknown rendering mode: " + style);
        }
        return HtmlRenderer._renderPlainString(s, g, x, y, w, h, f, defaultColor, style, paint);
    }

    private static double _renderPlainString(String s, Graphics g, int x, int y, int w, int h, Font f, Color foreground, int style, boolean paint) {
        if (f == null && (f = UIManager.getFont("controlFont")) == null) {
            int fs = 11;
            Object cfs = UIManager.get("customFontSize");
            if (cfs instanceof Integer) {
                fs = (Integer)cfs;
            }
            f = new Font("Dialog", 0, fs);
        }
        FontMetrics fm = g.getFontMetrics(f);
        int wid = Utilities.isMac() ? fm.stringWidth(s) : (int)fm.getStringBounds(s, g).getWidth();
        if (paint) {
            g.setColor(foreground);
            g.setFont(f);
            if (wid <= w || style == 0) {
                g.drawString(s, x, y);
            } else {
                char[] chars = s.toCharArray();
                if (chars.length == 0) {
                    return 0.0;
                }
                double chWidth = wid / chars.length;
                int estCharsToPaint = new Double((double)w / chWidth).intValue();
                if (estCharsToPaint > chars.length) {
                    estCharsToPaint = chars.length;
                }
                while (estCharsToPaint > 3) {
                    int newWidth;
                    if (estCharsToPaint < chars.length) {
                        chars[estCharsToPaint - 1] = 8230;
                    }
                    if ((newWidth = Utilities.isMac() ? fm.stringWidth(new String(chars, 0, estCharsToPaint)) : (int)fm.getStringBounds(chars, 0, estCharsToPaint, g).getWidth()) <= w) break;
                    --estCharsToPaint;
                }
                if (style == 1) {
                    int length = estCharsToPaint;
                    if (length <= 0) {
                        return 0.0;
                    }
                    if (paint) {
                        if (length > 3) {
                            g.drawChars(chars, 0, length, x, y);
                        } else {
                            Shape shape = g.getClip();
                            if (shape != null) {
                                if (s != null) {
                                    Area area = new Area(shape);
                                    area.intersect(new Area(new Rectangle(x, y, w, h)));
                                    g.setClip(area);
                                } else {
                                    g.setClip(new Rectangle(x, y, w, h));
                                }
                            }
                            g.drawString("\u2026", x, y);
                            if (shape != null) {
                                g.setClip(shape);
                            }
                        }
                    }
                }
            }
        }
        return wid;
    }

    public static double renderString(String s, Graphics g, int x, int y, int w, int h, Font f, Color defaultColor, int style, boolean paint) {
        switch (style) {
            case 0: 
            case 1: {
                break;
            }
            default: {
                throw new IllegalArgumentException("Unknown rendering mode: " + style);
            }
        }
        if (s.startsWith("<html") || s.startsWith("<HTML")) {
            return HtmlRenderer._renderHTML(s, 6, g, x, y, w, h, f, defaultColor, style, paint, null);
        }
        return HtmlRenderer.renderPlainString(s, g, x, y, w, h, f, defaultColor, style, paint);
    }

    public static double renderHTML(String s, Graphics g, int x, int y, int w, int h, Font f, Color defaultColor, int style, boolean paint) {
        if (style < 0 || style > 1) {
            throw new IllegalArgumentException("Unknown rendering mode: " + style);
        }
        return HtmlRenderer._renderHTML(s, 0, g, x, y, w, h, f, defaultColor, style, paint, null);
    }

    static double _renderHTML(String s, int pos, Graphics g, int x, int y, int w, int h, Font f, Color defaultColor, int style, boolean paint, Color background) {
        return HtmlRenderer._renderHTML(s, pos, g, x, y, w, h, f, defaultColor, style, paint, background, false);
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Lifted jumps to return sites
     */
    static double _renderHTML(String s, int pos, Graphics g, int x, int y, int w, int h, Font f, Color defaultColor, int style, boolean paint, Color background, boolean forcedForeground) {
        if (f == null && (f = UIManager.getFont("controlFont")) == null) {
            fs = 11;
            cfs = UIManager.get("customFontSize");
            if (cfs instanceof Integer) {
                fs = (Integer)cfs;
            }
            f = new Font("Dialog", 0, fs);
        }
        _colorStack = SwingUtilities.isEventDispatchThread() != false ? HtmlRenderer.colorStack : new Stack<Color>();
        g.setColor(defaultColor);
        g.setFont(f);
        if (HtmlLabelUI.antialias && g instanceof Graphics2D) {
            ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }
        chars = s.toCharArray();
        origX = x;
        done = false;
        inTag = false;
        inClosingTag = false;
        strikethrough = false;
        underline = false;
        link = false;
        bold = false;
        italic = false;
        truncated = false;
        widthPainted = 0.0;
        heightPainted = 0.0;
        lastWasWhitespace = false;
        lastHeight = 0.0;
        dotWidth = 0.0;
        dotsPainted = false;
        if (style == 1) {
            dotWidth = g.getFontMetrics().charWidth('.');
        }
        _colorStack.clear();
        while (!done) {
            block102 : {
                if (pos == s.length()) {
                    if (truncated == false) return widthPainted;
                    if (paint == false) return widthPainted;
                    if (dotsPainted != false) return widthPainted;
                    g.setColor(defaultColor);
                    g.setFont(f);
                    g.drawString("\u2026", x, y);
                    return widthPainted;
                }
                try {
                }
                catch (ArrayIndexOutOfBoundsException e) {
                    aib = new ArrayIndexOutOfBoundsException("HTML rendering failed at position " + pos + " in String \"" + s + "\".  Please report this at http://www.netbeans.org");
                    if (HtmlRenderer.STRICT_HTML) {
                        throw aib;
                    }
                    Logger.getLogger(HtmlRenderer.class.getName()).log(Level.WARNING, null, aib);
                    return HtmlRenderer.renderPlainString(s, g, x, y, w, h, f, defaultColor, style, paint);
                }
                {
                    v0 = inClosingTag = (inTag |= chars[pos] == '<') != false && pos + 1 < chars.length && chars[pos + 1] == '/';
                }
                if (truncated) {
                    g.setColor(defaultColor);
                    g.setFont(f);
                    if (paint) {
                        g.drawString("\u2026", x, y);
                        dotsPainted = true;
                    }
                    done = true;
                    continue;
                }
                if (inTag) ** GOTO lbl87
                if (lastWasWhitespace) {
                    while (pos < s.length() - 1 && Character.isWhitespace(chars[pos])) {
                        ++pos;
                    }
                    if (pos == chars.length - 1) {
                        if (style != 2) {
                            v1 = widthPainted;
                            return v1;
                        }
                        v1 = heightPainted;
                        return v1;
                    }
                }
                isAmp = false;
                nextLtIsEntity = false;
                nextTag = chars.length - 1;
                if (chars[pos] == '&') {
                    v2 = inEntity = pos != chars.length - 1;
                    if (inEntity) {
                        newPos = HtmlRenderer.substEntity(chars, pos + 1);
                        v3 = inEntity = newPos != -1;
                        if (inEntity) {
                            pos = newPos;
                            isAmp = chars[pos] == '&';
                            nextLtIsEntity = chars[pos] == '<';
                        } else {
                            nextLtIsEntity = false;
                            isAmp = true;
                        }
                    }
                } else {
                    nextLtIsEntity = false;
                }
                ** GOTO lbl227
lbl87: // 1 sources:
                done = (tagEnd = ++pos) >= chars.length - 1;
                while (!done) {
                    if (chars[tagEnd] != '>') {
                        done = tagEnd == chars.length - 1;
                        ++tagEnd;
                        continue;
                    }
                    break block102;
                }
                if (done) {
                    HtmlRenderer.throwBadHTML("Matching '>' not found", pos, chars);
                    break;
                }
            }
            if (!inClosingTag) ** GOTO lbl155
            block1 : switch (chars[++pos]) {
                case 'A': 
                case 'a': {
                    if (_colorStack.isEmpty() || forcedForeground) {
                        g.setColor(defaultColor);
                    } else {
                        g.setColor((Color)_colorStack.pop());
                    }
                    link = false;
                    break;
                }
                case 'H': 
                case 'P': 
                case 'h': 
                case 'p': {
                    break;
                }
                case 'B': 
                case 'b': {
                    if (chars[pos + 1] != 'r') {
                        if (chars[pos + 1] == 'R') break;
                        if (!bold && chars[pos + 1] != 'o' && chars[pos + 1] != 'O') {
                            HtmlRenderer.throwBadHTML("Closing bold tag w/o opening bold tag", pos, chars);
                        }
                        if (italic) {
                            g.setFont(HtmlRenderer.deriveFont(f, 2));
                        } else {
                            g.setFont(HtmlRenderer.deriveFont(f, 0));
                        }
                        bold = false;
                        break;
                    }
                    ** GOTO lbl224
                }
                case 'E': 
                case 'I': 
                case 'e': 
                case 'i': {
                    if (bold) {
                        g.setFont(HtmlRenderer.deriveFont(f, 1));
                    } else {
                        g.setFont(HtmlRenderer.deriveFont(f, 0));
                    }
                    if (!italic) {
                        HtmlRenderer.throwBadHTML("Closing italics tag w/oopening italics tag", pos, chars);
                    }
                    italic = false;
                    break;
                }
                case 'S': 
                case 's': {
                    switch (chars[pos + 1]) {
                        case 'T': 
                        case 't': {
                            if (italic) {
                                g.setFont(HtmlRenderer.deriveFont(f, 2));
                            } else {
                                g.setFont(HtmlRenderer.deriveFont(f, 0));
                            }
                            bold = false;
                            break block1;
                        }
                        case '>': {
                            strikethrough = false;
                        }
                    }
                    break;
                }
                case 'U': 
                case 'u': {
                    underline = false;
                    break;
                }
                case 'F': 
                case 'f': {
                    if (_colorStack.isEmpty() || forcedForeground) {
                        g.setColor(defaultColor);
                        break;
                    }
                    g.setColor((Color)_colorStack.pop());
                    break;
                }
                default: {
                    HtmlRenderer.throwBadHTML("Malformed or unsupported HTML", pos, chars);
                    break;
                }
            }
            ** GOTO lbl224
lbl155: // 1 sources:
            block14 : switch (chars[pos]) {
                case 'A': 
                case 'a': {
                    if (!forcedForeground) {
                        linkc = UIManager.getColor("nb.html.link.foreground");
                        if (linkc == null) {
                            linkc = Color.BLUE;
                        }
                        _colorStack.push(g.getColor());
                        linkc = HtmlLabelUI.ensureContrastingColor(linkc, background);
                        g.setColor(linkc);
                    }
                    link = true;
                    break;
                }
                case 'B': 
                case 'b': {
                    switch (chars[pos + 1]) {
                        case 'R': 
                        case 'r': {
                            if (style != 2) break;
                            x = origX;
                            lineHeight = g.getFontMetrics().getHeight();
                            y += lineHeight;
                            heightPainted += (double)lineHeight;
                            widthPainted = 0.0;
                            break block14;
                        }
                        case '>': {
                            bold = true;
                            if (italic) {
                                g.setFont(HtmlRenderer.deriveFont(f, 3));
                                break block14;
                            }
                            g.setFont(HtmlRenderer.deriveFont(f, 1));
                        }
                    }
                    break;
                }
                case 'E': 
                case 'I': 
                case 'e': 
                case 'i': {
                    italic = true;
                    if (bold) {
                        g.setFont(HtmlRenderer.deriveFont(f, 3));
                        break;
                    }
                    g.setFont(HtmlRenderer.deriveFont(f, 2));
                    break;
                }
                case 'S': 
                case 's': {
                    switch (chars[pos + 1]) {
                        case '>': {
                            strikethrough = true;
                            break block14;
                        }
                        case 'T': 
                        case 't': {
                            bold = true;
                            if (italic) {
                                g.setFont(HtmlRenderer.deriveFont(f, 3));
                                break block14;
                            }
                            g.setFont(HtmlRenderer.deriveFont(f, 1));
                        }
                    }
                    break;
                }
                case 'U': 
                case 'u': {
                    underline = true;
                    break;
                }
                case 'F': 
                case 'f': {
                    if (forcedForeground) break;
                    c = HtmlRenderer.findColor(chars, pos, tagEnd);
                    _colorStack.push(g.getColor());
                    g.setColor(c);
                    break;
                }
                case 'P': 
                case 'p': {
                    if (style != 2) break;
                    x = origX;
                    lineHeight = g.getFontMetrics().getHeight();
                    heightPainted = (y += lineHeight + lineHeight / 2) + lineHeight;
                    widthPainted = 0.0;
                    break;
                }
                case 'H': 
                case 'h': {
                    if (pos == 1) break;
                    HtmlRenderer.throwBadHTML("Malformed or unsupported HTML", pos, chars);
                    break;
                }
                default: {
                    HtmlRenderer.throwBadHTML("Malformed or unsupported HTML", pos, chars);
                }
            }
lbl224: // 19 sources:
            pos = tagEnd + (done != false ? 0 : 1);
            inTag = false;
            continue;
lbl227: // 4 sources:
            for (i = pos; i < chars.length; ++i) {
                if (chars[i] == '<' && !nextLtIsEntity || chars[i] == '&' && !isAmp && i != chars.length - 1) {
                    nextTag = i - 1;
                    break;
                }
                isAmp = false;
                nextLtIsEntity = false;
            }
            fm = g.getFontMetrics();
            r = fm.getStringBounds(chars, pos, nextTag + 1, g);
            if (Utilities.isMac()) {
                r.setRect(r.getX(), r.getY(), fm.stringWidth(new String(chars, pos, nextTag - pos + 1)), r.getHeight());
            }
            lastHeight = r.getHeight();
            length = nextTag + 1 - pos;
            goToNextRow = false;
            brutalWrap = false;
            if (truncated) {
                chWidth = dotWidth;
            } else {
                chWidth = r.getWidth() / (double)(nextTag + 1 - pos);
                if (chWidth == Double.POSITIVE_INFINITY || chWidth == Double.NEGATIVE_INFINITY) {
                    chWidth = fm.getMaxAdvance();
                }
            }
            if ((style != 0 && style == 1 && widthPainted + r.getWidth() > (double)w || style == 2 && widthPainted + r.getWidth() > (double)w) && chWidth > 3.0) {
                pixelsOff = widthPainted + (r.getWidth() + 5.0) - (double)w;
                estCharsOver = pixelsOff / chWidth;
                if (style == 1) {
                    charsToPaint = Math.round(Math.round(Math.ceil(((double)w - widthPainted) / chWidth)));
                    startPeriodsPos = pos + charsToPaint - 3;
                    if (startPeriodsPos >= chars.length) {
                        startPeriodsPos = chars.length - 4;
                    }
                    if ((length = startPeriodsPos - pos) < 0) {
                        length = 0;
                    }
                    r = fm.getStringBounds(chars, pos, pos + length, g);
                    if (Utilities.isMac()) {
                        r.setRect(r.getX(), r.getY(), fm.stringWidth(new String(chars, pos, length)), r.getHeight());
                    }
                    truncated = true;
                } else {
                    goToNextRow = true;
                    lastChar = new Double((double)nextTag - estCharsOver).intValue();
                    brutalWrap = x == 0;
                    for (i = lastChar; i > pos; --lastChar, --i) {
                        if (!Character.isWhitespace(chars[i])) continue;
                        length = lastChar - pos + 1;
                        brutalWrap = false;
                        break;
                    }
                    if (lastChar <= pos) {
                        if ((double)length <= estCharsOver || brutalWrap) {
                            if (brutalWrap) {
                                length = new Double(((double)w - widthPainted) / chWidth).intValue();
                                if (pos + length > nextTag) {
                                    length = nextTag - pos;
                                }
                                goToNextRow = true;
                            }
                        } else {
                            x = origX;
                            y = (int)((double)y + r.getHeight());
                            heightPainted += r.getHeight();
                            boundsChanged = false;
                            while (!done && Character.isWhitespace(chars[pos]) && pos < nextTag) {
                                boundsChanged = true;
                                done = ++pos == chars.length - 1;
                            }
                            if (pos == nextTag) {
                                lastWasWhitespace = true;
                            }
                            if (boundsChanged) {
                                r = fm.getStringBounds(chars, pos, nextTag + 1, g);
                                if (Utilities.isMac()) {
                                    r.setRect(r.getX(), r.getY(), fm.stringWidth(new String(chars, pos, nextTag - pos + 1)), r.getHeight());
                                }
                            }
                            goToNextRow = false;
                            widthPainted = 0.0;
                            if (chars[pos - 1 + length] == '<') {
                                --length;
                            }
                        }
                    }
                }
            }
            if (done) continue;
            if (paint) {
                g.drawChars(chars, pos, length, x, y);
            }
            if (strikethrough || underline || link) {
                lm = fm.getLineMetrics(chars, pos, length - 1, g);
                lineWidth = new Double((double)x + r.getWidth()).intValue();
                if (paint) {
                    if (strikethrough) {
                        stPos = Math.round(lm.getStrikethroughOffset()) + g.getFont().getBaselineFor(chars[pos]) + 1;
                        g.drawLine(x, y + stPos, lineWidth, y + stPos);
                    }
                    if (underline || link) {
                        stPos = Math.round(lm.getUnderlineOffset()) + g.getFont().getBaselineFor(chars[pos]) + 1;
                        g.drawLine(x, y + stPos, lineWidth, y + stPos);
                    }
                }
            }
            if (goToNextRow) {
                x = origX;
                y = (int)((double)y + r.getHeight());
                heightPainted += r.getHeight();
                widthPainted = 0.0;
                pos += length;
                while (pos < chars.length && Character.isWhitespace(chars[pos]) && chars[pos] != '<') {
                    ++pos;
                }
                lastWasWhitespace = true;
                done |= pos >= chars.length;
            } else {
                x = (int)((double)x + r.getWidth());
                widthPainted += r.getWidth();
                lastWasWhitespace = Character.isWhitespace(chars[nextTag]);
                pos = nextTag + 1;
            }
            done |= nextTag == chars.length;
        }
        if (style == 2) return heightPainted + lastHeight;
        return widthPainted;
    }

    private static Color findColor(char[] ch, int pos, int tagEnd) {
        String s;
        int colorPos = pos;
        boolean useUIManager = false;
        for (int i = pos; i < tagEnd; ++i) {
            if (ch[i] != 'c') continue;
            if (i + 6 >= ch.length) break;
            colorPos = i + 6;
            if (ch[colorPos] == '\'' || ch[colorPos] == '\"') {
                ++colorPos;
            }
            if (ch[colorPos] == '#') {
                ++colorPos;
                break;
            }
            if (ch[colorPos] != '!') break;
            useUIManager = true;
            ++colorPos;
            break;
        }
        if (colorPos == pos) {
            String out = "Could not find color identifier in font declaration";
            HtmlRenderer.throwBadHTML(out, pos, ch);
        }
        if (useUIManager) {
            int end = ch.length - 1;
            for (int i2 = colorPos; i2 < ch.length; ++i2) {
                if (ch[i2] != '\"' && ch[i2] != '\'') continue;
                end = i2;
                break;
            }
            s = new String(ch, colorPos, end - colorPos);
        } else {
            s = new String(ch, colorPos, Math.min(ch.length - colorPos, 6));
        }
        Color result = null;
        if (useUIManager) {
            result = UIManager.getColor(s);
            if (result == null) {
                HtmlRenderer.throwBadHTML("Could not resolve logical font declared in HTML: " + s, pos, ch);
                result = UIManager.getColor("textText");
                if (result == null) {
                    result = Color.BLACK;
                }
            }
        } else {
            try {
                int rgb = Integer.parseInt(s, 16);
                result = new Color(rgb);
            }
            catch (NumberFormatException nfe) {
                HtmlRenderer.throwBadHTML("Illegal hexadecimal color text: " + s + " in HTML string", colorPos, ch);
            }
        }
        if (result == null) {
            HtmlRenderer.throwBadHTML("Unresolvable html color: " + s + " in HTML string \n  ", pos, ch);
        }
        return result;
    }

    private static final Font deriveFont(Font f, int style) {
        Font result = Utilities.isMac() ? new Font(f.getName(), style, f.getSize()) : f.deriveFont(style);
        return result;
    }

    private static final int substEntity(char[] ch, int pos) {
        if (pos >= ch.length - 2) {
            return -1;
        }
        if (ch[pos] == '#') {
            return HtmlRenderer.substNumericEntity(ch, pos + 1);
        }
        for (int i = 0; i < entities.length; ++i) {
            char[] c = (char[])entities[i];
            boolean match = true;
            if (c.length < ch.length - pos) {
                for (int j = 0; j < c.length; ++j) {
                    match &= c[j] == ch[j + pos];
                }
            } else {
                match = false;
            }
            if (!match || ch[pos + c.length] != ';') continue;
            ch[pos + c.length] = entitySubstitutions[i];
            return pos + c.length;
        }
        return -1;
    }

    private static final int substNumericEntity(char[] ch, int pos) {
        for (int i = pos; i < ch.length; ++i) {
            if (ch[i] != ';') continue;
            try {
                ch[i] = (char)Integer.parseInt(new String(ch, pos, i - pos));
                return i;
            }
            catch (NumberFormatException nfe) {
                HtmlRenderer.throwBadHTML("Unparsable numeric entity: " + new String(ch, pos, i - pos), pos, ch);
            }
        }
        return -1;
    }

    private static void throwBadHTML(String msg, int pos, char[] chars) {
        char[] chh = new char[pos];
        Arrays.fill(chh, ' ');
        chh[pos - 1] = 94;
        String out = msg + "\n  " + new String(chars) + "\n  " + new String(chh) + "\n Full HTML string:" + new String(chars);
        if (!STRICT_HTML) {
            if (LOG.isLoggable(Level.WARNING)) {
                if (badStrings == null) {
                    badStrings = new HashSet<String>();
                }
                if (!badStrings.contains(msg)) {
                    StringTokenizer tk = new StringTokenizer(out, "\n", false);
                    while (tk.hasMoreTokens()) {
                        LOG.warning(tk.nextToken());
                    }
                    badStrings.add(msg.intern());
                }
            }
        } else {
            throw new IllegalArgumentException(out);
        }
    }

    public static interface Renderer
    extends TableCellRenderer,
    TreeCellRenderer,
    ListCellRenderer {
        public void setParentFocused(boolean var1);

        public void setCentered(boolean var1);

        public void setIndent(int var1);

        public void setHtml(boolean var1);

        public void setRenderStyle(int var1);

        public void setIcon(Icon var1);

        public void reset();

        public void setText(String var1);

        public void setIconTextGap(int var1);
    }

}

