/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.openide.awt;

import javax.swing.JTabbedPane;
import org.openide.util.Lookup;

public class TabbedPaneFactory {
    public static final String PROP_CLOSE = "close";
    public static final String NO_CLOSE_BUTTON = "noCloseButton";

    public static TabbedPaneFactory getDefault() {
        TabbedPaneFactory res = (TabbedPaneFactory)Lookup.getDefault().lookup(TabbedPaneFactory.class);
        if (null == res) {
            return new TabbedPaneFactory();
        }
        return res;
    }

    public JTabbedPane createTabbedPane() {
        return new JTabbedPane();
    }

    public static JTabbedPane createCloseButtonTabbedPane() {
        return TabbedPaneFactory.getDefault().createTabbedPane();
    }
}

