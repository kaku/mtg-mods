/*
 * Decompiled with CFR 0_118.
 */
package org.openide.awt;

import java.awt.event.ActionEvent;
import java.util.List;

interface ContextActionPerformer<T> {
    public void actionPerformed(ActionEvent var1, List<? extends T> var2);
}

