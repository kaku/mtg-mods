/*
 * Decompiled with CFR 0_118.
 */
package org.openide.awt;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.Vector;
import javax.swing.AbstractListModel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

@Deprecated
public class ListPane
extends JList {
    static final long serialVersionUID = 3828318151121500783L;
    private int fixedCellWidth = 100;
    private int fixedCellHeight = 100;
    private int visibleRowCount = 6;
    private int visibleColumnCount = 4;
    private int realRowCount = 1;
    private int realColumnCount = 1;
    ListDataListener dataL;
    PropertyChangeListener propertyL;
    InputListener inputL;
    ListSelectionListener selectionL;
    boolean updateLayoutStateNeeded = true;

    public ListPane(ListModel dataModel) {
        super(dataModel);
        this.addListListeners();
    }

    public ListPane(Object[] listData) {
        this(new AbstractListModel(){

            @Override
            public int getSize() {
                return val$listData.length;
            }

            @Override
            public Object getElementAt(int i) {
                return val$listData[i];
            }
        });
    }

    public ListPane(Vector listData) {
        this(new AbstractListModel(){

            @Override
            public int getSize() {
                return Vector.this.size();
            }

            @Override
            public Object getElementAt(int i) {
                return Vector.this.elementAt(i);
            }
        });
    }

    public ListPane() {
        this(new AbstractListModel(){

            @Override
            public int getSize() {
                return 0;
            }

            @Override
            public Object getElementAt(int i) {
                return null;
            }
        });
    }

    @Override
    public boolean isOpaque() {
        return true;
    }

    public int getVisibleColumnCount() {
        return this.visibleColumnCount;
    }

    public void setVisibleColumnCount(int visibleColumnCount) {
        int oldValue = this.visibleColumnCount;
        this.visibleColumnCount = Math.max(0, visibleColumnCount);
        this.firePropertyChange("visibleColumnCount", oldValue, visibleColumnCount);
    }

    @Override
    public void ensureIndexIsVisible(int index) {
        Point first = this.indexToLocation(index);
        if (first != null) {
            Rectangle cellBounds = new Rectangle(first.x, first.y, this.fixedCellWidth, this.fixedCellHeight);
            this.scrollRectToVisible(cellBounds);
        }
    }

    @Override
    public int locationToIndex(Point location) {
        int x = location.x / this.fixedCellWidth;
        if (x >= this.realColumnCount) {
            return -1;
        }
        int y = location.y / this.fixedCellHeight;
        if (y >= this.realRowCount) {
            return -1;
        }
        int ret = y * this.realColumnCount + x;
        return ret >= this.getModel().getSize() ? -1 : ret;
    }

    @Override
    public Point indexToLocation(int index) {
        if (index >= this.getModel().getSize()) {
            return null;
        }
        int y = index / this.realColumnCount;
        int x = index % this.realColumnCount;
        return new Point(x * this.fixedCellWidth, y * this.fixedCellHeight);
    }

    @Override
    public Rectangle getCellBounds(int index1, int index2) {
        Point p1 = this.indexToLocation(index1);
        Point p2 = this.indexToLocation(index2);
        int x1 = p1.x;
        int y1 = p1.y;
        int x2 = p2.x + this.fixedCellWidth;
        int y2 = p2.y + this.fixedCellHeight;
        if (p1.y != p2.y) {
            x1 = 0;
            x2 = this.fixedCellWidth * this.realColumnCount;
        }
        return new Rectangle(x1, y1, x2 - x1, y2 - y1);
    }

    @Override
    public Dimension getPreferredScrollableViewportSize() {
        Insets insets = this.getInsets();
        int w = insets.left + insets.right + this.visibleColumnCount * this.fixedCellWidth;
        int h = insets.top + insets.bottom + this.visibleRowCount * this.fixedCellHeight;
        Dimension dim = new Dimension(w, h);
        return dim;
    }

    @Override
    public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
        if (orientation == 0) {
            return 1;
        }
        int row = this.getFirstVisibleIndex();
        if (row == -1) {
            return 0;
        }
        if (direction > 0) {
            Rectangle r = this.getCellBounds(row, row);
            return r == null ? 0 : r.height - (visibleRect.y - r.y);
        }
        Rectangle r = this.getCellBounds(row, row);
        if (r.y == visibleRect.y && row == 0) {
            return 0;
        }
        if (r.y == visibleRect.y) {
            Rectangle prevR = this.getCellBounds(row - 1, row - 1);
            return prevR == null ? 0 : prevR.height;
        }
        return visibleRect.y - r.y;
    }

    @Override
    public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
        return orientation == 1 ? visibleRect.height : visibleRect.width;
    }

    @Override
    public boolean getScrollableTracksViewportWidth() {
        return true;
    }

    @Override
    public boolean getScrollableTracksViewportHeight() {
        return false;
    }

    protected void paintBackground(Graphics g) {
        if (this.isOpaque()) {
            Color backup = g.getColor();
            g.setColor(this.getBackground());
            g.fillRect(0, 0, this.getWidth(), this.getHeight());
            g.setColor(backup);
        }
    }

    private void paintCell(Graphics g, int index) {
        Object value = this.getModel().getElementAt(index);
        boolean cellHasFocus = this.hasFocus() && index == this.getSelectionModel().getLeadSelectionIndex();
        boolean isSelected = this.getSelectionModel().isSelectedIndex(index);
        Component renderer = this.getCellRenderer().getListCellRendererComponent(this, value, index, isSelected, cellHasFocus);
        renderer.setSize(this.fixedCellWidth, this.fixedCellHeight);
        renderer.paint(g);
    }

    @Override
    protected void paintComponent(Graphics g) {
        this.updateLayoutState();
        if (this.getCellRenderer() == null) {
            return;
        }
        this.paintBackground(g);
        int last = this.getModel().getSize();
        for (int i = 0; i < last; ++i) {
            int dy;
            int dx;
            this.paintCell(g, i);
            if ((i + 1) % this.realColumnCount == 0) {
                dx = (- this.fixedCellWidth) * (this.realColumnCount - 1);
                dy = this.fixedCellHeight;
            } else {
                dx = this.fixedCellWidth;
                dy = 0;
            }
            g.translate(dx, dy);
        }
    }

    private void updateLayoutState() {
        int y;
        Dimension d = this.getSize();
        int x = d.width / this.fixedCellWidth;
        if (x < 1) {
            x = 1;
        }
        if (x != this.realColumnCount) {
            this.realColumnCount = x;
            this.updateLayoutStateNeeded = true;
        }
        if ((y = d.height / this.fixedCellHeight) != this.realRowCount) {
            this.realRowCount = y;
            this.updateLayoutStateNeeded = true;
        }
        while (this.realRowCount * this.realColumnCount < this.getModel().getSize()) {
            ++this.realRowCount;
        }
        this.locationToIndex(this.getVisibleRect().getLocation());
        if (this.updateLayoutStateNeeded) {
            this.updateLayoutStateNeeded = false;
            this.revalidate();
        }
    }

    @Override
    public Dimension getPreferredSize() {
        Insets insets = this.getInsets();
        int max = this.getModel().getSize() - 1;
        if (max <= 0) {
            return new Dimension(this.fixedCellWidth, this.fixedCellHeight);
        }
        int y = max / this.realColumnCount + 1;
        int x = max < this.realColumnCount ? max + 1 : this.realColumnCount;
        int xParent = this.getParent().getSize().width;
        int yParent = this.getParent().getSize().height;
        int xRes = Math.max(xParent, x * this.fixedCellWidth);
        int yRes = Math.max(yParent, y * this.fixedCellHeight);
        Dimension d = new Dimension(xRes, yRes);
        return d;
    }

    @Override
    public Dimension getMinimumSize() {
        return new Dimension(this.fixedCellWidth, this.fixedCellHeight);
    }

    private void addListListeners() {
        this.inputL = this.createInputListener();
        this.addMouseListener(this.inputL);
        this.addKeyListener(this.inputL);
        this.addFocusListener(this.inputL);
        this.propertyL = this.createPropertyListener();
        this.addPropertyChangeListener(this.propertyL);
        this.dataL = this.createDataListener();
        ListModel model = this.getModel();
        if (model != null) {
            model.addListDataListener(this.dataL);
        }
        if (this.selectionL == null) {
            this.selectionL = new ListSelectionListener(){

                @Override
                public void valueChanged(ListSelectionEvent e) {
                    ListPane.this.repaint();
                }
            };
            ListSelectionModel selectionModel = this.getSelectionModel();
            if (selectionModel != null) {
                selectionModel.addListSelectionListener(this.selectionL);
            }
        }
    }

    private InputListener createInputListener() {
        return new InputListener();
    }

    private ListDataListener createDataListener() {
        return new DataListener();
    }

    private PropertyChangeListener createPropertyListener() {
        return new PropertyListener();
    }

    private void mySetSelectionInterval(int anchor, int lead) {
        super.setSelectionInterval(anchor, lead);
    }

    private void myAddSelectionInterval(int anchor, int lead) {
        super.addSelectionInterval(anchor, lead);
    }

    private void myRemoveSelectionInterval(int index0, int index1) {
        super.removeSelectionInterval(index0, index1);
    }

    @Override
    public void setSelectionInterval(int anchor, int lead) {
    }

    @Override
    public void addSelectionInterval(int anchor, int lead) {
    }

    @Override
    public void removeSelectionInterval(int index0, int index1) {
    }

    private class PropertyListener
    implements PropertyChangeListener,
    Serializable {
        static final long serialVersionUID = -6765578311995604737L;

        PropertyListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent e) {
            String propertyName = e.getPropertyName();
            if (propertyName.equals("model")) {
                ListModel oldModel = (ListModel)e.getOldValue();
                ListModel newModel = (ListModel)e.getNewValue();
                if (oldModel != null) {
                    oldModel.removeListDataListener(ListPane.this.dataL);
                }
                if (newModel != null) {
                    newModel.addListDataListener(ListPane.this.dataL);
                    ListPane.this.updateLayoutStateNeeded = true;
                    ListPane.this.repaint();
                }
            } else if (propertyName.equals("selectionModel")) {
                ListSelectionModel oldModelS = (ListSelectionModel)e.getOldValue();
                ListSelectionModel newModelS = (ListSelectionModel)e.getNewValue();
                if (oldModelS != null) {
                    oldModelS.removeListSelectionListener(ListPane.this.selectionL);
                }
                if (newModelS != null) {
                    newModelS.addListSelectionListener(ListPane.this.selectionL);
                }
                ListPane.this.updateLayoutStateNeeded = true;
                ListPane.this.repaint();
            } else if (propertyName.equals("cellRenderer") || propertyName.equals("font") || propertyName.equals("fixedCellHeight") || propertyName.equals("fixedCellWidth")) {
                ListPane.this.updateLayoutStateNeeded = true;
                ListPane.this.repaint();
            }
        }
    }

    private class DataListener
    implements ListDataListener,
    Serializable {
        static final long serialVersionUID = -2252515707418441L;

        DataListener() {
        }

        @Override
        public void intervalAdded(ListDataEvent e) {
            ListPane.this.updateLayoutStateNeeded = true;
            int minIndex = Math.min(e.getIndex0(), e.getIndex1());
            int maxIndex = Math.max(e.getIndex0(), e.getIndex1());
            ListSelectionModel sm = ListPane.this.getSelectionModel();
            if (sm != null) {
                sm.insertIndexInterval(minIndex, maxIndex - minIndex, true);
            }
        }

        @Override
        public void intervalRemoved(ListDataEvent e) {
            ListPane.this.updateLayoutStateNeeded = true;
            ListSelectionModel sm = ListPane.this.getSelectionModel();
            if (sm != null) {
                sm.removeIndexInterval(e.getIndex0(), e.getIndex1());
            }
        }

        @Override
        public void contentsChanged(ListDataEvent e) {
            ListPane.this.updateLayoutStateNeeded = true;
        }
    }

    private class InputListener
    extends MouseAdapter
    implements FocusListener,
    KeyListener,
    Serializable {
        static final long serialVersionUID = -7907848327510962576L;
        transient int dragFirstIndex;
        transient int dragLastIndex;

        InputListener() {
            this.dragFirstIndex = -1;
            this.dragLastIndex = -1;
        }

        @Override
        public void mousePressed(MouseEvent e) {
            this.updateSelection(ListPane.this.locationToIndex(e.getPoint()), e);
            if (!ListPane.this.hasFocus()) {
                ListPane.this.requestFocus();
            }
        }

        @Override
        public void focusGained(FocusEvent e) {
            this.repaintCellFocus();
        }

        @Override
        public void focusLost(FocusEvent e) {
            this.repaintCellFocus();
        }

        protected void repaintCellFocus() {
            ListPane.this.repaint();
        }

        @Override
        public void keyTyped(KeyEvent e) {
        }

        /*
         * Enabled force condition propagation
         * Lifted jumps to return sites
         */
        @Override
        public void keyPressed(KeyEvent e) {
            int s = ListPane.this.getLeadSelectionIndex();
            if (s < 0) {
                if (ListPane.this.getModel().getSize() <= 0) return;
                s = 0;
            } else {
                switch (e.getKeyCode()) {
                    case 37: {
                        --s;
                        break;
                    }
                    case 39: {
                        ++s;
                        break;
                    }
                    case 38: {
                        s -= ListPane.this.realColumnCount;
                        break;
                    }
                    case 40: {
                        s += ListPane.this.realColumnCount;
                        break;
                    }
                    case 36: {
                        s = 0;
                        break;
                    }
                    case 35: {
                        s = ListPane.this.getModel().getSize() - 1;
                        break;
                    }
                    case 33: {
                        s -= ListPane.this.realColumnCount * ListPane.this.realRowCount;
                        break;
                    }
                    case 34: {
                        s += ListPane.this.realColumnCount * ListPane.this.realRowCount;
                        break;
                    }
                    default: {
                        return;
                    }
                }
            }
            if (s < 0) {
                s = 0;
            }
            if (s > ListPane.this.getModel().getSize() - 1) {
                s = ListPane.this.getModel().getSize() - 1;
            }
            if (s < 0) return;
            this.updateSelection(s, e);
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }

        protected void updateSelection(int index, InputEvent e) {
            ListSelectionModel sm = ListPane.this.getSelectionModel();
            if (index != -1) {
                ListPane.this.setValueIsAdjusting(true);
                if (e.isShiftDown()) {
                    if (e.isControlDown()) {
                        if (this.dragFirstIndex == -1) {
                            ListPane.this.myAddSelectionInterval(index, index);
                        } else if (this.dragLastIndex == -1) {
                            ListPane.this.myAddSelectionInterval(this.dragFirstIndex, index);
                        } else {
                            ListPane.this.myRemoveSelectionInterval(this.dragFirstIndex, this.dragLastIndex);
                            ListPane.this.myAddSelectionInterval(this.dragFirstIndex, index);
                        }
                    } else if (this.dragFirstIndex == -1) {
                        ListPane.this.myAddSelectionInterval(index, index);
                    } else {
                        ListPane.this.mySetSelectionInterval(this.dragFirstIndex, index);
                    }
                    if (this.dragFirstIndex == -1) {
                        this.dragFirstIndex = index;
                        this.dragLastIndex = -1;
                    } else {
                        this.dragLastIndex = index;
                    }
                } else {
                    if (e.isControlDown()) {
                        if (ListPane.this.isSelectedIndex(index)) {
                            ListPane.this.myRemoveSelectionInterval(index, index);
                        } else {
                            ListPane.this.myAddSelectionInterval(index, index);
                        }
                    } else {
                        ListPane.this.mySetSelectionInterval(index, index);
                    }
                    this.dragFirstIndex = index;
                    this.dragLastIndex = -1;
                }
                ListPane.this.setValueIsAdjusting(false);
            } else {
                sm.clearSelection();
            }
        }
    }

}

