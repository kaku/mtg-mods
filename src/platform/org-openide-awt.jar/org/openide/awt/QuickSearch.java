/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.openide.awt;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.KeyboardFocusManager;
import java.awt.LayoutManager;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.ref.WeakReference;
import javax.activation.DataContentHandler;
import javax.activation.DataContentHandlerFactory;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.text.Document;
import org.openide.util.ImageUtilities;
import org.openide.util.RequestProcessor;

public class QuickSearch {
    private static final String ICON_FIND = "org/openide/awt/resources/quicksearch/find.png";
    private static final String ICON_FIND_WITH_MENU = "org/openide/awt/resources/quicksearch/findMenu.png";
    private static final Object CLIENT_PROPERTY_KEY = new Object();
    private final JComponent component;
    private final Object constraints;
    private final Callback callback;
    private final JMenu popupMenu;
    private final boolean asynchronous;
    private boolean enabled = true;
    private SearchTextField searchTextField;
    private KeyAdapter quickSearchKeyAdapter;
    private SearchFieldListener searchFieldListener;
    private JPanel searchPanel;
    private final RequestProcessor rp;
    private AnimationTimer animationTimer;
    private boolean alwaysShown = false;

    private QuickSearch(JComponent component, Object constraints, Callback callback, boolean asynchronous, JMenu popupMenu) {
        this.component = component;
        this.constraints = constraints;
        this.callback = callback;
        this.asynchronous = asynchronous;
        this.popupMenu = popupMenu;
        this.rp = asynchronous ? new RequestProcessor(QuickSearch.class) : null;
        this.setUpSearch();
    }

    public static QuickSearch attach(JComponent component, Object constraints, Callback callback) {
        return QuickSearch.attach(component, constraints, callback, false, null);
    }

    public static QuickSearch attach(JComponent component, Object constraints, Callback callback, boolean asynchronous) {
        return QuickSearch.attach(component, constraints, callback, asynchronous, null);
    }

    public static QuickSearch attach(JComponent component, Object constraints, Callback callback, JMenu popupMenu) {
        return QuickSearch.attach(component, constraints, callback, false, popupMenu);
    }

    public static QuickSearch attach(JComponent component, Object constraints, Callback callback, boolean asynchronous, JMenu popupMenu) {
        Object qso = component.getClientProperty(CLIENT_PROPERTY_KEY);
        if (qso instanceof QuickSearch) {
            throw new IllegalStateException("A quick search is attached to this component already, detach it first.");
        }
        QuickSearch qs = new QuickSearch(component, constraints, callback, asynchronous, popupMenu);
        component.putClientProperty(CLIENT_PROPERTY_KEY, qs);
        return qs;
    }

    public void detach() {
        this.setEnabled(false);
        this.component.putClientProperty(CLIENT_PROPERTY_KEY, null);
    }

    public boolean isAlwaysShown() {
        return this.alwaysShown;
    }

    public void setAlwaysShown(boolean alwaysShown) {
        this.alwaysShown = alwaysShown;
        if (alwaysShown) {
            this.displaySearchField();
        } else {
            this.removeSearchField();
        }
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        if (this.enabled == enabled) {
            return;
        }
        this.enabled = enabled;
        if (enabled) {
            this.component.addKeyListener(this.quickSearchKeyAdapter);
        } else {
            this.removeSearchField();
            this.component.removeKeyListener(this.quickSearchKeyAdapter);
        }
    }

    public void processKeyEvent(KeyEvent ke) {
        if (!this.isEnabled()) {
            return;
        }
        if (this.searchPanel != null) {
            if (!this.isKeyEventInSearchFieldIgnored(ke)) {
                this.searchTextField.setCaretPosition(this.searchTextField.getText().length());
                this.searchTextField.processKeyEvent(ke);
            }
        } else {
            switch (ke.getID()) {
                case 401: {
                    this.quickSearchKeyAdapter.keyPressed(ke);
                    break;
                }
                case 402: {
                    this.quickSearchKeyAdapter.keyReleased(ke);
                    break;
                }
                case 400: {
                    this.quickSearchKeyAdapter.keyTyped(ke);
                }
            }
        }
    }

    private boolean isKeyEventInSearchFieldIgnored(KeyEvent ke) {
        if (ke.getKeyCode() == 127) {
            return !this.searchTextField.isFocusOwner();
        }
        return false;
    }

    private void fireQuickSearchUpdate(String searchText) {
        if (this.asynchronous) {
            this.rp.post((Runnable)new LazyFire(QS_FIRE.UPDATE, searchText));
        } else {
            this.callback.quickSearchUpdate(searchText);
        }
    }

    private void fireShowNextSelection(boolean forward) {
        if (this.asynchronous) {
            this.rp.post((Runnable)new LazyFire(QS_FIRE.NEXT, forward));
        } else {
            this.callback.showNextSelection(forward);
        }
    }

    private void findMaxPrefix(String prefix, DataContentHandlerFactory newPrefixSetter) {
        if (this.asynchronous) {
            this.rp.post((Runnable)new LazyFire(QS_FIRE.MAX, prefix, newPrefixSetter));
        } else {
            prefix = this.callback.findMaxPrefix(prefix);
            newPrefixSetter.createDataContentHandler(prefix);
        }
    }

    private void setUpSearch() {
        this.searchTextField = new SearchTextField();
        this.quickSearchKeyAdapter = new KeyAdapter(){

            @Override
            public void keyTyped(KeyEvent e) {
                int modifiers = e.getModifiers();
                int keyCode = e.getKeyCode();
                char c = e.getKeyChar();
                if (c == '+' || c == '-' || c == ' ') {
                    return;
                }
                if (modifiers > 0 && modifiers != 1 || e.isActionKey()) {
                    return;
                }
                if (Character.isISOControl(c) || keyCode == 16 || keyCode == 27) {
                    return;
                }
                QuickSearch.this.displaySearchField();
                KeyStroke stroke = KeyStroke.getKeyStrokeForEvent(e);
                QuickSearch.this.searchTextField.setText(String.valueOf(stroke.getKeyChar()));
                e.consume();
            }
        };
        if (this.isEnabled()) {
            this.component.addKeyListener(this.quickSearchKeyAdapter);
        }
        this.searchFieldListener = new SearchFieldListener();
        this.searchTextField.addKeyListener(this.searchFieldListener);
        this.searchTextField.addFocusListener(this.searchFieldListener);
        this.searchTextField.getDocument().addDocumentListener(this.searchFieldListener);
        if (this.isAlwaysShown()) {
            this.displaySearchField();
        }
    }

    private void displaySearchField() {
        JLabel lbl;
        if (this.searchPanel != null || !this.isEnabled()) {
            return;
        }
        this.searchTextField.setOriginalFocusOwner();
        this.searchTextField.setFont(this.component.getFont());
        this.searchPanel = new SearchPanel(this.component, this.isAlwaysShown());
        if (this.popupMenu != null) {
            lbl = new JLabel(ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/quicksearch/findMenu.png", (boolean)false));
            lbl.addMouseListener(new MouseAdapter(){

                @Override
                public void mousePressed(MouseEvent e) {
                    QuickSearch.this.maybeShowPopup(e, lbl);
                }
            });
        } else {
            lbl = new JLabel(ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/quicksearch/find.png", (boolean)false));
        }
        this.animationTimer = this.asynchronous ? new AnimationTimer(lbl, lbl.getIcon()) : null;
        this.searchPanel.setLayout(new BoxLayout(this.searchPanel, 0));
        if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
            if (this.popupMenu != null) {
                final JPopupMenu dummy = new JPopupMenu();
                dummy.addPopupMenuListener(new PopupMenuListener(){

                    @Override
                    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                dummy.setVisible(false);
                            }
                        });
                    }

                    @Override
                    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                    }

                    @Override
                    public void popupMenuCanceled(PopupMenuEvent e) {
                    }

                });
                this.searchTextField.putClientProperty("JTextField.Search.FindPopup", dummy);
                this.searchTextField.putClientProperty("JTextField.Search.FindAction", new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        QuickSearch.this.maybeShowPopup(null, QuickSearch.this.searchTextField);
                    }
                });
            }
        } else {
            this.searchPanel.add(lbl);
        }
        this.searchPanel.add(this.searchTextField);
        this.searchPanel.setBackground(this.component.getBackground());
        lbl.setLabelFor(this.searchTextField);
        this.searchTextField.setColumns(10);
        this.searchTextField.setMaximumSize(this.searchTextField.getPreferredSize());
        this.searchTextField.putClientProperty("JTextField.variant", "search");
        lbl.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
        if (this.constraints == null) {
            this.component.add(this.searchPanel);
        } else {
            this.component.add((Component)this.searchPanel, this.constraints);
        }
        this.component.invalidate();
        this.component.revalidate();
        this.component.repaint();
        this.searchTextField.requestFocus();
        this.searchTextField.selectAll();
    }

    protected void maybeShowPopup(MouseEvent evt, Component comp) {
        if (evt != null && !SwingUtilities.isLeftMouseButton(evt)) {
            return;
        }
        JPopupMenu pm = this.popupMenu.getPopupMenu();
        pm.show(comp, 0, comp.getHeight() - 1);
        this.searchTextField.setText("");
        this.searchTextField.requestOriginalFocusOwner();
    }

    private void removeSearchField() {
        if (this.isAlwaysShown()) {
            return;
        }
        if (this.searchPanel == null) {
            return;
        }
        if (this.animationTimer != null) {
            this.animationTimer.stopProgressAnimation();
        }
        JPanel sp = this.searchPanel;
        this.searchPanel = null;
        this.component.remove(sp);
        this.component.invalidate();
        this.component.revalidate();
        this.component.repaint();
    }

    JTextField getSearchField() {
        return this.searchTextField;
    }

    public static String findMaxPrefix(String str1, String str2, boolean ignoreCase) {
        int i;
        int n1 = str1.length();
        int n2 = str2.length();
        if (ignoreCase) {
            char c1;
            char c2;
            for (i = 0; i < n1 && i < n2 && (c1 = Character.toUpperCase(str1.charAt(i))) == (c2 = Character.toUpperCase(str2.charAt(i))); ++i) {
            }
        } else {
            char c2;
            char c1;
            while (i < n1 && i < n2 && (c1 = str1.charAt(i)) == (c2 = str2.charAt(i))) {
                ++i;
            }
        }
        return str1.substring(0, i);
    }

    public static interface Callback {
        public void quickSearchUpdate(String var1);

        public void showNextSelection(boolean var1);

        public String findMaxPrefix(String var1);

        public void quickSearchConfirmed();

        public void quickSearchCanceled();
    }

    private class SearchFieldListener
    extends KeyAdapter
    implements DocumentListener,
    FocusListener {
        private boolean ignoreEvents;

        SearchFieldListener() {
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            if (this.ignoreEvents) {
                return;
            }
            this.searchForNode();
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            if (this.ignoreEvents) {
                return;
            }
            this.searchForNode();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            if (this.ignoreEvents) {
                return;
            }
            this.searchForNode();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void keyPressed(KeyEvent e) {
            int keyCode = e.getKeyCode();
            if (keyCode == 27) {
                QuickSearch.this.removeSearchField();
                QuickSearch.this.searchTextField.requestOriginalFocusOwner();
                this.ignoreEvents = true;
                try {
                    QuickSearch.this.searchTextField.setText("");
                }
                finally {
                    this.ignoreEvents = false;
                }
                QuickSearch.this.callback.quickSearchCanceled();
                e.consume();
            } else if (keyCode == 38 || keyCode == 114 && e.isShiftDown()) {
                QuickSearch.this.fireShowNextSelection(false);
                e.consume();
            } else if (keyCode == 40 || keyCode == 114) {
                QuickSearch.this.fireShowNextSelection(true);
                e.consume();
            } else if (keyCode == 9) {
                QuickSearch.this.findMaxPrefix(QuickSearch.this.searchTextField.getText(), new DataContentHandlerFactory(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public DataContentHandler createDataContentHandler(final String maxPrefix) {
                        if (!SwingUtilities.isEventDispatchThread()) {
                            SwingUtilities.invokeLater(new Runnable(){

                                @Override
                                public void run() {
                                    1.this.createDataContentHandler(maxPrefix);
                                    QuickSearch.this.searchTextField.transferFocus();
                                }
                            });
                            return null;
                        }
                        SearchFieldListener.this.ignoreEvents = true;
                        try {
                            QuickSearch.this.searchTextField.setText(maxPrefix);
                        }
                        finally {
                            SearchFieldListener.this.ignoreEvents = false;
                        }
                        return null;
                    }

                });
                e.consume();
            } else if (keyCode == 10) {
                QuickSearch.this.removeSearchField();
                QuickSearch.this.callback.quickSearchConfirmed();
                QuickSearch.this.component.requestFocusInWindow();
                e.consume();
            }
        }

        private void searchForNode() {
            String text = QuickSearch.this.searchTextField.getText();
            QuickSearch.this.fireQuickSearchUpdate(text);
        }

        @Override
        public void focusGained(FocusEvent e) {
            if (e.getSource() == QuickSearch.this.searchTextField) {
                int n = QuickSearch.this.searchTextField.getText().length();
                QuickSearch.this.searchTextField.select(n, n);
            }
        }

        @Override
        public void focusLost(FocusEvent e) {
            if (e.isTemporary() || QuickSearch.this.isAlwaysShown()) {
                return;
            }
            Component oppositeComponent = e.getOppositeComponent();
            if (e.getSource() != QuickSearch.this.searchTextField) {
                ((Component)e.getSource()).removeFocusListener(this);
            }
            if (oppositeComponent instanceof JMenuItem || oppositeComponent instanceof JPopupMenu) {
                oppositeComponent.addFocusListener(this);
                return;
            }
            if (oppositeComponent == QuickSearch.this.searchTextField) {
                return;
            }
            if (QuickSearch.this.searchPanel != null) {
                QuickSearch.this.removeSearchField();
                QuickSearch.this.callback.quickSearchCanceled();
            }
        }

    }

    private class SearchTextField
    extends JTextField {
        private WeakReference<Component> originalFocusOwner;

        public SearchTextField() {
            this.originalFocusOwner = new WeakReference<Object>(null);
        }

        void setOriginalFocusOwner() {
            Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            this.originalFocusOwner = focusOwner != null && QuickSearch.this.component.isAncestorOf(focusOwner) ? new WeakReference<Component>(focusOwner) : new WeakReference<JComponent>(QuickSearch.this.component);
        }

        void requestOriginalFocusOwner() {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    Component fo = (Component)SearchTextField.this.originalFocusOwner.get();
                    if (fo != null) {
                        fo.requestFocusInWindow();
                    }
                }
            });
        }

        @Override
        public boolean isManagingFocus() {
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void processKeyEvent(KeyEvent ke) {
            if (ke.getKeyCode() == 27) {
                QuickSearch.this.removeSearchField();
                ke.consume();
                QuickSearch.this.searchFieldListener.ignoreEvents = true;
                try {
                    this.setText("");
                }
                finally {
                    QuickSearch.this.searchFieldListener.ignoreEvents = false;
                }
                this.requestOriginalFocusOwner();
                QuickSearch.this.callback.quickSearchCanceled();
            } else {
                super.processKeyEvent(ke);
            }
        }

    }

    private static class SearchPanel
    extends JPanel {
        public static final boolean isAquaLaF = "Aqua".equals(UIManager.getLookAndFeel().getID());
        private JComponent component;
        private boolean alwaysShown = false;

        public SearchPanel(JComponent component, boolean alwaysShown) {
            this.component = component;
            this.alwaysShown = alwaysShown;
            if (isAquaLaF) {
                this.setBorder(BorderFactory.createEmptyBorder(9, 6, 8, 2));
            } else {
                this.setBorder(BorderFactory.createEmptyBorder(2, 6, 2, 2));
            }
            this.setOpaque(true);
        }

        @Override
        protected void paintComponent(Graphics g) {
            if (isAquaLaF && g instanceof Graphics2D) {
                Graphics2D g2d = (Graphics2D)g;
                if (this.alwaysShown) {
                    g2d.setColor(this.component.getBackground());
                    g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
                } else {
                    g2d.setPaint(new GradientPaint(0.0f, 0.0f, UIManager.getColor("NbExplorerView.quicksearch.background.top"), 0.0f, this.getHeight(), UIManager.getColor("NbExplorerView.quicksearch.background.bottom")));
                    g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
                    g2d.setColor(UIManager.getColor("NbExplorerView.quicksearch.border"));
                    g2d.drawLine(0, 0, this.getWidth(), 0);
                }
            } else {
                super.paintComponent(g);
            }
        }
    }

    private class LazyFire
    implements Runnable {
        private final QS_FIRE fire;
        private final String searchText;
        private final boolean forward;
        private final DataContentHandlerFactory newPrefixSetter;

        LazyFire(QS_FIRE fire, String searchText) {
            this(fire, searchText, true, null);
        }

        LazyFire(QS_FIRE fire, boolean forward) {
            this(fire, null, forward);
        }

        LazyFire(QS_FIRE fire, String searchText, boolean forward) {
            this(fire, searchText, forward, null);
        }

        LazyFire(QS_FIRE fire, String searchText, DataContentHandlerFactory newPrefixSetter) {
            this(fire, searchText, true, newPrefixSetter);
        }

        LazyFire(QS_FIRE fire, String searchText, boolean forward, DataContentHandlerFactory newPrefixSetter) {
            this.fire = fire;
            this.searchText = searchText;
            this.forward = forward;
            this.newPrefixSetter = newPrefixSetter;
            QuickSearch.this.animationTimer.startProgressAnimation();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         * Enabled force condition propagation
         * Lifted jumps to return sites
         */
        @Override
        public void run() {
            try {
                switch (this.fire) {
                    case UPDATE: {
                        QuickSearch.this.callback.quickSearchUpdate(this.searchText);
                        return;
                    }
                    case NEXT: {
                        QuickSearch.this.callback.showNextSelection(this.forward);
                        return;
                    }
                    case MAX: {
                        String mp = QuickSearch.this.callback.findMaxPrefix(this.searchText);
                        this.newPrefixSetter.createDataContentHandler(mp);
                    }
                }
            }
            finally {
                QuickSearch.this.animationTimer.stopProgressAnimation();
            }
        }
    }

    private static final class AnimationTimer {
        private final JLabel jLabel;
        private final Icon findIcon;
        private final Timer animationTimer;

        public AnimationTimer(final JLabel jLabel, Icon findIcon) {
            this.jLabel = jLabel;
            this.findIcon = findIcon;
            this.animationTimer = new Timer(100, new ActionListener(){
                ImageIcon[] icons;
                int index;

                @Override
                public void actionPerformed(ActionEvent e) {
                    if (this.icons == null) {
                        this.icons = new ImageIcon[8];
                        for (int i = 0; i < 8; ++i) {
                            this.icons[i] = ImageUtilities.loadImageIcon((String)("org/openide/awt/resources/quicksearch/progress_" + i + ".png"), (boolean)false);
                        }
                    }
                    jLabel.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 6));
                    jLabel.setIcon(this.icons[this.index]);
                    jLabel.repaint();
                    this.index = (this.index + 1) % 8;
                }
            });
        }

        public void startProgressAnimation() {
            if (this.animationTimer != null && !this.animationTimer.isRunning()) {
                this.animationTimer.start();
            }
        }

        public void stopProgressAnimation() {
            if (this.animationTimer != null && this.animationTimer.isRunning()) {
                this.animationTimer.stop();
                this.jLabel.setIcon(this.findIcon);
                this.jLabel.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
            }
        }

    }

    private static enum QS_FIRE {
        UPDATE,
        NEXT,
        MAX;
        

        private QS_FIRE() {
        }
    }

}

