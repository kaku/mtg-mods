/*
 * Decompiled with CFR 0_118.
 */
package org.openide.awt;

import java.awt.Component;

public interface StatusLineElementProvider {
    public Component getStatusLineElement();
}

