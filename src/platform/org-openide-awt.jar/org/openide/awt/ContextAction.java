/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 */
package org.openide.awt;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.swing.Action;
import org.openide.awt.ContextActionEnabler;
import org.openide.awt.ContextActionPerformer;
import org.openide.awt.ContextManager;
import org.openide.awt.ContextSelection;
import org.openide.awt.GeneralAction;
import org.openide.util.ContextAwareAction;
import org.openide.util.Lookup;

final class ContextAction<T>
implements Action,
ContextAwareAction {
    private final Class<T> type;
    final ContextSelection selectMode;
    private final Performer<? super T> performer;
    private final ContextManager global;
    private PropertyChangeSupport support;
    private boolean previousEnabled;

    public ContextAction(Performer<? super T> performer, ContextSelection selectMode, Lookup actionContext, Class<T> type, boolean surviveFocusChange) {
        if (performer == null) {
            throw new NullPointerException("Has to provide a key!");
        }
        this.type = type;
        this.selectMode = selectMode;
        this.performer = performer;
        this.global = ContextManager.findManager(actionContext, surviveFocusChange);
    }

    public String toString() {
        return super.toString() + "[type=" + this.type + ", performer=" + this.performer + "]";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.global.actionPerformed(e, this.performer, this.type, this.selectMode);
    }

    @Override
    public boolean isEnabled() {
        boolean r;
        assert (EventQueue.isDispatchThread());
        this.previousEnabled = r = this.global.isEnabled(this.type, this.selectMode, this.performer);
        return r;
    }

    @Override
    public synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
        boolean first = false;
        if (this.support == null) {
            this.support = new PropertyChangeSupport(this);
            first = true;
        }
        this.support.addPropertyChangeListener(listener);
        if (first) {
            this.global.registerListener(this.type, this);
        }
    }

    @Override
    public synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
        if (null != this.support) {
            this.support.removePropertyChangeListener(listener);
            if (!this.support.hasListeners(null)) {
                this.global.unregisterListener(this.type, this);
                this.support = null;
            }
        }
    }

    @Override
    public void putValue(String key, Object o) {
    }

    @Override
    public Object getValue(String key) {
        if ("enabler".equals(key)) {
            assert (EventQueue.isDispatchThread());
            this.updateState();
        }
        return null;
    }

    @Override
    public void setEnabled(boolean b) {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void updateState() {
        PropertyChangeSupport s;
        ContextAction contextAction = this;
        synchronized (contextAction) {
            s = this.support;
        }
        boolean prev = this.previousEnabled;
        if (s != null && prev != this.isEnabled()) {
            s.firePropertyChange("enabled", (Object)prev, (Object)(!prev));
        }
    }

    public Action createContextAwareInstance(Lookup actionContext) {
        return new ContextAction<T>(this.performer, this.selectMode, actionContext, this.type, this.global.isSurvive());
    }

    public int hashCode() {
        int t = this.type.hashCode();
        int m = this.selectMode.hashCode();
        int p = this.performer.hashCode();
        return (t << 2) + (m << 1) + p;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ContextAction) {
            ContextAction c = (ContextAction)obj;
            return this.type.equals(c.type) && this.selectMode.equals((Object)c.selectMode) && this.performer.equals(c.performer);
        }
        return false;
    }

    static class Performer<Data> {
        final Map delegate;

        public Performer(Map delegate) {
            this.delegate = delegate;
        }

        public Performer(ContextActionPerformer<Data> p, ContextActionEnabler<Data> e) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("delegate", p);
            map.put("enabler", e);
            this.delegate = map;
        }

        public void actionPerformed(ActionEvent ev, List<? extends Data> data, Lookup.Provider everything) {
            Object obj = this.delegate.get("delegate");
            if (obj instanceof ContextActionPerformer) {
                ContextActionPerformer perf = (ContextActionPerformer)obj;
                perf.actionPerformed(ev, data);
                return;
            }
            if (obj instanceof Performer) {
                Performer perf = (Performer)obj;
                perf.actionPerformed(ev, data, everything);
                return;
            }
            if (obj instanceof ContextAwareAction) {
                Action a = ((ContextAwareAction)obj).createContextAwareInstance(everything.getLookup());
                a.actionPerformed(ev);
                return;
            }
            GeneralAction.LOG.warning("No 'delegate' for " + this.delegate);
        }

        public boolean enabled(List<? extends Object> data) {
            Object o = this.delegate.get("enabler");
            if (o == null) {
                return true;
            }
            if (o instanceof ContextActionEnabler) {
                ContextActionEnabler en = (ContextActionEnabler)o;
                return en.enabled(data);
            }
            GeneralAction.LOG.warning("Wrong enabler for " + this.delegate + ":" + o);
            return false;
        }

        public int hashCode() {
            return this.delegate.hashCode() + 117;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof Performer) {
                Performer l = (Performer)obj;
                return this.delegate.equals(l.delegate);
            }
            return false;
        }
    }

}

