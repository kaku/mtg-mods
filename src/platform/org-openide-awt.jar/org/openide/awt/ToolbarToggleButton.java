/*
 * Decompiled with CFR 0_118.
 */
package org.openide.awt;

import javax.swing.Icon;
import javax.swing.JToggleButton;

@Deprecated
public class ToolbarToggleButton
extends JToggleButton {
    private static final long serialVersionUID = -4783163952526348942L;

    public ToolbarToggleButton() {
    }

    public ToolbarToggleButton(Icon icon) {
        super(icon);
    }

    public ToolbarToggleButton(Icon icon, boolean selected) {
        super(icon, selected);
    }
}

