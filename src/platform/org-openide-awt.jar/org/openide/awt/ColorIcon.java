/*
 * Decompiled with CFR 0_118.
 */
package org.openide.awt;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import javax.swing.Icon;

final class ColorIcon
implements Icon {
    private final Color color;
    private final int size;

    public ColorIcon(Color color, int size) {
        this.color = color;
        this.size = size;
    }

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        g.setColor(Color.black);
        g.drawRect(x, y, this.size - 1, this.size - 1);
        if (null == this.color) {
            g.drawLine(x, y + this.size - 1, x + this.size - 1, y);
        } else {
            g.setColor(this.color);
            g.fillRect(x + 1, y + 1, this.size - 2, this.size - 2);
        }
    }

    @Override
    public int getIconWidth() {
        return this.size;
    }

    @Override
    public int getIconHeight() {
        return this.size;
    }
}

