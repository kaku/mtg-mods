/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbPreferences
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.WeakSet
 *  org.openide.util.actions.ActionInvoker
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.util.actions.Presenter$Toolbar
 */
package org.openide.awt;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JToggleButton;
import org.openide.awt.Actions;
import org.openide.awt.Mnemonics;
import org.openide.util.ContextAwareAction;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbPreferences;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.WeakSet;
import org.openide.util.actions.ActionInvoker;
import org.openide.util.actions.Presenter;

class AlwaysEnabledAction
extends AbstractAction
implements PropertyChangeListener,
ContextAwareAction {
    private static final Logger LOG = Logger.getLogger(AlwaysEnabledAction.class.getName());
    private static final String PREFERENCES_NODE = "preferencesNode";
    private static final String PREFERENCES_KEY = "preferencesKey";
    private static final String PREFERENCES_DEFAULT = "preferencesDefault";
    final Map map;
    private final AlwaysEnabledAction parent;
    private PropertyChangeListener weakL;
    ActionListener delegate;
    final Lookup context;
    final Object equals;

    static AlwaysEnabledAction create(Map m) {
        return m.containsKey("preferencesKey") ? new CheckBox(m) : new AlwaysEnabledAction(m);
    }

    public AlwaysEnabledAction(Map m) {
        this.map = m;
        this.context = null;
        this.equals = this;
        this.parent = null;
    }

    AlwaysEnabledAction(Map m, AlwaysEnabledAction parent, Lookup context, Object equals) {
        this.map = m;
        this.parent = parent;
        this.context = context;
        this.equals = equals;
    }

    private static ActionListener bindToContext(ActionListener a, Lookup context) {
        if (context != null && a instanceof ContextAwareAction) {
            return ((ContextAwareAction)a).createContextAwareInstance(context);
        }
        return a;
    }

    protected ActionListener getDelegate() {
        if (this.delegate == null) {
            ActionListener al;
            if (this.parent == null) {
                Object listener = this.map.get("delegate");
                if (!(listener instanceof ActionListener)) {
                    throw new NullPointerException("No 'delegate' in " + this.map);
                }
                al = (ActionListener)listener;
            } else {
                al = this.parent.getDelegate();
            }
            this.delegate = AlwaysEnabledAction.bindToContext(al, this.context);
            if (this.delegate instanceof Action) {
                Action actionDelegate = (Action)this.delegate;
                if (this.weakL == null) {
                    this.weakL = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)actionDelegate);
                }
                actionDelegate.addPropertyChangeListener(this.weakL);
                this.syncActionDelegateProperty("Name", actionDelegate);
            }
        }
        return this.delegate;
    }

    private void syncActionDelegateProperty(String propertyName, Action actionDelegate) {
        Object value = AlwaysEnabledAction.extractCommonAttribute(this.map, propertyName);
        Object delegateValue = actionDelegate.getValue(propertyName);
        if (value != null) {
            if (delegateValue == null) {
                actionDelegate.putValue(propertyName, value);
            } else if (!delegateValue.equals(value)) {
                LOG.log(Level.FINE, "Value of property \"{0}\" of AlwaysEnabledAction is \"{1}\" but delegate {2} has \"{3}\"", new Object[]{propertyName, value, this.delegate, delegateValue});
            }
        }
    }

    @Override
    public boolean isEnabled() {
        if (this.delegate instanceof Action) {
            return ((Action)this.delegate).isEnabled();
        }
        return true;
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        assert (EventQueue.isDispatchThread());
        if (this.getDelegate() instanceof Action && !((Action)this.getDelegate()).isEnabled()) {
            Utilities.disabledActionBeep();
            this.firePropertyChange("enabled", null, this.isEnabled());
            return;
        }
        boolean async = Boolean.TRUE.equals(this.map.get("asynchronous"));
        Runnable ar = new Runnable(){

            @Override
            public void run() {
                AlwaysEnabledAction.this.getDelegate().actionPerformed(e);
            }
        };
        ActionInvoker.invokeAction((Action)this, (ActionEvent)e, (boolean)async, (Runnable)ar);
    }

    @Override
    public Object getValue(String name) {
        Object o;
        if (this.delegate instanceof Action) {
            Object ret = ((Action)this.delegate).getValue(name);
            if (ret != null) {
                return ret;
            }
            if ("iconBase".equals(name) && ((Action)this.delegate).getValue("SmallIcon") != null) {
                return null;
            }
        }
        return (o = AlwaysEnabledAction.extractCommonAttribute(this.map, name)) != null ? o : super.getValue(name);
    }

    static final Object extractCommonAttribute(Map fo, String name) {
        try {
            if ("Name".equals(name)) {
                String actionName = (String)fo.get("displayName");
                return actionName;
            }
            if ("MnemonicKey".equals(name)) {
                String actionName = (String)fo.get("displayName");
                if (null == actionName) {
                    return null;
                }
                int position = Mnemonics.findMnemonicAmpersand(actionName);
                if (position == -1) {
                    return null;
                }
                int vk = actionName.charAt(position + 1);
                if (vk >= 97 && vk <= 122) {
                    vk -= 32;
                }
                return vk;
            }
            if ("SmallIcon".equals(name)) {
                Image icon;
                Image image = icon = fo == null ? null : (Image)fo.get("iconBase");
                if (icon instanceof Icon) {
                    return (Icon)((Object)icon);
                }
                if (icon instanceof URL) {
                    icon = Toolkit.getDefaultToolkit().getImage((URL)((Object)icon));
                }
                if (icon instanceof Image) {
                    return ImageUtilities.image2Icon((Image)icon);
                }
                if (icon instanceof String) {
                    return ImageUtilities.loadImageIcon((String)((String)((Object)icon)), (boolean)true);
                }
            }
            if ("iconBase".equals(name)) {
                return fo == null ? null : fo.get("iconBase");
            }
            if ("noIconInMenu".equals(name)) {
                return fo == null ? null : fo.get("noIconInMenu");
            }
            if (!"delegate".equals(name) && !"instanceCreate".equals(name)) {
                return fo == null ? null : fo.get(name);
            }
        }
        catch (RuntimeException x) {
            LOG.log(Level.WARNING, "Could not get action attribute " + name, x);
        }
        return null;
    }

    public int hashCode() {
        if (this.equals == this) {
            return Object.super.hashCode();
        }
        return this.equals.hashCode();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof AlwaysEnabledAction) {
            AlwaysEnabledAction other = (AlwaysEnabledAction)obj;
            if (this == this.equals && other == other.equals) {
                return this == other;
            }
            if (this.equals.equals(other.equals)) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        return "AlwaysEnabledAction[" + this.getValue("Name") + "]";
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getSource() == this.delegate) {
            this.firePropertyChange(evt.getPropertyName(), evt.getOldValue(), evt.getNewValue());
        }
    }

    public Action createContextAwareInstance(Lookup actionContext) {
        return new AlwaysEnabledAction(this.map, this, actionContext, this.equals);
    }

    static class DefaultIconToggleButton
    extends JToggleButton {
        private Icon unknownIcon;

        DefaultIconToggleButton() {
        }

        @Override
        public Icon getIcon() {
            Icon retValue = super.getIcon();
            if (null == retValue && (null == this.getText() || this.getText().isEmpty())) {
                if (this.unknownIcon == null) {
                    this.unknownIcon = ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/unknown.gif", (boolean)false);
                }
                retValue = this.unknownIcon;
            }
            return retValue;
        }
    }

    static final class CheckBox
    extends AlwaysEnabledAction
    implements Presenter.Menu,
    Presenter.Popup,
    Presenter.Toolbar,
    PreferenceChangeListener,
    LookupListener {
        private static final long serialVersionUID = 1;
        private static final ActionListener EMPTY = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent ae) {
            }
        };
        private JCheckBoxMenuItem menuItem;
        private JCheckBoxMenuItem popupItem;
        private WeakSet<AbstractButton> toolbarItems;
        private Preferences preferencesNode;
        private Lookup.Result<Preferences> preferencesNodeResult;
        private boolean prefsListening;

        CheckBox(Map m) {
            super(m);
        }

        CheckBox(Map m, AlwaysEnabledAction parent, Lookup context, Object equals) {
            super(m, parent, context, equals);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.togglePreferencesSelected();
            super.actionPerformed(e);
        }

        public JMenuItem getMenuPresenter() {
            if (this.menuItem == null) {
                this.menuItem = new JCheckBoxMenuItem();
                this.menuItem.setSelected(this.isPreferencesSelected());
                Actions.connect((JMenuItem)this.menuItem, this, false);
            }
            return this.menuItem;
        }

        public JMenuItem getPopupPresenter() {
            if (this.popupItem == null) {
                this.popupItem = new JCheckBoxMenuItem();
                this.popupItem.setSelected(this.isPreferencesSelected());
                Actions.connect((JMenuItem)this.popupItem, this, true);
            }
            return this.popupItem;
        }

        public AbstractButton getToolbarPresenter() {
            if (this.toolbarItems == null) {
                this.toolbarItems = new WeakSet(4);
            }
            DefaultIconToggleButton b = new DefaultIconToggleButton();
            this.toolbarItems.add((Object)b);
            b.setSelected(this.isPreferencesSelected());
            Actions.connect((AbstractButton)b, this);
            return b;
        }

        @Override
        public void preferenceChange(PreferenceChangeEvent pce) {
            this.updateItemsSelected();
        }

        @Override
        protected ActionListener getDelegate() {
            return EMPTY;
        }

        @Override
        public Action createContextAwareInstance(Lookup actionContext) {
            return new CheckBox(this.map, this, actionContext, this.equals);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private boolean isPreferencesSelected() {
            boolean value;
            String key = (String)this.getValue("preferencesKey");
            Preferences prefs = this.prefs();
            if (key != null && prefs != null) {
                Object defaultValue = this.getValue("preferencesDefault");
                value = prefs.getBoolean(key, defaultValue instanceof Boolean ? (Boolean)defaultValue : false);
                CheckBox checkBox = this;
                synchronized (checkBox) {
                    if (!this.prefsListening) {
                        this.prefsListening = true;
                        prefs.addPreferenceChangeListener(this);
                    }
                }
            } else {
                value = false;
            }
            return value;
        }

        private void updateItemsSelected() {
            boolean selected = this.isPreferencesSelected();
            if (this.menuItem != null) {
                this.menuItem.setSelected(selected);
            }
            if (this.popupItem != null) {
                this.popupItem.setSelected(selected);
            }
            if (this.toolbarItems != null) {
                for (AbstractButton b : this.toolbarItems) {
                    b.setSelected(selected);
                }
            }
        }

        private synchronized Preferences prefs() {
            if (this.preferencesNode == null) {
                Object prefsNodeOrLookup = this.getValue("preferencesNode");
                if (prefsNodeOrLookup instanceof String) {
                    String nodeName = (String)prefsNodeOrLookup;
                    if (nodeName.startsWith("system:")) {
                        this.preferencesNode = Preferences.systemRoot();
                        if (this.preferencesNode != null) {
                            nodeName = nodeName.substring("system:".length());
                            try {
                                this.preferencesNode = this.preferencesNode.nodeExists(nodeName) ? this.preferencesNode.node(nodeName) : null;
                            }
                            catch (BackingStoreException ex) {
                                this.preferencesNode = null;
                            }
                        }
                    } else if (nodeName.startsWith("user:")) {
                        this.preferencesNode = Preferences.userRoot();
                        if (this.preferencesNode != null) {
                            nodeName = nodeName.substring("user:".length());
                            try {
                                this.preferencesNode = this.preferencesNode.nodeExists(nodeName) ? this.preferencesNode.node(nodeName) : null;
                            }
                            catch (BackingStoreException ex) {
                                this.preferencesNode = null;
                            }
                        }
                    } else {
                        this.preferencesNode = NbPreferences.root();
                        if (this.preferencesNode != null) {
                            try {
                                this.preferencesNode = this.preferencesNode.nodeExists(nodeName) ? this.preferencesNode.node(nodeName) : null;
                            }
                            catch (BackingStoreException ex) {
                                this.preferencesNode = null;
                            }
                        }
                    }
                } else if (prefsNodeOrLookup instanceof Preferences) {
                    this.preferencesNode = (Preferences)prefsNodeOrLookup;
                } else {
                    if (prefsNodeOrLookup instanceof Lookup) {
                        Lookup prefsLookup = (Lookup)prefsNodeOrLookup;
                        this.preferencesNodeResult = prefsLookup.lookupResult(Preferences.class);
                        Collection instances = this.preferencesNodeResult.allInstances();
                        if (instances.size() > 0) {
                            this.preferencesNode = (Preferences)instances.iterator().next();
                            this.preferencesNodeResult.addLookupListener((LookupListener)this);
                        }
                        return (Preferences)prefsLookup.lookup(Preferences.class);
                    }
                    this.preferencesNode = null;
                }
            }
            return this.preferencesNode;
        }

        public void resultChanged(LookupEvent ev) {
            this.preferencesNode = null;
            this.preferencesNodeResult = null;
            this.updateItemsSelected();
        }

        private void togglePreferencesSelected() {
            String key = (String)this.getValue("preferencesKey");
            Preferences prefs = this.prefs();
            if (key != null && prefs != null) {
                Object defaultValue = this.getValue("preferencesDefault");
                prefs.putBoolean(key, !prefs.getBoolean(key, defaultValue instanceof Boolean ? (Boolean)defaultValue : false));
            }
        }

    }

}

