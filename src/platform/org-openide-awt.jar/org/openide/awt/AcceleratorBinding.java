/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 */
package org.openide.awt;

import java.util.Collection;
import javax.swing.Action;
import javax.swing.KeyStroke;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;

public abstract class AcceleratorBinding {
    private static final Iter ALL = new Iter();

    protected AcceleratorBinding() {
        assert (this.getClass().getName().equals("org.netbeans.core.NbKeymap$AcceleratorBindingImpl"));
    }

    protected abstract KeyStroke keyStrokeForAction(Action var1, FileObject var2);

    public static void setAccelerator(Action action, FileObject definingFile) {
        for (AcceleratorBinding bnd : ALL.all()) {
            KeyStroke key = bnd.keyStrokeForAction(action, definingFile);
            if (key == null) continue;
            action.putValue("AcceleratorKey", key);
            break;
        }
    }

    private static final class Iter
    implements LookupListener {
        private final Lookup.Result<AcceleratorBinding> result = Lookup.getDefault().lookupResult(AcceleratorBinding.class);
        private Collection<? extends AcceleratorBinding> all;

        Iter() {
            this.resultChanged(null);
            this.result.addLookupListener((LookupListener)this);
        }

        final Collection<? extends AcceleratorBinding> all() {
            return this.all;
        }

        public void resultChanged(LookupEvent ev) {
            this.all = this.result.allInstances();
        }
    }

}

