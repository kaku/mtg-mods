/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.Lookup
 *  org.openide.util.Parameters
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.ActionInvoker
 */
package org.openide.awt;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.ActionMap;
import org.openide.awt.AlwaysEnabledAction;
import org.openide.awt.ContextAction;
import org.openide.awt.ContextSelection;
import org.openide.awt.GlobalManager;
import org.openide.util.ContextAwareAction;
import org.openide.util.Lookup;
import org.openide.util.Parameters;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.actions.ActionInvoker;

final class GeneralAction {
    static final Logger LOG = Logger.getLogger(GeneralAction.class.getName());

    private GeneralAction() {
    }

    public static ContextAwareAction callback(String key, Action defaultDelegate, Lookup context, boolean surviveFocusChange, boolean async) {
        if (key == null) {
            throw new NullPointerException();
        }
        return new DelegateAction(null, key, context, defaultDelegate, surviveFocusChange, async);
    }

    public static Action alwaysEnabled(Map map) {
        return new AlwaysEnabledAction(map);
    }

    public static ContextAwareAction callback(Map map) {
        Action fallback = (Action)map.get("fallback");
        DelegateAction d = new DelegateAction(map, fallback);
        Parameters.notNull((CharSequence)"key", (Object)d.key);
        return d;
    }

    public static <T> ContextAwareAction context(ContextAction.Performer<? super T> perf, ContextSelection selectionType, Lookup context, Class<T> dataType) {
        return new ContextAction<T>(perf, selectionType, context, dataType, false);
    }

    public static ContextAwareAction context(Map map) {
        Class dataType = GeneralAction.readClass(map.get("type"));
        return new DelegateAction(map, (Action)GeneralAction._context(map, dataType, Utilities.actionsGlobalContext()));
    }

    public static Action bindContext(Map map, Lookup context) {
        Class dataType = GeneralAction.readClass(map.get("type"));
        return new BaseDelAction(map, (Action)GeneralAction._context(map, dataType, context));
    }

    private static <T> ContextAwareAction _context(Map map, Class<T> dataType, Lookup context) {
        ContextSelection sel = GeneralAction.readSelection(map.get("selectionType"));
        ContextAction.Performer perf = new ContextAction.Performer(map);
        boolean survive = Boolean.TRUE.equals(map.get("surviveFocusChange"));
        return new ContextAction(perf, sel, context, dataType, survive);
    }

    private static ContextSelection readSelection(Object obj) {
        if (obj instanceof ContextSelection) {
            return (ContextSelection)((Object)obj);
        }
        if (obj instanceof String) {
            return ContextSelection.valueOf((String)obj);
        }
        throw new IllegalStateException("Cannot parse 'selectionType' value: " + obj);
    }

    private static Class<?> readClass(Object obj) {
        if (obj instanceof Class) {
            return (Class)obj;
        }
        if (obj instanceof String) {
            ClassLoader l = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
            if (l == null) {
                l = Thread.currentThread().getContextClassLoader();
            }
            if (l == null) {
                l = GeneralAction.class.getClassLoader();
            }
            try {
                return Class.forName((String)obj, false, l);
            }
            catch (Exception ex) {
                throw new IllegalStateException(ex);
            }
        }
        throw new IllegalStateException("Cannot read 'type' value: " + obj);
    }

    static final Object extractCommonAttribute(Map fo, Action action, String name) {
        return AlwaysEnabledAction.extractCommonAttribute(fo, name);
    }

    public Logger getLOG() {
        return LOG;
    }

    static class BaseDelAction
    implements Action,
    PropertyChangeListener {
        final Map map;
        final Action fallback;
        final Object key;
        final boolean async;
        final GlobalManager global;
        private PropertyChangeSupport support;
        final PropertyChangeListener weakL;
        Map<String, Object> attrs;

        protected BaseDelAction(Map map, Object key, Lookup actionContext, Action fallback, boolean surviveFocusChange, boolean async) {
            this.map = map;
            this.key = key;
            this.fallback = fallback;
            this.global = GlobalManager.findManager(actionContext, surviveFocusChange);
            this.weakL = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)fallback);
            this.async = async;
            if (fallback != null) {
                fallback.addPropertyChangeListener(this.weakL);
            }
        }

        protected BaseDelAction(Map map, Action fallback) {
            this(map, map.get("key"), Utilities.actionsGlobalContext(), fallback, Boolean.TRUE.equals(map.get("surviveFocusChange")), Boolean.TRUE.equals(map.get("asynchronous")));
        }

        public String toString() {
            return super.toString() + "[key=" + this.key + "]";
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            assert (EventQueue.isDispatchThread());
            Action a = this.findAction();
            if (a != null) {
                ActionInvoker.invokeAction((Action)a, (ActionEvent)e, (boolean)this.async, (Runnable)null);
            }
        }

        @Override
        public boolean isEnabled() {
            assert (EventQueue.isDispatchThread());
            Action a = this.findAction();
            return a == null ? false : a.isEnabled();
        }

        @Override
        public synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
            boolean first = false;
            if (this.support == null) {
                this.support = new PropertyChangeSupport(this);
                first = true;
            }
            this.support.addPropertyChangeListener(listener);
            if (first) {
                this.global.registerListener(this.key, this);
            }
        }

        @Override
        public synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
            if (this.support != null) {
                this.support.removePropertyChangeListener(listener);
                if (!this.support.hasListeners(null)) {
                    this.global.unregisterListener(this.key, this);
                    this.support = null;
                }
            }
        }

        @Override
        public void putValue(String key, Object value) {
            if (this.attrs == null) {
                this.attrs = new HashMap<String, Object>();
            }
            this.attrs.put(key, value);
        }

        @Override
        public Object getValue(String key) {
            if (this.attrs != null && this.attrs.containsKey(key)) {
                return this.attrs.get(key);
            }
            Object ret = GeneralAction.extractCommonAttribute(this.map, this, key);
            if (ret != null) {
                return ret;
            }
            Action a = this.findAction();
            return a == null ? null : a.getValue(key);
        }

        @Override
        public void setEnabled(boolean b) {
        }

        void updateState(ActionMap prev, ActionMap now, boolean fire) {
            Action prevAction;
            if (this.key == null) {
                return;
            }
            boolean prevEnabled = false;
            if (prev != null && (prevAction = prev.get(this.key)) != null) {
                prevEnabled = fire && prevAction.isEnabled();
                prevAction.removePropertyChangeListener(this.weakL);
            }
            if (now != null) {
                PropertyChangeSupport sup;
                boolean nowEnabled;
                Action nowAction = now.get(this.key);
                if (nowAction != null) {
                    nowAction.addPropertyChangeListener(this.weakL);
                    nowEnabled = nowAction.isEnabled();
                } else {
                    nowEnabled = this.fallback != null && this.fallback.isEnabled();
                }
                PropertyChangeSupport propertyChangeSupport = sup = fire ? this.support : null;
                if (sup != null && nowEnabled != prevEnabled) {
                    sup.firePropertyChange("enabled", prevEnabled, !prevEnabled);
                }
            }
        }

        private Action findAction() {
            Action a = this.global.findGlobalAction(this.key);
            return a == null ? this.fallback : a;
        }

        public Action createContextAwareInstance(Lookup actionContext) {
            Action f = this.fallback;
            if (f instanceof ContextAwareAction) {
                f = ((ContextAwareAction)f).createContextAwareInstance(actionContext);
            }
            DelegateAction other = new DelegateAction(this.map, this.key, actionContext, f, this.global.isSurvive(), this.async);
            if (this.attrs != null) {
                other.attrs = new HashMap<String, Object>(this.attrs);
            }
            return other;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("enabled".equals(evt.getPropertyName())) {
                PropertyChangeSupport sup;
                BaseDelAction baseDelAction = this;
                synchronized (baseDelAction) {
                    sup = this.support;
                }
                if (sup != null) {
                    sup.firePropertyChange("enabled", evt.getOldValue(), evt.getNewValue());
                }
            }
        }

        public int hashCode() {
            int k = this.key == null ? 37 : this.key.hashCode();
            int m = this.map == null ? 17 : this.map.hashCode();
            int f = this.fallback == null ? 7 : this.fallback.hashCode();
            return (k << 2) + (m << 1) + f;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj instanceof DelegateAction) {
                DelegateAction d = (DelegateAction)obj;
                if (this.key != null && !this.key.equals(d.key)) {
                    return false;
                }
                if (this.map != null && !this.map.equals(d.map)) {
                    return false;
                }
                if (this.fallback != null && !this.fallback.equals(d.fallback)) {
                    return false;
                }
                return true;
            }
            return false;
        }
    }

    static final class DelegateAction
    extends BaseDelAction
    implements ContextAwareAction {
        public DelegateAction(Map map, Object key, Lookup actionContext, Action fallback, boolean surviveFocusChange, boolean async) {
            super(map, key, actionContext, fallback, surviveFocusChange, async);
        }

        public DelegateAction(Map map, Action fallback) {
            super(map, fallback);
        }
    }

}

