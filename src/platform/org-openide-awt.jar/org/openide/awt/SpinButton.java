/*
 * Decompiled with CFR 0_118.
 */
package org.openide.awt;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Vector;
import org.openide.awt.SpinButtonListener;

@Deprecated
public class SpinButton
extends Canvas {
    static final long serialVersionUID = -3525959415481788776L;
    public static final boolean DEFAULT_ORIENTATION = false;
    public static final int DEFAULT_MINIMUM = 0;
    public static final int DEFAULT_MAXIMUM = 100;
    public static final int DEFAULT_STEP = 1;
    public static final int DEFAULT_REPEAT_DELAY = 300;
    public static final int DEFAULT_REPEAT_RATE = 70;
    private static final boolean SPIN_UP = true;
    private static final boolean SPIN_DOWN = false;
    protected boolean orientation = false;
    protected boolean arrowsOrientation = false;
    protected int minimum = 0;
    protected int maximum = 100;
    protected int step = 1;
    protected int value = 0;
    protected int repeatDelay = 300;
    protected int repeatRate = 70;
    protected RepeatThread rt = null;
    protected boolean running = false;
    protected boolean repeating = true;
    protected boolean runningDir = false;
    protected boolean boundsIgnored = false;
    private PropertyChangeSupport valueSupport;
    private Vector<SpinButtonListener> spinButtonListeners;

    public SpinButton() {
        this.valueSupport = new PropertyChangeSupport(this);
        this.spinButtonListeners = new Vector(3, 3);
        this.setBackground(SystemColor.control);
        this.setForeground(SystemColor.controlText);
        this.addMouseListener(new MouseAdapter(){

            @Override
            public void mousePressed(MouseEvent evt) {
                Dimension d = SpinButton.this.getSize();
                boolean newDir = true;
                newDir = SpinButton.this.orientation ? evt.getX() > (d.width - 1) / 2 : evt.getY() <= (d.height - 1) / 2;
                if ((newDir && SpinButton.this.value >= SpinButton.this.maximum || !newDir && SpinButton.this.value <= SpinButton.this.minimum) && !SpinButton.this.boundsIgnored) {
                    return;
                }
                SpinButton.this.switchRun(newDir);
                SpinButton.this.repaint();
            }

            @Override
            public void mouseReleased(MouseEvent evt) {
                boolean r = SpinButton.this.running;
                SpinButton.this.switchStop();
                if (r) {
                    SpinButton.this.repaint();
                }
            }
        });
    }

    @Override
    public void setForeground(Color color) {
        super.setForeground(color);
        this.repaint();
    }

    public void setOrientation(boolean aDir) {
        this.orientation = aDir;
        this.switchStop();
        this.repaint();
    }

    public void setArrowsOrientation(boolean aDir) {
        this.arrowsOrientation = aDir;
        this.switchStop();
        this.repaint();
    }

    public boolean getOrientation() {
        return this.orientation;
    }

    public boolean getArrowsOrientation() {
        return this.arrowsOrientation;
    }

    public void setMinimum(int aMin) {
        this.minimum = aMin;
        if (this.maximum < this.minimum) {
            this.maximum = this.minimum;
        }
        if (this.value < this.minimum) {
            this.setValue(this.value);
        }
        this.switchStop();
        this.repaint();
    }

    public int getMinimum() {
        return this.minimum;
    }

    public void setMaximum(int aMax) {
        this.maximum = aMax;
        if (this.maximum < this.minimum) {
            this.minimum = this.maximum;
        }
        if (this.value > this.maximum) {
            this.setValue(this.value);
        }
        this.switchStop();
        this.repaint();
    }

    public int getMaximum() {
        return this.maximum;
    }

    public void setValue(int aValue) {
        int oldValue = this.value;
        this.value = aValue;
        if (!this.boundsIgnored) {
            if (this.value < this.minimum) {
                this.value = this.minimum;
            }
            if (this.value > this.maximum) {
                this.value = this.maximum;
            }
        }
        if (this.value != oldValue) {
            this.valueSupport.firePropertyChange("value", new Integer(oldValue), new Integer(this.value));
        }
        if (this.getValue() == this.minimum || this.getValue() == this.maximum || oldValue == this.minimum || oldValue == this.maximum) {
            this.repaint();
        }
    }

    public int getValue() {
        return this.value;
    }

    public void setStep(int aStep) {
        this.step = aStep;
        this.switchStop();
        this.repaint();
    }

    public int getStep() {
        return this.step;
    }

    public void setDelay(int aDelay) {
        this.repeatDelay = aDelay;
        this.switchStop();
        this.repaint();
    }

    public int getDelay() {
        return this.repeatDelay;
    }

    public void setRate(int aRate) {
        this.repeatRate = aRate;
        this.switchStop();
        this.repaint();
    }

    public int getRate() {
        return this.repeatRate;
    }

    public boolean isBoundsIgnored() {
        return this.boundsIgnored;
    }

    public void setBoundsIgnored(boolean ignored) {
        this.boundsIgnored = ignored;
    }

    public boolean isRepeating() {
        return this.repeating;
    }

    public void setRepeating(boolean aRepeating) {
        this.repeating = aRepeating;
    }

    @Override
    public void paint(Graphics g) {
        Dimension d = this.getSize();
        int left = 0;
        int top = 0;
        int w = d.width - 1;
        int h = d.height - 1;
        g.setColor(this.getBackground());
        g.fillRect(left, top, w, h);
        if (this.orientation) {
            this.paintBorder(g, left, top, w /= 2, h, this.running && !this.runningDir, false);
            w = d.width - 1 - (left += w + 1);
            this.paintBorder(g, left, top, w, h, this.running && this.runningDir, true);
        } else {
            this.paintBorder(g, left, top, w, h /= 2, this.running && this.runningDir, true);
            h = d.height - 1 - (top += h + 1);
            this.paintBorder(g, left, top, w, h, this.running && !this.runningDir, false);
        }
    }

    private void paintBorder(Graphics g, int x, int y, int w, int h, boolean isDown, boolean aDir) {
        g.setColor(Color.black);
        if (!isDown) {
            g.drawLine(x, y + h, x + w, y + h);
            g.drawLine(x + w, y, x + w, y + h);
        } else {
            g.drawLine(x, y, x + w, y);
            g.drawLine(x, y, x, y + h);
            ++x;
            ++y;
        }
        g.setColor(SystemColor.controlHighlight);
        g.draw3DRect(x, y, --w, --h, !isDown);
        this.paintArrow(g, x, y, w, h, aDir);
    }

    private void paintArrow(Graphics g, int x, int y, int w, int h, boolean aDir) {
        if (w <= 0 || h <= 0) {
            return;
        }
        int wd = w / 4;
        int hd = h / 4;
        int[] xP = new int[3];
        int[] yP = new int[3];
        if (this.arrowsOrientation) {
            if (aDir) {
                xP[0] = x + wd;
                xP[2] = x + w - wd;
            } else {
                xP[0] = x + w - wd;
                xP[2] = x + wd;
            }
            xP[1] = xP[0];
            yP[0] = y + hd;
            yP[1] = y + h - hd;
            yP[2] = y + h / 2;
        } else {
            if (aDir) {
                yP[0] = y + h - hd;
                yP[2] = y + hd;
            } else {
                yP[0] = y + hd;
                yP[2] = y + h - hd;
            }
            yP[1] = yP[0];
            xP[0] = x + wd;
            xP[1] = x + w - wd;
            xP[2] = x + w / 2;
        }
        if ((aDir && this.value >= this.maximum || !aDir && this.value <= this.minimum) && !this.boundsIgnored) {
            Color fg = this.getForeground();
            Color bg = this.getBackground();
            g.setColor(new Color((fg.getRed() + 2 * bg.getRed()) / 3, (fg.getGreen() + 2 * bg.getGreen()) / 3, (fg.getBlue() + 2 * bg.getBlue()) / 3));
        } else {
            g.setColor(this.getForeground());
        }
        g.fillPolygon(xP, yP, 3);
    }

    protected synchronized void switchRun(boolean aDirect) {
        if (this.running) {
            this.rt.finish = true;
        }
        this.rt = new RepeatThread();
        this.rt.start();
        this.runningDir = aDirect;
        this.running = true;
    }

    public synchronized void switchStop() {
        if (this.rt == null) {
            return;
        }
        this.rt.finish = true;
        this.running = false;
    }

    @Override
    public Dimension getMinimumSize() {
        return this.countSize();
    }

    @Override
    public Dimension getPreferredSize() {
        return this.countSize();
    }

    private Dimension countSize() {
        int x;
        int y = x = 11;
        if (this.orientation) {
            x += x;
        } else {
            y += y;
        }
        return new Dimension(x, y);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.valueSupport.addPropertyChangeListener(l);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.valueSupport.removePropertyChangeListener(l);
    }

    public void addSpinButtonListener(SpinButtonListener spinButtonListener) {
        this.spinButtonListeners.addElement(spinButtonListener);
    }

    public void removeSpinButtonListener(SpinButtonListener spinButtonListener) {
        this.spinButtonListeners.removeElement(spinButtonListener);
    }

    public void notifySpinButtonListenersAboutUpMove() {
        int k = this.spinButtonListeners.size();
        for (int i = 0; i < k; ++i) {
            this.spinButtonListeners.elementAt(i).moveUp();
        }
    }

    public void notifySpinButtonListenersAboutDownMove() {
        int k = this.spinButtonListeners.size();
        for (int i = 0; i < k; ++i) {
            this.spinButtonListeners.elementAt(i).moveDown();
        }
    }

    protected void repeatThreadNotify() {
        int old_val = this.getValue();
        if (this.runningDir) {
            this.setValue(this.getValue() + this.step);
            if (this.value != old_val) {
                this.notifySpinButtonListenersAboutUpMove();
            }
        } else {
            this.setValue(this.getValue() - this.step);
            if (this.value != old_val) {
                this.notifySpinButtonListenersAboutDownMove();
            }
        }
        if (this.getValue() == old_val && !this.boundsIgnored) {
            this.switchStop();
            this.repaint();
        }
    }

    @Deprecated
    protected final class RepeatThread
    extends Thread {
        boolean finish;

        RepeatThread() {
            this.finish = false;
            this.finish = false;
        }

        @Override
        public void run() {
            SpinButton.this.repeatThreadNotify();
            try {
                RepeatThread.sleep(SpinButton.this.repeatDelay);
            }
            catch (InterruptedException e) {
                // empty catch block
            }
            if (!SpinButton.this.repeating) {
                return;
            }
            while (!this.finish) {
                SpinButton.this.repeatThreadNotify();
                if (this.finish) break;
                try {
                    RepeatThread.sleep(SpinButton.this.repeatRate);
                }
                catch (InterruptedException e) {}
            }
        }
    }

}

