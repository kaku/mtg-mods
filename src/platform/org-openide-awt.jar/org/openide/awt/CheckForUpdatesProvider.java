/*
 * Decompiled with CFR 0_118.
 */
package org.openide.awt;

public interface CheckForUpdatesProvider {
    public boolean openCheckForUpdatesWizard(boolean var1);

    public boolean notifyAvailableUpdates(boolean var1);

    public String getContentDescription();
}

