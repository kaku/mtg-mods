/*
 * Decompiled with CFR 0_118.
 */
package org.openide.awt;

import java.awt.event.MouseEvent;
import javax.swing.Icon;
import javax.swing.JButton;

@Deprecated
public class ToolbarButton
extends JButton {
    private static final long serialVersionUID = 6564434578524381134L;

    public ToolbarButton() {
    }

    public ToolbarButton(Icon icon) {
        super(icon);
    }

    @Override
    public void processMouseEvent(MouseEvent e) {
        super.processMouseEvent(e);
    }
}

