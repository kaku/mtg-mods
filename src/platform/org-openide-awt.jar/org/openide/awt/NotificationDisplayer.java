/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.openide.awt;

import java.awt.Image;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import org.openide.awt.Notification;
import org.openide.awt.NotificationCategoryFactory;
import org.openide.awt.StatusDisplayer;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public abstract class NotificationDisplayer {
    public static NotificationDisplayer getDefault() {
        NotificationDisplayer res = (NotificationDisplayer)Lookup.getDefault().lookup(NotificationDisplayer.class);
        if (null == res) {
            Logger.getLogger(NotificationDisplayer.class.getName()).log(Level.INFO, "No NotificationDisplayer implementation available.");
            res = new SimpleNotificationDisplayer();
        }
        return res;
    }

    public Notification notify(String title, Icon icon, String detailsText, ActionListener detailsAction) {
        return this.notify(title, icon, detailsText, detailsAction, Priority.NORMAL);
    }

    public abstract Notification notify(String var1, Icon var2, String var3, ActionListener var4, Priority var5);

    public Notification notify(String title, Icon icon, String detailsText, ActionListener detailsAction, Priority priority, Category category) {
        return this.notify(title, icon, detailsText, detailsAction, priority);
    }

    public Notification notify(String title, Icon icon, String detailsText, ActionListener detailsAction, Priority priority, String categoryName) {
        return this.notify(title, icon, detailsText, detailsAction, priority, NotificationCategoryFactory.getInstance().getCategory(categoryName));
    }

    public abstract Notification notify(String var1, Icon var2, JComponent var3, JComponent var4, Priority var5);

    public Notification notify(String title, Icon icon, JComponent balloonDetails, JComponent popupDetails, Priority priority, Category category) {
        return this.notify(title, icon, balloonDetails, popupDetails, priority);
    }

    public Notification notify(String title, Icon icon, JComponent balloonDetails, JComponent popupDetails, Priority priority, String categoryName) {
        return this.notify(title, icon, balloonDetails, popupDetails, priority, NotificationCategoryFactory.getInstance().getCategory(categoryName));
    }

    static Category createCategory(Map<String, String> attrs) {
        return NotificationCategoryFactory.create(attrs);
    }

    private static class NotificationImpl
    extends Notification {
        private final StatusDisplayer.Message msg;

        public NotificationImpl(StatusDisplayer.Message msg) {
            this.msg = msg;
        }

        @Override
        public void clear() {
            this.msg.clear(0);
        }
    }

    private static class SimpleNotificationDisplayer
    extends NotificationDisplayer {
        private SimpleNotificationDisplayer() {
        }

        @Override
        public Notification notify(String title, Icon icon, String detailsText, ActionListener detailsAction, Priority priority) {
            return this.notify(title + " - " + detailsText, priority);
        }

        @Override
        public Notification notify(String title, Icon icon, JComponent balloonDetails, JComponent popupDetails, Priority priority) {
            return this.notify(title, priority);
        }

        private Notification notify(String text, Priority priority) {
            int importance = 1;
            switch (priority) {
                case HIGH: {
                    importance = 100;
                    break;
                }
                case NORMAL: {
                    importance = 50;
                    break;
                }
                case LOW: 
                case SILENT: {
                    importance = 1;
                }
            }
            StatusDisplayer.Message msg = StatusDisplayer.getDefault().setStatusText(text, importance);
            return new NotificationImpl(msg);
        }
    }

    public static final class Category
    implements Comparable<Category> {
        public static final Category INFO = new Category("default_category_info", NbBundle.getMessage(NotificationDisplayer.class, (String)"INFO_CATEGORY"), NbBundle.getMessage(NotificationDisplayer.class, (String)"INFO_CATEGORY_DESCRIPTION"));
        public static final Category WARNING = new Category("default_category_warning", NbBundle.getMessage(NotificationDisplayer.class, (String)"WARNING_CATEGORY"), NbBundle.getMessage(NotificationDisplayer.class, (String)"WARNING_CATEGORY_DESCRIPTION"));
        public static final Category ERROR = new Category("default_category_error", NbBundle.getMessage(NotificationDisplayer.class, (String)"ERROR_CATEGORY"), NbBundle.getMessage(NotificationDisplayer.class, (String)"ERROR_CATEGORY_DESCRIPTION"));
        private final String name;
        private final String displayName;
        private final String description;
        private int index;

        Category(String name, String displayName, String description) {
            this.name = name;
            this.displayName = displayName;
            this.description = description;
        }

        public String getName() {
            return this.name;
        }

        public String getDisplayName() {
            return this.displayName;
        }

        void setIndex(int index) {
            this.index = index;
        }

        public String getDescription() {
            return this.description;
        }

        @Override
        public int compareTo(Category other) {
            return this.index - other.index;
        }

        public static List<Category> getCategories() {
            return NotificationCategoryFactory.getInstance().getCategories();
        }

        static List<Category> getDefaultCategories() {
            ArrayList<Category> defaultCategories = new ArrayList<Category>();
            defaultCategories.add(ERROR);
            defaultCategories.add(WARNING);
            defaultCategories.add(INFO);
            return defaultCategories;
        }
    }

    public static enum Priority {
        HIGH(new ImageIcon(ImageUtilities.loadImage((String)"org/openide/awt/resources/priority_high.png"))),
        NORMAL(new ImageIcon(ImageUtilities.loadImage((String)"org/openide/awt/resources/priority_normal.png"))),
        LOW(new ImageIcon(ImageUtilities.loadImage((String)"org/openide/awt/resources/priority_low.png"))),
        SILENT(new ImageIcon(ImageUtilities.loadImage((String)"org/openide/awt/resources/priority_silent.png")));
        
        private final Icon icon;

        private Priority(Icon icon) {
            this.icon = icon;
        }

        public Icon getIcon() {
            return this.icon;
        }
    }

}

