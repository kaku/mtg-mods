/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.HelpCtx
 *  org.openide.util.HelpCtx$Provider
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.BooleanStateAction
 *  org.openide.util.actions.SystemAction
 */
package org.openide.awt;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.actions.Closable;
import org.netbeans.api.actions.Editable;
import org.netbeans.api.actions.Openable;
import org.netbeans.api.actions.Printable;
import org.netbeans.api.actions.Viewable;
import org.openide.awt.AcceleratorBinding;
import org.openide.awt.ActionDefaultPerfomer;
import org.openide.awt.AlwaysEnabledAction;
import org.openide.awt.ContextAction;
import org.openide.awt.ContextSelection;
import org.openide.awt.DynamicMenuContent;
import org.openide.awt.GeneralAction;
import org.openide.awt.InjectorAny;
import org.openide.awt.InjectorExactlyOne;
import org.openide.awt.JMenuPlus;
import org.openide.awt.Mnemonics;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.ContextAwareAction;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.util.actions.BooleanStateAction;
import org.openide.util.actions.SystemAction;

public class Actions {
    private static final String IDENTIFIER = "(?:\\p{javaJavaIdentifierStart}\\p{javaJavaIdentifierPart}*)";
    private static final Pattern FQN = Pattern.compile("(?:\\p{javaJavaIdentifierStart}\\p{javaJavaIdentifierPart}*)(?:[.](?:\\p{javaJavaIdentifierStart}\\p{javaJavaIdentifierPart}*))*");
    private static final ButtonActionConnectorGetter GET = new ButtonActionConnectorGetter();

    private static Icon nonNullIcon(Icon i) {
        return null;
    }

    public static String findKey(SystemAction action) {
        return Actions.findKey((Action)action);
    }

    private static String findKey(Action action) {
        if (action == null) {
            return null;
        }
        KeyStroke accelerator = (KeyStroke)action.getValue("AcceleratorKey");
        if (accelerator == null) {
            return null;
        }
        int modifiers = accelerator.getModifiers();
        String acceleratorText = "";
        if (modifiers > 0) {
            acceleratorText = KeyEvent.getKeyModifiersText(modifiers);
            acceleratorText = acceleratorText + "+";
        } else if (accelerator.getKeyCode() == 0) {
            return "";
        }
        acceleratorText = acceleratorText + KeyEvent.getKeyText(accelerator.getKeyCode());
        return acceleratorText;
    }

    @Deprecated
    public static void connect(JMenuItem item, SystemAction action, boolean popup) {
        Actions.connect(item, (Action)action, popup);
    }

    public static void connect(JMenuItem item, Action action, boolean popup) {
        for (ButtonActionConnector bac : Actions.buttonActionConnectors()) {
            if (!bac.connect(item, action, popup)) continue;
            return;
        }
        MenuBridge b = new MenuBridge(item, action, popup);
        b.prepare();
        if (item instanceof MenuItem) {
            ((MenuItem)item).setBridge(b);
        }
        item.putClientProperty("hideWhenDisabled", action.getValue("hideWhenDisabled"));
    }

    public static void connect(JCheckBoxMenuItem item, BooleanStateAction action, boolean popup) {
        CheckMenuBridge b = new CheckMenuBridge(item, action, popup);
        b.prepare();
    }

    @Deprecated
    public static void connect(AbstractButton button, SystemAction action) {
        Actions.connect(button, (Action)action);
    }

    public static void connect(AbstractButton button, Action action) {
        for (ButtonActionConnector bac : Actions.buttonActionConnectors()) {
            if (!bac.connect(button, action)) continue;
            return;
        }
        ButtonBridge b = new ButtonBridge(button, action);
        b.prepare();
    }

    public static void connect(AbstractButton button, BooleanStateAction action) {
        BooleanButtonBridge b = new BooleanButtonBridge(button, action);
        b.prepare();
    }

    @Deprecated
    public static void setMenuText(AbstractButton item, String text, boolean useMnemonic) {
        String msg = NbBundle.getMessage(Actions.class, (String)"USE_MNEMONICS");
        if ("always".equals(msg)) {
            useMnemonic = true;
        } else if ("never".equals(msg)) {
            useMnemonic = false;
        } else assert ("default".equals(msg));
        if (useMnemonic) {
            Mnemonics.setLocalizedText(item, text);
        } else {
            item.setText(Actions.cutAmpersand(text));
        }
    }

    public static String cutAmpersand(String text) {
        if (null == text) {
            return null;
        }
        String result = text;
        int i = text.indexOf("(&");
        result = i >= 0 && i + 3 < text.length() && text.charAt(i + 3) == ')' ? text.substring(0, i) + text.substring(i + 4) : ((i = text.indexOf(38)) < 0 ? text : (i == text.length() - 1 ? text.substring(0, i) : (" ".equals(text.substring(i + 1, i + 2)) ? text : text.substring(0, i) + text.substring(i + 1))));
        return result;
    }

    public static Action alwaysEnabled(ActionListener delegate, String displayName, String iconBase, boolean noIconInMenu) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("delegate", delegate);
        map.put("displayName", displayName);
        map.put("iconBase", iconBase);
        map.put("noIconInMenu", noIconInMenu);
        return Actions.alwaysEnabled(map);
    }

    static Action alwaysEnabled(Map map) {
        return AlwaysEnabledAction.create(map);
    }

    public static Action checkbox(String preferencesNode, String preferencesKey, String displayName, String iconBase, boolean noIconInMenu) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("preferencesNode", preferencesNode);
        map.put("preferencesKey", preferencesKey);
        map.put("displayName", displayName);
        map.put("iconBase", iconBase);
        map.put("noIconInMenu", noIconInMenu);
        return Actions.checkbox(map);
    }

    static Action checkbox(Map map) {
        return AlwaysEnabledAction.create(map);
    }

    public static ContextAwareAction callback(String key, Action fallback, boolean surviveFocusChange, String displayName, String iconBase, boolean noIconInMenu) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("key", key);
        map.put("surviveFocusChange", surviveFocusChange);
        map.put("fallback", fallback);
        map.put("displayName", displayName);
        map.put("iconBase", iconBase);
        map.put("noIconInMenu", noIconInMenu);
        return Actions.callback(map);
    }

    static ContextAwareAction callback(Map fo) {
        return GeneralAction.callback(fo);
    }

    public static ContextAwareAction context(Class<?> type, boolean single, boolean surviveFocusChange, ContextAwareAction delegate, String key, String displayName, String iconBase, boolean noIconInMenu) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("key", key);
        map.put("surviveFocusChange", surviveFocusChange);
        map.put("delegate", (Object)delegate);
        map.put("type", type);
        map.put("selectionType", (Object)(single ? ContextSelection.EXACTLY_ONE : ContextSelection.ANY));
        map.put("displayName", displayName);
        map.put("iconBase", iconBase);
        map.put("noIconInMenu", noIconInMenu);
        return GeneralAction.context(map);
    }

    static Action context(Map fo) {
        Object context = fo.get("context");
        if (context instanceof Lookup) {
            Lookup lkp = (Lookup)context;
            return GeneralAction.bindContext(fo, lkp);
        }
        return GeneralAction.context(fo);
    }

    static ContextAction.Performer<?> inject(Map fo) {
        Object t = fo.get("selectionType");
        if (ContextSelection.EXACTLY_ONE.toString().equals(t)) {
            return new InjectorExactlyOne(fo);
        }
        if (ContextSelection.ANY.toString().equals(t)) {
            return new InjectorAny(fo);
        }
        throw new IllegalStateException("no selectionType parameter in " + fo);
    }

    static ContextAction.Performer<?> performer(Map fo) {
        String type = (String)fo.get("type");
        if (type.equals(Openable.class.getName())) {
            return new ActionDefaultPerfomer(0);
        }
        if (type.equals(Viewable.class.getName())) {
            return new ActionDefaultPerfomer(1);
        }
        if (type.equals(Editable.class.getName())) {
            return new ActionDefaultPerfomer(2);
        }
        if (type.equals(Closable.class.getName())) {
            return new ActionDefaultPerfomer(3);
        }
        if (type.equals(Printable.class.getName())) {
            return new ActionDefaultPerfomer(4);
        }
        throw new IllegalStateException(type);
    }

    public static Action forID(String category, String id) throws IllegalArgumentException {
        if (category.startsWith("Actions/")) {
            throw new IllegalArgumentException("category should not start with Actions/: " + category);
        }
        if (!FQN.matcher(id).matches()) {
            throw new IllegalArgumentException("id must be valid fully qualified name: " + id);
        }
        String path = "Actions/" + category + "/" + id.replace('.', '-') + ".instance";
        Action a = (Action)FileUtil.getConfigObject((String)path, Action.class);
        if (a == null) {
            return null;
        }
        FileObject def = FileUtil.getConfigFile((String)path);
        if (def != null) {
            AcceleratorBinding.setAccelerator(a, def);
        }
        return a;
    }

    private static HelpCtx findHelp(Action a) {
        if (a instanceof HelpCtx.Provider) {
            return ((HelpCtx.Provider)a).getHelpCtx();
        }
        return HelpCtx.DEFAULT_HELP;
    }

    static void prepareMenuBridgeItemsInContainer(Container c) {
        Component[] comps = c.getComponents();
        for (int i = 0; i < comps.length; ++i) {
            MenuBridge bridge;
            JComponent cop;
            if (!(comps[i] instanceof JComponent) || (bridge = (MenuBridge)(cop = (JComponent)comps[i]).getClientProperty("menubridgeresizehack")) == null) continue;
            bridge.updateState(null);
        }
    }

    static void prepareMargins(JMenuItem item, Action action) {
        item.setHorizontalTextPosition(4);
        item.setHorizontalAlignment(2);
    }

    static void updateKey(JMenuItem item, Action action) {
        if (!(item instanceof JMenu)) {
            item.setAccelerator((KeyStroke)action.getValue("AcceleratorKey"));
        }
    }

    private static Collection<? extends ButtonActionConnector> buttonActionConnectors() {
        return GET.all();
    }

    private static final class ButtonActionConnectorGetter
    implements LookupListener {
        private final Lookup.Result<ButtonActionConnector> result = Lookup.getDefault().lookupResult(ButtonActionConnector.class);
        private Collection<? extends ButtonActionConnector> all;

        ButtonActionConnectorGetter() {
            this.result.addLookupListener((LookupListener)this);
            this.resultChanged(null);
        }

        final Collection<? extends ButtonActionConnector> all() {
            return this.all;
        }

        public void resultChanged(LookupEvent ev) {
            this.all = this.result.allInstances();
            this.all.iterator().hasNext();
        }
    }

    public static interface ButtonActionConnector {
        public boolean connect(AbstractButton var1, Action var2);

        public boolean connect(JMenuItem var1, Action var2, boolean var3);
    }

    @Deprecated
    public static class SubMenu
    extends JMenuPlus
    implements DynamicMenuContent {
        private static final long serialVersionUID = -4446966671302959091L;
        private SubMenuBridge bridge;

        public SubMenu(SystemAction aAction, SubMenuModel model) {
            this(aAction, model, true);
        }

        public SubMenu(SystemAction aAction, SubMenuModel model, boolean popup) {
            this((Action)aAction, model, popup);
        }

        public SubMenu(Action aAction, SubMenuModel model, boolean popup) {
            this.bridge = new SubMenuBridge(new JMenuItem(), this, aAction, model, popup);
            this.bridge.prepare();
        }

        @Override
        public JComponent[] getMenuPresenters() {
            return this.bridge.getMenuPresenters();
        }

        @Override
        public JComponent[] synchMenuPresenters(JComponent[] items) {
            return this.bridge.synchMenuPresenters(items);
        }
    }

    @Deprecated
    public static class ToolbarToggleButton
    extends org.openide.awt.ToolbarToggleButton {
        private static final long serialVersionUID = -4783163952526348942L;

        public ToolbarToggleButton(BooleanStateAction aAction) {
            super(null, false);
            Actions.connect((AbstractButton)this, aAction);
        }

        @Override
        public Dimension getMaximumSize() {
            return this.getPreferredSize();
        }

        @Override
        public Dimension getMinimumSize() {
            return this.getPreferredSize();
        }
    }

    @Deprecated
    public static class ToolbarButton
    extends org.openide.awt.ToolbarButton {
        private static final long serialVersionUID = 6564434578524381134L;

        public ToolbarButton(SystemAction aAction) {
            super(null);
            Actions.connect((AbstractButton)this, aAction);
        }

        public ToolbarButton(Action aAction) {
            super(null);
            Actions.connect((AbstractButton)this, aAction);
        }

        @Override
        public Dimension getMaximumSize() {
            return this.getPreferredSize();
        }

        @Override
        public Dimension getMinimumSize() {
            return this.getPreferredSize();
        }
    }

    public static class CheckboxMenuItem
    extends JCheckBoxMenuItem {
        private static final long serialVersionUID = 6190621106981774043L;

        public CheckboxMenuItem(BooleanStateAction aAction, boolean useMnemonic) {
            Actions.connect(this, aAction, !useMnemonic);
        }
    }

    public static class MenuItem
    extends JMenuItem
    implements DynamicMenuContent {
        static final long serialVersionUID = -21757335363267194L;
        private Bridge bridge;

        public MenuItem(SystemAction aAction, boolean useMnemonic) {
            Actions.connect((JMenuItem)this, aAction, !useMnemonic);
        }

        public MenuItem(Action aAction, boolean useMnemonic) {
            Actions.connect((JMenuItem)this, aAction, !useMnemonic);
        }

        void setBridge(Bridge br) {
            this.bridge = br;
        }

        @Override
        public JComponent[] synchMenuPresenters(JComponent[] items) {
            if (this.bridge != null) {
                this.bridge.updateState(null);
            }
            return this.getMenuPresenters();
        }

        @Override
        public JComponent[] getMenuPresenters() {
            return new JComponent[]{this};
        }
    }

    @Deprecated
    private static final class SubMenuBridge
    extends MenuBridge
    implements ChangeListener,
    DynamicMenuContent {
        private SubMenuModel model;
        private List<JMenuItem> currentOnes;
        private JMenuItem single;
        private JMenu multi;

        public SubMenuBridge(JMenuItem one, JMenu more, Action action, SubMenuModel model, boolean popup) {
            super(one, action, popup);
            this.single = one;
            this.multi = more;
            Actions.setMenuText(this.multi, (String)action.getValue("Name"), popup);
            Actions.prepareMargins(one, action);
            Actions.prepareMargins(more, action);
            this.currentOnes = new ArrayList<JMenuItem>();
            this.model = model;
        }

        @Override
        public void stateChanged(ChangeEvent ev) {
            if (!EventQueue.isDispatchThread()) {
                new IllegalStateException("This must happen in the event thread!").printStackTrace();
            }
        }

        @Override
        public void updateState(String changedProperty) {
            super.updateState(changedProperty);
        }

        @Override
        public JComponent[] getMenuPresenters() {
            return this.synchMenuPresenters(null);
        }

        @Override
        public JComponent[] synchMenuPresenters(JComponent[] items) {
            this.currentOnes.clear();
            int cnt = this.model.getCount();
            if (cnt == 0) {
                this.updateState(null);
                this.currentOnes.add(this.single);
                this.single.setEnabled(false);
            } else if (cnt == 1) {
                this.updateState(null);
                this.currentOnes.add(this.single);
                this.single.setEnabled(this.action.isEnabled());
                HelpCtx help = this.model.getHelpCtx(0);
                this.associateHelp(this.single, help == null ? Actions.findHelp(this.action) : help);
            } else {
                this.currentOnes.add(this.multi);
                this.multi.removeAll();
                Mnemonics.setLocalizedText(this.multi, (String)this.action.getValue("Name"));
                boolean addSeparator = false;
                int count = this.model.getCount();
                for (int i = 0; i < count; ++i) {
                    String label = this.model.getLabel(i);
                    if (label == null) {
                        addSeparator = this.multi.getItemCount() > 0;
                    } else {
                        if (addSeparator) {
                            this.multi.addSeparator();
                            addSeparator = false;
                        }
                        JMenuItem item = new JMenuItem();
                        Mnemonics.setLocalizedText(item, label);
                        if (i == 0) {
                            Actions.updateKey(item, this.action);
                        }
                        item.addActionListener(new ISubActionListener(i, this.model));
                        HelpCtx help = this.model.getHelpCtx(i);
                        this.associateHelp(item, help == null ? Actions.findHelp(this.action) : help);
                        this.multi.add(item);
                    }
                    this.associateHelp(this.multi, Actions.findHelp(this.action));
                }
                this.multi.setEnabled(true);
            }
            return this.currentOnes.toArray(new JMenuItem[this.currentOnes.size()]);
        }

        private void associateHelp(JComponent comp, HelpCtx help) {
            if (help != null && !help.equals((Object)HelpCtx.DEFAULT_HELP) && help.getHelpID() != null) {
                HelpCtx.setHelpIDString((JComponent)comp, (String)help.getHelpID());
            } else {
                HelpCtx.setHelpIDString((JComponent)comp, (String)null);
            }
        }
    }

    private static class ISubActionListener
    implements ActionListener {
        int index;
        SubMenuModel support;

        public ISubActionListener(int index, SubMenuModel support) {
            this.index = index;
            this.support = support;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.support.performActionAt(this.index);
        }
    }

    private static final class CheckMenuBridge
    extends BooleanButtonBridge {
        private boolean popup;
        private boolean hasOwnIcon = false;

        public CheckMenuBridge(JCheckBoxMenuItem item, BooleanStateAction action, boolean popup) {
            super(item, action);
            this.popup = popup;
            if (popup) {
                Actions.prepareMargins(item, (Action)action);
            }
            Object base = action.getValue("iconBase");
            Object i = null;
            i = action instanceof SystemAction ? action.getValue("icon") : action.getValue("SmallIcon");
            this.hasOwnIcon = base != null || i != null;
        }

        @Override
        public void updateState(String changedProperty) {
            Object s;
            super.updateState(changedProperty);
            if (changedProperty == null || !changedProperty.equals("AcceleratorKey")) {
                Actions.updateKey((JMenuItem)this.comp, this.action);
            }
            if ((changedProperty == null || changedProperty.equals("Name")) && (s = this.action.getValue("Name")) instanceof String) {
                Actions.setMenuText((JMenuItem)this.comp, (String)s, true);
            }
        }

        @Override
        protected void updateButtonIcon() {
            if (this.hasOwnIcon) {
                super.updateButtonIcon();
                return;
            }
            if (!this.popup) {
                this.button.setIcon(ImageUtilities.loadImageIcon((String)"org/openide/resources/actions/empty.gif", (boolean)true));
            }
        }

        @Override
        protected boolean useTextIcons() {
            return false;
        }
    }

    private static class MenuBridge
    extends ButtonBridge {
        private boolean popup;

        public MenuBridge(JMenuItem item, Action action, boolean popup) {
            super(item, action);
            this.popup = popup;
            if (popup) {
                Actions.prepareMargins(item, action);
            } else {
                item.putClientProperty("menubridgeresizehack", this);
            }
        }

        @Override
        protected void prepare() {
            if (this.popup) {
                this.addNotify();
            } else {
                super.prepare();
            }
        }

        @Override
        public void updateState(String changedProperty) {
            if (this.button == null) {
                this.button = (AbstractButton)this.comp;
            }
            if (changedProperty == null || changedProperty.equals("enabled")) {
                this.button.setEnabled(this.action.isEnabled());
            }
            if (changedProperty == null || !changedProperty.equals("AcceleratorKey")) {
                Actions.updateKey((JMenuItem)this.comp, this.action);
            }
            if (!this.popup && (changedProperty == null || changedProperty.equals("icon") || changedProperty.equals("SmallIcon") || changedProperty.equals("iconBase"))) {
                this.updateButtonIcon();
            }
            if (changedProperty == null || changedProperty.equals("Name")) {
                Object s = null;
                boolean useMnemonic = true;
                if (this.popup) {
                    s = this.action.getValue("popupText");
                }
                if (s == null) {
                    s = this.action.getValue("menuText");
                    boolean bl = useMnemonic = !this.popup;
                }
                if (s == null) {
                    s = this.action.getValue("Name");
                    boolean bl = useMnemonic = !this.popup;
                }
                if (s instanceof String) {
                    Actions.setMenuText((JMenuItem)this.comp, (String)s, useMnemonic);
                }
            }
        }

        @Override
        protected void updateButtonIcon() {
            Object i = null;
            Object obj = this.action.getValue("noIconInMenu");
            Object base = this.action.getValue("iconBase");
            if (Boolean.TRUE.equals(obj)) {
                return;
            }
            if (this.action instanceof SystemAction) {
                SystemAction sa = (SystemAction)this.action;
                i = sa.getIcon(this.useTextIcons());
                if (i != null) {
                    this.button.setIcon((Icon)i);
                    this.button.setDisabledIcon(ImageUtilities.createDisabledIcon((Icon)((Icon)i)));
                }
            } else if (base == null && (i = this.action.getValue("SmallIcon")) instanceof Icon) {
                this.button.setIcon((Icon)i);
                this.button.setDisabledIcon(ImageUtilities.createDisabledIcon((Icon)((Icon)i)));
            }
            if (base instanceof String) {
                ImageIcon dImgIcon;
                ImageIcon rImgIcon;
                ImageIcon pImgIcon;
                String b = (String)base;
                ImageIcon imgIcon = null;
                if (i == null && (imgIcon = ImageUtilities.loadImageIcon((String)b, (boolean)true)) != null) {
                    this.button.setIcon(imgIcon);
                    this.button.setDisabledIcon(ImageUtilities.createDisabledIcon((Icon)imgIcon));
                }
                if ((pImgIcon = ImageUtilities.loadImageIcon((String)MenuBridge.insertBeforeSuffix(b, "_pressed"), (boolean)true)) != null) {
                    this.button.setPressedIcon(pImgIcon);
                }
                if ((rImgIcon = ImageUtilities.loadImageIcon((String)MenuBridge.insertBeforeSuffix(b, "_rollover"), (boolean)true)) != null) {
                    this.button.setRolloverIcon(rImgIcon);
                }
                if ((dImgIcon = ImageUtilities.loadImageIcon((String)MenuBridge.insertBeforeSuffix(b, "_disabled"), (boolean)true)) != null) {
                    this.button.setDisabledIcon(dImgIcon);
                } else if (imgIcon != null) {
                    this.button.setDisabledIcon(ImageUtilities.createDisabledIcon((Icon)imgIcon));
                }
            }
        }

        @Override
        protected boolean useTextIcons() {
            return false;
        }
    }

    private static class BooleanButtonBridge
    extends ButtonBridge {
        public BooleanButtonBridge(AbstractButton button, BooleanStateAction action) {
            super(button, (Action)action);
        }

        @Override
        public void updateState(String changedProperty) {
            super.updateState(changedProperty);
            if (changedProperty == null || changedProperty.equals("booleanState")) {
                this.button.setSelected(((BooleanStateAction)this.action).getBooleanState());
            }
        }
    }

    private static class ButtonBridge
    extends Bridge
    implements ActionListener {
        private static Logger UILOG = Logger.getLogger("org.netbeans.ui.actions");
        protected AbstractButton button;

        public ButtonBridge(AbstractButton button, Action action) {
            super(button, action);
            button.addActionListener(action);
            this.button = button;
            button.addActionListener(this);
        }

        @Override
        public void actionPerformed(ActionEvent ev) {
            LogRecord rec = new LogRecord(Level.FINER, "UI_ACTION_BUTTON_PRESS");
            rec.setParameters(new Object[]{this.button, this.button.getClass().getName(), this.action, this.action.getClass().getName(), this.action.getValue("Name")});
            rec.setResourceBundle(NbBundle.getBundle(Actions.class));
            rec.setResourceBundleName(Actions.class.getPackage().getName() + ".Bundle");
            rec.setLoggerName(UILOG.getName());
            UILOG.log(rec);
        }

        protected void updateButtonIcon() {
            ImageIcon imgIcon;
            String b;
            Object i = null;
            Object base = this.action.getValue("iconBase");
            boolean useSmallIcon = true;
            Object prop = this.button.getClientProperty("PreferredIconSize");
            if (prop instanceof Integer && (Integer)prop == 24) {
                useSmallIcon = false;
            }
            if (this.action instanceof SystemAction) {
                if (base instanceof String) {
                    b = (String)base;
                    imgIcon = ButtonBridge.loadImage(b, useSmallIcon, null);
                    if (imgIcon != null) {
                        i = imgIcon;
                        this.button.setIcon(imgIcon);
                        this.button.setDisabledIcon(ImageUtilities.createDisabledIcon((Icon)imgIcon));
                    } else {
                        SystemAction sa = (SystemAction)this.action;
                        i = sa.getIcon(this.useTextIcons());
                        this.button.setIcon((Icon)i);
                        this.button.setDisabledIcon(ImageUtilities.createDisabledIcon((Icon)((Icon)i)));
                    }
                } else {
                    SystemAction sa = (SystemAction)this.action;
                    i = sa.getIcon(this.useTextIcons());
                    this.button.setIcon((Icon)i);
                    this.button.setDisabledIcon(ImageUtilities.createDisabledIcon((Icon)((Icon)i)));
                }
            } else if (base instanceof String) {
                b = (String)base;
                imgIcon = ButtonBridge.loadImage(b, useSmallIcon, null);
                if (imgIcon != null) {
                    i = imgIcon;
                    this.button.setIcon((Icon)i);
                    this.button.setDisabledIcon(ImageUtilities.createDisabledIcon((Icon)imgIcon));
                } else {
                    i = this.action.getValue("SmallIcon");
                    if (i instanceof Icon) {
                        this.button.setIcon((Icon)i);
                        this.button.setDisabledIcon(ImageUtilities.createDisabledIcon((Icon)((Icon)i)));
                    } else {
                        this.button.setIcon(Actions.nonNullIcon(null));
                    }
                }
            } else {
                i = this.action.getValue("SmallIcon");
                if (i instanceof Icon) {
                    this.button.setIcon((Icon)i);
                    this.button.setDisabledIcon(ImageUtilities.createDisabledIcon((Icon)((Icon)i)));
                } else {
                    this.button.setIcon(Actions.nonNullIcon(null));
                }
            }
            if (base instanceof String) {
                ImageIcon dImgIcon;
                ImageIcon pImgIcon;
                ImageIcon rImgIcon;
                b = (String)base;
                imgIcon = null;
                if (i == null) {
                    imgIcon = ButtonBridge.loadImage(b, useSmallIcon, null);
                    if (imgIcon != null) {
                        this.button.setIcon(imgIcon);
                    }
                    i = imgIcon;
                }
                if ((pImgIcon = ButtonBridge.loadImage(b, useSmallIcon, "_pressed")) != null) {
                    this.button.setPressedIcon(pImgIcon);
                }
                if ((rImgIcon = ButtonBridge.loadImage(b, useSmallIcon, "_rollover")) != null) {
                    this.button.setRolloverIcon(rImgIcon);
                }
                if ((dImgIcon = ButtonBridge.loadImage(b, useSmallIcon, "_disabled")) != null) {
                    this.button.setDisabledIcon(dImgIcon);
                } else if (imgIcon != null) {
                    this.button.setDisabledIcon(ImageUtilities.createDisabledIcon((Icon)imgIcon));
                }
                ImageIcon sImgIcon = ButtonBridge.loadImage(b, useSmallIcon, "_selected");
                if (sImgIcon != null) {
                    this.button.setSelectedIcon(sImgIcon);
                }
                if ((sImgIcon = ButtonBridge.loadImage(b, useSmallIcon, "_rolloverSelected")) != null) {
                    this.button.setRolloverSelectedIcon(sImgIcon);
                }
                if ((sImgIcon = ButtonBridge.loadImage(b, useSmallIcon, "_disabledSelected")) != null) {
                    this.button.setDisabledSelectedIcon(sImgIcon);
                }
            }
        }

        static ImageIcon loadImage(String iconBase, boolean useSmallIcon, String suffix) {
            String bigBase;
            ImageIcon icon;
            if (!useSmallIcon && (icon = ImageUtilities.loadImageIcon((String)ButtonBridge.insertBeforeSuffix(bigBase = ButtonBridge.insertBeforeSuffix(iconBase, "24"), suffix), (boolean)true)) != null) {
                return icon;
            }
            return ImageUtilities.loadImageIcon((String)ButtonBridge.insertBeforeSuffix(iconBase, suffix), (boolean)true);
        }

        static String insertBeforeSuffix(String path, String toInsert) {
            if (toInsert == null) {
                return path;
            }
            String withoutSuffix = path;
            String suffix = "";
            if (path.lastIndexOf(46) >= 0) {
                withoutSuffix = path.substring(0, path.lastIndexOf(46));
                suffix = path.substring(path.lastIndexOf(46), path.length());
            }
            return withoutSuffix + toInsert + suffix;
        }

        @Override
        public void updateState(String changedProperty) {
            if (changedProperty == null || changedProperty.equals("enabled")) {
                this.button.setEnabled(this.action.isEnabled());
            }
            if (changedProperty == null || changedProperty.equals("icon") || changedProperty.equals("SmallIcon") || changedProperty.equals("iconBase")) {
                this.updateButtonIcon();
            }
            if (changedProperty == null || changedProperty.equals("AcceleratorKey") || changedProperty.equals("Name") && this.action.getValue("ShortDescription") == null || changedProperty.equals("ShortDescription")) {
                String tip = Actions.findKey(this.action);
                String toolTip = (String)this.action.getValue("ShortDescription");
                if (toolTip == null) {
                    toolTip = (String)this.action.getValue("Name");
                    String string = toolTip = toolTip == null ? "" : Actions.cutAmpersand(toolTip);
                }
                if (tip == null || tip.equals("")) {
                    this.button.setToolTipText(toolTip);
                } else {
                    this.button.setToolTipText(NbBundle.getMessage(Actions.class, (String)"FMT_ButtonHint", (Object)toolTip, (Object)tip));
                }
            }
            if (this.button instanceof Accessible && (changedProperty == null || changedProperty.equals("Name"))) {
                this.button.getAccessibleContext().setAccessibleName((String)this.action.getValue("Name"));
            }
        }

        protected boolean useTextIcons() {
            return true;
        }
    }

    private static abstract class Bridge
    implements PropertyChangeListener {
        protected JComponent comp;
        protected Action action;
        private final PropertyChangeListener actionL;

        public Bridge(JComponent comp, Action action) {
            if (comp == null || action == null) {
                throw new IllegalArgumentException("None of the arguments can be null: comp=" + comp + ", action=" + action);
            }
            this.comp = comp;
            this.action = action;
            this.actionL = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)action);
            HelpCtx help = Actions.findHelp(action);
            if (help != null && !help.equals((Object)HelpCtx.DEFAULT_HELP) && help.getHelpID() != null) {
                HelpCtx.setHelpIDString((JComponent)comp, (String)help.getHelpID());
            }
        }

        protected void prepare() {
            this.comp.addPropertyChangeListener(new VisL());
            if (this.comp.isShowing()) {
                this.addNotify();
            } else {
                this.updateState(null);
            }
        }

        final void addNotify() {
            this.action.addPropertyChangeListener(this.actionL);
            this.updateState(null);
        }

        final void removeNotify() {
            this.action.removePropertyChangeListener(this.actionL);
        }

        public abstract void updateState(String var1);

        @Override
        public void propertyChange(PropertyChangeEvent ev) {
            if (!EventQueue.isDispatchThread()) {
                new IllegalStateException("This must happen in the event thread!").printStackTrace();
            }
            this.updateState(ev.getPropertyName());
        }

        private class VisL
        implements PropertyChangeListener {
            VisL() {
            }

            @Override
            public void propertyChange(PropertyChangeEvent ev) {
                if ("ancestor".equals(ev.getPropertyName())) {
                    if (ev.getNewValue() != null) {
                        Bridge.this.addNotify();
                    } else {
                        Bridge.this.removeNotify();
                    }
                }
            }
        }

    }

    @Deprecated
    public static interface SubMenuModel {
        public int getCount();

        public String getLabel(int var1);

        public HelpCtx getHelpCtx(int var1);

        public void performActionAt(int var1);

        public void addChangeListener(ChangeListener var1);

        public void removeChangeListener(ChangeListener var1);
    }

}

