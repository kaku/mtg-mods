/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ChangeSupport
 */
package org.openide.awt;

import java.util.Enumeration;
import java.util.Vector;
import javax.swing.UIManager;
import javax.swing.event.ChangeListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;
import javax.swing.undo.UndoableEdit;
import org.openide.util.ChangeSupport;

public interface UndoRedo {
    public static final UndoRedo NONE = new Empty();

    public boolean canUndo();

    public boolean canRedo();

    public void undo() throws CannotUndoException;

    public void redo() throws CannotRedoException;

    public void addChangeListener(ChangeListener var1);

    public void removeChangeListener(ChangeListener var1);

    public String getUndoPresentationName();

    public String getRedoPresentationName();

    @Deprecated
    public static final class Empty
    implements UndoRedo {
        @Override
        public boolean canUndo() {
            return false;
        }

        @Override
        public boolean canRedo() {
            return false;
        }

        @Override
        public void undo() throws CannotUndoException {
            throw new CannotUndoException();
        }

        @Override
        public void redo() throws CannotRedoException {
            throw new CannotRedoException();
        }

        @Override
        public void addChangeListener(ChangeListener l) {
        }

        @Override
        public void removeChangeListener(ChangeListener l) {
        }

        @Override
        public String getUndoPresentationName() {
            return "";
        }

        @Override
        public String getRedoPresentationName() {
            return "";
        }
    }

    public static class Manager
    extends UndoManager
    implements UndoRedo {
        static final long serialVersionUID = 6721367974521509720L;
        private int indexOfNextAdd;
        private int limit;
        private boolean inProgress;
        private boolean hasBeenDone;
        private boolean alive;
        private final ChangeSupport cs;

        public Manager() {
            this.cs = new ChangeSupport((Object)this);
            this.hasBeenDone = true;
            this.alive = true;
            this.inProgress = true;
            this.indexOfNextAdd = 0;
            this.limit = 100;
            this.edits.ensureCapacity(this.limit);
        }

        @Override
        public void die() {
            int size = this.edits.size();
            for (int i = size - 1; i >= 0; --i) {
                UndoableEdit e = (UndoableEdit)this.edits.elementAt(i);
                e.die();
            }
            this.alive = false;
        }

        @Override
        public boolean isInProgress() {
            return this.inProgress;
        }

        @Override
        public void end() {
            this.inProgress = false;
            this.trimEdits(this.indexOfNextAdd, this.edits.size() - 1);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void undo() throws CannotUndoException {
            if (this.inProgress) {
                UndoableEdit edit = this.editToBeUndone();
                if (edit == null) {
                    throw new CannotUndoException();
                }
                this.undoTo(edit);
            } else {
                int i;
                if (!this.canUndo()) {
                    throw new CannotUndoException();
                }
                try {
                    for (i = this.edits.size() - 1; i >= 0; --i) {
                        ((UndoableEdit)this.edits.get(i)).undo();
                    }
                    this.hasBeenDone = false;
                }
                finally {
                    if (i != -1) {
                        int size = this.edits.size();
                        while (++i < size) {
                            ((UndoableEdit)this.edits.get(i)).redo();
                        }
                    }
                }
            }
            this.cs.fireChange();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        protected void undoTo(UndoableEdit edit) throws CannotUndoException {
            int i = this.indexOfNextAdd;
            boolean done = false;
            try {
                while (!done) {
                    UndoableEdit next = (UndoableEdit)this.edits.get(--i);
                    next.undo();
                    done = next == edit;
                }
                this.indexOfNextAdd = i;
            }
            finally {
                if (!done) {
                    ++i;
                    while (i < this.indexOfNextAdd) {
                        ((UndoableEdit)this.edits.get(i)).redo();
                        ++i;
                    }
                }
            }
        }

        @Override
        public boolean canUndo() {
            if (this.inProgress) {
                UndoableEdit edit = this.editToBeUndone();
                return edit != null && edit.canUndo();
            }
            return !this.isInProgress() && this.alive && this.hasBeenDone;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void redo() throws CannotRedoException {
            if (this.inProgress) {
                UndoableEdit edit = this.editToBeRedone();
                if (edit == null) {
                    throw new CannotRedoException();
                }
                this.redoTo(edit);
            } else {
                int i;
                if (!this.canRedo()) {
                    throw new CannotRedoException();
                }
                int size = this.edits.size();
                try {
                    for (i = 0; i < size; ++i) {
                        ((UndoableEdit)this.edits.get(i)).redo();
                    }
                    this.hasBeenDone = true;
                }
                finally {
                    if (i != size) {
                        while (--i >= 0) {
                            ((UndoableEdit)this.edits.get(i)).undo();
                        }
                    }
                }
            }
            this.cs.fireChange();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        protected void redoTo(UndoableEdit edit) throws CannotRedoException {
            int i = this.indexOfNextAdd;
            boolean done = false;
            try {
                while (!done) {
                    UndoableEdit next = (UndoableEdit)this.edits.elementAt(i++);
                    next.redo();
                    done = next == edit;
                }
                this.indexOfNextAdd = i;
            }
            finally {
                if (!done) {
                    i -= 2;
                    while (i >= this.indexOfNextAdd) {
                        ((UndoableEdit)this.edits.get(i)).undo();
                        --i;
                    }
                }
            }
        }

        @Override
        public boolean canRedo() {
            if (this.inProgress) {
                UndoableEdit edit = this.editToBeRedone();
                return edit != null && edit.canRedo();
            }
            return !this.isInProgress() && this.alive && !this.hasBeenDone;
        }

        @Override
        public void undoOrRedo() throws CannotRedoException, CannotUndoException {
            if (this.indexOfNextAdd == this.edits.size()) {
                this.undo();
            } else {
                this.redo();
            }
        }

        @Override
        public boolean canUndoOrRedo() {
            if (this.indexOfNextAdd == this.edits.size()) {
                return this.canUndo();
            }
            return this.canRedo();
        }

        @Override
        public int getLimit() {
            return this.limit;
        }

        @Override
        public void setLimit(int l) {
            if (!this.inProgress) {
                throw new RuntimeException("Attempt to call UndoManager.setLimit() after UndoManager.end() has been called");
            }
            this.limit = l;
            this.trimForLimit();
        }

        @Override
        protected void trimForLimit() {
            int size;
            if (this.limit >= 0 && (size = this.edits.size()) > this.limit) {
                int halfLimit = this.limit / 2;
                int keepTo = this.indexOfNextAdd - 1 + halfLimit;
                int keepFrom = this.indexOfNextAdd - 1 - halfLimit;
                if (keepTo - keepFrom + 1 > this.limit) {
                    ++keepFrom;
                }
                if (keepFrom < 0) {
                    keepTo -= keepFrom;
                    keepFrom = 0;
                }
                if (keepTo >= size) {
                    int delta = size - keepTo - 1;
                    keepTo += delta;
                    keepFrom += delta;
                }
                this.trimEdits(keepTo + 1, size - 1);
                this.trimEdits(0, keepFrom - 1);
            }
        }

        @Override
        protected void trimEdits(int from, int to) {
            if (from <= to) {
                for (int i = to; from <= i; --i) {
                    UndoableEdit e = (UndoableEdit)this.edits.elementAt(i);
                    e.die();
                    this.edits.removeElementAt(i);
                }
                if (this.indexOfNextAdd > to) {
                    this.indexOfNextAdd -= to - from + 1;
                } else if (this.indexOfNextAdd >= from) {
                    this.indexOfNextAdd = from;
                }
            }
        }

        @Override
        public void discardAllEdits() {
            Enumeration cursor = this.edits.elements();
            while (cursor.hasMoreElements()) {
                UndoableEdit e = (UndoableEdit)cursor.nextElement();
                e.die();
            }
            this.edits = new Vector();
            this.indexOfNextAdd = 0;
            this.cs.fireChange();
        }

        @Override
        protected UndoableEdit lastEdit() {
            int count = this.edits.size();
            if (count > 0) {
                return (UndoableEdit)this.edits.elementAt(count - 1);
            }
            return null;
        }

        @Override
        protected UndoableEdit editToBeUndone() {
            int i = this.indexOfNextAdd;
            while (i > 0) {
                UndoableEdit edit;
                if (!(edit = (UndoableEdit)this.edits.elementAt(--i)).isSignificant()) continue;
                return edit;
            }
            return null;
        }

        @Override
        protected UndoableEdit editToBeRedone() {
            int count = this.edits.size();
            int i = this.indexOfNextAdd;
            while (i < count) {
                UndoableEdit edit;
                if (!(edit = (UndoableEdit)this.edits.elementAt(i++)).isSignificant()) continue;
                return edit;
            }
            return null;
        }

        @Override
        public void undoableEditHappened(UndoableEditEvent ue) {
            this.addEdit(ue.getEdit());
            this.cs.fireChange();
        }

        @Override
        public boolean addEdit(UndoableEdit anEdit) {
            boolean retVal;
            this.trimEdits(this.indexOfNextAdd, this.edits.size() - 1);
            if (!this.inProgress) {
                retVal = false;
            } else {
                UndoableEdit last = this.lastEdit();
                if (last == null) {
                    this.edits.addElement(anEdit);
                } else if (!last.addEdit(anEdit)) {
                    if (anEdit.replaceEdit(last)) {
                        this.edits.removeElementAt(this.edits.size() - 1);
                    }
                    this.edits.addElement(anEdit);
                }
                retVal = true;
            }
            this.indexOfNextAdd = this.edits.size();
            this.trimForLimit();
            return retVal;
        }

        @Override
        public boolean replaceEdit(UndoableEdit anEdit) {
            return false;
        }

        @Override
        public boolean isSignificant() {
            Enumeration cursor = this.edits.elements();
            while (cursor.hasMoreElements()) {
                if (!((UndoableEdit)cursor.nextElement()).isSignificant()) continue;
                return true;
            }
            return false;
        }

        @Override
        public String getPresentationName() {
            UndoableEdit last = this.lastEdit();
            if (last != null) {
                return last.getPresentationName();
            }
            return "";
        }

        @Override
        public String getUndoPresentationName() {
            if (this.canUndo()) {
                if (this.inProgress) {
                    if (this.canUndo()) {
                        return this.editToBeUndone().getUndoPresentationName();
                    }
                    return UIManager.getString("AbstractUndoableEdit.undoText");
                }
                UndoableEdit last = this.lastEdit();
                if (last != null) {
                    return last.getUndoPresentationName();
                }
                String name = this.getPresentationName();
                name = !"".equals(name) ? UIManager.getString("AbstractUndoableEdit.undoText") + " " + name : UIManager.getString("AbstractUndoableEdit.undoText");
                return name;
            }
            return "";
        }

        @Override
        public String getRedoPresentationName() {
            if (this.canRedo()) {
                UndoableEdit last = this.lastEdit();
                if (last != null) {
                    if (this.inProgress) {
                        if (this.canRedo()) {
                            return this.editToBeRedone().getRedoPresentationName();
                        }
                        return UIManager.getString("AbstractUndoableEdit.redoText");
                    }
                    return super.getRedoPresentationName();
                }
                String name = this.getPresentationName();
                name = !"".equals(name) ? UIManager.getString("AbstractUndoableEdit.redoText") + " " + name : UIManager.getString("AbstractUndoableEdit.redoText");
                return name;
            }
            return "";
        }

        @Override
        public String getUndoOrRedoPresentationName() {
            if (this.indexOfNextAdd == this.edits.size()) {
                return this.getUndoPresentationName();
            }
            return this.getRedoPresentationName();
        }

        @Override
        public String toString() {
            return super.toString() + " hasBeenDone: " + this.hasBeenDone + " alive: " + this.alive + " inProgress: " + this.inProgress + " edits: " + this.edits + " limit: " + this.limit + " indexOfNextAdd: " + this.indexOfNextAdd;
        }

        @Override
        public void addChangeListener(ChangeListener l) {
            this.cs.addChangeListener(l);
        }

        @Override
        public void removeChangeListener(ChangeListener l) {
            this.cs.removeChangeListener(l);
        }
    }

    public static interface Provider {
        public UndoRedo getUndoRedo();
    }

}

