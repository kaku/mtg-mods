/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.openide.awt;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import org.openide.awt.SwingBrowserImpl;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

public class HtmlBrowser
extends JPanel {
    private static final long serialVersionUID = 2912844785502987960L;
    public static final int DEFAULT_WIDTH = 400;
    public static final int DEFAULT_HEIGHT = 600;
    private static Factory browserFactory;
    private static String homePage;
    final Impl browserImpl;
    private boolean ignoreChangeInLocationField = false;
    private boolean toolbarVisible = false;
    private boolean statusLineVisible = false;
    private BrowserListener browserListener;
    private JButton bBack;
    private JButton bForward;
    private JButton bReload;
    private JButton bStop;
    private JTextField txtLocation;
    private JLabel lStatusLine;
    final Component browserComponent;
    private JPanel head;
    private RequestProcessor rp = new RequestProcessor();
    private final Component extraToolbar;

    public HtmlBrowser() {
        this(true, true);
    }

    public HtmlBrowser(boolean toolbar, boolean statusLine) {
        this(null, toolbar, statusLine);
    }

    public HtmlBrowser(Factory fact, boolean toolbar, boolean statusLine) {
        this(fact, toolbar, statusLine, null);
    }

    public HtmlBrowser(Factory fact, boolean toolbar, boolean statusLine, Component extraToolbar) {
        Impl impl = null;
        Component comp = null;
        try {
            if (fact == null) {
                Impl[] arr = new Impl[1];
                comp = HtmlBrowser.findComponent(arr);
                impl = arr[0];
            } else {
                try {
                    impl = fact.createHtmlBrowserImpl();
                    comp = impl.getComponent();
                }
                catch (UnsupportedOperationException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                    impl = new SwingBrowserImpl();
                    comp = impl.getComponent();
                }
            }
        }
        catch (RuntimeException e) {
            Exceptions.attachLocalizedMessage((Throwable)e, (String)NbBundle.getMessage(HtmlBrowser.class, (String)"EXC_Module"));
            Exceptions.printStackTrace((Throwable)e);
        }
        this.browserImpl = impl;
        this.browserComponent = comp;
        this.extraToolbar = extraToolbar;
        this.setLayout(new BorderLayout(0, 2));
        this.add(this.browserComponent != null ? this.browserComponent : new JScrollPane(), "Center");
        this.browserListener = new BrowserListener();
        if (toolbar) {
            this.initToolbar();
        }
        if (statusLine) {
            this.initStatusLine();
        }
        this.browserImpl.addPropertyChangeListener(this.browserListener);
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(HtmlBrowser.class, (String)"ACS_HtmlBrowser"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(HtmlBrowser.class, (String)"ACSD_HtmlBrowser"));
    }

    public static void setHomePage(String u) {
        homePage = u;
    }

    public static String getHomePage() {
        if (homePage == null) {
            return NbBundle.getMessage(HtmlBrowser.class, (String)"PROP_HomePage");
        }
        return homePage;
    }

    @Deprecated
    public static void setFactory(Factory brFactory) {
        browserFactory = brFactory;
    }

    private static Component findComponent(Impl[] handle) {
        Lookup.Result r = Lookup.getDefault().lookup(new Lookup.Template(Factory.class));
        for (Factory f : r.allInstances()) {
            try {
                Impl impl = f.createHtmlBrowserImpl();
                Component c = impl != null ? impl.getComponent() : null;
                if (c == null) continue;
                handle[0] = impl;
                return c;
            }
            catch (UnsupportedOperationException ex) {
                continue;
            }
        }
        Factory f2 = browserFactory;
        if (f2 != null) {
            try {
                handle[0] = f2.createHtmlBrowserImpl();
                return handle[0].getComponent();
            }
            catch (UnsupportedOperationException ex) {
                // empty catch block
            }
        }
        handle[0] = new SwingBrowserImpl();
        return handle[0].getComponent();
    }

    private void initToolbar() {
        this.toolbarVisible = true;
        this.head = new JPanel(new GridBagLayout());
        this.bBack = new JButton();
        this.bBack.setBorder(BorderFactory.createEmptyBorder());
        this.bBack.setBorderPainted(false);
        this.bBack.setContentAreaFilled(false);
        this.bBack.setIcon(ImageUtilities.loadImageIcon((String)"org/openide/resources/html/back_normal.png", (boolean)true));
        this.bBack.setRolloverIcon(ImageUtilities.loadImageIcon((String)"org/openide/resources/html/back_hover.png", (boolean)true));
        this.bBack.setDisabledIcon(ImageUtilities.loadImageIcon((String)"org/openide/resources/html/back_disabled.png", (boolean)true));
        this.bBack.setSelectedIcon(this.bBack.getIcon());
        this.bBack.setToolTipText(NbBundle.getMessage(HtmlBrowser.class, (String)"CTL_Back"));
        this.bForward = new JButton();
        this.bForward.setBorder(BorderFactory.createEmptyBorder());
        this.bForward.setBorderPainted(false);
        this.bForward.setContentAreaFilled(false);
        this.bForward.setIcon(ImageUtilities.loadImageIcon((String)"org/openide/resources/html/forward_normal.png", (boolean)true));
        this.bForward.setRolloverIcon(ImageUtilities.loadImageIcon((String)"org/openide/resources/html/forward_hover.png", (boolean)true));
        this.bForward.setDisabledIcon(ImageUtilities.loadImageIcon((String)"org/openide/resources/html/forward_disabled.png", (boolean)true));
        this.bForward.setSelectedIcon(this.bForward.getIcon());
        this.bForward.setToolTipText(NbBundle.getMessage(HtmlBrowser.class, (String)"CTL_Forward"));
        this.bReload = new JButton();
        this.bReload.setBorder(BorderFactory.createEmptyBorder());
        this.bReload.setBorderPainted(false);
        this.bReload.setContentAreaFilled(false);
        this.bReload.setIcon(ImageUtilities.loadImageIcon((String)"org/openide/resources/html/refresh.png", (boolean)true));
        this.bReload.setRolloverIcon(ImageUtilities.loadImageIcon((String)"org/openide/resources/html/refresh_hover.png", (boolean)true));
        this.bReload.setDisabledIcon(ImageUtilities.loadImageIcon((String)"org/openide/resources/html/refresh.png", (boolean)true));
        this.bReload.setSelectedIcon(this.bReload.getIcon());
        this.bReload.setToolTipText(NbBundle.getMessage(HtmlBrowser.class, (String)"CTL_Reload"));
        this.bReload.setFocusPainted(false);
        this.bStop = new JButton();
        this.bStop.setBorderPainted(false);
        this.bStop.setBorder(BorderFactory.createEmptyBorder());
        this.bStop.setContentAreaFilled(false);
        this.bStop.setIcon(ImageUtilities.loadImageIcon((String)"org/openide/resources/html/stop.png", (boolean)true));
        this.bStop.setRolloverIcon(ImageUtilities.loadImageIcon((String)"org/openide/resources/html/stop_hover.png", (boolean)true));
        this.bStop.setDisabledIcon(ImageUtilities.loadImageIcon((String)"org/openide/resources/html/stop.png", (boolean)true));
        this.bStop.setSelectedIcon(this.bStop.getIcon());
        this.bStop.setToolTipText(NbBundle.getMessage(HtmlBrowser.class, (String)"CTL_Stop"));
        this.bStop.setFocusPainted(false);
        this.txtLocation = new JTextField();
        this.txtLocation.setEditable(true);
        this.txtLocation.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 1) {
                    if (null != HtmlBrowser.this.txtLocation.getSelectedText() || HtmlBrowser.this.txtLocation.isFocusOwner()) {
                        return;
                    }
                    HtmlBrowser.this.txtLocation.selectAll();
                }
            }
        });
        this.head.add((Component)this.bBack, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, 17, 0, new Insets(0, 0, 0, 1), 0, 0));
        this.head.add((Component)this.bForward, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, 17, 0, new Insets(0, 0, 0, 4), 0, 0));
        this.head.add((Component)this.txtLocation, new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, 10, 2, new Insets(0, 0, 0, 4), 0, 0));
        this.head.add((Component)this.bReload, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, 13, 0, new Insets(0, 0, 0, 4), 0, 0));
        this.head.add((Component)this.bStop, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, 13, 0, new Insets(0, 0, 0, 0), 0, 0));
        if (null != this.extraToolbar) {
            this.head.add(this.extraToolbar, new GridBagConstraints(0, 1, 5, 1, 1.0, 0.0, 17, 2, new Insets(3, 0, 0, 0), 0, 0));
        }
        this.head.setBorder(BorderFactory.createEmptyBorder(8, 10, null == this.extraToolbar ? 8 : 3, 10));
        if (this.browserImpl != null) {
            this.bBack.setEnabled(this.browserImpl.isBackward());
            this.bForward.setEnabled(this.browserImpl.isForward());
        }
        this.txtLocation.addActionListener(this.browserListener);
        this.bBack.addActionListener(this.browserListener);
        this.bForward.addActionListener(this.browserListener);
        this.bReload.addActionListener(this.browserListener);
        this.bStop.addActionListener(this.browserListener);
        this.bBack.getAccessibleContext().setAccessibleName(this.bBack.getToolTipText());
        this.bForward.getAccessibleContext().setAccessibleName(this.bForward.getToolTipText());
        this.bReload.getAccessibleContext().setAccessibleName(this.bReload.getToolTipText());
        this.bStop.getAccessibleContext().setAccessibleName(this.bStop.getToolTipText());
        this.txtLocation.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(HtmlBrowser.class, (String)"ACSD_HtmlBrowser_Location"));
        this.add((Component)this.head, "North");
    }

    private void destroyToolbar() {
        this.remove(this.head);
        this.head = null;
        this.toolbarVisible = false;
    }

    private void initStatusLine() {
        this.statusLineVisible = true;
        this.lStatusLine = new JLabel(NbBundle.getMessage(HtmlBrowser.class, (String)"CTL_Loading"));
        this.add((Component)this.lStatusLine, "South");
        this.lStatusLine.setLabelFor(this);
    }

    private void destroyStatusLine() {
        this.remove(this.lStatusLine);
        this.lStatusLine = null;
        this.statusLineVisible = false;
    }

    public void setURL(String str) {
        if (null != str) {
            try {
                str = new URL(str).toExternalForm();
            }
            catch (ThreadDeath td) {
                throw td;
            }
            catch (Throwable e) {
                // empty catch block
            }
        }
        this.browserImpl.setLocation(str);
    }

    public void setURL(URL url) {
        class URLSetter
        implements Runnable {
            private boolean doReload;
            final /* synthetic */ URL val$url;

            URLSetter() {
                this.val$url = var2_2;
                this.doReload = false;
            }

            @Override
            public void run() {
                if (!SwingUtilities.isEventDispatchThread()) {
                    boolean sameHosts = false;
                    sameHosts = "nbfs".equals(this.val$url.getProtocol()) ? true : this.val$url.getHost() != null && this$0.browserImpl.getURL() != null && this.val$url.getHost().equals(this$0.browserImpl.getURL().getHost());
                    this.doReload = sameHosts && this.val$url.equals(this$0.browserImpl.getURL());
                    SwingUtilities.invokeLater(this);
                } else if (this.doReload) {
                    this$0.browserImpl.reloadDocument();
                } else {
                    this$0.browserImpl.setURL(this.val$url);
                }
            }
        }
        if (url == null) {
            this.txtLocation.setText(null);
            return;
        }
        this.rp.post((Runnable)new URLSetter(this, url));
    }

    public final URL getDocumentURL() {
        return this.browserImpl.getURL();
    }

    public final void setEnableHome(boolean b) {
    }

    public final void setEnableLocation(boolean b) {
        this.txtLocation.setEnabled(b);
        this.txtLocation.setVisible(b);
    }

    public boolean isStatusLineVisible() {
        return this.statusLineVisible;
    }

    public void setStatusLineVisible(boolean v) {
        if (v == this.statusLineVisible) {
            return;
        }
        if (v) {
            this.initStatusLine();
        } else {
            this.destroyStatusLine();
        }
    }

    public boolean isToolbarVisible() {
        return this.toolbarVisible;
    }

    public void setToolbarVisible(boolean v) {
        if (v == this.toolbarVisible) {
            return;
        }
        if (v) {
            this.initToolbar();
        } else {
            this.destroyToolbar();
        }
    }

    public final Impl getBrowserImpl() {
        return this.browserImpl;
    }

    public final Component getBrowserComponent() {
        return this.browserComponent;
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension superPref = super.getPreferredSize();
        return new Dimension(Math.max(400, superPref.width), Math.max(600, superPref.height));
    }

    private void updateLocationBar() {
        if (this.toolbarVisible) {
            this.ignoreChangeInLocationField = true;
            String url = this.browserImpl.getLocation();
            this.txtLocation.setText(url);
            this.ignoreChangeInLocationField = false;
        }
    }

    @Override
    public void requestFocus() {
        if (this.browserComponent != null) {
            boolean ownerFound = false;
            if (this.browserComponent instanceof JComponent) {
                ownerFound = ((JComponent)this.browserComponent).requestDefaultFocus();
            }
            if (!ownerFound) {
                this.browserComponent.requestFocus();
            }
        } else {
            super.requestFocus();
        }
    }

    @Override
    public boolean requestFocusInWindow() {
        if (this.browserComponent != null) {
            boolean ownerFound = false;
            if (this.browserComponent instanceof JComponent) {
                ownerFound = ((JComponent)this.browserComponent).requestDefaultFocus();
            }
            if (!ownerFound) {
                return this.browserComponent.requestFocusInWindow();
            }
            return true;
        }
        return super.requestFocusInWindow();
    }

    @Override
    public AccessibleContext getAccessibleContext() {
        if (this.accessibleContext == null) {
            this.accessibleContext = new AccessibleHtmlBrowser();
        }
        return this.accessibleContext;
    }

    static {
        homePage = null;
    }

    private class AccessibleHtmlBrowser
    extends JPanel.AccessibleJPanel {
        AccessibleHtmlBrowser() {
            super(HtmlBrowser.this);
        }

        @Override
        public void setAccessibleName(String name) {
            super.setAccessibleName(name);
            if (HtmlBrowser.this.browserComponent instanceof Accessible) {
                HtmlBrowser.this.browserComponent.getAccessibleContext().setAccessibleName(name);
            }
        }

        @Override
        public void setAccessibleDescription(String desc) {
            super.setAccessibleDescription(desc);
            if (HtmlBrowser.this.browserComponent instanceof Accessible) {
                HtmlBrowser.this.browserComponent.getAccessibleContext().setAccessibleDescription(desc);
            }
        }
    }

    private static final class TrivialURLDisplayer
    extends URLDisplayer {
        @Override
        public void showURL(URL u) {
            Desktop d;
            if (Desktop.isDesktopSupported() && (d = Desktop.getDesktop()).isSupported(Desktop.Action.BROWSE)) {
                try {
                    d.browse(u.toURI());
                    return;
                }
                catch (Exception x) {
                    Logger.getLogger(HtmlBrowser.class.getName()).log(Level.INFO, "Showing: " + u, x);
                }
            }
            HtmlBrowser browser = new HtmlBrowser();
            browser.setURL(u);
            JFrame frame = new JFrame();
            frame.setDefaultCloseOperation(2);
            frame.getContentPane().add(browser);
            frame.pack();
            frame.setVisible(true);
        }
    }

    public static abstract class URLDisplayer {
        protected URLDisplayer() {
        }

        public static URLDisplayer getDefault() {
            URLDisplayer dflt = (URLDisplayer)Lookup.getDefault().lookup(URLDisplayer.class);
            if (dflt == null) {
                dflt = new TrivialURLDisplayer();
            }
            return dflt;
        }

        public abstract void showURL(URL var1);

        public void showURLExternal(URL u) {
            this.showURL(u);
        }
    }

    public static abstract class Impl {
        static final long serialVersionUID = 2912844785502962114L;
        public static final String PROP_STATUS_MESSAGE = "statusMessage";
        public static final String PROP_URL = "url";
        public static final String PROP_TITLE = "title";
        public static final String PROP_FORWARD = "forward";
        public static final String PROP_BACKWARD = "backward";
        public static final String PROP_HISTORY = "history";
        public static final String PROP_BROWSER_WAS_CLOSED = "browser.was.closed";
        public static final String PROP_LOADING = "loading";

        public abstract Component getComponent();

        public abstract void reloadDocument();

        public abstract void stopLoading();

        public abstract void setURL(URL var1);

        public abstract URL getURL();

        public String getLocation() {
            URL url = this.getURL();
            return null == url ? null : url.toString();
        }

        public void setLocation(String location) {
            URL url;
            try {
                url = new URL(location);
            }
            catch (MalformedURLException ee) {
                try {
                    url = new URL("http://" + location);
                }
                catch (MalformedURLException e) {
                    String errorMessage = NbBundle.getMessage(SwingBrowserImpl.class, (String)"FMT_InvalidURL", (Object[])new Object[]{location});
                    if (this instanceof SwingBrowserImpl) {
                        ((SwingBrowserImpl)this).setStatusText(errorMessage);
                    } else {
                        Logger.getLogger(HtmlBrowser.class.getName()).log(Level.INFO, errorMessage, ee);
                    }
                    return;
                }
            }
            this.setURL(url);
        }

        public abstract String getStatusMessage();

        public abstract String getTitle();

        public abstract boolean isForward();

        public abstract void forward();

        public abstract boolean isBackward();

        public abstract void backward();

        public abstract boolean isHistory();

        public abstract void showHistory();

        public abstract void addPropertyChangeListener(PropertyChangeListener var1);

        public abstract void removePropertyChangeListener(PropertyChangeListener var1);

        public void dispose() {
        }

        public Lookup getLookup() {
            return Lookup.EMPTY;
        }
    }

    private class BrowserListener
    implements ActionListener,
    PropertyChangeListener {
        BrowserListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            String property = evt.getPropertyName();
            if (property == null) {
                return;
            }
            if (property.equals("url") || property.equals("title")) {
                HtmlBrowser.this.firePropertyChange(evt.getPropertyName(), evt.getOldValue(), evt.getNewValue());
            }
            if (property.equals("url")) {
                HtmlBrowser.this.updateLocationBar();
            } else if (property.equals("statusMessage")) {
                String s = HtmlBrowser.this.browserImpl.getStatusMessage();
                if (s == null || s.length() < 1) {
                    s = NbBundle.getMessage(HtmlBrowser.class, (String)"CTL_Document_done");
                }
                if (HtmlBrowser.this.lStatusLine != null) {
                    HtmlBrowser.this.lStatusLine.setText(s);
                }
            } else if (property.equals("forward") && HtmlBrowser.this.bForward != null) {
                HtmlBrowser.this.bForward.setEnabled(HtmlBrowser.this.browserImpl.isForward());
            } else if (property.equals("backward") && HtmlBrowser.this.bBack != null) {
                HtmlBrowser.this.bBack.setEnabled(HtmlBrowser.this.browserImpl.isBackward());
            } else if (property.equals("loading") && HtmlBrowser.this.bStop != null) {
                HtmlBrowser.this.bStop.setEnabled((Boolean)evt.getNewValue());
            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == HtmlBrowser.this.txtLocation) {
                if (HtmlBrowser.this.ignoreChangeInLocationField) {
                    return;
                }
                String txt = HtmlBrowser.this.txtLocation.getText();
                if (txt == null || txt.length() == 0) {
                    return;
                }
                HtmlBrowser.this.setURL(txt);
            } else if (e.getSource() == HtmlBrowser.this.bBack) {
                HtmlBrowser.this.browserImpl.backward();
            } else if (e.getSource() == HtmlBrowser.this.bForward) {
                HtmlBrowser.this.browserImpl.forward();
            } else if (e.getSource() == HtmlBrowser.this.bReload) {
                HtmlBrowser.this.updateLocationBar();
                HtmlBrowser.this.browserImpl.reloadDocument();
            } else if (e.getSource() == HtmlBrowser.this.bStop) {
                HtmlBrowser.this.browserImpl.stopLoading();
            }
        }
    }

    public static interface Factory {
        public Impl createHtmlBrowserImpl();
    }

}

