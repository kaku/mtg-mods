/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 */
package org.openide.awt;

import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ButtonUI;
import javax.swing.plaf.basic.BasicButtonUI;
import org.openide.util.ImageUtilities;

public final class CloseButtonFactory {
    private static Icon closeTabImage;
    private static Icon closeTabPressedImage;
    private static Icon closeTabMouseOverImage;
    private static Icon bigCloseTabImage;
    private static Icon bigCloseTabPressedImage;
    private static Icon bigCloseTabMouseOverImage;

    private CloseButtonFactory() {
    }

    public static JButton createCloseButton() {
        JButton closeButton = new JButton(){

            @Override
            public void updateUI() {
                this.setUI(new BasicButtonUI());
            }
        };
        int size = 16;
        closeButton.setPreferredSize(new Dimension(size, size));
        closeButton.setContentAreaFilled(false);
        closeButton.setFocusable(false);
        closeButton.setBorder(BorderFactory.createEmptyBorder());
        closeButton.setBorderPainted(false);
        closeButton.setRolloverEnabled(true);
        closeButton.setIcon(CloseButtonFactory.getCloseTabImage());
        closeButton.setRolloverIcon(CloseButtonFactory.getCloseTabRolloverImage());
        closeButton.setPressedIcon(CloseButtonFactory.getCloseTabPressedImage());
        return closeButton;
    }

    public static JButton createBigCloseButton() {
        JButton closeButton = new JButton(){

            @Override
            public void updateUI() {
                this.setUI(new BasicButtonUI());
            }
        };
        int size = 19;
        closeButton.setPreferredSize(new Dimension(size, size));
        closeButton.setContentAreaFilled(false);
        closeButton.setFocusable(false);
        closeButton.setBorder(BorderFactory.createEmptyBorder());
        closeButton.setBorderPainted(false);
        closeButton.setRolloverEnabled(true);
        closeButton.setIcon(CloseButtonFactory.getBigCloseTabImage());
        closeButton.setRolloverIcon(CloseButtonFactory.getBigCloseTabRolloverImage());
        closeButton.setPressedIcon(CloseButtonFactory.getBigCloseTabPressedImage());
        return closeButton;
    }

    private static boolean isWindowsVistaLaF() {
        return CloseButtonFactory.isWindowsLaF() && (CloseButtonFactory.isWindowsVista() || CloseButtonFactory.isWindows7()) && CloseButtonFactory.isWindowsXPLaF();
    }

    private static boolean isWindows8LaF() {
        return CloseButtonFactory.isWindowsLaF() && CloseButtonFactory.isWindows8() && CloseButtonFactory.isWindowsXPLaF();
    }

    private static boolean isWindowsVista() {
        String osName = System.getProperty("os.name");
        return osName.indexOf("Vista") >= 0 || osName.equals("Windows NT (unknown)") && "6.0".equals(System.getProperty("os.version"));
    }

    private static boolean isWindows8() {
        String osName = System.getProperty("os.name");
        return osName.indexOf("Windows 8") >= 0 || osName.equals("Windows NT (unknown)") && "6.2".equals(System.getProperty("os.version"));
    }

    private static boolean isWindows7() {
        String osName = System.getProperty("os.name");
        return osName.indexOf("Windows 7") >= 0 || osName.equals("Windows NT (unknown)") && "6.1".equals(System.getProperty("os.version"));
    }

    private static boolean isWindowsXPLaF() {
        Boolean isXP = (Boolean)Toolkit.getDefaultToolkit().getDesktopProperty("win.xpstyle.themeActive");
        return CloseButtonFactory.isWindowsLaF() && isXP != null && isXP != false;
    }

    private static boolean isWindowsLaF() {
        String lfID = UIManager.getLookAndFeel().getID();
        return lfID.endsWith("Windows");
    }

    private static boolean isAquaLaF() {
        return "Aqua".equals(UIManager.getLookAndFeel().getID());
    }

    private static boolean isGTKLaF() {
        return "GTK".equals(UIManager.getLookAndFeel().getID());
    }

    private static Icon getCloseTabImage() {
        String path;
        if (null == closeTabImage && null != (path = UIManager.getString("nb.close.tab.icon.enabled.name"))) {
            closeTabImage = ImageUtilities.loadImageIcon((String)path, (boolean)true);
        }
        if (null == closeTabImage) {
            closeTabImage = CloseButtonFactory.isWindows8LaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/win8_bigclose_enabled.png", (boolean)true) : (CloseButtonFactory.isWindowsVistaLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/vista_close_enabled.png", (boolean)true) : (CloseButtonFactory.isWindowsXPLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/xp_close_enabled.png", (boolean)true) : (CloseButtonFactory.isWindowsLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/win_close_enabled.png", (boolean)true) : (CloseButtonFactory.isAquaLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/mac_close_enabled.png", (boolean)true) : (CloseButtonFactory.isGTKLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/gtk_close_enabled.png", (boolean)true) : ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/metal_close_enabled.png", (boolean)true))))));
        }
        return closeTabImage;
    }

    private static Icon getCloseTabPressedImage() {
        String path;
        if (null == closeTabPressedImage && null != (path = UIManager.getString("nb.close.tab.icon.pressed.name"))) {
            closeTabPressedImage = ImageUtilities.loadImageIcon((String)path, (boolean)true);
        }
        if (null == closeTabPressedImage) {
            closeTabPressedImage = CloseButtonFactory.isWindows8LaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/win8_bigclose_pressed.png", (boolean)true) : (CloseButtonFactory.isWindowsVistaLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/vista_close_pressed.png", (boolean)true) : (CloseButtonFactory.isWindowsXPLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/xp_close_pressed.png", (boolean)true) : (CloseButtonFactory.isWindowsLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/win_close_pressed.png", (boolean)true) : (CloseButtonFactory.isAquaLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/mac_close_pressed.png", (boolean)true) : (CloseButtonFactory.isGTKLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/gtk_close_pressed.png", (boolean)true) : ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/metal_close_pressed.png", (boolean)true))))));
        }
        return closeTabPressedImage;
    }

    private static Icon getCloseTabRolloverImage() {
        String path;
        if (null == closeTabMouseOverImage && null != (path = UIManager.getString("nb.close.tab.icon.rollover.name"))) {
            closeTabMouseOverImage = ImageUtilities.loadImageIcon((String)path, (boolean)true);
        }
        if (null == closeTabMouseOverImage) {
            closeTabMouseOverImage = CloseButtonFactory.isWindows8LaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/win8_bigclose_rollover.png", (boolean)true) : (CloseButtonFactory.isWindowsVistaLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/vista_close_rollover.png", (boolean)true) : (CloseButtonFactory.isWindowsXPLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/xp_close_rollover.png", (boolean)true) : (CloseButtonFactory.isWindowsLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/win_close_rollover.png", (boolean)true) : (CloseButtonFactory.isAquaLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/mac_close_rollover.png", (boolean)true) : (CloseButtonFactory.isGTKLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/gtk_close_rollover.png", (boolean)true) : ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/metal_close_rollover.png", (boolean)true))))));
        }
        return closeTabMouseOverImage;
    }

    private static Icon getBigCloseTabImage() {
        String path;
        if (null == bigCloseTabImage && null != (path = UIManager.getString("nb.bigclose.tab.icon.enabled.name"))) {
            bigCloseTabImage = ImageUtilities.loadImageIcon((String)path, (boolean)true);
        }
        if (null == bigCloseTabImage) {
            bigCloseTabImage = CloseButtonFactory.isWindows8LaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/win8_bigclose_enabled.png", (boolean)true) : (CloseButtonFactory.isWindowsVistaLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/vista_bigclose_enabled.png", (boolean)true) : (CloseButtonFactory.isWindowsXPLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/xp_bigclose_enabled.png", (boolean)true) : (CloseButtonFactory.isWindowsLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/win_bigclose_enabled.png", (boolean)true) : (CloseButtonFactory.isAquaLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/mac_bigclose_enabled.png", (boolean)true) : (CloseButtonFactory.isGTKLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/gtk_bigclose_enabled.png", (boolean)true) : ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/metal_bigclose_enabled.png", (boolean)true))))));
        }
        return bigCloseTabImage;
    }

    private static Icon getBigCloseTabPressedImage() {
        String path;
        if (null == bigCloseTabPressedImage && null != (path = UIManager.getString("nb.bigclose.tab.icon.pressed.name"))) {
            bigCloseTabPressedImage = ImageUtilities.loadImageIcon((String)path, (boolean)true);
        }
        if (null == bigCloseTabPressedImage) {
            bigCloseTabPressedImage = CloseButtonFactory.isWindows8LaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/win8_bigclose_pressed.png", (boolean)true) : (CloseButtonFactory.isWindowsVistaLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/vista_bigclose_pressed.png", (boolean)true) : (CloseButtonFactory.isWindowsXPLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/xp_bigclose_pressed.png", (boolean)true) : (CloseButtonFactory.isWindowsLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/win_bigclose_pressed.png", (boolean)true) : (CloseButtonFactory.isAquaLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/mac_bigclose_pressed.png", (boolean)true) : (CloseButtonFactory.isGTKLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/gtk_bigclose_pressed.png", (boolean)true) : ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/metal_bigclose_pressed.png", (boolean)true))))));
        }
        return bigCloseTabPressedImage;
    }

    private static Icon getBigCloseTabRolloverImage() {
        String path;
        if (null == bigCloseTabMouseOverImage && null != (path = UIManager.getString("nb.bigclose.tab.icon.rollover.name"))) {
            bigCloseTabMouseOverImage = ImageUtilities.loadImageIcon((String)path, (boolean)true);
        }
        if (null == bigCloseTabMouseOverImage) {
            bigCloseTabMouseOverImage = CloseButtonFactory.isWindows8LaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/win8_bigclose_rollover.png", (boolean)true) : (CloseButtonFactory.isWindowsVistaLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/vista_bigclose_rollover.png", (boolean)true) : (CloseButtonFactory.isWindowsXPLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/xp_bigclose_rollover.png", (boolean)true) : (CloseButtonFactory.isWindowsLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/win_bigclose_rollover.png", (boolean)true) : (CloseButtonFactory.isAquaLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/mac_bigclose_rollover.png", (boolean)true) : (CloseButtonFactory.isGTKLaF() ? ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/gtk_bigclose_rollover.png", (boolean)true) : ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/metal_bigclose_rollover.png", (boolean)true))))));
        }
        return bigCloseTabMouseOverImage;
    }

}

