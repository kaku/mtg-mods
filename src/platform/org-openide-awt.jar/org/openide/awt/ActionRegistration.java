/*
 * Decompiled with CFR 0_118.
 */
package org.openide.awt;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(value=RetentionPolicy.SOURCE)
@Target(value={ElementType.TYPE, ElementType.FIELD, ElementType.METHOD})
public @interface ActionRegistration {
    public String displayName();

    public String menuText() default "";

    public String popupText() default "";

    public String iconBase() default "";

    public boolean iconInMenu() default 1;

    public String key() default "";

    public boolean asynchronous() default 0;

    public boolean surviveFocusChange() default 0;

    public boolean lazy() default 1;
}

