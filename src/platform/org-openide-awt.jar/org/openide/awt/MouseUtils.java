/*
 * Decompiled with CFR 0_118.
 */
package org.openide.awt;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.ref.WeakReference;
import javax.swing.SwingUtilities;

public class MouseUtils {
    private static int DOUBLE_CLICK_DELTA = 300;
    private static int tempx = 0;
    private static int tempy = 0;
    private static long temph = 0;
    private static int tempm = 0;
    private static WeakReference<MouseEvent> tempe;

    @Deprecated
    public static boolean isRightMouseButton(MouseEvent e) {
        return SwingUtilities.isRightMouseButton(e);
    }

    @Deprecated
    public static boolean isLeftMouseButton(MouseEvent e) {
        return SwingUtilities.isLeftMouseButton(e);
    }

    public static boolean isDoubleClick(MouseEvent e) {
        if (e.getID() != 500 || e.getClickCount() == 0) {
            return false;
        }
        return e.getClickCount() % 2 == 0 || MouseUtils.isDoubleClickImpl(e);
    }

    private static boolean isDoubleClickImpl(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();
        long h = e.getWhen();
        int m = e.getModifiers();
        if (tempx == x && tempy == y && h - temph < (long)DOUBLE_CLICK_DELTA && tempe != null && e != tempe.get() && m == tempm) {
            tempx = 0;
            tempy = 0;
            temph = 0;
            tempm = 0;
            tempe = null;
            return true;
        }
        tempx = x;
        tempy = y;
        temph = h;
        tempm = m;
        tempe = new WeakReference<MouseEvent>(e);
        return false;
    }

    public static abstract class PopupMouseAdapter
    extends MouseAdapter {
        @Deprecated
        public static final int DEFAULT_THRESHOLD = 5;

        @Deprecated
        public PopupMouseAdapter(int threshold) {
            this();
        }

        public PopupMouseAdapter() {
        }

        @Override
        public void mousePressed(MouseEvent e) {
            this.maybePopup(e);
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            this.maybePopup(e);
        }

        private void maybePopup(MouseEvent e) {
            if (e.isPopupTrigger()) {
                this.showPopup(e);
            }
        }

        protected abstract void showPopup(MouseEvent var1);
    }

}

