/*
 * Decompiled with CFR 0_118.
 */
package org.openide.awt;

import javax.accessibility.AccessibleContext;
import javax.swing.JMenu;

@Deprecated
public class JMenuPlus
extends JMenu {
    private static final long serialVersionUID = -7700146216422707913L;

    public JMenuPlus() {
        this("");
    }

    public JMenuPlus(String label) {
        super(label);
        this.enableInputMethods(false);
        this.getAccessibleContext().setAccessibleDescription(label);
    }
}

