/*
 * Decompiled with CFR 0_118.
 */
package org.openide.awt;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;

@Deprecated
public class EqualFlowLayout
extends FlowLayout {
    static final long serialVersionUID = -1996929627282401218L;

    public EqualFlowLayout() {
    }

    public EqualFlowLayout(int align) {
        super(align);
    }

    public EqualFlowLayout(int align, int hgap, int vgap) {
        super(align, hgap, vgap);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private int getMaximumWidth(Container target) {
        int maxWidth = 0;
        Object object = target.getTreeLock();
        synchronized (object) {
            int nmembers = target.getComponentCount();
            for (int i = 0; i < nmembers; ++i) {
                Component m = target.getComponent(i);
                if (!m.isVisible()) continue;
                Dimension d = m.getPreferredSize();
                maxWidth = Math.max(d.width, maxWidth);
            }
        }
        return maxWidth;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Dimension preferredLayoutSize(Container target) {
        int maxWidth = this.getMaximumWidth(target);
        Object object = target.getTreeLock();
        synchronized (object) {
            Dimension dim = new Dimension(0, 0);
            int nmembers = target.getComponentCount();
            for (int i = 0; i < nmembers; ++i) {
                Component m = target.getComponent(i);
                if (!m.isVisible()) continue;
                Dimension d = m.getPreferredSize();
                dim.height = Math.max(dim.height, d.height);
                if (i > 0) {
                    dim.width += this.getHgap();
                }
                dim.width += maxWidth;
            }
            Insets insets = target.getInsets();
            dim.width += insets.left + insets.right + this.getHgap() * 2;
            dim.height += insets.top + insets.bottom + this.getVgap() * 2;
            return dim;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Dimension minimumLayoutSize(Container target) {
        Object object = target.getTreeLock();
        synchronized (object) {
            Dimension dim = new Dimension(0, 0);
            int nmembers = target.getComponentCount();
            for (int i = 0; i < nmembers; ++i) {
                Component m = target.getComponent(i);
                if (!m.isVisible()) continue;
                Dimension d = m.getMinimumSize();
                dim.height = Math.max(dim.height, d.height);
                if (i > 0) {
                    dim.width += this.getHgap();
                }
                dim.width += d.width;
            }
            Insets insets = target.getInsets();
            dim.width += insets.left + insets.right + this.getHgap() * 2;
            dim.height += insets.top + insets.bottom + this.getVgap() * 2;
            return dim;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void moveComponents2(Container target, int x, int y, int width, int height, int rowStart, int rowEnd) {
        Object object = target.getTreeLock();
        synchronized (object) {
            switch (this.getAlignment()) {
                case 0: {
                    break;
                }
                case 1: {
                    x += width / 2;
                    break;
                }
                case 2: {
                    x += width;
                }
            }
            for (int i = rowStart; i < rowEnd; ++i) {
                Component m = target.getComponent(i);
                if (!m.isVisible()) continue;
                m.setLocation(x, y + (height - m.getSize().height) / 2);
                x += this.getHgap() + m.getSize().width;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void layoutContainer(Container target) {
        int maxWidth = this.getMaximumWidth(target);
        Object object = target.getTreeLock();
        synchronized (object) {
            Insets insets = target.getInsets();
            int maxwidth = target.getSize().width - (insets.left + insets.right + this.getHgap() * 2);
            int nmembers = target.getComponentCount();
            int x = 0;
            int y = insets.top + this.getVgap();
            int rowh = 0;
            int start = 0;
            for (int i = 0; i < nmembers; ++i) {
                Component m = target.getComponent(i);
                if (!m.isVisible()) continue;
                Dimension d = m.getPreferredSize();
                d.width = maxWidth;
                m.setSize(d.width, d.height);
                if (x == 0 || x + d.width <= maxwidth) {
                    if (x > 0) {
                        x += this.getHgap();
                    }
                    x += d.width;
                    rowh = Math.max(rowh, d.height);
                    continue;
                }
                this.moveComponents2(target, insets.left + this.getHgap(), y, maxwidth - x, rowh, start, i);
                x = d.width;
                y += this.getVgap() + rowh;
                rowh = d.height;
                start = i;
            }
            this.moveComponents2(target, insets.left + this.getHgap(), y, maxwidth - x, rowh, start, nmembers);
        }
    }
}

