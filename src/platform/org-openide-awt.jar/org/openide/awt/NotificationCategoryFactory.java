/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 *  org.openide.util.lookup.Lookups
 */
package org.openide.awt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import org.openide.awt.NotificationDisplayer;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

class NotificationCategoryFactory {
    static final String ATTR_CATEGORY_NAME = "categoryName";
    static final String ATTR_BUNDLE_NAME = "localizingBundle";
    static final String ATTR_DISPLAY_NAME_KEY = "diplayNameKey";
    static final String ATTR_DESCRIPTION_KEY = "descriptionKey";
    private static final String CATEGORY_LIST_PATH = "Notification/Category";
    private static NotificationCategoryFactory theInstance;
    private Lookup.Result<NotificationDisplayer.Category> lookupRes;
    private Map<String, NotificationDisplayer.Category> name2category;
    private List<NotificationDisplayer.Category> categories;

    private NotificationCategoryFactory() {
    }

    static NotificationDisplayer.Category create(Map<String, String> attrs) {
        String categoryName = attrs.get("categoryName");
        String bundleName = attrs.get("localizingBundle");
        String displayNameKey = attrs.get("diplayNameKey");
        String descriptionKey = attrs.get("descriptionKey");
        return NotificationCategoryFactory.create(categoryName, bundleName, displayNameKey, descriptionKey);
    }

    static NotificationDisplayer.Category create(String categoryName, String bundleName, String displayNameKey, String descriptionKey) {
        ResourceBundle bundle = NbBundle.getBundle((String)bundleName);
        String displayName = bundle.getString(displayNameKey);
        String description = bundle.getString(descriptionKey);
        return new NotificationDisplayer.Category(categoryName, displayName, description);
    }

    public static NotificationCategoryFactory getInstance() {
        if (null == theInstance) {
            theInstance = new NotificationCategoryFactory();
        }
        return theInstance;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    NotificationDisplayer.Category getCategory(String categoryName) {
        assert (null != categoryName);
        NotificationCategoryFactory notificationCategoryFactory = this;
        synchronized (notificationCategoryFactory) {
            this.initCategories();
            return this.name2category.get(categoryName);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    List<NotificationDisplayer.Category> getCategories() {
        NotificationCategoryFactory notificationCategoryFactory = this;
        synchronized (notificationCategoryFactory) {
            this.initCategories();
            return this.categories;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void initCategories() {
        NotificationCategoryFactory notificationCategoryFactory = this;
        synchronized (notificationCategoryFactory) {
            if (null == this.name2category) {
                if (null == this.lookupRes) {
                    this.lookupRes = this.initLookup();
                    this.lookupRes.addLookupListener(new LookupListener(){

                        /*
                         * WARNING - Removed try catching itself - possible behaviour change.
                         */
                        public void resultChanged(LookupEvent ev) {
                            NotificationCategoryFactory notificationCategoryFactory = NotificationCategoryFactory.this;
                            synchronized (notificationCategoryFactory) {
                                NotificationCategoryFactory.this.name2category = null;
                                NotificationCategoryFactory.this.categories = null;
                            }
                        }
                    });
                }
                int index = 0;
                this.categories = new ArrayList<NotificationDisplayer.Category>(NotificationDisplayer.Category.getDefaultCategories());
                this.categories.addAll(this.lookupRes.allInstances());
                this.name2category = new HashMap<String, NotificationDisplayer.Category>(this.categories.size());
                for (NotificationDisplayer.Category c : this.categories) {
                    this.name2category.put(c.getName(), c);
                    c.setIndex(index++);
                }
            }
        }
    }

    private Lookup.Result<NotificationDisplayer.Category> initLookup() {
        Lookup lkp = Lookups.forPath((String)"Notification/Category");
        Lookup.Template template = new Lookup.Template(NotificationDisplayer.Category.class);
        Lookup.Result res = lkp.lookup(template);
        return res;
    }

}

