/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.openide.awt;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.NoRouteToHostException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.CSS;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.HTMLFrameHyperlinkEvent;
import javax.swing.text.html.StyleSheet;
import org.openide.awt.HtmlBrowser;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

final class SwingBrowserImpl
extends HtmlBrowser.Impl
implements Runnable {
    private static final int NO_NAVIGATION = 1;
    private static final int NAVIGATION_BACK = 2;
    private static final int NAVIGATION_FWD = 3;
    private static final RequestProcessor rp = new RequestProcessor("Swing Browser");
    private URL url;
    private URL loadingURL;
    private PropertyChangeSupport pcs;
    private String statusMessage = "";
    private SwingBrowser swingBrowser;
    private final JScrollPane scroll;
    private Vector<Object> historyList;
    private int historyIndex;
    private int historyNavigating = 1;
    private String title = null;
    boolean fetchingTitle = false;
    private static Logger LOG = Logger.getLogger(SwingBrowserImpl.class.getName());

    SwingBrowserImpl() {
        this.pcs = new PropertyChangeSupport(this);
        this.swingBrowser = new SwingBrowser();
        this.scroll = new JScrollPane(this.swingBrowser);
        this.historyList = new Vector(5, 3);
        this.historyIndex = -1;
        this.swingBrowser.addPropertyChangeListener("page", new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getNewValue() instanceof URL) {
                    URL old = SwingBrowserImpl.this.url;
                    SwingBrowserImpl.this.url = (URL)evt.getNewValue();
                    SwingBrowserImpl.this.pcs.firePropertyChange("url", old, SwingBrowserImpl.this.url);
                    if (((URL)evt.getNewValue()).equals(SwingBrowserImpl.this.loadingURL)) {
                        SwingBrowserImpl.this.loadingURL = null;
                    }
                    if (SwingBrowserImpl.this.historyNavigating == 2) {
                        int idx = SwingBrowserImpl.this.historyList.lastIndexOf(evt.getNewValue(), SwingBrowserImpl.this.historyIndex - 1);
                        if (idx != -1) {
                            SwingBrowserImpl.this.historyIndex = idx;
                        }
                    } else if (SwingBrowserImpl.this.historyNavigating == 3) {
                        int idx = SwingBrowserImpl.this.historyList.indexOf(evt.getNewValue(), SwingBrowserImpl.this.historyIndex + 1);
                        if (idx != -1) {
                            SwingBrowserImpl.this.historyIndex = idx;
                        }
                    } else {
                        while (SwingBrowserImpl.this.historyList.size() > SwingBrowserImpl.this.historyIndex + 1) {
                            SwingBrowserImpl.this.historyList.remove(SwingBrowserImpl.this.historyList.size() - 1);
                        }
                        SwingBrowserImpl.this.historyList.add(evt.getNewValue());
                        SwingBrowserImpl.this.historyIndex = SwingBrowserImpl.this.historyList.size() - 1;
                    }
                    SwingBrowserImpl.this.historyNavigating = 1;
                    SwingBrowserImpl.this.pcs.firePropertyChange("backward", null, null);
                    SwingBrowserImpl.this.pcs.firePropertyChange("forward", null, null);
                    SwingUtilities.invokeLater(SwingBrowserImpl.this);
                }
            }
        });
    }

    @Override
    public Component getComponent() {
        return this.scroll;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void reloadDocument() {
        RequestProcessor requestProcessor = rp;
        synchronized (requestProcessor) {
            try {
                if (this.url == null || this.loadingURL != null) {
                    return;
                }
                Document doc = this.swingBrowser.getDocument();
                this.loadingURL = this.url;
                if (doc instanceof AbstractDocument) {
                    String protocol = this.url.getProtocol();
                    if ("ftp".equalsIgnoreCase(protocol) || "http".equalsIgnoreCase(protocol)) {
                        ((AbstractDocument)doc).setAsynchronousLoadPriority(5);
                    } else {
                        ((AbstractDocument)doc).setAsynchronousLoadPriority(-1);
                    }
                }
                rp.post((Runnable)this);
            }
            catch (Exception e) {
                LOG.log(Level.WARNING, null, e);
                this.statusMessage = "" + e;
                this.pcs.firePropertyChange("statusMessage", null, this.statusMessage);
            }
        }
    }

    @Override
    public void stopLoading() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setURL(URL url) {
        RequestProcessor requestProcessor = rp;
        synchronized (requestProcessor) {
            try {
                if (url == null) {
                    return;
                }
                this.loadingURL = url;
                rp.post((Runnable)this);
            }
            catch (Exception e) {
                LOG.log(Level.WARNING, null, e);
                this.statusMessage = "" + e;
                this.pcs.firePropertyChange("statusMessage", null, this.statusMessage);
            }
        }
    }

    @Override
    public URL getURL() {
        return this.url;
    }

    @Override
    public String getStatusMessage() {
        return this.statusMessage;
    }

    @Override
    public String getTitle() {
        if (this.title == null) {
            Mutex.EVENT.readAccess((Runnable)this);
        }
        return this.title == null ? NbBundle.getMessage(SwingBrowserImpl.class, (String)"LBL_Loading") : this.title;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void updateTitle() {
        assert (SwingUtilities.isEventDispatchThread());
        if (this.fetchingTitle) {
            return;
        }
        this.fetchingTitle = true;
        String oldTitle = this.getTitle();
        try {
            URL u;
            Document d = this.swingBrowser.getDocument();
            this.title = (String)d.getProperty("title");
            if ((this.title == null || this.title.trim().length() == 0) && (u = this.getURL()) != null) {
                this.title = u.getFile();
                if (this.title.length() == 0) {
                    this.title = NbBundle.getMessage(SwingBrowserImpl.class, (String)"LBL_Untitled");
                } else {
                    int i = this.title.lastIndexOf("/");
                    if (i != -1 && i != this.title.length() - 1) {
                        this.title = this.title.substring(i + 1);
                    }
                }
            }
            if (this.title != null) {
                if (this.title.length() > 60) {
                    this.title = NbBundle.getMessage(SwingBrowserImpl.class, (String)"LBL_Title", (Object[])new Object[]{this.title.substring(0, 57)});
                }
                if (!oldTitle.equals(this.title)) {
                    this.pcs.firePropertyChange("title", oldTitle, this.title);
                }
            }
        }
        finally {
            this.fetchingTitle = false;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void run() {
        if (SwingUtilities.isEventDispatchThread()) {
            this.title = null;
            this.updateTitle();
        } else {
            URL requestedURL;
            RequestProcessor requestProcessor = rp;
            synchronized (requestProcessor) {
                Document doc;
                if (this.url != null && this.url.sameFile(this.url) && (doc = this.swingBrowser.getDocument()) != null) {
                    doc.putProperty("stream", null);
                }
                requestedURL = this.loadingURL;
                this.loadingURL = null;
            }
            try {
                this.swingBrowser.setPage(requestedURL);
                this.setStatusText(null);
            }
            catch (UnknownHostException uhe) {
                this.setStatusText(NbBundle.getMessage(SwingBrowserImpl.class, (String)"FMT_UnknownHost", (Object[])new Object[]{requestedURL}));
            }
            catch (NoRouteToHostException nrthe) {
                this.setStatusText(NbBundle.getMessage(SwingBrowserImpl.class, (String)"FMT_NoRouteToHost", (Object[])new Object[]{requestedURL}));
            }
            catch (IOException ioe) {
                this.setStatusText(NbBundle.getMessage(SwingBrowserImpl.class, (String)"FMT_InvalidURL", (Object[])new Object[]{requestedURL}));
            }
            SwingUtilities.invokeLater(this);
        }
    }

    void setStatusText(String s) {
        this.statusMessage = s;
        this.pcs.firePropertyChange("statusMessage", null, this.statusMessage);
    }

    @Override
    public boolean isForward() {
        return this.historyIndex >= 0 && this.historyIndex < this.historyList.size() - 1 && this.historyNavigating == 1;
    }

    @Override
    public void forward() {
        if (this.isForward()) {
            this.historyNavigating = 3;
            this.setURL((URL)this.historyList.elementAt(this.historyIndex + 1));
        }
    }

    @Override
    public boolean isBackward() {
        return this.historyIndex > 0 && this.historyIndex < this.historyList.size() && this.historyNavigating == 1;
    }

    @Override
    public void backward() {
        if (this.isBackward()) {
            this.historyNavigating = 2;
            this.setURL((URL)this.historyList.elementAt(this.historyIndex - 1));
        }
    }

    @Override
    public boolean isHistory() {
        return false;
    }

    @Override
    public void showHistory() {
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    private static String findEncodingFromURL(InputStream stream) {
        try {
            byte[] arr = new byte[4096];
            int len = stream.read(arr, 0, arr.length);
            String txt = new String(arr, 0, len >= 0 ? len : 0).toUpperCase();
            return SwingBrowserImpl.findEncoding(txt);
        }
        catch (Exception x) {
            x.printStackTrace();
            return null;
        }
    }

    private static String findEncoding(String txt) {
        int content;
        int headLen = txt.indexOf("</HEAD>");
        if (headLen == -1) {
            headLen = txt.length();
        }
        if ((content = txt.indexOf("CONTENT-TYPE")) == -1 || content > headLen) {
            return null;
        }
        int charset = txt.indexOf("CHARSET=", content);
        if (charset == -1) {
            return null;
        }
        int charend = txt.indexOf(34, charset);
        int charend2 = txt.indexOf(39, charset);
        if (charend == -1 && charend2 == -1) {
            return null;
        }
        if (charend2 != -1 && (charend == -1 || charend > charend2)) {
            charend = charend2;
        }
        return txt.substring(charset + "CHARSET=".length(), charend);
    }

    private static class FilteredStyleSheet
    extends StyleSheet {
        private FilteredStyleSheet() {
        }

        @Override
        public void addCSSAttribute(MutableAttributeSet attr, CSS.Attribute key, String value) {
            value = FilteredStyleSheet.fixFontSize(key, value);
            super.addCSSAttribute(attr, key, value);
        }

        @Override
        public boolean addCSSAttributeFromHTML(MutableAttributeSet attr, CSS.Attribute key, String value) {
            value = FilteredStyleSheet.fixFontSize(key, value);
            return super.addCSSAttributeFromHTML(attr, key, value);
        }

        private static String fixFontSize(CSS.Attribute key, String value) {
            String strPercentage;
            int percentage;
            if ("font-size".equals(key.toString()) && null != value && value.endsWith("%") && (percentage = Integer.parseInt(strPercentage = value.replace("%", ""))) < 100) {
                value = "100%";
            }
            return value;
        }
    }

    private static class FilteredInputStream
    extends FilterInputStream {
        private final URLConnection conn;
        private final SwingBrowserImpl browser;

        FilteredInputStream(URLConnection conn, SwingBrowserImpl browser) {
            super(null);
            this.conn = conn;
            this.browser = browser;
        }

        private synchronized void openStream() throws IOException {
            if (this.in == null) {
                this.in = this.conn.getInputStream();
            }
        }

        @Override
        public int available() throws IOException {
            this.openStream();
            return super.available();
        }

        @Override
        public long skip(long n) throws IOException {
            this.openStream();
            return super.skip(n);
        }

        @Override
        public void reset() throws IOException {
            this.openStream();
            super.reset();
        }

        @Override
        public void close() throws IOException {
            this.openStream();
            super.close();
            Mutex.EVENT.readAccess((Runnable)this.browser);
        }

        @Override
        public int read(byte[] b) throws IOException {
            this.openStream();
            return super.read(b);
        }

        @Override
        public int read(byte[] b, int off, int len) throws IOException {
            this.openStream();
            return super.read(b, off, len);
        }

        @Override
        public int read() throws IOException {
            this.openStream();
            return super.read();
        }
    }

    private class SwingBrowser
    extends JEditorPane {
        private boolean lastPaintException;

        private SwingBrowser() {
            this.lastPaintException = false;
            this.setEditable(false);
            this.addHyperlinkListener(new HyperlinkListener(SwingBrowserImpl.this){
                final /* synthetic */ SwingBrowserImpl val$this$0;

                @Override
                public void hyperlinkUpdate(HyperlinkEvent e) {
                    if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                        if (e instanceof HTMLFrameHyperlinkEvent) {
                            HTMLFrameHyperlinkEvent evt = (HTMLFrameHyperlinkEvent)e;
                            HTMLDocument doc = (HTMLDocument)SwingBrowser.this.getDocument();
                            URL old = SwingBrowserImpl.this.getURL();
                            doc.processHTMLFrameHyperlinkEvent(evt);
                            SwingBrowserImpl.this.pcs.firePropertyChange("url", old, e.getURL());
                        } else {
                            try {
                                SwingBrowserImpl.this.setURL(e.getURL());
                            }
                            catch (Exception ex) {
                                LOG.log(Level.WARNING, null, ex);
                            }
                        }
                    }
                }
            });
            ActionMap actionMap = this.getActionMap();
            actionMap.put("caret-up", new ScrollAction(-1));
            actionMap.put("caret-down", new ScrollAction(1));
        }

        @Override
        public EditorKit getEditorKitForContentType(String type) {
            if ("text/html".equals(type)) {
                return new HTMLEditorKit(){

                    @Override
                    public Document createDefaultDocument() {
                        StyleSheet styles = this.getStyleSheet();
                        FilteredStyleSheet ss = new FilteredStyleSheet();
                        ss.addStyleSheet(styles);
                        HTMLDocument doc = new HTMLDocument(ss);
                        doc.setParser(this.getParser());
                        doc.setAsynchronousLoadPriority(4);
                        doc.setTokenThreshold(100);
                        return doc;
                    }
                };
            }
            return super.getEditorKitForContentType(type);
        }

        @Override
        protected InputStream getStream(URL page) throws IOException {
            SwingUtilities.invokeLater(SwingBrowserImpl.this);
            try {
                String charset = SwingBrowserImpl.findEncodingFromURL(page.openStream());
                LOG.log(Level.FINE, "Url " + page + " has charset " + charset);
                if (charset != null) {
                    this.putClientProperty("charset", charset);
                }
            }
            catch (IllegalArgumentException iaE) {
                MalformedURLException e = new MalformedURLException();
                e.initCause(iaE);
                throw e;
            }
            if (Boolean.getBoolean("org.openide.awt.SwingBrowserImpl.do-not-block-awt")) {
                this.setContentType("text/html");
                return new FilteredInputStream(page.openConnection(), SwingBrowserImpl.this);
            }
            return super.getStream(page);
        }

        @Override
        public Dimension getPreferredSize() {
            try {
                return super.getPreferredSize();
            }
            catch (RuntimeException e) {
                return new Dimension(400, 600);
            }
        }

        @Override
        public void paint(Graphics g) {
            try {
                super.paint(g);
                this.lastPaintException = false;
            }
            catch (RuntimeException e) {
                if (!this.lastPaintException) {
                    this.repaint();
                }
                this.lastPaintException = true;
            }
        }

        @Override
        public void scrollToReference(String reference) {
            if (!this.isShowing() || null == this.getParent() || this.getWidth() < 1 || this.getHeight() < 1) {
                return;
            }
            super.scrollToReference(reference);
        }

        @Deprecated
        @Override
        public void layout() {
            try {
                super.layout();
            }
            catch (ArrayIndexOutOfBoundsException aioobE) {
                StackTraceElement[] stack = aioobE.getStackTrace();
                if (stack.length > 0 && stack[0].getClassName().endsWith("BoxView")) {
                    Logger.getLogger(SwingBrowser.class.getName()).log(Level.INFO, null, aioobE);
                }
                throw aioobE;
            }
        }

        private class ScrollAction
        extends AbstractAction {
            int direction;

            public ScrollAction(int direction) {
                this.direction = direction;
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                Rectangle r = SwingBrowser.this.getVisibleRect();
                int increment = SwingBrowser.this.getScrollableUnitIncrement(r, 1, this.direction);
                r.y += increment * this.direction;
                SwingBrowser.this.scrollRectToVisible(r);
            }
        }

    }

}

