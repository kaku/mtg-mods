/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package org.openide.awt;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javax.swing.AbstractButton;
import javax.swing.JLabel;
import javax.swing.UIManager;
import org.openide.util.Utilities;

public final class Mnemonics {
    private static final String PROP_TEXT = "text";
    private static final String PROP_MNEMONIC = "mnemonic";
    private static final String PROP_DISPLAYED_MNEMONIC_INDEX = "displayedMnemonicIndex";
    private static final PropertyChangeListener MNEMONIC_INDEX_LISTENER = new PropertyChangeListener(){

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            AbstractButton b = (AbstractButton)evt.getSource();
            if (b.getDisplayedMnemonicIndex() == -1) {
                Integer mnemonic = (Integer)b.getClientProperty("mnemonic");
                Integer index = (Integer)b.getClientProperty("displayedMnemonicIndex");
                if (mnemonic != null && index != null && Utilities.compareObjects((Object)b.getText(), (Object)b.getClientProperty("text"))) {
                    b.setMnemonic(mnemonic);
                    b.setDisplayedMnemonicIndex(index);
                }
            }
        }
    };

    private Mnemonics() {
    }

    private static void setLocalizedText2(Object item, String text) {
        if (text == null) {
            Mnemonics.setText(item, null);
            return;
        }
        int i = Mnemonics.findMnemonicAmpersand(text);
        Mnemonics.setMnemonicIndex(item, -1);
        if (i < 0) {
            Mnemonics.setText(item, text);
            Mnemonics.setMnemonic(item, 0);
        } else {
            Mnemonics.setText(item, text.substring(0, i) + text.substring(i + 1));
            if (Mnemonics.isAquaLF()) {
                Mnemonics.setMnemonic(item, 0);
            } else {
                char ch = text.charAt(i + 1);
                if (text.startsWith("<html>")) {
                    Mnemonics.setText(item, text.substring(0, i) + "<u>" + ch + "</u>" + text.substring(i + 2));
                    i += 3;
                }
                if (ch >= 'A' && ch <= 'Z' || ch >= 'a' && ch <= 'z' || ch >= '0' && ch <= '9') {
                    Mnemonics.setMnemonic(item, ch);
                    Mnemonics.setMnemonicIndex(item, i);
                } else {
                    try {
                        int latinCode = Mnemonics.getLatinKeycode(ch);
                        Mnemonics.setMnemonic(item, latinCode);
                        Mnemonics.setMnemonicIndex(item, i);
                    }
                    catch (MissingResourceException e) {
                        Logger.getLogger(Mnemonics.class.getName()).info("Mapping from a non-Latin character '" + ch + "' not found in a localized (branded) version of " + "openide/awt/src/org/openide/awt/Mnemonics.properties - " + "mnemonic cannot be assigned in " + text);
                    }
                }
            }
        }
    }

    public static void setLocalizedText(AbstractButton item, String text) {
        Mnemonics.setLocalizedText2(item, text);
    }

    public static void setLocalizedText(JLabel item, String text) {
        Mnemonics.setLocalizedText2(item, text);
    }

    public static int findMnemonicAmpersand(String text) {
        int i = -1;
        boolean isHTML = text.startsWith("<html>");
        do {
            if ((i = text.indexOf(38, i + 1)) < 0 || i + 1 >= text.length()) continue;
            if (isHTML) {
                boolean startsEntity = false;
                for (int j = i + 1; j < text.length(); ++j) {
                    char c = text.charAt(j);
                    if (c == ';') {
                        startsEntity = true;
                        break;
                    }
                    if (!Character.isLetterOrDigit(c)) break;
                }
                if (startsEntity) continue;
                return i;
            }
            if (text.charAt(i + 1) == ' ' || text.charAt(i + 1) == '\'' && i > 0 && text.charAt(i - 1) == '\'') continue;
            return i;
        } while (i >= 0);
        return -1;
    }

    private static int getLatinKeycode(char localeChar) throws MissingResourceException {
        String str = Mnemonics.getBundle().getString("MNEMONIC_" + localeChar);
        if (str.length() == 1) {
            return str.charAt(0);
        }
        return Integer.parseInt(str);
    }

    private static void setMnemonicIndex(Object item, int index) {
        if (item instanceof AbstractButton) {
            AbstractButton b = (AbstractButton)item;
            b.putClientProperty("displayedMnemonicIndex", index);
            b.removePropertyChangeListener("displayedMnemonicIndex", MNEMONIC_INDEX_LISTENER);
            b.setDisplayedMnemonicIndex(index);
            b.addPropertyChangeListener("displayedMnemonicIndex", MNEMONIC_INDEX_LISTENER);
        } else if (item instanceof JLabel) {
            ((JLabel)item).setDisplayedMnemonicIndex(index);
        }
    }

    private static void setText(Object item, String text) {
        if (item instanceof AbstractButton) {
            AbstractButton b = (AbstractButton)item;
            b.putClientProperty("text", text);
            b.setText(text);
        } else {
            ((JLabel)item).setText(text);
        }
    }

    private static void setMnemonic(Object item, int mnem) {
        if (Mnemonics.isAquaLF()) {
            return;
        }
        if (mnem >= 97 && mnem <= 122) {
            mnem += -32;
        }
        if (item instanceof AbstractButton) {
            AbstractButton b = (AbstractButton)item;
            b.putClientProperty("mnemonic", mnem);
            b.setMnemonic(mnem);
        } else {
            ((JLabel)item).setDisplayedMnemonic(mnem);
        }
    }

    private static ResourceBundle getBundle() {
        return ResourceBundle.getBundle("org.openide.awt.Mnemonics");
    }

    static boolean isAquaLF() {
        return "Aqua".equals(UIManager.getLookAndFeel().getID());
    }

}

