/*
 * Decompiled with CFR 0_118.
 */
package org.openide.awt;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.openide.awt.ActionID;

@Retention(value=RetentionPolicy.SOURCE)
@Target(value={ElementType.TYPE, ElementType.FIELD, ElementType.METHOD})
public @interface ActionReference {
    public String path();

    public int position() default Integer.MAX_VALUE;

    public ActionID id() default @ActionID(id="", category="");

    public String name() default "";

    public int separatorBefore() default Integer.MAX_VALUE;

    public int separatorAfter() default Integer.MAX_VALUE;
}

