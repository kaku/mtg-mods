/*
 * Decompiled with CFR 0_118.
 */
package org.openide.awt;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ComponentListener;
import java.awt.event.ContainerListener;
import java.awt.event.FocusListener;
import java.awt.event.HierarchyBoundsListener;
import java.awt.event.HierarchyListener;
import java.awt.event.InputMethodListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeListener;
import java.beans.VetoableChangeListener;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.AncestorListener;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.LabelUI;
import org.openide.awt.HtmlLabelUI;
import org.openide.awt.HtmlRenderer;

class HtmlRendererImpl
extends JLabel
implements HtmlRenderer.Renderer {
    private static final Rectangle bounds = new Rectangle();
    private static final boolean swingRendering = Boolean.getBoolean("nb.useSwingHtmlRendering");
    private static final Insets EMPTY_INSETS = new Insets(0, 0, 0, 0);
    private static boolean noCacheGraphics = Boolean.getBoolean("nb.renderer.nocache");
    private static Reference<Graphics> scratchGraphics = null;
    private boolean centered = false;
    private boolean parentFocused = false;
    private Boolean html = null;
    private int indent = 0;
    private Border border = null;
    private boolean selected = false;
    private boolean leadSelection = false;
    private Dimension prefSize = null;
    private Type type = Type.UNKNOWN;
    private int renderStyle = 0;
    private boolean enabled = true;

    HtmlRendererImpl() {
    }

    @Override
    public void reset() {
        this.parentFocused = false;
        this.setCentered(false);
        this.html = null;
        this.indent = 0;
        this.border = null;
        this.setIcon(null);
        this.setOpaque(false);
        this.selected = false;
        this.leadSelection = false;
        this.prefSize = null;
        this.type = Type.UNKNOWN;
        this.renderStyle = 0;
        this.setFont(UIManager.getFont("controlFont"));
        this.setIconTextGap(3);
        this.setEnabled(true);
        this.border = null;
        HtmlRendererImpl.EMPTY_INSETS.top = 0;
        HtmlRendererImpl.EMPTY_INSETS.left = 0;
        HtmlRendererImpl.EMPTY_INSETS.right = 0;
        HtmlRendererImpl.EMPTY_INSETS.bottom = 0;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean leadSelection, int row, int column) {
        this.reset();
        this.configureFrom(value, table, selected, leadSelection);
        this.type = Type.TABLE;
        if (swingRendering && selected) {
            this.setBackground(table.getSelectionBackground());
            this.setForeground(table.getSelectionForeground());
            this.setOpaque(true);
        }
        return this;
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean leadSelection) {
        this.reset();
        this.configureFrom(value, tree, selected, leadSelection);
        this.type = Type.TREE;
        if (swingRendering && selected) {
            if (HtmlLabelUI.isGTK()) {
                this.setBackground(HtmlLabelUI.getBackgroundFor(this));
                this.setForeground(HtmlLabelUI.getForegroundFor(this));
            }
            this.setOpaque(true);
        }
        return this;
    }

    public Component getListCellRendererComponent(JList list, Object value, int index, boolean selected, boolean leadSelection) {
        this.reset();
        this.configureFrom(value, list, selected, leadSelection);
        this.type = Type.LIST;
        if (swingRendering && selected) {
            this.setBackground(list.getSelectionBackground());
            this.setForeground(list.getSelectionForeground());
            this.setOpaque(true);
        }
        if (HtmlLabelUI.isGTK()) {
            if (index == -1) {
                Color borderC = UIManager.getColor("controlShadow");
                borderC = borderC == null ? Color.GRAY : borderC;
                this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(borderC), BorderFactory.createEmptyBorder(3, 2, 3, 2)));
            } else {
                this.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
            }
        }
        return this;
    }

    private void configureFrom(Object value, JComponent target, boolean selected, boolean leadSelection) {
        if (value == null) {
            value = "";
        }
        this.setText(value == null ? "" : value.toString());
        this.setSelected(selected);
        if (selected) {
            this.setParentFocused(this.checkFocused(target));
        } else {
            this.setParentFocused(false);
        }
        this.setEnabled(target.isEnabled());
        this.setLeadSelection(leadSelection);
        Font font = target.getFont();
        if (font.getName().toUpperCase().contains("LUCIDA")) {
            Font fontTemp = UIManager.getLookAndFeelDefaults().getFont("Label.font");
            font = new Font(fontTemp.getName(), font.getStyle(), fontTemp.getSize());
        }
        this.setFont(font);
    }

    private boolean checkFocused(JComponent c) {
        boolean result;
        Component focused = KeyboardFocusManager.getCurrentKeyboardFocusManager().getPermanentFocusOwner();
        boolean bl = result = c == focused;
        if (!result) {
            result = c.isAncestorOf(focused);
        }
        return result;
    }

    @Override
    public void addNotify() {
        if (swingRendering) {
            super.addNotify();
        }
    }

    @Override
    public void removeNotify() {
        if (swingRendering) {
            super.removeNotify();
        }
    }

    public void setSelected(boolean val) {
        this.selected = val;
    }

    @Override
    public void setParentFocused(boolean val) {
        this.parentFocused = val;
    }

    public void setLeadSelection(boolean val) {
        this.leadSelection = val;
    }

    @Override
    public void setCentered(boolean val) {
        this.centered = val;
        if (val) {
            this.setIconTextGap(5);
        }
        if (swingRendering) {
            if (val) {
                this.setVerticalTextPosition(3);
                this.setHorizontalAlignment(0);
                this.setHorizontalTextPosition(0);
            } else {
                this.setVerticalTextPosition(0);
                this.setHorizontalAlignment(10);
                this.setHorizontalTextPosition(11);
            }
        }
    }

    @Override
    public void setIndent(int pixels) {
        this.indent = pixels;
    }

    @Override
    public void setHtml(boolean val) {
        Boolean wasHtml = this.html;
        String txt = this.getText();
        Boolean bl = this.html = val ? Boolean.TRUE : Boolean.FALSE;
        if (swingRendering && this.html != wasHtml) {
            this.firePropertyChange("text", txt, this.getText());
        }
    }

    @Override
    public void setRenderStyle(int style) {
        this.renderStyle = style;
    }

    int getRenderStyle() {
        return this.renderStyle;
    }

    boolean isLeadSelection() {
        return this.leadSelection;
    }

    boolean isCentered() {
        return this.centered;
    }

    boolean isParentFocused() {
        return this.parentFocused;
    }

    boolean isHtml() {
        if (this.html == null) {
            String s = this.getText();
            this.html = this.checkHtml(s);
        }
        return this.html;
    }

    private Boolean checkHtml(String s) {
        Boolean result = s == null ? Boolean.FALSE : (s.startsWith("<html") || s.startsWith("<HTML") ? Boolean.TRUE : Boolean.FALSE);
        return result;
    }

    boolean isSelected() {
        return this.selected;
    }

    int getIndent() {
        return this.indent;
    }

    Type getType() {
        return this.type;
    }

    @Override
    public Dimension getPreferredSize() {
        if (!swingRendering) {
            if (this.prefSize == null) {
                this.prefSize = this.getUI().getPreferredSize(this);
            }
            return this.prefSize;
        }
        return super.getPreferredSize();
    }

    @Override
    public String getText() {
        String result = super.getText();
        if (swingRendering && Boolean.TRUE.equals(this.html)) {
            result = this.ensureHtmlTags(result);
        } else if (swingRendering && this.html == null) {
            this.html = this.checkHtml(super.getText());
            if (Boolean.TRUE.equals(this.html)) {
                result = this.ensureHtmlTags(result);
            }
        }
        return result;
    }

    private String ensureHtmlTags(String s) {
        if (!(s = HtmlRendererImpl.ensureLegalFontColorTags(s)).startsWith("<HTML") && !s.startsWith("<html")) {
            s = "<html>" + s + "</html>";
        }
        return s;
    }

    private static String ensureLegalFontColorTags(String s) {
        String check = s.toUpperCase();
        int start = 0;
        int fidx = check.indexOf("<FONT", start);
        StringBuffer sb = null;
        if (fidx != -1 && fidx <= s.length()) {
            while (fidx != -1 && fidx <= s.length()) {
                int bangIdx;
                int eidx;
                int cidx = check.indexOf("COLOR", start);
                int tagEnd = check.indexOf(62, start);
                start = tagEnd + 1;
                if (tagEnd == -1) break;
                if (cidx != -1 && cidx < tagEnd && (eidx = check.indexOf(61, cidx)) != -1 && (bangIdx = check.indexOf(33, eidx)) != -1 && bangIdx < tagEnd) {
                    int colorStart = bangIdx + 1;
                    int colorEnd = tagEnd;
                    for (int i = colorStart; i < tagEnd; ++i) {
                        char c = s.charAt(i);
                        if (Character.isLetter(c)) continue;
                        colorEnd = i;
                        break;
                    }
                    if (sb == null) {
                        sb = new StringBuffer(s);
                    }
                    String colorString = s.substring(colorStart, colorEnd);
                    String converted = HtmlRendererImpl.convertToStandardColor(colorString);
                    sb.replace(bangIdx, colorEnd, converted);
                    s = sb.toString();
                    check = s.toUpperCase();
                }
                start = fidx = check.indexOf("<FONT", start);
            }
        }
        if (sb != null) {
            return sb.toString();
        }
        return s;
    }

    private static String convertToStandardColor(String colorString) {
        Color c = UIManager.getColor(colorString);
        if (c == null) {
            c = Color.BLACK;
        }
        StringBuffer sb = new StringBuffer(7);
        sb.append('#');
        sb.append(HtmlRendererImpl.hexString(c.getRed()));
        sb.append(HtmlRendererImpl.hexString(c.getGreen()));
        sb.append(HtmlRendererImpl.hexString(c.getBlue()));
        return sb.toString();
    }

    private static String hexString(int r) {
        String s = Integer.toHexString(r);
        if (s.length() == 1) {
            s = "" + '0' + s;
        }
        return s;
    }

    @Override
    protected final void firePropertyChange(String name, Object old, Object nue) {
        if (swingRendering) {
            if ("text".equals(name) && this.isHtml()) {
                nue = this.getText();
            }
            super.firePropertyChange(name, old, nue);
        }
    }

    @Override
    public Border getBorder() {
        Border result = this.indent != 0 && swingRendering ? BorderFactory.createEmptyBorder(0, this.indent, 0, 0) : this.border;
        return result;
    }

    @Override
    public void setBorder(Border b) {
        Border old = this.border;
        this.border = b;
        if (swingRendering) {
            this.firePropertyChange("border", old, b);
        }
    }

    @Override
    public Insets getInsets() {
        return this.getInsets(null);
    }

    @Override
    public Insets getInsets(Insets insets) {
        Insets result;
        Border b = this.getBorder();
        if (b == null) {
            result = EMPTY_INSETS;
        } else {
            try {
                result = b.getBorderInsets(this);
            }
            catch (NullPointerException e) {
                Logger.getLogger(HtmlRendererImpl.class.getName()).log(Level.FINE, null, e);
                result = EMPTY_INSETS;
            }
        }
        if (null != insets) {
            insets.set(result.top, result.left, result.bottom, result.right);
            return insets;
        }
        return result;
    }

    @Override
    public void setEnabled(boolean b) {
        this.enabled = b;
        if (swingRendering) {
            super.setEnabled(b);
        }
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    @Override
    public void updateUI() {
        if (swingRendering) {
            super.updateUI();
        } else {
            this.setUI(HtmlLabelUI.createUI(this));
        }
    }

    @Override
    public Graphics getGraphics() {
        Graphics result = null;
        if (this.isDisplayable()) {
            result = super.getGraphics();
        }
        if (result == null) {
            result = HtmlRendererImpl.scratchGraphics();
        }
        return result;
    }

    private static final Graphics scratchGraphics() {
        Graphics result = null;
        if (scratchGraphics != null && (result = scratchGraphics.get()) != null) {
            result.setClip(null);
        }
        if (result == null) {
            result = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage(1, 1).getGraphics();
            if (!noCacheGraphics) {
                scratchGraphics = new SoftReference<Graphics>(result);
            }
        }
        return result;
    }

    @Override
    public void setBounds(int x, int y, int w, int h) {
        if (swingRendering) {
            super.setBounds(x, y, w, h);
        }
        bounds.setBounds(x, y, w, h);
    }

    @Deprecated
    @Override
    public void reshape(int x, int y, int w, int h) {
        if (swingRendering) {
            super.reshape(x, y, w, h);
        }
    }

    @Override
    public int getWidth() {
        return HtmlRendererImpl.bounds.width;
    }

    @Override
    public int getHeight() {
        return HtmlRendererImpl.bounds.height;
    }

    @Override
    public Point getLocation() {
        return bounds.getLocation();
    }

    @Override
    public void validate() {
    }

    @Override
    public void repaint(long tm, int x, int y, int w, int h) {
    }

    @Override
    public void repaint() {
    }

    @Override
    public void invalidate() {
    }

    @Override
    public void revalidate() {
    }

    @Override
    public void addAncestorListener(AncestorListener l) {
        if (swingRendering) {
            super.addAncestorListener(l);
        }
    }

    @Override
    public void addComponentListener(ComponentListener l) {
        if (swingRendering) {
            super.addComponentListener(l);
        }
    }

    @Override
    public void addContainerListener(ContainerListener l) {
        if (swingRendering) {
            super.addContainerListener(l);
        }
    }

    @Override
    public void addHierarchyListener(HierarchyListener l) {
        if (swingRendering) {
            super.addHierarchyListener(l);
        }
    }

    @Override
    public void addHierarchyBoundsListener(HierarchyBoundsListener l) {
        if (swingRendering) {
            super.addHierarchyBoundsListener(l);
        }
    }

    @Override
    public void addInputMethodListener(InputMethodListener l) {
        if (swingRendering) {
            super.addInputMethodListener(l);
        }
    }

    @Override
    public void addFocusListener(FocusListener fl) {
        if (swingRendering) {
            super.addFocusListener(fl);
        }
    }

    @Override
    public void addMouseListener(MouseListener ml) {
        if (swingRendering) {
            super.addMouseListener(ml);
        }
    }

    @Override
    public void addMouseWheelListener(MouseWheelListener ml) {
        if (swingRendering) {
            super.addMouseWheelListener(ml);
        }
    }

    @Override
    public void addMouseMotionListener(MouseMotionListener ml) {
        if (swingRendering) {
            super.addMouseMotionListener(ml);
        }
    }

    @Override
    public void addVetoableChangeListener(VetoableChangeListener vl) {
        if (swingRendering) {
            super.addVetoableChangeListener(vl);
        }
    }

    @Override
    public void addPropertyChangeListener(String s, PropertyChangeListener l) {
        if (swingRendering) {
            super.addPropertyChangeListener(s, l);
        }
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener l) {
        if (swingRendering) {
            super.addPropertyChangeListener(l);
        }
    }

    static enum Type {
        UNKNOWN,
        TREE,
        LIST,
        TABLE;
        

        private Type() {
        }
    }

}

