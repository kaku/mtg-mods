/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.openide.awt;

import java.awt.Color;
import java.awt.Dialog;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import org.openide.awt.ColorComboBoxRendererWrapper;
import org.openide.awt.ColorValue;
import org.openide.util.NbBundle;

public final class ColorComboBox
extends JComboBox {
    private final boolean allowCustomColors;
    private ColorValue lastSelection;

    public ColorComboBox() {
        this(new Color[]{Color.BLACK, Color.BLUE, Color.CYAN, Color.DARK_GRAY, Color.GRAY, Color.GREEN, Color.LIGHT_GRAY, Color.MAGENTA, Color.ORANGE, Color.PINK, Color.RED, Color.WHITE, Color.YELLOW}, new String[0], true);
    }

    public ColorComboBox(Color[] values, String[] names, boolean allowCustomColors) {
        super.setModel(ColorComboBox.createModel(values, names, allowCustomColors));
        this.allowCustomColors = allowCustomColors;
        this.setEditable(false);
        this.setRenderer(new ColorComboBoxRendererWrapper(this));
        if (allowCustomColors) {
            this.addItemListener(new ItemListener(){

                @Override
                public void itemStateChanged(ItemEvent e) {
                    if (e.getStateChange() == 1) {
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                if (ColorComboBox.this.getSelectedItem() == ColorValue.CUSTOM_COLOR) {
                                    ColorComboBox.this.pickCustomColor();
                                }
                                ColorComboBox.this.lastSelection = (ColorValue)ColorComboBox.this.getSelectedItem();
                            }
                        });
                    }
                }

            });
        }
    }

    public void setModel(Color[] colors, String[] names) {
        super.setModel(ColorComboBox.createModel(colors, names, this.allowCustomColors));
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                ColorComboBox.this.repaint();
            }
        });
    }

    public Color getSelectedColor() {
        ColorValue cv = (ColorValue)this.getSelectedItem();
        return null == cv ? null : cv.color;
    }

    public void setSelectedColor(Color newColor) {
        if (null == newColor) {
            this.setSelectedIndex(-1);
            return;
        }
        for (int i = 0; i < this.getItemCount(); ++i) {
            ColorValue cv = (ColorValue)this.getItemAt(i);
            if (!newColor.equals(cv.color)) continue;
            this.setSelectedItem(cv);
            return;
        }
        if (this.allowCustomColors) {
            this.removeCustomValue();
            ColorValue cv = new ColorValue(newColor, true);
            DefaultComboBoxModel model = (DefaultComboBoxModel)this.getModel();
            model.insertElementAt(cv, 0);
            this.setSelectedItem(cv);
        }
    }

    private void removeCustomValue() {
        for (int i = 0; i < this.getItemCount(); ++i) {
            ColorValue cv = (ColorValue)this.getItemAt(i);
            if (!cv.isCustom) continue;
            DefaultComboBoxModel model = (DefaultComboBoxModel)this.getModel();
            model.removeElementAt(i);
            return;
        }
    }

    private void pickCustomColor() {
        Color c = JColorChooser.showDialog(SwingUtilities.getAncestorOfClass(Dialog.class, this), NbBundle.getMessage(ColorComboBox.class, (String)"SelectColor"), this.lastSelection != null ? this.lastSelection.color : null);
        if (c != null) {
            this.setSelectedColor(c);
        } else if (this.lastSelection != null) {
            this.setSelectedItem(this.lastSelection);
        }
    }

    private static DefaultComboBoxModel createModel(Color[] colors, String[] names, boolean allowCustomColors) {
        DefaultComboBoxModel<ColorValue> model = new DefaultComboBoxModel<ColorValue>();
        for (int i = 0; i < colors.length; ++i) {
            Color c = colors[i];
            String text = null;
            if (i < names.length) {
                text = names[i];
            }
            if (null == text) {
                text = ColorValue.toText(c);
            }
            model.addElement(new ColorValue(text, c, false));
        }
        if (allowCustomColors) {
            model.addElement(ColorValue.CUSTOM_COLOR);
        }
        return model;
    }

}

