/*
 * Decompiled with CFR 0_118.
 */
package org.openide.awt;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JPopupMenu;
import javax.swing.JToggleButton;
import org.openide.awt.DropDownButton;
import org.openide.awt.DropDownToggleButton;

public final class DropDownButtonFactory {
    public static final String PROP_DROP_DOWN_MENU = "dropDownMenu";

    private DropDownButtonFactory() {
    }

    public static JButton createDropDownButton(Icon icon, JPopupMenu dropDownMenu) {
        return new DropDownButton(icon, dropDownMenu);
    }

    public static JToggleButton createDropDownToggleButton(Icon icon, JPopupMenu dropDownMenu) {
        return new DropDownToggleButton(icon, dropDownMenu);
    }
}

