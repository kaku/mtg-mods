/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 */
package org.openide.awt;

import java.awt.Insets;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import org.openide.awt.DynamicMenuContent;
import org.openide.util.ImageUtilities;

@Deprecated
public class JInlineMenu
extends JMenuItem
implements DynamicMenuContent {
    static final long serialVersionUID = -2310488127953523571L;
    private static final Icon BLANK_ICON = ImageUtilities.loadImageIcon((String)"org/openide/resources/actions/empty.gif", (boolean)false);
    private JComponent[] items = new JComponent[0];
    boolean upToDate;
    private List addedItems;

    public JInlineMenu() {
        this.setEnabled(false);
        this.setVisible(false);
        this.upToDate = true;
    }

    @Override
    public Insets getInsets() {
        return new Insets(0, 0, 0, 0);
    }

    public void setMenuItems(JMenuItem[] newItems) {
        JComponent[] local = new JComponent[newItems.length];
        for (int i = 0; i < newItems.length; ++i) {
            local[i] = newItems[i] != null ? newItems[i] : new JSeparator();
        }
        this.items = local;
        this.upToDate = false;
        this.alignItems();
    }

    private void alignItems() {
        boolean shouldAlign;
        boolean bl = shouldAlign = this.getIcon() != null;
        if (!shouldAlign) {
            for (int i = 0; i < this.items.length; ++i) {
                if (!(this.items[i] instanceof JMenuItem) || ((JMenuItem)this.items[i]).getIcon() == null) continue;
                shouldAlign = true;
                break;
            }
        }
        if (!shouldAlign) {
            return;
        }
        JMenuItem curItem = null;
        for (int i = 0; i < this.items.length; ++i) {
            if (!(this.items[i] instanceof JMenuItem) || (curItem = (JMenuItem)this.items[i]).getIcon() != null) continue;
            curItem.setIcon(BLANK_ICON);
        }
    }

    private static int findIndex(Object of, Object[] arr) {
        int menuLength = arr.length;
        for (int i = 0; i < menuLength; ++i) {
            if (of != arr[i]) continue;
            return i;
        }
        return -1;
    }

    @Override
    public JComponent[] synchMenuPresenters(JComponent[] items) {
        return this.items;
    }

    @Override
    public JComponent[] getMenuPresenters() {
        return this.items;
    }
}

