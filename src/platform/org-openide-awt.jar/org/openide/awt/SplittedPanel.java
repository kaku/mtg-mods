/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.openide.awt;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.LayoutManager2;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Enumeration;
import java.util.EventObject;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleRole;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSeparator;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.metal.MetalLookAndFeel;
import org.openide.awt.MouseUtils;
import org.openide.util.NbBundle;

@Deprecated
public class SplittedPanel
extends JComponent
implements Accessible {
    static final long serialVersionUID = 5058424218525927233L;
    public static final int NONE = 0;
    public static final int VERTICAL = 1;
    public static final int HORIZONTAL = 2;
    public static final Object ADD_SPLITTER = new Integer(0);
    public static final Object ADD_FIRST = new Integer(1);
    public static final Object ADD_SECOND = new Integer(2);
    public static final Object ADD_LEFT = ADD_FIRST;
    public static final Object ADD_TOP = ADD_FIRST;
    public static final Object ADD_RIGHT = ADD_SECOND;
    public static final Object ADD_BOTTOM = ADD_SECOND;
    public static final int FIRST_PREFERRED = -1;
    public static final int SECOND_PREFERRED = -2;
    public static final int RAISED_SPLITTER = 0;
    public static final int EMPTY_SPLITTER = 1;
    public static final int DEFAULT_SPLITTER = 0;
    private static MessageFormat nameFormat = null;
    private static MessageFormat descriptionFormat = null;
    private static final int DEFAULT_SPLIT_TYPE = 2;
    private int resetPosition = -1;
    private Boolean popupMenuEnabled;
    private boolean drawBumps;
    private Component firstComponent = null;
    private Component secondComponent = null;
    private Component splitter = null;
    private int splitterType = 0;
    private transient MouseListenerAdapter mouseAdapter;
    private int splitType = 0;
    private int splitPosition = 50;
    private boolean absolute = false;
    private boolean dragable = true;
    private boolean continuousLayout = true;
    private boolean splitTypeChangeEnabled = true;
    private boolean swapPanesEnabled = true;
    private boolean keepSecondSame = false;
    private boolean keepFirstSame = false;
    private transient boolean splitIsChanging = false;
    private int dragPos = -1;
    private boolean panesSwapped = false;
    private transient JPopupMenu popupMenu;
    private transient JRadioButtonMenuItem verticalCMI;
    private transient JRadioButtonMenuItem horizontalCMI;
    private transient JMenuItem swapCMI;
    private transient JMenuItem splitterCMI;
    private transient Vector<SplitChangeListener> listeners;
    private AccessibleContext accessibleContext;

    public SplittedPanel() {
        this.splitter = new DefaultSplitter(this.getDefaultSplitterSize());
        this.accessibleContext = null;
        this.setLayout(new SplitLayout());
        this.add(this.splitter, ADD_SPLITTER);
        this.init();
        RuntimeException rte = new RuntimeException("SplittedPanel is deprecated.  Please use JSplitPane instead");
        Logger.getLogger(SplittedPanel.class.getName()).log(Level.WARNING, null, rte);
    }

    private void init() {
        this.setSplitterCursor();
        this.mouseAdapter = new MouseListenerAdapter();
        if (this.dragable) {
            this.splitter.addMouseMotionListener(this.mouseAdapter);
            this.splitter.addMouseListener(this.mouseAdapter);
            this.addSplitChangeListener(this.mouseAdapter);
        }
        this.initAccessible();
    }

    @Override
    public void updateUI() {
        super.updateUI();
        this.updateSplitting();
        Object o = UIManager.get("nb.SplittedPanel.drawBumps");
        this.drawBumps = Boolean.TRUE.equals(o);
    }

    protected void updateSplitting() {
        if (this.firstComponent != null && this.secondComponent != null) {
            this.invalidate();
            this.firstComponent.invalidate();
            this.splitter.invalidate();
            this.secondComponent.invalidate();
            this.validate();
        }
    }

    protected void computeSizesAfterFlip() {
        if (this.firstComponent == null || this.secondComponent == null) {
            return;
        }
        Dimension ourSize = this.getSize();
        switch (this.splitType) {
            case 1: {
                if (ourSize.width == 0) break;
                int splitterSize = this.splitter.getPreferredSize().height;
                int newHeight = (ourSize.height - splitterSize) * this.firstComponent.getSize().width / ourSize.width;
                this.firstComponent.setSize(new Dimension(ourSize.width, newHeight));
                this.secondComponent.setSize(new Dimension(ourSize.width, ourSize.height - newHeight - splitterSize));
                break;
            }
            case 2: {
                if (ourSize.height == 0) break;
                int splitterSize = this.splitter.getPreferredSize().width;
                int newWidth = (ourSize.width - splitterSize) * this.firstComponent.getSize().height / ourSize.height;
                this.firstComponent.setSize(new Dimension(newWidth, ourSize.height));
                this.secondComponent.setSize(new Dimension(ourSize.width - newWidth - splitterSize, ourSize.height));
            }
        }
    }

    protected void setSplitterCursor() {
        if (this.dragable) {
            if (this.splitType == 1) {
                this.splitter.setCursor(Cursor.getPredefinedCursor(8));
            } else {
                this.splitter.setCursor(Cursor.getPredefinedCursor(10));
            }
        } else {
            this.splitter.setCursor(Cursor.getPredefinedCursor(0));
        }
    }

    private boolean isPopupMenuEnabled() {
        if (this.popupMenuEnabled == null) {
            Object o = this.getClientProperty("popupMenuEnabled");
            this.popupMenuEnabled = o instanceof Boolean ? (Boolean)o : Boolean.TRUE;
        }
        return this.popupMenuEnabled;
    }

    protected void updatePopupMenu() {
        if (this.popupMenu == null) {
            this.popupMenu = new JPopupMenu();
            ResourceBundle awtBundle = NbBundle.getBundle(SplittedPanel.class);
            this.verticalCMI = new JRadioButtonMenuItem(awtBundle.getString("SplittedPanelVertical"));
            this.popupMenu.add(this.verticalCMI);
            this.horizontalCMI = new JRadioButtonMenuItem(awtBundle.getString("SplittedPanelHorizontal"));
            this.popupMenu.add(this.horizontalCMI);
            this.popupMenu.add(new JSeparator());
            this.swapCMI = new JMenuItem(awtBundle.getString("SplittedPanelSwap"));
            this.popupMenu.add(this.swapCMI);
            this.popupMenu.add(new JSeparator());
            this.splitterCMI = new JMenuItem(awtBundle.getString("ResetSplitter"));
            this.popupMenu.add(this.splitterCMI);
            ActionListener al = new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    if (SplittedPanel.this.horizontalCMI.equals(e.getSource())) {
                        SplittedPanel.this.setSplitType(2);
                    } else {
                        SplittedPanel.this.setSplitType(1);
                    }
                }
            };
            this.verticalCMI.addActionListener(al);
            this.horizontalCMI.addActionListener(al);
            this.swapCMI.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    SplittedPanel.this.swapPanes();
                }
            });
            this.splitterCMI.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    SplittedPanel.this.resetSplitter();
                }
            });
        }
        if (this.splitType == 1) {
            this.verticalCMI.setSelected(true);
            this.horizontalCMI.setSelected(false);
        } else {
            this.verticalCMI.setSelected(false);
            this.horizontalCMI.setSelected(true);
        }
        if (this.splitTypeChangeEnabled) {
            this.verticalCMI.setEnabled(true);
            this.horizontalCMI.setEnabled(true);
        } else {
            this.verticalCMI.setEnabled(false);
            this.horizontalCMI.setEnabled(false);
        }
        if (this.swapPanesEnabled) {
            this.swapCMI.setEnabled(true);
        } else {
            this.swapCMI.setEnabled(false);
        }
        this.splitterCMI.setEnabled(this.getSplitPosition() != -1 && this.getSplitPosition() != -2);
    }

    private void resetSplitter() {
        if (this.getSplitPosition() != -1 && this.getSplitPosition() != -2) {
            this.setSplitPosition(this.resetPosition);
            if (this.splitterCMI != null) {
                this.splitterCMI.setEnabled(false);
            }
        }
    }

    public void swapPanes() {
        if (!this.swapPanesEnabled) {
            return;
        }
        if (this.firstComponent == null || this.secondComponent == null) {
            return;
        }
        this.splitIsChanging = true;
        boolean bl = this.panesSwapped = !this.panesSwapped;
        if (this.keepSecondSame) {
            this.keepSecondSame = false;
            this.keepFirstSame = true;
        } else if (this.keepFirstSame) {
            this.keepSecondSame = true;
            this.keepFirstSame = false;
        }
        Component aFirstComponent = this.firstComponent;
        Component aSecondComponent = this.secondComponent;
        this.remove(aFirstComponent);
        this.remove(aSecondComponent);
        this.add(aSecondComponent, ADD_FIRST);
        this.add(aFirstComponent, ADD_SECOND);
        this.updateSplitting();
        this.splitIsChanging = false;
    }

    public boolean getPanesSwapped() {
        return this.panesSwapped;
    }

    public int getSplitType() {
        return this.splitType;
    }

    public void setSplitType(int value) {
        if (this.splitType == value) {
            return;
        }
        int oldSplitType = this.splitType;
        this.splitType = value;
        if (oldSplitType != 0 && this.splitType != 0) {
            this.computeSizesAfterFlip();
        }
        this.setSplitterCursor();
        this.updateSplitting();
        this.updatePopupMenu();
        this.initAccessible();
    }

    public int getSplitPosition() {
        return this.splitPosition;
    }

    public void setSplitPosition(int value) {
        if (this.splitPosition == value) {
            return;
        }
        int oldValue = this.splitPosition;
        this.splitPosition = value;
        this.splitIsChanging = true;
        this.updateSplitting();
        this.fireSplitChange(oldValue, this.splitPosition);
        this.splitIsChanging = false;
    }

    public int getSplitterType() {
        return this.splitterType;
    }

    private int getDefaultSplitterSize() {
        Object o = UIManager.get("nb.SplittedPanel.dividerSize");
        if (o != null) {
            return (Integer)o;
        }
        o = UIManager.get("SplitPane.dividerSize");
        if (o != null) {
            return (Integer)o;
        }
        return 6;
    }

    public void setSplitterType(int type) {
        if (this.splitterType == type) {
            return;
        }
        this.splitterType = type;
        switch (this.splitterType) {
            case 1: {
                this.splitter = new EmptySplitter();
                break;
            }
            default: {
                this.splitter = new DefaultSplitter(this.getDefaultSplitterSize());
            }
        }
        this.add(this.splitter, ADD_SPLITTER);
        this.updateSplitting();
    }

    public Component getSplitterComponent() {
        return this.splitter;
    }

    public void setSplitterComponent(Component comp) {
        if (this.splitter == comp) {
            return;
        }
        if (this.dragable) {
            this.splitter.removeMouseMotionListener(this.mouseAdapter);
            this.splitter.removeMouseListener(this.mouseAdapter);
        }
        this.remove(this.splitter);
        this.splitter = comp;
        this.add(this.splitter, ADD_SPLITTER);
        if (this.dragable) {
            this.splitter.addMouseMotionListener(this.mouseAdapter);
            this.splitter.addMouseListener(this.mouseAdapter);
        }
        this.setSplitterCursor();
        this.updateSplitting();
    }

    public boolean isSplitAbsolute() {
        return this.absolute;
    }

    public void setSplitAbsolute(boolean value) {
        if (this.absolute == value) {
            return;
        }
        this.absolute = value;
        this.updateSplitting();
    }

    public boolean isSplitDragable() {
        return this.dragable;
    }

    public void setSplitDragable(boolean value) {
        if (this.dragable == value) {
            return;
        }
        this.dragable = value;
        if (this.dragable) {
            this.splitter.addMouseMotionListener(this.mouseAdapter);
            this.splitter.addMouseListener(this.mouseAdapter);
        } else {
            this.splitter.removeMouseMotionListener(this.mouseAdapter);
            this.splitter.removeMouseListener(this.mouseAdapter);
        }
        this.setSplitterCursor();
    }

    public boolean isContinuousLayout() {
        return this.continuousLayout;
    }

    public void setContinuousLayout(boolean value) {
        this.continuousLayout = value;
    }

    public boolean getKeepFirstSame() {
        return this.keepFirstSame;
    }

    public void setKeepFirstSame(boolean value) {
        this.keepFirstSame = value;
    }

    public boolean getKeepSecondSame() {
        return this.keepSecondSame;
    }

    public void setKeepSecondSame(boolean value) {
        this.keepSecondSame = value;
    }

    public boolean isSplitTypeChangeEnabled() {
        return this.splitTypeChangeEnabled;
    }

    public void setSplitTypeChangeEnabled(boolean value) {
        if (this.splitTypeChangeEnabled == value) {
            return;
        }
        this.splitTypeChangeEnabled = value;
        this.updatePopupMenu();
    }

    public boolean isSwapPanesEnabled() {
        return this.swapPanesEnabled;
    }

    public void setSwapPanesEnabled(boolean value) {
        if (this.swapPanesEnabled == value) {
            return;
        }
        this.swapPanesEnabled = value;
        this.updatePopupMenu();
    }

    public void addSplitChangeListener(SplitChangeListener l) {
        if (this.listeners == null) {
            this.listeners = new Vector();
        }
        this.listeners.addElement(l);
    }

    public void removeSplitChangeListener(SplitChangeListener l) {
        if (this.listeners == null) {
            return;
        }
        this.listeners.removeElement(l);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void fireSplitChange(int oldValue, int newValue) {
        Vector<SplitChangeListener> l;
        if (this.listeners == null) {
            return;
        }
        SplittedPanel splittedPanel = this;
        synchronized (splittedPanel) {
            l = new Vector<SplitChangeListener>(this.listeners);
        }
        Enumeration<SplitChangeListener> en = l.elements();
        SplitChangeEvent evt = new SplitChangeEvent(this, oldValue, newValue);
        while (en.hasMoreElements()) {
            SplitChangeListener scl = en.nextElement();
            scl.splitChanged(evt);
        }
    }

    @Override
    public AccessibleContext getAccessibleContext() {
        if (this.accessibleContext == null) {
            this.accessibleContext = new JComponent.AccessibleJComponent(){

                @Override
                public AccessibleRole getAccessibleRole() {
                    return AccessibleRole.SPLIT_PANE;
                }
            };
            this.initAccessible();
        }
        return this.accessibleContext;
    }

    private void initAccessible() {
        ResourceBundle bundle;
        if (nameFormat == null) {
            bundle = NbBundle.getBundle(SplittedPanel.class);
            nameFormat = new MessageFormat(bundle.getString("ACS_SplittedPanel_Name"));
        }
        Object[] arrobject = new Object[2];
        arrobject[0] = this.firstComponent == null || !(this.firstComponent instanceof Accessible) ? null : this.firstComponent.getAccessibleContext().getAccessibleName();
        arrobject[1] = this.secondComponent == null || !(this.secondComponent instanceof Accessible) ? null : this.secondComponent.getAccessibleContext().getAccessibleName();
        this.getAccessibleContext().setAccessibleName(nameFormat.format(arrobject));
        if (descriptionFormat == null) {
            bundle = NbBundle.getBundle(SplittedPanel.class);
            descriptionFormat = new MessageFormat(bundle.getString("ACS_SplittedPanel_Description"));
        }
        Object[] arrobject2 = new Object[2];
        arrobject2[0] = this.firstComponent == null || !(this.firstComponent instanceof Accessible) ? null : this.firstComponent.getAccessibleContext().getAccessibleDescription();
        arrobject2[1] = this.secondComponent == null || !(this.secondComponent instanceof Accessible) ? null : this.secondComponent.getAccessibleContext().getAccessibleDescription();
        this.getAccessibleContext().setAccessibleDescription(descriptionFormat.format(arrobject2));
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        ois.defaultReadObject();
        this.init();
    }

    class SplitLayout
    implements LayoutManager2,
    Serializable {
        static final long serialVersionUID = 2034500275182524789L;

        SplitLayout() {
        }

        @Override
        public void addLayoutComponent(String name, Component comp) {
            throw new IllegalArgumentException("You must use the add(Component, Object) method for adding");
        }

        @Override
        public void addLayoutComponent(Component comp, Object constraints) {
            if (constraints == SplittedPanel.ADD_SPLITTER) {
                SplittedPanel.this.splitter = comp;
            } else if (constraints == SplittedPanel.ADD_FIRST) {
                if (SplittedPanel.this.firstComponent != null && SplittedPanel.this.secondComponent == null) {
                    SplittedPanel.this.secondComponent = SplittedPanel.this.firstComponent;
                }
                SplittedPanel.this.firstComponent = comp;
                if (SplittedPanel.this.secondComponent != null && SplittedPanel.this.splitType == 0) {
                    SplittedPanel.this.splitType = 2;
                }
            } else if (constraints == SplittedPanel.ADD_SECOND) {
                if (SplittedPanel.this.firstComponent == null) {
                    SplittedPanel.this.firstComponent = comp;
                } else {
                    SplittedPanel.this.secondComponent = comp;
                    if (SplittedPanel.this.splitType == 0) {
                        SplittedPanel.this.splitType = 2;
                    }
                }
            } else {
                throw new IllegalArgumentException("You must use one of the SplittedPanel.ADD_XXX constraints Objects");
            }
            SplittedPanel.this.initAccessible();
        }

        @Override
        public void removeLayoutComponent(Component comp) {
            if (comp.equals(SplittedPanel.this.secondComponent)) {
                SplittedPanel.this.secondComponent = null;
            } else if (comp.equals(SplittedPanel.this.firstComponent)) {
                SplittedPanel.this.firstComponent = null;
                if (SplittedPanel.this.secondComponent != null) {
                    SplittedPanel.this.firstComponent = SplittedPanel.this.secondComponent;
                    SplittedPanel.this.secondComponent = null;
                }
            }
            SplittedPanel.this.initAccessible();
        }

        @Override
        public Dimension preferredLayoutSize(Container parent) {
            Dimension d;
            int width = 0;
            int height = 0;
            if (SplittedPanel.this.firstComponent != null) {
                d = SplittedPanel.this.firstComponent.getPreferredSize();
                width = d.width;
                height = d.height;
            }
            if (SplittedPanel.this.secondComponent != null) {
                d = SplittedPanel.this.secondComponent.getPreferredSize();
                if (SplittedPanel.this.splitType == 1) {
                    int splitterSize = SplittedPanel.access$800((SplittedPanel)SplittedPanel.this).getPreferredSize().height;
                    if (width < d.width) {
                        width = d.width;
                    }
                    height += splitterSize + d.height;
                } else {
                    int splitterSize = SplittedPanel.access$800((SplittedPanel)SplittedPanel.this).getPreferredSize().width;
                    if (height < d.height) {
                        height = d.height;
                    }
                    width += splitterSize + d.width;
                }
            }
            return new Dimension(width, height);
        }

        @Override
        public Dimension minimumLayoutSize(Container parent) {
            Dimension d;
            int width = 0;
            int height = 0;
            if (SplittedPanel.this.firstComponent != null) {
                d = SplittedPanel.this.firstComponent.getMinimumSize();
                width = d.width;
                height = d.height;
            }
            if (SplittedPanel.this.secondComponent != null) {
                d = SplittedPanel.this.secondComponent.getMinimumSize();
                if (SplittedPanel.this.splitType == 1) {
                    int splitterSize = SplittedPanel.access$800((SplittedPanel)SplittedPanel.this).getMinimumSize().height;
                    if (width < d.width) {
                        width = d.width;
                    }
                    height += splitterSize + d.height;
                } else {
                    int splitterSize = SplittedPanel.access$800((SplittedPanel)SplittedPanel.this).getMinimumSize().width;
                    if (height < d.height) {
                        height = d.height;
                    }
                    width += splitterSize + d.width;
                }
            }
            return new Dimension(width, height);
        }

        @Override
        public void layoutContainer(Container parent) {
            Dimension d = parent.getSize();
            int sPosition = SplittedPanel.this.splitPosition;
            if (SplittedPanel.this.splitPosition == -1) {
                sPosition = SplittedPanel.this.splitType == 1 ? SplittedPanel.access$200((SplittedPanel)SplittedPanel.this).getPreferredSize().height : SplittedPanel.access$200((SplittedPanel)SplittedPanel.this).getPreferredSize().width;
            } else if (SplittedPanel.this.splitPosition == -2) {
                sPosition = SplittedPanel.this.splitType == 1 ? d.height - SplittedPanel.access$800((SplittedPanel)SplittedPanel.this).getPreferredSize().width - SplittedPanel.access$300((SplittedPanel)SplittedPanel.this).getPreferredSize().height : d.width - SplittedPanel.access$800((SplittedPanel)SplittedPanel.this).getPreferredSize().height - SplittedPanel.access$300((SplittedPanel)SplittedPanel.this).getPreferredSize().width;
            } else if (!SplittedPanel.this.absolute) {
                int sp = SplittedPanel.this.splitPosition;
                if (sp > 100) {
                    sp = 100;
                }
                sPosition = SplittedPanel.this.splitType == 1 ? d.height * sp / 100 : d.width * sp / 100;
            }
            if (SplittedPanel.this.splitType != 0 && SplittedPanel.this.firstComponent != null && SplittedPanel.this.secondComponent != null) {
                int splitterSize;
                if (SplittedPanel.this.splitType == 1) {
                    splitterSize = SplittedPanel.access$800((SplittedPanel)SplittedPanel.this).getPreferredSize().height;
                    if (SplittedPanel.this.firstComponent == null || SplittedPanel.this.secondComponent == null) {
                        splitterSize = 0;
                    }
                    if (SplittedPanel.this.keepSecondSame && !SplittedPanel.this.splitIsChanging) {
                        Dimension secondSize = SplittedPanel.this.secondComponent.getSize();
                        if (secondSize.height != 0) {
                            sPosition = d.height - secondSize.height - splitterSize;
                        }
                    }
                    if (sPosition + splitterSize > d.height) {
                        sPosition = d.height - splitterSize;
                    }
                    if (sPosition < 0) {
                        sPosition = 0;
                    }
                    SplittedPanel.this.firstComponent.setBounds(new Rectangle(0, 0, d.width, sPosition));
                    SplittedPanel.this.splitter.setBounds(new Rectangle(0, sPosition, d.width, splitterSize));
                    SplittedPanel.this.secondComponent.setBounds(new Rectangle(0, sPosition + splitterSize, d.width, d.height - sPosition - splitterSize));
                } else {
                    splitterSize = SplittedPanel.access$800((SplittedPanel)SplittedPanel.this).getPreferredSize().width;
                    if (SplittedPanel.this.firstComponent == null || SplittedPanel.this.secondComponent == null) {
                        splitterSize = 0;
                    }
                    if (SplittedPanel.this.keepSecondSame && !SplittedPanel.this.splitIsChanging) {
                        Dimension secondSize = SplittedPanel.this.secondComponent.getSize();
                        if (secondSize.width != 0) {
                            sPosition = d.width - secondSize.width - splitterSize;
                        }
                    }
                    if (sPosition + splitterSize > d.width) {
                        sPosition = d.width - splitterSize;
                    }
                    if (sPosition < 0) {
                        sPosition = 0;
                    }
                    SplittedPanel.this.firstComponent.setBounds(new Rectangle(0, 0, sPosition, d.height));
                    SplittedPanel.this.splitter.setBounds(new Rectangle(sPosition, 0, splitterSize, d.height));
                    SplittedPanel.this.secondComponent.setBounds(new Rectangle(sPosition + splitterSize, 0, d.width - sPosition - splitterSize, d.height));
                }
            } else if (SplittedPanel.this.firstComponent != null) {
                SplittedPanel.this.firstComponent.setBounds(new Rectangle(0, 0, d.width - 1, d.height - 1));
                if (SplittedPanel.this.splitter != null) {
                    SplittedPanel.this.splitter.setBounds(0, 0, 0, 0);
                }
            }
        }

        @Override
        public Dimension maximumLayoutSize(Container target) {
            return new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
        }

        @Override
        public float getLayoutAlignmentX(Container target) {
            return 0.0f;
        }

        @Override
        public float getLayoutAlignmentY(Container target) {
            return 0.0f;
        }

        @Override
        public void invalidateLayout(Container target) {
        }
    }

    class MouseListenerAdapter
    extends MouseUtils.PopupMouseAdapter
    implements MouseListener,
    MouseMotionListener,
    SplitChangeListener {
        MouseListenerAdapter() {
        }

        @Override
        protected void showPopup(MouseEvent e) {
            SplittedPanel.this.updatePopupMenu();
            if (SplittedPanel.this.isPopupMenuEnabled()) {
                SplittedPanel.this.popupMenu.show(SplittedPanel.this.splitter, e.getX(), e.getY());
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            super.mouseReleased(e);
            if (!SplittedPanel.this.continuousLayout) {
                if (SplittedPanel.this.dragPos == -1) {
                    return;
                }
                if (!SplittedPanel.this.absolute) {
                    Dimension d = SplittedPanel.this.getSize();
                    if (SplittedPanel.this.splitType == 1) {
                        SplittedPanel.this.dragPos = 100 * SplittedPanel.this.dragPos / d.height;
                    } else {
                        SplittedPanel.this.dragPos = 100 * SplittedPanel.this.dragPos / d.width;
                    }
                }
                SplittedPanel.this.setSplitPosition(SplittedPanel.this.dragPos);
                SplittedPanel.this.dragPos = -1;
            }
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            if (SplittedPanel.this.continuousLayout) {
                Dimension d = SplittedPanel.this.getSize();
                Point splitterPos = SplittedPanel.this.splitter.getLocation();
                e.translatePoint(splitterPos.x, splitterPos.y);
                if (SplittedPanel.this.splitType == 1) {
                    SplittedPanel.this.dragPos = e.getY();
                    if (SplittedPanel.this.dragPos > d.height) {
                        SplittedPanel.this.dragPos = d.height;
                    }
                } else {
                    SplittedPanel.this.dragPos = e.getX();
                    if (SplittedPanel.this.dragPos > d.width) {
                        SplittedPanel.this.dragPos = d.width;
                    }
                }
                if (SplittedPanel.this.dragPos < 0) {
                    SplittedPanel.this.dragPos = 0;
                }
                if (SplittedPanel.this.continuousLayout) {
                    if (SplittedPanel.this.dragPos == -1) {
                        return;
                    }
                    int newDragPos = SplittedPanel.this.dragPos;
                    if (!SplittedPanel.this.absolute) {
                        newDragPos = SplittedPanel.this.splitType == 1 ? 100 * SplittedPanel.this.dragPos / d.height : 100 * SplittedPanel.this.dragPos / d.width;
                    }
                    SplittedPanel.this.setSplitPosition(newDragPos);
                }
            }
        }

        @Override
        public void mouseMoved(MouseEvent e) {
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            super.mouseClicked(e);
            if (e.getClickCount() == 2 && SplittedPanel.this.isPopupMenuEnabled()) {
                SplittedPanel.this.resetSplitter();
            }
        }

        @Override
        public void splitChanged(SplitChangeEvent evt) {
            if (evt.getNewValue() == -1 || evt.getNewValue() == -2) {
                SplittedPanel.this.resetPosition = evt.getNewValue();
                if (SplittedPanel.this.splitterCMI != null) {
                    SplittedPanel.this.splitterCMI.setEnabled(true);
                }
            }
        }
    }

    class DefaultSplitter
    extends JComponent
    implements Accessible {
        static final long serialVersionUID = -4223135481223014719L;
        private int splitterSize;

        public DefaultSplitter(int aSplitterSize) {
            this.splitterSize = aSplitterSize;
            if (this.splitterSize < 2) {
                this.splitterSize = 2;
            }
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(this.splitterSize, this.splitterSize);
        }

        @Override
        public void paint(Graphics g) {
            super.paint(g);
            if (this.splitterSize <= 2) {
                return;
            }
            Dimension size = this.getSize();
            int height = size.height - 1;
            g.setColor(this.getBackground());
            Color high = UIManager.getColor("controlLtHighlight");
            Color low = UIManager.getColor("controlDkShadow");
            boolean isMetal = UIManager.getLookAndFeel().getClass() == MetalLookAndFeel.class;
            boolean firstHasBorder = true;
            boolean secondHasBorder = true;
            if (SplittedPanel.this.firstComponent instanceof JComponent) {
                Border b1 = ((JComponent)SplittedPanel.this.firstComponent).getBorder();
                boolean bl = firstHasBorder = b1 != null && !(b1 instanceof EmptyBorder);
            }
            if (SplittedPanel.this.secondComponent instanceof JComponent) {
                Border b2 = ((JComponent)SplittedPanel.this.secondComponent).getBorder();
                boolean bl = secondHasBorder = b2 != null && !(b2 instanceof EmptyBorder);
            }
            if (SplittedPanel.this.panesSwapped) {
                boolean b = firstHasBorder;
                firstHasBorder = secondHasBorder;
                secondHasBorder = b;
            }
            if (isMetal && this.splitterSize > 3 && SplittedPanel.this.drawBumps) {
                int startx;
                int starty = firstHasBorder && SplittedPanel.this.splitType == 1 ? 0 : 2;
                int x = startx = firstHasBorder && SplittedPanel.this.splitType == 2 ? 0 : 2;
                while (x + 1 < size.width) {
                    int y = starty;
                    while (y + 1 < height) {
                        g.setColor(this.getBackground().brighter());
                        g.drawLine(x, y, x, y);
                        if (x < size.width && y < height) {
                            g.drawLine(x + 2, y + 2, x + 2, y + 2);
                        }
                        g.setColor(this.getBackground().darker().darker());
                        g.drawLine(x + 1, y + 1, x + 1, y + 1);
                        if (x < size.width && y < height) {
                            g.drawLine(x + 3, y + 3, x + 3, y + 3);
                        }
                        y += 4;
                    }
                    x += 4;
                }
            }
            if (SplittedPanel.this.splitType == 2) {
                int pos = (size.width - this.splitterSize) / 2;
                if (!firstHasBorder) {
                    g.setColor(isMetal ? low : high);
                    g.drawLine(pos, 0, pos, size.height - 1);
                    if (isMetal) {
                        g.setColor(high);
                        g.drawLine(pos + 1, 0, pos + 1, size.height - 1);
                    }
                }
                if (!secondHasBorder) {
                    g.setColor(isMetal ? high : low);
                    g.drawLine(pos + this.splitterSize - 1, 0, pos + this.splitterSize - 1, size.height - 1);
                    if (isMetal) {
                        g.setColor(low);
                        g.drawLine(pos + this.splitterSize - 2, 0, pos + this.splitterSize - 2, size.height - 1);
                    }
                }
            } else if (SplittedPanel.this.splitType == 1) {
                int pos = (size.height - this.splitterSize) / 2;
                if (!firstHasBorder) {
                    g.setColor(isMetal ? low : high);
                    g.drawLine(0, pos, size.width - 1, pos);
                    if (isMetal) {
                        g.setColor(high);
                        g.drawLine(0, pos + 1, size.width - 1, pos + 1);
                    }
                }
                if (!secondHasBorder) {
                    g.setColor(isMetal ? high : low);
                    g.drawLine(0, pos + this.splitterSize - 1, size.width - 1, pos + this.splitterSize - 1);
                    if (isMetal) {
                        g.setColor(low);
                        g.drawLine(0, pos + this.splitterSize - 2, size.width - 1, pos + this.splitterSize - 2);
                    }
                }
            }
        }

        @Override
        public AccessibleContext getAccessibleContext() {
            return SplittedPanel.this.getAccessibleContext();
        }
    }

    @Deprecated
    public static class EmptySplitter
    extends JComponent
    implements Accessible {
        static final long serialVersionUID = 929648193440460693L;
        private int width;
        private AccessibleContext accessibleContext;

        public EmptySplitter() {
            this(0);
        }

        public EmptySplitter(int width) {
            ResourceBundle bundle = NbBundle.getBundle(SplittedPanel.class);
            this.accessibleContext = null;
            this.width = width;
            this.getAccessibleContext().setAccessibleName(bundle.getString("ACS_SplittedPanel_EmptySplitter"));
            this.getAccessibleContext().setAccessibleName(bundle.getString("ACSD_SplittedPanel_EmptySplitter"));
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(this.width, this.width);
        }

        @Override
        public AccessibleContext getAccessibleContext() {
            if (this.accessibleContext == null) {
                this.accessibleContext = new JComponent.AccessibleJComponent(){

                    @Override
                    public AccessibleRole getAccessibleRole() {
                        return AccessibleRole.SPLIT_PANE;
                    }
                };
            }
            return this.accessibleContext;
        }

    }

    @Deprecated
    public static class SplitChangeEvent
    extends EventObject {
        static final long serialVersionUID = 6748966611210836878L;
        private int oldValue;
        private int newValue;

        public SplitChangeEvent(SplittedPanel splittedPanel, int oldValue, int newValue) {
            super(splittedPanel);
            this.oldValue = oldValue;
            this.newValue = newValue;
        }

        public int getOldValue() {
            return this.oldValue;
        }

        public int getNewValue() {
            return this.newValue;
        }
    }

    public static interface SplitChangeListener {
        public void splitChanged(SplitChangeEvent var1);
    }

}

