/*
 * Decompiled with CFR 0_118.
 */
package org.openide.awt;

@Deprecated
public interface SpinButtonListener {
    public void moveUp();

    public void moveDown();

    public void changeValue();
}

