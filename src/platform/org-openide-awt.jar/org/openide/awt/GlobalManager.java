/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Mutex
 *  org.openide.util.Utilities
 *  org.openide.util.WeakSet
 */
package org.openide.awt;

import java.beans.PropertyChangeListener;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.ActionMap;
import org.openide.awt.ContextManager;
import org.openide.awt.GeneralAction;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Mutex;
import org.openide.util.Utilities;
import org.openide.util.WeakSet;

class GlobalManager
implements LookupListener {
    private static final Logger LOG = GeneralAction.LOG;
    private static final Map<ContextManager.LookupRef, Reference<GlobalManager>> CACHE = new HashMap<ContextManager.LookupRef, Reference<GlobalManager>>();
    private static final Map<ContextManager.LookupRef, Reference<GlobalManager>> SURVIVE = new HashMap<ContextManager.LookupRef, Reference<GlobalManager>>();
    private Lookup.Result<ActionMap> result;
    private Reference<ActionMap> actionMap = new WeakReference<Object>(null);
    private Map<Object, Set<GeneralAction.BaseDelAction>> listeners = new HashMap<Object, Set<GeneralAction.BaseDelAction>>();
    private PropertyChangeListener changeL;

    private GlobalManager(Lookup lookup) {
        this.result = lookup.lookupResult(ActionMap.class);
        this.result.addLookupListener((LookupListener)this);
        this.resultChanged(null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static GlobalManager findManager(Lookup context, boolean survive) {
        Map<ContextManager.LookupRef, Reference<GlobalManager>> map = CACHE;
        synchronized (map) {
            GlobalManager g;
            Map<ContextManager.LookupRef, Reference<GlobalManager>> map2 = survive ? SURVIVE : CACHE;
            ContextManager.LookupRef lr = new ContextManager.LookupRef(context);
            GMReference ref = map2.get(lr);
            GlobalManager globalManager = g = ref == null ? null : ref.get();
            if (g == null) {
                g = survive ? new SurviveManager(context) : new GlobalManager(context);
                ref = new GMReference(g, lr, survive);
                map2.put(lr, ref);
            }
            return g;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void clearCache(ContextManager.LookupRef lr, GMReference ref, boolean survive) {
        Map<ContextManager.LookupRef, Reference<GlobalManager>> map = CACHE;
        synchronized (map) {
            Map<ContextManager.LookupRef, Reference<GlobalManager>> map2;
            Map<ContextManager.LookupRef, Reference<GlobalManager>> map3 = map2 = survive ? SURVIVE : CACHE;
            if (map2.get(lr) == ref) {
                map2.remove(lr);
            }
        }
    }

    public Action findGlobalAction(Object key) {
        Action a;
        if (key == null) {
            return null;
        }
        ActionMap map = this.actionMap.get();
        Action action = a = map == null ? null : map.get(key);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Action for key: {0} is: {1}", new Object[]{key, a});
        }
        return a;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final void registerListener(Object key, GeneralAction.BaseDelAction a) {
        if (key == null) {
            return;
        }
        Map<ContextManager.LookupRef, Reference<GlobalManager>> map = CACHE;
        synchronized (map) {
            WeakSet existing = this.listeners.get(key);
            if (existing == null) {
                existing = new WeakSet();
                this.listeners.put(key, (Set<GeneralAction.BaseDelAction>)existing);
            }
            existing.add((GeneralAction.BaseDelAction)a);
            a.updateState(new ActionMap(), this.actionMap.get(), false);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final void unregisterListener(Object key, GeneralAction.BaseDelAction a) {
        if (key == null) {
            return;
        }
        Map<ContextManager.LookupRef, Reference<GlobalManager>> map = CACHE;
        synchronized (map) {
            Set<GeneralAction.BaseDelAction> existing = this.listeners.get(key);
            if (existing != null) {
                existing.remove(a);
                if (existing.isEmpty()) {
                    this.listeners.remove(key);
                }
            }
        }
    }

    public final void resultChanged(LookupEvent ev) {
        ActionMap a;
        ActionMap prev;
        Collection all = this.result.allItems();
        ActionMap actionMap = a = all.isEmpty() ? null : (ActionMap)((Lookup.Item)all.iterator().next()).getInstance();
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "changed map : {0}", a);
            LOG.log(Level.FINE, "previous map: {0}", this.actionMap.get());
        }
        if (a == (prev = this.actionMap.get())) {
            return;
        }
        final ActionMap newMap = this.newMap(prev, a);
        this.actionMap = new WeakReference<ActionMap>(newMap);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("clearActionPerformers");
        }
        Mutex.EVENT.readAccess(new Runnable(){

            @Override
            public void run() {
                GlobalManager.this.notifyListeners(prev, newMap);
            }
        });
    }

    final void notifyListeners(ActionMap prev, ActionMap now) {
        if (prev == null) {
            prev = new ActionMap();
        }
        if (now == null) {
            now = new ActionMap();
        }
        HashSet<Object> keys = new HashSet<Object>();
        Object[] allPrev = prev.allKeys();
        Object[] allNow = now.allKeys();
        if (allPrev != null) {
            keys.addAll(Arrays.asList(allPrev));
        }
        if (allNow != null) {
            keys.addAll(Arrays.asList(allNow));
        }
        for (Object k : keys) {
            Set<GeneralAction.BaseDelAction> actions = this.listeners.get(k);
            if (actions == null) continue;
            for (GeneralAction.BaseDelAction del : actions) {
                if (del == null) continue;
                del.updateState(prev, now, true);
            }
        }
    }

    public boolean isSurvive() {
        return false;
    }

    protected ActionMap newMap(ActionMap prev, ActionMap newMap) {
        return newMap;
    }

    private static final class SurviveManager
    extends GlobalManager {
        private SurviveManager(Lookup context) {
            super(context);
        }

        @Override
        public boolean isSurvive() {
            return true;
        }

        @Override
        protected ActionMap newMap(ActionMap prev, ActionMap newMap) {
            Object[] allK;
            Object[] all;
            ArrayList<Object> old = new ArrayList<Object>();
            if (prev != null && (all = prev.allKeys()) != null) {
                Object[] toRem;
                old.addAll(Arrays.asList(all));
                if (newMap != null && (toRem = newMap.allKeys()) != null) {
                    old.removeAll(Arrays.asList(toRem));
                }
            }
            ActionMap merged = new ActionMap();
            if (newMap != null && (allK = newMap.allKeys()) != null) {
                for (int i = 0; i < allK.length; ++i) {
                    Object o = allK[i];
                    merged.put(o, newMap.get(o));
                }
            }
            for (Object o : old) {
                merged.put(o, prev.get(o));
            }
            return merged;
        }
    }

    private static final class GMReference
    extends WeakReference<GlobalManager>
    implements Runnable {
        private ContextManager.LookupRef context;
        private boolean survive;

        public GMReference(GlobalManager m, ContextManager.LookupRef context, boolean survive) {
            super(m, Utilities.activeReferenceQueue());
            this.context = context;
            this.survive = survive;
        }

        @Override
        public void run() {
            GlobalManager.clearCache(this.context, this, this.survive);
        }
    }

}

