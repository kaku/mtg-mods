/*
 * Decompiled with CFR 0_118.
 */
package org.openide.awt;

enum ContextSelection {
    EXACTLY_ONE,
    ANY,
    EACH,
    ALL;
    

    private ContextSelection() {
    }
}

