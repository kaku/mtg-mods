/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Mutex
 */
package org.openide.awt;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import org.openide.util.ImageUtilities;
import org.openide.util.Mutex;

public class ToolbarWithOverflow
extends JToolBar {
    private JButton overflowButton;
    private JPopupMenu popup;
    private JToolBar overflowToolbar;
    private boolean displayOverflowOnHover = true;
    private final String toolbarArrowHorizontal = "org/openide/awt/resources/toolbar_arrow_horizontal.png";
    private final String toolbarArrowVertical = "org/openide/awt/resources/toolbar_arrow_vertical.png";
    private final String PROP_PREF_ICON_SIZE = "PreferredIconSize";
    private final String PROP_DRAGGER = "_toolbar_dragger_";
    private final String PROP_JDEV_DISABLE_OVERFLOW = "nb.toolbar.overflow.disable";
    private AWTEventListener awtEventListener;
    private ComponentAdapter componentAdapter;
    private static JPopupMenu showingPopup = null;

    public ToolbarWithOverflow() {
        this(0);
    }

    public ToolbarWithOverflow(int orientation) {
        this(null, orientation);
    }

    public ToolbarWithOverflow(String name) {
        this(name, 0);
    }

    public ToolbarWithOverflow(String name, int orientation) {
        super(name, orientation);
        this.setupOverflowButton();
        this.popup = new SafePopupMenu();
        this.popup.setBorderPainted(false);
        this.popup.setBorder(BorderFactory.createEmptyBorder());
        this.overflowToolbar = new SafeToolBar("overflowToolbar", orientation == 0 ? 1 : 0);
        this.overflowToolbar.setFloatable(false);
        this.overflowToolbar.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
    }

    private ComponentListener getComponentListener() {
        if (this.componentAdapter == null) {
            this.componentAdapter = new ComponentAdapter(){

                @Override
                public void componentResized(ComponentEvent e) {
                    ToolbarWithOverflow.this.maybeAddOverflow();
                }
            };
        }
        return this.componentAdapter;
    }

    private AWTEventListener getAWTEventListener() {
        if (this.awtEventListener == null) {
            this.awtEventListener = new AWTEventListener(){

                @Override
                public void eventDispatched(AWTEvent event) {
                    MouseEvent e = (MouseEvent)event;
                    if (ToolbarWithOverflow.this.isVisible() && !ToolbarWithOverflow.this.isShowing() && ToolbarWithOverflow.this.popup.isShowing()) {
                        showingPopup = null;
                        ToolbarWithOverflow.this.popup.setVisible(false);
                        return;
                    }
                    if (event.getSource() == ToolbarWithOverflow.this.popup) {
                        if (ToolbarWithOverflow.this.popup.isShowing() && e.getID() == 505) {
                            int minX = ToolbarWithOverflow.access$200((ToolbarWithOverflow)ToolbarWithOverflow.this).getLocationOnScreen().x;
                            int maxX = ToolbarWithOverflow.access$200((ToolbarWithOverflow)ToolbarWithOverflow.this).getLocationOnScreen().x + ToolbarWithOverflow.this.popup.getWidth();
                            int minY = ToolbarWithOverflow.access$200((ToolbarWithOverflow)ToolbarWithOverflow.this).getLocationOnScreen().y;
                            int maxY = ToolbarWithOverflow.access$200((ToolbarWithOverflow)ToolbarWithOverflow.this).getLocationOnScreen().y + ToolbarWithOverflow.this.popup.getHeight();
                            if (e.getXOnScreen() < minX || e.getXOnScreen() >= maxX || e.getYOnScreen() < minY || e.getYOnScreen() >= maxY) {
                                showingPopup = null;
                                ToolbarWithOverflow.this.popup.setVisible(false);
                            }
                        }
                    } else if (ToolbarWithOverflow.this.popup.isShowing() && ToolbarWithOverflow.this.overflowButton.isShowing() && (e.getID() == 503 || e.getID() == 505)) {
                        int maxY;
                        int minX = ToolbarWithOverflow.access$400((ToolbarWithOverflow)ToolbarWithOverflow.this).getLocationOnScreen().x;
                        int maxX = ToolbarWithOverflow.this.getOrientation() == 0 ? minX + ToolbarWithOverflow.this.popup.getWidth() : minX + ToolbarWithOverflow.this.overflowButton.getWidth() + ToolbarWithOverflow.this.popup.getWidth();
                        int minY = ToolbarWithOverflow.access$400((ToolbarWithOverflow)ToolbarWithOverflow.this).getLocationOnScreen().y;
                        int n = maxY = ToolbarWithOverflow.this.getOrientation() == 0 ? minY + ToolbarWithOverflow.this.overflowButton.getHeight() + ToolbarWithOverflow.this.popup.getHeight() : minY + ToolbarWithOverflow.this.popup.getHeight();
                        if (e.getXOnScreen() < minX || e.getYOnScreen() < minY || e.getXOnScreen() > maxX || e.getYOnScreen() > maxY) {
                            showingPopup = null;
                            ToolbarWithOverflow.this.popup.setVisible(false);
                        }
                    }
                }
            };
        }
        return this.awtEventListener;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        if (!Boolean.TRUE.equals(this.getClientProperty("nb.toolbar.overflow.disable"))) {
            this.addComponentListener(this.getComponentListener());
            Toolkit.getDefaultToolkit().addAWTEventListener(this.getAWTEventListener(), 48);
        }
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        if (this.componentAdapter != null) {
            this.removeComponentListener(this.componentAdapter);
        }
        if (this.awtEventListener != null) {
            Toolkit.getDefaultToolkit().removeAWTEventListener(this.awtEventListener);
        }
    }

    @Override
    public void updateUI() {
        Mutex.EVENT.readAccess(new Runnable(){

            @Override
            public void run() {
                ToolbarWithOverflow.this.superUpdateUI();
            }
        });
    }

    final void superUpdateUI() {
        super.updateUI();
    }

    public boolean isDisplayOverflowOnHover() {
        return this.displayOverflowOnHover;
    }

    public void setDisplayOverflowOnHover(boolean displayOverflowOnHover) {
        this.displayOverflowOnHover = displayOverflowOnHover;
        this.setupOverflowButton();
    }

    @Override
    public Dimension getPreferredSize() {
        Component[] comps = this.getAllComponents();
        Insets insets = this.getInsets();
        int width = null == insets ? 0 : insets.left + insets.right;
        int height = null == insets ? 0 : insets.top + insets.bottom;
        for (int i = 0; i < comps.length; ++i) {
            Component comp = comps[i];
            if (!comp.isVisible()) continue;
            width += this.getOrientation() == 0 ? comp.getPreferredSize().width : comp.getPreferredSize().height;
            height = Math.max(height, this.getOrientation() == 0 ? comp.getPreferredSize().height + (insets == null ? 0 : insets.top + insets.bottom) : comp.getPreferredSize().width + (insets == null ? 0 : insets.left + insets.right));
        }
        if (this.overflowToolbar.getComponentCount() > 0) {
            width += this.getOrientation() == 0 ? this.overflowButton.getPreferredSize().width : this.overflowButton.getPreferredSize().height;
        }
        Dimension dim = this.getOrientation() == 0 ? new Dimension(width, height) : new Dimension(height, width);
        return dim;
    }

    @Override
    public void setOrientation(int o) {
        super.setOrientation(o);
        this.setupOverflowButton();
    }

    @Override
    public void removeAll() {
        super.removeAll();
        this.overflowToolbar.removeAll();
    }

    @Override
    public void validate() {
        if (!Boolean.TRUE.equals(this.getClientProperty("nb.toolbar.overflow.disable"))) {
            int visibleButtons = this.computeVisibleButtons();
            if (visibleButtons == -1) {
                this.handleOverflowRemoval();
            } else {
                this.handleOverflowAddittion(visibleButtons);
            }
        }
        super.validate();
    }

    private void setupOverflowButton() {
        this.overflowButton = new JButton(ImageUtilities.loadImageIcon((String)(this.getOrientation() == 0 ? "org/openide/awt/resources/toolbar_arrow_vertical.png" : "org/openide/awt/resources/toolbar_arrow_horizontal.png"), (boolean)false)){

            @Override
            public void updateUI() {
                Mutex.EVENT.readAccess(new Runnable(){

                    @Override
                    public void run() {
                        4.this.superUpdateUI();
                    }
                });
            }

            private void superUpdateUI() {
                super.updateUI();
            }

        };
        this.overflowButton.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent e) {
                if (ToolbarWithOverflow.this.popup.isShowing()) {
                    showingPopup = null;
                    ToolbarWithOverflow.this.popup.setVisible(false);
                } else {
                    ToolbarWithOverflow.this.displayOverflow();
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                if (showingPopup != null && showingPopup != ToolbarWithOverflow.this.popup) {
                    showingPopup.setVisible(false);
                    showingPopup = null;
                }
                if (ToolbarWithOverflow.this.displayOverflowOnHover) {
                    ToolbarWithOverflow.this.displayOverflow();
                }
            }
        });
    }

    private void displayOverflow() {
        if (!this.overflowButton.isShowing()) {
            return;
        }
        int x = this.getOrientation() == 0 ? this.overflowButton.getLocationOnScreen().x : this.overflowButton.getLocationOnScreen().x + this.overflowButton.getWidth();
        int y = this.getOrientation() == 0 ? this.overflowButton.getLocationOnScreen().y + this.overflowButton.getHeight() : this.overflowButton.getLocationOnScreen().y;
        this.popup.setLocation(x, y);
        showingPopup = this.popup;
        this.popup.setVisible(true);
    }

    private void maybeAddOverflow() {
        this.validate();
        this.repaint();
    }

    private int computeVisibleButtons() {
        if (this.isShowing()) {
            int w = this.getOrientation() == 0 ? this.overflowButton.getIcon().getIconWidth() + 4 : this.getWidth() - this.getInsets().left - this.getInsets().right;
            int h = this.getOrientation() == 0 ? this.getHeight() - this.getInsets().top - this.getInsets().bottom : this.overflowButton.getIcon().getIconHeight() + 4;
            this.overflowButton.setMaximumSize(new Dimension(w, h));
            this.overflowButton.setMinimumSize(new Dimension(w, h));
            this.overflowButton.setPreferredSize(new Dimension(w, h));
        }
        this.handleIconResize();
        Component[] comps = this.getAllComponents();
        int sizeSoFar = 0;
        int maxSize = this.getOrientation() == 0 ? this.getWidth() : this.getHeight();
        int overflowButtonSize = this.getOrientation() == 0 ? this.overflowButton.getPreferredSize().width : this.overflowButton.getPreferredSize().height;
        int showingButtons = 0;
        int visibleButtons = 0;
        Insets insets = this.getInsets();
        if (null != insets) {
            sizeSoFar = this.getOrientation() == 0 ? insets.left + insets.right : insets.top + insets.bottom;
        }
        for (int i = 0; i < comps.length; ++i) {
            Component comp = comps[i];
            if (!comp.isVisible()) continue;
            if (showingButtons == visibleButtons) {
                int size;
                int n = size = this.getOrientation() == 0 ? comp.getPreferredSize().width : comp.getPreferredSize().height;
                if (sizeSoFar + size <= maxSize) {
                    sizeSoFar += size;
                    ++visibleButtons;
                }
            }
            ++showingButtons;
        }
        if (visibleButtons < showingButtons && visibleButtons > 0 && sizeSoFar + overflowButtonSize > maxSize) {
            --visibleButtons;
        }
        if (visibleButtons == 0 && comps.length > 0 && comps[0] instanceof JComponent && Boolean.TRUE.equals(((JComponent)comps[0]).getClientProperty("_toolbar_dragger_"))) {
            visibleButtons = 1;
        }
        if (visibleButtons == showingButtons) {
            visibleButtons = -1;
        }
        return visibleButtons;
    }

    private void handleOverflowAddittion(int visibleButtons) {
        Component[] comps = this.getAllComponents();
        this.removeAll();
        this.overflowToolbar.setOrientation(this.getOrientation() == 0 ? 1 : 0);
        this.popup.removeAll();
        for (Component comp : comps) {
            if (visibleButtons > 0) {
                this.add(comp);
                if (!comp.isVisible()) continue;
                --visibleButtons;
                continue;
            }
            this.overflowToolbar.add(comp);
        }
        this.popup.add(this.overflowToolbar);
        this.add(this.overflowButton);
    }

    private void handleOverflowRemoval() {
        if (this.overflowToolbar.getComponents().length > 0) {
            this.remove(this.overflowButton);
            this.handleIconResize();
            for (Component comp : this.overflowToolbar.getComponents()) {
                this.add(comp);
            }
            this.overflowToolbar.removeAll();
            this.popup.removeAll();
        }
    }

    private void handleIconResize() {
        for (Component comp : this.overflowToolbar.getComponents()) {
            boolean smallToolbarIcons;
            boolean bl = smallToolbarIcons = this.getClientProperty("PreferredIconSize") == null;
            if (smallToolbarIcons) {
                ((JComponent)comp).putClientProperty("PreferredIconSize", null);
                continue;
            }
            ((JComponent)comp).putClientProperty("PreferredIconSize", 24);
        }
    }

    private Component[] getAllComponents() {
        Component[] toolbarComps;
        Component[] overflowComps = this.overflowToolbar.getComponents();
        if (overflowComps.length == 0) {
            toolbarComps = this.getComponents();
        } else if (this.getComponentCount() > 0) {
            toolbarComps = new Component[this.getComponents().length - 1];
            System.arraycopy(this.getComponents(), 0, toolbarComps, 0, toolbarComps.length);
        } else {
            toolbarComps = new Component[]{};
        }
        Component[] comps = new Component[toolbarComps.length + overflowComps.length];
        System.arraycopy(toolbarComps, 0, comps, 0, toolbarComps.length);
        System.arraycopy(overflowComps, 0, comps, toolbarComps.length, overflowComps.length);
        return comps;
    }

    private static class SafePopupMenu
    extends JPopupMenu {
        private SafePopupMenu() {
        }

        @Override
        public void updateUI() {
            Mutex.EVENT.readAccess(new Runnable(){

                @Override
                public void run() {
                    SafePopupMenu.this.superUpdateUI();
                }
            });
        }

        final void superUpdateUI() {
            super.updateUI();
        }

    }

    private static class SafeToolBar
    extends JToolBar {
        public SafeToolBar(String name, int orientation) {
            super(name, orientation);
        }

        @Override
        public void updateUI() {
            Mutex.EVENT.readAccess(new Runnable(){

                @Override
                public void run() {
                    SafeToolBar.this.superUpdateUI();
                }
            });
        }

        final void superUpdateUI() {
            super.updateUI();
        }

    }

}

