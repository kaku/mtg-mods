/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Parameters
 */
package org.openide.awt;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import javax.swing.Icon;
import javax.swing.UIManager;
import org.openide.util.ImageUtilities;
import org.openide.util.Parameters;

class IconWithArrow
implements Icon {
    private static final String ARROW_IMAGE_NAME = "org/openide/awt/resources/arrow.png";
    private Icon orig;
    private Icon arrow = ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/arrow.png", (boolean)false);
    private boolean paintRollOver;
    private static final int GAP = 6;

    public IconWithArrow(Icon orig, boolean paintRollOver) {
        Parameters.notNull((CharSequence)"original icon", (Object)orig);
        this.orig = orig;
        this.paintRollOver = paintRollOver;
    }

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        int height = this.getIconHeight();
        this.orig.paintIcon(c, g, x, y + (height - this.orig.getIconHeight()) / 2);
        this.arrow.paintIcon(c, g, x + 6 + this.orig.getIconWidth(), y + (height - this.arrow.getIconHeight()) / 2);
        if (this.paintRollOver) {
            Color brighter = UIManager.getColor("controlHighlight");
            Color darker = UIManager.getColor("controlShadow");
            if (null == brighter || null == darker) {
                brighter = c.getBackground().brighter();
                darker = c.getBackground().darker();
            }
            if (null != brighter && null != darker) {
                g.setColor(darker);
                g.drawLine(x + this.orig.getIconWidth() + 2, y, x + this.orig.getIconWidth() + 2, y + this.getIconHeight());
            }
        }
    }

    @Override
    public int getIconWidth() {
        return this.orig.getIconWidth() + 6 + this.arrow.getIconWidth();
    }

    @Override
    public int getIconHeight() {
        return Math.max(this.orig.getIconHeight(), this.arrow.getIconHeight());
    }

    public static int getArrowAreaWidth() {
        return 8;
    }
}

