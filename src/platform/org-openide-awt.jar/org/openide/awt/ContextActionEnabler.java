/*
 * Decompiled with CFR 0_118.
 */
package org.openide.awt;

import java.util.List;

interface ContextActionEnabler<T> {
    public boolean enabled(List<? extends T> var1);
}

