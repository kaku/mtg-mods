/*
 * Decompiled with CFR 0_118.
 */
package org.openide.awt;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.plaf.UIResource;
import org.openide.awt.ColorIcon;
import org.openide.awt.ColorValue;

class ColorComboBoxRendererWrapper
implements ListCellRenderer,
UIResource {
    private final ListCellRenderer renderer;
    private static final boolean isGTK = "GTK".equals(UIManager.getLookAndFeel().getID());

    ColorComboBoxRendererWrapper(JComboBox comboBox) {
        this.renderer = comboBox.getRenderer();
        if (this.renderer instanceof ColorComboBoxRendererWrapper) {
            throw new IllegalStateException("Custom renderer is already initialized.");
        }
        comboBox.setRenderer(this);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        Component res = this.renderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        if (res instanceof JLabel) {
            ListCellRenderer listCellRenderer = this.renderer;
            synchronized (listCellRenderer) {
                JLabel label = (JLabel)res;
                int height = isGTK ? 10 : Math.max(res.getPreferredSize().height - 4, 4);
                ColorIcon icon = null;
                if (value instanceof ColorValue) {
                    ColorValue color = (ColorValue)value;
                    icon = value == ColorValue.CUSTOM_COLOR ? null : new ColorIcon(color.color, height);
                    label.setText(color.text);
                } else {
                    icon = null;
                }
                label.setIcon(icon);
            }
        }
        return res;
    }
}

