/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 */
package org.openide.awt;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Map;
import org.openide.awt.Actions;
import org.openide.awt.ContextAction;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

final class InjectorAny
extends ContextAction.Performer<Object> {
    public InjectorAny(Map fo) {
        super(fo);
    }

    @Override
    public void actionPerformed(ActionEvent ev, List<? extends Object> data, Lookup.Provider everything) {
        String clazz = (String)this.delegate.get("injectable");
        ClassLoader l = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        if (l == null) {
            l = Thread.currentThread().getContextClassLoader();
        }
        if (l == null) {
            l = Actions.class.getClassLoader();
        }
        try {
            Class clazzC = Class.forName(clazz, true, l);
            Constructor c = clazzC.getConstructor(List.class);
            ActionListener action = (ActionListener)c.newInstance(data);
            action.actionPerformed(ev);
        }
        catch (Exception ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }
}

