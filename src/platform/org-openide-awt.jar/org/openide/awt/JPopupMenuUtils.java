/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 */
package org.openide.awt;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;

public class JPopupMenuUtils {
    private static boolean problemTested = false;
    private static boolean problem = false;
    private static RequestProcessor reqProc;
    private static RequestProcessor.Task task;

    public static void dynamicChange(JPopupMenu popup, boolean usedToBeContained) {
        if (!popup.isShowing()) {
            return;
        }
        if (JPopupMenuUtils.isProblemConfig()) {
            JPopupMenuUtils.callRefreshLater(popup);
            return;
        }
        JPopupMenuUtils.refreshPopup(popup);
        Point p = popup.getLocationOnScreen();
        Point newPt = JPopupMenuUtils.getPopupMenuOrigin(popup, p);
        boolean willBeContained = JPopupMenuUtils.willPopupBeContained(popup, newPt);
        if (usedToBeContained != willBeContained) {
            popup.setVisible(false);
        }
        if (!newPt.equals(p)) {
            // empty if block
        }
        if (usedToBeContained != willBeContained) {
            popup.setVisible(true);
        }
    }

    private static void refreshPopup(JPopupMenu popup) {
        popup.pack();
        popup.invalidate();
        Container c = popup.getParent();
        if (c != null) {
            c.validate();
        }
    }

    private static void callRefreshLater(final JPopupMenu popup) {
        if (reqProc == null) {
            reqProc = new RequestProcessor();
        }
        if (task == null) {
            task = reqProc.create(new Runnable(){

                @Override
                public void run() {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            task = null;
                            if (!popup.isShowing()) {
                                return;
                            }
                            Point p = popup.getLocationOnScreen();
                            Point newPt = JPopupMenuUtils.getPopupMenuOrigin(popup, p);
                            popup.setVisible(false);
                            JPopupMenuUtils.refreshPopup(popup);
                            if (!newPt.equals(p)) {
                                // empty if block
                            }
                            popup.setVisible(true);
                        }
                    });
                }

            });
        }
        task.schedule(100);
    }

    private static boolean isProblemConfig() {
        if (problemTested) {
            return problem;
        }
        problem = false;
        String needHack = System.getProperty("netbeans.popup.linuxhack");
        if (needHack != null) {
            problem = true;
        }
        return problem;
    }

    public static void dynamicChangeToSubmenu(JPopupMenu popup, boolean usedToBeContained) {
        Component invoker = popup.getInvoker();
        if (!(invoker instanceof JMenu)) {
            return;
        }
        JMenu menu = (JMenu)invoker;
        if (!popup.isShowing()) {
            return;
        }
        if (JPopupMenuUtils.isProblemConfig()) {
            JPopupMenuUtils.callRefreshLater2(popup, menu);
            return;
        }
        JPopupMenuUtils.refreshPopup(popup);
        Point p = popup.getLocationOnScreen();
        Dimension popupSize = popup.getPreferredSize();
        Rectangle popupRect = new Rectangle(p, popupSize);
        Rectangle screenRect = JPopupMenuUtils.getScreenRect();
        boolean willBeContained = JPopupMenuUtils.isPopupContained(popup);
        if (!screenRect.contains(popupRect)) {
            menu.setPopupMenuVisible(false);
            menu.setPopupMenuVisible(true);
        } else if (usedToBeContained != willBeContained) {
            popup.setVisible(false);
            popup.setVisible(true);
        }
    }

    private static void callRefreshLater2(final JPopupMenu popup, final JMenu menu) {
        if (reqProc == null) {
            reqProc = new RequestProcessor();
        }
        if (task == null) {
            task = reqProc.create(new Runnable(){

                @Override
                public void run() {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            task = null;
                            if (!popup.isShowing()) {
                                return;
                            }
                            popup.setVisible(false);
                            JPopupMenuUtils.refreshPopup(popup);
                            popup.setVisible(true);
                            Point p = popup.getLocationOnScreen();
                            Dimension popupSize = popup.getPreferredSize();
                            Rectangle popupRect = new Rectangle(p, popupSize);
                            Rectangle screenRect = JPopupMenuUtils.getScreenRect();
                            if (!screenRect.contains(popupRect)) {
                                menu.setPopupMenuVisible(false);
                                menu.setPopupMenuVisible(true);
                            }
                        }
                    });
                }

            });
        }
        task.schedule(100);
    }

    static Point getPopupMenuOrigin(JPopupMenu popup, Point p) {
        Point newPt = new Point(p);
        Dimension popupSize = popup.getPreferredSize();
        Rectangle screenRect = JPopupMenuUtils.getScreenRect();
        int popupRight = newPt.x + popupSize.width;
        int popupBottom = newPt.y + popupSize.height;
        int screenRight = screenRect.x + screenRect.width;
        int screenBottom = screenRect.y + screenRect.height;
        if (popupRight > screenRight) {
            newPt.x = screenRight - popupSize.width;
        }
        if (newPt.x < screenRect.x) {
            newPt.x = screenRect.x;
        }
        if (popupBottom > screenBottom) {
            newPt.y = screenBottom - popupSize.height;
        }
        if (newPt.y < screenRect.y) {
            newPt.y = screenRect.y;
        }
        return newPt;
    }

    public static boolean isPopupContained(JPopupMenu popup) {
        if (!popup.isShowing()) {
            return false;
        }
        return JPopupMenuUtils.willPopupBeContained(popup, popup.getLocationOnScreen());
    }

    private static boolean willPopupBeContained(JPopupMenu popup, Point origin) {
        if (!popup.isShowing()) {
            return false;
        }
        Window w = SwingUtilities.windowForComponent(popup.getInvoker());
        Rectangle r = new Rectangle(origin, popup.getSize());
        return w != null && w.getBounds().contains(r);
    }

    public static Rectangle getScreenRect() {
        return Utilities.getUsableScreenBounds();
    }

}

