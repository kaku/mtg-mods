/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.sendopts;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.sendopts.CommandException;
import org.netbeans.api.sendopts.CommandLine;
import org.netbeans.modules.sendopts.DefaultProcessor;
import org.netbeans.modules.sendopts.OptionImpl;
import org.netbeans.spi.sendopts.Env;
import org.netbeans.spi.sendopts.OptionProcessor;

public final class Option {
    private final int shortName;
    private final String longName;
    final OptionImpl impl;
    private final String[] keys;
    private final String[] bundles;
    public static final char NO_SHORT_NAME = '\uffff';
    private static String[] EMPTY = new String[2];

    private Option(char shortName, String longName, int type) {
        this.shortName = shortName == '\uffff' ? -1 : (int)shortName;
        this.longName = longName;
        switch (type) {
            case 0: {
                this.impl = OptionImpl.createNoArg(this);
                break;
            }
            case 1: {
                this.impl = OptionImpl.createOneArg(this, false);
                break;
            }
            case 2: {
                this.impl = OptionImpl.createOneArg(this, true);
                break;
            }
            case 3: {
                this.impl = OptionImpl.createAdd(this, false);
                break;
            }
            case 4: {
                this.impl = OptionImpl.createAdd(this, true);
                break;
            }
            case 5: {
                this.impl = OptionImpl.createAlways(this);
                break;
            }
            default: {
                throw new IllegalArgumentException("Type: " + type);
            }
        }
        this.keys = EMPTY;
        this.bundles = EMPTY;
    }

    Option(int type, Option[] arr) {
        this.shortName = -1;
        this.longName = null;
        this.impl = OptionImpl.create(this, type, Arrays.asList(arr));
        this.keys = EMPTY;
        this.bundles = EMPTY;
    }

    private Option(Option old, int typeOfDescription, String bundle, String description) {
        this.shortName = old.shortName;
        this.longName = old.longName;
        this.impl = OptionImpl.cloneImpl(old.impl, this, null);
        this.keys = (String[])old.keys.clone();
        this.bundles = (String[])old.bundles.clone();
        this.keys[typeOfDescription] = description;
        this.bundles[typeOfDescription] = bundle;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(this)));
        sb.append('[');
        sb.append(this.shortName);
        sb.append(',');
        sb.append(this.longName);
        sb.append(',');
        this.impl.append(sb);
        sb.append(']');
        return sb.toString();
    }

    public boolean equals(Object o) {
        if (o instanceof Option) {
            Option option = (Option)o;
            return this.impl.root == option.impl.root;
        }
        return false;
    }

    public int hashCode() {
        return System.identityHashCode(this.impl.root);
    }

    public static Option withoutArgument(char shortName, String longName) {
        return new Option(shortName, longName, 0);
    }

    public static Option optionalArgument(char shortName, String longName) {
        return new Option(shortName, longName, 1);
    }

    public static Option requiredArgument(char shortName, String longName) {
        return new Option(shortName, longName, 2);
    }

    public static Option additionalArguments(char shortName, String longName) {
        return new Option(shortName, longName, 3);
    }

    public static Option defaultArguments() {
        return new Option('\uffff', null, 4);
    }

    public static Option always() {
        return new Option('\uffff', null, 5);
    }

    public static Option displayName(Option option, String bundleName, String key) {
        return new Option(option, 0, bundleName, key);
    }

    public static Option shortDescription(Option option, String bundleName, String key) {
        return new Option(option, 1, bundleName, key);
    }

    static {
        OptionImpl.Trampoline.DEFAULT = new OptionImpl.Trampoline(){

            @Override
            public OptionImpl impl(Option o) {
                return o.impl;
            }

            @Override
            public Env create(CommandLine cmd, InputStream is, OutputStream os, OutputStream err, File currentDir) {
                return new Env(cmd, is, os, err, currentDir);
            }

            @Override
            public void usage(PrintWriter w, Option o, int max) {
                if (o.keys[1] != null) {
                    w.print(this.key(o.bundles[1], o.keys[1], Locale.getDefault()));
                }
            }

            @Override
            public Option[] getOptions(OptionProcessor p) {
                return p.getOptions().toArray(new Option[0]);
            }

            @Override
            public void process(OptionProcessor provider, Env env, Map<Option, String[]> options) throws CommandException {
                provider.process(env, Collections.unmodifiableMap(options));
            }

            @Override
            public String getLongName(Option o) {
                return o.longName;
            }

            @Override
            public int getShortName(Option o) {
                return o.shortName;
            }

            @Override
            public String getDisplayName(Option o, Locale l) {
                return this.key(o.bundles[0], o.keys[0], l);
            }

            private String key(String bundle, String key, Locale l) {
                if (key == null) {
                    return null;
                }
                if (bundle == OptionImpl.NO_BUNDLE) {
                    return key;
                }
                ClassLoader loader = DefaultProcessor.findClassLoader();
                try {
                    ResourceBundle b = ResourceBundle.getBundle(bundle, l, loader);
                    return b.getString(key);
                }
                catch (MissingResourceException ex) {
                    OptionImpl.LOG.log(Level.WARNING, null, ex);
                    return key;
                }
            }
        };
    }

}

