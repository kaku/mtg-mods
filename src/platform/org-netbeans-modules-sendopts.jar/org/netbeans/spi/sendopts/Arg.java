/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.sendopts;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(value=RetentionPolicy.RUNTIME)
@Target(value={ElementType.FIELD})
public @interface Arg {
    public char shortName() default 65535;

    public String longName();

    public boolean implicit() default 0;

    public String defaultValue() default "\u0000";
}

