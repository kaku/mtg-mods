/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.sendopts;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value={ElementType.FIELD})
@Retention(value=RetentionPolicy.RUNTIME)
public @interface Description {
    public String displayName() default "";

    public String shortDescription();
}

