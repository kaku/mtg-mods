/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.sendopts;

import org.netbeans.api.sendopts.CommandException;
import org.netbeans.spi.sendopts.Env;

public interface ArgsProcessor {
    public void process(Env var1) throws CommandException;
}

