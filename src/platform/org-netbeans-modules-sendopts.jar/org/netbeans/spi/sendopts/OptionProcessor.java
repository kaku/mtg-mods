/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.sendopts;

import java.util.Map;
import java.util.Set;
import org.netbeans.api.sendopts.CommandException;
import org.netbeans.spi.sendopts.Env;
import org.netbeans.spi.sendopts.Option;

public abstract class OptionProcessor {
    protected OptionProcessor() {
    }

    protected abstract Set<Option> getOptions();

    protected abstract void process(Env var1, Map<Option, String[]> var2) throws CommandException;
}

