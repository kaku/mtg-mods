/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.sendopts;

import org.netbeans.spi.sendopts.Option;

public final class OptionGroups {
    private OptionGroups() {
    }

    public static /* varargs */ Option someOf(Option ... options) {
        return new Option(2, options);
    }

    public static /* varargs */ Option anyOf(Option ... options) {
        return new Option(3, options);
    }

    public static /* varargs */ Option oneOf(Option ... options) {
        return new Option(0, options);
    }

    public static /* varargs */ Option allOf(Option ... options) {
        return new Option(1, options);
    }
}

