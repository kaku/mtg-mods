/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.sendopts;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import org.netbeans.api.sendopts.CommandLine;

public final class Env {
    private final InputStream is;
    private final PrintStream os;
    private final PrintStream err;
    private final File currentDir;
    private final CommandLine cmd;

    Env(CommandLine cmd, InputStream is, OutputStream os, OutputStream err, File currentDir) {
        this.is = is;
        this.os = os instanceof PrintStream ? (PrintStream)os : new PrintStream(os);
        this.err = err instanceof PrintStream ? (PrintStream)err : new PrintStream(err);
        this.currentDir = currentDir;
        this.cmd = cmd;
    }

    public PrintStream getOutputStream() {
        return this.os;
    }

    public PrintStream getErrorStream() {
        return this.err;
    }

    public File getCurrentDirectory() {
        return this.currentDir;
    }

    public InputStream getInputStream() {
        return this.is;
    }

    public void usage() {
        PrintWriter pw = new PrintWriter(this.getOutputStream());
        this.cmd.usage(pw);
        pw.flush();
    }
}

