/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.api.sendopts;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.ServiceLoader;
import java.util.Set;
import org.netbeans.api.sendopts.CommandException;
import org.netbeans.modules.sendopts.DefaultProcessor;
import org.netbeans.modules.sendopts.OptionImpl;
import org.netbeans.spi.sendopts.Env;
import org.netbeans.spi.sendopts.Option;
import org.netbeans.spi.sendopts.OptionProcessor;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public final class CommandLine {
    private static final int ERROR_BASE = 50345;
    private final Collection<? extends OptionProcessor> processors;

    CommandLine(Collection<? extends OptionProcessor> p) {
        this.processors = p;
    }

    public static CommandLine getDefault() {
        return new CommandLine(null);
    }

    public static /* varargs */ CommandLine create(Class<?> ... classes) {
        ArrayList<OptionProcessor> arr = new ArrayList<OptionProcessor>();
        for (Class c : classes) {
            arr.add(DefaultProcessor.create(c));
        }
        return new CommandLine(arr);
    }

    public /* varargs */ void process(String ... args) throws CommandException {
        this.process(args, (InputStream)null, (OutputStream)null, (OutputStream)null, (File)null);
    }

    public void process(String[] args, InputStream is, OutputStream os, OutputStream err, File currentDir) throws CommandException {
        int i;
        int i2;
        if (is == null) {
            is = System.in;
        }
        if (os == null) {
            os = System.out;
        }
        if (err == null) {
            err = System.err;
        }
        if (currentDir == null) {
            currentDir = new File(System.getProperty("user.dir"));
        }
        Env env = OptionImpl.Trampoline.DEFAULT.create(this, is, os, err, currentDir);
        ArrayList<String> additionalParams = new ArrayList<String>();
        ArrayList<OptionImpl> opts = new ArrayList<OptionImpl>();
        OptionImpl acceptsAdons = null;
        OptionImpl[] mainOptions = this.getOptions();
        LinkedHashSet<OptionImpl> allOptions = new LinkedHashSet<OptionImpl>();
        for (int i3 = 0; i3 < mainOptions.length; ++i3) {
            mainOptions[i3] = mainOptions[i3].addWorkingCopy(allOptions);
        }
        OptionImpl[] arr = allOptions.toArray(new OptionImpl[0]);
        boolean optionMode = true;
        for (i = 0; i < args.length; ++i) {
            if (args[i] == null) continue;
            if (optionMode) {
                if (args[i].startsWith("--")) {
                    OptionImpl opt;
                    if (args[i].length() == 2) {
                        optionMode = false;
                        continue;
                    }
                    String text = args[i].substring(2);
                    String value = null;
                    int textEqual = text.indexOf(61);
                    if (textEqual >= 0) {
                        value = text.substring(textEqual + 1);
                        text = text.substring(0, textEqual);
                    }
                    if ((opt = this.findByLongName(text, arr)) == null) {
                        throw new CommandException(args[i], 50346, CommandLine.getMessage("MSG_Unknown", args[i]));
                    }
                    if (opt.getArgumentType() == 1 && value == null) {
                        do {
                            if (++i == args.length) {
                                throw new CommandException(CommandLine.getMessage("MSG_MissingArgument", "--" + opt.getLongName()), 50347);
                            }
                            if (!args[i].equals("--")) break;
                            optionMode = false;
                        } while (true);
                        if (optionMode && args[i].startsWith("-")) {
                            throw new CommandException(CommandLine.getMessage("MSG_MissingArgument", "--" + opt.getLongName()), 50347);
                        }
                        value = args[i];
                    }
                    if (value != null) {
                        if (opt.getArgumentType() != 1 && opt.getArgumentType() != 2) {
                            throw new CommandException(CommandLine.getMessage("MSG_OPTION_CANNOT_HAVE_VALUE", opt, value), 50347);
                        }
                        opt.associateValue(value);
                    }
                    if (opt.getArgumentType() == 3) {
                        if (acceptsAdons != null) {
                            String oName1 = CommandLine.findOptionName(acceptsAdons, args);
                            String oName2 = CommandLine.findOptionName(opt, args);
                            String msg = CommandLine.getMessage("MSG_CannotTogether", oName1, oName2);
                            throw new CommandException(msg, 50348);
                        }
                        acceptsAdons = opt;
                    }
                    opts.add(opt);
                    continue;
                }
                if (args[i].startsWith("-") && args[i].length() > 1) {
                    for (int j = 1; j < args[i].length(); ++j) {
                        char ch = args[i].charAt(j);
                        OptionImpl opt = this.findByShortName(ch, arr);
                        if (opt == null) {
                            throw new CommandException(CommandLine.getMessage("MSG_UNKNOWN_OPTION", args[i]), 50346);
                        }
                        if (args[i].length() == j + 1 && opt.getArgumentType() == 1) {
                            throw new CommandException(CommandLine.getMessage("MSG_MissingArgument", args[i]), 50347);
                        }
                        if (args[i].length() > j && (opt.getArgumentType() == 1 || opt.getArgumentType() == 2)) {
                            opt.associateValue(args[i].substring(j + 1));
                            j = args[i].length();
                        }
                        if (opt.getArgumentType() == 3) {
                            if (acceptsAdons != null) {
                                String oName1 = CommandLine.findOptionName(acceptsAdons, args);
                                String oName2 = CommandLine.findOptionName(opt, args);
                                String msg = CommandLine.getMessage("MSG_CannotTogether", oName1, oName2);
                                throw new CommandException(msg, 50348);
                            }
                            acceptsAdons = opt;
                        }
                        opts.add(opt);
                    }
                    continue;
                }
            }
            additionalParams.add(args[i]);
        }
        if (acceptsAdons == null && !additionalParams.isEmpty()) {
            for (i = 0; i < arr.length; ++i) {
                if (arr[i].getArgumentType() != 4) continue;
                if (acceptsAdons != null) {
                    throw new CommandException(CommandLine.getMessage("MSG_TWO_DEFAULT_OPTIONS", acceptsAdons, arr[i]), 50348);
                }
                acceptsAdons = arr[i];
                opts.add(acceptsAdons);
            }
            if (acceptsAdons == null) {
                throw new CommandException(CommandLine.getMessage("MSG_NOT_PROCESSED_PARAMS", additionalParams), 50347);
            }
        }
        OptionImpl.Appearance[] postProcess = new OptionImpl.Appearance[mainOptions.length];
        HashSet used = new HashSet<OptionImpl>(opts);
        for (i2 = 0; i2 < mainOptions.length; ++i2) {
            OptionImpl.Appearance res;
            postProcess[i2] = res = mainOptions[i2].checkConsistent(used);
            if (!res.isThere()) continue;
            mainOptions[i2].markConsistent(res);
        }
        used = new HashSet(opts);
        for (i2 = 0; i2 < mainOptions.length; ++i2) {
            OptionImpl error;
            if (!postProcess[i2].isError() || (error = mainOptions[i2].findNotUsedOption(used)) == null) continue;
            throw new CommandException(postProcess[i2].errorMessage(args), 50349);
        }
        LinkedHashMap<OptionProcessor, HashMap<Option, String[]>> providers = new LinkedHashMap<OptionProcessor, HashMap<Option, String[]>>();
        for (i2 = 0; i2 < mainOptions.length; ++i2) {
            if (!postProcess[i2].isThere()) continue;
            HashMap<Option, String[]> param = (HashMap<Option, String[]>)providers.get(mainOptions[i2].getProvider());
            if (param == null) {
                param = new HashMap<Option, String[]>();
                providers.put(mainOptions[i2].getProvider(), param);
            }
            mainOptions[i2].process(additionalParams.toArray(new String[0]), param);
        }
        for (Map.Entry pair : providers.entrySet()) {
            OptionImpl.Trampoline.DEFAULT.process((OptionProcessor)pair.getKey(), env, (Map)pair.getValue());
        }
    }

    public void usage(PrintWriter w) {
        int i;
        OptionImpl[] mainOptions = this.getOptions();
        LinkedHashSet<OptionImpl> allOptions = new LinkedHashSet<OptionImpl>();
        for (int i2 = 0; i2 < mainOptions.length; ++i2) {
            mainOptions[i2].addWorkingCopy(allOptions);
        }
        OptionImpl[] arr = allOptions.toArray(new OptionImpl[0]);
        int max = 25;
        String[] prefixes = new String[arr.length];
        for (i = 0; i < arr.length; ++i) {
            StringBuilder sb = new StringBuilder();
            String ownDisplay = OptionImpl.Trampoline.DEFAULT.getDisplayName(arr[i].getOption(), Locale.getDefault());
            if (ownDisplay != null) {
                sb.append(ownDisplay);
            } else {
                String sep = "";
                if (arr[i].getShortName() != -1) {
                    sb.append('-');
                    sb.append((char)arr[i].getShortName());
                    sep = ", ";
                }
                if (arr[i].getLongName() != null) {
                    sb.append(sep);
                    sb.append("--");
                    sb.append(arr[i].getLongName());
                } else if (sep.length() == 0) continue;
                switch (arr[i].getArgumentType()) {
                    case 0: {
                        break;
                    }
                    case 1: {
                        sb.append(' ');
                        sb.append(CommandLine.getMessage("MSG_OneArg", new Object[0]));
                        break;
                    }
                    case 2: {
                        sb.append(' ');
                        sb.append(CommandLine.getMessage("MSG_OptionalArg", new Object[0]));
                        break;
                    }
                    case 3: {
                        sb.append(' ');
                        sb.append(CommandLine.getMessage("MSG_AddionalArgs", new Object[0]));
                        break;
                    }
                    default: {
                        assert (false);
                        break;
                    }
                }
            }
            if (sb.length() > max) {
                max = sb.length();
            }
            prefixes[i] = sb.toString();
        }
        for (i = 0; i < arr.length; ++i) {
            if (prefixes[i] == null) continue;
            w.print("  ");
            w.print(prefixes[i]);
            for (int j = prefixes[i].length(); j < max; ++j) {
                w.print(' ');
            }
            w.print(' ');
            arr[i].usage(w, max);
            w.println();
        }
        w.flush();
    }

    private OptionImpl[] getOptions() {
        ArrayList<OptionImpl> arr = new ArrayList<OptionImpl>();
        Iterable proc = this.processors;
        if (proc == null) {
            try {
                proc = this.lookupOptionProcessors();
            }
            catch (LinkageError ex) {
                proc = ServiceLoader.load(OptionProcessor.class);
            }
        }
        for (OptionProcessor p : proc) {
            Option[] all = OptionImpl.Trampoline.DEFAULT.getOptions(p);
            for (int i = 0; i < all.length; ++i) {
                arr.add(OptionImpl.cloneImpl(OptionImpl.find(all[i]), all[i], p));
            }
        }
        return arr.toArray(new OptionImpl[0]);
    }

    private Collection<? extends OptionProcessor> lookupOptionProcessors() {
        return Lookup.getDefault().lookupAll(OptionProcessor.class);
    }

    private OptionImpl findByLongName(String lng, OptionImpl[] arr) {
        boolean abbrev = false;
        OptionImpl best = null;
        for (int i = 0; i < arr.length; ++i) {
            String on = arr[i].getLongName();
            if (on == null) continue;
            if (lng.equals(on)) {
                return arr[i];
            }
            if (!on.startsWith(lng)) continue;
            abbrev = best == null;
            best = arr[i];
        }
        return abbrev ? best : null;
    }

    private OptionImpl findByShortName(char ch, OptionImpl[] arr) {
        for (int i = 0; i < arr.length; ++i) {
            if (ch != arr[i].getShortName()) continue;
            return arr[i];
        }
        return null;
    }

    private static String findOptionName(OptionImpl opt, String[] args) {
        for (int i = 0; i < args.length; ++i) {
            if (!args[i].startsWith("-")) continue;
            if (args[i].startsWith("--")) {
                String text = args[i].substring(2);
                int textEqual = text.indexOf(61);
                if (textEqual >= 0) {
                    text = text.substring(0, textEqual);
                }
                if (!text.startsWith(opt.getLongName())) continue;
                return args[i];
            }
            if (opt.getShortName() != args[i].charAt(1)) continue;
            return "-" + (char)opt.getShortName();
        }
        return opt.toString();
    }

    private static /* varargs */ String getMessage(String msg, Object ... args) {
        Class<CommandException> c = CommandException.class;
        try {
            return NbBundle.getMessage(c, (String)msg, (Object[])args);
        }
        catch (LinkageError ex) {
            ResourceBundle b = ResourceBundle.getBundle(c.getPackage().getName() + ".Bundle");
            String res = b.getString(msg);
            if (args != null && args.length > 0) {
                res = MessageFormat.format(res, args);
            }
            return res;
        }
    }
}

