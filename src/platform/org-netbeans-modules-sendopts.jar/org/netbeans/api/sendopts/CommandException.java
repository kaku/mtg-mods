/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.sendopts;

public final class CommandException
extends Exception {
    private final int exitCode;
    private final String locMsg;

    public CommandException(int exitCode) {
        this("Error code: " + exitCode, exitCode, null);
    }

    public CommandException(int exitCode, String locMsg) {
        this("Error code: " + exitCode, exitCode, locMsg);
    }

    CommandException(String msg, int exitCode, String locMsg) {
        super(msg);
        this.exitCode = exitCode;
        this.locMsg = locMsg;
    }

    CommandException(String msg, int exitCode) {
        this(msg, exitCode, null);
    }

    public int getExitCode() {
        return this.exitCode;
    }

    @Override
    public String getLocalizedMessage() {
        if (this.locMsg != null) {
            return this.locMsg;
        }
        if (this.getCause() != null) {
            return this.getCause().getLocalizedMessage();
        }
        return super.getLocalizedMessage();
    }
}

