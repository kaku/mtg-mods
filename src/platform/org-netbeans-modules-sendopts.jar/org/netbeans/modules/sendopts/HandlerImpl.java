/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.sendopts;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import org.netbeans.api.sendopts.CommandException;
import org.netbeans.api.sendopts.CommandLine;
import org.openide.util.NbBundle;

final class HandlerImpl {
    HandlerImpl() {
    }

    static int execute(String[] arr, InputStream is, OutputStream os, OutputStream err, File pwd) {
        try {
            CommandLine.getDefault().process(arr, is, os, err, pwd);
            for (int i = 0; i < arr.length; ++i) {
                arr[i] = null;
            }
            return 0;
        }
        catch (CommandException ex) {
            PrintStream ps = new PrintStream(err);
            ps.println(ex.getLocalizedMessage());
            int ret = ex.getExitCode();
            if (ret == 0) {
                ret = Integer.MIN_VALUE;
            }
            return ret;
        }
    }

    static void usage(PrintWriter w) {
        w.print(NbBundle.getMessage(HandlerImpl.class, (String)"MSG_OptionsHeader"));
        CommandLine.getDefault().usage(w);
        w.println();
    }
}

