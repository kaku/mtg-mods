/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.sendopts;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import org.netbeans.api.sendopts.CommandException;
import org.netbeans.api.sendopts.CommandLine;
import org.netbeans.spi.sendopts.Env;
import org.netbeans.spi.sendopts.Option;
import org.netbeans.spi.sendopts.OptionProcessor;
import org.openide.util.NbBundle;

public abstract class OptionImpl
implements Cloneable {
    public static final Logger LOG = Logger.getLogger(OptionImpl.class.getName());
    public static final Object NO_DEFAULT = new Object();
    public static final String NO_BUNDLE = new String();
    static final String[] NO_VALUE = new String[0];
    public final Option root;
    Option option;
    final int argumentType;
    OptionProcessor provider;
    private Appearance appear;

    OptionImpl(Option o, OptionProcessor p, int a) {
        this.root = o;
        this.option = o;
        this.provider = p;
        this.argumentType = a;
    }

    public int getShortName() {
        return Trampoline.DEFAULT.getShortName(this.option);
    }

    public String getLongName() {
        return Trampoline.DEFAULT.getLongName(this.option);
    }

    public int getArgumentType() {
        return this.argumentType;
    }

    public int hashCode() {
        return this.option.hashCode();
    }

    public boolean equals(Object obj) {
        if (obj instanceof OptionImpl) {
            return this.option.equals(((OptionImpl)obj).option);
        }
        return false;
    }

    public final void append(StringBuffer sb) {
        String c = this.getClass().getName();
        int i = c.indexOf(36);
        assert (i >= 0);
        sb.append(c.substring(i + 1));
        this.appendInternals(sb);
    }

    void appendInternals(StringBuffer sb) {
    }

    public String toString() {
        return "Impl:" + this.option;
    }

    public String findName(boolean used, String[] args) {
        if (used) {
            for (int i = 0; i < args.length; ++i) {
                if (args[i].startsWith("-" + (char)this.getShortName())) {
                    return "-" + (char)this.getShortName();
                }
                if (!args[i].startsWith("--" + this.getLongName())) continue;
                return "--" + this.getLongName();
            }
        } else {
            if (this.getLongName() != null) {
                return "--" + this.getLongName();
            }
            if (this.getShortName() != -1) {
                return "-" + (char)this.getShortName();
            }
        }
        return null;
    }

    public Option getOption() {
        return this.option;
    }

    public OptionProcessor getProvider() {
        return this.provider;
    }

    private static <T extends OptionImpl> T doClone(T impl) {
        try {
            return (T)((OptionImpl)impl.clone());
        }
        catch (CloneNotSupportedException ex) {
            assert (false);
            return null;
        }
    }

    public static OptionImpl cloneImpl(OptionImpl proto, Option option, OptionProcessor prov) {
        OptionImpl n;
        try {
            n = (OptionImpl)proto.clone();
        }
        catch (CloneNotSupportedException ex) {
            assert (false);
            return null;
        }
        n.option = option;
        if (prov != null) {
            n.provider = prov;
        }
        return n;
    }

    public final OptionImpl addWorkingCopy(Collection<OptionImpl> allOptions) {
        if (allOptions.contains(this)) {
            OptionImpl elem;
            Iterator<OptionImpl> it = allOptions.iterator();
            while (!(elem = it.next()).equals(this)) {
            }
            return elem;
        }
        return this.handleAdd(allOptions);
    }

    protected abstract OptionImpl handleAdd(Collection<OptionImpl> var1);

    public abstract Appearance checkConsistent(Set<OptionImpl> var1);

    public void markConsistent(Appearance a) {
        assert (this.appear == null || this.appear == a);
        this.appear = a;
    }

    public abstract void process(String[] var1, Map<Option, String[]> var2) throws CommandException;

    public abstract void associateValue(String var1) throws CommandException;

    public void usage(PrintWriter w, int max) {
        Trampoline.DEFAULT.usage(w, this.option, max);
    }

    public OptionImpl findNotUsedOption(Set<OptionImpl> used) {
        return this.appear == null && used.contains(this) ? this : null;
    }

    public static OptionImpl createAlways(Option o) {
        class Always
        extends OptionImpl {
            public Always(Option o, OptionProcessor p, int t) {
                super(o, p, 5);
            }

            @Override
            public void process(String[] additionalArgs, Map<Option, String[]> optionsAndTheirArgs) throws CommandException {
                optionsAndTheirArgs.put(this.option, NO_VALUE);
            }

            @Override
            public void associateValue(String value) {
                throw new IllegalStateException();
            }

            @Override
            public Appearance checkConsistent(Set<OptionImpl> presentOptions) {
                return Appearance.YES;
            }

            @Override
            protected OptionImpl handleAdd(Collection<OptionImpl> allOptions) {
                Always n = (Always)OptionImpl.doClone(this);
                allOptions.add(n);
                return n;
            }
        }
        return new Always(o, null, 0);
    }

    public static OptionImpl createNoArg(Option o) {
        class NoArg
        extends OptionImpl {
            public NoArg(Option o, OptionProcessor p, int t) {
                super(o, p, 0);
            }

            @Override
            public void process(String[] additionalArgs, Map<Option, String[]> optionsAndTheirArgs) throws CommandException {
                optionsAndTheirArgs.put(this.option, NO_VALUE);
            }

            @Override
            public void associateValue(String value) {
                throw new IllegalStateException();
            }

            @Override
            public Appearance checkConsistent(Set<OptionImpl> presentOptions) {
                return presentOptions.contains(this) ? Appearance.YES : Appearance.NO;
            }

            @Override
            protected OptionImpl handleAdd(Collection<OptionImpl> allOptions) {
                NoArg n = (NoArg)OptionImpl.doClone(this);
                allOptions.add(n);
                return n;
            }
        }
        return new NoArg(o, null, 0);
    }

    public static OptionImpl createOneArg(Option option, boolean required) {
        class OneOptionImpl
        extends OptionImpl {
            private String arg;

            public OneOptionImpl(Option option, OptionProcessor p, int type, String value) {
                super(option, p, type);
                this.arg = value;
            }

            @Override
            public void process(String[] additionalArgs, Map<Option, String[]> optionsAndTheirArgs) throws CommandException {
                if (this.arg != null) {
                    optionsAndTheirArgs.put(this.option, new String[]{this.arg});
                } else {
                    assert (this.argumentType == 2);
                    optionsAndTheirArgs.put(this.option, NO_VALUE);
                }
            }

            @Override
            public void associateValue(String value) throws CommandException {
                this.arg = value;
            }

            @Override
            public OptionImpl handleAdd(Collection<OptionImpl> allOptions) {
                OneOptionImpl one = (OneOptionImpl)OptionImpl.doClone(this);
                allOptions.add(one);
                return one;
            }

            @Override
            public Appearance checkConsistent(Set<OptionImpl> presentOptions) {
                return presentOptions.contains(this) ? Appearance.YES : Appearance.NO;
            }
        }
        return new OneOptionImpl(option, null, required ? 1 : 2, null);
    }

    public static OptionImpl createAdd(Option option, boolean defaultOption) {
        class AddOptionImpl
        extends OptionImpl {
            private boolean processed;
            final /* synthetic */ boolean val$defaultOption;

            /*
             * WARNING - Possible parameter corruption
             */
            public AddOptionImpl(Option option, OptionProcessor p) {
                this.val$defaultOption = optionProcessor;
                super((Option)option, (OptionProcessor)((Object)p), optionProcessor != false ? 4 : 3);
            }

            @Override
            public void process(String[] additionalArgs, Map<Option, String[]> optionsAndTheirArgs) throws CommandException {
                optionsAndTheirArgs.put(this.option, additionalArgs);
            }

            @Override
            public void associateValue(String value) throws CommandException {
                throw new IllegalStateException();
            }

            @Override
            public Appearance checkConsistent(Set<OptionImpl> presentOptions) {
                return presentOptions.contains(this) ? Appearance.YES : Appearance.NO;
            }

            @Override
            protected OptionImpl handleAdd(Collection<OptionImpl> allOptions) {
                AddOptionImpl n = (AddOptionImpl)OptionImpl.doClone(this);
                allOptions.add(n);
                return n;
            }
        }
        return new AddOptionImpl(option, null, defaultOption);
    }

    public static OptionImpl find(Option op) {
        return Trampoline.DEFAULT.impl(op);
    }

    public static OptionImpl create(Option option, int type, List<Option> alternatives) {
        class AlternativeOptionsImpl
        extends OptionImpl {
            private List<OptionImpl> arr;
            private List<OptionImpl> used;
            private Set<OptionImpl> missing;
            final /* synthetic */ int val$type;

            /*
             * WARNING - Possible parameter corruption
             */
            public AlternativeOptionsImpl(Option self, OptionProcessor p, List<OptionImpl> arr) {
                this.val$type = list;
                super((Option)self, (OptionProcessor)((Object)p), 0);
                this.arr = arr;
            }

            @Override
            public Appearance checkConsistent(Set<OptionImpl> presentOptions) {
                class MF
                implements MessageFactory {
                    MF() {
                    }

                    @Override
                    public String errorMessage(String[] args) {
                        switch (1AlternativeOptionsImpl.this.val$type) {
                            case 0: {
                                assert (1AlternativeOptionsImpl.this.used.size() >= 2);
                                String n1 = ((OptionImpl)1AlternativeOptionsImpl.this.used.get(0)).findName(true, args);
                                String n2 = ((OptionImpl)1AlternativeOptionsImpl.this.used.get(1)).findName(true, args);
                                assert (n1 != null);
                                assert (n2 != null);
                                return NbBundle.getMessage(CommandLine.class, (String)"MSG_CannotTogether", (Object)n1, (Object)n2);
                            }
                            case 1: {
                                StringBuffer sb = new StringBuffer();
                                String app = "";
                                for (OptionImpl i : 1AlternativeOptionsImpl.this.arr) {
                                    sb.append(app);
                                    sb.append(i.findName(false, args));
                                    app = ", ";
                                }
                                return NbBundle.getMessage(CommandLine.class, (String)"MSG_MissingOptions", (Object)sb.toString());
                            }
                            case 2: {
                                StringBuffer sb = new StringBuffer();
                                String app = "";
                                for (OptionImpl i : 1AlternativeOptionsImpl.this.arr) {
                                    sb.append(app);
                                    sb.append(i.findName(false, args));
                                    app = ", ";
                                }
                                return NbBundle.getMessage(CommandLine.class, (String)"MSG_NeedAtLeastOne", (Object)sb.toString());
                            }
                        }
                        throw new IllegalStateException("Type: " + 1AlternativeOptionsImpl.this.val$type);
                    }
                }
                int cnt = 0;
                this.used = new ArrayList<OptionImpl>();
                this.missing = new HashSet<OptionImpl>();
                HashSet<OptionImpl> maybe = new HashSet<OptionImpl>();
                for (int i = 0; i < this.arr.size(); ++i) {
                    Appearance check = this.arr.get(i).checkConsistent(presentOptions);
                    if (check.isError()) {
                        return check;
                    }
                    if (Appearance.NO == check) {
                        this.missing.add(this.arr.get(i));
                        continue;
                    }
                    if (Appearance.MAYBE == check) {
                        maybe.add(this.arr.get(i));
                        continue;
                    }
                    ++cnt;
                    this.used.add(this.arr.get(i));
                }
                if (cnt == 0) {
                    return this.val$type == 3 ? Appearance.MAYBE : Appearance.NO;
                }
                switch (this.val$type) {
                    case 0: {
                        if (cnt != true) break;
                        return Appearance.YES;
                    }
                    case 1: {
                        if (!this.missing.isEmpty()) break;
                        this.used.addAll(maybe);
                        return Appearance.YES;
                    }
                    case 2: 
                    case 3: {
                        if (cnt < true) break;
                        return Appearance.YES;
                    }
                }
                return Appearance.createError(new MF());
            }

            @Override
            public void markConsistent(Appearance a) {
                super.markConsistent(a);
                for (OptionImpl i : this.arr) {
                    i.markConsistent(a);
                }
            }

            @Override
            public OptionImpl findNotUsedOption(Set<OptionImpl> used) {
                OptionImpl me = super.findNotUsedOption(used);
                if (me != null) {
                    return me;
                }
                for (OptionImpl i : this.arr) {
                    me = i.findNotUsedOption(used);
                    if (me == null) continue;
                    return me;
                }
                return null;
            }

            @Override
            public void associateValue(String value) throws CommandException {
            }

            @Override
            public OptionImpl handleAdd(Collection<OptionImpl> allOptions) {
                ArrayList<OptionImpl> copy = new ArrayList<OptionImpl>();
                for (int i = 0; i < this.arr.size(); ++i) {
                    copy.add(this.arr.get(i).addWorkingCopy(allOptions));
                }
                AlternativeOptionsImpl alt = (AlternativeOptionsImpl)OptionImpl.doClone(this);
                alt.arr = copy;
                allOptions.add(alt);
                return alt;
            }

            @Override
            public void process(String[] additionalArgs, Map<Option, String[]> optionsAndTheirArgs) throws CommandException {
                optionsAndTheirArgs.put(this.option, NO_VALUE);
                for (OptionImpl i : this.used) {
                    i.process(additionalArgs, optionsAndTheirArgs);
                }
            }

            @Override
            public String findName(boolean usedOrAny, String[] args) {
                for (OptionImpl elem : this.arr) {
                    String n = elem.findName(usedOrAny, args);
                    if (n == null) continue;
                    return n;
                }
                return null;
            }

        }
        ArrayList<OptionImpl> list = new ArrayList<OptionImpl>();
        for (int i = 0; i < alternatives.size(); ++i) {
            list.add(OptionImpl.find(alternatives.get(i)));
        }
        return new AlternativeOptionsImpl(option, null, list, type);
    }

    public static abstract class Trampoline {
        public static Trampoline DEFAULT;

        public abstract OptionImpl impl(Option var1);

        public abstract Env create(CommandLine var1, InputStream var2, OutputStream var3, OutputStream var4, File var5);

        public abstract void usage(PrintWriter var1, Option var2, int var3);

        public abstract Option[] getOptions(OptionProcessor var1);

        public abstract void process(OptionProcessor var1, Env var2, Map<Option, String[]> var3) throws CommandException;

        public abstract String getLongName(Option var1);

        public abstract int getShortName(Option var1);

        public abstract String getDisplayName(Option var1, Locale var2);

        static {
            try {
                Class.forName(Option.class.getName(), true, Trampoline.class.getClassLoader());
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public static interface MessageFactory {
        public String errorMessage(String[] var1);
    }

    public static class Appearance {
        private MessageFactory msg;
        public static final Appearance YES = new Appearance();
        public static final Appearance NO = new Appearance();
        public static final Appearance MAYBE = new Appearance();

        private Appearance() {
        }

        public static final Appearance createError(MessageFactory msg) {
            Appearance a = new Appearance();
            a.msg = msg;
            return a;
        }

        public final boolean isThere() {
            return this == YES;
        }

        public boolean isError() {
            return YES != this && NO != this && MAYBE != this;
        }

        public String errorMessage(String[] args) {
            return this.msg.errorMessage(args);
        }

        public String toString() {
            if (this == YES) {
                return "YES";
            }
            if (this == NO) {
                return "NO";
            }
            if (this == MAYBE) {
                return "MAYBE";
            }
            return "ERROR[" + this.errorMessage(new String[0]) + "]";
        }
    }

}

