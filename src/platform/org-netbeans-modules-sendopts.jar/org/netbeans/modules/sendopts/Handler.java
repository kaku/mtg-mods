/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.CLIHandler
 *  org.netbeans.CLIHandler$Args
 */
package org.netbeans.modules.sendopts;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import org.netbeans.CLIHandler;
import org.netbeans.modules.sendopts.HandlerImpl;

public class Handler
extends CLIHandler {
    public Handler() {
        super(3);
    }

    protected int cli(CLIHandler.Args args) {
        return HandlerImpl.execute(args.getArguments(), args.getInputStream(), args.getOutputStream(), args.getErrorStream(), args.getCurrentDirectory());
    }

    protected void usage(PrintWriter w) {
        HandlerImpl.usage(w);
    }
}

