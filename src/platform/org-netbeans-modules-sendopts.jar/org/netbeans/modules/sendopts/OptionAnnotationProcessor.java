/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.sendopts;

import java.util.Collections;
import java.util.Set;
import javax.annotation.processing.Completion;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import org.netbeans.modules.sendopts.OptionAnnotationProcessorImpl;

public final class OptionAnnotationProcessor
implements Processor {
    private Processor delegate;
    private String msg;

    @Override
    public Set<String> getSupportedOptions() {
        if (this.delegate() != null) {
            return this.delegate().getSupportedOptions();
        }
        return Collections.emptySet();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        if (this.delegate() != null) {
            return this.delegate().getSupportedAnnotationTypes();
        }
        return Collections.emptySet();
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        if (this.delegate() != null) {
            return this.delegate().getSupportedSourceVersion();
        }
        return SourceVersion.RELEASE_7;
    }

    @Override
    public void init(ProcessingEnvironment processingEnv) {
        if (this.delegate() != null) {
            this.delegate().init(processingEnv);
        } else {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "Please add org.openide.filesystems module on classpath to generate declarative registration for @Arg");
            if (this.msg != null) {
                processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, this.msg);
            }
        }
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if (this.delegate() != null) {
            return this.delegate().process(annotations, roundEnv);
        }
        return true;
    }

    @Override
    public Iterable<? extends Completion> getCompletions(Element element, AnnotationMirror annotation, ExecutableElement member, String userText) {
        if (this.delegate() != null) {
            return this.delegate().getCompletions(element, annotation, member, userText);
        }
        return Collections.emptySet();
    }

    private Processor delegate() {
        if (this.delegate == null) {
            try {
                this.delegate = new OptionAnnotationProcessorImpl();
            }
            catch (LinkageError ex) {
                this.msg = ex.getMessage();
            }
        }
        return this.delegate;
    }
}

