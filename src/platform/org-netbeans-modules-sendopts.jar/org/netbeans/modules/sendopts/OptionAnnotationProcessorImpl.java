/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 */
package org.netbeans.modules.sendopts;

import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import org.netbeans.modules.sendopts.DefaultProcessor;
import org.netbeans.spi.sendopts.Arg;
import org.netbeans.spi.sendopts.Description;
import org.netbeans.spi.sendopts.OptionProcessor;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;

@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
final class OptionAnnotationProcessorImpl
extends LayerGeneratingProcessor {
    OptionAnnotationProcessorImpl() {
    }

    public Set<String> getSupportedAnnotationTypes() {
        HashSet<String> set = new HashSet<String>();
        set.add(Arg.class.getName());
        return set;
    }

    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        PrimitiveType boolType = this.processingEnv.getTypeUtils().getPrimitiveType(TypeKind.BOOLEAN);
        TypeMirror stringType = this.processingEnv.getElementUtils().getTypeElement("java.lang.String").asType();
        ArrayType stringArray = this.processingEnv.getTypeUtils().getArrayType(stringType);
        HashSet<TypeElement> processors = new HashSet<TypeElement>();
        for (Element e : roundEnv.getElementsAnnotatedWith(Arg.class)) {
            if (e.getModifiers().contains((Object)Modifier.STATIC)) {
                throw new LayerGenerationException("@Arg can be applied only to non-static fields", e);
            }
            if (!e.getModifiers().contains((Object)Modifier.PUBLIC)) {
                throw new LayerGenerationException("@Arg can be applied only to public fields", e);
            }
            if (e.getModifiers().contains((Object)Modifier.FINAL)) {
                throw new LayerGenerationException("@Arg can be applied only to non-final fields", e);
            }
            Arg arg = e.getAnnotation(Arg.class);
            if (arg.longName().isEmpty() && arg.shortName() == '\uffff') {
                throw new LayerGenerationException("At least one of longName or shortName attributes needs to be non-empty", e);
            }
            if (arg.implicit() && !e.asType().equals(stringArray)) {
                throw new LayerGenerationException("implicit @Arg can only be applied to String[] fields", e);
            }
            processors.add(TypeElement.class.cast(e.getEnclosingElement()));
        }
        for (TypeElement te : processors) {
            int cnt = 1;
            String typeName = this.processingEnv.getElementUtils().getBinaryName(te).toString().replace('.', '-');
            LayerBuilder.File f = this.layer(new Element[]{te}).file("Services/OptionProcessors/" + typeName + ".instance");
            f.methodvalue("instanceCreate", DefaultProcessor.class.getName(), "create");
            f.stringvalue("instanceOf", OptionProcessor.class.getName());
            f.stringvalue("class", this.processingEnv.getElementUtils().getBinaryName(te).toString());
            for (Element e2 : te.getEnclosedElements()) {
                Arg o = e2.getAnnotation(Arg.class);
                if (o == null) continue;
                Description d = e2.getAnnotation(Description.class);
                if (o.shortName() != '\uffff') {
                    f.charvalue("" + cnt + ".shortName", o.shortName());
                }
                if (!o.longName().isEmpty()) {
                    f.stringvalue("" + cnt + ".longName", o.longName());
                }
                if (boolType == e2.asType()) {
                    f.stringvalue("" + cnt + ".type", "withoutArgument");
                } else if (stringType == e2.asType()) {
                    if (o.defaultValue().equals("\u0000")) {
                        f.stringvalue("" + cnt + ".type", "requiredArgument");
                    } else {
                        f.stringvalue("" + cnt + ".type", "optionalArgument");
                    }
                } else {
                    if (!stringArray.equals(e2.asType())) {
                        throw new LayerGenerationException("Field type has to be either boolean, String or String[]!", e2);
                    }
                    f.stringvalue("" + cnt + ".type", "additionalArguments");
                }
                if (o.implicit()) {
                    f.boolvalue("" + cnt + ".implicit", true);
                }
                if (d != null) {
                    this.writeBundle(f, "" + cnt + ".displayName", d.displayName(), e2);
                    this.writeBundle(f, "" + cnt + ".shortDescription", d.shortDescription(), e2);
                }
                ++cnt;
            }
            f.write();
        }
        return true;
    }

    private void writeBundle(LayerBuilder.File f, String key, String value, Element e) throws LayerGenerationException {
        if (value.isEmpty()) {
            return;
        }
        this.layer(new Element[]{e}).file("dummy/file").bundlevalue(key, value);
        if (value.startsWith("#")) {
            Element referenceElement;
            for (referenceElement = e; referenceElement != null && referenceElement.getKind() != ElementKind.PACKAGE; referenceElement = referenceElement.getEnclosingElement()) {
            }
            if (referenceElement == null) {
                throw new LayerGenerationException("No reference element to determine package in '" + value + "'", e);
            }
            value = ((PackageElement)referenceElement).getQualifiedName() + ".Bundle" + value;
        }
        f.stringvalue(key, value);
    }
}

