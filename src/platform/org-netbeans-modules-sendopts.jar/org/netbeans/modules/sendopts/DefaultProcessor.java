/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.sendopts;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.sendopts.CommandException;
import org.netbeans.modules.sendopts.OptionImpl;
import org.netbeans.spi.sendopts.Arg;
import org.netbeans.spi.sendopts.ArgsProcessor;
import org.netbeans.spi.sendopts.Description;
import org.netbeans.spi.sendopts.Env;
import org.netbeans.spi.sendopts.Option;
import org.netbeans.spi.sendopts.OptionProcessor;
import org.openide.util.Lookup;

public final class DefaultProcessor
extends OptionProcessor {
    private static final Option defArgs = Option.defaultArguments();
    private final String clazz;
    private final Set<Option> options;

    private DefaultProcessor(String clazz, Set<Option> arr) {
        this.clazz = clazz;
        this.options = Collections.unmodifiableSet(arr);
    }

    private static Option createOption(String type, Character shortName, String longName, String displayName, String description) {
        String[] arr;
        Option o = null;
        if (shortName == null) {
            shortName = Character.valueOf('\uffff');
        }
        switch (Type.valueOf(type)) {
            case withoutArgument: {
                o = Option.withoutArgument(shortName.charValue(), longName);
                break;
            }
            case requiredArgument: {
                o = Option.requiredArgument(shortName.charValue(), longName);
                break;
            }
            case optionalArgument: {
                o = Option.optionalArgument(shortName.charValue(), longName);
                break;
            }
            case additionalArguments: {
                o = Option.additionalArguments(shortName.charValue(), longName);
                break;
            }
            default: {
                assert (false);
                break;
            }
        }
        if (displayName != null) {
            arr = DefaultProcessor.fixBundles(displayName.split("#", 2));
            o = Option.displayName(o, arr[0], arr[1]);
        }
        if (description != null) {
            arr = DefaultProcessor.fixBundles(description.split("#", 2));
            o = Option.shortDescription(o, arr[0], arr[1]);
        }
        return o;
    }

    public static OptionProcessor create(Class<?> clazz) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("class", clazz.getName());
        int cnt = 1;
        for (Field e : clazz.getFields()) {
            Arg o = e.getAnnotation(Arg.class);
            if (o == null) continue;
            Description d = e.getAnnotation(Description.class);
            if (o.shortName() != '\uffff') {
                map.put("" + cnt + ".shortName", Character.valueOf(o.shortName()));
            }
            if (!o.longName().isEmpty()) {
                map.put("" + cnt + ".longName", o.longName());
            }
            if (e.getType() == Boolean.TYPE) {
                map.put("" + cnt + ".type", "withoutArgument");
            } else if (String.class == e.getType()) {
                if (o.defaultValue().equals("\u0000")) {
                    map.put("" + cnt + ".type", "requiredArgument");
                } else {
                    map.put("" + cnt + ".type", "optionalArgument");
                }
            } else {
                if (!String[].class.equals(e.getType())) {
                    throw new IllegalStateException("Field type has to be either boolean, String or String[]! " + e);
                }
                map.put("" + cnt + ".type", "additionalArguments");
            }
            if (o.implicit()) {
                map.put("" + cnt + ".implicit", true);
            }
            if (d != null) {
                DefaultProcessor.writeBundle(map, "" + cnt + ".displayName", d.displayName(), e);
                DefaultProcessor.writeBundle(map, "" + cnt + ".shortDescription", d.shortDescription(), e);
            }
            ++cnt;
        }
        return DefaultProcessor.create(map);
    }

    static OptionProcessor create(Map<?, ?> map) {
        String c = (String)map.get("class");
        LinkedHashSet<Option> arr = new LinkedHashSet<Option>();
        int cnt = 1;
        do {
            Character shortName = (Character)map.get("" + cnt + ".shortName");
            String longName = (String)map.get("" + cnt + ".longName");
            if (shortName == null && longName == null) break;
            String type = (String)map.get("" + cnt + ".type");
            String displayName = (String)map.get("" + cnt + ".displayName");
            String description = (String)map.get("" + cnt + ".shortDescription");
            arr.add(DefaultProcessor.createOption(type, shortName, longName, displayName, description));
            if (Boolean.TRUE.equals(map.get("" + cnt + ".implicit"))) {
                arr.add(defArgs);
            }
            ++cnt;
        } while (true);
        return new DefaultProcessor(c, arr);
    }

    @Override
    protected Set<Option> getOptions() {
        return this.options;
    }

    @Override
    protected void process(Env env, Map<Option, String[]> optionValues) throws CommandException {
        try {
            ClassLoader l = DefaultProcessor.findClassLoader();
            Class realClazz = Class.forName(this.clazz, true, l);
            Object instance = realClazz.newInstance();
            Map<Option, Field> map = DefaultProcessor.processFields(realClazz, this.options);
            for (Map.Entry<Option, String[]> entry : optionValues.entrySet()) {
                Option option = entry.getKey();
                Type type = Type.valueOf(option);
                Field f = map.get(option);
                assert (f != null);
                switch (type) {
                    case withoutArgument: {
                        f.setBoolean(instance, true);
                        break;
                    }
                    case requiredArgument: {
                        f.set(instance, entry.getValue()[0]);
                        break;
                    }
                    case optionalArgument: {
                        if (entry.getValue().length == 1) {
                            f.set(instance, entry.getValue()[0]);
                            break;
                        }
                        f.set(instance, f.getAnnotation(Arg.class).defaultValue());
                        break;
                    }
                    case additionalArguments: {
                        f.set(instance, entry.getValue());
                        break;
                    }
                    case defaultArguments: {
                        f.set(instance, entry.getValue());
                    }
                }
            }
            if (instance instanceof Runnable) {
                ((Runnable)instance).run();
            }
            if (instance instanceof ArgsProcessor) {
                ((ArgsProcessor)instance).process(env);
            }
        }
        catch (Exception exception) {
            throw (CommandException)new CommandException(10, exception.getLocalizedMessage()).initCause(exception);
        }
    }

    public static ClassLoader findClassLoader() {
        ClassLoader l = null;
        try {
            l = DefaultProcessor.findClassLoaderFromLookup();
        }
        catch (LinkageError ex) {
            // empty catch block
        }
        if (l == null) {
            l = Thread.currentThread().getContextClassLoader();
        }
        if (l == null) {
            l = DefaultProcessor.class.getClassLoader();
        }
        return l;
    }

    private static ClassLoader findClassLoaderFromLookup() {
        return (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
    }

    private static Map<Option, Field> processFields(Class<?> type, Set<Option> options) {
        HashMap<Option, Field> map = new HashMap<Option, Field>();
        for (Field f : type.getFields()) {
            Arg arg = f.getAnnotation(Arg.class);
            if (arg == null) continue;
            Option o = null;
            for (Option c : options) {
                char shortN = (char)OptionImpl.Trampoline.DEFAULT.getShortName(c);
                String longN = OptionImpl.Trampoline.DEFAULT.getLongName(c);
                if (shortN != arg.shortName() || !DefaultProcessor.equalStrings(longN, arg)) continue;
                o = c;
                break;
            }
            assert (o != null);
            map.put(o, f);
            if (!arg.implicit()) continue;
            map.put(defArgs, f);
        }
        assert (map.size() == options.size());
        return map;
    }

    private static boolean equalStrings(String longN, Arg arg) {
        if (longN == null) {
            return arg.longName().isEmpty();
        }
        return longN.equals(arg.longName());
    }

    private static void writeBundle(Map<String, Object> f, String key, String value, Field e) throws IllegalStateException {
        if (value.isEmpty()) {
            return;
        }
        if (value.startsWith("#")) {
            Package pkg = e.getDeclaringClass().getPackage();
            value = pkg.getName() + ".Bundle" + value;
        }
        f.put(key, value);
    }

    private static String[] fixBundles(String[] oneOrTwo) {
        if (oneOrTwo.length == 2) {
            return oneOrTwo;
        }
        return new String[]{OptionImpl.NO_BUNDLE, oneOrTwo[0]};
    }

    private static final class Type
    extends Enum<Type> {
        public static final /* enum */ Type withoutArgument = new Type();
        public static final /* enum */ Type requiredArgument = new Type();
        public static final /* enum */ Type optionalArgument = new Type();
        public static final /* enum */ Type additionalArguments = new Type();
        public static final /* enum */ Type defaultArguments = new Type();
        private static final /* synthetic */ Type[] $VALUES;

        public static Type[] values() {
            return (Type[])$VALUES.clone();
        }

        public static Type valueOf(String name) {
            return Enum.valueOf(Type.class, name);
        }

        private Type() {
            super(string, n);
        }

        public static Type valueOf(Option o) {
            OptionImpl impl = OptionImpl.Trampoline.DEFAULT.impl(o);
            switch (impl.argumentType) {
                case 0: {
                    return withoutArgument;
                }
                case 1: {
                    return requiredArgument;
                }
                case 2: {
                    return optionalArgument;
                }
                case 3: {
                    return additionalArguments;
                }
                case 4: {
                    return defaultArguments;
                }
            }
            assert (false);
            return null;
        }

        static {
            $VALUES = new Type[]{withoutArgument, requiredArgument, optionalArgument, additionalArguments, defaultArguments};
        }
    }

}

