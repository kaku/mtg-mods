/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.actions.ActionInvoker
 */
package org.netbeans.modules.openide.actions;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import org.openide.actions.ActionManager;
import org.openide.util.actions.ActionInvoker;

public class ActionsBridgeImpl
extends ActionInvoker {
    protected void invokeAction(Action action, ActionEvent ev) {
        ActionManager.getDefault().invokeAction(action, ev);
    }
}

