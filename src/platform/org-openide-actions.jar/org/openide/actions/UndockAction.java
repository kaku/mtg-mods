/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallbackSystemAction
 */
package org.openide.actions;

import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallbackSystemAction;

@Deprecated
public class UndockAction
extends CallbackSystemAction {
    public String getName() {
        return NbBundle.getMessage(UndockAction.class, (String)"UndockWindow");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(UndockAction.class);
    }

    protected String iconResource() {
        return "org/openide/resources/actions/undock.gif";
    }

    protected boolean asynchronous() {
        return false;
    }
}

