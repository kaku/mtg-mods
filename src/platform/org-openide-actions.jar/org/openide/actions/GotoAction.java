/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallbackSystemAction
 */
package org.openide.actions;

import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallbackSystemAction;

public class GotoAction
extends CallbackSystemAction {
    public GotoAction() {
        this.putProperty((Object)"noIconInMenu", (Object)Boolean.TRUE);
    }

    public String getName() {
        return NbBundle.getMessage(GotoAction.class, (String)"Goto");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(GotoAction.class);
    }

    protected boolean asynchronous() {
        return false;
    }
}

