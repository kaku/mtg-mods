/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Children
 *  org.openide.nodes.Index
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.NodeAction
 */
package org.openide.actions;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.nodes.Children;
import org.openide.nodes.Index;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;

public final class MoveUpAction
extends NodeAction {
    private static final String PROP_ORDER_LISTENER = "sellistener";
    private static Logger err = Logger.getLogger("org.openide.actions.MoveUpAction");
    private Reference curIndexCookie;

    protected void initialize() {
        err.fine("initialize");
        super.initialize();
        OrderingListener sl = new OrderingListener();
        this.putProperty((Object)"sellistener", (Object)sl);
    }

    private Index getCurIndexCookie() {
        return this.curIndexCookie == null ? null : (Index)this.curIndexCookie.get();
    }

    protected void performAction(Node[] activatedNodes) {
        Index cookie = this.getIndexCookie(activatedNodes);
        if (cookie == null) {
            return;
        }
        int nodeIndex = cookie.indexOf(activatedNodes[0]);
        if (nodeIndex > 0) {
            cookie.moveUp(nodeIndex);
        }
    }

    protected boolean asynchronous() {
        return false;
    }

    protected boolean enable(Node[] activatedNodes) {
        err.fine("enable; activatedNodes=" + (activatedNodes == null ? null : Arrays.asList(activatedNodes)));
        Index idx = this.getCurIndexCookie();
        if (idx != null) {
            idx.removeChangeListener((ChangeListener)this.getProperty((Object)"sellistener"));
        }
        Index cookie = this.getIndexCookie(activatedNodes);
        if (err != null) {
            err.fine("enable; cookie=" + (Object)cookie);
        }
        if (cookie == null) {
            return false;
        }
        cookie.addChangeListener((ChangeListener)((OrderingListener)this.getProperty((Object)"sellistener")));
        this.curIndexCookie = new WeakReference<Index>(cookie);
        int index = cookie.indexOf(activatedNodes[0]);
        if (err != null) {
            err.fine("enable; index=" + index);
            if (index == -1) {
                Node parent = activatedNodes[0].getParentNode();
                err.fine("enable; parent=" + (Object)parent + "; parent.children=" + Arrays.asList(parent.getChildren().getNodes()));
            }
        }
        return index > 0;
    }

    public String getName() {
        return NbBundle.getMessage(MoveUpAction.class, (String)"MoveUp");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(MoveUpAction.class);
    }

    private Index getIndexCookie(Node[] activatedNodes) {
        if (activatedNodes == null || activatedNodes.length != 1) {
            return null;
        }
        Node parent = activatedNodes[0].getParentNode();
        if (parent == null) {
            return null;
        }
        return (Index)parent.getCookie(Index.class);
    }

    private final class OrderingListener
    implements ChangeListener {
        OrderingListener() {
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            Node[] activatedNodes;
            err.fine("stateChanged; activatedNodes=" + ((activatedNodes = MoveUpAction.this.getActivatedNodes()) == null ? null : Arrays.asList(activatedNodes)));
            Index cookie = MoveUpAction.this.getIndexCookie(activatedNodes);
            if (err != null) {
                err.fine("stateChanged; cookie=" + (Object)cookie);
            }
            if (cookie == null) {
                MoveUpAction.this.setEnabled(false);
            } else {
                int index = cookie.indexOf(activatedNodes[0]);
                if (err != null) {
                    err.fine("stateChanged; index=" + index);
                }
                MoveUpAction.this.setEnabled(index > 0);
            }
        }
    }

}

