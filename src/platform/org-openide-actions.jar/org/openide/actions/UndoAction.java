/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.UndoRedo
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.CallableSystemAction
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 *  org.openide.windows.WindowManager
 */
package org.openide.actions;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.undo.CannotUndoException;
import org.openide.actions.RedoAction;
import org.openide.actions.UndoRedoAction;
import org.openide.awt.UndoRedo;
import org.openide.util.ContextAwareAction;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.util.actions.CallableSystemAction;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class UndoAction
extends CallableSystemAction
implements ContextAwareAction {
    private static Listener listener;
    private static UndoRedo last;
    private static String SWING_DEFAULT_LABEL;
    private static UndoAction undoAction;
    private static RedoAction redoAction;

    public boolean isEnabled() {
        UndoAction.initializeUndoRedo();
        return super.isEnabled();
    }

    static synchronized void initializeUndoRedo() {
        if (listener != null) {
            return;
        }
        listener = new Listener();
        TopComponent.Registry r = WindowManager.getDefault().getRegistry();
        r.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)listener, (Object)r));
        last = UndoAction.getUndoRedo();
        last.addChangeListener((ChangeListener)listener);
        UndoAction.updateStatus();
    }

    static synchronized void updateStatus() {
        if (undoAction == null) {
            undoAction = (UndoAction)UndoAction.findObject(UndoAction.class, (boolean)false);
        }
        if (redoAction == null) {
            redoAction = (RedoAction)UndoAction.findObject(RedoAction.class, (boolean)false);
        }
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                UndoRedo ur = UndoAction.getUndoRedo();
                if (undoAction != null) {
                    undoAction.setEnabled(ur.canUndo());
                }
                if (redoAction != null) {
                    redoAction.setEnabled(ur.canRedo());
                }
            }
        });
    }

    static UndoRedo getUndoRedo() {
        TopComponent el = WindowManager.getDefault().getRegistry().getActivated();
        return el == null ? UndoRedo.NONE : el.getUndoRedo();
    }

    public String getName() {
        String undo = UndoAction.getUndoRedo().getUndoPresentationName();
        Logger.getLogger(UndoAction.class.getName()).log(Level.FINE, "getUndoRedo().getUndoPresentationName() returns " + undo);
        Logger.getLogger(UndoAction.class.getName()).log(Level.FINE, "SWING_DEFAULT_LABEL is " + SWING_DEFAULT_LABEL);
        if (undo != null && SWING_DEFAULT_LABEL != null && undo.startsWith(SWING_DEFAULT_LABEL)) {
            undo = undo.substring(SWING_DEFAULT_LABEL.length()).trim();
        }
        Logger.getLogger(UndoAction.class.getName()).log(Level.FINE, "Name adapted by SWING_DEFAULT_LABEL is " + undo);
        String presentationName = null;
        presentationName = undo == null || undo.trim().length() == 0 ? NbBundle.getMessage(UndoAction.class, (String)"UndoSimple") : NbBundle.getMessage(UndoAction.class, (String)"UndoWithParameter", (Object)undo);
        Logger.getLogger(UndoAction.class.getName()).log(Level.FINE, "Result name is " + presentationName);
        return presentationName;
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(UndoAction.class);
    }

    protected String iconResource() {
        return "org/openide/resources/actions/undo.gif";
    }

    public void performAction() {
        try {
            UndoRedo undoRedo = UndoAction.getUndoRedo();
            if (undoRedo.canUndo()) {
                undoRedo.undo();
            }
        }
        catch (CannotUndoException ex) {
            UndoRedoAction.cannotUndoRedo(ex);
        }
        UndoAction.updateStatus();
    }

    protected boolean asynchronous() {
        return false;
    }

    public Action createContextAwareInstance(Lookup actionContext) {
        return new UndoRedoAction(actionContext, true, false);
    }

    static {
        last = UndoRedo.NONE;
        SWING_DEFAULT_LABEL = UIManager.getString("AbstractUndoableEdit.undoText");
        undoAction = null;
        redoAction = null;
    }

    private static final class Listener
    implements PropertyChangeListener,
    ChangeListener {
        Listener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent ev) {
            UndoAction.updateStatus();
            last.removeChangeListener((ChangeListener)this);
            last = UndoAction.getUndoRedo();
            last.addChangeListener((ChangeListener)this);
        }

        @Override
        public void stateChanged(ChangeEvent ev) {
            UndoAction.updateStatus();
        }
    }

}

