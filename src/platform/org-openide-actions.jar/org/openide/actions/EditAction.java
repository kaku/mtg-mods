/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.EditCookie
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CookieAction
 */
package org.openide.actions;

import org.openide.cookies.EditCookie;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CookieAction;

public class EditAction
extends CookieAction {
    protected boolean surviveFocusChange() {
        return false;
    }

    public String getName() {
        return NbBundle.getBundle(EditAction.class).getString("Edit");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(EditAction.class);
    }

    protected int mode() {
        return 4;
    }

    protected Class[] cookieClasses() {
        return new Class[]{EditCookie.class};
    }

    protected void performAction(Node[] activatedNodes) {
        for (int i = 0; i < activatedNodes.length; ++i) {
            EditCookie es = (EditCookie)activatedNodes[i].getCookie(EditCookie.class);
            if (es == null) continue;
            es.edit();
        }
    }

    protected boolean asynchronous() {
        return false;
    }
}

