/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallbackSystemAction
 */
package org.openide.actions;

import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallbackSystemAction;

public final class PopupAction
extends CallbackSystemAction {
    protected void initialize() {
        super.initialize();
        this.putProperty((Object)"OpenIDE-Transmodal-Action", (Object)Boolean.TRUE);
    }

    public String getName() {
        return NbBundle.getMessage(PopupAction.class, (String)"Popup");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(PopupAction.class);
    }

    protected boolean asynchronous() {
        return false;
    }
}

