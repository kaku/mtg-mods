/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallbackSystemAction
 */
package org.openide.actions;

import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallbackSystemAction;

public class PreviousTabAction
extends CallbackSystemAction {
    protected String iconResource() {
        return "org/openide/resources/actions/previousTab.gif";
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(PreviousTabAction.class);
    }

    public String getName() {
        return NbBundle.getMessage(PreviousTabAction.class, (String)"PreviousTab");
    }

    protected boolean asynchronous() {
        return false;
    }
}

