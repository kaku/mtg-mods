/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeOperation
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.NodeAction
 */
package org.openide.actions;

import org.openide.nodes.Node;
import org.openide.nodes.NodeOperation;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;

public final class OpenLocalExplorerAction
extends NodeAction {
    protected void performAction(Node[] activatedNodes) {
        NodeOperation.getDefault().explore(activatedNodes[0]);
    }

    protected boolean asynchronous() {
        return false;
    }

    protected boolean enable(Node[] activatedNodes) {
        if (activatedNodes == null || activatedNodes.length != 1 || activatedNodes[0].isLeaf()) {
            return false;
        }
        return true;
    }

    public String getName() {
        return NbBundle.getMessage(OpenLocalExplorerAction.class, (String)"OpenLocalExplorer");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(OpenLocalExplorerAction.class);
    }

    protected String iconResource() {
        return "org/openide/resources/actions/openLocalExplorer.gif";
    }
}

