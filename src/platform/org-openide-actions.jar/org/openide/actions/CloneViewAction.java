/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallbackSystemAction
 */
package org.openide.actions;

import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallbackSystemAction;

public class CloneViewAction
extends CallbackSystemAction {
    public Object getActionMapKey() {
        return "cloneWindow";
    }

    public String getName() {
        return NbBundle.getMessage(CloneViewAction.class, (String)"CloneView");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(CloneViewAction.class);
    }

    protected String iconResource() {
        return "org/openide/resources/actions/clone.gif";
    }

    protected boolean asynchronous() {
        return false;
    }
}

