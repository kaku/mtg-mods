/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Index
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CookieAction
 */
package org.openide.actions;

import org.openide.nodes.Index;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CookieAction;

public class ReorderAction
extends CookieAction {
    protected boolean surviveFocusChange() {
        return false;
    }

    public String getName() {
        return NbBundle.getBundle(ReorderAction.class).getString("Reorder");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(ReorderAction.class);
    }

    protected Class[] cookieClasses() {
        return new Class[]{Index.class};
    }

    protected int mode() {
        return 8;
    }

    protected void performAction(Node[] activatedNodes) {
        Node n = activatedNodes[0];
        Index order = (Index)n.getCookie(Index.class);
        order.reorder();
    }

    protected boolean asynchronous() {
        return false;
    }
}

