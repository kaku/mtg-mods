/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Index
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.NodeAction
 */
package org.openide.actions;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.nodes.Index;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;

public final class MoveDownAction
extends NodeAction {
    private static final String PROP_ORDER_LISTENER = "sellistener";
    private Reference<Index> curIndexCookie;

    protected void initialize() {
        super.initialize();
        OrderingListener sl = new OrderingListener();
        this.putProperty((Object)"sellistener", (Object)sl);
    }

    private Index getCurIndexCookie() {
        return this.curIndexCookie == null ? null : this.curIndexCookie.get();
    }

    protected void performAction(Node[] activatedNodes) {
        Index cookie = this.getIndexCookie(activatedNodes);
        if (cookie == null) {
            return;
        }
        int nodeIndex = cookie.indexOf(activatedNodes[0]);
        if (nodeIndex >= 0 && nodeIndex < cookie.getNodesCount() - 1) {
            cookie.moveDown(nodeIndex);
        }
    }

    protected boolean asynchronous() {
        return false;
    }

    protected boolean enable(Node[] activatedNodes) {
        Index cookie;
        Index idx = this.getCurIndexCookie();
        if (idx != null) {
            idx.removeChangeListener((ChangeListener)this.getProperty((Object)"sellistener"));
            idx = null;
        }
        if ((cookie = this.getIndexCookie(activatedNodes)) == null) {
            return false;
        }
        int nodeIndex = cookie.indexOf(activatedNodes[0]);
        cookie.addChangeListener((ChangeListener)((OrderingListener)this.getProperty((Object)"sellistener")));
        this.curIndexCookie = new WeakReference<Index>(cookie);
        return nodeIndex >= 0 && nodeIndex < cookie.getNodesCount() - 1;
    }

    public String getName() {
        return NbBundle.getMessage(MoveDownAction.class, (String)"MoveDown");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(MoveDownAction.class);
    }

    private Index getIndexCookie(Node[] activatedNodes) {
        if (activatedNodes == null || activatedNodes.length != 1) {
            return null;
        }
        Node parent = activatedNodes[0].getParentNode();
        if (parent == null) {
            return null;
        }
        return (Index)parent.getCookie(Index.class);
    }

    private final class OrderingListener
    implements ChangeListener {
        OrderingListener() {
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            Node[] activatedNodes = MoveDownAction.this.getActivatedNodes();
            Index cookie = MoveDownAction.this.getIndexCookie(activatedNodes);
            if (cookie == null) {
                MoveDownAction.this.setEnabled(false);
            } else {
                int nodeIndex = cookie.indexOf(activatedNodes[0]);
                MoveDownAction.this.setEnabled(nodeIndex >= 0 && nodeIndex < cookie.getNodesCount() - 1);
            }
        }
    }

}

