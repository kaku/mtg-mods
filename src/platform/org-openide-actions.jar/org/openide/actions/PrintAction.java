/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.PrintCookie
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CookieAction
 */
package org.openide.actions;

import org.openide.cookies.PrintCookie;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CookieAction;

public class PrintAction
extends CookieAction {
    public PrintAction() {
        this.putValue("noIconInMenu", (Object)Boolean.TRUE);
    }

    protected Class[] cookieClasses() {
        return new Class[]{PrintCookie.class};
    }

    protected void performAction(Node[] activatedNodes) {
        for (int i = 0; i < activatedNodes.length; ++i) {
            PrintCookie pc = (PrintCookie)activatedNodes[i].getCookie(PrintCookie.class);
            if (pc == null) continue;
            pc.print();
        }
    }

    protected boolean asynchronous() {
        return true;
    }

    protected int mode() {
        return 8;
    }

    public String getName() {
        return NbBundle.getMessage(PrintAction.class, (String)"Print");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(PrintAction.class);
    }

    protected String iconResource() {
        return "org/openide/resources/actions/print.png";
    }
}

