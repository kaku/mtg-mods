/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.actions.Savable
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.awt.StatusDisplayer
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.UserQuestionException
 *  org.openide.util.actions.CookieAction
 */
package org.openide.actions;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.netbeans.api.actions.Savable;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.StatusDisplayer;
import org.openide.nodes.Node;
import org.openide.util.ContextAwareAction;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.UserQuestionException;
import org.openide.util.actions.CookieAction;

public class SaveAction
extends CookieAction {
    private static Class<? extends Node.Cookie> dataObject;
    private static Method getNodeDelegate;

    public SaveAction() {
        this.putValue("noIconInMenu", (Object)Boolean.TRUE);
    }

    protected Class[] cookieClasses() {
        return new Class[]{Savable.class};
    }

    public Action createContextAwareInstance(Lookup actionContext) {
        return new Delegate(this, actionContext);
    }

    final void performAction(Lookup context) {
        Collection cookieList = context.lookupAll(Savable.class);
        LinkedList nodeList = new LinkedList(context.lookupAll(Node.class));
        block0 : for (Savable savable : cookieList) {
            for (Node node : nodeList) {
                if (!savable.equals(node.getLookup().lookup(Savable.class))) continue;
                this.performAction(savable, node);
                nodeList.remove((Object)node);
                continue block0;
            }
            this.performAction(savable, null);
        }
    }

    protected void performAction(Node[] activatedNodes) {
        for (int i = 0; i < activatedNodes.length; ++i) {
            Node node = activatedNodes[i];
            Savable sc = (Savable)node.getLookup().lookup(Savable.class);
            assert (sc != null);
            if (sc == null) {
                return;
            }
            this.performAction(sc, node);
        }
    }

    /*
     * Loose catch block
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Lifted jumps to return sites
     */
    private void performAction(Savable sc, Node n) {
        UserQuestionException userEx = null;
        do {
            try {
                if (userEx == null) {
                    sc.save();
                } else {
                    userEx.confirmed();
                }
                StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(SaveAction.class, (String)"MSG_saved", (Object)this.getSaveMessage(sc, n)));
                return;
            }
            catch (UserQuestionException ex) {
                NotifyDescriptor.Confirmation nd = new NotifyDescriptor.Confirmation((Object)ex.getLocalizedMessage(), 0);
                Object res = DialogDisplayer.getDefault().notify((NotifyDescriptor)nd);
                if (!NotifyDescriptor.OK_OPTION.equals(res)) return;
                userEx = ex;
                continue;
            }
            break;
        } while (true);
        catch (IOException e) {
            Exceptions.attachLocalizedMessage((Throwable)e, (String)NbBundle.getMessage(SaveAction.class, (String)"EXC_notsaved", (Object)this.getSaveMessage(sc, n), (Object)e.getLocalizedMessage()));
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            return;
        }
    }

    protected boolean asynchronous() {
        return false;
    }

    private String getSaveMessage(Savable sc, Node n) {
        Node.Cookie obj;
        if (n == null) {
            return sc.toString();
        }
        if (dataObject == null) {
            ClassLoader l = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
            if (l == null) {
                l = this.getClass().getClassLoader();
            }
            try {
                dataObject = Class.forName("org.openide.loaders.DataObject", true, l).asSubclass(Node.Cookie.class);
                getNodeDelegate = dataObject.getMethod("getNodeDelegate", new Class[0]);
            }
            catch (Exception ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        if (getNodeDelegate != null && (obj = n.getCookie(dataObject)) != null) {
            try {
                n = (Node)getNodeDelegate.invoke((Object)obj, new Object[0]);
            }
            catch (Exception ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return n.getDisplayName();
    }

    protected int mode() {
        return 7;
    }

    public String getName() {
        return NbBundle.getMessage(SaveAction.class, (String)"Save");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(SaveAction.class);
    }

    protected String iconResource() {
        return "org/openide/resources/actions/save.png";
    }

    private static final class Delegate
    extends AbstractAction
    implements ContextAwareAction {
        final SaveAction sa;
        final Lookup context;

        public Delegate(SaveAction sa, Lookup context) {
            this.sa = sa;
            this.context = context;
        }

        public Action createContextAwareInstance(Lookup actionContext) {
            return new Delegate(this.sa, actionContext);
        }

        @Override
        public Object getValue(String key) {
            return this.sa.getValue(key);
        }

        @Override
        public void putValue(String key, Object value) {
            this.sa.putValue(key, value);
        }

        @Override
        public boolean isEnabled() {
            return this.context.lookup(Savable.class) != null;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.sa.performAction(this.context);
        }
    }

}

