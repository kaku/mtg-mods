/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallbackSystemAction
 */
package org.openide.actions;

import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallbackSystemAction;

public class CopyAction
extends CallbackSystemAction {
    protected void initialize() {
        super.initialize();
    }

    public Object getActionMapKey() {
        return "copy-to-clipboard";
    }

    public String getName() {
        return NbBundle.getMessage(CopyAction.class, (String)"Copy");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(CopyAction.class);
    }

    protected String iconResource() {
        return "org/openide/resources/actions/copy.gif";
    }

    protected boolean asynchronous() {
        return false;
    }
}

