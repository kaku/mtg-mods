/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Actions
 *  org.openide.awt.Actions$SubMenu
 *  org.openide.awt.Actions$SubMenuModel
 *  org.openide.explorer.ExplorerManager
 *  org.openide.nodes.Node
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Result
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.NodeAction
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.datatransfer.NewType
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 *  org.openide.windows.WindowManager
 */
package org.openide.actions;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.Collection;
import javax.swing.Action;
import javax.swing.JMenuItem;
import javax.swing.event.ChangeListener;
import org.openide.actions.PasteAction;
import org.openide.awt.Actions;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Node;
import org.openide.util.ChangeSupport;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;
import org.openide.util.actions.Presenter;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.NewType;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public final class NewAction
extends NodeAction {
    private static ActSubMenuModel model = new ActSubMenuModel(null);

    protected void performAction(Node[] activatedNodes) {
        NewAction.performAction(activatedNodes, 0);
    }

    protected boolean asynchronous() {
        return false;
    }

    private static void performAction(Node[] activatedNodes, int indx) {
        NewType[] types = NewAction.getNewTypes(activatedNodes);
        if (types.length <= indx) {
            return;
        }
        NewAction.performAction(activatedNodes, types[indx]);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void performAction(Node[] activatedNodes, NewType type) {
        PasteAction.NodeSelector sel = null;
        try {
            ExplorerManager em = PasteAction.findExplorerManager();
            if (em != null) {
                sel = new PasteAction.NodeSelector(em, activatedNodes);
            }
            type.create();
        }
        catch (IOException e) {
            Exceptions.printStackTrace((Throwable)e);
        }
        finally {
            if (sel != null) {
                sel.select();
            }
        }
    }

    private static NewType[] getNewTypes() {
        return NewAction.getNewTypes(WindowManager.getDefault().getRegistry().getCurrentNodes());
    }

    private static NewType[] getNewTypes(Node[] activatedNodes) {
        if (activatedNodes == null || activatedNodes.length != 1) {
            return new NewType[0];
        }
        return activatedNodes[0].getNewTypes();
    }

    protected boolean enable(Node[] activatedNodes) {
        NewType[] types = NewAction.getNewTypes();
        NewAction.model.cs.fireChange();
        return types.length > 0;
    }

    public String getName() {
        return NewAction.createName(NewAction.getNewTypes());
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(NewAction.class);
    }

    public JMenuItem getMenuPresenter() {
        return new Actions.SubMenu((SystemAction)this, (Actions.SubMenuModel)model, false);
    }

    public JMenuItem getPopupPresenter() {
        return new Actions.SubMenu((SystemAction)this, (Actions.SubMenuModel)model, true);
    }

    public Action createContextAwareInstance(Lookup actionContext) {
        return new DelegateAction(this, actionContext);
    }

    private static String createName(NewType[] newTypes) {
        if (newTypes != null && newTypes.length == 1) {
            return NbBundle.getMessage(NewAction.class, (String)"NewArg", (Object)newTypes[0].getName());
        }
        return NbBundle.getMessage(NewAction.class, (String)"New");
    }

    private static final class DelegateAction
    implements Action,
    Presenter.Menu,
    Presenter.Popup {
        private final NodeAction delegate;
        private final ActSubMenuModel model;

        public DelegateAction(NodeAction a, Lookup actionContext) {
            this.delegate = a;
            this.model = new ActSubMenuModel(actionContext);
        }

        public String toString() {
            return super.toString() + "[delegate=" + (Object)this.delegate + "]";
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.model.performActionAt(0);
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener listener) {
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener listener) {
        }

        @Override
        public void putValue(String key, Object o) {
        }

        @Override
        public Object getValue(String key) {
            if ("Name".equals(key)) {
                return NewAction.createName(this.model.newTypes());
            }
            return this.delegate.getValue(key);
        }

        @Override
        public boolean isEnabled() {
            return this.model.getCount() > 0;
        }

        @Override
        public void setEnabled(boolean b) {
        }

        public JMenuItem getMenuPresenter() {
            return new Actions.SubMenu((Action)this, (Actions.SubMenuModel)this.model, false);
        }

        public JMenuItem getPopupPresenter() {
            return new Actions.SubMenu((Action)this, (Actions.SubMenuModel)this.model, true);
        }
    }

    private static class ActSubMenuModel
    implements Actions.SubMenuModel {
        static final long serialVersionUID = -4273674308662494596L;
        final ChangeSupport cs;
        private Lookup lookup;
        private Node prevNode;
        private NewType[] prevTypes;

        ActSubMenuModel(Lookup lookup) {
            this.cs = new ChangeSupport((Object)this);
            this.lookup = lookup;
        }

        private NewType[] newTypes() {
            Collection c;
            if (this.lookup != null && (c = this.lookup.lookupResult(Node.class).allItems()).size() == 1) {
                for (Lookup.Item item : c) {
                    Node n = (Node)item.getInstance();
                    if (n == null) continue;
                    if (n == this.prevNode && this.prevTypes != null) {
                        return this.prevTypes;
                    }
                    this.prevNode = n;
                    this.prevTypes = n.getNewTypes();
                    return this.prevTypes;
                }
            }
            return NewAction.getNewTypes();
        }

        public int getCount() {
            return this.newTypes().length;
        }

        public String getLabel(int index) {
            NewType[] newTypes = this.newTypes();
            if (newTypes.length <= index) {
                return null;
            }
            return newTypes[index].getName();
        }

        public HelpCtx getHelpCtx(int index) {
            NewType[] newTypes = this.newTypes();
            if (newTypes.length <= index) {
                return null;
            }
            return newTypes[index].getHelpCtx();
        }

        public void performActionAt(int index) {
            NewType[] nt = this.newTypes();
            if (nt.length <= index) {
                return;
            }
            Node[] arr = this.lookup != null ? this.lookup.lookupAll(Node.class).toArray((T[])new Node[0]) : WindowManager.getDefault().getRegistry().getCurrentNodes();
            NewAction.performAction(arr, nt[index]);
        }

        public void addChangeListener(ChangeListener l) {
            this.cs.addChangeListener(l);
        }

        public void removeChangeListener(ChangeListener l) {
            this.cs.removeChangeListener(l);
        }
    }

}

