/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$InputLine
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.actions.NodeAction
 */
package org.openide.actions;

import java.awt.EventQueue;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.actions.NodeAction;

public class RenameAction
extends NodeAction {
    private static final RequestProcessor RP = new RequestProcessor(RenameAction.class);

    protected boolean surviveFocusChange() {
        return false;
    }

    public String getName() {
        return NbBundle.getMessage(RenameAction.class, (String)"Rename");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(RenameAction.class);
    }

    protected boolean enable(Node[] activatedNodes) {
        if (activatedNodes == null || activatedNodes.length != 1) {
            return false;
        }
        return activatedNodes[0].canRename();
    }

    protected void performAction(final Node[] activatedNodes) {
        if (activatedNodes == null || activatedNodes.length == 0) {
            return;
        }
        Node n = activatedNodes[0];
        if (EventQueue.isDispatchThread() && Boolean.TRUE.equals(n.getValue("slowRename"))) {
            RP.post(new Runnable(){

                @Override
                public void run() {
                    RenameAction.this.performAction(activatedNodes);
                }
            });
            return;
        }
        NotifyDescriptor.InputLine dlg = new NotifyDescriptor.InputLine(NbBundle.getMessage(RenameAction.class, (String)"CTL_RenameLabel"), NbBundle.getMessage(RenameAction.class, (String)"CTL_RenameTitle"));
        dlg.setInputText(n.getName());
        if (NotifyDescriptor.OK_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)dlg))) {
            String newname = null;
            try {
                newname = dlg.getInputText();
                if (!newname.equals("")) {
                    n.setName(dlg.getInputText());
                }
            }
            catch (IllegalArgumentException e) {
                boolean needToAnnotate;
                boolean bl = needToAnnotate = Exceptions.findLocalizedMessage((Throwable)e) == null;
                if (needToAnnotate) {
                    Exceptions.attachLocalizedMessage((Throwable)e, (String)NbBundle.getMessage(RenameAction.class, (String)"MSG_BadFormat", (Object)n.getName(), (Object)newname));
                }
                Exceptions.printStackTrace((Throwable)e);
            }
        }
    }

    protected boolean asynchronous() {
        return false;
    }

}

