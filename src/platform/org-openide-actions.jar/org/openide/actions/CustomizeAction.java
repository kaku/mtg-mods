/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeOperation
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.NodeAction
 */
package org.openide.actions;

import org.openide.nodes.Node;
import org.openide.nodes.NodeOperation;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;

public class CustomizeAction
extends NodeAction {
    protected void performAction(Node[] activatedNodes) {
        NodeOperation.getDefault().customize(activatedNodes[0]);
    }

    protected boolean asynchronous() {
        return false;
    }

    protected boolean enable(Node[] activatedNodes) {
        if (activatedNodes == null || activatedNodes.length != 1) {
            return false;
        }
        return activatedNodes[0].hasCustomizer();
    }

    public String getName() {
        return NbBundle.getMessage(CustomizeAction.class, (String)"Customize");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(CustomizeAction.class);
    }
}

