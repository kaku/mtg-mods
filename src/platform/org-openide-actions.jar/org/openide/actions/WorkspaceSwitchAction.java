/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.JMenuPlus
 *  org.openide.awt.Mnemonics
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallableSystemAction
 *  org.openide.windows.WindowManager
 *  org.openide.windows.Workspace
 */
package org.openide.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import org.openide.awt.JMenuPlus;
import org.openide.awt.Mnemonics;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallableSystemAction;
import org.openide.windows.WindowManager;
import org.openide.windows.Workspace;

@Deprecated
public class WorkspaceSwitchAction
extends CallableSystemAction {
    public String getName() {
        return NbBundle.getBundle(WorkspaceSwitchAction.class).getString("WorkspacesItems");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(WorkspaceSwitchAction.class);
    }

    public JMenuItem getMenuPresenter() {
        JMenuPlus menu = new JMenuPlus();
        Mnemonics.setLocalizedText((AbstractButton)menu, (String)this.getName());
        menu.setHorizontalTextPosition(4);
        menu.setHorizontalAlignment(2);
        menu.setIcon(this.getIcon());
        HelpCtx.setHelpIDString((JComponent)menu, (String)WorkspaceSwitchAction.class.getName());
        WindowManager pool = WindowManager.getDefault();
        Hashtable menu2Workspace = new Hashtable(10);
        Hashtable workspace2Menu = new Hashtable(10);
        Hashtable workspace2Listener = new Hashtable(10);
        Workspace[] currentDeskRef = new Workspace[]{pool.getCurrentWorkspace()};
        Workspace[] workspaces = pool.getWorkspaces();
        for (int i = 0; i < workspaces.length; ++i) {
            this.attachWorkspace(workspaces[i], currentDeskRef, workspace2Menu, menu2Workspace, workspace2Listener, (JMenu)menu);
        }
        JRadioButtonMenuItem curItem = (JRadioButtonMenuItem)workspace2Menu.get((Object)currentDeskRef[0]);
        if (curItem != null) {
            curItem.setSelected(true);
        }
        pool.addPropertyChangeListener(this.getWorkspacePoolListener(workspace2Menu, menu2Workspace, workspace2Listener, currentDeskRef, (JMenu)menu));
        return menu;
    }

    public void performAction() {
        assert (false);
    }

    private ActionListener createActionListener(JRadioButtonMenuItem menuItem, final Workspace[] currentDeskRef, final Hashtable menu2Workspace, final Hashtable workspace2Menu) {
        return new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                Workspace desk = (Workspace)menu2Workspace.get(this);
                if (desk == null) {
                    return;
                }
                if (workspace2Menu.get((Object)desk) == null) {
                    return;
                }
                ((JRadioButtonMenuItem)workspace2Menu.get((Object)desk)).setSelected(true);
                if (desk == currentDeskRef[0]) {
                    return;
                }
                if (currentDeskRef[0] != null) {
                    ((JRadioButtonMenuItem)workspace2Menu.get((Object)currentDeskRef[0])).setSelected(false);
                }
                currentDeskRef[0] = desk;
                desk.activate();
            }
        };
    }

    private PropertyChangeListener getWorkspacePoolListener(final Hashtable workspace2Menu, final Hashtable menu2Workspace, final Hashtable workspace2Listener, final Workspace[] currentDeskRef, final JMenu menu) {
        PropertyChangeListener pcl1 = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent che) {
                if (che.getPropertyName().equals("currentWorkspace")) {
                    Workspace newDesk = (Workspace)che.getNewValue();
                    if (currentDeskRef[0] == newDesk) {
                        return;
                    }
                    JRadioButtonMenuItem menu2 = (JRadioButtonMenuItem)workspace2Menu.get((Object)currentDeskRef[0]);
                    if (menu2 != null) {
                        menu2.setSelected(false);
                    }
                    currentDeskRef[0] = newDesk;
                    menu2 = (JRadioButtonMenuItem)workspace2Menu.get((Object)newDesk);
                    if (menu2 != null) {
                        menu2.setSelected(true);
                    }
                } else if (che.getPropertyName().equals("workspaces")) {
                    int i;
                    Workspace[] newWorkspaces = (Workspace[])che.getNewValue();
                    Workspace[] oldWorkspaces = (Workspace[])che.getOldValue();
                    List<Workspace> newList = Arrays.asList(newWorkspaces);
                    List<Workspace> oldList = Arrays.asList(oldWorkspaces);
                    for (i = 0; i < oldWorkspaces.length; ++i) {
                        if (newList.indexOf((Object)oldWorkspaces[i]) >= 0) continue;
                        WorkspaceSwitchAction.this.detachWorkspace(oldWorkspaces[i], workspace2Menu, menu2Workspace, workspace2Listener, menu);
                    }
                    for (i = 0; i < newWorkspaces.length; ++i) {
                        if (oldList.indexOf((Object)newWorkspaces[i]) >= 0) continue;
                        WorkspaceSwitchAction.this.attachWorkspace(newWorkspaces[i], currentDeskRef, workspace2Menu, menu2Workspace, workspace2Listener, menu);
                    }
                }
            }
        };
        return pcl1;
    }

    void attachWorkspace(Workspace workspace, Workspace[] currentDeskRef, Hashtable workspace2Menu, Hashtable menu2Workspace, Hashtable workspace2Listener, JMenu menu) {
        JRadioButtonMenuItem menuItem = new JRadioButtonMenuItem();
        Mnemonics.setLocalizedText((AbstractButton)menuItem, (String)workspace.getDisplayName());
        HelpCtx.setHelpIDString((JComponent)menuItem, (String)WorkspaceSwitchAction.class.getName());
        ActionListener listener = this.createActionListener(menuItem, currentDeskRef, menu2Workspace, workspace2Menu);
        menuItem.addActionListener(listener);
        menu2Workspace.put(listener, workspace);
        workspace2Listener.put(workspace, listener);
        workspace2Menu.put(workspace, menuItem);
        workspace.addPropertyChangeListener(this.createNameListener(menuItem));
        menu.add(menuItem);
    }

    void detachWorkspace(Workspace workspace, Hashtable workspace2Menu, Hashtable menu2Workspace, Hashtable workspace2Listener, JMenu menu) {
        JRadioButtonMenuItem menuItem = (JRadioButtonMenuItem)workspace2Menu.get((Object)workspace);
        workspace2Menu.remove((Object)workspace);
        menu2Workspace.remove(workspace2Listener.get((Object)workspace));
        workspace2Listener.remove((Object)workspace);
        menu.remove(menuItem);
    }

    private PropertyChangeListener createNameListener(final JRadioButtonMenuItem item) {
        return new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent ev) {
                if (ev.getPropertyName().equals("name")) {
                    item.setText((String)ev.getNewValue());
                }
            }
        };
    }

}

