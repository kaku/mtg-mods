/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallbackSystemAction
 */
package org.openide.actions;

import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallbackSystemAction;

public class FindAction
extends CallbackSystemAction {
    public String getName() {
        return NbBundle.getMessage(FindAction.class, (String)"Find");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(FindAction.class);
    }

    protected String iconResource() {
        return null;
    }

    protected boolean asynchronous() {
        return false;
    }
}

