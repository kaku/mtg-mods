/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 */
package org.openide.actions;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.Action;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;

public abstract class ActionManager {
    public static final String PROP_CONTEXT_ACTIONS = "contextActions";
    private PropertyChangeSupport supp = null;

    public static ActionManager getDefault() {
        ActionManager am = (ActionManager)Lookup.getDefault().lookup(ActionManager.class);
        if (am == null) {
            am = new Trivial();
        }
        return am;
    }

    public abstract SystemAction[] getContextActions();

    @Deprecated
    public void invokeAction(Action a, ActionEvent e) {
        a.actionPerformed(e);
    }

    public final void addPropertyChangeListener(PropertyChangeListener listener) {
        if (this.supp == null) {
            this.supp = new PropertyChangeSupport(this);
        }
        this.supp.addPropertyChangeListener(listener);
    }

    public final void removePropertyChangeListener(PropertyChangeListener listener) {
        if (this.supp != null) {
            this.supp.removePropertyChangeListener(listener);
        }
    }

    protected final void firePropertyChange(String name, Object o, Object n) {
        if (this.supp != null) {
            this.supp.firePropertyChange(name, o, n);
        }
    }

    private static final class Trivial
    extends ActionManager {
        private Trivial() {
        }

        @Override
        public SystemAction[] getContextActions() {
            return new SystemAction[0];
        }
    }

}

