/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.ViewCookie
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CookieAction
 */
package org.openide.actions;

import org.openide.cookies.ViewCookie;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CookieAction;

public class ViewAction
extends CookieAction {
    protected boolean surviveFocusChange() {
        return false;
    }

    public String getName() {
        return NbBundle.getBundle(ViewAction.class).getString("View");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(ViewAction.class);
    }

    protected int mode() {
        return 4;
    }

    protected Class[] cookieClasses() {
        return new Class[]{ViewCookie.class};
    }

    protected void performAction(Node[] activatedNodes) {
        if (activatedNodes == null) {
            return;
        }
        for (int i = 0; i < activatedNodes.length; ++i) {
            ViewCookie es = (ViewCookie)activatedNodes[i].getCookie(ViewCookie.class);
            if (es == null) continue;
            es.view();
        }
    }

    protected boolean asynchronous() {
        return false;
    }
}

