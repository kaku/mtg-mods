/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.actions.CallableSystemAction
 */
package org.openide.actions;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import javax.swing.JComponent;
import javax.swing.UIManager;
import org.openide.actions.HeapView;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.actions.CallableSystemAction;

public class GarbageCollectAction
extends CallableSystemAction {
    private static RequestProcessor RP;
    private static final boolean NIMBUS_LAF;

    public String getName() {
        return NbBundle.getBundle(GarbageCollectAction.class).getString("CTL_GarbageCollect");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(GarbageCollectAction.class);
    }

    public void performAction() {
        GarbageCollectAction.gc();
    }

    private static void gc() {
        if (RP == null) {
            RP = new RequestProcessor("GarbageCollectAction");
        }
        RP.post(new Runnable(){

            @Override
            public void run() {
                System.gc();
                System.runFinalization();
                System.gc();
            }
        });
    }

    protected boolean asynchronous() {
        return false;
    }

    public Component getToolbarPresenter() {
        return new HeapViewWrapper();
    }

    static {
        NIMBUS_LAF = "Nimbus".equals(UIManager.getLookAndFeel().getID());
    }

    private static final class HeapViewWrapper
    extends JComponent {
        public HeapViewWrapper() {
            this.add(new HeapView());
            this.setLayout(null);
        }

        @Override
        public boolean isOpaque() {
            return false;
        }

        @Override
        public Dimension getMinimumSize() {
            return this.calcPreferredSize();
        }

        @Override
        public Dimension getPreferredSize() {
            return this.calcPreferredSize();
        }

        @Override
        public Dimension getMaximumSize() {
            Dimension pref = this.calcPreferredSize();
            Container parent = this.getParent();
            if (parent != null && parent.getHeight() > 0) {
                pref.height = parent.getHeight();
            }
            return pref;
        }

        public Dimension calcPreferredSize() {
            Dimension pref = this.getHeapView().heapViewPreferredSize();
            ++pref.height;
            pref.width += 6;
            return pref;
        }

        @Override
        public void layout() {
            int w = this.getWidth();
            int h = this.getHeight();
            HeapView heapView = this.getHeapView();
            if (NIMBUS_LAF) {
                heapView.setBounds(0, 0, w, h);
            } else {
                heapView.setBounds(4, 2, w - 6, h - 4);
            }
        }

        private HeapView getHeapView() {
            return (HeapView)this.getComponent(0);
        }
    }

}

