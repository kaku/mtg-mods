/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.UndoRedo
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallableSystemAction
 */
package org.openide.actions;

import javax.swing.Action;
import javax.swing.UIManager;
import javax.swing.undo.CannotRedoException;
import org.openide.actions.UndoAction;
import org.openide.actions.UndoRedoAction;
import org.openide.awt.UndoRedo;
import org.openide.util.ContextAwareAction;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallableSystemAction;

public class RedoAction
extends CallableSystemAction
implements ContextAwareAction {
    private static String SWING_DEFAULT_LABEL = UIManager.getString("AbstractUndoableEdit.redoText");

    public boolean isEnabled() {
        UndoAction.initializeUndoRedo();
        return super.isEnabled();
    }

    public String getName() {
        String redo = UndoAction.getUndoRedo().getRedoPresentationName();
        if (redo != null && SWING_DEFAULT_LABEL != null && redo.startsWith(SWING_DEFAULT_LABEL)) {
            redo = redo.substring(SWING_DEFAULT_LABEL.length()).trim();
        }
        if (redo == null || redo.trim().length() == 0) {
            return NbBundle.getMessage(RedoAction.class, (String)"RedoSimple");
        }
        return NbBundle.getMessage(RedoAction.class, (String)"RedoWithParameter", (Object)redo);
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(RedoAction.class);
    }

    protected String iconResource() {
        return "org/openide/resources/actions/redo.gif";
    }

    public void performAction() {
        try {
            UndoRedo undoRedo = UndoAction.getUndoRedo();
            if (undoRedo.canRedo()) {
                undoRedo.redo();
            }
        }
        catch (CannotRedoException ex) {
            UndoRedoAction.cannotUndoRedo(ex);
        }
        UndoAction.updateStatus();
    }

    protected boolean asynchronous() {
        return false;
    }

    public Action createContextAwareInstance(Lookup actionContext) {
        return new UndoRedoAction(actionContext, false, false);
    }
}

