/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.UndoRedo
 *  org.openide.awt.UndoRedo$Provider
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.HelpCtx
 *  org.openide.util.HelpCtx$Provider
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 *  org.openide.windows.WindowManager
 */
package org.openide.actions;

import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.EventListener;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import org.openide.awt.UndoRedo;
import org.openide.util.ContextAwareAction;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

final class UndoRedoAction
extends AbstractAction
implements ContextAwareAction,
PropertyChangeListener,
ChangeListener,
LookupListener,
Runnable,
HelpCtx.Provider {
    private static final Logger LOG = Logger.getLogger(UndoRedoAction.class.getName());
    private UndoRedo last = UndoRedo.NONE;
    private final boolean doUndo;
    private final Lookup.Result<UndoRedo.Provider> result;
    private final boolean fallback;
    private PropertyChangeListener weakPCL;
    private ChangeListener weakCL;
    private LookupListener weakLL;

    UndoRedoAction(Lookup context, boolean doUndo, boolean fallback) {
        this.doUndo = doUndo;
        this.fallback = fallback;
        this.result = context.lookupResult(UndoRedo.Provider.class);
    }

    public String toString() {
        return Object.super.toString() + "[undo=" + this.doUndo + ", fallback: " + this.fallback + "]";
    }

    public static Action create(Map<?, ?> map) {
        if (Boolean.TRUE.equals(map.get("redo"))) {
            return new UndoRedoAction(Utilities.actionsGlobalContext(), false, true);
        }
        if (Boolean.TRUE.equals(map.get("undo"))) {
            return new UndoRedoAction(Utilities.actionsGlobalContext(), true, true);
        }
        throw new IllegalStateException();
    }

    @Override
    public boolean isEnabled() {
        this.initializeUndoRedo();
        return super.isEnabled();
    }

    void initializeUndoRedo() {
        assert (EventQueue.isDispatchThread());
        if (this.weakLL != null) {
            return;
        }
        String res = this.doUndo ? "org/openide/resources/actions/undo.gif" : "org/openide/resources/actions/redo.gif";
        this.putValue("iconBase", res);
        this.putValue("SmallIcon", ImageUtilities.loadImageIcon((String)res, (boolean)true));
        if (this.fallback) {
            TopComponent.Registry r = WindowManager.getDefault().getRegistry();
            this.weakPCL = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)r);
            r.addPropertyChangeListener(this.weakPCL);
        }
        this.weakCL = WeakListeners.change((ChangeListener)this, (Object)null);
        this.weakLL = (LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this, this.result);
        this.result.addLookupListener(this.weakLL);
        this.last = UndoRedo.NONE;
        this.run();
    }

    @Override
    public void run() {
        if (!EventQueue.isDispatchThread()) {
            EventQueue.invokeLater(this);
            return;
        }
        UndoRedo ur = this.getUndoRedo();
        this.last.removeChangeListener(this.weakCL);
        if (this.doUndo) {
            this.setEnabled(ur.canUndo());
        } else {
            this.setEnabled(ur.canRedo());
        }
        this.putValue("Name", this.getName());
        this.last = ur;
        this.last.addChangeListener(this.weakCL);
    }

    private UndoRedo getUndoRedo() {
        UndoRedo ur;
        TopComponent el;
        assert (EventQueue.isDispatchThread());
        for (UndoRedo.Provider provider : this.result.allInstances()) {
            UndoRedo ur2 = provider.getUndoRedo();
            if (ur2 == null) continue;
            return ur2;
        }
        if (this.fallback && (el = WindowManager.getDefault().getRegistry().getActivated()) != null && (ur = el.getUndoRedo()) != null) {
            return ur;
        }
        return UndoRedo.NONE;
    }

    private String getName() {
        assert (EventQueue.isDispatchThread());
        String undo = this.doUndo ? this.getUndoRedo().getUndoPresentationName() : this.getUndoRedo().getRedoPresentationName();
        LOG.log(Level.FINE, this.doUndo ? "getUndoRedo().getUndoPresentationName() returns {0}" : "getUndoRedo().getRedoPresentationName() returns {0}", undo);
        if (undo != null && this.getDefaultSwingText() != null && undo.startsWith(this.getDefaultSwingText())) {
            undo = undo.substring(this.getDefaultSwingText().length()).trim();
        }
        LOG.log(Level.FINE, "Name adapted by SWING_DEFAULT_LABEL is {0}", undo);
        String presentationName = null;
        presentationName = undo == null || undo.trim().length() == 0 ? NbBundle.getMessage(UndoRedoAction.class, (String)(this.doUndo ? "UndoSimple" : "RedoSimple")) : NbBundle.getMessage(UndoRedoAction.class, (String)(this.doUndo ? "UndoWithParameter" : "RedoWithParameter"), (Object)undo);
        LOG.log(Level.FINE, "Result name is {0}", presentationName);
        return presentationName;
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(UndoRedoAction.class);
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        UndoRedo undoRedo = this.getUndoRedo();
        if (this.doUndo) {
            try {
                if (undoRedo.canUndo()) {
                    undoRedo.undo();
                }
            }
            catch (CannotUndoException ex) {
                UndoRedoAction.cannotUndoRedo(ex);
            }
        } else {
            try {
                if (undoRedo.canRedo()) {
                    undoRedo.redo();
                }
            }
            catch (CannotRedoException ex) {
                UndoRedoAction.cannotUndoRedo(ex);
            }
        }
        this.run();
    }

    static void cannotUndoRedo(RuntimeException ex) throws MissingResourceException, HeadlessException {
        if (ex.getMessage() != null) {
            JOptionPane.showMessageDialog(WindowManager.getDefault().getMainWindow(), ex.getMessage(), NbBundle.getMessage(UndoRedoAction.class, (String)(ex instanceof CannotUndoException ? "LBL_CannotUndo" : "LBL_CannotRedo")), 0);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent ev) {
        if ("activated".equals(ev.getPropertyName())) {
            this.run();
        }
    }

    @Override
    public void stateChanged(ChangeEvent ev) {
        this.run();
    }

    public void resultChanged(LookupEvent ev) {
        this.run();
    }

    public Action createContextAwareInstance(Lookup actionContext) {
        return new UndoRedoAction(actionContext, this.doUndo, false);
    }

    private String getDefaultSwingText() {
        return this.doUndo ? UIManager.getString("AbstractUndoableEdit.undoText") : UIManager.getString("AbstractUndoableEdit.redoText");
    }
}

