/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.openide.actions;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.ImageObserver;
import java.awt.image.Kernel;
import java.text.MessageFormat;
import java.util.prefs.Preferences;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import org.openide.actions.GarbageCollectAction;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;

class HeapView
extends JComponent {
    private static final boolean AUTOMATIC_REFRESH = System.getProperty("org.netbeans.log.startup") == null;
    private static final int STYLE_DEFAULT = 0;
    private static final int STYLE_OVERLAY = 1;
    private static final int TICK = 1500;
    private static final int HEAP_GROW_ANIMATE_TIME = 1000;
    private static final int BORDER_W = 2;
    private static final int BORDER_H = 4;
    private static final Color[] GRID_COLORS = new Color[8];
    private static Color border1Color;
    private static Color border2Color;
    private static Color border3Color;
    private static Color minTickColor;
    private static Color maxTickColor;
    private static Color textBlurColor;
    private static Color textColor;
    private static Color background1Color;
    private static Color background2Color;
    private static final int KERNEL_SIZE = 3;
    private static final float BLUR_FACTOR = 0.1f;
    private static final int SHIFT_X = 0;
    private static final int SHIFT_Y = 1;
    private final ConvolveOp blur;
    private final MessageFormat format;
    private float[] graph;
    private int graphIndex;
    private boolean graphFilled;
    private long lastTotal;
    private Timer updateTimer;
    private Image bgImage;
    private int cachedWidth;
    private int cachedHeight;
    private BufferedImage textImage;
    private BufferedImage dropShadowImage;
    private HeapGrowTimer heapGrowTimer;
    private int maxTextWidth;
    private String heapSizeText;
    private Image tickGradientImage;
    private BufferedImage gridOverlayImage;
    private final RequestProcessor RP = new RequestProcessor(HeapView.class.getName());
    private static final String TICK_STYLE = "tickStyle";
    private static final String SHOW_TEXT = "showText";
    private static final String DROP_SHADOW = "dropShadow";
    private boolean containsMouse;
    private boolean cachedBorderVaild;

    public HeapView() {
        int kw = 3;
        int kh = 3;
        float blurFactor = 0.1f;
        float[] kernelData = new float[kw * kh];
        for (int i = 0; i < kernelData.length; ++i) {
            kernelData[i] = blurFactor;
        }
        this.blur = new ConvolveOp(new Kernel(kw, kh, kernelData));
        this.format = new MessageFormat("{0,choice,0#{0,number,0.0}|999<{0,number,0}}/{1,choice,0#{1,number,0.0}|999<{1,number,0}}MB");
        this.heapSizeText = "";
        this.enableEvents(16);
        this.setToolTipText(NbBundle.getMessage(GarbageCollectAction.class, (String)"CTL_GC"));
        this.updateUI();
    }

    @Override
    public boolean isOpaque() {
        return true;
    }

    @Override
    public void updateUI() {
        Font f = new JLabel().getFont();
        f = new Font(f.getName(), 1, f.getSize());
        this.setFont(f);
        this.revalidate();
        this.repaint();
    }

    public void setTickStyle(int style) {
        this.prefs().putInt("tickStyle", style);
        this.repaint();
    }

    public int getTickStyle() {
        return this.prefs().getInt("tickStyle", 1);
    }

    public void setShowText(boolean showText) {
        this.prefs().putBoolean("showText", showText);
        this.repaint();
    }

    public boolean getShowText() {
        return this.prefs().getBoolean("showText", true);
    }

    public void setShowDropShadow(boolean show) {
        this.prefs().putBoolean("dropShadow", show);
        this.repaint();
    }

    public boolean getShowDropShadow() {
        return this.prefs().getBoolean("dropShadow", true);
    }

    @Override
    public void setFont(Font font) {
        super.setFont(font);
        this.updateTextWidth();
    }

    Dimension heapViewPreferredSize() {
        Dimension size = new Dimension(this.maxTextWidth + 8, this.getFontMetrics(this.getFont()).getHeight() + 8);
        return size;
    }

    private Preferences prefs() {
        return NbPreferences.forModule(HeapView.class);
    }

    private void updateTextWidth() {
        String maxString = this.format.format(new Object[]{new Float(888.8f), new Float(888.8f)});
        this.maxTextWidth = this.getFontMetrics(this.getFont()).stringWidth(maxString) + 4;
    }

    @Override
    protected void processMouseEvent(MouseEvent e) {
        super.processMouseEvent(e);
        if (!e.isConsumed()) {
            if (e.isPopupTrigger()) {
                this.showPopup(e.getX(), e.getY());
            } else if (e.getID() == 504) {
                this.containsMouse = true;
                this.cachedBorderVaild = false;
                this.repaint();
            } else if (e.getID() == 505) {
                this.containsMouse = false;
                this.cachedBorderVaild = false;
                this.repaint();
            }
        }
        if (e.getID() == 500 && SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 1) {
            ((GarbageCollectAction)GarbageCollectAction.get(GarbageCollectAction.class)).performAction();
        }
    }

    private void showPopup(int x, int y) {
        JPopupMenu popup = new JPopupMenu();
        JCheckBoxMenuItem cbmi = new JCheckBoxMenuItem(NbBundle.getMessage(HeapView.class, (String)"LBL_ShowText"));
        cbmi.setSelected(this.getShowText());
        cbmi.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                HeapView.this.setShowText(((JCheckBoxMenuItem)e.getSource()).isSelected());
            }
        });
        popup.add(cbmi);
        cbmi = new JCheckBoxMenuItem(NbBundle.getMessage(HeapView.class, (String)"LBL_DropShadow"));
        cbmi.setSelected(this.getShowDropShadow());
        cbmi.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                HeapView.this.setShowDropShadow(((JCheckBoxMenuItem)e.getSource()).isSelected());
            }
        });
        popup.add(cbmi);
        cbmi = new JCheckBoxMenuItem(NbBundle.getMessage(HeapView.class, (String)"LBL_OverlayGrid"));
        cbmi.setSelected(this.getTickStyle() == 1);
        cbmi.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                int style = ((JCheckBoxMenuItem)e.getSource()).isSelected() ? 1 : 0;
                HeapView.this.setTickStyle(style);
            }
        });
        popup.add(cbmi);
        popup.show(this, x, y);
    }

    private int getGraphStartIndex() {
        if (this.graphFilled) {
            return this.graphIndex;
        }
        return 0;
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D)g;
        int width = this.getWidth();
        int height = this.getHeight();
        if (width - 2 > 0 && height - 4 > 0) {
            this.startTimerIfNecessary();
            this.updateCacheIfNecessary(width, height);
            this.paintCachedBackground(g2, width, height);
            g.translate(1, 2);
            if (this.containsMouse) {
                g.clipRect(1, 0, width - 4, height - 4);
            } else {
                g.clipRect(0, 0, width - 2, height - 4);
            }
            int innerW = width - 2;
            int innerH = height - 4;
            if (this.heapGrowTimer != null) {
                Composite lastComposite = ((Graphics2D)g).getComposite();
                float percent = 1.0f - this.heapGrowTimer.getPercent();
                ((Graphics2D)g).setComposite(AlphaComposite.getInstance(3, percent));
                g.drawImage(this.heapGrowTimer.image, 0, 0, null);
                ((Graphics2D)g).setComposite(lastComposite);
            }
            this.paintTicks(g2, innerW, innerH);
            if (this.getTickStyle() == 1) {
                g2.drawImage(this.getGridOverlayImage(), 0, 0, null);
            }
            if (this.getShowText()) {
                if (this.getShowDropShadow()) {
                    this.paintDropShadowText(g, innerW, innerH);
                } else {
                    g.setColor(textColor);
                    this.paintText(g, innerW, innerH);
                }
            }
            g.translate(-1, -2);
        } else {
            this.stopTimerIfNecessary();
            g.setColor(this.getBackground());
            g.fillRect(0, 0, width, height);
        }
    }

    private void paintTicks(Graphics2D g, int width, int height) {
        if (this.graphIndex > 0 || this.graphFilled) {
            float[] localGraph;
            int index = this.getGraphStartIndex();
            int x = 0;
            if (!this.graphFilled) {
                x = width - this.graphIndex;
            }
            if ((localGraph = this.graph) == null) {
                return;
            }
            float min = localGraph[index];
            index = (index + 1) % localGraph.length;
            while (index != this.graphIndex) {
                min = Math.min(min, localGraph[index]);
                index = (index + 1) % localGraph.length;
            }
            int minHeight = (int)(min * (float)height);
            if (minHeight > 0) {
                g.drawImage(this.tickGradientImage, x, height - minHeight, width, height, x, height - minHeight, width, height, null);
            }
            index = this.getGraphStartIndex();
            do {
                int tickHeight;
                if ((tickHeight = (int)(localGraph[index] * (float)height)) > minHeight) {
                    g.drawImage(this.tickGradientImage, x, height - tickHeight, x + 1, height - minHeight, x, height - tickHeight, x + 1, height - minHeight, null);
                }
                index = (index + 1) % localGraph.length;
                ++x;
            } while (index != this.graphIndex);
        }
    }

    private void paintText(Graphics g, int w, int h) {
        g.setFont(this.getFont());
        String text = this.getHeapSizeText();
        FontMetrics fm = g.getFontMetrics();
        int textWidth = fm.stringWidth(text);
        g.drawString(text, (w - this.maxTextWidth) / 2 + (this.maxTextWidth - textWidth), h / 2 + fm.getAscent() / 2 - 2);
    }

    private void paintDropShadowText(Graphics g, int w, int h) {
        BufferedImage dsi = this.dropShadowImage;
        BufferedImage tsi = this.textImage;
        if (dsi != null && tsi != null) {
            Graphics2D blurryImageG = dsi.createGraphics();
            blurryImageG.setComposite(AlphaComposite.Clear);
            blurryImageG.fillRect(0, 0, w, h);
            blurryImageG.setComposite(AlphaComposite.SrcOver);
            blurryImageG.drawImage(tsi, this.blur, 0, 1);
            blurryImageG.setColor(textColor);
            blurryImageG.setFont(this.getFont());
            this.paintText(blurryImageG, w, h);
            blurryImageG.dispose();
            g.drawImage(dsi, 0, 0, null);
        } else {
            class InitTextAndDropShadow
            implements Runnable {
                final /* synthetic */ int val$w;
                final /* synthetic */ int val$h;

                InitTextAndDropShadow() {
                    this.val$w = n;
                    this.val$h = n2;
                }

                @Override
                public void run() {
                    BufferedImage ti = new BufferedImage(this.val$w, this.val$h, 2);
                    BufferedImage ds = new BufferedImage(this.val$w, this.val$h, 2);
                    Graphics2D textImageG = ti.createGraphics();
                    textImageG.setComposite(AlphaComposite.Clear);
                    textImageG.fillRect(0, 0, this.val$w, this.val$h);
                    textImageG.setComposite(AlphaComposite.SrcOver);
                    textImageG.setColor(textBlurColor);
                    this$0.paintText(textImageG, this.val$w, this.val$h);
                    textImageG.dispose();
                    this$0.textImage = ti;
                    this$0.dropShadowImage = ds;
                    this$0.repaint();
                }
            }
            this.RP.post((Runnable)new InitTextAndDropShadow(this, w, h));
        }
    }

    private String getHeapSizeText() {
        return this.heapSizeText;
    }

    private void paintGridOverlay(Graphics2D g, int w, int h) {
        int numCells = GRID_COLORS.length / 2;
        int cellSize = (h - numCells - 1) / numCells;
        int c1 = 13683900;
        int c2 = 15394775;
        g.setPaint(new GradientPaint(0.0f, 0.0f, new Color(c1 >> 16 & 255, c1 >> 8 & 255, c1 & 255, 48), 0.0f, h, new Color(c2 >> 16 & 255, c2 >> 8 & 255, c2 & 255, 64)));
        for (int x = 0; x < w; x += cellSize + 1) {
            g.fillRect(x, 0, 1, h);
        }
        for (int y = h - cellSize - 1; y >= 0; y -= cellSize + 1) {
            g.fillRect(0, y, w, 1);
        }
    }

    private void paintCachedBackground(Graphics2D g, int w, int h) {
        if (this.bgImage != null) {
            g.drawImage(this.bgImage, 0, 0, null);
        }
    }

    private void paintBackgroundTiles(Graphics2D g, int w, int h) {
        g.translate(1, 2);
        w -= 2;
        int numCells = GRID_COLORS.length / 2;
        int cellSize = ((h -= 4) - numCells - 1) / numCells;
        for (int i = 0; i < numCells; ++i) {
            int colorIndex = i;
            int y = h - cellSize * (i + 1) - i;
            int x = 1;
            g.setPaint(new GradientPaint(0.0f, y, GRID_COLORS[colorIndex * 2], 0.0f, y + cellSize - 1, GRID_COLORS[colorIndex * 2 + 1]));
            while (x < w) {
                int endX = Math.min(w, x + cellSize);
                g.fillRect(x, y, endX - x, cellSize);
                x = endX + 1;
            }
            y += cellSize + 1;
        }
        g.translate(-1, -2);
    }

    private void paintBackground(Graphics2D g, int w, int h) {
        g.setPaint(new GradientPaint(0.0f, 0.0f, background1Color, 0.0f, h, background2Color));
        g.fillRect(0, 0, w, h);
    }

    private void paintBorder(Graphics g, int w, int h) {
        if (this.containsMouse) {
            g.setColor(border3Color);
            g.drawRect(0, 0, w - 1, h - 1);
            g.drawRect(1, 1, w - 3, h - 3);
        } else {
            g.setColor(border1Color);
            g.drawRect(0, 0, w - 1, h - 2);
            g.setColor(border2Color);
            g.fillRect(1, 1, w - 2, 1);
            g.setColor(border3Color);
            g.fillRect(0, h - 1, w, 1);
        }
    }

    private void updateCacheIfNecessary(int w, int h) {
        if (this.cachedWidth != w || this.cachedHeight != h || !this.cachedBorderVaild) {
            this.cachedWidth = w;
            this.cachedHeight = h;
            this.cachedBorderVaild = true;
            this.updateCache(w, h);
        }
    }

    private Image getGridOverlayImage() {
        if (this.gridOverlayImage == null) {
            this.gridOverlayImage = new BufferedImage(this.getInnerWidth(), this.getInnerHeight(), 2);
            Graphics2D g = this.gridOverlayImage.createGraphics();
            this.paintGridOverlay(g, this.getInnerWidth(), this.getInnerHeight());
            g.dispose();
        }
        return this.gridOverlayImage;
    }

    private void updateCache(int w, int h) {
        this.disposeImages();
        this.textImage = null;
        this.dropShadowImage = null;
        this.bgImage = this.createImage(w, h);
        if (this.bgImage == null) {
            return;
        }
        Graphics2D imageG = (Graphics2D)this.bgImage.getGraphics();
        this.paintBackground(imageG, w, h);
        this.paintBackgroundTiles(imageG, w, h);
        this.paintBorder(imageG, w, h);
        imageG.dispose();
        h -= 4;
        if (this.graph == null || this.graph.length != (w -= 2)) {
            this.graph = new float[w];
            this.graphFilled = false;
            this.graphIndex = 0;
        }
        GradientPaint tickGradient = new GradientPaint(0.0f, h, minTickColor, w, 0.0f, maxTickColor);
        this.tickGradientImage = this.createImage(w, h);
        imageG = (Graphics2D)this.tickGradientImage.getGraphics();
        imageG.setPaint(tickGradient);
        imageG.fillRect(0, 0, w, h);
        imageG.dispose();
        if (this.gridOverlayImage != null) {
            this.gridOverlayImage.flush();
            this.gridOverlayImage = null;
        }
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.stopTimerIfNecessary();
    }

    private void startTimerIfNecessary() {
        if (!AUTOMATIC_REFRESH) {
            return;
        }
        if (this.updateTimer == null) {
            this.updateTimer = new Timer(1500, new ActionHandler());
            this.updateTimer.setRepeats(true);
            this.updateTimer.start();
        }
    }

    private void stopTimerIfNecessary() {
        if (this.updateTimer != null) {
            this.graph = null;
            this.graphFilled = false;
            this.updateTimer.stop();
            this.updateTimer = null;
            this.lastTotal = 0;
            this.disposeImages();
            this.cachedHeight = -1;
            this.cachedHeight = -1;
            if (this.heapGrowTimer != null) {
                this.heapGrowTimer.stop();
                this.heapGrowTimer = null;
            }
        }
    }

    private void disposeImages() {
        if (this.bgImage != null) {
            this.bgImage.flush();
            this.bgImage = null;
        }
        if (this.textImage != null) {
            this.textImage.flush();
            this.textImage = null;
        }
        if (this.dropShadowImage != null) {
            this.dropShadowImage.flush();
            this.dropShadowImage = null;
        }
        if (this.tickGradientImage != null) {
            this.tickGradientImage.flush();
            this.tickGradientImage = null;
        }
        if (this.gridOverlayImage != null) {
            this.gridOverlayImage.flush();
            this.gridOverlayImage = null;
        }
    }

    private void update() {
        if (!this.isShowing()) {
            this.stopTimerIfNecessary();
            return;
        }
        Runtime r = Runtime.getRuntime();
        long total = r.totalMemory();
        float[] localGraph = this.graph;
        if (localGraph == null) {
            return;
        }
        if (total != this.lastTotal) {
            if (this.lastTotal != 0) {
                this.startHeapAnimate();
                int index = this.getGraphStartIndex();
                do {
                    localGraph[index] = (float)((double)localGraph[index] * (double)this.lastTotal / (double)total);
                } while ((index = (index + 1) % localGraph.length) != this.graphIndex);
            }
            this.lastTotal = total;
        }
        if (this.heapGrowTimer == null) {
            long used = total - r.freeMemory();
            localGraph[this.graphIndex] = (float)((double)used / (double)total);
            this.graphIndex = (this.graphIndex + 1) % localGraph.length;
            if (this.graphIndex == 0) {
                this.graphFilled = true;
            }
            this.heapSizeText = this.format.format(new Object[]{new Double((double)used / 1024.0 / 1024.0), new Double((double)total / 1024.0 / 1024.0)});
        }
        this.repaint();
    }

    private void startHeapAnimate() {
        if (this.heapGrowTimer == null) {
            this.heapGrowTimer = new HeapGrowTimer();
            this.heapGrowTimer.start();
        }
    }

    private void stopHeapAnimate() {
        if (this.heapGrowTimer != null) {
            this.heapGrowTimer.stop();
            this.heapGrowTimer = null;
        }
    }

    private int getInnerWidth() {
        return this.getWidth() - 2;
    }

    private int getInnerHeight() {
        return this.getHeight() - 4;
    }

    static {
        Color c = UIManager.getColor("nb.heapview.border1");
        if (null == c) {
            c = new Color(10920597);
        }
        border1Color = c;
        c = UIManager.getColor("nb.heapview.border2");
        if (null == c) {
            c = new Color(12631213);
        }
        border2Color = c;
        c = UIManager.getColor("nb.heapview.border3");
        if (null == c) {
            c = Color.WHITE;
        }
        border3Color = c;
        c = UIManager.getColor("nb.heapview.mintick.color");
        if (null == c) {
            c = new Color(13096621);
        }
        minTickColor = c;
        c = UIManager.getColor("nb.heapview.maxtick.color");
        if (null == c) {
            c = new Color(6380815);
        }
        maxTickColor = c;
        c = UIManager.getColor("nb.heapview.textblur");
        if (null == c) {
            c = Color.WHITE;
        }
        textBlurColor = c;
        c = UIManager.getColor("nb.heapview.foreground");
        if (null == c) {
            c = Color.WHITE;
        }
        textColor = c;
        c = UIManager.getColor("nb.heapview.background1");
        if (null == c) {
            c = new Color(13683900);
        }
        background1Color = c;
        c = UIManager.getColor("nb.heapview.background2");
        if (null == c) {
            c = new Color(15394775);
        }
        background2Color = c;
        c = UIManager.getColor("nb.heapview.grid1.start");
        if (null == c) {
            c = new Color(14933967);
        }
        HeapView.GRID_COLORS[0] = c;
        c = UIManager.getColor("nb.heapview.grid1.end");
        if (null == c) {
            c = new Color(15197395);
        }
        HeapView.GRID_COLORS[1] = c;
        c = UIManager.getColor("nb.heapview.grid2.start");
        if (null == c) {
            c = new Color(14342086);
        }
        HeapView.GRID_COLORS[2] = c;
        c = UIManager.getColor("nb.heapview.grid2.end");
        if (null == c) {
            c = new Color(14671051);
        }
        HeapView.GRID_COLORS[3] = c;
        c = UIManager.getColor("nb.heapview.grid3.start");
        if (null == c) {
            c = new Color(13881279);
        }
        HeapView.GRID_COLORS[4] = c;
        c = UIManager.getColor("nb.heapview.grid3.end");
        if (null == c) {
            c = new Color(14144451);
        }
        HeapView.GRID_COLORS[5] = c;
        c = UIManager.getColor("nb.heapview.grid4.start");
        if (null == c) {
            c = new Color(13552314);
        }
        HeapView.GRID_COLORS[6] = c;
        c = UIManager.getColor("nb.heapview.grid4.end");
        if (null == c) {
            c = new Color(13683900);
        }
        HeapView.GRID_COLORS[7] = c;
    }

    private final class HeapGrowTimer
    extends Timer {
        private final long startTime;
        private float percent;
        BufferedImage image;

        HeapGrowTimer() {
            super(30, null);
            this.setRepeats(true);
            this.startTime = System.currentTimeMillis();
            this.percent = 0.0f;
            int w = HeapView.this.getWidth() - 2;
            int h = HeapView.this.getHeight() - 4;
            this.image = new BufferedImage(w, h, 2);
            Graphics2D g = this.image.createGraphics();
            HeapView.this.paintTicks(g, w, h);
            g.dispose();
        }

        public float getPercent() {
            return this.percent;
        }

        @Override
        protected void fireActionPerformed(ActionEvent e) {
            long time = System.currentTimeMillis();
            long delta = Math.max(0, time - this.startTime);
            if (delta > 1000) {
                HeapView.this.stopHeapAnimate();
            } else {
                this.percent = (float)delta / 1000.0f;
                HeapView.this.repaint();
            }
        }
    }

    private final class ActionHandler
    implements ActionListener {
        private ActionHandler() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            HeapView.this.update();
        }
    }

}

