/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallbackSystemAction
 */
package org.openide.actions;

import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallbackSystemAction;

public class DeleteAction
extends CallbackSystemAction {
    public DeleteAction() {
        this.putValue("noIconInMenu", (Object)Boolean.TRUE);
    }

    protected void initialize() {
        super.initialize();
    }

    public Object getActionMapKey() {
        return "delete";
    }

    public String getName() {
        return NbBundle.getMessage(DeleteAction.class, (String)"Delete");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(DeleteAction.class);
    }

    protected String iconResource() {
        return "org/openide/resources/actions/delete.gif";
    }

    protected boolean asynchronous() {
        return true;
    }
}

