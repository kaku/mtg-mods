/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Actions
 *  org.openide.awt.Actions$MenuItem
 *  org.openide.awt.Actions$ToolbarButton
 *  org.openide.awt.JInlineMenu
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.nodes.NodeOperation
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.NodeAction
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.util.actions.Presenter$Toolbar
 *  org.openide.util.actions.SystemAction
 */
package org.openide.actions;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import javax.swing.Action;
import javax.swing.JMenuItem;
import org.openide.actions.CustomizeAction;
import org.openide.awt.Actions;
import org.openide.awt.JInlineMenu;
import org.openide.nodes.Node;
import org.openide.nodes.NodeOperation;
import org.openide.util.ContextAwareAction;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;
import org.openide.util.actions.Presenter;
import org.openide.util.actions.SystemAction;

public class PropertiesAction
extends NodeAction {
    protected void performAction(Node[] activatedNodes) {
        if (activatedNodes.length == 1) {
            NodeOperation.getDefault().showProperties(activatedNodes[0]);
        } else {
            NodeOperation.getDefault().showProperties(activatedNodes);
        }
    }

    protected boolean asynchronous() {
        return false;
    }

    protected boolean enable(Node[] activatedNodes) {
        return activatedNodes != null;
    }

    public JMenuItem getPopupPresenter() {
        Actions.MenuItem prop = new Actions.MenuItem((SystemAction)this, false);
        CustomizeAction customizeAction = (CustomizeAction)SystemAction.get(CustomizeAction.class);
        if (customizeAction.isEnabled()) {
            JInlineMenu mi = new JInlineMenu();
            mi.setMenuItems(new JMenuItem[]{new Actions.MenuItem((SystemAction)customizeAction, false), prop});
            return mi;
        }
        return prop;
    }

    public String getName() {
        return NbBundle.getMessage(PropertiesAction.class, (String)"Properties");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(PropertiesAction.class);
    }

    protected String iconResource() {
        return "org/openide/resources/actions/properties.gif";
    }

    public Action createContextAwareInstance(Lookup actionContext) {
        return new DelegateAction(this, actionContext);
    }

    private static final class DelegateAction
    implements Action,
    Presenter.Menu,
    Presenter.Toolbar,
    Presenter.Popup {
        private PropertiesAction delegate;
        private Lookup lookup;

        public DelegateAction(PropertiesAction a, Lookup actionContext) {
            this.delegate = a;
            this.lookup = actionContext;
        }

        private Node[] nodes() {
            Collection c = this.lookup.lookupAll(Node.class);
            return c.toArray((T[])new Node[c.size()]);
        }

        public String toString() {
            return super.toString() + "[delegate=" + (Object)((Object)this.delegate) + "]";
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.delegate.performAction(this.nodes());
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener listener) {
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener listener) {
        }

        @Override
        public void putValue(String key, Object o) {
        }

        @Override
        public Object getValue(String key) {
            return this.delegate.getValue(key);
        }

        @Override
        public boolean isEnabled() {
            return this.delegate.enable(this.nodes());
        }

        @Override
        public void setEnabled(boolean b) {
            assert (false);
        }

        public JMenuItem getMenuPresenter() {
            return new Actions.MenuItem((Action)this, true);
        }

        public JMenuItem getPopupPresenter() {
            Actions.MenuItem prop = new Actions.MenuItem((Action)this, false);
            Object customizeAction = SystemAction.get(CustomizeAction.class);
            if (this.lookup != null) {
                customizeAction = ((ContextAwareAction)customizeAction).createContextAwareInstance(this.lookup);
            }
            if (customizeAction.isEnabled()) {
                JInlineMenu mi = new JInlineMenu();
                mi.setMenuItems(new JMenuItem[]{new Actions.MenuItem((Action)customizeAction, false), prop});
                return mi;
            }
            for (Node n : this.nodes()) {
                for (Node.PropertySet ps : n.getPropertySets()) {
                    if (ps.getProperties().length <= 0) continue;
                    return prop;
                }
            }
            return new JInlineMenu();
        }

        public Component getToolbarPresenter() {
            return new Actions.ToolbarButton((Action)this);
        }
    }

}

