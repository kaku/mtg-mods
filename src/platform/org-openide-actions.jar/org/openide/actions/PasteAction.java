/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Actions
 *  org.openide.awt.Actions$SubMenu
 *  org.openide.awt.Actions$SubMenuModel
 *  org.openide.awt.Actions$ToolbarButton
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeMemberEvent
 *  org.openide.nodes.NodeReorderEvent
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.SharedClassObject
 *  org.openide.util.UserCancelException
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.CallbackSystemAction
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.util.actions.Presenter$Toolbar
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.datatransfer.PasteType
 *  org.openide.windows.TopComponent
 */
package org.openide.actions;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.MenuShortcut;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EventListener;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JMenuItem;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.actions.ActionManager;
import org.openide.awt.Actions;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.ChangeSupport;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.SharedClassObject;
import org.openide.util.UserCancelException;
import org.openide.util.WeakListeners;
import org.openide.util.actions.CallbackSystemAction;
import org.openide.util.actions.Presenter;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.PasteType;
import org.openide.windows.TopComponent;

public final class PasteAction
extends CallbackSystemAction {
    private static ActSubMenuModel globalModel;
    private static PasteType[] types;

    private static synchronized ActSubMenuModel model() {
        if (globalModel == null) {
            globalModel = new ActSubMenuModel(null);
        }
        return globalModel;
    }

    protected void initialize() {
        super.initialize();
        this.setEnabled(false);
    }

    public String getName() {
        return NbBundle.getMessage(PasteAction.class, (String)"Paste");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(PasteAction.class);
    }

    protected String iconResource() {
        return "org/openide/resources/actions/paste.gif";
    }

    public JMenuItem getMenuPresenter() {
        return new Actions.SubMenu((SystemAction)this, (Actions.SubMenuModel)PasteAction.model(), false);
    }

    public JMenuItem getPopupPresenter() {
        return new Actions.SubMenu((SystemAction)this, (Actions.SubMenuModel)PasteAction.model(), true);
    }

    public Action createContextAwareInstance(Lookup actionContext) {
        return new DelegateAction(this, actionContext);
    }

    public Object getActionMapKey() {
        return "paste-from-clipboard";
    }

    public void actionPerformed(ActionEvent ev) {
        Action ac;
        PasteType[] arr;
        PasteType t = ev.getSource() instanceof PasteType ? (PasteType)ev.getSource() : ((arr = this.getPasteTypes()) != null && arr.length > 0 ? arr[0] : null);
        if (t == null && (ac = PasteAction.findActionFromActivatedTopComponentMap()) != null) {
            Object obj = ac.getValue("delegates");
            if (obj instanceof PasteType[]) {
                PasteType[] arr2 = (PasteType[])obj;
                if (arr2.length > 0) {
                    t = arr2[0];
                }
            } else if (obj instanceof Action[]) {
                Action[] arr3 = (Action[])obj;
                if (arr3.length > 0) {
                    arr3[0].actionPerformed(ev);
                    return;
                }
            } else {
                ac.actionPerformed(ev);
                return;
            }
        }
        if (t != null) {
            new ActionPT(t, ev.getActionCommand());
        } else {
            Toolkit.getDefaultToolkit().beep();
            Logger.getLogger(PasteAction.class.getName()).log(Level.INFO, "No paste types available when performing paste action. ActionEvent: {0}", ev);
        }
    }

    protected boolean asynchronous() {
        return false;
    }

    @Deprecated
    public void setPasteTypes(PasteType[] types) {
        PasteAction.types = types;
        if (types == null || types.length == 0) {
            this.setEnabled(false);
        } else {
            this.setEnabled(true);
        }
        PasteAction.model().checkStateChanged(true);
    }

    public PasteType[] getPasteTypes() {
        return types;
    }

    private static Action findActionFromActivatedTopComponentMap() {
        TopComponent tc = TopComponent.getRegistry().getActivated();
        if (tc != null) {
            ActionMap map = tc.getActionMap();
            return PasteAction.findActionFromMap(map);
        }
        return null;
    }

    private static Action findActionFromMap(ActionMap map) {
        if (map != null) {
            return map.get("paste-from-clipboard");
        }
        return null;
    }

    private static Clipboard getClipboard() {
        Clipboard c = (Clipboard)Lookup.getDefault().lookup(Clipboard.class);
        if (c == null) {
            c = Toolkit.getDefaultToolkit().getSystemClipboard();
        }
        return c;
    }

    static ExplorerManager findExplorerManager() {
        Exception t = null;
        try {
            Class c = Class.forName("org.openide.windows.TopComponent");
            Method m = c.getMethod("getRegistry", new Class[0]);
            Object o = m.invoke(null, new Object[0]);
            c = Class.forName("org.openide.windows.TopComponent$Registry");
            m = c.getMethod("getActivated", new Class[0]);
            o = m.invoke(o, new Object[0]);
            if (o instanceof ExplorerManager.Provider) {
                return ((ExplorerManager.Provider)o).getExplorerManager();
            }
        }
        catch (ClassNotFoundException x) {
        }
        catch (ExceptionInInitializerError x) {
        }
        catch (LinkageError x) {
        }
        catch (SecurityException x) {
            t = x;
        }
        catch (NoSuchMethodException x) {
            t = x;
        }
        catch (IllegalAccessException x) {
            t = x;
        }
        catch (IllegalArgumentException x) {
            t = x;
        }
        catch (InvocationTargetException x) {
            t = x;
        }
        if (t != null) {
            Logger.getLogger(PasteAction.class.getName()).log(Level.WARNING, null, t);
        }
        return null;
    }

    private static final class ActionPT
    extends AbstractAction
    implements Runnable {
        private static final RequestProcessor RP = new RequestProcessor("Pasting");
        private PasteType t;
        private NodeSelector sel;
        private boolean secondInvocation;

        public ActionPT(PasteType t, String command) {
            this.t = t;
            ExplorerManager em = PasteAction.findExplorerManager();
            if (em != null) {
                this.sel = new NodeSelector(em, null);
            }
            if ("waitFinished".equals(command)) {
                this.run();
            } else {
                RP.post((Runnable)this);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent ev) {
            try {
                Transferable trans = this.t.paste();
                Clipboard clipboard = PasteAction.getClipboard();
                if (trans != null) {
                    ClipboardOwner owner = trans instanceof ClipboardOwner ? (ClipboardOwner)((Object)trans) : new StringSelection("");
                    clipboard.setContents(trans, owner);
                }
            }
            catch (UserCancelException exc) {}
            catch (IOException e) {
                Exceptions.printStackTrace((Throwable)e);
            }
            finally {
                EventQueue.invokeLater(this);
            }
        }

        @Override
        public void run() {
            if (this.secondInvocation) {
                if (this.sel != null) {
                    this.sel.select();
                }
            } else {
                this.secondInvocation = true;
                ActionManager.getDefault().invokeAction(this, new ActionEvent((Object)this.t, 1001, "Name"));
            }
        }

        @Override
        public boolean isEnabled() {
            return ((PasteAction)SystemAction.get(PasteAction.class)).isEnabled();
        }

        @Override
        public Object getValue(String key) {
            return ((PasteAction)SystemAction.get(PasteAction.class)).getValue(key);
        }
    }

    private static final class DelegateAction
    extends AbstractAction
    implements Presenter.Menu,
    Presenter.Popup,
    Presenter.Toolbar,
    ChangeListener {
        private PasteAction delegate;
        private ActSubMenuModel model;

        public DelegateAction(PasteAction a, Lookup actionContext) {
            this.delegate = a;
            this.model = new ActSubMenuModel(actionContext);
            this.model.addChangeListener(this);
        }

        public String toString() {
            return Object.super.toString() + "[delegate=" + (Object)((Object)this.delegate) + "]";
        }

        @Override
        public void putValue(String key, Object value) {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (this.model != null) {
                this.model.performActionAt(0, e);
            }
        }

        @Override
        public boolean isEnabled() {
            return this.model != null && this.model.isEnabled();
        }

        @Override
        public Object getValue(String key) {
            return this.delegate.getValue(key);
        }

        @Override
        public void setEnabled(boolean b) {
        }

        public JMenuItem getMenuPresenter() {
            return new Actions.SubMenu((Action)this, (Actions.SubMenuModel)this.model, false);
        }

        public JMenuItem getPopupPresenter() {
            return new Actions.SubMenu((Action)this, (Actions.SubMenuModel)this.model, true);
        }

        public Component getToolbarPresenter() {
            return new Actions.ToolbarButton((Action)this);
        }

        @Override
        public void stateChanged(ChangeEvent evt) {
            this.firePropertyChange("enabled", null, null);
        }
    }

    static final class NodeSelector
    implements NodeListener,
    Runnable {
        private List<Node> added;
        private Node node;
        private ExplorerManager em;
        private Node[] children;

        public NodeSelector(ExplorerManager em, Node[] n) {
            this.em = em;
            if (n != null && n.length > 0) {
                this.node = n[0];
            } else {
                Node[] arr = em.getSelectedNodes();
                if (arr.length != 0) {
                    this.node = arr[0];
                } else {
                    return;
                }
            }
            this.children = this.node.getChildren().getNodes(true);
            this.added = new ArrayList<Node>();
            this.node.addNodeListener((NodeListener)this);
        }

        public void select() {
            if (this.added != null) {
                this.node.getChildren().getNodes(true);
                Children.MUTEX.readAccess((Runnable)this);
            }
        }

        @Override
        public void run() {
            this.node.removeNodeListener((NodeListener)this);
            if (this.added.isEmpty()) {
                return;
            }
            Iterator<Node> i$ = this.added.iterator();
            block3 : while (i$.hasNext()) {
                for (Node n = i$.next(); n != null; n = n.getParentNode()) {
                    if (n.equals((Object)this.em.getRootContext())) continue block3;
                }
                return;
            }
            try {
                this.em.setSelectedNodes(this.added.toArray((T[])new Node[this.added.size()]));
            }
            catch (PropertyVetoException ex) {
                Logger.getLogger(PasteAction.class.getName()).log(Level.WARNING, null, ex);
            }
            catch (IllegalStateException ex) {
                Logger.getLogger(PasteAction.class.getName()).log(Level.WARNING, null, ex);
            }
        }

        public void childrenAdded(NodeMemberEvent ev) {
            this.added.addAll(Arrays.asList(ev.getDelta()));
        }

        public void childrenRemoved(NodeMemberEvent ev) {
        }

        public void childrenReordered(NodeReorderEvent ev) {
        }

        public void nodeDestroyed(NodeEvent ev) {
        }

        public void propertyChange(PropertyChangeEvent evt) {
        }
    }

    private static class ActSubMenuModel
    implements Actions.SubMenuModel,
    LookupListener,
    PropertyChangeListener {
        private final ChangeSupport cs;
        private Lookup.Result<ActionMap> result;
        private boolean enabled;
        private PropertyChangeListener actionWeakL;
        private PropertyChangeListener pasteTypeWeakL;
        private LookupListener weakLookup;

        public ActSubMenuModel(Lookup lookup) {
            this.cs = new ChangeSupport((Object)this);
            this.attachListenerToChangesInMap(lookup);
        }

        private ActionMap map() {
            if (this.result == null) {
                TopComponent tc = TopComponent.getRegistry().getActivated();
                if (tc != null) {
                    return tc.getActionMap();
                }
            } else {
                Iterator i$ = this.result.allInstances().iterator();
                if (i$.hasNext()) {
                    ActionMap am = (ActionMap)i$.next();
                    return am;
                }
            }
            return null;
        }

        private void attachListenerToChangesInMap(Lookup lookup) {
            if (lookup == null) {
                TopComponent.getRegistry().addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)TopComponent.getRegistry()));
            } else {
                this.result = lookup.lookupResult(ActionMap.class);
                this.weakLookup = (LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this, this.result);
                this.result.addLookupListener(this.weakLookup);
            }
            this.checkStateChanged(false);
        }

        private Object[] getPasteTypesOrActions(Action[] actionToWorkWith) {
            Object obj;
            Action x = PasteAction.findActionFromMap(this.map());
            if (x == null) {
                Object[] arr;
                PasteAction a = (PasteAction)SharedClassObject.findObject(PasteAction.class);
                if (actionToWorkWith != null) {
                    actionToWorkWith[0] = a;
                }
                Object[] arrobject = arr = a == null ? null : a.getPasteTypes();
                if (arr != null) {
                    return arr;
                }
                return new Object[0];
            }
            if (actionToWorkWith != null) {
                actionToWorkWith[0] = x;
            }
            if ((obj = x.getValue("delegates")) instanceof Object[]) {
                return (Object[])obj;
            }
            return new Object[]{x};
        }

        private boolean isEnabledImpl(Object[] pasteTypesOrActions) {
            if (pasteTypesOrActions == null) {
                pasteTypesOrActions = this.getPasteTypesOrActions(null);
            }
            if (pasteTypesOrActions.length == 1 && pasteTypesOrActions[0] instanceof Action) {
                return ((Action)pasteTypesOrActions[0]).isEnabled();
            }
            return pasteTypesOrActions.length > 0;
        }

        public boolean isEnabled() {
            return this.isEnabledImpl(null);
        }

        public int getCount() {
            return this.getPasteTypesOrActions(null).length;
        }

        public String getLabel(int index) {
            Object[] arr = this.getPasteTypesOrActions(null);
            if (arr.length <= index) {
                return null;
            }
            if (arr[index] instanceof PasteType) {
                return ((PasteType)arr[index]).getName();
            }
            return (String)((Action)arr[index]).getValue("Name");
        }

        public HelpCtx getHelpCtx(int index) {
            Object[] arr = this.getPasteTypesOrActions(null);
            if (arr.length <= index) {
                return null;
            }
            if (arr[index] instanceof PasteType) {
                return ((PasteType)arr[index]).getHelpCtx();
            }
            Object helpID = ((Action)arr[index]).getValue("helpID");
            if (helpID instanceof String) {
                return new HelpCtx((String)helpID);
            }
            return null;
        }

        public MenuShortcut getMenuShortcut(int index) {
            return null;
        }

        public void performActionAt(int index) {
            this.performActionAt(index, null);
        }

        public void performActionAt(int index, ActionEvent ev) {
            Action[] action = new Action[1];
            Object[] arr = this.getPasteTypesOrActions(action);
            if (arr.length <= index) {
                return;
            }
            if (arr[index] instanceof PasteType) {
                PasteType t = (PasteType)arr[index];
                new ActionPT(t, ev == null ? null : ev.getActionCommand());
                return;
            }
            Action a = (Action)arr[index];
            a.actionPerformed(new ActionEvent(a, 1001, "Name"));
        }

        public synchronized void addChangeListener(ChangeListener listener) {
            this.cs.addChangeListener(listener);
        }

        public synchronized void removeChangeListener(ChangeListener listener) {
            this.cs.removeChangeListener(listener);
        }

        protected void checkStateChanged(boolean fire) {
            boolean en;
            Action[] listen = new Action[1];
            Object[] arr = this.getPasteTypesOrActions(listen);
            Action a = null;
            if (arr.length == 1 && arr[0] instanceof Action) {
                a = (Action)arr[0];
                a.removePropertyChangeListener(this.pasteTypeWeakL);
                this.pasteTypeWeakL = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)a);
                a.addPropertyChangeListener(this.pasteTypeWeakL);
            }
            if (listen[0] != a) {
                listen[0].removePropertyChangeListener(this.actionWeakL);
                this.actionWeakL = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)listen[0]);
                listen[0].addPropertyChangeListener(this.actionWeakL);
            }
            if ((en = this.isEnabledImpl(arr)) == this.enabled) {
                return;
            }
            this.enabled = en;
            if (!fire) {
                return;
            }
            this.cs.fireChange();
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            this.checkStateChanged(true);
        }

        public void resultChanged(LookupEvent ev) {
            this.checkStateChanged(true);
        }
    }

}

