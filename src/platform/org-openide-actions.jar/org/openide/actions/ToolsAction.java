/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Actions
 *  org.openide.awt.DynamicMenuContent
 *  org.openide.awt.JMenuPlus
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.Lookups
 */
package org.openide.actions;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.event.ChangeListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import org.openide.actions.ActionManager;
import org.openide.actions.Bundle;
import org.openide.awt.Actions;
import org.openide.awt.DynamicMenuContent;
import org.openide.awt.JMenuPlus;
import org.openide.util.ContextAwareAction;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.actions.Presenter;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

public class ToolsAction
extends SystemAction
implements ContextAwareAction,
Presenter.Menu,
Presenter.Popup {
    static final long serialVersionUID = 4906417339959070129L;
    private static ScheduledFuture<G> taskGl;

    static final G gl() {
        return ToolsAction.gl(Long.MAX_VALUE);
    }

    /*
     * Loose catch block
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Lifted jumps to return sites
     */
    static final G gl(long timeOut) {
        ToolsAction.initGl();
        do {
            try {
                return taskGl.get(timeOut, TimeUnit.MILLISECONDS);
            }
            catch (TimeoutException ex) {
                return null;
            }
            catch (InterruptedException ex) {
                continue;
            }
            break;
        } while (true);
        catch (Exception ex) {
            taskGl = null;
            throw new IllegalStateException(ex);
        }
    }

    private static synchronized void initGl() {
        if (taskGl == null) {
            taskGl = RequestProcessor.getDefault().schedule((Callable)new G(), 0, TimeUnit.MILLISECONDS);
        }
    }

    protected void initialize() {
        super.initialize();
    }

    public String getName() {
        return ToolsAction.getActionName();
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(ToolsAction.class);
    }

    public JMenuItem getMenuPresenter() {
        return new Inline((Action)((Object)this));
    }

    public JMenuItem getPopupPresenter() {
        return new Popup((Action)((Object)this));
    }

    public void actionPerformed(ActionEvent ev) {
        assert (false);
    }

    public Action createContextAwareInstance(Lookup actionContext) {
        return new DelegateAction(this, actionContext);
    }

    private static String getActionName() {
        return NbBundle.getMessage(ToolsAction.class, (String)"CTL_Tools");
    }

    static List<Action> getToolActions() {
        ActionManager am = ActionManager.getDefault();
        ArrayList<Action> arr = new ArrayList<Action>();
        arr.addAll(Arrays.asList(am.getContextActions()));
        String pref = arr.isEmpty() ? null : "";
        for (Lookup.Item item : ToolsAction.gl().result.allItems()) {
            Action action = (Action)item.getInstance();
            if (action == null) continue;
            String where = item.getId().replaceFirst("[^/]*$", "");
            if (pref != null && !pref.equals(where)) {
                arr.add(null);
            }
            pref = where;
            arr.add(action);
        }
        return arr;
    }

    private static List<JMenuItem> generate(Action toolsAction, boolean forMenu) {
        List<Action> actions = ToolsAction.getToolActions();
        ArrayList<JMenuItem> list = new ArrayList<JMenuItem>(actions.size());
        boolean separator = false;
        boolean firstItemAdded = false;
        Lookup lookup = toolsAction instanceof Lookup.Provider ? ((Lookup.Provider)toolsAction).getLookup() : null;
        Iterator<Action> i$ = actions.iterator();
        while (i$.hasNext()) {
            JMenuItem mi;
            Action a = i$.next();
            if (lookup != null && a instanceof ContextAwareAction) {
                a = ((ContextAwareAction)a).createContextAwareInstance(lookup);
            }
            if (a == null) {
                if (!firstItemAdded) continue;
                separator = true;
                continue;
            }
            boolean isPopup = a instanceof Presenter.Popup;
            boolean isMenu = a instanceof Presenter.Menu;
            if (!(forMenu && isMenu || !forMenu && isPopup) && (isMenu || isPopup) || !a.isEnabled()) continue;
            if (forMenu && isMenu) {
                mi = ((Presenter.Menu)a).getMenuPresenter();
            } else if (!forMenu && isPopup) {
                mi = ((Presenter.Popup)a).getPopupPresenter();
            } else {
                if (isMenu || isPopup) continue;
                mi = new JMenuItem();
                Actions.connect((JMenuItem)mi, (Action)a, (boolean)(!forMenu));
            }
            if (separator) {
                list.add(null);
                separator = false;
            }
            list.add(mi);
            firstItemAdded = true;
        }
        return list;
    }

    @Deprecated
    public static void setModel(Model m) {
        throw new SecurityException();
    }

    private static final class DelegateAction
    implements Action,
    Presenter.Menu,
    Presenter.Popup,
    Lookup.Provider {
        private ToolsAction delegate;
        private Lookup lookup;
        private PropertyChangeSupport support;

        public DelegateAction(ToolsAction delegate, Lookup actionContext) {
            this.support = new PropertyChangeSupport(this);
            this.delegate = delegate;
            this.lookup = actionContext;
        }

        public String toString() {
            return super.toString() + "[delegate=" + (Object)((Object)this.delegate) + "]";
        }

        public Lookup getLookup() {
            return this.lookup;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
        }

        @Override
        public void putValue(String key, Object o) {
        }

        @Override
        public Object getValue(String key) {
            return this.delegate.getValue(key);
        }

        @Override
        public boolean isEnabled() {
            return this.delegate.isEnabled();
        }

        @Override
        public void setEnabled(boolean b) {
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener listener) {
            this.support.addPropertyChangeListener(listener);
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener listener) {
            this.support.removePropertyChangeListener(listener);
        }

        public JMenuItem getMenuPresenter() {
            return new Inline(this);
        }

        public JMenuItem getPopupPresenter() {
            return new Popup(this);
        }
    }

    private static class G
    implements PropertyChangeListener,
    LookupListener,
    Callable<G> {
        public static final String PROP_STATE = "actionsState";
        private int timestamp = 1;
        private Action[] actions = null;
        private PropertyChangeSupport supp;
        Lookup.Result<Action> result;

        public G() {
            this.supp = new PropertyChangeSupport(this);
        }

        @Override
        public G call() {
            ActionManager am = ActionManager.getDefault();
            am.addPropertyChangeListener(this);
            this.result = Lookups.forPath((String)"UI/ToolActions").lookupResult(Action.class);
            this.result.addLookupListener((LookupListener)this);
            this.actionsListChanged();
            return this;
        }

        public final void addPropertyChangeListener(PropertyChangeListener listener) {
            this.supp.addPropertyChangeListener(listener);
        }

        public final void removePropertyChangeListener(PropertyChangeListener listener) {
            this.supp.removePropertyChangeListener(listener);
        }

        protected final void firePropertyChange(String name, Object o, Object n) {
            this.supp.firePropertyChange(name, o, n);
        }

        private void actionsListChanged() {
            ++this.timestamp;
            Action[] copy = this.actions;
            if (copy != null) {
                for (int i = 0; i < copy.length; ++i) {
                    Action act = copy[i];
                    if (act == null) continue;
                    act.removePropertyChangeListener(this);
                }
            }
            ActionManager am = ActionManager.getDefault();
            ArrayList<SystemAction> all = new ArrayList<SystemAction>();
            all.addAll(Arrays.asList(am.getContextActions()));
            all.addAll(this.result.allInstances());
            copy = all.toArray(new Action[0]);
            for (int i = 0; i < copy.length; ++i) {
                Action act = copy[i];
                if (act == null) continue;
                act.addPropertyChangeListener(this);
            }
            this.actions = copy;
            this.firePropertyChange("actionsState", null, null);
        }

        private void actionStateChanged() {
            ++this.timestamp;
            this.firePropertyChange("actionsState", null, null);
        }

        @Override
        public void propertyChange(PropertyChangeEvent ev) {
            String prop = ev.getPropertyName();
            if (prop == null || prop.equals("contextActions")) {
                this.actionsListChanged();
            } else if (prop.equals("enabled")) {
                this.actionStateChanged();
            }
        }

        private boolean isPopupEnabled(Action toolsAction) {
            boolean en = false;
            Action[] copy = this.actions;
            Lookup lookup = toolsAction instanceof Lookup.Provider ? ((Lookup.Provider)toolsAction).getLookup() : null;
            for (int i = 0; i < copy.length; ++i) {
                Action act;
                if (copy[i] == null) continue;
                if (lookup != null && copy[i] instanceof ContextAwareAction) {
                    act = ((ContextAwareAction)copy[i]).createContextAwareInstance(lookup);
                    if (act == null) {
                        throw new IllegalStateException("createContextAwareInstance for " + copy[i] + " returned null!");
                    }
                } else {
                    act = copy[i];
                }
                if (!act.isEnabled()) continue;
                en = true;
                break;
            }
            return en;
        }

        private int getTimestamp() {
            return this.timestamp;
        }

        public void resultChanged(LookupEvent ev) {
            this.actionsListChanged();
        }
    }

    private static final class Popup
    extends JMenuItem
    implements DynamicMenuContent {
        static final long serialVersionUID = 2269006599727576059L;
        private JMenu menu;
        private Action toolsAction;

        public Popup(Action toolsAction) {
            this.menu = new MyMenu();
            this.toolsAction = toolsAction;
            HelpCtx.setHelpIDString((JComponent)this.menu, (String)ToolsAction.class.getName());
        }

        public JComponent[] synchMenuPresenters(JComponent[] items) {
            JComponent[] arrjComponent;
            if (ToolsAction.gl().isPopupEnabled(this.toolsAction)) {
                JMenuItem[] arrjMenuItem = new JMenuItem[1];
                arrjComponent = arrjMenuItem;
                arrjMenuItem[0] = this.menu;
            } else {
                arrjComponent = new JMenuItem[]{};
            }
            return arrjComponent;
        }

        public JComponent[] getMenuPresenters() {
            return this.synchMenuPresenters(new JComponent[0]);
        }

        private class MyMenu
        extends JMenuPlus
        implements PopupMenuListener {
            private JPopupMenu lastPopup;

            MyMenu() {
                super(ToolsAction.getActionName());
                this.lastPopup = null;
            }

            public JPopupMenu getPopupMenu() {
                JPopupMenu popup = super.getPopupMenu();
                this.fillSubmenu(popup);
                return popup;
            }

            private void fillSubmenu(JPopupMenu pop) {
                if (this.lastPopup == null) {
                    pop.addPopupMenuListener(this);
                    this.lastPopup = pop;
                    this.removeAll();
                    for (Component item : ToolsAction.generate(Popup.this.toolsAction, false)) {
                        if (item == null) {
                            this.addSeparator();
                            continue;
                        }
                        this.add(item);
                    }
                    if (this.getMenuComponentCount() == 0) {
                        JMenuItem empty = new JMenuItem(NbBundle.getMessage(ToolsAction.class, (String)"CTL_EmptySubMenu"));
                        empty.setEnabled(false);
                        this.add(empty);
                    }
                }
            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {
            }

            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                if (this.lastPopup != null) {
                    this.lastPopup.removePopupMenuListener(this);
                    this.lastPopup = null;
                }
            }
        }

    }

    private static final class Inline
    extends JMenuItem
    implements DynamicMenuContent {
        static final long serialVersionUID = 2269006599727576059L;
        private int timestamp = 0;
        private Action toolsAction;

        Inline(Action toolsAction) {
            this.toolsAction = toolsAction;
        }

        public JComponent[] synchMenuPresenters(JComponent[] items) {
            G g = ToolsAction.gl(50);
            if (g == null) {
                JMenuItem init = new JMenuItem();
                init.setText(Bundle.LAB_ToolsActionInitializing());
                init.setEnabled(false);
                return new JMenuItem[]{init};
            }
            if (this.timestamp == g.getTimestamp()) {
                return items;
            }
            List l = ToolsAction.generate(this.toolsAction, true);
            this.timestamp = ToolsAction.gl().getTimestamp();
            return l.toArray(new JMenuItem[l.size()]);
        }

        public JComponent[] getMenuPresenters() {
            return this.synchMenuPresenters(new JComponent[0]);
        }
    }

    @Deprecated
    public static interface Model {
        public SystemAction[] getActions();

        public void addChangeListener(ChangeListener var1);

        public void removeChangeListener(ChangeListener var1);
    }

}

