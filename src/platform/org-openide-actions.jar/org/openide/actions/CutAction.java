/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallbackSystemAction
 */
package org.openide.actions;

import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallbackSystemAction;

public class CutAction
extends CallbackSystemAction {
    protected void initialize() {
        super.initialize();
    }

    public Object getActionMapKey() {
        return "cut-to-clipboard";
    }

    public String getName() {
        return NbBundle.getMessage(CutAction.class, (String)"Cut");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(CutAction.class);
    }

    protected String iconResource() {
        return "org/openide/resources/actions/cut.gif";
    }

    protected boolean asynchronous() {
        return false;
    }
}

