/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallbackSystemAction
 */
package org.openide.actions;

import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallbackSystemAction;

public class CloseViewAction
extends CallbackSystemAction {
    public Object getActionMapKey() {
        return "closeWindow";
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(CloseViewAction.class);
    }

    public String getName() {
        return NbBundle.getMessage(CloseViewAction.class, (String)"CloseView");
    }

    protected boolean asynchronous() {
        return false;
    }
}

