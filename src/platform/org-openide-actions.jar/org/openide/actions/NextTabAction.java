/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallbackSystemAction
 */
package org.openide.actions;

import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallbackSystemAction;

public class NextTabAction
extends CallbackSystemAction {
    protected String iconResource() {
        return "org/openide/resources/actions/nextTab.gif";
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(NextTabAction.class);
    }

    public String getName() {
        return NbBundle.getMessage(NextTabAction.class, (String)"NextTab");
    }

    protected boolean asynchronous() {
        return false;
    }
}

