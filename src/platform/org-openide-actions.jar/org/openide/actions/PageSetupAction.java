/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.text.PrintPreferences
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallableSystemAction
 */
package org.openide.actions;

import java.awt.print.PageFormat;
import java.awt.print.PrinterJob;
import org.openide.text.PrintPreferences;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallableSystemAction;

public final class PageSetupAction
extends CallableSystemAction {
    public PageSetupAction() {
        this.putValue("noIconInMenu", (Object)Boolean.TRUE);
    }

    public synchronized void performAction() {
        PrinterJob pj = PrinterJob.getPrinterJob();
        PrintPreferences.setPageFormat((PageFormat)pj.pageDialog(PrintPreferences.getPageFormat((PrinterJob)pj)));
    }

    protected boolean asynchronous() {
        return false;
    }

    public String getName() {
        return NbBundle.getMessage(PageSetupAction.class, (String)"PageSetup");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(PageSetupAction.class);
    }
}

