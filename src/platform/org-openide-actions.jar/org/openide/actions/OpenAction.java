/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.OpenCookie
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CookieAction
 */
package org.openide.actions;

import org.openide.cookies.OpenCookie;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CookieAction;

public class OpenAction
extends CookieAction {
    protected Class[] cookieClasses() {
        return new Class[]{OpenCookie.class};
    }

    protected boolean surviveFocusChange() {
        return false;
    }

    protected int mode() {
        return 7;
    }

    public String getName() {
        return NbBundle.getMessage(OpenAction.class, (String)"Open");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(OpenAction.class);
    }

    protected void performAction(Node[] activatedNodes) {
        for (int i = 0; i < activatedNodes.length; ++i) {
            OpenCookie oc = (OpenCookie)activatedNodes[i].getCookie(OpenCookie.class);
            if (oc == null) continue;
            oc.open();
        }
    }

    protected boolean asynchronous() {
        return false;
    }
}

