/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.options.keymap.api;

public interface ShortcutAction {
    public String getDisplayName();

    public String getId();

    public String getDelegatingActionId();

    public ShortcutAction getKeymapManagerInstance(String var1);
}

