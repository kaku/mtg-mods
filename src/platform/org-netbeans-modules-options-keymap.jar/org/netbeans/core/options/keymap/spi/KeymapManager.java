/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.options.keymap.spi;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.core.options.keymap.api.ShortcutAction;

public abstract class KeymapManager {
    private String name;

    protected KeymapManager(String name) {
        this.name = name;
    }

    public abstract Map<String, Set<ShortcutAction>> getActions();

    public abstract void refreshActions();

    public abstract Map<ShortcutAction, Set<String>> getKeymap(String var1);

    public abstract Map<ShortcutAction, Set<String>> getDefaultKeymap(String var1);

    public abstract void saveKeymap(String var1, Map<ShortcutAction, Set<String>> var2);

    public abstract List<String> getProfiles();

    public String getProfileDisplayName(String profileName) {
        return profileName;
    }

    public abstract String getCurrentProfile();

    public abstract void setCurrentProfile(String var1);

    public abstract void deleteProfile(String var1);

    public abstract boolean isCustomProfile(String var1);

    public final String getName() {
        return this.name;
    }

    public static interface WithRevert {
        public void revertActions(String var1, Collection<ShortcutAction> var2) throws IOException;

        public void revertProfile(String var1) throws IOException;
    }

}

