/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.options.keymap.api;

import java.util.Set;
import org.netbeans.core.options.keymap.api.ShortcutAction;

public interface ShortcutsFinder {
    public ShortcutAction findActionForShortcut(String var1);

    public ShortcutAction findActionForId(String var1);

    public String showShortcutsDialog();

    public String[] getShortcuts(ShortcutAction var1);

    public void refreshActions();

    @Deprecated
    public void setShortcuts(ShortcutAction var1, Set<String> var2);

    @Deprecated
    public void apply();

    public Writer localCopy();

    public static interface Writer
    extends ShortcutsFinder {
        @Override
        public void setShortcuts(ShortcutAction var1, Set<String> var2);

        @Override
        public void apply();
    }

}

