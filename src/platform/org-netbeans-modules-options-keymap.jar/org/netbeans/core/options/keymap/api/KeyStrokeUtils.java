/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 */
package org.netbeans.core.options.keymap.api;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.KeyStroke;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.core.options.keymap.api.ShortcutAction;
import org.netbeans.core.options.keymap.api.ShortcutsFinder;
import org.openide.util.Lookup;
import org.openide.util.Utilities;

public class KeyStrokeUtils {
    private static final Logger LOG = Logger.getLogger(KeyStrokeUtils.class.getName());
    private static final String EMACS_CTRL = "Ctrl+";
    private static final String EMACS_ALT = "Alt+";
    private static final String EMACS_SHIFT = "Shift+";
    private static final String EMACS_META = "Meta+";
    private static final String STRING_META;
    private static final String STRING_ALT;

    public static String getKeyStrokesAsText(@NullAllowed KeyStroke[] keyStrokes, @NonNull String delim) {
        if (keyStrokes == null) {
            return "";
        }
        if (keyStrokes.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(KeyStrokeUtils.getKeyStrokeAsText(keyStrokes[0]));
        int k = keyStrokes.length;
        for (int i = 1; i < k; ++i) {
            sb.append(delim).append(KeyStrokeUtils.getKeyStrokeAsText(keyStrokes[i]));
        }
        return new String(sb);
    }

    @CheckForNull
    public static KeyStroke getKeyStroke(@NonNull String keyStroke) {
        int modifiers = 0;
        do {
            if (keyStroke.startsWith("Ctrl+")) {
                modifiers |= 128;
                keyStroke = keyStroke.substring("Ctrl+".length());
                continue;
            }
            if (keyStroke.startsWith("Alt+")) {
                modifiers |= 512;
                keyStroke = keyStroke.substring("Alt+".length());
                continue;
            }
            if (keyStroke.startsWith("Shift+")) {
                modifiers |= 64;
                keyStroke = keyStroke.substring("Shift+".length());
                continue;
            }
            if (keyStroke.startsWith("Meta+")) {
                modifiers |= 256;
                keyStroke = keyStroke.substring("Meta+".length());
                continue;
            }
            if (keyStroke.startsWith(STRING_ALT)) {
                modifiers |= 512;
                keyStroke = keyStroke.substring(STRING_ALT.length());
                continue;
            }
            if (!keyStroke.startsWith(STRING_META)) break;
            modifiers |= 256;
            keyStroke = keyStroke.substring(STRING_META.length());
        } while (true);
        KeyStroke ks = Utilities.stringToKey((String)keyStroke);
        if (ks == null) {
            return null;
        }
        KeyStroke result = KeyStroke.getKeyStroke(ks.getKeyCode(), modifiers);
        return result;
    }

    public static String getKeyStrokeAsText(@NonNull KeyStroke keyStroke) {
        int modifiers = keyStroke.getModifiers();
        StringBuilder sb = new StringBuilder();
        if ((modifiers & 128) > 0) {
            sb.append("Ctrl+");
        }
        if ((modifiers & 512) > 0) {
            sb.append(STRING_ALT);
        }
        if ((modifiers & 64) > 0) {
            sb.append("Shift+");
        }
        if ((modifiers & 256) > 0) {
            sb.append(STRING_META);
        }
        if (keyStroke.getKeyCode() != 16 && keyStroke.getKeyCode() != 17 && keyStroke.getKeyCode() != 157 && keyStroke.getKeyCode() != 18 && keyStroke.getKeyCode() != 65406) {
            sb.append(Utilities.keyToString((KeyStroke)KeyStroke.getKeyStroke(keyStroke.getKeyCode(), 0)));
        }
        return sb.toString();
    }

    @CheckForNull
    public static KeyStroke[] getKeyStrokes(@NonNull String key) {
        assert (key != null);
        ArrayList<KeyStroke> result = new ArrayList<KeyStroke>();
        String delimiter = " ";
        StringTokenizer st = new StringTokenizer(key, delimiter);
        while (st.hasMoreTokens()) {
            String ks = st.nextToken().trim();
            KeyStroke keyStroke = KeyStrokeUtils.getKeyStroke(ks);
            if (keyStroke != null) {
                result.add(keyStroke);
                continue;
            }
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Invalid keystroke string: ''{0}''", ks);
            }
            return null;
        }
        return result.toArray(new KeyStroke[result.size()]);
    }

    public static List<KeyStroke[]> getKeyStrokesForAction(@NonNull String actionId, @NullAllowed KeyStroke defaultKeyStroke) {
        for (ShortcutsFinder sf : Lookup.getDefault().lookupAll(ShortcutsFinder.class)) {
            String[] shortcuts;
            ShortcutAction sa = sf.findActionForId(actionId);
            if (sa == null || (shortcuts = sf.getShortcuts(sa)) == null || shortcuts.length <= 0) continue;
            LinkedList<KeyStroke[]> ks = new LinkedList<KeyStroke[]>();
            for (int i = 0; i < shortcuts.length; ++i) {
                KeyStroke[] s;
                if (shortcuts[i] == null || (s = KeyStrokeUtils.getKeyStrokes(shortcuts[i])) == null) continue;
                ks.add(s);
            }
            return KeyStrokeUtils.sortKeyStrokesByPreference(ks);
        }
        return defaultKeyStroke == null ? Collections.emptyList() : Collections.singletonList(new KeyStroke[]{defaultKeyStroke});
    }

    private static List<KeyStroke[]> sortKeyStrokesByPreference(List<KeyStroke[]> keystrokes) {
        if (keystrokes.size() < 2) {
            return keystrokes;
        }
        KeyStroke[] best = null;
        boolean isSolaris = Utilities.getOperatingSystem() == 8;
        for (int i = 0; i < keystrokes.size(); ++i) {
            boolean solarisKey;
            KeyStroke[] ks = keystrokes.get(i);
            if (ks.length > 1) continue;
            boolean bl = solarisKey = ks[0].getKeyCode() >= 65480 && ks[0].getKeyCode() <= 65489;
            if (isSolaris != solarisKey || best != null && best[0].getKeyCode() <= ks[0].getKeyCode()) continue;
            best = ks;
        }
        if (best != null) {
            keystrokes.remove(best);
            keystrokes.add(0, best);
        }
        return keystrokes;
    }

    public static void refreshActionCache() {
        for (ShortcutsFinder sf : Lookup.getDefault().lookupAll(ShortcutsFinder.class)) {
            sf.refreshActions();
        }
    }

    static {
        if (Utilities.isMac()) {
            STRING_META = KeyEvent.getKeyText(157).concat("+");
            STRING_ALT = KeyEvent.getKeyText(18).concat("+");
        } else {
            STRING_META = "Meta+";
            STRING_ALT = "Alt+";
        }
    }
}

