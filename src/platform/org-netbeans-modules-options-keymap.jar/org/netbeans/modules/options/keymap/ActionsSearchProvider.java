/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.quicksearch.SearchProvider
 *  org.netbeans.spi.quicksearch.SearchRequest
 *  org.netbeans.spi.quicksearch.SearchResponse
 *  org.openide.awt.StatusDisplayer
 *  org.openide.cookies.EditorCookie
 *  org.openide.nodes.Node
 *  org.openide.text.CloneableEditor
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package org.netbeans.modules.options.keymap;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JEditorPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.text.TextAction;
import org.netbeans.core.options.keymap.api.ShortcutAction;
import org.netbeans.core.options.keymap.spi.KeymapManager;
import org.netbeans.modules.options.keymap.KeymapModel;
import org.netbeans.spi.quicksearch.SearchProvider;
import org.netbeans.spi.quicksearch.SearchRequest;
import org.netbeans.spi.quicksearch.SearchResponse;
import org.openide.awt.StatusDisplayer;
import org.openide.cookies.EditorCookie;
import org.openide.nodes.Node;
import org.openide.text.CloneableEditor;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class ActionsSearchProvider
implements SearchProvider {
    private volatile SearchRequest currentRequest;

    public void evaluate(final SearchRequest request, final SearchResponse response) {
        this.currentRequest = request;
        final HashMap<Object, String> duplicateCheck = new HashMap<Object, String>();
        final ArrayList<ActionInfo> possibleResults = new ArrayList<ActionInfo>(7);
        for (final KeymapManager m : Lookup.getDefault().lookupAll(KeymapManager.class)) {
            final Object[] ret = new Object[2];
            KeymapModel.waitFinished(new Runnable(){

                @Override
                public void run() {
                    ret[0] = m.getKeymap(m.getCurrentProfile());
                    ret[1] = m.getActions().entrySet();
                }
            });
            Map curKeymap = (Map)ret[0];
            Set entrySet = (Set)ret[1];
            for (Map.Entry entry : entrySet) {
                for (ShortcutAction sa : (Set)entry.getValue()) {
                    if (this.currentRequest != request) {
                        return;
                    }
                    ActionInfo actInfo = this.getActionInfo(sa, (Set)curKeymap.get(sa), (String)entry.getKey());
                    if (actInfo == null || this.doEvaluation(sa.getDisplayName(), request, actInfo, response, possibleResults, duplicateCheck)) continue;
                    return;
                }
            }
        }
        try {
            SwingUtilities.invokeAndWait(new Runnable(){

                @Override
                public void run() {
                    Node[] actNodes = TopComponent.getRegistry().getActivatedNodes();
                    for (int i = 0; i < actNodes.length; ++i) {
                        Action[] acts = actNodes[i].getActions(false);
                        for (int j = 0; j < acts.length; ++j) {
                            String displayName;
                            if (ActionsSearchProvider.this.currentRequest != request) {
                                return;
                            }
                            Action action = ActionsSearchProvider.this.checkNodeAction(acts[j]);
                            if (action == null) continue;
                            ActionInfo actInfo = new ActionInfo(action, null, null, null);
                            Object name = action.getValue("Name");
                            if (!(name instanceof String) || ActionsSearchProvider.this.doEvaluation(displayName = ((String)name).replaceFirst("&(?! )", ""), request, actInfo, response, possibleResults, duplicateCheck)) continue;
                            return;
                        }
                    }
                }
            });
        }
        catch (InterruptedException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (InvocationTargetException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        for (ActionInfo actInfo : possibleResults) {
            if (this.currentRequest != request) {
                return;
            }
            if (this.addAction(actInfo, response, duplicateCheck)) continue;
            return;
        }
    }

    private boolean addAction(ActionInfo actInfo, SearchResponse response, Map<Object, String> duplicateCheck) {
        Object shortcut;
        KeyStroke stroke = null;
        Set<String> shortcuts = actInfo.getShortcuts();
        if (shortcuts != null && shortcuts.size() > 0) {
            String shortcut2 = shortcuts.iterator().next();
            stroke = Utilities.stringToKey((String)shortcut2);
        }
        Action action = actInfo.getAction();
        if (stroke == null && (shortcut = action.getValue("AcceleratorKey")) instanceof KeyStroke) {
            stroke = (KeyStroke)shortcut;
        }
        String displayName = null;
        ShortcutAction sa = actInfo.getShortcutAction();
        if (sa != null) {
            displayName = sa.getDisplayName();
        } else {
            Object name = action.getValue("Name");
            if (name instanceof String) {
                displayName = ((String)name).replaceFirst("&(?! )", "");
            }
        }
        if (actInfo.getCategory() != null && !actInfo.getCategory().isEmpty() && !actInfo.getCategory().equals(displayName)) {
            displayName = displayName + " (" + actInfo.getCategory() + ")";
        }
        if (duplicateCheck.put(action, displayName) != null) {
            return true;
        }
        return response.addResult((Runnable)new ActionResult(action), displayName, null, Collections.singletonList(stroke));
    }

    private boolean doEvaluation(String name, SearchRequest request, ActionInfo actInfo, SearchResponse response, List<ActionInfo> possibleResults, Map<Object, String> duplicateCheck) {
        int index = name.toLowerCase().indexOf(request.getText().toLowerCase());
        if (index == 0) {
            return this.addAction(actInfo, response, duplicateCheck);
        }
        if (index != -1) {
            possibleResults.add(actInfo);
        }
        return true;
    }

    private ActionInfo getActionInfo(final ShortcutAction sa, final Set<String> shortcuts, final String category) {
        final ActionInfo[] result = new ActionInfo[1];
        try {
            SwingUtilities.invokeAndWait(new Runnable(){

                @Override
                public void run() {
                    Class clazz = sa.getClass();
                    Field f = null;
                    try {
                        f = clazz.getDeclaredField("action");
                        f.setAccessible(true);
                        Action action = (Action)f.get(sa);
                        if (!action.isEnabled()) {
                            return;
                        }
                        result[0] = new ActionInfo(action, sa, shortcuts, category);
                        return;
                    }
                    catch (Throwable thr) {
                        if (thr instanceof ThreadDeath) {
                            throw (ThreadDeath)thr;
                        }
                        Logger.getLogger(this.getClass().getName()).log(Level.FINE, "Some problem getting action " + sa.getDisplayName(), thr);
                        return;
                    }
                }
            });
        }
        catch (InterruptedException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (InvocationTargetException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        return result[0];
    }

    private static ActionEvent createActionEvent(Action action) {
        Object evSource = null;
        int evId = 1001;
        if (action instanceof TextAction) {
            EditorCookie ec = (EditorCookie)Utilities.actionsGlobalContext().lookup(EditorCookie.class);
            if (ec == null) {
                return null;
            }
            JEditorPane[] editorPanes = ec.getOpenedPanes();
            if (editorPanes == null || editorPanes.length <= 0) {
                return null;
            }
            evSource = editorPanes[0];
        }
        if (evSource == null) {
            evSource = TopComponent.getRegistry().getActivated();
        }
        if (evSource == null) {
            evSource = WindowManager.getDefault().getMainWindow();
        }
        return new ActionEvent(evSource, evId, null);
    }

    private Action checkNodeAction(Action action) {
        if (action == null) {
            return null;
        }
        try {
            if (action.isEnabled()) {
                return action;
            }
        }
        catch (Throwable thr) {
            if (thr instanceof ThreadDeath) {
                throw (ThreadDeath)thr;
            }
            Logger.getLogger(this.getClass().getName()).log(Level.FINE, "Problem asking isEnabled on action " + action, thr);
        }
        return null;
    }

    private static class ActionInfo {
        private Action action;
        private ShortcutAction shortcutAction = null;
        private Set<String> shortcuts = null;
        private String category = null;

        public ActionInfo(Action action, ShortcutAction shortcutAction, Set<String> shortcuts, String category) {
            this.action = action;
            this.shortcutAction = shortcutAction;
            this.shortcuts = shortcuts;
            this.category = category;
        }

        public Action getAction() {
            return this.action;
        }

        public ShortcutAction getShortcutAction() {
            return this.shortcutAction;
        }

        public Set<String> getShortcuts() {
            return this.shortcuts;
        }

        public String getCategory() {
            return this.category;
        }
    }

    private static class ActionResult
    implements Runnable {
        private static Logger UILOG = Logger.getLogger("org.netbeans.ui.actions");
        private Action command;

        public ActionResult(Action command) {
            this.command = command;
        }

        @Override
        public void run() {
            try {
                Action activeCommand;
                JEditorPane pane;
                Action a = this.command;
                ActionEvent ae = ActionsSearchProvider.createActionEvent(this.command);
                Object p = ae.getSource();
                if (p instanceof CloneableEditor && (activeCommand = (pane = ((CloneableEditor)p).getEditorPane()).getActionMap().get(this.command.getValue("Name"))) != null) {
                    a = activeCommand;
                }
                a.actionPerformed(ae);
                this.uiLog(true);
            }
            catch (Throwable thr) {
                this.uiLog(false);
                if (thr instanceof ThreadDeath) {
                    throw (ThreadDeath)thr;
                }
                Object name = this.command.getValue("Name");
                String displayName = "";
                if (name instanceof String) {
                    displayName = (String)name;
                }
                Logger.getLogger(this.getClass().getName()).log(Level.FINE, displayName + " action can not be invoked.", thr);
                StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(this.getClass(), (String)"MSG_ActionFailure", (Object)displayName));
            }
        }

        private void uiLog(boolean success) {
            LogRecord rec = new LogRecord(Level.FINER, success ? "LOG_QUICKSEARCH_ACTION" : "LOG_QUICKSEARCH_ACTION_FAILED");
            rec.setParameters(new Object[]{this.command.getClass().getName(), this.command.getValue("Name")});
            rec.setResourceBundle(NbBundle.getBundle(ActionsSearchProvider.class));
            rec.setResourceBundleName(ActionsSearchProvider.class.getPackage().getName() + ".Bundle");
            rec.setLoggerName(UILOG.getName());
            UILOG.log(rec);
        }
    }

}

