/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.keymap;

import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.net.URL;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.border.Border;
import org.netbeans.modules.options.keymap.Popupable;
import org.openide.util.NbBundle;

public class SpecialkeyPanel
extends JPanel
implements ActionListener {
    private Popupable parent;
    private JTextField target;
    private JButton downButton;
    private JButton enterButton;
    private JButton escButton;
    private JButton leftButton;
    private JButton rightButton;
    private JButton tabButton;
    private JButton upButton;
    private JButton wheelDownButton;
    private JButton wheelUpButton;

    public SpecialkeyPanel(final Popupable parent, JTextField target) {
        this.parent = parent;
        this.target = target;
        this.initComponents();
        target.addFocusListener(new FocusAdapter(){

            @Override
            public void focusLost(FocusEvent e) {
                parent.hidePopup();
            }
        });
        this.downButton.addActionListener(this);
        this.enterButton.addActionListener(this);
        this.escButton.addActionListener(this);
        this.leftButton.addActionListener(this);
        this.rightButton.addActionListener(this);
        this.tabButton.addActionListener(this);
        this.upButton.addActionListener(this);
        this.wheelUpButton.addActionListener(this);
        this.wheelDownButton.addActionListener(this);
    }

    private void initComponents() {
        this.tabButton = new JButton();
        this.escButton = new JButton();
        this.upButton = new JButton();
        this.enterButton = new JButton();
        this.leftButton = new JButton();
        this.downButton = new JButton();
        this.rightButton = new JButton();
        this.wheelUpButton = new JButton();
        this.wheelDownButton = new JButton();
        this.setBorder(BorderFactory.createEtchedBorder());
        this.tabButton.setText("Tab");
        this.escButton.setFont(new Font("Lucida Grande", 0, 9));
        this.escButton.setText("ESC");
        this.escButton.setAlignmentY(0.0f);
        this.escButton.setIconTextGap(0);
        this.upButton.setIcon(new ImageIcon(this.getClass().getResource("/org/netbeans/modules/options/keymap/up.png")));
        this.upButton.setText(NbBundle.getMessage(SpecialkeyPanel.class, (String)"SpecialkeyPanel.upButton.text"));
        this.enterButton.setIcon(new ImageIcon(this.getClass().getResource("/org/netbeans/modules/options/keymap/enter.png")));
        this.enterButton.setText(NbBundle.getMessage(SpecialkeyPanel.class, (String)"SpecialkeyPanel.enterButton.text"));
        this.leftButton.setIcon(new ImageIcon(this.getClass().getResource("/org/netbeans/modules/options/keymap/left.png")));
        this.leftButton.setText(NbBundle.getMessage(SpecialkeyPanel.class, (String)"SpecialkeyPanel.leftButton.text_1"));
        this.downButton.setIcon(new ImageIcon(this.getClass().getResource("/org/netbeans/modules/options/keymap/down.png")));
        this.rightButton.setIcon(new ImageIcon(this.getClass().getResource("/org/netbeans/modules/options/keymap/right.png")));
        this.rightButton.setText(NbBundle.getMessage(SpecialkeyPanel.class, (String)"SpecialkeyPanel.rightButton.text"));
        this.wheelUpButton.setText(NbBundle.getMessage(SpecialkeyPanel.class, (String)"SpecialkeyPanel.wheelUpButton.text_1"));
        this.wheelDownButton.setText(NbBundle.getMessage(SpecialkeyPanel.class, (String)"SpecialkeyPanel.wheelDownButton.text_1"));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.tabButton, -1, -1, 32767).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.escButton, -2, 64, -2).addComponent(this.leftButton, -2, 50, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.downButton, -2, 50, -2).addComponent(this.upButton, -2, 50, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addGroup(layout.createSequentialGroup().addComponent(this.rightButton, -2, 53, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.wheelDownButton)).addGroup(layout.createSequentialGroup().addComponent(this.enterButton, -2, 53, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.wheelUpButton, -1, -1, 32767))).addGap(0, 0, 32767))).addContainerGap()));
        layout.linkSize(0, this.downButton, this.enterButton, this.escButton, this.leftButton, this.rightButton, this.upButton);
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.tabButton, -2, 30, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.upButton, GroupLayout.Alignment.TRAILING, -2, 30, -2).addComponent(this.enterButton, GroupLayout.Alignment.TRAILING, -1, -1, 32767).addComponent(this.escButton, -1, -1, 32767).addComponent(this.wheelUpButton, -2, 30, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.rightButton, -1, -1, 32767).addComponent(this.downButton, -2, 30, -2).addComponent(this.leftButton, -1, -1, 32767).addComponent(this.wheelDownButton, -2, 30, -2)).addContainerGap(-1, 32767)));
        layout.linkSize(1, this.downButton, this.enterButton, this.escButton, this.leftButton, this.rightButton, this.upButton);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source instanceof JButton && this.parent != null) {
            this.parent.hidePopup();
            String text = source == this.upButton ? "UP" : (source == this.downButton ? "DOWN" : (source == this.leftButton ? "LEFT" : (source == this.rightButton ? "RIGHT" : (source == this.enterButton ? "ENTER" : (source == this.escButton ? "ESCAPE" : (source == this.wheelUpButton ? "MOUSE_WHEEL_UP" : (source == this.wheelDownButton ? "MOUSE_WHEEL_DOWN" : ((JButton)source).getText().toUpperCase().replaceAll(" ", "_"))))))));
            String space = " ";
            String scText = this.target.getText();
            if (scText.length() == 0 || scText.endsWith(" ") || scText.endsWith("+")) {
                this.target.setText(scText + text);
            } else {
                this.target.setText(scText + " " + text);
            }
            this.target.requestFocus();
        }
    }

}

