/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.options.keymap;

import java.util.LinkedHashSet;
import java.util.Set;
import javax.swing.KeyStroke;
import org.netbeans.core.options.keymap.api.KeyStrokeUtils;
import org.openide.util.Utilities;

public class ShortcutProvider {
    private static final int[] letters;
    private static LinkedHashSet<String> shortcutSet;

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public static Set<String> getSet() {
        int i;
        if (shortcutSet != null) return shortcutSet;
        shortcutSet = new LinkedHashSet();
        for (i = 0; i < letters.length; ++i) {
            shortcutSet.add(KeyStrokeUtils.getKeyStrokeAsText(KeyStroke.getKeyStroke(letters[i], 2)));
        }
        if (Utilities.isMac()) {
            for (i = 0; i < letters.length; ++i) {
                shortcutSet.add(KeyStrokeUtils.getKeyStrokeAsText(KeyStroke.getKeyStroke(letters[i], 4)));
            }
        } else {
            for (i = 0; i < letters.length; ++i) {
                shortcutSet.add(KeyStrokeUtils.getKeyStrokeAsText(KeyStroke.getKeyStroke(letters[i], 8)));
            }
        }
        for (i = 0; i < letters.length; ++i) {
            shortcutSet.add(KeyStrokeUtils.getKeyStrokeAsText(KeyStroke.getKeyStroke(letters[i], 3)));
        }
        if (Utilities.isMac()) {
            i = 0;
            while (i < letters.length) {
                shortcutSet.add(KeyStrokeUtils.getKeyStrokeAsText(KeyStroke.getKeyStroke(letters[i], 5)));
                ++i;
            }
            return shortcutSet;
        }
        i = 0;
        while (i < letters.length) {
            shortcutSet.add(KeyStrokeUtils.getKeyStrokeAsText(KeyStroke.getKeyStroke(letters[i], 9)));
            ++i;
        }
        return shortcutSet;
    }

    static {
        int[] arrn = new int[60];
        arrn[0] = 65;
        arrn[1] = 66;
        arrn[2] = 67;
        arrn[3] = 68;
        arrn[4] = 69;
        arrn[5] = 70;
        arrn[6] = 71;
        arrn[7] = 72;
        arrn[8] = 73;
        arrn[9] = 74;
        arrn[10] = 75;
        arrn[11] = 76;
        arrn[12] = 77;
        arrn[13] = 78;
        arrn[14] = 79;
        arrn[15] = 80;
        arrn[16] = 81;
        arrn[17] = 82;
        arrn[18] = 83;
        arrn[19] = 84;
        arrn[20] = 85;
        arrn[21] = 86;
        arrn[22] = 87;
        arrn[23] = 88;
        arrn[24] = 89;
        arrn[25] = 90;
        arrn[26] = 9;
        arrn[27] = 112;
        arrn[28] = 113;
        arrn[29] = 114;
        arrn[30] = 115;
        arrn[31] = 116;
        arrn[32] = 117;
        arrn[33] = 118;
        arrn[34] = 119;
        arrn[35] = 120;
        arrn[36] = 121;
        arrn[37] = 8;
        arrn[38] = 92;
        arrn[39] = 222;
        arrn[40] = 192;
        arrn[41] = 10;
        arrn[42] = 27;
        arrn[43] = 91;
        arrn[44] = 93;
        arrn[45] = 59;
        arrn[46] = 44;
        arrn[47] = 46;
        arrn[48] = 47;
        arrn[49] = 45;
        arrn[50] = 61;
        arrn[51] = 32;
        arrn[52] = Utilities.isMac() ? 156 : 155;
        arrn[53] = 36;
        arrn[54] = 33;
        arrn[55] = 34;
        arrn[56] = 38;
        arrn[57] = 40;
        arrn[58] = 37;
        arrn[59] = 39;
        letters = arrn;
    }
}

