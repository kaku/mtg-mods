/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 */
package org.netbeans.modules.options.keymap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Vector;
import javax.swing.SwingUtilities;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableModel;
import org.netbeans.core.options.keymap.api.ShortcutAction;
import org.netbeans.core.options.keymap.api.ShortcutsFinder;
import org.netbeans.modules.options.keymap.ActionHolder;
import org.netbeans.modules.options.keymap.KeymapModel;
import org.netbeans.modules.options.keymap.MutableShortcutsModel;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;

class KeymapViewModel
extends DefaultTableModel
implements Runnable {
    private KeymapModel model = new KeymapModel();
    private MutableShortcutsModel mutableModel = new MutableShortcutsModel(this.model, null);
    private String currentProfile;
    static final ActionsComparator actionsComparator = new ActionsComparator();
    private String searchText = "";
    private volatile Task initTask;
    private Map<String, List<String>> categories;
    private boolean supressDataEvents;

    public KeymapViewModel() {
        super(new String[]{NbBundle.getMessage(KeymapViewModel.class, (String)"ActionsColumnName"), NbBundle.getMessage(KeymapViewModel.class, (String)"ShortcutColumnName"), NbBundle.getMessage(KeymapViewModel.class, (String)"CategoryColumnName")}, 0);
    }

    MutableShortcutsModel getMutableModel() {
        return this.mutableModel;
    }

    void update() {
        this.postUpdate();
    }

    @Override
    public void run() {
        this.update0();
    }

    private void scheduleUpdate() {
        if (SwingUtilities.isEventDispatchThread()) {
            this.run();
        } else {
            SwingUtilities.invokeLater(this);
        }
    }

    public Task postUpdate() {
        Task t = this.initTask;
        if (t != null && t.isFinished()) {
            this.scheduleUpdate();
            return t;
        }
        if (t == null) {
            this.initTask = KeymapModel.RP.post(new Runnable(){

                @Override
                public void run() {
                    KeymapViewModel.this.mutableModel.getCategories();
                    KeymapViewModel.this.mutableModel.getItems("");
                    KeymapViewModel.this.scheduleUpdate();
                }
            });
            return this.initTask;
        }
        if (t.isFinished()) {
            this.scheduleUpdate();
        }
        return t;
    }

    public Class getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0: {
                return ActionHolder.class;
            }
        }
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 1) {
            return true;
        }
        return false;
    }

    void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public Map<String, List<String>> getCategories() {
        if (this.categories == null) {
            this.categories = new TreeMap<String, List<String>>();
            ArrayList<String> c = new ArrayList<String>(this.model.getActionCategories());
            Collections.sort(c);
            for (String cn : c) {
                String folderName = "";
                StringTokenizer st = new StringTokenizer(cn, "/");
                while (st.hasMoreTokens()) {
                    String name = st.nextToken();
                    List<String> asd = this.categories.get(folderName);
                    if (asd == null) {
                        asd = new ArrayList<String>();
                        this.categories.put(folderName, asd);
                    }
                    String string = folderName = folderName.length() == 0 ? name : folderName + '/' + name;
                    if (!asd.isEmpty() && asd.get(asd.size() - 1).equals(folderName)) continue;
                    asd.add(folderName);
                }
            }
        }
        return this.categories;
    }

    @Override
    public void fireTableDataChanged() {
        if (!this.supressDataEvents) {
            super.fireTableDataChanged();
        }
    }

    @Override
    public void fireTableRowsInserted(int firstRow, int lastRow) {
        if (!this.supressDataEvents) {
            super.fireTableRowsInserted(firstRow, lastRow);
        }
    }

    @Override
    public void fireTableRowsDeleted(int firstRow, int lastRow) {
        if (!this.supressDataEvents) {
            super.fireTableRowsDeleted(firstRow, lastRow);
        }
    }

    @Override
    public void fireTableChanged(TableModelEvent e) {
        if (!this.supressDataEvents) {
            super.fireTableChanged(e);
        }
    }

    private void update0() {
        String searchTxt;
        boolean caseSensitiveSearch = false;
        if (this.searchText.matches(".*[A-Z].*")) {
            caseSensitiveSearch = true;
            searchTxt = this.searchText;
        } else {
            searchTxt = this.searchText.toLowerCase();
        }
        this.supressDataEvents = true;
        this.getDataVector().removeAllElements();
        for (String categorySet : this.getCategories().keySet()) {
            for (String category : this.getCategories().get(categorySet)) {
                for (Object o : this.mutableModel.getItems(category, false)) {
                    ShortcutAction sca = (ShortcutAction)o;
                    String[] shortcuts = this.mutableModel.getShortcuts(sca);
                    String displayName = sca.getDisplayName();
                    if (displayName.isEmpty() || !this.searched(caseSensitiveSearch ? displayName : displayName.toLowerCase(), searchTxt)) continue;
                    if (shortcuts.length == 0) {
                        this.addRow(new Object[]{new ActionHolder(sca, false), "", category});
                        continue;
                    }
                    for (int i = 0; i < shortcuts.length; ++i) {
                        String shortcut = shortcuts[i];
                        Object[] arrobject = new Object[3];
                        arrobject[0] = i == 0 ? new ActionHolder(sca, false) : new ActionHolder(sca, true);
                        arrobject[1] = shortcut;
                        arrobject[2] = category;
                        this.addRow(arrobject);
                    }
                }
            }
        }
        this.supressDataEvents = false;
        this.fireTableDataChanged();
    }

    private boolean searched(String displayName, String searchText) {
        if (displayName.length() == 0 || displayName.startsWith(searchText) || displayName.contains(searchText)) {
            return true;
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void runWithoutEvents(Runnable r) {
        try {
            this.supressDataEvents = true;
            r.run();
        }
        finally {
            this.supressDataEvents = false;
        }
    }

    static class ActionsComparator
    implements Comparator {
        ActionsComparator() {
        }

        public int compare(Object o1, Object o2) {
            if (o1 instanceof String) {
                if (o2 instanceof String) {
                    return ((String)o1).compareTo((String)o2);
                }
                return 1;
            }
            if (o2 instanceof String) {
                return -1;
            }
            return ((ShortcutAction)o1).getDisplayName().compareTo(((ShortcutAction)o2).getDisplayName());
        }
    }

}

