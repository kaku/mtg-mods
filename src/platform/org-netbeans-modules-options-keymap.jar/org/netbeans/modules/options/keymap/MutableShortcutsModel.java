/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.options.keymap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.core.options.keymap.api.KeyStrokeUtils;
import org.netbeans.core.options.keymap.api.ShortcutAction;
import org.netbeans.core.options.keymap.api.ShortcutsFinder;
import org.netbeans.core.options.keymap.spi.KeymapManager;
import org.netbeans.modules.options.keymap.KeymapModel;
import org.netbeans.modules.options.keymap.KeymapViewModel;
import org.netbeans.modules.options.keymap.LayersBridge;
import org.netbeans.modules.options.keymap.ShortcutsFinderImpl;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.Utilities;

class MutableShortcutsModel
extends ShortcutsFinderImpl
implements ShortcutsFinder.Writer {
    private volatile String currentProfile;
    private Map<String, List<Object>[]> categoryToActionsCache = new HashMap<String, List<Object>[]>();
    private volatile Map<String, Map<ShortcutAction, Set<String>>> modifiedProfiles = new HashMap<String, Map<ShortcutAction, Set<String>>>();
    private volatile Set<String> revertedProfiles = new HashSet<String>();
    private volatile Set<ShortcutAction> revertedActions = new HashSet<ShortcutAction>();
    private volatile Set<String> deletedProfiles = new HashSet<String>();
    @NullAllowed
    private ShortcutsFinder master;
    private volatile boolean dirty;
    private List<ChangeListener> chListeners;
    private volatile boolean applyInProgress = false;

    public MutableShortcutsModel(@NonNull KeymapModel model, ShortcutsFinder master) {
        super(model);
        this.master = master == null ? (ShortcutsFinder)Lookup.getDefault().lookup(ShortcutsFinder.class) : master;
    }

    String getProfileDisplayName(String id) {
        String s = this.model.getProfileName(id);
        return s != null ? s : id;
    }

    List<String> getProfiles() {
        HashSet<String> result = new HashSet<String>(this.model.getProfiles());
        result.addAll(this.modifiedProfiles.keySet());
        ArrayList<String> r = new ArrayList<String>(result);
        Collections.sort(r);
        return r;
    }

    boolean isChangedProfile(String profile) {
        return this.modifiedProfiles.containsKey(profile);
    }

    boolean isCustomProfile(String profile) {
        return this.model.isCustomProfile(profile);
    }

    synchronized boolean deleteOrRestoreProfile(String profile) {
        if (this.model.isCustomProfile(profile)) {
            this.deletedProfiles.add(profile);
            this.modifiedProfiles.remove(profile);
            this.clearShortcuts(profile);
            this.setDirty();
            return true;
        }
        this.modifiedProfiles.remove(profile);
        this.revertedProfiles.add(profile);
        this.clearShortcuts(profile);
        this.setDirty();
        return false;
    }

    public void addChangeListener(ChangeListener l) {
        if (this.chListeners == null) {
            this.chListeners = new LinkedList<ChangeListener>();
        }
        this.chListeners.add(l);
    }

    public void removeChangeListener(ChangeListener l) {
        if (this.chListeners != null) {
            this.chListeners.remove(l);
        }
    }

    protected void fireChanged() {
        if (this.chListeners == null || this.chListeners.isEmpty()) {
            return;
        }
        ChangeListener[] ll = this.chListeners.toArray(new ChangeListener[this.chListeners.size()]);
        ChangeEvent e = new ChangeEvent(this);
        for (ChangeListener l : ll) {
            l.stateChanged(e);
        }
    }

    @Override
    protected String getCurrentProfile() {
        if (this.currentProfile == null) {
            return this.model.getCurrentProfile();
        }
        return this.currentProfile;
    }

    void setCurrentProfile(String currentKeymap) {
        this.currentProfile = currentKeymap;
        this.setDirty();
    }

    void setDirty() {
        boolean old = this.dirty;
        this.dirty = this.isDirty();
        if (old != this.dirty) {
            this.fireChanged();
        }
    }

    private boolean isDirty() {
        boolean isChanged;
        boolean bl = isChanged = !this.getCurrentProfile().equals(this.model.getCurrentProfile());
        if (isChanged) {
            return true;
        }
        for (KeymapManager m : KeymapModel.getKeymapManagerInstances()) {
            List<String> profiles = m.getProfiles();
            if (profiles == null) continue;
            if (!this.modifiedProfiles.isEmpty()) {
                isChanged |= !profiles.containsAll(this.modifiedProfiles.keySet());
            }
            if (isChanged) {
                return true;
            }
            if (!this.deletedProfiles.isEmpty()) {
                isChanged |= profiles.containsAll(this.deletedProfiles);
            }
            if (isChanged) {
                return true;
            }
            for (String profile : profiles) {
                Map<ShortcutAction, Set<String>> saved = m.getKeymap(profile);
                Map<ShortcutAction, Set<String>> current = this.modifiedProfiles.get(profile);
                if (current == null) continue;
                for (Map.Entry<ShortcutAction, Set<String>> entry : current.entrySet()) {
                    Set<String> savedShortcut = saved.get(entry.getKey());
                    Set<String> currentShortcut = current.get(entry.getKey());
                    boolean bl2 = savedShortcut == null ? !currentShortcut.isEmpty() : !savedShortcut.equals(currentShortcut);
                    if (!(isChanged |= bl2)) continue;
                    return true;
                }
            }
        }
        return false;
    }

    synchronized void cloneProfile(String newProfileName) {
        HashMap<ShortcutAction, Set<String>> result = new HashMap<ShortcutAction, Set<String>>();
        this.cloneProfile("", result);
        this.modifiedProfiles.put(newProfileName, result);
        this.deletedProfiles.remove(newProfileName);
        this.setDirty();
    }

    private void cloneProfile(String category, Map<ShortcutAction, Set<String>> result) {
        for (Object o : this.getItems(category)) {
            String[] shortcuts = this.getShortcuts((ShortcutAction)o);
            result.put((ShortcutAction)o, new HashSet<String>(Arrays.asList(shortcuts)));
        }
    }

    @Override
    public ShortcutAction findActionForShortcut(String shortcut) {
        return this.findActionForShortcut(shortcut, "", false, null, "");
    }

    Collection<ShortcutAction> filterSameScope(Set<ShortcutAction> actions, ShortcutAction anchor) {
        return KeymapModel.filterSameScope(actions, anchor);
    }

    Set<ShortcutAction> findActionForShortcutPrefix(String shortcut) {
        HashSet<ShortcutAction> set = new HashSet<ShortcutAction>();
        if (shortcut.length() == 0) {
            return set;
        }
        if (shortcut.contains(" ")) {
            this.findActionForShortcut(shortcut.substring(0, shortcut.lastIndexOf(32)), "", true, set, shortcut);
        } else {
            this.findActionForShortcut(shortcut, "", true, set, shortcut);
        }
        return set;
    }

    private ShortcutAction findActionForShortcut(String shortcut, String category, boolean prefixSearch, Set<ShortcutAction> set, String completeMultikeySC) {
        Map<ShortcutAction, Set<String>> map = this.modifiedProfiles.get(this.getCurrentProfile());
        if (map != null) {
            for (Map.Entry<ShortcutAction, Set<String>> entry : map.entrySet()) {
                for (String sc : entry.getValue()) {
                    ShortcutAction action = entry.getKey();
                    if (this.isImpliedAction(action)) continue;
                    if (prefixSearch) {
                        if (!sc.equals(shortcut) && (!sc.startsWith(completeMultikeySC) || !shortcut.equals(completeMultikeySC) || !sc.contains(" "))) continue;
                        set.add(entry.getKey());
                        continue;
                    }
                    if (!sc.equals(shortcut)) continue;
                    return entry.getKey();
                }
            }
        }
        for (Object o : this.getItems(category)) {
            ShortcutAction action = (ShortcutAction)o;
            if (this.isImpliedAction(action)) continue;
            String[] shortcuts = this.getShortcuts(action);
            int k = shortcuts.length;
            for (int i = 0; i < k; ++i) {
                if (prefixSearch) {
                    if (!shortcuts[i].equals(shortcut) && (!shortcuts[i].startsWith(completeMultikeySC) || !shortcut.equals(completeMultikeySC) || !shortcuts[i].contains(" "))) continue;
                    set.add(action);
                    continue;
                }
                if (!shortcuts[i].equals(shortcut)) continue;
                return action;
            }
        }
        return null;
    }

    @Override
    protected ShortcutAction findActionForId(String actionId, String category, boolean delegate) {
        for (Object o : this.getItems(category)) {
            String id = delegate ? LayersBridge.getOrigActionClass((ShortcutAction)o) : ((ShortcutAction)o).getId();
            if (id == null || !actionId.equals(id)) continue;
            return (ShortcutAction)o;
        }
        return null;
    }

    @Override
    protected Map<ShortcutAction, Set<String>> getKeymap(String profile) {
        Map<ShortcutAction, Set<String>> base = this.revertedProfiles.contains(profile) ? this.model.getKeymapDefaults(profile) : super.getKeymap(profile);
        Map<ShortcutAction, Set<String>> p = this.modifiedProfiles.get(profile);
        if (p != null) {
            base = new HashMap<ShortcutAction, Set<String>>(base);
            base.putAll(p);
        }
        return base;
    }

    @Override
    public String[] getShortcuts(ShortcutAction action) {
        Map<ShortcutAction, Set<String>> actionToShortcuts;
        String profile = this.getCurrentProfile();
        Map<ShortcutAction, Set<String>> p = this.modifiedProfiles.get(profile);
        if (p != null && (actionToShortcuts = p).containsKey(action)) {
            Set<String> s = actionToShortcuts.get(action);
            return s.toArray(new String[s.size()]);
        }
        return super.getShortcuts(action);
    }

    public Set<String> getAllCurrentlyUsedShortcuts() {
        LinkedHashSet<String> set = new LinkedHashSet<String>();
        String profile = this.getCurrentProfile();
        HashSet<ShortcutAction> processed = new HashSet<ShortcutAction>();
        Map<ShortcutAction, Set<String>> modMap = this.modifiedProfiles.get(profile);
        if (modMap != null) {
            processed.addAll(modMap.keySet());
            for (Map.Entry<ShortcutAction, Set<String>> entry : modMap.entrySet()) {
                for (String sc : entry.getValue()) {
                    set.add(sc);
                    if (!sc.contains(" ")) continue;
                    set.add(sc.substring(0, sc.indexOf(32)));
                }
            }
        }
        for (Map.Entry<ShortcutAction, Set<String>> entry : this.getProfileMap(profile).entrySet()) {
            if (processed.contains(entry.getKey())) continue;
            for (String sc : entry.getValue()) {
                set.add(sc);
                if (!sc.contains(" ")) continue;
                set.add(sc.substring(0, sc.indexOf(32)));
            }
        }
        return set;
    }

    void addShortcut(ShortcutAction action, String shortcut) {
        ShortcutAction act = this.findActionForShortcut(shortcut);
        LinkedHashSet<String> s = new LinkedHashSet<String>();
        s.addAll(Arrays.asList(this.getShortcuts(action)));
        s.add(shortcut);
        this.setShortcuts(action, s);
    }

    synchronized Collection<ShortcutAction> revertShortcutsToDefault(ShortcutAction action, boolean force) {
        if (this.model.isCustomProfile(this.getCurrentProfile())) {
            return null;
        }
        Map<ShortcutAction, Set<String>> m = this.model.getKeymapDefaults(this.getCurrentProfile());
        Set shortcuts = (m = MutableShortcutsModel.convertFromEmacs(m)).get(action);
        if (shortcuts == null) {
            shortcuts = Collections.emptySet();
        }
        HashSet<ShortcutAction> conflictingActions = new HashSet<ShortcutAction>();
        for (String sc : shortcuts) {
            ShortcutAction ac = this.findActionForShortcut(sc);
            if (ac == null || ac.equals(action)) continue;
            conflictingActions.add(ac);
        }
        Collection<ShortcutAction> filtered = KeymapModel.filterSameScope(conflictingActions, action);
        if (!filtered.isEmpty() && !force) {
            return conflictingActions;
        }
        this.revertedActions.add(action);
        this.setShortcuts(action, shortcuts);
        for (ShortcutAction a : filtered) {
            String[] ss = this.getShortcuts(a);
            HashSet<String> newSs = new HashSet<String>(Arrays.asList(ss));
            newSs.removeAll(shortcuts);
            this.setShortcuts(a, newSs);
        }
        return null;
    }

    @Override
    public synchronized void setShortcuts(ShortcutAction action, Set<String> shortcuts) {
        Map<ShortcutAction, Set<String>> actionToShortcuts = this.modifiedProfiles.get(this.getCurrentProfile());
        if (actionToShortcuts == null) {
            actionToShortcuts = new HashMap<ShortcutAction, Set<String>>();
            this.modifiedProfiles.put(this.getCurrentProfile(), actionToShortcuts);
        }
        actionToShortcuts.put(action, shortcuts);
        this.setDirty();
    }

    public void removeShortcut(ShortcutAction action, String shortcut) {
        LinkedHashSet<String> s = new LinkedHashSet<String>(Arrays.asList(this.getShortcuts(action)));
        s.remove(shortcut);
        this.setShortcuts(action, s);
    }

    @Override
    public void apply() {
        this.postApply();
    }

    private Map<String, Map<ShortcutAction, Set<String>>> cloneProfileMap(Map<String, Map<ShortcutAction, Set<String>>> profiles) {
        HashMap<String, Map<ShortcutAction, Set<String>>> result = new HashMap<String, Map<ShortcutAction, Set<String>>>(profiles.size());
        for (Map.Entry<String, Map<ShortcutAction, Set<String>>> e : profiles.entrySet()) {
            result.put(e.getKey(), new HashMap<ShortcutAction, Set<String>>(e.getValue()));
        }
        return result;
    }

    synchronized Task postApply() {
        if (this.applyInProgress) {
            return null;
        }
        this.applyInProgress = true;
        final HashSet<String> revertedProfiles = new HashSet<String>(this.revertedProfiles);
        final HashSet<ShortcutAction> revertedActions = new HashSet<ShortcutAction>(this.revertedActions);
        final Map<String, Map<ShortcutAction, Set<String>>> modifiedProfiles = this.cloneProfileMap(this.modifiedProfiles);
        final HashSet<String> deletedProfiles = new HashSet<String>(this.deletedProfiles);
        final String currentProfile = this.currentProfile;
        return RequestProcessor.getDefault().post(new Runnable(){

            @Override
            public void run() {
                for (String profile22 : revertedProfiles) {
                    try {
                        MutableShortcutsModel.this.model.revertProfile(profile22);
                    }
                    catch (IOException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
                if (!revertedActions.isEmpty()) {
                    try {
                        MutableShortcutsModel.this.model.revertActions(revertedActions);
                    }
                    catch (IOException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
                for (String profile22 : modifiedProfiles.keySet()) {
                    Map actionToShortcuts = (Map)modifiedProfiles.get(profile22);
                    actionToShortcuts = MutableShortcutsModel.convertToEmacs(actionToShortcuts);
                    MutableShortcutsModel.this.model.changeKeymap(profile22, actionToShortcuts);
                }
                for (String profile22 : deletedProfiles) {
                    MutableShortcutsModel.this.model.deleteProfile(profile22);
                }
                String prof = currentProfile;
                if (prof == null) {
                    prof = MutableShortcutsModel.this.model.getCurrentProfile();
                }
                MutableShortcutsModel.this.model.setCurrentProfile(prof);
                MutableShortcutsModel.this.clearState();
                MutableShortcutsModel.this.model = new KeymapModel();
                MutableShortcutsModel.this.applyInProgress = false;
                MutableShortcutsModel.this.clearCache();
                if (MutableShortcutsModel.this.master != null) {
                    MutableShortcutsModel.this.master.refreshActions();
                }
            }
        });
    }

    public boolean isChanged() {
        return this.dirty;
    }

    private synchronized void clearState() {
        this.modifiedProfiles = new HashMap<String, Map<ShortcutAction, Set<String>>>();
        this.deletedProfiles = new HashSet<String>();
        this.revertedActions = new HashSet<ShortcutAction>();
        this.revertedProfiles = new HashSet<String>();
        this.currentProfile = null;
        this.dirty = false;
    }

    public void cancel() {
        this.clearState();
    }

    Map<String, Map<ShortcutAction, Set<String>>> getModifiedProfiles() {
        return this.modifiedProfiles;
    }

    Set<String> getDeletedProfiles() {
        return this.deletedProfiles;
    }

    void setModifiedProfiles(Map<String, Map<ShortcutAction, Set<String>>> mp) {
        this.modifiedProfiles = mp;
    }

    void setDeletedProfiles(Set<String> dp) {
        this.deletedProfiles = dp;
    }

    private static Map<ShortcutAction, Set<String>> convertToEmacs(Map<ShortcutAction, Set<String>> shortcuts) {
        HashMap<ShortcutAction, Set<String>> result = new HashMap<ShortcutAction, Set<String>>();
        for (Map.Entry<ShortcutAction, Set<String>> entry : shortcuts.entrySet()) {
            ShortcutAction action = entry.getKey();
            HashSet<String> newSet = new HashSet<String>();
            for (String s : entry.getValue()) {
                KeyStroke[] ks;
                if (s.length() == 0 || (ks = MutableShortcutsModel.getKeyStrokes(s, " ")) == null) continue;
                StringBuffer sb = new StringBuffer(Utilities.keyToString((KeyStroke)ks[0], (boolean)true));
                int k = ks.length;
                for (int i = 1; i < k; ++i) {
                    sb.append(' ').append(Utilities.keyToString((KeyStroke)ks[i], (boolean)true));
                }
                newSet.add(sb.toString());
            }
            result.put(action, newSet);
        }
        return result;
    }

    private static KeyStroke[] getKeyStrokes(String keyStrokes, String delim) {
        if (keyStrokes.length() == 0) {
            return new KeyStroke[0];
        }
        StringTokenizer st = new StringTokenizer(keyStrokes, delim);
        ArrayList<KeyStroke> result = new ArrayList<KeyStroke>();
        while (st.hasMoreTokens()) {
            String ks = st.nextToken().trim();
            KeyStroke keyStroke = KeyStrokeUtils.getKeyStroke(ks);
            if (keyStroke == null) {
                return null;
            }
            result.add(keyStroke);
        }
        return result.toArray(new KeyStroke[result.size()]);
    }

    public Set<String> getCategories() {
        return this.model.getActionCategories();
    }

    public List<Object> getItems(String category) {
        return this.getItems(category, true);
    }

    public List<Object> getItems(String category, boolean prefix) {
        List<Object>[] result = this.categoryToActionsCache.get(category);
        if (result == null) {
            ArrayList<ShortcutAction> allActions = new ArrayList<ShortcutAction>();
            List thisActions = Collections.emptyList();
            HashSet<String> filtered = new HashSet<String>(this.model.getActionCategories());
            Iterator<String> it = filtered.iterator();
            while (it.hasNext()) {
                String cat = it.next();
                if (!cat.startsWith(category)) {
                    it.remove();
                    continue;
                }
                if (category.length() <= 0 || cat.length() <= category.length() || cat.charAt(category.length()) == '/') continue;
                it.remove();
            }
            for (String c : filtered) {
                Set<ShortcutAction> act = this.model.getActions(c);
                allActions.addAll(act);
                if (c.length() != category.length()) continue;
                thisActions = new ArrayList<ShortcutAction>(act);
            }
            Collections.sort(allActions, new KeymapViewModel.ActionsComparator());
            if (!thisActions.isEmpty()) {
                Collections.sort(thisActions, new KeymapViewModel.ActionsComparator());
            }
            result = new List[]{allActions, thisActions};
            this.categoryToActionsCache.put(category, result);
        }
        return prefix ? result[0] : result[1];
    }

    boolean differsFromDefault(String profile) {
        if (this.modifiedProfiles.containsKey(profile)) {
            return true;
        }
        if (this.revertedProfiles.contains(profile)) {
            return false;
        }
        return !this.model.getKeymapDefaults(profile).equals(this.model.getKeymap(profile));
    }

}

