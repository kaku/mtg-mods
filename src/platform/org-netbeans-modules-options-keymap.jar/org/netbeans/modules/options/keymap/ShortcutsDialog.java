/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.keymap;

import java.awt.AWTKeyStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.Set;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle;
import org.netbeans.core.options.keymap.api.KeyStrokeUtils;
import org.netbeans.core.options.keymap.api.ShortcutAction;
import org.netbeans.core.options.keymap.api.ShortcutsFinder;
import org.netbeans.modules.options.keymap.ShortcutListener;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class ShortcutsDialog
extends JPanel {
    public static final String PROP_SHORTCUT_VALID = "ShortcutsDialog.PROP_SHORTCUT_VALID";
    private Listener listener = null;
    private JButton bTab = new JButton();
    private JButton bClear = new JButton();
    private ShortcutsFinder f = null;
    private boolean shortcutValid = false;
    private JLabel lConflict;
    private JLabel lShortcut;
    private JTextField tfShortcut;

    void init(ShortcutsFinder f) {
        this.f = f;
        ShortcutsDialog.loc(this.lShortcut, "Shortcut");
        this.lConflict.setForeground(Color.red);
        ShortcutsDialog.loc(this.bTab, "CTL_Tab");
        this.bTab.getAccessibleContext().setAccessibleName(ShortcutsDialog.loc("AN_Tab"));
        this.bTab.getAccessibleContext().setAccessibleDescription(ShortcutsDialog.loc("AD_Tab"));
        ShortcutsDialog.loc(this.bClear, "CTL_Clear");
        this.bClear.getAccessibleContext().setAccessibleName(ShortcutsDialog.loc("AN_Clear"));
        this.bClear.getAccessibleContext().setAccessibleDescription(ShortcutsDialog.loc("AD_Clear"));
        this.tfShortcut.setFocusTraversalKeys(1, Collections.emptySet());
        this.tfShortcut.setFocusTraversalKeys(3, Collections.emptySet());
        this.tfShortcut.getAccessibleContext().setAccessibleName(ShortcutsDialog.loc("AN_Shortcut"));
        this.tfShortcut.getAccessibleContext().setAccessibleDescription(ShortcutsDialog.loc("AD_Shortcut"));
        this.lShortcut.setDisplayedMnemonic(ShortcutsDialog.loc("CTL_Shortcut_Mnemonic").charAt(0));
        this.tfShortcut.setFocusTraversalKeys(2, Collections.emptySet());
        this.listener = new Listener();
        this.tfShortcut.addKeyListener(this.listener);
    }

    private static String loc(String key) {
        return NbBundle.getMessage(ShortcutsDialog.class, (String)key);
    }

    private static void loc(Component c, String key) {
        if (c instanceof AbstractButton) {
            Mnemonics.setLocalizedText((AbstractButton)((AbstractButton)c), (String)ShortcutsDialog.loc(key));
        } else {
            Mnemonics.setLocalizedText((JLabel)((JLabel)c), (String)ShortcutsDialog.loc(key));
        }
    }

    public ShortcutsDialog() {
        this.initComponents();
    }

    Listener getListener() {
        return this.listener;
    }

    public JLabel getLShortcut() {
        return this.lShortcut;
    }

    public JTextField getTfShortcut() {
        return this.tfShortcut;
    }

    private void initComponents() {
        this.lShortcut = new JLabel();
        this.tfShortcut = new JTextField();
        this.lConflict = new JLabel();
        this.lShortcut.setLabelFor(this.tfShortcut);
        this.lShortcut.setText("Shortcut:");
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.lConflict, -1, 484, 32767).addGroup(layout.createSequentialGroup().addComponent(this.lShortcut).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.tfShortcut, -1, 406, 32767))).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lShortcut).addComponent(this.tfShortcut, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lConflict).addContainerGap(41, 32767)));
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ShortcutsDialog.class, (String)"AN_ShortcutsDialog"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ShortcutsDialog.class, (String)"AD_ShortcutsDialog"));
    }

    public JButton getBTab() {
        return this.bTab;
    }

    public JButton getBClear() {
        return this.bClear;
    }

    public boolean isShortcutValid() {
        return this.shortcutValid;
    }

    private void setShortcutValid(boolean valid) {
        if (valid != this.shortcutValid) {
            this.shortcutValid = valid;
            this.firePropertyChange("ShortcutsDialog.PROP_SHORTCUT_VALID", !this.shortcutValid, this.shortcutValid);
        }
    }

    class Listener
    implements ActionListener,
    KeyListener {
        private KeyStroke backspaceKS;
        private KeyStroke tabKS;
        private String key;

        Listener() {
            this.backspaceKS = KeyStroke.getKeyStroke(8, 0);
            this.tabKS = KeyStroke.getKeyStroke(9, 0);
            this.key = "";
        }

        @Override
        public void keyTyped(KeyEvent e) {
            e.consume();
        }

        @Override
        public void keyPressed(KeyEvent e) {
            boolean add;
            KeyStroke keyStroke = ShortcutListener.createKeyStroke(e);
            boolean bl = add = e.getKeyCode() != 16 && e.getKeyCode() != 17 && e.getKeyCode() != 18 && e.getKeyCode() != 157 && e.getKeyCode() != 65406;
            if (keyStroke.equals(this.backspaceKS) && !this.key.equals("")) {
                int i = this.key.lastIndexOf(32);
                this.key = i < 0 ? "" : this.key.substring(0, i);
                ShortcutsDialog.this.getTfShortcut().setText(this.key);
            } else {
                this.addKeyStroke(keyStroke, add);
            }
            if (add) {
                this.updateWarning();
            } else {
                ShortcutsDialog.this.setShortcutValid(false);
            }
            e.consume();
        }

        @Override
        public void keyReleased(KeyEvent e) {
            e.consume();
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == ShortcutsDialog.this.getBClear()) {
                this.key = "";
                ShortcutsDialog.this.getTfShortcut().setText(this.key);
            } else if (e.getSource() == ShortcutsDialog.this.getBTab()) {
                this.addKeyStroke(this.tabKS, true);
            }
            this.updateWarning();
        }

        private void updateWarning() {
            String text = ShortcutsDialog.this.getTfShortcut().getText();
            ShortcutAction action = ShortcutsDialog.this.f.findActionForShortcut(text);
            if (action != null) {
                ShortcutsDialog.this.lConflict.setText(MessageFormat.format(ShortcutsDialog.loc("Shortcut_Conflict"), action.getDisplayName()));
                ShortcutsDialog.this.setShortcutValid(true);
            } else {
                ShortcutsDialog.this.lConflict.setText("");
                ShortcutsDialog.this.setShortcutValid(text != null && text.length() > 0);
            }
        }

        private void addKeyStroke(KeyStroke keyStroke, boolean add) {
            String k = KeyStrokeUtils.getKeyStrokeAsText(keyStroke);
            if (this.key.equals("")) {
                ShortcutsDialog.this.getTfShortcut().setText(k);
                if (add) {
                    this.key = k;
                }
            } else {
                ShortcutsDialog.this.getTfShortcut().setText(this.key + " " + k);
                if (add) {
                    this.key = this.key + " " + k;
                }
            }
        }
    }

}

