/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$InputLine
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.Mnemonics
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.xml.EntityCatalog
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.modules.options.keymap;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.AbstractListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle;
import javax.swing.ListModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;
import org.netbeans.core.options.keymap.api.KeyStrokeUtils;
import org.netbeans.core.options.keymap.api.ShortcutAction;
import org.netbeans.modules.options.keymap.ExportShortcutsAction;
import org.netbeans.modules.options.keymap.KeymapPanel;
import org.netbeans.modules.options.keymap.MutableShortcutsModel;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.xml.EntityCatalog;
import org.openide.xml.XMLUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ProfilesPanel
extends JPanel {
    private static final Logger LOG = Logger.getLogger(ProfilesPanel.class.getName());
    private ProfileListModel model;
    private KeymapPanel keymapPanel;
    private static final String PUBLIC_ID = "-//NetBeans//DTD Keymap Preferences 1.0//EN";
    private static final String SYSTEM_ID = "http://www.netbeans.org/dtds/KeymapPreferences-1_0.dtd";
    private static final String ATTR_ACTION_ID = "id";
    private static final String ELEM_SHORTCUT = "shortcut";
    private static final String ELEM_XML_ROOT = "keymap-preferences";
    private static final String ATTR_SHORTCUT_STRING = "shortcut_string";
    private static final String ELEM_ACTION = "action";
    private JButton deleteButton;
    private JButton duplicateButton;
    private JButton exportButton;
    private JButton importButton;
    private JScrollPane jScrollPane1;
    private JList profilesList;
    private JButton restoreButton;

    public ProfilesPanel(KeymapPanel k) {
        this.keymapPanel = k;
        this.model = new ProfileListModel();
        this.initComponents();
        this.model.setData(this.getKeymapPanel().getMutableModel().getProfiles());
        this.profilesList.setSelectedValue(this.getKeymapPanel().getMutableModel().getCurrentProfile(), true);
    }

    private KeymapPanel getKeymapPanel() {
        return this.keymapPanel;
    }

    ProfileListModel getModel() {
        return this.model;
    }

    private void initComponents() {
        this.jScrollPane1 = new JScrollPane();
        this.profilesList = new JList();
        this.duplicateButton = new JButton();
        this.restoreButton = new JButton();
        this.deleteButton = new JButton();
        this.exportButton = new JButton();
        this.importButton = new JButton();
        this.profilesList.setModel(this.model);
        this.profilesList.setSelectionMode(0);
        this.profilesList.addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent evt) {
                ProfilesPanel.this.profilesListValueChanged(evt);
            }
        });
        this.jScrollPane1.setViewportView(this.profilesList);
        this.profilesList.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ProfilesPanel.class, (String)"ProfilesPanel.profilesList.AccessibleContext.accessibleName"));
        this.profilesList.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ProfilesPanel.class, (String)"ProfilesPanel.profilesList.AccessibleContext.accessibleDescription"));
        Mnemonics.setLocalizedText((AbstractButton)this.duplicateButton, (String)NbBundle.getMessage(ProfilesPanel.class, (String)"ProfilesPanel.duplicateButton.text"));
        this.duplicateButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                ProfilesPanel.this.duplicateButtonActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.restoreButton, (String)NbBundle.getMessage(ProfilesPanel.class, (String)"ProfilesPanel.restoreButton.text"));
        this.restoreButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                ProfilesPanel.this.restoreButtonActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.deleteButton, (String)NbBundle.getMessage(ProfilesPanel.class, (String)"ProfilesPanel.deleteButton.text"));
        this.deleteButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                ProfilesPanel.this.deleteButtonActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.exportButton, (String)NbBundle.getMessage(ProfilesPanel.class, (String)"ProfilesPanel.exportButton.text"));
        this.exportButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                ProfilesPanel.this.exportButtonActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.importButton, (String)NbBundle.getMessage(ProfilesPanel.class, (String)"ProfilesPanel.importButton.text"));
        this.importButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                ProfilesPanel.this.importButtonActionPerformed(evt);
            }
        });
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jScrollPane1, -1, 234, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.duplicateButton, GroupLayout.Alignment.TRAILING).addComponent(this.restoreButton, GroupLayout.Alignment.TRAILING).addComponent(this.deleteButton, GroupLayout.Alignment.TRAILING).addComponent(this.exportButton, GroupLayout.Alignment.TRAILING).addComponent(this.importButton, GroupLayout.Alignment.TRAILING)).addContainerGap()));
        layout.linkSize(0, this.deleteButton, this.duplicateButton, this.exportButton, this.importButton, this.restoreButton);
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false).addComponent(this.jScrollPane1, GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup().addComponent(this.duplicateButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.restoreButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.deleteButton).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.exportButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.importButton))).addContainerGap(-1, 32767)));
        this.jScrollPane1.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ProfilesPanel.class, (String)"ProfilesPanel.jScrollPane1.AccessibleContext.accessibleName"));
        this.jScrollPane1.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ProfilesPanel.class, (String)"ProfilesPanel.jScrollPane1.AccessibleContext.accessibleDescription"));
        this.duplicateButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ProfilesPanel.class, (String)"ProfilesPanel.duplicateButton.AccessibleContext.accessibleDescription"));
        this.restoreButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ProfilesPanel.class, (String)"ProfilesPanel.restoreButton.AccessibleContext.accessibleDescription"));
        this.deleteButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ProfilesPanel.class, (String)"ProfilesPanel.deleteButton.AccessibleContext.accessibleDescription"));
        this.exportButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ProfilesPanel.class, (String)"ProfilesPanel.exportButton.AccessibleContext.accessibleDescription"));
        this.importButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ProfilesPanel.class, (String)"ProfilesPanel.importButton.AccessibleContext.accessibleDescription"));
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ProfilesPanel.class, (String)"ProfilesPanel.AccessibleContext.accessibleName"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ProfilesPanel.class, (String)"ProfilesPanel.AccessibleContext.accessibleDescription"));
    }

    private void restoreButtonActionPerformed(ActionEvent evt) {
        this.deleteOrRestoreSelectedProfile();
    }

    private void deleteButtonActionPerformed(ActionEvent evt) {
        this.deleteOrRestoreSelectedProfile();
    }

    private void profilesListValueChanged(ListSelectionEvent evt) {
        String profile = (String)this.profilesList.getSelectedValue();
        if (profile == null) {
            this.deleteButton.setEnabled(false);
            this.duplicateButton.setEnabled(false);
            this.exportButton.setEnabled(false);
            this.restoreButton.setEnabled(false);
            return;
        }
        this.duplicateButton.setEnabled(true);
        this.exportButton.setEnabled(true);
        MutableShortcutsModel model = this.getKeymapPanel().getMutableModel();
        if (model.isCustomProfile(profile)) {
            this.deleteButton.setEnabled(true);
            this.restoreButton.setEnabled(false);
        } else {
            boolean change = model.isChangedProfile(profile);
            if (!change) {
                change |= model.differsFromDefault(profile);
            }
            this.deleteButton.setEnabled(false);
            this.restoreButton.setEnabled(change);
        }
    }

    private void duplicateButtonActionPerformed(ActionEvent evt) {
        this.duplicateProfile();
    }

    private String duplicateProfile() {
        String newName = null;
        NotifyDescriptor.InputLine il = new NotifyDescriptor.InputLine(KeymapPanel.loc("CTL_Create_New_Profile_Message"), KeymapPanel.loc("CTL_Create_New_Profile_Title"));
        String profileToDuplicate = (String)this.profilesList.getSelectedValue();
        il.setInputText(profileToDuplicate);
        DialogDisplayer.getDefault().notify((NotifyDescriptor)il);
        if (il.getValue() == NotifyDescriptor.OK_OPTION) {
            newName = il.getInputText();
            for (String s : this.getKeymapPanel().getMutableModel().getProfiles()) {
                if (!newName.equals(s)) continue;
                NotifyDescriptor.Message md = new NotifyDescriptor.Message((Object)KeymapPanel.loc("CTL_Duplicate_Profile_Name"), 0);
                DialogDisplayer.getDefault().notify((NotifyDescriptor)md);
                return null;
            }
            MutableShortcutsModel currentModel = this.getKeymapPanel().getMutableModel();
            String currrentProfile = currentModel.getCurrentProfile();
            this.getKeymapPanel().getMutableModel().setCurrentProfile(profileToDuplicate);
            this.getKeymapPanel().getMutableModel().cloneProfile(newName);
            currentModel.setCurrentProfile(currrentProfile);
            this.model.addItem(il.getInputText());
            this.profilesList.setSelectedValue(il.getInputText(), true);
        }
        return newName;
    }

    private void exportButtonActionPerformed(ActionEvent evt) {
        JFileChooser chooser = ProfilesPanel.getFileChooser();
        chooser.setSelectedFile(new File("exported-" + this.profilesList.getSelectedValue() + "-profile.xml"));
        int ret = chooser.showSaveDialog(this);
        if (ret == 0) {
            Document doc = XMLUtil.createDocument((String)"keymap-preferences", (String)null, (String)"-//NetBeans//DTD Keymap Preferences 1.0//EN", (String)"http://www.netbeans.org/dtds/KeymapPreferences-1_0.dtd");
            Node root = doc.getElementsByTagName("keymap-preferences").item(0);
            MutableShortcutsModel kmodel = this.getKeymapPanel().getMutableModel();
            for (Object o : kmodel.getItems("")) {
                ShortcutAction sca = (ShortcutAction)o;
                String[] shortcuts = kmodel.getShortcuts(sca);
                if (shortcuts.length <= 0) continue;
                String id = sca.getId();
                Element actionElement = doc.createElement("action");
                actionElement.setAttribute("id", id);
                for (int i = 0; i < shortcuts.length; ++i) {
                    Element shortcutElement = doc.createElement("shortcut");
                    String shortcutToStore = ProfilesPanel.shortcutToPortableRepresentation(shortcuts[i]);
                    shortcutElement.setAttribute("shortcut_string", shortcutToStore);
                    actionElement.appendChild(shortcutElement);
                }
                root.appendChild(actionElement);
            }
            File f = chooser.getSelectedFile();
            try {
                FileOutputStream fos = new FileOutputStream(f);
                XMLUtil.write((Document)doc, (OutputStream)fos, (String)"UTF-8");
                fos.close();
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
    }

    private static String shortcutToPortableRepresentation(String key) {
        assert (key != null);
        StringBuilder buf = new StringBuilder();
        String delimiter = " ";
        StringTokenizer st = new StringTokenizer(key, delimiter);
        while (st.hasMoreTokens()) {
            String ks = st.nextToken().trim();
            KeyStroke keyStroke = KeyStrokeUtils.getKeyStroke(ks);
            if (keyStroke != null) {
                buf.append(Utilities.keyToString((KeyStroke)keyStroke, (boolean)true));
                if (!st.hasMoreTokens()) continue;
                buf.append(' ');
                continue;
            }
            return null;
        }
        return buf.toString();
    }

    private static JFileChooser getFileChooser() {
        JFileChooser chooser = new JFileChooser();
        XMLFileFilter filter = new XMLFileFilter();
        chooser.setAcceptAllFileFilterUsed(false);
        chooser.addChoosableFileFilter(filter);
        chooser.setFileFilter(filter);
        return chooser;
    }

    private void importButtonActionPerformed(ActionEvent evt) {
        JFileChooser chooser = ProfilesPanel.getFileChooser();
        int ret = chooser.showOpenDialog(this);
        boolean[] notFound = new boolean[1];
        if (ret == 0) {
            try {
                InputSource is = new InputSource(new FileInputStream(chooser.getSelectedFile()));
                MutableShortcutsModel kmodel = this.getKeymapPanel().getMutableModel();
                String newProfile = this.duplicateProfile();
                if (newProfile == null) {
                    return;
                }
                kmodel.setCurrentProfile(newProfile);
                Document doc = XMLUtil.parse((InputSource)is, (boolean)false, (boolean)true, (ErrorHandler)null, (EntityResolver)EntityCatalog.getDefault());
                Node root = doc.getElementsByTagName("keymap-preferences").item(0);
                NodeList nl = root.getChildNodes();
                for (int i = 0; i < nl.getLength(); ++i) {
                    Node action = nl.item(i);
                    NamedNodeMap attributes = action.getAttributes();
                    if (attributes == null) continue;
                    String id = attributes.item(0).getNodeValue();
                    ShortcutAction sca = kmodel.findActionForId(id);
                    NodeList childList = action.getChildNodes();
                    int childCount = childList.getLength();
                    LinkedHashSet<String> shortcuts = new LinkedHashSet<String>(childCount);
                    for (int j = 0; j < childCount; ++j) {
                        NamedNodeMap attrs = childList.item(j).getAttributes();
                        if (attrs == null) continue;
                        String sc = attrs.item(0).getNodeValue();
                        shortcuts.add(ExportShortcutsAction.portableRepresentationToShortcut(sc));
                    }
                    if (sca == null) {
                        notFound[0] = true;
                        LOG.log(Level.WARNING, "Failed to import binding for: {0}, keys: {1}", new Object[]{id, shortcuts});
                        continue;
                    }
                    kmodel.setShortcuts(sca, shortcuts);
                }
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            catch (SAXException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        if (notFound[0]) {
            NotifyDescriptor nd = new NotifyDescriptor((Object)NbBundle.getMessage(ProfilesPanel.class, (String)"Import.failed.unknown.id"), NbBundle.getMessage(ProfilesPanel.class, (String)"Import.failed.title"), -1, 1, new Object[]{NotifyDescriptor.OK_OPTION}, NotifyDescriptor.OK_OPTION);
            DialogDisplayer.getDefault().notify(nd);
        }
    }

    private void deleteOrRestoreSelectedProfile() {
        String currentProfile = (String)this.profilesList.getSelectedValue();
        MutableShortcutsModel keymapModel = this.getKeymapPanel().getMutableModel();
        if (keymapModel.deleteOrRestoreProfile(currentProfile)) {
            this.model.removeItem(this.profilesList.getSelectedIndex());
            this.profilesList.setSelectedIndex(0);
        } else {
            this.profilesListValueChanged(null);
        }
    }

    public String getSelectedProfile() {
        return (String)this.profilesList.getSelectedValue();
    }

    private class ProfileListModel
    extends AbstractListModel {
        private ArrayList<String> delegate;

        private ProfileListModel() {
            this.delegate = new ArrayList();
        }

        @Override
        public int getSize() {
            return this.delegate.size();
        }

        @Override
        public Object getElementAt(int index) {
            return this.delegate.get(index);
        }

        public void setData(Collection<String> c) {
            this.delegate.clear();
            this.delegate.addAll(c);
            this.fireContentsChanged(this, 0, c.size());
        }

        private void addItem(String inputText) {
            this.delegate.add(inputText);
            int size = this.delegate.size();
            this.fireContentsChanged(this, size, size);
        }

        private void removeItem(int index) {
            this.delegate.remove(index);
            this.fireContentsChanged(this, index, index);
        }
    }

    private static class XMLFileFilter
    extends FileFilter {
        private XMLFileFilter() {
        }

        @Override
        public boolean accept(File file) {
            if (file.isDirectory()) {
                return true;
            }
            if (file.getAbsolutePath().endsWith(".xml")) {
                return true;
            }
            return false;
        }

        @Override
        public String getDescription() {
            return "XML " + NbBundle.getMessage(ProfilesPanel.class, (String)"CTL_Files") + "(*.xml)";
        }
    }

}

