/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.keymap;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.util.Collection;
import javax.swing.AbstractListModel;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListModel;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.netbeans.core.options.keymap.api.ShortcutAction;
import org.netbeans.modules.options.keymap.ActionHolder;
import org.netbeans.modules.options.keymap.KeymapViewModel;
import org.netbeans.modules.options.keymap.MutableShortcutsModel;
import org.netbeans.modules.options.keymap.TableSorter;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.NbBundle;

public class ShortcutPopupPanel
extends JPanel {
    private static final AbstractListModel modelWithAddAlternative = new Model(true);
    private static final AbstractListModel modelWithoutAddAltenrnative = new Model(false);
    private static AbstractListModel model = new DefaultListModel();
    private int row;
    private JTable table;
    private JPopupMenu pm;
    private boolean displayAlternative;
    private boolean customProfile;
    private JScrollPane jScrollPane1;
    private JList list;

    ShortcutPopupPanel(JTable table, JPopupMenu pm) {
        this.initComponents();
        this.table = table;
        this.pm = pm;
        this.addFocusListener(new FocusAdapter(){

            @Override
            public void focusGained(FocusEvent e) {
                ShortcutPopupPanel.this.list.requestFocus();
                ShortcutPopupPanel.this.list.setSelectedIndex(0);
            }
        });
        this.list.addKeyListener(new KeyAdapter(){

            @Override
            public void keyTyped(KeyEvent e) {
                if (e.getKeyChar() == '\u001b') {
                    ShortcutPopupPanel.this.pm.setVisible(false);
                }
            }
        });
    }

    public void setRow(int row) {
        this.row = row;
    }

    void setDisplayAddAlternative(boolean shortcutSet) {
        model = shortcutSet ? modelWithAddAlternative : modelWithoutAddAltenrnative;
        this.list.setModel(model);
        this.displayAlternative = shortcutSet;
        this.setPreferredSize(this.list.getPreferredSize());
    }

    private void addAlternative() {
        String category = (String)this.table.getValueAt(this.row, 2);
        ShortcutAction action = ((ActionHolder)this.table.getValueAt(this.row, 0)).getAction();
        Object[] newRow = new Object[]{new ActionHolder(action, true), "", category, ""};
        ((DefaultTableModel)((TableSorter)this.table.getModel()).getTableModel()).insertRow(this.row + 1, newRow);
        this.pm.setVisible(false);
        this.table.editCellAt(this.row + 1, 1);
    }

    private void clear() {
        this.pm.setVisible(false);
        String scText = (String)this.table.getValueAt(this.row, 1);
        ShortcutAction action = ((ActionHolder)this.table.getValueAt(this.row, 0)).getAction();
        KeymapViewModel keymapViewModel = (KeymapViewModel)((TableSorter)this.table.getModel()).getTableModel();
        if (scText.length() != 0) {
            keymapViewModel.getMutableModel().removeShortcut(action, scText);
        }
        if (((ActionHolder)this.table.getValueAt(this.row, 0)).isAlternative()) {
            keymapViewModel.removeRow(this.row);
        } else {
            this.table.setValueAt("", this.row, 1);
            keymapViewModel.update();
        }
    }

    private void resetToDefault() {
        this.pm.setVisible(false);
        ShortcutAction action = ((ActionHolder)this.table.getValueAt(this.row, 0)).getAction();
        KeymapViewModel mod = (KeymapViewModel)((TableSorter)this.table.getModel()).getTableModel();
        Collection<ShortcutAction> conflicts = mod.getMutableModel().revertShortcutsToDefault(action, false);
        if (conflicts != null) {
            if (!this.overrideAll(conflicts)) {
                return;
            }
            mod.getMutableModel().revertShortcutsToDefault(action, true);
        }
        mod.update();
        mod.fireTableDataChanged();
    }

    private boolean overrideAll(Collection<ShortcutAction> actions) {
        JPanel innerPane = new JPanel();
        StringBuffer display = new StringBuffer();
        for (ShortcutAction sc : actions) {
            display.append(" '" + sc.getDisplayName() + "'<br>");
        }
        innerPane.add(new JLabel(NbBundle.getMessage(KeymapViewModel.class, (String)"Override_All", (Object)display)));
        DialogDescriptor descriptor = new DialogDescriptor((Object)innerPane, NbBundle.getMessage(KeymapViewModel.class, (String)"Conflicting_Shortcut_Dialog"), true, 0, (Object)null, null);
        DialogDisplayer.getDefault().notify((NotifyDescriptor)descriptor);
        if (descriptor.getValue().equals(DialogDescriptor.YES_OPTION)) {
            return true;
        }
        return false;
    }

    private void initComponents() {
        this.jScrollPane1 = new JScrollPane();
        this.list = new JList();
        this.jScrollPane1.setBorder(null);
        this.jScrollPane1.setHorizontalScrollBarPolicy(31);
        this.jScrollPane1.setVerticalScrollBarPolicy(21);
        this.list.setModel(model);
        this.list.addMouseMotionListener(new MouseMotionAdapter(){

            @Override
            public void mouseMoved(MouseEvent evt) {
                ShortcutPopupPanel.this.listMouseMoved(evt);
            }
        });
        this.list.addKeyListener(new KeyAdapter(){

            @Override
            public void keyPressed(KeyEvent evt) {
                ShortcutPopupPanel.this.listKeyPressed(evt);
            }
        });
        this.list.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent evt) {
                ShortcutPopupPanel.this.listMouseClicked(evt);
            }
        });
        this.jScrollPane1.setViewportView(this.list);
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane1, -1, 112, 32767));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane1, -1, 70, 32767));
    }

    private void listMouseMoved(MouseEvent evt) {
        this.list.setSelectedIndex(this.list.locationToIndex(new Point(evt.getX(), evt.getY())));
    }

    private void listMouseClicked(MouseEvent evt) {
        int index = this.list.locationToIndex(new Point(evt.getX(), evt.getY()));
        this.itemSelected(index);
    }

    void setCustomProfile(boolean customProfile) {
        this.customProfile = customProfile;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private void itemSelected(int index) {
        if (this.displayAlternative) {
            switch (index) {
                case 0: {
                    this.pm.setVisible(false);
                    this.table.editCellAt(this.row, 1);
                    return;
                }
                case 1: {
                    this.addAlternative();
                    return;
                }
                case 2: {
                    this.resetToDefault();
                    return;
                }
                case 3: {
                    this.clear();
                    return;
                }
            }
            throw new UnsupportedOperationException("Invalid popup selection item");
        }
        switch (index) {
            case 0: {
                this.pm.setVisible(false);
                this.table.editCellAt(this.row, 1);
                return;
            }
            case 1: {
                this.resetToDefault();
                return;
            }
            case 2: {
                this.clear();
                return;
            }
            default: {
                throw new UnsupportedOperationException("Invalid popup selection item");
            }
        }
    }

    private void listKeyPressed(KeyEvent evt) {
        int index = this.list.getSelectedIndex();
        if (evt.getKeyCode() == 38) {
            this.list.setSelectedIndex(index == 0 ? model.getSize() - 1 : index - 1);
        }
        if (evt.getKeyCode() == 40) {
            this.list.setSelectedIndex(index == model.getSize() - 1 ? 0 : index + 1);
        }
        if (evt.getKeyCode() == 10 && !this.list.isSelectionEmpty()) {
            this.itemSelected(this.list.getSelectedIndex());
        }
        evt.consume();
    }

    private static class Model
    extends AbstractListModel {
        private boolean displayAlternative;
        String[] elms = new String[]{NbBundle.getMessage(ShortcutPopupPanel.class, (String)"Edit"), NbBundle.getMessage(ShortcutPopupPanel.class, (String)"Add_Alternative"), NbBundle.getMessage(ShortcutPopupPanel.class, (String)"Reset_to_Default"), NbBundle.getMessage(ShortcutPopupPanel.class, (String)"Clear")};
        String[] elms0 = new String[]{this.elms[0], this.elms[2], this.elms[3]};

        public Model(boolean displayAlternative) {
            this.displayAlternative = displayAlternative;
        }

        @Override
        public int getSize() {
            return this.displayAlternative ? this.elms.length : this.elms0.length;
        }

        @Override
        public Object getElementAt(int index) {
            return this.displayAlternative ? this.elms[index] : this.elms0[index];
        }
    }

}

