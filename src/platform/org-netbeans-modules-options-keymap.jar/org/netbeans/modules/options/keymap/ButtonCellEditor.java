/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.keymap;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Window;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Arrays;
import java.util.Collection;
import java.util.EventObject;
import java.util.Set;
import javax.swing.DefaultCellEditor;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.TableCellEditor;
import org.netbeans.core.options.keymap.api.ShortcutAction;
import org.netbeans.modules.options.keymap.ActionHolder;
import org.netbeans.modules.options.keymap.KeymapViewModel;
import org.netbeans.modules.options.keymap.MutableShortcutsModel;
import org.netbeans.modules.options.keymap.ShortcutCellPanel;
import org.netbeans.modules.options.keymap.ShortcutTextField;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.NbBundle;

class ButtonCellEditor
extends DefaultCellEditor {
    private Object action;
    private KeymapViewModel model;
    private String orig;
    private KeyAdapter escapeAdapter;
    private static final ShortcutCellPanel cell = new ShortcutCellPanel();

    public ButtonCellEditor(KeymapViewModel model) {
        super(new ShortcutTextField());
        this.escapeAdapter = new KeyAdapter(){

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 27) {
                    JTable table = (JTable)cell.getParent();
                    table.getCellEditor().cancelCellEditing();
                    ButtonCellEditor.this.model.update();
                }
            }
        };
        this.model = model;
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return true;
    }

    private void removeConflictingShortcut(ShortcutAction action, String shortcutPrefix) {
        if (shortcutPrefix.contains(" ")) {
            shortcutPrefix = shortcutPrefix.substring(0, shortcutPrefix.indexOf(32));
        }
        String[] shortcuts = this.model.getMutableModel().getShortcuts(action);
        for (int i = 0; i < shortcuts.length; ++i) {
            if (!shortcuts[i].startsWith(shortcutPrefix)) continue;
            this.model.getMutableModel().removeShortcut(action, shortcuts[i]);
        }
    }

    @Override
    public boolean stopCellEditing() {
        String s = cell.toString();
        Window ancestorWindow = (Window)SwingUtilities.getRoot(cell);
        if (ancestorWindow == null) {
            return true;
        }
        JTable parent = (JTable)cell.getParent();
        ShortcutAction sca = (ShortcutAction)this.action;
        Set<ShortcutAction> conflictingAction = this.model.getMutableModel().findActionForShortcutPrefix(s);
        conflictingAction.remove(sca);
        Collection<ShortcutAction> sameScopeActions = this.model.getMutableModel().filterSameScope(conflictingAction, sca);
        if (!conflictingAction.isEmpty()) {
            if (!SwingUtilities.isEventDispatchThread()) {
                cell.getTextField().setText(this.orig);
                this.fireEditingCanceled();
                return true;
            }
            Object overrride = this.overrride(conflictingAction, sameScopeActions);
            ancestorWindow.toFront();
            parent.requestFocus();
            if (overrride.equals(DialogDescriptor.YES_OPTION)) {
                for (ShortcutAction sa : conflictingAction) {
                    this.removeConflictingShortcut(sa, s);
                }
            } else if (overrride == DialogDescriptor.CANCEL_OPTION) {
                cell.getTextField().setText(this.orig);
                this.fireEditingCanceled();
                this.setBorderEmpty();
                return true;
            }
        }
        cell.getTextField().removeActionListener(this.delegate);
        cell.getTextField().removeKeyListener(this.escapeAdapter);
        this.model.getMutableModel().removeShortcut((ShortcutAction)this.action, this.orig);
        if (s.length() != 0) {
            this.model.getMutableModel().addShortcut((ShortcutAction)this.action, s);
        }
        this.fireEditingStopped();
        this.setBorderEmpty();
        this.model.update();
        return true;
    }

    @Override
    public void cancelCellEditing() {
        cell.getTextField().setText(this.orig);
        this.fireEditingCanceled();
        this.setBorderEmpty();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        JComponent c = (JComponent)super.getTableCellEditorComponent(table, value, isSelected, row, column);
        cell.setText((String)value);
        this.orig = cell.getTextField().getText();
        this.action = ((ActionHolder)table.getValueAt(row, 0)).getAction();
        final JTextField textField = cell.getTextField();
        textField.addActionListener(this.delegate);
        textField.setBorder(new LineBorder(Color.BLACK));
        if (!Arrays.asList(textField.getKeyListeners()).contains(this.escapeAdapter)) {
            textField.addKeyListener(this.escapeAdapter);
        }
        cell.setBgColor(c.getBackground());
        cell.setFgCOlor(c.getForeground(), false);
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                textField.requestFocus();
            }
        });
        return cell;
    }

    @Override
    public Object getCellEditorValue() {
        return cell.getTextField().getText();
    }

    @Override
    public Component getComponent() {
        return cell.getTextField();
    }

    private Object overrride(Set<ShortcutAction> conflictingActions, Collection<ShortcutAction> sameScope) {
        StringBuffer conflictingActionList = new StringBuffer();
        for (ShortcutAction sa : conflictingActions) {
            conflictingActionList.append("<li>'").append(sa.getDisplayName()).append("'</li>");
        }
        JPanel innerPane = new JPanel();
        innerPane.add(new JLabel(NbBundle.getMessage(ButtonCellEditor.class, (String)(sameScope.isEmpty() ? "Override_Shortcut2" : "Override_Shortcut"), (Object)conflictingActionList)));
        DialogDescriptor descriptor = new DialogDescriptor((Object)innerPane, NbBundle.getMessage(ButtonCellEditor.class, (String)"Conflicting_Shortcut_Dialog"), true, sameScope.isEmpty() ? 1 : 0, (Object)null, null);
        DialogDisplayer.getDefault().notify((NotifyDescriptor)descriptor);
        Object o = descriptor.getValue();
        if (!sameScope.isEmpty() && o == DialogDescriptor.NO_OPTION) {
            return DialogDescriptor.CANCEL_OPTION;
        }
        return o;
    }

    private void setBorderEmpty() {
        ((JComponent)this.getComponent()).setBorder(new EmptyBorder(0, 0, 0, 0));
    }

}

