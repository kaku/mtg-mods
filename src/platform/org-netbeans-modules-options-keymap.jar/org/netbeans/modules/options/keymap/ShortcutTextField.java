/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.keymap;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Vector;
import javax.swing.JList;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.SwingUtilities;
import org.netbeans.modules.options.keymap.KeymapPanel;
import org.netbeans.modules.options.keymap.KeymapViewModel;
import org.netbeans.modules.options.keymap.MutableShortcutsModel;
import org.netbeans.modules.options.keymap.ShortcutListener;
import org.netbeans.modules.options.keymap.ShortcutProvider;
import org.openide.util.NbBundle;

public class ShortcutTextField
extends JTextField {
    private Popup popup;
    JList list = new JList();
    JScrollPane pane = new JScrollPane();

    public ShortcutTextField(String text) {
        super(text);
        this.pane.setViewportView(this.list);
        this.pane.setMaximumSize(new Dimension(350, 350));
        this.pane.setHorizontalScrollBarPolicy(31);
        this.addKeyListener(new ShortcutListener(true));
        this.addKeyListener(new ShortcutCompletionListener());
        this.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                if (ShortcutTextField.this.popup != null) {
                    ShortcutTextField.this.popup.hide();
                    ShortcutTextField.this.popup = null;
                }
            }
        });
        this.addFocusListener(new FocusAdapter(){

            @Override
            public void focusGained(FocusEvent e) {
                JTextField textField = (JTextField)e.getComponent();
                ((ShortcutListener)textField.getKeyListeners()[0]).clear();
                textField.selectAll();
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (ShortcutTextField.this.popup != null) {
                    ShortcutTextField.this.popup.hide();
                    ShortcutTextField.this.popup = null;
                }
            }
        });
        this.list.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent e) {
                int selectedIndex = ((JList)e.getSource()).getSelectedIndex();
                ShortcutTextField.this.confirm(selectedIndex);
            }
        });
    }

    public ShortcutTextField() {
        this("");
    }

    private void confirm(int selectedIndex) {
        if (selectedIndex != -1) {
            Object elementAt = this.list.getModel().getElementAt(selectedIndex);
            this.setText(elementAt.toString());
        }
        this.popup.hide();
        this.popup = null;
    }

    private void ensureSelectionVisible(int index) {
        Rectangle bounds = this.list.getCellBounds(index, index);
        if (bounds != null) {
            this.list.scrollRectToVisible(bounds);
        }
    }

    private Vector<String> getFreeShortcuts(String prefix) {
        Vector<String> vec = new Vector<String>();
        for (String s : this.getAllFreeShortcuts()) {
            if (!s.startsWith(prefix)) continue;
            vec.add(s);
        }
        if (vec.size() == 0) {
            vec.add(NbBundle.getMessage(ShortcutTextField.class, (String)"No_Free_Shortcut", (Object)prefix));
        }
        return vec;
    }

    private Set<String> getAllFreeShortcuts() {
        KeymapViewModel model = ((KeymapPanel)SwingUtilities.getAncestorOfClass(KeymapPanel.class, this)).getModel();
        Set<String> allCurrentlyUsedShortcuts = model.getMutableModel().getAllCurrentlyUsedShortcuts();
        LinkedHashSet<String> result = new LinkedHashSet<String>();
        result.addAll(ShortcutProvider.getSet());
        result.removeAll(allCurrentlyUsedShortcuts);
        return result;
    }

    private class ShortcutCompletionListener
    extends KeyAdapter {
        private ShortcutCompletionListener() {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            JTextField tf = (JTextField)e.getSource();
            int selectedIndex = ShortcutTextField.this.list.getSelectedIndex();
            ShortcutTextField.this.list.setListData(ShortcutTextField.this.getFreeShortcuts(tf.getText()));
            int keyCode = e.getKeyCode();
            if (ShortcutTextField.this.popup == null) {
                if (keyCode == 10 || keyCode == 27) {
                    return;
                }
                Point p = new Point(tf.getX(), tf.getY());
                SwingUtilities.convertPointToScreen(p, tf.getParent());
                ShortcutTextField.this.popup = PopupFactory.getSharedInstance().getPopup(tf, ShortcutTextField.this.pane, p.x, p.y + tf.getHeight() + 1);
            }
            ShortcutTextField.this.pane.setPreferredSize(new Dimension(ShortcutTextField.this.list.getPreferredSize().width + ShortcutTextField.this.pane.getVerticalScrollBar().getPreferredSize().width + 2, Math.min(350, ShortcutTextField.this.list.getPreferredSize().height) + 5));
            ShortcutTextField.this.popup.show();
            switch (keyCode) {
                case 40: {
                    int index = selectedIndex == -1 || selectedIndex == ShortcutTextField.this.list.getModel().getSize() - 1 ? 0 : selectedIndex + 1;
                    ShortcutTextField.this.list.setSelectedIndex(index);
                    ShortcutTextField.this.ensureSelectionVisible(index);
                    e.consume();
                    break;
                }
                case 38: {
                    int index;
                    int n = index = selectedIndex == -1 ? 0 : selectedIndex - 1;
                    if (selectedIndex == 0) {
                        index = ShortcutTextField.this.list.getModel().getSize() - 1;
                    }
                    ShortcutTextField.this.list.setSelectedIndex(index);
                    ShortcutTextField.this.ensureSelectionVisible(index);
                    e.consume();
                    break;
                }
                case 27: {
                    ShortcutTextField.this.popup.hide();
                    ShortcutTextField.this.popup = null;
                    e.consume();
                    break;
                }
                case 10: {
                    ShortcutTextField.this.confirm(selectedIndex);
                }
            }
        }
    }

}

