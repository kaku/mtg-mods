/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.windows.WindowManager
 */
package org.netbeans.modules.options.keymap;

import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.KeyStroke;
import org.netbeans.core.options.keymap.api.KeyStrokeUtils;
import org.netbeans.core.options.keymap.api.ShortcutAction;
import org.netbeans.core.options.keymap.spi.KeymapManager;
import org.netbeans.modules.options.keymap.KeymapModel;
import org.netbeans.modules.options.keymap.LayersBridge;
import org.netbeans.modules.options.keymap.XMLStorage;
import org.openide.ErrorManager;
import org.openide.awt.HtmlBrowser;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.windows.WindowManager;

public class ExportShortcutsAction {
    private static Action exportIDEActionsAction = new AbstractAction(){

        @Override
        public void actionPerformed(ActionEvent e) {
            LayersBridge layersBridge = new LayersBridge();
            Map<String, Set<ShortcutAction>> categoryToActions = layersBridge.getActions();
            Map m = ExportShortcutsAction.resolveNames(categoryToActions);
            ExportShortcutsAction.generateLayersXML(layersBridge, m);
        }
    };
    private static Action exportIDEShortcutsAction = new AbstractAction(){

        @Override
        public void actionPerformed(ActionEvent e) {
            HashMap<String, Map<String, ShortcutAction>> allKeyMaps = new HashMap<String, Map<String, ShortcutAction>>();
            LayersBridge layersBridge = new LayersBridge();
            layersBridge.getActions();
            List<String> keyMaps = layersBridge.getProfiles();
            for (String keyMapName : keyMaps) {
                Map<ShortcutAction, Set<String>> actionToShortcuts = layersBridge.getKeymap(keyMapName);
                Map<String, ShortcutAction> shortcutToAction = LayersBridge.shortcutToAction(actionToShortcuts);
                allKeyMaps.put(keyMapName, shortcutToAction);
            }
            ExportShortcutsAction.generateLayersXML(layersBridge, allKeyMaps);
        }
    };
    private static Action exportEditorShortcutsAction = new AbstractAction(){

        @Override
        public void actionPerformed(ActionEvent e) {
            KeymapManager editorBridge = null;
            for (KeymapManager km : KeymapModel.getKeymapManagerInstances()) {
                if (!"EditorBridge".equals(km.getName())) continue;
                editorBridge = km;
                break;
            }
            if (editorBridge != null) {
                final KeymapManager feb = editorBridge;
                final Object[] o = new Object[1];
                KeymapModel.waitFinished(new Runnable(){

                    @Override
                    public void run() {
                        o[0] = feb.getKeymap(feb.getCurrentProfile());
                    }
                });
                ExportShortcutsAction.generateEditorXML((Map)o[0]);
            }
        }

    };
    private static Action exportShortcutsToHTMLAction = new AbstractAction(){

        @Override
        public void actionPerformed(ActionEvent e) {
            ExportShortcutsAction.exportShortcutsOfAllProfilesToHTML();
        }
    };

    private ExportShortcutsAction() {
    }

    public static Action getExportIDEActionsAction() {
        return exportIDEActionsAction;
    }

    public static Action getExportIDEShortcutsAction() {
        return exportIDEShortcutsAction;
    }

    public static Action getExportEditorShortcutsAction() {
        return exportEditorShortcutsAction;
    }

    public static Action getExportShortcutsToHTMLAction() {
        return exportShortcutsToHTMLAction;
    }

    public static void exportShortcutsOfProfileToHTML(String profile) {
        boolean showSystemSpecificShortcuts = true;
        ExportShortcutsAction.exportShortcutsToHTML(new KeymapModel(), Arrays.asList(profile), true);
    }

    private static void exportShortcutsOfAllProfilesToHTML() {
        KeymapModel keymapModel = new KeymapModel();
        List<String> allProfiles = keymapModel.getProfiles();
        boolean showSystemSpecificShortcuts = false;
        ExportShortcutsAction.exportShortcutsToHTML(keymapModel, allProfiles, false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void exportShortcutsToHTML(KeymapModel keymapModel, Collection<String> profiles, boolean displayHumanReadibleShortcuts) {
        TreeMap<String, Map<ShortcutAction, Set<String>>> keymaps = new TreeMap<String, Map<ShortcutAction, Set<String>>>();
        for (String profile : profiles) {
            keymaps.put(profile, keymapModel.getKeymap(profile));
        }
        try {
            StringBuffer sb = new StringBuffer();
            XMLStorage.Attribs attribs = new XMLStorage.Attribs(true);
            XMLStorage.generateFolderStart(sb, "html", attribs, "");
            XMLStorage.generateFolderStart(sb, "body", attribs, "  ");
            attribs.add("border", "1");
            attribs.add("cellpadding", "1");
            attribs.add("cellspacing", "0");
            XMLStorage.generateFolderStart(sb, "table", attribs, "    ");
            attribs = new XMLStorage.Attribs(true);
            XMLStorage.generateFolderStart(sb, "tr", attribs, "      ");
            XMLStorage.generateFolderStart(sb, "td", attribs, "        ");
            XMLStorage.generateFolderStart(sb, "h2", attribs, "        ");
            sb.append("Action Name");
            XMLStorage.generateFolderEnd(sb, "h2", "        ");
            XMLStorage.generateFolderEnd(sb, "td", "        ");
            for (String profile2 : keymaps.keySet()) {
                XMLStorage.generateFolderStart(sb, "td", attribs, "        ");
                XMLStorage.generateFolderStart(sb, "h2", attribs, "        ");
                sb.append(profile2);
                XMLStorage.generateFolderEnd(sb, "h2", "        ");
                XMLStorage.generateFolderEnd(sb, "td", "        ");
            }
            ExportShortcutsAction.exportShortcutsToHTML2(keymapModel, sb, keymaps, displayHumanReadibleShortcuts);
            XMLStorage.generateFolderEnd(sb, "table", "    ");
            XMLStorage.generateFolderEnd(sb, "body", "  ");
            XMLStorage.generateFolderEnd(sb, "html", "");
            FileObject fo = FileUtil.createData((FileObject)FileUtil.getConfigRoot(), (String)"shortcuts.html");
            FileLock fileLock = fo.lock();
            try {
                OutputStream outputStream = fo.getOutputStream(fileLock);
                Throwable throwable = null;
                try {
                    OutputStreamWriter writer = new OutputStreamWriter(outputStream);
                    Throwable throwable2 = null;
                    try {
                        writer.write(sb.toString());
                        writer.close();
                        if (fo.canRead() && displayHumanReadibleShortcuts) {
                            HtmlBrowser.URLDisplayer.getDefault().showURLExternal(fo.toURL());
                        }
                    }
                    catch (Throwable x2) {
                        throwable2 = x2;
                        throw x2;
                    }
                    finally {
                        if (writer != null) {
                            if (throwable2 != null) {
                                try {
                                    writer.close();
                                }
                                catch (Throwable x2) {
                                    throwable2.addSuppressed(x2);
                                }
                            } else {
                                writer.close();
                            }
                        }
                    }
                }
                catch (Throwable x2) {
                    throwable = x2;
                    throw x2;
                }
                finally {
                    if (outputStream != null) {
                        if (throwable != null) {
                            try {
                                outputStream.close();
                            }
                            catch (Throwable x2) {
                                throwable.addSuppressed(x2);
                            }
                        } else {
                            outputStream.close();
                        }
                    }
                }
            }
            catch (IOException ex) {
                ErrorManager.getDefault().notify((Throwable)ex);
            }
            finally {
                fileLock.releaseLock();
            }
        }
        catch (IOException ex) {
            ErrorManager.getDefault().notify((Throwable)ex);
        }
    }

    private static void exportShortcutsToHTML2(KeymapModel keymapModel, StringBuffer sb, Map<String, Map<ShortcutAction, Set<String>>> keymaps, boolean displayHumanReadibleShortcuts) {
        ArrayList<String> categories = new ArrayList<String>(keymapModel.getActionCategories());
        Collections.sort(categories);
        XMLStorage.Attribs attribs = new XMLStorage.Attribs(true);
        for (String category : categories) {
            XMLStorage.generateFolderStart(sb, "tr", attribs, "      ");
            attribs.add("colspan", Integer.toString(keymaps.size() + 1));
            attribs.add("rowspan", "1");
            XMLStorage.generateFolderStart(sb, "td", attribs, "        ");
            attribs = new XMLStorage.Attribs(true);
            XMLStorage.generateFolderStart(sb, "h3", attribs, "        ");
            sb.append(category);
            XMLStorage.generateFolderEnd(sb, "h3", "        ");
            XMLStorage.generateFolderEnd(sb, "td", "        ");
            XMLStorage.generateFolderEnd(sb, "tr", "      ");
            ExportShortcutsAction.exportShortcutsToHTML3(sb, keymapModel, category, keymaps, displayHumanReadibleShortcuts);
        }
    }

    private static void exportShortcutsToHTML3(StringBuffer sb, KeymapModel keymapModel, String category, Map<String, Map<ShortcutAction, Set<String>>> keymaps, boolean displayHumanReadibleShortcuts) {
        Set<ShortcutAction> actions = keymapModel.getActions(category);
        TreeMap<String, ShortcutAction> sortedActions = new TreeMap<String, ShortcutAction>();
        for (ShortcutAction action : actions) {
            sortedActions.put(action.getDisplayName(), action);
        }
        XMLStorage.Attribs attribs = new XMLStorage.Attribs(true);
        for (Map.Entry entry : sortedActions.entrySet()) {
            String actionName = (String)entry.getKey();
            ShortcutAction action2 = (ShortcutAction)entry.getValue();
            XMLStorage.generateFolderStart(sb, "tr", attribs, "      ");
            XMLStorage.generateFolderStart(sb, "td", attribs, "        ");
            sb.append(actionName);
            XMLStorage.generateFolderEnd(sb, "td", "        ");
            for (String profile : keymaps.keySet()) {
                Map<ShortcutAction, Set<String>> keymap = keymaps.get(profile);
                Set<String> shortcuts = keymap.get(action2);
                XMLStorage.generateFolderStart(sb, "td", attribs, "        ");
                ExportShortcutsAction.printShortcuts(shortcuts, sb, displayHumanReadibleShortcuts);
                XMLStorage.generateFolderEnd(sb, "td", "        ");
            }
            XMLStorage.generateFolderEnd(sb, "tr", "      ");
        }
    }

    private static void printShortcuts(Set<String> shortcuts, StringBuffer sb, boolean displayHumanReadibleShortcuts) {
        if (shortcuts == null) {
            sb.append('-');
            return;
        }
        Iterator<String> it = shortcuts.iterator();
        while (it.hasNext()) {
            String shortcut = it.next();
            if (displayHumanReadibleShortcuts) {
                sb.append(ExportShortcutsAction.portableRepresentationToShortcut(shortcut));
            } else {
                sb.append(shortcut);
            }
            if (!it.hasNext()) continue;
            sb.append(", ");
        }
    }

    static String portableRepresentationToShortcut(String portable) {
        assert (portable != null);
        StringBuilder buf = new StringBuilder();
        String delimiter = " ";
        StringTokenizer st = new StringTokenizer(portable, delimiter);
        while (st.hasMoreTokens()) {
            String ks = st.nextToken().trim();
            KeyStroke keyStroke = Utilities.stringToKey((String)ks);
            if (keyStroke != null) {
                buf.append(KeyStrokeUtils.getKeyStrokeAsText(keyStroke));
                if (!st.hasMoreTokens()) continue;
                buf.append(' ');
                continue;
            }
            return null;
        }
        return buf.toString();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void generateLayersXML(LayersBridge layersBridge, Map<String, Map<String, ShortcutAction>> categoryToActions) {
        Writer fw = null;
        try {
            fw = ExportShortcutsAction.openWriter();
            if (fw == null) {
                return;
            }
            StringBuffer sb = XMLStorage.generateHeader();
            XMLStorage.Attribs attribs = new XMLStorage.Attribs(true);
            XMLStorage.generateFolderStart(sb, "filesystem", attribs, "");
            attribs.add("name", "Keymaps");
            XMLStorage.generateFolderStart(sb, "folder", attribs, "    ");
            ExportShortcutsAction.generateShadowsToXML(layersBridge, sb, categoryToActions, "        ");
            XMLStorage.generateFolderEnd(sb, "folder", "    ");
            XMLStorage.generateFolderEnd(sb, "filesystem", "");
            fw.write(sb.toString());
        }
        catch (IOException e) {
            ErrorManager.getDefault().notify((Throwable)e);
        }
        finally {
            try {
                if (fw != null) {
                    fw.flush();
                    fw.close();
                }
            }
            catch (IOException e) {}
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void generateEditorXML(Map<ShortcutAction, Set<String>> actionToShortcuts) {
        Writer fw = null;
        try {
            fw = ExportShortcutsAction.openWriter();
            if (fw == null) {
                return;
            }
            StringBuffer sb = XMLStorage.generateHeader();
            XMLStorage.Attribs attribs = new XMLStorage.Attribs(true);
            XMLStorage.generateFolderStart(sb, "bindings", attribs, "");
            TreeMap<String, Set<String>> sortedMap = new TreeMap<String, Set<String>>();
            for (ShortcutAction action : actionToShortcuts.keySet()) {
                sortedMap.put(action.getDisplayName(), actionToShortcuts.get(action));
            }
            for (String actionName : sortedMap.keySet()) {
                Set shortcuts = (Set)sortedMap.get(actionName);
                for (String shortcut : shortcuts) {
                    attribs = new XMLStorage.Attribs(true);
                    attribs.add("actionName", actionName);
                    attribs.add("key", shortcut);
                    XMLStorage.generateLeaf(sb, "bind", attribs, "  ");
                }
            }
            XMLStorage.generateFolderEnd(sb, "bindings", "");
            fw.write(sb.toString());
        }
        catch (IOException e) {
            ErrorManager.getDefault().notify((Throwable)e);
        }
        finally {
            try {
                if (fw != null) {
                    fw.flush();
                    fw.close();
                }
            }
            catch (IOException e) {}
        }
    }

    private static Map<String, Map<String, ShortcutAction>> resolveNames(Map<String, Set<ShortcutAction>> categoryToActions) {
        HashMap<String, Map<String, ShortcutAction>> result = new HashMap<String, Map<String, ShortcutAction>>();
        for (Map.Entry<String, Set<ShortcutAction>> entry : categoryToActions.entrySet()) {
            String category = entry.getKey();
            Set<ShortcutAction> actions = entry.getValue();
            HashMap<String, ShortcutAction> actionsMap = new HashMap<String, ShortcutAction>();
            for (ShortcutAction action : actions) {
                actionsMap.put(action.getDisplayName(), action);
            }
            result.put(category, actionsMap);
        }
        return result;
    }

    private static void generateShadowsToXML(LayersBridge layersBridge, StringBuffer sb, Map<String, Map<String, ShortcutAction>> shortcutToAction, String indentation) {
        for (String key : shortcutToAction.keySet()) {
            Map<String, ShortcutAction> value = shortcutToAction.get(key);
            XMLStorage.Attribs attribs = new XMLStorage.Attribs(true);
            attribs.add("name", key);
            XMLStorage.generateFolderStart(sb, "folder", attribs, indentation);
            ExportShortcutsAction.generateShadowsToXML2(layersBridge, sb, value, "    " + indentation);
            XMLStorage.generateFolderEnd(sb, "folder", indentation);
        }
    }

    private static void generateShadowsToXML2(LayersBridge layersBridge, StringBuffer sb, Map<String, ShortcutAction> shortcutToAction, String indentation) {
        for (String key : shortcutToAction.keySet()) {
            ShortcutAction value = shortcutToAction.get(key);
            DataObject dob = layersBridge.getDataObject(value);
            if (dob == null) {
                System.out.println("no Dataobject " + value);
                continue;
            }
            FileObject fo = dob.getPrimaryFile();
            XMLStorage.Attribs attribs = new XMLStorage.Attribs(true);
            attribs.add("name", key + ".shadow");
            XMLStorage.generateFolderStart(sb, "file", attribs, indentation);
            XMLStorage.Attribs attribs2 = new XMLStorage.Attribs(true);
            attribs2.add("name", "originalFile");
            attribs2.add("stringvalue", fo.getPath());
            XMLStorage.generateLeaf(sb, "attr", attribs2, indentation + "    ");
            XMLStorage.generateFolderEnd(sb, "file", indentation);
        }
    }

    private static Writer openWriter() throws IOException {
        JFileChooser fileChooser = new JFileChooser();
        int result = fileChooser.showSaveDialog(WindowManager.getDefault().getMainWindow());
        if (result != 0) {
            return null;
        }
        File f = fileChooser.getSelectedFile();
        return new FileWriter(f);
    }

    private static String loc(String key) {
        return NbBundle.getMessage(ExportShortcutsAction.class, (String)key);
    }

}

