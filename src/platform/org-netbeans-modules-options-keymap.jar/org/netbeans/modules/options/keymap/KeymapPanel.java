/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.awt.Mnemonics
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 *  org.openide.util.TaskListener
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.options.keymap;

import java.awt.AWTEvent;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.RowSorterEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.text.Document;
import org.netbeans.core.options.keymap.api.ShortcutAction;
import org.netbeans.modules.options.keymap.ActionHolder;
import org.netbeans.modules.options.keymap.ButtonCellEditor;
import org.netbeans.modules.options.keymap.ButtonCellRenderer;
import org.netbeans.modules.options.keymap.ExportShortcutsAction;
import org.netbeans.modules.options.keymap.KeymapModel;
import org.netbeans.modules.options.keymap.KeymapViewModel;
import org.netbeans.modules.options.keymap.MutableShortcutsModel;
import org.netbeans.modules.options.keymap.Popupable;
import org.netbeans.modules.options.keymap.ProfilesPanel;
import org.netbeans.modules.options.keymap.ShortcutCellPanel;
import org.netbeans.modules.options.keymap.ShortcutListener;
import org.netbeans.modules.options.keymap.ShortcutPopupPanel;
import org.netbeans.modules.options.keymap.SpecialkeyPanel;
import org.netbeans.modules.options.keymap.TableSorter;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.TaskListener;
import org.openide.util.Utilities;

public class KeymapPanel
extends JPanel
implements ActionListener,
Popupable,
ChangeListener {
    private static final int SEARCH_DELAY_TIME_LONG = 300;
    private static final int SEARCH_DELAY_TIME_SHORT = 20;
    private volatile KeymapViewModel keymapModel;
    private TableSorter sorter;
    private JPopupMenu popup = new JPopupMenu();
    private boolean ignoreActionEvents;
    private Popup searchPopup;
    private SpecialkeyPanel specialkeyList;
    private ShortcutPopupPanel popupPanel;
    private JProgressBar actionProgress;
    private JTable actionsTable;
    private JPanel actionsView;
    private JButton btnPrintAsHTML;
    private JComboBox cbProfile;
    private Box.Filler filler1;
    private Box.Filler filler2;
    private JPanel jPanel1;
    private JScrollPane jScrollPane1;
    private JSeparator jSeparator1;
    private JLabel lProfile;
    private JList liShortcuts;
    private JButton manageButton;
    private JButton moreButton;
    private JTextField searchField;
    private JLabel searchLabel;
    private JTextField searchSCField;
    private JLabel searchSCLabel;
    private JScrollPane spShortcuts;
    private JLabel waitLabel;

    public KeymapPanel() {
        this.sorter = new TableSorter(this.getModel());
        this.initComponents();
        this.specialkeyList = new SpecialkeyPanel(this, this.searchSCField);
        this.moreButton.addFocusListener(new FocusAdapter(){

            @Override
            public void focusLost(FocusEvent e) {
                KeymapPanel.this.hidePopup();
            }
        });
        this.sorter.setTableHeader(this.actionsTable.getTableHeader());
        this.sorter.getTableHeader().setReorderingAllowed(false);
        this.actionsTable.setSelectionMode(0);
        this.actionsTable.setAutoscrolls(true);
        ActionListener al = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                KeymapPanel.this.getModel().setSearchText(KeymapPanel.this.searchField.getText());
                KeymapPanel.this.getModel().update();
            }
        };
        final Timer searchDelayTimer = new Timer(300, al);
        searchDelayTimer.setRepeats(false);
        this.searchField.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent e) {
                KeymapPanel.this.searchSCField.setText("");
                ((ShortcutListener)KeymapPanel.this.searchSCField.getKeyListeners()[0]).clear();
                if (KeymapPanel.this.searchField.getText().length() > 3) {
                    searchDelayTimer.setInitialDelay(20);
                }
                searchDelayTimer.restart();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                if (KeymapPanel.this.searchField.getText().length() > 3) {
                    searchDelayTimer.setInitialDelay(300);
                }
                searchDelayTimer.restart();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                KeymapPanel.this.searchSCField.setText("");
                KeymapPanel.this.getModel().setSearchText(KeymapPanel.this.searchField.getText());
                KeymapPanel.this.getModel().update();
            }
        });
        this.searchSCField.addKeyListener(new ShortcutListener(false));
        ActionListener al2 = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                KeymapPanel.this.narrowByShortcut();
            }
        };
        final Timer searchDelayTimer2 = new Timer(20, al2);
        searchDelayTimer2.setRepeats(false);
        this.searchSCField.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent e) {
                KeymapPanel.this.searchField.setText("");
                searchDelayTimer2.restart();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                searchDelayTimer2.restart();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                searchDelayTimer2.restart();
            }
        });
        this.actionsTable.addMouseListener(new ButtonCellMouseListener(this.actionsTable));
        this.actionsTable.addKeyListener(new KeyAdapter(){

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() != 525 && e.getKeyCode() != 113) {
                    return;
                }
                int leadRow = KeymapPanel.this.actionsTable.getSelectionModel().getLeadSelectionIndex();
                int leadColumn = KeymapPanel.this.actionsTable.getColumnModel().getSelectionModel().getLeadSelectionIndex();
                if (leadRow != -1 && leadColumn != -1 && !KeymapPanel.this.actionsTable.isEditing()) {
                    KeymapPanel.this.showPopupMenu(leadRow, leadColumn, -1, -1);
                    e.consume();
                }
            }
        });
        TableColumn column = this.actionsTable.getColumnModel().getColumn(1);
        column.setCellEditor(new ButtonCellEditor(this.getModel()));
        column.setCellRenderer(new ButtonCellRenderer(this.actionsTable.getDefaultRenderer(ButtonCellRenderer.class)));
        this.setColumnWidths();
        this.popupPanel = new ShortcutPopupPanel(this.actionsTable, this.popup);
        this.popup.add(this.popupPanel);
        this.cbProfile.addActionListener(this);
        this.manageButton.addActionListener(this);
    }

    private void narrowByShortcut() {
        if (this.searchSCField.getText().length() != 0) {
            final String searchText = this.searchSCField.getText();
            this.getModel().runWithoutEvents(new Runnable(){

                @Override
                public void run() {
                    KeymapPanel.this.getModel().getDataVector().removeAllElements();
                    for (String categorySet : KeymapPanel.this.getModel().getCategories().keySet()) {
                        for (String category : KeymapPanel.this.getModel().getCategories().get(categorySet)) {
                            for (Object o : KeymapPanel.this.getMutableModel().getItems(category, false)) {
                                ShortcutAction sca = (ShortcutAction)o;
                                String[] shortcuts = KeymapPanel.this.getMutableModel().getShortcuts(sca);
                                for (int i = 0; i < shortcuts.length; ++i) {
                                    String shortcut = shortcuts[i];
                                    if (!KeymapPanel.this.searched(shortcut, searchText)) continue;
                                    KeymapPanel.this.getModel().addRow(new Object[]{new ActionHolder(sca, false), shortcut, category, ""});
                                }
                            }
                        }
                    }
                }
            });
            this.getModel().fireTableDataChanged();
        } else {
            this.getModel().update();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    KeymapViewModel getModel() {
        if (this.keymapModel == null) {
            KeymapViewModel tmpModel = new KeymapViewModel();
            KeymapPanel keymapPanel = this;
            synchronized (keymapPanel) {
                if (this.keymapModel == null) {
                    this.keymapModel = tmpModel;
                    tmpModel.getMutableModel().addChangeListener(this);
                }
            }
        }
        return this.keymapModel;
    }

    MutableShortcutsModel getMutableModel() {
        return this.getModel().getMutableModel();
    }

    void applyChanges() {
        this.stopCurrentCellEditing();
        this.getMutableModel().apply();
    }

    void cancel() {
        this.stopCurrentCellEditing();
        if (this.keymapModel == null) {
            return;
        }
        this.getMutableModel().cancel();
    }

    boolean dataValid() {
        return true;
    }

    boolean isChanged() {
        return this.getMutableModel().isChanged();
    }

    void update() {
        class I
        implements Runnable,
        TaskListener {
            int stage;

            I() {
            }

            @Override
            public void run() {
                if (this.stage > 0) {
                    ((CardLayout)KeymapPanel.this.actionsView.getLayout()).show(KeymapPanel.this.actionsView, "actions");
                } else {
                    KeymapPanel.this.getMutableModel().refreshActions();
                    Task t = KeymapPanel.this.getModel().postUpdate();
                    t.addTaskListener((TaskListener)this);
                }
            }

            public void taskFinished(Task t) {
                ++this.stage;
                SwingUtilities.invokeLater(this);
            }
        }
        this.getModel().setSearchText("");
        this.searchSCField.setText("");
        ((ShortcutListener)this.searchSCField.getKeyListeners()[0]).clear();
        this.searchField.setText("");
        this.refreshProfileCombo();
        KeymapModel.RP.post((Runnable)new I());
    }

    private void refreshProfileCombo() {
        this.ignoreActionEvents = true;
        String currentProfile = this.getMutableModel().getCurrentProfile();
        List<String> keymaps = this.getMutableModel().getProfiles();
        DefaultComboBoxModel<Object> model = new DefaultComboBoxModel<Object>(keymaps.toArray());
        currentProfile = this.getMutableModel().getProfileDisplayName(currentProfile);
        this.cbProfile.setModel(model);
        this.cbProfile.setSelectedItem(currentProfile);
        this.ignoreActionEvents = false;
    }

    private void stopCurrentCellEditing() {
        int row = this.actionsTable.getEditingRow();
        int col = this.actionsTable.getEditingColumn();
        if (row != -1) {
            this.actionsTable.getCellEditor(row, col).stopCellEditing();
        }
    }

    private boolean searched(String shortcut, String searchText) {
        if (searchText.length() == 0 || shortcut.startsWith(searchText) || shortcut.contains(searchText)) {
            return true;
        }
        return false;
    }

    private void setColumnWidths() {
        TableColumn column = null;
        block6 : for (int i = 0; i < this.actionsTable.getColumnCount(); ++i) {
            column = this.actionsTable.getColumnModel().getColumn(i);
            switch (i) {
                case 0: {
                    column.setPreferredWidth(250);
                    continue block6;
                }
                case 1: {
                    column.setPreferredWidth(175);
                    continue block6;
                }
                case 2: {
                    column.setPreferredWidth(60);
                    continue block6;
                }
                case 3: {
                    column.setPreferredWidth(60);
                }
            }
        }
    }

    private void initComponents() {
        this.lProfile = new JLabel();
        this.cbProfile = new JComboBox();
        this.manageButton = new JButton();
        this.spShortcuts = new JScrollPane();
        this.liShortcuts = new JList();
        this.searchField = new JTextField();
        this.searchLabel = new JLabel();
        this.searchSCLabel = new JLabel();
        this.searchSCField = new JTextField();
        this.moreButton = new JButton();
        this.actionsView = new JPanel();
        this.jPanel1 = new JPanel();
        this.actionProgress = new JProgressBar();
        this.waitLabel = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this.actionsTable = new KeymapTable();
        this.filler1 = new Box.Filler(new Dimension(0, 0), new Dimension(0, 0), new Dimension(32767, 0));
        this.filler2 = new Box.Filler(new Dimension(0, 0), new Dimension(0, 0), new Dimension(0, 32767));
        this.btnPrintAsHTML = new JButton();
        this.jSeparator1 = new JSeparator();
        this.lProfile.setLabelFor(this.cbProfile);
        Mnemonics.setLocalizedText((JLabel)this.lProfile, (String)NbBundle.getMessage(KeymapPanel.class, (String)"CTL_Keymap_Name"));
        Mnemonics.setLocalizedText((AbstractButton)this.manageButton, (String)NbBundle.getMessage(KeymapPanel.class, (String)"CTL_Duplicate"));
        this.spShortcuts.setViewportView(this.liShortcuts);
        this.searchField.setText(NbBundle.getMessage(KeymapPanel.class, (String)"KeymapPanel.searchField.text"));
        this.searchLabel.setLabelFor(this.searchField);
        Mnemonics.setLocalizedText((JLabel)this.searchLabel, (String)NbBundle.getMessage(KeymapPanel.class, (String)"KeymapPanel.searchLabel.text"));
        this.searchSCLabel.setLabelFor(this.searchSCField);
        Mnemonics.setLocalizedText((JLabel)this.searchSCLabel, (String)NbBundle.getMessage(KeymapPanel.class, (String)"KeymapPanel.searchSCLabel.text"));
        this.searchSCField.setText(NbBundle.getMessage(KeymapPanel.class, (String)"KeymapPanel.searchSCField.text"));
        Mnemonics.setLocalizedText((AbstractButton)this.moreButton, (String)NbBundle.getMessage(KeymapPanel.class, (String)"KeymapPanel.moreButton.text"));
        this.moreButton.setBorder(BorderFactory.createEtchedBorder());
        this.moreButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                KeymapPanel.this.moreButtonActionPerformed(evt);
            }
        });
        this.actionsView.setLayout(new CardLayout());
        this.actionProgress.setIndeterminate(true);
        this.actionProgress.setString(NbBundle.getMessage(KeymapPanel.class, (String)"KeymapPanel.actionProgress.string"));
        this.waitLabel.setFont(new Font("Dialog", 2, 12));
        Mnemonics.setLocalizedText((JLabel)this.waitLabel, (String)NbBundle.getMessage(KeymapPanel.class, (String)"KeymapPanel.waitLabel.text"));
        GroupLayout jPanel1Layout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addGap(0, 0, 32767).addComponent(this.waitLabel).addGap(0, 0, 32767)).addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup().addContainerGap(225, 32767).addComponent(this.actionProgress, -2, -1, -2).addContainerGap(224, 32767)));
        jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addGap(0, 57, 32767).addComponent(this.waitLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.actionProgress, -2, -1, -2).addGap(0, 90, 32767)));
        this.actionsView.add((Component)this.jPanel1, "wait");
        this.jScrollPane1.setPreferredSize(new Dimension(453, 100));
        this.actionsTable.setModel(this.sorter);
        this.jScrollPane1.setViewportView(this.actionsTable);
        this.actionsView.add((Component)this.jScrollPane1, "actions");
        Mnemonics.setLocalizedText((AbstractButton)this.btnPrintAsHTML, (String)NbBundle.getMessage(KeymapPanel.class, (String)"KeymapPanel.btnPrintAsHTML.text"));
        this.btnPrintAsHTML.setToolTipText(NbBundle.getMessage(KeymapPanel.class, (String)"KeymapPanel.btnPrintAsHTML.toolTipText"));
        this.btnPrintAsHTML.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                KeymapPanel.this.btnPrintAsHTMLActionPerformed(evt);
            }
        });
        this.jSeparator1.setOrientation(1);
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.spShortcuts, -2, 175, -2).addGap(83, 83, 83).addComponent(this.filler1, -2, -1, -2)).addGroup(layout.createSequentialGroup().addComponent(this.lProfile).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cbProfile, 0, -1, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnPrintAsHTML).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jSeparator1, -2, 2, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.manageButton)))).addGroup(layout.createSequentialGroup().addGap(0, 0, 32767).addComponent(this.searchLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.searchField, -2, 120, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.searchSCLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.searchSCField, -2, 125, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.moreButton)).addComponent(this.actionsView, -1, -1, 32767)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.filler2, -2, -1, -2)));
        layout.linkSize(0, this.searchField, this.searchSCField);
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.manageButton, GroupLayout.Alignment.TRAILING).addComponent(this.jSeparator1, -2, 23, -2).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lProfile).addComponent(this.cbProfile, -2, -1, -2).addComponent(this.btnPrintAsHTML))).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGap(18, 18, 18).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.moreButton).addComponent(this.searchSCField, -2, -1, -2).addComponent(this.searchSCLabel).addComponent(this.searchField, -2, -1, -2).addComponent(this.searchLabel)).addGap(10, 10, 10).addComponent(this.actionsView, -1, -1, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)).addGroup(layout.createSequentialGroup().addGap(87, 87, 87).addComponent(this.filler2, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767))).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.spShortcuts, GroupLayout.Alignment.TRAILING, -2, 0, -2).addComponent(this.filler1, GroupLayout.Alignment.TRAILING, -2, -1, -2)).addContainerGap()));
        this.searchField.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(KeymapPanel.class, (String)"KeymapPanel.searchField.AccessibleContext.accessibleDescription"));
    }

    @Override
    public void hidePopup() {
        if (this.searchPopup != null) {
            this.searchPopup.hide();
            this.searchPopup = null;
        }
    }

    private void moreButtonActionPerformed(ActionEvent evt) {
        if (this.searchPopup != null) {
            return;
        }
        JComponent tf = (JComponent)evt.getSource();
        Point p = new Point(tf.getX(), tf.getY());
        SwingUtilities.convertPointToScreen(p, this);
        Rectangle usableScreenBounds = Utilities.getUsableScreenBounds();
        if (p.x + this.specialkeyList.getWidth() > usableScreenBounds.width) {
            p.x = usableScreenBounds.width - this.specialkeyList.getWidth();
        }
        if (p.y + this.specialkeyList.getHeight() > usableScreenBounds.height) {
            p.y = usableScreenBounds.height - this.specialkeyList.getHeight();
        }
        this.searchPopup = PopupFactory.getSharedInstance().getPopup(this, this.specialkeyList, p.x, p.y);
        this.searchPopup.show();
    }

    private void btnPrintAsHTMLActionPerformed(ActionEvent evt) {
        if (this.getMutableModel().getCurrentProfile() != null) {
            ExportShortcutsAction.exportShortcutsOfProfileToHTML(this.getMutableModel().getCurrentProfile());
        }
    }

    @Override
    public Popup getPopup() {
        return this.searchPopup;
    }

    private boolean showPopupMenu(int row, int col, int x, int y) {
        JTable table = this.actionsTable;
        if (col != 1) {
            return false;
        }
        Object valueAt = table.getValueAt(row, col);
        ShortcutCellPanel scCell = (ShortcutCellPanel)table.getCellRenderer(row, col).getTableCellRendererComponent(table, valueAt, true, true, row, col);
        Rectangle cellRect = table.getCellRect(row, col, false);
        JButton button = scCell.getButton();
        if (x < 0 || x > cellRect.x + cellRect.width - button.getWidth()) {
            boolean isShortcutSet = scCell.getTextField().getText().length() != 0;
            final ShortcutPopupPanel panel = (ShortcutPopupPanel)this.popup.getComponents()[0];
            panel.setDisplayAddAlternative(isShortcutSet);
            panel.setRow(row);
            if (x == -1 || y == -1) {
                x = button.getX() + 1;
                y = button.getY() + 1;
            }
            panel.setCustomProfile(this.keymapModel.getMutableModel().isCustomProfile(this.keymapModel.getMutableModel().getCurrentProfile()));
            this.popup.show(table, x, y);
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    panel.requestFocus();
                }
            });
            this.popup.requestFocus();
            return true;
        }
        return false;
    }

    static String loc(String key) {
        return NbBundle.getMessage(KeymapPanel.class, (String)key);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.ignoreActionEvents) {
            return;
        }
        Object source = e.getSource();
        if (source == this.cbProfile) {
            String profile = (String)this.cbProfile.getSelectedItem();
            if (profile != null) {
                this.getMutableModel().setCurrentProfile(profile);
            }
            this.getModel().update();
        } else if (source == this.manageButton) {
            Map<String, Map<ShortcutAction, Set<String>>> modifiedProfiles = this.getMutableModel().getModifiedProfiles();
            Set<String> deletedProfiles = this.getMutableModel().getDeletedProfiles();
            ProfilesPanel profilesPanel = new ProfilesPanel(this);
            DialogDescriptor dd = new DialogDescriptor((Object)profilesPanel, NbBundle.getMessage(KeymapPanel.class, (String)"CTL_Manage_Keymap_Profiles"), true, new Object[]{DialogDescriptor.CLOSED_OPTION}, DialogDescriptor.CLOSED_OPTION, 0, new HelpCtx("org.netbeans.modules.options.keymap.ProfilesPanel"), null);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)dd);
            String selectedProfile = profilesPanel.getSelectedProfile();
            this.getMutableModel().setCurrentProfile(selectedProfile);
            this.refreshProfileCombo();
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if (e.getSource() == this.getMutableModel()) {
            this.firePropertyChange("changed", Boolean.FALSE, Boolean.TRUE);
        }
    }

    class ButtonCellMouseListener
    implements MouseListener {
        private JTable table;

        public ButtonCellMouseListener(JTable table) {
            this.table = table;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            this.forwardEvent(e);
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }

        private void forwardEvent(MouseEvent e) {
            int col;
            Point p = new Point(e.getX(), e.getY());
            int row = this.table.rowAtPoint(p);
            if (KeymapPanel.this.showPopupMenu(row, col = this.table.columnAtPoint(p), e.getX(), e.getY())) {
                e.consume();
            }
        }
    }

    private class KeymapTable
    extends JTable {
        int lastRow;
        int lastColumn;
        private String selectedActionId;

        private KeymapTable() {
        }

        @Override
        public boolean editCellAt(int row, int column) {
            this.lastRow = row;
            this.lastColumn = column;
            boolean editCellAt = super.editCellAt(row, column);
            ((DefaultCellEditor)this.getCellEditor(this.lastRow, this.lastColumn)).getComponent().requestFocus();
            return editCellAt;
        }

        @Override
        protected void processKeyEvent(KeyEvent e) {
            if (!this.isEditing()) {
                super.processKeyEvent(e);
            } else {
                Component component = ((DefaultCellEditor)this.getCellEditor(this.lastRow, this.lastColumn)).getComponent();
                component.requestFocus();
                component.dispatchEvent(new KeyEvent(component, e.getID(), e.getWhen(), e.getModifiers(), e.getKeyCode(), e.getKeyChar()));
            }
        }

        @Override
        public void valueChanged(ListSelectionEvent e) {
            super.valueChanged(e);
            if (!e.getValueIsAdjusting()) {
                int index = this.getSelectedRow();
                this.selectedActionId = this.getActionId(index);
            }
        }

        @Override
        public void sorterChanged(RowSorterEvent e) {
            String aid = this.selectedActionId;
            int colIndex = this.getSelectedColumn();
            super.sorterChanged(e);
            this.restoreSelection(aid, colIndex);
        }

        private void restoreSelection(String id, int colIndex) {
            if (id == null) {
                this.clearSelection();
                return;
            }
            TableModel tm = this.getModel();
            for (int i = 0; i < tm.getRowCount(); ++i) {
                ActionHolder ah = (ActionHolder)tm.getValueAt(i, 0);
                if (ah == null || !id.equals(ah.getAction().getId())) continue;
                this.changeSelection(i, colIndex, false, false);
                break;
            }
        }

        private String getActionId(int modelIndex) {
            ActionHolder h;
            if (modelIndex >= 0 && modelIndex < this.getModel().getRowCount() && (h = (ActionHolder)this.getModel().getValueAt(modelIndex, 0)) != null) {
                ShortcutAction sa = h.getAction();
                return sa.getId();
            }
            return null;
        }

        @Override
        public void tableChanged(TableModelEvent e) {
            String aid = this.selectedActionId;
            int colIndex = this.getSelectedColumn();
            super.tableChanged(e);
            this.restoreSelection(aid, colIndex);
        }
    }

}

