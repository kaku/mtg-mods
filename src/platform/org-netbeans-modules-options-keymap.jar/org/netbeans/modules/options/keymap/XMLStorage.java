/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.modules.options.keymap;

import java.awt.Color;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.openide.ErrorManager;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.util.RequestProcessor;
import org.openide.xml.XMLUtil;
import org.xml.sax.ContentHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class XMLStorage {
    private static final Map<Color, String> colorToName = new HashMap<Color, String>();
    private static final Map<String, Color> nameToColor = new HashMap<String, Color>();
    private static final Map<String, Integer> nameToFontStyle = new HashMap<String, Integer>();
    private static final Map<Integer, String> fontStyleToName = new HashMap<Integer, String>();
    private static RequestProcessor requestProcessor;

    static String colorToString(Color color) {
        if (colorToName.containsKey(color)) {
            return colorToName.get(color);
        }
        return Integer.toHexString(color.getRGB());
    }

    static Color stringToColor(String color) {
        if (nameToColor.containsKey(color)) {
            return nameToColor.get(color);
        }
        return new Color((int)Long.parseLong(color, 16));
    }

    static void save(final FileObject fo, final String content) {
        requestProcessor.post(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                try {
                    FileLock lock = fo.lock();
                    try {
                        OutputStream os = fo.getOutputStream(lock);
                        OutputStreamWriter writer = new OutputStreamWriter(os, "UTF-8");
                        try {
                            writer.write(content);
                        }
                        finally {
                            writer.close();
                        }
                    }
                    finally {
                        lock.releaseLock();
                    }
                }
                catch (IOException ex) {
                    ErrorManager.getDefault().notify((Throwable)ex);
                }
            }
        });
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static Object load(FileObject fo, Handler handler) {
        try {
            XMLReader reader = XMLUtil.createXMLReader();
            reader.setEntityResolver(handler);
            reader.setContentHandler(handler);
            InputStream is = fo.getInputStream();
            try {
                reader.parse(new InputSource(is));
            }
            finally {
                is.close();
            }
            return handler.getResult();
        }
        catch (SAXException ex) {
            System.out.println("File: " + (Object)fo);
            ex.printStackTrace();
            return handler.getResult();
        }
        catch (IOException ex) {
            System.out.println("File: " + (Object)fo);
            ex.printStackTrace();
            return handler.getResult();
        }
        catch (Exception ex) {
            System.out.println("File: " + (Object)fo);
            ex.printStackTrace();
            return handler.getResult();
        }
    }

    static StringBuffer generateHeader() {
        StringBuffer sb = new StringBuffer();
        sb.append("<?xml version=\"1.0\"?>\n\n");
        return sb;
    }

    static void generateFolderStart(StringBuffer sb, String name, Attribs attributes, String indentation) {
        sb.append(indentation).append('<').append(name);
        if (attributes != null) {
            if (!attributes.oneLine) {
                sb.append('\n');
            } else {
                sb.append(' ');
            }
            XMLStorage.generateAttributes(sb, attributes, indentation + "    ");
            if (!attributes.oneLine) {
                sb.append(indentation);
            }
            sb.append(">\n");
        } else {
            sb.append(">\n");
        }
    }

    static void generateFolderEnd(StringBuffer sb, String name, String indentation) {
        sb.append(indentation).append("</").append(name).append(">\n");
    }

    static void generateLeaf(StringBuffer sb, String name, Attribs attributes, String indentation) {
        sb.append(indentation).append('<').append(name);
        if (attributes != null) {
            if (!attributes.oneLine) {
                sb.append('\n');
            } else {
                sb.append(' ');
            }
            XMLStorage.generateAttributes(sb, attributes, indentation + "    ");
            if (!attributes.oneLine) {
                sb.append(indentation);
            }
            sb.append("/>\n");
        } else {
            sb.append("/>\n");
        }
    }

    private static void generateAttributes(StringBuffer sb, Attribs attributes, String indentation) {
        if (attributes == null) {
            return;
        }
        int k = attributes.names.size();
        for (int i = 0; i < k; ++i) {
            if (!attributes.oneLine) {
                sb.append(indentation);
            }
            sb.append((String)attributes.names.get(i)).append("=\"").append((String)attributes.values.get(i)).append('\"');
            if (!attributes.oneLine) {
                sb.append("\n");
                continue;
            }
            if (i >= k - 1) continue;
            sb.append(' ');
        }
    }

    static {
        colorToName.put(Color.black, "black");
        nameToColor.put("black", Color.black);
        colorToName.put(Color.blue, "blue");
        nameToColor.put("blue", Color.blue);
        colorToName.put(Color.cyan, "cyan");
        nameToColor.put("cyan", Color.cyan);
        colorToName.put(Color.darkGray, "darkGray");
        nameToColor.put("darkGray", Color.darkGray);
        colorToName.put(Color.gray, "gray");
        nameToColor.put("gray", Color.gray);
        colorToName.put(Color.green, "green");
        nameToColor.put("green", Color.green);
        colorToName.put(Color.lightGray, "lightGray");
        nameToColor.put("lightGray", Color.lightGray);
        colorToName.put(Color.magenta, "magenta");
        nameToColor.put("magenta", Color.magenta);
        colorToName.put(Color.orange, "orange");
        nameToColor.put("orange", Color.orange);
        colorToName.put(Color.pink, "pink");
        nameToColor.put("pink", Color.pink);
        colorToName.put(Color.red, "red");
        nameToColor.put("red", Color.red);
        colorToName.put(Color.white, "white");
        nameToColor.put("white", Color.white);
        colorToName.put(Color.yellow, "yellow");
        nameToColor.put("yellow", Color.yellow);
        nameToFontStyle.put("plain", 0);
        fontStyleToName.put(0, "plain");
        nameToFontStyle.put("bold", 1);
        fontStyleToName.put(1, "bold");
        nameToFontStyle.put("italic", 2);
        fontStyleToName.put(2, "italic");
        nameToFontStyle.put("bold+italic", 3);
        fontStyleToName.put(3, "bold+italic");
        requestProcessor = new RequestProcessor("XMLStorage");
    }

    static class Attribs {
        private List<String> names = new ArrayList<String>();
        private List<String> values = new ArrayList<String>();
        private boolean oneLine;

        Attribs(boolean oneLine) {
            this.oneLine = oneLine;
        }

        void add(String name, String value) {
            int i = this.names.indexOf(name);
            if (i >= 0) {
                this.names.remove(i);
                this.values.remove(i);
            }
            this.names.add(name);
            this.values.add(value);
        }

        void clear() {
            this.names.clear();
            this.values.clear();
        }
    }

    static class Handler
    extends DefaultHandler {
        private Object result;

        Handler() {
        }

        void setResult(Object result) {
            this.result = result;
        }

        Object getResult() {
            return this.result;
        }
    }

}

