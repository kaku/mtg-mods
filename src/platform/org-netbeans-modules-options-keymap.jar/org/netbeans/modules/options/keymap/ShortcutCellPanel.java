/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.options.keymap;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.net.URL;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.UIResource;
import org.netbeans.modules.options.keymap.Popupable;
import org.netbeans.modules.options.keymap.ShortcutTextField;
import org.netbeans.modules.options.keymap.SpecialkeyPanel;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class ShortcutCellPanel
extends JPanel
implements Comparable,
Popupable {
    private Popup popup;
    private final SpecialkeyPanel specialkeyList;
    PopupFactory factory = PopupFactory.getSharedInstance();
    private JButton changeButton;
    private JTextField scField;

    public ShortcutCellPanel() {
        this.initComponents();
        this.specialkeyList = new SpecialkeyPanel(this, this.scField);
        this.changeButton.addFocusListener(new FocusAdapter(){

            @Override
            public void focusLost(FocusEvent e) {
                ShortcutCellPanel.this.hidePopup();
            }
        });
        this.changeButton.addKeyListener(new KeyAdapter(){

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 27) {
                    JTable table = (JTable)ShortcutCellPanel.this.scField.getParent().getParent();
                    int editingRow = table.getEditingRow();
                    table.editCellAt(editingRow, 1);
                    table.setRowSelectionInterval(editingRow, editingRow);
                    ShortcutCellPanel.this.scField.requestFocus();
                    return;
                }
            }
        });
        this.scField.addFocusListener(new FocusAdapter(){

            @Override
            public void focusGained(FocusEvent e) {
                ShortcutCellPanel.this.changeButton.setText("");
                ShortcutCellPanel.this.changeButton.setIcon(new ImageIcon(this.getClass().getResource("/org/netbeans/modules/options/keymap/more_opened.png")));
            }

            @Override
            public void focusLost(FocusEvent e) {
                ShortcutCellPanel.this.changeButton.setIcon(null);
                ShortcutCellPanel.this.changeButton.setText("...");
            }
        });
        this.setFocusable(true);
    }

    ShortcutCellPanel(String displayedShortcut) {
        this();
        this.setText(displayedShortcut);
    }

    public void setText(String shortcut) {
        this.scField.setText(shortcut);
    }

    @Override
    public void hidePopup() {
        if (this.popup != null) {
            this.popup.hide();
            this.popup = null;
        }
    }

    @Override
    public String toString() {
        return this.scField.getText();
    }

    @Override
    public void setSize(Dimension d) {
        super.setSize(d);
        int buttonWidth = this.changeButton.getPreferredSize().width;
        this.scField.setPreferredSize(new Dimension(d.width - buttonWidth, d.height));
        this.changeButton.setPreferredSize(new Dimension(buttonWidth, d.height));
    }

    @Override
    protected void paintComponent(Graphics g) {
        Color c = this.getBackground();
        if (c instanceof UIResource) {
            super.setBackground(new Color(c.getRGB()));
        }
        super.paintComponent(g);
        super.setBackground(c);
    }

    @Override
    public void setBackground(Color bg) {
        super.setBackground(bg);
        if (this.scField != null) {
            this.scField.setBackground(bg);
        }
    }

    void setBgColor(Color col) {
        this.setBackground(col);
        Color c = UIManager.getDefaults().getColor("control");
        if (c == null) {
            c = new Color(204, 204, 204);
        }
        this.changeButton.setBackground(c);
    }

    void setFgCOlor(Color col, boolean selected) {
        Color def = selected ? UIManager.getDefaults().getColor("List[Selected].textForeground") : UIManager.getDefaults().getColor("Label.foreground");
        if (def != null) {
            col = !(def instanceof UIResource) ? new ColorUIResource(def) : def;
        }
        this.scField.setForeground(col);
    }

    public JButton getButton() {
        return this.changeButton;
    }

    public JTextField getTextField() {
        return this.scField;
    }

    private void initComponents() {
        this.scField = new ShortcutTextField();
        this.changeButton = new JButton();
        this.setBackground(new Color(204, 204, 204));
        this.setPreferredSize(new Dimension(134, 15));
        this.scField.setText(NbBundle.getMessage(ShortcutCellPanel.class, (String)"ShortcutCellPanel.scField.text"));
        this.scField.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.changeButton.setBackground(new Color(204, 204, 204));
        Mnemonics.setLocalizedText((AbstractButton)this.changeButton, (String)NbBundle.getMessage(ShortcutCellPanel.class, (String)"ShortcutCellPanel.changeButton.text"));
        this.changeButton.setHorizontalTextPosition(0);
        this.changeButton.setMaximumSize(new Dimension(25, 15));
        this.changeButton.setMinimumSize(new Dimension(25, 15));
        this.changeButton.setPreferredSize(new Dimension(25, 15));
        this.changeButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                ShortcutCellPanel.this.changeButtonActionPerformed(evt);
            }
        });
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addGap(0, 0, 0).addComponent(this.scField, -1, 109, 32767).addGap(0, 0, 0).addComponent(this.changeButton, -2, 25, -2)));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.scField, -2, 15, -2).addComponent(this.changeButton, -2, 15, -2));
    }

    private void changeButtonActionPerformed(ActionEvent evt) {
        JButton tf = this.changeButton;
        Point p = new Point(tf.getX(), tf.getY());
        SwingUtilities.convertPointToScreen(p, this);
        if (this.popup == null) {
            this.changeButton.setText("");
            this.changeButton.setIcon(new ImageIcon(this.getClass().getResource("/org/netbeans/modules/options/keymap/more_closed.png")));
            this.popup = Utilities.isUnix() ? PopupFactory.getSharedInstance().getPopup(null, this.specialkeyList, p.x, p.y + tf.getHeight()) : this.factory.getPopup(this, this.specialkeyList, p.x, p.y + tf.getHeight());
            this.popup.show();
        } else {
            this.changeButton.setText("");
            this.changeButton.setIcon(new ImageIcon(this.getClass().getResource("/org/netbeans/modules/options/keymap/more_opened.png")));
            this.hidePopup();
        }
    }

    public int compareTo(Object o) {
        return this.toString().compareTo(o.toString());
    }

    @Override
    public Popup getPopup() {
        return this.popup;
    }

    void setButtontext(String text) {
        this.changeButton.setText(text);
    }

}

