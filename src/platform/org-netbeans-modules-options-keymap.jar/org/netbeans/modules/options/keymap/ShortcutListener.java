/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.options.keymap;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import org.netbeans.core.options.keymap.api.KeyStrokeUtils;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;

public class ShortcutListener
implements KeyListener {
    private JTextField textField;
    private boolean enterConfirms;
    private KeyStroke backspaceKS = KeyStroke.getKeyStroke(8, 0);
    private KeyStroke enterKS = KeyStroke.getKeyStroke(10, 0);
    private String key = "";
    private static final Method keyEvent_getExtendedKeyCode;

    public ShortcutListener(boolean enterConfirms) {
        this.enterConfirms = enterConfirms;
    }

    public void clear() {
        this.key = "";
    }

    @Override
    public void keyTyped(KeyEvent e) {
        e.consume();
    }

    static KeyStroke createKeyStroke(KeyEvent e) {
        int code = e.getKeyCode();
        if (keyEvent_getExtendedKeyCode != null) {
            try {
                int ecode = (Integer)keyEvent_getExtendedKeyCode.invoke(e, new Object[0]);
                if (ecode != 0) {
                    code = ecode;
                }
            }
            catch (IllegalAccessException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            catch (IllegalArgumentException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            catch (InvocationTargetException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return KeyStroke.getKeyStroke(code, e.getModifiers());
    }

    @Override
    public void keyPressed(KeyEvent e) {
        boolean add;
        assert (e.getSource() instanceof JTextField);
        if (e.getKeyCode() == 40 || e.getKeyCode() == 38 || e.getKeyCode() == 27) {
            return;
        }
        this.textField = (JTextField)e.getSource();
        KeyStroke keyStroke = ShortcutListener.createKeyStroke(e);
        boolean bl = add = e.getKeyCode() != 16 && e.getKeyCode() != 17 && e.getKeyCode() != 18 && e.getKeyCode() != 157 && e.getKeyCode() != 65406;
        if (!this.enterConfirms || !keyStroke.equals(this.enterKS)) {
            if (keyStroke.equals(this.backspaceKS) && !this.key.equals("")) {
                int i = this.key.lastIndexOf(32);
                this.key = i < 0 ? "" : this.key.substring(0, i);
                this.textField.setText(this.key);
            } else {
                this.addKeyStroke(keyStroke, add);
            }
            e.consume();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        e.consume();
    }

    private void addKeyStroke(KeyStroke keyStroke, boolean add) {
        String s = Utilities.keyToString((KeyStroke)keyStroke, (boolean)true);
        KeyStroke mappedStroke = Utilities.stringToKey((String)s);
        if (!keyStroke.equals(mappedStroke)) {
            return;
        }
        String k = KeyStrokeUtils.getKeyStrokeAsText(keyStroke);
        if (this.key.equals("")) {
            this.textField.setText(k);
            if (add) {
                this.key = k;
            }
        } else {
            this.textField.setText(this.key + " " + k);
            if (add) {
                this.key = this.key + " " + k;
            }
        }
    }

    static {
        Class<KeyEvent> eventClass = KeyEvent.class;
        Method m = null;
        try {
            m = eventClass.getMethod("getExtendedKeyCode", new Class[0]);
        }
        catch (NoSuchMethodException ex) {
        }
        catch (SecurityException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        keyEvent_getExtendedKeyCode = m;
    }
}

