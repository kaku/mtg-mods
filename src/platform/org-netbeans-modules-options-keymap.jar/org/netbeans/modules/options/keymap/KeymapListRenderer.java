/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 */
package org.netbeans.modules.options.keymap;

import java.awt.Component;
import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import org.netbeans.core.options.keymap.api.ShortcutAction;
import org.netbeans.modules.options.keymap.KeymapViewModel;
import org.netbeans.modules.options.keymap.MutableShortcutsModel;
import org.openide.ErrorManager;

public class KeymapListRenderer
extends DefaultTreeCellRenderer {
    private KeymapViewModel keymapViewModel;
    private static ErrorManager log = ErrorManager.getDefault().getInstance(KeymapListRenderer.class.getName());

    public KeymapListRenderer(KeymapViewModel keymapViewModel) {
        if (keymapViewModel == null) {
            throw new NullPointerException();
        }
        this.keymapViewModel = keymapViewModel;
        this.setLeafIcon(null);
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        if (leaf) {
            String[] shortcuts;
            String displayName = ((ShortcutAction)value).getDisplayName();
            StringBuffer text = new StringBuffer(displayName);
            if (log.isLoggable(1)) {
                text.append(" <");
                text.append(((ShortcutAction)value).getId());
                text.append("> ");
            }
            if ((shortcuts = this.keymapViewModel.getMutableModel().getShortcuts((ShortcutAction)value)).length == 1) {
                text.append("  [").append(shortcuts[0]).append("]");
            } else if (shortcuts.length > 1) {
                int k = shortcuts.length;
                text.append("  [").append(shortcuts[0]);
                for (int i = 1; i < k; ++i) {
                    text.append(",").append(shortcuts[i]);
                }
                text.append("]");
            }
            this.setText(text.toString());
        }
        return this;
    }
}

