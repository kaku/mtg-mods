/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 */
package org.netbeans.modules.options.keymap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.netbeans.core.options.keymap.api.ShortcutAction;
import org.netbeans.core.options.keymap.spi.KeymapManager;
import org.netbeans.modules.options.keymap.CompoundAction;
import org.netbeans.modules.options.keymap.LayersBridge;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

public class KeymapModel {
    static final RequestProcessor RP = new RequestProcessor(KeymapModel.class);
    private static final Logger LOG = Logger.getLogger(KeymapModel.class.getName());
    private static final Logger UI_LOG = Logger.getLogger("org.netbeans.ui.options");
    private static volatile List<KeymapManager> managers = null;
    private static volatile KeymapModel INSTANCE;
    private volatile AL actionData;
    private volatile PL profileData;
    private volatile KL keymapData;
    static final Object LOCK;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static KeymapModel create() {
        if (INSTANCE != null) {
            return INSTANCE;
        }
        Class<KeymapModel> class_ = KeymapModel.class;
        synchronized (KeymapModel.class) {
            INSTANCE = new KeymapModel();
            // ** MonitorExit[var0] (shouldn't be in output)
            return INSTANCE;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static Collection<? extends KeymapManager> getKeymapManagerInstances() {
        if (managers != null) {
            return managers;
        }
        final Lookup.Result r = Lookup.getDefault().lookupResult(KeymapManager.class);
        ArrayList<KeymapManager> al = new ArrayList<KeymapManager>(r.allInstances());
        al.trimToSize();
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Dumping registered KeymapManagers: ");
            for (KeymapManager m : al) {
                LOG.fine("    KeymapManager: " + KeymapModel.s2s(m));
            }
            LOG.fine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        }
        Class<KeymapModel> i$ = KeymapModel.class;
        synchronized (KeymapModel.class) {
            if (managers == null) {
                managers = al;
                r.addLookupListener(new LookupListener(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    public void resultChanged(LookupEvent ev) {
                        Class<KeymapModel> class_ = KeymapModel.class;
                        synchronized (KeymapModel.class) {
                            managers = null;
                            r.removeLookupListener((LookupListener)this);
                            // ** MonitorExit[var2_2] (shouldn't be in output)
                            if (INSTANCE != null) {
                                INSTANCE.refreshActions();
                            }
                            return;
                        }
                    }
                });
            }
            // ** MonitorExit[i$] (shouldn't be in output)
            return al;
        }
    }

    public Set<String> getActionCategories() {
        AL data = this.ensureActionsLoaded();
        return data.categories;
    }

    private PL ensureProfilesLoaded() {
        PL p = this.profileData;
        if (p == null) {
            p = new PL();
            KeymapModel.waitFinished(p);
        }
        return p.current;
    }

    private KL ensureKeymapsLoaded(String forProfile) {
        KL k = this.keymapData;
        if (k == null || k.keyMaps.get(forProfile) == null) {
            k = new KL(this.ensureActionsLoaded(), forProfile);
            KeymapModel.waitFinished(k);
        }
        return k.current;
    }

    private static void findDuplicateIds(String category, Collection<ShortcutAction> actions, Set<String> allActionIds, Set<String> duplicateIds) {
        for (ShortcutAction sa : actions) {
            String id = sa.getId();
            if (!allActionIds.add(id)) {
                duplicateIds.add(id);
                continue;
            }
            id = LayersBridge.getOrigActionClass(sa);
            if (id == null || allActionIds.add(id)) continue;
            duplicateIds.add(id);
        }
    }

    public Set<ShortcutAction> getActions(String category) {
        AL al = this.ensureActionsLoaded();
        Set actions = (Set)al.categoryToActions.get(category);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Category '" + category + "' actions (" + actions.size() + "), KeymapModel=" + this + ":");
            for (ShortcutAction sa : actions) {
                LOG.fine("    id='" + sa.getId() + "', did='" + sa.getDelegatingActionId() + ", " + KeymapModel.s2s(sa));
            }
            LOG.fine("---------------------------");
        }
        return actions;
    }

    boolean isDuplicateId(String id) {
        AL al = this.ensureActionsLoaded();
        if (!al.duplicateIds.contains(id)) {
            return false;
        }
        LOG.log(Level.WARNING, "Duplicate action ID used: {0}", new Object[]{id});
        return true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void refreshActions() {
        KeymapModel keymapModel = this;
        synchronized (keymapModel) {
            this.actionData = null;
            this.profileData = null;
            this.keymapData = null;
        }
        for (KeymapManager m : KeymapModel.getKeymapManagerInstances()) {
            Object object = LOCK;
            synchronized (object) {
                m.refreshActions();
                continue;
            }
        }
    }

    public String getCurrentProfile() {
        return this.ensureProfilesLoaded().currentProfile;
    }

    public void setCurrentProfile(String profileName) {
        String prev = this.getCurrentProfile();
        if (!prev.equals(profileName)) {
            LogRecord rec = new LogRecord(Level.CONFIG, "KEYMAP_SET_PROFILE");
            rec.setParameters(new Object[]{profileName, prev});
            rec.setResourceBundle(NbBundle.getBundle(KeymapModel.class));
            rec.setResourceBundleName(KeymapModel.class.getPackage().getName() + ".Bundle");
            rec.setLoggerName(UI_LOG.getName());
            UI_LOG.log(rec);
        }
        final String profile = this.displayNameToName(profileName);
        KeymapModel.waitFinished(new Runnable(){

            @Override
            public void run() {
                for (KeymapManager m : KeymapModel.getKeymapManagerInstances()) {
                    m.setCurrentProfile(profile);
                }
                KeymapModel.this.profileData = null;
            }
        });
    }

    public void revertActions(final Collection<ShortcutAction> ac) throws IOException {
        final IOException[] exc = new IOException[1];
        KeymapModel.waitFinished(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                try {
                    for (KeymapManager m : KeymapModel.getKeymapManagerInstances()) {
                        if (m instanceof KeymapManager.WithRevert) {
                            try {
                                ((KeymapManager.WithRevert)((Object)m)).revertActions(KeymapModel.this.getCurrentProfile(), ac);
                                continue;
                            }
                            catch (IOException ex) {
                                exc[0] = ex;
                                KeymapModel keymapModel = KeymapModel.this;
                                synchronized (keymapModel) {
                                    KeymapModel.this.keymapData = null;
                                }
                                return;
                            }
                        }
                        Map<ShortcutAction, Set<String>> actions = m.getDefaultKeymap(KeymapModel.this.getCurrentProfile());
                        HashMap<ShortcutAction, Set<String>> keymap = new HashMap<ShortcutAction, Set<String>>(m.getKeymap(KeymapModel.this.getCurrentProfile()));
                        for (ShortcutAction a : ac) {
                            Set<String> defKeys = actions.get(a);
                            if (defKeys == null) {
                                keymap.remove(a);
                                continue;
                            }
                            keymap.put(a, defKeys);
                        }
                        m.saveKeymap(KeymapModel.this.getCurrentProfile(), keymap);
                    }
                }
                finally {
                    KeymapModel i$ = KeymapModel.this;
                    synchronized (i$) {
                        KeymapModel.this.keymapData = null;
                    }
                }
            }
        });
        if (exc[0] != null) {
            throw exc[0];
        }
    }

    public void revertProfile(final String profileName) throws IOException {
        final IOException[] exc = new IOException[1];
        KeymapModel.waitFinished(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                try {
                    for (KeymapManager m : KeymapModel.getKeymapManagerInstances()) {
                        if (m instanceof KeymapManager.WithRevert) {
                            try {
                                ((KeymapManager.WithRevert)((Object)m)).revertProfile(profileName);
                                continue;
                            }
                            catch (IOException ex) {
                                exc[0] = ex;
                                KeymapModel keymapModel = KeymapModel.this;
                                synchronized (keymapModel) {
                                    KeymapModel.this.profileData = null;
                                    KeymapModel.this.keymapData = null;
                                }
                                return;
                            }
                        }
                        m.saveKeymap(profileName, m.getDefaultKeymap(profileName));
                    }
                }
                finally {
                    KeymapModel i$ = KeymapModel.this;
                    synchronized (i$) {
                        KeymapModel.this.profileData = null;
                        KeymapModel.this.keymapData = null;
                    }
                }
            }
        });
        if (exc[0] != null) {
            throw exc[0];
        }
    }

    public List<String> getProfiles() {
        return new ArrayList<String>(this.getProfilesMap().keySet());
    }

    public boolean isCustomProfile(String profile) {
        profile = this.displayNameToName(profile);
        Boolean b = (Boolean)this.ensureProfilesLoaded().customProfiles.get(profile);
        return b == null || b != false;
    }

    public Map<ShortcutAction, Set<String>> getKeymap(String profile) {
        profile = this.displayNameToName(profile);
        return (Map)this.ensureKeymapsLoaded(profile).keyMaps.get(profile);
    }

    public Map<ShortcutAction, Set<String>> getKeymapDefaults(String profile) {
        profile = this.displayNameToName(profile);
        return (Map)this.ensureKeymapsLoaded(profile).keyMapDefaults.get(profile);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void deleteProfile(String profile) {
        profile = this.displayNameToName(profile);
        for (KeymapManager m : KeymapModel.getKeymapManagerInstances()) {
            Object object = LOCK;
            synchronized (object) {
                m.deleteProfile(profile);
                continue;
            }
        }
    }

    public void changeKeymap(String profileName, Map<ShortcutAction, Set<String>> actionToShortcuts) {
        final String profile = this.displayNameToName(profileName);
        this.log("changeKeymap.actionToShortcuts", actionToShortcuts.entrySet());
        final HashMap<ShortcutAction, Set<String>> m = new HashMap<ShortcutAction, Set<String>>(this.getKeymap(profile));
        m.putAll(actionToShortcuts);
        KeymapModel.waitFinished(new Runnable(){

            @Override
            public void run() {
                KL k = KeymapModel.this.keymapData;
                if (k != null) {
                    HashMap<String, Map> newMap = new HashMap<String, Map>();
                    newMap.putAll(k.keyMaps);
                    newMap.put(profile, m);
                    k.keyMaps = newMap;
                }
                KeymapModel.this.log("changeKeymap.m", m.entrySet());
                for (KeymapManager km : KeymapModel.getKeymapManagerInstances()) {
                    km.saveKeymap(profile, m);
                }
            }
        });
    }

    private void log(String name, Collection items) {
        if (!LOG.isLoggable(Level.FINE)) {
            return;
        }
        LOG.fine(name);
        for (Object item : items) {
            LOG.fine("  " + item);
        }
    }

    static Set<ShortcutAction> mergeActions(Collection<ShortcutAction> res, Collection<ShortcutAction> adding, String name, Map<ShortcutAction, CompoundAction> sharedActions) {
        String id;
        HashSet<ShortcutAction> result = new HashSet<ShortcutAction>();
        HashMap<String, ShortcutAction> idToAction = new HashMap<String, ShortcutAction>();
        HashMap<String, ShortcutAction> delegateIdToAction = new HashMap<String, ShortcutAction>();
        for (ShortcutAction action2 : res) {
            id = action2.getId();
            idToAction.put(id, action2);
            String delegate = action2.getDelegatingActionId();
            if (delegate == null) continue;
            delegateIdToAction.put(delegate, action2);
        }
        for (ShortcutAction action2 : adding) {
            id = action2.getId();
            if (delegateIdToAction.containsKey(id)) {
                ShortcutAction origAction = (ShortcutAction)delegateIdToAction.remove(id);
                idToAction.remove(origAction.getId());
                KeymapManager origActionKeymapManager = KeymapModel.findOriginator(origAction);
                HashMap<String, ShortcutAction> ss = new HashMap<String, ShortcutAction>();
                ss.put(origActionKeymapManager.getName(), origAction);
                ss.put(name, action2);
                CompoundAction compoundAction = new CompoundAction(ss);
                result.add(compoundAction);
                sharedActions.put(origAction, compoundAction);
                sharedActions.put(action2, compoundAction);
                continue;
            }
            String delegatingId = action2.getDelegatingActionId();
            if (idToAction.containsKey(delegatingId)) {
                ShortcutAction origAction = (ShortcutAction)idToAction.remove(delegatingId);
                KeymapManager origActionKeymapManager = KeymapModel.findOriginator(origAction);
                HashMap<String, ShortcutAction> ss = new HashMap<String, ShortcutAction>();
                ss.put(origActionKeymapManager.getName(), origAction);
                ss.put(name, action2);
                CompoundAction compoundAction = new CompoundAction(ss);
                result.add(compoundAction);
                sharedActions.put(origAction, compoundAction);
                sharedActions.put(action2, compoundAction);
                continue;
            }
            ShortcutAction old = (ShortcutAction)idToAction.get(id);
            if (old != null) {
                if (old instanceof CompoundAction) {
                    ((CompoundAction)old).addAction(name, action2);
                    sharedActions.put(action2, (CompoundAction)old);
                    continue;
                }
                idToAction.remove(id);
                ShortcutAction origAction = old;
                KeymapManager origActionKeymapManager = KeymapModel.findOriginator(origAction);
                HashMap<String, ShortcutAction> ss = new HashMap<String, ShortcutAction>();
                ss.put(origActionKeymapManager.getName(), origAction);
                ss.put(name, action2);
                CompoundAction compoundAction = new CompoundAction(ss);
                result.remove(origAction);
                result.add(compoundAction);
                sharedActions.put(origAction, compoundAction);
                sharedActions.put(action2, compoundAction);
                continue;
            }
            if (sharedActions.containsKey(action2)) continue;
            result.add(action2);
        }
        result.addAll(idToAction.values());
        return result;
    }

    static Collection<ShortcutAction> filterSameScope(Set<ShortcutAction> actions, ShortcutAction anchor) {
        KeymapManager mgr = KeymapModel.findOriginator(anchor);
        if (mgr == null) {
            return Collections.EMPTY_SET;
        }
        List sameActions = null;
        for (ShortcutAction sa : actions) {
            KeymapManager m2 = KeymapModel.findOriginator(sa);
            if (mgr != m2) continue;
            if (sameActions == null) {
                sameActions = new LinkedList<ShortcutAction>();
            }
            sameActions.add(sa);
        }
        return sameActions == null ? Collections.EMPTY_LIST : sameActions;
    }

    private static KeymapManager findOriginator(ShortcutAction a) {
        for (KeymapManager km : KeymapModel.getKeymapManagerInstances()) {
            if (a.getKeymapManagerInstance(km.getName()) == null) continue;
            return km;
        }
        return null;
    }

    private static Map<ShortcutAction, Set<String>> mergeShortcuts(Map<ShortcutAction, Set<String>> res, Map<ShortcutAction, Set<String>> adding, Map<ShortcutAction, CompoundAction> sharedActions) {
        Iterator<ShortcutAction> i$ = adding.keySet().iterator();
        while (i$.hasNext()) {
            ShortcutAction action = i$.next();
            Set<String> shortcuts = adding.get(action);
            if (shortcuts.isEmpty()) continue;
            if (sharedActions.containsKey(action)) {
                action = sharedActions.get(action);
            }
            res.put(action, shortcuts);
            if (!LOG.isLoggable(Level.FINE)) continue;
            LOG.fine("Action='" + action.getId() + "' (" + KeymapModel.s2s(action) + ") shortcuts: " + shortcuts);
        }
        return res;
    }

    private AL ensureActionsLoaded() {
        AL al = this.actionData;
        if (al != null) {
            return al;
        }
        al = new AL();
        KeymapModel.waitFinished(al);
        return al.current;
    }

    private String displayNameToName(String keymapDisplayName) {
        String name = this.getProfilesMap().get(keymapDisplayName);
        return name == null ? keymapDisplayName : name;
    }

    String getProfileName(String id) {
        Map<String, String> m = this.getProfilesMap();
        for (Map.Entry<String, String> e : m.entrySet()) {
            if (!e.getValue().equals(id)) continue;
            return e.getKey();
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void waitFinished(Runnable r) {
        Object object = LOCK;
        synchronized (object) {
            r.run();
        }
    }

    private Map<String, String> getProfilesMap() {
        return this.ensureProfilesLoaded().profilesMap;
    }

    private static String s2s(Object o) {
        return o == null ? "null" : o.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(o));
    }

    static {
        LOCK = new String("Keymap lock");
    }

    private class KL
    implements Runnable {
        private AL actionData;
        private String profile;
        private volatile KL current;
        private volatile Map<String, Map<ShortcutAction, Set<String>>> keyMaps;
        private Map<String, Map<ShortcutAction, Set<String>>> keyMapDefaults;

        public KL(AL actionData, String profile) {
            this.keyMaps = new HashMap<String, Map<ShortcutAction, Set<String>>>();
            this.keyMapDefaults = new HashMap<String, Map<ShortcutAction, Set<String>>>();
            this.actionData = actionData;
            this.profile = profile;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            this.current = KeymapModel.this.keymapData;
            if (this.current != null && this.current.keyMaps.get(this.profile) != null) {
                return;
            }
            Map res = new HashMap();
            Map defRes = new HashMap();
            for (KeymapManager m : KeymapModel.getKeymapManagerInstances()) {
                Map<ShortcutAction, Set<String>> mm = m.getKeymap(this.profile);
                res = KeymapModel.mergeShortcuts(res, mm, this.actionData.sharedActions);
                mm = m.getDefaultKeymap(this.profile);
                defRes = KeymapModel.mergeShortcuts(defRes, mm, this.actionData.sharedActions);
            }
            KL i$ = this;
            synchronized (i$) {
                if (KeymapModel.this.keymapData != null && KeymapModel.access$600((KeymapModel)KeymapModel.this).keyMaps.get(this.profile) != null) {
                    this.current = KeymapModel.this.keymapData;
                } else {
                    if (KeymapModel.this.keymapData != null) {
                        this.keyMaps.putAll(KeymapModel.access$600((KeymapModel)KeymapModel.this).keyMaps);
                        this.keyMapDefaults.putAll(KeymapModel.access$600((KeymapModel)KeymapModel.this).keyMapDefaults);
                    }
                    this.keyMaps.put(this.profile, res);
                    this.keyMapDefaults.put(this.profile, defRes);
                    this.current = this;
                    KeymapModel.this.keymapData = this.current;
                }
            }
        }
    }

    private class PL
    implements Runnable {
        private volatile PL current;
        private Map<String, String> profilesMap;
        private String currentProfile;
        private Map<String, Boolean> customProfiles;

        private PL() {
            this.profilesMap = new HashMap<String, String>();
            this.customProfiles = new HashMap<String, Boolean>();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            this.current = KeymapModel.this.profileData;
            if (this.current != null) {
                return;
            }
            for (KeymapManager m : KeymapModel.getKeymapManagerInstances()) {
                List<String> l = m.getProfiles();
                if (this.currentProfile == null) {
                    this.currentProfile = m.getCurrentProfile();
                }
                if (l == null || !this.profilesMap.isEmpty()) continue;
                for (String name : l) {
                    this.profilesMap.put(m.getProfileDisplayName(name), name);
                    this.customProfiles.put(name, Boolean.TRUE.equals(this.customProfiles.get(name)) || m.isCustomProfile(name));
                }
            }
            if (this.currentProfile == null) {
                this.currentProfile = "NetBeans";
            }
            KeymapModel i$ = KeymapModel.this;
            synchronized (i$) {
                this.current = KeymapModel.this.profileData == null ? (KeymapModel.this.profileData = this) : KeymapModel.this.profileData;
            }
        }
    }

    private class AL
    implements Runnable {
        private volatile AL current;
        private Set<String> categories;
        private Set<String> allActionIds;
        private Set<String> duplicateIds;
        private Map<String, Set<ShortcutAction>> categoryToActions;
        private Map<ShortcutAction, CompoundAction> sharedActions;

        private AL() {
            this.allActionIds = new HashSet<String>();
            this.duplicateIds = new HashSet<String>();
            this.categoryToActions = new HashMap<String, Set<ShortcutAction>>();
            this.sharedActions = new HashMap<ShortcutAction, CompoundAction>();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            this.current = KeymapModel.this.actionData;
            if (this.current != null) {
                return;
            }
            ArrayList<Map<String, Set<ShortcutAction>>> mgrActions = new ArrayList<Map<String, Set<ShortcutAction>>>();
            HashSet<String> categoryIds = new HashSet<String>();
            HashMap<String, Set<ShortcutAction>> cats = new HashMap<String, Set<ShortcutAction>>();
            Collection<? extends KeymapManager> mgrInstances = KeymapModel.getKeymapManagerInstances();
            for (KeymapManager m : mgrInstances) {
                Map<String, Set<ShortcutAction>> a = m.getActions();
                mgrActions.add(a);
                categoryIds.addAll(a.keySet());
            }
            HashSet<String> allIds = new HashSet<String>();
            HashSet<String> duplIds = new HashSet<String>();
            categoryIds.add("");
            for (String category : categoryIds) {
                Iterator<? extends KeymapManager> mgrIt = mgrInstances.iterator();
                Set actions = new HashSet<ShortcutAction>();
                for (Map aa : mgrActions) {
                    Set s = (Set)aa.get(category);
                    KeymapManager mgr = mgrIt.next();
                    if (s == null) continue;
                    actions = KeymapModel.mergeActions(actions, s, mgr.getName(), this.sharedActions);
                }
                KeymapModel.findDuplicateIds(category, actions, allIds, duplIds);
                cats.put(category, actions);
            }
            this.allActionIds = allIds;
            this.duplicateIds = duplIds;
            this.categoryToActions = cats;
            this.categories = categoryIds;
            KeymapModel i$ = KeymapModel.this;
            synchronized (i$) {
                this.current = KeymapModel.this.actionData != null ? KeymapModel.this.actionData : (KeymapModel.this.actionData = this);
            }
        }
    }

}

