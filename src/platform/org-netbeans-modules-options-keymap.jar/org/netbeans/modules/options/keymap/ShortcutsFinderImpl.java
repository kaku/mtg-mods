/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.options.keymap;

import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import org.netbeans.core.options.keymap.api.KeyStrokeUtils;
import org.netbeans.core.options.keymap.api.ShortcutAction;
import org.netbeans.core.options.keymap.api.ShortcutsFinder;
import org.netbeans.modules.options.keymap.KeymapModel;
import org.netbeans.modules.options.keymap.KeymapPanel;
import org.netbeans.modules.options.keymap.LayersBridge;
import org.netbeans.modules.options.keymap.MutableShortcutsModel;
import org.netbeans.modules.options.keymap.ShortcutsDialog;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class ShortcutsFinderImpl
implements ShortcutsFinder {
    protected KeymapModel model;
    private volatile Map<String, Map<ShortcutAction, Set<String>>> shortcutsCache = Collections.emptyMap();

    public ShortcutsFinderImpl() {
        this(KeymapModel.create());
    }

    public ShortcutsFinderImpl(KeymapModel model) {
        this.model = model;
    }

    @Override
    public ShortcutAction findActionForShortcut(String sc) {
        for (String c : this.model.getActionCategories()) {
            for (ShortcutAction action : this.model.getActions(c)) {
                String[] shortcuts = this.getShortcuts(action);
                int k = shortcuts.length;
                for (int i = 0; i < k; ++i) {
                    if (!shortcuts[i].equals(sc) || this.isImpliedAction(action)) continue;
                    return action;
                }
            }
        }
        return null;
    }

    protected boolean isImpliedAction(ShortcutAction action) {
        return action != null && "run-macro".equals(action.getId());
    }

    @Override
    public ShortcutAction findActionForId(String actionId) {
        if (this.model.isDuplicateId(actionId)) {
            return null;
        }
        ShortcutAction ac = this.findActionForId(actionId, "", false);
        if (ac == null) {
            ac = this.findActionForId(actionId, "", true);
        }
        return ac;
    }

    protected ShortcutAction findActionForId(String actionId, String category, boolean delegate) {
        if (!category.isEmpty()) {
            throw new IllegalArgumentException();
        }
        for (String c : this.model.getActionCategories()) {
            for (ShortcutAction action : this.model.getActions(c)) {
                String id = delegate ? LayersBridge.getOrigActionClass(action) : action.getId();
                if (id == null || !actionId.equals(id)) continue;
                return action;
            }
        }
        return null;
    }

    @Override
    public String showShortcutsDialog() {
        final ShortcutsDialog d = new ShortcutsDialog();
        d.init(this);
        final DialogDescriptor descriptor = new DialogDescriptor((Object)d, ShortcutsFinderImpl.loc("Add_Shortcut_Dialog"), true, new Object[]{DialogDescriptor.OK_OPTION, DialogDescriptor.CANCEL_OPTION}, DialogDescriptor.OK_OPTION, 0, null, (ActionListener)d.getListener());
        descriptor.setClosingOptions(new Object[]{DialogDescriptor.OK_OPTION, DialogDescriptor.CANCEL_OPTION});
        descriptor.setAdditionalOptions(new Object[]{d.getBClear(), d.getBTab()});
        descriptor.setValid(d.isShortcutValid());
        d.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName() == null || "ShortcutsDialog.PROP_SHORTCUT_VALID".equals(evt.getPropertyName())) {
                    descriptor.setValid(d.isShortcutValid());
                }
            }
        });
        DialogDisplayer.getDefault().notify((NotifyDescriptor)descriptor);
        if (descriptor.getValue() == DialogDescriptor.OK_OPTION) {
            return d.getTfShortcut().getText();
        }
        return null;
    }

    protected String getCurrentProfile() {
        return this.model.getCurrentProfile();
    }

    @Override
    public String[] getShortcuts(ShortcutAction action) {
        String currentProfile = this.getCurrentProfile();
        Map<ShortcutAction, Set<String>> profileMap = this.getProfileMap(currentProfile);
        Set<String> shortcuts = profileMap.get(action);
        if (shortcuts == null) {
            return new String[0];
        }
        return shortcuts.toArray(new String[shortcuts.size()]);
    }

    protected void clearShortcuts(String profile) {
        this.shortcutsCache.remove(profile);
    }

    protected Map<ShortcutAction, Set<String>> getKeymap(String profile) {
        return this.model.getKeymap(profile);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected Map<ShortcutAction, Set<String>> getProfileMap(String profile) {
        Map<ShortcutAction, Set<String>> res = this.shortcutsCache.get(profile);
        if (res == null) {
            Map<ShortcutAction, Set<String>> profileMap = ShortcutsFinderImpl.convertFromEmacs(this.getKeymap(profile));
            ShortcutsFinderImpl shortcutsFinderImpl = this;
            synchronized (shortcutsFinderImpl) {
                res = this.shortcutsCache.get(profile);
                if (res == null) {
                    HashMap<String, Map<ShortcutAction, Set<String>>> m = new HashMap<String, Map<ShortcutAction, Set<String>>>(this.shortcutsCache);
                    m.put(profile, profileMap);
                    this.shortcutsCache = m;
                    res = profileMap;
                }
            }
        }
        return res;
    }

    @Override
    public void refreshActions() {
        this.clearCache();
        this.model.refreshActions();
    }

    protected void clearCache() {
        this.shortcutsCache = Collections.emptyMap();
    }

    @Override
    public void setShortcuts(ShortcutAction action, Set<String> shortcuts) {
        throw new UnsupportedOperationException("Finder must be cloned first");
    }

    @Override
    public void apply() {
        throw new UnsupportedOperationException("Finder must be cloned first");
    }

    protected static String loc(String key) {
        return NbBundle.getMessage(KeymapPanel.class, (String)key);
    }

    protected static Map<ShortcutAction, Set<String>> convertFromEmacs(Map<ShortcutAction, Set<String>> emacs) {
        HashMap<ShortcutAction, Set<String>> result = new HashMap<ShortcutAction, Set<String>>();
        for (Map.Entry<ShortcutAction, Set<String>> entry : emacs.entrySet()) {
            ShortcutAction action = entry.getKey();
            LinkedHashSet<String> shortcuts = new LinkedHashSet<String>();
            for (String emacsShortcut : entry.getValue()) {
                KeyStroke[] keyStroke = Utilities.stringToKeys((String)emacsShortcut);
                shortcuts.add(KeyStrokeUtils.getKeyStrokesAsText(keyStroke, " "));
            }
            result.put(action, shortcuts);
        }
        return result;
    }

    @Override
    public ShortcutsFinder.Writer localCopy() {
        MutableShortcutsModel local = new MutableShortcutsModel(this.model, this);
        this.model.getActionCategories();
        this.model.getKeymap(this.model.getCurrentProfile());
        return local;
    }

}

