/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.cookies.InstanceCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.filesystems.FileSystem$Status
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataShadow
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.keymap;

import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import org.netbeans.core.options.keymap.api.ShortcutAction;
import org.netbeans.core.options.keymap.spi.KeymapManager;
import org.openide.ErrorManager;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataShadow;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class LayersBridge
extends KeymapManager
implements KeymapManager.WithRevert {
    private static final String EXT_REMOVED = "removed";
    private static final Logger LOG = Logger.getLogger(LayersBridge.class.getName());
    static final String KEYMAPS_FOLDER = "Keymaps";
    private static final String SHORTCUTS_FOLDER = "Shortcuts";
    private static final String LAYERS_BRIDGE = "LayersBridge";
    private Map<GlobalAction, DataObject> actionToDataObject = new HashMap<GlobalAction, DataObject>();
    private Map<String, Set<ShortcutAction>> categoryToActions;
    private Map<GlobalAction, GlobalAction> actions = new HashMap<GlobalAction, GlobalAction>();
    private volatile List<String> keymapNames;
    private volatile Map<String, String> keymapDisplayNames;
    private Map<String, Map<ShortcutAction, Set<String>>> keymaps = new HashMap<String, Map<ShortcutAction, Set<String>>>();
    private volatile Map<ShortcutAction, Set<String>> baseKeyMap;
    private Map<String, Map<ShortcutAction, Set<String>>> keymapDefaults = new HashMap<String, Map<ShortcutAction, Set<String>>>();
    private static final GlobalAction REMOVED = new GlobalAction(null, null, "<removed>"){};
    private static final String OPENIDE_DELEGATE_ACTION = "org.openide.awt.GeneralAction$DelegateAction";
    private static volatile Field KEY_FIELD;
    private String cachedProfile;

    public LayersBridge() {
        super("LayersBridge");
    }

    @Override
    public synchronized Map<String, Set<ShortcutAction>> getActions() {
        if (this.categoryToActions == null) {
            this.categoryToActions = new HashMap<String, Set<ShortcutAction>>();
            this.initActions("Actions", null);
            this.categoryToActions.remove("Hidden");
            this.categoryToActions = Collections.unmodifiableMap(this.categoryToActions);
        }
        return this.categoryToActions;
    }

    private void initActions(String folder, String category) {
        FileObject fo = FileUtil.getConfigFile((String)folder);
        if (fo == null) {
            return;
        }
        DataFolder root = DataFolder.findFolder((FileObject)fo);
        Enumeration en = root.children();
        while (en.hasMoreElements()) {
            DataObject dataObject = (DataObject)en.nextElement();
            if (!(dataObject instanceof DataFolder)) continue;
            this.initActions((DataFolder)dataObject, null, category);
        }
    }

    private void initActions(DataFolder folder, String folderName, String category) {
        String name = folder.getName();
        if (category != null) {
            name = category;
        } else {
            String bundleName = (String)folder.getPrimaryFile().getAttribute("SystemFileSystem.localizingBundle");
            if (bundleName != null) {
                try {
                    name = NbBundle.getBundle((String)bundleName).getString(folder.getPrimaryFile().getPath());
                }
                catch (MissingResourceException ex) {
                    ErrorManager.getDefault().notify((Throwable)ex);
                }
            }
            if (folderName != null) {
                name = folderName + '/' + name;
            }
        }
        Enumeration en = folder.children();
        while (en.hasMoreElements()) {
            DataObject dataObject = (DataObject)en.nextElement();
            if (dataObject instanceof DataFolder) {
                this.initActions((DataFolder)dataObject, name, category);
                continue;
            }
            GlobalAction action = this.createAction(dataObject, name, dataObject.getPrimaryFile().getName(), false);
            if (action == null || this.actions.containsKey(action)) continue;
            this.actions.put(action, action);
            Set<ShortcutAction> a = this.categoryToActions.get(name);
            if (a == null) {
                a = new HashSet<ShortcutAction>();
                this.categoryToActions.put(name, a);
            }
            a.add(action);
            while (dataObject instanceof DataShadow) {
                dataObject = ((DataShadow)dataObject).getOriginal();
            }
            this.actionToDataObject.put(action, dataObject);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void refreshKeymapNames() {
        Object f;
        DataFolder root = LayersBridge.getRootFolder("Keymaps", null);
        Enumeration en = root.children(false);
        ArrayList<String> names = new ArrayList<String>();
        HashMap<String, String> displayNames = new HashMap<String, String>();
        while (en.hasMoreElements()) {
            String displayName;
            f = ((DataObject)en.nextElement()).getPrimaryFile();
            if (!f.isFolder()) continue;
            String name = f.getNameExt();
            try {
                displayName = f.getFileSystem().getStatus().annotateName(name, Collections.singleton(f));
            }
            catch (FileStateInvalidException fsie) {
                displayName = name;
            }
            names.add(name);
            displayNames.put(name, displayName);
        }
        if (names.isEmpty()) {
            names.add("NetBeans");
        }
        f = this;
        synchronized (f) {
            this.keymapNames = names;
            this.keymapDisplayNames = displayNames;
        }
    }

    @Override
    public List<String> getProfiles() {
        List<String> names = this.keymapNames;
        if (names == null) {
            this.refreshKeymapNames();
            names = this.keymapNames;
        }
        return Collections.unmodifiableList(names);
    }

    @Override
    public String getProfileDisplayName(String profileName) {
        String displayName;
        Map<String, String> m = this.keymapDisplayNames;
        if (m == null) {
            this.refreshKeymapNames();
            m = this.keymapDisplayNames;
        }
        return (displayName = m.get(profileName)) == null ? profileName : displayName;
    }

    @Override
    public Map<ShortcutAction, Set<String>> getKeymap(String profile) {
        if (!this.keymaps.containsKey(profile)) {
            DataFolder root = LayersBridge.getRootFolder("Shortcuts", null);
            Map<ShortcutAction, Set<String>> m = this.readKeymap(root);
            root = LayersBridge.getRootFolder("Keymaps", profile);
            this.overrideWithKeyMap(m, this.readKeymap(root), profile);
            m.remove(REMOVED);
            this.keymaps.put(profile, m);
        }
        return Collections.unmodifiableMap(this.keymaps.get(profile));
    }

    private Map<ShortcutAction, Set<String>> getBaseKeyMap() {
        if (this.baseKeyMap == null) {
            DataFolder root = LayersBridge.getRootFolder("Shortcuts", null);
            Map<ShortcutAction, Set<String>> m = this.readKeymap(root);
            this.baseKeyMap = m;
        }
        return this.baseKeyMap;
    }

    private Map<ShortcutAction, Set<String>> overrideWithKeyMap(Map<ShortcutAction, Set<String>> base, Map<ShortcutAction, Set<String>> keyMap, String profile) {
        HashSet overrideKeyStrokes = new HashSet();
        Map<String, ShortcutAction> shortcuts = null;
        for (ShortcutAction a : keyMap.keySet()) {
            overrideKeyStrokes.addAll(keyMap.get(a));
        }
        Iterator<Map.Entry<ShortcutAction, Set<String>>> it = base.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<ShortcutAction, Set<String>> en = it.next();
            Set<String> keys = en.getValue();
            if (LOG.isLoggable(Level.FINER)) {
                for (String s : keys) {
                    ShortcutAction sa;
                    if (!overrideKeyStrokes.contains(s)) continue;
                    if (shortcuts == null) {
                        shortcuts = LayersBridge.shortcutToAction(keyMap);
                    }
                    if ((sa = shortcuts.get(s)).getId().equals(en.getKey().getId())) continue;
                    LOG.finer("[" + profile + "] change keybinding " + s + " from " + en.getKey().getId() + " to " + sa.getId());
                }
            }
            keys.removeAll(overrideKeyStrokes);
            if (!keys.isEmpty()) continue;
            it.remove();
        }
        base.putAll(keyMap);
        return base;
    }

    @Override
    public synchronized Map<ShortcutAction, Set<String>> getDefaultKeymap(String profile) {
        if (!this.keymapDefaults.containsKey(profile)) {
            DataFolder root = LayersBridge.getRootFolder("Shortcuts", null);
            Map<ShortcutAction, Set<String>> m = this.readKeymap(root, true);
            this.overrideWithKeyMap(m, this.readOriginalKeymap(root), profile);
            root = LayersBridge.getRootFolder("Keymaps", profile);
            this.overrideWithKeyMap(m, this.readKeymap(root, true), profile);
            this.overrideWithKeyMap(m, this.readOriginalKeymap(root), profile);
            m.remove(REMOVED);
            this.keymapDefaults.put(profile, m);
        }
        return Collections.unmodifiableMap(this.keymapDefaults.get(profile));
    }

    DataObject getDataObject(Object action) {
        return this.actionToDataObject.get(action);
    }

    @Override
    public void revertProfile(String profile) throws IOException {
        final DataFolder root = LayersBridge.getRootFolder("Keymaps", profile);
        if (root == null) {
            return;
        }
        final Collection reverts = (Collection)root.getPrimaryFile().getAttribute("revealEntries");
        root.getPrimaryFile().getFileSystem().runAtomicAction(new FileSystem.AtomicAction(){

            public void run() throws IOException {
                FileObject[] ch;
                for (Callable c : reverts) {
                    try {
                        c.call();
                        continue;
                    }
                    catch (IOException ex) {
                        throw ex;
                    }
                    catch (Exception ex) {
                        throw new IOException("Unexpected error", ex);
                    }
                }
                for (FileObject f : ch = root.getPrimaryFile().getChildren()) {
                    if (!f.canRevert()) continue;
                    f.revert();
                }
                root.getPrimaryFile().refresh();
            }
        });
        this.keymaps.remove(profile);
    }

    private Map<ShortcutAction, Set<String>> readOriginalKeymap(DataFolder root) {
        HashMap<ShortcutAction, Set<String>> keymap = new HashMap<ShortcutAction, Set<String>>();
        if (root == null) {
            return keymap;
        }
        FileObject fo = root.getPrimaryFile();
        Collection entries = (Collection)fo.getAttribute("revealEntries");
        HashSet<String> names = new HashSet<String>();
        for (FileObject f : entries) {
            try {
                GlobalAction action;
                names.add(f.getName());
                if ("removed".equals(f.getExt()) && FileUtil.findBrother((FileObject)f, (String)"shadow") == null) {
                    action = REMOVED;
                } else {
                    DataObject dataObject;
                    FileObject orig = DataShadow.findOriginal((FileObject)f);
                    if (orig == null || (dataObject = DataObject.find((FileObject)orig)) instanceof DataFolder) continue;
                    action = this.createActionWithLookup(dataObject, null, f.getName(), false);
                }
                if (action == null) continue;
                String shortcut = f.getName();
                LOG.log(Level.FINEST, "Overriden action {0}: {1}, by {2}", new Object[]{action.getId(), shortcut, f.getPath()});
                Set<String> s = keymap.get(action);
                if (s == null) {
                    s = new HashSet<String>();
                    keymap.put(action, s);
                }
                s.add(shortcut);
            }
            catch (IOException ex) {}
        }
        return keymap;
    }

    @Override
    public void revertActions(String profile, Collection<ShortcutAction> actions) throws IOException {
        Map<ShortcutAction, Set<String>> defaultKeyMap = this.getDefaultKeymap(profile);
        Map<ShortcutAction, Set<String>> keyMap = this.getKeymap(profile);
        DataFolder root = LayersBridge.getRootFolder("Keymaps", profile);
        if (root == null) {
            return;
        }
        final FileObject fo = root.getPrimaryFile();
        final Collection entries = (Collection)fo.getAttribute("revealEntries");
        final HashSet<String> keys = new HashSet<String>();
        final HashSet<String> discard = new HashSet<String>();
        for (ShortcutAction ac : actions) {
            Set<String> sc = defaultKeyMap.get(ac);
            if (sc != null) {
                keys.addAll(sc);
            }
            if ((sc = keyMap.get(ac)) == null) continue;
            discard.addAll(sc);
        }
        fo.getFileSystem().runAtomicAction(new FileSystem.AtomicAction(){

            public void run() throws IOException {
                if (keys != null) {
                    for (FileObject f : entries) {
                        if (!keys.remove(f.getName())) continue;
                        try {
                            ((Callable)f).call();
                            continue;
                        }
                        catch (IOException ex) {
                            throw ex;
                        }
                        catch (Exception ex) {
                            throw new IOException("Cannot revert", ex);
                        }
                    }
                    Iterator it = keys.iterator();
                    while (it.hasNext()) {
                        String s = (String)it.next();
                        FileObject mask = fo.getFileObject(s, "removed");
                        if (mask == null) continue;
                        mask.delete();
                        it.remove();
                    }
                }
                for (String s : discard) {
                    FileObject ex = fo.getFileObject(s, "shadow");
                    if (ex == null) continue;
                    ex.delete();
                }
            }
        });
    }

    private Map<ShortcutAction, Set<String>> readKeymap(DataFolder root) {
        return this.readKeymap(root, false);
    }

    private Map<ShortcutAction, Set<String>> readKeymap(DataFolder root, boolean ignoreUserRemoves) {
        LOG.log(Level.FINEST, "Reading keymap from: {0}", (Object)root);
        HashMap<ShortcutAction, Set<String>> keymap = new HashMap<ShortcutAction, Set<String>>();
        if (root == null) {
            return keymap;
        }
        Enumeration en = root.children(false);
        while (en.hasMoreElements()) {
            GlobalAction action;
            DataObject dataObject = (DataObject)en.nextElement();
            if (dataObject instanceof DataFolder || (action = this.createActionWithLookup(dataObject, null, dataObject.getPrimaryFile().getName(), ignoreUserRemoves)) == null) continue;
            String shortcut = dataObject.getPrimaryFile().getName();
            LOG.log(Level.FINEST, "Action {0}: {1}, by {2}", new Object[]{action.getId(), shortcut, dataObject.getPrimaryFile().getPath()});
            Set<String> s = keymap.get(action);
            if (s == null) {
                s = new HashSet<String>();
                keymap.put(action, s);
            }
            s.add(shortcut);
        }
        return keymap;
    }

    @Override
    public void deleteProfile(String profile) {
        FileObject root = FileUtil.getConfigFile((String)"Keymaps");
        if (root == null) {
            return;
        }
        if ((root = root.getFileObject(profile)) == null) {
            return;
        }
        try {
            root.delete();
        }
        catch (IOException ex) {
            ErrorManager.getDefault().notify((Throwable)ex);
        }
    }

    @Override
    public void saveKeymap(String profile, Map<ShortcutAction, Set<String>> actionToShortcuts) {
        this.keymaps.remove(profile);
        this.keymapDefaults.remove(profile);
        DataFolder defaultFolder = LayersBridge.getRootFolder("Shortcuts", null);
        DataFolder folder = LayersBridge.getRootFolder("Keymaps", profile);
        if (folder == null) {
            folder = LayersBridge.getRootFolder("Keymaps", null);
            try {
                folder = DataFolder.create((DataFolder)folder, (String)profile);
            }
            catch (IOException ex) {
                ErrorManager.getDefault().notify((Throwable)ex);
                return;
            }
        }
        this.saveKeymap(defaultFolder, folder, actionToShortcuts);
    }

    private void saveKeymap(DataFolder defaultMap, DataFolder folder, Map<ShortcutAction, Set<String>> actionToShortcuts) {
        GlobalAction action;
        LOG.log(Level.FINEST, "Saving keymap to: {0}", folder.getPrimaryFile().getPath());
        this.getActions();
        Map<String, ShortcutAction> shortcutToAction = LayersBridge.shortcutToAction(actionToShortcuts);
        HashSet<String> definedShortcuts = new HashSet<String>(shortcutToAction.keySet());
        FileObject targetDir = folder.getPrimaryFile();
        Enumeration en = folder.children();
        while (en.hasMoreElements()) {
            DataObject dataObject = (DataObject)en.nextElement();
            if (dataObject.getPrimaryFile().getExt().equals("removed")) continue;
            GlobalAction a1 = (GlobalAction)shortcutToAction.get(dataObject.getName());
            if (a1 != null) {
                GlobalAction action2 = this.createAction(dataObject, null, dataObject.getPrimaryFile().getName(), false);
                if (action2 == null) {
                    LOG.log(Level.FINEST, "Broken action shortcut will be removed: {0}, will replace by {1}", new Object[]{dataObject.getName(), a1.getId()});
                } else if (action2.equals(a1)) {
                    LOG.log(Level.FINEST, "Found same binding: {0} -> {1}", new Object[]{dataObject.getName(), action2.getId()});
                    shortcutToAction.remove(dataObject.getName());
                    continue;
                }
            }
            try {
                LOG.log(Level.FINEST, "Removing obsolete binding: {0}", dataObject.getName());
                dataObject.delete();
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        HashSet<String> defaultNames = new HashSet<String>();
        en = defaultMap.children();
        while (en.hasMoreElements()) {
            DataObject dataObject = (DataObject)en.nextElement();
            GlobalAction ga = (GlobalAction)shortcutToAction.get(dataObject.getName());
            if (ga == null || !ga.equals(action = this.createAction(dataObject, null, dataObject.getPrimaryFile().getName(), false))) continue;
            LOG.log(Level.FINEST, "Leaving default shortcut: {0}", dataObject.getName());
            defaultNames.add(dataObject.getName());
        }
        for (String shortcut : shortcutToAction.keySet()) {
            action = (GlobalAction)shortcutToAction.get(shortcut);
            DataObject dataObject = this.actionToDataObject.get(action);
            if (dataObject == null) {
                if (System.getProperty("org.netbeans.optionsDialog") == null) continue;
                System.out.println("No original DataObject specified! Not possible to create shadow1. " + action);
                continue;
            }
            FileObject f = targetDir.getFileObject(shortcut, "removed");
            try {
                if (f != null) {
                    f.delete();
                }
                if (defaultNames.contains(shortcut)) continue;
                DataShadow.create((DataFolder)folder, (String)shortcut, (DataObject)dataObject);
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        if (defaultMap != null) {
            en = defaultMap.children();
            while (en.hasMoreElements()) {
                DataObject dataObject = (DataObject)en.nextElement();
                if (definedShortcuts.contains(dataObject.getName())) continue;
                try {
                    FileObject pf = dataObject.getPrimaryFile();
                    if (targetDir.getFileObject(pf.getName(), "removed") != null) continue;
                    LOG.log(Level.FINEST, "Masking out binding: {0}", pf.getName());
                    folder.getPrimaryFile().createData(pf.getName(), "removed");
                }
                catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private static DataFolder getExistingProfile(String profile) {
        FileObject root = FileUtil.getConfigRoot();
        FileObject fo1 = root.getFileObject("Keymaps");
        if (fo1 == null) {
            return null;
        }
        FileObject fo2 = fo1.getFileObject(profile);
        if (fo2 == null) {
            return null;
        }
        return DataFolder.findFolder((FileObject)fo2);
    }

    private static DataFolder getRootFolder(String name1, String name2) {
        FileObject root = FileUtil.getConfigRoot();
        FileObject fo1 = root.getFileObject(name1);
        try {
            if (fo1 == null) {
                fo1 = root.createFolder(name1);
            }
            if (fo1 == null) {
                return null;
            }
            if (name2 == null) {
                return DataFolder.findFolder((FileObject)fo1);
            }
            FileObject fo2 = fo1.getFileObject(name2);
            if (fo2 == null) {
                fo2 = fo1.createFolder(name2);
            }
            if (fo2 == null) {
                return null;
            }
            return DataFolder.findFolder((FileObject)fo2);
        }
        catch (IOException ex) {
            ErrorManager.getDefault().notify((Throwable)ex);
            return null;
        }
    }

    private GlobalAction createActionWithLookup(DataObject dataObject, String prefix, String name, boolean ignoreUserRemoves) {
        GlobalAction a = this.createAction(dataObject, prefix, name, ignoreUserRemoves);
        if (a == null) {
            return null;
        }
        GlobalAction b = this.actions.get(a);
        return b == null ? a : b;
    }

    private GlobalAction createAction(DataObject dataObject, String prefix, String name, boolean ignoreUserRemoves) {
        InstanceCookie ic = (InstanceCookie)dataObject.getCookie(InstanceCookie.class);
        FileObject pf = dataObject.getPrimaryFile();
        if (ignoreUserRemoves && pf.canRevert()) {
            return null;
        }
        if (ic == null) {
            if (!"removed".equals(pf.getExt())) {
                LOG.log(Level.WARNING, "Invalid shortcut: {0}", (Object)dataObject);
                return null;
            }
            if (FileUtil.findBrother((FileObject)pf, (String)"shadow") != null) {
                return null;
            }
            return REMOVED;
        }
        try {
            Object action = ic.instanceCreate();
            if (action == null) {
                return null;
            }
            if (!(action instanceof Action)) {
                return null;
            }
            return LayersBridge.createAction((Action)action, prefix, name);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private static GlobalAction createAction(Action a, String prefix, String name) {
        String id = name;
        try {
            if (a.getClass().getName().equals("org.openide.awt.GeneralAction$DelegateAction")) {
                String key;
                if (KEY_FIELD == null) {
                    Class c = a.getClass();
                    Field f = c.getSuperclass().getDeclaredField("key");
                    f.setAccessible(true);
                    KEY_FIELD = f;
                }
                if ((key = (String)KEY_FIELD.get(a)) != null) {
                    id = key;
                }
            }
        }
        catch (NoSuchFieldException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (IllegalAccessException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        return new GlobalAction(a, prefix, id);
    }

    static Map<String, ShortcutAction> shortcutToAction(Map<ShortcutAction, Set<String>> actionToShortcuts) {
        HashMap<String, ShortcutAction> shortcutToAction = new HashMap<String, ShortcutAction>();
        for (Map.Entry<ShortcutAction, Set<String>> entry : actionToShortcuts.entrySet()) {
            ShortcutAction action = entry.getKey();
            Set<String> shortcuts = entry.getValue();
            ShortcutAction shortcutAction = action = action != null ? action.getKeymapManagerInstance("LayersBridge") : null;
            if (!(action instanceof GlobalAction)) continue;
            for (String multiShortcut : shortcuts) {
                shortcutToAction.put(multiShortcut, action);
            }
        }
        return shortcutToAction;
    }

    @Override
    public void refreshActions() {
        this.refreshKeymapNames();
    }

    @Override
    public String getCurrentProfile() {
        return this.cachedProfile;
    }

    @Override
    public void setCurrentProfile(String profileName) {
        this.cachedProfile = profileName;
    }

    @Override
    public boolean isCustomProfile(String profileName) {
        DataFolder profileFolder = LayersBridge.getExistingProfile(profileName);
        if (profileFolder == null) {
            return true;
        }
        FileObject f = profileFolder.getPrimaryFile();
        if (!f.canRevert()) {
            return false;
        }
        FileObject parentF = profileFolder.getPrimaryFile().getParent();
        if (parentF == null) {
            return true;
        }
        Collection col = (Collection)parentF.getAttribute("revealEntries");
        if (col == null) {
            return true;
        }
        for (FileObject f2 : col) {
            if (!f2.getNameExt().equals(profileName)) continue;
            return false;
        }
        return true;
    }

    static String getOrigActionClass(ShortcutAction sa) {
        if (!(sa instanceof GlobalAction)) {
            return null;
        }
        GlobalAction ga = (GlobalAction)sa;
        return ga.action == null ? null : ga.action.getClass().getName();
    }

    private static class GlobalAction
    implements ShortcutAction {
        private Action action;
        String name;
        private String id;

        private GlobalAction(Action a, String prefix, String n) {
            this.action = a;
            this.id = n;
        }

        @Override
        public String getDisplayName() {
            if (this.name == null) {
                try {
                    this.name = (String)this.action.getValue("Name");
                }
                catch (MissingResourceException ex) {
                    LOG.log(Level.WARNING, "Missing resources for action {0}, class {1}: {2}", new Object[]{this.id, this.action.getClass().getName(), ex.getLocalizedMessage()});
                }
                if (this.name == null) {
                    this.name = "";
                }
                this.name = this.name.replaceAll("&", "").trim();
            }
            return this.name;
        }

        @Override
        public String getId() {
            if (this.id == null) {
                this.id = this.action.getClass().getName();
            }
            return this.id;
        }

        @Override
        public String getDelegatingActionId() {
            return null;
        }

        public boolean equals(Object o) {
            if (!(o instanceof GlobalAction)) {
                return false;
            }
            return ((GlobalAction)o).action == this.action || ((GlobalAction)o).action.equals(this.action);
        }

        public int hashCode() {
            return this.action == null ? 111 : this.action.hashCode();
        }

        public String toString() {
            return "GlobalAction[" + this.getDisplayName() + ":" + this.id + "]";
        }

        @Override
        public ShortcutAction getKeymapManagerInstance(String keymapManagerName) {
            if ("LayersBridge".equals(keymapManagerName)) {
                return this;
            }
            return null;
        }
    }

}

