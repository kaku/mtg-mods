/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.options.keymap;

import javax.swing.Popup;

public interface Popupable {
    public Popup getPopup();

    public void hidePopup();
}

