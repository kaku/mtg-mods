/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.options.keymap;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.netbeans.core.options.keymap.api.ShortcutAction;

public class CompoundAction
implements ShortcutAction {
    private static final String DEFAULT_PROVIDER = "EditorBridge";
    private Map<String, ShortcutAction> actions;

    public CompoundAction(Map<String, ShortcutAction> actions) {
        this.actions = actions;
    }

    void addAction(String mgr, ShortcutAction ac) {
        this.actions.put(mgr, ac);
    }

    @Override
    public String getDisplayName() {
        ShortcutAction s = this.actions.get("EditorBridge");
        if (s != null) {
            return s.getDisplayName();
        }
        for (ShortcutAction sa : this.actions.values()) {
            String dn = sa.getDisplayName();
            if (dn == null) continue;
            return dn;
        }
        return "";
    }

    @Override
    public String getId() {
        ShortcutAction s = this.actions.get("EditorBridge");
        if (s != null) {
            return s.getId();
        }
        for (ShortcutAction sa : this.actions.values()) {
            String id = sa.getId();
            if (id == null) continue;
            return id;
        }
        return "<error>";
    }

    @Override
    public String getDelegatingActionId() {
        ShortcutAction s = this.actions.get("EditorBridge");
        if (s != null) {
            return s.getDelegatingActionId();
        }
        for (ShortcutAction sa : this.actions.values()) {
            String id = sa.getDelegatingActionId();
            if (id == null) continue;
            return id;
        }
        return null;
    }

    public boolean equals(Object o) {
        if (!(o instanceof CompoundAction)) {
            return false;
        }
        if (this.actions.get("EditorBridge") != null) {
            return this.getKeymapManagerInstance("EditorBridge").equals(((CompoundAction)o).getKeymapManagerInstance("EditorBridge"));
        }
        if (this.actions.keySet().isEmpty()) {
            return false;
        }
        String k = this.actions.keySet().iterator().next();
        return this.getKeymapManagerInstance(k).equals(((CompoundAction)o).getKeymapManagerInstance(k));
    }

    public int hashCode() {
        if (this.actions.get("EditorBridge") != null) {
            return this.getKeymapManagerInstance("EditorBridge").hashCode() * 2;
        }
        if (this.actions.keySet().isEmpty()) {
            return 0;
        }
        String k = this.actions.keySet().iterator().next();
        return this.actions.get(k).hashCode() * 2;
    }

    @Override
    public ShortcutAction getKeymapManagerInstance(String keymapManagerName) {
        return this.actions.get(keymapManagerName);
    }

    public String toString() {
        return "CompoundAction[" + this.actions + "]";
    }
}

