/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.keymap;

import org.netbeans.core.options.keymap.api.ShortcutAction;
import org.openide.util.NbBundle;

public class ActionHolder {
    private ShortcutAction action;
    private boolean isAlternative;

    public ActionHolder(ShortcutAction action, boolean isAlternative) {
        this.action = action;
        this.isAlternative = isAlternative;
    }

    public ShortcutAction getAction() {
        return this.action;
    }

    public boolean isAlternative() {
        return this.isAlternative;
    }

    public String toString() {
        String displayName = this.action.getDisplayName();
        return this.isAlternative ? displayName + " " + NbBundle.getMessage(ActionHolder.class, (String)"Alternative_Shortcut") : displayName;
    }
}

