/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.options.keymap;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;
import org.netbeans.modules.options.keymap.ShortcutCellPanel;

public class ButtonCellRenderer
implements TableCellRenderer {
    private TableCellRenderer defaultRenderer;
    private static ShortcutCellPanel panel;

    public ButtonCellRenderer(TableCellRenderer defaultRenderer) {
        this.defaultRenderer = defaultRenderer;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JLabel c = (JLabel)this.defaultRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        if (value instanceof String) {
            Rectangle cellRect = table.getCellRect(row, column, false);
            String scCell = (String)value;
            Dimension d = new Dimension((int)cellRect.getWidth(), (int)cellRect.getHeight());
            if (panel == null) {
                panel = new ShortcutCellPanel(scCell);
            }
            panel.setText(scCell);
            panel.setSize(d);
            if (isSelected) {
                panel.setBgColor(table.getSelectionBackground());
                if (UIManager.getLookAndFeel().getID().equals("GTK")) {
                    panel.setFgCOlor(table.getForeground(), true);
                } else {
                    panel.setFgCOlor(table.getSelectionForeground(), true);
                }
            } else {
                panel.setBgColor(c.getBackground());
                panel.setFgCOlor(c.getForeground(), false);
            }
            if (hasFocus) {
                panel.setBorder(c.getBorder());
            } else {
                panel.setBorder(null);
            }
            return panel;
        }
        return c;
    }
}

