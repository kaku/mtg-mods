/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.options.keymap;

import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import org.netbeans.modules.options.keymap.KeymapPanel;
import org.netbeans.modules.options.keymap.KeymapViewModel;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

public final class KeymapPanelController
extends OptionsPanelController {
    private KeymapPanel keymapPanel;

    public void update() {
        this.getKeymapPanel().update();
    }

    public void applyChanges() {
        this.getKeymapPanel().applyChanges();
    }

    public void cancel() {
        this.getKeymapPanel().cancel();
    }

    public boolean isValid() {
        return this.getKeymapPanel().dataValid();
    }

    public boolean isChanged() {
        return this.getKeymapPanel().isChanged();
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx("netbeans.optionsDialog.keymaps");
    }

    public Lookup getLookup() {
        return Lookups.singleton((Object)this.getKeymapPanel().getModel());
    }

    public JComponent getComponent(Lookup masterLookup) {
        return this.getKeymapPanel();
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.getKeymapPanel().addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.getKeymapPanel().removePropertyChangeListener(l);
    }

    private synchronized KeymapPanel getKeymapPanel() {
        if (this.keymapPanel == null) {
            this.keymapPanel = new KeymapPanel();
        }
        return this.keymapPanel;
    }
}

