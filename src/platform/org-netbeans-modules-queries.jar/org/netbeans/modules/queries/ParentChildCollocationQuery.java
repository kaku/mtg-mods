/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.queries;

import java.net.URI;
import org.netbeans.spi.queries.CollocationQueryImplementation2;

public class ParentChildCollocationQuery
implements CollocationQueryImplementation2 {
    @Override
    public boolean areCollocated(URI file1, URI file2) {
        String f2;
        if (file1.equals(file2)) {
            return true;
        }
        String f1 = file1.toString();
        if (!f1.endsWith("/")) {
            f1 = f1 + "/";
        }
        if (!(f2 = file2.toString()).endsWith("/")) {
            f2 = f2 + "/";
        }
        return f1.startsWith(f2) || f2.startsWith(f1);
    }

    @Override
    public URI findRoot(URI file) {
        return null;
    }
}

