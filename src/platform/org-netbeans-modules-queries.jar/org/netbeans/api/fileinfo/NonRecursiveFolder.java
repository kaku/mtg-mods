/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.api.fileinfo;

import org.openide.filesystems.FileObject;

public interface NonRecursiveFolder {
    public FileObject getFolder();
}

