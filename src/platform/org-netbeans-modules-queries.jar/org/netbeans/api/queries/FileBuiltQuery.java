/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 */
package org.netbeans.api.queries;

import java.util.Collection;
import javax.swing.event.ChangeListener;
import org.netbeans.spi.queries.FileBuiltQueryImplementation;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public final class FileBuiltQuery {
    private static final Lookup.Result<FileBuiltQueryImplementation> implementations = Lookup.getDefault().lookupResult(FileBuiltQueryImplementation.class);

    private FileBuiltQuery() {
    }

    public static Status getStatus(FileObject file) {
        if (!file.isValid()) {
            return null;
        }
        for (FileBuiltQueryImplementation fbqi : implementations.allInstances()) {
            Status s = fbqi.getStatus(file);
            if (s == null) continue;
            return s;
        }
        return null;
    }

    public static interface Status {
        public boolean isBuilt();

        public void addChangeListener(ChangeListener var1);

        public void removeChangeListener(ChangeListener var1);
    }

}

