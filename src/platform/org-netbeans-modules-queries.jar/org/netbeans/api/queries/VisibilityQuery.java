/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Parameters
 */
package org.netbeans.api.queries;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.spi.queries.VisibilityQueryImplementation;
import org.netbeans.spi.queries.VisibilityQueryImplementation2;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Parameters;

public final class VisibilityQuery {
    private static final VisibilityQuery INSTANCE = new VisibilityQuery();
    private final ResultListener resultListener;
    private final VqiChangedListener vqiListener;
    private final List<ChangeListener> listeners;
    private Lookup.Result<VisibilityQueryImplementation> vqiResult;
    private List<VisibilityQueryImplementation> cachedVqiInstances;

    public static final VisibilityQuery getDefault() {
        return INSTANCE;
    }

    private VisibilityQuery() {
        this.resultListener = new ResultListener();
        this.vqiListener = new VqiChangedListener();
        this.listeners = new ArrayList<ChangeListener>();
        this.vqiResult = null;
        this.cachedVqiInstances = null;
    }

    public boolean isVisible(FileObject file) {
        Parameters.notNull((CharSequence)"file", (Object)file);
        for (VisibilityQueryImplementation vqi : this.getVqiInstances()) {
            if (vqi.isVisible(file)) continue;
            return false;
        }
        return true;
    }

    public boolean isVisible(File file) {
        Parameters.notNull((CharSequence)"file", (Object)file);
        for (VisibilityQueryImplementation vqi : this.getVqiInstances()) {
            FileObject fo;
            if (!(vqi instanceof VisibilityQueryImplementation2 ? !((VisibilityQueryImplementation2)vqi).isVisible(file) : (fo = FileUtil.toFileObject((File)file)) != null && !vqi.isVisible(fo))) continue;
            return false;
        }
        return true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addChangeListener(ChangeListener l) {
        List<ChangeListener> list = this.listeners;
        synchronized (list) {
            this.listeners.add(l);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeChangeListener(ChangeListener l) {
        List<ChangeListener> list = this.listeners;
        synchronized (list) {
            this.listeners.remove(l);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fireChange(ChangeEvent event) {
        ArrayList<ChangeListener> lists;
        assert (event != null);
        List<ChangeListener> list = this.listeners;
        synchronized (list) {
            lists = new ArrayList<ChangeListener>(this.listeners);
        }
        for (ChangeListener listener : lists) {
            try {
                listener.stateChanged(event);
            }
            catch (RuntimeException x) {
                Exceptions.printStackTrace((Throwable)x);
            }
        }
    }

    private synchronized List<VisibilityQueryImplementation> getVqiInstances() {
        if (this.cachedVqiInstances == null) {
            this.vqiResult = Lookup.getDefault().lookupResult(VisibilityQueryImplementation.class);
            this.vqiResult.addLookupListener((LookupListener)this.resultListener);
            this.setupChangeListeners(null, new ArrayList<VisibilityQueryImplementation>(this.vqiResult.allInstances()));
        }
        return this.cachedVqiInstances;
    }

    private synchronized void setupChangeListeners(List<VisibilityQueryImplementation> oldVqiInstances, List<VisibilityQueryImplementation> newVqiInstances) {
        if (oldVqiInstances != null) {
            HashSet<VisibilityQueryImplementation> removed = new HashSet<VisibilityQueryImplementation>(oldVqiInstances);
            removed.removeAll(newVqiInstances);
            for (VisibilityQueryImplementation vqi : removed) {
                vqi.removeChangeListener(this.vqiListener);
            }
        }
        HashSet<VisibilityQueryImplementation> added = new HashSet<VisibilityQueryImplementation>(newVqiInstances);
        if (oldVqiInstances != null) {
            added.removeAll(oldVqiInstances);
        }
        for (VisibilityQueryImplementation vqi : added) {
            vqi.addChangeListener(this.vqiListener);
        }
        this.cachedVqiInstances = newVqiInstances;
    }

    private class VqiChangedListener
    implements ChangeListener {
        private VqiChangedListener() {
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            VisibilityQuery.this.fireChange(e);
        }
    }

    private class ResultListener
    implements LookupListener {
        private ResultListener() {
        }

        public void resultChanged(LookupEvent ev) {
            VisibilityQuery.this.setupChangeListeners(VisibilityQuery.this.cachedVqiInstances, new ArrayList(VisibilityQuery.this.vqiResult.allInstances()));
            VisibilityQuery.this.fireChange(new ChangeEvent(this));
        }
    }

}

