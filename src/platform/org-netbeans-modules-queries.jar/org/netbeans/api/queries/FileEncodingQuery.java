/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.util.Lookup
 *  org.openide.util.NbPreferences
 */
package org.netbeans.api.queries;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import org.netbeans.modules.queries.UnknownEncoding;
import org.netbeans.spi.queries.FileEncodingQueryImplementation;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;

public class FileEncodingQuery {
    private static final int BUFSIZ = 4096;
    private static final String DEFAULT_ENCODING = "default-encoding";
    private static final String UTF_8 = "UTF-8";
    private static final Logger LOG = Logger.getLogger(FileEncodingQuery.class.getName());
    static final String ENCODER_SELECTED = "encoder-selected";
    static final String DECODER_SELECTED = "decoder-selected";

    private FileEncodingQuery() {
    }

    public static Charset getEncoding(FileObject file) {
        if (file == null) {
            throw new IllegalArgumentException();
        }
        ArrayList<Charset> delegates = new ArrayList<Charset>();
        for (FileEncodingQueryImplementation impl : Lookup.getDefault().lookupAll(FileEncodingQueryImplementation.class)) {
            Charset encoding = impl.getEncoding(file);
            if (encoding != null) {
                LOG.log(Level.FINE, "{0}: received encoding {1} from {2}", new Object[]{file, encoding, impl});
                delegates.add(encoding);
                continue;
            }
            LOG.log(Level.FINER, "{0}: received no encoding from {1}", new Object[]{file, impl});
        }
        try {
            if (file.getFileSystem().isDefault()) {
                delegates.add(Charset.forName("UTF-8"));
            } else {
                delegates.add(Charset.defaultCharset());
            }
        }
        catch (FileStateInvalidException ex) {
            delegates.add(Charset.defaultCharset());
        }
        LOG.log(Level.FINE, "{0}: using encodings {1}", new Object[]{file, delegates});
        return new ProxyCharset(delegates);
    }

    public static Charset getDefaultEncoding() {
        Preferences prefs = NbPreferences.forModule(FileEncodingQuery.class);
        String defaultEncoding = prefs.get("default-encoding", "UTF-8");
        return Charset.forName(defaultEncoding);
    }

    public static void setDefaultEncoding(Charset encoding) {
        if (encoding == null) {
            throw new IllegalArgumentException();
        }
        Preferences prefs = NbPreferences.forModule(FileEncodingQuery.class);
        prefs.put("default-encoding", encoding.name());
    }

    private static class ProxyCharset
    extends Charset {
        private static final ByteBuffer EMPTY_BYTE_BUFFER = ByteBuffer.allocate(0);
        private static final CharBuffer EMPTY_CHAR_BUFFER = CharBuffer.allocate(0);
        private final List<? extends Charset> delegates;

        private ProxyCharset(List<? extends Charset> delegates) {
            super(delegates.get(0).name(), delegates.get(0).aliases().toArray(new String[delegates.get(0).aliases().size()]));
            this.delegates = delegates;
        }

        @Override
        public boolean contains(Charset charset) {
            return this.delegates.get(0).contains(charset);
        }

        @Override
        public CharsetDecoder newDecoder() {
            return new ProxyDecoder(this.delegates.get(0).newDecoder());
        }

        @Override
        public CharsetEncoder newEncoder() {
            return new ProxyEncoder(this.delegates.get(0).newEncoder());
        }

        private class ProxyEncoder
        extends CharsetEncoder {
            private CharsetEncoder currentEncoder;
            private CharBuffer buffer;
            private CharBuffer remainder;
            private CodingErrorAction malformedInputAction;
            private CodingErrorAction unmappableCharAction;
            private byte[] replace;
            private boolean initialized;
            private ByteBuffer lastByteBuffer;

            private ProxyEncoder(CharsetEncoder defaultEncoder) {
                super(ProxyCharset.this, defaultEncoder.averageBytesPerChar(), defaultEncoder.maxBytesPerChar(), defaultEncoder.replacement());
                this.buffer = CharBuffer.allocate(4096);
                this.currentEncoder = defaultEncoder;
                this.initialized = true;
            }

            @Override
            protected CoderResult encodeLoop(CharBuffer in, ByteBuffer out) {
                this.lastByteBuffer = out;
                if (this.buffer == null) {
                    CoderResult result;
                    if (this.remainder != null) {
                        result = this.currentEncoder.encode(this.remainder, out, false);
                        if (!this.remainder.hasRemaining()) {
                            this.remainder = null;
                        }
                        if (!result.isUnderflow()) {
                            return result;
                        }
                    }
                    result = this.currentEncoder.encode(in, out, false);
                    return result;
                }
                if (this.buffer.remaining() == 0 || this.buffer.position() > 0 && in.limit() == 0) {
                    CoderResult result = this.encodeHead(in, out, false);
                    return result;
                }
                if (this.buffer.remaining() < in.remaining()) {
                    int limit = in.limit();
                    in.limit(in.position() + this.buffer.remaining());
                    this.buffer.put(in);
                    in.limit(limit);
                    CoderResult result = this.encodeHead(in, out, false);
                    return result;
                }
                this.buffer.put(in);
                return CoderResult.UNDERFLOW;
            }

            private CoderResult encodeHead(CharBuffer in, ByteBuffer out, boolean flush) {
                CharBuffer b = this.buffer == null ? this.remainder : this.buffer;
                b.flip();
                CoderResult result = null;
                for (int i = 0; i < ProxyCharset.this.delegates.size(); ++i) {
                    this.currentEncoder = ((Charset)ProxyCharset.this.delegates.get(i)).newEncoder();
                    if (this.malformedInputAction != null) {
                        this.currentEncoder.onMalformedInput(this.malformedInputAction);
                    }
                    if (this.unmappableCharAction != null) {
                        this.currentEncoder.onUnmappableCharacter(this.unmappableCharAction);
                    }
                    if (this.replace != null) {
                        this.currentEncoder.replaceWith(this.replace);
                    }
                    int outPos = out.position();
                    try {
                        CharBuffer view = b.asReadOnlyBuffer();
                        result = this.currentEncoder.encode(view, out, in == null);
                        if (result.isOverflow()) {
                            LOG.log(Level.FINEST, "encoder-selected", this.currentEncoder);
                            this.remainder = view;
                            this.buffer = null;
                            return result;
                        }
                        if (result.isUnmappable()) {
                            return result;
                        }
                        if (in != null) {
                            result = this.currentEncoder.encode(in, out, false);
                        }
                        if (result.isUnderflow() && flush) {
                            result = this.currentEncoder.flush(out);
                        }
                        LOG.log(Level.FINEST, "encoder-selected", this.currentEncoder);
                        this.buffer = null;
                        return result;
                    }
                    catch (UnknownEncoding e) {
                        if (outPos == out.position()) continue;
                        this.buffer = null;
                        return result;
                    }
                }
                this.buffer = null;
                assert (result != null);
                return result;
            }

            @Override
            protected CoderResult implFlush(ByteBuffer out) {
                this.lastByteBuffer = null;
                if (this.buffer != null || this.remainder != null) {
                    return this.encodeHead(null, out, true);
                }
                this.currentEncoder.encode(EMPTY_CHAR_BUFFER, out, true);
                return this.currentEncoder.flush(out);
            }

            @Override
            protected void implReset() {
                if (this.lastByteBuffer != null) {
                    this.implFlush(this.lastByteBuffer);
                }
                this.currentEncoder.reset();
            }

            @Override
            protected void implOnMalformedInput(CodingErrorAction action) {
                if (this.buffer != null || !this.initialized) {
                    this.malformedInputAction = action;
                } else {
                    this.currentEncoder.onMalformedInput(action);
                }
            }

            @Override
            protected void implOnUnmappableCharacter(CodingErrorAction action) {
                if (this.buffer != null || !this.initialized) {
                    this.unmappableCharAction = action;
                } else {
                    this.currentEncoder.onUnmappableCharacter(action);
                }
            }

            @Override
            protected void implReplaceWith(byte[] replace) {
                if (this.buffer != null || !this.initialized) {
                    this.replace = replace;
                } else {
                    this.currentEncoder.replaceWith(replace);
                }
            }
        }

        private class ProxyDecoder
        extends CharsetDecoder {
            private CharsetDecoder currentDecoder;
            private ByteBuffer buffer;
            private ByteBuffer remainder;
            private CodingErrorAction malformedInputAction;
            private CodingErrorAction unmappableCharAction;
            private String replace;
            private boolean initialized;
            private CharBuffer lastCharBuffer;

            private ProxyDecoder(CharsetDecoder defaultDecoder) {
                super(ProxyCharset.this, defaultDecoder.averageCharsPerByte(), defaultDecoder.maxCharsPerByte());
                this.buffer = ByteBuffer.allocate(4096);
                this.currentDecoder = defaultDecoder;
                this.initialized = true;
            }

            @Override
            protected CoderResult decodeLoop(ByteBuffer in, CharBuffer out) {
                this.lastCharBuffer = out;
                if (this.buffer == null) {
                    if (this.remainder != null) {
                        ByteBuffer tmp = ByteBuffer.allocate(this.remainder.remaining() + in.remaining());
                        tmp.put(this.remainder);
                        tmp.put(in);
                        tmp.flip();
                        CoderResult result = this.currentDecoder.decode(tmp, out, false);
                        this.remainder = tmp.hasRemaining() ? tmp : null;
                        return result;
                    }
                    return this.currentDecoder.decode(in, out, false);
                }
                if (this.buffer.remaining() == 0) {
                    return this.decodeHead(in, out, false);
                }
                if (this.buffer.remaining() < in.remaining()) {
                    int limit = in.limit();
                    in.limit(in.position() + this.buffer.remaining());
                    this.buffer.put(in);
                    in.limit(limit);
                    return this.decodeHead(in, out, false);
                }
                this.buffer.put(in);
                return CoderResult.UNDERFLOW;
            }

            private CoderResult decodeHead(ByteBuffer in, CharBuffer out, boolean flush) {
                this.buffer.flip();
                CoderResult result = null;
                for (int i = 0; i < ProxyCharset.this.delegates.size(); ++i) {
                    this.currentDecoder = ((Charset)ProxyCharset.this.delegates.get(i)).newDecoder();
                    if (this.malformedInputAction != null) {
                        this.currentDecoder.onMalformedInput(this.malformedInputAction);
                    }
                    if (this.unmappableCharAction != null) {
                        this.currentDecoder.onUnmappableCharacter(this.unmappableCharAction);
                    }
                    if (this.replace != null) {
                        this.currentDecoder.replaceWith(this.replace);
                    }
                    int outPos = out.position();
                    try {
                        ByteBuffer view = this.buffer.asReadOnlyBuffer();
                        result = this.currentDecoder.decode(view, out, in == null);
                        if (view.hasRemaining()) {
                            if (flush) {
                                this.currentDecoder.flush(out);
                            }
                            LOG.log(Level.FINEST, "decoder-selected", this.currentDecoder);
                            this.remainder = view;
                            this.buffer = null;
                            return result;
                        }
                        if (in != null) {
                            result = this.currentDecoder.decode(in, out, false);
                        }
                        if (flush) {
                            result = this.currentDecoder.flush(out);
                        }
                        LOG.log(Level.FINEST, "decoder-selected", this.currentDecoder);
                        this.buffer = null;
                        return result;
                    }
                    catch (UnknownEncoding e) {
                        if (outPos == out.position()) continue;
                        this.buffer = null;
                        return result;
                    }
                }
                this.buffer = null;
                assert (result != null);
                return result;
            }

            @Override
            protected CoderResult implFlush(CharBuffer out) {
                this.lastCharBuffer = null;
                if (this.buffer != null) {
                    return this.decodeHead(null, out, true);
                }
                this.currentDecoder.decode(EMPTY_BYTE_BUFFER, out, true);
                return this.currentDecoder.flush(out);
            }

            @Override
            protected void implReset() {
                if (this.lastCharBuffer != null) {
                    this.implFlush(this.lastCharBuffer);
                }
                this.currentDecoder.reset();
            }

            @Override
            protected void implOnMalformedInput(CodingErrorAction action) {
                if (this.buffer != null || !this.initialized) {
                    this.malformedInputAction = action;
                } else {
                    this.currentDecoder.onMalformedInput(action);
                }
            }

            @Override
            protected void implOnUnmappableCharacter(CodingErrorAction action) {
                if (this.buffer != null || !this.initialized) {
                    this.unmappableCharAction = action;
                } else {
                    this.currentDecoder.onUnmappableCharacter(action);
                }
            }

            @Override
            protected void implReplaceWith(String replace) {
                if (this.buffer != null || !this.initialized) {
                    this.replace = replace;
                } else {
                    this.currentDecoder.replaceWith(replace);
                }
            }
        }

    }

}

