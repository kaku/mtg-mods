/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 */
package org.netbeans.api.queries;

import java.net.URI;
import java.util.Collection;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.queries.FileEncodingQuery;
import org.netbeans.spi.queries.VersioningQueryImplementation;
import org.openide.util.Lookup;

public final class VersioningQuery {
    private static final Logger LOG = Logger.getLogger(FileEncodingQuery.class.getName());
    private static final Lookup.Result<VersioningQueryImplementation> implementations = Lookup.getDefault().lookupResult(VersioningQueryImplementation.class);

    private VersioningQuery() {
    }

    public static boolean isManaged(URI uri) {
        URI normUri;
        Objects.requireNonNull(uri);
        boolean asserts = false;
        if (!$assertionsDisabled) {
            asserts = true;
            if (!true) {
                throw new AssertionError();
            }
        }
        if (asserts && !uri.equals(normUri = uri.normalize())) {
            throw new IllegalArgumentException("Must pass a normalized URI: " + uri + " vs. " + normUri);
        }
        for (VersioningQueryImplementation vqi : implementations.allInstances()) {
            if (!vqi.isManaged(uri)) continue;
            LOG.log(Level.FINE, "{0} is VCS managed", new Object[]{uri});
            return true;
        }
        LOG.log(Level.FINE, "{0} isn't managed by any VCS", new Object[]{uri});
        return false;
    }

    public static String getRemoteLocation(URI uri) {
        URI normUri;
        Objects.requireNonNull(uri);
        boolean asserts = false;
        if (!$assertionsDisabled) {
            asserts = true;
            if (!true) {
                throw new AssertionError();
            }
        }
        if (asserts && !uri.equals(normUri = uri.normalize())) {
            throw new IllegalArgumentException("Must pass a normalized URI: " + uri + " vs. " + normUri);
        }
        for (VersioningQueryImplementation vqi : implementations.allInstances()) {
            String remoteLocation = vqi.getRemoteLocation(uri);
            if (remoteLocation == null) continue;
            LOG.log(Level.FINE, "{0}: received remote location {1}", new Object[]{uri, remoteLocation});
            return remoteLocation;
        }
        LOG.log(Level.FINE, "{0}: received no remote location", new Object[]{uri});
        return null;
    }
}

