/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Parameters
 *  org.openide.util.Utilities
 */
package org.netbeans.api.queries;

import java.io.File;
import java.net.URI;
import java.util.Collection;
import java.util.logging.Logger;
import org.netbeans.spi.queries.SharabilityQueryImplementation;
import org.netbeans.spi.queries.SharabilityQueryImplementation2;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Lookup;
import org.openide.util.Parameters;
import org.openide.util.Utilities;

public final class SharabilityQuery {
    private static final Lookup.Result<SharabilityQueryImplementation> implementations = Lookup.getDefault().lookupResult(SharabilityQueryImplementation.class);
    private static final Lookup.Result<SharabilityQueryImplementation2> implementations2 = Lookup.getDefault().lookupResult(SharabilityQueryImplementation2.class);
    private static final Logger LOG = Logger.getLogger(SharabilityQuery.class.getName());
    @Deprecated
    public static final int UNKNOWN = 0;
    @Deprecated
    public static final int SHARABLE = 1;
    @Deprecated
    public static final int NOT_SHARABLE = 2;
    @Deprecated
    public static final int MIXED = 3;

    private SharabilityQuery() {
    }

    @Deprecated
    public static int getSharability(File file) {
        File normFile;
        Parameters.notNull((CharSequence)"file", (Object)file);
        boolean asserts = false;
        if (!$assertionsDisabled) {
            asserts = true;
            if (!true) {
                throw new AssertionError();
            }
        }
        if (asserts && !Utilities.isMac() && !file.equals(normFile = FileUtil.normalizeFile((File)file))) {
            throw new IllegalArgumentException("Must pass a normalized file: " + file + " vs. " + normFile);
        }
        URI uri = null;
        for (Object sqi2 : implementations2.allInstances()) {
            Sharability x;
            if (uri == null) {
                uri = Utilities.toURI((File)file);
            }
            if ((x = sqi2.getSharability(uri)) == Sharability.UNKNOWN) continue;
            return x.ordinal();
        }
        for (Object sqi2 : implementations.allInstances()) {
            int x = sqi2.getSharability(file);
            if (x == 0) continue;
            return x;
        }
        return 0;
    }

    public static Sharability getSharability(URI uri) {
        URI normUri;
        Parameters.notNull((CharSequence)"uri", (Object)uri);
        boolean asserts = false;
        if (!$assertionsDisabled) {
            asserts = true;
            if (!true) {
                throw new AssertionError();
            }
        }
        if (asserts && !uri.equals(normUri = uri.normalize())) {
            throw new IllegalArgumentException("Must pass a normalized URI: " + uri + " vs. " + normUri);
        }
        for (SharabilityQueryImplementation2 sqi : implementations2.allInstances()) {
            Sharability x = sqi.getSharability(uri);
            if (x == Sharability.UNKNOWN) continue;
            return x;
        }
        if ("file".equals(uri.getScheme())) {
            File file = FileUtil.normalizeFile((File)Utilities.toFile((URI)uri));
            for (SharabilityQueryImplementation sqi2 : implementations.allInstances()) {
                int x = sqi2.getSharability(file);
                if (x == 0) continue;
                return Sharability.values()[x];
            }
        }
        return Sharability.UNKNOWN;
    }

    public static Sharability getSharability(FileObject fo) {
        return SharabilityQuery.getSharability(fo.toURI());
    }

    public static enum Sharability {
        UNKNOWN,
        SHARABLE,
        NOT_SHARABLE,
        MIXED;
        

        private Sharability() {
        }
    }

}

