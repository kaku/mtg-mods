/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Utilities
 */
package org.netbeans.api.queries;

import java.io.File;
import java.net.URI;
import java.util.Collection;
import org.netbeans.spi.queries.CollocationQueryImplementation;
import org.netbeans.spi.queries.CollocationQueryImplementation2;
import org.openide.filesystems.FileUtil;
import org.openide.util.Lookup;
import org.openide.util.Utilities;

public final class CollocationQuery {
    private static final Lookup.Result<CollocationQueryImplementation> implementations = Lookup.getDefault().lookupResult(CollocationQueryImplementation.class);
    private static final Lookup.Result<CollocationQueryImplementation2> implementations2 = Lookup.getDefault().lookupResult(CollocationQueryImplementation2.class);

    private CollocationQuery() {
    }

    @Deprecated
    public static boolean areCollocated(File file1, File file2) {
        if (!file1.equals(FileUtil.normalizeFile((File)file1))) {
            throw new IllegalArgumentException("Parameter file1 was not normalized. Was " + file1 + " instead of " + FileUtil.normalizeFile((File)file1));
        }
        if (!file2.equals(FileUtil.normalizeFile((File)file2))) {
            throw new IllegalArgumentException("Parameter file2 was not normalized. Was " + file2 + " instead of " + FileUtil.normalizeFile((File)file2));
        }
        URI uri1 = Utilities.toURI((File)file1);
        URI uri2 = Utilities.toURI((File)file2);
        for (Object cqi2 : implementations2.allInstances()) {
            if (!cqi2.areCollocated(uri1, uri2)) continue;
            return true;
        }
        for (Object cqi2 : implementations.allInstances()) {
            if (!cqi2.areCollocated(file1, file2)) continue;
            return true;
        }
        return false;
    }

    public static boolean areCollocated(URI file1, URI file2) {
        if (!file1.equals(file1.normalize())) {
            throw new IllegalArgumentException("Parameter file1 was not normalized. Was " + file1 + " instead of " + file1.normalize());
        }
        if (!file2.equals(file2.normalize())) {
            throw new IllegalArgumentException("Parameter file2 was not normalized. Was " + file2 + " instead of " + file2.normalize());
        }
        for (CollocationQueryImplementation2 cqi : implementations2.allInstances()) {
            if (!cqi.areCollocated(file1, file2)) continue;
            return true;
        }
        if ("file".equals(file1.getScheme()) && "file".equals(file2.getScheme())) {
            File f1 = FileUtil.normalizeFile((File)Utilities.toFile((URI)file1));
            File f2 = FileUtil.normalizeFile((File)Utilities.toFile((URI)file2));
            for (CollocationQueryImplementation cqi2 : implementations.allInstances()) {
                if (!cqi2.areCollocated(f1, f2)) continue;
                return true;
            }
        }
        return false;
    }

    @Deprecated
    public static File findRoot(File file) {
        if (!file.equals(FileUtil.normalizeFile((File)file))) {
            throw new IllegalArgumentException("Parameter file was not normalized. Was " + file + " instead of " + FileUtil.normalizeFile((File)file));
        }
        URI uri = Utilities.toURI((File)file);
        for (CollocationQueryImplementation2 cqi2 : implementations2.allInstances()) {
            URI root = cqi2.findRoot(uri);
            if (root == null) continue;
            return Utilities.toFile((URI)root);
        }
        for (CollocationQueryImplementation cqi : implementations.allInstances()) {
            File root = cqi.findRoot(file);
            if (root == null) continue;
            return root;
        }
        return null;
    }

    public static URI findRoot(URI file) {
        if (!file.equals(file.normalize())) {
            throw new IllegalArgumentException("Parameter file was not normalized. Was " + file + " instead of " + file.normalize());
        }
        for (CollocationQueryImplementation2 cqi : implementations2.allInstances()) {
            URI root = cqi.findRoot(file);
            if (root == null) continue;
            return root;
        }
        if ("file".equals(file.getScheme())) {
            File f = FileUtil.normalizeFile((File)Utilities.toFile((URI)file));
            for (CollocationQueryImplementation cqi2 : implementations.allInstances()) {
                File root = cqi2.findRoot(f);
                if (root == null) continue;
                return Utilities.toURI((File)root);
            }
        }
        return null;
    }
}

