/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.queries;

import java.io.File;
import org.netbeans.spi.queries.VisibilityQueryImplementation;

public interface VisibilityQueryImplementation2
extends VisibilityQueryImplementation {
    public boolean isVisible(File var1);
}

