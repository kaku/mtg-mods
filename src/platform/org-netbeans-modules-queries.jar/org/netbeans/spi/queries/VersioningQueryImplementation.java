/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.queries;

import java.net.URI;

public interface VersioningQueryImplementation {
    public boolean isManaged(URI var1);

    public String getRemoteLocation(URI var1);
}

