/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.queries;

import javax.swing.event.ChangeListener;
import org.openide.filesystems.FileObject;

public interface VisibilityQueryImplementation {
    public boolean isVisible(FileObject var1);

    public void addChangeListener(ChangeListener var1);

    public void removeChangeListener(ChangeListener var1);
}

