/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.queries;

import java.io.File;

@Deprecated
public interface SharabilityQueryImplementation {
    public int getSharability(File var1);
}

