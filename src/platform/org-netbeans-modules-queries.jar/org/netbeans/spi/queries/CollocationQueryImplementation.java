/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.queries;

import java.io.File;

@Deprecated
public interface CollocationQueryImplementation {
    public boolean areCollocated(File var1, File var2);

    public File findRoot(File var1);
}

