/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.queries;

import java.nio.charset.Charset;
import org.netbeans.modules.queries.UnknownEncoding;
import org.openide.filesystems.FileObject;

public abstract class FileEncodingQueryImplementation {
    public abstract Charset getEncoding(FileObject var1);

    protected static void throwUnknownEncoding() {
        throw new UnknownEncoding();
    }
}

