/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.queries;

import java.net.URI;

public interface CollocationQueryImplementation2 {
    public boolean areCollocated(URI var1, URI var2);

    public URI findRoot(URI var1);
}

