/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.queries;

import org.netbeans.api.queries.FileBuiltQuery;
import org.openide.filesystems.FileObject;

public interface FileBuiltQueryImplementation {
    public FileBuiltQuery.Status getStatus(FileObject var1);
}

