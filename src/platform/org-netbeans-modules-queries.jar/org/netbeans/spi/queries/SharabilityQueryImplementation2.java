/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.queries;

import java.net.URI;
import org.netbeans.api.queries.SharabilityQuery;

public interface SharabilityQueryImplementation2 {
    public SharabilityQuery.Sharability getSharability(URI var1);
}

