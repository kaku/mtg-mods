/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.queries;

import javax.swing.event.ChangeEvent;
import org.openide.filesystems.FileObject;

public final class VisibilityQueryChangeEvent
extends ChangeEvent {
    private final FileObject[] fileObjects;

    public VisibilityQueryChangeEvent(Object source, FileObject[] changedFileObjects) {
        super(source);
        this.fileObjects = changedFileObjects;
    }

    public FileObject[] getFileObjects() {
        return this.fileObjects;
    }
}

