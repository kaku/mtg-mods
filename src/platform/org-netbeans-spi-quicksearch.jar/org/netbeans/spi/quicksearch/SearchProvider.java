/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.quicksearch;

import org.netbeans.spi.quicksearch.SearchRequest;
import org.netbeans.spi.quicksearch.SearchResponse;

public interface SearchProvider {
    public void evaluate(SearchRequest var1, SearchResponse var2);
}

