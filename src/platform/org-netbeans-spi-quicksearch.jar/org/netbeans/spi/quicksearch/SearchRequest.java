/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.quicksearch;

import java.util.List;
import javax.swing.KeyStroke;
import org.netbeans.modules.quicksearch.Accessor;
import org.netbeans.spi.quicksearch.AccessorImpl;

public final class SearchRequest {
    private String text;
    private List<? extends KeyStroke> stroke;

    SearchRequest(String text, List<? extends KeyStroke> stroke) {
        this.text = text;
        this.stroke = stroke;
    }

    public String getText() {
        return this.text;
    }

    public List<? extends KeyStroke> getShortcut() {
        return this.stroke;
    }

    static {
        Accessor.DEFAULT = new AccessorImpl();
    }
}

