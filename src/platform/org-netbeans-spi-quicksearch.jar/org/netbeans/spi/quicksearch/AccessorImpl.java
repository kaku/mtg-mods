/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.quicksearch;

import java.util.List;
import javax.swing.KeyStroke;
import org.netbeans.modules.quicksearch.Accessor;
import org.netbeans.modules.quicksearch.CategoryResult;
import org.netbeans.spi.quicksearch.SearchRequest;
import org.netbeans.spi.quicksearch.SearchResponse;

class AccessorImpl
extends Accessor {
    AccessorImpl() {
    }

    @Override
    public SearchRequest createRequest(String text, List<? extends KeyStroke> stroke) {
        return new SearchRequest(text, stroke);
    }

    @Override
    public SearchResponse createResponse(CategoryResult catResult, SearchRequest sRequest) {
        return new SearchResponse(catResult, sRequest);
    }
}

