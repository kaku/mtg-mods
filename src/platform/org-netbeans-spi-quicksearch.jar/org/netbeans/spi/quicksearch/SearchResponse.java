/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckReturnValue
 */
package org.netbeans.spi.quicksearch;

import java.util.List;
import javax.swing.KeyStroke;
import org.netbeans.api.annotations.common.CheckReturnValue;
import org.netbeans.modules.quicksearch.CategoryResult;
import org.netbeans.modules.quicksearch.ResultsModel;
import org.netbeans.spi.quicksearch.SearchRequest;

public final class SearchResponse {
    private CategoryResult catResult;
    private SearchRequest sRequest;

    SearchResponse(CategoryResult catResult, SearchRequest sRequest) {
        this.catResult = catResult;
        this.sRequest = sRequest;
    }

    @CheckReturnValue
    public boolean addResult(Runnable action, String htmlDisplayName) {
        return this.addResult(action, htmlDisplayName, null, null);
    }

    @CheckReturnValue
    public boolean addResult(Runnable action, String htmlDisplayName, String displayHint, List<? extends KeyStroke> shortcut) {
        return this.catResult.addItem(new ResultsModel.ItemResult(this.catResult, this.sRequest, action, htmlDisplayName, shortcut, displayHint));
    }
}

