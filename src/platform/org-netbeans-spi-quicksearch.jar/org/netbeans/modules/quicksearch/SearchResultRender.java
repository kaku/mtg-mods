/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 *  org.openide.awt.HtmlRenderer$Renderer
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.quicksearch;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import org.netbeans.modules.quicksearch.CategoryResult;
import org.netbeans.modules.quicksearch.ProviderModel;
import org.netbeans.modules.quicksearch.QuickSearchComboBar;
import org.netbeans.modules.quicksearch.QuickSearchPopup;
import org.netbeans.modules.quicksearch.ResultsModel;
import org.openide.awt.HtmlRenderer;
import org.openide.util.Utilities;

class SearchResultRender
extends JLabel
implements ListCellRenderer {
    private QuickSearchPopup popup;
    private JLabel categoryLabel;
    private JPanel rendererComponent;
    private JLabel resultLabel;
    private JLabel shortcutLabel;
    private JPanel dividerLine;
    private JPanel itemPanel;
    private JPanel itemLinePanel;
    private JLabel cutLabel;

    public SearchResultRender(QuickSearchPopup popup) {
        this.popup = popup;
        this.configRenderer();
    }

    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        if (!(value instanceof ResultsModel.ItemResult)) {
            return new JLabel();
        }
        ResultsModel.ItemResult ir = (ResultsModel.ItemResult)value;
        List<? extends KeyStroke> shortcut = ir.getShortcut();
        this.resultLabel.setText(ir.getDisplayName());
        if (shortcut != null && shortcut.size() > 0 && shortcut.get(0) != null) {
            this.shortcutLabel.setText(SearchResultRender.getKeyStrokeAsText(shortcut.get(0)));
            this.itemPanel.add((Component)this.shortcutLabel, "East");
        } else {
            this.itemPanel.remove(this.shortcutLabel);
        }
        CategoryResult cr = ir.getCategory();
        if (cr.isFirstItem(ir)) {
            this.categoryLabel.setText(cr.getCategory().getDisplayName());
            if (index > 0) {
                this.rendererComponent.add((Component)this.dividerLine, "North");
            }
        } else {
            this.categoryLabel.setText("");
            this.rendererComponent.remove(this.dividerLine);
        }
        this.categoryLabel.setPreferredSize(new Dimension(this.popup.getCategoryWidth(), this.categoryLabel.getPreferredSize().height));
        this.itemPanel.setPreferredSize(new Dimension(this.popup.getResultWidth(), this.itemPanel.getPreferredSize().height));
        if (this.isCut(ir.getDisplayName(), this.resultLabel.getWidth())) {
            this.itemLinePanel.add((Component)this.cutLabel, "East");
        } else {
            this.itemLinePanel.remove(this.cutLabel);
        }
        if (isSelected) {
            this.resultLabel.setBackground(list.getSelectionBackground());
            this.resultLabel.setForeground(list.getSelectionForeground());
            this.shortcutLabel.setBackground(list.getSelectionBackground());
            this.shortcutLabel.setForeground(list.getSelectionForeground());
            this.cutLabel.setBackground(list.getSelectionBackground());
            this.cutLabel.setForeground(list.getSelectionForeground());
        } else {
            this.resultLabel.setBackground(QuickSearchComboBar.getResultBackground());
            this.resultLabel.setForeground(list.getForeground());
            this.shortcutLabel.setBackground(QuickSearchComboBar.getResultBackground());
            this.shortcutLabel.setForeground(list.getForeground());
            this.cutLabel.setBackground(QuickSearchComboBar.getResultBackground());
            this.cutLabel.setForeground(list.getForeground());
        }
        if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
            this.rendererComponent.setOpaque(false);
        }
        return this.rendererComponent;
    }

    private boolean isCut(String text, int realWidth) {
        double width = HtmlRenderer.renderHTML((String)text, (Graphics)this.resultLabel.getGraphics(), (int)0, (int)10, (int)Integer.MAX_VALUE, (int)20, (Font)this.resultLabel.getFont(), (Color)Color.BLACK, (int)0, (boolean)false);
        return (int)width > realWidth - 4;
    }

    private void configRenderer() {
        this.categoryLabel = new JLabel();
        this.categoryLabel.setFont(this.categoryLabel.getFont().deriveFont(1));
        this.categoryLabel.setBorder(new EmptyBorder(0, 5, 0, 0));
        this.categoryLabel.setForeground(QuickSearchComboBar.getCategoryTextColor());
        this.resultLabel = HtmlRenderer.createLabel();
        ((HtmlRenderer.Renderer)this.resultLabel).setHtml(true);
        ((HtmlRenderer.Renderer)this.resultLabel).setRenderStyle(0);
        this.resultLabel.setOpaque(true);
        this.resultLabel.setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 4));
        this.shortcutLabel = new JLabel();
        this.shortcutLabel.setOpaque(true);
        this.shortcutLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 4));
        this.cutLabel = new JLabel("...");
        this.cutLabel.setOpaque(true);
        this.cutLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 4));
        this.itemLinePanel = new JPanel();
        this.itemLinePanel.setBackground(QuickSearchComboBar.getResultBackground());
        this.itemLinePanel.setLayout(new BorderLayout());
        this.itemLinePanel.add((Component)this.resultLabel, "Center");
        this.itemPanel = new JPanel();
        this.itemPanel.setBackground(QuickSearchComboBar.getResultBackground());
        this.itemPanel.setBorder(BorderFactory.createEmptyBorder(2, 4, 2, 3));
        this.itemPanel.setLayout(new BorderLayout());
        this.itemPanel.add((Component)this.itemLinePanel, "Center");
        this.dividerLine = new JPanel();
        this.dividerLine.setBackground(QuickSearchComboBar.getPopupBorderColor());
        this.dividerLine.setPreferredSize(new Dimension(this.dividerLine.getPreferredSize().width, 1));
        this.rendererComponent = new JPanel();
        this.rendererComponent.setLayout(new BorderLayout());
        this.rendererComponent.add((Component)this.itemPanel, "Center");
        this.rendererComponent.add((Component)this.categoryLabel, "West");
    }

    static String getKeyStrokeAsText(KeyStroke keyStroke) {
        if (keyStroke == null) {
            return "";
        }
        int modifiers = keyStroke.getModifiers();
        StringBuffer sb = new StringBuffer();
        if ((modifiers & 128) > 0) {
            sb.append("Ctrl+");
        }
        if ((modifiers & 512) > 0) {
            sb.append("Alt+");
        }
        if ((modifiers & 64) > 0) {
            sb.append("Shift+");
        }
        if ((modifiers & 256) > 0) {
            if (Utilities.isMac()) {
                sb.append("\u2318+");
            } else if (SearchResultRender.isSolaris()) {
                sb.append("\u25c6+");
            } else {
                sb.append("Meta+");
            }
        }
        if (keyStroke.getKeyCode() != 16 && keyStroke.getKeyCode() != 17 && keyStroke.getKeyCode() != 157 && keyStroke.getKeyCode() != 18 && keyStroke.getKeyCode() != 65406) {
            sb.append(Utilities.keyToString((KeyStroke)KeyStroke.getKeyStroke(keyStroke.getKeyCode(), 0)));
        }
        return sb.toString();
    }

    private static boolean isSolaris() {
        String osName = System.getProperty("os.name");
        return osName != null && osName.startsWith("SunOS");
    }
}

