/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.modules.quicksearch;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.CharConversionException;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.swing.AbstractListModel;
import javax.swing.KeyStroke;
import javax.swing.Timer;
import org.netbeans.modules.quicksearch.CategoryResult;
import org.netbeans.spi.quicksearch.SearchRequest;
import org.openide.xml.XMLUtil;

public final class ResultsModel
extends AbstractListModel
implements ActionListener {
    private static ResultsModel instance;
    private List<? extends CategoryResult> results;
    private Timer fireTimer;
    static final int COALESCE_TIME = 200;

    private ResultsModel() {
    }

    public static ResultsModel getInstance() {
        if (instance == null) {
            instance = new ResultsModel();
        }
        return instance;
    }

    public void setContent(List<? extends CategoryResult> categories) {
        List<? extends CategoryResult> oldRes = this.results;
        this.results = categories;
        if (oldRes != null) {
            for (CategoryResult cr : oldRes) {
                cr.setObsolete(true);
            }
        }
        this.maybeFireChanges();
    }

    public List<? extends CategoryResult> getContent() {
        return this.results;
    }

    @Override
    public int getSize() {
        if (this.results == null) {
            return 0;
        }
        int size = 0;
        for (CategoryResult cr : this.results) {
            size += cr.getItems().size();
        }
        return size;
    }

    @Override
    public Object getElementAt(int index) {
        if (this.results == null) {
            return null;
        }
        int catIndex = index;
        int catSize = 0;
        List<ItemResult> catItems = null;
        for (CategoryResult cr : this.results) {
            catItems = cr.getItems();
            catSize = catItems.size();
            if (catIndex < catSize) {
                return catIndex >= 0 ? catItems.get(catIndex) : null;
            }
            catIndex -= catSize;
        }
        return null;
    }

    void categoryChanged(CategoryResult cr) {
        if (this.results != null && this.results.contains(cr)) {
            this.maybeFireChanges();
        }
    }

    private void maybeFireChanges() {
        if (this.fireTimer == null) {
            this.fireTimer = new Timer(200, this);
        }
        if (!this.fireTimer.isRunning()) {
            this.fireTimer.start();
        } else {
            this.fireTimer.restart();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.fireTimer.stop();
        this.fireContentsChanged(this, 0, this.getSize());
    }

    public static final class ItemResult {
        private static final String HTML = "<html>";
        private CategoryResult category;
        private Runnable action;
        private String displayName;
        private List<? extends KeyStroke> shortcut;
        private String displayHint;
        private Date date;

        public ItemResult(CategoryResult category, SearchRequest sRequest, Runnable action, String displayName) {
            this(category, sRequest, action, displayName, null, null);
        }

        public ItemResult(CategoryResult category, Runnable action, String displayName, Date date) {
            this(category, null, action, displayName, null, null);
            this.date = date;
        }

        public ItemResult(CategoryResult category, SearchRequest sRequest, Runnable action, String displayName, List<? extends KeyStroke> shortcut, String displayHint) {
            this.category = category;
            this.action = action;
            this.displayName = sRequest != null ? this.highlightSubstring(displayName, sRequest) : displayName;
            this.shortcut = shortcut;
            this.displayHint = displayHint;
        }

        public Runnable getAction() {
            return this.action;
        }

        public Date getDate() {
            return this.date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public String getDisplayName() {
            return this.displayName;
        }

        public String getDisplayHint() {
            return this.displayHint;
        }

        public List<? extends KeyStroke> getShortcut() {
            return this.shortcut;
        }

        public CategoryResult getCategory() {
            return this.category;
        }

        private String highlightSubstring(String text, SearchRequest sRequest) {
            if (text.startsWith("<html>")) {
                return text;
            }
            String searchedText = sRequest.getText();
            int index = text.toLowerCase(Locale.ENGLISH).indexOf(searchedText.toLowerCase(Locale.ENGLISH));
            if (index == -1) {
                return "<html>" + ItemResult.safeEscape(text);
            }
            int endIndex = index + searchedText.length();
            StringBuilder sb = new StringBuilder("<html>");
            if (index > 0) {
                sb.append(ItemResult.safeEscape(text.substring(0, index)));
            }
            sb.append("<b>");
            sb.append(ItemResult.safeEscape(text.substring(index, endIndex)));
            sb.append("</b>");
            if (endIndex < text.length()) {
                sb.append(ItemResult.safeEscape(text.substring(endIndex, text.length())));
            }
            return sb.toString();
        }

        private static String safeEscape(String raw) {
            try {
                return XMLUtil.toElementContent((String)raw);
            }
            catch (CharConversionException x) {
                return raw;
            }
        }
    }

}

