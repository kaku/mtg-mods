/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.quicksearch;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String MSG_INVALID_SEARCH_TEST() {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_INVALID_SEARCH_TEST");
    }

    private void Bundle() {
    }
}

