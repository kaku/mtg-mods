/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$Status
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.quicksearch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.spi.quicksearch.SearchProvider;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.util.lookup.Lookups;

public final class ProviderModel {
    private static final String SEARCH_PROVIDERS_FOLDER = "/QuickSearch";
    private static final String COMMAND_PREFIX = "command";
    private static ProviderModel instance;
    private List<Category> categories;
    private LinkedHashSet<String> knownCommands;

    private ProviderModel() {
    }

    public static ProviderModel getInstance() {
        if (instance == null) {
            instance = new ProviderModel();
        }
        return instance;
    }

    public List<Category> getCategories() {
        if (this.categories == null) {
            this.categories = ProviderModel.loadCategories();
        }
        return this.categories;
    }

    public boolean isKnownCommand(String command) {
        if (this.knownCommands == null) {
            this.knownCommands = new LinkedHashSet();
            for (Category cat : this.getCategories()) {
                this.knownCommands.add(cat.getCommandPrefix());
            }
        }
        return this.knownCommands.contains(command);
    }

    private static List<Category> loadCategories() {
        FileObject[] categoryFOs = FileUtil.getConfigFile((String)"/QuickSearch").getChildren();
        List sortedCats = FileUtil.getOrder(Arrays.asList(categoryFOs), (boolean)false);
        ArrayList<Category> categories = new ArrayList<Category>(sortedCats.size());
        for (FileObject curFO : sortedCats) {
            String displayName = null;
            try {
                displayName = curFO.getFileSystem().getStatus().annotateName(curFO.getNameExt(), Collections.singleton(curFO));
            }
            catch (FileStateInvalidException ex) {
                Logger.getLogger(ProviderModel.class.getName()).log(Level.WARNING, "Obtaining display name for " + (Object)curFO + " failed.", (Throwable)ex);
            }
            String commandPrefix = null;
            Object cpAttr = curFO.getAttribute("command");
            if (cpAttr instanceof String) {
                commandPrefix = (String)cpAttr;
            }
            categories.add(new Category(curFO, displayName, commandPrefix));
        }
        return categories;
    }

    public static class Category {
        private FileObject fo;
        private String displayName;
        private String commandPrefix;
        private List<SearchProvider> providers;

        public Category(FileObject fo, String displayName, String commandPrefix) {
            this.fo = fo;
            this.displayName = displayName;
            this.commandPrefix = commandPrefix;
        }

        public List<SearchProvider> getProviders() {
            if (this.providers == null) {
                Collection catProviders = Lookups.forPath((String)this.fo.getPath()).lookupAll(SearchProvider.class);
                this.providers = new ArrayList<SearchProvider>(catProviders);
            }
            return this.providers;
        }

        public String getName() {
            return this.fo.getNameExt();
        }

        public String getDisplayName() {
            return this.displayName;
        }

        public String getCommandPrefix() {
            return this.commandPrefix;
        }
    }

}

