/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.quicksearch;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.Timer;
import javax.swing.border.Border;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.quicksearch.AbstractQuickSearchComboBar;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class QuickSearchComboBar
extends AbstractQuickSearchComboBar {
    private static final String ICON_FIND = "org/openide/awt/resources/quicksearch/findMenu.png";
    private static final String ICON_PROGRESS_0 = "org/openide/awt/resources/quicksearch/progress_0.png";
    private static final String ICON_PROGRESS_1 = "org/openide/awt/resources/quicksearch/progress_1.png";
    private static final String ICON_PROGRESS_2 = "org/openide/awt/resources/quicksearch/progress_2.png";
    private static final String ICON_PROGRESS_3 = "org/openide/awt/resources/quicksearch/progress_3.png";
    private static final String ICON_PROGRESS_4 = "org/openide/awt/resources/quicksearch/progress_4.png";
    private static final String ICON_PROGRESS_5 = "org/openide/awt/resources/quicksearch/progress_5.png";
    private static final String ICON_PROGRESS_6 = "org/openide/awt/resources/quicksearch/progress_6.png";
    private static final String ICON_PROGRESS_7 = "org/openide/awt/resources/quicksearch/progress_7.png";
    private static final String[] ICON_PROGRESS = new String[]{"org/openide/awt/resources/quicksearch/progress_0.png", "org/openide/awt/resources/quicksearch/progress_1.png", "org/openide/awt/resources/quicksearch/progress_2.png", "org/openide/awt/resources/quicksearch/progress_3.png", "org/openide/awt/resources/quicksearch/progress_4.png", "org/openide/awt/resources/quicksearch/progress_5.png", "org/openide/awt/resources/quicksearch/progress_6.png", "org/openide/awt/resources/quicksearch/progress_7.png"};
    private final ImageIcon findIcon = ImageUtilities.loadImageIcon((String)"org/openide/awt/resources/quicksearch/findMenu.png", (boolean)false);
    private final Timer animationTimer;
    private JLabel jLabel2;
    private JPanel jPanel1;
    private JScrollPane jScrollPane1;
    private JSeparator jSeparator1;

    public QuickSearchComboBar(KeyStroke ks) {
        super(ks);
        this.animationTimer = new Timer(100, new ActionListener(){
            ImageIcon[] icons;
            int index;

            @Override
            public void actionPerformed(ActionEvent e) {
                if (this.icons == null) {
                    this.icons = new ImageIcon[8];
                    for (int i = 0; i < 8; ++i) {
                        this.icons[i] = ImageUtilities.loadImageIcon((String)ICON_PROGRESS[i], (boolean)false);
                    }
                }
                QuickSearchComboBar.this.jLabel2.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 6));
                QuickSearchComboBar.this.jLabel2.setIcon(this.icons[this.index]);
                QuickSearchComboBar.this.jLabel2.repaint();
                this.index = (this.index + 1) % 8;
            }
        });
        this.initComponents();
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.jLabel2 = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this.jSeparator1 = new JSeparator();
        this.setLayout(new GridBagLayout());
        this.jPanel1.setBorder(BorderFactory.createLineBorder(QuickSearchComboBar.getComboBorderColor()));
        this.jPanel1.setName("jPanel1");
        this.jPanel1.setLayout(new GridBagLayout());
        this.jLabel2.setIcon(this.findIcon);
        this.jLabel2.setToolTipText(NbBundle.getMessage(QuickSearchComboBar.class, (String)"QuickSearchComboBar.jLabel2.toolTipText"));
        this.jLabel2.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this.jLabel2.setName("jLabel2");
        this.jLabel2.addMouseListener(new MouseAdapter(){

            @Override
            public void mousePressed(MouseEvent evt) {
                QuickSearchComboBar.this.jLabel2MousePressed(evt);
            }
        });
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(1, 2, 1, 2);
        this.jPanel1.add((Component)this.jLabel2, gridBagConstraints);
        this.jScrollPane1.setBorder(null);
        this.jScrollPane1.setHorizontalScrollBarPolicy(31);
        this.jScrollPane1.setVerticalScrollBarPolicy(21);
        this.jScrollPane1.setViewportBorder(null);
        this.jScrollPane1.setMinimumSize(new Dimension(2, 18));
        this.jScrollPane1.setName("jScrollPane1");
        this.jScrollPane1.setViewportView(this.command);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 0, 2);
        this.jPanel1.add((Component)this.jScrollPane1, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.jPanel1, gridBagConstraints);
    }

    private void jLabel2MousePressed(MouseEvent evt) {
        this.maybeShowPopup(evt);
    }

    @Override
    protected JTextComponent createCommandField() {
        DynamicWidthTA res = new DynamicWidthTA();
        res.setRows(1);
        res.setBorder(BorderFactory.createEmptyBorder(1, 4, 1, 1));
        for (InputMap curIm = res.getInputMap((int)0); curIm != null; curIm = curIm.getParent()) {
            curIm.remove(KeyStroke.getKeyStroke(79, 192));
        }
        return res;
    }

    @Override
    protected JComponent getInnerComponent() {
        return this.jPanel1;
    }

    void startProgressAnimation() {
        if (this.animationTimer != null && !this.animationTimer.isRunning()) {
            this.animationTimer.start();
        }
    }

    void stopProgressAnimation() {
        if (this.animationTimer != null && this.animationTimer.isRunning()) {
            this.animationTimer.stop();
            this.jLabel2.setIcon(this.findIcon);
            this.jLabel2.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        }
    }

    private final class DynamicWidthTA
    extends JTextArea {
        private Dimension prefWidth;

        private DynamicWidthTA() {
        }

        @Override
        public Dimension getPreferredSize() {
            if (this.prefWidth == null) {
                Dimension orig = super.getPreferredSize();
                this.prefWidth = new Dimension(QuickSearchComboBar.this.computePrefWidth(), orig.height);
            }
            return this.prefWidth;
        }

        @Override
        public Dimension getMinimumSize() {
            return this.getPreferredSize();
        }
    }

}

