/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.quicksearch.recent;

import java.util.List;
import javax.swing.KeyStroke;
import org.netbeans.modules.quicksearch.ResultsModel;
import org.netbeans.modules.quicksearch.recent.Bundle;
import org.netbeans.modules.quicksearch.recent.RecentSearches;
import org.netbeans.spi.quicksearch.SearchProvider;
import org.netbeans.spi.quicksearch.SearchRequest;
import org.netbeans.spi.quicksearch.SearchResponse;

public class RecentProvider
implements SearchProvider {
    @Override
    public void evaluate(SearchRequest request, SearchResponse response) {
        boolean atLeastOne = false;
        boolean limitReached = false;
        for (ResultsModel.ItemResult itemR : RecentSearches.getDefault().getSearches()) {
            if (itemR.getDisplayName().toLowerCase().indexOf(request.getText().toLowerCase()) == -1) continue;
            if (!response.addResult(itemR.getAction(), itemR.getDisplayName(), itemR.getDisplayHint(), itemR.getShortcut())) {
                limitReached = true;
                break;
            }
            atLeastOne = true;
        }
        if (atLeastOne && !limitReached && request.getText().isEmpty()) {
            this.addClearAction(response);
        }
    }

    private void addClearAction(SearchResponse response) {
        boolean add = response.addResult(new Runnable(){

            @Override
            public void run() {
                RecentSearches.getDefault().clear();
            }
        }, "<html><i>" + Bundle.RecentSearches_clear() + "</i></html>");
    }

}

