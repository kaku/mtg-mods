/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.util.Cancellable
 *  org.openide.util.Exceptions
 *  org.openide.util.NbPreferences
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 */
package org.netbeans.modules.quicksearch.recent;

import java.awt.EventQueue;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.modules.quicksearch.CategoryResult;
import org.netbeans.modules.quicksearch.CommandEvaluator;
import org.netbeans.modules.quicksearch.ProviderModel;
import org.netbeans.modules.quicksearch.ResultsModel;
import org.netbeans.modules.quicksearch.recent.Bundle;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Cancellable;
import org.openide.util.Exceptions;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;

public class RecentSearches {
    private static final int MAX_ITEMS = 5;
    private static final long FIVE_DAYS = 432000000;
    private LinkedList<ResultsModel.ItemResult> recent = new LinkedList();
    private static RecentSearches instance;
    private static final char dateSep = ':';
    private static final Pattern RECENT_PREFS_PATTERN;
    private static RequestProcessor RP;
    private static final String RECENT_SEARCHES = "recentSearches";

    private RecentSearches() {
        this.readRecentFromPrefs();
    }

    public static RecentSearches getDefault() {
        if (instance == null) {
            instance = new RecentSearches();
        }
        return instance;
    }

    public synchronized void add(ResultsModel.ItemResult result) {
        Date now = new GregorianCalendar().getTime();
        for (ResultsModel.ItemResult ir : this.recent) {
            if (!this.stripHTMLnames(ir.getDisplayName()).equals(this.stripHTMLnames(result.getDisplayName()))) continue;
            ir.setDate(now);
            return;
        }
        if ("SearchSetup".equals(result.getAction().getClass().getSimpleName())) {
            return;
        }
        if (this.recent.size() >= 5) {
            this.recent.removeLast();
        }
        result.setDate(now);
        this.recent.addFirst(result);
        this.storeRecentToPrefs();
    }

    public synchronized List<ResultsModel.ItemResult> getSearches() {
        LinkedList<ResultsModel.ItemResult> fiveDayList = new LinkedList<ResultsModel.ItemResult>();
        for (ResultsModel.ItemResult ir : this.recent) {
            if (new GregorianCalendar().getTime().getTime() - ir.getDate().getTime() >= 432000000) continue;
            fiveDayList.add(ir);
        }
        return fiveDayList;
    }

    public synchronized void clear() {
        this.recent.clear();
        RP.post(new Runnable(){

            @Override
            public void run() {
                RecentSearches.this.storeRecentToPrefs();
            }
        });
    }

    private Preferences prefs() {
        return NbPreferences.forModule(RecentSearches.class);
    }

    private void storeRecentToPrefs() {
        Iterator<ResultsModel.ItemResult> it = this.recent.iterator();
        for (int i = 0; i < 5; ++i) {
            if (it.hasNext()) {
                ResultsModel.ItemResult td = it.next();
                CategoryResult cr = td.getCategory();
                ProviderModel.Category category = cr == null ? null : cr.getCategory();
                String categoryName = category == null ? null : this.stripHTMLnames(category.getDisplayName());
                this.prefs().put("recentSearches" + i, this.stripHTMLnames(td.getDisplayName()) + ':' + td.getDate().getTime() + (categoryName == null ? "" : new StringBuilder().append(':').append(categoryName).toString()));
                continue;
            }
            this.prefs().put("recentSearches" + i, "");
        }
    }

    private void readRecentFromPrefs() {
        for (int i = 0; i < 5; ++i) {
            String item = this.prefs().get("recentSearches" + i, "");
            Matcher m = RECENT_PREFS_PATTERN.matcher(item);
            if (!m.find()) continue;
            try {
                String name = m.group(1);
                long time = Long.parseLong(m.group(2));
                String categ = m.group(3);
                ResultsModel.ItemResult incomplete = new ResultsModel.ItemResult(null, new FakeAction(name, categ), name, new Date(time));
                this.recent.add(incomplete);
                continue;
            }
            catch (NumberFormatException nfe) {
                Logger l = Logger.getLogger(RecentSearches.class.getName());
                l.log(Level.INFO, "Failed to read recent searches", item);
            }
        }
    }

    private String stripHTMLnames(String s) {
        return this.translateHTMLEntities(s.replaceAll("<.*?>", "")).trim();
    }

    private String translateHTMLEntities(String s) {
        return s.replaceAll("\\&amp;", "&");
    }

    static {
        RECENT_PREFS_PATTERN = Pattern.compile("^(.*):(\\d{8,})(?::(.*))?$");
        RP = new RequestProcessor(RecentSearches.class.getName(), 2);
    }

    public final class FakeAction
    implements Runnable {
        private String name;
        private String category;
        private Runnable action;

        private FakeAction(String name, String category) {
            this.name = name;
            this.category = category;
        }

        @Override
        public void run() {
            if (this.action == null || this.action instanceof FakeAction) {
                RP.post(new Runnable(){

                    @Override
                    public void run() {
                        FakeAction.this.findAndRunAction();
                    }
                });
            } else {
                this.action.run();
            }
        }

        private void findAndRunAction() {
            final AtomicBoolean cancelled = new AtomicBoolean(false);
            ProgressHandle handle = ProgressHandleFactory.createHandle((String)Bundle.LBL_SearchingRecentResult(), (Cancellable)new Cancellable(){

                public boolean cancel() {
                    cancelled.set(true);
                    return true;
                }
            });
            handle.start();
            ResultsModel model = ResultsModel.getInstance();
            Task evaluate = CommandEvaluator.evaluate(this.stripHTMLandPackageNames(this.name), model);
            RP.post((Runnable)evaluate);
            int tries = 0;
            boolean found = false;
            while (tries++ < 30 && !cancelled.get()) {
                if (this.checkActionWasFound(model, true)) {
                    found = true;
                    break;
                }
                if (!evaluate.isFinished()) continue;
                found = this.checkActionWasFound(model, false);
                break;
            }
            handle.finish();
            if (!found && !cancelled.get()) {
                NotifyDescriptor.Message nd = new NotifyDescriptor.Message((Object)Bundle.MSG_RecentResultNotFound(), 1);
                DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)nd);
            }
        }

        private boolean checkActionWasFound(ResultsModel model, boolean wait) {
            if (wait) {
                try {
                    Thread.sleep(1000);
                }
                catch (InterruptedException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            int rSize = model.getSize();
            for (int j = 0; j < rSize; ++j) {
                ResultsModel.ItemResult res = (ResultsModel.ItemResult)model.getElementAt(j);
                if (!this.nameMatches(res) || !this.categoryMatches(res)) continue;
                this.action = res.getAction();
                if (this.action instanceof FakeAction) continue;
                EventQueue.invokeLater(this.action);
                return true;
            }
            return false;
        }

        private boolean nameMatches(ResultsModel.ItemResult res) {
            return RecentSearches.this.stripHTMLnames(res.getDisplayName()).equals(RecentSearches.this.stripHTMLnames(this.name));
        }

        private boolean categoryMatches(ResultsModel.ItemResult res) {
            if (this.category == null) {
                return true;
            }
            if (res.getCategory() == null || res.getCategory().getCategory() == null) {
                return false;
            }
            return RecentSearches.this.stripHTMLnames(res.getCategory().getCategory().getDisplayName()).equals(RecentSearches.this.stripHTMLnames(this.category));
        }

        private String stripHTMLandPackageNames(String s) {
            s = RecentSearches.this.stripHTMLnames(s);
            return s.replaceAll("\\(.*\\)", "").trim();
        }

    }

}

