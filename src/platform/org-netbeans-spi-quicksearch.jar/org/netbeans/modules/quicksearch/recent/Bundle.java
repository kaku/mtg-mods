/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.quicksearch.recent;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String LBL_SearchingRecentResult() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_SearchingRecentResult");
    }

    static String MSG_RecentResultNotFound() {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_RecentResultNotFound");
    }

    static String RecentSearches_clear() {
        return NbBundle.getMessage(Bundle.class, (String)"RecentSearches.clear");
    }

    private void Bundle() {
    }
}

