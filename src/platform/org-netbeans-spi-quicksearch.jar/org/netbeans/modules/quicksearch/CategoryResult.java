/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.quicksearch;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.SwingUtilities;
import org.netbeans.modules.quicksearch.ProviderModel;
import org.netbeans.modules.quicksearch.ResultsModel;
import org.netbeans.spi.quicksearch.SearchRequest;
import org.openide.util.NbBundle;

public final class CategoryResult
implements Runnable {
    static final int MAX_RESULTS = 7;
    static final int ALL_MAX_RESULTS = 30;
    private final boolean allResults;
    private final Object LOCK = new Object();
    private final ProviderModel.Category category;
    private final List<ResultsModel.ItemResult> items;
    private boolean obsolete;
    private int previousSize;
    private boolean moreResults = false;

    public CategoryResult(ProviderModel.Category category, boolean allResults) {
        this.category = category;
        this.allResults = allResults;
        this.items = new ArrayList<ResultsModel.ItemResult>(allResults ? 30 : 7);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean addItem(ResultsModel.ItemResult item) {
        Object object = this.LOCK;
        synchronized (object) {
            if (this.obsolete) {
                return false;
            }
            if (this.items.size() >= (this.allResults ? 30 : 7)) {
                if (!this.allResults) {
                    this.moreResults = true;
                }
                return false;
            }
            this.items.add(item);
        }
        if (EventQueue.isDispatchThread()) {
            this.run();
        } else {
            SwingUtilities.invokeLater(this);
        }
        return true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public List<ResultsModel.ItemResult> getItems() {
        ArrayList<ResultsModel.ItemResult> rItems = null;
        Object object = this.LOCK;
        synchronized (object) {
            rItems = new ArrayList<ResultsModel.ItemResult>(this.items);
            if (this.moreResults) {
                rItems.add(new ResultsModel.ItemResult(this, null, this, NbBundle.getMessage(this.getClass(), (String)"LBL_MoreResults")));
            }
        }
        return rItems;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isFirstItem(ResultsModel.ItemResult ir) {
        Object object = this.LOCK;
        synchronized (object) {
            if (this.items.size() > 0 && this.items.get(0).equals(ir)) {
                return true;
            }
        }
        return false;
    }

    public ProviderModel.Category getCategory() {
        return this.category;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setObsolete(boolean obsolete) {
        Object object = this.LOCK;
        synchronized (object) {
            this.obsolete = obsolete;
        }
    }

    public boolean isObsolete() {
        return this.obsolete;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void run() {
        int curSize = 0;
        boolean shouldNotify = false;
        Object object = this.LOCK;
        synchronized (object) {
            curSize = this.items.size();
            shouldNotify = !this.obsolete && this.items.size() <= (this.allResults ? 30 : 7);
        }
        if (!shouldNotify) {
            return;
        }
        if (curSize > this.previousSize) {
            this.previousSize = curSize;
            ResultsModel.getInstance().categoryChanged(this);
        }
    }
}

