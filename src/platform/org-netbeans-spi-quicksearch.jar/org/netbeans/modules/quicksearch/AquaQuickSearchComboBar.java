/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.quicksearch;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.quicksearch.AbstractQuickSearchComboBar;

public class AquaQuickSearchComboBar
extends AbstractQuickSearchComboBar {
    public AquaQuickSearchComboBar(KeyStroke ks) {
        super(ks);
        this.setLayout(new BorderLayout());
        this.add((Component)this.command, "Center");
    }

    @Override
    protected JTextComponent createCommandField() {
        DynamicWidthTF res = new DynamicWidthTF();
        final JPopupMenu dummy = new JPopupMenu();
        dummy.addPopupMenuListener(new PopupMenuListener(){

            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        dummy.setVisible(false);
                    }
                });
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {
            }

        });
        res.putClientProperty("JTextField.variant", "search");
        res.putClientProperty("JTextField.Search.FindPopup", dummy);
        res.putClientProperty("JTextField.Search.FindAction", new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                AquaQuickSearchComboBar.this.maybeShowPopup(null);
            }
        });
        return res;
    }

    @Override
    protected JComponent getInnerComponent() {
        return this.command;
    }

    private final class DynamicWidthTF
    extends JTextField {
        private Dimension prefWidth;

        private DynamicWidthTF() {
        }

        @Override
        public Dimension getPreferredSize() {
            if (this.prefWidth == null) {
                Dimension orig = super.getPreferredSize();
                this.prefWidth = new Dimension(AquaQuickSearchComboBar.this.computePrefWidth(), orig.height);
            }
            return this.prefWidth;
        }

        @Override
        public Dimension getMinimumSize() {
            return this.getPreferredSize();
        }
    }

}

