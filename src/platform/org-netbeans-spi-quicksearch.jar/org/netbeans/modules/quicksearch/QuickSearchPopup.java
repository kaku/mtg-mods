/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 *  org.openide.util.TaskListener
 */
package org.netbeans.modules.quicksearch;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.quicksearch.AbstractQuickSearchComboBar;
import org.netbeans.modules.quicksearch.CategoryResult;
import org.netbeans.modules.quicksearch.CommandEvaluator;
import org.netbeans.modules.quicksearch.ProviderModel;
import org.netbeans.modules.quicksearch.QuickSearchComboBar;
import org.netbeans.modules.quicksearch.ResultsModel;
import org.netbeans.modules.quicksearch.SearchResultRender;
import org.netbeans.modules.quicksearch.recent.RecentSearches;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.TaskListener;

public class QuickSearchPopup
extends JPanel
implements ListDataListener,
ActionListener,
TaskListener,
Runnable {
    private static final String CUSTOM_WIDTH = "customWidth";
    private static final int RESIZE_AREA_WIDTH = 5;
    private AbstractQuickSearchComboBar comboBar;
    private ResultsModel rModel;
    private Rectangle popupBounds = new Rectangle();
    private Timer updateTimer;
    private static final int COALESCE_TIME = 300;
    private String searchedText;
    private int catWidth;
    private int resultWidth;
    private int defaultResultWidth = -1;
    private int customWidth = -1;
    private int longestText = -1;
    private boolean canResize = false;
    private Task evalTask;
    private Task saveTask;
    private static final RequestProcessor RP = new RequestProcessor(QuickSearchPopup.class);
    private static final RequestProcessor evaluatorRP = new RequestProcessor(QuickSearchPopup.class + ".evaluator");
    private static final Logger LOG = Logger.getLogger(QuickSearchPopup.class.getName());
    private JLabel hintLabel;
    private JSeparator hintSep;
    private JList jList1;
    private JScrollPane jScrollPane1;
    private JLabel noResultsLabel;
    private JLabel searchingLabel;
    private JSeparator searchingSep;
    private JPanel statusPanel;
    private boolean explicitlyInvoked = false;

    public QuickSearchPopup(AbstractQuickSearchComboBar comboBar) {
        this.comboBar = comboBar;
        this.initComponents();
        this.loadSettings();
        this.makeResizable();
        this.rModel = ResultsModel.getInstance();
        this.jList1.setModel(this.rModel);
        this.jList1.setCellRenderer(new SearchResultRender(this));
        this.rModel.addListDataListener(this);
        if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
            this.jList1.setBackground(QuickSearchComboBar.getResultBackground());
        }
        this.updateStatusPanel(this.evalTask != null);
        this.setVisible(false);
    }

    void invoke() {
        ResultsModel.ItemResult result = (ResultsModel.ItemResult)this.jList1.getModel().getElementAt(this.jList1.getSelectedIndex());
        if (result != null) {
            RecentSearches.getDefault().add(result);
            result.getAction().run();
            if (this.comboBar.getCommand().isFocusOwner()) {
                this.comboBar.getCommand().setText("");
            }
            this.clearModel();
        }
    }

    void selectNext() {
        int oldSel = this.jList1.getSelectedIndex();
        if (oldSel >= 0 && oldSel < this.jList1.getModel().getSize() - 1) {
            this.jList1.setSelectedIndex(oldSel + 1);
        }
        if (this.jList1.getModel().getSize() > 0) {
            this.setVisible(true);
        }
    }

    void selectPrev() {
        int oldSel = this.jList1.getSelectedIndex();
        if (oldSel > 0) {
            this.jList1.setSelectedIndex(oldSel - 1);
        }
        if (this.jList1.getModel().getSize() > 0) {
            this.setVisible(true);
        }
    }

    public JList getList() {
        return this.jList1;
    }

    public void clearModel() {
        this.rModel.setContent(null);
        this.longestText = -1;
    }

    public void maybeEvaluate(String text) {
        this.searchedText = text;
        if (text.length() > 0) {
            this.updateStatusPanel(true);
            this.updatePopup(true);
        }
        if (this.updateTimer == null) {
            this.updateTimer = new Timer(300, this);
        }
        if (!this.updateTimer.isRunning()) {
            this.updateTimer.start();
        } else {
            this.updateTimer.restart();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.updateTimer.stop();
        if (this.comboBar.getCommand().isFocusOwner()) {
            evaluatorRP.post(new Runnable(){

                @Override
                public void run() {
                    if (QuickSearchPopup.this.evalTask != null) {
                        QuickSearchPopup.this.evalTask.removeTaskListener((TaskListener)QuickSearchPopup.this);
                    }
                    QuickSearchPopup.this.evalTask = CommandEvaluator.evaluate(QuickSearchPopup.this.searchedText, QuickSearchPopup.this.rModel);
                    QuickSearchPopup.this.evalTask.addTaskListener((TaskListener)QuickSearchPopup.this);
                    RP.post((Runnable)QuickSearchPopup.this.evalTask);
                }
            });
        }
    }

    private void initComponents() {
        this.jScrollPane1 = new JScrollPane();
        this.jList1 = new JList();
        this.statusPanel = new JPanel();
        this.searchingSep = new JSeparator();
        this.searchingLabel = new JLabel();
        this.noResultsLabel = new JLabel();
        this.hintSep = new JSeparator();
        this.hintLabel = new JLabel();
        this.setBorder(BorderFactory.createLineBorder(QuickSearchComboBar.getPopupBorderColor()));
        this.setLayout(new BorderLayout());
        this.jScrollPane1.setBorder(null);
        this.jScrollPane1.setHorizontalScrollBarPolicy(31);
        this.jScrollPane1.setVerticalScrollBarPolicy(21);
        this.jList1.setFocusable(false);
        this.jList1.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent evt) {
                QuickSearchPopup.this.jList1MouseClicked(evt);
            }

            @Override
            public void mousePressed(MouseEvent evt) {
                QuickSearchPopup.this.jList1MousePressed(evt);
            }
        });
        this.jList1.addMouseMotionListener(new MouseMotionAdapter(){

            @Override
            public void mouseDragged(MouseEvent evt) {
                QuickSearchPopup.this.jList1MouseDragged(evt);
            }

            @Override
            public void mouseMoved(MouseEvent evt) {
                QuickSearchPopup.this.jList1MouseMoved(evt);
            }
        });
        this.jScrollPane1.setViewportView(this.jList1);
        this.add((Component)this.jScrollPane1, "Center");
        this.statusPanel.setBackground(QuickSearchComboBar.getResultBackground());
        this.statusPanel.setLayout(new GridBagLayout());
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        this.statusPanel.add((Component)this.searchingSep, gridBagConstraints);
        this.searchingLabel.setText(NbBundle.getMessage(QuickSearchPopup.class, (String)"QuickSearchPopup.searchingLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 17;
        this.statusPanel.add((Component)this.searchingLabel, gridBagConstraints);
        this.noResultsLabel.setForeground(Color.red);
        this.noResultsLabel.setHorizontalAlignment(0);
        this.noResultsLabel.setText(NbBundle.getMessage(QuickSearchPopup.class, (String)"QuickSearchPopup.noResultsLabel.text"));
        this.noResultsLabel.setFocusable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        this.statusPanel.add((Component)this.noResultsLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        this.statusPanel.add((Component)this.hintSep, gridBagConstraints);
        this.hintLabel.setBackground(QuickSearchComboBar.getResultBackground());
        this.hintLabel.setHorizontalAlignment(0);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = 1;
        this.statusPanel.add((Component)this.hintLabel, gridBagConstraints);
        this.add((Component)this.statusPanel, "Last");
    }

    private void jList1MouseMoved(MouseEvent evt) {
        if (evt.getX() < 5) {
            this.setCursor(Cursor.getPredefinedCursor(10));
        } else {
            this.setCursor(Cursor.getDefaultCursor());
        }
        Point loc = evt.getPoint();
        int index = this.jList1.locationToIndex(loc);
        if (index == -1) {
            return;
        }
        Rectangle rect = this.jList1.getCellBounds(index, index);
        if (rect != null && rect.contains(loc)) {
            this.jList1.setSelectedIndex(index);
        }
    }

    private void jList1MouseClicked(MouseEvent evt) {
        if (!SwingUtilities.isLeftMouseButton(evt)) {
            return;
        }
        this.comboBar.invokeSelectedItem();
    }

    private void jList1MouseDragged(MouseEvent evt) {
        this.processMouseMotionEvent(evt);
    }

    private void jList1MousePressed(MouseEvent evt) {
        this.processMouseEvent(evt);
    }

    @Override
    public void intervalAdded(ListDataEvent e) {
        this.updatePopup(this.evalTask != null);
    }

    @Override
    public void intervalRemoved(ListDataEvent e) {
        this.updatePopup(this.evalTask != null);
    }

    @Override
    public void contentsChanged(ListDataEvent e) {
        if (this.customWidth < 0) {
            if (this.rModel.getContent() == null) {
                this.longestText = -1;
                this.resultWidth = -1;
            } else {
                for (CategoryResult r : this.rModel.getContent()) {
                    for (ResultsModel.ItemResult i : r.getItems()) {
                        int l = i.getDisplayName().length();
                        if (l <= this.longestText) continue;
                        this.longestText = l;
                        this.resultWidth = -1;
                    }
                }
            }
        }
        this.updatePopup(this.evalTask != null);
    }

    public void updatePopup(boolean isInProgress) {
        this.updatePopup(isInProgress, true);
    }

    private void updatePopup(boolean isInProgress, boolean canRetry) {
        JLayeredPane lPane;
        int modelSize = this.rModel.getSize();
        if (modelSize > 0 && this.jList1.getSelectedIndex() < 0) {
            this.jList1.setSelectedIndex(0);
        }
        if ((lPane = JLayeredPane.getLayeredPaneAbove(this.comboBar)) == null) {
            return;
        }
        if (!this.isDisplayable()) {
            lPane.add((Component)this, new Integer(JLayeredPane.POPUP_LAYER + 1));
        }
        boolean statusVisible = this.updateStatusPanel(isInProgress);
        try {
            this.computePopupBounds(this.popupBounds, lPane, modelSize);
        }
        catch (Exception e) {
            LOG.log(canRetry ? Level.INFO : Level.SEVERE, null, e);
            this.retryUpdatePopup(canRetry, isInProgress);
            return;
        }
        this.setBounds(this.popupBounds);
        if ((modelSize > 0 || statusVisible) && this.comboBar.getCommand().isFocusOwner()) {
            if (modelSize > 0 && !this.isVisible()) {
                this.jList1.setSelectedIndex(0);
            }
            if (this.jList1.getSelectedIndex() >= modelSize) {
                this.jList1.setSelectedIndex(modelSize - 1);
            }
            if (this.explicitlyInvoked || !this.searchedText.isEmpty()) {
                this.setVisible(true);
            }
        } else {
            this.setVisible(false);
        }
        this.explicitlyInvoked = false;
        this.revalidate();
    }

    private void retryUpdatePopup(boolean canRetry, final boolean isInProgress) {
        if (canRetry) {
            EventQueue.invokeLater(new Runnable(){

                @Override
                public void run() {
                    QuickSearchPopup.this.updatePopup(isInProgress, false);
                }
            });
        }
    }

    void explicitlyInvoked() {
        this.explicitlyInvoked = true;
    }

    public int getCategoryWidth() {
        if (this.catWidth <= 0) {
            this.catWidth = QuickSearchPopup.computeWidth(this.jList1, 20, 30);
        }
        return this.catWidth;
    }

    public int getResultWidth() {
        if (this.customWidth > 0) {
            return Math.max(this.customWidth, this.getDefaultResultWidth());
        }
        if (this.resultWidth <= 0) {
            this.resultWidth = QuickSearchPopup.computeWidth(this.jList1, this.limit(this.longestText, 42, 128), 50);
        }
        return this.resultWidth;
    }

    private int getDefaultResultWidth() {
        if (this.defaultResultWidth <= 0) {
            this.defaultResultWidth = QuickSearchPopup.computeWidth(this.jList1, 42, 50);
        }
        return this.defaultResultWidth;
    }

    private int limit(int value, int min, int max) {
        assert (min <= max);
        return Math.min(max, Math.max(min, value));
    }

    public int getPopupWidth() {
        int maxWidth = this.getParent() == null ? Integer.MAX_VALUE : this.getParent().getWidth() - 10;
        return Math.min(this.getCategoryWidth() + this.getResultWidth() + 3, maxWidth);
    }

    public void taskFinished(Task task) {
        this.evalTask = null;
        if (SwingUtilities.isEventDispatchThread()) {
            this.run();
        } else {
            SwingUtilities.invokeLater(this);
        }
    }

    @Override
    public void run() {
        this.updatePopup(this.evalTask != null);
    }

    private void computePopupBounds(Rectangle result, JLayeredPane lPane, int modelSize) {
        Dimension cSize = this.comboBar.getSize();
        int width = this.getPopupWidth();
        Point location = new Point(cSize.width - width - 1, this.comboBar.getBottomLineY() - 1);
        if (SwingUtilities.getWindowAncestor(this.comboBar) != null) {
            location = SwingUtilities.convertPoint(this.comboBar, location, lPane);
        }
        result.setLocation(location);
        this.jList1.setFixedCellHeight(15);
        this.jList1.setFixedCellHeight(-1);
        this.jList1.setVisibleRowCount(modelSize);
        Dimension preferredSize = this.jList1.getPreferredSize();
        preferredSize.width = width;
        preferredSize.height += this.statusPanel.getPreferredSize().height + 3;
        result.setSize(preferredSize);
    }

    private static int computeWidth(JComponent comp, int maxCharCount, int percent) {
        FontMetrics fm = comp.getFontMetrics(comp.getFont());
        int charW = fm.charWidth('X');
        int result = charW * maxCharCount;
        Window w = SwingUtilities.windowForComponent(comp);
        if (w != null) {
            result = Math.min(result, w.getWidth() * percent / 100);
        }
        return result;
    }

    private boolean updateStatusPanel(boolean isInProgress) {
        boolean shouldBeVisible = false;
        this.searchingSep.setVisible(isInProgress);
        this.searchingLabel.setVisible(isInProgress);
        if (this.comboBar instanceof QuickSearchComboBar) {
            if (isInProgress) {
                ((QuickSearchComboBar)this.comboBar).startProgressAnimation();
            } else {
                ((QuickSearchComboBar)this.comboBar).stopProgressAnimation();
            }
        }
        shouldBeVisible = shouldBeVisible || isInProgress;
        boolean searchedNotEmpty = this.searchedText != null && this.searchedText.trim().length() > 0;
        boolean areNoResults = this.rModel.getSize() <= 0 && searchedNotEmpty && !isInProgress;
        this.noResultsLabel.setVisible(areNoResults);
        this.comboBar.setNoResults(areNoResults);
        shouldBeVisible = shouldBeVisible || areNoResults;
        this.hintLabel.setText(this.getHintText());
        boolean isNarrowed = CommandEvaluator.isTemporaryCatSpecified() && searchedNotEmpty;
        this.hintSep.setVisible(isNarrowed);
        this.hintLabel.setVisible(isNarrowed);
        shouldBeVisible = shouldBeVisible || isNarrowed;
        return shouldBeVisible;
    }

    private String getHintText() {
        ProviderModel.Category temp = CommandEvaluator.getTemporaryCat();
        if (temp != null) {
            return NbBundle.getMessage(QuickSearchPopup.class, (String)"QuickSearchPopup.hintLabel.text", (Object)temp.getDisplayName(), (Object)SearchResultRender.getKeyStrokeAsText(this.comboBar.getKeyStroke()));
        }
        return null;
    }

    private void makeResizable() {
        this.addMouseMotionListener(new MouseMotionAdapter(){

            @Override
            public void mouseDragged(MouseEvent e) {
                if (QuickSearchPopup.this.canResize) {
                    QuickSearchPopup.this.customWidth = Math.max(1, QuickSearchPopup.this.getResultWidth() - e.getX());
                    QuickSearchPopup.this.run();
                    QuickSearchPopup.this.saveSettings();
                }
            }
        });
        this.addMouseListener(new MouseAdapter(){

            @Override
            public void mousePressed(MouseEvent e) {
                QuickSearchPopup.this.canResize = e.getX() < 5;
            }
        });
    }

    private void loadSettings() {
        RP.post(new Runnable(){

            @Override
            public void run() {
                Preferences p = NbPreferences.forModule(QuickSearchPopup.class);
                QuickSearchPopup.this.customWidth = p.getInt("customWidth", -1);
            }
        });
    }

    private synchronized void saveSettings() {
        if (this.saveTask == null) {
            this.saveTask = RP.create(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    Preferences p = NbPreferences.forModule(QuickSearchPopup.class);
                    p.putInt("customWidth", QuickSearchPopup.this.customWidth);
                    QuickSearchPopup quickSearchPopup = QuickSearchPopup.this;
                    synchronized (quickSearchPopup) {
                        QuickSearchPopup.this.saveTask = null;
                    }
                }
            });
            RP.post((Runnable)this.saveTask, 1000);
        }
    }

}

