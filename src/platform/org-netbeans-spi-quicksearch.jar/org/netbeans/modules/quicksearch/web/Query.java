/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.quicksearch.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.net.Socket;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.quicksearch.web.Result;
import org.openide.util.NbBundle;

final class Query {
    private Thread searchThread;
    private static Query theInstance;
    private int searchOffset;
    private static final String SITE_SEARCH;
    static final int MAX_NUM_OF_RESULTS = 50;
    private static final String[] URL_PATTERNS;

    private Query() {
    }

    public static Query getDefault() {
        if (null == theInstance) {
            theInstance = new Query();
        }
        return theInstance;
    }

    public Result search(String searchString) {
        this.abort();
        Result res = new Result();
        this.searchThread = new Thread(this.createSearch(searchString, res, 0));
        this.searchThread.start();
        try {
            this.searchThread.join(20000);
        }
        catch (InterruptedException iE) {
            // empty catch block
        }
        return res;
    }

    public Result searchMore(String searchString) {
        this.searchOffset += 50;
        Result res = new Result();
        Thread searchMoreThread = new Thread(this.createSearch(searchString, res, this.searchOffset));
        searchMoreThread.start();
        try {
            searchMoreThread.join(20000);
        }
        catch (InterruptedException iE) {
            // empty catch block
        }
        return res;
    }

    private void abort() {
        if (null == this.searchThread) {
            return;
        }
        this.searchOffset = 0;
        this.searchThread.interrupt();
        this.searchThread = null;
    }

    private Runnable createSearch(final String searchString, final Result result, final int searchOffset) {
        Runnable res = new Runnable(){

            @Override
            public void run() {
                String query = searchString;
                query = query.replaceAll(" ", "+");
                query = query.replaceAll("#", "%23");
                query = query + "&num=50";
                query = query + "&hl=" + Locale.getDefault().getLanguage();
                if (null != SITE_SEARCH) {
                    query = query + "&sitesearch=" + SITE_SEARCH;
                }
                if (searchOffset > 0) {
                    query = query + "&start=" + searchOffset;
                }
                try {
                    String line;
                    Socket s = new Socket("google.com", 80);
                    PrintStream p = new PrintStream(s.getOutputStream());
                    p.print("GET /search?q=" + query + " HTTP/1.0\r\n");
                    p.print("User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1) Gecko/20061010 Firefox/2.0\r\n");
                    p.print("Connection: close\r\n\r\n");
                    InputStreamReader in = new InputStreamReader(s.getInputStream());
                    BufferedReader buffer = new BufferedReader(in);
                    StringBuffer rawHtml = new StringBuffer();
                    while ((line = buffer.readLine()) != null) {
                        rawHtml.append(line);
                    }
                    in.close();
                    result.parse(rawHtml.toString(), searchOffset);
                    result.filterUrl(URL_PATTERNS);
                }
                catch (IOException ioE) {
                    Logger.getLogger(Query.class.getName()).log(Level.INFO, null, ioE);
                }
            }
        };
        return res;
    }

    private static String getSiteSearch() {
        String res = null;
        try {
            res = NbBundle.getMessage(Query.class, (String)"quicksearch.web.site");
        }
        catch (MissingResourceException mrE) {
            // empty catch block
        }
        return res;
    }

    private static String[] getUrlPatterns() {
        try {
            String patterns = NbBundle.getMessage(Query.class, (String)"quicksearch.web.url_patterns");
            return patterns.split("\\s|,|;|:");
        }
        catch (MissingResourceException mrE) {
            return null;
        }
    }

    static {
        SITE_SEARCH = Query.getSiteSearch();
        URL_PATTERNS = Query.getUrlPatterns();
    }

}

