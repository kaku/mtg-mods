/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.quicksearch.web;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.modules.quicksearch.web.Item;

final class Result {
    private List<Item> items = new ArrayList<Item>(50);
    private boolean searchFinished = false;

    Result() {
    }

    public List<Item> getItems() {
        return this.items;
    }

    public boolean isSearchFinished() {
        return this.searchFinished;
    }

    void parse(String html, int currentSearchOffset) {
        this.searchFinished = true;
        this.items.clear();
        try {
            html = new String(html.getBytes(), "UTF-8");
        }
        catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Result.class.getName()).log(Level.FINE, null, ex);
        }
        Pattern p = Pattern.compile("<a\\s+href\\s*=\\s*\"(.*?)\"[^>]*>(.*?)</a>", 10);
        Matcher m = p.matcher(html);
        while (m.find()) {
            String url = m.group(1);
            String title = m.group(2);
            if (url.startsWith("/")) {
                int searchOffset = this.findSearchOffset(url);
                if (searchOffset <= currentSearchOffset) continue;
                this.searchFinished = false;
                continue;
            }
            if (url.contains("google.com")) continue;
            title = "<html>" + title;
            Item si = new Item(url, title, null);
            this.items.add(si);
        }
    }

    public void filterUrl(String[] urlPatterns) {
        if (null == urlPatterns || urlPatterns.length == 0) {
            return;
        }
        ArrayList<Item> filteredItems = new ArrayList<Item>(this.items.size());
        block0 : for (Item item : this.items) {
            for (int i = 0; i < urlPatterns.length; ++i) {
                if (urlPatterns[i].length() == 0 || !item.getUrl().toLowerCase(Locale.ENGLISH).matches(urlPatterns[i])) continue;
                filteredItems.add(item);
                continue block0;
            }
        }
        this.items = filteredItems;
    }

    private int findSearchOffset(String url) {
        int startIndex = url.indexOf("&amp;start=");
        if (startIndex < 0) {
            return -1;
        }
        int endIndex = url.indexOf("&amp;", startIndex + 1);
        if (endIndex < 0) {
            endIndex = url.length();
        }
        if (endIndex < startIndex) {
            return -1;
        }
        String offset = url.substring(startIndex, endIndex);
        try {
            return Integer.parseInt(offset);
        }
        catch (NumberFormatException nfE) {
            return -1;
        }
    }
}

