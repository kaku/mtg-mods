/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.quicksearch.web;

import java.awt.Toolkit;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.quicksearch.web.Item;
import org.netbeans.modules.quicksearch.web.Query;
import org.netbeans.modules.quicksearch.web.Result;
import org.netbeans.spi.quicksearch.SearchProvider;
import org.netbeans.spi.quicksearch.SearchRequest;
import org.netbeans.spi.quicksearch.SearchResponse;
import org.openide.awt.HtmlBrowser;
import org.openide.awt.StatusDisplayer;
import org.openide.util.NbBundle;

public class WebQuickSearchProviderImpl
implements SearchProvider {
    private Query query;

    @Override
    public void evaluate(SearchRequest request, SearchResponse response) {
        if (null == this.query) {
            this.query = Query.getDefault();
        }
        Result res = this.query.search(request.getText());
        do {
            for (Item item : res.getItems()) {
                if (response.addResult(WebQuickSearchProviderImpl.createAction(item.getUrl()), item.getTitle())) continue;
                return;
            }
        } while (!(res = this.query.searchMore(request.getText())).isSearchFinished());
    }

    private static Runnable createAction(final String url) {
        return new Runnable(){

            @Override
            public void run() {
                String extendedUrl = WebQuickSearchProviderImpl.appendId(url);
                try {
                    HtmlBrowser.URLDisplayer displayer = HtmlBrowser.URLDisplayer.getDefault();
                    if (displayer != null) {
                        displayer.showURL(new URL(extendedUrl));
                    }
                }
                catch (Exception e) {
                    StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(WebQuickSearchProviderImpl.class, (String)"Err_CannotDisplayURL", (Object)extendedUrl));
                    Toolkit.getDefaultToolkit().beep();
                    Logger.getLogger(WebQuickSearchProviderImpl.class.getName()).log(Level.FINE, null, e);
                }
            }
        };
    }

    private static String appendId(String url) {
        StringBuffer res = new StringBuffer(url);
        if (url.contains("?")) {
            res.append('&');
        } else {
            res.append('?');
        }
        res.append("cid=925878");
        return res.toString();
    }

}

