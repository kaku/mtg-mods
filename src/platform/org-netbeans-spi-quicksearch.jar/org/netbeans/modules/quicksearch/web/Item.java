/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.quicksearch.web;

public class Item {
    private String url;
    private String title;
    private String description;

    Item(String url, String title, String description) {
        this.url = url;
        this.title = title;
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public String getTitle() {
        return this.title;
    }

    public String getUrl() {
        return this.url;
    }
}

