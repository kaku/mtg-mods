/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.quicksearch;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.MissingResourceException;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.ListModel;
import javax.swing.MenuElement;
import javax.swing.MenuSelectionManager;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.quicksearch.Bundle;
import org.netbeans.modules.quicksearch.CategoryResult;
import org.netbeans.modules.quicksearch.CommandEvaluator;
import org.netbeans.modules.quicksearch.ProviderModel;
import org.netbeans.modules.quicksearch.QuickSearchPopup;
import org.netbeans.modules.quicksearch.ResultsModel;
import org.netbeans.modules.quicksearch.SearchResultRender;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;

public abstract class AbstractQuickSearchComboBar
extends JPanel {
    QuickSearchPopup displayer;
    WeakReference<TopComponent> caller;
    Color origForeground;
    protected final KeyStroke keyStroke;
    protected JTextComponent command;

    public AbstractQuickSearchComboBar(KeyStroke ks) {
        this.displayer = new QuickSearchPopup(this);
        this.keyStroke = ks;
        this.initComponents();
        this.setShowHint(true);
        this.command.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent arg0) {
                this.textChanged();
            }

            @Override
            public void removeUpdate(DocumentEvent arg0) {
                this.textChanged();
            }

            @Override
            public void changedUpdate(DocumentEvent arg0) {
                this.textChanged();
            }

            private void textChanged() {
                if (AbstractQuickSearchComboBar.this.command.isFocusOwner()) {
                    AbstractQuickSearchComboBar.this.displayer.maybeEvaluate(AbstractQuickSearchComboBar.this.command.getText());
                }
            }
        });
        if (this.command.getDocument() instanceof AbstractDocument) {
            AbstractDocument ad = (AbstractDocument)this.command.getDocument();
            ad.setDocumentFilter(new InvalidSearchTextDocumentFilter());
        }
    }

    public KeyStroke getKeyStroke() {
        return this.keyStroke;
    }

    protected abstract JTextComponent createCommandField();

    protected abstract JComponent getInnerComponent();

    private void initComponents() {
        this.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this.setMaximumSize(new Dimension(200, Integer.MAX_VALUE));
        this.setName("Form");
        this.setOpaque(false);
        this.addFocusListener(new FocusAdapter(){

            @Override
            public void focusLost(FocusEvent evt) {
                AbstractQuickSearchComboBar.this.formFocusLost(evt);
            }
        });
        this.command = this.createCommandField();
        String shortcutText = "";
        if (!SearchResultRender.getKeyStrokeAsText(this.keyStroke).isEmpty()) {
            shortcutText = "(" + SearchResultRender.getKeyStrokeAsText(this.keyStroke) + ")";
        }
        this.command.setToolTipText(NbBundle.getMessage(AbstractQuickSearchComboBar.class, (String)"AbstractQuickSearchComboBar.command.toolTipText", (Object[])new Object[]{shortcutText}));
        this.command.setName("command");
        this.command.addFocusListener(new FocusAdapter(){

            @Override
            public void focusGained(FocusEvent evt) {
                AbstractQuickSearchComboBar.this.commandFocusGained(evt);
            }

            @Override
            public void focusLost(FocusEvent evt) {
                AbstractQuickSearchComboBar.this.commandFocusLost(evt);
            }
        });
        this.command.addKeyListener(new KeyAdapter(){

            @Override
            public void keyPressed(KeyEvent evt) {
                AbstractQuickSearchComboBar.this.commandKeyPressed(evt);
            }
        });
        this.command.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent e) {
                AbstractQuickSearchComboBar.this.displayer.explicitlyInvoked();
            }
        });
    }

    private void formFocusLost(FocusEvent evt) {
        this.displayer.setVisible(false);
    }

    private void commandKeyPressed(KeyEvent evt) {
        if (evt.getKeyCode() == 40) {
            this.displayer.selectNext();
            evt.consume();
        } else if (evt.getKeyCode() == 38) {
            this.displayer.selectPrev();
            evt.consume();
        } else if (evt.getKeyCode() == 10) {
            evt.consume();
            this.invokeSelectedItem();
        } else if (evt.getKeyCode() == 27) {
            this.returnFocus(true);
            this.displayer.clearModel();
        } else if (evt.getKeyCode() == 121 && evt.isShiftDown()) {
            evt.consume();
            this.maybeShowPopup(null);
        }
    }

    public void invokeSelectedItem() {
        Runnable action;
        JList list = this.displayer.getList();
        ResultsModel.ItemResult ir = (ResultsModel.ItemResult)list.getSelectedValue();
        if (ir != null && (action = ir.getAction()) instanceof CategoryResult) {
            CategoryResult cr = (CategoryResult)action;
            this.evaluate(cr.getCategory());
            return;
        }
        if (list.getModel().getSize() > 0) {
            this.returnFocus(false);
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    AbstractQuickSearchComboBar.this.displayer.invoke();
                }
            });
        }
    }

    private void returnFocus(boolean force) {
        TopComponent tc;
        this.displayer.setVisible(false);
        if (this.caller != null && (tc = this.caller.get()) != null) {
            tc.requestActive();
            tc.requestFocus();
            return;
        }
        if (force) {
            KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();
        }
    }

    private void commandFocusLost(FocusEvent evt) {
        this.displayer.setVisible(false);
        this.setShowHint(true);
    }

    private void commandFocusGained(FocusEvent evt) {
        this.caller = new WeakReference<TopComponent>(TopComponent.getRegistry().getActivated());
        this.setShowHint(false);
        CommandEvaluator.dropTemporaryCat();
    }

    protected void maybeShowPopup(MouseEvent evt) {
        if (evt != null && !SwingUtilities.isLeftMouseButton(evt)) {
            return;
        }
        JPopupMenu pm = new JPopupMenu();
        LinkedHashSet<ProviderModel.Category> evalCats = new LinkedHashSet<ProviderModel.Category>();
        evalCats.addAll(CommandEvaluator.getEvalCats());
        AllMenuItem allCats = new AllMenuItem(evalCats);
        pm.add(allCats);
        for (ProviderModel.Category cat : ProviderModel.getInstance().getCategories()) {
            if ("Recent".equals(cat.getName())) continue;
            CategoryCheckBoxMenuItem item = new CategoryCheckBoxMenuItem(cat, evalCats);
            pm.add(item);
        }
        pm.show(this.getInnerComponent(), 0, this.getInnerComponent().getHeight() - 1);
    }

    private void updateCats(Set<ProviderModel.Category> evalCats) {
        CommandEvaluator.setEvalCats(evalCats);
        CommandEvaluator.dropTemporaryCat();
        this.setShowHint(!this.command.isFocusOwner());
    }

    private void updateCheckBoxes(Container container, Set<ProviderModel.Category> evalCats) {
        Container parent = container.getParent();
        for (Component c : parent.getComponents()) {
            if (!(c instanceof CategoryCheckBoxMenuItem)) continue;
            CategoryCheckBoxMenuItem ci = (CategoryCheckBoxMenuItem)c;
            ci.setSelected(evalCats.contains(ci.category));
            ci.setTooltipText();
        }
    }

    public void evaluate(ProviderModel.Category tempCategory) {
        if (tempCategory != null) {
            CommandEvaluator.setTemporaryCat(tempCategory);
        } else {
            CommandEvaluator.dropTemporaryCat();
        }
        this.displayer.maybeEvaluate(this.command.getText());
    }

    public void setNoResults(boolean areNoResults) {
        if (this.command == null || this.origForeground == null) {
            return;
        }
        if (this.command.getForeground().equals(this.command.getDisabledTextColor())) {
            return;
        }
        this.command.setForeground(areNoResults ? Color.RED : this.origForeground);
    }

    private void setShowHint(boolean showHint) {
        if (this.origForeground == null) {
            this.origForeground = this.command.getForeground();
        }
        if (showHint) {
            this.command.setForeground(this.command.getDisabledTextColor());
            Set<ProviderModel.Category> evalCats = CommandEvaluator.getEvalCats();
            if (evalCats.size() < 3 && !CommandEvaluator.isTemporaryCatSpecified()) {
                ProviderModel.Category bestFound = null;
                for (ProviderModel.Category c : evalCats) {
                    if (bestFound != null && !"Recent".equals(bestFound.getName())) continue;
                    bestFound = c;
                }
                this.command.setText(this.getHintText(bestFound));
            } else {
                this.command.setText(this.getHintText(null));
            }
        } else {
            this.command.setForeground(this.origForeground);
            this.command.setText("");
        }
    }

    private String getHintText(ProviderModel.Category cat) {
        StringBuilder sb = new StringBuilder();
        if (cat != null) {
            sb.append(NbBundle.getMessage(AbstractQuickSearchComboBar.class, (String)"MSG_DiscoverabilityHint2", (Object)cat.getDisplayName()));
        } else {
            sb.append(NbBundle.getMessage(AbstractQuickSearchComboBar.class, (String)"MSG_DiscoverabilityHint"));
        }
        String keyStrokeAsText = SearchResultRender.getKeyStrokeAsText(this.keyStroke);
        if (!keyStrokeAsText.isEmpty()) {
            sb.append(" (");
            sb.append(keyStrokeAsText);
            sb.append(")");
        }
        return sb.toString();
    }

    @Override
    public void requestFocus() {
        super.requestFocus();
        this.command.requestFocus();
    }

    public JTextComponent getCommand() {
        return this.command;
    }

    public int getBottomLineY() {
        return this.getInnerComponent().getY() + this.getInnerComponent().getHeight();
    }

    static Color getComboBorderColor() {
        Color shadow = UIManager.getColor(Utilities.isWindows() ? "Nb.ScrollPane.Border.color" : "TextField.shadow");
        return shadow != null ? shadow : AbstractQuickSearchComboBar.getPopupBorderColor();
    }

    static Color getPopupBorderColor() {
        Color shadow = UIManager.getColor("controlShadow");
        return shadow != null ? shadow : Color.GRAY;
    }

    static Color getTextBackground() {
        Color textB = UIManager.getColor("TextPane.background");
        if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
            textB = UIManager.getColor("NbExplorerView.background");
        }
        return textB != null ? textB : Color.WHITE;
    }

    static Color getResultBackground() {
        return AbstractQuickSearchComboBar.getTextBackground();
    }

    static Color getCategoryTextColor() {
        Color shadow = UIManager.getColor("textInactiveText");
        if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
            shadow = UIManager.getColor("Table.foreground");
        }
        return shadow != null ? shadow : Color.DARK_GRAY;
    }

    protected int computePrefWidth() {
        FontMetrics fm = this.command.getFontMetrics(this.command.getFont());
        ProviderModel pModel = ProviderModel.getInstance();
        int maxWidth = 0;
        for (ProviderModel.Category cat : pModel.getCategories()) {
            if ("Recent".equals(cat.getName())) continue;
            maxWidth = Math.max(maxWidth, fm.stringWidth(this.getHintText(cat)));
        }
        return Math.min(350, maxWidth);
    }

    private static String dispalyNameFor(ProviderModel.Category category) {
        if (null != category.getCommandPrefix()) {
            return NbBundle.getMessage(AbstractQuickSearchComboBar.class, (String)"LBL_CategoryAndCommandPrefix", (Object)category.getDisplayName(), (Object)category.getCommandPrefix());
        }
        return category.getDisplayName();
    }

    private void showMenuPath(MenuElement[] selectedPath) {
        if (selectedPath != null && selectedPath.length > 1 && selectedPath[0] instanceof JPopupMenu) {
            ((JPopupMenu)selectedPath[0]).setVisible(true);
            MenuSelectionManager.defaultManager().setSelectedPath(selectedPath);
        }
    }

    private class AllMenuItem
    extends JMenuItem
    implements ActionListener {
        private Set<ProviderModel.Category> evalCats;
        private int totalCount;
        private MenuElement[] selectedPath;

        public AllMenuItem(Set<ProviderModel.Category> evalCats) {
            this.selectedPath = null;
            this.evalCats = evalCats;
            this.totalCount = ProviderModel.getInstance().getCategories().size();
            this.getModel().addChangeListener(new ChangeListener(AbstractQuickSearchComboBar.this){
                final /* synthetic */ AbstractQuickSearchComboBar val$this$0;

                @Override
                public void stateChanged(ChangeEvent e) {
                    if (AllMenuItem.this.isShowing() && AllMenuItem.this.model.isArmed()) {
                        AllMenuItem.this.selectedPath = MenuSelectionManager.defaultManager().getSelectedPath();
                    }
                }
            });
            this.addActionListener(this);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (this.evalCats.size() == this.totalCount) {
                Iterator<ProviderModel.Category> iterator = this.evalCats.iterator();
                while (iterator.hasNext()) {
                    ProviderModel.Category c = iterator.next();
                    if ("Recent".equals(c.getName())) continue;
                    iterator.remove();
                }
            } else {
                this.evalCats.addAll(ProviderModel.getInstance().getCategories());
            }
            AbstractQuickSearchComboBar.this.updateCats(this.evalCats);
            AbstractQuickSearchComboBar.this.updateCheckBoxes(this, this.evalCats);
        }

        @Override
        public void doClick(int pressTime) {
            super.doClick(pressTime);
            AbstractQuickSearchComboBar.this.showMenuPath(this.selectedPath);
        }

        @Override
        public String getText() {
            if (this.evalCats == null || this.evalCats.size() != this.totalCount) {
                return NbBundle.getMessage(this.getClass(), (String)"LBL_AllCategories");
            }
            return NbBundle.getMessage(this.getClass(), (String)"LBL_NoCategory");
        }

    }

    private class CategoryCheckBoxMenuItem
    extends JCheckBoxMenuItem
    implements ActionListener {
        private MenuElement[] selectedPath;
        private ProviderModel.Category category;
        private final Set<ProviderModel.Category> evalCats;

        public CategoryCheckBoxMenuItem(ProviderModel.Category category, Set<ProviderModel.Category> evalCats) {
            super(AbstractQuickSearchComboBar.dispalyNameFor(category), evalCats.contains(category));
            this.selectedPath = null;
            this.category = category;
            this.evalCats = evalCats;
            this.setTooltipText();
            this.getModel().addChangeListener(new ChangeListener(AbstractQuickSearchComboBar.this){
                final /* synthetic */ AbstractQuickSearchComboBar val$this$0;

                @Override
                public void stateChanged(ChangeEvent e) {
                    if (CategoryCheckBoxMenuItem.this.isShowing() && CategoryCheckBoxMenuItem.this.model.isArmed()) {
                        CategoryCheckBoxMenuItem.this.selectedPath = MenuSelectionManager.defaultManager().getSelectedPath();
                    }
                }
            });
            this.addActionListener(this);
            this.addMouseListener(new MouseAdapter(AbstractQuickSearchComboBar.this){
                final /* synthetic */ AbstractQuickSearchComboBar val$this$0;

                @Override
                public void mouseClicked(MouseEvent e) {
                    CategoryCheckBoxMenuItem.this.mouseClickedOnItem(e);
                }
            });
        }

        @Override
        public void doClick(int pressTime) {
            super.doClick(pressTime);
            this.setTooltipText();
            AbstractQuickSearchComboBar.this.showMenuPath(this.selectedPath);
        }

        private void mouseClickedOnItem(MouseEvent e) {
            if (SwingUtilities.isRightMouseButton(e)) {
                e.consume();
                if (this.isSelected()) {
                    Iterator<ProviderModel.Category> iterator = this.evalCats.iterator();
                    while (iterator.hasNext()) {
                        ProviderModel.Category c = iterator.next();
                        if ("Recent".equals(c.getName())) continue;
                        iterator.remove();
                    }
                    this.evalCats.add(this.category);
                } else {
                    this.evalCats.addAll(ProviderModel.getInstance().getCategories());
                    this.evalCats.remove(this.category);
                }
                AbstractQuickSearchComboBar.this.updateCheckBoxes(this, this.evalCats);
                AbstractQuickSearchComboBar.this.updateCats(this.evalCats);
            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (this.isSelected()) {
                this.evalCats.add(this.category);
            } else {
                this.evalCats.remove(this.category);
            }
            AbstractQuickSearchComboBar.this.updateCats(this.evalCats);
        }

        private void setTooltipText() throws MissingResourceException {
            boolean selected = this.evalCats.contains(this.category);
            String bundleKey = selected ? "MSG_RightClickEnablesAllOthers" : "MSG_RightClickDisablesOthers";
            StringBuilder tooltip = new StringBuilder("<html>");
            tooltip.append(NbBundle.getMessage(AbstractQuickSearchComboBar.class, (String)bundleKey));
            if (null != this.category.getCommandPrefix()) {
                tooltip.append("<br/>");
                tooltip.append(NbBundle.getMessage(AbstractQuickSearchComboBar.class, (String)"LBL_TooltipCommandPrefix", (Object)this.category.getCommandPrefix()));
            }
            tooltip.append("</html>");
            this.setToolTipText(tooltip.toString());
        }

        @Override
        public Point getToolTipLocation(MouseEvent event) {
            Point p = new Point((event.getX() - 25) / 5 * 5, (event.getY() + 15) / 5 * 5);
            return p;
        }

    }

    static class InvalidSearchTextDocumentFilter
    extends DocumentFilter {
        private static final int SEARCH_TEXT_LENGTH_LIMIT = 256;
        private static final int SEARCH_NUM_WORDS_LIMIT = 20;

        InvalidSearchTextDocumentFilter() {
        }

        @Override
        public void insertString(DocumentFilter.FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
            String normalized = this.normalizeWhiteSpaces(string);
            if (this.isLengthInLimit(normalized, fb, 0)) {
                super.insertString(fb, offset, normalized, attr);
            } else {
                this.warnAboutInvalidText();
            }
        }

        @Override
        public void replace(DocumentFilter.FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
            String normalized;
            String string = normalized = text == null ? null : this.normalizeWhiteSpaces(text);
            if (normalized == null || this.isLengthInLimit(normalized, fb, length)) {
                super.replace(fb, offset, length, normalized, attrs);
            } else {
                this.warnAboutInvalidText();
            }
        }

        private boolean isLengthInLimit(String newContent, DocumentFilter.FilterBypass fb, int charsToBeRemoved) {
            return this.isLengthInLimit(newContent, 256 - fb.getDocument().getLength() + charsToBeRemoved);
        }

        boolean isLengthInLimit(String newContent, int remainingChars) {
            return newContent.length() <= remainingChars && newContent.split(" ").length <= 20;
        }

        String normalizeWhiteSpaces(String s) {
            String replaced = s.replaceAll("\\s+", " ");
            return replaced.length() > 1 ? replaced.trim() : replaced;
        }

        private void warnAboutInvalidText() {
            NotifyDescriptor.Message nd = new NotifyDescriptor.Message((Object)Bundle.MSG_INVALID_SEARCH_TEST(), 0);
            DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)nd);
        }
    }

}

