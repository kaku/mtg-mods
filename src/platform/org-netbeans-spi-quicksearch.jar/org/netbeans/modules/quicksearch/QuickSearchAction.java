/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallableSystemAction
 */
package org.netbeans.modules.quicksearch;

import java.awt.Component;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.quicksearch.AbstractQuickSearchComboBar;
import org.netbeans.modules.quicksearch.AquaQuickSearchComboBar;
import org.netbeans.modules.quicksearch.ProviderModel;
import org.netbeans.modules.quicksearch.QuickSearchComboBar;
import org.netbeans.modules.quicksearch.QuickSearchPopup;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallableSystemAction;

public final class QuickSearchAction
extends CallableSystemAction {
    private static final boolean isAqua = "Aqua".equals(UIManager.getLookAndFeel().getID());
    AbstractQuickSearchComboBar comboBar;

    public void performAction() {
        if (this.comboBar == null) {
            this.comboBar = isAqua ? new AquaQuickSearchComboBar((KeyStroke)this.getValue("AcceleratorKey")) : new QuickSearchComboBar((KeyStroke)this.getValue("AcceleratorKey"));
        }
        this.comboBar.displayer.explicitlyInvoked();
        if (this.comboBar.getCommand().isFocusOwner()) {
            this.comboBar.evaluate(null);
        } else {
            this.comboBar.requestFocus();
        }
    }

    public String getName() {
        return NbBundle.getMessage(QuickSearchAction.class, (String)"CTL_QuickSearchAction");
    }

    protected String iconResource() {
        return "org/netbeans/modules/jumpto/resources/edit_parameters.png";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }

    public Component getToolbarPresenter() {
        if (this.comboBar == null) {
            this.comboBar = isAqua ? new AquaQuickSearchComboBar((KeyStroke)this.getValue("AcceleratorKey")) : new QuickSearchComboBar((KeyStroke)this.getValue("AcceleratorKey"));
        }
        return this.comboBar;
    }
}

