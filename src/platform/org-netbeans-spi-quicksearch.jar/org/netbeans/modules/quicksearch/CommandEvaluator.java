/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 */
package org.netbeans.modules.quicksearch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.KeyStroke;
import org.netbeans.modules.quicksearch.Accessor;
import org.netbeans.modules.quicksearch.CategoryResult;
import org.netbeans.modules.quicksearch.ProviderModel;
import org.netbeans.modules.quicksearch.ResultsModel;
import org.netbeans.spi.quicksearch.SearchProvider;
import org.netbeans.spi.quicksearch.SearchRequest;
import org.netbeans.spi.quicksearch.SearchResponse;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;

public class CommandEvaluator {
    static final String RECENT = "Recent";
    private static final String PROP_ENABLED_CATEGORIES = "enabledCategories";
    private static final Pattern COMMAND_PATTERN = Pattern.compile("(\\w+)(\\s+)(.+)");
    private static final RequestProcessor RP = new RequestProcessor("QuickSearch Command Evaluator", 10);
    private static Set<ProviderModel.Category> evalCats = CommandEvaluator.loadEvalCats();
    private static ProviderModel.Category temporaryCat = null;

    public static Task evaluate(String command, ResultsModel model) {
        ArrayList<CategoryResult> l = new ArrayList<CategoryResult>();
        String[] commands = CommandEvaluator.parseCommand(command);
        SearchRequest sRequest = Accessor.DEFAULT.createRequest(commands[1], null);
        ArrayList<RequestProcessor.Task> tasks = new ArrayList<RequestProcessor.Task>();
        ArrayList<ProviderModel.Category> provCats = new ArrayList<ProviderModel.Category>();
        boolean allResults = CommandEvaluator.getProviderCategories(commands, provCats);
        for (ProviderModel.Category curCat : provCats) {
            CategoryResult catResult = new CategoryResult(curCat, allResults);
            SearchResponse sResponse = Accessor.DEFAULT.createResponse(catResult, sRequest);
            for (SearchProvider provider : curCat.getProviders()) {
                RequestProcessor.Task t = CommandEvaluator.runEvaluation(provider, sRequest, sResponse, curCat);
                if (t == null) continue;
                tasks.add(t);
            }
            l.add(catResult);
        }
        model.setContent(l);
        return new Wait4AllTask(tasks);
    }

    private static Set<ProviderModel.Category> loadEvalCats() {
        final LinkedHashSet<ProviderModel.Category> cats = new LinkedHashSet<ProviderModel.Category>(ProviderModel.getInstance().getCategories());
        RP.post(new Runnable(){

            @Override
            public void run() {
                String ec = NbPreferences.forModule(CommandEvaluator.class).get("enabledCategories", null);
                if (ec != null) {
                    LinkedHashSet<String> categoryNames = new LinkedHashSet<String>();
                    categoryNames.addAll(Arrays.asList(ec.split(":")));
                    Iterator iterator = cats.iterator();
                    while (iterator.hasNext()) {
                        ProviderModel.Category category = (ProviderModel.Category)iterator.next();
                        if (categoryNames.contains(category.getName()) || "Recent".equals(category.getName())) continue;
                        iterator.remove();
                    }
                }
            }
        });
        return cats;
    }

    private static void storeEvalCats() {
        RP.post(new Runnable(){

            @Override
            public void run() {
                StringBuilder sb = new StringBuilder();
                for (ProviderModel.Category category : evalCats) {
                    if ("Recent".equals(category.getName())) continue;
                    sb.append(category.getName());
                    sb.append(':');
                }
                NbPreferences.forModule(CommandEvaluator.class).put("enabledCategories", sb.toString());
            }
        });
    }

    public static Set<ProviderModel.Category> getEvalCats() {
        return evalCats;
    }

    public static void setEvalCats(Set<ProviderModel.Category> cat) {
        evalCats = cat == null ? new LinkedHashSet<ProviderModel.Category>(ProviderModel.getInstance().getCategories()) : cat;
        CommandEvaluator.storeEvalCats();
    }

    public static void dropTemporaryCat() {
        temporaryCat = null;
    }

    public static void setTemporaryCat(ProviderModel.Category temporaryCat) {
        CommandEvaluator.temporaryCat = temporaryCat;
    }

    public static boolean isTemporaryCatSpecified() {
        return temporaryCat != null;
    }

    public static ProviderModel.Category getTemporaryCat() {
        return temporaryCat;
    }

    private static String[] parseCommand(String command) {
        String[] results = new String[2];
        Matcher m = COMMAND_PATTERN.matcher(command);
        if (m.matches()) {
            results[0] = m.group(1);
            if (ProviderModel.getInstance().isKnownCommand(results[0])) {
                results[1] = m.group(3);
            } else {
                results[0] = null;
                results[1] = command;
            }
        } else {
            results[1] = command;
        }
        return results;
    }

    static boolean getProviderCategories(String[] commands, List<ProviderModel.Category> result) {
        List<ProviderModel.Category> cats = ProviderModel.getInstance().getCategories();
        for (ProviderModel.Category cat : cats) {
            if (!"Recent".equals(cat.getName())) continue;
            result.add(cat);
        }
        if (commands[1] == null || commands[1].trim().equals("")) {
            return false;
        }
        if (commands[0] != null) {
            for (ProviderModel.Category curCat : cats) {
                String commandPrefix = curCat.getCommandPrefix();
                if (commandPrefix == null || !commandPrefix.equalsIgnoreCase(commands[0])) continue;
                result.add(curCat);
                return true;
            }
        }
        if (temporaryCat != null) {
            result.add(temporaryCat);
            return true;
        }
        for (ProviderModel.Category c : evalCats) {
            if ("Recent".equals(c.getName())) continue;
            result.add(c);
        }
        return result.size() < 3;
    }

    private static RequestProcessor.Task runEvaluation(final SearchProvider provider, final SearchRequest request, final SearchResponse response, ProviderModel.Category cat) {
        return RP.post(new Runnable(){

            @Override
            public void run() {
                provider.evaluate(request, response);
            }
        });
    }

    private static class Wait4AllTask
    extends Task
    implements Runnable {
        private static final long TIMEOUT = 60000;
        private final List<RequestProcessor.Task> tasks;

        private Wait4AllTask(List<RequestProcessor.Task> tasks) {
            this.tasks = tasks;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            try {
                this.notifyRunning();
                for (RequestProcessor.Task task : this.tasks) {
                    try {
                        task.waitFinished(60000);
                    }
                    catch (InterruptedException ex) {}
                }
            }
            finally {
                this.notifyFinished();
            }
        }
    }

}

