/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.quicksearch;

import java.util.List;
import javax.swing.KeyStroke;
import org.netbeans.modules.quicksearch.CategoryResult;
import org.netbeans.spi.quicksearch.SearchRequest;
import org.netbeans.spi.quicksearch.SearchResponse;

public abstract class Accessor {
    public static Accessor DEFAULT;

    public abstract SearchRequest createRequest(String var1, List<? extends KeyStroke> var2);

    public abstract SearchResponse createResponse(CategoryResult var1, SearchRequest var2);

    static {
        block2 : {
            Class<SearchRequest> c = SearchRequest.class;
            try {
                Class.forName(c.getName(), true, c.getClassLoader());
            }
            catch (ClassNotFoundException ex) {
                if ($assertionsDisabled) break block2;
                throw new AssertionError(ex);
            }
        }
    }
}

