/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 */
package org.netbeans.core.ui.sysopen;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.util.ContextAwareAction;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;

public final class SystemOpenAction
extends AbstractAction
implements ContextAwareAction {
    public SystemOpenAction() {
        super(NbBundle.getMessage(SystemOpenAction.class, (String)"CTL_SystemOpenAction"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        new ContextAction(Utilities.actionsGlobalContext()).actionPerformed(e);
    }

    public Action createContextAwareInstance(Lookup context) {
        return new ContextAction(context);
    }

    private static final class ContextAction
    extends AbstractAction {
        private final Set<File> files;

        public ContextAction(Lookup context) {
            super(NbBundle.getMessage(SystemOpenAction.class, (String)"CTL_SystemOpenAction"));
            this.putValue("hideWhenDisabled", true);
            this.files = new HashSet<File>();
            for (DataObject d : context.lookupAll(DataObject.class)) {
                File f = FileUtil.toFile((FileObject)d.getPrimaryFile());
                if (f == null || Utilities.isWindows() && f.isFile() && !f.getName().contains(".")) {
                    this.files.clear();
                    break;
                }
                this.files.add(f);
            }
        }

        @Override
        public boolean isEnabled() {
            return !this.files.isEmpty() && Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.OPEN);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            RequestProcessor.getDefault().post(new Runnable(){

                @Override
                public void run() {
                    Desktop desktop = Desktop.getDesktop();
                    for (File f : ContextAction.this.files) {
                        try {
                            desktop.open(f);
                        }
                        catch (IOException x) {
                            Logger.getLogger(SystemOpenAction.class.getName()).log(Level.INFO, null, x);
                        }
                    }
                }
            });
        }

    }

}

