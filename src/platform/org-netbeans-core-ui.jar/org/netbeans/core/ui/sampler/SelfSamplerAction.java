/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.sampler.Sampler
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.ui.sampler;

import java.awt.AWTEvent;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.concurrent.atomic.AtomicReference;
import javax.swing.AbstractAction;
import javax.swing.SwingWorker;
import org.netbeans.modules.sampler.Sampler;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.NbBundle;

public class SelfSamplerAction
extends AbstractAction
implements AWTEventListener {
    private static final String ACTION_NAME_START = NbBundle.getMessage(SelfSamplerAction.class, (String)"SelfSamplerAction_ActionNameStart");
    private static final String ACTION_NAME_STOP = NbBundle.getMessage(SelfSamplerAction.class, (String)"SelfSamplerAction_ActionNameStop");
    private static final String NOT_SUPPORTED = NbBundle.getMessage(SelfSamplerAction.class, (String)"SelfSamplerAction_NotSupported");
    private final AtomicReference<Sampler> RUNNING = new AtomicReference();

    public SelfSamplerAction() {
        this.putValue("Name", ACTION_NAME_START);
        this.putValue("ShortDescription", ACTION_NAME_START);
        this.putValue("iconBase", "org/netbeans/core/ui/sampler/selfSampler.png");
        if (System.getProperty(SelfSamplerAction.class.getName() + ".sniff") != null) {
            Toolkit.getDefaultToolkit().addAWTEventListener(this, 8);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Sampler c = Sampler.createManualSampler((String)"Self Sampler");
        if (c != null) {
            if (this.RUNNING.compareAndSet((Sampler)null, c)) {
                this.putValue("Name", ACTION_NAME_STOP);
                this.putValue("ShortDescription", ACTION_NAME_STOP);
                this.putValue("iconBase", "org/netbeans/core/ui/sampler/selfSamplerRunning.png");
                c.start();
            } else {
                c = this.RUNNING.getAndSet(null);
                if (c != null) {
                    final Sampler controller = c;
                    this.setEnabled(false);
                    SwingWorker worker = new SwingWorker(){

                        protected Object doInBackground() throws Exception {
                            controller.stop();
                            return null;
                        }

                        @Override
                        protected void done() {
                            SelfSamplerAction.this.putValue("Name", ACTION_NAME_START);
                            SelfSamplerAction.this.putValue("ShortDescription", ACTION_NAME_START);
                            SelfSamplerAction.this.putValue("iconBase", "org/netbeans/core/ui/sampler/selfSampler.png");
                            SelfSamplerAction.this.setEnabled(true);
                        }
                    };
                    worker.execute();
                }
            }
        } else {
            NotifyDescriptor.Message d = new NotifyDescriptor.Message((Object)NOT_SUPPORTED, 1);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)d);
        }
    }

    @Override
    public void eventDispatched(AWTEvent event) {
        KeyEvent kevent = (KeyEvent)event;
        if (kevent.getID() == 402 && kevent.getKeyCode() == 65406) {
            this.actionPerformed(new ActionEvent(this, event.getID(), "shortcut"));
            kevent.consume();
        }
    }

    final boolean isProfileMe(Sampler c) {
        return c == this.RUNNING.get();
    }

}

