/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.ui.notifications;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.core.ui.notifications.BalloonManager;
import org.netbeans.core.ui.notifications.NotificationDisplayerImpl;
import org.netbeans.core.ui.notifications.NotificationImpl;

public class PopupList {
    private static JPopupMenu current;
    private static final int MAX_VISIBLE_ROWS = 10;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void show(Component invoker) {
        PopupList.dismiss();
        BalloonManager.dismiss();
        Class<PopupList> class_ = PopupList.class;
        synchronized (PopupList.class) {
            current = PopupList.createPopup();
            if (null != current) {
                Dimension size = current.getPreferredSize();
                current.show(invoker, - size.width + invoker.getWidth(), - size.height);
            }
            // ** MonitorExit[var1_1] (shouldn't be in output)
            return;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void dismiss() {
        Class<PopupList> class_ = PopupList.class;
        synchronized (PopupList.class) {
            if (null != current) {
                current.setVisible(false);
            }
            current = null;
            // ** MonitorExit[var0] (shouldn't be in output)
            return;
        }
    }

    static JPopupMenu createPopup() {
        NotificationDisplayerImpl displayer = NotificationDisplayerImpl.getInstance();
        List<NotificationImpl> notifications = displayer.getNotifications();
        if (notifications.isEmpty()) {
            return null;
        }
        JPopupMenu res = new JPopupMenu();
        Dimension prefSize = new Dimension();
        int avgRowHeight = 0;
        int rowCount = 0;
        final JPanel panel = new JPanel(new GridBagLayout());
        JScrollPane scroll = new JScrollPane(panel);
        panel.setOpaque(true);
        Color panelBackground = UIManager.getColor("nb.core.ui.popupList.background");
        if (null == panelBackground) {
            panelBackground = UIManager.getColor("Tree.background");
        }
        panel.setBackground(panelBackground);
        for (NotificationImpl n : notifications) {
            final JPanel row = new JPanel(new GridBagLayout());
            row.setOpaque(false);
            JComponent component = n.getPopupComponent();
            if (null == component) continue;
            row.add((Component)component, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, 17, 0, new Insets(2, 2, 2, 2), 1, 1));
            BalloonManager.DismissButton btnDismiss = new BalloonManager.DismissButton();
            final NotificationImpl notification = n;
            btnDismiss.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    notification.clear();
                    panel.remove(row);
                    panel.getParent().invalidate();
                    panel.getParent().repaint();
                    panel.getParent().getParent().invalidate();
                    panel.getParent().getParent().repaint();
                    if (panel.getComponentCount() == 1) {
                        PopupList.dismiss();
                    }
                }
            });
            row.add((Component)btnDismiss, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, 12, 0, new Insets(5, 3, 3, 3), 1, 1));
            panel.add((Component)row, new GridBagConstraints(0, rowCount, 1, 1, 1.0, 0.0, 17, 2, new Insets(0, 0, 0, 0), 1, 1));
            Dimension size = row.getPreferredSize();
            if (size.width > prefSize.width) {
                prefSize.width = size.width;
            }
            if (rowCount++ < 10) {
                prefSize.height += size.height;
            }
            avgRowHeight += size.height;
        }
        panel.add((Component)new JLabel(), new GridBagConstraints(0, rowCount, 1, 1, 0.0, 1.0, 10, 0, new Insets(0, 0, 0, 0), 1, 1));
        scroll.setHorizontalScrollBarPolicy(31);
        scroll.setBorder(BorderFactory.createEmptyBorder());
        scroll.getVerticalScrollBar().setUnitIncrement(avgRowHeight /= notifications.size());
        scroll.getVerticalScrollBar().setBlockIncrement(10 * avgRowHeight);
        int scrollBarWidth = scroll.getVerticalScrollBar().getPreferredSize().width;
        if (scrollBarWidth <= 0) {
            scrollBarWidth = UIManager.getInt("ScrollBar.width");
        }
        if (scrollBarWidth <= 0) {
            scrollBarWidth = 18;
        }
        prefSize.width += scrollBarWidth;
        Insets i = scroll.getInsets();
        if (null != i) {
            prefSize.width += i.left + i.right;
            prefSize.height += i.top + i.bottom;
        }
        if (rowCount <= 10) {
            prefSize = panel.getPreferredSize();
        }
        prefSize.height = Math.min(prefSize.height, 600);
        if (prefSize.width > 800) {
            prefSize.width = 800;
            scroll.setHorizontalScrollBarPolicy(30);
        }
        scroll.getViewport().setPreferredSize(prefSize);
        scroll.getViewport().setMinimumSize(prefSize);
        res.add(scroll);
        return res;
    }

}

