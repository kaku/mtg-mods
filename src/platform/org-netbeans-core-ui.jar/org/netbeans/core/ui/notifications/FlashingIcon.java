/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.core.ui.notifications;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JToolTip;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import org.netbeans.core.ui.notifications.BalloonManager;
import org.netbeans.core.ui.notifications.NotificationDisplayerImpl;
import org.netbeans.core.ui.notifications.NotificationImpl;
import org.netbeans.core.ui.notifications.PopupList;
import org.openide.util.RequestProcessor;

class FlashingIcon
extends JLabel
implements MouseListener,
PropertyChangeListener {
    protected int STOP_FLASHING_DELAY = 5000;
    protected int DISAPPEAR_DELAY_MILLIS = this.STOP_FLASHING_DELAY + 50000;
    protected int FLASHING_FREQUENCY = 500;
    private boolean keepRunning = false;
    private boolean isIconVisible = false;
    private boolean keepFlashing = true;
    private long startTime = 0;
    private RequestProcessor.Task timerTask;
    private NotificationImpl currentNotification;

    protected FlashingIcon() {
        this.addMouseListener(this);
        this.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 3));
    }

    @Override
    public void addNotify() {
        super.addNotify();
        NotificationDisplayerImpl displayer = NotificationDisplayerImpl.getInstance();
        int notificationCount = displayer.size();
        this.setText(notificationCount > 1 ? String.valueOf(notificationCount) : null);
        this.currentNotification = displayer.getTopNotification();
        if (null != this.currentNotification) {
            this.setIcon(this.currentNotification.getIcon());
            this.setToolTipText(this.currentNotification.getTitle());
        }
        this.setVisible(displayer.size() > 0);
        displayer.addPropertyChangeListener(this);
    }

    @Override
    public void removeNotify() {
        NotificationDisplayerImpl displayer = NotificationDisplayerImpl.getInstance();
        if (displayer != null) {
            displayer.removePropertyChangeListener(this);
        }
        this.currentNotification = null;
        super.removeNotify();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void startFlashing() {
        FlashingIcon flashingIcon = this;
        synchronized (flashingIcon) {
            this.startTime = System.currentTimeMillis();
            this.isIconVisible = !this.isIconVisible;
            this.keepRunning = true;
            this.keepFlashing = true;
            if (null == this.timerTask) {
                this.timerTask = RequestProcessor.getDefault().post((Runnable)new Timer());
            } else {
                this.timerTask.run();
            }
            this.setVisible(true);
        }
        this.repaint();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void disappear() {
        FlashingIcon flashingIcon = this;
        synchronized (flashingIcon) {
            this.keepRunning = false;
            this.isIconVisible = false;
            this.keepFlashing = false;
            if (null != this.timerTask) {
                this.timerTask.cancel();
            }
            this.timerTask = null;
            this.setToolTipText(null);
            this.setVisible(false);
        }
        this.repaint();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void stopFlashing() {
        FlashingIcon flashingIcon = this;
        synchronized (flashingIcon) {
            if (this.keepRunning && !this.isIconVisible) {
                this.isIconVisible = true;
                this.repaint();
            }
        }
        this.keepFlashing = false;
        this.isIconVisible = true;
    }

    protected void flashIcon() {
        this.isIconVisible = !this.isIconVisible;
        this.invalidate();
        this.revalidate();
        this.repaint();
    }

    @Override
    public void setIcon(Icon icon) {
        if (null != icon) {
            icon = new MyIcon(icon);
            this.isIconVisible = true;
        }
        super.setIcon(icon);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        this.stopFlashing();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        this.stopFlashing();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        this.stopFlashing();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (this.isIconVisible) {
            this.onMouseClick();
        }
    }

    protected void onMouseClick() {
        PopupList.show(this);
    }

    protected void timeout() {
    }

    @Override
    public Cursor getCursor() {
        if (this.isIconVisible) {
            return Cursor.getPredefinedCursor(12);
        }
        return Cursor.getDefaultCursor();
    }

    @Override
    public Point getToolTipLocation(MouseEvent event) {
        JToolTip tip = this.createToolTip();
        tip.setTipText(this.getToolTipText());
        Dimension d = tip.getPreferredSize();
        Point retValue = new Point(this.getWidth() - d.width, - d.height);
        return retValue;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("notificationAdded".equals(evt.getPropertyName())) {
            NotificationImpl ni = (NotificationImpl)evt.getNewValue();
            this.setNotification(ni, ni.showBallon());
            PopupList.dismiss();
        } else if ("notificationRemoved".equals(evt.getPropertyName())) {
            NotificationImpl removed = (NotificationImpl)evt.getNewValue();
            if (removed.equals(this.currentNotification)) {
                NotificationImpl top = NotificationDisplayerImpl.getInstance().getTopNotification();
                this.setNotification(top, false);
                BalloonManager.dismiss();
                this.stopFlashing();
            } else {
                int notificationCount = NotificationDisplayerImpl.getInstance().size();
                this.setText(notificationCount > 1 ? String.valueOf(notificationCount) : null);
            }
        }
    }

    private boolean canShowBalloon() {
        return !Boolean.getBoolean("nb.notification.balloon.disable");
    }

    private void setNotification(final NotificationImpl n, boolean showBalloon) {
        NotificationDisplayerImpl displayer = NotificationDisplayerImpl.getInstance();
        int notificationCount = displayer.size();
        this.setText(notificationCount > 1 ? String.valueOf(notificationCount) : null);
        this.currentNotification = n;
        if (null != this.currentNotification) {
            this.setIcon(this.currentNotification.getIcon());
            this.setToolTipText(this.currentNotification.getTitle());
            if (showBalloon) {
                if (this.canShowBalloon()) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            if (null == FlashingIcon.this.currentNotification || null == FlashingIcon.this.currentNotification.getBalloonComp()) {
                                return;
                            }
                            BalloonManager.show(FlashingIcon.this, FlashingIcon.this.currentNotification.getBalloonComp(), null, new ActionListener(){

                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    n.clear();
                                }
                            }, 3000);
                        }

                    });
                } else {
                    this.startFlashing();
                }
            }
        } else {
            BalloonManager.dismiss();
            this.stopFlashing();
        }
        this.setVisible(displayer.size() > 0);
    }

    private class MyIcon
    implements Icon {
        private Icon orig;

        public MyIcon(Icon orig) {
            this.orig = orig;
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            if (FlashingIcon.this.isIconVisible) {
                this.orig.paintIcon(c, g, x, y);
            }
        }

        @Override
        public int getIconWidth() {
            return this.orig.getIconWidth();
        }

        @Override
        public int getIconHeight() {
            return this.orig.getIconHeight();
        }
    }

    private class Timer
    implements Runnable {
        private Timer() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            FlashingIcon flashingIcon = FlashingIcon.this;
            synchronized (flashingIcon) {
                long currentTime = System.currentTimeMillis();
                if (FlashingIcon.this.keepFlashing) {
                    if (currentTime - FlashingIcon.this.startTime < (long)FlashingIcon.this.STOP_FLASHING_DELAY) {
                        FlashingIcon.this.flashIcon();
                    } else {
                        FlashingIcon.this.stopFlashing();
                        if (FlashingIcon.this.DISAPPEAR_DELAY_MILLIS == -1) {
                            FlashingIcon.this.timerTask = null;
                        }
                    }
                }
                if (FlashingIcon.this.DISAPPEAR_DELAY_MILLIS > 0 && currentTime - FlashingIcon.this.startTime >= (long)FlashingIcon.this.DISAPPEAR_DELAY_MILLIS) {
                    FlashingIcon.this.timeout();
                } else if (null != FlashingIcon.this.timerTask) {
                    FlashingIcon.this.timerTask.schedule(FlashingIcon.this.FLASHING_FREQUENCY);
                }
            }
        }
    }

}

