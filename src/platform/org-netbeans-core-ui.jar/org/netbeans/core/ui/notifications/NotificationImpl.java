/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Notification
 *  org.openide.awt.NotificationDisplayer
 *  org.openide.awt.NotificationDisplayer$Priority
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.core.ui.notifications;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.CharConversionException;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.core.ui.notifications.NotificationDisplayerImpl;
import org.netbeans.core.ui.notifications.PopupList;
import org.openide.awt.Notification;
import org.openide.awt.NotificationDisplayer;
import org.openide.xml.XMLUtil;

class NotificationImpl
extends Notification
implements Comparable<NotificationImpl> {
    private final String title;
    private final Icon icon;
    private final NotificationDisplayer.Priority priority;
    private JComponent balloonComp;
    private JComponent popupComponent;
    private String detailsText;
    private ActionListener al;

    NotificationImpl(String title, Icon icon, NotificationDisplayer.Priority priority) {
        this.title = title;
        this.icon = icon;
        this.priority = priority;
    }

    public void clear() {
        NotificationDisplayerImpl.getInstance().remove(this);
    }

    @Override
    public int compareTo(NotificationImpl n) {
        int res = this.priority.compareTo((Enum)n.priority);
        if (0 == res) {
            res = this.title.compareTo(n.title);
        }
        return res;
    }

    public JComponent getBalloonComp() {
        return this.balloonComp;
    }

    public Icon getIcon() {
        return this.icon;
    }

    public JComponent getPopupComponent() {
        return this.popupComponent;
    }

    public String getTitle() {
        return this.title;
    }

    boolean showBallon() {
        return this.priority != NotificationDisplayer.Priority.SILENT;
    }

    void setDetails(String detailsText, ActionListener al) {
        this.detailsText = detailsText;
        this.al = al;
    }

    void setDetails(JComponent balloonComp, JComponent popupComp) {
        this.balloonComp = balloonComp;
        this.popupComponent = popupComp;
    }

    void initDecorations() {
        if (null != this.detailsText) {
            this.balloonComp = this.createDetails(this.detailsText, this.al);
            this.popupComponent = this.createDetails(this.detailsText, this.al);
        }
        JComponent titleComp = this.createTitle(this.title);
        JComponent balloon = this.createContent(this.icon, titleComp, this.balloonComp);
        balloon.setBorder(BorderFactory.createEmptyBorder(8, 5, 0, 0));
        this.balloonComp = balloon;
        titleComp = this.createTitle(this.title);
        this.popupComponent = this.createContent(this.icon, titleComp, this.popupComponent);
    }

    private JComponent createContent(Icon icon, JComponent titleComp, JComponent popupDetails) {
        JPanel panel = new JPanel(new GridBagLayout());
        panel.setOpaque(false);
        panel.add((Component)new JLabel(icon), new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, 10, 0, new Insets(3, 3, 3, 3), 0, 0));
        panel.add((Component)titleComp, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, 17, 0, new Insets(3, 3, 3, 3), 0, 0));
        panel.add((Component)popupDetails, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, 17, 0, new Insets(3, 3, 3, 3), 0, 0));
        panel.add((Component)new JLabel(), new GridBagConstraints(2, 2, 1, 1, 1.0, 1.0, 10, 0, new Insets(0, 0, 0, 0), 0, 0));
        ActionListener actionListener = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                NotificationImpl.this.clear();
                PopupList.dismiss();
            }
        };
        this.addActionListener(popupDetails, actionListener);
        return panel;
    }

    private void addActionListener(Container c, ActionListener al) {
        if (c instanceof AbstractButton) {
            ((AbstractButton)c).addActionListener(al);
        }
        for (Component child : c.getComponents()) {
            if (!(child instanceof Container)) continue;
            this.addActionListener((Container)child, al);
        }
    }

    private JComponent createTitle(String title) {
        return new JLabel("<html>" + title);
    }

    private JComponent createDetails(String text, ActionListener action) {
        if (null == action) {
            return new JLabel(text);
        }
        try {
            text = "<html><u>" + XMLUtil.toElementContent((String)text);
        }
        catch (CharConversionException ex) {
            throw new IllegalArgumentException(ex);
        }
        JPanel details = new JPanel(new GridBagLayout());
        details.setOpaque(false);
        JButton btn = new JButton(text);
        btn.setFocusable(false);
        btn.setBorder(BorderFactory.createEmptyBorder(4, 2, 4, 2));
        btn.setRolloverEnabled(false);
        btn.setBackground(details.getBackground());
        btn.setBorderPainted(false);
        btn.setFocusPainted(false);
        btn.setOpaque(false);
        btn.setContentAreaFilled(false);
        btn.putClientProperty("dontDrawBackground", Boolean.TRUE);
        btn.addActionListener(action);
        btn.setCursor(Cursor.getPredefinedCursor(12));
        btn.setForeground(UIManager.getColor("nb.core.ui.linkButtonFg"));
        details.add((Component)btn, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, 17, 0, new Insets(3, 0, 3, 0), 0, 0));
        return details;
    }

}

