/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.StatusLineElementProvider
 */
package org.netbeans.core.ui.notifications;

import java.awt.Component;
import org.netbeans.core.ui.notifications.FlashingIcon;
import org.openide.awt.StatusLineElementProvider;

public final class StatusLineElement
implements StatusLineElementProvider {
    private static FlashingIcon icon = null;

    public Component getStatusLineElement() {
        return StatusLineElement.getVisualizer();
    }

    private static Component getVisualizer() {
        if (null == icon) {
            icon = new FlashingIcon();
        }
        return icon;
    }
}

