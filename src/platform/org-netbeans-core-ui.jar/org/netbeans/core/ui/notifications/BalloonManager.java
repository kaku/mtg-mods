/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 */
package org.netbeans.core.ui.notifications;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.awt.geom.RoundRectangle2D;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ButtonUI;
import javax.swing.plaf.basic.BasicButtonUI;
import javax.swing.plaf.metal.MetalLookAndFeel;
import org.openide.util.ImageUtilities;

class BalloonManager {
    private static Balloon currentBalloon;
    private static JLayeredPane currentPane;
    private static ComponentListener listener;
    private static WindowStateListener windowListener;
    private static Window ownerWindow;

    BalloonManager() {
    }

    public static synchronized void show(JComponent owner, JComponent content, ActionListener defaultAction, ActionListener dismissAction, int timeoutMillis) {
        assert (null != owner);
        assert (null != content);
        BalloonManager.dismiss();
        currentBalloon = new Balloon(content, defaultAction, dismissAction, timeoutMillis);
        currentPane = JLayeredPane.getLayeredPaneAbove(owner);
        listener = new ComponentListener(){

            @Override
            public void componentResized(ComponentEvent e) {
                BalloonManager.dismiss();
            }

            @Override
            public void componentMoved(ComponentEvent e) {
                BalloonManager.dismiss();
            }

            @Override
            public void componentShown(ComponentEvent e) {
            }

            @Override
            public void componentHidden(ComponentEvent e) {
                BalloonManager.dismiss();
            }
        };
        windowListener = new WindowStateListener(){

            @Override
            public void windowStateChanged(WindowEvent e) {
                BalloonManager.dismiss();
            }
        };
        ownerWindow = SwingUtilities.getWindowAncestor(owner);
        if (null != ownerWindow) {
            ownerWindow.addWindowStateListener(windowListener);
        }
        currentPane.addComponentListener(listener);
        BalloonManager.configureBalloon(currentBalloon, currentPane, owner);
        currentPane.add((Component)currentBalloon, new Integer(JLayeredPane.POPUP_LAYER - 1));
    }

    public static synchronized void dismiss() {
        if (null != currentBalloon) {
            currentBalloon.setVisible(false);
            currentBalloon.stopDismissTimer();
            currentPane.remove(currentBalloon);
            currentPane.repaint();
            currentPane.removeComponentListener(listener);
            if (null != ownerWindow) {
                ownerWindow.removeWindowStateListener(windowListener);
            }
            currentBalloon.content.removeMouseListener(currentBalloon.mouseListener);
            currentBalloon = null;
            currentPane = null;
            listener = null;
            ownerWindow = null;
            windowListener = null;
        }
    }

    public static synchronized void dismissSlowly(final int timeout) {
        if (null != currentBalloon) {
            if (currentBalloon.timeoutMillis > 0) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        if (currentBalloon != null) {
                            currentBalloon.startDismissTimer(timeout);
                        }
                    }
                });
            } else {
                BalloonManager.dismiss();
            }
        }
    }

    public static synchronized void stopDismissSlowly() {
        if (null != currentBalloon && currentBalloon.timeoutMillis > 0) {
            currentBalloon.timeoutMillis = ToolTipManager.sharedInstance().getDismissDelay();
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    if (currentBalloon != null) {
                        currentBalloon.stopDismissTimer();
                    }
                }
            });
        }
    }

    private static void configureBalloon(Balloon balloon, JLayeredPane pane, JComponent ownerComp) {
        Rectangle ownerCompBounds = ownerComp.getBounds();
        ownerCompBounds = SwingUtilities.convertRectangle(ownerComp.getParent(), ownerCompBounds, pane);
        int paneWidth = pane.getWidth();
        int paneHeight = pane.getHeight();
        Dimension balloonSize = balloon.getPreferredSize();
        balloonSize.height += ARC;
        if (ownerCompBounds.x + ownerCompBounds.width + balloonSize.width < paneWidth && ownerCompBounds.y + ownerCompBounds.height + balloonSize.height + ARC < paneHeight) {
            balloon.setArrowLocation(14);
            balloon.setBounds(ownerCompBounds.x + ownerCompBounds.width - ARC / 2, ownerCompBounds.y + ownerCompBounds.height, balloonSize.width + ARC, balloonSize.height);
        } else if (ownerCompBounds.x + ownerCompBounds.width + balloonSize.width < paneWidth && ownerCompBounds.y - balloonSize.height - ARC > 0) {
            balloon.setArrowLocation(12);
            balloon.setBounds(ownerCompBounds.x + ownerCompBounds.width - ARC / 2, ownerCompBounds.y - balloonSize.height, balloonSize.width + ARC, balloonSize.height);
        } else if (ownerCompBounds.x - balloonSize.width > 0 && ownerCompBounds.y + ownerCompBounds.height + balloonSize.height + ARC < paneHeight) {
            balloon.setArrowLocation(16);
            balloon.setBounds(ownerCompBounds.x - balloonSize.width + ARC / 2, ownerCompBounds.y + ownerCompBounds.height, balloonSize.width + ARC, balloonSize.height);
        } else {
            balloon.setArrowLocation(18);
            balloon.setBounds(ownerCompBounds.x - balloonSize.width, ownerCompBounds.y - balloonSize.height, balloonSize.width + ARC, balloonSize.height);
        }
    }

    static class DismissButton
    extends JButton {
        public DismissButton() {
            this.setIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/core/ui/resources/dismiss_enabled.png", (boolean)true));
            this.setRolloverIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/core/ui/resources/dismiss_rollover.png", (boolean)true));
            this.setPressedIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/core/ui/resources/dismiss_pressed.png", (boolean)true));
            this.setBorder(BorderFactory.createEmptyBorder());
            this.setBorderPainted(false);
            this.setFocusable(false);
            this.setOpaque(false);
            this.setRolloverEnabled(true);
        }

        @Override
        public void paint(Graphics g) {
            Icon icon = null;
            icon = this.getModel().isArmed() && this.getModel().isPressed() ? this.getPressedIcon() : (this.getModel().isRollover() ? this.getRolloverIcon() : this.getIcon());
            icon.paintIcon(this, g, 0, 0);
        }

        @Override
        public void updateUI() {
            this.setUI(new BasicButtonUI());
        }
    }

    private static class Balloon
    extends JPanel {
        private static final int Y_OFFSET = 8;
        private static final int ARC = UIManager.getInt("nb.core.ui.balloon.arc");
        private static final int SHADOW_SIZE = 3;
        private JComponent content;
        private MouseListener mouseListener;
        private ActionListener defaultAction;
        private JButton btnDismiss;
        private int arrowLocation = 14;
        private float currentAlpha = 1.0f;
        private Timer dismissTimer;
        private int timeoutMillis;
        private boolean isMouseOverEffect = false;
        private static final float ALPHA_DECREMENT = 0.03f;
        private static final int DISMISS_REPAINT_REPEAT = 100;
        private static Color mouseOverGradientStartColor = null;
        private static Color mouseOverGradientFinishColor = null;
        private static Color defaultGradientStartColor = null;
        private static Color defaultGradientFinishColor = null;
        private static final boolean isMetal = UIManager.getLookAndFeel() instanceof MetalLookAndFeel;
        private static final boolean isNimbus = "Nimbus".equals(UIManager.getLookAndFeel().getID());

        public Balloon(final JComponent content, final ActionListener defaultAction, ActionListener dismissAction, final int timeoutMillis) {
            super(new GridBagLayout());
            this.content = content;
            this.defaultAction = defaultAction;
            this.timeoutMillis = timeoutMillis;
            content.setOpaque(false);
            this.btnDismiss = new DismissButton();
            this.btnDismiss.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    BalloonManager.dismiss();
                }
            });
            if (null != dismissAction) {
                this.btnDismiss.addActionListener(dismissAction);
            }
            this.add((Component)content, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, 11, 1, new Insets(0, 0, 0, 0), 0, 0));
            this.add((Component)this.btnDismiss, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, 12, 0, new Insets(7, 0, 0, 7), 0, 0));
            this.setOpaque(false);
            this.mouseListener = new MouseListener(){

                @Override
                public void mouseClicked(MouseEvent e) {
                    BalloonManager.dismiss();
                    if (null != defaultAction) {
                        defaultAction.actionPerformed(new ActionEvent(Balloon.this, 0, "", e.getWhen(), e.getModifiers()));
                    }
                }

                @Override
                public void mousePressed(MouseEvent e) {
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                    if (null != defaultAction) {
                        content.setCursor(Cursor.getPredefinedCursor(12));
                    }
                    Balloon.this.stopDismissTimer();
                    Balloon.this.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    content.setCursor(Cursor.getDefaultCursor());
                    if (Balloon.this.timeoutMillis > 0) {
                        Balloon.this.startDismissTimer(ToolTipManager.sharedInstance().getDismissDelay());
                    }
                }
            };
            content.addMouseListener(this.mouseListener);
            if (timeoutMillis > 0) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        Balloon.this.startDismissTimer(timeoutMillis);
                    }
                });
            }
            MouseAdapter mouseOverAdapter = new MouseAdapter(){

                @Override
                public void mouseEntered(MouseEvent e) {
                    Balloon.this.isMouseOverEffect = true;
                    Balloon.this.repaint();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    Balloon.this.isMouseOverEffect = false;
                    Balloon.this.repaint();
                }
            };
            this.addMouseListener(mouseOverAdapter);
            content.addMouseListener(mouseOverAdapter);
            this.btnDismiss.addMouseListener(mouseOverAdapter);
            this.handleMouseOver(content, mouseOverAdapter);
        }

        private void handleMouseOver(Container c, MouseListener ml) {
            c.addMouseListener(ml);
            for (Component child : c.getComponents()) {
                child.addMouseListener(ml);
                if (!(child instanceof Container)) continue;
                this.handleMouseOver((Container)child, ml);
            }
        }

        synchronized void startDismissTimer(int timeout) {
            this.stopDismissTimer();
            this.currentAlpha = 1.0f;
            this.dismissTimer = new Timer(100, new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    Balloon.access$624(Balloon.this, 0.03f);
                    if (Balloon.this.currentAlpha <= 0.03f) {
                        Balloon.this.stopDismissTimer();
                        BalloonManager.dismiss();
                    }
                    Balloon.this.repaint();
                }
            });
            this.dismissTimer.setInitialDelay(timeout);
            this.dismissTimer.start();
        }

        synchronized void stopDismissTimer() {
            if (null != this.dismissTimer) {
                this.dismissTimer.stop();
                this.dismissTimer = null;
                this.currentAlpha = 1.0f;
            }
        }

        void setArrowLocation(int arrowLocation) {
            this.arrowLocation = arrowLocation;
            if (arrowLocation == 12 || arrowLocation == 18) {
                this.setBorder(BorderFactory.createEmptyBorder(0, 0, 8, this.btnDismiss.getWidth()));
            } else {
                this.setBorder(BorderFactory.createEmptyBorder(8, 0, 0, this.btnDismiss.getWidth()));
            }
        }

        private Shape getMask(int w, int h) {
            --w;
            w -= 3;
            GeneralPath path = new GeneralPath();
            Area area = null;
            switch (this.arrowLocation) {
                case 14: {
                    area = new Area(new RoundRectangle2D.Float(0.0f, 8.0f, w, h - 8 - 3, ARC, ARC));
                    path.moveTo(ARC / 2, 0.0f);
                    path.lineTo(ARC / 2, 8.0f);
                    path.lineTo(ARC / 2 + 8, 8.0f);
                    break;
                }
                case 12: {
                    area = new Area(new RoundRectangle2D.Float(0.0f, 3.0f, w, h - 8 - 3, ARC, ARC));
                    path.moveTo(ARC / 2, h - 1);
                    path.lineTo(ARC / 2, h - 1 - 8);
                    path.lineTo(ARC / 2 + 8, h - 1 - 8);
                    break;
                }
                case 16: {
                    area = new Area(new RoundRectangle2D.Float(0.0f, 8.0f, w, h - 8 - 3, ARC, ARC));
                    path.moveTo(w - ARC / 2, 0.0f);
                    path.lineTo(w - ARC / 2, 8.0f);
                    path.lineTo(w - ARC / 2 - 8, 8.0f);
                    break;
                }
                case 18: {
                    area = new Area(new RoundRectangle2D.Float(0.0f, 3.0f, w, h - 8 - 3, ARC, ARC));
                    path.moveTo(w - ARC / 2, h - 1);
                    path.lineTo(w - ARC / 2 - 8, h - 1 - 8);
                    path.lineTo(w - ARC / 2, h - 1 - 8);
                }
            }
            path.closePath();
            area.add(new Area(path));
            return area;
        }

        private Shape getShadowMask(Shape parentMask) {
            Area area = new Area(parentMask);
            AffineTransform tx = new AffineTransform();
            tx.translate(3.0, 3.0);
            area.transform(tx);
            area.subtract(new Area(parentMask));
            return area;
        }

        @Override
        protected void paintBorder(Graphics g) {
        }

        @Override
        protected void paintComponent(Graphics g) {
            Graphics2D g2d = (Graphics2D)g;
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            Composite oldC = g2d.getComposite();
            Shape s = this.getMask(this.getWidth(), this.getHeight());
            g2d.setComposite(AlphaComposite.getInstance(3, 0.25f * this.currentAlpha));
            g2d.setColor(Color.black);
            g2d.fill(this.getShadowMask(s));
            g2d.setColor(UIManager.getColor("ToolTip.background"));
            g2d.setComposite(AlphaComposite.getInstance(3, this.currentAlpha));
            Point p1 = s.getBounds().getLocation();
            Point2D.Double p2 = new Point2D.Double(p1.getX(), p1.getY() + s.getBounds().getHeight());
            if (this.isMouseOverEffect) {
                g2d.setPaint(new GradientPaint(p2, Balloon.getMouseOverGradientStartColor(), p1, Balloon.getMouseOverGradientFinishColor()));
            } else {
                g2d.setPaint(new GradientPaint(p2, Balloon.getDefaultGradientStartColor(), p1, Balloon.getDefaultGradientFinishColor()));
            }
            g2d.fill(s);
            g2d.setColor(Color.black);
            g2d.draw(s);
            g2d.setComposite(oldC);
        }

        @Override
        protected void paintChildren(Graphics g) {
            Graphics2D g2d = (Graphics2D)g;
            Composite oldC = g2d.getComposite();
            g2d.setComposite(AlphaComposite.getInstance(3, this.currentAlpha));
            super.paintChildren(g);
            g2d.setComposite(oldC);
        }

        private static Color getMouseOverGradientStartColor() {
            if (null == mouseOverGradientStartColor && null == (Balloon.mouseOverGradientStartColor = UIManager.getColor("nb.core.ui.balloon.mouseOverGradientStartColor"))) {
                Color c;
                mouseOverGradientStartColor = new Color(224, 224, 185);
                if ((isMetal || isNimbus) && null != (c = UIManager.getColor("ToolTip.background"))) {
                    mouseOverGradientStartColor = c.darker();
                }
            }
            return mouseOverGradientStartColor;
        }

        private static Color getMouseOverGradientFinishColor() {
            if (null == mouseOverGradientFinishColor && null == (Balloon.mouseOverGradientFinishColor = UIManager.getColor("nb.core.ui.balloon.mouseOverGradientFinishColor"))) {
                Color c;
                mouseOverGradientFinishColor = new Color(255, 255, 241);
                if ((isMetal || isNimbus) && null != (c = UIManager.getColor("ToolTip.background"))) {
                    mouseOverGradientFinishColor = c.brighter();
                }
            }
            return mouseOverGradientFinishColor;
        }

        private static Color getDefaultGradientStartColor() {
            if (null == defaultGradientStartColor && null == (Balloon.defaultGradientStartColor = UIManager.getColor("nb.core.ui.balloon.defaultGradientStartColor"))) {
                Color c;
                defaultGradientStartColor = new Color(225, 225, 225);
                if ((isMetal || isNimbus) && null != (c = UIManager.getColor("ToolTip.background"))) {
                    defaultGradientStartColor = c.darker();
                }
            }
            return defaultGradientStartColor;
        }

        private static Color getDefaultGradientFinishColor() {
            if (null == defaultGradientFinishColor && null == (Balloon.defaultGradientFinishColor = UIManager.getColor("nb.core.ui.balloon.defaultGradientFinishColor"))) {
                Color c;
                defaultGradientFinishColor = new Color(255, 255, 255);
                if ((isMetal || isNimbus) && null != (c = UIManager.getColor("ToolTip.background"))) {
                    defaultGradientFinishColor = c;
                }
            }
            return defaultGradientFinishColor;
        }

        static /* synthetic */ float access$624(Balloon x0, float x1) {
            return x0.currentAlpha -= x1;
        }

    }

}

