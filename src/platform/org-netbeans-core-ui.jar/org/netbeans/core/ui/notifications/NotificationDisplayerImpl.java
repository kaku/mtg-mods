/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Notification
 *  org.openide.awt.NotificationDisplayer
 *  org.openide.awt.NotificationDisplayer$Priority
 *  org.openide.util.Lookup
 *  org.openide.util.Parameters
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.core.ui.notifications;

import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.CharConversionException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.netbeans.core.ui.notifications.NotificationImpl;
import org.openide.awt.Notification;
import org.openide.awt.NotificationDisplayer;
import org.openide.util.Lookup;
import org.openide.util.Parameters;
import org.openide.xml.XMLUtil;

public final class NotificationDisplayerImpl
extends NotificationDisplayer {
    static final String PROP_NOTIFICATION_ADDED = "notificationAdded";
    static final String PROP_NOTIFICATION_REMOVED = "notificationRemoved";
    private static final List<NotificationImpl> model = new LinkedList<NotificationImpl>();
    private static final PropertyChangeSupport propSupport = new PropertyChangeSupport(NotificationDisplayerImpl.class);

    static NotificationDisplayerImpl getInstance() {
        return (NotificationDisplayerImpl)((Object)Lookup.getDefault().lookup(NotificationDisplayerImpl.class));
    }

    public Notification notify(String title, Icon icon, String detailsText, ActionListener detailsAction, NotificationDisplayer.Priority priority) {
        Parameters.notNull((CharSequence)"detailsText", (Object)detailsText);
        NotificationImpl n = this.createNotification(title, icon, priority);
        n.setDetails(detailsText, detailsAction);
        this.add(n);
        return n;
    }

    public Notification notify(String title, Icon icon, JComponent balloonDetails, JComponent popupDetails, NotificationDisplayer.Priority priority) {
        Parameters.notNull((CharSequence)"balloonDetails", (Object)balloonDetails);
        Parameters.notNull((CharSequence)"popupDetails", (Object)popupDetails);
        NotificationImpl n = this.createNotification(title, icon, priority);
        n.setDetails(balloonDetails, popupDetails);
        this.add(n);
        return n;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void add(NotificationImpl n) {
        List<NotificationImpl> list = model;
        synchronized (list) {
            model.add(n);
            Collections.sort(model);
        }
        this.firePropertyChange("notificationAdded", n);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void remove(NotificationImpl n) {
        List<NotificationImpl> list = model;
        synchronized (list) {
            if (!model.contains(n)) {
                return;
            }
            model.remove(n);
        }
        this.firePropertyChange("notificationRemoved", n);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    int size() {
        List<NotificationImpl> list = model;
        synchronized (list) {
            return model.size();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    List<NotificationImpl> getNotifications() {
        ArrayList<NotificationImpl> res = null;
        List<NotificationImpl> list = model;
        synchronized (list) {
            res = new ArrayList<NotificationImpl>(model);
        }
        return res;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    NotificationImpl getTopNotification() {
        NotificationImpl res = null;
        List<NotificationImpl> list = model;
        synchronized (list) {
            if (!model.isEmpty()) {
                res = model.get(0);
            }
        }
        return res;
    }

    void addPropertyChangeListener(PropertyChangeListener l) {
        propSupport.addPropertyChangeListener(l);
    }

    void removePropertyChangeListener(PropertyChangeListener l) {
        propSupport.removePropertyChangeListener(l);
    }

    private void firePropertyChange(final String propName, final NotificationImpl notification) {
        Runnable r = new Runnable(){

            @Override
            public void run() {
                if ("notificationAdded".equals(propName)) {
                    notification.initDecorations();
                }
                propSupport.firePropertyChange(propName, null, notification);
            }
        };
        if (SwingUtilities.isEventDispatchThread()) {
            r.run();
        } else {
            SwingUtilities.invokeLater(r);
        }
    }

    private NotificationImpl createNotification(String title, Icon icon, NotificationDisplayer.Priority priority) {
        Parameters.notNull((CharSequence)"title", (Object)title);
        Parameters.notNull((CharSequence)"icon", (Object)icon);
        Parameters.notNull((CharSequence)"priority", (Object)priority);
        try {
            title = XMLUtil.toElementContent((String)title);
        }
        catch (CharConversionException ex) {
            throw new IllegalArgumentException(ex);
        }
        return new NotificationImpl(title, icon, priority);
    }

}

