/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.ProxySettings
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.core.ui.options.general;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.URL;
import java.net.URLConnection;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import org.netbeans.core.ProxySettings;
import org.netbeans.core.ui.options.general.GeneralOptionsPanel;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;

class GeneralOptionsModel {
    private static final Logger LOGGER = Logger.getLogger(GeneralOptionsModel.class.getName());
    private static final String NON_PROXY_HOSTS_DELIMITER = "|";
    private static final RequestProcessor rp = new RequestProcessor(GeneralOptionsModel.class);

    GeneralOptionsModel() {
    }

    private static Preferences getProxyPreferences() {
        return NbPreferences.root().node("org/netbeans/core");
    }

    boolean getUsageStatistics() {
        String key = System.getProperty("nb.show.statistics.ui");
        if (key != null) {
            return GeneralOptionsModel.getProxyPreferences().getBoolean(key, Boolean.FALSE);
        }
        return false;
    }

    void setUsageStatistics(boolean use) {
        String key = System.getProperty("nb.show.statistics.ui");
        if (key != null && use != this.getUsageStatistics()) {
            GeneralOptionsModel.getProxyPreferences().putBoolean(key, use);
        }
    }

    int getProxyType() {
        return GeneralOptionsModel.getProxyPreferences().getInt("proxyType", 1);
    }

    void setProxyType(int proxyType) {
        if (proxyType != this.getProxyType()) {
            if (1 == proxyType) {
                GeneralOptionsModel.getProxyPreferences().putInt("proxyType", GeneralOptionsModel.usePAC() ? 3 : 1);
            } else {
                GeneralOptionsModel.getProxyPreferences().putInt("proxyType", proxyType);
            }
        }
    }

    String getHttpProxyHost() {
        return ProxySettings.getHttpHost();
    }

    void setHttpProxyHost(String proxyHost) {
        if (!proxyHost.equals(this.getHttpProxyHost())) {
            GeneralOptionsModel.getProxyPreferences().put("proxyHttpHost", proxyHost);
        }
    }

    String getHttpProxyPort() {
        return ProxySettings.getHttpPort();
    }

    void setHttpProxyPort(String proxyPort) {
        if (!proxyPort.equals(this.getHttpProxyPort())) {
            GeneralOptionsModel.getProxyPreferences().put("proxyHttpPort", GeneralOptionsModel.validatePort(proxyPort) ? proxyPort : "");
        }
    }

    String getHttpsProxyHost() {
        return ProxySettings.getHttpsHost();
    }

    void setHttpsProxyHost(String proxyHost) {
        if (!proxyHost.equals(this.getHttpsProxyHost())) {
            GeneralOptionsModel.getProxyPreferences().put("proxyHttpsHost", proxyHost);
        }
    }

    String getHttpsProxyPort() {
        return ProxySettings.getHttpsPort();
    }

    void setHttpsProxyPort(String proxyPort) {
        if (!proxyPort.equals(this.getHttpsProxyPort())) {
            GeneralOptionsModel.getProxyPreferences().put("proxyHttpsPort", GeneralOptionsModel.validatePort(proxyPort) ? proxyPort : "");
        }
    }

    String getSocksHost() {
        return ProxySettings.getSocksHost();
    }

    void setSocksHost(String socksHost) {
        if (!socksHost.equals(this.getSocksHost())) {
            GeneralOptionsModel.getProxyPreferences().put("proxySocksHost", socksHost);
        }
    }

    String getSocksPort() {
        return ProxySettings.getSocksPort();
    }

    void setSocksPort(String socksPort) {
        if (!socksPort.equals(this.getSocksPort())) {
            GeneralOptionsModel.getProxyPreferences().put("proxySocksPort", GeneralOptionsModel.validatePort(socksPort) ? socksPort : "");
        }
    }

    String getOriginalHttpsHost() {
        return GeneralOptionsModel.getProxyPreferences().get("proxyHttpsHost", "");
    }

    String getOriginalHttpsPort() {
        return GeneralOptionsModel.getProxyPreferences().get("proxyHttpsPort", "");
    }

    String getOriginalSocksHost() {
        return GeneralOptionsModel.getProxyPreferences().get("proxySocksHost", "");
    }

    String getOriginalSocksPort() {
        return GeneralOptionsModel.getProxyPreferences().get("proxySocksPort", "");
    }

    String getNonProxyHosts() {
        return GeneralOptionsModel.code2view(ProxySettings.getNonProxyHosts());
    }

    void setNonProxyHosts(String nonProxy) {
        if (!nonProxy.equals(this.getNonProxyHosts())) {
            GeneralOptionsModel.getProxyPreferences().put("proxyNonProxyHosts", GeneralOptionsModel.view2code(nonProxy));
        }
    }

    boolean useProxyAuthentication() {
        return ProxySettings.useAuthentication();
    }

    void setUseProxyAuthentication(boolean use) {
        if (use != this.useProxyAuthentication()) {
            GeneralOptionsModel.getProxyPreferences().putBoolean("useProxyAuthentication", use);
        }
    }

    boolean useProxyAllProtocols() {
        return ProxySettings.useProxyAllProtocols();
    }

    void setUseProxyAllProtocols(boolean use) {
        if (use != this.useProxyAllProtocols()) {
            GeneralOptionsModel.getProxyPreferences().putBoolean("useProxyAllProtocols", use);
        }
    }

    String getProxyAuthenticationUsername() {
        return ProxySettings.getAuthenticationUsername();
    }

    void setAuthenticationUsername(String username) {
        GeneralOptionsModel.getProxyPreferences().put("proxyAuthenticationUsername", username);
    }

    char[] getProxyAuthenticationPassword() {
        return ProxySettings.getAuthenticationPassword();
    }

    void setAuthenticationPassword(char[] password) {
        ProxySettings.setAuthenticationPassword((char[])password);
    }

    static boolean usePAC() {
        String pacUrl = GeneralOptionsModel.getProxyPreferences().get("systemPAC", "");
        return pacUrl != null && pacUrl.length() > 0;
    }

    static void testConnection(final GeneralOptionsPanel panel, final int proxyType, final String proxyHost, final String proxyPortString, final String nonProxyHosts) {
        rp.post(new Runnable(){

            @Override
            public void run() {
                GeneralOptionsModel.testProxy(panel, proxyType, proxyHost, proxyPortString, nonProxyHosts);
            }
        });
    }

    private static void testProxy(GeneralOptionsPanel panel, int proxyType, String proxyHost, String proxyPortString, String nonProxyHosts) {
        Proxy testingProxy;
        String testingUrlHost;
        URL testingUrl;
        panel.updateTestConnectionStatus(TestingStatus.WAITING, null);
        TestingStatus status = TestingStatus.FAILED;
        String message = null;
        try {
            testingUrl = new URL("http://netbeans.org");
            testingUrlHost = testingUrl.getHost();
        }
        catch (MalformedURLException ex) {
            LOGGER.log(Level.SEVERE, "Cannot create url from string.", ex);
            panel.updateTestConnectionStatus(status, message);
            return;
        }
        switch (proxyType) {
            case 0: {
                testingProxy = Proxy.NO_PROXY;
                break;
            }
            case 1: 
            case 3: {
                nonProxyHosts = ProxySettings.getSystemNonProxyHosts();
                if (GeneralOptionsModel.isNonProxy(testingUrlHost, nonProxyHosts)) {
                    testingProxy = Proxy.NO_PROXY;
                    break;
                }
                String host = ProxySettings.getTestSystemHttpHost();
                if (host == null || host.isEmpty()) {
                    testingProxy = Proxy.NO_PROXY;
                    break;
                }
                int port = 0;
                try {
                    port = Integer.valueOf(ProxySettings.getTestSystemHttpPort());
                }
                catch (NumberFormatException ex) {
                    LOGGER.log(Level.INFO, "Cannot parse port number", ex);
                }
                testingProxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(host, port));
                break;
            }
            case 2: {
                nonProxyHosts = GeneralOptionsModel.view2code(nonProxyHosts);
                if (GeneralOptionsModel.isNonProxy(testingUrl.getHost(), nonProxyHosts)) {
                    testingProxy = Proxy.NO_PROXY;
                    break;
                }
                try {
                    int proxyPort = Integer.valueOf(proxyPortString);
                    testingProxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort));
                    break;
                }
                catch (NumberFormatException ex) {
                    LOGGER.log(Level.INFO, "Cannot parse port number", ex);
                    status = TestingStatus.FAILED;
                    message = NbBundle.getMessage(GeneralOptionsModel.class, (String)"LBL_GeneralOptionsPanel_PortError");
                    panel.updateTestConnectionStatus(status, message);
                    return;
                }
            }
            default: {
                testingProxy = Proxy.NO_PROXY;
            }
        }
        try {
            status = GeneralOptionsModel.testHttpConnection(testingUrl, testingProxy) ? TestingStatus.OK : TestingStatus.FAILED;
        }
        catch (IOException ex) {
            LOGGER.log(Level.INFO, "Cannot connect via http protocol.", ex);
            message = ex.getLocalizedMessage();
        }
        panel.updateTestConnectionStatus(status, message);
    }

    private static boolean testHttpConnection(URL url, Proxy proxy) throws IOException {
        boolean result = false;
        HttpURLConnection httpConnection = (HttpURLConnection)url.openConnection(proxy);
        httpConnection.setConnectTimeout(5000);
        httpConnection.connect();
        if (httpConnection.getResponseCode() == 200 || httpConnection.getResponseCode() == 302) {
            result = true;
        }
        httpConnection.disconnect();
        return result;
    }

    private static boolean isNonProxy(String host, String nonProxyHosts) {
        boolean isNonProxy = false;
        if (host != null && nonProxyHosts != null) {
            StringTokenizer st = new StringTokenizer(nonProxyHosts, "|", false);
            while (st.hasMoreTokens()) {
                if (!st.nextToken().equals(host)) continue;
                isNonProxy = true;
                break;
            }
        }
        return isNonProxy;
    }

    private static boolean validatePort(String port) {
        boolean ok;
        block3 : {
            if (port.trim().length() == 0) {
                return true;
            }
            ok = false;
            try {
                Integer.parseInt(port);
                ok = true;
            }
            catch (NumberFormatException nfe) {
                if ($assertionsDisabled) break block3;
                throw new AssertionError(nfe);
            }
        }
        return ok;
    }

    private static String code2view(String code) {
        return code == null ? code : code.replace("|", ", ");
    }

    private static String view2code(String view) {
        return view == null ? view : view.replace(", ", "|");
    }

    static enum TestingStatus {
        OK,
        FAILED,
        WAITING,
        NOT_TESTED;
        

        private TestingStatus() {
        }
    }

}

