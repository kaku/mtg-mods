/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.ui.options.filetypes;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.ui.options.filetypes.FileAssociationsModel;
import org.netbeans.core.ui.options.filetypes.NewExtensionPanel;
import org.netbeans.core.ui.options.filetypes.OpenAsPanel;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public final class OpenAsAction
implements ActionListener {
    private static final Logger LOGGER = Logger.getLogger(OpenAsAction.class.getName());
    private final DataObject dob;

    public OpenAsAction(DataObject obj) {
        this.dob = obj;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        String mimeType;
        OpenAsPanel openAsPanel = new OpenAsPanel();
        FileAssociationsModel model = new FileAssociationsModel();
        openAsPanel.setModel(model);
        FileObject fo = this.dob.getPrimaryFile();
        String extension = fo.getExt();
        openAsPanel.setExtension(extension);
        String openLabel = NbBundle.getMessage(NewExtensionPanel.class, (String)"OpenAsPanel.open");
        DialogDescriptor dd = new DialogDescriptor((Object)openAsPanel, NbBundle.getMessage(OpenAsPanel.class, (String)"OpenAsPanel.title"), true, new Object[]{openLabel, DialogDescriptor.CANCEL_OPTION}, (Object)openLabel, 0, null, null);
        DialogDisplayer.getDefault().createDialog(dd).setVisible(true);
        if (openLabel.equals(dd.getValue()) && (mimeType = openAsPanel.getMimeType()) != null) {
            this.dob.addPropertyChangeListener(new PropertyChangeListener(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    block7 : {
                        if (evt.getPropertyName().equals("valid") && !((Boolean)evt.getNewValue()).booleanValue()) {
                            LOGGER.log(Level.FINE, "PROP_VALID {0} - {1}", new Object[]{evt.getNewValue(), evt});
                            try {
                                OpenCookie openCookie = (OpenCookie)DataObject.find((FileObject)OpenAsAction.this.dob.getPrimaryFile()).getCookie(OpenCookie.class);
                                if (openCookie != null) {
                                    openCookie.open();
                                }
                                break block7;
                            }
                            catch (DataObjectNotFoundException ex) {
                                LOGGER.log(Level.INFO, null, (Throwable)ex);
                            }
                            finally {
                                OpenAsAction.this.dob.removePropertyChangeListener((PropertyChangeListener)this);
                            }
                        }
                        OpenAsAction.this.dob.removePropertyChangeListener((PropertyChangeListener)this);
                    }
                }
            });
            model.setMimeType(extension, mimeType);
            model.store();
            try {
                this.dob.setValid(false);
            }
            catch (PropertyVetoException ex) {
                LOGGER.log(Level.INFO, "Can't convert", ex);
                NotifyDescriptor.Message nd = new NotifyDescriptor.Message((Object)NbBundle.getMessage(OpenAsAction.class, (String)"ERR_CantConvert", (Object)fo.getPath(), (Object)mimeType), 0);
                DialogDisplayer.getDefault().notify((NotifyDescriptor)nd);
                return;
            }
            try {
                OpenCookie openCookie = (OpenCookie)DataObject.find((FileObject)fo).getLookup().lookup(OpenCookie.class);
                if (openCookie != null) {
                    openCookie.open();
                    return;
                }
            }
            catch (DataObjectNotFoundException ex) {
                LOGGER.log(Level.INFO, null, (Throwable)ex);
            }
            NotifyDescriptor.Message nd = new NotifyDescriptor.Message((Object)NbBundle.getMessage(OpenAsAction.class, (String)"ERR_CantOpen", (Object)fo.getPath()), 0);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)nd);
        }
    }

}

