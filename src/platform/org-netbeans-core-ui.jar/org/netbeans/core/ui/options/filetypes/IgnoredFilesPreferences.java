/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.util.NbPreferences
 */
package org.netbeans.core.ui.options.filetypes;

import java.util.prefs.Preferences;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.NbPreferences;

final class IgnoredFilesPreferences {
    private static final String PROP_IGNORED_FILES = "IgnoredFiles";
    static final String DEFAULT_IGNORED_FILES = "^(CVS|SCCS|vssver.?\\.scc|#.*#|%.*%|_svn)$|~$|^\\.(?!(htaccess|git.+|hgignore)$).*$";
    private static String syntaxError;

    private IgnoredFilesPreferences() {
    }

    private static Preferences getPreferences() {
        return NbPreferences.root().node("org/netbeans/core");
    }

    static String getIgnoredFiles() {
        return IgnoredFilesPreferences.getPreferences().get("IgnoredFiles", "^(CVS|SCCS|vssver.?\\.scc|#.*#|%.*%|_svn)$|~$|^\\.(?!(htaccess|git.+|hgignore)$).*$");
    }

    static void setIgnoredFiles(String ignoredFiles) {
        if (IgnoredFilesPreferences.isValid(ignoredFiles)) {
            IgnoredFilesPreferences.getPreferences().put("IgnoredFiles", ignoredFiles);
        } else {
            NotifyDescriptor.Message descriptor = new NotifyDescriptor.Message((Object)syntaxError);
            DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)descriptor);
        }
    }

    static boolean isValid(String ignoredFiles) {
        try {
            Pattern.compile(ignoredFiles);
        }
        catch (PatternSyntaxException e) {
            syntaxError = e.getLocalizedMessage();
            return false;
        }
        syntaxError = null;
        return true;
    }

    static String getSyntaxError() {
        return syntaxError;
    }
}

