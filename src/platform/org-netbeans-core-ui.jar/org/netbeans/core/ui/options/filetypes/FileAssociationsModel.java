/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$Status
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.MIMEResolver
 *  org.openide.filesystems.MIMEResolver$UIHelpers
 *  org.openide.util.Exceptions
 */
package org.netbeans.core.ui.options.filetypes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Logger;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.MIMEResolver;
import org.openide.util.Exceptions;

final class FileAssociationsModel
extends MIMEResolver.UIHelpers {
    private static final String MIME_RESOLVERS_PATH = "Services/MIMEResolver";
    private static final Logger LOGGER = Logger.getLogger(FileAssociationsModel.class.getName());
    private HashMap<String, String> extensionToMimeAll = new HashMap();
    private HashMap<String, String> extensionToMimeSystem = new HashMap();
    private HashMap<String, String> extensionToMimeUser = new HashMap();
    private TreeSet<String> mimeTypes = new TreeSet();
    private HashMap<String, MimeItem> mimeToItem = new HashMap();
    private HashMap<String, String> modifiedExtensionToMimeAll = new HashMap();
    private boolean initialized = false;
    private final FileChangeListener mimeResolversListener;

    FileAssociationsModel() {
        super(new MIMEResolver(){

            public String findMIMEType(FileObject fo) {
                return null;
            }
        });
        this.mimeResolversListener = new FileChangeAdapter(){

            public void fileDeleted(FileEvent fe) {
                FileAssociationsModel.this.initialized = false;
            }

            public void fileRenamed(FileRenameEvent fe) {
                FileAssociationsModel.this.initialized = false;
            }

            public void fileDataCreated(FileEvent fe) {
                FileAssociationsModel.this.initialized = false;
            }

            public void fileChanged(FileEvent fe) {
                FileAssociationsModel.this.initialized = false;
            }
        };
        FileObject resolvers = FileUtil.getConfigFile((String)"Services/MIMEResolver");
        if (resolvers != null) {
            resolvers.addFileChangeListener(FileUtil.weakFileChangeListener((FileChangeListener)this.mimeResolversListener, (Object)resolvers));
        }
    }

    boolean containsExtension(String extension) {
        return this.extensionToMimeAll.containsKey(extension);
    }

    String getAssociatedAlso(String extension, String newMimeType) {
        StringBuilder result = new StringBuilder();
        for (String extensionKey : this.getExtensions()) {
            if (extensionKey.equals(extension) || !this.extensionToMimeAll.get(extensionKey).equals(newMimeType)) continue;
            if (result.length() != 0) {
                result.append(", ");
            }
            result.append(extensionKey);
        }
        return result.toString();
    }

    List<String> getExtensions() {
        this.init();
        ArrayList<String> list = new ArrayList<String>(this.extensionToMimeAll.keySet());
        Collections.sort(list, String.CASE_INSENSITIVE_ORDER);
        return list;
    }

    Set<String> getMimeTypes() {
        this.init();
        return this.mimeTypes;
    }

    private void readMimeTypesFromLoaders() {
        FileObject[] children = FileUtil.getConfigFile((String)"Loaders").getChildren();
        for (int i = 0; i < children.length; ++i) {
            FileObject child = children[i];
            String mime1 = child.getNameExt();
            FileObject[] subchildren = child.getChildren();
            for (int j = 0; j < subchildren.length; ++j) {
                FileObject subchild = subchildren[j];
                FileObject factoriesFO = subchild.getFileObject("Factories");
                if (factoriesFO == null || factoriesFO.getChildren().length <= 0) continue;
                this.mimeTypes.add(mime1 + "/" + subchild.getNameExt());
            }
        }
        this.mimeTypes.remove("content/unknown");
    }

    String getMimeType(String extension) {
        this.init();
        return this.extensionToMimeAll.get(extension);
    }

    MimeItem getMimeItem(String extension) {
        return this.mimeToItem.get(this.getMimeType(extension));
    }

    void remove(String extension) {
        this.extensionToMimeUser.remove(extension);
        this.extensionToMimeAll.remove(extension);
    }

    void setDefault(String extension) {
        this.remove(extension);
        this.extensionToMimeAll.put(extension, this.extensionToMimeSystem.get(extension));
    }

    boolean setMimeType(String extension, String newMimeType) {
        String oldMmimeType = this.getMimeType(extension);
        if (!newMimeType.equals(oldMmimeType)) {
            LOGGER.fine("setMimeType - " + extension + "=" + newMimeType);
            this.extensionToMimeUser.put(extension, newMimeType);
            this.extensionToMimeAll.put(extension, newMimeType);
            if (!this.modifiedExtensionToMimeAll.containsKey(extension)) {
                this.modifiedExtensionToMimeAll.put(extension, oldMmimeType);
            }
            return true;
        }
        return false;
    }

    boolean isInitialExtensionToMimeMapping(String extension, String mimeType) {
        String initialMimeType = this.modifiedExtensionToMimeAll.get(extension);
        if (initialMimeType != null && initialMimeType.equals(mimeType)) {
            this.modifiedExtensionToMimeAll.remove(extension);
            return this.modifiedExtensionToMimeAll.isEmpty();
        }
        return false;
    }

    boolean canBeRestored(String extension) {
        return this.extensionToMimeUser.containsKey(extension) && this.extensionToMimeSystem.containsKey(extension);
    }

    boolean canBeRemoved(String extension) {
        return !this.extensionToMimeSystem.containsKey(extension);
    }

    private static String getLoaderDisplayName(String mimeType) {
        FileSystem filesystem = null;
        try {
            filesystem = FileUtil.getConfigRoot().getFileSystem();
        }
        catch (FileStateInvalidException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        FileObject factoriesFO = FileUtil.getConfigFile((String)("Loaders/" + mimeType + "/Factories"));
        if (factoriesFO != null) {
            FileObject[] children;
            for (FileObject child : children = factoriesFO.getChildren()) {
                String displayName;
                String childName = child.getNameExt();
                if (childName.equals(displayName = filesystem.getStatus().annotateName(childName, Collections.singleton(child)))) continue;
                return displayName;
            }
        }
        return null;
    }

    ArrayList<MimeItem> getMimeItems() {
        this.init();
        ArrayList<MimeItem> items = new ArrayList<MimeItem>(this.mimeToItem.values());
        Collections.sort(items);
        return items;
    }

    void store() {
        this.modifiedExtensionToMimeAll.clear();
        HashMap<String, HashSet<String>> mimeToExtensions = new HashMap<String, HashSet<String>>();
        for (Map.Entry<String, String> entry : this.extensionToMimeUser.entrySet()) {
            String extension = entry.getKey();
            String mimeType = entry.getValue();
            HashSet<String> extensions = (HashSet<String>)mimeToExtensions.get(mimeType);
            if (extensions == null) {
                extensions = new HashSet<String>();
                mimeToExtensions.put(mimeType, extensions);
            }
            extensions.add(extension);
        }
        this.storeUserDefinedResolver(mimeToExtensions);
    }

    private void init() {
        if (this.initialized) {
            return;
        }
        LOGGER.fine("FileAssociationsModel.init");
        this.initialized = true;
        for (FileObject mimeResolverFO : this.getOrderedResolvers()) {
            boolean userDefined = this.isUserDefined(mimeResolverFO);
            Map mimeToExtensions = this.getMIMEToExtensions(mimeResolverFO);
            for (Map.Entry entry : mimeToExtensions.entrySet()) {
                String mimeType = (String)entry.getKey();
                Set extensions = (Set)entry.getValue();
                for (String extension : extensions) {
                    if (extension.equalsIgnoreCase("xml") && !userDefined && "text/xml".equals(this.extensionToMimeAll.get(extension))) continue;
                    this.extensionToMimeAll.put(extension, mimeType);
                    if (userDefined) {
                        this.extensionToMimeUser.put(extension, mimeType);
                        continue;
                    }
                    this.extensionToMimeSystem.put(extension, mimeType);
                }
                this.mimeTypes.add(mimeType);
            }
        }
        this.readMimeTypesFromLoaders();
        for (String mimeType : this.mimeTypes) {
            MimeItem mimeItem = new MimeItem(mimeType, FileAssociationsModel.getLoaderDisplayName(mimeType));
            this.mimeToItem.put(mimeType, mimeItem);
        }
        LOGGER.fine("extensionToMimeSystem=" + this.extensionToMimeSystem);
        LOGGER.fine("extensionToMimeUser=" + this.extensionToMimeUser);
    }

    static final class MimeItem
    implements Comparable<MimeItem> {
        String mimeType;
        String displayName;

        MimeItem(String mimeType, String displayName) {
            this.mimeType = mimeType;
            this.displayName = displayName;
        }

        String getMimeType() {
            return this.mimeType;
        }

        public String toString() {
            return this.displayName == null ? this.mimeType : this.displayName + " (" + this.mimeType + ")";
        }

        @Override
        public int compareTo(MimeItem o) {
            return this.toString().compareToIgnoreCase(o.toString());
        }
    }

}

