/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package org.netbeans.core.ui.options.filetypes;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JComponent;
import org.netbeans.core.ui.options.filetypes.FileAssociationsPanel;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public final class FileAssociationsOptionsPanelController
extends OptionsPanelController {
    private FileAssociationsPanel panel;
    private final PropertyChangeSupport pcs;
    private boolean changed;

    public FileAssociationsOptionsPanelController() {
        this.pcs = new PropertyChangeSupport((Object)this);
    }

    public void update() {
        this.getPanel().load();
        this.changed = false;
    }

    public void applyChanges() {
        if (this.isChanged()) {
            this.getPanel().store();
            this.changed = false;
        }
    }

    public void cancel() {
    }

    public boolean isValid() {
        return this.getPanel().valid();
    }

    public boolean isChanged() {
        return this.changed;
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx("netbeans.optionsDialog.advanced.files");
    }

    public JComponent getComponent(Lookup masterLookup) {
        return this.getPanel();
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    private FileAssociationsPanel getPanel() {
        if (this.panel == null) {
            this.panel = new FileAssociationsPanel(this);
        }
        return this.panel;
    }

    void changed(boolean isChanged) {
        if (!this.changed) {
            this.pcs.firePropertyChange("changed", false, true);
        }
        this.changed = isChanged;
        this.pcs.firePropertyChange("valid", null, null);
    }
}

