/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.beaninfo.editors.HtmlBrowser
 *  org.netbeans.beaninfo.editors.HtmlBrowser$FactoryEditor
 *  org.netbeans.core.ProxySettings
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotificationLineSupport
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.awt.Mnemonics
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 */
package org.netbeans.core.ui.options.general;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.netbeans.beaninfo.editors.HtmlBrowser;
import org.netbeans.core.ProxySettings;
import org.netbeans.core.ui.options.general.AdvancedProxyPanel;
import org.netbeans.core.ui.options.general.GeneralOptionsModel;
import org.netbeans.core.ui.options.general.WebBrowsersOptionsModel;
import org.netbeans.core.ui.options.general.WebBrowsersOptionsPanel;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotificationLineSupport;
import org.openide.awt.HtmlBrowser;
import org.openide.awt.Mnemonics;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

public class GeneralOptionsPanel
extends JPanel
implements ActionListener {
    private GeneralOptionsModel model;
    private HtmlBrowser.FactoryEditor editor;
    private AdvancedProxyPanel advancedPanel;
    private boolean valid = true;
    private JButton bMoreProxy;
    private JButton bReloadProxy;
    private JButton bTestConnection;
    private JComboBox cbWebBrowser;
    private JButton editBrowserButton;
    private JLabel errorLabel;
    private JPanel jPanel1;
    private JSeparator jSeparator2;
    private JSeparator jSeparator3;
    private JCheckBox jUsageCheck;
    private JLabel lProxyHost;
    private JLabel lProxyPort;
    private JLabel lUsage;
    private JLabel lWebBrowser;
    private JLabel lWebProxy;
    private JLabel lblLearnMore;
    private JLabel lblTestResult;
    private JLabel lblUsageInfo;
    private JProgressBar pbProxyWaiting;
    private JRadioButton rbHTTPProxy;
    private JRadioButton rbNoProxy;
    private JRadioButton rbUseSystemProxy;
    private JTextField tfProxyHost;
    private JTextField tfProxyPort;

    public GeneralOptionsPanel() {
        this.initComponents();
        Color nbErrorForeground = UIManager.getColor("nb.errorForeground");
        if (nbErrorForeground == null) {
            nbErrorForeground = new Color(255, 0, 0);
        }
        this.errorLabel.setForeground(nbErrorForeground);
        Image img = ImageUtilities.loadImage((String)"org/netbeans/core/ui/resources/error.gif", (boolean)true);
        this.errorLabel.setIcon(new ImageIcon(img));
        this.errorLabel.setVisible(false);
        GeneralOptionsPanel.loc(this.lWebBrowser, "Web_Browser");
        GeneralOptionsPanel.loc(this.lWebProxy, "Web_Proxy");
        GeneralOptionsPanel.loc(this.lProxyHost, "Proxy_Host");
        GeneralOptionsPanel.loc(this.lProxyPort, "Proxy_Port");
        this.cbWebBrowser.getAccessibleContext().setAccessibleName(GeneralOptionsPanel.loc("AN_Web_Browser", new String[0]));
        this.cbWebBrowser.getAccessibleContext().setAccessibleDescription(GeneralOptionsPanel.loc("AD_Web_Browser", new String[0]));
        this.tfProxyHost.getAccessibleContext().setAccessibleName(GeneralOptionsPanel.loc("AN_Host", new String[0]));
        this.tfProxyHost.getAccessibleContext().setAccessibleDescription(GeneralOptionsPanel.loc("AD_Host", new String[0]));
        this.tfProxyPort.getAccessibleContext().setAccessibleName(GeneralOptionsPanel.loc("AN_Port", new String[0]));
        this.tfProxyPort.getAccessibleContext().setAccessibleDescription(GeneralOptionsPanel.loc("AD_Port", new String[0]));
        this.rbNoProxy.addActionListener(this);
        this.rbUseSystemProxy.addActionListener(this);
        this.rbHTTPProxy.addActionListener(this);
        this.cbWebBrowser.addActionListener(this);
        this.tfProxyHost.addActionListener(this);
        this.tfProxyPort.addActionListener(this);
        this.tfProxyPort.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent e) {
                GeneralOptionsPanel.this.validatePortValue();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                GeneralOptionsPanel.this.validatePortValue();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                GeneralOptionsPanel.this.validatePortValue();
            }
        });
        ButtonGroup bgProxy = new ButtonGroup();
        bgProxy.add(this.rbNoProxy);
        bgProxy.add(this.rbUseSystemProxy);
        bgProxy.add(this.rbHTTPProxy);
        GeneralOptionsPanel.loc(this.rbNoProxy, "No_Proxy");
        GeneralOptionsPanel.loc(this.rbUseSystemProxy, "Use_System_Proxy_Settings");
        GeneralOptionsPanel.loc(this.rbHTTPProxy, "Use_HTTP_Proxy");
        GeneralOptionsPanel.loc(this.lUsage, "Usage_Statistics");
        this.lUsage.getAccessibleContext().setAccessibleDescription(GeneralOptionsPanel.loc("AD_Usage_Statistics", new String[0]));
        this.lUsage.getAccessibleContext().setAccessibleName(GeneralOptionsPanel.loc("AN_Usage_Statistics", new String[0]));
        GeneralOptionsPanel.loc(this.jUsageCheck, "Usage_Check");
        this.jUsageCheck.getAccessibleContext().setAccessibleDescription(GeneralOptionsPanel.loc("AD_Usage_Check", new String[0]));
        this.jUsageCheck.getAccessibleContext().setAccessibleName(GeneralOptionsPanel.loc("AN_Usage_Check", new String[0]));
        this.lblUsageInfo.setText(GeneralOptionsPanel.loc("CTL_Usage_Info", new String[0]));
        this.lblUsageInfo.getAccessibleContext().setAccessibleDescription(GeneralOptionsPanel.loc("AD_Usage_Info", new String[0]));
        this.lblUsageInfo.getAccessibleContext().setAccessibleName(GeneralOptionsPanel.loc("AN_Usage_Info", new String[0]));
        this.lblLearnMore.setText(GeneralOptionsPanel.loc("CTL_Learn_More", new String[0]));
        this.lblLearnMore.getAccessibleContext().setAccessibleDescription(GeneralOptionsPanel.loc("AD_Learn_More", new String[0]));
        this.lblLearnMore.getAccessibleContext().setAccessibleName(GeneralOptionsPanel.loc("AN_Learn_More", new String[0]));
        this.pbProxyWaiting.setVisible(false);
        this.rbUseSystemProxy.setToolTipText(this.getUseSystemProxyToolTip());
        if (System.getProperty("nb.show.statistics.ui") == null) {
            this.jSeparator3.setVisible(false);
            this.lUsage.setVisible(false);
            this.jUsageCheck.setVisible(false);
            this.lblUsageInfo.setVisible(false);
            this.lblLearnMore.setVisible(false);
        }
    }

    private void initComponents() {
        this.lWebBrowser = new JLabel();
        this.cbWebBrowser = new JComboBox();
        this.jSeparator2 = new JSeparator();
        this.lWebProxy = new JLabel();
        this.rbNoProxy = new JRadioButton();
        this.rbUseSystemProxy = new JRadioButton();
        this.rbHTTPProxy = new JRadioButton();
        this.lProxyHost = new JLabel();
        this.tfProxyHost = new JTextField();
        this.lProxyPort = new JLabel();
        this.tfProxyPort = new JTextField();
        this.bMoreProxy = new JButton();
        this.editBrowserButton = new JButton();
        this.jSeparator3 = new JSeparator();
        this.errorLabel = new JLabel();
        this.jPanel1 = new JPanel();
        this.lblLearnMore = new JLabel();
        this.lblUsageInfo = new JLabel();
        this.jUsageCheck = new JCheckBox();
        this.lUsage = new JLabel();
        this.bReloadProxy = new JButton();
        this.bTestConnection = new JButton();
        this.lblTestResult = new JLabel();
        this.pbProxyWaiting = new JProgressBar();
        this.lWebBrowser.setLabelFor(this.cbWebBrowser);
        Mnemonics.setLocalizedText((JLabel)this.lWebBrowser, (String)NbBundle.getMessage(GeneralOptionsPanel.class, (String)"GeneralOptionsPanel.lWebBrowser.text"));
        Mnemonics.setLocalizedText((JLabel)this.lWebProxy, (String)NbBundle.getMessage(GeneralOptionsPanel.class, (String)"LBL_GeneralOptionsPanel_lWebProxy"));
        Mnemonics.setLocalizedText((AbstractButton)this.rbNoProxy, (String)NbBundle.getMessage(GeneralOptionsPanel.class, (String)"GeneralOptionsPanel.rbNoProxy.text"));
        this.rbNoProxy.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        Mnemonics.setLocalizedText((AbstractButton)this.rbUseSystemProxy, (String)NbBundle.getMessage(GeneralOptionsPanel.class, (String)"GeneralOptionsPanel.rbUseSystemProxy.text"));
        this.rbUseSystemProxy.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        Mnemonics.setLocalizedText((AbstractButton)this.rbHTTPProxy, (String)NbBundle.getMessage(GeneralOptionsPanel.class, (String)"CTL_Use_HTTP_Proxy", (Object[])new Object[0]));
        this.rbHTTPProxy.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.lProxyHost.setLabelFor(this.tfProxyHost);
        Mnemonics.setLocalizedText((JLabel)this.lProxyHost, (String)NbBundle.getMessage(GeneralOptionsPanel.class, (String)"CTL_Proxy_Host", (Object[])new Object[0]));
        this.tfProxyHost.addFocusListener(new FocusAdapter(){

            @Override
            public void focusGained(FocusEvent evt) {
                GeneralOptionsPanel.this.tfProxyHostFocusGained(evt);
            }

            @Override
            public void focusLost(FocusEvent evt) {
                GeneralOptionsPanel.this.tfProxyHostFocusLost(evt);
            }
        });
        this.lProxyPort.setLabelFor(this.tfProxyPort);
        Mnemonics.setLocalizedText((JLabel)this.lProxyPort, (String)NbBundle.getMessage(GeneralOptionsPanel.class, (String)"CTL_Proxy_Port", (Object[])new Object[0]));
        this.tfProxyPort.setColumns(4);
        this.tfProxyPort.addFocusListener(new FocusAdapter(){

            @Override
            public void focusGained(FocusEvent evt) {
                GeneralOptionsPanel.this.tfProxyPortFocusGained(evt);
            }

            @Override
            public void focusLost(FocusEvent evt) {
                GeneralOptionsPanel.this.tfProxyPortFocusLost(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.bMoreProxy, (String)NbBundle.getMessage(GeneralOptionsPanel.class, (String)"LBL_GeneralOptionsPanel_bMoreProxy"));
        this.bMoreProxy.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                GeneralOptionsPanel.this.bMoreProxyActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.editBrowserButton, (String)NbBundle.getMessage(GeneralOptionsPanel.class, (String)"GeneralOptionsPanel.editBrowserButton.text"));
        this.editBrowserButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                GeneralOptionsPanel.this.editBrowserButtonActionPerformed(evt);
            }
        });
        this.errorLabel.setHorizontalAlignment(4);
        this.jPanel1.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this.lblLearnMore, (String)NbBundle.getMessage(GeneralOptionsPanel.class, (String)"CTL_Learn_More"));
        this.lblLearnMore.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseEntered(MouseEvent evt) {
                GeneralOptionsPanel.this.lblLearnMoreMouseEntered(evt);
            }

            @Override
            public void mousePressed(MouseEvent evt) {
                GeneralOptionsPanel.this.lblLearnMoreMousePressed(evt);
            }
        });
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 15, 0, 0);
        this.jPanel1.add((Component)this.lblLearnMore, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.lblUsageInfo, (String)"<html>The usage statistics help us better understand user\nrequirements and prioritize improvements in future releases. We will never\nreverse-engineer the collected data to find specific details about your projects.</html>");
        this.lblUsageInfo.setFocusable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 15, 5, 0);
        this.jPanel1.add((Component)this.lblUsageInfo, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.jUsageCheck, (String)"Help us improve the NetBeans IDE by providing anonymous usage data");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 15, 5, 0);
        this.jPanel1.add((Component)this.jUsageCheck, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.lUsage, (String)"Usage Statistics:");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.jPanel1.add((Component)this.lUsage, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.bReloadProxy, (String)NbBundle.getMessage(GeneralOptionsPanel.class, (String)"GeneralOptionsPanel.bReloadProxy.text"));
        this.bReloadProxy.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                GeneralOptionsPanel.this.bReloadProxyActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.bTestConnection, (String)NbBundle.getMessage(GeneralOptionsPanel.class, (String)"GeneralOptionsPanel.bTestConnection.text"));
        this.bTestConnection.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                GeneralOptionsPanel.this.bTestConnectionActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((JLabel)this.lblTestResult, (String)NbBundle.getMessage(GeneralOptionsPanel.class, (String)"GeneralOptionsPanel.lblTestResult.text"));
        this.pbProxyWaiting.setIndeterminate(true);
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addComponent(this.lWebBrowser).addGap(18, 18, 18).addComponent(this.cbWebBrowser, 0, 1317, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.editBrowserButton)).addComponent(this.jSeparator2, -1, 1495, 32767).addGroup(layout.createSequentialGroup().addComponent(this.lWebProxy).addGap(18, 18, 18).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGap(90, 90, 90).addComponent(this.errorLabel, -1, 1313, 32767)).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.rbHTTPProxy).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lProxyHost).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.tfProxyHost, -1, 1055, 32767)).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.rbNoProxy).addGroup(layout.createSequentialGroup().addComponent(this.rbUseSystemProxy).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.bReloadProxy))).addGap(0, 0, 32767))).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lProxyPort).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.tfProxyPort, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.bMoreProxy)).addGroup(layout.createSequentialGroup().addComponent(this.bTestConnection).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblTestResult).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.pbProxyWaiting, -2, 90, -2).addGap(0, 0, 32767))).addContainerGap()))).addComponent(this.jSeparator3, -1, 1495, 32767).addGroup(layout.createSequentialGroup().addComponent(this.jPanel1, -2, 0, 32767).addContainerGap()))));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lWebBrowser).addComponent(this.cbWebBrowser, -2, -1, -2).addComponent(this.editBrowserButton)).addGap(18, 18, 18).addComponent(this.jSeparator2, -2, 10, -2).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.rbNoProxy).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.rbUseSystemProxy).addComponent(this.bReloadProxy)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.rbHTTPProxy).addComponent(this.lProxyHost).addComponent(this.tfProxyHost, -2, -1, -2).addComponent(this.lProxyPort).addComponent(this.tfProxyPort, -2, -1, -2).addComponent(this.bMoreProxy))).addComponent(this.lWebProxy)).addGap(30, 30, 30).addComponent(this.errorLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(this.lblTestResult).addComponent(this.bTestConnection).addComponent(this.pbProxyWaiting, -2, 16, -2)).addGap(11, 11, 11).addComponent(this.jSeparator3, -2, 10, -2).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.jPanel1, -2, -1, -2).addContainerGap()));
        this.bMoreProxy.getAccessibleContext().setAccessibleName(NbBundle.getMessage(GeneralOptionsPanel.class, (String)"LBL_GeneralOptionsPanel_bMoreProxy.AN"));
        this.bMoreProxy.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(GeneralOptionsPanel.class, (String)"LBL_GeneralOptionsPanel_bMoreProxy.AD"));
        this.editBrowserButton.getAccessibleContext().setAccessibleName(NbBundle.getMessage(GeneralOptionsPanel.class, (String)"GeneralOptionsPanel.editBrowserButton.AN"));
        this.editBrowserButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(GeneralOptionsPanel.class, (String)"GeneralOptionsPanel.editBrowserButton.AD"));
    }

    private void editBrowserButtonActionPerformed(ActionEvent evt) {
        final WebBrowsersOptionsModel wbModel = new WebBrowsersOptionsModel();
        WebBrowsersOptionsPanel wbPanel = new WebBrowsersOptionsPanel(wbModel, this.cbWebBrowser.getSelectedItem().toString());
        DialogDescriptor dialogDesc = new DialogDescriptor((Object)wbPanel, GeneralOptionsPanel.loc("LBL_WebBrowsersPanel_Title", new String[0]), true, new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                if (DialogDescriptor.OK_OPTION.equals(e.getSource())) {
                    wbModel.applyChanges();
                } else {
                    wbModel.discardChanges();
                }
            }
        });
        dialogDesc.setHelpCtx(new HelpCtx("WebBrowsersManager"));
        DialogDisplayer.getDefault().createDialog(dialogDesc).setVisible(true);
        if (dialogDesc.getValue().equals(DialogDescriptor.OK_OPTION)) {
            this.updateWebBrowsers();
            int items = this.cbWebBrowser.getItemCount();
            for (int i = 0; i < items; ++i) {
                Object item = this.cbWebBrowser.getItemAt(i);
                if (!item.equals(wbModel.getSelectedValue())) continue;
                this.cbWebBrowser.setSelectedItem(item);
                break;
            }
        }
    }

    private void bMoreProxyActionPerformed(ActionEvent evt) {
        assert (this.model != null);
        if (this.advancedPanel == null) {
            this.advancedPanel = new AdvancedProxyPanel(this.model);
        }
        DialogDescriptor dd = new DialogDescriptor((Object)this.advancedPanel, GeneralOptionsPanel.loc("LBL_AdvancedProxyPanel_Title", new String[0]));
        this.advancedPanel.setDialogDescriptor(dd);
        dd.createNotificationLineSupport();
        this.advancedPanel.update(this.tfProxyHost.getText(), this.tfProxyPort.getText());
        DialogDisplayer.getDefault().createDialog(dd).setVisible(true);
        if (DialogDescriptor.OK_OPTION.equals(dd.getValue())) {
            this.advancedPanel.applyChanges();
            this.tfProxyHost.setText(this.model.getHttpProxyHost());
            this.tfProxyPort.setText(this.model.getHttpProxyPort());
            this.isChanged();
        }
    }

    private void tfProxyPortFocusLost(FocusEvent evt) {
        this.tfProxyPort.select(0, 0);
    }

    private void tfProxyHostFocusLost(FocusEvent evt) {
        this.tfProxyHost.select(0, 0);
    }

    private void tfProxyPortFocusGained(FocusEvent evt) {
        this.tfProxyPort.setCaretPosition(0);
        this.tfProxyPort.selectAll();
    }

    private void tfProxyHostFocusGained(FocusEvent evt) {
        this.tfProxyHost.setCaretPosition(0);
        this.tfProxyHost.selectAll();
    }

    private void lblLearnMoreMouseEntered(MouseEvent evt) {
        evt.getComponent().setCursor(Cursor.getPredefinedCursor(12));
    }

    private void lblLearnMoreMousePressed(MouseEvent evt) {
        URL u = null;
        try {
            u = new URL(GeneralOptionsPanel.loc("METRICS_INFO_URL", new String[0]));
        }
        catch (MalformedURLException exc) {
            // empty catch block
        }
        if (u != null) {
            HtmlBrowser.URLDisplayer.getDefault().showURL(u);
        }
    }

    private void bReloadProxyActionPerformed(ActionEvent evt) {
        ProxySettings.reload();
        this.rbUseSystemProxy.setToolTipText(this.getUseSystemProxyToolTip());
    }

    private void bTestConnectionActionPerformed(ActionEvent evt) {
        String port;
        String host;
        String nonProxyHosts;
        int type;
        if (this.rbNoProxy.isSelected()) {
            type = 0;
            host = null;
            port = null;
            nonProxyHosts = null;
        } else if (this.rbUseSystemProxy.isSelected()) {
            type = 1;
            host = null;
            port = null;
            nonProxyHosts = null;
        } else {
            type = 2;
            host = this.tfProxyHost.getText();
            port = this.tfProxyPort.getText();
            nonProxyHosts = this.advancedPanel == null ? null : this.advancedPanel.getNonProxyHosts();
        }
        GeneralOptionsModel.testConnection(this, type, host, port, nonProxyHosts);
    }

    private void validatePortValue() {
        this.clearError();
        boolean oldValid = this.valid;
        this.valid = this.isPortValid();
        if (!this.valid) {
            this.showError(GeneralOptionsPanel.loc("LBL_GeneralOptionsPanel_PortError", new String[0]));
        }
        if (oldValid != this.valid) {
            this.firePropertyChange("valid", oldValid, this.valid);
        }
    }

    private boolean isPortValid() {
        String port = this.tfProxyPort.getText();
        boolean portStatus = true;
        if (port != null && port.length() > 0) {
            try {
                Integer.parseInt(port);
            }
            catch (NumberFormatException nfex) {
                portStatus = false;
            }
        }
        return portStatus;
    }

    private void showError(String message) {
        this.errorLabel.setVisible(true);
        this.errorLabel.setText(message);
        this.bTestConnection.setEnabled(false);
    }

    private void clearError() {
        this.errorLabel.setText("");
        this.errorLabel.setVisible(false);
        this.bTestConnection.setEnabled(true);
    }

    private static /* varargs */ String loc(String key, String ... params) {
        return NbBundle.getMessage(GeneralOptionsPanel.class, (String)key, (Object[])params);
    }

    private String getUseSystemProxyToolTip() {
        if (this.rbUseSystemProxy.isSelected()) {
            String toolTip;
            String sHost = GeneralOptionsPanel.getProxyPreferences().get("systemProxyHttpHost", "");
            if (GeneralOptionsModel.usePAC()) {
                toolTip = GeneralOptionsPanel.getPacFile();
            } else if (sHost == null || sHost.trim().length() == 0) {
                toolTip = GeneralOptionsPanel.loc("GeneralOptionsPanel_rbUseSystemProxy_Direct", new String[0]);
            } else {
                String sPort = GeneralOptionsPanel.getProxyPreferences().get("systemProxyHttpPort", "");
                toolTip = GeneralOptionsPanel.loc("GeneralOptionsPanel_rbUseSystemProxy_Format", sHost, sPort);
            }
            return toolTip;
        }
        return null;
    }

    private static void loc(Component c, String key) {
        if (!(c instanceof JLabel)) {
            c.getAccessibleContext().setAccessibleName(GeneralOptionsPanel.loc("AN_" + key, new String[0]));
            c.getAccessibleContext().setAccessibleDescription(GeneralOptionsPanel.loc("AD_" + key, new String[0]));
        }
        if (c instanceof AbstractButton) {
            Mnemonics.setLocalizedText((AbstractButton)((AbstractButton)c), (String)GeneralOptionsPanel.loc("CTL_" + key, new String[0]));
        } else {
            Mnemonics.setLocalizedText((JLabel)((JLabel)c), (String)GeneralOptionsPanel.loc("CTL_" + key, new String[0]));
        }
    }

    void update() {
        this.model = new GeneralOptionsModel();
        switch (this.model.getProxyType()) {
            case 0: {
                this.rbNoProxy.setSelected(true);
                this.bReloadProxy.setEnabled(false);
                this.tfProxyHost.setEnabled(false);
                this.tfProxyPort.setEnabled(false);
                this.lProxyHost.setEnabled(false);
                this.lProxyPort.setEnabled(false);
                this.bMoreProxy.setEnabled(false);
                break;
            }
            case 1: {
                this.rbUseSystemProxy.setSelected(true);
                this.bReloadProxy.setEnabled(true);
                this.tfProxyHost.setEnabled(false);
                this.tfProxyPort.setEnabled(false);
                this.lProxyHost.setEnabled(false);
                this.lProxyPort.setEnabled(false);
                this.bMoreProxy.setEnabled(false);
                break;
            }
            case 2: {
                this.rbHTTPProxy.setSelected(true);
                this.bReloadProxy.setEnabled(false);
                this.tfProxyHost.setEnabled(true);
                this.tfProxyPort.setEnabled(true);
                this.lProxyHost.setEnabled(true);
                this.lProxyPort.setEnabled(true);
                this.bMoreProxy.setEnabled(true);
                break;
            }
            case 3: {
                this.rbUseSystemProxy.setSelected(true);
                this.bReloadProxy.setEnabled(true);
                this.tfProxyHost.setEnabled(false);
                this.tfProxyPort.setEnabled(false);
                this.lProxyHost.setEnabled(false);
                this.lProxyPort.setEnabled(false);
                this.bMoreProxy.setEnabled(false);
            }
        }
        this.tfProxyHost.setText(this.model.getHttpProxyHost());
        this.tfProxyPort.setText(this.model.getHttpProxyPort());
        this.rbUseSystemProxy.setToolTipText(this.getUseSystemProxyToolTip());
        this.jUsageCheck.setSelected(this.model.getUsageStatistics());
        this.updateWebBrowsers();
    }

    private void updateWebBrowsers() {
        if (this.editor == null) {
            this.editor = (HtmlBrowser.FactoryEditor)Lookup.getDefault().lookup(HtmlBrowser.FactoryEditor.class);
        }
        this.cbWebBrowser.removeAllItems();
        String[] tags = this.editor.getTags();
        if (tags.length > 0) {
            for (String tag : tags) {
                this.cbWebBrowser.addItem(tag);
            }
            this.cbWebBrowser.setSelectedItem(this.editor.getAsText());
            this.lWebBrowser.setVisible(true);
            this.cbWebBrowser.setVisible(true);
            this.editBrowserButton.setVisible(true);
            this.jSeparator2.setVisible(true);
        } else {
            this.lWebBrowser.setVisible(false);
            this.cbWebBrowser.setVisible(false);
            this.editBrowserButton.setVisible(false);
            this.jSeparator2.setVisible(false);
        }
    }

    void applyChanges() {
        if (this.model == null) {
            return;
        }
        if (this.rbNoProxy.isSelected()) {
            this.model.setProxyType(0);
        } else if (this.rbUseSystemProxy.isSelected()) {
            this.model.setProxyType(1);
        } else {
            this.model.setProxyType(2);
        }
        this.model.setHttpProxyHost(this.tfProxyHost.getText());
        this.model.setHttpProxyPort(this.tfProxyPort.getText());
        if (this.model.useProxyAllProtocols()) {
            this.model.setHttpsProxyHost(this.tfProxyHost.getText());
            this.model.setHttpsProxyPort(this.tfProxyPort.getText());
            this.model.setSocksHost(this.tfProxyHost.getText());
            this.model.setSocksPort(this.tfProxyPort.getText());
        }
        if (this.editor == null) {
            this.editor = (HtmlBrowser.FactoryEditor)Lookup.getDefault().lookup(HtmlBrowser.FactoryEditor.class);
        }
        this.editor.setAsText((String)this.cbWebBrowser.getSelectedItem());
        this.model.setUsageStatistics(this.jUsageCheck.isSelected());
    }

    void cancel() {
    }

    boolean dataValid() {
        return this.isPortValid();
    }

    boolean isChanged() {
        String browser;
        if (this.model == null) {
            return false;
        }
        if (this.editor == null) {
            this.editor = (HtmlBrowser.FactoryEditor)Lookup.getDefault().lookup(HtmlBrowser.FactoryEditor.class);
        }
        if ((browser = this.editor.getAsText()) != null && !browser.equals((String)this.cbWebBrowser.getSelectedItem())) {
            return true;
        }
        int proxyType = this.model.getProxyType();
        if (this.rbNoProxy.isSelected() && proxyType != 0) {
            return true;
        }
        if (this.rbUseSystemProxy.isSelected() && proxyType != 1) {
            return true;
        }
        if (this.rbHTTPProxy.isSelected() && proxyType != 2) {
            return true;
        }
        if (!this.tfProxyHost.getText().equals(this.model.getHttpProxyHost())) {
            return true;
        }
        if (!this.tfProxyPort.getText().equals(this.model.getHttpProxyPort())) {
            return true;
        }
        if (this.jUsageCheck.isSelected() != this.model.getUsageStatistics()) {
            return true;
        }
        return false;
    }

    void updateTestConnectionStatus(final GeneralOptionsModel.TestingStatus status, final String message) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                switch (status) {
                    case NOT_TESTED: {
                        GeneralOptionsPanel.this.lblTestResult.setText(" ");
                        GeneralOptionsPanel.this.lblTestResult.setIcon(null);
                        GeneralOptionsPanel.this.lblTestResult.setToolTipText("");
                        GeneralOptionsPanel.this.pbProxyWaiting.setVisible(false);
                        break;
                    }
                    case WAITING: {
                        GeneralOptionsPanel.this.lblTestResult.setText(" ");
                        GeneralOptionsPanel.this.lblTestResult.setIcon(null);
                        GeneralOptionsPanel.this.lblTestResult.setToolTipText("");
                        GeneralOptionsPanel.this.pbProxyWaiting.setVisible(true);
                        break;
                    }
                    case OK: {
                        GeneralOptionsPanel.this.lblTestResult.setText(" ");
                        GeneralOptionsPanel.this.lblTestResult.setIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/core/ui/options/general/ok.png", (boolean)true));
                        GeneralOptionsPanel.this.lblTestResult.setToolTipText(GeneralOptionsPanel.loc("GeneralOptionsPanel.proxy.result.ok", new String[0]));
                        GeneralOptionsPanel.this.pbProxyWaiting.setVisible(false);
                        break;
                    }
                    case FAILED: {
                        GeneralOptionsPanel.this.lblTestResult.setText(message);
                        GeneralOptionsPanel.this.lblTestResult.setIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/core/ui/options/general/error.png", (boolean)true));
                        GeneralOptionsPanel.this.lblTestResult.setToolTipText(GeneralOptionsPanel.loc("GeneralOptionsPanel.proxy.result.failed", new String[]{message}));
                        GeneralOptionsPanel.this.pbProxyWaiting.setVisible(false);
                    }
                }
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.bReloadProxy.setEnabled(this.rbUseSystemProxy.isSelected());
        this.tfProxyHost.setEnabled(this.rbHTTPProxy.isSelected());
        this.tfProxyPort.setEnabled(this.rbHTTPProxy.isSelected());
        this.lProxyHost.setEnabled(this.rbHTTPProxy.isSelected());
        this.lProxyPort.setEnabled(this.rbHTTPProxy.isSelected());
        this.bMoreProxy.setEnabled(this.rbHTTPProxy.isSelected());
        if (this.rbHTTPProxy.isSelected()) {
            this.tfProxyHost.requestFocusInWindow();
        }
        this.rbUseSystemProxy.setToolTipText(this.getUseSystemProxyToolTip());
    }

    private static String getPacFile() {
        return GeneralOptionsPanel.getProxyPreferences().get("systemPAC", "");
    }

    private static Preferences getProxyPreferences() {
        return NbPreferences.forModule(ProxySettings.class);
    }

}

