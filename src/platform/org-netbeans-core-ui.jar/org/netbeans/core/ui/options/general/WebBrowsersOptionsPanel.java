/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.explorer.propertysheet.PropertyPanel
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.ui.options.general;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.ListModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.Document;
import org.netbeans.core.ui.options.general.WebBrowsersOptionsModel;
import org.openide.awt.Mnemonics;
import org.openide.explorer.propertysheet.PropertyPanel;
import org.openide.util.NbBundle;

public class WebBrowsersOptionsPanel
extends JPanel
implements ListSelectionListener {
    private WebBrowsersOptionsModel browsersModel;
    private DocumentListener fieldDocListener;
    private JButton addButton;
    private JLabel browsersLabel;
    private JList browsersList;
    private JPanel customPropertyPanel;
    private JScrollPane jScrollPane1;
    private JLabel nameLabel;
    private JTextField nameTextField;
    private JButton removeButton;

    public WebBrowsersOptionsPanel(WebBrowsersOptionsModel mdl, String selectedItem) {
        this.browsersModel = mdl;
        this.initComponents();
        List<WebBrowsersOptionsModel.PropertyPanelDesc> propPanelDescs = this.browsersModel.getPropertyPanels();
        for (WebBrowsersOptionsModel.PropertyPanelDesc panelDesc : propPanelDescs) {
            this.customPropertyPanel.add((Component)panelDesc.panel, panelDesc.id);
        }
        this.browsersList.setModel(this.browsersModel);
        this.browsersList.addListSelectionListener(this);
        this.browsersList.setSelectedValue(selectedItem, true);
        this.fieldDocListener = new BrowsersDocListener();
        this.addListenerToField();
    }

    private void initComponents() {
        this.browsersLabel = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this.browsersList = new JList();
        this.addButton = new JButton();
        this.removeButton = new JButton();
        this.nameLabel = new JLabel();
        this.nameTextField = new JTextField();
        this.customPropertyPanel = new JPanel();
        this.browsersLabel.setLabelFor(this.browsersList);
        Mnemonics.setLocalizedText((JLabel)this.browsersLabel, (String)NbBundle.getMessage(WebBrowsersOptionsPanel.class, (String)"WebBrowsersOptionsPanel.browsersLabel.text"));
        this.jScrollPane1.setViewportView(this.browsersList);
        this.browsersList.getAccessibleContext().setAccessibleName(NbBundle.getMessage(WebBrowsersOptionsPanel.class, (String)"WebBrowsersOptionsPanel.browsersList.AccessibleContext.accessibleName"));
        this.browsersList.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(WebBrowsersOptionsPanel.class, (String)"WebBrowsersOptionsPanel.browsersList.AccessibleContext.accessibleDescription"));
        Mnemonics.setLocalizedText((AbstractButton)this.addButton, (String)NbBundle.getMessage(WebBrowsersOptionsPanel.class, (String)"WebBrowsersOptionsPanel.addButton.text"));
        this.addButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                WebBrowsersOptionsPanel.this.addButtonActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.removeButton, (String)NbBundle.getMessage(WebBrowsersOptionsPanel.class, (String)"WebBrowsersOptionsPanel.removeButton.text"));
        this.removeButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                WebBrowsersOptionsPanel.this.removeButtonActionPerformed(evt);
            }
        });
        this.nameLabel.setLabelFor(this.nameTextField);
        Mnemonics.setLocalizedText((JLabel)this.nameLabel, (String)NbBundle.getMessage(WebBrowsersOptionsPanel.class, (String)"WebBrowsersOptionsPanel.nameLabel.text"));
        this.nameTextField.setText(NbBundle.getMessage(WebBrowsersOptionsPanel.class, (String)"WebBrowsersOptionsPanel.nameTextField.text"));
        this.customPropertyPanel.setMaximumSize(new Dimension(350, 250));
        this.customPropertyPanel.setPreferredSize(new Dimension(300, 200));
        this.customPropertyPanel.setLayout(new CardLayout());
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.jScrollPane1, -2, 170, -2).addGap(12, 12, 12).addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addGroup(layout.createSequentialGroup().addComponent(this.nameLabel).addGap(18, 18, 18).addComponent(this.nameTextField, -1, 299, 32767)).addComponent(this.customPropertyPanel, -1, 362, 32767))).addComponent(this.browsersLabel).addGroup(layout.createSequentialGroup().addComponent(this.addButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.removeButton))).addContainerGap()));
        layout.linkSize(0, this.addButton, this.removeButton);
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.browsersLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.nameLabel).addComponent(this.nameTextField, -2, -1, -2)).addGap(18, 18, 18).addComponent(this.customPropertyPanel, -2, 188, -2)).addComponent(this.jScrollPane1, GroupLayout.Alignment.TRAILING, -1, 259, 32767)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.addButton).addComponent(this.removeButton)).addContainerGap()));
        this.browsersLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(WebBrowsersOptionsPanel.class, (String)"WebBrowsersOptionsPanel.browsersLabel.AccessibleContext.accessibleDescription"));
        this.addButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(WebBrowsersOptionsPanel.class, (String)"WebBrowsersOptionsPanel.addButton.AccessibleContext.accessibleDescription"));
        this.removeButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(WebBrowsersOptionsPanel.class, (String)"WebBrowsersOptionsPanel.removeButton.AccessibleContext.accessibleDescription"));
        this.nameLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(WebBrowsersOptionsPanel.class, (String)"WebBrowsersOptionsPanel.nameLabel.AccessibleContext.accessibleDescription"));
        this.nameTextField.getAccessibleContext().setAccessibleName(NbBundle.getMessage(WebBrowsersOptionsPanel.class, (String)"WebBrowsersOptionsPanel.nameTextField.AccessibleContext.accessibleName"));
        this.nameTextField.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(WebBrowsersOptionsPanel.class, (String)"WebBrowsersOptionsPanel.nameTextField.AccessibleContext.accessibleDescription"));
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(WebBrowsersOptionsPanel.class, (String)"WebBrowsersOptionsPanel.AccessibleContext.accessibleName"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(WebBrowsersOptionsPanel.class, (String)"WebBrowsersOptionsPanel.AccessibleContext.accessibleDescription"));
    }

    private void removeButtonActionPerformed(ActionEvent evt) {
        int index = this.browsersList.getSelectedIndex();
        this.browsersModel.removeBrowser(index);
        if (index > 1) {
            this.browsersList.setSelectedIndex(index - 1);
        } else {
            this.browsersList.setSelectedIndex(0);
        }
        if (this.browsersModel.getSize() == 0) {
            this.removeButton.setEnabled(false);
            this.removeListenerFromField();
            this.nameTextField.setText("");
            this.addListenerToField();
        }
    }

    private void addButtonActionPerformed(ActionEvent evt) {
        this.browsersModel.addBrowser();
        int index = this.browsersModel.getSize() - 1;
        this.customPropertyPanel.add((Component)this.browsersModel.getPropertyPanel(index), this.browsersModel.getPropertyPanelID(index));
        this.browsersList.setSelectedIndex(index);
        this.browsersList.ensureIndexIsVisible(index);
        if (this.browsersModel.getSize() > 0 && !this.removeButton.isEnabled()) {
            this.removeButton.setEnabled(true);
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent evt) {
        if (!evt.getValueIsAdjusting() && !this.browsersModel.isAdjusting()) {
            int index = this.browsersList.getSelectedIndex();
            String panelID = this.browsersModel.getPropertyPanelID(index);
            ((CardLayout)this.customPropertyPanel.getLayout()).show(this.customPropertyPanel, panelID);
            this.nameTextField.setText(this.browsersModel.getBrowserName(index));
            if (this.browsersModel.isDefaultBrowser(index)) {
                this.nameTextField.setEditable(false);
                this.removeButton.setEnabled(false);
            } else {
                this.nameTextField.setEditable(true);
                this.removeButton.setEnabled(true);
            }
            this.browsersModel.setSelectedValue(this.browsersList.getSelectedValue());
        }
    }

    private void removeListenerFromField() {
        this.nameTextField.getDocument().removeDocumentListener(this.fieldDocListener);
    }

    private void addListenerToField() {
        this.nameTextField.getDocument().addDocumentListener(this.fieldDocListener);
    }

    private class BrowsersDocListener
    implements DocumentListener {
        private BrowsersDocListener() {
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            this.update(e);
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            this.update(e);
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
        }

        private void update(DocumentEvent evt) {
            int index = WebBrowsersOptionsPanel.this.browsersList.getSelectedIndex();
            Document doc = evt.getDocument();
            if (doc.equals(WebBrowsersOptionsPanel.this.nameTextField.getDocument())) {
                WebBrowsersOptionsPanel.this.browsersModel.setBrowserName(index, WebBrowsersOptionsPanel.this.nameTextField.getText());
            }
        }
    }

}

