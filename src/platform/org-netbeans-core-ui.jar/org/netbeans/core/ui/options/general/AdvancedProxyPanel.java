/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.NotificationLineSupport
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.ui.options.general;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.netbeans.core.ui.options.general.GeneralOptionsModel;
import org.openide.DialogDescriptor;
import org.openide.NotificationLineSupport;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class AdvancedProxyPanel
extends JPanel {
    private GeneralOptionsModel options;
    private String oldHttpsHost;
    private String oldHttpsPort;
    private String oldSocksHost;
    private String oldSocksPort;
    private DialogDescriptor dd = null;
    private JCheckBox cbSameProxySettings;
    private JCheckBox cbUseProxyAuthentication;
    private JLabel lHttpProxyHost;
    private JLabel lHttpProxyPort;
    private JLabel lHttpsProxyHost;
    private JLabel lHttpsProxyPort;
    private JLabel lNonProxyHosts;
    private JLabel lNonProxyHostsDescription;
    private JLabel lSocksHost;
    private JLabel lSocksPort;
    private JLabel lUserName;
    private JLabel lUserPassword;
    private JPasswordField pfUserPassword;
    private JSeparator sSeparator;
    private JTextField tfHttpProxyHost;
    private JTextField tfHttpProxyPort;
    private JTextField tfHttpsProxyHost;
    private JTextField tfHttpsProxyPort;
    private JTextField tfNonProxyHosts;
    private JTextField tfSocksHost;
    private JTextField tfSocksPort;
    private JTextField tfUserName;

    AdvancedProxyPanel(GeneralOptionsModel model) {
        this.options = model;
        this.initComponents();
        this.tfHttpProxyHost.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent arg0) {
                AdvancedProxyPanel.this.followHttpHostIfDemand();
            }

            @Override
            public void removeUpdate(DocumentEvent arg0) {
                AdvancedProxyPanel.this.followHttpHostIfDemand();
            }

            @Override
            public void changedUpdate(DocumentEvent arg0) {
                AdvancedProxyPanel.this.followHttpHostIfDemand();
            }
        });
        this.tfHttpProxyPort.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent arg0) {
                AdvancedProxyPanel.this.followHttpPortIfDemand();
            }

            @Override
            public void removeUpdate(DocumentEvent arg0) {
                AdvancedProxyPanel.this.followHttpPortIfDemand();
            }

            @Override
            public void changedUpdate(DocumentEvent arg0) {
                AdvancedProxyPanel.this.followHttpPortIfDemand();
            }
        });
        this.tfHttpsProxyPort.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent arg0) {
                AdvancedProxyPanel.this.validatePortValue(AdvancedProxyPanel.this.tfHttpsProxyPort.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent arg0) {
                AdvancedProxyPanel.this.validatePortValue(AdvancedProxyPanel.this.tfHttpsProxyPort.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent arg0) {
                AdvancedProxyPanel.this.validatePortValue(AdvancedProxyPanel.this.tfHttpsProxyPort.getText());
            }
        });
        this.tfSocksPort.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent arg0) {
                AdvancedProxyPanel.this.validatePortValue(AdvancedProxyPanel.this.tfSocksPort.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent arg0) {
                AdvancedProxyPanel.this.validatePortValue(AdvancedProxyPanel.this.tfSocksPort.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent arg0) {
                AdvancedProxyPanel.this.validatePortValue(AdvancedProxyPanel.this.tfSocksPort.getText());
            }
        });
    }

    public void update(String httpHost, String httpPort) {
        this.readOptions();
        if (!this.options.getHttpProxyHost().equals(httpHost)) {
            this.tfHttpProxyHost.setText(httpHost);
        }
        if (!this.options.getHttpProxyPort().equals(httpPort)) {
            this.tfHttpProxyPort.setText(httpPort);
        }
    }

    public void applyChanges() {
        this.writeOptions();
    }

    private void readOptions() {
        this.cbSameProxySettings.setSelected(this.options.useProxyAllProtocols());
        this.cbUseProxyAuthentication.setSelected(this.options.useProxyAuthentication());
        this.tfHttpProxyHost.setText(this.options.getHttpProxyHost());
        this.tfHttpProxyPort.setText(this.options.getHttpProxyPort());
        this.tfHttpsProxyHost.setText(this.options.getHttpsProxyHost());
        this.tfHttpsProxyPort.setText(this.options.getHttpsProxyPort());
        this.tfSocksHost.setText(this.options.getSocksHost());
        this.tfSocksPort.setText(this.options.getSocksPort());
        this.tfUserName.setText(this.options.getProxyAuthenticationUsername());
        this.pfUserPassword.setText(new String(this.options.getProxyAuthenticationPassword()));
        this.tfNonProxyHosts.setText(this.options.getNonProxyHosts());
        this.oldHttpsHost = this.options.getOriginalHttpsHost();
        this.oldHttpsPort = this.options.getOriginalHttpsPort();
        this.oldSocksHost = this.options.getOriginalSocksHost();
        this.oldSocksPort = this.options.getOriginalSocksPort();
        this.followHttpProxyIfDemand();
        this.updateAuthentication();
    }

    private void writeOptions() {
        this.options.setUseProxyAllProtocols(this.cbSameProxySettings.isSelected());
        this.options.setUseProxyAuthentication(this.cbUseProxyAuthentication.isSelected());
        this.options.setHttpProxyHost(this.tfHttpProxyHost.getText());
        this.options.setHttpProxyPort(this.tfHttpProxyPort.getText());
        if (!this.cbSameProxySettings.isSelected()) {
            this.options.setHttpsProxyHost(this.tfHttpsProxyHost.getText());
            this.options.setHttpsProxyPort(this.tfHttpsProxyPort.getText());
            this.options.setSocksHost(this.tfSocksHost.getText());
            this.options.setSocksPort(this.tfSocksPort.getText());
        }
        this.options.setNonProxyHosts(this.tfNonProxyHosts.getText());
        this.options.setAuthenticationUsername(this.tfUserName.getText());
        this.options.setAuthenticationPassword(this.pfUserPassword.getPassword());
    }

    private void followHttpProxyIfDemand() {
        boolean same = this.cbSameProxySettings.isSelected();
        this.tfHttpsProxyHost.setEnabled(!same);
        this.tfHttpsProxyPort.setEnabled(!same);
        this.tfSocksHost.setEnabled(!same);
        this.tfSocksPort.setEnabled(!same);
        this.lHttpsProxyHost.setEnabled(!same);
        this.lHttpsProxyPort.setEnabled(!same);
        this.lSocksHost.setEnabled(!same);
        this.lSocksPort.setEnabled(!same);
        this.followHttpHostIfDemand();
        this.followHttpPortIfDemand();
    }

    private void updateAuthentication() {
        boolean use = this.cbUseProxyAuthentication.isSelected();
        this.tfUserName.setEnabled(use);
        this.lUserName.setEnabled(use);
        this.pfUserPassword.setEnabled(use);
        this.lUserPassword.setEnabled(use);
    }

    private void followHttpHostIfDemand() {
        if (!this.cbSameProxySettings.isSelected()) {
            return;
        }
        String host = this.tfHttpProxyHost.getText();
        this.tfHttpsProxyHost.setText(host);
        this.tfSocksHost.setText(host);
    }

    private void followHttpPortIfDemand() {
        String port = this.tfHttpProxyPort.getText();
        this.validatePortValue(port);
        if (!this.cbSameProxySettings.isSelected()) {
            return;
        }
        this.tfHttpsProxyPort.setText(port);
        this.tfSocksPort.setText(port);
    }

    private void validatePortValue(String port) {
        this.clearError();
        if (port != null && port.length() > 0) {
            try {
                Integer.parseInt(port);
            }
            catch (NumberFormatException nfex) {
                this.showError(NbBundle.getMessage(AdvancedProxyPanel.class, (String)"LBL_AdvancedProxyPanel_PortError"));
            }
        }
    }

    private void showError(String message) {
        if (this.dd != null) {
            NotificationLineSupport notificationLineSupport = this.dd.getNotificationLineSupport();
            if (notificationLineSupport != null) {
                notificationLineSupport.setErrorMessage(message);
            }
            this.dd.setValid(false);
        }
    }

    private void clearError() {
        if (this.dd != null) {
            NotificationLineSupport notificationLineSupport = this.dd.getNotificationLineSupport();
            if (notificationLineSupport != null) {
                notificationLineSupport.clearMessages();
            }
            this.dd.setValid(true);
        }
    }

    public void setDialogDescriptor(DialogDescriptor dd) {
        this.dd = dd;
    }

    protected String getNonProxyHosts() {
        return this.tfNonProxyHosts.getText();
    }

    private void initComponents() {
        this.lHttpProxyHost = new JLabel();
        this.tfHttpProxyHost = new JTextField();
        this.cbSameProxySettings = new JCheckBox();
        this.lHttpsProxyHost = new JLabel();
        this.tfHttpsProxyHost = new JTextField();
        this.lSocksHost = new JLabel();
        this.tfSocksHost = new JTextField();
        this.lHttpProxyPort = new JLabel();
        this.lHttpsProxyPort = new JLabel();
        this.lSocksPort = new JLabel();
        this.tfHttpProxyPort = new JTextField();
        this.tfHttpsProxyPort = new JTextField();
        this.tfSocksPort = new JTextField();
        this.lNonProxyHosts = new JLabel();
        this.tfNonProxyHosts = new JTextField();
        this.lNonProxyHostsDescription = new JLabel();
        this.sSeparator = new JSeparator();
        this.cbUseProxyAuthentication = new JCheckBox();
        this.lUserName = new JLabel();
        this.lUserPassword = new JLabel();
        this.tfUserName = new JTextField();
        this.pfUserPassword = new JPasswordField();
        this.lHttpProxyHost.setLabelFor(this.tfHttpProxyHost);
        Mnemonics.setLocalizedText((JLabel)this.lHttpProxyHost, (String)NbBundle.getMessage(AdvancedProxyPanel.class, (String)"LBL_AdvancedProxyPanel_lHttpProxyHost"));
        Mnemonics.setLocalizedText((AbstractButton)this.cbSameProxySettings, (String)NbBundle.getMessage(AdvancedProxyPanel.class, (String)"LBL_AdvancedProxyPanel_cbSameProxySettings"));
        this.cbSameProxySettings.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.cbSameProxySettings.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                AdvancedProxyPanel.this.cbSameProxySettingsActionPerformed(evt);
            }
        });
        this.lHttpsProxyHost.setLabelFor(this.tfHttpsProxyHost);
        Mnemonics.setLocalizedText((JLabel)this.lHttpsProxyHost, (String)NbBundle.getMessage(AdvancedProxyPanel.class, (String)"LBL_AdvancedProxyPanel_lHttpsProxyHots"));
        this.lSocksHost.setLabelFor(this.tfSocksHost);
        Mnemonics.setLocalizedText((JLabel)this.lSocksHost, (String)NbBundle.getMessage(AdvancedProxyPanel.class, (String)"LBL_AdvancedProxyPanel_lSocksHost"));
        this.lHttpProxyPort.setLabelFor(this.tfHttpProxyPort);
        Mnemonics.setLocalizedText((JLabel)this.lHttpProxyPort, (String)NbBundle.getMessage(AdvancedProxyPanel.class, (String)"LBL_AdvancedProxyPanel_lHttpProxyPort"));
        this.lHttpsProxyPort.setLabelFor(this.tfHttpsProxyPort);
        Mnemonics.setLocalizedText((JLabel)this.lHttpsProxyPort, (String)NbBundle.getMessage(AdvancedProxyPanel.class, (String)"LBL_AdvancedProxyPanel_lHttpsProxyPort"));
        this.lSocksPort.setLabelFor(this.tfSocksPort);
        Mnemonics.setLocalizedText((JLabel)this.lSocksPort, (String)NbBundle.getMessage(AdvancedProxyPanel.class, (String)"LBL_AdvancedProxyPanel_lSocksPort"));
        this.tfHttpProxyPort.setColumns(4);
        this.tfHttpsProxyPort.setColumns(4);
        this.tfSocksPort.setColumns(4);
        this.lNonProxyHosts.setLabelFor(this.tfNonProxyHosts);
        Mnemonics.setLocalizedText((JLabel)this.lNonProxyHosts, (String)NbBundle.getMessage(AdvancedProxyPanel.class, (String)"LBL_AdvancedProxyPanel_lNonProxyHosts"));
        Mnemonics.setLocalizedText((JLabel)this.lNonProxyHostsDescription, (String)NbBundle.getMessage(AdvancedProxyPanel.class, (String)"LBL_AdvancedProxyPanel_lNonProxyHostsDescription"));
        Mnemonics.setLocalizedText((AbstractButton)this.cbUseProxyAuthentication, (String)NbBundle.getMessage(AdvancedProxyPanel.class, (String)"LBL_AdvancedProxyPanel_cbUseProxyAuthentication"));
        this.cbUseProxyAuthentication.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.cbUseProxyAuthentication.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                AdvancedProxyPanel.this.cbUseProxyAuthenticationActionPerformed(evt);
            }
        });
        this.lUserName.setLabelFor(this.tfUserName);
        Mnemonics.setLocalizedText((JLabel)this.lUserName, (String)NbBundle.getMessage(AdvancedProxyPanel.class, (String)"LBL_AdvancedProxyPanel_lUserName"));
        this.lUserPassword.setLabelFor(this.pfUserPassword);
        Mnemonics.setLocalizedText((JLabel)this.lUserPassword, (String)NbBundle.getMessage(AdvancedProxyPanel.class, (String)"LBL_AdvancedProxyPanel_lUserPassword"));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGap(12, 12, 12).addComponent(this.cbUseProxyAuthentication)).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.sSeparator, -1, 512, 32767)).addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.lUserName).addComponent(this.lUserPassword)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.pfUserPassword, -1, 423, 32767).addComponent(this.tfUserName, -1, 423, 32767))).addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup().addGap(12, 12, 12).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.lHttpProxyHost).addComponent(this.lHttpsProxyHost).addComponent(this.lSocksHost).addComponent(this.lNonProxyHosts)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.lNonProxyHostsDescription, -2, 349, -2).addComponent(this.cbSameProxySettings, -2, 325, -2).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.tfHttpProxyHost, GroupLayout.Alignment.LEADING, -1, 281, 32767).addComponent(this.tfSocksHost, GroupLayout.Alignment.LEADING, -1, 281, 32767).addComponent(this.tfHttpsProxyHost, GroupLayout.Alignment.LEADING, -1, 281, 32767)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.lHttpProxyPort).addComponent(this.lHttpsProxyPort).addComponent(this.lSocksPort)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.tfHttpProxyPort).addComponent(this.tfSocksPort).addComponent(this.tfHttpsProxyPort))).addComponent(this.tfNonProxyHosts, -1, 388, 32767)))).addGap(18, 18, 18)));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lHttpProxyHost).addComponent(this.tfHttpProxyHost, -2, -1, -2).addComponent(this.lHttpProxyPort).addComponent(this.tfHttpProxyPort, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cbSameProxySettings).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lHttpsProxyHost).addComponent(this.tfHttpsProxyHost, -2, -1, -2).addComponent(this.lHttpsProxyPort).addComponent(this.tfHttpsProxyPort, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lSocksHost).addComponent(this.tfSocksHost, -2, -1, -2).addComponent(this.lSocksPort).addComponent(this.tfSocksPort, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lNonProxyHosts).addComponent(this.tfNonProxyHosts, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lNonProxyHostsDescription).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.sSeparator, -2, 10, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cbUseProxyAuthentication).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lUserName).addComponent(this.tfUserName, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lUserPassword).addComponent(this.pfUserPassword, -2, -1, -2)).addContainerGap(30, 32767)));
        this.tfHttpProxyHost.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(AdvancedProxyPanel.class, (String)"ACD_AdvancedProxyPanel_tfHttpProxyHost"));
        this.cbSameProxySettings.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(AdvancedProxyPanel.class, (String)"ACD_AdvancedProxyPanel_cbSameProxySettings"));
        this.tfHttpsProxyHost.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(AdvancedProxyPanel.class, (String)"ACD_AdvancedProxyPanel_tfHttpsProxyHost"));
        this.tfSocksHost.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(AdvancedProxyPanel.class, (String)"ACD_AdvancedProxyPanel_tfSocksHost"));
        this.tfHttpProxyPort.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(AdvancedProxyPanel.class, (String)"ACD_AdvancedProxyPanel_tfHttpProxyPort"));
        this.tfHttpsProxyPort.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(AdvancedProxyPanel.class, (String)"ACD_AdvancedProxyPanel_tfHttpsProxyPort"));
        this.tfSocksPort.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(AdvancedProxyPanel.class, (String)"ACD_AdvancedProxyPanel_tfSocksPort"));
        this.tfNonProxyHosts.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(AdvancedProxyPanel.class, (String)"ACD_AdvancedProxyPanel_tfNonProxyHosts"));
        this.cbUseProxyAuthentication.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(AdvancedProxyPanel.class, (String)"ACD_AdvancedProxyPanel_cbUseProxyAuthentication"));
        this.tfUserName.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(AdvancedProxyPanel.class, (String)"ACD_AdvancedProxyPanel_tfUserName"));
        this.pfUserPassword.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(AdvancedProxyPanel.class, (String)"ACD_AdvancedProxyPanel_pfUserPassword"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(AdvancedProxyPanel.class, (String)"ACD_AdvancedProxyPanel"));
    }

    private void cbUseProxyAuthenticationActionPerformed(ActionEvent evt) {
        this.updateAuthentication();
    }

    private void cbSameProxySettingsActionPerformed(ActionEvent evt) {
        if (this.cbSameProxySettings.isSelected()) {
            this.oldHttpsHost = this.tfHttpsProxyHost.getText();
            this.oldHttpsPort = this.tfHttpsProxyPort.getText();
            this.oldSocksHost = this.tfSocksHost.getText();
            this.oldSocksPort = this.tfSocksPort.getText();
        } else {
            this.tfHttpsProxyHost.setText(this.oldHttpsHost);
            this.tfHttpsProxyPort.setText(this.oldHttpsPort);
            this.tfSocksHost.setText(this.oldSocksHost);
            this.tfSocksPort.setText(this.oldSocksPort);
        }
        this.followHttpProxyIfDemand();
    }

}

