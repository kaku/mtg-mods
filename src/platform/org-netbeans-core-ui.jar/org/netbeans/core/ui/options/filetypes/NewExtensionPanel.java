/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.ui.options.filetypes;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import javax.accessibility.AccessibleContext;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.netbeans.core.ui.options.filetypes.FileAssociationsModel;
import org.openide.DialogDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

final class NewExtensionPanel
extends JPanel {
    private FileAssociationsModel model;
    private JLabel lblExtension;
    private JTextField tfExtension;

    public NewExtensionPanel() {
        this.initComponents();
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(NewExtensionPanel.class, (String)"NewExtensionPanel.AD"));
    }

    void setModel(FileAssociationsModel model) {
        this.model = model;
    }

    private void initComponents() {
        this.lblExtension = new JLabel();
        this.tfExtension = new JTextField();
        this.lblExtension.setLabelFor(this.tfExtension);
        Mnemonics.setLocalizedText((JLabel)this.lblExtension, (String)NbBundle.getMessage(NewExtensionPanel.class, (String)"NewExtensionPanel.lblExtension.text"));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.lblExtension).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.tfExtension, -1, 200, 32767).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lblExtension).addComponent(this.tfExtension, -2, -1, -2)).addContainerGap(-1, 32767)));
        this.lblExtension.getAccessibleContext().setAccessibleName(NbBundle.getMessage(NewExtensionPanel.class, (String)"NewExtensionPanel.lblExtension.AN"));
        this.lblExtension.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(NewExtensionPanel.class, (String)"NewExtensionPanel.lblExtension.AD"));
    }

    String getExtension() {
        return this.tfExtension.getText();
    }

    private boolean isExtensionValid() {
        String extension = this.tfExtension.getText();
        return extension != null && extension.length() != 0 && extension.indexOf(".") == -1 && !this.model.containsExtension(extension);
    }

    void addExtensionListener(final DialogDescriptor dd) {
        dd.setValid(this.isExtensionValid());
        this.tfExtension.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent e) {
                dd.setValid(NewExtensionPanel.this.isExtensionValid());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                dd.setValid(NewExtensionPanel.this.isExtensionValid());
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                dd.setValid(NewExtensionPanel.this.isExtensionValid());
            }
        });
    }

}

