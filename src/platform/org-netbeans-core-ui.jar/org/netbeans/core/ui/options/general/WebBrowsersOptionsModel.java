/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.InstanceCookie
 *  org.openide.explorer.propertysheet.PropertyPanel
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.ui.options.general;

import java.beans.FeatureDescriptor;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.DefaultListModel;
import org.openide.cookies.InstanceCookie;
import org.openide.explorer.propertysheet.PropertyPanel;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class WebBrowsersOptionsModel
extends DefaultListModel {
    private static final String BROWSERS_FOLDER = "Services/Browsers";
    private static final String BROWSER_TEMPLATE = "Templates/Services/Browsers/ExtWebBrowser.settings";
    private static final String EA_HIDDEN = "hidden";
    private List<WebBrowserDesc> browsersList = new ArrayList<WebBrowserDesc>();
    private Map<Integer, WebBrowserDesc> index2desc = new TreeMap<Integer, WebBrowserDesc>();
    private boolean isAdjusting = false;
    private Object selectedValue = null;

    public WebBrowsersOptionsModel() {
        FileObject servicesBrowsers = FileUtil.getConfigFile((String)"Services/Browsers");
        if (servicesBrowsers != null) {
            DataObject[] browserSettings;
            DataFolder folder = DataFolder.findFolder((FileObject)servicesBrowsers);
            for (DataObject browserSetting : browserSettings = folder.getChildren()) {
                InstanceCookie cookie = (InstanceCookie)browserSetting.getCookie(InstanceCookie.class);
                FileObject primaryFile = browserSetting.getPrimaryFile();
                if (cookie == null || Boolean.TRUE.equals(primaryFile.getAttribute("hidden"))) continue;
                WebBrowserDesc browserDesc = new WebBrowserDesc(browserSetting);
                this.browsersList.add(browserDesc);
            }
        }
        int index = 0;
        for (WebBrowserDesc desc : this.browsersList) {
            this.addElement(desc.getOrigName());
            this.index2desc.put(index++, desc);
        }
    }

    public void addBrowser() {
        WebBrowserDesc desc = new WebBrowserDesc();
        desc.setChangeStatus(ChangeStatus.ADDED);
        this.browsersList.add(desc);
        this.adjustListItems();
    }

    public void removeBrowser(int idx) {
        this.index2desc.get(idx).setChangeStatus(ChangeStatus.REMOVED);
        this.adjustListItems();
    }

    private void adjustListItems() {
        this.isAdjusting = true;
        this.removeAllElements();
        this.index2desc.clear();
        int index = 0;
        for (WebBrowserDesc desc : this.browsersList) {
            if (desc.getChangeStatus().equals((Object)ChangeStatus.REMOVED)) continue;
            String newName = desc.getNewName();
            if (newName != null) {
                this.addElement(newName);
            } else {
                this.addElement(desc.getOrigName());
            }
            this.index2desc.put(index++, desc);
        }
        this.isAdjusting = false;
    }

    public boolean isAdjusting() {
        return this.isAdjusting;
    }

    public void updateList() {
        this.adjustListItems();
    }

    public PropertyPanel getPropertyPanel(int index) {
        return this.index2desc.get(index).getPropertyPanel();
    }

    public String getPropertyPanelID(int index) {
        return this.index2desc.get(index).getPropertyPanelID();
    }

    public String getBrowserName(int index) {
        String retVal = null;
        String newName = this.index2desc.get(index).getNewName();
        retVal = newName != null ? newName : this.index2desc.get(index).getOrigName();
        return retVal;
    }

    public boolean isDefaultBrowser(int index) {
        return this.index2desc.get(index).isDefaultBrowser();
    }

    public void setBrowserName(int index, String name) {
        this.index2desc.get(index).setNewName(name);
    }

    public void setSelectedValue(Object obj) {
        this.selectedValue = obj;
    }

    public Object getSelectedValue() {
        return this.selectedValue;
    }

    public void applyChanges() {
        for (WebBrowserDesc desc : this.browsersList) {
            desc.applyChanges();
        }
    }

    public void discardChanges() {
        for (WebBrowserDesc desc : this.browsersList) {
            desc.discardChanges();
        }
    }

    public List<PropertyPanelDesc> getPropertyPanels() {
        ArrayList<PropertyPanelDesc> list = new ArrayList<PropertyPanelDesc>();
        Collection<WebBrowserDesc> col = this.index2desc.values();
        for (WebBrowserDesc wbd : col) {
            list.add(new PropertyPanelDesc(wbd.getPropertyPanel(), wbd.getPropertyPanelID()));
        }
        return list;
    }

    private static class WebBrowserDesc {
        private String origName = null;
        private String newName = null;
        private ChangeStatus changeStatus = ChangeStatus.NONE;
        private DataObject browserSettings;
        private PropertyPanel propertyPanel = null;
        private String propertyPanelID;
        private static int propertyPanelIDCounter = 0;

        public WebBrowserDesc() {
            this.newName = NbBundle.getBundle(WebBrowsersOptionsModel.class).getString("LBL_ExternalBrowser_Name");
            this.browserSettings = this.createNewBrowserSettings(this.newName);
            this.findPropertyPanel();
        }

        public WebBrowserDesc(DataObject brSettings) {
            this.browserSettings = brSettings;
            this.origName = this.browserSettings.getNodeDelegate().getDisplayName();
            this.findPropertyPanel();
        }

        private void findPropertyPanel() {
            try {
                InstanceCookie cookie = (InstanceCookie)this.browserSettings.getCookie(InstanceCookie.class);
                PropertyDescriptor[] propDesc = Introspector.getBeanInfo(cookie.instanceClass()).getPropertyDescriptors();
                FeatureDescriptor fallbackProp = null;
                for (PropertyDescriptor pd : propDesc) {
                    if (fallbackProp == null && !pd.isExpert() && !pd.isHidden()) {
                        fallbackProp = pd;
                    }
                    if (!pd.isPreferred() || pd.isExpert() || pd.isHidden()) continue;
                    this.propertyPanel = new PropertyPanel(cookie.instanceCreate(), pd.getName(), 2);
                    this.propertyPanelID = "PROPERTY_PANEL_" + propertyPanelIDCounter++;
                    this.propertyPanel.setChangeImmediate(false);
                    break;
                }
                if (this.propertyPanel == null) {
                    this.propertyPanel = new PropertyPanel(cookie.instanceCreate(), fallbackProp.getName(), 2);
                    this.propertyPanelID = "PROPERTY_PANEL_" + propertyPanelIDCounter++;
                }
            }
            catch (Exception ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }

        public PropertyPanel getPropertyPanel() {
            return this.propertyPanel;
        }

        public String getPropertyPanelID() {
            return this.propertyPanelID;
        }

        public String getOrigName() {
            return this.origName;
        }

        public String getNewName() {
            return this.newName;
        }

        public void setNewName(String name) {
            this.newName = name;
        }

        public boolean isDefaultBrowser() {
            return this.browserSettings.getPrimaryFile().getAttribute("SystemFileSystem.icon") != null;
        }

        public void setChangeStatus(ChangeStatus stat) {
            this.changeStatus = stat;
        }

        public ChangeStatus getChangeStatus() {
            return this.changeStatus;
        }

        public void applyChanges() {
            this.propertyPanel.updateValue();
            if (this.getChangeStatus() == ChangeStatus.REMOVED && this.browserSettings != null) {
                try {
                    this.browserSettings.delete();
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            if (this.newName != null && !this.newName.equals(this.origName)) {
                try {
                    this.browserSettings.rename(this.newName);
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }

        public void discardChanges() {
            ChangeStatus status = this.getChangeStatus();
            if (status == ChangeStatus.ADDED && this.browserSettings != null) {
                try {
                    this.browserSettings.delete();
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }

        private DataObject createNewBrowserSettings(String name) {
            DataObject createdSettings = null;
            try {
                FileObject extWebBrowserTemplate = FileUtil.getConfigFile((String)"Templates/Services/Browsers/ExtWebBrowser.settings");
                FileObject browsersFolderFO = FileUtil.getConfigFile((String)"Services/Browsers");
                if (extWebBrowserTemplate == null) {
                    return null;
                }
                DataObject templateDO = DataObject.find((FileObject)extWebBrowserTemplate);
                DataFolder browsersFolderDF = DataFolder.findFolder((FileObject)browsersFolderFO);
                createdSettings = templateDO.createFromTemplate(browsersFolderDF, name);
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            return createdSettings;
        }
    }

    public static class PropertyPanelDesc {
        public PropertyPanel panel;
        public String id;

        public PropertyPanelDesc(PropertyPanel pp, String s) {
            this.panel = pp;
            this.id = s;
        }
    }

    private static enum ChangeStatus {
        NONE,
        REMOVED,
        ADDED;
        

        private ChangeStatus() {
        }
    }

}

