/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 */
package org.netbeans.core.ui.options.filetypes;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.prefs.Preferences;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.netbeans.core.ui.options.filetypes.FileAssociationsModel;
import org.netbeans.core.ui.options.filetypes.FileAssociationsOptionsPanelController;
import org.netbeans.core.ui.options.filetypes.IgnoredFilesPreferences;
import org.netbeans.core.ui.options.filetypes.NewExtensionPanel;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

final class FileAssociationsPanel
extends JPanel {
    private final FileAssociationsOptionsPanelController controller;
    private final String chooseExtensionItem;
    private NewExtensionPanel newExtensionPanel;
    private FileAssociationsModel model;
    private DocumentListener patternListener;
    ArrayList<String> newlyAddedExtensions = new ArrayList();
    private JCheckBox autoScan;
    private JButton btnDefault;
    private JButton btnDefaultIgnored;
    private JButton btnNew;
    private JButton btnRemove;
    private JComboBox cbExtension;
    private JComboBox cbType;
    private JScrollPane jScrollPane1;
    private JLabel lblAssociatedAlso;
    private JLabel lblAssociatedAlsoExt;
    private JLabel lblExtension;
    private JLabel lblFileAssociations;
    private JLabel lblFilesIgnored;
    private JLabel lblPattern;
    private JLabel lblType;
    private JSeparator sepFileAssociations;
    private JSeparator setFilesIgnored;
    private JTextArea txtPattern;
    private JLabel txtPatternError;

    FileAssociationsPanel(FileAssociationsOptionsPanelController controller) {
        this.controller = controller;
        this.initComponents();
        this.chooseExtensionItem = NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.cbExtension.choose");
        this.patternListener = new PatternDocumentListener();
    }

    private void initComponents() {
        this.lblExtension = new JLabel();
        this.cbExtension = new JComboBox();
        this.btnNew = new JButton();
        this.btnRemove = new JButton();
        this.lblType = new JLabel();
        this.cbType = new JComboBox();
        this.btnDefault = new JButton();
        this.lblAssociatedAlso = new JLabel();
        this.lblAssociatedAlsoExt = new JLabel();
        this.sepFileAssociations = new JSeparator();
        this.lblFileAssociations = new JLabel();
        this.lblFilesIgnored = new JLabel();
        this.setFilesIgnored = new JSeparator();
        this.lblPattern = new JLabel();
        this.btnDefaultIgnored = new JButton();
        this.jScrollPane1 = new JScrollPane();
        this.txtPattern = new JTextArea();
        this.txtPatternError = new JLabel();
        this.autoScan = new JCheckBox();
        this.setPreferredSize(new Dimension(360, 360));
        this.lblExtension.setLabelFor(this.cbExtension);
        Mnemonics.setLocalizedText((JLabel)this.lblExtension, (String)NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.lblExtension.text"));
        this.cbExtension.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FileAssociationsPanel.this.cbExtensionActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.btnNew, (String)NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.btnNew.text"));
        this.btnNew.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FileAssociationsPanel.this.btnNewActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.btnRemove, (String)NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.btnRemove.text"));
        this.btnRemove.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FileAssociationsPanel.this.btnRemoveActionPerformed(evt);
            }
        });
        this.lblType.setLabelFor(this.cbType);
        Mnemonics.setLocalizedText((JLabel)this.lblType, (String)NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.lblType.text"));
        this.cbType.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FileAssociationsPanel.this.cbTypeActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.btnDefault, (String)NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.btnDefault.text"));
        this.btnDefault.setToolTipText(NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.btnDefault.tooltip"));
        this.btnDefault.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FileAssociationsPanel.this.btnDefaultActionPerformed(evt);
            }
        });
        this.lblAssociatedAlso.setLabelFor(this.lblAssociatedAlsoExt);
        Mnemonics.setLocalizedText((JLabel)this.lblAssociatedAlso, (String)NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.lblAssociatedAlso.text"));
        this.lblAssociatedAlso.setEnabled(false);
        this.lblAssociatedAlsoExt.setEnabled(false);
        this.lblAssociatedAlsoExt.setPreferredSize(new Dimension(200, 14));
        Mnemonics.setLocalizedText((JLabel)this.lblFileAssociations, (String)NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.lblFileAssociations.text"));
        Mnemonics.setLocalizedText((JLabel)this.lblFilesIgnored, (String)NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.lblFilesIgnored.text"));
        this.lblPattern.setLabelFor(this.txtPattern);
        Mnemonics.setLocalizedText((JLabel)this.lblPattern, (String)NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.lblPattern.text"));
        this.lblPattern.setToolTipText(NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.lblPattern.tooltip"));
        Mnemonics.setLocalizedText((AbstractButton)this.btnDefaultIgnored, (String)NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.btnDefaultIgnored.text"));
        this.btnDefaultIgnored.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FileAssociationsPanel.this.btnDefaultIgnoredActionPerformed(evt);
            }
        });
        this.jScrollPane1.setHorizontalScrollBarPolicy(31);
        this.jScrollPane1.setVerticalScrollBarPolicy(21);
        this.txtPattern.setLineWrap(true);
        this.jScrollPane1.setViewportView(this.txtPattern);
        this.txtPattern.getAccessibleContext().setAccessibleParent(this);
        this.txtPatternError.setForeground(Color.red);
        this.txtPatternError.setVerticalAlignment(1);
        this.txtPatternError.setFocusable(false);
        Mnemonics.setLocalizedText((AbstractButton)this.autoScan, (String)NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.autoScan.text", (Object[])new Object[0]));
        this.autoScan.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FileAssociationsPanel.this.autoScanActionPerformed(evt);
            }
        });
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.lblFileAssociations).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.sepFileAssociations)).addGroup(layout.createSequentialGroup().addComponent(this.autoScan).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.txtPatternError, -1, 0, 32767)).addGroup(layout.createSequentialGroup().addComponent(this.lblFilesIgnored).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.setFilesIgnored)).addGroup(layout.createSequentialGroup().addGap(10, 10, 10).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.lblExtension).addComponent(this.lblType)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.lblAssociatedAlso).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblAssociatedAlsoExt, -1, 1, 32767)).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.cbExtension, 0, 1, 32767).addComponent(this.cbType, 0, 1, 32767)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.btnNew).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnRemove)).addComponent(this.btnDefault))))).addGroup(layout.createSequentialGroup().addComponent(this.lblPattern).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jScrollPane1).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnDefaultIgnored).addGap(0, 0, 0)))))));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(this.sepFileAssociations, -2, -1, -2).addComponent(this.lblFileAssociations)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.btnNew).addComponent(this.cbExtension, -2, -1, -2).addComponent(this.btnRemove).addComponent(this.lblExtension)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.cbType, -2, -1, -2).addComponent(this.btnDefault).addComponent(this.lblType)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lblAssociatedAlso).addComponent(this.lblAssociatedAlsoExt, -2, -1, -2)).addGap(18, 18, 18).addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(this.setFilesIgnored, -2, -1, -2).addComponent(this.lblFilesIgnored)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.lblPattern).addComponent(this.btnDefaultIgnored).addComponent(this.jScrollPane1, -2, 45, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.txtPatternError, -1, 129, 32767).addContainerGap()).addGroup(layout.createSequentialGroup().addGap(20, 20, 20).addComponent(this.autoScan)))));
        this.lblExtension.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.lblExtension.AN"));
        this.lblExtension.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.lblExtension.AD"));
        this.btnNew.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.btnNew.AN"));
        this.btnNew.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.btnNew.AD"));
        this.btnRemove.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.btnRemove.AN"));
        this.btnRemove.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.btnRemove.AD"));
        this.lblType.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.lblType.AN"));
        this.lblType.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.lblType.AD"));
        this.btnDefault.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.btnDefault.AN"));
        this.btnDefault.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.btnDefault.AD"));
        this.lblAssociatedAlso.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.lblAssociatedAlso.AN"));
        this.lblAssociatedAlso.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.lblAssociatedAlso.AD"));
        this.lblPattern.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.lblPattern.AN"));
        this.lblPattern.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.lblPattern.AD"));
        this.btnDefaultIgnored.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.btnDefaultIgnored.AN"));
        this.btnDefaultIgnored.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.btnDefaultIgnored.AD"));
        this.txtPatternError.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.txtPatternError.AN"));
        this.txtPatternError.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FileAssociationsPanel.class, (String)"FileAssociationsPanel.txtPatternError.AD"));
    }

    private void cbExtensionActionPerformed(ActionEvent evt) {
        if (this.cbExtension.getSelectedItem() == null || this.chooseExtensionItem.equals(this.cbExtension.getSelectedItem().toString())) {
            return;
        }
        String newExtension = this.cbExtension.getSelectedItem().toString();
        if (this.cbExtension.getItemAt(0).equals(this.chooseExtensionItem)) {
            this.cbExtension.removeItem(this.chooseExtensionItem);
        }
        this.cbType.setEnabled(!newExtension.equalsIgnoreCase("xml"));
        this.cbType.setSelectedItem(this.model.getMimeItem(newExtension));
        this.btnRemove.setEnabled(this.model.canBeRemoved(newExtension));
    }

    private void cbTypeActionPerformed(ActionEvent evt) {
        String newMimeType;
        if (this.cbType.getSelectedItem() == null || this.chooseExtensionItem.equals(this.cbExtension.getSelectedItem().toString())) {
            return;
        }
        String extension = this.cbExtension.getSelectedItem().toString();
        if (this.model.setMimeType(extension, newMimeType = ((FileAssociationsModel.MimeItem)this.cbType.getSelectedItem()).getMimeType())) {
            this.fireChanged(extension, newMimeType);
        }
        this.btnDefault.setEnabled(this.model.canBeRestored(extension));
        this.lblAssociatedAlsoExt.setText(this.model.getAssociatedAlso(extension, newMimeType));
    }

    private void btnDefaultActionPerformed(ActionEvent evt) {
        String extension = this.cbExtension.getSelectedItem().toString();
        this.model.setDefault(extension);
        this.btnDefault.setEnabled(false);
        this.cbType.setSelectedItem(this.model.getMimeItem(extension));
        this.fireChanged(extension, this.model.getMimeType(extension));
    }

    private void btnNewActionPerformed(ActionEvent evt) {
        if (this.newExtensionPanel == null) {
            this.newExtensionPanel = new NewExtensionPanel();
        }
        this.newExtensionPanel.setModel(this.model);
        DialogDescriptor dd = new DialogDescriptor((Object)this.newExtensionPanel, NbBundle.getMessage(NewExtensionPanel.class, (String)"NewExtensionPanel.title"));
        this.newExtensionPanel.addExtensionListener(dd);
        DialogDisplayer.getDefault().createDialog(dd).setVisible(true);
        if (DialogDescriptor.OK_OPTION.equals(dd.getValue())) {
            this.newlyAddedExtensions.add(this.newExtensionPanel.getExtension());
            this.fireChanged(null, null);
            ArrayList<String> newItems = new ArrayList<String>();
            for (int i = 0; i < this.cbExtension.getItemCount(); ++i) {
                newItems.add(this.cbExtension.getItemAt(i).toString());
            }
            if (newItems.remove(this.chooseExtensionItem)) {
                this.cbType.setEnabled(true);
            }
            newItems.add(this.newExtensionPanel.getExtension());
            Collections.sort(newItems, String.CASE_INSENSITIVE_ORDER);
            this.cbExtension.removeAllItems();
            for (String item : newItems) {
                this.cbExtension.addItem(item);
            }
            this.cbExtension.setSelectedItem(this.newExtensionPanel.getExtension());
        }
    }

    private void btnRemoveActionPerformed(ActionEvent evt) {
        String extension = this.cbExtension.getSelectedItem().toString();
        this.model.remove(extension);
        int selectedIndex = this.cbExtension.getSelectedIndex();
        this.cbExtension.removeItem(extension);
        if (this.cbExtension.getItemCount() > 0) {
            this.cbExtension.setSelectedIndex(Math.max(0, --selectedIndex));
        }
        this.newlyAddedExtensions.remove(extension);
        this.fireChanged(null, null);
    }

    private void btnDefaultIgnoredActionPerformed(ActionEvent evt) {
        this.txtPattern.setText("^(CVS|SCCS|vssver.?\\.scc|#.*#|%.*%|_svn)$|~$|^\\.(?!(htaccess|git.+|hgignore)$).*$");
    }

    private void autoScanActionPerformed(ActionEvent evt) {
        this.fireChanged(null, null);
    }

    void load() {
        this.cbExtension.removeAllItems();
        this.cbType.removeAllItems();
        this.cbType.setEnabled(false);
        this.btnDefault.setEnabled(false);
        this.btnRemove.setEnabled(false);
        this.lblAssociatedAlsoExt.setText(null);
        this.model = new FileAssociationsModel();
        this.cbExtension.addItem(this.chooseExtensionItem);
        for (String extension : this.model.getExtensions()) {
            this.cbExtension.addItem(extension);
        }
        for (FileAssociationsModel.MimeItem mimePair : this.model.getMimeItems()) {
            this.cbType.addItem(mimePair);
        }
        this.txtPattern.getDocument().removeDocumentListener(this.patternListener);
        this.txtPattern.setText(IgnoredFilesPreferences.getIgnoredFiles());
        this.txtPattern.getDocument().addDocumentListener(this.patternListener);
        this.btnDefaultIgnored.setEnabled(!"^(CVS|SCCS|vssver.?\\.scc|#.*#|%.*%|_svn)$|~$|^\\.(?!(htaccess|git.+|hgignore)$).*$".equals(this.txtPattern.getText()));
        boolean manual = NbPreferences.root().node("org/openide/actions/FileSystemRefreshAction").getBoolean("manual", false);
        this.autoScan.setSelected(!manual);
    }

    void store() {
        this.newlyAddedExtensions.clear();
        this.model.store();
        IgnoredFilesPreferences.setIgnoredFiles(this.txtPattern.getText());
        Preferences nd = NbPreferences.root().node("org/openide/actions/FileSystemRefreshAction");
        boolean manual = nd.getBoolean("manual", false);
        if (manual == this.autoScan.isSelected()) {
            nd.putBoolean("manual", !manual);
        }
    }

    boolean valid() {
        return IgnoredFilesPreferences.isValid(this.txtPattern.getText());
    }

    private void fireChanged(String extension, String mimeType) {
        boolean isChanged = false;
        if (extension == null) {
            isChanged |= !this.txtPattern.getText().equals(IgnoredFilesPreferences.getIgnoredFiles());
            boolean manual = NbPreferences.root().node("org/openide/actions/FileSystemRefreshAction").getBoolean("manual", false);
            isChanged |= this.autoScan.isSelected() == manual;
            isChanged |= !this.newlyAddedExtensions.isEmpty();
        } else {
            isChanged |= !this.model.isInitialExtensionToMimeMapping(extension, mimeType);
        }
        this.controller.changed(isChanged);
    }

    private class PatternDocumentListener
    implements DocumentListener {
        private PatternDocumentListener() {
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            this.patternChanged();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            this.patternChanged();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
        }

        private void patternChanged() {
            FileAssociationsPanel.this.fireChanged(null, null);
            FileAssociationsPanel.this.btnDefaultIgnored.setEnabled(!"^(CVS|SCCS|vssver.?\\.scc|#.*#|%.*%|_svn)$|~$|^\\.(?!(htaccess|git.+|hgignore)$).*$".equals(FileAssociationsPanel.this.txtPattern.getText()));
            if (IgnoredFilesPreferences.getSyntaxError() != null) {
                FileAssociationsPanel.this.txtPatternError.setText("<html><pre>" + IgnoredFilesPreferences.getSyntaxError() + "</pre></html>");
            } else {
                FileAssociationsPanel.this.txtPatternError.setText(null);
            }
        }
    }

}

