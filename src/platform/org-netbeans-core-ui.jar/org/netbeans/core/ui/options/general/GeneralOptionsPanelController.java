/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package org.netbeans.core.ui.options.general;

import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import org.netbeans.core.ui.options.general.GeneralOptionsPanel;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public final class GeneralOptionsPanelController
extends OptionsPanelController {
    private GeneralOptionsPanel generalOptionsPanel;

    public void update() {
        this.getGeneralOptionsPanel().update();
    }

    public void applyChanges() {
        this.getGeneralOptionsPanel().applyChanges();
    }

    public void cancel() {
        this.getGeneralOptionsPanel().cancel();
    }

    public boolean isValid() {
        return this.getGeneralOptionsPanel().dataValid();
    }

    public boolean isChanged() {
        return this.getGeneralOptionsPanel().isChanged();
    }

    public HelpCtx getHelpCtx() {
        return null;
    }

    public JComponent getComponent(Lookup masterLookup) {
        return this.getGeneralOptionsPanel();
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.getGeneralOptionsPanel().addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.getGeneralOptionsPanel().removePropertyChangeListener(l);
    }

    private GeneralOptionsPanel getGeneralOptionsPanel() {
        if (this.generalOptionsPanel == null) {
            this.generalOptionsPanel = new GeneralOptionsPanel();
        }
        return this.generalOptionsPanel;
    }
}

