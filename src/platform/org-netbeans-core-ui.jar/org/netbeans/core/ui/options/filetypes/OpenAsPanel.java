/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.ui.options.filetypes;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ArrayList;
import javax.accessibility.AccessibleContext;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import org.netbeans.core.ui.options.filetypes.FileAssociationsModel;
import org.openide.util.NbBundle;

public class OpenAsPanel
extends JPanel {
    private FileAssociationsModel model;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JScrollPane jScrollPane2;
    private JLabel lblDescription;
    private JLabel lblExtension;
    private JList lstMimeTypes;

    public OpenAsPanel() {
        this.initComponents();
        this.lblDescription.setText("<html>" + this.lblDescription.getText() + "</html>");
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(OpenAsPanel.class, (String)"OpenAsPanel.AD"));
    }

    void setModel(FileAssociationsModel model) {
        this.model = model;
    }

    String getMimeType() {
        if (this.lstMimeTypes.getSelectedValue() != null) {
            return ((FileAssociationsModel.MimeItem)this.lstMimeTypes.getSelectedValue()).getMimeType();
        }
        return null;
    }

    void setExtension(String extension) {
        this.lblExtension.setText(NbBundle.getMessage(OpenAsPanel.class, (String)"OpenAsPanel.lblExtension.text", (Object)extension));
        this.updateList();
    }

    void updateList() {
        this.lstMimeTypes.setListData(this.model.getMimeItems().toArray());
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.lblExtension = new JLabel();
        this.jScrollPane2 = new JScrollPane();
        this.lstMimeTypes = new JList();
        this.jPanel2 = new JPanel();
        this.lblDescription = new JLabel();
        this.setPreferredSize(new Dimension(250, 300));
        this.setLayout(new BorderLayout());
        this.lblExtension.setLabelFor(this.lstMimeTypes);
        this.lblExtension.setText("Treat files as:");
        this.lstMimeTypes.setSelectionMode(0);
        this.jScrollPane2.setViewportView(this.lstMimeTypes);
        GroupLayout jPanel1Layout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane2, -1, 283, 32767).addComponent(this.lblExtension)).addContainerGap()));
        jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.lblExtension).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jScrollPane2, -1, 240, 32767)));
        this.add((Component)this.jPanel1, "Center");
        this.lblDescription.setText(NbBundle.getMessage(OpenAsPanel.class, (String)"OpenAsPanel.lblDescription.text"));
        GroupLayout jPanel2Layout = new GroupLayout(this.jPanel2);
        this.jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel2Layout.createSequentialGroup().addContainerGap().addComponent(this.lblDescription).addContainerGap(-1, 32767)));
        jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel2Layout.createSequentialGroup().addContainerGap().addComponent(this.lblDescription).addContainerGap(-1, 32767)));
        this.add((Component)this.jPanel2, "North");
    }
}

