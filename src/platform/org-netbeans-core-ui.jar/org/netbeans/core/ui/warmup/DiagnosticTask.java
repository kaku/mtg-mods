/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.ui.warmup;

import java.lang.management.ClassLoadingMXBean;
import java.lang.management.CompilationMXBean;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.RuntimeMXBean;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.openide.util.NbBundle;

public final class DiagnosticTask
implements Runnable {
    private static final Logger LOG = Logger.getLogger(DiagnosticTask.class.getName());
    private static boolean executed;

    @Override
    public void run() {
        if (executed) {
            return;
        }
        String diagInfo = this.logParams();
        LOG.info(diagInfo);
        this.logEnv();
        if (Boolean.getBoolean("netbeans.full.hack")) {
            LOG.info("Using netbeans.full.hack=true; see http://wiki.netbeans.org/DevFaqNetBeansFullHack");
        }
        executed = true;
    }

    private void logEnv() {
        try {
            OperatingSystemMXBean osBean = ManagementFactory.getOperatingSystemMXBean();
            Method m = osBean.getClass().getMethod("getTotalPhysicalMemorySize", new Class[0]);
            m.setAccessible(true);
            long freeMem = (Long)m.invoke(osBean, new Object[0]);
            LOG.log(Level.INFO, "Total memory {0}", freeMem);
            LogRecord lr = new LogRecord(Level.INFO, "MEMORY");
            lr.setResourceBundle(NbBundle.getBundle(DiagnosticTask.class));
            lr.setParameters(new Object[]{freeMem});
            Logger.getLogger("org.netbeans.ui.performance").log(lr);
        }
        catch (NoSuchMethodException ex) {
            LOG.log(Level.INFO, null, ex);
        }
        catch (SecurityException ex) {
            LOG.log(Level.INFO, null, ex);
        }
        catch (IllegalAccessException ex) {
            LOG.log(Level.INFO, null, ex);
        }
        catch (IllegalArgumentException ex) {
            LOG.log(Level.INFO, null, ex);
        }
        catch (InvocationTargetException ex) {
            LOG.log(Level.INFO, null, ex);
        }
    }

    private void logMemoryUsage(StringBuilder sb, MemoryUsage usage, String label) {
        long init = usage.getInit();
        long max = usage.getMax();
        sb.append(label).append(" usage: initial ").append(this.formatBytes(init)).append(" maximum ").append(this.formatBytes(max)).append('\n');
    }

    private String formatBytes(long bytes) {
        if (bytes > 0x100000) {
            return MessageFormat.format("{0,number,0.0MB}", (double)bytes / 1024.0 / 1024.0);
        }
        if (bytes > 1024) {
            return MessageFormat.format("{0,number,0.0kB}", (double)bytes / 1024.0);
        }
        return MessageFormat.format("{0,number,0b}", bytes);
    }

    private String formatTime(long time) {
        StringBuilder sb = new StringBuilder();
        if (time > 86400000) {
            sb.append(MessageFormat.format("{0,number,0d}", time / 86400000));
            time %= 86400000;
        }
        if (time > 3600000 || sb.length() > 0) {
            sb.append(MessageFormat.format("{0,number,0h}", time / 3600000));
            time %= 3600000;
        }
        if (time > 60000 || sb.length() > 0) {
            sb.append(MessageFormat.format("{0,number,0m}", time / 60000));
            time %= 60000;
        }
        sb.append(MessageFormat.format("{0,number,0s}", time / 1000));
        return sb.toString();
    }

    private String logParams() {
        StringBuilder sb = new StringBuilder(500);
        sb.append("Diagnostic information\n");
        try {
            RuntimeMXBean rmBean = ManagementFactory.getRuntimeMXBean();
            CompilationMXBean cmpMBean = ManagementFactory.getCompilationMXBean();
            MemoryMXBean memoryBean = ManagementFactory.getMemoryMXBean();
            ClassLoadingMXBean clMBean = ManagementFactory.getClassLoadingMXBean();
            sb.append("Input arguments:");
            for (String s : rmBean.getInputArguments()) {
                sb.append("\n\t").append(s);
            }
            if (cmpMBean != null) {
                sb.append("\nCompiler: " + cmpMBean.getName()).append('\n');
            }
            MemoryUsage usage = memoryBean.getHeapMemoryUsage();
            this.logMemoryUsage(sb, usage, "Heap memory");
            usage = memoryBean.getNonHeapMemoryUsage();
            this.logMemoryUsage(sb, usage, "Non heap memory");
            for (GarbageCollectorMXBean gcMBean : ManagementFactory.getGarbageCollectorMXBeans()) {
                sb.append("Garbage collector: ").append(gcMBean.getName()).append(" (Collections=").append(gcMBean.getCollectionCount()).append(" Total time spent=").append(this.formatTime(gcMBean.getCollectionTime())).append(")\n");
            }
            int clsLoaded = clMBean.getLoadedClassCount();
            long clsTotal = clMBean.getTotalLoadedClassCount();
            long clsUnloaded = clMBean.getUnloadedClassCount();
            sb.append("Classes: loaded=").append(clsLoaded).append(" total loaded=").append(clsTotal).append(" unloaded ").append(clsUnloaded).append('\n');
        }
        catch (NullPointerException ex) {
            LOG.log(Level.WARNING, null, ex);
        }
        return sb.toString();
    }
}

