/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.Places
 *  org.openide.util.Cancellable
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.ui.warmup;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.lang.reflect.Method;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.SwingUtilities;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.core.ui.warmup.Bundle;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.modules.Places;
import org.openide.util.Cancellable;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.windows.WindowManager;

public final class MenuWarmUpTask
implements Runnable {
    private Component[] comps;

    @Override
    public void run() {
        try {
            SwingUtilities.invokeAndWait(new Runnable(){

                @Override
                public void run() {
                    Frame main = WindowManager.getDefault().getMainWindow();
                    assert (main != null);
                    main.addWindowListener(new NbWindowsAdapter());
                    if (main instanceof JFrame) {
                        MenuWarmUpTask.this.comps = ((JFrame)main).getJMenuBar().getComponents();
                    }
                }
            });
        }
        catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
            return;
        }
        catch (Exception e) {
            return;
        }
        if (this.comps != null) {
            this.walkMenu(this.comps);
            this.comps = null;
        }
    }

    private void walkMenu(Component[] items) {
        for (int i = 0; i < items.length; ++i) {
            if (!(items[i] instanceof JMenu)) continue;
            try {
                Class cls = items[i].getClass();
                Method m = cls.getDeclaredMethod("doInitialize", new Class[0]);
                m.setAccessible(true);
                m.invoke(items[i], new Object[0]);
                this.walkMenu(((JMenu)items[i]).getMenuComponents());
                continue;
            }
            catch (Exception e) {
                // empty catch block
            }
        }
    }

    private static class NbWindowsAdapter
    extends WindowAdapter
    implements Runnable,
    Cancellable {
        private static final RequestProcessor rp = new RequestProcessor("Refresh-After-WindowActivated", 1, true);
        private RequestProcessor.Task task;
        private AtomicBoolean goOn;
        private static final Logger UILOG = Logger.getLogger("org.netbeans.ui.focus");
        private static final Logger LOG = Logger.getLogger("org.netbeans.core.ui.focus");
        private boolean warnedNoRefresh;
        private int counter;

        private NbWindowsAdapter() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void windowActivated(WindowEvent e) {
            if (e.getOppositeWindow() == null) {
                RequestProcessor requestProcessor = rp;
                synchronized (requestProcessor) {
                    if (this.task != null) {
                        LOG.fine("Scheduling task after activation");
                        this.task.schedule(1500);
                        this.task = null;
                    } else {
                        LOG.fine("Activation without prepared refresh task");
                    }
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void windowDeactivated(WindowEvent e) {
            if (e.getOppositeWindow() == null) {
                RequestProcessor requestProcessor = rp;
                synchronized (requestProcessor) {
                    if (this.task != null) {
                        this.task.cancel();
                    } else {
                        this.task = rp.create((Runnable)this);
                    }
                    LOG.fine("Window deactivated, preparing refresh task");
                }
                if (UILOG.isLoggable(Level.FINE)) {
                    LogRecord r = new LogRecord(Level.FINE, "LOG_WINDOW_DEACTIVATED");
                    r.setResourceBundleName("org.netbeans.core.ui.warmup.Bundle");
                    r.setResourceBundle(NbBundle.getBundle(MenuWarmUpTask.class));
                    r.setLoggerName(UILOG.getName());
                    UILOG.log(r);
                }
            }
        }

        private static boolean isNoRefresh() {
            if (Boolean.getBoolean("netbeans.indexing.noFileRefresh")) {
                return true;
            }
            return NbPreferences.root().node("org/openide/actions/FileSystemRefreshAction").getBoolean("manual", false);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            block17 : {
                class HandleBridge
                extends ActionEvent
                implements Runnable {
                    private FileObject previous;
                    private long next;
                    final /* synthetic */ ProgressHandle val$h;

                    public HandleBridge() {
                        this.val$h = object;
                        super(source, 0, "");
                    }

                    @Override
                    public void setSource(Object newSource) {
                        if (newSource instanceof Object[]) {
                            long now = System.currentTimeMillis();
                            boolean again = now > this.next;
                            Object[] arr = (Object[])newSource;
                            if (arr.length >= 3 && arr[0] instanceof Integer && arr[1] instanceof Integer && arr[2] instanceof FileObject) {
                                FileObject fo;
                                if (!(this.getSource() instanceof Object[])) {
                                    this.val$h.switchToDeterminate(((Integer)arr[1]).intValue());
                                    LOG.log(Level.FINE, "First refresh progress event delivered: {0}/{1} where {2}, goOn: {3}", arr);
                                }
                                if ((Integer)arr[0] < (Integer)arr[1]) {
                                    this.val$h.progress(((Integer)arr[0]).intValue());
                                }
                                if (this.previous != (fo = (FileObject)arr[2]).getParent() && again) {
                                    this.previous = fo.getParent();
                                    if (this.previous != null) {
                                        this.val$h.progress(this.previous.getPath());
                                    }
                                    this.next = now + 500;
                                }
                                super.setSource(newSource);
                            }
                            if (arr.length >= 4 && arr[3] instanceof AtomicBoolean) {
                                this$0.goOn = (AtomicBoolean)arr[3];
                            }
                            if (arr.length >= 5 && arr[4] == null && again) {
                                arr[4] = Utilities.actionsGlobalContext().lookup(FileObject.class);
                                LOG.log(Level.FINE, "Preferring {0}", arr[4]);
                            }
                        }
                    }

                    @Override
                    public void run() {
                        if (EventQueue.isDispatchThread()) {
                            try {
                                this.val$h.suspend(Bundle.MSG_Refresh_Suspend());
                            }
                            catch (Throwable t) {}
                        } else {
                            EventQueue.invokeLater(this);
                        }
                    }
                }
                if (NbWindowsAdapter.isNoRefresh()) {
                    if (!this.warnedNoRefresh) {
                        LOG.info("External Changes Refresh on focus gain disabled");
                        this.warnedNoRefresh = true;
                    }
                    LOG.fine("Refresh disabled, aborting");
                    return;
                }
                ProgressHandle h = ProgressHandleFactory.createHandle((String)Bundle.MSG_Refresh(), (Cancellable)this, (Action)null);
                if (!LOG.isLoggable(Level.FINE)) {
                    int delay = Integer.parseInt(Bundle.MSG_RefreshDelay());
                    h.setInitialDelay(delay);
                }
                h.start();
                Runnable run = null;
                HandleBridge handleBridge = new HandleBridge(this, (Object)this, h);
                handleBridge.run();
                try {
                    FileObject udFo = FileUtil.toFileObject((File)Places.getUserDirectory());
                    if (udFo != null) {
                        udFo = udFo.getFileSystem().getRoot();
                    }
                    if (udFo != null) {
                        run = (Runnable)udFo.getAttribute("refreshSlow");
                    }
                }
                catch (Exception ex) {
                    LOG.log(Level.FINE, "Error getting refreshSlow", ex);
                }
                long now = System.currentTimeMillis();
                try {
                    if (run == null) {
                        LOG.fine("Starting classical refresh");
                        FileUtil.refreshAll();
                    } else {
                        if (run instanceof AtomicBoolean) {
                            this.goOn = (AtomicBoolean)((Object)run);
                            LOG.fine("goOn controller registered");
                        }
                        LOG.fine("Starting slow refresh");
                        run.equals(handleBridge);
                        run.run();
                    }
                    long took = System.currentTimeMillis() - now;
                    if (UILOG.isLoggable(Level.FINE)) {
                        LogRecord r = new LogRecord(Level.FINE, "LOG_WINDOW_ACTIVATED");
                        r.setParameters(new Object[]{took});
                        r.setResourceBundleName("org.netbeans.core.ui.warmup.Bundle");
                        r.setResourceBundle(NbBundle.getBundle(MenuWarmUpTask.class));
                        r.setLoggerName(UILOG.getName());
                        UILOG.log(r);
                    }
                    LOG.log(Level.FINE, "Refresh done in {0} ms", took);
                    AtomicBoolean ab = this.goOn;
                    if (ab == null || ab.get()) {
                        long sfs = System.currentTimeMillis();
                        FileUtil.getConfigRoot().getFileSystem().refresh(true);
                        LOG.log(Level.FINE, "SystemFileSystem refresh done {0} ms", System.currentTimeMillis() - sfs);
                        break block17;
                    }
                    LOG.fine("Skipping SystemFileSystem refresh");
                }
                catch (FileStateInvalidException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
                finally {
                    h.finish();
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public boolean cancel() {
            RequestProcessor requestProcessor = rp;
            synchronized (requestProcessor) {
                if (this.task != null) {
                    this.task.cancel();
                }
                if (this.goOn != null) {
                    this.goOn.set(false);
                    LOG.log(Level.FINE, "Signaling cancel to {0}", System.identityHashCode(this.goOn));
                } else {
                    LOG.log(Level.FINE, "Cannot signal cancel, goOn is null");
                }
            }
            ++this.counter;
            if (UILOG.isLoggable(Level.FINE)) {
                LogRecord r = new LogRecord(Level.FINE, "LOG_WINDOW_REFRESH_CANCEL");
                r.setParameters(new Object[]{this.counter});
                r.setResourceBundleName("org.netbeans.core.ui.warmup.Bundle");
                r.setResourceBundle(NbBundle.getBundle(MenuWarmUpTask.class));
                r.setLoggerName(UILOG.getName());
                UILOG.log(r);
            }
            return true;
        }

    }

}

