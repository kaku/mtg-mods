/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.ui.warmup;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String MSG_Refresh() {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_Refresh");
    }

    static String MSG_RefreshDelay() {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_RefreshDelay");
    }

    static String MSG_Refresh_Suspend() {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_Refresh_Suspend");
    }

    private void Bundle() {
    }
}

