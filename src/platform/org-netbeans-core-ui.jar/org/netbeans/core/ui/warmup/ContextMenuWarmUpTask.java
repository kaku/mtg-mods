/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.actions.ActionManager
 *  org.openide.actions.ToolsAction
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.Lookup
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.ProxyLookup
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.ui.warmup;

import java.awt.EventQueue;
import javax.swing.Action;
import javax.swing.JMenuItem;
import org.openide.actions.ActionManager;
import org.openide.actions.ToolsAction;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.ContextAwareAction;
import org.openide.util.Lookup;
import org.openide.util.actions.Presenter;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.ProxyLookup;
import org.openide.windows.TopComponent;

public final class ContextMenuWarmUpTask
implements Runnable {
    @Override
    public void run() {
        assert (EventQueue.isDispatchThread());
        ActionManager.getDefault().getContextActions();
        JMenuItem mi = new JMenuItem();
        ContextMenuWarmUpTask.warmUpToolsPopupMenuItem();
    }

    private static void warmUpToolsPopupMenuItem() {
        Action action;
        ProxyLookup lookup;
        JMenuItem toolsMenuItem;
        SystemAction toolsAction = SystemAction.get(ToolsAction.class);
        if (toolsAction instanceof ContextAwareAction && (action = ((ContextAwareAction)toolsAction).createContextAwareInstance((Lookup)(lookup = new ProxyLookup(new Lookup[]{new AbstractNode(Children.LEAF).getLookup(), new TopComponent().getLookup()})))) instanceof Presenter.Popup && (toolsMenuItem = ((Presenter.Popup)action).getPopupPresenter()) instanceof Runnable) {
            ((Runnable)((Object)toolsMenuItem)).run();
        }
    }
}

