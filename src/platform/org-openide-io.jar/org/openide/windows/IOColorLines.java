/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 */
package org.openide.windows;

import java.awt.Color;
import java.io.IOException;
import org.openide.util.Lookup;
import org.openide.windows.InputOutput;
import org.openide.windows.OutputListener;

public abstract class IOColorLines {
    private static IOColorLines find(InputOutput io) {
        if (io instanceof Lookup.Provider) {
            Lookup.Provider p = (Lookup.Provider)io;
            return (IOColorLines)p.getLookup().lookup(IOColorLines.class);
        }
        return null;
    }

    public static void println(InputOutput io, CharSequence text, Color color) throws IOException {
        IOColorLines iocl = IOColorLines.find(io);
        if (iocl != null) {
            iocl.println(text, null, false, color);
        }
    }

    public static void println(InputOutput io, CharSequence text, OutputListener listener, boolean important, Color color) throws IOException {
        IOColorLines iocl = IOColorLines.find(io);
        if (iocl != null) {
            iocl.println(text, listener, important, color);
        }
    }

    public static boolean isSupported(InputOutput io) {
        return IOColorLines.find(io) != null;
    }

    protected abstract void println(CharSequence var1, OutputListener var2, boolean var3, Color var4) throws IOException;
}

