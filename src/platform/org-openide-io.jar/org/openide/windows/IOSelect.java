/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.Parameters
 */
package org.openide.windows;

import java.util.Set;
import org.openide.util.Lookup;
import org.openide.util.Parameters;
import org.openide.windows.InputOutput;

public abstract class IOSelect {
    private static IOSelect find(InputOutput io) {
        if (io instanceof Lookup.Provider) {
            Lookup.Provider p = (Lookup.Provider)io;
            return (IOSelect)p.getLookup().lookup(IOSelect.class);
        }
        return null;
    }

    public static void select(InputOutput io, Set<AdditionalOperation> extraOps) {
        Parameters.notNull((CharSequence)"extraOps", extraOps);
        IOSelect ios = IOSelect.find(io);
        if (ios != null) {
            ios.select(extraOps);
        } else {
            io.select();
        }
    }

    public static boolean isSupported(InputOutput io) {
        return IOSelect.find(io) != null;
    }

    protected abstract void select(Set<AdditionalOperation> var1);

    public static enum AdditionalOperation {
        OPEN,
        REQUEST_VISIBLE,
        REQUEST_ACTIVE;
        

        private AdditionalOperation() {
        }
    }

}

