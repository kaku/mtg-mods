/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 */
package org.openide.windows;

import org.openide.util.Lookup;
import org.openide.windows.InputOutput;

public abstract class IOPosition {
    private static IOPosition find(InputOutput io) {
        if (io instanceof Lookup.Provider) {
            Lookup.Provider p = (Lookup.Provider)io;
            return (IOPosition)p.getLookup().lookup(IOPosition.class);
        }
        return null;
    }

    public static Position currentPosition(InputOutput io) {
        IOPosition iop = IOPosition.find(io);
        return iop != null ? iop.currentPosition() : null;
    }

    public static boolean isSupported(InputOutput io) {
        return IOPosition.find(io) != null;
    }

    protected abstract Position currentPosition();

    public static interface Position {
        public void scrollTo();
    }

}

