/*
 * Decompiled with CFR 0_118.
 */
package org.openide.windows;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import org.openide.windows.OutputListener;

public abstract class OutputWriter
extends PrintWriter {
    protected OutputWriter(Writer w) {
        super(w);
    }

    public abstract void println(String var1, OutputListener var2) throws IOException;

    public void println(String s, OutputListener l, boolean important) throws IOException {
        this.println(s, l);
    }

    public abstract void reset() throws IOException;
}

