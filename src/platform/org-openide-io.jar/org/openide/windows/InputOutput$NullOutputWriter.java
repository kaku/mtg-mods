/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.io.NullOutputStream
 */
package org.openide.windows;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import org.openide.util.io.NullOutputStream;
import org.openide.windows.OutputListener;
import org.openide.windows.OutputWriter;

final class InputOutput$NullOutputWriter
extends OutputWriter {
    InputOutput$NullOutputWriter() {
        super(new OutputStreamWriter((OutputStream)new NullOutputStream()));
    }

    @Override
    public void reset() {
    }

    @Override
    public void println(String s, OutputListener l) {
    }
}

