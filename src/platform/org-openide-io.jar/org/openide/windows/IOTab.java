/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 */
package org.openide.windows;

import javax.swing.Icon;
import org.openide.util.Lookup;
import org.openide.windows.InputOutput;

public abstract class IOTab {
    private static IOTab find(InputOutput io) {
        if (io instanceof Lookup.Provider) {
            Lookup.Provider p = (Lookup.Provider)io;
            return (IOTab)p.getLookup().lookup(IOTab.class);
        }
        return null;
    }

    public static Icon getIcon(InputOutput io) {
        IOTab iot = IOTab.find(io);
        return iot != null ? iot.getIcon() : null;
    }

    public static void setIcon(InputOutput io, Icon icon) {
        IOTab iot = IOTab.find(io);
        if (iot != null) {
            iot.setIcon(icon);
        }
    }

    public static String getToolTipText(InputOutput io) {
        IOTab iot = IOTab.find(io);
        return iot != null ? iot.getToolTipText() : null;
    }

    public static void setToolTipText(InputOutput io, String text) {
        IOTab iot = IOTab.find(io);
        if (iot != null) {
            iot.setToolTipText(text);
        }
    }

    public static boolean isSupported(InputOutput io) {
        return IOTab.find(io) != null;
    }

    protected abstract Icon getIcon();

    protected abstract void setIcon(Icon var1);

    protected abstract String getToolTipText();

    protected abstract void setToolTipText(String var1);
}

