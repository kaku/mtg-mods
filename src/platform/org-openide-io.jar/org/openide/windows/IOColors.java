/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 */
package org.openide.windows;

import java.awt.Color;
import org.openide.util.Lookup;
import org.openide.windows.InputOutput;

public abstract class IOColors {
    private static IOColors find(InputOutput io) {
        if (io instanceof Lookup.Provider) {
            Lookup.Provider p = (Lookup.Provider)io;
            return (IOColors)p.getLookup().lookup(IOColors.class);
        }
        return null;
    }

    public static Color getColor(InputOutput io, OutputType type) {
        IOColors ioc = IOColors.find(io);
        return ioc != null ? ioc.getColor(type) : null;
    }

    public static void setColor(InputOutput io, OutputType type, Color color) {
        IOColors ioc = IOColors.find(io);
        if (ioc != null) {
            ioc.setColor(type, color);
        }
    }

    public static boolean isSupported(InputOutput io) {
        return IOColors.find(io) != null;
    }

    protected abstract Color getColor(OutputType var1);

    protected abstract void setColor(OutputType var1, Color var2);

    public static enum OutputType {
        OUTPUT,
        ERROR,
        HYPERLINK,
        HYPERLINK_IMPORTANT,
        INPUT,
        LOG_SUCCESS,
        LOG_FAILURE,
        LOG_WARNING,
        LOG_DEBUG;
        

        private OutputType() {
        }
    }

}

