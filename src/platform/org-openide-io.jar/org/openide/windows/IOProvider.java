/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.util.Lookup
 */
package org.openide.windows;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Collection;
import javax.swing.Action;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.openide.util.Lookup;
import org.openide.windows.IOContainer;
import org.openide.windows.InputOutput;
import org.openide.windows.OutputListener;
import org.openide.windows.OutputWriter;

public abstract class IOProvider {
    public static IOProvider getDefault() {
        IOProvider iop = (IOProvider)Lookup.getDefault().lookup(IOProvider.class);
        if (iop == null) {
            iop = new Trivial();
        }
        return iop;
    }

    public static IOProvider get(String name) {
        Collection res = Lookup.getDefault().lookupAll(IOProvider.class);
        for (IOProvider iop : res) {
            if (!iop.getName().equals(name)) continue;
            return iop;
        }
        return IOProvider.getDefault();
    }

    protected IOProvider() {
    }

    public abstract InputOutput getIO(String var1, boolean var2);

    public InputOutput getIO(String name, Action[] actions) {
        return this.getIO(name, true);
    }

    public InputOutput getIO(String name, Action[] actions, IOContainer ioContainer) {
        return this.getIO(name, actions);
    }

    @NonNull
    public InputOutput getIO(@NonNull String name, boolean newIO, @NonNull Action[] actions, @NullAllowed IOContainer ioContainer) {
        return this.getIO(name, actions);
    }

    public String getName() {
        return "";
    }

    public abstract OutputWriter getStdOut();

    private static final class Trivial
    extends IOProvider {
        private static final Reader in = new BufferedReader(new InputStreamReader(System.in));
        private static final PrintStream out = System.out;
        private static final PrintStream err = System.err;

        @Override
        public InputOutput getIO(String name, boolean newIO) {
            return new TrivialIO(name);
        }

        @Override
        public OutputWriter getStdOut() {
            return new TrivialOW(out, "stdout");
        }

        private static final class TrivialOW
        extends OutputWriter {
            private static int count = 0;
            private final String name;
            private final PrintStream stream;

            public TrivialOW(PrintStream stream, String name) {
                super(new StringWriter());
                this.stream = stream;
                this.name = name != null ? name : "anon-" + ++count;
            }

            private void prefix(boolean hyperlink) {
                if (hyperlink) {
                    this.stream.print("[" + this.name + "]* ");
                } else {
                    this.stream.print("[" + this.name + "]  ");
                }
            }

            @Override
            public void println(String s, OutputListener l) throws IOException {
                this.prefix(l != null);
                this.stream.println(s);
            }

            @Override
            public void reset() throws IOException {
            }

            @Override
            public void println(float x) {
                this.prefix(false);
                this.stream.println(x);
            }

            @Override
            public void println(double x) {
                this.prefix(false);
                this.stream.println(x);
            }

            @Override
            public void println() {
                this.prefix(false);
                this.stream.println();
            }

            @Override
            public void println(Object x) {
                this.prefix(false);
                this.stream.println(x);
            }

            @Override
            public void println(int x) {
                this.prefix(false);
                this.stream.println(x);
            }

            @Override
            public void println(char x) {
                this.prefix(false);
                this.stream.println(x);
            }

            @Override
            public void println(long x) {
                this.prefix(false);
                this.stream.println(x);
            }

            @Override
            public void println(char[] x) {
                this.prefix(false);
                this.stream.println(x);
            }

            @Override
            public void println(boolean x) {
                this.prefix(false);
                this.stream.println(x);
            }

            @Override
            public void println(String x) {
                this.prefix(false);
                this.stream.println(x);
            }

            @Override
            public void write(int c) {
                this.stream.write(c);
            }

            @Override
            public void write(char[] buf, int off, int len) {
                String s = new String(buf, off, len);
                if (s.endsWith("\n")) {
                    this.println(s.substring(0, s.length() - 1));
                } else {
                    try {
                        this.stream.write(s.getBytes());
                    }
                    catch (IOException x) {
                        // empty catch block
                    }
                }
            }

            @Override
            public void write(String s, int off, int len) {
                if ((s = s.substring(off, off + len)).endsWith("\n")) {
                    this.println(s.substring(0, s.length() - 1));
                } else {
                    try {
                        this.stream.write(s.getBytes());
                    }
                    catch (IOException x) {
                        // empty catch block
                    }
                }
            }
        }

        private final class TrivialIO
        implements InputOutput {
            private final String name;

            public TrivialIO(String name) {
                this.name = name;
            }

            @Override
            public Reader getIn() {
                return in;
            }

            @Override
            public OutputWriter getOut() {
                return new TrivialOW(out, this.name);
            }

            @Override
            public OutputWriter getErr() {
                return new TrivialOW(err, this.name);
            }

            @Override
            public Reader flushReader() {
                return this.getIn();
            }

            @Override
            public boolean isClosed() {
                return false;
            }

            @Override
            public boolean isErrSeparated() {
                return false;
            }

            @Override
            public boolean isFocusTaken() {
                return false;
            }

            @Override
            public void closeInputOutput() {
            }

            @Override
            public void select() {
            }

            @Override
            public void setErrSeparated(boolean value) {
            }

            @Override
            public void setErrVisible(boolean value) {
            }

            @Override
            public void setFocusTaken(boolean value) {
            }

            @Override
            public void setInputVisible(boolean value) {
            }

            @Override
            public void setOutputVisible(boolean value) {
            }
        }

    }

}

