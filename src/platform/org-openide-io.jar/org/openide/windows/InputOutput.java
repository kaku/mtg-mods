/*
 * Decompiled with CFR 0_118.
 */
package org.openide.windows;

import java.io.Reader;
import org.openide.windows.InputOutput$Null;
import org.openide.windows.OutputWriter;

public interface InputOutput {
    public static final InputOutput NULL = new InputOutput$Null();
    @Deprecated
    public static final Reader nullReader = InputOutput$Null.NULL_READER;
    @Deprecated
    public static final OutputWriter nullWriter = InputOutput$Null.NULL_WRITER;

    public OutputWriter getOut();

    public Reader getIn();

    public OutputWriter getErr();

    public void closeInputOutput();

    public boolean isClosed();

    public void setOutputVisible(boolean var1);

    public void setErrVisible(boolean var1);

    public void setInputVisible(boolean var1);

    public void select();

    public boolean isErrSeparated();

    public void setErrSeparated(boolean var1);

    public boolean isFocusTaken();

    public void setFocusTaken(boolean var1);

    @Deprecated
    public Reader flushReader();
}

