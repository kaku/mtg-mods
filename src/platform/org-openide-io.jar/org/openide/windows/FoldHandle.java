/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 */
package org.openide.windows;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.CheckForNull;
import org.openide.windows.IOFolding;

public final class FoldHandle {
    private final IOFolding.FoldHandleDefinition definition;
    private static final Logger LOG = Logger.getLogger(FoldHandle.class.getName());
    private FoldHandle currentChild;
    private boolean finished = false;

    FoldHandle(IOFolding.FoldHandleDefinition definition) {
        this.definition = definition;
    }

    public void finish() {
        this.definition.finish();
        this.finished = true;
    }

    public FoldHandle startFold(boolean expanded) {
        this.currentChild = new FoldHandle(this.definition.startFold(expanded));
        return this.currentChild;
    }

    public void setExpanded(boolean expanded) {
        this.definition.setExpanded(expanded);
    }

    public boolean isFinished() {
        return this.finished;
    }

    @CheckForNull
    public FoldHandle getLastNestedFold() {
        return this.currentChild;
    }

    @CheckForNull
    public FoldHandle getCurrentNestedFold() {
        return this.currentChild != null && !this.currentChild.isFinished() ? this.currentChild : null;
    }

    public void silentFinish() {
        if (!this.finished) {
            if (this.currentChild != null && !this.currentChild.finished) {
                this.currentChild.silentFinish();
            }
            try {
                this.finish();
            }
            catch (IllegalStateException ex) {
                LOG.log(Level.FINE, "Cannot finish fold", ex);
            }
        }
    }

    @CheckForNull
    public FoldHandle silentStartFold(boolean expanded) {
        if (!this.finished) {
            if (this.currentChild != null && !this.currentChild.finished) {
                this.currentChild.silentFinish();
            }
            try {
                return this.startFold(expanded);
            }
            catch (IllegalStateException ex) {
                LOG.log(Level.FINE, "Cannot start fold", ex);
                return null;
            }
        }
        LOG.log(Level.FINE, "silentStartFold - already finished");
        return null;
    }
}

