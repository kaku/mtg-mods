/*
 * Decompiled with CFR 0_118.
 */
package org.openide.windows;

import java.util.EventObject;
import org.openide.windows.InputOutput;

public abstract class OutputEvent
extends EventObject {
    static final long serialVersionUID = 4809584286971828815L;

    public OutputEvent(InputOutput src) {
        super(src);
    }

    public abstract String getLine();

    public InputOutput getInputOutput() {
        return (InputOutput)this.getSource();
    }
}

