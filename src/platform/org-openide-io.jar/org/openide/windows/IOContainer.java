/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Parameters
 */
package org.openide.windows;

import java.awt.Component;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import org.openide.util.Lookup;
import org.openide.util.Parameters;

public final class IOContainer {
    private static IOContainer defaultIOContainer;
    private Provider provider;
    private static final Logger LOGGER;

    public static IOContainer create(Provider provider) {
        Parameters.notNull((CharSequence)"provider", (Object)provider);
        return new IOContainer(provider);
    }

    public static IOContainer getDefault() {
        if (defaultIOContainer == null) {
            Provider provider = (Provider)Lookup.getDefault().lookup(Provider.class);
            if (provider == null) {
                provider = new Trivial();
            }
            defaultIOContainer = IOContainer.create(provider);
        }
        return defaultIOContainer;
    }

    private IOContainer(Provider provider) {
        this.provider = provider;
    }

    public void open() {
        this.log("open()", new Object[0]);
        this.provider.open();
    }

    public void requestActive() {
        this.log("requestActive()", new Object[0]);
        this.provider.requestActive();
    }

    public void requestVisible() {
        this.log("requestVisible()", new Object[0]);
        this.provider.requestVisible();
    }

    public boolean isActivated() {
        this.log("isActivated()", new Object[0]);
        return this.provider.isActivated();
    }

    public void add(JComponent comp, CallBacks cb) {
        this.log("add()", comp, cb);
        this.provider.add(comp, cb);
    }

    public void remove(JComponent comp) {
        this.log("remove()", comp);
        this.provider.remove(comp);
    }

    public void select(JComponent comp) {
        this.log("select()", comp);
        this.provider.select(comp);
    }

    public JComponent getSelected() {
        this.log("getSelected()", new Object[0]);
        return this.provider.getSelected();
    }

    public void setTitle(JComponent comp, String name) {
        this.log("setTitle()", comp, name);
        this.provider.setTitle(comp, name);
    }

    public void setToolTipText(JComponent comp, String text) {
        this.log("setToolTipText()", comp, text);
        this.provider.setToolTipText(comp, text);
    }

    public void setIcon(JComponent comp, Icon icon) {
        this.log("setIcon()", comp, icon);
        this.provider.setIcon(comp, icon);
    }

    public void setToolbarActions(JComponent comp, Action[] toolbarActions) {
        this.log("setToolbarActions()", comp, toolbarActions);
        this.provider.setToolbarActions(comp, toolbarActions);
    }

    public boolean isCloseable(JComponent comp) {
        this.log("isCloseable()", comp);
        return this.provider.isCloseable(comp);
    }

    private synchronized /* varargs */ void log(String msg, Object ... items) {
        LOGGER.log(Level.FINER, "{0}: {1} {2}", new Object[]{this.provider.getClass(), msg, Arrays.asList(items)});
        IOContainer.checkIsEDT();
    }

    private static void checkIsEDT() {
        if (!SwingUtilities.isEventDispatchThread()) {
            StackTraceElement[] elems;
            Level level = Level.FINE;
            assert ((level = Level.WARNING) != null);
            boolean isJDKProblem = false;
            for (StackTraceElement elem : elems = Thread.currentThread().getStackTrace()) {
                if (!"java.awt.EventDispatchThread".equals(elem.getClassName())) continue;
                isJDKProblem = true;
                break;
            }
            if (!isJDKProblem) {
                LOGGER.log(level, null, new IllegalStateException("Should be called from AWT thread."));
            } else {
                LOGGER.log(level, null, new IllegalStateException("Known problem in JDK occurred. If you are interested, vote and report at:\nhttp://bugs.sun.com/view_bug.do?bug_id=6424157, http://bugs.sun.com/view_bug.do?bug_id=6553239 \nAlso see related discussion at http://www.netbeans.org/issues/show_bug.cgi?id=90590"));
            }
        }
    }

    static {
        LOGGER = Logger.getLogger(IOContainer.class.getName());
    }

    private static class Trivial
    extends JTabbedPane
    implements Provider {
        private JFrame frame;

        private Trivial() {
        }

        @Override
        public void open() {
            if (this.frame == null) {
                this.frame = new JFrame();
                this.frame.setDefaultCloseOperation(1);
                this.frame.add(this);
                if (this.getTabCount() > 0) {
                    this.frame.setTitle(this.getTitleAt(0));
                }
                this.frame.pack();
            }
            this.frame.setVisible(true);
        }

        @Override
        public void requestActive() {
            if (this.frame == null) {
                this.open();
            }
            this.frame.requestFocus();
        }

        @Override
        public void requestVisible() {
            this.open();
        }

        @Override
        public boolean isActivated() {
            if (this.frame == null) {
                return false;
            }
            return this.frame.isActive();
        }

        @Override
        public void add(JComponent comp, CallBacks cb) {
            this.add(comp);
        }

        @Override
        public void remove(JComponent comp) {
            this.remove(comp);
        }

        @Override
        public void select(JComponent comp) {
            this.setSelectedComponent(comp);
        }

        @Override
        public JComponent getSelected() {
            return (JComponent)this.getSelectedComponent();
        }

        @Override
        public void setTitle(JComponent comp, String name) {
            this.setTitleAt(this.indexOfComponent(comp), name);
        }

        @Override
        public void setToolTipText(JComponent comp, String text) {
            this.setToolTipTextAt(this.indexOfComponent(comp), text);
        }

        @Override
        public void setIcon(JComponent comp, Icon icon) {
            this.setIconAt(this.indexOfComponent(comp), icon);
        }

        @Override
        public void setToolbarActions(JComponent comp, Action[] toolbarActions) {
        }

        @Override
        public boolean isCloseable(JComponent comp) {
            return true;
        }
    }

    public static interface CallBacks {
        public void closed();

        public void selected();

        public void activated();

        public void deactivated();
    }

    public static interface Provider {
        public void open();

        public void requestActive();

        public void requestVisible();

        public boolean isActivated();

        public void add(JComponent var1, CallBacks var2);

        public void remove(JComponent var1);

        public void select(JComponent var1);

        public JComponent getSelected();

        public void setTitle(JComponent var1, String var2);

        public void setToolTipText(JComponent var1, String var2);

        public void setIcon(JComponent var1, Icon var2);

        public void setToolbarActions(JComponent var1, Action[] var2);

        public boolean isCloseable(JComponent var1);
    }

}

