/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.io.NullInputStream
 */
package org.openide.windows;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import org.openide.util.io.NullInputStream;
import org.openide.windows.InputOutput;
import org.openide.windows.InputOutput$NullOutputWriter;
import org.openide.windows.OutputWriter;

final class InputOutput$Null
implements InputOutput {
    static Reader NULL_READER = new InputStreamReader((InputStream)new NullInputStream());
    static OutputWriter NULL_WRITER = new InputOutput$NullOutputWriter();

    @Override
    public OutputWriter getOut() {
        return NULL_WRITER;
    }

    @Override
    public Reader getIn() {
        return NULL_READER;
    }

    @Override
    public OutputWriter getErr() {
        return NULL_WRITER;
    }

    @Override
    public void closeInputOutput() {
    }

    @Override
    public boolean isClosed() {
        return true;
    }

    @Override
    public void setOutputVisible(boolean value) {
    }

    @Override
    public void setErrVisible(boolean value) {
    }

    @Override
    public void setInputVisible(boolean value) {
    }

    @Override
    public void select() {
    }

    @Override
    public boolean isErrSeparated() {
        return false;
    }

    @Override
    public void setErrSeparated(boolean value) {
    }

    @Override
    public boolean isFocusTaken() {
        return false;
    }

    @Override
    public void setFocusTaken(boolean value) {
    }

    @Override
    public Reader flushReader() {
        return NULL_READER;
    }
}

