/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 */
package org.openide.windows;

import java.awt.Color;
import java.io.IOException;
import org.openide.util.Lookup;
import org.openide.windows.InputOutput;
import org.openide.windows.OutputListener;

public abstract class IOColorPrint {
    private static IOColorPrint find(InputOutput io) {
        if (io instanceof Lookup.Provider) {
            Lookup.Provider p = (Lookup.Provider)io;
            return (IOColorPrint)p.getLookup().lookup(IOColorPrint.class);
        }
        return null;
    }

    public static void print(InputOutput io, CharSequence text, Color color) throws IOException {
        IOColorPrint iocl = IOColorPrint.find(io);
        if (iocl != null) {
            iocl.print(text, null, false, color);
        }
    }

    public static void print(InputOutput io, CharSequence text, OutputListener listener, boolean important, Color color) throws IOException {
        IOColorPrint iocl = IOColorPrint.find(io);
        if (iocl != null) {
            iocl.print(text, listener, important, color);
        }
    }

    public static boolean isSupported(InputOutput io) {
        return IOColorPrint.find(io) != null;
    }

    protected abstract void print(CharSequence var1, OutputListener var2, boolean var3, Color var4) throws IOException;
}

