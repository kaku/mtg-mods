/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckReturnValue
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.Parameters
 */
package org.openide.windows;

import org.netbeans.api.annotations.common.CheckReturnValue;
import org.netbeans.api.annotations.common.NonNull;
import org.openide.util.Lookup;
import org.openide.util.Parameters;
import org.openide.windows.FoldHandle;
import org.openide.windows.InputOutput;

public abstract class IOFolding {
    public static boolean isSupported(@NonNull InputOutput io) {
        Parameters.notNull((CharSequence)"parent", (Object)io);
        return IOFolding.findIOFolding(io) != null;
    }

    private static IOFolding findIOFolding(InputOutput parent) {
        return parent instanceof Lookup.Provider ? (IOFolding)((Lookup.Provider)parent).getLookup().lookup(IOFolding.class) : null;
    }

    @NonNull
    protected abstract FoldHandleDefinition startFold(boolean var1);

    @CheckReturnValue
    @NonNull
    public static FoldHandle startFold(@NonNull InputOutput io, boolean expanded) {
        Parameters.notNull((CharSequence)"io", (Object)io);
        IOFolding folding = IOFolding.findIOFolding(io);
        if (folding == null) {
            throw new UnsupportedOperationException("The InputOutput doesn't support folding");
        }
        return new FoldHandle(folding.startFold(expanded));
    }

    protected static abstract class FoldHandleDefinition {
        protected FoldHandleDefinition() {
        }

        public abstract void finish();

        public abstract FoldHandleDefinition startFold(boolean var1);

        public abstract void setExpanded(boolean var1);
    }

}

