/*
 * Decompiled with CFR 0_118.
 */
package org.openide.windows;

import java.util.EventListener;
import org.openide.windows.OutputEvent;

public interface OutputListener
extends EventListener {
    public void outputLineSelected(OutputEvent var1);

    public void outputLineAction(OutputEvent var1);

    public void outputLineCleared(OutputEvent var1);
}

