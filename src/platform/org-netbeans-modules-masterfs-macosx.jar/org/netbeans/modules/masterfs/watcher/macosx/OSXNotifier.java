/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.jna.Callback
 *  com.sun.jna.Library
 *  com.sun.jna.Native
 *  com.sun.jna.NativeLong
 *  com.sun.jna.Pointer
 *  org.netbeans.modules.masterfs.providers.Notifier
 */
package org.netbeans.modules.masterfs.watcher.macosx;

import com.sun.jna.Callback;
import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Exchanger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.masterfs.providers.Notifier;

public final class OSXNotifier
extends Notifier<Void> {
    private static final Level DEBUG_LOG_LEVEL = Level.FINE;
    private static final Level PERF_LOG_LEVEL = Level.FINE;
    private static final long kFSEventStreamEventIdSinceNow = -1;
    private static final int kFSEventStreamCreateFlagNoDefer = 2;
    private static final int kFSEventStreamEventFlagMustScanSubDirs = 1;
    private static final int kFSEventStreamEventFlagMount = 64;
    private static final int kFSEventStreamEventFlagUnmount = 128;
    private static final double LATENCY = 1.0;
    private static final int ENC_MAC_ROMAN = 0;
    private static final String DEFAULT_RUN_LOOP_MODE = "kCFRunLoopDefaultMode";
    private static final Logger LOG = Logger.getLogger(OSXNotifier.class.getName());
    private final CoreFoundation cf = (CoreFoundation)Native.loadLibrary((String)"CoreFoundation", CoreFoundation.class);
    private final CoreServices cs = (CoreServices)Native.loadLibrary((String)"CoreServices", CoreServices.class);
    private final EventCallback callback;
    private final BlockingQueue<String> events;
    private ExecutorService worker;
    private Pointer[] rtData;
    private static final String ALL_CHANGE = "ALL-CHANGE";

    public OSXNotifier() {
        this.callback = new EventCallbackImpl();
        this.events = new LinkedBlockingQueue<String>();
    }

    public Void addWatch(String path) throws IOException {
        return null;
    }

    public void removeWatch(Void key) throws IOException {
    }

    public String nextEvent() throws IOException, InterruptedException {
        String event = this.events.take();
        return event == "ALL-CHANGE" ? null : event;
    }

    public synchronized void start() throws IOException {
        Object _data;
        if (this.worker != null) {
            throw new IllegalStateException("FileSystemWatcher already started.");
        }
        this.worker = Executors.newSingleThreadExecutor(new DaemonThreadFactory());
        final Exchanger<Object> exchanger = new Exchanger<Object>();
        this.worker.execute(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                try {
                    Pointer[] _rtData = null;
                    try {
                        _rtData = OSXNotifier.this.createFSEventStream();
                    }
                    catch (Throwable ex) {
                        exchanger.exchange(ex);
                    }
                    finally {
                        if (_rtData != null) {
                            exchanger.exchange(_rtData);
                            OSXNotifier.this.cf.CFRunLoopRun();
                        }
                    }
                }
                catch (InterruptedException ie) {
                    LOG.log(Level.WARNING, "Watcher interruped during start", ie);
                }
            }
        });
        try {
            _data = exchanger.exchange(null);
        }
        catch (InterruptedException ex) {
            throw (InterruptedIOException)new InterruptedIOException().initCause(ex);
        }
        assert (_data != null);
        if (_data instanceof Throwable) {
            this.worker.shutdown();
            this.worker = null;
            throw new IOException(_data);
        }
        this.rtData = _data;
    }

    public synchronized void stop() throws IOException {
        if (this.worker == null) {
            throw new IllegalStateException("FileSystemWatcher is not started.");
        }
        assert (this.rtData != null);
        assert (this.rtData.length == 2);
        assert (this.rtData[0] != null);
        assert (this.rtData[1] != null);
        this.cs.FSEventStreamStop(this.rtData[0]);
        this.cs.FSEventStreamInvalidate(this.rtData[0]);
        this.cs.FSEventStreamRelease(this.rtData[0]);
        this.cf.CFRunLoopStop(this.rtData[1]);
        this.worker.shutdown();
        this.worker = null;
        this.rtData = null;
    }

    private Pointer[] createFSEventStream() throws IOException {
        Pointer root = this.cf.CFStringCreateWithCString(Pointer.NULL, "/", 0);
        if (root == Pointer.NULL) {
            throw new IOException("Path creation failed.");
        }
        Pointer arr = this.cf.CFArrayCreateMutable(Pointer.NULL, new NativeLong(1), Pointer.NULL);
        if (arr == Pointer.NULL) {
            throw new IOException("Path list creation failed.");
        }
        this.cf.CFArrayAppendValue(arr, root);
        Pointer eventStream = this.cs.FSEventStreamCreate(Pointer.NULL, this.callback, Pointer.NULL, arr, -1, 1.0, 2);
        if (eventStream == Pointer.NULL) {
            throw new IOException("Creation of FSEventStream failed.");
        }
        Pointer loop = this.cf.CFRunLoopGetCurrent();
        if (eventStream == Pointer.NULL) {
            throw new IOException("Cannot find run loop for caller.");
        }
        Pointer kCFRunLoopDefaultMode = this.findDefaultMode(loop);
        if (kCFRunLoopDefaultMode == null) {
            throw new IOException("Caller has no defaul run loop mode.");
        }
        this.cs.FSEventStreamScheduleWithRunLoop(eventStream, loop, kCFRunLoopDefaultMode);
        if (LOG.isLoggable(DEBUG_LOG_LEVEL)) {
            LOG.log(DEBUG_LOG_LEVEL, this.getStreamDescription(eventStream));
        }
        this.cs.FSEventStreamStart(eventStream);
        return new Pointer[]{eventStream, loop};
    }

    private Pointer findDefaultMode(Pointer runLoop) {
        Pointer modes = this.cf.CFRunLoopCopyAllModes(runLoop);
        if (modes != Pointer.NULL) {
            int modesCount = this.cf.CFArrayGetCount(modes).intValue();
            for (int i = 0; i < modesCount; ++i) {
                Pointer mode = this.cf.CFArrayGetValueAtIndex(modes, new NativeLong((long)i));
                if (mode == Pointer.NULL || !"kCFRunLoopDefaultMode".equals(this.cf.CFStringGetCStringPtr(mode, 0))) continue;
                return mode;
            }
        }
        return null;
    }

    private String getStreamDescription(Pointer eventStream) {
        Pointer desc = this.cs.FSEventStreamCopyDescription(eventStream);
        return desc == Pointer.NULL ? "" : this.cf.CFStringGetCStringPtr(desc, 0);
    }

    private class EventCallbackImpl
    implements EventCallback {
        private EventCallbackImpl() {
        }

        @Override
        public void invoke(Pointer streamRef, Pointer clientCallBackInfo, NativeLong numEvents, Pointer eventPaths, Pointer eventFlags, Pointer eventIds) {
            int[] flags;
            long st = System.currentTimeMillis();
            int length = numEvents.intValue();
            Pointer[] pointers = eventPaths.getPointerArray(0, length);
            if (eventFlags == null) {
                flags = new int[length];
                LOG.log(DEBUG_LOG_LEVEL, "FSEventStreamCallback eventFlags == null, expected int[] of size {0}", length);
            } else {
                flags = eventFlags.getIntArray(0, length);
            }
            for (int i = 0; i < length; ++i) {
                Pointer p = pointers[i];
                int flag = flags[i];
                String path = p.getString(0);
                if ((flag & 1) == 1 || (flag & 64) == 64 || (flag & 128) == 128) {
                    OSXNotifier.this.events.add("ALL-CHANGE");
                } else {
                    OSXNotifier.this.events.add(path);
                }
                LOG.log(DEBUG_LOG_LEVEL, "Event on {0}", new Object[]{path});
            }
            LOG.log(PERF_LOG_LEVEL, "Callback time: {0}", System.currentTimeMillis() - st);
        }
    }

    private static class DaemonThreadFactory
    implements ThreadFactory {
        private DaemonThreadFactory() {
        }

        @Override
        public Thread newThread(Runnable r) {
            Thread t = new Thread(r);
            t.setDaemon(true);
            return t;
        }
    }

    public static interface CoreServices
    extends Library {
        public Pointer FSEventStreamCreate(Pointer var1, EventCallback var2, Pointer var3, Pointer var4, long var5, double var7, int var9);

        public Pointer FSEventStreamCopyDescription(Pointer var1);

        public void FSEventStreamScheduleWithRunLoop(Pointer var1, Pointer var2, Pointer var3);

        public void FSEventStreamUnscheduleFromRunLoop(Pointer var1, Pointer var2, Pointer var3);

        public void FSEventStreamStart(Pointer var1);

        public void FSEventStreamStop(Pointer var1);

        public void FSEventStreamInvalidate(Pointer var1);

        public void FSEventStreamRelease(Pointer var1);
    }

    public static interface CoreFoundation
    extends Library {
        public Pointer CFRunLoopGetCurrent();

        public void CFRunLoopRun();

        public void CFRunLoopStop(Pointer var1);

        public Pointer CFRunLoopCopyAllModes(Pointer var1);

        public Pointer CFArrayCreateMutable(Pointer var1, NativeLong var2, Pointer var3);

        public void CFArrayAppendValue(Pointer var1, Pointer var2);

        public Pointer CFArrayGetValueAtIndex(Pointer var1, NativeLong var2);

        public NativeLong CFArrayGetCount(Pointer var1);

        public Pointer CFStringCreateWithCString(Pointer var1, String var2, int var3);

        public String CFStringGetCStringPtr(Pointer var1, int var2);
    }

    public static interface EventCallback
    extends Callback {
        public void invoke(Pointer var1, Pointer var2, NativeLong var3, Pointer var4, Pointer var5, Pointer var6);
    }

}

