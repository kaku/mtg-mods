/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package org.netbeans.swing.popupswitcher;

import java.awt.Rectangle;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import org.netbeans.swing.popupswitcher.SwitcherTableItem;
import org.openide.util.Utilities;

class SwitcherTableModel
extends AbstractTableModel {
    private TableModelEvent event;
    private int rows;
    private int cols;
    private SwitcherTableItem[] items;

    SwitcherTableModel(SwitcherTableItem[] items, int rowHeight) {
        this(items, rowHeight, Utilities.getUsableScreenBounds().height);
    }

    SwitcherTableModel(SwitcherTableItem[] items, int rowHeight, int tableHeight) {
        this.items = items;
        this.computeRowsAndCols(rowHeight, tableHeight);
    }

    private void computeRowsAndCols(int rowHeight, int tableHeight) {
        int nOfItems = this.items.length;
        int maxRowsPerCol = 0;
        if (rowHeight > 0) {
            maxRowsPerCol = tableHeight / rowHeight;
        }
        if (nOfItems > 0 && maxRowsPerCol > 0) {
            int nOfColumns = nOfItems / maxRowsPerCol;
            if (nOfItems % maxRowsPerCol > 0) {
                ++nOfColumns;
            }
            nOfColumns = Math.max(nOfColumns, 1);
            int nOfRows = nOfItems / nOfColumns;
            if (nOfItems % nOfColumns > 0) {
                ++nOfRows;
            }
            this.setRowsAndColumns(nOfRows, nOfColumns);
        } else {
            this.setRowsAndColumns(0, 0);
        }
    }

    private void setRowsAndColumns(int rows, int cols) {
        if (this.rows != rows || this.cols != cols) {
            this.rows = rows;
            this.cols = cols;
            if (this.event == null) {
                this.event = new TableModelEvent(this);
            }
            this.fireTableChanged(this.event);
        }
    }

    public Class getColumnClass(int columnIndex) {
        return SwitcherTableItem.class;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return "";
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (rowIndex == -1 || columnIndex == -1) {
            return null;
        }
        int docIdx = columnIndex * this.getRowCount() + rowIndex;
        return docIdx < this.items.length ? this.items[docIdx] : null;
    }

    @Override
    public int getRowCount() {
        return this.rows;
    }

    @Override
    public int getColumnCount() {
        return this.cols;
    }
}

