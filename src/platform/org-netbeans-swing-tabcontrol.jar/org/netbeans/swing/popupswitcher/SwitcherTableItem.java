/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.popupswitcher;

import javax.swing.Icon;

public class SwitcherTableItem
implements Comparable {
    private String name;
    private String htmlName;
    private String description;
    private Icon icon;
    private boolean active;
    private Activatable activatable;

    public SwitcherTableItem(Activatable activatable, String name) {
        this(activatable, name, null);
    }

    public SwitcherTableItem(Activatable activatable, String name, Icon icon) {
        this(activatable, name, name, icon, false);
    }

    public SwitcherTableItem(Activatable activatable, String name, String htmlName, Icon icon, boolean active) {
        this(activatable, name, htmlName, icon, active, null);
    }

    public SwitcherTableItem(Activatable activatable, String name, String htmlName, Icon icon, boolean active, String description) {
        this.activatable = activatable;
        this.name = name;
        this.htmlName = htmlName;
        this.icon = icon;
        this.active = active;
        this.description = description;
    }

    public void activate() {
        this.activatable.activate();
    }

    public String getName() {
        return this.name;
    }

    public String getHtmlName() {
        return this.htmlName;
    }

    public String getDescription() {
        return this.description;
    }

    public Icon getIcon() {
        return this.icon;
    }

    public Activatable getActivatable() {
        return this.activatable;
    }

    public boolean isActive() {
        return this.active;
    }

    public String toString() {
        return super.toString() + "[name=" + this.name + ", icon=" + this.icon + "]";
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o instanceof SwitcherTableItem) {
            SwitcherTableItem item = (SwitcherTableItem)o;
            boolean result = item.getName().equals(this.name) && item.getActivatable().equals(this.activatable);
            return result;
        }
        return false;
    }

    public int hashCode() {
        return (this.name == null ? 1 : this.name.hashCode()) * this.activatable.hashCode();
    }

    public int compareTo(Object o) {
        String name1 = this.getName();
        String name2 = null;
        if (o instanceof SwitcherTableItem) {
            name2 = ((SwitcherTableItem)o).getName();
        }
        if (name2 == null) {
            return name1 == null ? 0 : -1;
        }
        return name1 == null ? 1 : name1.compareToIgnoreCase(name2);
    }

    public static interface Activatable {
        public void activate();
    }

}

