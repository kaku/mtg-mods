/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 *  org.openide.awt.HtmlRenderer$Renderer
 *  org.openide.util.Utilities
 */
package org.netbeans.swing.popupswitcher;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.lang.ref.SoftReference;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.netbeans.swing.popupswitcher.SwitcherTableItem;
import org.netbeans.swing.popupswitcher.SwitcherTableModel;
import org.openide.awt.HtmlRenderer;
import org.openide.util.Utilities;

public class SwitcherTable
extends JTable {
    private static final Border rendererBorder = BorderFactory.createEmptyBorder(2, 5, 0, 5);
    private Icon nullIcon = new NullIcon();
    private Color foreground;
    private Color background;
    private Color selForeground;
    private Color selBackground;
    private Dimension prefSize;
    private boolean needCalcRowHeight = true;
    private final boolean showIcons;
    private static final boolean TABNAMES_HTML = Boolean.parseBoolean(System.getProperty("nb.tabnames.html", "true"));
    private static SoftReference<BufferedImage> ctx = null;

    public SwitcherTable(SwitcherTableItem[] items) {
        this(items, 0);
    }

    public SwitcherTable(SwitcherTableItem[] items, int y) {
        this.init();
        int gap = y == 0 ? 10 : 5;
        int height = Utilities.getUsableScreenBounds().height - y - gap;
        this.setModel(new SwitcherTableModel(items, this.getRowHeight(), height));
        this.getSelectionModel().clearSelection();
        this.getSelectionModel().setAnchorSelectionIndex(-1);
        this.getSelectionModel().setLeadSelectionIndex(-1);
        this.setAutoscrolls(false);
        boolean hasIcons = false;
        for (SwitcherTableItem i : items) {
            if (i.getIcon() == null || i.getIcon().getIconWidth() <= 0) continue;
            hasIcons = true;
            break;
        }
        this.showIcons = hasIcons;
    }

    private void init() {
        Border b = UIManager.getBorder("nb.popupswitcher.border");
        if (null == b) {
            b = BorderFactory.createLineBorder(this.getForeground());
        }
        this.setBorder(b);
        this.setShowHorizontalLines(false);
        this.calcRowHeight(SwitcherTable.getOffscreenGraphics());
    }

    public void setSwitcherItems(SwitcherTableItem[] newItems, int y) {
        int gap = y == 0 ? 10 : 5;
        int height = Utilities.getUsableScreenBounds().height - y - gap;
        this.setModel(new SwitcherTableModel(newItems, this.getRowHeight(), height));
        this.prefSize = null;
    }

    @Override
    public void updateUI() {
        this.needCalcRowHeight = true;
        super.updateUI();
    }

    @Override
    public void setFont(Font f) {
        this.needCalcRowHeight = true;
        super.setFont(f);
    }

    @Override
    public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        SwitcherTableItem item = (SwitcherTableItem)this.getSwitcherTableModel().getValueAt(row, column);
        boolean selected = row == this.getSelectedRow() && column == this.getSelectedColumn() && item != null;
        Component ren = renderer.getTableCellRendererComponent(this, item, selected, selected, row, column);
        JLabel lbl = null;
        if (ren instanceof JLabel) {
            JLabel prototype = (JLabel)ren;
            lbl = HtmlRenderer.createLabel();
            if (lbl instanceof HtmlRenderer.Renderer) {
                ((HtmlRenderer.Renderer)lbl).setRenderStyle(1);
            }
            lbl.setForeground(prototype.getForeground());
            lbl.setBackground(prototype.getBackground());
            lbl.setFont(prototype.getFont());
            ren = lbl;
        }
        if (item == null) {
            if (null != lbl) {
                lbl.setOpaque(false);
                lbl.setIcon(null);
            }
            return ren;
        }
        Icon icon = item.getIcon();
        if (icon == null || icon.getIconWidth() == 0) {
            icon = this.nullIcon;
        }
        boolean active = item.isActive();
        if (null != lbl) {
            lbl.setText(selected || active && !TABNAMES_HTML ? this.stripHtml(item.getHtmlName()) : item.getHtmlName());
            lbl.setBorder(rendererBorder);
            if (this.showIcons) {
                lbl.setIcon(icon);
                lbl.setIconTextGap(26 - icon.getIconWidth());
            }
        }
        if (active) {
            if (TABNAMES_HTML) {
                if (null != lbl) {
                    lbl.setText(lbl.getText() + " \u2190");
                }
            } else if (Utilities.isWindows()) {
                ren.setFont(this.getFont().deriveFont(1, this.getFont().getSize()));
            } else {
                ren.setFont(new Font(this.getFont().getName(), 1, this.getFont().getSize()));
            }
        }
        if (null != lbl) {
            lbl.setOpaque(true);
        }
        return ren;
    }

    private String stripHtml(String htmlText) {
        if (null == htmlText) {
            return null;
        }
        String res = htmlText.replaceAll("<[^>]*>", "");
        res = res.replaceAll("&nbsp;", " ");
        res = res.trim();
        return res;
    }

    @Override
    public Color getForeground() {
        if (this.foreground == null) {
            this.foreground = UIManager.getColor("nb.popupswitcher.foreground");
            if (this.foreground == null) {
                this.foreground = UIManager.getColor("ComboBox.foreground");
            }
        }
        return this.foreground != null ? this.foreground : super.getForeground();
    }

    @Override
    public Color getBackground() {
        if (this.background == null) {
            this.background = UIManager.getColor("nb.popupswitcher.background");
            if (this.background == null) {
                this.background = UIManager.getColor("ComboBox.background");
            }
            if (null != this.background) {
                this.background = new Color(this.background.getRGB());
            }
        }
        return this.background != null ? this.background : super.getBackground();
    }

    @Override
    public Color getSelectionForeground() {
        if (this.selForeground == null) {
            this.selForeground = UIManager.getColor("nb.popupswitcher.selectionForeground");
            if (this.selForeground == null) {
                this.selForeground = UIManager.getColor("ComboBox.selectionForeground");
            }
        }
        return this.selForeground != null ? this.selForeground : super.getSelectionForeground();
    }

    @Override
    public Color getSelectionBackground() {
        if (this.selBackground == null) {
            this.selBackground = UIManager.getColor("nb.popupswitcher.selectionBackground");
            if (this.selBackground == null) {
                this.selBackground = UIManager.getColor("ComboBox.selectionBackground");
            }
        }
        return this.selBackground != null ? this.selBackground : super.getSelectionBackground();
    }

    private void calcRowHeight(Graphics g) {
        Font f = this.getFont();
        FontMetrics fm = g.getFontMetrics(f);
        int rowHeight = Math.max(fm.getHeight(), 16) + 4;
        this.needCalcRowHeight = false;
        this.setRowHeight(rowHeight);
    }

    private static Graphics2D getOffscreenGraphics() {
        BufferedImage result = null;
        if (ctx != null) {
            result = ctx.get();
        }
        if (result == null) {
            result = new BufferedImage(10, 10, 1);
            ctx = new SoftReference<BufferedImage>(result);
        }
        return (Graphics2D)result.getGraphics();
    }

    @Override
    public Dimension getPreferredSize() {
        if (this.prefSize == null) {
            int i;
            int cols = this.getColumnCount();
            int rows = this.getRowCount();
            int columnWidth = 0;
            for (i = 0; i < cols; ++i) {
                for (int j = 0; j < rows; ++j) {
                    TableCellRenderer ren = this.getCellRenderer(j, i);
                    Component c = this.prepareRenderer(ren, j, i);
                    columnWidth = Math.max(c.getPreferredSize().width + 1, columnWidth);
                }
            }
            columnWidth = Math.min(columnWidth, 250);
            for (i = 0; i < cols; ++i) {
                this.getColumnModel().getColumn(i).setPreferredWidth(columnWidth);
            }
            this.prefSize = new Dimension(columnWidth * cols, rows * this.getRowHeight());
        }
        return this.prefSize;
    }

    private SwitcherTableModel getSwitcherTableModel() {
        return (SwitcherTableModel)this.getModel();
    }

    public SwitcherTableItem getSelectedItem() {
        return (SwitcherTableItem)this.getValueAt(this.getSelectedRow(), this.getSelectedColumn());
    }

    @Override
    public void paint(Graphics g) {
        if (this.needCalcRowHeight) {
            this.calcRowHeight(g);
        }
        super.paint(g);
    }

    @Override
    public void changeSelection(int rowIndex, int columnIndex, boolean toggle, boolean extend) {
        super.changeSelection(rowIndex, columnIndex, false, false);
    }

    public int getLastValidRow() {
        int lastColIdx = this.getColumnCount() - 1;
        for (int i = this.getRowCount() - 1; i >= 0; --i) {
            if (this.getValueAt(i, lastColIdx) == null) continue;
            return i;
        }
        return -1;
    }

    private static class NullIcon
    implements Icon {
        private NullIcon() {
        }

        @Override
        public int getIconWidth() {
            return 16;
        }

        @Override
        public int getIconHeight() {
            return 16;
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
        }
    }

}

