/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol;

import org.netbeans.swing.tabcontrol.DefaultTabDataModel;
import org.netbeans.swing.tabcontrol.TabDataModel;

public interface SlideBarDataModel
extends TabDataModel {
    public static final int EAST = 1;
    public static final int WEST = 2;
    public static final int SOUTH = 3;
    public static final int NORTH = 4;

    public int getOrientation();

    public void setOrientation(int var1);

    public static class Impl
    extends DefaultTabDataModel
    implements SlideBarDataModel {
        private int orientation = 1;

        @Override
        public int getOrientation() {
            return this.orientation;
        }

        @Override
        public void setOrientation(int orientation) {
            this.orientation = orientation;
        }
    }

}

