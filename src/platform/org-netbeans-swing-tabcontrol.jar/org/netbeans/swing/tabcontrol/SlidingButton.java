/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol;

import java.awt.Color;
import java.awt.Component;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JToggleButton;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ButtonUI;
import org.netbeans.swing.tabcontrol.SlidingButtonUI;
import org.netbeans.swing.tabcontrol.TabData;

public final class SlidingButton
extends JToggleButton {
    public static final String UI_CLASS_ID = "SlidingButtonUI";
    private static final boolean isAqua = "Aqua".equals(UIManager.getLookAndFeel().getID());
    private int orientation;
    private TabData data;
    private Timer blinkTimer = null;
    private boolean blinkState = false;

    public SlidingButton(TabData buttonData, int orientation) {
        Object slidingName;
        super(buttonData.getText(), buttonData.getIcon(), false);
        Component comp = buttonData.getComponent();
        if (comp instanceof JComponent && (slidingName = ((JComponent)comp).getClientProperty("SlidingName")) instanceof String) {
            this.setText((String)slidingName);
        }
        this.orientation = orientation;
        this.data = buttonData;
        this.setFocusable(false);
        this.setRolloverEnabled(true);
        this.setIconTextGap(4);
        this.setVerticalAlignment(0);
        this.setHorizontalAlignment(0);
        if ("Nimbus".equals(UIManager.getLookAndFeel().getID())) {
            Insets insets = UIManager.getInsets("Button.contentMargins");
            if (insets != null) {
                this.setBorder(BorderFactory.createEmptyBorder(insets.top, insets.left, insets.bottom, insets.right));
            } else {
                this.setBorder(BorderFactory.createEmptyBorder(6, 14, 6, 14));
            }
        } else if (isAqua) {
            this.setBorder(BorderFactory.createEmptyBorder(4, 10, 4, 10));
            this.putClientProperty("JComponent.sizeVariant", "small");
            this.setOpaque(false);
        } else {
            this.setMargin(new Insets(0, 3, 0, 3));
        }
        this.setBorderPainted(false);
    }

    @Override
    public void addNotify() {
        super.addNotify();
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.setBlinking(false);
    }

    @Override
    public String getToolTipText() {
        return this.data.getTooltip();
    }

    @Override
    public void updateUI() {
        ButtonUI ui = (ButtonUI)UIManager.getUI(this);
        if (ui == null) {
            ui = (ButtonUI)SlidingButtonUI.createUI(this);
        }
        this.setUI(ui);
    }

    @Override
    public String getUIClassID() {
        return "SlidingButtonUI";
    }

    public int getOrientation() {
        return this.orientation;
    }

    public boolean isBlinking() {
        return this.blinkState;
    }

    public void setBlinking(boolean val) {
        if (!val && this.blinkTimer != null) {
            this.blinkTimer.stop();
            this.blinkTimer = null;
            boolean wasBlinkState = this.blinkState;
            this.blinkState = false;
            if (wasBlinkState) {
                this.repaint();
            }
        } else if (val && this.blinkTimer == null) {
            this.blinkTimer = new Timer(700, new BlinkListener());
            this.blinkState = true;
            this.blinkTimer.start();
            this.repaint();
        }
    }

    public final boolean isBlinkState() {
        return this.blinkState;
    }

    @Override
    public final Color getBackground() {
        return this.isBlinkState() ? new Color(252, 250, 244) : super.getBackground();
    }

    public TabData getData() {
        return this.data;
    }

    private class BlinkListener
    implements ActionListener {
        private BlinkListener() {
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            SlidingButton.this.blinkState = !SlidingButton.this.blinkState;
            SlidingButton.this.repaint();
        }
    }

}

