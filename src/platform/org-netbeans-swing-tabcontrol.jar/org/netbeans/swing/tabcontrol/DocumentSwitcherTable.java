/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.CloseButtonFactory
 *  org.openide.windows.TopComponent
 */
package org.netbeans.swing.tabcontrol;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import javax.swing.ButtonModel;
import javax.swing.DefaultButtonModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import org.netbeans.swing.popupswitcher.SwitcherTable;
import org.netbeans.swing.popupswitcher.SwitcherTableItem;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.event.TabActionEvent;
import org.openide.awt.CloseButtonFactory;
import org.openide.windows.TopComponent;

class DocumentSwitcherTable
extends SwitcherTable {
    private final JButton btnClose;
    private final TabDisplayer displayer;
    private static final boolean SHOW_CLOSE_BUTTON = !Boolean.getBoolean("nb.tabs.suppressCloseButton");
    private int lastRow = -1;
    private int lastCol = -1;
    private boolean inCloseButtonRect = false;

    public DocumentSwitcherTable(TabDisplayer displayer, SwitcherTableItem[] items, int y) {
        super(items, y);
        this.displayer = displayer;
        this.btnClose = this.createCloseButton();
        ToolTipManager.sharedInstance().registerComponent(this);
    }

    @Override
    public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        SwitcherTableItem item = (SwitcherTableItem)this.getModel().getValueAt(row, column);
        boolean selected = row == this.getSelectedRow() && column == this.getSelectedColumn() && item != null;
        Component renComponent = super.prepareRenderer(renderer, row, column);
        if (selected && this.isClosable((Item)item)) {
            String text;
            if (renComponent instanceof JLabel && (text = ((JLabel)renComponent).getText()).endsWith(" \u2190")) {
                text = text.substring(0, text.length() - 2);
                ((JLabel)renComponent).setText(text);
            }
            JPanel res = new JPanel(new BorderLayout(0, 0));
            res.add(renComponent, "Center");
            res.add((Component)this.btnClose, "East");
            res.setBackground(renComponent.getBackground());
            return res;
        }
        return renComponent;
    }

    boolean onMouseEvent(MouseEvent e) {
        Point p = e.getPoint();
        p = SwingUtilities.convertPoint((Component)e.getSource(), p, this);
        int selRow = this.getSelectedRow();
        int selCol = this.getSelectedColumn();
        if (selRow < 0 || selCol < 0) {
            return false;
        }
        Rectangle rect = this.getCellRect(selRow, selCol, false);
        if (rect.contains(p)) {
            Dimension size = this.btnClose.getPreferredSize();
            int x = rect.x + rect.width - size.width;
            int y = rect.y + (rect.height - size.height) / 2;
            Rectangle btnRect = new Rectangle(x, y, size.width, size.height);
            boolean inButton = btnRect.contains(p);
            boolean mustRepaint = this.inCloseButtonRect != inButton;
            this.inCloseButtonRect = inButton;
            if (inButton && e.getID() == 501) {
                Item item = (Item)this.getModel().getValueAt(selRow, selCol);
                TabData tab = item.getTabData();
                int tabIndex = this.displayer.getModel().indexOf(tab);
                if (tabIndex >= 0) {
                    TabActionEvent evt = new TabActionEvent(this.displayer, "close", tabIndex);
                    this.displayer.postActionEvent(evt);
                    return true;
                }
            }
            if (mustRepaint && this.lastRow == selRow && this.lastCol == selCol) {
                this.repaint(btnRect);
            }
            this.lastCol = selCol;
            this.lastRow = selRow;
            return inButton;
        }
        return false;
    }

    @Override
    public String getToolTipText(MouseEvent event) {
        int row = this.rowAtPoint(event.getPoint());
        int col = this.columnAtPoint(event.getPoint());
        if (row >= 0 && col <= 0) {
            SwitcherTableItem item = (SwitcherTableItem)this.getModel().getValueAt(row, col);
            return item.getDescription();
        }
        return null;
    }

    private JButton createCloseButton() {
        Icon rolloverIcon;
        JButton res = CloseButtonFactory.createCloseButton();
        res.setModel(new DefaultButtonModel(){

            @Override
            public boolean isRollover() {
                return DocumentSwitcherTable.this.inCloseButtonRect;
            }
        });
        Icon defaultIcon = UIManager.getIcon("nb.popupswitcher.closebutton.defaultIcon");
        if (null != defaultIcon) {
            res.setIcon(defaultIcon);
        }
        if (null != (rolloverIcon = UIManager.getIcon("nb.popupswitcher.closebutton.rolloverIcon"))) {
            res.setRolloverIcon(rolloverIcon);
        }
        return res;
    }

    boolean isClosable(Item item) {
        if (!SHOW_CLOSE_BUTTON) {
            return false;
        }
        WinsysInfoForTabbedContainer winsysInfo = this.displayer.getContainerWinsysInfo();
        if (null != winsysInfo) {
            if (!winsysInfo.isTopComponentClosingEnabled()) {
                return false;
            }
            TabData tab = item.getTabData();
            Component tc = tab.getComponent();
            if (tc instanceof TopComponent) {
                return winsysInfo.isTopComponentClosingEnabled((TopComponent)tc);
            }
        }
        return true;
    }

    static class Item
    extends SwitcherTableItem {
        private final TabData tabData;

        public Item(SwitcherTableItem.Activatable activatable, String name, String htmlName, TabData tab, boolean active) {
            super(activatable, name, htmlName, tab.getIcon(), active, tab.getTooltip());
            this.tabData = tab;
        }

        public TabData getTabData() {
            return this.tabData;
        }
    }

}

