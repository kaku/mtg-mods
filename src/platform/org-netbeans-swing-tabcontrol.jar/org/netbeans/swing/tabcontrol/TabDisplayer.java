/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.swing.tabcontrol;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.dnd.Autoscroll;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleRole;
import javax.accessibility.AccessibleSelection;
import javax.swing.JComponent;
import javax.swing.SingleSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.ComponentConverter;
import org.netbeans.swing.tabcontrol.DefaultTabDataModel;
import org.netbeans.swing.tabcontrol.LocationInformer;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayerUI;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbed;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.event.TabActionEvent;
import org.netbeans.swing.tabcontrol.plaf.AbstractTabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.AquaEditorTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.AquaViewTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.BasicSlidingTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.BasicTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.TabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.ToolbarTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.WinClassicEditorTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.WinClassicViewTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.WinXPEditorTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.WinXPViewTabDisplayerUI;
import org.openide.util.NbBundle;

public final class TabDisplayer
extends JComponent
implements Accessible,
Autoscroll {
    private boolean initialized = false;
    private TabDataModel model;
    private SingleSelectionModel sel = null;
    private boolean active;
    private final int type;
    public static final int TYPE_VIEW = 0;
    public static final int TYPE_EDITOR = 1;
    public static final int TYPE_SLIDING = 2;
    public static final int TYPE_TOOLBAR = 3;
    public static final String PROP_ACTIVE = "active";
    public static final String COMMAND_CLOSE = "close";
    public static final String COMMAND_SELECT = "select";
    public static final String COMMAND_POPUP_REQUEST = "popup";
    public static final String COMMAND_MAXIMIZE = "maximize";
    public static final String COMMAND_CLOSE_ALL = "closeAll";
    public static final String COMMAND_CLOSE_ALL_BUT_THIS = "closeAllButThis";
    public static final String COMMAND_ENABLE_AUTO_HIDE = "enableAutoHide";
    public static final String COMMAND_MINIMIZE_GROUP = "minimizeGroup";
    public static final String COMMAND_RESTORE_GROUP = "restoreGroup";
    public static final String COMMAND_CLOSE_GROUP = "closeGroup";
    public static final String COMMAND_DISABLE_AUTO_HIDE = "disableAutoHide";
    public static final String EDITOR_TAB_DISPLAYER_UI_CLASS_ID = "EditorTabDisplayerUI";
    public static final String VIEW_TAB_DISPLAYER_UI_CLASS_ID = "ViewTabDisplayerUI";
    public static final String SLIDING_TAB_DISPLAYER_UI_CLASS_ID = "SlidingTabDisplayerUI";
    public static final String TOOLBAR_TAB_DISPLAYER_UI_CLASS_ID = "ToolbarTabDisplayerUI";
    public static final String PROP_ORIENTATION = "orientation";
    public static final Object ORIENTATION_EAST = "east";
    public static final Object ORIENTATION_WEST = "west";
    public static final Object ORIENTATION_NORTH = "north";
    public static final Object ORIENTATION_SOUTH = "south";
    public static final Object ORIENTATION_CENTER = "center";
    public static final Object ORIENTATION_INVISIBLE = "invisible";
    private transient List<ActionListener> actionListenerList;
    private WinsysInfoForTabbed winsysInfo = null;
    private WinsysInfoForTabbedContainer containerWinsysInfo = null;
    @Deprecated
    private LocationInformer locationInformer = null;
    private boolean showClose = !Boolean.getBoolean("nb.tabs.suppressCloseButton");
    private ComponentConverter componentConverter = ComponentConverter.DEFAULT;

    public TabDisplayer() {
        this(new DefaultTabDataModel(), 0);
    }

    public TabDisplayer(TabDataModel model, int type) {
        this(model, type, (WinsysInfoForTabbed)null);
    }

    @Deprecated
    public TabDisplayer(TabDataModel model, int type, LocationInformer locationInformer) {
        this(model, type, (WinsysInfoForTabbed)null);
        this.locationInformer = locationInformer;
    }

    @Deprecated
    public TabDisplayer(TabDataModel model, int type, WinsysInfoForTabbed winsysInfo) {
        this(model, type, WinsysInfoForTabbedContainer.getDefault(winsysInfo));
    }

    public TabDisplayer(TabDataModel model, int type, WinsysInfoForTabbedContainer containerWinsysInfo) {
        switch (type) {
            case 0: 
            case 1: 
            case 2: 
            case 3: {
                break;
            }
            default: {
                throw new IllegalArgumentException("Unknown UI type: " + type);
            }
        }
        this.model = model;
        this.type = type;
        this.winsysInfo = containerWinsysInfo;
        this.containerWinsysInfo = containerWinsysInfo;
        this.showClose &= containerWinsysInfo.isTopComponentClosingEnabled();
        this.putClientProperty("orientation", ORIENTATION_NORTH);
        this.initialized = true;
        this.updateUI();
        this.setFocusable(false);
    }

    public final TabDisplayerUI getUI() {
        return (TabDisplayerUI)this.ui;
    }

    @Override
    public final void updateUI() {
        if (!this.initialized) {
            return;
        }
        if (this.type == 3) {
            this.setUI(new ToolbarTabDisplayerUI(this));
            return;
        }
        if (this.type == 2) {
            this.setUI(new BasicSlidingTabDisplayerUI(this));
            return;
        }
        ComponentUI ui = null;
        if (UIManager.get(this.getUIClassID()) != null) {
            try {
                ui = UIManager.getUI(this);
            }
            catch (Error error) {
                System.err.println("Could not load a UI for " + this.getUIClassID() + " - missing class?");
            }
        } else {
            ui = this.findUIStandalone();
        }
        if (ui == null) {
            ui = this.getType() == 0 ? WinClassicViewTabDisplayerUI.createUI(this) : WinClassicEditorTabDisplayerUI.createUI(this);
        }
        this.setUI((TabDisplayerUI)ui);
    }

    private ComponentUI findUIStandalone() {
        ComponentUI result = null;
        String lf = UIManager.getLookAndFeel().getID();
        switch (this.type) {
            case 0: {
                if ("Aqua".equals(lf)) {
                    result = AquaViewTabDisplayerUI.createUI(this);
                    break;
                }
                if (!"Windows".equals(lf)) break;
                result = TabDisplayer.isXPLF() ? WinXPViewTabDisplayerUI.createUI(this) : WinClassicViewTabDisplayerUI.createUI(this);
                break;
            }
            case 1: {
                if ("Aqua".equals(lf)) {
                    result = AquaEditorTabDisplayerUI.createUI(this);
                    break;
                }
                if (!"Windows".equals(lf)) break;
                result = TabDisplayer.isXPLF() ? WinXPEditorTabDisplayerUI.createUI(this) : WinClassicEditorTabDisplayerUI.createUI(this);
            }
        }
        return result;
    }

    private static boolean isXPLF() {
        Boolean isXP = (Boolean)Toolkit.getDefaultToolkit().getDesktopProperty("win.xpstyle.themeActive");
        return isXP == null ? false : isXP;
    }

    @Override
    public String getUIClassID() {
        switch (this.getType()) {
            case 0: {
                return "ViewTabDisplayerUI";
            }
            case 1: {
                return "EditorTabDisplayerUI";
            }
            case 2: {
                return "SlidingTabDisplayerUI";
            }
            case 3: {
                return "ToolbarTabDisplayerUI";
            }
        }
        throw new IllegalArgumentException("Unknown UI type: " + this.getType());
    }

    public final int getType() {
        return this.type;
    }

    @Override
    public final Dimension getPreferredSize() {
        return this.getUI().getPreferredSize(this);
    }

    @Override
    public final Font getFont() {
        return this.getUI().getTxtFont();
    }

    @Override
    public final Dimension getMinimumSize() {
        Dimension d = this.getUI().getMinimumSize(this);
        d.width = 50;
        return d;
    }

    public final void requestAttention(int tab) {
        this.getUI().requestAttention(tab);
    }

    public final void cancelRequestAttention(int tab) {
        this.getUI().cancelRequestAttention(tab);
    }

    public final void setAttentionHighlight(int tab, boolean highlight) {
        this.getUI().setAttentionHighlight(tab, highlight);
    }

    public final boolean requestAttention(TabData data) {
        boolean result;
        int idx = this.getModel().indexOf(data);
        boolean bl = result = idx >= 0;
        if (result) {
            this.requestAttention(idx);
        }
        return result;
    }

    void setSelectionModel(SingleSelectionModel sel) {
        this.sel = sel;
    }

    public SingleSelectionModel getSelectionModel() {
        return this.sel;
    }

    public final TabDataModel getModel() {
        return this.model;
    }

    public final void setActive(boolean active) {
        if (active != this.active) {
            this.active = active;
            this.firePropertyChange("active", !active, active);
        }
    }

    public final boolean isActive() {
        return this.active;
    }

    @Override
    public final String getToolTipText(MouseEvent event) {
        if (this.ui != null) {
            int index;
            Point p = event.getPoint();
            if (event.getSource() != this) {
                Component c = (Component)event.getSource();
                p = SwingUtilities.convertPoint(c, p, this);
            }
            if ((index = this.getUI().tabForCoordinate(p)) != -1) {
                return this.getTooltipForTab(index);
            }
        }
        return super.getToolTipText(event);
    }

    private String getTooltipForTab(int tabIndex) {
        TabCellRenderer cellRenderer;
        BasicTabDisplayerUI basicUI;
        if (1 == this.getType() && this.getUI() instanceof BasicTabDisplayerUI && (cellRenderer = (basicUI = (BasicTabDisplayerUI)this.getUI()).getTabCellRenderer(tabIndex)) instanceof AbstractTabCellRenderer && (((AbstractTabCellRenderer)cellRenderer).getState() & 512) != 0) {
            return NbBundle.getMessage(TabDisplayer.class, (String)"TT_TabDisplayer_Close");
        }
        return this.getModel().getTab((int)tabIndex).tip;
    }

    public final void makeTabVisible(int index) {
        this.getUI().makeTabVisible(index);
    }

    public final Rectangle getTabRect(int tab, Rectangle dest) {
        if (dest == null) {
            dest = new Rectangle();
        }
        this.getUI().getTabRect(tab, dest);
        return dest;
    }

    @Deprecated
    public final Image getDragImage(int index) {
        return null;
    }

    public final synchronized void addActionListener(ActionListener listener) {
        if (this.actionListenerList == null) {
            this.actionListenerList = new ArrayList<ActionListener>();
        }
        this.actionListenerList.add(listener);
    }

    public final synchronized void removeActionListener(ActionListener listener) {
        if (this.actionListenerList != null) {
            this.actionListenerList.remove(listener);
        }
    }

    public void registerShortcuts(JComponent comp) {
        this.getUI().registerShortcuts(comp);
    }

    public void unregisterShortcuts(JComponent comp) {
        this.getUI().unregisterShortcuts(comp);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final void postActionEvent(TabActionEvent event) {
        List<ActionListener> list;
        TabDisplayer tabDisplayer = this;
        synchronized (tabDisplayer) {
            if (this.actionListenerList == null) {
                return;
            }
            list = Collections.unmodifiableList(this.actionListenerList);
        }
        for (int i = 0; i < list.size(); ++i) {
            list.get(i).actionPerformed(event);
        }
    }

    public int tabForCoordinate(Point p) {
        return this.getUI().tabForCoordinate(p);
    }

    @Deprecated
    public WinsysInfoForTabbed getWinsysInfo() {
        return this.winsysInfo;
    }

    public WinsysInfoForTabbedContainer getContainerWinsysInfo() {
        return this.containerWinsysInfo;
    }

    @Deprecated
    public LocationInformer getLocationInformer() {
        return this.locationInformer;
    }

    @Override
    public AccessibleContext getAccessibleContext() {
        if (this.accessibleContext == null) {
            this.accessibleContext = new AccessibleTabDisplayer();
        }
        return this.accessibleContext;
    }

    public final void setShowCloseButton(boolean val) {
        boolean wasShow = this.isShowCloseButton();
        if (wasShow != val) {
            this.showClose = val;
            if (this.isShowing()) {
                this.repaint();
            }
            this.firePropertyChange("showCloseButton", !val, val);
        }
    }

    public final boolean isShowCloseButton() {
        return this.showClose;
    }

    public void setComponentConverter(ComponentConverter converter) {
        this.componentConverter = converter;
    }

    public ComponentConverter getComponentConverter() {
        return this.componentConverter;
    }

    @Override
    public Insets getAutoscrollInsets() {
        return this.getUI().getAutoscrollInsets();
    }

    @Override
    public void autoscroll(Point cursorLocn) {
        this.getUI().autoscroll(cursorLocn);
    }

    protected class AccessibleTabDisplayer
    extends JComponent.AccessibleJComponent
    implements AccessibleSelection,
    ChangeListener {
        public AccessibleTabDisplayer() {
            super(TabDisplayer.this);
            TabDisplayer.this.getModel().addChangeListener(this);
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            Object o = e.getSource();
            this.firePropertyChange("AccessibleSelection", null, o);
        }

        @Override
        public AccessibleRole getAccessibleRole() {
            return AccessibleRole.PAGE_TAB_LIST;
        }

        @Override
        public int getAccessibleChildrenCount() {
            return TabDisplayer.this.getModel().size();
        }

        @Override
        public Accessible getAccessibleChild(int i) {
            if (i < 0 || i >= TabDisplayer.this.getModel().size()) {
                return null;
            }
            TabData data = TabDisplayer.this.getModel().getTab(i);
            if (data.getComponent() instanceof Accessible) {
                return (Accessible)((Object)data.getComponent());
            }
            return null;
        }

        @Override
        public AccessibleSelection getAccessibleSelection() {
            return this;
        }

        @Override
        public Accessible getAccessibleAt(Point p) {
            int tab = TabDisplayer.this.tabForCoordinate(p);
            if (tab == -1) {
                tab = TabDisplayer.this.getSelectionModel().getSelectedIndex();
            }
            return this.getAccessibleChild(tab);
        }

        @Override
        public int getAccessibleSelectionCount() {
            return 1;
        }

        @Override
        public Accessible getAccessibleSelection(int i) {
            int index = TabDisplayer.this.getSelectionModel().getSelectedIndex();
            return this.getAccessibleChild(index);
        }

        @Override
        public boolean isAccessibleChildSelected(int i) {
            return i == TabDisplayer.this.getSelectionModel().getSelectedIndex();
        }

        @Override
        public void addAccessibleSelection(int i) {
        }

        @Override
        public void removeAccessibleSelection(int i) {
        }

        @Override
        public void clearAccessibleSelection() {
        }

        @Override
        public void selectAllAccessibleSelection() {
        }
    }

}

