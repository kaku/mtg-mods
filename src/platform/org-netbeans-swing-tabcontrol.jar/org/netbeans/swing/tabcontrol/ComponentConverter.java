/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol;

import java.awt.Component;
import org.netbeans.swing.tabcontrol.TabData;

public interface ComponentConverter {
    public static final ComponentConverter DEFAULT = new ComponentConverter(){

        @Override
        public Component getComponent(TabData data) {
            return data.getComponent();
        }
    };

    public Component getComponent(TabData var1);

    public static final class Fixed
    implements ComponentConverter {
        private final Component component;

        public Fixed(Component component) {
            this.component = component;
        }

        @Override
        public Component getComponent(TabData data) {
            return this.component;
        }
    }

}

