/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.swing.tabcontrol;

import java.awt.Component;
import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.SingleSelectionModel;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.event.TabActionEvent;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.openide.windows.TopComponent;

public abstract class TabDisplayerUI
extends ComponentUI {
    protected SingleSelectionModel selectionModel = null;
    protected final TabDisplayer displayer;
    private static Map<Integer, String[]> buttonIconPaths;

    protected TabDisplayerUI(TabDisplayer displayer) {
        this.displayer = displayer;
    }

    @Override
    public void installUI(JComponent c) {
        assert (c == this.displayer);
        this.selectionModel = this.displayer.getSelectionModel();
        if (this.selectionModel == null) {
            this.selectionModel = this.createSelectionModel();
        }
        this.installSelectionModel();
    }

    @Override
    public void uninstallUI(JComponent c) {
        assert (c == this.displayer);
    }

    protected Font getTxtFont() {
        Font result = UIManager.getFont("TabbedPane.font");
        if (result != null) {
            return result;
        }
        result = UIManager.getFont("controlFont");
        return result;
    }

    public abstract Polygon getExactTabIndication(int var1);

    public abstract Polygon getInsertTabIndication(int var1);

    public abstract int tabForCoordinate(Point var1);

    public abstract Rectangle getTabRect(int var1, Rectangle var2);

    public Image createImageOfTab(int index) {
        return null;
    }

    protected abstract SingleSelectionModel createSelectionModel();

    protected final boolean shouldPerformAction(String command, int tab, MouseEvent event) {
        TabActionEvent evt = new TabActionEvent(this.displayer, command, tab, event);
        this.displayer.postActionEvent(evt);
        return !evt.isConsumed();
    }

    protected final boolean shouldPerformAction(TabActionEvent e) {
        TabActionEvent evt = new TabActionEvent(this.displayer, e.getActionCommand(), e.getTabIndex(), e.getMouseEvent());
        evt.setGroupName(e.getGroupName());
        this.displayer.postActionEvent(evt);
        return !evt.isConsumed();
    }

    public void makeTabVisible(int index) {
    }

    public final boolean isTabBusy(int tabIndex) {
        WinsysInfoForTabbedContainer winsysInfo = this.displayer.getContainerWinsysInfo();
        if (null == winsysInfo) {
            return false;
        }
        TabDataModel model = this.displayer.getModel();
        if (tabIndex < 0 || tabIndex >= model.size()) {
            return false;
        }
        TabData td = model.getTab(tabIndex);
        if (td.getComponent() instanceof TopComponent) {
            return winsysInfo.isTopComponentBusy((TopComponent)td.getComponent());
        }
        return false;
    }

    private void installSelectionModel() {
        this.displayer.setSelectionModel(this.selectionModel);
    }

    public abstract int dropIndexOfPoint(Point var1);

    public abstract void registerShortcuts(JComponent var1);

    public abstract void unregisterShortcuts(JComponent var1);

    protected abstract void requestAttention(int var1);

    protected abstract void cancelRequestAttention(int var1);

    protected void setAttentionHighlight(int tab, boolean highlight) {
    }

    public Icon getButtonIcon(int buttonId, int buttonState) {
        Icon res = null;
        TabDisplayerUI.initIcons();
        String[] paths = buttonIconPaths.get(buttonId);
        if (null != paths && buttonState >= 0 && buttonState < paths.length) {
            res = TabControlButtonFactory.getIcon(paths[buttonState]);
        }
        return res;
    }

    public void postTabAction(TabActionEvent e) {
        if (this.shouldPerformAction(e)) {
            // empty if block
        }
    }

    public Insets getAutoscrollInsets() {
        return new Insets(0, 0, 0, 0);
    }

    public void autoscroll(Point location) {
    }

    private static void initIcons() {
        if (null == buttonIconPaths) {
            String[] iconPaths;
            buttonIconPaths = new HashMap<Integer, String[]>(10);
            iconPaths = new String[]{"org/openide/awt/resources/metal_bigclose_enabled.png", "org/openide/awt/resources/metal_bigclose_pressed.png", iconPaths[0], "org/openide/awt/resources/metal_bigclose_rollover.png"};
            buttonIconPaths.put(1, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/metal_slideright_enabled.png", "org/netbeans/swing/tabcontrol/resources/metal_slideright_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/metal_slideright_rollover.png"};
            buttonIconPaths.put(6, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/metal_slideleft_enabled.png", "org/netbeans/swing/tabcontrol/resources/metal_slideleft_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/metal_slideleft_rollover.png"};
            buttonIconPaths.put(5, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/metal_slidebottom_enabled.png", "org/netbeans/swing/tabcontrol/resources/metal_slidebottom_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/metal_slidebottom_rollover.png"};
            buttonIconPaths.put(7, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/metal_pin_enabled.png", "org/netbeans/swing/tabcontrol/resources/metal_pin_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/metal_pin_rollover.png"};
            buttonIconPaths.put(2, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/metal_scrollleft_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/metal_scrollleft_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/metal_scrollleft_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/metal_scrollleft_pressed.png";
            buttonIconPaths.put(9, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/metal_scrollright_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/metal_scrollright_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/metal_scrollright_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/metal_scrollright_pressed.png";
            buttonIconPaths.put(10, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/metal_popup_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/metal_popup_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/metal_popup_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/metal_popup_pressed.png";
            buttonIconPaths.put(8, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/metal_maximize_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/metal_maximize_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/metal_maximize_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/metal_maximize_pressed.png";
            buttonIconPaths.put(3, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/metal_restore_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/metal_restore_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/metal_restore_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/metal_restore_pressed.png";
            buttonIconPaths.put(4, iconPaths);
        }
    }
}

