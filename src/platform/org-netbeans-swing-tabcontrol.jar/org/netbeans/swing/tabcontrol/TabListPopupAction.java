/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol;

import java.awt.Point;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.netbeans.swing.tabcontrol.ButtonPopupSwitcher;
import org.netbeans.swing.tabcontrol.TabDisplayer;

public class TabListPopupAction
extends AbstractAction {
    private TabDisplayer displayer;

    public TabListPopupAction(TabDisplayer displayer) {
        this.displayer = displayer;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if ("pressed".equals(ae.getActionCommand())) {
            JComponent jc = (JComponent)ae.getSource();
            Point p = new Point(jc.getWidth(), jc.getHeight());
            SwingUtilities.convertPointToScreen(p, jc);
            if (!ButtonPopupSwitcher.isShown()) {
                ButtonPopupSwitcher.showPopup(jc, this.displayer, p.x, p.y);
            } else {
                ButtonPopupSwitcher.hidePopup();
            }
            if (jc instanceof AbstractButton) {
                AbstractButton jb = (AbstractButton)jc;
                jb.getModel().setPressed(false);
                jb.getModel().setRollover(false);
                jb.getModel().setArmed(false);
                jb.repaint();
            }
        }
    }
}

