/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol;

import java.awt.Component;
import java.awt.Graphics;
import javax.swing.Icon;

public final class TabData
implements Comparable {
    Icon icon;
    String txt;
    String tip;
    Object userObject;
    static final Icon NO_ICON = new Icon(){

        @Override
        public int getIconHeight() {
            return 0;
        }

        @Override
        public int getIconWidth() {
            return 0;
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
        }

        public String toString() {
            return "empty icon";
        }
    };

    public TabData(Object userObject, Icon i, String caption, String tooltip) {
        this.userObject = userObject;
        this.icon = i;
        this.txt = caption;
        this.tip = tooltip;
    }

    public Object getUserObject() {
        return this.userObject;
    }

    public Component getComponent() {
        if (this.userObject instanceof Component) {
            return (Component)this.userObject;
        }
        return null;
    }

    public Icon getIcon() {
        if (this.icon == null) {
            return NO_ICON;
        }
        return this.icon;
    }

    public String getText() {
        return this.txt;
    }

    public String getTooltip() {
        return this.tip;
    }

    public String toString() {
        return this.txt;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o instanceof TabData) {
            TabData td = (TabData)o;
            boolean result = td.userObject.equals(this.userObject) && td.txt.equals(this.txt);
            return result;
        }
        return false;
    }

    public int hashCode() {
        return (this.txt == null ? 0 : this.txt.hashCode()) ^ (this.userObject == null ? 0 : this.userObject.hashCode());
    }

    public int compareTo(Object o) {
        String arg1 = this.getText();
        String arg2 = o instanceof TabData ? ((TabData)o).getText() : null;
        if (arg2 == null) {
            if (arg1 == null) {
                return 0;
            }
            return 1;
        }
        if (arg1 == null) {
            return -1;
        }
        return arg1.compareTo(arg2);
    }

}

