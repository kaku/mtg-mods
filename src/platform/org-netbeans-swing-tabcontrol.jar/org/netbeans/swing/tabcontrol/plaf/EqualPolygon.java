/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;

public final class EqualPolygon
extends Polygon {
    private static final Comparator<Point> comparator = new PointsComparator();

    public EqualPolygon() {
    }

    public EqualPolygon(int[] x, int[] y, int n) {
        this.xpoints = new int[n];
        this.ypoints = new int[n];
        System.arraycopy(x, 0, this.xpoints, 0, this.xpoints.length);
        System.arraycopy(y, 0, this.ypoints, 0, this.ypoints.length);
        this.npoints = n;
    }

    public EqualPolygon(Polygon p) {
        super(p.xpoints, p.ypoints, p.npoints);
    }

    public EqualPolygon(Rectangle r) {
        super(new int[]{r.x, r.x + r.width, r.x + r.width, r.x}, new int[]{r.y, r.y, r.y + r.height, r.y + r.height}, 4);
    }

    public EqualPolygon(int[] x, int[] y) {
        super(x, y, x.length);
    }

    public void moveTo(int x, int y) {
        this.addPoint(x, y);
    }

    public void lineTo(int x, int y) {
        this.addPoint(x, y);
    }

    public Object clone() {
        return new EqualPolygon(this.xpoints, this.ypoints, this.xpoints.length);
    }

    public String toString() {
        StringBuffer sb = new StringBuffer("EqualPolygon: ");
        for (int i = 0; i < this.npoints; ++i) {
            sb.append(' ');
            sb.append(this.xpoints[i]);
            sb.append(',');
            sb.append(this.ypoints[i]);
        }
        return sb.toString();
    }

    public int hashCode() {
        return this.arrayHashCode(this.xpoints) ^ this.arrayHashCode(this.ypoints);
    }

    private int arrayHashCode(int[] o) {
        int result = 0;
        for (int i = 0; i < this.npoints; ++i) {
            result += o[i] ^ i;
        }
        return result;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o instanceof Polygon) {
            Polygon p = (Polygon)o;
            int[] ox = p.xpoints;
            int[] oy = p.ypoints;
            boolean result = Arrays.equals(this.xpoints, ox) && Arrays.equals(this.ypoints, oy);
            return result &= p.npoints == this.npoints;
        }
        return false;
    }

    private Point[] sortPoints(Point[] p) {
        HashSet<Point> set = new HashSet<Point>(Arrays.asList(p));
        p = new Point[set.size()];
        p = set.toArray(p);
        Arrays.sort(p, comparator);
        return p;
    }

    private static class PointsComparator
    implements Comparator<Point> {
        private PointsComparator() {
        }

        @Override
        public int compare(Point a, Point b) {
            int result = a.y * (a.x - b.x) - b.y * (b.x - a.x);
            return result;
        }
    }

}

