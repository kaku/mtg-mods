/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.lang.ref.WeakReference;
import javax.swing.JComponent;

class StackLayout
implements LayoutManager {
    private WeakReference<Component> visibleComp = null;

    StackLayout() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void showComponent(Component c, Container parent) {
        Component comp = this.getVisibleComponent();
        if (comp != c) {
            if (!parent.isAncestorOf(c) && c != null) {
                parent.add(c);
            }
            Object object = parent.getTreeLock();
            synchronized (object) {
                if (comp != null) {
                    comp.setVisible(false);
                }
                this.visibleComp = new WeakReference<Component>(c);
                if (c != null) {
                    c.setVisible(true);
                }
                if (c instanceof JComponent) {
                    ((JComponent)c).revalidate();
                } else {
                    parent.validate();
                }
            }
        }
    }

    public Component getVisibleComponent() {
        return this.visibleComp == null ? null : this.visibleComp.get();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void addLayoutComponent(String name, Component comp) {
        Object object = comp.getTreeLock();
        synchronized (object) {
            comp.setVisible(false);
            if (comp == this.getVisibleComponent()) {
                this.visibleComp = null;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeLayoutComponent(Component comp) {
        Object object = comp.getTreeLock();
        synchronized (object) {
            if (comp == this.getVisibleComponent()) {
                this.visibleComp = null;
            }
            comp.setVisible(true);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void layoutContainer(Container parent) {
        Component visibleComp = this.getVisibleComponent();
        if (visibleComp != null) {
            Object object = parent.getTreeLock();
            synchronized (object) {
                Insets insets = parent.getInsets();
                visibleComp.setBounds(insets.left, insets.top, parent.getWidth() - (insets.left + insets.right), parent.getHeight() - (insets.top + insets.bottom));
            }
        }
    }

    @Override
    public Dimension minimumLayoutSize(Container parent) {
        Component c = this.getVisibleComponent();
        return c != null ? c.getMinimumSize() : StackLayout.getEmptySize();
    }

    @Override
    public Dimension preferredLayoutSize(Container parent) {
        Component c = this.getVisibleComponent();
        return c != null ? c.getPreferredSize() : StackLayout.getEmptySize();
    }

    private static Dimension getEmptySize() {
        return new Dimension(50, 50);
    }
}

