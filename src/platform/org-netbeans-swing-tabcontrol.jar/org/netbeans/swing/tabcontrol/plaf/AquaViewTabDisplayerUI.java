/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.plaf.AbstractViewTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.BusyTabsSupport;
import org.netbeans.swing.tabcontrol.plaf.ColorUtil;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.netbeans.swing.tabcontrol.plaf.TabLayoutModel;
import org.openide.awt.HtmlRenderer;

public final class AquaViewTabDisplayerUI
extends AbstractViewTabDisplayerUI {
    private static final int TXT_X_PAD = 5;
    private static final int ICON_X_PAD = 2;
    private static Map<Integer, String[]> buttonIconPaths;
    private Dimension prefSize = new Dimension(100, 19);

    private AquaViewTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
    }

    public static ComponentUI createUI(JComponent c) {
        return new AquaViewTabDisplayerUI((TabDisplayer)c);
    }

    @Override
    protected AbstractViewTabDisplayerUI.Controller createController() {
        return new OwnController();
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        FontMetrics fm = this.getTxtFontMetrics();
        int height = fm == null ? 21 : fm.getAscent() + 2 * fm.getDescent() + 3;
        Insets insets = c.getInsets();
        this.prefSize.height = ++height + insets.bottom + insets.top;
        return this.prefSize;
    }

    private boolean isMouseOver(int index) {
        return ((OwnController)this.getController()).getMouseIndex() == index && !this.isSelected(index);
    }

    @Override
    protected void paintTabContent(Graphics g, int index, String text, int x, int y, int width, int height) {
        FontMetrics fm = this.getTxtFontMetrics();
        g.setFont(this.getTxtFont());
        int textW = width;
        if (this.isSelected(index)) {
            Component buttons = this.getControlButtons();
            if (null != buttons) {
                Dimension buttonsSize = buttons.getPreferredSize();
                if (width < buttonsSize.width + 2) {
                    buttons.setVisible(false);
                } else {
                    buttons.setVisible(true);
                    textW = width - (buttonsSize.width + 2 + 10);
                    buttons.setLocation(x + textW + 10 - 2, y + (height - buttonsSize.height) / 2);
                }
            }
        } else {
            textW = width - 10;
        }
        if (text.length() == 0) {
            return;
        }
        int textHeight = fm.getHeight();
        int textX = x + 5;
        if (index == 0) {
            textX = x + 5;
        }
        int textY = textHeight > height ? -1 * ((textHeight - height) / 2) + fm.getAscent() - 1 : height / 2 - textHeight / 2 + fm.getAscent();
        boolean slidedOut = false;
        WinsysInfoForTabbedContainer winsysInfo = this.displayer.getContainerWinsysInfo();
        if (null != winsysInfo && winsysInfo.isSlidedOutContainer()) {
            slidedOut = false;
        }
        if (this.isTabBusy(index) && !slidedOut) {
            Icon busyIcon = BusyTabsSupport.getDefault().getBusyIcon(this.isSelected(index));
            textW -= busyIcon.getIconWidth() - 3 - 5;
            busyIcon.paintIcon(this.displayer, g, textX, y + (height - busyIcon.getIconHeight()) / 2);
            textX += busyIcon.getIconWidth() + 3;
        }
        int realTextWidth = (int)HtmlRenderer.renderString((String)text, (Graphics)g, (int)textX, (int)textY, (int)textW, (int)height, (Font)this.getTxtFont(), (Color)UIManager.getColor("textText"), (int)1, (boolean)false);
        if (textW > (realTextWidth = Math.min(realTextWidth, textW))) {
            textX += (textW - realTextWidth) / 2;
        }
        HtmlRenderer.renderString((String)text, (Graphics)g, (int)textX, (int)textY, (int)textW, (int)height, (Font)this.getTxtFont(), (Color)UIManager.getColor("textText"), (int)1, (boolean)true);
    }

    @Override
    protected void paintTabBorder(Graphics g, int index, int x, int y, int width, int height) {
        Color borderColor = UIManager.getColor("NbTabControl.borderColor");
        Color borderShadowColor = UIManager.getColor("NbTabControl.borderShadowColor");
        g.setColor(borderColor);
        if (index > 0) {
            g.drawLine(x, y, x, y + height);
            if (!this.isSelected(index)) {
                g.setColor(borderShadowColor);
                g.drawLine(x + 1, y + 1, x + 1, y + height - 1);
            }
        }
        if (index < this.getDataModel().size() - 1 || !this.isUseStretchingTabs()) {
            g.setColor(borderColor);
            g.drawLine(x + width, y, x + width, y + height);
            if (!this.isSelected(index)) {
                g.setColor(borderShadowColor);
                g.drawLine(x + width - 1, y + 1, x + width - 1, y + height - 1);
            }
        }
        g.setColor(borderColor);
        if (!this.isSelected(index)) {
            g.drawLine(x, y + height - 1, x + width, y + height - 1);
        }
        g.drawLine(x, y, x + width, y);
        if (this.getDataModel().size() == 1) {
            g.setColor(UIManager.getColor("NbTabControl.editorTabBackground"));
            g.drawLine(x, y + height - 1, x + width, y + height - 1);
        }
        if (this.isSelected(index) && this.isFocused(index)) {
            g.setColor(UIManager.getColor("NbTabControl.focusedTabBackground"));
            g.drawLine(x + (index == 0 ? 0 : 1), y + 1, x + width - 1, y + 1);
            g.drawLine(x + (index == 0 ? 0 : 1), y + 2, x + width - 1, y + 2);
        }
    }

    @Override
    protected void paintTabBackground(Graphics g, int index, int x, int y, int width, int height) {
        Graphics2D g2d = (Graphics2D)g;
        Paint p = g2d.getPaint();
        if (this.isSelected(index)) {
            g2d.setPaint(ColorUtil.getGradientPaint(x, y, UIManager.getColor("NbTabControl.selectedTabBrighterBackground"), x, y + height / 2, UIManager.getColor("NbTabControl.selectedTabDarkerBackground")));
        } else if (this.isMouseOver(index)) {
            g2d.setPaint(ColorUtil.getGradientPaint(x, y, UIManager.getColor("NbTabControl.mouseoverTabBrighterBackground"), x, y + height / 2, UIManager.getColor("NbTabControl.mouseoverTabDarkerBackground")));
        } else {
            g2d.setPaint(ColorUtil.getGradientPaint(x, y, UIManager.getColor("NbTabControl.inactiveTabBrighterBackground"), x, y + height / 2, UIManager.getColor("NbTabControl.inactiveTabDarkerBackground")));
        }
        g2d.fillRect(x, y, width, height);
        g2d.setPaint(p);
    }

    private static void initIcons() {
        if (null == buttonIconPaths) {
            String[] iconPaths;
            buttonIconPaths = new HashMap<Integer, String[]>(7);
            iconPaths = new String[]{"org/openide/awt/resources/mac_bigclose_enabled.png", "org/openide/awt/resources/mac_bigclose_pressed.png", iconPaths[0], "org/openide/awt/resources/mac_bigclose_rollover.png"};
            buttonIconPaths.put(1, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/mac_slideright_enabled.png", "org/netbeans/swing/tabcontrol/resources/mac_slideright_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/mac_slideright_rollover.png"};
            buttonIconPaths.put(6, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/mac_slideleft_enabled.png", "org/netbeans/swing/tabcontrol/resources/mac_slideleft_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/mac_slideleft_rollover.png"};
            buttonIconPaths.put(5, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/mac_slidebottom_enabled.png", "org/netbeans/swing/tabcontrol/resources/mac_slidebottom_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/mac_slidebottom_rollover.png"};
            buttonIconPaths.put(7, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/mac_pin_enabled.png", "org/netbeans/swing/tabcontrol/resources/mac_pin_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/mac_pin_rollover.png"};
            buttonIconPaths.put(2, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/mac_restore_group_enabled.png", "org/netbeans/swing/tabcontrol/resources/mac_restore_group_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/mac_restore_group_rollover.png"};
            buttonIconPaths.put(11, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/mac_minimize_enabled.png", "org/netbeans/swing/tabcontrol/resources/mac_minimize_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/mac_minimize_rollover.png"};
            buttonIconPaths.put(12, iconPaths);
        }
    }

    @Override
    public Icon getButtonIcon(int buttonId, int buttonState) {
        Icon res = null;
        AquaViewTabDisplayerUI.initIcons();
        String[] paths = buttonIconPaths.get(buttonId);
        if (null != paths && buttonState >= 0 && buttonState < paths.length) {
            res = TabControlButtonFactory.getIcon(paths[buttonState]);
        }
        return res;
    }

    private class OwnController
    extends AbstractViewTabDisplayerUI.Controller {
        private int lastIndex;

        private OwnController() {
            this.lastIndex = -1;
        }

        public int getMouseIndex() {
            return this.lastIndex;
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            super.mouseMoved(e);
            Point pos = e.getPoint();
            this.updateHighlight(AquaViewTabDisplayerUI.this.getLayoutModel().indexOfPoint(pos.x, pos.y));
        }

        @Override
        public void mouseExited(MouseEvent e) {
            super.mouseExited(e);
            if (!this.inControlButtonsRect(e.getPoint())) {
                this.updateHighlight(-1);
            }
        }

        private void updateHighlight(int curIndex) {
            int y;
            int h;
            int w;
            int x;
            if (curIndex == this.lastIndex) {
                return;
            }
            TabLayoutModel tlm = AquaViewTabDisplayerUI.this.getLayoutModel();
            Rectangle repaintRect = null;
            if (curIndex != -1) {
                x = tlm.getX(curIndex) - 1;
                y = tlm.getY(curIndex);
                w = tlm.getW(curIndex) + 2;
                h = tlm.getH(curIndex);
                repaintRect = new Rectangle(x, y, w, h);
            }
            if (this.lastIndex != -1 && this.lastIndex < AquaViewTabDisplayerUI.this.getDataModel().size()) {
                x = tlm.getX(this.lastIndex) - 1;
                y = tlm.getY(this.lastIndex);
                w = tlm.getW(this.lastIndex) + 2;
                h = tlm.getH(this.lastIndex);
                repaintRect = repaintRect != null ? repaintRect.union(new Rectangle(x, y, w, h)) : new Rectangle(x, y, w, h);
            }
            if (repaintRect != null) {
                AquaViewTabDisplayerUI.this.getDisplayer().repaint(repaintRect);
            }
            this.lastIndex = curIndex;
        }
    }

}

