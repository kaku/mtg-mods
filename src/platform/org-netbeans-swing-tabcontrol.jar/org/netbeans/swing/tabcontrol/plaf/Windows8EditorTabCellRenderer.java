/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import org.netbeans.swing.tabcontrol.plaf.WinVistaEditorTabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.Windows8ViewTabDisplayerUI;

final class Windows8EditorTabCellRenderer
extends WinVistaEditorTabCellRenderer {
    @Override
    void paintTabGradient(Graphics g, Polygon poly) {
        Rectangle rect = poly.getBounds();
        boolean selected = this.isSelected();
        boolean focused = selected && this.isActive();
        boolean attention = this.isAttention();
        boolean mouseOver = this.isArmed();
        Windows8ViewTabDisplayerUI.paintTabBackground((Graphics2D)g, rect.x, rect.y, rect.width, rect.height, selected, focused, attention, mouseOver);
    }

    @Override
    String findIconPath() {
        if (this.inCloseButton() && this.isPressed()) {
            return "org/openide/awt/resources/win8_bigclose_pressed.png";
        }
        if (this.inCloseButton()) {
            return "org/openide/awt/resources/win8_bigclose_rollover.png";
        }
        return "org/openide/awt/resources/win8_bigclose_enabled.png";
    }
}

