/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ChangeSupport
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;
import javax.swing.SingleSelectionModel;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListDataEvent;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.event.ArrayDiff;
import org.netbeans.swing.tabcontrol.event.ComplexListDataEvent;
import org.netbeans.swing.tabcontrol.event.ComplexListDataListener;
import org.netbeans.swing.tabcontrol.event.VeryComplexListDataEvent;
import org.openide.util.ChangeSupport;

final class DefaultTabSelectionModel
implements SingleSelectionModel,
ComplexListDataListener {
    TabDataModel dataModel;
    int sel = -1;
    private final ChangeSupport cs;

    public DefaultTabSelectionModel(TabDataModel tdm) {
        this.cs = new ChangeSupport((Object)this);
        this.dataModel = tdm;
        this.attach();
    }

    public void attach() {
        this.dataModel.addComplexListDataListener(this);
    }

    public void detach() {
        this.dataModel.removeComplexListDataListener(this);
    }

    @Override
    public void clearSelection() {
        this.sel = -1;
        this.cs.fireChange();
    }

    @Override
    public int getSelectedIndex() {
        return this.sel;
    }

    @Override
    public boolean isSelected() {
        return this.sel != -1;
    }

    @Override
    public void setSelectedIndex(int index) {
        if (index != this.sel) {
            int oldIndex = this.sel;
            if (index < -1 || index >= this.dataModel.size()) {
                throw new IllegalArgumentException("Selected index set to " + index + " but model size is only " + this.dataModel.size());
            }
            this.sel = index;
            this.cs.fireChange();
        }
    }

    private void adjustSelectionForEvent(ListDataEvent e) {
        if (e.getType() == 0 || this.sel == -1) {
            return;
        }
        int start = e.getIndex0();
        int end = e.getIndex1() + 1;
        if (e.getType() == 2) {
            if (this.sel < start) {
                return;
            }
            if (this.sel >= start) {
                if (this.sel > end) {
                    this.sel -= end - start;
                } else {
                    this.sel = start;
                    if (this.sel >= this.dataModel.size()) {
                        this.sel = this.dataModel.size() - 1;
                    }
                }
                this.cs.fireChange();
            }
        } else {
            if (this.sel < start) {
                return;
            }
            if (this.sel >= start) {
                this.sel = end - 1 == start ? ++this.sel : (this.sel < end ? end + (this.sel - start) - 1 : (this.sel += end - start - 1));
                this.cs.fireChange();
            }
        }
    }

    @Override
    public void contentsChanged(ListDataEvent e) {
        this.adjustSelectionForEvent(e);
    }

    @Override
    public void intervalAdded(ListDataEvent e) {
        this.adjustSelectionForEvent(e);
    }

    @Override
    public void intervalRemoved(ListDataEvent e) {
        this.adjustSelectionForEvent(e);
    }

    @Override
    public void indicesAdded(ComplexListDataEvent e) {
        if (this.sel < 0) {
            return;
        }
        int[] indices = e.getIndices();
        Arrays.sort(indices);
        int offset = 0;
        for (int i = 0; i < indices.length && this.sel >= indices[i]; ++i) {
            ++offset;
        }
        if (offset > 0) {
            this.sel += offset;
            this.cs.fireChange();
        }
    }

    @Override
    public void indicesRemoved(ComplexListDataEvent e) {
        if (this.sel < 0) {
            return;
        }
        int[] indices = e.getIndices();
        Arrays.sort(indices);
        int offset = -1;
        for (int i = 0; i < indices.length && this.sel > indices[i]; ++i) {
            --offset;
        }
        if (this.sel == this.dataModel.size()) {
            --this.sel;
            this.cs.fireChange();
            return;
        }
        if (this.dataModel.size() == 0) {
            this.sel = -1;
            this.cs.fireChange();
        } else if (offset != 0) {
            this.sel = Math.max(-1, Math.min(this.sel + offset, -1));
            this.cs.fireChange();
        }
    }

    @Override
    public void indicesChanged(ComplexListDataEvent e) {
        if (this.sel < 0) {
            return;
        }
        if (e instanceof VeryComplexListDataEvent) {
            ArrayDiff dif = ((VeryComplexListDataEvent)e).getDiff();
            boolean changed = false;
            if (dif == null) {
                return;
            }
            Set<Integer> deleted = dif.getDeletedIndices();
            Set<Integer> added = dif.getAddedIndices();
            Integer idx = new Integer(this.getSelectedIndex());
            if (this.dataModel.size() == 0) {
                this.sel = -1;
                this.cs.fireChange();
                return;
            }
            Iterator<Integer> i = deleted.iterator();
            int offset = 0;
            while (i.hasNext()) {
                Integer curr = i.next();
                if (curr.compareTo(idx) > 0) continue;
                ++offset;
            }
            for (Integer curr : added) {
                if (curr.compareTo(idx) < 0) continue;
                --offset;
            }
            this.sel -= offset;
            if (this.sel < 0) {
                int n = this.sel = this.dataModel.size() > 0 ? 0 : -1;
            }
            if (this.sel >= this.dataModel.size()) {
                this.sel = this.dataModel.size() - 1;
            }
            if (offset != 0) {
                this.cs.fireChange();
            }
        }
    }

    @Override
    public void addChangeListener(ChangeListener listener) {
        this.cs.addChangeListener(listener);
    }

    @Override
    public synchronized void removeChangeListener(ChangeListener listener) {
        this.cs.removeChangeListener(listener);
    }
}

