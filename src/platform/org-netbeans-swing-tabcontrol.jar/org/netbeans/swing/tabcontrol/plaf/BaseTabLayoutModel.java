/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JComponent;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.plaf.BasicScrollingTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.TabLayoutModel;
import org.openide.awt.HtmlRenderer;

class BaseTabLayoutModel
implements TabLayoutModel {
    protected TabDataModel model;
    protected int textHeight = -1;
    protected int padX = 5;
    protected int padY = 5;
    protected JComponent renderTarget;
    private static Map<String, Integer> widthMap = new HashMap<String, Integer>(31);

    protected BaseTabLayoutModel(TabDataModel model, JComponent renderTarget) {
        this.model = model;
        this.renderTarget = renderTarget;
    }

    private Font getFont() {
        return this.renderTarget.getFont();
    }

    protected int iconWidth(int index) {
        Icon ic = this.model.getTab(index).getIcon();
        int result = ic != null ? ic.getIconWidth() : 0;
        return result;
    }

    protected int iconHeight(int index) {
        Icon ic = this.model.getTab(index).getIcon();
        int result = ic != null ? ic.getIconHeight() : 0;
        return result;
    }

    protected int textWidth(int index) {
        try {
            String text = this.model.getTab(index).getText();
            return BaseTabLayoutModel.textWidth(text, this.getFont());
        }
        catch (NullPointerException npe) {
            IllegalArgumentException iae = new IllegalArgumentException("Error fetching width for tab " + index + " - model size is " + this.model.size() + " TabData is " + this.model.getTab(index) + " model contents: " + this.model);
            throw iae;
        }
    }

    static int textWidth(String text, Font f) {
        Integer result = widthMap.get(text);
        if (result == null) {
            double wid = HtmlRenderer.renderString((String)text, (Graphics)BasicScrollingTabDisplayerUI.getOffscreenGraphics(), (int)0, (int)0, (int)Integer.MAX_VALUE, (int)Integer.MAX_VALUE, (Font)f, (Color)Color.BLACK, (int)1, (boolean)false);
            result = new Integer(Math.round(Math.round(wid)));
            widthMap.put(text, result);
        }
        return result;
    }

    protected int textHeight(int index) {
        if (this.textHeight == -1) {
            String testStr = "Zgj";
            Font f = this.getFont();
            this.textHeight = new Double(f.getStringBounds(testStr, BasicScrollingTabDisplayerUI.getOffscreenGraphics().getFontRenderContext()).getWidth()).intValue() + 2;
        }
        return this.textHeight;
    }

    @Override
    public int getX(int index) {
        int result = this.renderTarget.getInsets().left;
        for (int i = 0; i < index; ++i) {
            result += this.getW(i);
        }
        return result;
    }

    @Override
    public int getY(int index) {
        return this.renderTarget.getInsets().top;
    }

    @Override
    public int getH(int index) {
        return Math.max(this.textHeight(index) + this.padY, this.model.getTab(index).getIcon().getIconHeight() + this.padY);
    }

    @Override
    public int getW(int index) {
        int iconWidth = this.iconWidth(index);
        if (0 == iconWidth) {
            iconWidth = 5;
        }
        return this.textWidth(index) + iconWidth + this.padX;
    }

    @Override
    public int indexOfPoint(int x, int y) {
        int max = this.model.size();
        int pos = this.renderTarget.getInsets().left;
        for (int i = 0; i < max; ++i) {
            if ((pos += this.getW(i)) <= x) continue;
            return i;
        }
        return -1;
    }

    @Override
    public int dropIndexOfPoint(int x, int y) {
        Insets insets = this.renderTarget.getInsets();
        int contentWidth = this.renderTarget.getWidth() - (insets.left + insets.right);
        int contentHeight = this.renderTarget.getHeight() - (insets.bottom + insets.top);
        if (y < insets.top || y > contentHeight || x < insets.left || x > contentWidth) {
            return -1;
        }
        int max = this.model.size();
        int pos = insets.left;
        for (int i = 0; i < max; ++i) {
            int delta = this.getW(i);
            if (x <= (pos += delta) - delta / 2) {
                return i;
            }
            if (x >= pos) continue;
            return i + 1;
        }
        return max;
    }

    @Override
    public void setPadding(Dimension d) {
        this.padX = d.width;
        this.padY = d.height;
    }
}

