/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Polygon;
import java.awt.Rectangle;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.UIManager;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.plaf.AbstractTabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.BusyTabsSupport;
import org.netbeans.swing.tabcontrol.plaf.ColorUtil;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.netbeans.swing.tabcontrol.plaf.TabPainter;

final class AquaEditorTabCellRenderer
extends AbstractTabCellRenderer {
    private static final int TOP_INSET = 0;
    private static final int LEFT_INSET = 3;
    private static final int RIGHT_INSET = 0;
    static final int BOTTOM_INSET = 0;
    private static final TabPainter leftClip = new AquaLeftClipPainter();
    private static final TabPainter rightClip = new AquaRightClipPainter();
    private static final TabPainter normal = new AquaPainter();
    private Font txtFont;

    public AquaEditorTabCellRenderer() {
        super(leftClip, normal, rightClip, new Dimension(32, 42));
    }

    @Override
    public Font getFont() {
        if (this.txtFont == null) {
            this.txtFont = (Font)UIManager.get("windowTitleFont");
            if (this.txtFont == null) {
                this.txtFont = new Font("Dialog", 0, 11);
            } else if (this.txtFont.isBold()) {
                this.txtFont = new Font(this.txtFont.getName(), 0, this.txtFont.getSize());
            }
        }
        return this.txtFont;
    }

    @Override
    protected void paintIconAndText(Graphics g) {
        if (this.isBusy()) {
            this.setIcon(BusyTabsSupport.getDefault().getBusyIcon(this.isSelected()));
        }
        super.paintIconAndText(g);
    }

    @Override
    public int getPixelsToAddToSelection() {
        return 0;
    }

    @Override
    public Dimension getPadding() {
        Dimension d = super.getPadding();
        d.width = this.isShowCloseButton() && !Boolean.getBoolean("nb.tabs.suppressCloseButton") ? 32 : 16;
        return d;
    }

    private static final Color getTxtColor() {
        Color result = UIManager.getColor("TabbedPane.foreground");
        if (result == null) {
            result = new Color(0, 0, 0);
        }
        return result;
    }

    @Override
    public Color getSelectedActivatedForeground() {
        return AquaEditorTabCellRenderer.getTxtColor();
    }

    @Override
    public Color getSelectedForeground() {
        return AquaEditorTabCellRenderer.getTxtColor();
    }

    private static void paintTabGradient(Graphics g, AquaEditorTabCellRenderer ren, Polygon poly) {
        Rectangle rect = poly.getBounds();
        boolean selected = ren.isSelected();
        boolean focused = selected && ren.isActive();
        boolean attention = ren.isAttention();
        boolean mouseOver = ren.isArmed();
        if (focused && !attention) {
            ColorUtil.paintMacGradientFill((Graphics2D)g, rect, UIManager.getColor("NbTabControl.selectedTabBrighterBackground"), UIManager.getColor("NbTabControl.selectedTabDarkerBackground"));
        } else if (selected && !attention) {
            ColorUtil.paintMacGradientFill((Graphics2D)g, rect, UIManager.getColor("NbTabControl.selectedTabBrighterBackground"), UIManager.getColor("NbTabControl.selectedTabDarkerBackground"));
        } else if (mouseOver && !attention) {
            ColorUtil.paintMacGradientFill((Graphics2D)g, rect, UIManager.getColor("NbTabControl.mouseoverTabBrighterBackground"), UIManager.getColor("NbTabControl.mouseoverTabDarkerBackground"));
        } else if (attention) {
            Color a = new Color(255, 255, 128);
            Color b = new Color(230, 200, 64);
            ColorUtil.xpFillRectGradient((Graphics2D)g, rect, a, b);
        } else {
            ColorUtil.paintMacGradientFill((Graphics2D)g, rect, UIManager.getColor("NbTabControl.inactiveTabBrighterBackground"), UIManager.getColor("NbTabControl.inactiveTabDarkerBackground"));
        }
    }

    @Override
    protected int getCaptionYAdjustment() {
        return 0;
    }

    @Override
    protected int getIconYAdjustment() {
        return -2;
    }

    private static class AquaRightClipPainter
    implements TabPainter {
        private AquaRightClipPainter() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return new Insets(0, 3, 0, 0);
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        @Override
        public Polygon getInteriorPolygon(Component c) {
            AquaEditorTabCellRenderer ren = (AquaEditorTabCellRenderer)c;
            Insets ins = this.getBorderInsets(c);
            Polygon p = new Polygon();
            int x = 0;
            int y = 0;
            int width = c.getWidth() + 1;
            int height = c.getHeight() - ins.bottom;
            p.addPoint(x, y + ins.top);
            p.addPoint(x + width, y + ins.top);
            p.addPoint(x + width, y + height - 1);
            p.addPoint(x, y + height - 1);
            return p;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            AquaEditorTabCellRenderer ren = (AquaEditorTabCellRenderer)c;
            Color borderColor = UIManager.getColor("NbTabControl.borderColor");
            g.setColor(borderColor);
            g.drawLine(x, y, width, y);
            if (!ren.isSelected()) {
                g.setColor(borderColor);
                g.drawLine(x, y + height - 1, width, y + height - 1);
            } else {
                g.setColor(UIManager.getColor("NbTabControl.selectedTabDarkerBackground"));
                g.drawLine(x, y + height - 1, width, y + height - 1);
            }
            if (!ren.isLeftmost() && !ren.isSelected()) {
                g.setColor(UIManager.getColor("NbTabControl.editorBorderShadowColor"));
                g.drawLine(x, y + 1, x, y + height - 2);
            }
        }

        @Override
        public void paintInterior(Graphics g, Component c) {
            AquaEditorTabCellRenderer ren = (AquaEditorTabCellRenderer)c;
            Polygon poly = this.getInteriorPolygon(ren);
            AquaEditorTabCellRenderer.paintTabGradient(g, ren, poly);
            if (ren.isActive() && ren.isSelected()) {
                int x = 0;
                int y = 0;
                int width = ren.getWidth();
                g.setColor(UIManager.getColor("NbTabControl.focusedTabBackground"));
                g.drawLine(x, y + 1, x + width, y + 1);
                g.drawLine(x, y + 2, x + width, y + 2);
            }
        }

        @Override
        public boolean supportsCloseButton(JComponent renderer) {
            return false;
        }

        @Override
        public void getCloseButtonRectangle(JComponent jc, Rectangle rect, Rectangle bounds) {
            rect.setBounds(-20, -20, 0, 0);
        }
    }

    private static class AquaLeftClipPainter
    implements TabPainter {
        private AquaLeftClipPainter() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return new Insets(0, 3, 0, 0);
        }

        @Override
        public Polygon getInteriorPolygon(Component c) {
            AquaEditorTabCellRenderer ren = (AquaEditorTabCellRenderer)c;
            Insets ins = this.getBorderInsets(c);
            Polygon p = new Polygon();
            int x = 0;
            int y = 0;
            int width = ren.isRightmost() ? c.getWidth() - 1 : c.getWidth();
            int height = c.getHeight() - ins.bottom;
            p.addPoint(x, y + ins.top);
            p.addPoint(x + width, y + ins.top);
            p.addPoint(x + width, y + height - 1);
            p.addPoint(x, y + height - 1);
            return p;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            AquaEditorTabCellRenderer ren = (AquaEditorTabCellRenderer)c;
            Color borderColor = UIManager.getColor("NbTabControl.borderColor");
            g.setColor(borderColor);
            g.drawLine(x, y, width - 1, y);
            if (!ren.isSelected()) {
                g.setColor(borderColor);
                g.drawLine(x, y + height - 1, width, y + height - 1);
            } else {
                g.setColor(UIManager.getColor("NbTabControl.selectedTabDarkerBackground"));
                g.drawLine(x, y + height - 1, width, y + height - 1);
            }
            if (ren.isRightmost() || !ren.isSelected()) {
                g.setColor(borderColor);
                g.drawLine(x + width - 1, y, x + width - 1, y + height - 1);
                g.setColor(UIManager.getColor("NbTabControl.editorBorderShadowColor"));
                g.drawLine(x + width - 2, y + 1, x + width - 2, y + height - (ren.isSelected() ? 1 : 2));
            }
            if (ren.isActive() && ren.isSelected()) {
                g.setColor(UIManager.getColor("NbTabControl.focusedTabBackground"));
                g.drawLine(x, y + 1, x + width - 1, y + 1);
                g.drawLine(x, y + 2, x + width - 1, y + 2);
            }
        }

        @Override
        public void paintInterior(Graphics g, Component c) {
            AquaEditorTabCellRenderer ren = (AquaEditorTabCellRenderer)c;
            Polygon poly = this.getInteriorPolygon(ren);
            AquaEditorTabCellRenderer.paintTabGradient(g, ren, poly);
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        @Override
        public boolean supportsCloseButton(JComponent renderer) {
            return false;
        }

        @Override
        public void getCloseButtonRectangle(JComponent jc, Rectangle rect, Rectangle bounds) {
            rect.setBounds(-20, -20, 0, 0);
        }
    }

    private static class AquaPainter
    implements TabPainter {
        private AquaPainter() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return new Insets(0, 3, 0, 0);
        }

        @Override
        public void getCloseButtonRectangle(JComponent jc, Rectangle rect, Rectangle bounds) {
            AquaEditorTabCellRenderer ren = (AquaEditorTabCellRenderer)jc;
            if (!ren.isShowCloseButton()) {
                rect.x = -100;
                rect.y = -100;
                rect.width = 0;
                rect.height = 0;
                return;
            }
            String iconPath = this.findIconPath(ren);
            Icon icon = TabControlButtonFactory.getIcon(iconPath);
            int iconWidth = icon.getIconWidth();
            int iconHeight = icon.getIconHeight();
            rect.x = bounds.x + bounds.width - iconWidth - 5;
            rect.y = bounds.y + Math.max(0, bounds.height / 2 - iconHeight / 2);
            rect.width = iconWidth;
            rect.height = iconHeight;
        }

        private String findIconPath(AquaEditorTabCellRenderer renderer) {
            if (renderer.inCloseButton() && renderer.isPressed()) {
                return "org/openide/awt/resources/mac_close_pressed.png";
            }
            if (renderer.inCloseButton()) {
                return "org/openide/awt/resources/mac_close_rollover.png";
            }
            return "org/openide/awt/resources/mac_close_enabled.png";
        }

        @Override
        public Polygon getInteriorPolygon(Component c) {
            AquaEditorTabCellRenderer ren = (AquaEditorTabCellRenderer)c;
            Insets ins = this.getBorderInsets(c);
            Polygon p = new Polygon();
            int x = 0;
            int y = 0;
            int width = ren.isRightmost() ? c.getWidth() - 1 : c.getWidth();
            int height = c.getHeight() - ins.bottom;
            p.addPoint(x, y + ins.top);
            p.addPoint(x + width, y + ins.top);
            p.addPoint(x + width, y + height - 1);
            p.addPoint(x, y + height - 1);
            return p;
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            AquaEditorTabCellRenderer ren = (AquaEditorTabCellRenderer)c;
            Color borderColor = UIManager.getColor("NbTabControl.borderColor");
            Color shadowColor = UIManager.getColor("NbTabControl.borderShadowColor");
            g.setColor(borderColor);
            g.drawLine(x, y, width - 1, y);
            if (!ren.isSelected()) {
                g.setColor(borderColor);
                g.drawLine(x, y + height - 1, width, y + height - 1);
            } else {
                g.setColor(UIManager.getColor("NbTabControl.selectedTabDarkerBackground"));
                g.drawLine(x, y + height - 1, width, y + height - 1);
            }
            if (ren.isRightmost() || !ren.isSelected()) {
                g.setColor(borderColor);
                g.drawLine(x + width - 1, y, x + width - 1, y + height - 1);
                g.setColor(shadowColor);
                g.drawLine(x + width - 2, y + 1, x + width - 2, y + height - (ren.isSelected() ? 1 : 2));
            } else if (ren.isSelected()) {
                g.setColor(borderColor);
                g.drawLine(x + width - 1, y, x + width - 1, y + height - 1);
            }
            if (!ren.isLeftmost() && !ren.isSelected()) {
                g.setColor(shadowColor);
                g.drawLine(x, y + 1, x, y + height - 2);
            }
        }

        @Override
        public void paintInterior(Graphics g, Component c) {
            AquaEditorTabCellRenderer ren = (AquaEditorTabCellRenderer)c;
            Polygon poly = this.getInteriorPolygon(ren);
            AquaEditorTabCellRenderer.paintTabGradient(g, ren, poly);
            Rectangle r = new Rectangle();
            this.getCloseButtonRectangle(ren, r, new Rectangle(0, 0, ren.getWidth(), ren.getHeight()));
            if (ren.isActive() && ren.isSelected()) {
                int x = 0;
                int y = 0;
                int width = ren.getWidth();
                g.setColor(UIManager.getColor("NbTabControl.focusedTabBackground"));
                g.drawLine(x, y + 1, x + width - 2, y + 1);
                g.drawLine(x, y + 2, x + width - 2, y + 2);
            }
            if (!g.hitClip(r.x, r.y, r.width, r.height)) {
                return;
            }
            String iconPath = this.findIconPath(ren);
            Icon icon = TabControlButtonFactory.getIcon(iconPath);
            icon.paintIcon(ren, g, r.x, r.y);
        }

        @Override
        public boolean supportsCloseButton(JComponent renderer) {
            return renderer instanceof TabDisplayer ? ((TabDisplayer)renderer).isShowCloseButton() : true;
        }
    }

}

