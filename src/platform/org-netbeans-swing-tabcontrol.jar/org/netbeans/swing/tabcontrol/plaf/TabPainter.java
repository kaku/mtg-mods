/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.Rectangle;
import javax.swing.JComponent;
import javax.swing.border.Border;

public interface TabPainter
extends Border {
    public Polygon getInteriorPolygon(Component var1);

    public void paintInterior(Graphics var1, Component var2);

    public void getCloseButtonRectangle(JComponent var1, Rectangle var2, Rectangle var3);

    public boolean supportsCloseButton(JComponent var1);
}

