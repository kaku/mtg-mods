/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import javax.swing.Timer;
import javax.swing.event.ListDataEvent;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.event.ArrayDiff;
import org.netbeans.swing.tabcontrol.event.ComplexListDataEvent;
import org.netbeans.swing.tabcontrol.event.VeryComplexListDataEvent;
import org.openide.util.Utilities;

public abstract class TabState {
    public static final int CLIP_RIGHT = 1;
    public static final int CLIP_LEFT = 2;
    public static final int ARMED = 4;
    public static final int PRESSED = 8;
    public static final int SELECTED = 16;
    public static final int ACTIVE = 32;
    public static final int NOT_ONSCREEN = 64;
    public static final int LEFTMOST = 128;
    public static final int RIGHTMOST = 256;
    public static final int CLOSE_BUTTON_ARMED = 512;
    public static final int BEFORE_SELECTED = 1024;
    public static final int AFTER_SELECTED = 2048;
    public static final int MOUSE_IN_TABS_AREA = 4096;
    public static final int MOUSE_PRESSED_IN_CLOSE_BUTTON = 8192;
    public static final int ATTENTION = 16384;
    public static final int BEFORE_ARMED = 32768;
    public static final int BUSY = 65536;
    public static final int HIGHLIGHT = 131072;
    public static int STATE_LAST = 131072;
    private int pressedIndex = -1;
    private int containsMouseIndex = -1;
    private int closeButtonContainsMouseIndex = -1;
    private int mousePressedInCloseButtonIndex = -1;
    private boolean mouseInTabsArea = false;
    private boolean active = false;
    private int selectedIndex = -1;
    private int prev = -1;
    private int curr = -1;
    private int lastChangeType = 0;
    private int lastAffected = 0;
    private int lastChange = 0;
    public static final int REPAINT_ON_MOUSE_ENTER_TAB = 1;
    public static final int REPAINT_ALL_ON_MOUSE_ENTER_TABS_AREA = 3;
    public static final int REPAINT_ON_MOUSE_ENTER_CLOSE_BUTTON = 4;
    public static final int REPAINT_ON_MOUSE_PRESSED = 8;
    public static final int REPAINT_SELECTION_ON_ACTIVATION_CHANGE = 16;
    public static final int REPAINT_ALL_TABS_ON_ACTIVATION_CHANGE = 32;
    public static final int REPAINT_ON_SELECTION_CHANGE = 64;
    public static final int REPAINT_ALL_TABS_ON_SELECTION_CHANGE = 128;
    public static final int REPAINT_ON_CLOSE_BUTTON_PRESSED = 256;
    private final HashSet<Integer> alarmTabs = new HashSet(6);
    private final HashSet<Integer> highlightTabs = new HashSet(6);
    private Timer alarmTimer = null;
    private boolean attentionToggle = false;
    public static final int NO_CHANGE = 0;
    public static final int CHANGE_TAB_TO_TAB = 1;
    public static final int CHANGE_TAB_TO_NONE = 2;
    public static final int CHANGE_NONE_TO_TAB = 3;
    public static final int CHANGE_TAB_TO_SELF = 4;
    public static final int ALL_TABS = Integer.MAX_VALUE;

    public int getState(int tab) {
        int result = 0;
        if (tab == this.pressedIndex) {
            result |= 8;
        }
        if (tab == this.containsMouseIndex) {
            result |= 4;
        }
        if (tab == this.closeButtonContainsMouseIndex) {
            result |= 512;
        }
        if (tab == this.mousePressedInCloseButtonIndex) {
            result |= 8192;
        }
        if (this.mouseInTabsArea) {
            result |= 4096;
        }
        if (this.active) {
            result |= 32;
        }
        if (tab == this.selectedIndex) {
            result |= 16;
        }
        if (tab != 0 && tab == this.selectedIndex + 1) {
            result |= 2048;
        }
        if (tab == this.selectedIndex - 1) {
            result |= 1024;
        }
        if (tab == this.containsMouseIndex - 1) {
            result |= 32768;
        }
        if (this.isAlarmTab(tab)) {
            result |= 16384;
        }
        if (this.isHighlightTab(tab)) {
            result |= 131072;
        }
        return result;
    }

    String getStateString(int tab) {
        return TabState.stateToString(this.getState(tab));
    }

    public void clearTransientStates() {
        this.pressedIndex = -1;
        this.containsMouseIndex = -1;
        this.closeButtonContainsMouseIndex = -1;
        this.mousePressedInCloseButtonIndex = -1;
        this.mouseInTabsArea = false;
        this.lastChangeType = 0;
        this.lastChange = 0;
        this.prev = -1;
        this.curr = -1;
    }

    public final int setPressed(int i) {
        this.prev = this.pressedIndex;
        this.pressedIndex = i;
        this.curr = i;
        this.possibleChange(this.prev, this.curr, 8);
        return this.prev;
    }

    public final int setContainsMouse(int i) {
        this.prev = this.containsMouseIndex;
        this.containsMouseIndex = i;
        this.curr = i;
        this.possibleChange(this.prev, this.curr, 4);
        return this.prev;
    }

    public final int setCloseButtonContainsMouse(int i) {
        this.prev = this.closeButtonContainsMouseIndex;
        this.closeButtonContainsMouseIndex = i;
        this.curr = i;
        this.possibleChange(this.prev, this.curr, 512);
        return this.prev;
    }

    public final int setMousePressedInCloseButton(int i) {
        this.prev = this.mousePressedInCloseButtonIndex;
        this.mousePressedInCloseButtonIndex = i;
        this.curr = i;
        this.possibleChange(this.prev, this.curr, 8192);
        return this.prev;
    }

    public final int setSelected(int i) {
        this.prev = this.selectedIndex;
        this.selectedIndex = i;
        this.curr = i;
        this.removeAlarmTab(i);
        this.removeHighlightTab(i);
        this.possibleChange(this.prev, this.curr, 16);
        return this.prev;
    }

    public final boolean setMouseInTabsArea(boolean b) {
        boolean prev = this.mouseInTabsArea;
        this.mouseInTabsArea = b;
        this.possibleChange(prev, b, 4096);
        return prev;
    }

    public final boolean setActive(boolean b) {
        boolean prev = this.active;
        this.active = b;
        this.possibleChange(prev, b, 32);
        this.removeAlarmTab(this.selectedIndex);
        this.removeHighlightTab(this.selectedIndex);
        return prev;
    }

    private boolean isAlarmTab(int tab) {
        return this.attentionToggle && this.alarmTabs.contains(new Integer(tab));
    }

    public final void addAlarmTab(int alarmTab) {
        Integer in = new Integer(alarmTab);
        boolean added = this.alarmTabs.contains(in);
        boolean wasEmpty = this.alarmTabs.isEmpty();
        if (!added) {
            this.alarmTabs.add(new Integer(alarmTab));
            this.repaintTab(alarmTab);
        }
        if (wasEmpty) {
            this.startAlarmTimer();
            this.attentionToggle = true;
            this.repaintTab(alarmTab);
        }
    }

    private boolean isHighlightTab(int tab) {
        return this.highlightTabs.contains(tab) && !this.alarmTabs.contains(tab);
    }

    public final void addHighlightTab(int highlightTab) {
        boolean added = this.highlightTabs.add(highlightTab);
        if (added) {
            this.repaintTab(highlightTab);
        }
    }

    public final void removeAlarmTab(int alarmTab) {
        Integer in = new Integer(alarmTab);
        boolean contained = this.alarmTabs.contains(in);
        if (contained) {
            this.alarmTabs.remove(in);
            boolean empty = this.alarmTabs.isEmpty();
            boolean wasAttentionToggled = this.attentionToggle;
            if (this.alarmTabs.isEmpty()) {
                this.stopAlarmTimer();
            }
            if (wasAttentionToggled) {
                this.repaintTab(alarmTab);
            }
        }
    }

    public final void removeHighlightTab(int highlightTab) {
        boolean removed = this.highlightTabs.remove(highlightTab);
        if (removed) {
            this.repaintTab(highlightTab);
        }
    }

    private void startAlarmTimer() {
        if (this.alarmTimer == null) {
            ActionListener al = new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent ae) {
                    if (!TabState.this.isDisplayable()) {
                        TabState.this.stopAlarmTimer();
                    }
                    TabState.this.attentionToggle = !TabState.this.attentionToggle;
                    Iterator i = TabState.this.alarmTabs.iterator();
                    while (i.hasNext()) {
                        TabState.this.repaintTab((Integer)i.next());
                    }
                }
            };
            this.alarmTimer = new Timer(700, al);
            this.alarmTimer.setRepeats(true);
        }
        this.alarmTimer.start();
    }

    boolean isDisplayable() {
        return true;
    }

    private final void stopAlarmTimer() {
        if (this.alarmTimer != null && this.alarmTimer.isRunning()) {
            this.alarmTimer.stop();
            this.attentionToggle = false;
            this.repaintAllTabs();
        }
    }

    boolean hasAlarmTabs() {
        return !this.alarmTabs.isEmpty();
    }

    boolean hasHighlightTabs() {
        return !this.highlightTabs.isEmpty();
    }

    void pruneTabs(int max) {
        if (!this.hasAlarmTabs() && !this.hasHighlightTabs()) {
            return;
        }
        Iterator<Integer> i = this.alarmTabs.iterator();
        while (i.hasNext()) {
            if (i.next() < max) continue;
            i.remove();
        }
        i = this.highlightTabs.iterator();
        while (i.hasNext()) {
            if (i.next() < max) continue;
            i.remove();
        }
        if (this.alarmTabs.isEmpty()) {
            this.stopAlarmTimer();
        }
    }

    int[] getAlarmTabs() {
        int[] alarms = (int[])Utilities.toPrimitiveArray((Object[])this.alarmTabs.toArray(new Integer[0]));
        Arrays.sort(alarms);
        return alarms;
    }

    void intervalAdded(ListDataEvent evt) {
        int i;
        boolean changed;
        int start = evt.getIndex0();
        int end = evt.getIndex1();
        if (this.hasAlarmTabs()) {
            int[] alarms = (int[])Utilities.toPrimitiveArray((Object[])this.alarmTabs.toArray(new Integer[0]));
            changed = false;
            for (i = 0; i < alarms.length; ++i) {
                if (alarms[i] < start) continue;
                int[] arrn = alarms;
                int n = i;
                arrn[n] = arrn[n] + (end - start + 1);
                changed = true;
            }
            if (changed) {
                this.alarmTabs.clear();
                for (i = 0; i < alarms.length; ++i) {
                    this.addAlarmTab(alarms[i]);
                }
            }
        }
        if (this.hasHighlightTabs()) {
            int[] highlights = (int[])Utilities.toPrimitiveArray((Object[])this.highlightTabs.toArray(new Integer[0]));
            changed = false;
            for (i = 0; i < highlights.length; ++i) {
                if (highlights[i] < start) continue;
                int[] arrn = highlights;
                int n = i;
                arrn[n] = arrn[n] + (end - start + 1);
                changed = true;
            }
            if (changed) {
                this.highlightTabs.clear();
                for (i = 0; i < highlights.length; ++i) {
                    this.addHighlightTab(highlights[i]);
                }
            }
        }
    }

    void intervalRemoved(ListDataEvent evt) {
        int i;
        boolean changed;
        int start;
        int end;
        if (this.hasAlarmTabs()) {
            start = evt.getIndex0();
            end = evt.getIndex1();
            int[] alarms = (int[])Utilities.toPrimitiveArray((Object[])this.alarmTabs.toArray(new Integer[0]));
            Arrays.sort(alarms);
            if (end == start) {
                boolean changed2 = true;
                for (int i2 = 0; i2 < alarms.length; ++i2) {
                    if (alarms[i2] > end) {
                        int[] arrn = alarms;
                        int n = i2;
                        arrn[n] = arrn[n] - 1;
                        continue;
                    }
                    if (alarms[i2] != end) continue;
                    alarms[i2] = -1;
                }
                if (changed2) {
                    this.alarmTabs.clear();
                    boolean added = false;
                    for (int i3 = 0; i3 < alarms.length; ++i3) {
                        if (alarms[i3] == -1) continue;
                        this.addAlarmTab(alarms[i3]);
                        added = true;
                    }
                    if (!added) {
                        this.stopAlarmTimer();
                    }
                }
                return;
            }
            changed = false;
            for (i = 0; i < alarms.length; ++i) {
                if (alarms[i] < start || alarms[i] > end) continue;
                alarms[i] = -1;
                changed = true;
            }
            for (i = 0; i < alarms.length; ++i) {
                if (alarms[i] <= end) continue;
                int[] arrn = alarms;
                int n = i;
                arrn[n] = arrn[n] - (end - start + 1);
                changed = true;
            }
            if (changed) {
                this.alarmTabs.clear();
                boolean added = false;
                for (int i4 = 0; i4 < alarms.length; ++i4) {
                    if (alarms[i4] == -1) continue;
                    this.addAlarmTab(alarms[i4]);
                    added = true;
                }
                if (!added) {
                    this.stopAlarmTimer();
                }
            }
        }
        if (this.hasAlarmTabs()) {
            start = evt.getIndex0();
            end = evt.getIndex1();
            int[] highlights = (int[])Utilities.toPrimitiveArray((Object[])this.highlightTabs.toArray(new Integer[0]));
            Arrays.sort(highlights);
            changed = false;
            for (i = 0; i < highlights.length; ++i) {
                if (highlights[i] < start || highlights[i] > end) continue;
                highlights[i] = -1;
                changed = true;
            }
            for (i = 0; i < highlights.length; ++i) {
                if (highlights[i] <= end) continue;
                int[] arrn = highlights;
                int n = i;
                arrn[n] = arrn[n] - (end - start + 1);
                changed = true;
            }
            if (changed) {
                this.highlightTabs.clear();
                for (i = 0; i < highlights.length; ++i) {
                    if (highlights[i] == -1) continue;
                    this.addHighlightTab(highlights[i]);
                }
            }
        }
    }

    void indicesAdded(ComplexListDataEvent e) {
        int i;
        int j;
        boolean changed;
        int[] indices;
        if (this.hasAlarmTabs()) {
            int[] alarms = (int[])Utilities.toPrimitiveArray((Object[])this.alarmTabs.toArray(new Integer[0]));
            Arrays.sort(alarms);
            indices = e.getIndices();
            Arrays.sort(indices);
            changed = false;
            for (i = 0; i < indices.length; ++i) {
                for (j = 0; j < alarms.length; ++j) {
                    if (alarms[j] < indices[i]) continue;
                    int[] arrn = alarms;
                    int n = j;
                    arrn[n] = arrn[n] + 1;
                    changed = true;
                }
            }
            if (changed) {
                this.alarmTabs.clear();
                for (i = 0; i < alarms.length; ++i) {
                    if (alarms[i] == -1) continue;
                    this.addAlarmTab(alarms[i]);
                }
            }
        }
        if (this.hasHighlightTabs()) {
            int[] highlights = (int[])Utilities.toPrimitiveArray((Object[])this.highlightTabs.toArray(new Integer[0]));
            Arrays.sort(highlights);
            indices = e.getIndices();
            Arrays.sort(indices);
            changed = false;
            for (i = 0; i < indices.length; ++i) {
                for (j = 0; j < highlights.length; ++j) {
                    if (highlights[j] < indices[i]) continue;
                    int[] arrn = highlights;
                    int n = j;
                    arrn[n] = arrn[n] + 1;
                    changed = true;
                }
            }
            if (changed) {
                this.highlightTabs.clear();
                for (i = 0; i < highlights.length; ++i) {
                    if (highlights[i] == -1) continue;
                    this.addHighlightTab(highlights[i]);
                }
            }
        }
    }

    void indicesRemoved(ComplexListDataEvent e) {
        int[] indices;
        int j;
        int j2;
        int i;
        int alarm;
        boolean changed;
        if (this.hasAlarmTabs()) {
            indices = e.getIndices();
            Arrays.sort(indices);
            int[] alarms = (int[])Utilities.toPrimitiveArray((Object[])this.alarmTabs.toArray(new Integer[0]));
            Arrays.sort(alarms);
            if (alarms[alarms.length - 1] >= indices[0]) {
                changed = false;
                for (i = 0; i < alarms.length; ++i) {
                    for (j2 = 0; j2 < indices.length; ++j2) {
                        if (alarms[i] != indices[j2]) continue;
                        alarms[i] = -1;
                        changed = true;
                    }
                }
                for (i = 0; i < alarms.length; ++i) {
                    alarm = alarms[i];
                    for (j = 0; j < indices.length; ++j) {
                        if (alarm <= indices[j]) continue;
                        int[] arrn = alarms;
                        int n = i;
                        arrn[n] = arrn[n] - 1;
                        changed = true;
                    }
                }
                if (changed) {
                    this.alarmTabs.clear();
                    boolean addedSome = false;
                    for (int i2 = 0; i2 < alarms.length; ++i2) {
                        if (alarms[i2] < 0) continue;
                        this.addAlarmTab(alarms[i2]);
                        addedSome = true;
                    }
                    if (!addedSome) {
                        this.stopAlarmTimer();
                    }
                }
            }
        }
        if (this.hasHighlightTabs()) {
            indices = e.getIndices();
            Arrays.sort(indices);
            int[] highlights = (int[])Utilities.toPrimitiveArray((Object[])this.highlightTabs.toArray(new Integer[0]));
            Arrays.sort(highlights);
            if (highlights[highlights.length - 1] >= indices[0]) {
                changed = false;
                for (i = 0; i < highlights.length; ++i) {
                    for (j2 = 0; j2 < indices.length; ++j2) {
                        if (highlights[i] != indices[j2]) continue;
                        highlights[i] = -1;
                        changed = true;
                    }
                }
                for (i = 0; i < highlights.length; ++i) {
                    alarm = highlights[i];
                    for (j = 0; j < indices.length; ++j) {
                        if (alarm <= indices[j]) continue;
                        int[] arrn = highlights;
                        int n = i;
                        arrn[n] = arrn[n] - 1;
                        changed = true;
                    }
                }
                if (changed) {
                    this.highlightTabs.clear();
                    for (i = 0; i < highlights.length; ++i) {
                        if (highlights[i] < 0) continue;
                        this.addHighlightTab(highlights[i]);
                    }
                }
            }
        }
        this.repaintAllTabs();
    }

    void indicesChanged(ComplexListDataEvent e) {
        List<TabData> old;
        int i;
        int idx;
        List<TabData> nue;
        VeryComplexListDataEvent ve;
        boolean changed;
        ArrayDiff dif;
        if (this.hasAlarmTabs() && e instanceof VeryComplexListDataEvent) {
            ve = (VeryComplexListDataEvent)e;
            dif = ((VeryComplexListDataEvent)e).getDiff();
            old = Arrays.asList(dif.getOldData());
            nue = Arrays.asList(dif.getNewData());
            int[] alarms = (int[])Utilities.toPrimitiveArray((Object[])this.alarmTabs.toArray(new Integer[0]));
            changed = false;
            for (i = 0; i < alarms.length; ++i) {
                TabData o = old.get(alarms[i]);
                idx = nue.indexOf(o);
                changed |= idx != alarms[i];
                alarms[i] = nue.indexOf(o);
            }
            if (changed) {
                this.alarmTabs.clear();
                boolean addedSome = false;
                for (int i2 = 0; i2 < alarms.length; ++i2) {
                    if (alarms[i2] < 0) continue;
                    this.addAlarmTab(alarms[i2]);
                    addedSome = true;
                }
                if (!addedSome) {
                    this.stopAlarmTimer();
                }
            }
        }
        if (this.hasHighlightTabs() && e instanceof VeryComplexListDataEvent) {
            ve = (VeryComplexListDataEvent)e;
            dif = ((VeryComplexListDataEvent)e).getDiff();
            old = Arrays.asList(dif.getOldData());
            nue = Arrays.asList(dif.getNewData());
            int[] highlights = (int[])Utilities.toPrimitiveArray((Object[])this.highlightTabs.toArray(new Integer[0]));
            changed = false;
            for (i = 0; i < highlights.length; ++i) {
                TabData o = old.get(highlights[i]);
                idx = nue.indexOf(o);
                changed |= idx != highlights[i];
                highlights[i] = nue.indexOf(o);
            }
            if (changed) {
                this.highlightTabs.clear();
                for (i = 0; i < highlights.length; ++i) {
                    if (highlights[i] < 0) continue;
                    this.addHighlightTab(highlights[i]);
                }
            }
        }
    }

    void contentsChanged(ListDataEvent evt) {
        if (!this.hasAlarmTabs()) {
            return;
        }
    }

    protected void possibleChange(boolean prevVal, boolean currVal, int type) {
        this.lastChangeType = prevVal == currVal ? 0 : Integer.MAX_VALUE;
        if (this.lastChangeType != 0) {
            this.lastAffected = Integer.MAX_VALUE;
            this.change(Integer.MAX_VALUE, Integer.MAX_VALUE, type, this.lastChangeType);
        }
    }

    protected void possibleChange(int lastTab, int currTab, int type) {
        this.lastChangeType = lastTab == currTab ? 0 : (currTab == -1 ? 2 : (lastTab == -1 ? 3 : 1));
        if (this.lastChangeType != 0) {
            this.lastAffected = currTab;
            this.change(lastTab, currTab, type, this.lastChangeType);
        }
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(50);
        sb.append("TabState [lastTab=");
        sb.append(TabState.tabToString(this.prev));
        sb.append(" currTab=");
        sb.append(TabState.tabToString(this.curr));
        sb.append(" lastAffected=");
        sb.append(TabState.tabToString(this.lastAffected));
        sb.append(" lastChangeType=");
        sb.append(TabState.changeToString(this.lastChangeType));
        sb.append(" lastChange=");
        sb.append(TabState.stateToString(this.lastChange));
        sb.append(" <active=");
        sb.append(this.active);
        sb.append(" sel=");
        sb.append(TabState.tabToString(this.selectedIndex));
        sb.append(" mouse=");
        sb.append(TabState.tabToString(this.containsMouseIndex));
        sb.append(" inTabs=");
        sb.append(this.mouseInTabsArea);
        sb.append(" pressed=");
        sb.append(TabState.tabToString(this.pressedIndex));
        sb.append(" inCloseButton=");
        sb.append(TabState.tabToString(this.closeButtonContainsMouseIndex));
        sb.append(" pressedCloseButton=");
        sb.append(TabState.tabToString(this.mousePressedInCloseButtonIndex));
        sb.append(">]");
        return sb.toString();
    }

    protected void change(int lastTab, int currTab, int type, int changeType) {
        this.lastChange = type;
        if (changeType == 1) {
            this.maybeRepaint(lastTab, type);
        } else {
            if (changeType == 2) {
                this.maybeRepaint(lastTab, type);
                return;
            }
            if (changeType == Integer.MAX_VALUE && (this.getRepaintPolicy(currTab) & 3) != 0) {
                this.repaintAllTabs();
                return;
            }
        }
        this.maybeRepaint(currTab, type);
    }

    protected void maybeRepaint(int tab, int type) {
        int rpol = this.getRepaintPolicy(tab);
        boolean go = false;
        switch (type) {
            case 32: {
                boolean bl = go = (rpol & 16) != 0;
                if ((rpol & 32) == 0) break;
                type = Integer.MAX_VALUE;
                go = true;
                break;
            }
            case 4: {
                go = (rpol & 1) != 0 || tab == this.closeButtonContainsMouseIndex;
                this.closeButtonContainsMouseIndex = -1;
                break;
            }
            case 512: {
                go = (rpol & 4) != 0;
                break;
            }
            case 4096: {
                go = (rpol & 3) != 0;
                break;
            }
            case 8192: {
                go = (rpol & 256) != 0;
                break;
            }
            case 8: {
                go = (rpol & 8) != 0;
                break;
            }
            case 16: {
                boolean bl = go = (rpol & 64) != 0;
                if ((rpol & 128) == 0) break;
                type = Integer.MAX_VALUE;
                go = true;
                break;
            }
            case 16384: 
            case 131072: {
                go = true;
            }
        }
        if (go) {
            if (type == Integer.MAX_VALUE) {
                this.repaintAllTabs();
            } else {
                this.repaintTab(tab);
            }
        }
    }

    protected abstract void repaintTab(int var1);

    protected abstract void repaintAllTabs();

    static final String changeToString(int change) {
        switch (change) {
            case 0: {
                return "no change";
            }
            case 1: {
                return "tab to tab";
            }
            case 2: {
                return "tab to none";
            }
            case 3: {
                return "none to tab";
            }
            case 4: {
                return "tab to self";
            }
            case Integer.MAX_VALUE: {
                return "all tabs";
            }
        }
        return "??? " + change;
    }

    static final String tabToString(int tab) {
        if (tab == Integer.MAX_VALUE) {
            return "all tabs";
        }
        if (tab == -1) {
            return "none";
        }
        return Integer.toString(tab);
    }

    static final String stateToString(int st) {
        String[] states = new String[]{"clip right", "clip left", "armed", "pressed", "selected", "active", "not onscreen", "leftmost", "rightmost", "in closebutton", "before selected", "after selected", "mouse in tabs area", "mouse pressed in close button"};
        int[] vals = new int[]{1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192};
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < vals.length; ++i) {
            if ((st & vals[i]) == 0) continue;
            if (sb.length() > 0) {
                sb.append(',');
            }
            sb.append(states[i]);
        }
        if (sb.length() == 0) {
            sb.append("no flags set");
        }
        sb.append("=");
        sb.append(st);
        return sb.toString();
    }

    static String repaintPolicyToString(int policy) {
        if (policy == 0) {
            return "repaint nothing";
        }
        String[] names = new String[]{"REPAINT_ON_MOUSE_ENTER_TAB", "REPAINT_ALL_ON_MOUSE_ENTER_TABS_AREA", "REPAINT_ON_MOUSE_ENTER_CLOSE_BUTTON", "REPAINT_ON_MOUSE_PRESSED", "REPAINT_SELECTION_ON_ACTIVATION_CHANGE", "REPAINT_ALL_TABS_ON_ACTIVATION_CHANGE", "REPAINT_ON_SELECTION_CHANGE", "REPAINT_ALL_TABS_ON_SELECTION_CHANGE", "REPAINT_ON_CLOSE_BUTTON_PRESSED"};
        int[] vals = new int[]{1, 3, 4, 8, 16, 32, 64, 128, 256};
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < vals.length; ++i) {
            if ((policy & vals[i]) == 0) continue;
            sb.append(names[i]);
            if (i == vals.length - 1) continue;
            sb.append('+');
        }
        return sb.toString();
    }

    public abstract int getRepaintPolicy(int var1);

}

