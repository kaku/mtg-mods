/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;
import javax.swing.SingleSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.TabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.BasicScrollingTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.TabLayoutModel;
import org.openide.awt.HtmlRenderer;

final class ViewTabLayoutModel2
implements TabLayoutModel,
ChangeListener {
    private TabDisplayer displayer;
    private PaddingInfo padding;
    private List<Integer> index2Pos;
    private List<Integer> pos2Index;
    private int tabFixedWidth = -1;

    public ViewTabLayoutModel2(TabDisplayer displayer, PaddingInfo padding) {
        this.displayer = displayer;
        this.padding = padding;
        this.updatePermutations();
        displayer.getModel().addChangeListener(this);
    }

    @Override
    public int getH(int index) {
        this.checkIndex(index);
        Insets insets = this.displayer.getInsets();
        return this.displayer.getHeight() - (insets.bottom + insets.top);
    }

    @Override
    public int getY(int index) {
        this.checkIndex(index);
        return this.displayer.getInsets().top;
    }

    @Override
    public int getW(int index) {
        this.checkIndex(index);
        int tabPos = this.index2Pos.get(index);
        return this.getXCoords()[tabPos] - this.getX(index);
    }

    @Override
    public int getX(int index) {
        this.checkIndex(index);
        int tabPos = this.index2Pos.get(index);
        return tabPos > 0 ? this.getXCoords()[tabPos - 1] : this.displayer.getInsets().left;
    }

    @Override
    public int indexOfPoint(int x, int y) {
        Insets insets = this.displayer.getInsets();
        int contentWidth = this.displayer.getWidth() - (insets.left + insets.right);
        int contentHeight = this.displayer.getHeight() - (insets.bottom + insets.top);
        if (y < insets.top || y > contentHeight || x < insets.left || x > contentWidth) {
            return -1;
        }
        int size = this.displayer.getModel().size();
        int[] tabsXCoordinates = this.getXCoords();
        for (int i = 0; i < size; ++i) {
            int diff;
            int leftSide;
            if (tabsXCoordinates[i] <= 0 || (diff = x - (leftSide = i > 0 ? tabsXCoordinates[i - 1] : insets.left)) < 0 || diff >= this.getW(i)) continue;
            return this.pos2Index.get(i);
        }
        return -1;
    }

    @Override
    public int dropIndexOfPoint(int x, int y) {
        Insets insets = this.displayer.getInsets();
        int contentWidth = this.displayer.getWidth() - (insets.left + insets.right);
        int contentHeight = this.displayer.getHeight() - (insets.bottom + insets.top);
        if (y < insets.top || y > contentHeight || x < insets.left || x > contentWidth) {
            return -1;
        }
        throw new UnsupportedOperationException("not implemenetd yet....");
    }

    @Override
    public void setPadding(Dimension d) {
    }

    private void checkIndex(int index) {
        int size = this.displayer.getModel().size();
        if (index < 0 || index >= size) {
            throw new IllegalArgumentException("Index out of valid scope 0.." + (size - 1) + ": " + index);
        }
    }

    private int[] getXCoords() {
        TabDataModel model = this.displayer.getModel();
        int size = model.size();
        int[] tabsXCoord = new int[size];
        if (this.tabFixedWidth < 0) {
            this.tabFixedWidth = this.padding.txtPad.width + this.padding.txtIconsXPad + this.padding.iconsXPad;
        }
        Insets dispInsets = this.displayer.getInsets();
        double curX = dispInsets.left;
        int maxRight = this.displayer.getWidth() - dispInsets.right;
        for (int i = 0; i < size; ++i) {
            int tabIndex = this.pos2Index.get(i);
            String curText = model.getTab(tabIndex).getText();
            curX += HtmlRenderer.renderString((String)curText, (Graphics)BasicScrollingTabDisplayerUI.getOffscreenGraphics(), (int)0, (int)0, (int)Integer.MAX_VALUE, (int)Integer.MAX_VALUE, (Font)this.displayer.getFont(), (Color)Color.BLACK, (int)1, (boolean)false) + (double)this.tabFixedWidth;
            if (tabIndex == this.displayer.getSelectionModel().getSelectedIndex()) {
                Icon buttonIcon = this.displayer.getUI().getButtonIcon(1, tabIndex);
                if (buttonIcon != null) {
                    curX += (double)(buttonIcon.getIconWidth() + this.padding.iconsXPad);
                }
                if ((buttonIcon = this.displayer.getUI().getButtonIcon(2, tabIndex)) != null) {
                    curX += (double)(buttonIcon.getIconWidth() + this.padding.iconsXPad);
                }
            }
            tabsXCoord[i] = Math.round(Math.round(curX));
            if (curX > (double)maxRight) break;
        }
        return tabsXCoord;
    }

    private void updatePermutations() {
        int itemCount = this.displayer.getModel().size();
        this.index2Pos = new ArrayList<Integer>(itemCount);
        this.pos2Index = new ArrayList<Integer>(itemCount);
        for (int i = 0; i < itemCount; ++i) {
            this.index2Pos.add(itemCount - i - 1);
            this.pos2Index.add(0, i);
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        this.updatePermutations();
    }

    static final class PaddingInfo {
        Dimension txtPad;
        int txtIconsXPad;
        int iconsXPad;

        PaddingInfo() {
        }
    }

}

