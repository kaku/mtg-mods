/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.netbeans.swing.tabcontrol.plaf.ColorUtil;
import org.netbeans.swing.tabcontrol.plaf.GenericGlowingChiclet;

public class ChicletWrapper
implements Runnable {
    private boolean allowVertical = true;
    private boolean leftNotch = false;
    private boolean rightNotch = false;
    private int state = 0;
    private Rectangle bounds = new Rectangle();
    private float[] arcs = new float[4];
    GenericGlowingChiclet chiclet = GenericGlowingChiclet.INSTANCE;
    static int drawCount = 0;
    private static HashMap<CacheEntry, BufferedImage> cache = new HashMap();

    public void setState(int state) {
        this.state = state;
    }

    public void setBounds(int x, int y, int w, int h) {
        this.bounds.setBounds(x, y, w, h);
    }

    public void draw(Graphics g) {
        if (this.bounds.width == 0 || this.bounds.height == 0) {
            return;
        }
        BufferedImage img = this.findBufferedImage();
        ((Graphics2D)g).drawRenderedImage(img, AffineTransform.getTranslateInstance(0.0, 0.0));
        if (++drawCount % 100 == 0) {
            SwingUtilities.invokeLater(this);
        }
    }

    public void setArcs(float a, float b, float c, float d) {
        this.arcs[0] = a;
        this.arcs[1] = b;
        this.arcs[2] = c;
        this.arcs[3] = d;
    }

    public void setAllowVertical(boolean b) {
        this.allowVertical = b;
    }

    public void setNotch(boolean right, boolean left) {
        this.leftNotch = left;
        this.rightNotch = right;
    }

    public Long hash() {
        long result = (long)(this.state * 701) + Double.doubleToLongBits(this.arcs[0]) * 31 + Double.doubleToLongBits(this.arcs[1]) * 37 + Double.doubleToLongBits(this.arcs[2]) * 43 + Double.doubleToLongBits(this.arcs[3]) * 47 + (long)(this.bounds.width * 6703) + (long)(this.bounds.height * 1783);
        if (this.leftNotch) {
            result *= 3121;
        }
        if (this.rightNotch) {
            result *= 4817;
        }
        if (this.allowVertical) {
            result *= 1951;
        }
        return new Long(result);
    }

    private BufferedImage findBufferedImage() {
        Long hash = this.hash();
        CacheEntry entry = new CacheEntry(hash);
        BufferedImage result = cache.get(entry);
        if (result == null) {
            result = this.createImage();
        }
        cache.put(entry, result);
        return result;
    }

    private BufferedImage createImage() {
        BufferedImage img = new BufferedImage(this.bounds.width, this.bounds.height, 3);
        this.chiclet.setNotch(this.rightNotch, this.leftNotch);
        this.chiclet.setArcs(this.arcs[0], this.arcs[1], this.arcs[2], this.arcs[3]);
        this.chiclet.setBounds(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height);
        this.chiclet.setAllowVertical(this.allowVertical);
        this.chiclet.setState(this.state);
        Graphics g = img.getGraphics();
        g.translate(- this.bounds.x, - this.bounds.y);
        ColorUtil.setupAntialiasing(g);
        this.chiclet.draw((Graphics2D)g);
        g.translate(this.bounds.x, this.bounds.y);
        return img;
    }

    @Override
    public void run() {
        if (cache.size() < 5) {
            return;
        }
        HashMap<CacheEntry, BufferedImage> newCache = new HashMap<CacheEntry, BufferedImage>(cache);
        long startTime = System.currentTimeMillis();
        Object[] entries = newCache.keySet().toArray(new CacheEntry[0]);
        Arrays.sort(entries);
        for (int i = entries.length - 1; i >= entries.length / 3; --i) {
            if (startTime - entries[i].timestamp <= 240000) continue;
            newCache.remove(entries[i]);
        }
        cache = newCache;
    }

    private static final class CacheEntry
    implements Comparable {
        private final Long hash;
        long timestamp = System.currentTimeMillis();

        public CacheEntry(Long hash) {
            this.hash = hash;
        }

        public boolean equals(Object o) {
            if (o instanceof CacheEntry) {
                CacheEntry other = (CacheEntry)o;
                return other.hash() == this.hash();
            }
            if (o instanceof Long) {
                return ((Long)o).longValue() == this.hash();
            }
            return false;
        }

        long hash() {
            return this.hash;
        }

        public int hashCode() {
            return this.hash.intValue();
        }

        public int compareTo(Object o) {
            CacheEntry other = (CacheEntry)o;
            return (int)(this.timestamp - other.timestamp);
        }

        public String toString() {
            return "CacheEntry: " + new Date(this.timestamp) + " hash " + this.hash();
        }
    }

}

