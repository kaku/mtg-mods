/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JComponent;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.plaf.BasicScrollingTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.ScrollingTabLayoutModel;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.netbeans.swing.tabcontrol.plaf.WinVistaEditorTabCellRenderer;

abstract class AbstractWinEditorTabDisplayerUI
extends BasicScrollingTabDisplayerUI {
    private static final Rectangle scratch5 = new Rectangle();
    private static Map<Integer, String[]> buttonIconPaths;

    public AbstractWinEditorTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        int prefHeight = 22;
        Graphics2D g = BasicScrollingTabDisplayerUI.getOffscreenGraphics();
        if (g != null) {
            FontMetrics fm = g.getFontMetrics(this.displayer.getFont());
            Insets ins = this.getTabAreaInsets();
            prefHeight = fm.getHeight() + ins.top + ins.bottom + 6;
        }
        return new Dimension(this.displayer.getWidth(), prefHeight);
    }

    @Override
    public void paintBackground(Graphics g) {
        g.setColor(this.displayer.getBackground());
        g.fillRect(0, 0, this.displayer.getWidth(), this.displayer.getHeight());
    }

    @Override
    protected void paintAfterTabs(Graphics g) {
        Rectangle r = new Rectangle();
        this.getTabsVisibleArea(r);
        r.width = this.displayer.getWidth();
        Insets ins = this.getTabAreaInsets();
        int y = this.displayer.getHeight() - 0;
        int tabsWidth = this.getTabsAreaWidth();
        g.setColor(WinVistaEditorTabCellRenderer.getBorderColor());
        int last = this.scroll().getLastVisibleTab(tabsWidth);
        boolean l = false;
        if (last >= 0) {
            this.getTabRect(last, scratch5);
            last = AbstractWinEditorTabDisplayerUI.scratch5.x + AbstractWinEditorTabDisplayerUI.scratch5.width;
        }
        g.drawLine(last, y - 1, this.displayer.getWidth(), y - 1);
    }

    @Override
    protected Rectangle getTabRectForRepaint(int tab, Rectangle rect) {
        Rectangle res = super.getTabRectForRepaint(tab, rect);
        --res.x;
        res.width += 2;
        return res;
    }

    private static void initIcons() {
        if (null == buttonIconPaths) {
            buttonIconPaths = new HashMap<Integer, String[]>(7);
            String[] iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/vista_scrollleft_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/vista_scrollleft_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/vista_scrollleft_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/vista_scrollleft_pressed.png";
            buttonIconPaths.put(9, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/vista_scrollright_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/vista_scrollright_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/vista_scrollright_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/vista_scrollright_pressed.png";
            buttonIconPaths.put(10, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/vista_popup_enabled.png";
            iconPaths[2] = iconPaths[0];
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/vista_popup_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/vista_popup_pressed.png";
            buttonIconPaths.put(8, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/vista_maximize_enabled.png";
            iconPaths[2] = iconPaths[0];
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/vista_maximize_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/vista_maximize_pressed.png";
            buttonIconPaths.put(3, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/vista_restore_enabled.png";
            iconPaths[2] = iconPaths[0];
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/vista_restore_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/vista_restore_pressed.png";
            buttonIconPaths.put(4, iconPaths);
        }
    }

    @Override
    public Icon getButtonIcon(int buttonId, int buttonState) {
        Icon res = null;
        AbstractWinEditorTabDisplayerUI.initIcons();
        String[] paths = buttonIconPaths.get(buttonId);
        if (null != paths && buttonState >= 0 && buttonState < paths.length) {
            res = TabControlButtonFactory.getIcon(paths[buttonState]);
        }
        return res;
    }
}

