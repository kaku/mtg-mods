/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.UIManager;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.event.TabActionEvent;
import org.netbeans.swing.tabcontrol.plaf.AbstractViewTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.BusyTabsSupport;
import org.netbeans.swing.tabcontrol.plaf.ColorUtil;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.netbeans.swing.tabcontrol.plaf.TabLayoutModel;
import org.openide.awt.HtmlRenderer;

abstract class AbstractWinViewTabDisplayerUI
extends AbstractViewTabDisplayerUI {
    private final int TXT_X_PAD;
    private static final int TXT_Y_PAD = 3;
    private static final int ICON_X_PAD = 4;
    private static final int BUMP_X_PAD = 3;
    private static final int BUMP_Y_PAD_UPPER = 6;
    private static final int BUMP_Y_PAD_BOTTOM = 3;
    private static boolean colorsReady = false;
    private static Color unselFillBrightUpperC;
    private static Color unselFillDarkUpperC;
    private static Color unselFillBrightLowerC;
    private static Color unselFillDarkLowerC;
    private static Color selFillC;
    private static Color focusFillUpperC;
    private static Color focusFillBrightLowerC;
    private static Color focusFillDarkLowerC;
    private static Color mouseOverFillBrightUpperC;
    private static Color mouseOverFillDarkUpperC;
    private static Color mouseOverFillBrightLowerC;
    private static Color mouseOverFillDarkLowerC;
    private static Color txtC;
    private static Color borderC;
    private static Color selBorderC;
    private static Color borderInnerC;
    private static Map<Integer, String[]> buttonIconPaths;
    private Dimension prefSize;

    AbstractWinViewTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
        this.TXT_X_PAD = this.isUseStretchingTabs() ? 9 : 4;
        this.prefSize = new Dimension(100, 17);
    }

    @Override
    public void installUI(JComponent c) {
        super.installUI(c);
        AbstractWinViewTabDisplayerUI.initColors();
        c.setOpaque(true);
        this.getLayoutModel().setPadding(new Dimension(2 * this.TXT_X_PAD, 0));
    }

    @Override
    protected AbstractViewTabDisplayerUI.Controller createController() {
        return new OwnController();
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        FontMetrics fm = this.getTxtFontMetrics();
        int height = fm == null ? 17 : fm.getAscent() + 2 * fm.getDescent() + 3;
        Insets insets = c.getInsets();
        this.prefSize.height = height + insets.bottom + insets.top;
        return this.prefSize;
    }

    @Override
    protected void paintTabContent(Graphics g, int index, String text, int x, int y, int width, int height) {
        FontMetrics fm = this.getTxtFontMetrics();
        g.setFont(this.getTxtFont());
        if (0 == index) {
            ++x;
        }
        int txtWidth = width;
        if (this.isSelected(index)) {
            Component buttons = this.getControlButtons();
            if (null != buttons) {
                Dimension buttonsSize = buttons.getPreferredSize();
                if (width < buttonsSize.width + 4) {
                    buttons.setVisible(false);
                } else {
                    buttons.setVisible(true);
                    txtWidth = width - (buttonsSize.width + 4 + 2 * this.TXT_X_PAD);
                    buttons.setLocation(x + txtWidth + 2 * this.TXT_X_PAD, y + (height - buttonsSize.height) / 2 + this.getButtonYPadding());
                }
            }
        } else {
            txtWidth = width - 2 * this.TXT_X_PAD;
        }
        if (this.isUseStretchingTabs()) {
            ColorUtil.paintVistaTabDragTexture(this.getDisplayer(), g, x + 3, y + 6, height - 9);
        }
        boolean slidedOut = false;
        WinsysInfoForTabbedContainer winsysInfo = this.displayer.getContainerWinsysInfo();
        if (null != winsysInfo && winsysInfo.isSlidedOutContainer()) {
            slidedOut = false;
        }
        if (this.isTabBusy(index) && !slidedOut) {
            Icon busyIcon = BusyTabsSupport.getDefault().getBusyIcon(this.isSelected(index));
            txtWidth -= busyIcon.getIconWidth() - 3 - this.TXT_X_PAD;
            busyIcon.paintIcon(this.displayer, g, x + this.TXT_X_PAD, y + (height - busyIcon.getIconHeight()) / 2);
            x += busyIcon.getIconWidth() + 3;
        }
        HtmlRenderer.renderString((String)text, (Graphics)g, (int)(x + this.TXT_X_PAD), (int)(y + fm.getAscent() + 3), (int)txtWidth, (int)height, (Font)this.getTxtFont(), (Color)txtC, (int)1, (boolean)true);
    }

    @Override
    protected void paintTabBorder(Graphics g, int index, int x, int y, int width, int height) {
        boolean isFirst = index == 0 || index < 0;
        boolean isHighlighted = this.isTabHighlighted(index);
        g.translate(x, y);
        Color borderColor = isHighlighted ? selBorderC : borderC;
        g.setColor(borderColor);
        boolean left = false;
        if (isFirst) {
            g.drawLine(0, 0, 0, height - 2);
        }
        g.drawLine(0, 0, width - 1, 0);
        if (index < this.getDataModel().size() - 1 && this.isTabHighlighted(index + 1)) {
            g.setColor(selBorderC);
        }
        g.drawLine(width - 1, 0, width - 1, height - 2);
        g.setColor(borderC);
        g.drawLine(0, height - 1, width - 1, height - 1);
        g.setColor(borderInnerC);
        if (isFirst) {
            g.drawLine(1, 1, 1, height - 2);
        } else {
            g.drawLine(0, 1, 0, height - 2);
        }
        g.drawLine(width - 2, 1, width - 2, height - 2);
        g.drawLine(1, 1, width - 2, 1);
        g.translate(- x, - y);
    }

    @Override
    protected void paintTabBackground(Graphics g, int index, int x, int y, int width, int height) {
        y += 2;
        height -= 2;
        boolean selected = this.isSelected(index);
        boolean focused = selected && this.isActive();
        boolean attention = this.isAttention(index);
        boolean mouseOver = this.isMouseOver(index);
        if (focused && !attention) {
            ColorUtil.vistaFillRectGradient((Graphics2D)g, x, y, width, height, focusFillUpperC, focusFillBrightLowerC, focusFillDarkLowerC);
        } else if (selected && !attention) {
            g.setColor(selFillC);
            g.fillRect(x, y, width, height);
        } else if (mouseOver && !attention) {
            ColorUtil.vistaFillRectGradient((Graphics2D)g, x, y, width, height, mouseOverFillBrightUpperC, mouseOverFillDarkUpperC, mouseOverFillBrightLowerC, mouseOverFillDarkLowerC);
        } else if (attention) {
            Color a = new Color(255, 255, 128);
            Color b = new Color(230, 200, 64);
            ColorUtil.xpFillRectGradient((Graphics2D)g, x, y, width, height, a, b);
        } else {
            ColorUtil.vistaFillRectGradient((Graphics2D)g, x, y, width, height, unselFillBrightUpperC, unselFillDarkUpperC, unselFillBrightLowerC, unselFillDarkLowerC);
        }
    }

    @Override
    protected Font getTxtFont() {
        Font font = super.getTxtFont();
        if (!font.isBold()) {
            font = font.deriveFont(1);
        }
        return font;
    }

    private boolean isTabHighlighted(int index) {
        if (index < 0) {
            return false;
        }
        if (((OwnController)this.getController()).getMouseIndex() == index) {
            return true;
        }
        return this.isSelected(index) && this.isActive();
    }

    boolean isMouseOver(int index) {
        if (index < 0) {
            return false;
        }
        return ((OwnController)this.getController()).getMouseIndex() == index && !this.isSelected(index);
    }

    int getButtonYPadding() {
        return 0;
    }

    private static void initColors() {
        if (!colorsReady) {
            txtC = UIManager.getColor("TabbedPane.foreground");
            selFillC = UIManager.getColor("tab_sel_fill");
            focusFillUpperC = UIManager.getColor("tab_focus_fill_upper");
            focusFillBrightLowerC = UIManager.getColor("tab_focus_fill_bright_lower");
            focusFillDarkLowerC = UIManager.getColor("tab_focus_fill_dark_lower");
            unselFillBrightUpperC = UIManager.getColor("tab_unsel_fill_bright_upper");
            unselFillDarkUpperC = UIManager.getColor("tab_unsel_fill_dark_upper");
            unselFillBrightLowerC = UIManager.getColor("tab_unsel_fill_bright_lower");
            unselFillDarkLowerC = UIManager.getColor("tab_unsel_fill_dark_lower");
            mouseOverFillBrightUpperC = UIManager.getColor("tab_mouse_over_fill_bright_upper");
            mouseOverFillDarkUpperC = UIManager.getColor("tab_mouse_over_fill_dark_upper");
            mouseOverFillBrightLowerC = UIManager.getColor("tab_mouse_over_fill_bright_lower");
            mouseOverFillDarkLowerC = UIManager.getColor("tab_mouse_over_fill_dark_lower");
            borderC = UIManager.getColor("tab_border");
            selBorderC = UIManager.getColor("tab_sel_border");
            borderInnerC = UIManager.getColor("tab_border_inner");
            colorsReady = true;
        }
    }

    private static void initIcons() {
        if (null == buttonIconPaths) {
            String[] iconPaths;
            buttonIconPaths = new HashMap<Integer, String[]>(7);
            iconPaths = new String[]{"org/openide/awt/resources/vista_bigclose_enabled.png", "org/openide/awt/resources/vista_bigclose_pressed.png", iconPaths[0], "org/openide/awt/resources/vista_bigclose_rollover.png"};
            buttonIconPaths.put(1, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/vista_slideright_enabled.png", "org/netbeans/swing/tabcontrol/resources/vista_slideright_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/vista_slideright_rollover.png"};
            buttonIconPaths.put(6, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/vista_slideleft_enabled.png", "org/netbeans/swing/tabcontrol/resources/vista_slideleft_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/vista_slideleft_rollover.png"};
            buttonIconPaths.put(5, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/vista_slidebottom_enabled.png", "org/netbeans/swing/tabcontrol/resources/vista_slidebottom_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/vista_slidebottom_rollover.png"};
            buttonIconPaths.put(7, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/vista_pin_enabled.png", "org/netbeans/swing/tabcontrol/resources/vista_pin_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/vista_pin_rollover.png"};
            buttonIconPaths.put(2, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/vista_restore_group_enabled.png", "org/netbeans/swing/tabcontrol/resources/vista_restore_group_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/vista_restore_group_rollover.png"};
            buttonIconPaths.put(11, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/vista_minimize_enabled.png", "org/netbeans/swing/tabcontrol/resources/vista_minimize_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/vista_minimize_rollover.png"};
            buttonIconPaths.put(12, iconPaths);
        }
    }

    @Override
    public Icon getButtonIcon(int buttonId, int buttonState) {
        Icon res = null;
        AbstractWinViewTabDisplayerUI.initIcons();
        String[] paths = buttonIconPaths.get(buttonId);
        if (null != paths && buttonState >= 0 && buttonState < paths.length) {
            res = TabControlButtonFactory.getIcon(paths[buttonState]);
        }
        return res;
    }

    @Override
    public void postTabAction(TabActionEvent e) {
        super.postTabAction(e);
        if ("maximize".equals(e.getActionCommand())) {
            ((OwnController)this.getController()).updateHighlight(-1);
        }
    }

    private class OwnController
    extends AbstractViewTabDisplayerUI.Controller {
        private int lastIndex;

        private OwnController() {
            this.lastIndex = -1;
        }

        public int getMouseIndex() {
            return this.lastIndex;
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            super.mouseMoved(e);
            Point pos = e.getPoint();
            this.updateHighlight(AbstractWinViewTabDisplayerUI.this.getLayoutModel().indexOfPoint(pos.x, pos.y));
        }

        @Override
        public void mouseExited(MouseEvent e) {
            super.mouseExited(e);
            if (!this.inControlButtonsRect(e.getPoint())) {
                this.updateHighlight(-1);
            }
        }

        private void updateHighlight(int curIndex) {
            int y;
            int h;
            int w;
            int x;
            if (curIndex == this.lastIndex) {
                return;
            }
            TabLayoutModel tlm = AbstractWinViewTabDisplayerUI.this.getLayoutModel();
            Rectangle repaintRect = null;
            if (curIndex != -1) {
                x = tlm.getX(curIndex) - 1;
                y = tlm.getY(curIndex);
                w = tlm.getW(curIndex) + 2;
                h = tlm.getH(curIndex);
                repaintRect = new Rectangle(x, y, w, h);
            }
            if (this.lastIndex != -1 && this.lastIndex < AbstractWinViewTabDisplayerUI.this.getDataModel().size()) {
                x = tlm.getX(this.lastIndex) - 1;
                y = tlm.getY(this.lastIndex);
                w = tlm.getW(this.lastIndex) + 2;
                h = tlm.getH(this.lastIndex);
                repaintRect = repaintRect != null ? repaintRect.union(new Rectangle(x, y, w, h)) : new Rectangle(x, y, w, h);
            }
            if (repaintRect != null) {
                AbstractWinViewTabDisplayerUI.this.getDisplayer().repaint(repaintRect);
            }
            this.lastIndex = curIndex;
        }
    }

}

