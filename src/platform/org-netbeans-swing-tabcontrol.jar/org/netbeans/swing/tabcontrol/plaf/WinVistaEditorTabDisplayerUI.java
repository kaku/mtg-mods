/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.plaf.AbstractWinEditorTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.TabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.WinVistaEditorTabCellRenderer;

public final class WinVistaEditorTabDisplayerUI
extends AbstractWinEditorTabDisplayerUI {
    public WinVistaEditorTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
    }

    public static ComponentUI createUI(JComponent c) {
        return new WinVistaEditorTabDisplayerUI((TabDisplayer)c);
    }

    @Override
    protected TabCellRenderer createDefaultRenderer() {
        return new WinVistaEditorTabCellRenderer();
    }
}

