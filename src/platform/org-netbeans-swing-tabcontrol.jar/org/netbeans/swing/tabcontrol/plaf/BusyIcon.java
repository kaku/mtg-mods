/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.ImageObserver;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.UIManager;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;

abstract class BusyIcon
implements Icon {
    protected final int width;
    protected final int height;

    protected BusyIcon(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public static BusyIcon create(boolean selectedTab) {
        BusyIcon res = null;
        Icon img = UIManager.getIcon("nb.tabcontrol.busy.icon." + (selectedTab ? "selected" : "normal"));
        res = null != img ? new ImageBusyIcon(ImageUtilities.icon2Image((Icon)img)) : SwingXBusyIcon.create();
        if (null == res) {
            res = new ImageBusyIcon(ImageUtilities.loadImage((String)"org/netbeans/swing/tabcontrol/resources/busy_icon.png"));
        }
        return res;
    }

    abstract void tick();

    @Override
    public final int getIconWidth() {
        return this.width;
    }

    @Override
    public final int getIconHeight() {
        return this.height;
    }

    private static int getBusyIconSize() {
        int res = UIManager.getInt("Nb.BusyIcon.Height");
        if (res < 1) {
            res = 16;
        }
        return res;
    }

    private static class SwingXBusyIcon
    extends BusyIcon {
        private final Object painter;
        private final Method setFrameMethod;
        private final Method paintMethod;
        private int currentFrame = 0;
        private static final int POINTS = 8;

        private SwingXBusyIcon(Object painter, Method paint, Method setFrame, int height) {
            super(height, height);
            this.painter = painter;
            this.setFrameMethod = setFrame;
            this.paintMethod = paint;
        }

        public static BusyIcon create() {
            Object painter = null;
            ClassLoader cl = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
            try {
                Class painterClass = cl.loadClass("org.jdesktop.swingx.painter.BusyPainter");
                Constructor ctor = painterClass.getConstructor(Integer.TYPE);
                int height = BusyIcon.getBusyIconSize();
                painter = ctor.newInstance(height);
                Method setFrame = painterClass.getMethod("setFrame", Integer.TYPE);
                Method paint = painterClass.getMethod("paint", Graphics2D.class, Object.class, Integer.TYPE, Integer.TYPE);
                Method m = painterClass.getMethod("setPoints", Integer.TYPE);
                m.invoke(painter, 8);
                return new SwingXBusyIcon(painter, paint, setFrame, height);
            }
            catch (Exception ex) {
                Logger.getLogger(BusyIcon.class.getName()).log(Level.FINE, null, ex);
                return null;
            }
        }

        @Override
        public void tick() {
            this.currentFrame = (this.currentFrame + 1) % 8;
            try {
                this.setFrameMethod.invoke(this.painter, this.currentFrame);
            }
            catch (Exception ex) {
                // empty catch block
            }
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            if (g instanceof Graphics2D) {
                Graphics2D g2d = (Graphics2D)g;
                try {
                    g2d.translate(x, y);
                    this.paintMethod.invoke(this.painter, g, c, x, y);
                }
                catch (Exception ex) {
                    Logger.getLogger(BusyIcon.class.getName()).log(Level.FINE, null, ex);
                }
                g2d.translate(- x, - y);
            }
        }
    }

    private static class ImageBusyIcon
    extends BusyIcon {
        private final Image img;
        private int state = 0;
        private AffineTransform at;
        private static final int STEP = 15;

        public ImageBusyIcon(Image img) {
            super(img.getWidth(null), img.getHeight(null));
            this.img = img;
        }

        @Override
        void tick() {
            this.state += 15;
            if (this.state >= 360) {
                this.state = 0;
            }
            this.at = new AffineTransform();
            this.at.rotate((double)this.state * 3.141592653589793 / 180.0, this.width / 2, this.height / 2);
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            if (g instanceof Graphics2D) {
                Graphics2D g2d = (Graphics2D)g;
                g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
                g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
                g2d.translate(x, y);
                g2d.drawImage(this.img, this.at, null);
                g2d.translate(- x, - y);
            }
        }
    }

}

