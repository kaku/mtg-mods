/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.image.RescaleOp;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.plaf.synth.Region;
import javax.swing.plaf.synth.SynthContext;
import javax.swing.plaf.synth.SynthLookAndFeel;
import javax.swing.plaf.synth.SynthPainter;
import javax.swing.plaf.synth.SynthStyle;
import javax.swing.plaf.synth.SynthStyleFactory;
import org.netbeans.swing.tabcontrol.plaf.AbstractTabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.BusyTabsSupport;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.netbeans.swing.tabcontrol.plaf.TabPainter;

final class GtkEditorTabCellRenderer
extends AbstractTabCellRenderer {
    private static final TabPainter leftClip = new GtkLeftClipPainter();
    private static final TabPainter rightClip = new GtkRightClipPainter();
    private static final TabPainter normal = new GtkPainter();
    private static JTabbedPane dummyTab;
    static final Color ATTENTION_COLOR;
    private static final Logger LOG;
    private static final Insets INSETS;

    public GtkEditorTabCellRenderer() {
        super(leftClip, normal, rightClip, new Dimension(28, 32));
    }

    @Override
    public Color getSelectedForeground() {
        return UIManager.getColor("textText");
    }

    @Override
    public Color getForeground() {
        return this.getSelectedForeground();
    }

    @Override
    protected void paintIconAndText(Graphics g) {
        if (this.isBusy()) {
            this.setIcon(BusyTabsSupport.getDefault().getBusyIcon(this.isSelected()));
        }
        super.paintIconAndText(g);
    }

    @Override
    public int getPixelsToAddToSelection() {
        return 4;
    }

    @Override
    protected int getCaptionYAdjustment() {
        return 0;
    }

    @Override
    public Dimension getPadding() {
        Dimension d = super.getPadding();
        d.width = this.isShowCloseButton() && !Boolean.getBoolean("nb.tabs.suppressCloseButton") ? 28 : 14;
        return d;
    }

    private static void paintTabBackground(Graphics g, int index, int state, int x, int y, int w, int h) {
        if (dummyTab == null) {
            dummyTab = new JTabbedPane();
        }
        Region region = Region.TABBED_PANE_TAB;
        if (!(UIManager.getLookAndFeel() instanceof SynthLookAndFeel)) {
            return;
        }
        SynthLookAndFeel laf = (SynthLookAndFeel)UIManager.getLookAndFeel();
        SynthStyleFactory sf = SynthLookAndFeel.getStyleFactory();
        SynthStyle style = sf.getStyle(dummyTab, region);
        SynthContext context = new SynthContext(dummyTab, region, style, state == 256 ? 512 : state);
        SynthPainter painter = style.getPainter(context);
        if (state == 1024) {
            long t1 = System.currentTimeMillis();
            painter.paintTabbedPaneTabBackground(context, g, x, y, w, h, index);
            long t2 = System.currentTimeMillis();
            if (t2 - t1 > 200) {
                LOG.log(Level.WARNING, "painter.paintTabbedPaneTabBackground1 takes too long x=" + x + " y=" + y + " w=" + w + " h=" + h + " index:" + index + " Time=" + (t2 - t1));
            }
        } else {
            BufferedImage bufIm = new BufferedImage(w, h, 1);
            Graphics2D g2d = bufIm.createGraphics();
            g2d.setBackground(UIManager.getColor("Panel.background"));
            g2d.clearRect(0, 0, w, h);
            long t1 = System.currentTimeMillis();
            painter.paintTabbedPaneTabBackground(context, g2d, 0, 0, w, h, index);
            long t2 = System.currentTimeMillis();
            if (t2 - t1 > 200) {
                LOG.log(Level.WARNING, "painter.paintTabbedPaneTabBackground1 takes too long x=0 y=0 w=" + w + " h=" + h + " index:" + index + " Time=" + (t2 - t1));
            }
            RescaleOp op = state == 256 ? new RescaleOp(1.08f, 0.0f, null) : new RescaleOp(0.96f, 0.0f, null);
            BufferedImage img = op.filter(bufIm, null);
            g.drawImage(img, x, y, null);
        }
    }

    private static int getHeightDifference(GtkEditorTabCellRenderer ren) {
        return ren.isSelected() ? (ren.isActive() ? 0 : 1) : 2;
    }

    static {
        ATTENTION_COLOR = new Color(255, 238, 120);
        LOG = Logger.getLogger("org.netbeans.swing.tabcontrol.plaf.GtkEditorTabCellRenderer");
        INSETS = new Insets(0, 2, 0, 10);
    }

    private static class GtkRightClipPainter
    implements TabPainter {
        private GtkRightClipPainter() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return INSETS;
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        @Override
        public Polygon getInteriorPolygon(Component c) {
            GtkEditorTabCellRenderer ren = (GtkEditorTabCellRenderer)c;
            Insets ins = this.getBorderInsets(c);
            Polygon p = new Polygon();
            int x = 0;
            int y = 1;
            int width = c.getWidth() + 10;
            int height = ren.isSelected() ? c.getHeight() + 2 : c.getHeight() - 1;
            p.addPoint(x, y);
            p.addPoint(x + width, y);
            p.addPoint(x + width, y + height);
            p.addPoint(x, y + height);
            return p;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        }

        @Override
        public void paintInterior(Graphics g, Component c) {
            GtkEditorTabCellRenderer ren = (GtkEditorTabCellRenderer)c;
            Polygon p = this.getInteriorPolygon(c);
            int state = ren.isSelected() ? (ren.isActive() ? 256 : 512) : 1024;
            Rectangle bounds = p.getBounds();
            int yDiff = GtkEditorTabCellRenderer.getHeightDifference(ren);
            GtkEditorTabCellRenderer.paintTabBackground(g, 0, state, bounds.x, bounds.y + yDiff, bounds.width, bounds.height - yDiff);
        }

        @Override
        public boolean supportsCloseButton(JComponent renderer) {
            return false;
        }

        @Override
        public void getCloseButtonRectangle(JComponent jc, Rectangle rect, Rectangle bounds) {
            rect.setBounds(-20, -20, 0, 0);
        }
    }

    private static class GtkLeftClipPainter
    implements TabPainter {
        private GtkLeftClipPainter() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return INSETS;
        }

        @Override
        public Polygon getInteriorPolygon(Component c) {
            GtkEditorTabCellRenderer ren = (GtkEditorTabCellRenderer)c;
            Insets ins = this.getBorderInsets(c);
            Polygon p = new Polygon();
            int x = -3;
            int y = 1;
            int width = c.getWidth() + 3;
            int height = ren.isSelected() ? c.getHeight() + 2 : c.getHeight() - 1;
            p.addPoint(x, y);
            p.addPoint(x + width, y);
            p.addPoint(x + width, y + height);
            p.addPoint(x, y + height);
            return p;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        }

        @Override
        public void paintInterior(Graphics g, Component c) {
            GtkEditorTabCellRenderer ren = (GtkEditorTabCellRenderer)c;
            Polygon p = this.getInteriorPolygon(c);
            int state = ren.isSelected() ? (ren.isActive() ? 256 : 512) : 1024;
            Rectangle bounds = p.getBounds();
            int yDiff = GtkEditorTabCellRenderer.getHeightDifference(ren);
            GtkEditorTabCellRenderer.paintTabBackground(g, 0, state, bounds.x, bounds.y + yDiff, bounds.width, bounds.height - yDiff);
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        @Override
        public void getCloseButtonRectangle(JComponent jc, Rectangle rect, Rectangle bounds) {
            rect.setBounds(-20, -20, 0, 0);
        }

        @Override
        public boolean supportsCloseButton(JComponent renderer) {
            return false;
        }
    }

    private static class GtkPainter
    implements TabPainter {
        private GtkPainter() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return INSETS;
        }

        @Override
        public Polygon getInteriorPolygon(Component c) {
            GtkEditorTabCellRenderer ren = (GtkEditorTabCellRenderer)c;
            Insets ins = this.getBorderInsets(c);
            Polygon p = new Polygon();
            int x = ren.isLeftmost() ? 1 : 0;
            int y = 1;
            int width = ren.isLeftmost() ? c.getWidth() - 1 : c.getWidth();
            int height = ren.isSelected() ? c.getHeight() + 2 : c.getHeight() - 1;
            p.addPoint(x, y);
            p.addPoint(x + width, y);
            p.addPoint(x + width, y + height);
            p.addPoint(x, y + height);
            return p;
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            GtkEditorTabCellRenderer ren = (GtkEditorTabCellRenderer)c;
            Polygon p = this.getInteriorPolygon(c);
        }

        @Override
        public void paintInterior(Graphics g, Component c) {
            GtkEditorTabCellRenderer ren = (GtkEditorTabCellRenderer)c;
            Polygon p = this.getInteriorPolygon(c);
            int state = ren.isSelected() ? (ren.isActive() ? 256 : 512) : 1024;
            Rectangle bounds = p.getBounds();
            int yDiff = GtkEditorTabCellRenderer.getHeightDifference(ren);
            GtkEditorTabCellRenderer.paintTabBackground(g, 0, state, bounds.x, bounds.y + yDiff, bounds.width, bounds.height - yDiff);
            if (!this.supportsCloseButton((JComponent)c)) {
                return;
            }
            this.paintCloseButton(g, (JComponent)c);
        }

        @Override
        public void getCloseButtonRectangle(JComponent jc, Rectangle rect, Rectangle bounds) {
            boolean notSupported;
            boolean rightClip = ((GtkEditorTabCellRenderer)jc).isClipRight();
            boolean leftClip = ((GtkEditorTabCellRenderer)jc).isClipLeft();
            boolean bl = notSupported = !((GtkEditorTabCellRenderer)jc).isShowCloseButton();
            if (leftClip || rightClip || notSupported) {
                rect.x = -100;
                rect.y = -100;
                rect.width = 0;
                rect.height = 0;
            } else {
                String iconPath = this.findIconPath((GtkEditorTabCellRenderer)jc);
                Icon icon = TabControlButtonFactory.getIcon(iconPath);
                int iconWidth = icon.getIconWidth();
                int iconHeight = icon.getIconHeight();
                rect.x = bounds.x + bounds.width - iconWidth - 2;
                rect.y = bounds.y + Math.max(0, bounds.height / 2 - iconHeight / 2);
                rect.width = iconWidth;
                rect.height = iconHeight;
            }
        }

        private void paintCloseButton(Graphics g, JComponent c) {
            if (((AbstractTabCellRenderer)c).isShowCloseButton()) {
                Rectangle r = new Rectangle(0, 0, c.getWidth(), c.getHeight());
                Rectangle cbRect = new Rectangle();
                this.getCloseButtonRectangle(c, cbRect, r);
                String iconPath = this.findIconPath((GtkEditorTabCellRenderer)c);
                Icon icon = TabControlButtonFactory.getIcon(iconPath);
                icon.paintIcon(c, g, cbRect.x, cbRect.y);
            }
        }

        private String findIconPath(GtkEditorTabCellRenderer renderer) {
            if (renderer.inCloseButton() && renderer.isPressed()) {
                return "org/openide/awt/resources/gtk_close_pressed.png";
            }
            if (renderer.inCloseButton()) {
                return "org/openide/awt/resources/gtk_close_rollover.png";
            }
            return "org/openide/awt/resources/gtk_close_enabled.png";
        }

        @Override
        public boolean supportsCloseButton(JComponent renderer) {
            return ((AbstractTabCellRenderer)renderer).isShowCloseButton();
        }
    }

}

