/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Component;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ComponentListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.SingleSelectionModel;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListDataEvent;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.TabDisplayerUI;
import org.netbeans.swing.tabcontrol.event.ComplexListDataEvent;
import org.netbeans.swing.tabcontrol.event.ComplexListDataListener;
import org.netbeans.swing.tabcontrol.plaf.DefaultTabSelectionModel;
import org.netbeans.swing.tabcontrol.plaf.TabLayoutModel;

public abstract class AbstractTabDisplayerUI
extends TabDisplayerUI {
    protected TabLayoutModel layoutModel = null;
    protected MouseListener mouseListener = null;
    protected ComponentListener componentListener = null;
    protected PropertyChangeListener propertyChangeListener = null;
    protected ModelListener modelListener = null;
    protected ChangeListener selectionListener = null;
    protected HierarchyListener hierarchyListener = null;
    private Point scratchPoint = new Point();

    public AbstractTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
    }

    @Override
    public final void installUI(JComponent c) {
        assert (c == this.displayer);
        super.installUI(c);
        ToolTipManager.sharedInstance().registerComponent(this.displayer);
        this.layoutModel = this.createLayoutModel();
        this.mouseListener = this.createMouseListener();
        this.componentListener = this.createComponentListener();
        this.modelListener = this.createModelListener();
        this.propertyChangeListener = this.createPropertyChangeListener();
        this.selectionListener = this.createSelectionListener();
        this.hierarchyListener = this.createHierarchyListener();
        this.install();
        this.installListeners();
        this.displayer.setFont(this.createFont());
    }

    @Override
    public final void uninstallUI(JComponent c) {
        assert (c == this.displayer);
        ToolTipManager.sharedInstance().unregisterComponent(this.displayer);
        super.uninstallUI(c);
        this.uninstallListeners();
        this.uninstall();
        this.layoutModel = null;
        this.mouseListener = null;
        this.selectionModel = null;
        this.componentListener = null;
        this.selectionListener = null;
    }

    protected void install() {
    }

    protected void uninstall() {
    }

    protected final void installListeners() {
        this.displayer.addHierarchyListener(this.hierarchyListener);
        this.displayer.addPropertyChangeListener(this.propertyChangeListener);
        if (this.componentListener != null) {
            this.displayer.addComponentListener(this.componentListener);
        }
        this.displayer.getModel().addComplexListDataListener(this.modelListener);
        this.displayer.getModel().addChangeListener(this.modelListener);
        if (this.mouseListener != null) {
            this.displayer.addMouseListener(this.mouseListener);
            if (this.mouseListener instanceof MouseMotionListener) {
                this.displayer.addMouseMotionListener((MouseMotionListener)((Object)this.mouseListener));
            }
            if (this.mouseListener instanceof MouseWheelListener) {
                this.displayer.addMouseWheelListener((MouseWheelListener)((Object)this.mouseListener));
            }
        }
        this.selectionModel.addChangeListener(this.selectionListener);
    }

    protected final void uninstallListeners() {
        if (this.mouseListener instanceof MouseMotionListener) {
            this.displayer.removeMouseMotionListener((MouseMotionListener)((Object)this.mouseListener));
        }
        if (this.mouseListener instanceof MouseWheelListener) {
            this.displayer.removeMouseWheelListener((MouseWheelListener)((Object)this.mouseListener));
        }
        if (this.mouseListener != null) {
            this.displayer.removeMouseListener(this.mouseListener);
        }
        if (this.componentListener != null) {
            this.displayer.removeComponentListener(this.componentListener);
        }
        this.displayer.getModel().removeComplexListDataListener(this.modelListener);
        this.displayer.getModel().removeChangeListener(this.modelListener);
        this.displayer.removePropertyChangeListener(this.propertyChangeListener);
        this.displayer.removeHierarchyListener(this.hierarchyListener);
        this.selectionModel.removeChangeListener(this.selectionListener);
        this.mouseListener = null;
        this.componentListener = null;
        this.propertyChangeListener = null;
        this.selectionListener = null;
        this.modelListener = null;
        this.hierarchyListener = null;
    }

    protected HierarchyListener createHierarchyListener() {
        return new DisplayerHierarchyListener();
    }

    protected abstract TabLayoutModel createLayoutModel();

    protected abstract MouseListener createMouseListener();

    protected abstract ChangeListener createSelectionListener();

    protected Font createFont() {
        return UIManager.getFont("controlFont");
    }

    protected ModelListener createModelListener() {
        return new ModelListener();
    }

    protected ComponentListener createComponentListener() {
        return null;
    }

    protected PropertyChangeListener createPropertyChangeListener() {
        return new DisplayerPropertyChangeListener();
    }

    @Override
    protected SingleSelectionModel createSelectionModel() {
        return new DefaultTabSelectionModel(this.displayer.getModel());
    }

    @Override
    public int dropIndexOfPoint(Point p) {
        Point p2 = this.toDropPoint(p);
        int max = this.displayer.getModel().size();
        for (int i = 0; i < max; ++i) {
            Rectangle r = this.getTabRect(i, null);
            if (!r.contains(p2)) continue;
            return i;
        }
        return -1;
    }

    protected void modelChanged() {
        this.displayer.repaint();
    }

    protected Point toDropPoint(Point location) {
        if (this.displayer.getWidth() > this.displayer.getHeight()) {
            this.scratchPoint.setLocation(location.x, this.displayer.getHeight() / 2);
        } else {
            this.scratchPoint.setLocation(this.displayer.getWidth() / 2, location.y);
        }
        return this.scratchPoint;
    }

    @Override
    public void unregisterShortcuts(JComponent comp) {
    }

    @Override
    public void registerShortcuts(JComponent comp) {
    }

    protected class ModelListener
    implements ComplexListDataListener,
    ChangeListener {
        private boolean checkVisible;

        protected ModelListener() {
            this.checkVisible = false;
        }

        @Override
        public void contentsChanged(ListDataEvent e) {
        }

        @Override
        public void indicesAdded(ComplexListDataEvent e) {
        }

        @Override
        public void indicesChanged(ComplexListDataEvent e) {
        }

        @Override
        public void indicesRemoved(ComplexListDataEvent e) {
        }

        @Override
        public void intervalAdded(ListDataEvent e) {
        }

        @Override
        public void intervalRemoved(ListDataEvent e) {
        }

        @Override
        public final void stateChanged(ChangeEvent e) {
            AbstractTabDisplayerUI.this.modelChanged();
        }
    }

    protected class DisplayerHierarchyListener
    implements HierarchyListener {
        @Override
        public void hierarchyChanged(HierarchyEvent e) {
            if (e.getChanged() == AbstractTabDisplayerUI.this.displayer && (e.getChangeFlags() & 4) != 0) {
                if (AbstractTabDisplayerUI.this.displayer.isShowing()) {
                    ToolTipManager.sharedInstance().registerComponent(AbstractTabDisplayerUI.this.displayer);
                } else {
                    ToolTipManager.sharedInstance().unregisterComponent(AbstractTabDisplayerUI.this.displayer);
                }
            }
        }
    }

    protected class DisplayerPropertyChangeListener
    implements PropertyChangeListener {
        protected DisplayerPropertyChangeListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent e) {
            if (AbstractTabDisplayerUI.this.displayer.isShowing() && "active".equals(e.getPropertyName())) {
                this.activationChanged();
            }
        }

        protected void activationChanged() {
            int i = AbstractTabDisplayerUI.this.selectionModel.getSelectedIndex();
            if (i != -1) {
                Rectangle r = new Rectangle();
                AbstractTabDisplayerUI.this.getTabRect(i, r);
                if (r.width != 0 && r.height != 0) {
                    AbstractTabDisplayerUI.this.displayer.repaint(r.x, r.y, r.width, r.height);
                }
            }
        }
    }

}

