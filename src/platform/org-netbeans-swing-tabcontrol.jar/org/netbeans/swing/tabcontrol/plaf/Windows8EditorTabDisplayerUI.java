/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Dimension;
import java.awt.Graphics;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.plaf.AbstractWinEditorTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.TabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.netbeans.swing.tabcontrol.plaf.Windows8EditorTabCellRenderer;

public final class Windows8EditorTabDisplayerUI
extends AbstractWinEditorTabDisplayerUI {
    private static Map<Integer, String[]> buttonIconPaths;

    public Windows8EditorTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
    }

    public static ComponentUI createUI(JComponent c) {
        return new Windows8EditorTabDisplayerUI((TabDisplayer)c);
    }

    @Override
    protected TabCellRenderer createDefaultRenderer() {
        return new Windows8EditorTabCellRenderer();
    }

    @Override
    int getScrollButtonPadding() {
        return 3;
    }

    private static void initIcons() {
        if (null == buttonIconPaths) {
            buttonIconPaths = new HashMap<Integer, String[]>(7);
            String[] iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/win8_scrollleft_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/win8_scrollleft_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/win8_scrollleft_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/win8_scrollleft_pressed.png";
            buttonIconPaths.put(9, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/win8_scrollright_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/win8_scrollright_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/win8_scrollright_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/win8_scrollright_pressed.png";
            buttonIconPaths.put(10, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/win8_popup_enabled.png";
            iconPaths[2] = iconPaths[0];
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/win8_popup_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/win8_popup_pressed.png";
            buttonIconPaths.put(8, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/win8_maximize_enabled.png";
            iconPaths[2] = iconPaths[0];
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/win8_maximize_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/win8_maximize_pressed.png";
            buttonIconPaths.put(3, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/win8_restore_group_enabled.png";
            iconPaths[2] = iconPaths[0];
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/win8_restore_group_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/win8_restore_group_pressed.png";
            buttonIconPaths.put(4, iconPaths);
        }
    }

    @Override
    public Icon getButtonIcon(int buttonId, int buttonState) {
        Icon res = null;
        Windows8EditorTabDisplayerUI.initIcons();
        String[] paths = buttonIconPaths.get(buttonId);
        if (null != paths && buttonState >= 0 && buttonState < paths.length) {
            res = TabControlButtonFactory.getIcon(paths[buttonState]);
        }
        return res;
    }
}

