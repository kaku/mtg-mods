/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Polygon;
import java.awt.Rectangle;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.UIManager;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.plaf.AbstractTabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.BusyTabsSupport;
import org.netbeans.swing.tabcontrol.plaf.ColorUtil;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.netbeans.swing.tabcontrol.plaf.TabPainter;

class WinVistaEditorTabCellRenderer
extends AbstractTabCellRenderer {
    private static final int TOP_INSET = 0;
    private static final int LEFT_INSET = 3;
    private static final int RIGHT_INSET = 0;
    static final int BOTTOM_INSET = 0;
    private static final TabPainter leftClip = new WinVistaLeftClipPainter();
    private static final TabPainter rightClip = new WinVistaRightClipPainter();
    private static final TabPainter normal = new WinVistaPainter();

    public WinVistaEditorTabCellRenderer() {
        super(leftClip, normal, rightClip, new Dimension(32, 42));
    }

    @Override
    public int getPixelsToAddToSelection() {
        return 0;
    }

    @Override
    public Dimension getPadding() {
        Dimension d = super.getPadding();
        d.width = this.isShowCloseButton() && !Boolean.getBoolean("nb.tabs.suppressCloseButton") ? 32 : 16;
        return d;
    }

    private static Color getUnselFillBrightUpperColor() {
        Color result = UIManager.getColor("tab_unsel_fill_bright_upper");
        if (result == null) {
            result = new Color(235, 235, 235);
        }
        return result;
    }

    private static Color getUnselFillDarkUpperColor() {
        Color result = UIManager.getColor("tab_unsel_fill_dark_upper");
        if (result == null) {
            result = new Color(229, 229, 229);
        }
        return result;
    }

    private static Color getUnselFillBrightLowerColor() {
        Color result = UIManager.getColor("tab_unsel_fill_bright_lower");
        if (result == null) {
            result = new Color(214, 214, 214);
        }
        return result;
    }

    private static Color getUnselFillDarkLowerColor() {
        Color result = UIManager.getColor("tab_unsel_fill_dark_lower");
        if (result == null) {
            result = new Color(203, 203, 203);
        }
        return result;
    }

    private static Color getSelFillColor() {
        Color result = UIManager.getColor("tab_sel_fill");
        if (result == null) {
            result = new Color(244, 244, 244);
        }
        return result;
    }

    private static Color getFocusFillUpperColor() {
        Color result = UIManager.getColor("tab_focus_fill_upper");
        if (result == null) {
            result = new Color(242, 249, 252);
        }
        return result;
    }

    private static Color getFocusFillBrightLowerColor() {
        Color result = UIManager.getColor("tab_focus_fill_bright_lower");
        if (result == null) {
            result = new Color(225, 241, 249);
        }
        return result;
    }

    private static Color getFocusFillDarkLowerColor() {
        Color result = UIManager.getColor("tab_focus_fill_dark_lower");
        if (result == null) {
            result = new Color(216, 236, 246);
        }
        return result;
    }

    private static Color getMouseOverFillBrightUpperColor() {
        Color result = UIManager.getColor("tab_mouse_over_fill_bright_upper");
        if (result == null) {
            result = new Color(223, 242, 252);
        }
        return result;
    }

    private static Color getMouseOverFillDarkUpperColor() {
        Color result = UIManager.getColor("tab_mouse_over_fill_dark_upper");
        if (result == null) {
            result = new Color(214, 239, 252);
        }
        return result;
    }

    private static Color getMouseOverFillBrightLowerColor() {
        Color result = UIManager.getColor("tab_mouse_over_fill_bright_lower");
        if (result == null) {
            result = new Color(189, 228, 250);
        }
        return result;
    }

    private static Color getMouseOverFillDarkLowerColor() {
        Color result = UIManager.getColor("tab_mouse_over_fill_dark_lower");
        if (result == null) {
            result = new Color(171, 221, 248);
        }
        return result;
    }

    private static Color getTxtColor() {
        Color result = UIManager.getColor("TabbedPane.foreground");
        if (result == null) {
            result = new Color(0, 0, 0);
        }
        return result;
    }

    static Color getBorderColor() {
        Color result = UIManager.getColor("tab_border");
        if (result == null) {
            result = new Color(137, 140, 149);
        }
        return result;
    }

    private static Color getSelBorderColor() {
        Color result = UIManager.getColor("tab_sel_border");
        if (result == null) {
            result = new Color(60, 127, 177);
        }
        return result;
    }

    private static Color getBorderInnerColor() {
        Color result = UIManager.getColor("tab_border_inner");
        if (result == null) {
            result = new Color(255, 255, 255);
        }
        return result;
    }

    @Override
    public Color getSelectedActivatedForeground() {
        return WinVistaEditorTabCellRenderer.getTxtColor();
    }

    @Override
    public Color getSelectedForeground() {
        return WinVistaEditorTabCellRenderer.getTxtColor();
    }

    void paintTabGradient(Graphics g, Polygon poly) {
        Rectangle rect = poly.getBounds();
        boolean selected = this.isSelected();
        boolean focused = selected && this.isActive();
        boolean attention = this.isAttention();
        boolean mouseOver = this.isArmed();
        if (focused && !attention) {
            ++rect.height;
            ColorUtil.vistaFillRectGradient((Graphics2D)g, rect, WinVistaEditorTabCellRenderer.getFocusFillUpperColor(), WinVistaEditorTabCellRenderer.getFocusFillBrightLowerColor(), WinVistaEditorTabCellRenderer.getFocusFillDarkLowerColor());
        } else if (selected && !attention) {
            ++rect.height;
            g.setColor(WinVistaEditorTabCellRenderer.getSelFillColor());
            g.fillPolygon(poly);
        } else if (mouseOver && !attention) {
            ColorUtil.vistaFillRectGradient((Graphics2D)g, rect, WinVistaEditorTabCellRenderer.getMouseOverFillBrightUpperColor(), WinVistaEditorTabCellRenderer.getMouseOverFillDarkUpperColor(), WinVistaEditorTabCellRenderer.getMouseOverFillBrightLowerColor(), WinVistaEditorTabCellRenderer.getMouseOverFillDarkLowerColor());
        } else if (attention) {
            Color a = new Color(255, 255, 128);
            Color b = new Color(230, 200, 64);
            ColorUtil.xpFillRectGradient((Graphics2D)g, rect, a, b);
        } else {
            ColorUtil.vistaFillRectGradient((Graphics2D)g, rect, WinVistaEditorTabCellRenderer.getUnselFillBrightUpperColor(), WinVistaEditorTabCellRenderer.getUnselFillDarkUpperColor(), WinVistaEditorTabCellRenderer.getUnselFillBrightLowerColor(), WinVistaEditorTabCellRenderer.getUnselFillDarkLowerColor());
        }
    }

    @Override
    protected int getCaptionYAdjustment() {
        return 0;
    }

    @Override
    protected int getIconYAdjustment() {
        return -2;
    }

    @Override
    protected void paintIconAndText(Graphics g) {
        if (this.isBusy()) {
            this.setIcon(BusyTabsSupport.getDefault().getBusyIcon(this.isSelected()));
        }
        super.paintIconAndText(g);
    }

    String findIconPath() {
        if (this.inCloseButton() && this.isPressed()) {
            return "org/openide/awt/resources/vista_close_pressed.png";
        }
        if (this.inCloseButton()) {
            return "org/openide/awt/resources/vista_close_rollover.png";
        }
        return "org/openide/awt/resources/vista_close_enabled.png";
    }

    private static class WinVistaRightClipPainter
    implements TabPainter {
        private WinVistaRightClipPainter() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return new Insets(0, 3, 0, 0);
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        @Override
        public Polygon getInteriorPolygon(Component c) {
            WinVistaEditorTabCellRenderer ren = (WinVistaEditorTabCellRenderer)c;
            Insets ins = this.getBorderInsets(c);
            Polygon p = new Polygon();
            int x = 0;
            int y = 0;
            int width = c.getWidth() + 1;
            int height = c.getHeight() - ins.bottom;
            p.addPoint(x, y + ins.top);
            p.addPoint(x + width, y + ins.top);
            p.addPoint(x + width, y + height - 1);
            p.addPoint(x, y + height - 1);
            return p;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            WinVistaEditorTabCellRenderer ren = (WinVistaEditorTabCellRenderer)c;
            g.translate(x, y);
            Color borderColor = ren.isActive() && ren.isSelected() || ren.isArmed() ? WinVistaEditorTabCellRenderer.getSelBorderColor() : WinVistaEditorTabCellRenderer.getBorderColor();
            g.setColor(borderColor);
            boolean left = false;
            g.drawLine(0, 0, width, 0);
            g.setColor(WinVistaEditorTabCellRenderer.getBorderColor());
            if (!ren.isSelected()) {
                g.drawLine(0, height - 1, width - 1, height - 1);
            } else {
                g.drawLine(width - 1, height - 1, width - 1, height - 1);
            }
            g.setColor(WinVistaEditorTabCellRenderer.getBorderInnerColor());
            g.drawLine(1, 1, width, 1);
            if (ren.isSelected()) {
                ++height;
            }
            g.drawLine(0, 1, 0, height - 2);
            g.translate(- x, - y);
        }

        @Override
        public void paintInterior(Graphics g, Component c) {
            WinVistaEditorTabCellRenderer ren = (WinVistaEditorTabCellRenderer)c;
            Polygon poly = this.getInteriorPolygon(ren);
            ren.paintTabGradient(g, poly);
        }

        @Override
        public boolean supportsCloseButton(JComponent renderer) {
            return false;
        }

        @Override
        public void getCloseButtonRectangle(JComponent jc, Rectangle rect, Rectangle bounds) {
            rect.setBounds(-20, -20, 0, 0);
        }
    }

    private static class WinVistaLeftClipPainter
    implements TabPainter {
        private WinVistaLeftClipPainter() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return new Insets(0, 3, 0, 0);
        }

        @Override
        public Polygon getInteriorPolygon(Component c) {
            WinVistaEditorTabCellRenderer ren = (WinVistaEditorTabCellRenderer)c;
            Insets ins = this.getBorderInsets(c);
            Polygon p = new Polygon();
            int x = 0;
            int y = 0;
            int width = ren.isRightmost() ? c.getWidth() - 1 : c.getWidth();
            int height = c.getHeight() - ins.bottom;
            p.addPoint(x, y + ins.top);
            p.addPoint(x + width, y + ins.top);
            p.addPoint(x + width, y + height - 1);
            p.addPoint(x, y + height - 1);
            return p;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            WinVistaEditorTabCellRenderer ren = (WinVistaEditorTabCellRenderer)c;
            g.translate(x, y);
            Color borderColor = ren.isActive() && ren.isSelected() || ren.isArmed() ? WinVistaEditorTabCellRenderer.getSelBorderColor() : WinVistaEditorTabCellRenderer.getBorderColor();
            g.setColor(borderColor);
            boolean left = false;
            g.drawLine(0, 0, width - 1, 0);
            if (ren.isActive() && ren.isNextTabSelected() || ren.isNextTabArmed()) {
                g.setColor(WinVistaEditorTabCellRenderer.getSelBorderColor());
            }
            g.drawLine(width - 1, 0, width - 1, height - 2);
            g.setColor(WinVistaEditorTabCellRenderer.getBorderColor());
            if (!ren.isSelected()) {
                g.drawLine(0, height - 1, width - 1, height - 1);
            } else {
                g.drawLine(width - 1, height - 1, width - 1, height - 1);
            }
            g.setColor(WinVistaEditorTabCellRenderer.getBorderInnerColor());
            g.drawLine(0, 1, width - 2, 1);
            if (ren.isSelected()) {
                ++height;
            }
            g.drawLine(width - 2, 1, width - 2, height - 2);
            g.translate(- x, - y);
        }

        @Override
        public void paintInterior(Graphics g, Component c) {
            WinVistaEditorTabCellRenderer ren = (WinVistaEditorTabCellRenderer)c;
            Polygon poly = this.getInteriorPolygon(ren);
            ren.paintTabGradient(g, poly);
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        @Override
        public boolean supportsCloseButton(JComponent renderer) {
            return false;
        }

        @Override
        public void getCloseButtonRectangle(JComponent jc, Rectangle rect, Rectangle bounds) {
            rect.setBounds(-20, -20, 0, 0);
        }
    }

    private static class WinVistaPainter
    implements TabPainter {
        private WinVistaPainter() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return new Insets(0, 3, 0, 0);
        }

        @Override
        public void getCloseButtonRectangle(JComponent jc, Rectangle rect, Rectangle bounds) {
            WinVistaEditorTabCellRenderer ren = (WinVistaEditorTabCellRenderer)jc;
            if (!ren.isShowCloseButton()) {
                rect.x = -100;
                rect.y = -100;
                rect.width = 0;
                rect.height = 0;
                return;
            }
            String iconPath = ren.findIconPath();
            Icon icon = TabControlButtonFactory.getIcon(iconPath);
            int iconWidth = icon.getIconWidth();
            int iconHeight = icon.getIconHeight();
            rect.x = bounds.x + bounds.width - iconWidth - 2;
            rect.y = bounds.y + Math.max(0, bounds.height / 2 - iconHeight / 2);
            rect.width = iconWidth;
            rect.height = iconHeight;
        }

        @Override
        public Polygon getInteriorPolygon(Component c) {
            WinVistaEditorTabCellRenderer ren = (WinVistaEditorTabCellRenderer)c;
            Insets ins = this.getBorderInsets(c);
            Polygon p = new Polygon();
            int x = 0;
            int y = 0;
            int width = ren.isRightmost() ? c.getWidth() - 1 : c.getWidth();
            int height = c.getHeight() - ins.bottom;
            p.addPoint(x, y + ins.top);
            p.addPoint(x + width, y + ins.top);
            p.addPoint(x + width, y + height - 1);
            p.addPoint(x, y + height - 1);
            return p;
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            WinVistaEditorTabCellRenderer ren = (WinVistaEditorTabCellRenderer)c;
            g.translate(x, y);
            Color borderColor = ren.isActive() && ren.isSelected() || ren.isArmed() ? WinVistaEditorTabCellRenderer.getSelBorderColor() : WinVistaEditorTabCellRenderer.getBorderColor();
            g.setColor(borderColor);
            boolean left = false;
            if (ren.isLeftmost()) {
                g.drawLine(0, 0, 0, height - 1);
            }
            g.drawLine(0, 0, width - 1, 0);
            if (ren.isActive() && ren.isNextTabSelected() || ren.isNextTabArmed()) {
                g.setColor(WinVistaEditorTabCellRenderer.getSelBorderColor());
            }
            g.drawLine(width - 1, 0, width - 1, height - 2);
            g.setColor(WinVistaEditorTabCellRenderer.getBorderColor());
            if (!ren.isSelected()) {
                g.drawLine(0, height - 1, width - 1, height - 1);
            } else {
                g.drawLine(width - 1, height - 1, width - 1, height - 1);
            }
            g.setColor(WinVistaEditorTabCellRenderer.getBorderInnerColor());
            g.drawLine(1, 1, width - 2, 1);
            if (ren.isSelected()) {
                ++height;
            }
            if (ren.isLeftmost()) {
                g.drawLine(1, 1, 1, height - 2);
            } else {
                g.drawLine(0, 1, 0, height - 2);
            }
            g.drawLine(width - 2, 1, width - 2, height - 2);
            g.translate(- x, - y);
        }

        @Override
        public void paintInterior(Graphics g, Component c) {
            WinVistaEditorTabCellRenderer ren = (WinVistaEditorTabCellRenderer)c;
            Polygon poly = this.getInteriorPolygon(ren);
            ren.paintTabGradient(g, poly);
            Rectangle r = new Rectangle();
            this.getCloseButtonRectangle(ren, r, new Rectangle(0, 0, ren.getWidth(), ren.getHeight()));
            if (!g.hitClip(r.x, r.y, r.width, r.height)) {
                return;
            }
            String iconPath = ren.findIconPath();
            Icon icon = TabControlButtonFactory.getIcon(iconPath);
            icon.paintIcon(ren, g, r.x, r.y);
        }

        @Override
        public boolean supportsCloseButton(JComponent renderer) {
            return renderer instanceof TabDisplayer ? ((TabDisplayer)renderer).isShowCloseButton() : true;
        }
    }

}

