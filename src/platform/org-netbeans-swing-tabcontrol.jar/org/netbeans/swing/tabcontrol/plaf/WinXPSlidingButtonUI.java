/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.JComponent;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.SlidingButton;
import org.netbeans.swing.tabcontrol.SlidingButtonUI;
import org.netbeans.swing.tabcontrol.plaf.ColorUtil;
import org.netbeans.swing.tabcontrol.plaf.WinClassicEditorTabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.WindowsSlidingButtonUI;

public class WinXPSlidingButtonUI
extends WindowsSlidingButtonUI {
    private static final SlidingButtonUI INSTANCE = new WinXPSlidingButtonUI();
    private boolean defaults_initialized = false;
    protected JToggleButton hiddenToggle;

    WinXPSlidingButtonUI() {
    }

    public static ComponentUI createUI(JComponent c) {
        return INSTANCE;
    }

    @Override
    protected void installBorder(AbstractButton b) {
    }

    @Override
    public void installDefaults(AbstractButton b) {
        super.installDefaults(b);
        if (!this.defaults_initialized) {
            this.hiddenToggle = new JToggleButton();
            this.hiddenToggle.setText("");
            JToolBar bar = new JToolBar();
            bar.add(this.hiddenToggle);
            this.defaults_initialized = true;
        }
    }

    @Override
    protected void uninstallDefaults(AbstractButton b) {
        super.uninstallDefaults(b);
        this.defaults_initialized = false;
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        ColorUtil.setupAntialiasing(g);
        AbstractButton button = (AbstractButton)c;
        this.hiddenToggle.setBorderPainted(button.isBorderPainted());
        this.hiddenToggle.setRolloverEnabled(button.isRolloverEnabled());
        this.hiddenToggle.setFocusable(button.isFocusable());
        this.hiddenToggle.setFocusPainted(button.isFocusPainted());
        this.hiddenToggle.setMargin(button.getMargin());
        this.hiddenToggle.setBorder(button.getBorder());
        this.hiddenToggle.getModel().setRollover(button.getModel().isRollover());
        this.hiddenToggle.getModel().setPressed(button.getModel().isPressed());
        this.hiddenToggle.getModel().setArmed(button.getModel().isArmed());
        this.hiddenToggle.getModel().setSelected(button.getModel().isSelected());
        this.hiddenToggle.setBounds(button.getBounds());
        super.paint(g, c);
    }

    @Override
    protected void paintBackground(Graphics2D g, AbstractButton b) {
        if (((SlidingButton)b).isBlinkState()) {
            g.setColor(WinClassicEditorTabCellRenderer.ATTENTION_COLOR);
            g.fillRect(0, 0, b.getWidth(), b.getHeight());
            this.hiddenToggle.setFont(b.getFont());
        } else {
            this.hiddenToggle.paint(g);
        }
    }

    @Override
    protected void paintButtonPressed(Graphics g, AbstractButton b) {
        this.hiddenToggle.paint(g);
    }
}

