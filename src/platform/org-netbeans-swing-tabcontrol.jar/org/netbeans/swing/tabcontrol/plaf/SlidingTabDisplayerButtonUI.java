/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.geom.AffineTransform;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicToggleButtonUI;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.plaf.BasicSlidingTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.GenericGlowingChiclet;
import org.openide.awt.HtmlRenderer;

public class SlidingTabDisplayerButtonUI
extends BasicToggleButtonUI {
    private static final SlidingTabDisplayerButtonUI INSTANCE = new SlidingTabDisplayerButtonUI();
    private static SlidingTabDisplayerButtonUI AQUA_INSTANCE = null;

    protected SlidingTabDisplayerButtonUI() {
    }

    public static ComponentUI createUI(JComponent c) {
        return INSTANCE;
    }

    @Override
    public void installUI(JComponent c) {
        this.installDefaults((AbstractButton)c);
        this.installListeners((AbstractButton)c);
        this.installBorder((AbstractButton)c);
    }

    protected void installBorder(AbstractButton b) {
        b.setBorder(BorderFactory.createEtchedBorder());
    }

    @Override
    public void uninstallUI(JComponent c) {
        this.uninstallListeners((AbstractButton)c);
        this.uninstallDefaults((AbstractButton)c);
    }

    @Override
    public void installDefaults(AbstractButton b) {
        b.setFocusable(false);
    }

    @Override
    public Dimension getMinimumSize(JComponent c) {
        return this.getPreferredSize(c);
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        return null;
    }

    @Override
    public Dimension getMaximumSize(JComponent c) {
        return this.getPreferredSize(c);
    }

    @Override
    public final void paint(Graphics g, JComponent c) {
        BasicSlidingTabDisplayerUI.IndexButton b = (BasicSlidingTabDisplayerUI.IndexButton)c;
        Graphics2D g2d = (Graphics2D)g;
        this.paintBackground(g2d, b);
        Object orientation = b.getOrientation();
        AffineTransform tr = g2d.getTransform();
        if (orientation == TabDisplayer.ORIENTATION_EAST) {
            g2d.rotate(1.5707963267948966);
            g2d.translate(0, - c.getWidth());
        } else if (orientation == TabDisplayer.ORIENTATION_WEST) {
            g2d.rotate(-1.5707963267948966);
            g2d.translate(- c.getHeight(), 0);
        }
        this.paintIconAndText(g2d, b, orientation);
        g2d.setTransform(tr);
    }

    protected void paintBackground(Graphics2D g, BasicSlidingTabDisplayerUI.IndexButton b) {
        Color c = b.isSelected() ? Color.ORANGE : b.getBackground();
        g.setColor(c);
        g.fillRect(0, 0, b.getWidth(), b.getHeight());
    }

    protected final void paintIconAndText(Graphics2D g, BasicSlidingTabDisplayerUI.IndexButton b, Object orientation) {
        int txtX;
        FontMetrics fm = g.getFontMetrics(b.getFont());
        Insets ins = b.getInsets();
        boolean flip = orientation == TabDisplayer.ORIENTATION_EAST || orientation == TabDisplayer.ORIENTATION_WEST;
        int n = txtX = flip ? ins.top : ins.left;
        int txtY = orientation == TabDisplayer.ORIENTATION_EAST ? ins.right : (orientation == TabDisplayer.ORIENTATION_WEST ? ins.left : ins.top);
        int txtW = flip ? b.getHeight() - (ins.top + ins.bottom) : b.getWidth() - (ins.left + ins.right);
        int iconX = txtX;
        int iconY = txtY;
        int txtH = fm.getHeight();
        txtY += fm.getMaxAscent();
        Icon icon = b.getIcon();
        int iconH = icon.getIconHeight();
        int iconW = icon.getIconWidth();
        int workingHeight = flip ? b.getWidth() - (ins.left + ins.right) : b.getHeight() - (ins.top + ins.bottom);
        txtY += workingHeight / 2 - txtH / 2;
        iconY += workingHeight / 2 - iconH / 2;
        if (icon != null && iconW > 0 && iconH > 0) {
            txtX += iconW + b.getIconTextGap();
            icon.paintIcon(b, g, iconX, iconY);
            txtW -= iconH + b.getIconTextGap();
        }
        HtmlRenderer.renderString((String)b.getText(), (Graphics)g, (int)txtX, (int)txtY, (int)txtW, (int)txtH, (Font)b.getFont(), (Color)b.getForeground(), (int)1, (boolean)true);
    }

    public static final class Aqua
    extends SlidingTabDisplayerButtonUI {
        public static ComponentUI createUI(JComponent c) {
            if (AQUA_INSTANCE == null) {
                AQUA_INSTANCE = new Aqua();
            }
            return AQUA_INSTANCE;
        }

        @Override
        protected void installBorder(AbstractButton b) {
            b.setBorder(BorderFactory.createEmptyBorder(5, 2, 2, 2));
        }

        @Override
        protected void paintBackground(Graphics2D g, BasicSlidingTabDisplayerUI.IndexButton b) {
            GenericGlowingChiclet chic = GenericGlowingChiclet.INSTANCE;
            int state = 0;
            state |= b.isSelected() ? 2 : 0;
            state |= b.getModel().isPressed();
            chic.setState(state |= b.isActive() ? 4 : 0);
            chic.setArcs(0.2f, 0.2f, 0.2f, 0.2f);
            chic.setBounds(0, 1, b.getWidth(), b.getHeight());
            chic.setAllowVertical(true);
            chic.draw(g);
            chic.setAllowVertical(false);
        }
    }

}

