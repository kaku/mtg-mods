/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.KeyboardFocusManager;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SingleSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListDataEvent;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.TabDisplayerUI;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.event.ComplexListDataEvent;
import org.netbeans.swing.tabcontrol.event.ComplexListDataListener;
import org.netbeans.swing.tabcontrol.plaf.ColorUtil;
import org.netbeans.swing.tabcontrol.plaf.DefaultTabSelectionModel;
import org.netbeans.swing.tabcontrol.plaf.EqualPolygon;
import org.netbeans.swing.tabcontrol.plaf.NonStretchingViewTabLayoutModel;
import org.netbeans.swing.tabcontrol.plaf.TabControlButton;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.netbeans.swing.tabcontrol.plaf.TabLayoutModel;
import org.netbeans.swing.tabcontrol.plaf.TabState;
import org.netbeans.swing.tabcontrol.plaf.ViewTabLayoutModel;
import org.netbeans.swing.tabcontrol.plaf.ViewTabLayoutModel2;
import org.openide.windows.TopComponent;

public abstract class AbstractViewTabDisplayerUI
extends TabDisplayerUI {
    private TabDataModel dataModel;
    private TabLayoutModel layoutModel;
    private FontMetrics fm;
    private Font txtFont;
    private Component controlButtons;
    protected Controller controller;
    private TabControlButton btnClose;
    private TabControlButton btnAutoHidePin;
    private TabControlButton btnMinimizeMode;
    private final Action pinAction;
    private static final String PIN_ACTION = "pinAction";
    private static final String TRANSPARENCY_ACTION = "transparencyAction";
    private static final int ICON_X_PAD = 4;
    protected final TabState tabState;

    public AbstractViewTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
        this.pinAction = new PinAction();
        this.tabState = new ViewTabState();
        displayer.setLayout(null);
    }

    @Override
    public void installUI(JComponent c) {
        super.installUI(c);
        ToolTipManager.sharedInstance().registerComponent(this.displayer);
        this.controller = this.createController();
        this.dataModel = this.displayer.getModel();
        this.dataModel.addChangeListener(this.controller);
        this.dataModel.addComplexListDataListener(this.controller);
        this.layoutModel = this.createLayoutModel();
        if (!Boolean.getBoolean("winsys.non_stretching_view_tabs") && !this.isUseStretchingTabs()) {
            this.btnMinimizeMode = TabControlButtonFactory.createSlideGroupButton(this.displayer);
            c.setLayout(new PinButtonLayout());
        }
        this.displayer.addPropertyChangeListener(this.controller);
        this.selectionModel.addChangeListener(this.controller);
        this.displayer.addMouseListener(this.controller);
        this.displayer.addMouseMotionListener(this.controller);
        this.installControlButtons();
        this.dataModel.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                AbstractViewTabDisplayerUI.this.showHideControlButtons();
                if (null != AbstractViewTabDisplayerUI.this.dataModel) {
                    AbstractViewTabDisplayerUI.this.dataModel.removeChangeListener(this);
                }
            }
        });
        this.showHideControlButtons();
    }

    protected TabLayoutModel createLayoutModel() {
        if (Boolean.getBoolean("winsys.non_stretching_view_tabs")) {
            ViewTabLayoutModel2.PaddingInfo padding = new ViewTabLayoutModel2.PaddingInfo();
            padding.iconsXPad = 5;
            padding.txtIconsXPad = 10;
            padding.txtPad = new Dimension(5, 2);
            return new ViewTabLayoutModel2(this.displayer, padding);
        }
        if (this.isUseStretchingTabs()) {
            return new ViewTabLayoutModel(this.dataModel, this.displayer);
        }
        this.btnMinimizeMode = TabControlButtonFactory.createSlideGroupButton(this.displayer);
        this.displayer.setLayout(new PinButtonLayout());
        return new NonStretchingViewTabLayoutModel(this.dataModel, this.displayer);
    }

    void showHideControlButtons() {
        TabData tab;
        Component tabComponent = null;
        boolean tcSlidingEnabled = true;
        boolean tcClosingEnabled = true;
        int selIndex = Math.max(0, this.displayer.getSelectionModel().getSelectedIndex());
        if (selIndex >= 0 && selIndex < this.displayer.getModel().size() && (tabComponent = (tab = this.displayer.getModel().getTab(selIndex)).getComponent()) instanceof TopComponent) {
            tcSlidingEnabled = this.displayer.getContainerWinsysInfo().isTopComponentSlidingEnabled((TopComponent)tabComponent);
            tcClosingEnabled = this.displayer.getContainerWinsysInfo().isTopComponentClosingEnabled((TopComponent)tabComponent);
        }
        this.btnAutoHidePin.setVisible(tabComponent != null && !TabDisplayer.ORIENTATION_INVISIBLE.equals(this.displayer.getContainerWinsysInfo().getOrientation(tabComponent)) && this.displayer.getContainerWinsysInfo().isTopComponentSlidingEnabled() && tcSlidingEnabled);
        if (null != this.btnClose) {
            this.btnClose.setVisible(tabComponent != null && tcClosingEnabled);
        }
    }

    protected void installControlButtons() {
        if (null != this.getControlButtons()) {
            this.displayer.add(this.getControlButtons());
        }
        if (null != this.btnMinimizeMode) {
            this.displayer.add(this.btnMinimizeMode);
        }
    }

    protected Component getControlButtons() {
        if (null == this.controlButtons) {
            Icon icon;
            JPanel buttonsPanel = new JPanel(null);
            buttonsPanel.setOpaque(false);
            int width = 0;
            int height = 0;
            boolean showPin = null != this.displayer.getContainerWinsysInfo() && this.isUseStretchingTabs();
            this.btnAutoHidePin = TabControlButtonFactory.createSlidePinButton(this.displayer);
            if (showPin) {
                buttonsPanel.add(this.btnAutoHidePin);
                icon = this.btnAutoHidePin.getIcon();
                height = icon.getIconHeight();
            }
            if (null == this.displayer.getContainerWinsysInfo() || this.displayer.getContainerWinsysInfo().isTopComponentClosingEnabled()) {
                this.btnClose = TabControlButtonFactory.createCloseButton(this.displayer);
                buttonsPanel.add(this.btnClose);
                icon = this.btnClose.getIcon();
                height = Math.max(height, icon.getIconHeight());
            }
            if (null != this.btnClose) {
                icon = this.btnClose.getIcon();
                this.btnClose.setBounds(width, height / 2 - icon.getIconHeight() / 2, icon.getIconWidth(), icon.getIconHeight());
                width += icon.getIconWidth();
            }
            if (showPin) {
                if (0 != width) {
                    width += 4;
                }
                icon = this.btnAutoHidePin.getIcon();
                this.btnAutoHidePin.setBounds(width, height / 2 - icon.getIconHeight() / 2, icon.getIconWidth(), icon.getIconHeight());
                width += icon.getIconWidth();
                width += 4;
            }
            Dimension size = new Dimension(width, height);
            buttonsPanel.setMinimumSize(size);
            buttonsPanel.setSize(size);
            buttonsPanel.setPreferredSize(size);
            buttonsPanel.setMaximumSize(size);
            this.controlButtons = buttonsPanel;
        }
        return this.controlButtons;
    }

    @Override
    public void uninstallUI(JComponent c) {
        super.uninstallUI(c);
        ToolTipManager.sharedInstance().unregisterComponent(this.displayer);
        this.displayer.removePropertyChangeListener(this.controller);
        this.dataModel.removeChangeListener(this.controller);
        this.dataModel.removeComplexListDataListener(this.controller);
        this.selectionModel.removeChangeListener(this.controller);
        this.displayer.removeMouseListener(this.controller);
        this.displayer.removeMouseMotionListener(this.controller);
        if (this.controlButtons != null) {
            this.displayer.remove(this.controlButtons);
            this.controlButtons = null;
        }
        this.layoutModel = null;
        this.selectionModel = null;
        this.dataModel = null;
        this.controller = null;
    }

    protected Controller createController() {
        return new Controller();
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        ColorUtil.setupAntialiasing(g);
        this.paintDisplayerBackground(g, c);
        for (int i = 0; i < this.dataModel.size(); ++i) {
            TabData tabData = this.dataModel.getTab(i);
            int x = this.layoutModel.getX(i);
            int y = this.layoutModel.getY(i);
            int width = this.layoutModel.getW(i);
            int height = this.layoutModel.getH(i);
            String text = tabData.getText();
            if (!g.hitClip(x, y, width, height)) continue;
            this.paintTabBackground(g, i, x, y, width, height);
            this.paintTabContent(g, i, text, x, y, width, height);
            this.paintTabBorder(g, i, x, y, width, height);
        }
    }

    protected final TabDataModel getDataModel() {
        return this.dataModel;
    }

    public final TabLayoutModel getLayoutModel() {
        return this.layoutModel;
    }

    protected final TabDisplayer getDisplayer() {
        return this.displayer;
    }

    protected final SingleSelectionModel getSelectionModel() {
        return this.selectionModel;
    }

    public Controller getController() {
        return this.controller;
    }

    protected final boolean isSelected(int index) {
        if (index < 0) {
            return false;
        }
        return this.selectionModel.getSelectedIndex() == index;
    }

    protected final boolean isActive() {
        return this.displayer.isActive();
    }

    protected final boolean isFocused(int index) {
        if (index < 0) {
            return false;
        }
        return this.isSelected(index) && this.isActive();
    }

    @Override
    protected final SingleSelectionModel createSelectionModel() {
        return new DefaultTabSelectionModel(this.displayer.getModel());
    }

    @Override
    public int dropIndexOfPoint(Point p) {
        int result = 0;
        for (int i = 0; i < this.displayer.getModel().size(); ++i) {
            int x = this.getLayoutModel().getX(i);
            int w = this.getLayoutModel().getW(i);
            if (p.x < x || p.x > x + w) continue;
            if (i == this.displayer.getModel().size() - 1) {
                if (p.x > x + w / 2) {
                    result = this.displayer.getModel().size();
                    break;
                }
                result = i;
                break;
            }
            result = i;
            break;
        }
        return result;
    }

    @Override
    protected Font getTxtFont() {
        if (this.txtFont == null) {
            this.txtFont = (Font)UIManager.get("windowTitleFont");
            if (this.txtFont == null) {
                this.txtFont = new Font("Dialog", 0, 11);
            } else if (this.txtFont.isBold()) {
                this.txtFont = new Font(this.txtFont.getName(), 0, this.txtFont.getSize());
            }
        }
        return this.txtFont;
    }

    protected final FontMetrics getTxtFontMetrics() {
        if (this.fm == null) {
            TabDisplayer control = this.getDisplayer();
            this.fm = control.getFontMetrics(this.getTxtFont());
        }
        return this.fm;
    }

    protected abstract void paintTabContent(Graphics var1, int var2, String var3, int var4, int var5, int var6, int var7);

    protected abstract void paintTabBorder(Graphics var1, int var2, int var3, int var4, int var5, int var6);

    protected abstract void paintTabBackground(Graphics var1, int var2, int var3, int var4, int var5, int var6);

    protected void paintDisplayerBackground(Graphics g, JComponent c) {
        if (!this.isUseStretchingTabs()) {
            int x = 0;
            int width = c.getWidth();
            if (this.dataModel.size() > 0) {
                x += this.layoutModel.getX(this.dataModel.size() - 1);
                width -= (x += this.layoutModel.getW(this.dataModel.size() - 1) - 1);
            }
            this.paintTabBackground(g, -1, x, 0, width, c.getHeight());
            this.paintTabBorder(g, -1, x, 0, width, c.getHeight());
        }
    }

    @Override
    public void unregisterShortcuts(JComponent comp) {
        comp.getInputMap(1).remove(KeyStroke.getKeyStroke(8, 128));
        comp.getActionMap().remove("pinAction");
        comp.getInputMap(1).remove(KeyStroke.getKeyStroke(96, 128));
        comp.getActionMap().remove("transparencyAction");
    }

    @Override
    public void registerShortcuts(JComponent comp) {
        comp.getInputMap(1).put(KeyStroke.getKeyStroke(8, 128), "pinAction");
        comp.getActionMap().put("pinAction", this.pinAction);
        comp.getInputMap(1).put(KeyStroke.getKeyStroke(96, 128), "transparencyAction");
        comp.getActionMap().put("transparencyAction", new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent e) {
                AbstractViewTabDisplayerUI.this.shouldPerformAction("toggleTransparency", AbstractViewTabDisplayerUI.this.getSelectionModel().getSelectedIndex(), null);
            }
        });
    }

    @Override
    public Polygon getExactTabIndication(int index) {
        TabDisplayer control = this.getDisplayer();
        int height = control.getHeight();
        TabLayoutModel tlm = this.getLayoutModel();
        int tabXStart = tlm.getX(index);
        int tabXEnd = tabXStart + tlm.getW(index);
        int[] xpoints = new int[4];
        int[] ypoints = new int[4];
        xpoints[0] = tabXStart;
        ypoints[0] = 0;
        xpoints[1] = tabXEnd;
        ypoints[1] = 0;
        xpoints[2] = tabXEnd;
        ypoints[2] = height - 1;
        xpoints[3] = tabXStart;
        ypoints[3] = height - 1;
        return new EqualPolygon(xpoints, ypoints);
    }

    @Override
    public Polygon getInsertTabIndication(int index) {
        int tabXStart;
        int tabXEnd;
        EqualPolygon indication = new EqualPolygon();
        TabDisplayer control = this.getDisplayer();
        int height = control.getHeight();
        int width = control.getWidth();
        TabLayoutModel tlm = this.getLayoutModel();
        if (index == 0) {
            tabXStart = 0;
            tabXEnd = tlm.getW(0) / 2;
        } else if (index >= this.getDataModel().size()) {
            tabXStart = tlm.getX(index - 1) + tlm.getW(index - 1) / 2;
            tabXEnd = tabXStart + tlm.getW(index - 1);
            if (tabXEnd > width) {
                tabXEnd = width;
            }
        } else {
            tabXStart = tlm.getX(index - 1) + tlm.getW(index - 1) / 2;
            tabXEnd = tlm.getX(index) + tlm.getW(index) / 2;
        }
        indication.moveTo(tabXStart, 0);
        indication.lineTo(tabXEnd, 0);
        indication.lineTo(tabXEnd, height - 1);
        indication.lineTo(tabXStart, height - 1);
        return indication;
    }

    @Override
    public Image createImageOfTab(int index) {
        TabData td = this.displayer.getModel().getTab(index);
        JLabel lbl = new JLabel(td.getText());
        int width = lbl.getFontMetrics(lbl.getFont()).stringWidth(td.getText());
        int height = lbl.getFontMetrics(lbl.getFont()).getHeight();
        width = width + td.getIcon().getIconWidth() + 6;
        height = Math.max(height, td.getIcon().getIconHeight()) + 5;
        BufferedImage image = new BufferedImage(width, height, 2);
        Graphics2D g = image.createGraphics();
        g.setColor(lbl.getForeground());
        g.setFont(lbl.getFont());
        td.getIcon().paintIcon(lbl, g, 0, 0);
        g.drawString(td.getText(), 18, height / 2);
        return image;
    }

    @Override
    public Rectangle getTabRect(int index, Rectangle destination) {
        if (destination == null) {
            destination = new Rectangle();
        }
        if (index < 0 || index > this.displayer.getModel().size()) {
            destination.setBounds(0, 0, 0, 0);
            return destination;
        }
        destination.x = this.layoutModel.getX(index);
        destination.width = this.layoutModel.getW(index);
        destination.height = this.layoutModel.getH(index);
        destination.y = Math.min(0, this.displayer.getHeight() - destination.height);
        return destination;
    }

    @Override
    public int tabForCoordinate(Point p) {
        int max = this.displayer.getModel().size();
        if (max == 0 || p.y > this.displayer.getHeight() || p.y < 0 || p.x < 0 || p.x > this.displayer.getWidth()) {
            return -1;
        }
        for (int i = 0; i < max; ++i) {
            int left = this.layoutModel.getX(i);
            int right = left + this.layoutModel.getW(i);
            if (p.x <= left || p.x >= right) continue;
            return i;
        }
        return -1;
    }

    @Override
    public Dimension getMinimumSize(JComponent c) {
        int index = this.displayer.getSelectionModel().getSelectedIndex();
        TabDataModel model = this.displayer.getModel();
        if (index < 0 || index >= model.size()) {
            index = 0;
        }
        Dimension minSize = null;
        minSize = index >= model.size() ? new Dimension(100, 10) : new Dimension(this.layoutModel.getW(index), this.layoutModel.getH(index));
        minSize.width = Math.max(minSize.width, 100);
        minSize.height = Math.max(minSize.height, 10);
        return minSize;
    }

    protected int createRepaintPolicy() {
        return 93;
    }

    protected boolean isAttention(int tab) {
        if (tab < 0) {
            return false;
        }
        return (this.tabState.getState(tab) & 16384) != 0 || (this.tabState.getState(tab) & 131072) != 0;
    }

    protected boolean isHighlight(int tab) {
        if (tab < 0) {
            return false;
        }
        return (this.tabState.getState(tab) & 131072) != 0;
    }

    @Override
    protected void requestAttention(int tab) {
        this.tabState.addAlarmTab(tab);
    }

    @Override
    protected void cancelRequestAttention(int tab) {
        this.tabState.removeAlarmTab(tab);
    }

    @Override
    protected void setAttentionHighlight(int tab, boolean highlight) {
        if (highlight) {
            this.tabState.addHighlightTab(tab);
        } else {
            this.tabState.removeHighlightTab(tab);
        }
    }

    final boolean isUseStretchingTabs() {
        if (Boolean.getBoolean("winsys.stretching_view_tabs")) {
            return true;
        }
        if (this.displayer.getType() != 0) {
            return true;
        }
        WinsysInfoForTabbedContainer winsysInfo = this.displayer.getContainerWinsysInfo();
        if (null != winsysInfo && winsysInfo.isSlidedOutContainer()) {
            return true;
        }
        return null == winsysInfo || !winsysInfo.isModeSlidingEnabled();
    }

    int getModeButtonVerticalOffset() {
        return 0;
    }

    private final class PinButtonLayout
    implements LayoutManager {
        private PinButtonLayout() {
        }

        @Override
        public void addLayoutComponent(String name, Component comp) {
        }

        @Override
        public void removeLayoutComponent(Component comp) {
        }

        @Override
        public Dimension preferredLayoutSize(Container parent) {
            return parent.getPreferredSize();
        }

        @Override
        public Dimension minimumLayoutSize(Container parent) {
            return parent.getPreferredSize();
        }

        @Override
        public void layoutContainer(Container parent) {
            AbstractViewTabDisplayerUI.this.btnMinimizeMode.setBounds(0, 0, AbstractViewTabDisplayerUI.this.btnMinimizeMode.getIcon().getIconWidth(), AbstractViewTabDisplayerUI.this.btnMinimizeMode.getIcon().getIconHeight());
            AbstractViewTabDisplayerUI.this.btnMinimizeMode.setLocation(AbstractViewTabDisplayerUI.this.displayer.getWidth() - AbstractViewTabDisplayerUI.this.btnMinimizeMode.getWidth() - 5, (AbstractViewTabDisplayerUI.this.displayer.getHeight() - AbstractViewTabDisplayerUI.this.btnMinimizeMode.getHeight()) / 2 + AbstractViewTabDisplayerUI.this.getModeButtonVerticalOffset());
        }
    }

    private class PinAction
    extends AbstractAction {
        private PinAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (null != AbstractViewTabDisplayerUI.this.btnAutoHidePin) {
                AbstractViewTabDisplayerUI.this.btnAutoHidePin.performAction(null);
            }
        }
    }

    protected class Controller
    extends MouseAdapter
    implements MouseMotionListener,
    ChangeListener,
    PropertyChangeListener,
    ComplexListDataListener {
        private boolean selectionChanged;

        protected Controller() {
        }

        protected boolean shouldReact(MouseEvent e) {
            boolean isLeft = SwingUtilities.isLeftMouseButton(e);
            return isLeft;
        }

        @Override
        public void stateChanged(ChangeEvent ce) {
            AbstractViewTabDisplayerUI.this.displayer.repaint();
            AbstractViewTabDisplayerUI.this.showHideControlButtons();
        }

        @Override
        public void propertyChange(PropertyChangeEvent pce) {
            if ("active".equals(pce.getPropertyName())) {
                AbstractViewTabDisplayerUI.this.displayer.repaint();
            }
        }

        public boolean inControlButtonsRect(Point p) {
            Point p2;
            boolean res = false;
            if (null != AbstractViewTabDisplayerUI.this.controlButtons) {
                p2 = SwingUtilities.convertPoint(AbstractViewTabDisplayerUI.this.displayer, p, AbstractViewTabDisplayerUI.this.controlButtons);
                res |= AbstractViewTabDisplayerUI.this.controlButtons.contains(p2);
            }
            if (null != AbstractViewTabDisplayerUI.this.btnMinimizeMode) {
                p2 = SwingUtilities.convertPoint(AbstractViewTabDisplayerUI.this.displayer, p, AbstractViewTabDisplayerUI.this.btnMinimizeMode);
                res |= AbstractViewTabDisplayerUI.this.btnMinimizeMode.contains(p2);
            }
            return res;
        }

        @Override
        public void mousePressed(MouseEvent e) {
            boolean change;
            Point p = e.getPoint();
            int i = AbstractViewTabDisplayerUI.this.getLayoutModel().indexOfPoint(p.x, p.y);
            AbstractViewTabDisplayerUI.this.tabState.setPressed(i);
            SingleSelectionModel sel = AbstractViewTabDisplayerUI.this.getSelectionModel();
            boolean bl = this.selectionChanged = i != sel.getSelectedIndex();
            if ((i != -1 || !this.selectionChanged) && (change = AbstractViewTabDisplayerUI.this.shouldPerformAction("select", i, e))) {
                Component tc;
                AbstractViewTabDisplayerUI.this.getSelectionModel().setSelectedIndex(i);
                AbstractViewTabDisplayerUI.this.tabState.setSelected(i);
                Component component = tc = i >= 0 ? AbstractViewTabDisplayerUI.this.getDataModel().getTab(i).getComponent() : null;
                if (null != tc && tc instanceof TopComponent && !((TopComponent)tc).isAncestorOf(KeyboardFocusManager.getCurrentKeyboardFocusManager().getPermanentFocusOwner())) {
                    ((TopComponent)tc).requestActive();
                }
            }
            if (e.isPopupTrigger()) {
                AbstractViewTabDisplayerUI.this.shouldPerformAction("popup", i, e);
            }
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getClickCount() >= 2 && !e.isPopupTrigger()) {
                boolean change;
                SingleSelectionModel sel;
                Point p = e.getPoint();
                int i = AbstractViewTabDisplayerUI.this.getLayoutModel().indexOfPoint(p.x, p.y);
                boolean bl = this.selectionChanged = i != (sel = AbstractViewTabDisplayerUI.this.getSelectionModel()).getSelectedIndex();
                if ((i != -1 || !this.selectionChanged) && (change = AbstractViewTabDisplayerUI.this.shouldPerformAction("select", i, e))) {
                    AbstractViewTabDisplayerUI.this.getSelectionModel().setSelectedIndex(i);
                }
                if (i != -1 && e.getButton() == 1) {
                    AbstractViewTabDisplayerUI.this.shouldPerformAction("maximize", i, e);
                }
            } else if (e.getClickCount() == 1 && !e.isPopupTrigger() && e.getButton() == 2) {
                Point p = e.getPoint();
                int i = AbstractViewTabDisplayerUI.this.getLayoutModel().indexOfPoint(p.x, p.y);
                if (i >= 0) {
                    AbstractViewTabDisplayerUI.this.shouldPerformAction("close", i, e);
                }
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            AbstractViewTabDisplayerUI.this.tabState.setPressed(-1);
            Point p = e.getPoint();
            int i = AbstractViewTabDisplayerUI.this.getLayoutModel().indexOfPoint(p.x, p.y);
            if (e.isPopupTrigger()) {
                AbstractViewTabDisplayerUI.this.shouldPerformAction("popup", i, e);
            }
        }

        @Override
        public void indicesAdded(ComplexListDataEvent e) {
            AbstractViewTabDisplayerUI.this.tabState.indicesAdded(e);
        }

        @Override
        public void indicesRemoved(ComplexListDataEvent e) {
            AbstractViewTabDisplayerUI.this.tabState.indicesRemoved(e);
        }

        @Override
        public void indicesChanged(ComplexListDataEvent e) {
            AbstractViewTabDisplayerUI.this.tabState.indicesChanged(e);
        }

        @Override
        public void intervalAdded(ListDataEvent evt) {
            AbstractViewTabDisplayerUI.this.tabState.intervalAdded(evt);
        }

        @Override
        public void intervalRemoved(ListDataEvent evt) {
            AbstractViewTabDisplayerUI.this.tabState.intervalRemoved(evt);
        }

        @Override
        public void contentsChanged(ListDataEvent evt) {
            AbstractViewTabDisplayerUI.this.tabState.contentsChanged(evt);
        }

        @Override
        public void mouseDragged(MouseEvent e) {
        }

        @Override
        public void mouseMoved(MouseEvent e) {
        }
    }

    private class ViewTabState
    extends TabState {
        @Override
        public int getRepaintPolicy(int tab) {
            return AbstractViewTabDisplayerUI.this.createRepaintPolicy();
        }

        @Override
        public void repaintAllTabs() {
            AbstractViewTabDisplayerUI.this.displayer.repaint();
        }

        @Override
        public void repaintTab(int tab) {
            if (tab < 0 || tab >= AbstractViewTabDisplayerUI.this.displayer.getModel().size()) {
                return;
            }
            Rectangle r = AbstractViewTabDisplayerUI.this.getTabRect(tab, null);
            AbstractViewTabDisplayerUI.this.displayer.repaint(r);
        }

        @Override
        boolean isDisplayable() {
            return AbstractViewTabDisplayerUI.this.displayer.isDisplayable();
        }
    }

}

