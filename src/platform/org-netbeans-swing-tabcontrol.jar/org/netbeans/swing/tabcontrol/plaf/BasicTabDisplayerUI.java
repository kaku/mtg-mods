/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeListener;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SingleSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListDataEvent;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.event.ComplexListDataEvent;
import org.netbeans.swing.tabcontrol.plaf.AbstractTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.ColorUtil;
import org.netbeans.swing.tabcontrol.plaf.TabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.TabLayoutModel;
import org.netbeans.swing.tabcontrol.plaf.TabState;
import org.openide.windows.TopComponent;

public abstract class BasicTabDisplayerUI
extends AbstractTabDisplayerUI {
    protected TabState tabState = null;
    private static final boolean swingpainting = Boolean.getBoolean("nb.tabs.swingpainting");
    protected TabCellRenderer defaultRenderer = null;
    protected int repaintPolicy = 0;
    private Rectangle scratch = new Rectangle();
    private Rectangle scratch2 = new Rectangle();
    private Rectangle scratch3 = new Rectangle();
    private Point lastKnownMouseLocation = new Point();
    int pixelsToAdd = 0;

    public BasicTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
    }

    @Override
    protected void install() {
        super.install();
        this.tabState = this.createTabState();
        this.defaultRenderer = this.createDefaultRenderer();
        if (null != this.displayer.getContainerWinsysInfo()) {
            this.defaultRenderer.setShowCloseButton(this.displayer.getContainerWinsysInfo().isTopComponentClosingEnabled());
        }
        this.layoutModel.setPadding(this.defaultRenderer.getPadding());
        this.pixelsToAdd = this.defaultRenderer.getPixelsToAddToSelection();
        this.repaintPolicy = this.createRepaintPolicy();
        if (this.displayer.getSelectionModel().getSelectedIndex() != -1) {
            this.tabState.setSelected(this.displayer.getSelectionModel().getSelectedIndex());
            this.tabState.setActive(this.displayer.isActive());
        }
    }

    @Override
    protected void uninstall() {
        this.tabState = null;
        this.defaultRenderer = null;
        super.uninstall();
    }

    TabState getTabState() {
        return this.tabState;
    }

    protected TabState createTabState() {
        return new BasicTabState();
    }

    protected abstract TabCellRenderer createDefaultRenderer();

    public abstract Insets getTabAreaInsets();

    public TabCellRenderer getTabCellRenderer(int tab) {
        this.defaultRenderer.setShowCloseButton(this.displayer.isShowCloseButton());
        if (tab >= 0 && tab < this.displayer.getModel().size()) {
            TabData data = this.displayer.getModel().getTab(tab);
            boolean closingEnabled = true;
            if (data.getComponent() instanceof TopComponent) {
                closingEnabled = this.displayer.getContainerWinsysInfo().isTopComponentClosingEnabled((TopComponent)data.getComponent());
            }
            this.defaultRenderer.setShowCloseButton(this.displayer.isShowCloseButton() && closingEnabled);
        }
        return this.defaultRenderer;
    }

    protected final void getTabsVisibleArea(Rectangle rect) {
        Insets ins = this.getTabAreaInsets();
        rect.x = ins.left;
        rect.y = ins.top;
        rect.width = this.displayer.getWidth() - ins.right - ins.left;
        rect.height = this.displayer.getHeight() - ins.bottom - ins.top;
    }

    @Override
    protected MouseListener createMouseListener() {
        return new BasicDisplayerMouseListener();
    }

    @Override
    protected PropertyChangeListener createPropertyChangeListener() {
        return new BasicDisplayerPropertyChangeListener();
    }

    @Override
    public Polygon getExactTabIndication(int index) {
        Rectangle r = this.getTabRect(index, this.scratch);
        return this.getTabCellRenderer(index).getTabShape(this.tabState.getState(index), r);
    }

    @Override
    public Polygon getInsertTabIndication(int index) {
        Polygon p;
        if (index == this.getLastVisibleTab() + 1) {
            p = this.getExactTabIndication(index - 1);
            Rectangle r = this.getTabRect(index - 1, this.scratch);
            p.translate(r.width / 2, 0);
        } else {
            p = this.getExactTabIndication(index);
            Rectangle r = this.getTabRect(index, this.scratch);
            p.translate(- r.width / 2, 0);
        }
        return p;
    }

    @Override
    public int tabForCoordinate(Point p) {
        if (this.displayer.getModel().size() == 0) {
            return -1;
        }
        this.getTabsVisibleArea(this.scratch);
        if (!this.scratch.contains(p)) {
            return -1;
        }
        int tabIndex = this.layoutModel.indexOfPoint(p.x, p.y);
        if (tabIndex >= this.displayer.getModel().size()) {
            tabIndex = -1;
        }
        return tabIndex;
    }

    @Override
    public Rectangle getTabRect(int idx, Rectangle rect) {
        if (rect == null) {
            rect = new Rectangle();
        }
        if (idx < 0 || idx >= this.displayer.getModel().size()) {
            rect.height = 0;
            rect.width = 0;
            rect.y = 0;
            rect.x = 0;
            return rect;
        }
        rect.x = this.layoutModel.getX(idx);
        rect.y = this.layoutModel.getY(idx);
        rect.width = this.layoutModel.getW(idx);
        this.getTabsVisibleArea(this.scratch3);
        int maxPos = this.scratch.x + this.scratch3.width;
        if (rect.x > maxPos) {
            rect.width = 0;
        } else if (rect.x + rect.width > maxPos) {
            rect.width = maxPos - rect.x;
        }
        rect.height = this.layoutModel.getH(idx);
        this.getTabsVisibleArea(this.scratch2);
        if (rect.y + rect.height > this.scratch2.y + this.scratch2.height) {
            rect.height = this.scratch2.y + this.scratch2.height - rect.y;
        }
        if (rect.x + rect.width > this.scratch2.x + this.scratch2.width) {
            rect.width = this.scratch2.x + this.scratch2.width - rect.x;
        }
        return rect;
    }

    @Override
    public Image createImageOfTab(int index) {
        TabData td = this.displayer.getModel().getTab(index);
        JLabel lbl = new JLabel(td.getText());
        int width = lbl.getFontMetrics(lbl.getFont()).stringWidth(td.getText());
        int height = lbl.getFontMetrics(lbl.getFont()).getHeight();
        width = width + td.getIcon().getIconWidth() + 6;
        height = Math.max(height, td.getIcon().getIconHeight()) + 5;
        GraphicsConfiguration config = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
        BufferedImage image = config.createCompatibleImage(width, height);
        Graphics2D g = image.createGraphics();
        g.setColor(lbl.getForeground());
        g.setFont(lbl.getFont());
        td.getIcon().paintIcon(lbl, g, 0, 0);
        g.drawString(td.getText(), 18, height / 2);
        return image;
    }

    @Override
    public int dropIndexOfPoint(Point p) {
        Point p2 = this.toDropPoint(p);
        int start = this.getFirstVisibleTab();
        int end = this.getLastVisibleTab();
        for (int target = start; target <= end; ++target) {
            this.getTabRect(target, this.scratch);
            if (!this.scratch.contains(p2)) continue;
            if (target == end) {
                boolean flip;
                Object orientation = this.displayer.getClientProperty("orientation");
                boolean bl = flip = this.displayer.getType() == 2 && (orientation == TabDisplayer.ORIENTATION_EAST || orientation == TabDisplayer.ORIENTATION_WEST);
                if (flip ? p2.y > this.scratch.y + this.scratch.height / 2 : p2.x > this.scratch.x + this.scratch.width / 2) {
                    return target + 1;
                }
            }
            return target;
        }
        return -1;
    }

    protected boolean isAntialiased() {
        return ColorUtil.shouldAntialias();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public final void paint(Graphics g, JComponent c) {
        assert (c == this.displayer);
        ColorUtil.setupAntialiasing(g);
        this.paintBackground(g);
        int start = this.getFirstVisibleTab();
        if (start == -1 || !this.displayer.isShowing()) {
            return;
        }
        int stop = Math.min(this.getLastVisibleTab(), this.displayer.getModel().size() - 1);
        this.getTabsVisibleArea(this.scratch);
        if (g.hitClip(this.scratch.x, this.scratch.y, this.scratch.width, this.scratch.height)) {
            Shape s = g.getClip();
            try {
                if (s != null) {
                    Area a = new Area(s);
                    a.intersect(new Area(this.scratch.getBounds2D()));
                    g.setClip(a);
                } else {
                    g.setClip(this.scratch.x, this.scratch.y, this.scratch.width, this.scratch.height);
                }
                for (int i = start; i <= stop; ++i) {
                    int state;
                    this.getTabRect(i, this.scratch);
                    if (!g.hitClip(this.scratch.x, this.scratch.y, this.scratch.width + 1, this.scratch.height + 1) || ((state = this.tabState.getState(i)) & 64) != 0) continue;
                    TabCellRenderer ren = this.getTabCellRenderer(i);
                    TabData data = this.displayer.getModel().getTab(i);
                    if (this.isTabBusy(i)) {
                        state |= 65536;
                    }
                    JComponent renderer = ren.getRendererComponent(data, this.scratch, state);
                    renderer.setFont(this.displayer.getFont());
                    if (swingpainting) {
                        SwingUtilities.paintComponent(g, renderer, this.displayer, this.scratch);
                        continue;
                    }
                    try {
                        g.translate(this.scratch.x, this.scratch.y);
                        renderer.setBounds(this.scratch);
                        renderer.paint(g);
                    }
                    finally {
                        g.translate(- this.scratch.x, - this.scratch.y);
                    }
                }
            }
            finally {
                g.setClip(s);
            }
        }
        this.paintAfterTabs(g);
    }

    protected void paintBackground(Graphics g) {
    }

    protected void paintAfterTabs(Graphics g) {
    }

    protected int getFirstVisibleTab() {
        return this.displayer.getModel().size() > 0 ? 0 : -1;
    }

    protected int getLastVisibleTab() {
        return this.displayer.getModel().size() - 1;
    }

    @Override
    protected ChangeListener createSelectionListener() {
        return new BasicSelectionListener();
    }

    protected final Point getLastKnownMouseLocation() {
        return this.lastKnownMouseLocation;
    }

    protected void processMouseWheelEvent(MouseWheelEvent e) {
    }

    @Override
    protected final void requestAttention(int tab) {
        this.tabState.addAlarmTab(tab);
    }

    @Override
    protected final void cancelRequestAttention(int tab) {
        this.tabState.removeAlarmTab(tab);
    }

    @Override
    protected final void setAttentionHighlight(int tab, boolean highlight) {
        if (highlight) {
            this.tabState.addHighlightTab(tab);
        } else {
            this.tabState.removeHighlightTab(tab);
        }
    }

    @Override
    protected void modelChanged() {
        this.tabState.clearTransientStates();
        int idx = this.selectionModel.getSelectedIndex();
        this.tabState.setSelected(idx);
        this.tabState.pruneTabs(this.displayer.getModel().size());
        super.modelChanged();
    }

    protected int createRepaintPolicy() {
        return 93;
    }

    protected Rectangle getTabRectForRepaint(int tab, Rectangle rect) {
        return this.getTabRect(tab, rect);
    }

    @Override
    protected AbstractTabDisplayerUI.ModelListener createModelListener() {
        return new BasicModelListener();
    }

    protected class BasicModelListener
    extends AbstractTabDisplayerUI.ModelListener {
        protected BasicModelListener() {
        }

        @Override
        public void contentsChanged(ListDataEvent e) {
            super.contentsChanged(e);
            BasicTabDisplayerUI.this.tabState.contentsChanged(e);
        }

        @Override
        public void indicesAdded(ComplexListDataEvent e) {
            super.indicesAdded(e);
            BasicTabDisplayerUI.this.tabState.indicesAdded(e);
        }

        @Override
        public void indicesChanged(ComplexListDataEvent e) {
            BasicTabDisplayerUI.this.tabState.indicesChanged(e);
        }

        @Override
        public void indicesRemoved(ComplexListDataEvent e) {
            BasicTabDisplayerUI.this.tabState.indicesRemoved(e);
        }

        @Override
        public void intervalAdded(ListDataEvent e) {
            BasicTabDisplayerUI.this.tabState.intervalAdded(e);
        }

        @Override
        public void intervalRemoved(ListDataEvent e) {
            BasicTabDisplayerUI.this.tabState.intervalRemoved(e);
        }
    }

    protected class BasicSelectionListener
    implements ChangeListener {
        protected BasicSelectionListener() {
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            assert (e.getSource() == BasicTabDisplayerUI.this.selectionModel);
            int idx = BasicTabDisplayerUI.this.selectionModel.getSelectedIndex();
            BasicTabDisplayerUI.this.tabState.setSelected(idx >= 0 ? idx : -1);
            if (idx >= 0) {
                BasicTabDisplayerUI.this.makeTabVisible(BasicTabDisplayerUI.this.selectionModel.getSelectedIndex());
            }
        }
    }

    protected class BasicDisplayerMouseListener
    implements MouseListener,
    MouseMotionListener,
    MouseWheelListener {
        private int lastPressedTab;
        private long pressTime;

        protected BasicDisplayerMouseListener() {
            this.lastPressedTab = -1;
            this.pressTime = -1;
        }

        private int updateMouseLocation(MouseEvent e) {
            BasicTabDisplayerUI.access$1000((BasicTabDisplayerUI)BasicTabDisplayerUI.this).x = e.getX();
            BasicTabDisplayerUI.access$1000((BasicTabDisplayerUI)BasicTabDisplayerUI.this).y = e.getY();
            return BasicTabDisplayerUI.this.tabForCoordinate(BasicTabDisplayerUI.this.lastKnownMouseLocation);
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            int idx = this.updateMouseLocation(e);
            if (idx == -1) {
                return;
            }
            TabCellRenderer tcr = BasicTabDisplayerUI.this.getTabCellRenderer(idx);
            BasicTabDisplayerUI.this.getTabRect(idx, BasicTabDisplayerUI.this.scratch);
            int state = BasicTabDisplayerUI.this.tabState.getState(idx);
            this.potentialCommand(idx, e, state, tcr, BasicTabDisplayerUI.this.scratch);
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            this.mouseMoved(e);
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            int idx = this.updateMouseLocation(e);
            BasicTabDisplayerUI.this.tabState.setMouseInTabsArea(true);
            BasicTabDisplayerUI.this.tabState.setContainsMouse(idx);
        }

        @Override
        public void mouseExited(MouseEvent e) {
            this.updateMouseLocation(e);
            BasicTabDisplayerUI.this.tabState.setMouseInTabsArea(false);
            BasicTabDisplayerUI.this.tabState.setContainsMouse(-1);
            BasicTabDisplayerUI.this.tabState.setCloseButtonContainsMouse(-1);
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            int idx = this.updateMouseLocation(e);
            BasicTabDisplayerUI.this.tabState.setMouseInTabsArea(true);
            BasicTabDisplayerUI.this.tabState.setContainsMouse(idx);
            if (idx != -1) {
                TabCellRenderer tcr = BasicTabDisplayerUI.this.getTabCellRenderer(idx);
                BasicTabDisplayerUI.this.getTabRect(idx, BasicTabDisplayerUI.this.scratch);
                int state = BasicTabDisplayerUI.this.tabState.getState(idx);
                String s = tcr.getCommandAtPoint(e.getPoint(), state, BasicTabDisplayerUI.this.scratch);
                if ("close" == s) {
                    BasicTabDisplayerUI.this.tabState.setCloseButtonContainsMouse(idx);
                } else {
                    BasicTabDisplayerUI.this.tabState.setCloseButtonContainsMouse(-1);
                }
            } else {
                BasicTabDisplayerUI.this.tabState.setContainsMouse(-1);
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
            int idx = this.updateMouseLocation(e);
            BasicTabDisplayerUI.this.tabState.setPressed(idx);
            long time = e.getWhen();
            if (time - this.pressTime > 200) {
                this.lastPressedTab = idx;
            }
            this.pressTime = time;
            this.lastPressedTab = idx;
            if (idx != -1) {
                TabCellRenderer tcr = BasicTabDisplayerUI.this.getTabCellRenderer(idx);
                BasicTabDisplayerUI.this.getTabRect(idx, BasicTabDisplayerUI.this.scratch);
                int state = BasicTabDisplayerUI.this.tabState.getState(idx);
                String command = tcr.getCommandAtPoint(e.getPoint(), state, BasicTabDisplayerUI.this.scratch);
                if ("close" == command) {
                    BasicTabDisplayerUI.this.tabState.setCloseButtonContainsMouse(idx);
                    BasicTabDisplayerUI.this.tabState.setMousePressedInCloseButton(idx);
                    this.pressTime = -1;
                    this.lastPressedTab = -1;
                }
                this.potentialCommand(idx, e, state, tcr, BasicTabDisplayerUI.this.scratch);
            } else {
                BasicTabDisplayerUI.this.tabState.setMousePressedInCloseButton(-1);
                if (e.isPopupTrigger()) {
                    BasicTabDisplayerUI.this.displayer.repaint();
                    this.performCommand("popup", -1, e);
                }
            }
        }

        private void potentialCommand(int idx, MouseEvent e, int state, TabCellRenderer tcr, Rectangle bounds) {
            String command = tcr.getCommandAtPoint(e.getPoint(), state, bounds, e.getButton(), e.getID(), e.getModifiersEx());
            if (command == null || "select" == command) {
                if (e.isPopupTrigger()) {
                    BasicTabDisplayerUI.this.displayer.repaint();
                    this.performCommand("popup", idx, e);
                    return;
                }
                if (e.getID() == 500 && e.getClickCount() >= 2 && e.getButton() == 1) {
                    this.performCommand("maximize", idx, e);
                    return;
                }
            }
            if (command != null) {
                this.performCommand(command, this.lastPressedTab == -1 || this.lastPressedTab >= BasicTabDisplayerUI.this.displayer.getModel().size() ? idx : this.lastPressedTab, e);
            }
        }

        private void performCommand(String command, int idx, MouseEvent evt) {
            evt.consume();
            if ("select" == command) {
                boolean go;
                if (idx != BasicTabDisplayerUI.this.displayer.getSelectionModel().getSelectedIndex() && (go = BasicTabDisplayerUI.this.shouldPerformAction(command, idx, evt))) {
                    BasicTabDisplayerUI.this.selectionModel.setSelectedIndex(idx);
                }
            } else {
                boolean should;
                boolean bl = should = BasicTabDisplayerUI.this.shouldPerformAction(command, idx, evt) && BasicTabDisplayerUI.this.displayer.isShowCloseButton();
                if (should) {
                    if ("close" == command) {
                        BasicTabDisplayerUI.this.displayer.getModel().removeTab(idx);
                    } else if ("closeAll" == command) {
                        BasicTabDisplayerUI.this.displayer.getModel().removeTabs(0, BasicTabDisplayerUI.this.displayer.getModel().size());
                    } else if ("closeAllButThis" == command) {
                        int end;
                        int start;
                        if (idx != BasicTabDisplayerUI.this.displayer.getModel().size() - 1) {
                            start = idx + 1;
                            end = BasicTabDisplayerUI.this.displayer.getModel().size();
                            BasicTabDisplayerUI.this.displayer.getModel().removeTabs(start, end);
                        }
                        if (idx != 0) {
                            start = 0;
                            end = idx;
                            BasicTabDisplayerUI.this.displayer.getModel().removeTabs(start, end);
                        }
                    }
                }
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            int idx = this.updateMouseLocation(e);
            if (idx != -1) {
                TabCellRenderer tcr = BasicTabDisplayerUI.this.getTabCellRenderer(idx);
                BasicTabDisplayerUI.this.getTabRect(idx, BasicTabDisplayerUI.this.scratch);
                int state = BasicTabDisplayerUI.this.tabState.getState(idx);
                if ((state & 8) != 0 && (state & 2) != 0 || (state & 1) != 0) {
                    BasicTabDisplayerUI.this.makeTabVisible(idx);
                }
                this.potentialCommand(idx, e, state, tcr, BasicTabDisplayerUI.this.scratch);
            } else if (e.isPopupTrigger()) {
                BasicTabDisplayerUI.this.displayer.repaint();
                this.performCommand("popup", -1, e);
            }
            BasicTabDisplayerUI.this.tabState.setMouseInTabsArea(idx != -1);
            BasicTabDisplayerUI.this.tabState.setPressed(-1);
            BasicTabDisplayerUI.this.tabState.setMousePressedInCloseButton(-1);
        }

        @Override
        public final void mouseWheelMoved(MouseWheelEvent e) {
            this.updateMouseLocation(e);
            BasicTabDisplayerUI.this.processMouseWheelEvent(e);
        }
    }

    private class BasicDisplayerPropertyChangeListener
    extends AbstractTabDisplayerUI.DisplayerPropertyChangeListener {
        private BasicDisplayerPropertyChangeListener() {
        }

        @Override
        protected void activationChanged() {
            BasicTabDisplayerUI.this.tabState.setActive(BasicTabDisplayerUI.this.displayer.isActive());
        }
    }

    protected class BasicTabState
    extends TabState {
        protected BasicTabState() {
        }

        @Override
        public int getState(int tab) {
            if (BasicTabDisplayerUI.this.displayer.getModel().size() == 0) {
                return 64;
            }
            int result = super.getState(tab);
            if (tab == 0) {
                result |= 128;
            }
            if (tab == BasicTabDisplayerUI.this.displayer.getModel().size() - 1) {
                result |= 256;
            }
            return result;
        }

        @Override
        protected void repaintAllTabs() {
            BasicTabDisplayerUI.this.displayer.repaint();
        }

        @Override
        public int getRepaintPolicy(int tab) {
            return BasicTabDisplayerUI.this.repaintPolicy;
        }

        @Override
        protected void repaintTab(int tab) {
            if (tab == -1 || tab > BasicTabDisplayerUI.this.displayer.getModel().size()) {
                return;
            }
            BasicTabDisplayerUI.this.getTabRectForRepaint(tab, BasicTabDisplayerUI.this.scratch);
            BasicTabDisplayerUI.access$500((BasicTabDisplayerUI)BasicTabDisplayerUI.this).y = 0;
            BasicTabDisplayerUI.access$500((BasicTabDisplayerUI)BasicTabDisplayerUI.this).height = BasicTabDisplayerUI.this.displayer.getHeight();
            BasicTabDisplayerUI.this.displayer.repaint(BasicTabDisplayerUI.access$500((BasicTabDisplayerUI)BasicTabDisplayerUI.this).x, BasicTabDisplayerUI.access$500((BasicTabDisplayerUI)BasicTabDisplayerUI.this).y, BasicTabDisplayerUI.access$500((BasicTabDisplayerUI)BasicTabDisplayerUI.this).width, BasicTabDisplayerUI.access$500((BasicTabDisplayerUI)BasicTabDisplayerUI.this).height);
        }

        @Override
        boolean isDisplayable() {
            return BasicTabDisplayerUI.this.displayer.isDisplayable();
        }
    }

}

