/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.plaf.AbstractViewTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.BusyTabsSupport;
import org.netbeans.swing.tabcontrol.plaf.ColorUtil;
import org.netbeans.swing.tabcontrol.plaf.MetalEditorTabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.openide.awt.HtmlRenderer;

public final class MetalViewTabDisplayerUI
extends AbstractViewTabDisplayerUI {
    private static final int TXT_X_PAD = 5;
    private static final int ICON_X_LEFT_PAD = 5;
    private static final int ICON_X_RIGHT_PAD = 2;
    private static final int BUMP_X_PAD = 5;
    private static final int BUMP_Y_PAD = 4;
    private static Color inactBgColor;
    private static Color actBgColor;
    private static Color borderHighlight;
    private static Color borderShadow;
    private static Map<Integer, String[]> buttonIconPaths;
    private Dimension prefSize = new Dimension(100, 19);
    private Rectangle tempRect = new Rectangle();

    private MetalViewTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
    }

    public static ComponentUI createUI(JComponent c) {
        return new MetalViewTabDisplayerUI((TabDisplayer)c);
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        FontMetrics fm = this.getTxtFontMetrics();
        int height = fm == null ? 21 : fm.getAscent() + 2 * fm.getDescent() + 4;
        Insets insets = c.getInsets();
        this.prefSize.height = height + insets.bottom + insets.top;
        return this.prefSize;
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        super.paint(g, c);
        this.paintBottomBorder(g, c);
    }

    private void paintBottomBorder(Graphics g, JComponent c) {
        Color color = this.isActive() ? MetalViewTabDisplayerUI.getActBgColor() : MetalViewTabDisplayerUI.getInactBgColor();
        g.setColor(color);
        Rectangle bounds = c.getBounds();
        g.fillRect(1, bounds.height - 3, bounds.width - 1, 2);
        g.setColor(this.getBorderShadow());
        g.drawLine(1, bounds.height - 1, bounds.width - 1, bounds.height - 1);
    }

    @Override
    protected void paintTabContent(Graphics g, int index, String text, int x, int y, int width, int height) {
        boolean slidedOut = false;
        WinsysInfoForTabbedContainer winsysInfo = this.displayer.getContainerWinsysInfo();
        if (null != winsysInfo && winsysInfo.isSlidedOutContainer()) {
            slidedOut = false;
        }
        FontMetrics fm = this.getTxtFontMetrics();
        g.setFont(this.getTxtFont());
        int txtWidth = width;
        if (this.isSelected(index)) {
            int bumpWidth;
            Component buttons = this.getControlButtons();
            int buttonsWidth = 0;
            if (null != buttons) {
                Dimension buttonsSize = buttons.getPreferredSize();
                if (width < buttonsSize.width + 5 + 2) {
                    buttons.setVisible(false);
                } else {
                    buttons.setVisible(true);
                    buttonsWidth = buttonsSize.width + 5 + 2;
                    txtWidth = width - (buttonsWidth + 5);
                    buttons.setLocation(x + txtWidth + 5 + 5, y + (height - buttonsSize.height) / 2 - 1);
                }
            }
            if (this.isTabBusy(index) && !slidedOut) {
                Icon busyIcon = BusyTabsSupport.getDefault().getBusyIcon(this.isSelected(index));
                txtWidth -= busyIcon.getIconWidth() - 3 - 5;
                busyIcon.paintIcon(this.displayer, g, x + 5, y + (height - busyIcon.getIconHeight()) / 2);
                x += busyIcon.getIconWidth() + 3;
            }
            if ((bumpWidth = width - (5 + (txtWidth = (int)HtmlRenderer.renderString((String)text, (Graphics)g, (int)(x + 5), (int)(height - fm.getDescent() - 4), (int)txtWidth, (int)height, (Font)this.getTxtFont(), (Color)UIManager.getColor("textText"), (int)1, (boolean)true)) + 5 + buttonsWidth)) > 0) {
                this.paintBump(index, g, x + 5 + txtWidth + 5, y + 4, bumpWidth, height - 8);
            }
        } else {
            if (this.isTabBusy(index) && !slidedOut) {
                Icon busyIcon = BusyTabsSupport.getDefault().getBusyIcon(this.isSelected(index));
                txtWidth -= busyIcon.getIconWidth() - 3 - 5;
                busyIcon.paintIcon(this.displayer, g, x + 5, y + (height - busyIcon.getIconHeight()) / 2);
                x += busyIcon.getIconWidth() + 3;
            }
            txtWidth = width - 10;
            HtmlRenderer.renderString((String)text, (Graphics)g, (int)(x + 5), (int)(height - fm.getDescent() - 4), (int)txtWidth, (int)height, (Font)this.getTxtFont(), (Color)UIManager.getColor("textText"), (int)1, (boolean)true);
        }
    }

    @Override
    protected void paintTabBorder(Graphics g, int index, int x, int y, int width, int height) {
        Color highlight = this.getBorderHighlight();
        Color shadow = this.getBorderShadow();
        boolean isSelected = this.isSelected(index);
        boolean isFirst = index == 0;
        boolean isLast = index == this.getDataModel().size() - 1;
        g.translate(x, y);
        g.setColor(shadow);
        if (!isFirst) {
            g.drawLine(0, 0, 0, height - 5);
        }
        if (!isSelected) {
            g.drawLine(1, height - 5, isLast ? width - 1 : width, height - 5);
        }
        g.setColor(highlight);
        g.drawLine(1, 0, width - 1, 0);
        if (isFirst) {
            g.drawLine(0, 0, 0, height - 2);
        }
        if (!isSelected) {
            g.drawLine(0, height - 4, isLast ? width - 1 : width, height - 4);
        }
        if (isLast) {
            g.setColor(shadow);
            g.drawLine(width - 1, 0, width - 1, height - 5);
        }
        g.translate(- x, - y);
    }

    @Override
    protected void paintTabBackground(Graphics g, int index, int x, int y, int width, int height) {
        boolean selected = this.isSelected(index);
        boolean highlighted = selected && this.isActive();
        boolean attention = this.isAttention(index);
        if (highlighted && !attention) {
            g.setColor(MetalViewTabDisplayerUI.getActBgColor());
            g.fillRect(x, y, width, height - 3);
        } else if (attention) {
            g.setColor(MetalEditorTabCellRenderer.ATTENTION_COLOR);
            g.fillRect(x, y, width, height - 3);
        } else {
            g.setColor(MetalViewTabDisplayerUI.getInactBgColor());
            g.fillRect(x, y, width, height - 3);
        }
    }

    @Override
    int getModeButtonVerticalOffset() {
        return -1;
    }

    private void paintBump(int index, Graphics g, int x, int y, int width, int height) {
        ColorUtil.paintViewTabBump(g, x, y, width, height, this.isFocused(index) ? 4 : 2);
    }

    static Color getInactBgColor() {
        if (inactBgColor == null && (MetalViewTabDisplayerUI.inactBgColor = (Color)UIManager.get("inactiveCaption")) == null) {
            inactBgColor = new Color(204, 204, 204);
        }
        return inactBgColor;
    }

    static Color getActBgColor() {
        if (actBgColor == null && (MetalViewTabDisplayerUI.actBgColor = (Color)UIManager.get("activeCaption")) == null) {
            actBgColor = new Color(204, 204, 255);
        }
        return actBgColor;
    }

    private Color getBorderHighlight() {
        if (borderHighlight == null) {
            borderHighlight = MetalViewTabDisplayerUI.getInactBgColor().brighter();
        }
        return borderHighlight;
    }

    private Color getBorderShadow() {
        if (borderShadow == null) {
            borderShadow = MetalViewTabDisplayerUI.getInactBgColor().darker();
        }
        return borderShadow;
    }

    private static void initIcons() {
        if (null == buttonIconPaths) {
            String[] iconPaths;
            buttonIconPaths = new HashMap<Integer, String[]>(7);
            iconPaths = new String[]{"org/openide/awt/resources/metal_bigclose_enabled.png", "org/openide/awt/resources/metal_bigclose_pressed.png", iconPaths[0], "org/openide/awt/resources/metal_bigclose_rollover.png"};
            buttonIconPaths.put(1, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/metal_slideright_enabled.png", "org/netbeans/swing/tabcontrol/resources/metal_slideright_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/metal_slideright_rollover.png"};
            buttonIconPaths.put(6, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/metal_slideleft_enabled.png", "org/netbeans/swing/tabcontrol/resources/metal_slideleft_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/metal_slideleft_rollover.png"};
            buttonIconPaths.put(5, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/metal_slidebottom_enabled.png", "org/netbeans/swing/tabcontrol/resources/metal_slidebottom_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/metal_slidebottom_rollover.png"};
            buttonIconPaths.put(7, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/metal_pin_enabled.png", "org/netbeans/swing/tabcontrol/resources/metal_pin_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/metal_pin_rollover.png"};
            buttonIconPaths.put(2, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/metal_restore_group_enabled.png", "org/netbeans/swing/tabcontrol/resources/metal_restore_group_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/metal_restore_group_rollover.png"};
            buttonIconPaths.put(11, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/metal_minimize_enabled.png", "org/netbeans/swing/tabcontrol/resources/metal_minimize_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/metal_minimize_rollover.png"};
            buttonIconPaths.put(12, iconPaths);
        }
    }

    @Override
    public Icon getButtonIcon(int buttonId, int buttonState) {
        Icon res = null;
        MetalViewTabDisplayerUI.initIcons();
        String[] paths = buttonIconPaths.get(buttonId);
        if (null != paths && buttonState >= 0 && buttonState < paths.length) {
            res = TabControlButtonFactory.getIcon(paths[buttonState]);
        }
        return res;
    }
}

