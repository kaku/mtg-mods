/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Dimension;
import java.awt.Insets;
import javax.swing.JComponent;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.plaf.TabLayoutModel;

final class ViewTabLayoutModel
implements TabLayoutModel {
    private TabDataModel model;
    private JComponent renderTarget;

    public ViewTabLayoutModel(TabDataModel model, JComponent renderTarget) {
        this.model = model;
        this.renderTarget = renderTarget;
    }

    @Override
    public int getH(int index) {
        this.checkIndex(index);
        Insets insets = this.renderTarget.getInsets();
        return this.renderTarget.getHeight() - (insets.bottom + insets.top);
    }

    @Override
    public int getW(int index) {
        int nextX;
        this.checkIndex(index);
        int x = this.computeX(index);
        if (index < this.model.size() - 1) {
            nextX = this.computeX(index + 1);
        } else {
            Insets insets = this.renderTarget.getInsets();
            nextX = this.renderTarget.getWidth() - insets.right;
        }
        return nextX - x;
    }

    @Override
    public int getX(int index) {
        this.checkIndex(index);
        return this.computeX(index);
    }

    @Override
    public int getY(int index) {
        this.checkIndex(index);
        return this.renderTarget.getInsets().top;
    }

    @Override
    public int indexOfPoint(int x, int y) {
        Insets insets = this.renderTarget.getInsets();
        int contentWidth = this.renderTarget.getWidth() - (insets.left + insets.right);
        int contentHeight = this.renderTarget.getHeight() - (insets.bottom + insets.top);
        if (y < insets.top || y > contentHeight || x < insets.left || x > contentWidth) {
            return -1;
        }
        int size = this.model.size();
        for (int i = 0; i < size; ++i) {
            int diff = x - this.computeX(i);
            if (diff < 0 || diff >= this.getW(i)) continue;
            return i;
        }
        return -1;
    }

    @Override
    public int dropIndexOfPoint(int x, int y) {
        Insets insets = this.renderTarget.getInsets();
        int contentWidth = this.renderTarget.getWidth() - (insets.left + insets.right);
        int contentHeight = this.renderTarget.getHeight() - (insets.bottom + insets.top);
        if (y < insets.top || y > contentHeight || x < insets.left || x > contentWidth) {
            return -1;
        }
        int size = this.model.size();
        float tabWidth = (float)contentWidth / (float)size;
        x = x - insets.left + (int)tabWidth / 2;
        int result = (int)((float)x / tabWidth);
        return Math.min(result, this.model.size());
    }

    @Override
    public void setPadding(Dimension d) {
    }

    private void checkIndex(int index) {
        int size = this.model.size();
        if (index < 0 || index >= size) {
            throw new IllegalArgumentException("Index out of valid scope 0.." + (size - 1) + ": " + index);
        }
    }

    private int computeX(int index) {
        Insets insets = this.renderTarget.getInsets();
        int contentWidth = this.renderTarget.getWidth() - (insets.left + insets.right);
        return contentWidth * index / this.model.size() + insets.left;
    }
}

