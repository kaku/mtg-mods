/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.SingleSelectionModel;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.plaf.BasicScrollingTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.GtkEditorTabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.TabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;

public final class GtkEditorTabDisplayerUI
extends BasicScrollingTabDisplayerUI {
    private static Map<Integer, String[]> buttonIconPaths;

    public GtkEditorTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
    }

    public static ComponentUI createUI(JComponent c) {
        return new GtkEditorTabDisplayerUI((TabDisplayer)c);
    }

    @Override
    public Rectangle getTabRect(int idx, Rectangle rect) {
        Rectangle r = super.getTabRect(idx, rect);
        r.y = 0;
        r.height = this.displayer.getHeight();
        return r;
    }

    @Override
    public void install() {
        super.install();
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        int prefHeight = 28;
        Graphics2D g = BasicScrollingTabDisplayerUI.getOffscreenGraphics();
        if (g != null) {
            FontMetrics fm = g.getFontMetrics(this.displayer.getFont());
            Insets ins = this.getTabAreaInsets();
            prefHeight = fm.getHeight() + ins.top + ins.bottom + 12;
        }
        return new Dimension(this.displayer.getWidth(), prefHeight);
    }

    @Override
    protected void paintAfterTabs(Graphics g) {
        Rectangle bounds = this.displayer.getBounds();
        int lineY = bounds.y + bounds.height - 1;
        int sel = this.displayer.getSelectionModel().getSelectedIndex();
        Color shadowC = UIManager.getColor("controlShadow");
        Color color = shadowC = shadowC != null ? shadowC : Color.DARK_GRAY;
        if (sel != -1) {
            Color controlC = UIManager.getColor("control");
            controlC = controlC != null ? controlC : Color.GRAY;
            Rectangle tabRect = new Rectangle();
            this.displayer.getTabRect(sel, tabRect);
            g.setColor(shadowC);
            g.drawLine(bounds.x, lineY, bounds.x + tabRect.x - 1, lineY);
            g.drawLine(bounds.x + tabRect.x + tabRect.width, lineY, bounds.x + bounds.width - 1, lineY);
            g.setColor(controlC);
            g.drawLine(bounds.x + tabRect.x, lineY, bounds.x + tabRect.x + tabRect.width - 1, lineY);
        } else {
            g.setColor(shadowC);
            g.drawLine(bounds.x, lineY, bounds.x + bounds.width - 1, lineY);
        }
    }

    @Override
    protected TabCellRenderer createDefaultRenderer() {
        return new GtkEditorTabCellRenderer();
    }

    private static void initIcons() {
        if (null == buttonIconPaths) {
            buttonIconPaths = new HashMap<Integer, String[]>(7);
            String[] iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/gtk_scrollleft_enabled.png";
            iconPaths[2] = iconPaths[0];
            iconPaths[3] = iconPaths[0];
            iconPaths[1] = iconPaths[0];
            buttonIconPaths.put(9, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/gtk_scrollright_enabled.png";
            iconPaths[2] = iconPaths[0];
            iconPaths[3] = iconPaths[0];
            iconPaths[1] = iconPaths[0];
            buttonIconPaths.put(10, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/gtk_popup_enabled.png";
            iconPaths[2] = iconPaths[0];
            iconPaths[3] = iconPaths[0];
            iconPaths[1] = iconPaths[0];
            buttonIconPaths.put(8, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/gtk_maximize_enabled.png";
            iconPaths[2] = iconPaths[0];
            iconPaths[3] = iconPaths[0];
            iconPaths[1] = iconPaths[0];
            buttonIconPaths.put(3, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/gtk_restore_enabled.png";
            iconPaths[2] = iconPaths[0];
            iconPaths[3] = iconPaths[0];
            iconPaths[1] = iconPaths[0];
            buttonIconPaths.put(4, iconPaths);
        }
    }

    @Override
    public Icon getButtonIcon(int buttonId, int buttonState) {
        Icon res = null;
        GtkEditorTabDisplayerUI.initIcons();
        String[] paths = buttonIconPaths.get(buttonId);
        if (null != paths && buttonState >= 0 && buttonState < paths.length) {
            res = TabControlButtonFactory.getIcon(paths[buttonState]);
        }
        return res;
    }

    @Override
    protected Rectangle getControlButtonsRectangle(Container parent) {
        Component c = this.getControlButtons();
        return new Rectangle(parent.getWidth() - c.getWidth() - 4, 4, c.getWidth(), c.getHeight());
    }

    @Override
    public Insets getTabAreaInsets() {
        Insets retValue = super.getTabAreaInsets();
        retValue.right += 4;
        ++retValue.bottom;
        return retValue;
    }
}

