/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.openide.windows.WindowManager
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Component;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.SingleSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.ToolTipManager;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.TabListPopupAction;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.event.TabActionEvent;
import org.netbeans.swing.tabcontrol.plaf.TabControlButton;
import org.openide.util.ImageUtilities;
import org.openide.windows.WindowManager;

public class TabControlButtonFactory {
    private TabControlButtonFactory() {
    }

    public static Icon getIcon(String iconPath) {
        ImageIcon res = ImageUtilities.loadImageIcon((String)iconPath, (boolean)true);
        if (null == res) {
            Logger.getLogger(TabControlButtonFactory.class.getName()).log(Level.INFO, "Cannot find button icon: " + iconPath);
        }
        return res;
    }

    public static TabControlButton createCloseButton(TabDisplayer displayer) {
        return new CloseButton(displayer);
    }

    public static TabControlButton createCloseGroupButton(TabDisplayer displayer) {
        return new CloseGroupButton(displayer);
    }

    public static TabControlButton createSlidePinButton(TabDisplayer displayer) {
        return new SlidePinButton(displayer);
    }

    public static TabControlButton createSlideGroupButton(TabDisplayer displayer) {
        return new SlideGroupButton(displayer);
    }

    public static TabControlButton createRestoreGroupButton(TabDisplayer displayer, String groupName) {
        return new RestoreGroupButton(displayer, groupName);
    }

    public static TabControlButton createMaximizeRestoreButton(TabDisplayer displayer, boolean showBorder) {
        return new MaximizeRestoreButton(displayer, showBorder);
    }

    public static TabControlButton createScrollLeftButton(TabDisplayer displayer, Action scrollAction, boolean showBorder) {
        TimerButton button = new TimerButton(9, displayer, scrollAction, showBorder);
        button.setToolTipText(ResourceBundle.getBundle("org/netbeans/swing/tabcontrol/plaf/Bundle").getString("Tip_Scroll_Documents_Left"));
        return button;
    }

    public static TabControlButton createScrollRightButton(TabDisplayer displayer, Action scrollAction, boolean showBorder) {
        TimerButton button = new TimerButton(10, displayer, scrollAction, showBorder);
        button.setToolTipText(ResourceBundle.getBundle("org/netbeans/swing/tabcontrol/plaf/Bundle").getString("Tip_Scroll_Documents_Right"));
        return button;
    }

    public static TabControlButton createDropDownButton(TabDisplayer displayer, boolean showBorder) {
        return new DropDownButton(displayer, showBorder);
    }

    private static Component getActiveTab(TabDisplayer displayer) {
        Component res = null;
        int selIndex = displayer.getSelectionModel().getSelectedIndex();
        if (selIndex >= 0) {
            TabData tab = displayer.getModel().getTab(selIndex);
            res = tab.getComponent();
        }
        return res;
    }

    private static class DropDownButton
    extends TabControlButton {
        private boolean forcePressedIcon = false;

        public DropDownButton(TabDisplayer displayer, boolean showBorder) {
            super(8, displayer, showBorder);
            this.setAction(new TabListPopupAction(displayer));
            this.setToolTipText(ResourceBundle.getBundle("org/netbeans/swing/tabcontrol/plaf/Bundle").getString("Tip_Show_Opened_Documents_List"));
        }

        @Override
        protected void processMouseEvent(MouseEvent me) {
            super.processMouseEvent(me);
            if (this.isEnabled() && me.getID() == 501) {
                this.forcePressedIcon = true;
                this.repaint();
                this.getAction().actionPerformed(new ActionEvent(this, 1001, "pressed"));
            } else if (this.isEnabled() && me.getID() == 502) {
                this.forcePressedIcon = false;
                this.repaint();
            }
        }

        @Override
        protected String getTabActionCommand(ActionEvent e) {
            return null;
        }

        @Override
        void performAction(ActionEvent e) {
        }

        @Override
        public Icon getRolloverIcon() {
            if (this.forcePressedIcon) {
                return this.getPressedIcon();
            }
            return super.getRolloverIcon();
        }

        @Override
        public Icon getIcon() {
            if (this.forcePressedIcon) {
                return this.getPressedIcon();
            }
            return super.getIcon();
        }
    }

    private static class TimerButton
    extends TabControlButton
    implements ActionListener {
        Timer timer = null;
        int count = 0;

        public TimerButton(int buttonId, TabDisplayer displayer, Action a, boolean showBorder) {
            super(buttonId, displayer, showBorder);
            this.setAction(a);
        }

        private Timer getTimer() {
            if (this.timer == null) {
                this.timer = new Timer(400, this);
                this.timer.setRepeats(true);
            }
            return this.timer;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            ++this.count;
            if (this.count > 2) {
                if (this.count > 5) {
                    this.timer.setDelay(75);
                } else {
                    this.timer.setDelay(200);
                }
            }
            this.performAction();
        }

        private void performAction() {
            if (!this.isEnabled()) {
                this.stopTimer();
                return;
            }
            this.getAction().actionPerformed(new ActionEvent(this, 1001, this.getActionCommand()));
        }

        private void startTimer() {
            Timer t = this.getTimer();
            if (t.isRunning()) {
                return;
            }
            this.repaint();
            t.setDelay(400);
            t.start();
        }

        private void stopTimer() {
            if (this.timer != null) {
                this.timer.stop();
            }
            this.repaint();
            this.count = 0;
        }

        @Override
        protected void processMouseEvent(MouseEvent me) {
            if (this.isEnabled() && me.getID() == 501) {
                this.startTimer();
            } else if (me.getID() == 502) {
                this.stopTimer();
            }
            super.processMouseEvent(me);
        }

        @Override
        protected void processFocusEvent(FocusEvent fe) {
            super.processFocusEvent(fe);
            if (fe.getID() == 1005) {
                this.stopTimer();
            }
        }

        @Override
        protected String getTabActionCommand(ActionEvent e) {
            return null;
        }
    }

    private static class MaximizeRestoreButton
    extends TabControlButton {
        public MaximizeRestoreButton(TabDisplayer displayer, boolean showBorder) {
            super(-1, displayer, showBorder);
            ToolTipManager toolTipManager = ToolTipManager.sharedInstance();
            toolTipManager.registerComponent(this);
        }

        @Override
        protected String getTabActionCommand(ActionEvent e) {
            return "maximize";
        }

        @Override
        protected int getButtonId() {
            WinsysInfoForTabbedContainer winsysInfo;
            int retValue = 3;
            Component currentTab = TabControlButtonFactory.getActiveTab(this.getTabDisplayer());
            if (null != currentTab && null != (winsysInfo = this.getTabDisplayer().getContainerWinsysInfo()) && winsysInfo.inMaximizedMode(currentTab)) {
                retValue = 4;
            }
            return retValue;
        }

        @Override
        public String getToolTipText() {
            if (this.getButtonId() == 3) {
                return ResourceBundle.getBundle("org/netbeans/swing/tabcontrol/plaf/Bundle").getString("Tip_Maximize_Window");
            }
            return ResourceBundle.getBundle("org/netbeans/swing/tabcontrol/plaf/Bundle").getString("Tip_Restore_Window");
        }
    }

    private static class RestoreGroupButton
    extends TabControlButton {
        private final String groupName;
        private static boolean useCustomUI = true;

        public RestoreGroupButton(TabDisplayer displayer, String groupName) {
            super(displayer);
            assert (null != groupName);
            this.groupName = groupName;
            ToolTipManager toolTipManager = ToolTipManager.sharedInstance();
            toolTipManager.registerComponent(this);
        }

        @Override
        protected String getTabActionCommand(ActionEvent e) {
            return "restoreGroup";
        }

        @Override
        protected int getButtonId() {
            return 11;
        }

        @Override
        protected TabActionEvent createTabActionEvent(ActionEvent e) {
            TabActionEvent res = super.createTabActionEvent(e);
            res.setGroupName(this.groupName);
            return res;
        }

        @Override
        public String getToolTipText() {
            return ResourceBundle.getBundle("org/netbeans/swing/tabcontrol/plaf/Bundle").getString("Tip_Restore_Window_Group");
        }
    }

    private static class SlideGroupButton
    extends TabControlButton {
        public SlideGroupButton(TabDisplayer displayer) {
            super(displayer);
            ToolTipManager toolTipManager = ToolTipManager.sharedInstance();
            toolTipManager.registerComponent(this);
        }

        @Override
        protected String getTabActionCommand(ActionEvent e) {
            return "minimizeGroup";
        }

        @Override
        protected int getButtonId() {
            return 12;
        }

        @Override
        public String getToolTipText() {
            return ResourceBundle.getBundle("org/netbeans/swing/tabcontrol/plaf/Bundle").getString("Tip_Minimize_Window_Group");
        }

        @Override
        public void addNotify() {
            boolean isFloating;
            super.addNotify();
            Window w = SwingUtilities.getWindowAncestor(this.displayer);
            boolean bl = isFloating = w != WindowManager.getDefault().getMainWindow();
            if (isFloating) {
                this.setVisible(false);
            }
        }
    }

    private static class SlidePinButton
    extends TabControlButton {
        public SlidePinButton(TabDisplayer displayer) {
            super(displayer);
            ToolTipManager toolTipManager = ToolTipManager.sharedInstance();
            toolTipManager.registerComponent(this);
        }

        @Override
        protected String getTabActionCommand(ActionEvent e) {
            if (this.getButtonId() == 2) {
                return "disableAutoHide";
            }
            return "enableAutoHide";
        }

        @Override
        protected int getButtonId() {
            WinsysInfoForTabbedContainer winsysInfo;
            int retValue = 2;
            Component currentTab = TabControlButtonFactory.getActiveTab(this.getTabDisplayer());
            if (null != currentTab && null != (winsysInfo = this.getTabDisplayer().getContainerWinsysInfo())) {
                Object orientation = winsysInfo.getOrientation(currentTab);
                if (TabDisplayer.ORIENTATION_EAST.equals(orientation)) {
                    retValue = 6;
                } else if (TabDisplayer.ORIENTATION_WEST.equals(orientation)) {
                    retValue = 5;
                } else if (TabDisplayer.ORIENTATION_SOUTH.equals(orientation)) {
                    retValue = 7;
                }
            }
            return retValue;
        }

        @Override
        public String getToolTipText() {
            if (this.getButtonId() == 2) {
                return ResourceBundle.getBundle("org/netbeans/swing/tabcontrol/plaf/Bundle").getString("Tip_Pin");
            }
            return ResourceBundle.getBundle("org/netbeans/swing/tabcontrol/plaf/Bundle").getString("Tip_Minimize_Window");
        }

        @Override
        public void addNotify() {
            boolean isFloating;
            super.addNotify();
            Window w = SwingUtilities.getWindowAncestor(this.displayer);
            boolean bl = isFloating = w != WindowManager.getDefault().getMainWindow();
            if (isFloating) {
                this.setVisible(false);
            }
        }
    }

    private static class CloseGroupButton
    extends TabControlButton {
        public CloseGroupButton(TabDisplayer displayer) {
            super(1, displayer);
            this.setToolTipText(ResourceBundle.getBundle("org/netbeans/swing/tabcontrol/plaf/Bundle").getString("Tip_Close_Window_Group"));
        }

        @Override
        protected String getTabActionCommand(ActionEvent e) {
            return "closeGroup";
        }
    }

    private static class CloseButton
    extends TabControlButton {
        public CloseButton(TabDisplayer displayer) {
            super(1, displayer);
            this.setToolTipText(ResourceBundle.getBundle("org/netbeans/swing/tabcontrol/plaf/Bundle").getString("Tip_Close_Window"));
        }

        @Override
        protected String getTabActionCommand(ActionEvent e) {
            return "close";
        }
    }

}

