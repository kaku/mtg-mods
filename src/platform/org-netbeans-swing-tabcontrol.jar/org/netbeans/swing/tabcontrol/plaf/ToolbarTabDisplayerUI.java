/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 *  org.openide.util.Utilities
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.util.Arrays;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.SingleSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.plaf.AbstractTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.ColorUtil;
import org.netbeans.swing.tabcontrol.plaf.DefaultTabLayoutModel;
import org.netbeans.swing.tabcontrol.plaf.EqualPolygon;
import org.netbeans.swing.tabcontrol.plaf.TabLayoutModel;
import org.openide.awt.HtmlRenderer;
import org.openide.util.Utilities;

public class ToolbarTabDisplayerUI
extends AbstractTabDisplayerUI {
    private JToolBar toolbar = null;
    private static final Border buttonBorder;
    private static final boolean isMac;
    private ButtonGroup bg = new ButtonGroup();
    private static int fontHeight;
    private static int ascent;
    private static final boolean isAqua;

    public ToolbarTabDisplayerUI(TabDisplayer disp) {
        super(disp);
    }

    public static ComponentUI createUI(JComponent jc) {
        return new ToolbarTabDisplayerUI((TabDisplayer)jc);
    }

    @Override
    protected TabLayoutModel createLayoutModel() {
        return null;
    }

    @Override
    protected void install() {
        this.toolbar = new TabToolbar();
        this.toolbar.setLayout(new AutoGridLayout());
        this.toolbar.setFloatable(false);
        this.toolbar.setRollover(true);
        if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
            this.toolbar.setBackground(UIManager.getColor("NbExplorerView.background"));
        }
        this.displayer.setLayout(new BorderLayout());
        this.displayer.add((Component)this.toolbar, "Center");
        if (this.displayer.getModel() != null && this.displayer.getModel().size() > 0) {
            this.syncButtonsWithModel();
        }
    }

    @Override
    protected void modelChanged() {
        if (this.syncButtonsWithModel() && this.displayer.getParent() != null) {
            ((JComponent)this.displayer.getParent()).revalidate();
        }
    }

    @Override
    protected MouseListener createMouseListener() {
        return null;
    }

    private IndexButton findButtonFor(int index) {
        Component[] c = this.toolbar.getComponents();
        for (int i = 0; i < c.length; ++i) {
            if (!(c[i] instanceof IndexButton) || ((IndexButton)c[i]).getIndex() != index) continue;
            return (IndexButton)c[i];
        }
        return null;
    }

    @Override
    public void requestAttention(int tab) {
    }

    @Override
    public void cancelRequestAttention(int tab) {
    }

    @Override
    protected ChangeListener createSelectionListener() {
        return new ChangeListener(){
            private int lastKnownSelection;

            @Override
            public void stateChanged(ChangeEvent ce) {
                int selection = ToolbarTabDisplayerUI.this.selectionModel.getSelectedIndex();
                if (selection != this.lastKnownSelection) {
                    IndexButton last;
                    if (this.lastKnownSelection != -1 && (last = ToolbarTabDisplayerUI.this.findButtonFor(this.lastKnownSelection)) != null) {
                        last.getModel().setSelected(false);
                    }
                    if (selection != -1) {
                        IndexButton current = ToolbarTabDisplayerUI.this.findButtonFor(selection);
                        if (ToolbarTabDisplayerUI.this.toolbar.getComponentCount() == 0) {
                            ToolbarTabDisplayerUI.this.syncButtonsWithModel();
                        }
                        if (current != null) {
                            current.getModel().setSelected(true);
                        }
                    }
                }
                this.lastKnownSelection = selection;
            }
        };
    }

    @Override
    public Polygon getExactTabIndication(int index) {
        IndexButton jb = this.findButtonFor(index);
        if (jb != null) {
            return new EqualPolygon(jb.getBounds());
        }
        return new EqualPolygon(new Rectangle());
    }

    @Override
    public Polygon getInsertTabIndication(int index) {
        return this.getExactTabIndication(index);
    }

    @Override
    public Rectangle getTabRect(int index, Rectangle destination) {
        destination.setBounds(this.findButtonFor(index).getBounds());
        return destination;
    }

    @Override
    public int tabForCoordinate(Point p) {
        Point p1 = SwingUtilities.convertPoint(this.displayer, p, this.toolbar);
        Component c = this.toolbar.getComponentAt(p1);
        if (c instanceof IndexButton) {
            return ((IndexButton)c).getIndex();
        }
        return -1;
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        return this.toolbar.getPreferredSize();
    }

    @Override
    public Dimension getMinimumSize(JComponent c) {
        return this.toolbar.getMinimumSize();
    }

    private boolean syncButtonsWithModel() {
        int selIdx;
        boolean result;
        assert (SwingUtilities.isEventDispatchThread());
        int expected = this.displayer.getModel().size();
        int actual = this.toolbar.getComponentCount();
        boolean bl = result = actual != expected;
        if (result) {
            int i;
            if (expected > actual) {
                for (i = actual; i < expected; ++i) {
                    this.toolbar.add(new IndexButton());
                }
            } else if (expected < actual) {
                for (i = expected; i < actual; ++i) {
                    this.toolbar.remove(this.toolbar.getComponentCount() - 1);
                }
            }
        }
        if ((selIdx = this.selectionModel.getSelectedIndex()) != -1) {
            this.findButtonFor(selIdx).setSelected(true);
        }
        if (result) {
            this.displayer.revalidate();
            this.displayer.doLayout();
            this.displayer.repaint();
        }
        return result;
    }

    @Override
    public Icon getButtonIcon(int buttonId, int buttonState) {
        return null;
    }

    static /* synthetic */ boolean access$1500() {
        return isAqua;
    }

    static {
        isMac = "Aqua".equals(UIManager.getLookAndFeel().getID());
        Border b = (Border)UIManager.get("nb.tabbutton.border");
        if (b == null) {
            JToolBar toolbar = new JToolBar();
            JButton button = new JButton();
            toolbar.setRollover(true);
            toolbar.add(button);
            b = button.getBorder();
            toolbar.remove(button);
        }
        buttonBorder = b;
        fontHeight = -1;
        ascent = -1;
        isAqua = "Aqua".equals(UIManager.getLookAndFeel().getID());
    }

    static class TabToolbar
    extends JToolBar {
        TabToolbar() {
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Color color = g.getColor();
            g.setColor(UIManager.getColor("controlLtHighlight"));
            g.drawLine(0, 0, this.getWidth(), 0);
            g.drawLine(0, 0, 0, this.getHeight() - 1);
            g.setColor(UIManager.getColor("controlShadow"));
            g.drawLine(0, this.getHeight() - 1, this.getWidth(), this.getHeight() - 1);
            g.setColor(color);
        }
    }

    static class AutoGridLayout
    implements LayoutManager {
        private int h_margin_left = ToolbarTabDisplayerUI.access$1500() ? 0 : 2;
        private int h_margin_right = ToolbarTabDisplayerUI.access$1500() ? 0 : 1;
        private int v_margin_top = ToolbarTabDisplayerUI.access$1500() ? 0 : 2;
        private int v_margin_bottom = ToolbarTabDisplayerUI.access$1500() ? 0 : 3;
        private int h_gap = ToolbarTabDisplayerUI.access$1500() ? 0 : 1;
        private int v_gap = ToolbarTabDisplayerUI.access$1500() ? 0 : 1;

        AutoGridLayout() {
        }

        @Override
        public void addLayoutComponent(String name, Component comp) {
        }

        @Override
        public void removeLayoutComponent(Component comp) {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public Dimension preferredLayoutSize(Container parent) {
            Object object = parent.getTreeLock();
            synchronized (object) {
                int containerWidth = parent.getWidth();
                if (containerWidth <= 0 && parent.getParent() != null) {
                    containerWidth = parent.getParent().getWidth();
                }
                int count = parent.getComponentCount();
                if (containerWidth <= 0 || count == 0) {
                    int cumulatedWidth = 0;
                    int height = 0;
                    for (int i = 0; i < count; ++i) {
                        Dimension size = parent.getComponent(i).getPreferredSize();
                        cumulatedWidth += size.width;
                        if (i + 1 < count) {
                            cumulatedWidth += this.h_gap;
                        }
                        if (size.height <= height) continue;
                        height = size.height;
                    }
                    return new Dimension(cumulatedWidth += this.h_margin_left + this.h_margin_right, height += this.v_margin_top + this.v_margin_bottom);
                }
                int columnWidth = 0;
                int rowHeight = 0;
                for (int i = 0; i < count; ++i) {
                    Dimension size = parent.getComponent(i).getPreferredSize();
                    if (size.width > columnWidth) {
                        columnWidth = size.width;
                    }
                    if (size.height <= rowHeight) continue;
                    rowHeight = size.height;
                }
                int columnCount = 0;
                int w = this.h_margin_left + columnWidth + this.h_margin_right;
                while ((w += this.h_gap + columnWidth) <= containerWidth && ++columnCount < count) {
                }
                int rowCount = count / columnCount + (count % columnCount > 0 ? 1 : 0);
                int prefHeight = this.v_margin_top + rowCount * rowHeight + (rowCount - 1) * this.v_gap + this.v_margin_bottom;
                Dimension result = new Dimension(containerWidth, prefHeight);
                return result;
            }
        }

        @Override
        public Dimension minimumLayoutSize(Container parent) {
            return new Dimension(this.h_margin_left + this.h_margin_right, this.v_margin_top + this.v_margin_bottom);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void layoutContainer(Container parent) {
            Object object = parent.getTreeLock();
            synchronized (object) {
                int lastRowEmpty;
                int roundedRowCount;
                int count = parent.getComponentCount();
                if (count == 0) {
                    return;
                }
                int columnWidth = 0;
                int rowHeight = 0;
                for (int i = 0; i < count; ++i) {
                    Dimension size = parent.getComponent(i).getPreferredSize();
                    if (size.width > columnWidth) {
                        columnWidth = size.width;
                    }
                    if (size.height <= rowHeight) continue;
                    rowHeight = size.height;
                }
                int containerWidth = parent.getWidth();
                int columnCount = 0;
                int w = this.h_margin_left + columnWidth + this.h_margin_right;
                while ((w += this.h_gap + columnWidth) <= containerWidth && ++columnCount < count) {
                }
                if (count % columnCount > 0 && (lastRowEmpty = columnCount - count % columnCount) > (roundedRowCount = count / columnCount)) {
                    columnCount -= lastRowEmpty / (roundedRowCount + 1);
                }
                if (count > columnCount) {
                    columnWidth = (containerWidth - this.h_margin_left - this.h_margin_right - (columnCount - 1) * this.h_gap) / columnCount;
                }
                if (columnWidth < 0) {
                    columnWidth = 0;
                }
                int col = 0;
                int row = 0;
                for (int i2 = 0; i2 < count; ++i2) {
                    parent.getComponent(i2).setBounds(this.h_margin_left + col * (columnWidth + this.h_gap), this.v_margin_top + row * (rowHeight + this.v_gap), columnWidth, rowHeight);
                    if (++col < columnCount) continue;
                    col = 0;
                    ++row;
                }
            }
        }
    }

    public final class IndexButton
    extends JToggleButton
    implements ActionListener {
        private String lastKnownText;

        public IndexButton() {
            this.lastKnownText = null;
            this.addActionListener(this);
            this.setFont(ToolbarTabDisplayerUI.this.displayer.getFont());
            this.setFocusable(false);
            if (isMac) {
                this.putClientProperty("JButton.buttonType", "square");
            } else {
                this.setBorder(buttonBorder);
                this.setMargin(new Insets(0, 3, 0, 3));
            }
            this.setRolloverEnabled(true);
        }

        @Override
        public void addNotify() {
            super.addNotify();
            ToolTipManager.sharedInstance().registerComponent(this);
            ToolbarTabDisplayerUI.this.bg.add(this);
        }

        @Override
        public void removeNotify() {
            super.removeNotify();
            ToolTipManager.sharedInstance().unregisterComponent(this);
            ToolbarTabDisplayerUI.this.bg.remove(this);
        }

        public boolean isActive() {
            return ToolbarTabDisplayerUI.this.displayer.isActive();
        }

        @Override
        public String getText() {
            return " ";
        }

        public String doGetText() {
            int idx = this.getIndex();
            if (idx == -1) {
                return "";
            }
            if (this.getIndex() >= ToolbarTabDisplayerUI.this.displayer.getModel().size()) {
                return "This tab doesn't exist.";
            }
            this.lastKnownText = ToolbarTabDisplayerUI.this.displayer.getModel().getTab(idx).getText();
            return this.lastKnownText;
        }

        @Override
        public Dimension getPreferredSize() {
            Dimension result = super.getPreferredSize();
            String s = this.doGetText();
            int w = DefaultTabLayoutModel.textWidth(s, this.getFont());
            result.width += w;
            if (Utilities.isMac()) {
                result.height -= 3;
                result.width -= 5;
            }
            return result;
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            ColorUtil.setupAntialiasing(g);
            String s = this.doGetText();
            Insets ins = this.getInsets();
            int x = ins.left;
            int w = this.getWidth() - (ins.left + ins.right);
            int h = this.getHeight();
            int txtW = DefaultTabLayoutModel.textWidth(s, this.getFont());
            if (txtW < w) {
                x += w / 2 - txtW / 2;
            }
            if (fontHeight == -1) {
                FontMetrics fm = g.getFontMetrics(this.getFont());
                fontHeight = fm.getHeight();
                ascent = fm.getMaxAscent();
            }
            int y = ins.top + ascent + ((this.getHeight() - (ins.top + ins.bottom)) / 2 - fontHeight / 2);
            HtmlRenderer.renderString((String)s, (Graphics)g, (int)x, (int)y, (int)w, (int)h, (Font)this.getFont(), (Color)this.getForeground(), (int)1, (boolean)true);
        }

        @Override
        public String getToolTipText() {
            return ToolbarTabDisplayerUI.this.displayer.getModel().getTab(this.getIndex()).getTooltip();
        }

        @Override
        public final void actionPerformed(ActionEvent e) {
            ToolbarTabDisplayerUI.this.selectionModel.setSelectedIndex(this.getIndex());
        }

        public int getIndex() {
            if (this.getParent() != null) {
                return Arrays.asList(this.getParent().getComponents()).indexOf(this);
            }
            return -1;
        }

        @Override
        public Icon getIcon() {
            return null;
        }

        final boolean checkChanged() {
            boolean result = false;
            String txt = this.lastKnownText;
            String nu = this.doGetText();
            if (nu != txt) {
                this.firePropertyChange("text", this.lastKnownText, this.doGetText());
                result = true;
            }
            if (result) {
                this.firePropertyChange("preferredSize", null, null);
            }
            return result;
        }
    }

}

