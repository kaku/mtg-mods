/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import javax.swing.JComponent;
import org.netbeans.swing.tabcontrol.TabData;

public interface TabCellRenderer {
    public JComponent getRendererComponent(TabData var1, Rectangle var2, int var3);

    public String getCommandAtPoint(Point var1, int var2, Rectangle var3);

    public String getCommandAtPoint(Point var1, int var2, Rectangle var3, int var4, int var5, int var6);

    public Polygon getTabShape(int var1, Rectangle var2);

    public Dimension getPadding();

    public int getPixelsToAddToSelection();

    public Color getSelectedBackground();

    public Color getSelectedActivatedBackground();

    public boolean isShowCloseButton();

    public void setShowCloseButton(boolean var1);
}

