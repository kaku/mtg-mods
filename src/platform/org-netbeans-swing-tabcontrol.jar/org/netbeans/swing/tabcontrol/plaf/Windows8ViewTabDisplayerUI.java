/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.event.TabActionEvent;
import org.netbeans.swing.tabcontrol.plaf.AbstractWinViewTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.ColorUtil;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;

public final class Windows8ViewTabDisplayerUI
extends AbstractWinViewTabDisplayerUI {
    private static boolean colorsReady = false;
    private static Color unselFillUpperC;
    private static Color unselFillLowerC;
    private static Color selFillC;
    private static Color focusFillUpperC;
    private static Color focusFillLowerC;
    private static Color mouseOverFillUpperC;
    private static Color mouseOverFillLowerC;
    private static Color attentionFillUpperC;
    private static Color attentionFillLowerC;
    private static Map<Integer, String[]> buttonIconPaths;

    private Windows8ViewTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
    }

    public static ComponentUI createUI(JComponent c) {
        return new Windows8ViewTabDisplayerUI((TabDisplayer)c);
    }

    @Override
    public void installUI(JComponent c) {
        super.installUI(c);
        Windows8ViewTabDisplayerUI.initColors();
    }

    @Override
    protected void paintTabBackground(Graphics g, int index, int x, int y, int width, int height) {
        boolean selected = this.isSelected(index);
        boolean focused = selected && this.isActive();
        boolean attention = this.isAttention(index);
        boolean mouseOver = this.isMouseOver(index);
        Windows8ViewTabDisplayerUI.paintTabBackground((Graphics2D)g, x, y += 2, width, height -= 2, selected, focused, attention, mouseOver);
    }

    @Override
    int getButtonYPadding() {
        return 1;
    }

    static void paintTabBackground(Graphics2D g2d, int x, int y, int width, int height, boolean selected, boolean focused, boolean attention, boolean mouseOver) {
        Windows8ViewTabDisplayerUI.initColors();
        if (focused && !attention) {
            g2d.setPaint(ColorUtil.getGradientPaint(x, y, focusFillUpperC, x, y + height, focusFillLowerC));
        } else if (selected && !attention) {
            g2d.setColor(selFillC);
        } else if (mouseOver && !attention) {
            g2d.setPaint(ColorUtil.getGradientPaint(x, y, mouseOverFillUpperC, x, y + height, mouseOverFillLowerC));
        } else if (attention) {
            g2d.setPaint(ColorUtil.getGradientPaint(x, y, attentionFillUpperC, x, y + height, attentionFillLowerC));
        } else {
            g2d.setPaint(ColorUtil.getGradientPaint(x, y, unselFillUpperC, x, y + height, unselFillLowerC));
        }
        g2d.fillRect(x, y, width, height);
    }

    private static void initColors() {
        if (!colorsReady) {
            selFillC = UIManager.getColor("tab_sel_fill");
            focusFillUpperC = UIManager.getColor("tab_focus_fill_upper");
            focusFillLowerC = UIManager.getColor("tab_focus_fill_lower");
            unselFillUpperC = UIManager.getColor("tab_unsel_fill_upper");
            unselFillLowerC = UIManager.getColor("tab_unsel_fill_lower");
            mouseOverFillUpperC = UIManager.getColor("tab_mouse_over_fill_upper");
            mouseOverFillLowerC = UIManager.getColor("tab_mouse_over_fill_lower");
            attentionFillUpperC = UIManager.getColor("tab_attention_fill_upper");
            attentionFillLowerC = UIManager.getColor("tab_attention_fill_lower");
            colorsReady = true;
        }
    }

    private static void initIcons() {
        if (null == buttonIconPaths) {
            String[] iconPaths;
            buttonIconPaths = new HashMap<Integer, String[]>(7);
            iconPaths = new String[]{"org/openide/awt/resources/win8_bigclose_enabled.png", "org/openide/awt/resources/win8_bigclose_pressed.png", iconPaths[0], "org/openide/awt/resources/win8_bigclose_rollover.png"};
            buttonIconPaths.put(1, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/win8_pin_enabled.png", "org/netbeans/swing/tabcontrol/resources/win8_pin_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/win8_pin_rollover.png"};
            buttonIconPaths.put(2, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/win8_restore_group_enabled.png", "org/netbeans/swing/tabcontrol/resources/win8_restore_group_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/win8_restore_group_rollover.png"};
            buttonIconPaths.put(11, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/win8_minimize_enabled.png", "org/netbeans/swing/tabcontrol/resources/win8_minimize_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/win8_minimize_rollover.png"};
            buttonIconPaths.put(12, iconPaths);
        }
    }

    @Override
    public Icon getButtonIcon(int buttonId, int buttonState) {
        Icon res = null;
        Windows8ViewTabDisplayerUI.initIcons();
        String[] paths = buttonIconPaths.get(buttonId);
        if (null != paths && buttonState >= 0 && buttonState < paths.length) {
            res = TabControlButtonFactory.getIcon(paths[buttonState]);
        }
        if (null == res) {
            return super.getButtonIcon(buttonId, buttonState);
        }
        return res;
    }
}

