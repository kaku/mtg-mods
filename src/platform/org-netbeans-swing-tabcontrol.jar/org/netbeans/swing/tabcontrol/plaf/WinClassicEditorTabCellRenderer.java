/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Polygon;
import java.awt.Rectangle;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.UIManager;
import org.netbeans.swing.tabcontrol.plaf.AbstractTabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.BusyTabsSupport;
import org.netbeans.swing.tabcontrol.plaf.ColorUtil;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.netbeans.swing.tabcontrol.plaf.TabPainter;

final class WinClassicEditorTabCellRenderer
extends AbstractTabCellRenderer {
    private static final TabPainter leftClip = new WinClassicLeftClipPainter();
    private static final TabPainter rightClip = new WinClassicRightClipPainter();
    private static final TabPainter normal = new WinClassicPainter();
    private static final Color GTK_TABBED_PANE_BACKGROUND_1 = new Color(255, 255, 255);
    static final Color ATTENTION_COLOR = new Color(255, 238, 120);
    private static boolean isGenericUI = !"Windows".equals(UIManager.getLookAndFeel().getID());
    private static final Insets INSETS = new Insets(0, 2, 0, 10);

    public WinClassicEditorTabCellRenderer() {
        super(leftClip, normal, rightClip, new Dimension(28, 32));
    }

    @Override
    public Color getSelectedForeground() {
        return UIManager.getColor("textText");
    }

    @Override
    public Color getForeground() {
        return this.getSelectedForeground();
    }

    @Override
    public int getPixelsToAddToSelection() {
        return 4;
    }

    @Override
    protected int getCaptionYAdjustment() {
        return 0;
    }

    @Override
    public Dimension getPadding() {
        Dimension d = super.getPadding();
        d.width = this.isShowCloseButton() && !Boolean.getBoolean("nb.tabs.suppressCloseButton") ? 28 : 20;
        return d;
    }

    @Override
    protected void paintIconAndText(Graphics g) {
        if (this.isBusy()) {
            this.setIcon(BusyTabsSupport.getDefault().getBusyIcon(this.isSelected()));
        }
        super.paintIconAndText(g);
    }

    private static final Color getSelGradientColor() {
        if ("GTK".equals(UIManager.getLookAndFeel().getID())) {
            return GTK_TABBED_PANE_BACKGROUND_1;
        }
        return UIManager.getColor("winclassic_tab_sel_gradient");
    }

    private static final Color getSelGradientColor2() {
        return UIManager.getColor("TabbedPane.background");
    }

    private static class WinClassicRightClipPainter
    implements TabPainter {
        private WinClassicRightClipPainter() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return INSETS;
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        @Override
        public Polygon getInteriorPolygon(Component c) {
            WinClassicEditorTabCellRenderer ren = (WinClassicEditorTabCellRenderer)c;
            Insets ins = this.getBorderInsets(c);
            Polygon p = new Polygon();
            int x = 0;
            int y = isGenericUI ? 0 : 1;
            int width = c.getWidth();
            int height = ren.isSelected() ? c.getHeight() + 2 : c.getHeight() - 1;
            p.addPoint(x, y + ins.top + 2);
            p.addPoint(x + 2, y + ins.top);
            p.addPoint(x + width - 1, y + ins.top);
            p.addPoint(x + width - 1, y + height - 1);
            p.addPoint(x, y + height - 1);
            return p;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            WinClassicEditorTabCellRenderer ren = (WinClassicEditorTabCellRenderer)c;
            Polygon p = this.getInteriorPolygon(c);
            g.setColor(ren.isSelected() ? UIManager.getColor("controlLtHighlight") : UIManager.getColor("controlHighlight"));
            int[] xpoints = p.xpoints;
            int[] ypoints = p.ypoints;
            g.drawLine(xpoints[0], ypoints[0], xpoints[p.npoints - 1], ypoints[p.npoints - 1]);
            for (int i = 0; i < p.npoints - 1; ++i) {
                g.drawLine(xpoints[i], ypoints[i], xpoints[i + 1], ypoints[i + 1]);
                if (ren.isSelected() && i == p.npoints - 4) {
                    g.setColor(ren.isActive() ? UIManager.getColor("Table.selectionBackground") : UIManager.getColor("control"));
                } else if (i == p.npoints - 4) break;
                if (i == p.npoints - 3) break;
            }
        }

        @Override
        public void paintInterior(Graphics g, Component c) {
            boolean wantGradient;
            WinClassicEditorTabCellRenderer ren = (WinClassicEditorTabCellRenderer)c;
            boolean bl = wantGradient = ren.isSelected() && ren.isActive() || (ren.isClipLeft() || ren.isClipRight()) && ren.isPressed();
            if (wantGradient) {
                ((Graphics2D)g).setPaint(ColorUtil.getGradientPaint(0.0f, 0.0f, WinClassicEditorTabCellRenderer.getSelGradientColor(), ren.getWidth(), 0.0f, WinClassicEditorTabCellRenderer.getSelGradientColor2()));
            } else if (!ren.isAttention()) {
                g.setColor(ren.isSelected() ? UIManager.getColor("TabbedPane.background") : UIManager.getColor("tab_unsel_fill"));
            } else {
                g.setColor(WinClassicEditorTabCellRenderer.ATTENTION_COLOR);
            }
            Polygon p = this.getInteriorPolygon(c);
            g.fillPolygon(p);
        }

        @Override
        public boolean supportsCloseButton(JComponent renderer) {
            return false;
        }

        @Override
        public void getCloseButtonRectangle(JComponent jc, Rectangle rect, Rectangle bounds) {
            rect.setBounds(-20, -20, 0, 0);
        }
    }

    private static class WinClassicLeftClipPainter
    implements TabPainter {
        private WinClassicLeftClipPainter() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return INSETS;
        }

        @Override
        public Polygon getInteriorPolygon(Component c) {
            WinClassicEditorTabCellRenderer ren = (WinClassicEditorTabCellRenderer)c;
            Insets ins = this.getBorderInsets(c);
            Polygon p = new Polygon();
            int x = -3;
            int y = isGenericUI ? 0 : 1;
            int width = c.getWidth() + 3;
            int height = ren.isSelected() ? c.getHeight() + 2 : c.getHeight() - 1;
            p.addPoint(x, y + ins.top + 2);
            p.addPoint(x + 2, y + ins.top);
            p.addPoint(x + width - 3, y + ins.top);
            p.addPoint(x + width - 1, y + ins.top + 2);
            p.addPoint(x + width - 1, y + height - 1);
            p.addPoint(x, y + height - 1);
            return p;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            WinClassicEditorTabCellRenderer ren = (WinClassicEditorTabCellRenderer)c;
            Polygon p = this.getInteriorPolygon(c);
            g.setColor(ren.isSelected() ? UIManager.getColor("controlLtHighlight") : UIManager.getColor("controlHighlight"));
            int[] xpoints = p.xpoints;
            int[] ypoints = p.ypoints;
            g.drawLine(xpoints[0], ypoints[0], xpoints[p.npoints - 1], ypoints[p.npoints - 1]);
            for (int i = 0; i < p.npoints - 1; ++i) {
                g.drawLine(xpoints[i], ypoints[i], xpoints[i + 1], ypoints[i + 1]);
                if (i != p.npoints - 4) continue;
                g.setColor(ren.isSelected() ? UIManager.getColor("controlDkShadow") : UIManager.getColor("controlShadow"));
                g.drawLine(xpoints[i] + 1, ypoints[i] + 1, xpoints[i] + 2, ypoints[i] + 2);
            }
        }

        @Override
        public void paintInterior(Graphics g, Component c) {
            boolean wantGradient;
            WinClassicEditorTabCellRenderer ren = (WinClassicEditorTabCellRenderer)c;
            boolean bl = wantGradient = ren.isSelected() && ren.isActive() || (ren.isClipLeft() || ren.isClipRight()) && ren.isPressed();
            if (wantGradient) {
                ((Graphics2D)g).setPaint(ColorUtil.getGradientPaint(0.0f, 0.0f, WinClassicEditorTabCellRenderer.getSelGradientColor(), ren.getWidth(), 0.0f, WinClassicEditorTabCellRenderer.getSelGradientColor2()));
            } else if (!ren.isAttention()) {
                g.setColor(ren.isSelected() ? UIManager.getColor("TabbedPane.background") : UIManager.getColor("tab_unsel_fill"));
            } else {
                g.setColor(WinClassicEditorTabCellRenderer.ATTENTION_COLOR);
            }
            Polygon p = this.getInteriorPolygon(c);
            g.fillPolygon(p);
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        @Override
        public void getCloseButtonRectangle(JComponent jc, Rectangle rect, Rectangle bounds) {
            rect.setBounds(-20, -20, 0, 0);
        }

        @Override
        public boolean supportsCloseButton(JComponent renderer) {
            return false;
        }
    }

    private static class WinClassicPainter
    implements TabPainter {
        private WinClassicPainter() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return INSETS;
        }

        @Override
        public Polygon getInteriorPolygon(Component c) {
            WinClassicEditorTabCellRenderer ren = (WinClassicEditorTabCellRenderer)c;
            Insets ins = this.getBorderInsets(c);
            Polygon p = new Polygon();
            int x = ren.isLeftmost() ? 1 : 0;
            int y = isGenericUI ? 0 : 1;
            int width = ren.isLeftmost() ? c.getWidth() - 1 : c.getWidth();
            int height = ren.isSelected() ? c.getHeight() + 2 : c.getHeight() - 1;
            p.addPoint(x, y + ins.top + 2);
            p.addPoint(x + 2, y + ins.top);
            p.addPoint(x + width - 3, y + ins.top);
            p.addPoint(x + width - 1, y + ins.top + 2);
            p.addPoint(x + width - 1, y + height - 2);
            p.addPoint(x, y + height - 2);
            return p;
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            WinClassicEditorTabCellRenderer ren = (WinClassicEditorTabCellRenderer)c;
            Polygon p = this.getInteriorPolygon(c);
            g.setColor(ren.isSelected() ? UIManager.getColor("controlLtHighlight") : UIManager.getColor("controlHighlight"));
            int[] xpoints = p.xpoints;
            int[] ypoints = p.ypoints;
            g.drawLine(xpoints[0], ypoints[0], xpoints[p.npoints - 1], ypoints[p.npoints - 1]);
            for (int i = 0; i < p.npoints - 1; ++i) {
                g.drawLine(xpoints[i], ypoints[i], xpoints[i + 1], ypoints[i + 1]);
                if (i != p.npoints - 4) continue;
                g.setColor(ren.isSelected() ? UIManager.getColor("controlDkShadow") : UIManager.getColor("controlShadow"));
                g.drawLine(xpoints[i] + 1, ypoints[i] + 1, xpoints[i] + 2, ypoints[i] + 2);
            }
        }

        @Override
        public void paintInterior(Graphics g, Component c) {
            boolean wantGradient;
            WinClassicEditorTabCellRenderer ren = (WinClassicEditorTabCellRenderer)c;
            boolean bl = wantGradient = ren.isSelected() && ren.isActive() || (ren.isClipLeft() || ren.isClipRight()) && ren.isPressed();
            if (wantGradient) {
                ((Graphics2D)g).setPaint(ColorUtil.getGradientPaint(0.0f, 0.0f, WinClassicEditorTabCellRenderer.getSelGradientColor(), ren.getWidth(), 0.0f, WinClassicEditorTabCellRenderer.getSelGradientColor2()));
            } else if (!ren.isAttention()) {
                g.setColor(ren.isSelected() ? UIManager.getColor("TabbedPane.background") : UIManager.getColor("tab_unsel_fill"));
            } else {
                g.setColor(WinClassicEditorTabCellRenderer.ATTENTION_COLOR);
            }
            Polygon p = this.getInteriorPolygon(c);
            g.fillPolygon(p);
            if (!this.supportsCloseButton((JComponent)c)) {
                return;
            }
            this.paintCloseButton(g, (JComponent)c);
        }

        @Override
        public void getCloseButtonRectangle(JComponent jc, Rectangle rect, Rectangle bounds) {
            boolean notSupported;
            boolean rightClip = ((WinClassicEditorTabCellRenderer)jc).isClipRight();
            boolean leftClip = ((WinClassicEditorTabCellRenderer)jc).isClipLeft();
            boolean bl = notSupported = !((WinClassicEditorTabCellRenderer)jc).isShowCloseButton();
            if (leftClip || rightClip || notSupported) {
                rect.x = -100;
                rect.y = -100;
                rect.width = 0;
                rect.height = 0;
            } else {
                String iconPath = this.findIconPath((WinClassicEditorTabCellRenderer)jc);
                Icon icon = TabControlButtonFactory.getIcon(iconPath);
                int iconWidth = icon.getIconWidth();
                int iconHeight = icon.getIconHeight();
                rect.x = bounds.x + bounds.width - iconWidth - 2;
                rect.y = bounds.y + Math.max(0, bounds.height / 2 - iconHeight / 2);
                rect.width = iconWidth;
                rect.height = iconHeight;
            }
        }

        private void paintCloseButton(Graphics g, JComponent c) {
            if (((AbstractTabCellRenderer)c).isShowCloseButton()) {
                Rectangle r = new Rectangle(0, 0, c.getWidth(), c.getHeight());
                Rectangle cbRect = new Rectangle();
                this.getCloseButtonRectangle(c, cbRect, r);
                String iconPath = this.findIconPath((WinClassicEditorTabCellRenderer)c);
                Icon icon = TabControlButtonFactory.getIcon(iconPath);
                icon.paintIcon(c, g, cbRect.x, cbRect.y);
            }
        }

        private String findIconPath(WinClassicEditorTabCellRenderer renderer) {
            if (renderer.inCloseButton() && renderer.isPressed()) {
                return "org/openide/awt/resources/win_close_pressed.png";
            }
            if (renderer.inCloseButton()) {
                return "org/openide/awt/resources/win_close_rollover.png";
            }
            return "org/openide/awt/resources/win_close_enabled.png";
        }

        @Override
        public boolean supportsCloseButton(JComponent renderer) {
            return ((AbstractTabCellRenderer)renderer).isShowCloseButton();
        }
    }

}

