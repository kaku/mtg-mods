/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.Painter;
import javax.swing.SingleSelectionModel;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.plaf.AbstractViewTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.BusyTabsSupport;
import org.netbeans.swing.tabcontrol.plaf.ColorUtil;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.openide.awt.HtmlRenderer;

public final class NimbusViewTabDisplayerUI
extends AbstractViewTabDisplayerUI {
    private static final int BUMP_X_PAD = 0;
    private static final int BUMP_WIDTH = 0;
    private static final int TXT_X_PAD = 7;
    private static final int TXT_Y_PAD = 4;
    private static final int ICON_X_PAD = 2;
    private static Map<Integer, String[]> buttonIconPaths;
    private Dimension prefSize = new Dimension(100, 19);
    private Rectangle tempRect = new Rectangle();

    private NimbusViewTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
    }

    public static ComponentUI createUI(JComponent c) {
        return new NimbusViewTabDisplayerUI((TabDisplayer)c);
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        FontMetrics fm = this.getTxtFontMetrics();
        int height = fm == null ? 19 : fm.getAscent() + 2 * fm.getDescent() + 5;
        Insets insets = c.getInsets();
        this.prefSize.height = height + insets.bottom + insets.top;
        return this.prefSize;
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        ColorUtil.setupAntialiasing(g);
        Color col = c.getBackground();
        if (col != null) {
            g.setColor(col);
            g.fillRect(0, 0, c.getWidth(), c.getHeight());
        }
        this.paintOverallBorder(g, c);
        super.paint(g, c);
    }

    protected void paintOverallBorder(Graphics g, JComponent c) {
    }

    @Override
    protected Font getTxtFont() {
        Font result = UIManager.getFont("TabbedPane.font");
        if (result != null) {
            return result;
        }
        result = UIManager.getFont("controlFont");
        if (result != null) {
            return result;
        }
        return super.getTxtFont();
    }

    @Override
    protected void paintTabContent(Graphics g, int index, String text, int x, int y, int width, int height) {
        --height;
        FontMetrics fm = this.getTxtFontMetrics();
        g.setFont(this.getTxtFont());
        int txtWidth = width;
        if (this.isSelected(index)) {
            Component buttons = this.getControlButtons();
            if (null != buttons) {
                Dimension buttonsSize = buttons.getPreferredSize();
                if (width < buttonsSize.width + 4) {
                    buttons.setVisible(false);
                } else {
                    buttons.setVisible(true);
                    txtWidth = width - (buttonsSize.width + 4 + 7);
                    buttons.setLocation(x + txtWidth + 7 + 2, y + (height - buttonsSize.height) / 2 + 2);
                }
            }
        } else {
            txtWidth = width - 14;
        }
        this.drawBump(g, index, x + 4, y + 6, 0, height - 8);
        boolean slidedOut = false;
        WinsysInfoForTabbedContainer winsysInfo = this.displayer.getContainerWinsysInfo();
        if (null != winsysInfo && winsysInfo.isSlidedOutContainer()) {
            slidedOut = false;
        }
        if (this.isTabBusy(index) && !slidedOut) {
            Icon busyIcon = BusyTabsSupport.getDefault().getBusyIcon(this.isSelected(index));
            txtWidth -= busyIcon.getIconWidth() - 3 - 7;
            busyIcon.paintIcon(this.displayer, g, x + 7, y + (height - busyIcon.getIconHeight()) / 2);
            x += busyIcon.getIconWidth() + 3;
        }
        Color txtC = UIManager.getColor("TabbedPane.foreground");
        HtmlRenderer.renderString((String)text, (Graphics)g, (int)(x + 7), (int)(y + fm.getAscent() + 4), (int)txtWidth, (int)height, (Font)this.getTxtFont(), (Color)txtC, (int)1, (boolean)true);
    }

    @Override
    protected void paintTabBorder(Graphics g, int index, int x, int y, int width, int height) {
    }

    private static void paintTabBackgroundNative(Graphics g, int index, int state, int x, int y, int w, int h) {
    }

    @Override
    protected void paintTabBackground(Graphics g, int index, int x, int y, int width, int height) {
        boolean isLast;
        boolean isPreviousTabSelected;
        boolean bl = isLast = index == this.getDataModel().size() - 1;
        if (!isLast) {
            ++width;
        }
        Shape clip = g.getClip();
        boolean bl2 = isPreviousTabSelected = index - 1 == this.displayer.getSelectionModel().getSelectedIndex();
        if (isPreviousTabSelected) {
            g.setClip(x + 1, y, width - 1, height);
        }
        Object o = null;
        o = this.isSelected(index) ? (this.isActive() ? UIManager.get("TabbedPane:TabbedPaneTab[MouseOver+Selected].backgroundPainter") : UIManager.get("TabbedPane:TabbedPaneTab[Selected].backgroundPainter")) : UIManager.get("TabbedPane:TabbedPaneTab[Enabled].backgroundPainter");
        if (o != null && o instanceof Painter) {
            Painter painter = (Painter)o;
            BufferedImage bufIm = new BufferedImage(width, height, 1);
            Graphics2D g2d = bufIm.createGraphics();
            g2d.setBackground(UIManager.getColor("Panel.background"));
            g2d.clearRect(0, 0, width, height);
            painter.paint(g2d, null, width, height);
            g.drawImage(bufIm, x, y, null);
        }
        if (isPreviousTabSelected) {
            g.setClip(clip);
        }
    }

    @Override
    protected void paintDisplayerBackground(Graphics g, JComponent c) {
        int x = 0;
        int y = 0;
        int width = c.getWidth();
        int height = c.getHeight();
        Object o = null;
        o = UIManager.get("TabbedPane:TabbedPaneTab[Enabled].backgroundPainter");
        if (o != null && o instanceof Painter) {
            Painter painter = (Painter)o;
            BufferedImage bufIm = new BufferedImage(width, height, 1);
            Graphics2D g2d = bufIm.createGraphics();
            g2d.setBackground(UIManager.getColor("Panel.background"));
            g2d.clearRect(0, 0, width, height);
            painter.paint(g2d, null, width, height);
            g.drawImage(bufIm, x, y, null);
        }
    }

    private void drawBump(Graphics g, int index, int x, int y, int width, int height) {
    }

    private static void initIcons() {
        if (null == buttonIconPaths) {
            String[] iconPaths;
            buttonIconPaths = new HashMap<Integer, String[]>(7);
            iconPaths = new String[]{"org/openide/awt/resources/gtk_bigclose_enabled.png", "org/openide/awt/resources/gtk_bigclose_pressed.png", iconPaths[0], "org/openide/awt/resources/gtk_bigclose_rollover.png"};
            buttonIconPaths.put(1, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/gtk_slideright_enabled.png", "org/netbeans/swing/tabcontrol/resources/gtk_slideright_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/gtk_slideright_rollover.png"};
            buttonIconPaths.put(6, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/gtk_slideleft_enabled.png", "org/netbeans/swing/tabcontrol/resources/gtk_slideleft_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/gtk_slideleft_rollover.png"};
            buttonIconPaths.put(5, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/gtk_slidebottom_enabled.png", "org/netbeans/swing/tabcontrol/resources/gtk_slidebottom_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/gtk_slidebottom_rollover.png"};
            buttonIconPaths.put(7, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/gtk_pin_enabled.png", "org/netbeans/swing/tabcontrol/resources/gtk_pin_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/gtk_pin_rollover.png"};
            buttonIconPaths.put(2, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/nimbus_restore_group_enabled.png", "org/netbeans/swing/tabcontrol/resources/nimbus_restore_group_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/nimbus_restore_group_rollover.png"};
            buttonIconPaths.put(11, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/nimbus_minimize_enabled.png", "org/netbeans/swing/tabcontrol/resources/nimbus_minimize_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/nimbus_minimize_rollover.png"};
            buttonIconPaths.put(12, iconPaths);
        }
    }

    @Override
    public Icon getButtonIcon(int buttonId, int buttonState) {
        Icon res = null;
        NimbusViewTabDisplayerUI.initIcons();
        String[] paths = buttonIconPaths.get(buttonId);
        if (null != paths && buttonState >= 0 && buttonState < paths.length) {
            res = TabControlButtonFactory.getIcon(paths[buttonState]);
        }
        return res;
    }
}

