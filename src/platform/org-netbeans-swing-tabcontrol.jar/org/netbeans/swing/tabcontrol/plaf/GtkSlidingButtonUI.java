/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.JComponent;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.SlidingButtonUI;
import org.netbeans.swing.tabcontrol.plaf.ColorUtil;

public class GtkSlidingButtonUI
extends SlidingButtonUI {
    private boolean defaults_initialized = false;
    protected JToggleButton hiddenToggle;
    private static final GtkSlidingButtonUI INSTANCE = new GtkSlidingButtonUI();

    private GtkSlidingButtonUI() {
    }

    public static ComponentUI createUI(JComponent c) {
        return INSTANCE;
    }

    @Override
    public void installDefaults(AbstractButton b) {
        super.installDefaults(b);
        if (!this.defaults_initialized) {
            this.hiddenToggle = new JToggleButton();
            this.hiddenToggle.setText("");
            JToolBar bar = new JToolBar();
            bar.setRollover(true);
            bar.add(this.hiddenToggle);
            this.defaults_initialized = true;
        }
    }

    @Override
    protected void uninstallDefaults(AbstractButton b) {
        super.uninstallDefaults(b);
        this.defaults_initialized = false;
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        ColorUtil.setupAntialiasing(g);
        AbstractButton button = (AbstractButton)c;
        this.hiddenToggle.setBorderPainted(true);
        this.hiddenToggle.setRolloverEnabled(button.isRolloverEnabled());
        this.hiddenToggle.setFocusable(button.isFocusable());
        this.hiddenToggle.setFocusPainted(button.isFocusPainted());
        this.hiddenToggle.setMargin(button.getMargin());
        this.hiddenToggle.getModel().setRollover(button.getModel().isRollover());
        this.hiddenToggle.getModel().setPressed(button.getModel().isPressed());
        this.hiddenToggle.getModel().setArmed(button.getModel().isArmed());
        this.hiddenToggle.getModel().setSelected(button.getModel().isSelected());
        this.hiddenToggle.setBounds(button.getBounds());
        super.paint(g, c);
    }

    @Override
    protected void paintBackground(Graphics2D g, AbstractButton button) {
        this.hiddenToggle.paint(g);
    }

    @Override
    protected void paintButtonPressed(Graphics g, AbstractButton b) {
        this.hiddenToggle.paint(g);
    }
}

