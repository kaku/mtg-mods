/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.SingleSelectionModel;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.plaf.TabLayoutModel;

public final class ScrollingTabLayoutModel
implements TabLayoutModel {
    private int offset = -1;
    private TabLayoutModel wrapped;
    private boolean changed = true;
    TabDataModel mdl;
    SingleSelectionModel sel;
    private int makeVisibleTab = -1;
    int pixelsToAddToSelection = 0;
    private boolean lastTabClipped = false;
    private int firstVisibleTab = -1;
    private int lastVisibleTab = -1;
    private int width = -1;
    private int[] widths = null;
    private int minimumXposition = 0;
    private boolean recentlyResized = true;
    private Action fAction = null;
    private Action bAction = null;

    public ScrollingTabLayoutModel(TabLayoutModel wrapped, SingleSelectionModel sel, TabDataModel mdl) {
        this.wrapped = wrapped;
        this.mdl = mdl;
        this.sel = sel;
    }

    public ScrollingTabLayoutModel(TabLayoutModel wrapped, SingleSelectionModel sel, TabDataModel mdl, int minimumXposition) {
        this(wrapped, sel, mdl);
        this.minimumXposition = minimumXposition;
    }

    public void setMinimumXposition(int x) {
        this.minimumXposition = x;
        this.setChanged(true);
    }

    public void setPixelsToAddToSelection(int i) {
        this.pixelsToAddToSelection = i;
        this.setChanged(true);
    }

    public void clearCachedData() {
        this.setChanged(true);
    }

    private TabLayoutModel getWrapped() {
        return this.wrapped;
    }

    public int getOffset() {
        if (this.mdl.size() <= 1) {
            return -1;
        }
        return this.offset;
    }

    private void change() {
        int perTab;
        if (this.mdl.size() == 0) {
            this.widths = new int[0];
            this.updateActions();
            this.setChanged(false);
            return;
        }
        if (this.widths == null || this.widths.length != this.mdl.size()) {
            this.widths = new int[this.mdl.size()];
        }
        Arrays.fill(this.widths, 0);
        if (this.widths.length == 1) {
            this.offset = -1;
        }
        if (this.width < this.getMinimumLeftClippedWidth()) {
            int toBeShown = this.makeVisibleTab != -1 ? this.makeVisibleTab : this.sel.getSelectedIndex();
            if ((toBeShown = Math.min(this.widths.length - 1, toBeShown)) != -1) {
                this.widths[toBeShown] = this.width;
            } else {
                this.widths[0] = this.width;
            }
            this.firstVisibleTab = toBeShown;
            this.lastVisibleTab = toBeShown;
            this.setChanged(false);
            return;
        }
        int x = this.minimumXposition;
        int start = this.offset >= 0 ? this.offset : 0;
        int toRedistribute = -1;
        this.lastVisibleTab = -1;
        this.firstVisibleTab = start;
        this.lastTabClipped = false;
        if (start == this.mdl.size() - 1 && this.width < this.getWrapped().getW(start) + this.getMinimumLeftClippedWidth()) {
            this.lastVisibleTab = start;
            if (start != 0) {
                this.firstVisibleTab = start - 1;
                this.widths[start] = this.width - this.getMinimumLeftClippedWidth();
                this.widths[start - 1] = this.getMinimumLeftClippedWidth();
                this.lastTabClipped = this.width - this.getMinimumLeftClippedWidth() < this.getWrapped().getW(start);
            } else {
                this.firstVisibleTab = start;
                this.widths[start] = this.width;
                this.lastTabClipped = this.width < this.getWrapped().getW(start);
            }
            this.updateActions();
            this.setChanged(false);
            return;
        }
        for (int i = start; i < this.widths.length; ++i) {
            int w = i == this.offset ? this.getMinimumLeftClippedWidth() : this.getWrapped().getW(i);
            if (x + w > this.width) {
                if (this.width - x < this.getMinimumRightClippedWidth() && i != start) {
                    int[] arrn = this.widths;
                    int n = i - 1;
                    arrn[n] = arrn[n] + (this.width - x - 1);
                    toRedistribute = this.width - x;
                    this.lastVisibleTab = i - 1;
                    this.widths[i] = 0;
                } else {
                    this.widths[i] = this.width - x - 1;
                    this.lastVisibleTab = i;
                }
                this.lastTabClipped = true;
                break;
            }
            this.widths[i] = w;
            x += w;
            if (i != this.widths.length - 1) continue;
            this.lastVisibleTab = this.widths.length - 1;
        }
        int selected = this.sel.getSelectedIndex();
        if (this.pixelsToAddToSelection != 0 && selected > start && selected < this.lastVisibleTab) {
            int[] arrn = this.widths;
            int n = selected;
            arrn[n] = arrn[n] + this.pixelsToAddToSelection;
            perTab = this.pixelsToAddToSelection - 1 / (this.lastVisibleTab - start);
            int pixels = this.pixelsToAddToSelection - 1;
            for (int i2 = start; i2 <= this.lastVisibleTab; ++i2) {
                if (i2 == selected) continue;
                if (perTab == 0) {
                    int[] arrn2 = this.widths;
                    int n2 = i2;
                    arrn2[n2] = arrn2[n2] - 2;
                    if ((pixels -= 2) > 0) continue;
                    break;
                }
                int[] arrn3 = this.widths;
                int n3 = i2;
                arrn3[n3] = arrn3[n3] - perTab;
                if ((pixels -= perTab) <= 0) break;
            }
        }
        if (toRedistribute != -1 && this.lastVisibleTab != start && this.lastVisibleTab != start + 1) {
            perTab = toRedistribute / (this.lastVisibleTab + 1 - start);
            for (int i3 = start; i3 < this.lastVisibleTab; ++i3) {
                if (perTab != 0) {
                    int[] arrn = this.widths;
                    int n = i3;
                    arrn[n] = arrn[n] + perTab;
                    int[] arrn4 = this.widths;
                    int n4 = this.lastVisibleTab;
                    arrn4[n4] = arrn4[n4] - perTab;
                    continue;
                }
                int use = toRedistribute > 2 ? 2 : toRedistribute;
                int[] arrn = this.widths;
                int n = i3;
                arrn[n] = arrn[n] + use;
                int[] arrn5 = this.widths;
                int n5 = this.lastVisibleTab;
                arrn5[n5] = arrn5[n5] - use;
                if ((toRedistribute -= use) <= 0) break;
            }
        }
        this.updateActions();
        this.setChanged(false);
    }

    private void setChanged(boolean val) {
        if (this.changed != val) {
            this.changed = val;
        }
    }

    public int getPixelsToAddToSelection() {
        return this.pixelsToAddToSelection;
    }

    public boolean isLastTabClipped() {
        if (this.width < this.getMinimumLeftClippedWidth()) {
            return true;
        }
        return this.lastTabClipped;
    }

    public boolean makeVisible(int index, int width) {
        int totalWidth;
        if (width < 0) {
            this.setWidth(width);
            this.makeVisibleTab = index;
            return false;
        }
        boolean resized = width != this.width || this.recentlyResized;
        this.recentlyResized = false;
        this.setWidth(width);
        if (index == -1) {
            return false;
        }
        if (this.mdl.size() == 1) {
            this.setOffset(-1);
            return this.changed;
        }
        if (this.mdl.size() == 2 && (totalWidth = this.getWrapped().getW(0) + this.getWrapped().getW(1)) > width) {
            this.setOffset(0);
            return this.changed;
        }
        if (this.changed) {
            this.change();
        }
        if (index == 0) {
            int off = this.setOffset(-1);
            return off != -1;
        }
        int cachedWidthOfRequestedTab = this.getW(index);
        int widthForRequestedTab = this.getWrapped().getW(index);
        if (widthForRequestedTab > width) {
            this.setOffset(index - 1);
            return this.changed;
        }
        if (index == this.mdl.size() - 1 && !this.isLastTabClipped() && !resized && cachedWidthOfRequestedTab == width) {
            return false;
        }
        int newOffset = -2;
        int currW = 0;
        boolean isOffBack = false;
        boolean result = this.changed;
        boolean switchForward = false;
        if (index >= this.getLastVisibleTab(width)) {
            int selIdx = this.sel.getSelectedIndex();
            switchForward = index >= selIdx;
            currW = this.getWrapped().getW(index);
            if (index == selIdx) {
                currW += this.pixelsToAddToSelection;
            }
            int firstTab = index;
            do {
                if (--firstTab <= -1) continue;
                if (firstTab == selIdx) {
                    currW += this.pixelsToAddToSelection;
                }
                int wid = this.getWrapped().getW(firstTab);
                currW += wid;
            } while (currW <= width && firstTab >= -1);
            newOffset = firstTab;
            if ((currW <= width || switchForward) && this.getOffset() == -1 && ++newOffset == -1) {
                newOffset = 0;
            }
        } else if (index <= this.getFirstVisibleTab(width)) {
            isOffBack = true;
            newOffset = index - 1;
        }
        if (resized || !isOffBack || index == this.mdl.size() && this.getFirstVisibleTab(width) == index) {
            if (newOffset != -2) {
                this.setOffset(newOffset);
            }
            result = this.ensureAvailableSpaceUsed(false);
        } else if (newOffset != -2) {
            int old = this.offset;
            int nue = this.setOffset(Math.min(this.mdl.size(), newOffset));
            result = old != nue;
        }
        return result;
    }

    boolean ensureAvailableSpaceUsed(boolean useCached) {
        int lastTab;
        if (this.mdl.size() == 0) {
            return false;
        }
        boolean result = false;
        if (this.changed && !useCached) {
            result = true;
            this.change();
        }
        int last = this.mdl.size() - 1;
        int n = lastTab = useCached ? this.getCachedLastVisibleTab() : this.getLastVisibleTab(this.width);
        if (lastTab == last || lastTab == this.mdl.size() && last > -1) {
            int off = this.offset;
            int availableWidth = this.width - (this.getX(last) + this.getW(last));
            while (availableWidth > 0 && off > -1) {
                if ((availableWidth -= this.getWrapped().getW(off)) <= 0) continue;
                --off;
            }
            this.setOffset(off);
            if (this.changed) {
                result = true;
                this.change();
            }
        }
        return result;
    }

    int getMinimumRightClippedWidth() {
        return 40;
    }

    int getMinimumLeftClippedWidth() {
        return 40;
    }

    public void setWidth(int width) {
        if (this.width != width) {
            this.recentlyResized = true;
            if (width < this.width) {
                this.makeVisibleTab = this.sel.getSelectedIndex();
            }
            boolean needMakeVisible = width > 0 && this.width < 0 && this.makeVisibleTab != -1;
            this.width = width - this.minimumXposition;
            this.setChanged(width > this.getMinimumLeftClippedWidth());
            if (this.changed && needMakeVisible && width > this.getMinimumLeftClippedWidth()) {
                this.makeVisible(this.makeVisibleTab, width);
                this.makeVisibleTab = -1;
            }
        }
    }

    public int setOffset(int i) {
        int prevOffset = this.offset;
        if (this.mdl.size() == 1) {
            if (this.offset > -1) {
                this.offset = -1;
                this.setChanged(true);
            }
            return prevOffset;
        }
        if (this.mdl.size() == 2 && this.width < this.getMinimumLeftClippedWidth() + this.getMinimumRightClippedWidth()) {
            this.offset = -1;
            this.setChanged(false);
            return prevOffset;
        }
        if (i < -1) {
            i = -1;
        }
        if (i != this.offset) {
            this.setChanged(true);
            this.offset = i;
        }
        return prevOffset;
    }

    public int getFirstVisibleTab(int width) {
        this.setWidth(width);
        if (this.mdl.size() == 0) {
            return -1;
        }
        if (width < this.getMinimumLeftClippedWidth()) {
            int first = this.makeVisibleTab == -1 ? this.sel.getSelectedIndex() : this.makeVisibleTab;
            return first;
        }
        if (this.changed) {
            this.change();
        }
        return this.firstVisibleTab;
    }

    public int countVisibleTabs(int width) {
        return this.getLastVisibleTab(width) + 1 - this.getFirstVisibleTab(width);
    }

    public int getLastVisibleTab(int width) {
        this.setWidth(width);
        if (this.mdl.size() == 0) {
            return -1;
        }
        if (width < this.getMinimumLeftClippedWidth()) {
            int first = this.makeVisibleTab == -1 ? this.sel.getSelectedIndex() : this.makeVisibleTab;
            return first;
        }
        if (this.changed) {
            this.change();
        }
        return this.lastVisibleTab;
    }

    int getCachedLastVisibleTab() {
        return this.lastVisibleTab;
    }

    int getCachedFirstVisibleTab() {
        return this.firstVisibleTab;
    }

    @Override
    public int dropIndexOfPoint(int x, int y) {
        if (this.changed) {
            this.change();
        }
        int first = this.getFirstVisibleTab(this.width);
        int last = this.getLastVisibleTab(this.width);
        int pos = 0;
        for (int i = first; i <= last; ++i) {
            int lastPos = pos;
            pos += this.getW(i);
            int h = this.getH(i);
            int ay = this.getY(i);
            if (y < 0 || y > ay + h) {
                return -1;
            }
            if (i == last && x > lastPos + this.getW(i) / 2) {
                return last + 1;
            }
            if (x < lastPos || x > pos) continue;
            return i;
        }
        return -1;
    }

    @Override
    public void setPadding(Dimension d) {
        this.getWrapped().setPadding(d);
        this.setChanged(true);
    }

    @Override
    public int getH(int index) {
        if (this.changed) {
            this.change();
        }
        try {
            return this.getWrapped().getH(index);
        }
        catch (IndexOutOfBoundsException e) {
            return 0;
        }
    }

    @Override
    public int getW(int index) {
        if (this.changed || this.widths == null || index > this.widths.length) {
            this.change();
        }
        if (index >= this.widths.length) {
            return 0;
        }
        return this.widths[index];
    }

    @Override
    public int getX(int index) {
        if (this.changed) {
            this.change();
        }
        int result = this.minimumXposition;
        for (int i = 0; i < index; ++i) {
            result += this.getW(i);
        }
        return result;
    }

    @Override
    public int getY(int index) {
        if (this.changed) {
            this.change();
        }
        return this.getWrapped().getY(index);
    }

    @Override
    public int indexOfPoint(int x, int y) {
        int i;
        if (this.changed) {
            this.change();
        }
        int pos = this.minimumXposition;
        int n = i = this.offset == -1 ? 0 : this.offset;
        while (i < this.mdl.size()) {
            int lastPos = pos;
            int w = this.getW(i);
            pos += w;
            if (w == 0) break;
            int h = this.getH(i);
            int ay = this.getY(i);
            if (y < 0 || y > ay + h) {
                return -1;
            }
            if (x > lastPos && x < pos) {
                return i;
            }
            ++i;
        }
        return -1;
    }

    public Action getForwardAction() {
        if (this.fAction == null) {
            this.fAction = new ForwardAction();
        }
        return this.fAction;
    }

    public Action getBackwardAction() {
        if (this.bAction == null) {
            this.bAction = new BackwardAction();
        }
        return this.bAction;
    }

    private void updateActions() {
        if (this.width <= this.getMinimumLeftClippedWidth()) {
            this.bAction.setEnabled(false);
            this.fAction.setEnabled(false);
        }
        if (this.bAction != null) {
            this.bAction.setEnabled(this.mdl.size() > 1 && this.offset > -1);
        }
        if (this.fAction != null) {
            this.fAction.setEnabled(this.isLastTabClipped() && this.mdl.size() > 2 && (this.lastVisibleTab - this.firstVisibleTab > 1 || this.lastVisibleTab < this.mdl.size() - 1));
        }
    }

    private class BackwardAction
    extends AbstractAction {
        private BackwardAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            ScrollingTabLayoutModel.this.setOffset(ScrollingTabLayoutModel.this.getOffset() - 1);
            Component jc = (Component)this.getValue("control");
            if (jc != null) {
                jc.repaint();
            }
        }
    }

    private class ForwardAction
    extends AbstractAction {
        private ForwardAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            ScrollingTabLayoutModel.this.setOffset(ScrollingTabLayoutModel.this.getOffset() + 1);
            Component jc = (Component)this.getValue("control");
            if (jc != null) {
                jc.repaint();
            }
        }
    }

}

