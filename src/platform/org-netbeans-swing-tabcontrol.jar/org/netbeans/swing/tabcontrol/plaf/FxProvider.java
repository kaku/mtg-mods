/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import javax.swing.JComponent;
import javax.swing.JRootPane;

public abstract class FxProvider {
    protected JComponent comp;
    protected JRootPane root;
    private boolean running = false;
    protected Object orientation = null;

    public final void start(JComponent comp, JRootPane root, Object orientation) {
        if (this.running) {
            if (comp == this.comp && root == this.root) {
                return;
            }
            this.abort();
        }
        this.comp = comp;
        this.root = root;
        this.orientation = orientation;
        this.running = true;
        this.doStart();
    }

    public final void finish() {
        this.running = false;
        this.doFinish();
        this.cleanup();
    }

    public final void abort() {
        this.running = false;
        this.cleanup();
        this.comp = null;
        this.root = null;
    }

    public final boolean isRunning() {
        return this.running;
    }

    public abstract void cleanup();

    protected abstract void doStart();

    protected abstract void doFinish();
}

