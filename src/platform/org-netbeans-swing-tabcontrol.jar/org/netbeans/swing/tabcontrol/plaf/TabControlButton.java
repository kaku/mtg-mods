/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.event.ActionEvent;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.SingleSelectionModel;
import javax.swing.border.Border;
import javax.swing.plaf.ButtonUI;
import javax.swing.plaf.basic.BasicButtonUI;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.TabDisplayerUI;
import org.netbeans.swing.tabcontrol.event.TabActionEvent;

public abstract class TabControlButton
extends JButton {
    public static final int ID_CLOSE_BUTTON = 1;
    public static final int ID_PIN_BUTTON = 2;
    public static final int ID_MAXIMIZE_BUTTON = 3;
    public static final int ID_RESTORE_BUTTON = 4;
    public static final int ID_SLIDE_LEFT_BUTTON = 5;
    public static final int ID_SLIDE_RIGHT_BUTTON = 6;
    public static final int ID_SLIDE_DOWN_BUTTON = 7;
    public static final int ID_DROP_DOWN_BUTTON = 8;
    public static final int ID_SCROLL_LEFT_BUTTON = 9;
    public static final int ID_SCROLL_RIGHT_BUTTON = 10;
    public static final int ID_RESTORE_GROUP_BUTTON = 11;
    public static final int ID_SLIDE_GROUP_BUTTON = 12;
    public static final int STATE_DEFAULT = 0;
    public static final int STATE_PRESSED = 1;
    public static final int STATE_DISABLED = 2;
    public static final int STATE_ROLLOVER = 3;
    private int buttonId;
    protected final TabDisplayer displayer;
    private boolean showBorder;
    private boolean superConstructorsCompleted = true;

    TabControlButton(TabDisplayer displayer) {
        this(-1, displayer, false);
    }

    TabControlButton(int buttonId, TabDisplayer displayer) {
        this(buttonId, displayer, false);
    }

    TabControlButton(int buttonId, TabDisplayer displayer, boolean showBorder) {
        this.buttonId = buttonId;
        this.displayer = displayer;
        this.showBorder = showBorder;
        this.configureButton();
    }

    protected abstract String getTabActionCommand(ActionEvent var1);

    protected int getButtonId() {
        return this.buttonId;
    }

    @Override
    public Icon getIcon() {
        if (null != this.displayer) {
            return this.displayer.getUI().getButtonIcon(this.getButtonId(), 0);
        }
        return null;
    }

    @Override
    public Icon getPressedIcon() {
        if (null != this.displayer) {
            return this.displayer.getUI().getButtonIcon(this.getButtonId(), 1);
        }
        return null;
    }

    @Override
    public Icon getRolloverIcon() {
        if (null != this.displayer) {
            return this.displayer.getUI().getButtonIcon(this.getButtonId(), 3);
        }
        return null;
    }

    @Override
    public Icon getRolloverSelectedIcon() {
        return this.getRolloverIcon();
    }

    @Override
    public Icon getDisabledIcon() {
        if (null != this.displayer) {
            return this.displayer.getUI().getButtonIcon(this.getButtonId(), 2);
        }
        return null;
    }

    @Override
    public Icon getDisabledSelectedIcon() {
        return this.getDisabledIcon();
    }

    @Override
    public void updateUI() {
        this.setUI(new BasicButtonUI());
        if (this.superConstructorsCompleted) {
            this.configureButton();
        }
    }

    protected void configureButton() {
        this.setFocusable(false);
        this.setRolloverEnabled(this.getRolloverIcon() != null);
        if (this.showBorder) {
            this.setContentAreaFilled(true);
            this.setBorderPainted(true);
        } else {
            this.setContentAreaFilled(false);
            this.setBorderPainted(false);
            this.setBorder(BorderFactory.createEmptyBorder());
        }
        this.setFocusPainted(false);
        this.setOpaque(false);
    }

    @Override
    protected void fireActionPerformed(ActionEvent event) {
        super.fireActionPerformed(event);
        this.performAction(event);
        this.getModel().setRollover(false);
    }

    void performAction(ActionEvent e) {
        this.displayer.getUI().postTabAction(this.createTabActionEvent(e));
    }

    protected TabActionEvent createTabActionEvent(ActionEvent e) {
        return new TabActionEvent(this, this.getTabActionCommand(e), this.displayer.getSelectionModel().getSelectedIndex());
    }

    protected TabDisplayer getTabDisplayer() {
        return this.displayer;
    }
}

