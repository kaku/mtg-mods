/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.image.RescaleOp;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.synth.Region;
import javax.swing.plaf.synth.SynthContext;
import javax.swing.plaf.synth.SynthLookAndFeel;
import javax.swing.plaf.synth.SynthPainter;
import javax.swing.plaf.synth.SynthStyle;
import javax.swing.plaf.synth.SynthStyleFactory;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.plaf.AbstractViewTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.BusyTabsSupport;
import org.netbeans.swing.tabcontrol.plaf.ColorUtil;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.openide.awt.HtmlRenderer;

public final class GtkViewTabDisplayerUI
extends AbstractViewTabDisplayerUI {
    private static final int BUMP_X_PAD = 0;
    private static final int BUMP_WIDTH = 0;
    private static final int TXT_X_PAD = 7;
    private static final int TXT_Y_PAD = 5;
    private static final int ICON_X_PAD = 2;
    private static Map<Integer, String[]> buttonIconPaths;
    private static JTabbedPane dummyTab;
    private static final Logger LOG;
    private Dimension prefSize = new Dimension(100, 19);
    private Rectangle tempRect = new Rectangle();

    private GtkViewTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
    }

    public static ComponentUI createUI(JComponent c) {
        return new GtkViewTabDisplayerUI((TabDisplayer)c);
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        FontMetrics fm = this.getTxtFontMetrics();
        int height = fm == null ? 19 : fm.getAscent() + 2 * fm.getDescent() + 5;
        Insets insets = c.getInsets();
        this.prefSize.height = height + insets.bottom + insets.top;
        return this.prefSize;
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        ColorUtil.setupAntialiasing(g);
        Color col = c.getBackground();
        if (col != null) {
            g.setColor(col);
            g.fillRect(0, 0, c.getWidth(), c.getHeight());
        }
        this.paintOverallBorder(g, c);
        super.paint(g, c);
    }

    protected void paintOverallBorder(Graphics g, JComponent c) {
    }

    @Override
    protected Font getTxtFont() {
        Font result = UIManager.getFont("controlFont");
        if (result != null) {
            return result;
        }
        return super.getTxtFont();
    }

    @Override
    protected void paintTabContent(Graphics g, int index, String text, int x, int y, int width, int height) {
        --height;
        FontMetrics fm = this.getTxtFontMetrics();
        g.setFont(this.getTxtFont());
        int txtWidth = width;
        if (this.isSelected(index)) {
            Component buttons = this.getControlButtons();
            if (null != buttons) {
                Dimension buttonsSize = buttons.getPreferredSize();
                if (width < buttonsSize.width + 2) {
                    buttons.setVisible(false);
                } else {
                    buttons.setVisible(true);
                    txtWidth = width - (buttonsSize.width + 2 + 7);
                    buttons.setLocation(x + txtWidth + 7, y + (height - buttonsSize.height) / 2 + 2);
                }
            }
        } else {
            txtWidth = width - 14;
        }
        this.drawBump(g, index, x + 4, y + 6, 0, height - 8);
        boolean slidedOut = false;
        WinsysInfoForTabbedContainer winsysInfo = this.displayer.getContainerWinsysInfo();
        if (null != winsysInfo && winsysInfo.isSlidedOutContainer()) {
            slidedOut = false;
        }
        if (this.isTabBusy(index) && !slidedOut) {
            Icon busyIcon = BusyTabsSupport.getDefault().getBusyIcon(this.isSelected(index));
            txtWidth -= busyIcon.getIconWidth() - 3 - 7;
            busyIcon.paintIcon(this.displayer, g, x + 7, y + (height - busyIcon.getIconHeight()) / 2);
            x += busyIcon.getIconWidth() + 3;
        }
        Color txtC = UIManager.getColor("textText");
        HtmlRenderer.renderString((String)text, (Graphics)g, (int)(x + 7), (int)(y + fm.getAscent() + 5), (int)txtWidth, (int)height, (Font)this.getTxtFont(), (Color)txtC, (int)1, (boolean)true);
    }

    @Override
    protected void paintTabBorder(Graphics g, int index, int x, int y, int width, int height) {
    }

    private static void paintTabBackgroundNative(Graphics g, int index, int state, int x, int y, int w, int h) {
    }

    @Override
    protected void paintTabBackground(Graphics g, int index, int x, int y, int width, int height) {
        int state;
        int n = state = this.isSelected(index) ? 512 : 1024;
        if (dummyTab == null) {
            dummyTab = new JTabbedPane();
        }
        Region region = Region.TABBED_PANE_TAB;
        if (!(UIManager.getLookAndFeel() instanceof SynthLookAndFeel)) {
            return;
        }
        SynthLookAndFeel laf = (SynthLookAndFeel)UIManager.getLookAndFeel();
        SynthStyleFactory sf = SynthLookAndFeel.getStyleFactory();
        SynthStyle style = sf.getStyle(dummyTab, region);
        SynthContext context = new SynthContext(dummyTab, region, style, state);
        SynthPainter painter = style.getPainter(context);
        if (state == 512) {
            RescaleOp op = null;
            if (this.isActive()) {
                op = new RescaleOp(1.08f, 0.0f, null);
            } else {
                op = new RescaleOp(0.96f, 0.0f, null);
                ++y;
                --height;
            }
            BufferedImage bufIm = new BufferedImage(width, height, 1);
            Graphics2D g2d = bufIm.createGraphics();
            g2d.setBackground(UIManager.getColor("Panel.background"));
            g2d.clearRect(0, 0, width, height);
            long t1 = System.currentTimeMillis();
            painter.paintTabbedPaneTabBackground(context, g2d, 0, 0, width, height, index);
            long t2 = System.currentTimeMillis();
            if (t2 - t1 > 200) {
                LOG.log(Level.WARNING, "painter.paintTabbedPaneTabBackground1 takes too long x=0 y=0 w=" + width + " h=" + height + " index:" + index + " Time=" + (t2 - t1));
            }
            BufferedImage img = op.filter(bufIm, null);
            g.drawImage(img, x, y, null);
        } else {
            long t1 = System.currentTimeMillis();
            painter.paintTabbedPaneTabBackground(context, g, x, y + 2, width, height - 2, index);
            long t2 = System.currentTimeMillis();
            if (t2 - t1 > 200) {
                LOG.log(Level.WARNING, "painter.paintTabbedPaneTabBackground2 takes too long x=" + x + " y=" + (y + 2) + " w=" + width + " h=" + (height - 2) + " index:" + index + " Time=" + (t2 - t1));
            }
        }
    }

    private void drawBump(Graphics g, int index, int x, int y, int width, int height) {
    }

    private static void initIcons() {
        if (null == buttonIconPaths) {
            String[] iconPaths;
            buttonIconPaths = new HashMap<Integer, String[]>(7);
            iconPaths = new String[]{"org/openide/awt/resources/gtk_bigclose_enabled.png", "org/openide/awt/resources/gtk_bigclose_pressed.png", iconPaths[0], "org/openide/awt/resources/gtk_bigclose_rollover.png"};
            buttonIconPaths.put(1, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/gtk_slideright_enabled.png", "org/netbeans/swing/tabcontrol/resources/gtk_slideright_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/gtk_slideright_rollover.png"};
            buttonIconPaths.put(6, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/gtk_slideleft_enabled.png", "org/netbeans/swing/tabcontrol/resources/gtk_slideleft_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/gtk_slideleft_rollover.png"};
            buttonIconPaths.put(5, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/gtk_slidebottom_enabled.png", "org/netbeans/swing/tabcontrol/resources/gtk_slidebottom_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/gtk_slidebottom_rollover.png"};
            buttonIconPaths.put(7, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/gtk_pin_enabled.png", "org/netbeans/swing/tabcontrol/resources/gtk_pin_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/gtk_pin_rollover.png"};
            buttonIconPaths.put(2, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/gtk_restore_group_enabled.png", "org/netbeans/swing/tabcontrol/resources/gtk_restore_group_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/gtk_restore_group_rollover.png"};
            buttonIconPaths.put(11, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/gtk_minimize_enabled.png", "org/netbeans/swing/tabcontrol/resources/gtk_minimize_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/gtk_minimize_rollover.png"};
            buttonIconPaths.put(12, iconPaths);
        }
    }

    @Override
    public Icon getButtonIcon(int buttonId, int buttonState) {
        Icon res = null;
        GtkViewTabDisplayerUI.initIcons();
        String[] paths = buttonIconPaths.get(buttonId);
        if (null != paths && buttonState >= 0 && buttonState < paths.length) {
            res = TabControlButtonFactory.getIcon(paths[buttonState]);
        }
        return res;
    }

    static {
        LOG = Logger.getLogger("org.netbeans.swing.tabcontrol.plaf.GtkViewTabDisplayerUI");
    }
}

