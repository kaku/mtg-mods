/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.AlphaComposite;
import java.awt.Composite;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.SlidingButtonUI;

public class AquaSlidingButtonUI
extends SlidingButtonUI {
    private static AquaSlidingButtonUI AQUA_INSTANCE = null;

    private AquaSlidingButtonUI() {
    }

    public static ComponentUI createUI(JComponent c) {
        if (AQUA_INSTANCE == null) {
            AQUA_INSTANCE = new AquaSlidingButtonUI();
        }
        return AQUA_INSTANCE;
    }

    @Override
    public void installUI(JComponent c) {
        super.installUI(c);
        Font txtFont = (Font)UIManager.get("windowTitleFont");
        if (txtFont == null) {
            txtFont = new Font("Dialog", 0, 11);
        } else if (txtFont.isBold()) {
            txtFont = new Font(txtFont.getName(), 0, txtFont.getSize());
        }
        c.setFont(txtFont);
    }

    @Override
    protected void paintIcon(Graphics g, AbstractButton b, Rectangle iconRect) {
        Graphics2D g2d = (Graphics2D)g;
        Composite comp = g2d.getComposite();
        if (b.getModel().isRollover() || b.getModel().isSelected()) {
            g2d.setComposite(AlphaComposite.getInstance(3, 0.5f));
        }
        super.paintIcon(g, b, iconRect);
        g2d.setComposite(comp);
    }
}

