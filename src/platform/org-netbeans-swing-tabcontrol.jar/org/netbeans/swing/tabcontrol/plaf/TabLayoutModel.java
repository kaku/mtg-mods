/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Dimension;

public interface TabLayoutModel {
    public int getX(int var1);

    public int getY(int var1);

    public int getW(int var1);

    public int getH(int var1);

    public int indexOfPoint(int var1, int var2);

    public int dropIndexOfPoint(int var1, int var2);

    public void setPadding(Dimension var1);
}

