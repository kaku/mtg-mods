/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.Painter;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.plaf.BasicScrollingTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.NimbusEditorTabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.TabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;

public final class NimbusEditorTabDisplayerUI
extends BasicScrollingTabDisplayerUI {
    private static Map<Integer, String[]> buttonIconPaths;

    public NimbusEditorTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
    }

    public static ComponentUI createUI(JComponent c) {
        return new NimbusEditorTabDisplayerUI((TabDisplayer)c);
    }

    @Override
    public Rectangle getTabRect(int idx, Rectangle rect) {
        Rectangle r = super.getTabRect(idx, rect);
        r.y = 0;
        r.height = this.displayer.getHeight();
        return r;
    }

    @Override
    public void install() {
        super.install();
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        int prefHeight = 28;
        Graphics2D g = BasicScrollingTabDisplayerUI.getOffscreenGraphics();
        if (g != null) {
            FontMetrics fm = g.getFontMetrics(this.displayer.getFont());
            Insets ins = this.getTabAreaInsets();
            prefHeight = fm.getHeight() + ins.top + ins.bottom + 12;
        }
        return new Dimension(this.displayer.getWidth(), prefHeight);
    }

    @Override
    protected void paintBackground(Graphics g) {
        Graphics2D g2d = (Graphics2D)g;
        int w = this.displayer.getWidth();
        int h = this.displayer.getHeight();
        Painter painter = null;
        if (this.displayer.isActive()) {
            painter = (Painter)UIManager.get("TabbedPane:TabbedPaneTabArea[Enabled+MouseOver].backgroundPainter");
        }
        if (!this.displayer.isActive() || null == painter) {
            painter = (Painter)UIManager.get("TabbedPane:TabbedPaneTabArea[Enabled].backgroundPainter");
        }
        if (null != painter) {
            painter.paint(g2d, null, w, h);
        }
        Color c = (Color)UIManager.get("nimbusBorder");
        g.setColor(c);
        g.drawLine(0, h - 5, 0, h);
        g.drawLine(w - 1, h - 5, w - 1, h);
    }

    @Override
    protected void paintAfterTabs(Graphics g) {
    }

    @Override
    protected TabCellRenderer createDefaultRenderer() {
        return new NimbusEditorTabCellRenderer();
    }

    private static void initIcons() {
        if (null == buttonIconPaths) {
            buttonIconPaths = new HashMap<Integer, String[]>(7);
            String[] iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/nimbus_scrollleft_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/nimbus_scrollleft_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/nimbus_scrollleft_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/nimbus_scrollleft_pressed.png";
            buttonIconPaths.put(9, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/nimbus_scrollright_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/nimbus_scrollright_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/nimbus_scrollright_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/nimbus_scrollright_pressed.png";
            buttonIconPaths.put(10, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/nimbus_popup_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/nimbus_popup_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/nimbus_popup_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/nimbus_popup_pressed.png";
            buttonIconPaths.put(8, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/nimbus_maximize_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/nimbus_maximize_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/nimbus_maximize_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/nimbus_maximize_pressed.png";
            buttonIconPaths.put(3, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/nimbus_restore_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/nimbus_restore_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/nimbus_restore_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/nimbus_restore_pressed.png";
            buttonIconPaths.put(4, iconPaths);
        }
    }

    @Override
    public Icon getButtonIcon(int buttonId, int buttonState) {
        Icon res = null;
        NimbusEditorTabDisplayerUI.initIcons();
        String[] paths = buttonIconPaths.get(buttonId);
        if (null != paths && buttonState >= 0 && buttonState < paths.length) {
            res = TabControlButtonFactory.getIcon(paths[buttonState]);
        }
        return res;
    }

    @Override
    protected Rectangle getControlButtonsRectangle(Container parent) {
        Component c = this.getControlButtons();
        return new Rectangle(parent.getWidth() - c.getWidth() - 4, 2, c.getWidth(), c.getHeight());
    }

    @Override
    public Insets getTabAreaInsets() {
        Insets retValue = super.getTabAreaInsets();
        retValue.right += 4;
        return retValue;
    }
}

