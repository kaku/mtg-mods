/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Dimension;
import javax.swing.JComponent;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.plaf.BaseTabLayoutModel;

public final class DefaultTabLayoutModel
extends BaseTabLayoutModel {
    public DefaultTabLayoutModel(TabDataModel model, JComponent renderTarget) {
        super(model, renderTarget);
    }
}

