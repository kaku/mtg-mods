/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.image.RenderedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.SingleSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListDataEvent;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.ComponentConverter;
import org.netbeans.swing.tabcontrol.LocationInformer;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.TabDisplayerUI;
import org.netbeans.swing.tabcontrol.TabbedContainer;
import org.netbeans.swing.tabcontrol.TabbedContainerUI;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.event.ArrayDiff;
import org.netbeans.swing.tabcontrol.event.ComplexListDataEvent;
import org.netbeans.swing.tabcontrol.event.ComplexListDataListener;
import org.netbeans.swing.tabcontrol.event.TabActionEvent;
import org.netbeans.swing.tabcontrol.event.VeryComplexListDataEvent;
import org.netbeans.swing.tabcontrol.plaf.EqualPolygon;
import org.netbeans.swing.tabcontrol.plaf.FxProvider;
import org.netbeans.swing.tabcontrol.plaf.StackLayout;

public class DefaultTabbedContainerUI
extends TabbedContainerUI {
    private ActionListener actionListener = null;
    public static final String KEY_EDITOR_CONTENT_BORDER = "TabbedContainer.editor.contentBorder";
    public static final String KEY_EDITOR_TABS_BORDER = "TabbedContainer.editor.tabsBorder";
    public static final String KEY_EDITOR_OUTER_BORDER = "TabbedContainer.editor.outerBorder";
    public static final String KEY_VIEW_CONTENT_BORDER = "TabbedContainer.view.contentBorder";
    public static final String KEY_VIEW_TABS_BORDER = "TabbedContainer.view.tabsBorder";
    public static final String KEY_VIEW_OUTER_BORDER = "TabbedContainer.view.outerBorder";
    public static final String KEY_SLIDING_CONTENT_BORDER = "TabbedContainer.sliding.contentBorder";
    public static final String KEY_SLIDING_TABS_BORDER = "TabbedContainer.sliding.tabsBorder";
    public static final String KEY_SLIDING_OUTER_BORDER = "TabbedContainer.sliding.outerBorder";
    public static final String KEY_TOOLBAR_CONTENT_BORDER = "TabbedContainer.toolbar.contentBorder";
    public static final String KEY_TOOLBAR_TABS_BORDER = "TabbedContainer.toolbar.tabsBorder";
    public static final String KEY_TOOLBAR_OUTER_BORDER = "TabbedContainer.toolbar.outerBorder";
    protected ComponentListener componentListener = null;
    protected ChangeListener selectionListener = null;
    protected ComplexListDataListener modelListener = null;
    protected LayoutManager contentDisplayerLayout = null;
    protected PropertyChangeListener propertyChangeListener = null;
    protected FxProvider slideEffectManager = null;
    protected JComponent contentDisplayer = null;
    protected TabDisplayer tabDisplayer = null;
    private HierarchyListener hierarchyListener = null;
    private MouseListener forward = null;
    private Point scratchPoint = new Point();
    private boolean bug4924561knownShowing = false;
    static final boolean NO_EFFECTS = Boolean.getBoolean("nb.tabcontrol.no.fx");
    static final boolean NO_SCALE = Boolean.getBoolean("nb.tabcontrol.fx.no.scaling");
    static final boolean USE_SWINGPAINTING = Boolean.getBoolean("nb.tabcontrol.fx.swingpainting");
    static final boolean ADD_TO_GLASSPANE = Boolean.getBoolean("nb.tabcontrol.fx.use.resizing");
    static final boolean EFFECTS_EVERYWHERE = Boolean.getBoolean("nb.tabcontrol.fx.everywhere") || Boolean.getBoolean("nb.tabcontrol.fx.gratuitous");
    static final boolean USE_ALPHA = Boolean.getBoolean("nb.tabcontrol.fx.use.alpha") || Boolean.getBoolean("nb.tabcontrol.fx.gratuitous");
    static boolean SYNCHRONOUS_PAINTING = Boolean.getBoolean("nb.tabcontrol.fx.synchronous");
    static float INCREMENT = 0.07f;
    static int TIMER = 25;

    public DefaultTabbedContainerUI(TabbedContainer c) {
        super(c);
    }

    public static ComponentUI createUI(JComponent c) {
        return new DefaultTabbedContainerUI((TabbedContainer)c);
    }

    @Override
    public final void installUI(JComponent c) {
        assert (c == this.container);
        this.container.setLayout(this.createLayout());
        this.contentDisplayer = this.createContentDisplayer();
        if ("Aqua".equals(UIManager.getLookAndFeel().getID()) && (this.container.getType() == 0 || this.container.getType() == 2) && !Boolean.getBoolean("nb.explorerview.aqua.defaultbackground")) {
            this.contentDisplayer.setBackground(UIManager.getColor("NbExplorerView.background"));
            this.contentDisplayer.setOpaque(true);
        }
        this.tabDisplayer = this.createTabDisplayer();
        this.selectionListener = this.createSelectionListener();
        this.modelListener = this.createModelListener();
        this.componentListener = this.createComponentListener();
        this.propertyChangeListener = this.createPropertyChangeListener();
        this.contentDisplayerLayout = this.createContentDisplayerLayout();
        this.slideEffectManager = this.createFxProvider();
        this.actionListener = this.createDisplayerActionListener();
        this.container.setLayout(this.createLayout());
        this.hierarchyListener = new ContainerHierarchyListener();
        this.forward = new ForwardingMouseListener(this.container);
        this.installContentDisplayer();
        this.installTabDisplayer();
        this.installBorders();
        this.installListeners();
        this.install();
        this.tabDisplayer.getSelectionModel().addChangeListener(this.selectionListener);
    }

    TabDisplayer getTabDisplayer() {
        return this.tabDisplayer;
    }

    @Override
    public final void uninstallUI(JComponent c) {
        assert (c == this.container);
        this.tabDisplayer.getSelectionModel().removeChangeListener(this.selectionListener);
        this.uninstall();
        this.uninstallListeners();
        this.uninstallDisplayers();
        this.contentDisplayer = null;
        this.tabDisplayer = null;
        this.selectionListener = null;
        this.modelListener = null;
        this.componentListener = null;
        this.propertyChangeListener = null;
        this.contentDisplayerLayout = null;
        this.actionListener = null;
        this.forward = null;
    }

    protected void install() {
    }

    protected void uninstall() {
    }

    @Override
    protected boolean uichange() {
        this.installBorders();
        return false;
    }

    protected void installContentDisplayer() {
        this.contentDisplayer.setLayout(this.contentDisplayerLayout);
        this.container.add((Component)this.contentDisplayer, "Center");
    }

    protected void installTabDisplayer() {
        this.container.add((Component)this.tabDisplayer, "North");
        this.tabDisplayer.registerShortcuts(this.container);
    }

    protected void installBorders() {
        String contentKey;
        String tabsKey;
        String outerKey;
        switch (this.container.getType()) {
            case 1: {
                tabsKey = "TabbedContainer.editor.tabsBorder";
                contentKey = "TabbedContainer.editor.contentBorder";
                outerKey = "TabbedContainer.editor.outerBorder";
                break;
            }
            case 0: {
                tabsKey = "TabbedContainer.view.tabsBorder";
                contentKey = "TabbedContainer.view.contentBorder";
                outerKey = "TabbedContainer.view.outerBorder";
                break;
            }
            case 2: {
                tabsKey = "TabbedContainer.sliding.tabsBorder";
                contentKey = "TabbedContainer.sliding.contentBorder";
                outerKey = "TabbedContainer.sliding.outerBorder";
                break;
            }
            case 3: {
                tabsKey = "TabbedContainer.toolbar.tabsBorder";
                contentKey = "TabbedContainer.toolbar.contentBorder";
                outerKey = "TabbedContainer.toolbar.outerBorder";
                break;
            }
            default: {
                throw new IllegalStateException("Unknown type: " + this.container.getType());
            }
        }
        try {
            Border b = (Border)UIManager.get(contentKey);
            this.contentDisplayer.setBorder(b);
            b = (Border)UIManager.get(tabsKey);
            this.tabDisplayer.setBorder(b);
            b = (Border)UIManager.get(outerKey);
            this.container.setBorder(b);
        }
        catch (ClassCastException cce) {
            System.err.println("Expected a border from UIManager for " + tabsKey + "," + contentKey + "," + outerKey);
        }
    }

    protected void installListeners() {
        this.container.addComponentListener(this.componentListener);
        this.container.addHierarchyListener(this.hierarchyListener);
        this.tabDisplayer.addMouseListener(this.forward);
        this.contentDisplayer.addMouseListener(this.forward);
        if (this.container.isShowing()) {
            this.attachModelAndSelectionListeners();
        }
    }

    protected void attachModelAndSelectionListeners() {
        this.container.getModel().addComplexListDataListener(this.modelListener);
        this.container.addPropertyChangeListener(this.propertyChangeListener);
        this.tabDisplayer.setActive(this.container.isActive());
        this.tabDisplayer.addActionListener(this.actionListener);
    }

    protected void detachModelAndSelectionListeners() {
        this.container.getModel().removeComplexListDataListener(this.modelListener);
        this.container.removePropertyChangeListener(this.propertyChangeListener);
        this.tabDisplayer.removeActionListener(this.actionListener);
    }

    protected void uninstallListeners() {
        this.container.removeComponentListener(this.componentListener);
        this.container.removeHierarchyListener(this.hierarchyListener);
        this.componentListener = null;
        this.propertyChangeListener = null;
        this.tabDisplayer.removeMouseListener(this.forward);
        this.contentDisplayer.removeMouseListener(this.forward);
        this.detachModelAndSelectionListeners();
    }

    protected void uninstallDisplayers() {
        this.container.remove(this.contentDisplayer);
        this.container.remove(this.tabDisplayer);
        this.tabDisplayer.unregisterShortcuts(this.container);
        this.contentDisplayer.removeAll();
        this.contentDisplayer = null;
        this.tabDisplayer = null;
    }

    protected TabDisplayer createTabDisplayer() {
        TabDisplayer result = null;
        WinsysInfoForTabbedContainer winsysInfo = this.container.getContainerWinsysInfo();
        result = winsysInfo != null ? new TabDisplayer(this.container.getModel(), this.container.getType(), winsysInfo) : new TabDisplayer(this.container.getModel(), this.container.getType(), this.container.getLocationInformer());
        result.setName("Tab Displayer");
        result.setComponentConverter(this.container.getComponentConverter());
        return result;
    }

    protected JPanel createContentDisplayer() {
        JPanel result = new JPanel();
        result.setName("Content displayer");
        return result;
    }

    protected FxProvider createFxProvider() {
        if (NO_EFFECTS || this.tabDisplayer.getType() != 2 && !EFFECTS_EVERYWHERE) {
            return new NoOpFxProvider();
        }
        if (ADD_TO_GLASSPANE) {
            return new LiveComponentSlideFxProvider();
        }
        return new ImageSlideFxProvider();
    }

    protected LayoutManager createContentDisplayerLayout() {
        return new StackLayout();
    }

    protected LayoutManager createLayout() {
        if (this.container.getType() == 2) {
            return new SlidingTabsLayout();
        }
        if (this.container.getType() == 3) {
            return new ToolbarTabsLayout();
        }
        return new BorderLayout();
    }

    protected ComponentListener createComponentListener() {
        return new ContainerComponentListener();
    }

    protected PropertyChangeListener createPropertyChangeListener() {
        return new ContainerPropertyChangeListener();
    }

    private ActionListener createDisplayerActionListener() {
        return new DisplayerActionListener();
    }

    protected void ensureSelectedComponentIsShowing() {
        int i = this.tabDisplayer.getSelectionModel().getSelectedIndex();
        if (i != -1) {
            TabData td = this.container.getModel().getTab(i);
            this.showComponent(this.toComp(td));
        }
    }

    protected final Component toComp(TabData data) {
        return this.container.getComponentConverter().getComponent(data);
    }

    protected Component showComponent(Component c) {
        if (this.contentDisplayerLayout instanceof StackLayout) {
            StackLayout stack = (StackLayout)this.contentDisplayerLayout;
            Component last = stack.getVisibleComponent();
            stack.showComponent(c, this.contentDisplayer);
            if (c != null) {
                Integer offset = (Integer)((JComponent)c).getClientProperty("MultiViewBorderHack.topOffset");
                this.contentDisplayer.putClientProperty("MultiViewBorderHack.topOffset", offset);
            } else {
                this.contentDisplayer.putClientProperty("MultiViewBorderHack.topOffset", null);
            }
            if (last != c) {
                this.maybeRemoveLastComponent(last);
                return last;
            }
        }
        return null;
    }

    protected final void showComponentWithFxProvider(Component c) {
        if (this.slideEffectManager == null || !this.container.isShowing() || !(c instanceof JComponent)) {
            Component last = this.showComponent(c);
            this.maybeRemoveLastComponent(last);
        } else {
            this.slideEffectManager.start((JComponent)c, this.container.getRootPane(), this.tabDisplayer.getClientProperty("orientation"));
        }
    }

    private final void maybeRemoveLastComponent(Component c) {
        if (c != null && this.container.getContentPolicy() == 3) {
            this.contentDisplayer.remove(c);
        }
    }

    protected void initDisplayer() {
        if (this.container.getContentPolicy() == 1) {
            List<TabData> tabs = this.container.getModel().getTabs();
            Component curC = null;
            Iterator<TabData> iter = tabs.iterator();
            while (iter.hasNext()) {
                curC = this.toComp(iter.next());
                this.contentDisplayer.add(curC, "");
            }
        } else {
            int i = this.tabDisplayer.getSelectionModel().getSelectedIndex();
            if (i != -1) {
                TabData td = this.container.getModel().getTab(i);
                this.contentDisplayer.add(this.toComp(td), "");
            }
        }
        this.updateActiveState();
    }

    protected ComplexListDataListener createModelListener() {
        return new ModelListener();
    }

    protected ChangeListener createSelectionListener() {
        return new SelectionListener();
    }

    private void updateActiveState() {
        TabDisplayer displ = this.tabDisplayer;
        TabbedContainer cont = this.container;
        if (displ != null && cont != null) {
            displ.setActive(cont.isActive());
        }
    }

    @Override
    public Rectangle getTabRect(int tab, Rectangle r) {
        if (r == null) {
            r = new Rectangle();
        }
        this.tabDisplayer.getTabRect(tab, r);
        Point p = this.tabDisplayer.getLocation();
        r.x += p.x;
        r.y += p.y;
        return r;
    }

    @Override
    protected void requestAttention(int tab) {
        this.tabDisplayer.requestAttention(tab);
    }

    @Override
    protected void cancelRequestAttention(int tab) {
        this.tabDisplayer.cancelRequestAttention(tab);
    }

    @Override
    protected void setAttentionHighlight(int tab, boolean highlight) {
        this.tabDisplayer.setAttentionHighlight(tab, highlight);
    }

    @Override
    public void setShowCloseButton(boolean val) {
        this.tabDisplayer.setShowCloseButton(val);
    }

    @Override
    public boolean isShowCloseButton() {
        return this.tabDisplayer.isShowCloseButton();
    }

    protected final void updateOrientation() {
        if (!this.container.isDisplayable()) {
            return;
        }
        if (Boolean.FALSE.equals(this.container.getClientProperty("manageTabPosition"))) {
            return;
        }
        Object currOrientation = this.tabDisplayer.getClientProperty("orientation");
        Container window = this.container.getTopLevelAncestor();
        Rectangle containerBounds = this.container.getBounds();
        containerBounds = SwingUtilities.convertRectangle(this.container, containerBounds, window);
        boolean longestIsVertical = containerBounds.width < containerBounds.height;
        int distanceToLeft = containerBounds.x;
        int distanceToTop = containerBounds.y;
        int distanceToRight = window.getWidth() - (containerBounds.x + containerBounds.width);
        int distanceToBottom = window.getHeight() - (containerBounds.y + containerBounds.height);
        Object orientation = !longestIsVertical ? (distanceToBottom > distanceToTop ? TabDisplayer.ORIENTATION_NORTH : TabDisplayer.ORIENTATION_SOUTH) : (distanceToLeft > distanceToRight ? TabDisplayer.ORIENTATION_EAST : TabDisplayer.ORIENTATION_WEST);
        if (currOrientation != orientation) {
            this.tabDisplayer.putClientProperty("orientation", orientation);
            this.container.validate();
        }
    }

    @Override
    public int tabForCoordinate(Point p) {
        p = SwingUtilities.convertPoint(this.container, p, this.tabDisplayer);
        return this.tabDisplayer.tabForCoordinate(p);
    }

    @Override
    public void makeTabVisible(int tab) {
        this.tabDisplayer.makeTabVisible(tab);
    }

    @Override
    public SingleSelectionModel getSelectionModel() {
        return this.tabDisplayer.getSelectionModel();
    }

    @Override
    public Image createImageOfTab(int idx) {
        return this.tabDisplayer.getUI().createImageOfTab(idx);
    }

    @Override
    public Polygon getExactTabIndication(int idx) {
        Polygon result = this.tabDisplayer.getUI().getExactTabIndication(idx);
        this.scratchPoint.setLocation(0, 0);
        Point p = SwingUtilities.convertPoint(this.tabDisplayer, this.scratchPoint, this.container);
        result.translate(- p.x, - p.y);
        return this.appendContentBoundsTo(result);
    }

    @Override
    public Polygon getInsertTabIndication(int idx) {
        Polygon result = this.tabDisplayer.getUI().getInsertTabIndication(idx);
        this.scratchPoint.setLocation(0, 0);
        Point p = SwingUtilities.convertPoint(this.tabDisplayer, this.scratchPoint, this.container);
        result.translate(- p.x, - p.y);
        return this.appendContentBoundsTo(result);
    }

    private Polygon appendContentBoundsTo(Polygon p) {
        int width = this.contentDisplayer.getWidth();
        int height = this.contentDisplayer.getHeight();
        int[] xpoints = new int[p.npoints + 4];
        int[] ypoints = new int[xpoints.length];
        int pos = 0;
        Object orientation = this.tabDisplayer.getClientProperty("orientation");
        int tabsHeight = this.tabDisplayer.getHeight();
        if (orientation == null || orientation == TabDisplayer.ORIENTATION_NORTH) {
            xpoints[pos] = 0;
            ypoints[pos] = tabsHeight;
            xpoints[++pos] = p.xpoints[p.npoints - 1];
            ypoints[pos] = tabsHeight;
            ++pos;
            for (int i = 0; i < p.npoints - 2; ++i) {
                xpoints[pos] = p.xpoints[i];
                ypoints[pos] = p.ypoints[i];
                ++pos;
            }
            xpoints[pos] = xpoints[pos - 1];
            ypoints[pos] = tabsHeight;
            xpoints[++pos] = width - 1;
            ypoints[pos] = tabsHeight;
            xpoints[++pos] = width - 1;
            ypoints[pos] = height - 1;
            xpoints[++pos] = 0;
            ypoints[pos] = height - 1;
        } else if (orientation == TabDisplayer.ORIENTATION_SOUTH) {
            int yxlate = this.contentDisplayer.getHeight() * 2;
            xpoints[pos] = 0;
            ypoints[pos] = 0;
            xpoints[++pos] = this.container.getWidth();
            ypoints[pos] = 0;
            xpoints[++pos] = this.container.getWidth();
            ypoints[pos] = this.container.getHeight() - tabsHeight;
            ++pos;
            int upperRight = 0;
            int highestFound = Integer.MIN_VALUE;
            for (int i = p.npoints - 2; i >= 0; --i) {
                if (highestFound < p.ypoints[i]) {
                    upperRight = i;
                    highestFound = p.ypoints[i];
                    continue;
                }
                if (highestFound == p.ypoints[i]) break;
            }
            int curr = upperRight - 1;
            for (int i2 = p.npoints - 1; i2 >= 0; --i2) {
                xpoints[pos] = p.xpoints[curr];
                ypoints[pos] = ypoints[pos] == highestFound ? Math.min(this.tabDisplayer.getLocation().y, p.ypoints[curr] + yxlate) : p.ypoints[curr] + yxlate;
                ++pos;
                if (++curr != p.npoints - 1) continue;
                curr = 0;
            }
            xpoints[pos] = 0;
            ypoints[pos] = this.container.getHeight() - tabsHeight;
        } else {
            xpoints = p.xpoints;
            ypoints = p.ypoints;
        }
        EqualPolygon result = new EqualPolygon(xpoints, ypoints, xpoints.length);
        return result;
    }

    @Override
    public Rectangle getContentArea() {
        return this.contentDisplayer.getBounds();
    }

    @Override
    public Rectangle getTabsArea() {
        return this.tabDisplayer.getBounds();
    }

    @Override
    public int dropIndexOfPoint(Point p) {
        Point p2 = SwingUtilities.convertPoint(this.container, p, this.tabDisplayer);
        return this.tabDisplayer.getUI().dropIndexOfPoint(p2);
    }

    public Rectangle getTabsArea(Rectangle dest) {
        return this.tabDisplayer.getBounds();
    }

    static {
        boolean gratuitous = Boolean.getBoolean("nb.tabcontrol.fx.gratuitous");
        String s = System.getProperty("nb.tabcontrol.fx.increment");
        if (s != null) {
            try {
                INCREMENT = Float.parseFloat(s);
            }
            catch (Exception e) {
                System.err.println("Bad float value specified: \"" + s + "\"");
            }
        } else if (gratuitous) {
            INCREMENT = 0.02f;
        }
        if ((s = System.getProperty("nb.tabcontrol.fx.timer")) != null) {
            try {
                TIMER = Integer.parseInt(s);
            }
            catch (Exception e) {
                System.err.println("Bad integer value specified: \"" + s + "\"");
            }
        } else if (gratuitous) {
            TIMER = 7;
        }
        if (gratuitous) {
            SYNCHRONOUS_PAINTING = true;
        }
    }

    private static final class ForwardingMouseListener
    implements MouseListener {
        private final Container c;

        public ForwardingMouseListener(Container c) {
            this.c = c;
        }

        @Override
        public void mousePressed(MouseEvent me) {
            this.forward(me);
        }

        @Override
        public void mouseReleased(MouseEvent me) {
            this.forward(me);
        }

        @Override
        public void mouseClicked(MouseEvent me) {
            this.forward(me);
        }

        @Override
        public void mouseEntered(MouseEvent me) {
            this.forward(me);
        }

        @Override
        public void mouseExited(MouseEvent me) {
            this.forward(me);
        }

        private void forward(MouseEvent me) {
            MouseListener[] ml = this.c.getMouseListeners();
            if (ml.length == 0 || me.isConsumed()) {
                return;
            }
            MouseEvent me2 = SwingUtilities.convertMouseEvent((Component)me.getSource(), me, this.c);
            block7 : for (int i = 0; i < ml.length; ++i) {
                switch (me2.getID()) {
                    case 504: {
                        ml[i].mouseEntered(me2);
                        continue block7;
                    }
                    case 505: {
                        ml[i].mouseExited(me2);
                        continue block7;
                    }
                    case 501: {
                        ml[i].mousePressed(me2);
                        continue block7;
                    }
                    case 502: {
                        ml[i].mouseReleased(me2);
                        continue block7;
                    }
                    case 500: {
                        ml[i].mouseClicked(me2);
                        continue block7;
                    }
                    default: {
                        assert (false);
                        continue block7;
                    }
                }
            }
        }
    }

    private final class LiveComponentSlideFxProvider
    extends FxProvider
    implements ActionListener {
        private Timer timer;
        private Component prevGlassPane;
        private Dimension d;
        private LiveComponentResizingGlassPane customGlassPane;

        private LiveComponentSlideFxProvider() {
            this.timer = null;
            this.prevGlassPane = null;
            this.d = null;
            this.customGlassPane = null;
        }

        @Override
        protected void doStart() {
            if (this.timer == null) {
                this.timer = new Timer(DefaultTabbedContainerUI.TIMER, this);
                this.timer.setRepeats(true);
            }
            this.prevGlassPane = this.root.getGlassPane();
            if (this.prevGlassPane.isVisible() && this.prevGlassPane.isShowing()) {
                this.doFinish();
                return;
            }
            this.initSize();
            LiveComponentResizingGlassPane cp = this.getCustomGlassPane();
            this.root.setGlassPane(cp);
            cp.setIncrement(0.1f);
            cp.setBounds(this.root.getBounds());
            cp.setVisible(true);
            cp.revalidate();
            this.timer.start();
        }

        private void initSize() {
            boolean flip;
            this.d = this.comp.getPreferredSize();
            Dimension d2 = DefaultTabbedContainerUI.this.contentDisplayer.getSize();
            this.d.width = Math.max(d2.width, this.d.width);
            this.d.height = Math.max(d2.height, this.d.height);
            boolean bl = flip = this.orientation == TabDisplayer.ORIENTATION_EAST || this.orientation == TabDisplayer.ORIENTATION_WEST;
            if (this.d.width == 0 || this.d.height == 0) {
                if (flip) {
                    this.d.width = this.root.getWidth();
                    this.d.height = DefaultTabbedContainerUI.this.tabDisplayer.getHeight();
                } else {
                    this.d.width = DefaultTabbedContainerUI.this.tabDisplayer.getWidth();
                    this.d.height = this.root.getHeight();
                }
            } else if (flip) {
                this.d.height = Math.max(this.d.height, DefaultTabbedContainerUI.this.tabDisplayer.getHeight());
            } else {
                this.d.width = Math.max(this.d.width, DefaultTabbedContainerUI.this.tabDisplayer.getWidth());
            }
        }

        @Override
        public void cleanup() {
            this.timer.stop();
            this.root.setGlassPane(this.prevGlassPane);
            this.prevGlassPane.setVisible(false);
            this.customGlassPane.remove(this.comp);
        }

        @Override
        protected void doFinish() {
            DefaultTabbedContainerUI.this.showComponent(this.comp);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            float inc = this.customGlassPane.getIncrement();
            if (inc >= 1.0f) {
                this.finish();
            } else {
                this.customGlassPane.setIncrement(inc + DefaultTabbedContainerUI.INCREMENT);
            }
        }

        private LiveComponentResizingGlassPane getCustomGlassPane() {
            if (this.customGlassPane == null) {
                this.customGlassPane = new LiveComponentResizingGlassPane();
                this.customGlassPane.setOpaque(false);
            }
            return this.customGlassPane;
        }

        static /* synthetic */ Dimension access$3800(LiveComponentSlideFxProvider x0) {
            return x0.d;
        }

        private class LiveComponentResizingGlassPane
        extends JPanel {
            private float inc;
            private Rectangle rect;
            private Rectangle r2;
            private boolean changed;

            private LiveComponentResizingGlassPane() {
                this.inc = 0.0f;
                this.rect = new Rectangle();
                this.r2 = new Rectangle();
                this.changed = true;
            }

            private void setIncrement(float inc) {
                this.inc = inc;
                this.changed = true;
                if (this.isShowing() && LiveComponentSlideFxProvider.this.comp.getParent() != this) {
                    this.add(LiveComponentSlideFxProvider.this.comp);
                    LiveComponentSlideFxProvider.this.comp.setVisible(true);
                }
                this.doLayout();
            }

            @Override
            public void doLayout() {
                Rectangle r = this.getImageBounds();
                LiveComponentSlideFxProvider.this.comp.setBounds(r.x, r.y, r.width, r.height);
            }

            private float getIncrement() {
                return this.inc;
            }

            private Rectangle getImageBounds() {
                if (!this.changed) {
                    return this.rect;
                }
                TabDisplayer c = DefaultTabbedContainerUI.this.tabDisplayer;
                this.r2.setBounds(0, 0, c.getWidth(), c.getHeight());
                Rectangle dispBounds = SwingUtilities.convertRectangle(c, this.r2, this);
                if (LiveComponentSlideFxProvider.this.orientation == TabDisplayer.ORIENTATION_WEST) {
                    this.rect.x = dispBounds.x + dispBounds.width;
                    this.rect.y = dispBounds.y;
                    this.rect.width = Math.round(this.inc * (float)LiveComponentSlideFxProvider.access$3800((LiveComponentSlideFxProvider)LiveComponentSlideFxProvider.this).width);
                    this.rect.height = dispBounds.height;
                } else if (LiveComponentSlideFxProvider.this.orientation == TabDisplayer.ORIENTATION_EAST) {
                    this.rect.width = Math.round(this.inc * (float)LiveComponentSlideFxProvider.access$3800((LiveComponentSlideFxProvider)LiveComponentSlideFxProvider.this).width);
                    this.rect.height = dispBounds.height;
                    this.rect.x = dispBounds.x - this.rect.width;
                    this.rect.y = dispBounds.y;
                } else if (LiveComponentSlideFxProvider.this.orientation == TabDisplayer.ORIENTATION_SOUTH) {
                    this.rect.width = dispBounds.width;
                    this.rect.height = Math.round(this.inc * (float)LiveComponentSlideFxProvider.access$3800((LiveComponentSlideFxProvider)LiveComponentSlideFxProvider.this).height);
                    this.rect.x = dispBounds.x;
                    this.rect.y = dispBounds.y - this.rect.height;
                } else if (LiveComponentSlideFxProvider.this.orientation == TabDisplayer.ORIENTATION_NORTH) {
                    this.rect.x = dispBounds.x;
                    this.rect.y = dispBounds.y + dispBounds.height;
                    this.rect.width = dispBounds.width;
                    this.rect.height = Math.round(this.inc * (float)LiveComponentSlideFxProvider.access$3800((LiveComponentSlideFxProvider)LiveComponentSlideFxProvider.this).height);
                }
                this.changed = false;
                return this.rect;
            }
        }

    }

    private final class ImageSlideFxProvider
    extends FxProvider
    implements ActionListener {
        private Timer timer;
        private Component prevGlassPane;
        private Dimension d;
        private BufferedImage img;
        private ImageScalingGlassPane customGlassPane;

        private ImageSlideFxProvider() {
            this.timer = null;
            this.prevGlassPane = null;
            this.d = null;
            this.img = null;
            this.customGlassPane = null;
        }

        @Override
        protected void doStart() {
            if (this.timer == null) {
                this.timer = new Timer(DefaultTabbedContainerUI.TIMER, this);
                this.timer.setRepeats(true);
            }
            this.prevGlassPane = this.root.getGlassPane();
            if (this.prevGlassPane.isVisible() && this.prevGlassPane.isShowing()) {
                this.doFinish();
                return;
            }
            this.initSize();
            this.img = this.createImageOfComponent();
            ImageScalingGlassPane cp = this.getCustomGlassPane();
            this.root.setGlassPane(cp);
            cp.setIncrement(0.1f);
            cp.setBounds(this.root.getBounds());
            cp.setVisible(true);
            cp.revalidate();
            this.timer.start();
        }

        @Override
        public void cleanup() {
            this.timer.stop();
            this.root.setGlassPane(this.prevGlassPane);
            this.prevGlassPane.setVisible(false);
            if (this.img != null) {
                this.img.flush();
            }
            this.img = null;
        }

        @Override
        protected void doFinish() {
            DefaultTabbedContainerUI.this.showComponent(this.comp);
        }

        private void initSize() {
            boolean flip;
            this.d = this.comp.getPreferredSize();
            Dimension d2 = DefaultTabbedContainerUI.this.contentDisplayer.getSize();
            this.d.width = Math.max(d2.width, this.d.width);
            this.d.height = Math.max(d2.height, this.d.height);
            boolean bl = flip = this.orientation == TabDisplayer.ORIENTATION_EAST || this.orientation == TabDisplayer.ORIENTATION_WEST;
            if (this.d.width == 0 || this.d.height == 0) {
                if (flip) {
                    this.d.width = this.root.getWidth();
                    this.d.height = DefaultTabbedContainerUI.this.tabDisplayer.getHeight();
                } else {
                    this.d.width = DefaultTabbedContainerUI.this.tabDisplayer.getWidth();
                    this.d.height = this.root.getHeight();
                }
            } else if (flip) {
                this.d.height = Math.max(this.d.height, DefaultTabbedContainerUI.this.tabDisplayer.getHeight());
            } else {
                this.d.width = Math.max(this.d.width, DefaultTabbedContainerUI.this.tabDisplayer.getWidth());
            }
        }

        private BufferedImage createImageOfComponent() {
            if (DefaultTabbedContainerUI.USE_SWINGPAINTING) {
                return null;
            }
            if (this.d.width == 0 || this.d.height == 0) {
                this.finish();
            }
            BufferedImage img = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage(this.d.width, this.d.height);
            Graphics2D g2d = img.createGraphics();
            TabDisplayer c = DefaultTabbedContainerUI.this.tabDisplayer;
            c.setBounds(0, 0, this.d.width, this.d.height);
            this.comp.paint(g2d);
            return img;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            float inc = this.customGlassPane.getIncrement();
            if (inc >= 1.0f) {
                this.finish();
            } else {
                this.customGlassPane.setIncrement(inc + DefaultTabbedContainerUI.INCREMENT);
            }
        }

        private ImageScalingGlassPane getCustomGlassPane() {
            if (this.customGlassPane == null) {
                this.customGlassPane = new ImageScalingGlassPane();
                this.customGlassPane.setOpaque(false);
            }
            return this.customGlassPane;
        }

        static /* synthetic */ Dimension access$3300(ImageSlideFxProvider x0) {
            return x0.d;
        }

        private class ImageScalingGlassPane
        extends JPanel {
            private float inc;
            private Rectangle rect;
            private Rectangle r2;
            private boolean changed;

            private ImageScalingGlassPane() {
                this.inc = 0.0f;
                this.rect = new Rectangle();
                this.r2 = new Rectangle();
                this.changed = true;
            }

            private void setIncrement(float inc) {
                this.inc = inc;
                this.changed = true;
                if (this.isShowing()) {
                    Rectangle r = this.getImageBounds();
                    if (DefaultTabbedContainerUI.SYNCHRONOUS_PAINTING) {
                        this.paintImmediately(r.x, r.y, r.width, r.height);
                    } else {
                        this.repaint(r.x, r.y, r.width, r.height);
                    }
                }
            }

            private float getIncrement() {
                return this.inc;
            }

            private Rectangle getImageBounds() {
                if (!this.changed) {
                    return this.rect;
                }
                TabDisplayer c = DefaultTabbedContainerUI.this.tabDisplayer;
                this.r2.setBounds(0, 0, c.getWidth(), c.getHeight());
                Rectangle dispBounds = SwingUtilities.convertRectangle(c, this.r2, this);
                if (ImageSlideFxProvider.this.orientation == TabDisplayer.ORIENTATION_WEST) {
                    this.rect.x = dispBounds.x + dispBounds.width;
                    this.rect.y = dispBounds.y;
                    this.rect.width = Math.round(this.inc * (float)ImageSlideFxProvider.access$3300((ImageSlideFxProvider)ImageSlideFxProvider.this).width);
                    this.rect.height = dispBounds.height;
                } else if (ImageSlideFxProvider.this.orientation == TabDisplayer.ORIENTATION_EAST) {
                    this.rect.width = Math.round(this.inc * (float)ImageSlideFxProvider.access$3300((ImageSlideFxProvider)ImageSlideFxProvider.this).width);
                    this.rect.height = dispBounds.height;
                    this.rect.x = dispBounds.x - this.rect.width;
                    this.rect.y = dispBounds.y;
                } else if (ImageSlideFxProvider.this.orientation == TabDisplayer.ORIENTATION_SOUTH) {
                    this.rect.width = dispBounds.width;
                    this.rect.height = Math.round(this.inc * (float)ImageSlideFxProvider.access$3300((ImageSlideFxProvider)ImageSlideFxProvider.this).height);
                    this.rect.x = dispBounds.x;
                    this.rect.y = dispBounds.y - this.rect.height;
                } else if (ImageSlideFxProvider.this.orientation == TabDisplayer.ORIENTATION_NORTH) {
                    this.rect.x = dispBounds.x;
                    this.rect.y = dispBounds.y + dispBounds.height;
                    this.rect.width = dispBounds.width;
                    this.rect.height = Math.round(this.inc * (float)ImageSlideFxProvider.access$3300((ImageSlideFxProvider)ImageSlideFxProvider.this).height);
                }
                this.changed = false;
                return this.rect;
            }

            @Override
            public void paint(Graphics g) {
                try {
                    if (DefaultTabbedContainerUI.USE_SWINGPAINTING) {
                        SwingUtilities.paintComponent(g, ImageSlideFxProvider.this.comp, this, this.getImageBounds());
                    } else {
                        Graphics2D g2d = (Graphics2D)g;
                        Composite comp = null;
                        comp = g2d.getComposite();
                        g2d.setComposite(AlphaComposite.getInstance(3, Math.min(0.99f, this.inc)));
                        Rectangle r = this.getImageBounds();
                        if (DefaultTabbedContainerUI.NO_SCALE) {
                            AffineTransform at = AffineTransform.getTranslateInstance(r.x, r.y);
                            g2d.drawRenderedImage(ImageSlideFxProvider.this.img, at);
                        } else {
                            g2d.drawImage(ImageSlideFxProvider.this.img, r.x, r.y, r.x + r.width, r.y + r.height, 0, 0, ImageSlideFxProvider.access$3300((ImageSlideFxProvider)ImageSlideFxProvider.this).width, ImageSlideFxProvider.access$3300((ImageSlideFxProvider)ImageSlideFxProvider.this).height, this.getBackground(), null);
                        }
                        if (comp != null) {
                            g2d.setComposite(comp);
                        }
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                    ImageSlideFxProvider.this.finish();
                }
            }
        }

    }

    private final class NoOpFxProvider
    extends FxProvider {
        private NoOpFxProvider() {
        }

        @Override
        public void cleanup() {
        }

        @Override
        protected void doFinish() {
            DefaultTabbedContainerUI.this.showComponent(this.comp);
        }

        @Override
        protected void doStart() {
            this.finish();
        }
    }

    private class SlidingTabsLayout
    implements LayoutManager {
        private SlidingTabsLayout() {
        }

        @Override
        public void addLayoutComponent(String name, Component comp) {
        }

        @Override
        public void layoutContainer(Container parent) {
            TabDisplayer c = DefaultTabbedContainerUI.this.tabDisplayer;
            Object orientation = c.getClientProperty("orientation");
            Dimension d = DefaultTabbedContainerUI.this.tabDisplayer.getPreferredSize();
            Insets ins = DefaultTabbedContainerUI.this.container.getInsets();
            int width = parent.getWidth() - (ins.left + ins.right);
            int height = parent.getHeight() - (ins.top + ins.bottom);
            if (orientation == TabDisplayer.ORIENTATION_NORTH) {
                c.setBounds(ins.left, ins.top, width, d.height);
                DefaultTabbedContainerUI.this.contentDisplayer.setBounds(ins.left, ins.top + d.height, width, parent.getHeight() - (d.height + ins.top + ins.bottom));
            } else if (orientation == TabDisplayer.ORIENTATION_SOUTH) {
                DefaultTabbedContainerUI.this.contentDisplayer.setBounds(ins.top, ins.left, width, parent.getHeight() - (d.height + ins.top + ins.bottom));
                c.setBounds(ins.left, parent.getHeight() - (d.height + ins.top + ins.bottom), width, d.height);
            } else if (orientation == TabDisplayer.ORIENTATION_EAST) {
                DefaultTabbedContainerUI.this.contentDisplayer.setBounds(ins.left, ins.top, width - d.width, height);
                c.setBounds(parent.getWidth() - (ins.right + d.width), ins.top, d.width, height);
            } else if (orientation == TabDisplayer.ORIENTATION_WEST) {
                c.setBounds(ins.left, ins.top, d.width, height);
                DefaultTabbedContainerUI.this.contentDisplayer.setBounds(ins.left + d.width, ins.top, width - d.width, height);
            } else {
                throw new IllegalArgumentException("Unknown orientation: " + orientation);
            }
        }

        @Override
        public Dimension minimumLayoutSize(Container parent) {
            TabDisplayer c = DefaultTabbedContainerUI.this.tabDisplayer;
            Object orientation = c.getClientProperty("orientation");
            Dimension tabSize = DefaultTabbedContainerUI.this.tabDisplayer.getPreferredSize();
            Insets ins = DefaultTabbedContainerUI.this.container.getInsets();
            Dimension result = new Dimension();
            Dimension contentSize = DefaultTabbedContainerUI.this.contentDisplayer.getPreferredSize();
            if (DefaultTabbedContainerUI.this.tabDisplayer.getSelectionModel().getSelectedIndex() == -1) {
                contentSize.width = 0;
                contentSize.height = 0;
            }
            if (orientation == TabDisplayer.ORIENTATION_NORTH || orientation == TabDisplayer.ORIENTATION_SOUTH) {
                result.height = ins.top + ins.bottom + contentSize.height + tabSize.height;
                result.width = ins.left + ins.right + Math.max(contentSize.width, tabSize.width);
            } else {
                result.width = ins.left + ins.right + contentSize.width + tabSize.width;
                result.height = ins.top + ins.bottom + Math.max(contentSize.height, tabSize.height);
            }
            return result;
        }

        @Override
        public Dimension preferredLayoutSize(Container parent) {
            return this.minimumLayoutSize(parent);
        }

        @Override
        public void removeLayoutComponent(Component comp) {
        }
    }

    private class DisplayerActionListener
    implements ActionListener {
        private DisplayerActionListener() {
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            TabActionEvent tae = (TabActionEvent)ae;
            if (!DefaultTabbedContainerUI.this.shouldPerformAction(tae.getActionCommand(), tae.getTabIndex(), tae.getMouseEvent())) {
                tae.consume();
            }
        }
    }

    protected class ModelListener
    implements ComplexListDataListener {
        @Override
        public void contentsChanged(ListDataEvent e) {
            if (e instanceof ComplexListDataEvent) {
                ComplexListDataEvent clde = (ComplexListDataEvent)e;
                int index = clde.getIndex0();
                if (clde.isUserObjectChanged() && index != -1) {
                    boolean add;
                    Component comp = DefaultTabbedContainerUI.this.contentDisplayer.getComponent(index);
                    Component nue = DefaultTabbedContainerUI.this.toComp(DefaultTabbedContainerUI.this.tabDisplayer.getModel().getTab(index));
                    DefaultTabbedContainerUI.this.contentDisplayer.remove(comp);
                    boolean bl = add = DefaultTabbedContainerUI.this.container.getContentPolicy() == 1 || index == DefaultTabbedContainerUI.this.container.getSelectionModel().getSelectedIndex();
                    if (add) {
                        DefaultTabbedContainerUI.this.contentDisplayer.add(nue, index);
                    }
                }
                if (clde.isTextChanged()) {
                    this.maybeMakeSelectedTabVisible(clde);
                }
            }
        }

        private void maybeMakeSelectedTabVisible(ComplexListDataEvent clde) {
            if (!DefaultTabbedContainerUI.this.container.isShowing() || DefaultTabbedContainerUI.this.container.getWidth() < 10) {
                return;
            }
            if (DefaultTabbedContainerUI.this.tabDisplayer.getType() == 1) {
                int idx = DefaultTabbedContainerUI.this.tabDisplayer.getSelectionModel().getSelectedIndex();
                if (clde.getIndex0() == clde.getIndex1() && clde.getIndex0() == idx) {
                    DefaultTabbedContainerUI.this.tabDisplayer.makeTabVisible(idx);
                }
            }
        }

        @Override
        public void intervalAdded(ListDataEvent e) {
            if (DefaultTabbedContainerUI.this.container.getContentPolicy() == 1) {
                Component curC = null;
                for (int i = e.getIndex0(); i <= e.getIndex1(); ++i) {
                    curC = DefaultTabbedContainerUI.this.toComp(DefaultTabbedContainerUI.this.container.getModel().getTab(i));
                    DefaultTabbedContainerUI.this.contentDisplayer.add(curC, "");
                }
            }
        }

        @Override
        public void intervalRemoved(ListDataEvent e) {
            ComplexListDataEvent clde = (ComplexListDataEvent)e;
            TabData[] removedTabs = clde.getAffectedItems();
            for (int i = 0; i < removedTabs.length; ++i) {
                Component curComp = DefaultTabbedContainerUI.this.toComp(removedTabs[i]);
                DefaultTabbedContainerUI.this.contentDisplayer.remove(curComp);
            }
        }

        @Override
        public void indicesAdded(ComplexListDataEvent e) {
            Component curC = null;
            if (DefaultTabbedContainerUI.this.container.getContentPolicy() == 1) {
                int[] indices = e.getIndices();
                for (int i = 0; i < indices.length; ++i) {
                    curC = DefaultTabbedContainerUI.this.toComp(DefaultTabbedContainerUI.this.container.getModel().getTab(indices[i]));
                    DefaultTabbedContainerUI.this.contentDisplayer.add(curC, "");
                }
            }
        }

        @Override
        public void indicesRemoved(ComplexListDataEvent e) {
            int[] indices = e.getIndices();
            TabData[] removedTabs = e.getAffectedItems();
            for (int i = 0; i < indices.length; ++i) {
                Component curComp = DefaultTabbedContainerUI.this.toComp(removedTabs[i]);
                DefaultTabbedContainerUI.this.contentDisplayer.remove(curComp);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void indicesChanged(ComplexListDataEvent e) {
            if (e instanceof VeryComplexListDataEvent) {
                ArrayDiff dif = ((VeryComplexListDataEvent)e).getDiff();
                Set<Integer> deleted = dif.getDeletedIndices();
                Set<Integer> added = dif.getAddedIndices();
                TabData[] old = dif.getOldData();
                TabData[] nue = dif.getNewData();
                HashSet<Component> components = new HashSet<Component>();
                if (DefaultTabbedContainerUI.this.container.getContentPolicy() == 1) {
                    for (int i = 0; i < nue.length; ++i) {
                        components.add(DefaultTabbedContainerUI.this.toComp(nue[i]));
                    }
                }
                boolean changed = false;
                Object object = DefaultTabbedContainerUI.this.contentDisplayer.getTreeLock();
                synchronized (object) {
                    if (!deleted.isEmpty()) {
                        for (Integer idx : deleted) {
                            TabData del = old[idx];
                            if (components.contains(DefaultTabbedContainerUI.this.toComp(del))) continue;
                            DefaultTabbedContainerUI.this.contentDisplayer.remove(DefaultTabbedContainerUI.this.toComp(del));
                            changed = true;
                        }
                    }
                    if (DefaultTabbedContainerUI.this.container.getContentPolicy() == 1 && !added.isEmpty()) {
                        for (Integer idx : added) {
                            TabData add = nue[idx];
                            if (DefaultTabbedContainerUI.this.contentDisplayer.isAncestorOf(DefaultTabbedContainerUI.this.toComp(add))) continue;
                            DefaultTabbedContainerUI.this.contentDisplayer.add(DefaultTabbedContainerUI.this.toComp(add), "");
                            changed = true;
                        }
                    }
                }
                if (changed) {
                    DefaultTabbedContainerUI.this.contentDisplayer.revalidate();
                    DefaultTabbedContainerUI.this.contentDisplayer.repaint();
                }
            }
        }
    }

    protected class SelectionListener
    implements ChangeListener {
        @Override
        public void stateChanged(ChangeEvent e) {
            if (DefaultTabbedContainerUI.this.container.isShowing() || DefaultTabbedContainerUI.this.container.getClientProperty("tc") != null) {
                int idx = DefaultTabbedContainerUI.this.tabDisplayer.getSelectionModel().getSelectedIndex();
                if (idx != -1) {
                    Component c = DefaultTabbedContainerUI.this.toComp(DefaultTabbedContainerUI.this.container.getModel().getTab(idx));
                    c.setBounds(0, 0, DefaultTabbedContainerUI.this.contentDisplayer.getWidth(), DefaultTabbedContainerUI.this.contentDisplayer.getHeight());
                    DefaultTabbedContainerUI.this.showComponentWithFxProvider(c);
                } else {
                    DefaultTabbedContainerUI.this.showComponent(null);
                }
            }
        }
    }

    private class ToolbarTabsLayout
    implements LayoutManager {
        private ToolbarTabsLayout() {
        }

        @Override
        public void layoutContainer(Container container) {
            Dimension tabSize = DefaultTabbedContainerUI.this.tabDisplayer.getPreferredSize();
            Insets ins = container.getInsets();
            int w = container.getWidth() - (ins.left + ins.right);
            DefaultTabbedContainerUI.this.tabDisplayer.setBounds(ins.left, ins.top, w, tabSize.height);
            if (DefaultTabbedContainerUI.this.tabDisplayer.getModel().size() > 1) {
                Dimension newTabSize = DefaultTabbedContainerUI.this.tabDisplayer.getPreferredSize();
                if (newTabSize.height != tabSize.height) {
                    tabSize = newTabSize;
                    DefaultTabbedContainerUI.this.tabDisplayer.setBounds(ins.left, ins.top, w, tabSize.height);
                }
            }
            DefaultTabbedContainerUI.this.contentDisplayer.setBounds(ins.left, ins.top + tabSize.height, w, container.getHeight() - (ins.top + ins.bottom + tabSize.height));
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            Dimension tabSize = DefaultTabbedContainerUI.this.tabDisplayer.getMinimumSize();
            Dimension contentSize = DefaultTabbedContainerUI.this.contentDisplayer.getMinimumSize();
            Insets ins = container.getInsets();
            Dimension result = new Dimension(ins.left + ins.top, ins.right + ins.bottom);
            result.width += Math.max(tabSize.width, contentSize.width);
            result.height += tabSize.height + contentSize.height;
            return result;
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            Dimension tabSize = DefaultTabbedContainerUI.this.tabDisplayer.getPreferredSize();
            Dimension contentSize = DefaultTabbedContainerUI.this.contentDisplayer.getPreferredSize();
            Insets ins = container.getInsets();
            Dimension result = new Dimension(ins.left + ins.top, ins.right + ins.bottom);
            result.width += Math.max(tabSize.width, contentSize.width);
            result.height += tabSize.height + contentSize.height;
            return result;
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public void addLayoutComponent(String str, Component component) {
        }
    }

    private class ContainerHierarchyListener
    implements HierarchyListener {
        @Override
        public void hierarchyChanged(HierarchyEvent e) {
            if ((e.getChangeFlags() & 4) != 0) {
                boolean showing = DefaultTabbedContainerUI.this.container.isShowing();
                if (showing != DefaultTabbedContainerUI.this.bug4924561knownShowing) {
                    if (DefaultTabbedContainerUI.this.container.isShowing()) {
                        DefaultTabbedContainerUI.this.initDisplayer();
                        DefaultTabbedContainerUI.this.attachModelAndSelectionListeners();
                        DefaultTabbedContainerUI.this.ensureSelectedComponentIsShowing();
                        if (DefaultTabbedContainerUI.this.container.getType() == 2) {
                            DefaultTabbedContainerUI.this.updateOrientation();
                        }
                    } else {
                        DefaultTabbedContainerUI.this.detachModelAndSelectionListeners();
                        if (DefaultTabbedContainerUI.this.container.getType() == 2) {
                            DefaultTabbedContainerUI.this.updateOrientation();
                        }
                    }
                }
                DefaultTabbedContainerUI.this.bug4924561knownShowing = showing;
            }
        }
    }

    protected class ContainerComponentListener
    extends ComponentAdapter {
        @Override
        public void componentMoved(ComponentEvent e) {
            if (DefaultTabbedContainerUI.this.container.getType() == 2) {
                DefaultTabbedContainerUI.this.updateOrientation();
            }
        }

        @Override
        public void componentResized(ComponentEvent e) {
            if (DefaultTabbedContainerUI.this.container.getType() == 2) {
                DefaultTabbedContainerUI.this.updateOrientation();
            }
        }
    }

    protected class ContainerPropertyChangeListener
    implements PropertyChangeListener {
        protected ContainerPropertyChangeListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("active".equals(evt.getPropertyName())) {
                DefaultTabbedContainerUI.this.updateActiveState();
            }
        }
    }

    private static final class DefaultWindowBorder
    implements Border {
        private static final Insets insets = new Insets(1, 1, 2, 2);

        private DefaultWindowBorder() {
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            g.translate(x, y);
            g.setColor(UIManager.getColor("controlShadow"));
            g.drawRect(0, 0, w - 2, h - 2);
            g.setColor(UIManager.getColor("controlHighlight"));
            g.drawLine(w - 1, 1, w - 1, h - 1);
            g.drawLine(1, h - 1, w - 1, h - 1);
            g.translate(- x, - y);
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return insets;
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }
    }

}

