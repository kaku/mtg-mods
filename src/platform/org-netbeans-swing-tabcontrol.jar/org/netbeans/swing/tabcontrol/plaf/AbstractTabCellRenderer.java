/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ContainerListener;
import java.awt.event.HierarchyBoundsListener;
import java.awt.event.HierarchyListener;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.plaf.AquaEditorTabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.ColorUtil;
import org.netbeans.swing.tabcontrol.plaf.TabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.TabPainter;
import org.netbeans.swing.tabcontrol.plaf.TabState;
import org.openide.awt.HtmlRenderer;

public abstract class AbstractTabCellRenderer
extends JLabel
implements TabCellRenderer {
    private int state = 64;
    TabPainter leftBorder;
    TabPainter rightBorder;
    TabPainter normalBorder;
    private Dimension padding;
    private boolean showClose = true;
    private Rectangle scratch = new Rectangle();

    public AbstractTabCellRenderer(TabPainter leftClip, TabPainter noClip, TabPainter rightClip, Dimension padding) {
        this.setOpaque(false);
        this.setFocusable(false);
        this.setBorder(noClip);
        this.normalBorder = noClip;
        this.leftBorder = leftClip;
        this.rightBorder = rightClip;
        this.padding = padding;
    }

    public AbstractTabCellRenderer(TabPainter painter, Dimension padding) {
        this(painter, painter, painter, padding);
    }

    @Override
    public final void setShowCloseButton(boolean b) {
        this.showClose = b;
    }

    @Override
    public final boolean isShowCloseButton() {
        return this.showClose;
    }

    @Override
    public String getCommandAtPoint(Point p, int tabState, Rectangle bounds) {
        Polygon tabShape;
        this.setBounds(bounds);
        this.setState(tabState);
        if (this.supportsCloseButton(this.getBorder()) && this.isShowCloseButton()) {
            TabPainter cbp = (TabPainter)this.getBorder();
            cbp.getCloseButtonRectangle(this, this.scratch, bounds);
            if (this.getClass() != AquaEditorTabCellRenderer.class) {
                this.scratch.x -= 3;
                this.scratch.y -= 3;
                this.scratch.width += 6;
                this.scratch.height += 6;
            }
            if (this.scratch.contains(p)) {
                return "close";
            }
        }
        if ((tabShape = this.getTabShape(tabState, bounds)).contains(p)) {
            return "select";
        }
        return null;
    }

    @Override
    public String getCommandAtPoint(Point p, int tabState, Rectangle bounds, int mouseButton, int eventType, int modifiers) {
        String result = null;
        result = mouseButton == 2 && eventType == 502 ? "close" : this.getCommandAtPoint(p, tabState, bounds);
        if (result != null) {
            if ("select" == result) {
                boolean clipped;
                boolean bl = clipped = this.isClipLeft() || this.isClipRight();
                if (clipped && eventType == 502 && mouseButton == 1 || !clipped && eventType == 501 && mouseButton == 1) {
                    return result;
                }
            } else if ("close" == result && eventType == 502 && this.isShowCloseButton()) {
                if ((modifiers & 64) != 0) {
                    return "closeAll";
                }
                if ((modifiers & 512) != 0 && mouseButton != 2) {
                    return "closeAllButThis";
                }
                if (((tabState & 512) == 0 || (tabState & 8192) == 0) && mouseButton == 1) {
                    result = "select";
                }
                return result;
            }
        }
        return null;
    }

    protected final boolean isArmed() {
        return this.isPressed() || (this.state & 4) != 0;
    }

    protected final boolean isActive() {
        return (this.state & 32) != 0;
    }

    protected final boolean isPressed() {
        return (this.state & 8) != 0;
    }

    protected final boolean isSelected() {
        return (this.state & 16) != 0;
    }

    protected final boolean isClipRight() {
        return (this.state & 1) != 0;
    }

    protected final boolean isClipLeft() {
        return (this.state & 2) != 0;
    }

    protected final boolean isLeftmost() {
        return (this.state & 128) != 0;
    }

    protected final boolean isRightmost() {
        return (this.state & 256) != 0;
    }

    protected final boolean isAttention() {
        return (this.state & 16384) != 0 || (this.state & 131072) != 0;
    }

    protected final boolean isHighlight() {
        return (this.state & 131072) != 0;
    }

    protected final boolean isNextTabSelected() {
        return (this.state & 1024) != 0;
    }

    protected final boolean isNextTabArmed() {
        return (this.state & 32768) != 0;
    }

    protected final boolean isPreviousTabSelected() {
        return (this.state & 2048) != 0;
    }

    protected final boolean isBusy() {
        return (this.state & 65536) != 0;
    }

    @Override
    public Dimension getPadding() {
        return new Dimension(this.padding);
    }

    protected final void setState(int state) {
        boolean needChange;
        boolean bl = needChange = this.state != state;
        if (needChange) {
            int old = this.state;
            this.state = state;
            int newState = this.stateChanged(old, state);
            if ((newState & this.state) != state) {
                this.state = state;
                throw new IllegalStateException("StateChanged may add, but not remove bits from the state bitmask.  Expected state: " + TabState.stateToString(state) + " but got " + TabState.stateToString(this.state));
            }
            this.state = newState;
        }
    }

    public final int getState() {
        return this.state;
    }

    @Override
    public final JComponent getRendererComponent(TabData data, Rectangle bounds, int state) {
        this.setBounds(bounds);
        this.setText(data.getText());
        this.setIcon(data.getIcon());
        this.setState(state);
        return this;
    }

    protected int stateChanged(int oldState, int newState) {
        Color fg;
        Color bg;
        Color color = this.isSelected() ? (this.isActive() ? this.getSelectedActivatedBackground() : this.getSelectedBackground()) : (bg = UIManager.getColor("control"));
        Color color2 = this.isSelected() ? (this.isActive() ? this.getSelectedActivatedForeground() : this.getSelectedForeground()) : (fg = UIManager.getColor("textText"));
        if (this.isArmed() && this.isPressed() && (this.isClipLeft() || this.isClipRight())) {
            bg = this.getSelectedActivatedBackground();
            fg = this.getSelectedActivatedForeground();
        }
        if (this.isClipLeft()) {
            this.setIcon(null);
            this.setBorder(this.leftBorder);
        } else if (this.isClipRight()) {
            this.setBorder(this.rightBorder);
        } else {
            this.setBorder(this.normalBorder);
        }
        this.setBackground(bg);
        this.setForeground(fg);
        return newState;
    }

    @Override
    public void revalidate() {
    }

    @Override
    public void repaint() {
    }

    @Override
    public void validate() {
    }

    @Override
    public void repaint(long tm) {
    }

    @Override
    public void repaint(long tm, int x, int y, int w, int h) {
    }

    @Override
    protected final void firePropertyChange(String s, Object a, Object b) {
    }

    @Override
    public final void addHierarchyBoundsListener(HierarchyBoundsListener hbl) {
    }

    @Override
    public final void addHierarchyListener(HierarchyListener hl) {
    }

    @Override
    public final void addContainerListener(ContainerListener cl) {
    }

    @Override
    public void paintComponent(Graphics g) {
        g.setColor(this.getBackground());
        if (this.getBorder() instanceof TabPainter) {
            ((TabPainter)this.getBorder()).paintInterior(g, this);
        }
        this.paintIconAndText(g);
    }

    protected int getCaptionYAdjustment() {
        return -1;
    }

    protected int getIconYAdjustment() {
        return -1;
    }

    protected void paintIconAndText(Graphics g) {
        int txtX;
        g.setFont(this.getFont());
        FontMetrics fm = g.getFontMetrics(this.getFont());
        int txtH = fm.getHeight();
        Insets ins = this.getInsets();
        int availH = this.getHeight() - (ins.top + ins.bottom);
        int txtY = availH > txtH ? txtH + ins.top + (availH / 2 - txtH / 2) - 3 : txtH + ins.top;
        int centeringToAdd = this.getPixelsToAddToSelection() != 0 ? this.getPixelsToAddToSelection() / 2 : 0;
        Icon icon = this.getIcon();
        if (!this.isClipLeft() && icon != null && icon.getIconWidth() > 0 && icon.getIconHeight() > 0) {
            int iconY = availH > icon.getIconHeight() ? ins.top + (availH / 2 - icon.getIconHeight() / 2) + 2 : ins.top + 2;
            int iconX = ins.left + centeringToAdd;
            icon.paintIcon(this, g, iconX, iconY += this.getIconYAdjustment());
            txtX = iconX + icon.getIconWidth() + this.getIconTextGap();
        } else {
            txtX = ins.left + centeringToAdd;
        }
        if (icon != null && icon.getIconWidth() == 0) {
            txtX += 5;
        }
        txtY += this.getCaptionYAdjustment();
        int txtW = this.getWidth() - (txtX + ins.right);
        if (this.isClipLeft()) {
            String s = AbstractTabCellRenderer.preTruncateString(this.getText(), g, txtW - 4);
            Graphics2D g2d = null;
            Shape clip = null;
            if (g instanceof Graphics2D) {
                g2d = (Graphics2D)g;
                clip = g2d.getClip();
                g2d.clipRect(ins.left, ins.top, this.getWidth() - ins.left - ins.right, this.getHeight() - ins.top - ins.bottom);
            }
            txtW = (int)HtmlRenderer.renderString((String)s, (Graphics)g, (int)txtX, (int)txtY, (int)Integer.MAX_VALUE, (int)Integer.MAX_VALUE, (Font)this.getFont(), (Color)this.getForeground(), (int)0, (boolean)false);
            txtX = this.getWidth() - ins.right - txtW;
            txtW = (int)HtmlRenderer.renderString((String)s, (Graphics)g, (int)txtX, (int)txtY, (int)txtW, (int)txtH, (Font)this.getFont(), (Color)this.getForeground(), (int)0, (boolean)true);
            if (null != g2d) {
                g2d.setClip(clip);
            }
        } else {
            String s = this.isClipRight() ? this.getText() + "..." : this.getText();
            txtW = (int)HtmlRenderer.renderString((String)s, (Graphics)g, (int)txtX, (int)txtY, (int)txtW, (int)txtH, (Font)this.getFont(), (Color)this.getForeground(), (int)1, (boolean)true);
        }
    }

    static String preTruncateString(String s, Graphics g, int availPixels) {
        String test;
        if (s.length() < 3) {
            return s;
        }
        if ((s = AbstractTabCellRenderer.stripHTML(s)).length() < 2) {
            return "..." + s;
        }
        FontMetrics fm = g.getFontMetrics();
        int dotsWidth = fm.stringWidth("...");
        int beginIndex = s.length() - 2;
        String result = test = s.substring(beginIndex);
        while (fm.stringWidth(test) + dotsWidth < availPixels && --beginIndex > 0) {
            result = test;
            test = s.substring(beginIndex);
        }
        return "..." + result;
    }

    static boolean isHTML(String s) {
        boolean result = s.startsWith("<html>") || s.startsWith("<HTML>");
        return result;
    }

    static String stripHTML(String s) {
        if (AbstractTabCellRenderer.isHTML(s)) {
            StringBuffer result = new StringBuffer(s.length());
            char[] c = s.toCharArray();
            boolean inTag = false;
            for (int i = 0; i < c.length; ++i) {
                boolean wasInTag = inTag;
                if (!inTag) {
                    if (c[i] == '<') {
                        inTag = true;
                    }
                } else if (c[i] == '>') {
                    inTag = false;
                }
                if (inTag || wasInTag != inTag) continue;
                result.append(c[i]);
            }
            return result.toString();
        }
        return s;
    }

    @Override
    public Polygon getTabShape(int tabState, Rectangle bounds) {
        this.setBounds(bounds);
        this.setState(tabState);
        if (this.getBorder() instanceof TabPainter) {
            TabPainter pb = (TabPainter)this.getBorder();
            Polygon p = pb.getInteriorPolygon(this);
            p.translate(bounds.x, bounds.y);
            return p;
        }
        return new Polygon(new int[]{bounds.x, bounds.x + bounds.width - 1, bounds.x + bounds.width - 1, bounds.x}, new int[]{bounds.y, bounds.y, bounds.y + bounds.height - 1, bounds.y + bounds.height - 1}, 4);
    }

    @Override
    public Color getSelectedBackground() {
        Color base = UIManager.getColor("control");
        Color towards = UIManager.getColor("controlHighlight");
        if (base == null) {
            base = Color.GRAY;
        }
        if (towards == null) {
            towards = Color.WHITE;
        }
        Color result = ColorUtil.adjustTowards(base, 30, towards);
        return result;
    }

    @Override
    public Color getSelectedActivatedBackground() {
        return UIManager.getColor("TabRenderer.selectedActivatedBackground");
    }

    public Color getSelectedActivatedForeground() {
        return UIManager.getColor("TabRenderer.selectedActivatedForeground");
    }

    public Color getSelectedForeground() {
        return UIManager.getColor("TabRenderer.selectedForeground");
    }

    protected boolean inCloseButton() {
        return (this.state & 512) != 0;
    }

    @Override
    public int getPixelsToAddToSelection() {
        return 0;
    }

    private boolean supportsCloseButton(Border b) {
        if (b instanceof TabPainter) {
            return ((TabPainter)b).supportsCloseButton(this);
        }
        return false;
    }
}

