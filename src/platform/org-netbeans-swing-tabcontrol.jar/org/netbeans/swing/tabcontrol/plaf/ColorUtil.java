/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import org.netbeans.swing.tabcontrol.plaf.MetalViewTabDisplayerUI;

final class ColorUtil {
    private static Map<Integer, GradientPaint> gpCache = null;
    private static Map<RenderingHints.Key, Object> hintsMap = null;
    private static final boolean noGpCache = Boolean.getBoolean("netbeans.winsys.nogpcache");
    private static final boolean noAntialias = Boolean.getBoolean("nb.no.antialias");
    private static int focusedHeight = -1;
    private static int unfocusedHeight = -1;
    private static Icon unfocused = null;
    private static Icon focused = null;
    private static Rectangle scratch = new Rectangle();
    private static final int DEFAULT_IMAGE_WIDTH = 200;
    public static final int SEL_TYPE = 1;
    public static final int UNSEL_TYPE = 2;
    public static final int FOCUS_TYPE = 4;
    public static final int XP_REGULAR_TAB = 0;
    public static final int XP_HIGHLIGHTED_TAB = 1;
    public static final int XP_BORDER_RIGHT = 1;
    public static final int XP_BORDER_BOTTOM = 2;
    private static Icon XP_DRAG_IMAGE;
    private static Icon VISTA_DRAG_IMAGE;
    private static final boolean antialias;

    private ColorUtil() {
    }

    public static Color getMiddle(Color c1, Color c2) {
        return new Color((c1.getRed() + c2.getRed()) / 2, (c1.getGreen() + c2.getGreen()) / 2, (c1.getBlue() + c2.getBlue()) / 2);
    }

    public static GradientPaint getGradientPaint(float x1, float y1, Color upper, float x2, float y2, Color lower) {
        return ColorUtil.getGradientPaint(x1, y1, upper, x2, y2, lower, false);
    }

    public static GradientPaint getGradientPaint(float x1, float y1, Color upper, float x2, float y2, Color lower, boolean repeats) {
        boolean vertical;
        if (noGpCache) {
            return new GradientPaint(x1, y1, upper, x2, y2, lower, repeats);
        }
        if (upper == null) {
            upper = Color.BLUE;
        }
        if (lower == null) {
            lower = Color.ORANGE;
        }
        if (gpCache == null) {
            gpCache = new HashMap<Integer, GradientPaint>(20);
        }
        boolean horizontal = x1 == x2;
        boolean bl = vertical = y1 == y2;
        if (horizontal && vertical) {
            y1 = x1 + 28.0f;
        } else if (horizontal && !repeats) {
            x1 = 0.0f;
            x2 = 0.0f;
        } else if (vertical && !repeats) {
            y1 = 0.0f;
            y2 = 0.0f;
        }
        long bits = Double.doubleToLongBits(x1) + Double.doubleToLongBits(y1) * 37 + Double.doubleToLongBits(x2) * 43 + Double.doubleToLongBits(y2) * 47;
        int hash = ((int)bits ^ (int)(bits >> 32) ^ upper.hashCode() ^ lower.hashCode() * 17) * (repeats ? 31 : 1);
        Integer key = new Integer(hash);
        GradientPaint result = gpCache.get(key);
        if (result == null) {
            result = new GradientPaint(x1, y1, upper, x2, y2, lower, repeats);
            if (gpCache.size() > 40) {
                gpCache.clear();
            }
            gpCache.put(key, result);
        }
        return result;
    }

    private static Map getHints() {
        if (hintsMap == null && (ColorUtil.hintsMap = (Map)Toolkit.getDefaultToolkit().getDesktopProperty("awt.font.desktophints")) == null) {
            hintsMap = new HashMap<RenderingHints.Key, Object>();
            if (ColorUtil.shouldAntialias()) {
                hintsMap.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            }
        }
        return hintsMap;
    }

    public static final void setupAntialiasing(Graphics g) {
        if (noAntialias) {
            return;
        }
        ((Graphics2D)g).addRenderingHints(ColorUtil.getHints());
    }

    public static final boolean shouldAntialias() {
        return antialias;
    }

    private static final boolean gtkShouldAntialias() {
        Object o = Toolkit.getDefaultToolkit().getDesktopProperty("gnome.Xft/Antialias");
        return new Integer(1).equals(o);
    }

    public static boolean isBrighter(Color a, Color b) {
        int[] ac = new int[]{a.getRed(), a.getGreen(), a.getBlue()};
        int[] bc = new int[]{b.getRed(), b.getGreen(), b.getBlue()};
        int dif = 0;
        for (int i = 0; i < 3; ++i) {
            int currDif = ac[i] - bc[i];
            if (Math.abs(currDif) <= Math.abs(dif)) continue;
            dif = currDif;
        }
        return dif > 0;
    }

    private static int minMax(int i) {
        if (i < 0) {
            return 0;
        }
        if (i > 255) {
            return 255;
        }
        return i;
    }

    public static int averageDifference(Color a, Color b) {
        int[] ac = new int[]{a.getRed(), a.getGreen(), a.getBlue()};
        int[] bc = new int[]{b.getRed(), b.getGreen(), b.getBlue()};
        int dif = 0;
        for (int i = 0; i < 3; ++i) {
            dif += bc[i] - ac[i];
        }
        return dif / 3;
    }

    public static Color adjustComponentsTowards(Color toAdjust, Color towards) {
        int r = toAdjust.getRed();
        int g = toAdjust.getGreen();
        int b = toAdjust.getBlue();
        int ra = towards.getRed();
        int ga = towards.getGreen();
        int ba = towards.getBlue();
        r += ColorUtil.minMax((ra - r) / 3);
        g += ColorUtil.minMax((ga - g) / 3);
        b += ColorUtil.minMax((ba - b) / 3);
        return new Color(r, g, b);
    }

    public static Color adjustTowards(Color toAdjust, int amount, Color towards) {
        int r = toAdjust.getRed();
        int g = toAdjust.getGreen();
        int b = toAdjust.getBlue();
        int factor = ColorUtil.isBrighter(towards, toAdjust) ? 1 : -1;
        r = ColorUtil.minMax(r + factor * amount);
        g = ColorUtil.minMax(g + factor * amount);
        b = ColorUtil.minMax(b + factor * amount);
        return new Color(r, g, b);
    }

    public static Color adjustBy(Color toAdjust, int amount) {
        int r = ColorUtil.minMax(toAdjust.getRed() + amount);
        int g = ColorUtil.minMax(toAdjust.getGreen() + amount);
        int b = ColorUtil.minMax(toAdjust.getBlue() + amount);
        return new Color(r, g, b);
    }

    public static Color adjustBy(Color toAdjust, int[] amounts) {
        int r = ColorUtil.minMax(toAdjust.getRed() + amounts[0]);
        int g = ColorUtil.minMax(toAdjust.getGreen() + amounts[1]);
        int b = ColorUtil.minMax(toAdjust.getBlue() + amounts[2]);
        return new Color(r, g, b);
    }

    private static float minMax(float f) {
        return Math.max(0.0f, Math.min(1.0f, f));
    }

    public static void paintViewTabBump(Graphics g, int x, int y, int width, int height, int type) {
        ColorUtil.drawTexture(g, x, y, width, height, type, 0);
    }

    public static void paintDocTabBump(Graphics g, int x, int y, int width, int height, int type) {
        ColorUtil.drawTexture(g, x, y, width, height, type, 2);
    }

    private static void _drawTexture(Graphics g, int x, int y, int width, int height, int type, int yDecline) {
        Color brightC = UIManager.getColor("TabbedPane.highlight");
        Color darkC = type == 4 ? UIManager.getColor("TabbedPane.focus") : UIManager.getColor("controlDkShadow");
        if (width % 2 != 0) {
            --width;
        }
        if (height % 2 != 0) {
            --height;
        }
        for (int curX = x; curX < x + width; ++curX) {
            g.setColor((curX - x) % 2 == 0 ? brightC : darkC);
            for (int curY = y + (curX - x + yDecline) % 4; curY < y + height; curY += 4) {
                g.drawLine(curX, curY, curX, curY);
            }
        }
    }

    private static void drawTexture(Graphics g, int x, int y, int width, int height, int type, int yDecline) {
        if (!g.hitClip(x, y, width, height)) {
            return;
        }
        if (type == 4) {
            if (focused == null || height > focusedHeight * 2) {
                BufferedImage img = ColorUtil.createBitmap(height, type, yDecline);
                focusedHeight = height;
                focused = new ImageIcon(img);
            }
            ColorUtil.blitBitmap(g, focused, x, y, width, height);
        } else {
            if (unfocused == null || unfocusedHeight > height * 2) {
                BufferedImage img = ColorUtil.createBitmap(height, type, yDecline);
                unfocusedHeight = height;
                unfocused = new ImageIcon(img);
            }
            ColorUtil.blitBitmap(g, unfocused, x, y, width, height);
        }
    }

    private static BufferedImage createBitmap(int height, int type, int yDecline) {
        BufferedImage result = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage(200, height * 2);
        Graphics g = result.getGraphics();
        if (result.getAlphaRaster() == null) {
            Color c = type == 4 ? MetalViewTabDisplayerUI.getActBgColor() : MetalViewTabDisplayerUI.getInactBgColor();
            g.setColor(c);
            g.fillRect(0, 0, 200, height * 2);
        }
        ColorUtil._drawTexture(g, 0, 0, 200, height * 2, type, yDecline);
        return result;
    }

    private static void blitBitmap(Graphics g, Icon icon, int x, int y, int w, int h) {
        Shape clip = g.getClip();
        if (clip == null) {
            g.setClip(x, y, w, h);
        } else {
            scratch.setBounds(x, y, w, h);
            Area area = new Area(clip);
            area.intersect(new Area(scratch));
            g.setClip(area);
        }
        int iwidth = icon.getIconWidth();
        for (int widthPainted = 0; widthPainted < w; widthPainted += iwidth) {
            icon.paintIcon(null, g, x + widthPainted, y);
        }
        g.setClip(clip);
    }

    public static void paintXpTabHeader(int type, Graphics g, int x, int y, int width) {
        Color capBorderC = ColorUtil.getXpHeaderColor(type, false);
        Color capFillC = ColorUtil.getXpHeaderColor(type, true);
        g.setColor(capBorderC);
        g.drawLine(x + 2, y, x + width - 3, y);
        g.drawLine(x + 2, y, x, y + 2);
        g.drawLine(x + width - 3, y, x + width - 1, y + 2);
        g.setColor(capFillC);
        g.drawLine(x + 2, y + 1, x + width - 3, y + 1);
        g.drawLine(x + 1, y + 2, x + width - 2, y + 2);
    }

    public static void xpFillRectGradient(Graphics2D g, Rectangle rect, Color brightC, Color darkC) {
        ColorUtil.xpFillRectGradient(g, rect.x, rect.y, rect.width, rect.height, brightC, darkC, 3);
    }

    public static void xpFillRectGradient(Graphics2D g, int x, int y, int width, int height, Color brightC, Color darkC) {
        ColorUtil.xpFillRectGradient(g, x, y, width, height, brightC, darkC, 3);
    }

    public static void xpFillRectGradient(Graphics2D g, int x, int y, int width, int height, Color brightC, Color darkC, int borderType) {
        ColorUtil.paintXpGradientBorder(g, x, y, width, height, darkC, borderType);
        int gradWidth = (borderType & 1) != 0 ? width - 2 : width;
        int gradHeight = (borderType & 2) != 0 ? height - 2 : height;
        ColorUtil.paintXpGradientFill(g, x, y, gradWidth, gradHeight, brightC, darkC);
    }

    public static void paintXpTabDragTexture(Component control, Graphics g, int x, int y, int height) {
        if (XP_DRAG_IMAGE == null) {
            XP_DRAG_IMAGE = ColorUtil.initXpDragTextureImage();
        }
        int count = height / 4;
        int ypos = y;
        for (int i = 0; i < count; ++i) {
            XP_DRAG_IMAGE.paintIcon(control, g, x, ypos);
            ypos += 4;
        }
    }

    public static void vistaFillRectGradient(Graphics2D g, Rectangle rect, Color brightUpperC, Color darkUpperC, Color brightLowerC, Color darkLowerC) {
        ColorUtil.vistaFillRectGradient(g, rect.x, rect.y, rect.width, rect.height, brightUpperC, darkUpperC, brightLowerC, darkLowerC);
    }

    public static void vistaFillRectGradient(Graphics2D g, int x, int y, int width, int height, Color brightUpperC, Color darkUpperC, Color brightLowerC, Color darkLowerC) {
        ColorUtil.paintVistaGradientFill(g, x, y, width, height / 2, brightUpperC, darkUpperC);
        ColorUtil.paintVistaGradientFill(g, x, y + height / 2, width, height - height / 2, brightLowerC, darkLowerC);
    }

    public static void vistaFillRectGradient(Graphics2D g, Rectangle rect, Color upperC, Color brightLowerC, Color darkLowerC) {
        ColorUtil.vistaFillRectGradient(g, rect.x, rect.y, rect.width, rect.height, upperC, brightLowerC, darkLowerC);
    }

    public static void vistaFillRectGradient(Graphics2D g, int x, int y, int width, int height, Color upperC, Color brightLowerC, Color darkLowerC) {
        g.setColor(upperC);
        g.fillRect(x, y, width, height / 2);
        ColorUtil.paintVistaGradientFill(g, x, y + height / 2, width, height - height / 2, brightLowerC, darkLowerC);
    }

    public static void paintVistaTabDragTexture(Component control, Graphics g, int x, int y, int height) {
        if (VISTA_DRAG_IMAGE == null) {
            VISTA_DRAG_IMAGE = ColorUtil.initVistaDragTextureImage();
        }
        int count = height / 4;
        int ypos = y;
        g.setColor(Color.WHITE);
        for (int i = 0; i < count; ++i) {
            VISTA_DRAG_IMAGE.paintIcon(control, g, x, ypos);
            g.drawLine(x + 1, ypos + 2, x + 2, ypos + 2);
            g.drawLine(x + 2, ypos + 1, x + 2, ypos + 1);
            ypos += 4;
        }
    }

    public static Color adjustColor(Color c, int rDiff, int gDiff, int bDiff) {
        if (c == null) {
            c = Color.GRAY;
        }
        int red = Math.max(0, Math.min(255, c.getRed() + rDiff));
        int green = Math.max(0, Math.min(255, c.getGreen() + gDiff));
        int blue = Math.max(0, Math.min(255, c.getBlue() + bDiff));
        return new Color(red, green, blue);
    }

    private static void paintXpGradientBorder(Graphics g, int x, int y, int width, int height, Color darkC, int borderType) {
        Color color;
        if ((borderType & 1) != 0) {
            color = ColorUtil.adjustColor(darkC, -6, -5, -3);
            g.setColor(color);
            g.drawLine(x + width - 2, y, x + width - 2, y + height - 2);
            color = ColorUtil.adjustColor(darkC, -27, -26, -20);
            g.setColor(color);
            g.drawLine(x + width - 1, y, x + width - 1, y + height - 1);
        }
        if ((borderType & 2) != 0) {
            color = ColorUtil.adjustColor(darkC, -6, -5, -3);
            g.setColor(color);
            g.drawLine(x, y + height - 2, x + width - 2, y + height - 2);
            color = ColorUtil.adjustColor(darkC, -27, -26, -20);
            g.setColor(color);
            g.drawLine(x, y + height - 1, x + width - 1, y + height - 1);
        }
    }

    private static void paintXpGradientFill(Graphics2D g, int x, int y, int width, int height, Color brightC, Color darkC) {
        GradientPaint gradient = ColorUtil.getGradientPaint(x, y, brightC, x, y + height, darkC);
        g.setPaint(gradient);
        g.fillRect(x, y, width, height);
    }

    private static Color getXpHeaderColor(int type, boolean fill) {
        String colorKey = null;
        switch (type) {
            case 0: {
                colorKey = fill ? "tab_unsel_fill_bright" : "tab_border";
                break;
            }
            case 1: {
                colorKey = fill ? "tab_highlight_header_fill" : "tab_highlight_header";
                break;
            }
            default: {
                throw new IllegalArgumentException("Unknown type of tab header: " + type);
            }
        }
        return UIManager.getColor(colorKey);
    }

    private static final Icon initXpDragTextureImage() {
        BufferedImage i = new BufferedImage(3, 3, 1);
        Color hl = UIManager.getColor("controlLtHighlight");
        i.setRGB(2, 2, hl.getRGB());
        i.setRGB(2, 1, hl.getRGB());
        i.setRGB(1, 2, hl.getRGB());
        Color dk = UIManager.getColor("TabbedPane.darkShadow");
        i.setRGB(1, 1, dk.getRGB());
        Color corners = UIManager.getColor("TabbedPane.light");
        i.setRGB(0, 2, corners.getRGB());
        i.setRGB(2, 0, corners.getRGB());
        Color dk2 = UIManager.getColor("TabbedPane.shadow");
        i.setRGB(0, 1, dk2.getRGB());
        i.setRGB(1, 0, dk2.getRGB());
        Color up = UIManager.getColor("inactiveCaptionBorder");
        i.setRGB(0, 0, up.getRGB());
        return new ImageIcon(i);
    }

    public static void paintMacGradientFill(Graphics2D g, Rectangle rect, Color brightC, Color darkC) {
        Paint oldPaint = g.getPaint();
        if (null == brightC) {
            brightC = Color.gray;
        }
        if (null == darkC) {
            darkC = Color.gray;
        }
        g.setPaint(new GradientPaint(rect.x, rect.y, brightC, rect.x, rect.y + rect.height / 2, darkC));
        g.fillRect(rect.x, rect.y, rect.width, rect.height);
        g.setPaint(oldPaint);
    }

    private static void paintVistaGradientFill(Graphics2D g, int x, int y, int width, int height, Color brightC, Color darkC) {
        GradientPaint gradient = ColorUtil.getGradientPaint(x, y, brightC, x, y + height, darkC);
        g.setPaint(gradient);
        g.fillRect(x, y, width, height);
    }

    private static final Icon initVistaDragTextureImage() {
        BufferedImage i = new BufferedImage(2, 2, 1);
        int grey = new Color(124, 124, 124).getRGB();
        i.setRGB(1, 0, grey);
        i.setRGB(0, 1, grey);
        i.setRGB(0, 0, new Color(162, 163, 164).getRGB());
        i.setRGB(1, 1, new Color(107, 107, 107).getRGB());
        return new ImageIcon(i);
    }

    public boolean isBlueprintTheme() {
        return "blueprint".equals(Toolkit.getDefaultToolkit().getDesktopProperty("gnome.Net/ThemeName"));
    }

    static {
        antialias = Boolean.getBoolean("nb.cellrenderer.antialiasing") || "GTK".equals(UIManager.getLookAndFeel().getID()) && ColorUtil.gtkShouldAntialias() || Boolean.getBoolean("swing.aatext") || "Aqua".equals(UIManager.getLookAndFeel().getID());
    }
}

