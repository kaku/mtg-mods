/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.event.TabActionEvent;
import org.netbeans.swing.tabcontrol.plaf.AbstractViewTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.BusyTabsSupport;
import org.netbeans.swing.tabcontrol.plaf.ColorUtil;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.netbeans.swing.tabcontrol.plaf.TabLayoutModel;
import org.openide.awt.HtmlRenderer;

public final class WinXPViewTabDisplayerUI
extends AbstractViewTabDisplayerUI {
    private static final int TXT_X_PAD = 8;
    private static final int TXT_Y_PAD = 3;
    private static final int ICON_X_PAD = 2;
    private static final int BUMP_X_PAD = 3;
    private static final int BUMP_Y_PAD_UPPER = 5;
    private static final int BUMP_Y_PAD_BOTTOM = 3;
    private static final int HIGHLIGHTED_RAISE = 1;
    private static boolean colorsReady = false;
    private static Color unselFillBrightC;
    private static Color unselFillDarkC;
    private static Color selFillC;
    private static Color focusFillBrightC;
    private static Color focusFillDarkC;
    private static Color txtC;
    private static Color borderC;
    private static Color bottomBorderC;
    private static Color selBorderC;
    private static Color bgFillC;
    private static Map<Integer, String[]> buttonIconPaths;
    private Dimension prefSize = new Dimension(100, 17);
    private Rectangle tempRect = new Rectangle();

    private WinXPViewTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
    }

    public static ComponentUI createUI(JComponent c) {
        return new WinXPViewTabDisplayerUI((TabDisplayer)c);
    }

    @Override
    public void installUI(JComponent c) {
        super.installUI(c);
        WinXPViewTabDisplayerUI.initColors();
        WinXPViewTabDisplayerUI.initIcons();
        c.setOpaque(true);
    }

    @Override
    protected AbstractViewTabDisplayerUI.Controller createController() {
        return new OwnController();
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        FontMetrics fm = this.getTxtFontMetrics();
        int height = fm == null ? 17 : fm.getAscent() + 2 * fm.getDescent() + 3;
        Insets insets = c.getInsets();
        this.prefSize.height = height + insets.bottom + insets.top;
        return this.prefSize;
    }

    @Override
    protected void paintDisplayerBackground(Graphics g, JComponent c) {
        int tabCount = this.getDataModel().size();
        if (0 == tabCount) {
            return;
        }
        int x = this.getLayoutModel().getX(tabCount - 1);
        boolean y = false;
        int width = c.getWidth() - (x += this.getLayoutModel().getW(tabCount - 1) - 5);
        int height = c.getHeight();
        ColorUtil.paintXpTabHeader(0, g, x, 0, width);
        Color borderColor = borderC;
        g.setColor(borderColor);
        g.drawLine(x + width - 1, 3, x + width - 1, height - 2);
        g.setColor(bottomBorderC);
        g.drawLine(x, height - 1, x + width - 1, height - 1);
    }

    @Override
    protected void paintTabContent(Graphics g, int index, String text, int x, int y, int width, int height) {
        int highlightedRaiseCompensation;
        FontMetrics fm = this.getTxtFontMetrics();
        g.setFont(this.getTxtFont());
        if (!this.isTabInFront(index) && this.isMoreThanOne()) {
            ++y;
            --height;
        }
        int txtWidth = width;
        if (this.isSelected(index)) {
            Component buttons = this.getControlButtons();
            if (null != buttons) {
                Dimension buttonsSize = buttons.getPreferredSize();
                if (width < buttonsSize.width + 2) {
                    buttons.setVisible(false);
                } else {
                    buttons.setVisible(true);
                    txtWidth = width - (buttonsSize.width + 2 + 8);
                    buttons.setLocation(x + txtWidth + 8, y + (height - buttonsSize.height) / 2);
                }
            }
        } else {
            txtWidth = width - 16;
        }
        int n = highlightedRaiseCompensation = !this.isTabInFront(index) && this.isMoreThanOne() ? 1 : 0;
        if (this.isUseStretchingTabs()) {
            ColorUtil.paintXpTabDragTexture(this.getDisplayer(), g, x + 3, y + 5, height - 8 + highlightedRaiseCompensation);
        }
        boolean slidedOut = false;
        WinsysInfoForTabbedContainer winsysInfo = this.displayer.getContainerWinsysInfo();
        if (null != winsysInfo && winsysInfo.isSlidedOutContainer()) {
            slidedOut = false;
        }
        if (this.isTabBusy(index) && !slidedOut) {
            Icon busyIcon = BusyTabsSupport.getDefault().getBusyIcon(this.isSelected(index));
            txtWidth -= busyIcon.getIconWidth() - 3 - 8;
            busyIcon.paintIcon(this.displayer, g, x + 8, y + (height - busyIcon.getIconHeight()) / 2);
            x += busyIcon.getIconWidth() + 3;
        }
        HtmlRenderer.renderString((String)text, (Graphics)g, (int)(x + 8), (int)(y + fm.getAscent() + 3), (int)txtWidth, (int)height, (Font)this.getTxtFont(), (Color)txtC, (int)1, (boolean)true);
    }

    @Override
    protected void paintTabBorder(Graphics g, int index, int x, int y, int width, int height) {
        boolean isFirst = index == 0;
        boolean isHighlighted = this.isTabHighlighted(index);
        g.translate(x, y);
        ColorUtil.paintXpTabHeader(isHighlighted ? 1 : 0, g, 0, 0, width);
        Color borderColor = isHighlighted ? selBorderC : borderC;
        g.setColor(borderColor);
        if (isFirst) {
            g.drawLine(0, 3, 0, height - 2);
        }
        g.drawLine(width - 1, 3, width - 1, height - 2);
        g.setColor(bottomBorderC);
        g.drawLine(0, height - 1, width - 1, height - 1);
        g.translate(- x, - y);
    }

    @Override
    protected void paintTabBackground(Graphics g, int index, int x, int y, int width, int height) {
        y += 3;
        --width;
        height -= 4;
        boolean selected = this.isSelected(index);
        boolean focused = selected && this.isActive();
        boolean attention = this.isAttention(index);
        if (focused && !attention) {
            ColorUtil.xpFillRectGradient((Graphics2D)g, x, y, width, height, focusFillBrightC, focusFillDarkC);
        } else if (selected && this.isMoreThanOne() && !attention) {
            g.setColor(selFillC);
            g.fillRect(x, y, width, height);
        } else if (attention) {
            Color a = new Color(255, 255, 128);
            Color b = new Color(230, 200, 64);
            ColorUtil.xpFillRectGradient((Graphics2D)g, x, y, width, height, a, b);
        } else {
            ColorUtil.xpFillRectGradient((Graphics2D)g, x, y, width, height, unselFillBrightC, unselFillDarkC);
        }
    }

    @Override
    protected Font getTxtFont() {
        Font font = UIManager.getFont("Label.font");
        if (null == font) {
            font = super.getTxtFont();
        }
        if (!font.isBold()) {
            font = font.deriveFont(1);
        }
        return font;
    }

    private boolean isTabHighlighted(int index) {
        if (index < 0) {
            return false;
        }
        if (((OwnController)this.getController()).getMouseIndex() == index) {
            return true;
        }
        return this.isTabInFront(index) && this.isMoreThanOne();
    }

    private boolean isTabInFront(int index) {
        if (index < 0) {
            return false;
        }
        return this.isSelected(index) && (this.isActive() || this.isMoreThanOne());
    }

    private boolean isMoreThanOne() {
        return this.getDataModel().size() > 1;
    }

    private static void initColors() {
        if (!colorsReady) {
            txtC = UIManager.getColor("TabbedPane.foreground");
            selFillC = UIManager.getColor("TabbedPane.highlight");
            focusFillBrightC = UIManager.getColor("tab_focus_fill_bright");
            focusFillDarkC = UIManager.getColor("tab_focus_fill_dark");
            unselFillBrightC = UIManager.getColor("tab_unsel_fill_bright");
            unselFillDarkC = UIManager.getColor("tab_unsel_fill_dark");
            borderC = UIManager.getColor("tab_border");
            bottomBorderC = UIManager.getColor("tab_bottom_border");
            selBorderC = UIManager.getColor("tab_sel_border");
            bgFillC = UIManager.getColor("workplace_fill");
            colorsReady = true;
        }
    }

    private static void initIcons() {
        if (null == buttonIconPaths) {
            String[] iconPaths;
            buttonIconPaths = new HashMap<Integer, String[]>(7);
            iconPaths = new String[]{"org/openide/awt/resources/xp_bigclose_enabled.png", "org/openide/awt/resources/xp_bigclose_pressed.png", iconPaths[0], "org/openide/awt/resources/xp_bigclose_rollover.png"};
            buttonIconPaths.put(1, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/xp_slideright_enabled.png", "org/netbeans/swing/tabcontrol/resources/xp_slideright_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/xp_slideright_rollover.png"};
            buttonIconPaths.put(6, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/xp_slideleft_enabled.png", "org/netbeans/swing/tabcontrol/resources/xp_slideleft_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/xp_slideleft_rollover.png"};
            buttonIconPaths.put(5, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/xp_slidebottom_enabled.png", "org/netbeans/swing/tabcontrol/resources/xp_slidebottom_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/xp_slidebottom_rollover.png"};
            buttonIconPaths.put(7, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/xp_pin_enabled.png", "org/netbeans/swing/tabcontrol/resources/xp_pin_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/xp_pin_rollover.png"};
            buttonIconPaths.put(2, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/xp_restore_group_enabled.png", "org/netbeans/swing/tabcontrol/resources/xp_restore_group_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/xp_restore_group_rollover.png"};
            buttonIconPaths.put(11, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/xp_minimize_enabled.png", "org/netbeans/swing/tabcontrol/resources/xp_minimize_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/xp_minimize_rollover.png"};
            buttonIconPaths.put(12, iconPaths);
        }
    }

    @Override
    public Icon getButtonIcon(int buttonId, int buttonState) {
        Icon res = null;
        WinXPViewTabDisplayerUI.initIcons();
        String[] paths = buttonIconPaths.get(buttonId);
        if (null != paths && buttonState >= 0 && buttonState < paths.length) {
            res = TabControlButtonFactory.getIcon(paths[buttonState]);
        }
        return res;
    }

    @Override
    public void postTabAction(TabActionEvent e) {
        super.postTabAction(e);
        if ("maximize".equals(e.getActionCommand())) {
            ((OwnController)this.getController()).updateHighlight(-1);
        }
    }

    private class OwnController
    extends AbstractViewTabDisplayerUI.Controller {
        private int lastIndex;

        private OwnController() {
            this.lastIndex = -1;
        }

        public int getMouseIndex() {
            return this.lastIndex;
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            super.mouseMoved(e);
            Point pos = e.getPoint();
            if (!e.getSource().equals(WinXPViewTabDisplayerUI.this.displayer)) {
                pos = SwingUtilities.convertPoint((Component)e.getSource(), pos, WinXPViewTabDisplayerUI.this.displayer);
            }
            this.updateHighlight(WinXPViewTabDisplayerUI.this.getLayoutModel().indexOfPoint(pos.x, pos.y));
        }

        @Override
        public void mouseExited(MouseEvent e) {
            super.mouseExited(e);
            if (!this.inControlButtonsRect(e.getPoint())) {
                this.updateHighlight(-1);
            }
        }

        private void updateHighlight(int curIndex) {
            int y;
            int h;
            int w;
            int x;
            if (curIndex == this.lastIndex) {
                return;
            }
            TabLayoutModel tlm = WinXPViewTabDisplayerUI.this.getLayoutModel();
            Rectangle repaintRect = null;
            if (curIndex != -1) {
                x = tlm.getX(curIndex);
                y = tlm.getY(curIndex);
                w = tlm.getW(curIndex);
                h = tlm.getH(curIndex);
                repaintRect = new Rectangle(x, y, w, h);
            }
            if (this.lastIndex != -1 && this.lastIndex < WinXPViewTabDisplayerUI.this.getDataModel().size()) {
                x = tlm.getX(this.lastIndex);
                y = tlm.getY(this.lastIndex);
                w = tlm.getW(this.lastIndex);
                h = tlm.getH(this.lastIndex);
                repaintRect = repaintRect != null ? repaintRect.union(new Rectangle(x, y, w, h)) : new Rectangle(x, y, w, h);
            }
            if (repaintRect != null) {
                WinXPViewTabDisplayerUI.this.getDisplayer().repaint(repaintRect);
            }
            this.lastIndex = curIndex;
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            super.mouseEntered(e);
            this.mouseMoved(e);
        }
    }

}

