/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.SingleSelectionModel;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.plaf.BasicScrollingTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.ColorUtil;
import org.netbeans.swing.tabcontrol.plaf.ScrollingTabLayoutModel;
import org.netbeans.swing.tabcontrol.plaf.TabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.netbeans.swing.tabcontrol.plaf.TabState;
import org.netbeans.swing.tabcontrol.plaf.WinClassicEditorTabCellRenderer;

public final class WinClassicEditorTabDisplayerUI
extends BasicScrollingTabDisplayerUI {
    private static final Rectangle scratch5 = new Rectangle();
    private static Map<Integer, String[]> buttonIconPaths;
    private static boolean isGenericUI;

    public WinClassicEditorTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
    }

    public static ComponentUI createUI(JComponent c) {
        return new WinClassicEditorTabDisplayerUI((TabDisplayer)c);
    }

    @Override
    public Rectangle getTabRect(int idx, Rectangle rect) {
        Rectangle r = super.getTabRect(idx, rect);
        r.y = 0;
        r.height = this.displayer.getHeight();
        return r;
    }

    @Override
    public void install() {
        super.install();
        if (!isGenericUI) {
            this.displayer.setBackground(UIManager.getColor("tab_unsel_fill"));
            this.displayer.setOpaque(true);
        }
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        int prefHeight = 28;
        Graphics2D g = BasicScrollingTabDisplayerUI.getOffscreenGraphics();
        if (g != null) {
            FontMetrics fm = g.getFontMetrics(this.displayer.getFont());
            Insets ins = this.getTabAreaInsets();
            prefHeight = fm.getHeight() + ins.top + ins.bottom + (isGenericUI ? 5 : 6);
        }
        return new Dimension(this.displayer.getWidth(), prefHeight);
    }

    private void genericPaintAfterTabs(Graphics g) {
        g.setColor(UIManager.getColor("controlShadow"));
        Insets ins = this.displayer.getInsets();
        Rectangle r = new Rectangle();
        this.getTabsVisibleArea(r);
        r.width = this.displayer.getWidth();
        boolean selEnd = false;
        int last = this.getLastVisibleTab();
        if (last > -1) {
            this.getTabRect(last, scratch5);
            g.drawLine(WinClassicEditorTabDisplayerUI.scratch5.x + WinClassicEditorTabDisplayerUI.scratch5.width, this.displayer.getHeight() - 1, this.displayer.getWidth() - (ins.left + ins.right) - 4, this.displayer.getHeight() - 1);
            g.drawLine(0, this.displayer.getHeight() - 2, 2, this.displayer.getHeight() - 2);
            if ("GTK".equals(UIManager.getLookAndFeel().getID())) {
                boolean sel = last == this.displayer.getSelectionModel().getSelectedIndex();
                int x = WinClassicEditorTabDisplayerUI.scratch5.x + WinClassicEditorTabDisplayerUI.scratch5.width;
                g.setColor(sel ? UIManager.getColor("controlShadow") : ColorUtil.adjustTowards(g.getColor(), 20, UIManager.getColor("control")));
                g.drawLine(x, WinClassicEditorTabDisplayerUI.scratch5.y + 5, x, WinClassicEditorTabDisplayerUI.scratch5.y + WinClassicEditorTabDisplayerUI.scratch5.height - 2);
                g.setColor(ColorUtil.adjustTowards(g.getColor(), 20, UIManager.getColor("control")));
                g.drawLine(x + 1, WinClassicEditorTabDisplayerUI.scratch5.y + 6, x + 1, WinClassicEditorTabDisplayerUI.scratch5.y + WinClassicEditorTabDisplayerUI.scratch5.height - 2);
            }
            if ((this.tabState.getState(this.getFirstVisibleTab()) & 2) != 0 && this.getFirstVisibleTab() != this.displayer.getSelectionModel().getSelectedIndex()) {
                GradientPaint gp = ColorUtil.getGradientPaint(0.0f, this.displayer.getHeight() / 2, UIManager.getColor("control"), 0.0f, this.displayer.getHeight(), UIManager.getColor("controlShadow"));
                ((Graphics2D)g).setPaint(gp);
                g.drawLine(0, this.displayer.getHeight() / 2, 0, this.displayer.getHeight());
            } else {
                g.setColor(UIManager.getColor("controlShadow"));
                g.drawLine(0, this.displayer.getHeight(), 0, this.displayer.getHeight() - 2);
            }
            if ((this.tabState.getState(this.getLastVisibleTab()) & 1) != 0 && this.getLastVisibleTab() != this.displayer.getSelectionModel().getSelectedIndex()) {
                GradientPaint gp = ColorUtil.getGradientPaint(0.0f, this.displayer.getHeight() / 2, UIManager.getColor("control"), 0.0f, this.displayer.getHeight(), UIManager.getColor("controlShadow"));
                ((Graphics2D)g).setPaint(gp);
                this.getTabRect(this.getLastVisibleTab(), scratch5);
                g.drawLine(WinClassicEditorTabDisplayerUI.scratch5.x + WinClassicEditorTabDisplayerUI.scratch5.width, this.displayer.getHeight() / 2, WinClassicEditorTabDisplayerUI.scratch5.x + WinClassicEditorTabDisplayerUI.scratch5.width, this.displayer.getHeight());
            }
        } else {
            g.drawLine(r.x, this.displayer.getHeight() - ins.bottom, r.x + r.width - 4, this.displayer.getHeight() - ins.bottom);
        }
    }

    @Override
    protected void paintAfterTabs(Graphics g) {
        if (isGenericUI) {
            this.genericPaintAfterTabs(g);
            return;
        }
        Rectangle r = new Rectangle();
        this.getTabsVisibleArea(r);
        r.width = this.displayer.getWidth();
        g.setColor(this.displayer.isActive() ? this.defaultRenderer.getSelectedActivatedBackground() : this.defaultRenderer.getSelectedBackground());
        Insets ins = this.getTabAreaInsets();
        ++ins.bottom;
        g.fillRect(r.x, r.y + r.height, r.x + r.width, this.displayer.getHeight() - (r.y + r.height));
        g.setColor(UIManager.getColor("controlLtHighlight"));
        int selEnd = 0;
        int i = this.selectionModel.getSelectedIndex();
        if (i != -1) {
            this.getTabRect(i, scratch5);
            if (WinClassicEditorTabDisplayerUI.scratch5.width != 0) {
                if (r.x < WinClassicEditorTabDisplayerUI.scratch5.x) {
                    g.drawLine(r.x, this.displayer.getHeight() - ins.bottom, WinClassicEditorTabDisplayerUI.scratch5.x - 1, this.displayer.getHeight() - ins.bottom);
                }
                if (WinClassicEditorTabDisplayerUI.scratch5.x + WinClassicEditorTabDisplayerUI.scratch5.width < r.x + r.width) {
                    selEnd = WinClassicEditorTabDisplayerUI.scratch5.x + WinClassicEditorTabDisplayerUI.scratch5.width;
                    if (!this.scroll().isLastTabClipped()) {
                        --selEnd;
                    }
                    g.drawLine(selEnd, this.displayer.getHeight() - ins.bottom, r.x + r.width, this.displayer.getHeight() - ins.bottom);
                }
            }
            return;
        }
        g.drawLine(r.x, this.displayer.getHeight() - ins.bottom, r.x + r.width, this.displayer.getHeight() - ins.bottom);
    }

    @Override
    protected TabCellRenderer createDefaultRenderer() {
        return new WinClassicEditorTabCellRenderer();
    }

    private static void initIcons() {
        if (null == buttonIconPaths) {
            buttonIconPaths = new HashMap<Integer, String[]>(7);
            String[] iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/win_scrollleft_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/win_scrollleft_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/win_scrollleft_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/win_scrollleft_pressed.png";
            buttonIconPaths.put(9, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/win_scrollright_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/win_scrollright_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/win_scrollright_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/win_scrollright_pressed.png";
            buttonIconPaths.put(10, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/win_popup_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/win_popup_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/win_popup_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/win_popup_pressed.png";
            buttonIconPaths.put(8, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/win_maximize_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/win_maximize_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/win_maximize_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/win_maximize_pressed.png";
            buttonIconPaths.put(3, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/win_restore_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/win_restore_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/win_restore_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/win_restore_pressed.png";
            buttonIconPaths.put(4, iconPaths);
        }
    }

    @Override
    public Icon getButtonIcon(int buttonId, int buttonState) {
        Icon res = null;
        WinClassicEditorTabDisplayerUI.initIcons();
        String[] paths = buttonIconPaths.get(buttonId);
        if (null != paths && buttonState >= 0 && buttonState < paths.length) {
            res = TabControlButtonFactory.getIcon(paths[buttonState]);
        }
        return res;
    }

    @Override
    protected Rectangle getControlButtonsRectangle(Container parent) {
        Component c = this.getControlButtons();
        return new Rectangle(parent.getWidth() - c.getWidth() - 4, 4, c.getWidth(), c.getHeight());
    }

    @Override
    public Insets getTabAreaInsets() {
        Insets retValue = super.getTabAreaInsets();
        retValue.right += 4;
        return retValue;
    }

    static {
        isGenericUI = !"Windows".equals(UIManager.getLookAndFeel().getID());
    }
}

