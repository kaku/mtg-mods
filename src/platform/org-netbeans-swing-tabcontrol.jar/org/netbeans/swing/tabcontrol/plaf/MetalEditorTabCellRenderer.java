/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Polygon;
import java.awt.Rectangle;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.swing.tabcontrol.plaf.AbstractTabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.BusyTabsSupport;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.netbeans.swing.tabcontrol.plaf.TabPainter;

class MetalEditorTabCellRenderer
extends AbstractTabCellRenderer {
    private static final MetalTabPainter metalborder = new MetalTabPainter();
    private static final MetalRightClippedTabPainter rightBorder = new MetalRightClippedTabPainter();
    private static final MetalLeftClippedTabPainter leftBorder = new MetalLeftClippedTabPainter();
    static final Color ATTENTION_COLOR = new Color(255, 238, 120);

    public MetalEditorTabCellRenderer() {
        super(leftBorder, metalborder, rightBorder, new Dimension(34, 29));
        this.setBorder(metalborder);
    }

    @Override
    protected int getCaptionYAdjustment() {
        return 0;
    }

    @Override
    public Dimension getPadding() {
        Dimension d = super.getPadding();
        d.width = this.isShowCloseButton() && !Boolean.getBoolean("nb.tabs.suppressCloseButton") ? 34 : 24;
        return d;
    }

    @Override
    protected void paintIconAndText(Graphics g) {
        if (this.isBusy()) {
            this.setIcon(BusyTabsSupport.getDefault().getBusyIcon(this.isSelected()));
        }
        super.paintIconAndText(g);
    }

    private static class MetalRightClippedTabPainter
    implements TabPainter {
        private MetalRightClippedTabPainter() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            MetalEditorTabCellRenderer mtr = (MetalEditorTabCellRenderer)c;
            return new Insets(mtr.isSelected() ? 3 : 5, mtr.isSelected() ? 10 : 9, 1, 0);
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        @Override
        public Polygon getInteriorPolygon(Component c) {
            MetalEditorTabCellRenderer mtr = (MetalEditorTabCellRenderer)c;
            Insets ins = this.getBorderInsets(c);
            Polygon p = new Polygon();
            int x = mtr.isLeftmost() ? 1 : 0;
            int y = 0;
            int width = c.getWidth() + 2;
            int height = mtr.isSelected() ? c.getHeight() + 3 : c.getHeight();
            p.addPoint(x, y + ins.top + 6);
            p.addPoint(x + 6, y + ins.top);
            p.addPoint(x + width, y + ins.top);
            p.addPoint(x + width, y + height);
            p.addPoint(x, y + height);
            return p;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            MetalEditorTabCellRenderer mtr = (MetalEditorTabCellRenderer)c;
            Polygon p = this.getInteriorPolygon(c);
            g.setColor(UIManager.getColor("controlHighlight"));
            g.drawPolygon(p);
            p.translate(-1, 0);
            Insets ins = this.getBorderInsets(c);
            g.drawLine(x + 6, y + ins.top + 1, x + width - 1, y + ins.top + 1);
            if (mtr.isSelected()) {
                g.drawLine(4, ins.top + 6, 4, ins.top + 6);
                g.drawLine(2, ins.top + 8, 2, ins.top + 8);
                g.drawLine(4, ins.top + 10, 4, ins.top + 10);
                g.drawLine(2, ins.top + 12, 2, ins.top + 12);
                g.drawLine(4, ins.top + 14, 4, ins.top + 14);
                g.drawLine(2, ins.top + 16, 2, ins.top + 16);
            }
            g.setColor(UIManager.getColor("controlDkShadow"));
            g.drawPolygon(p);
            if (mtr.isSelected()) {
                g.drawLine(5, ins.top + 7, 5, ins.top + 7);
                g.drawLine(3, ins.top + 9, 3, ins.top + 9);
                g.drawLine(5, ins.top + 11, 5, ins.top + 11);
                g.drawLine(3, ins.top + 13, 3, ins.top + 13);
                g.drawLine(5, ins.top + 15, 5, ins.top + 15);
                g.drawLine(3, ins.top + 17, 3, ins.top + 17);
            }
            if (!mtr.isSelected()) {
                g.setColor(UIManager.getColor("controlDkShadow"));
                g.drawLine(x, c.getHeight() - 1, c.getWidth() - 1, c.getHeight() - 1);
            }
        }

        @Override
        public void paintInterior(Graphics g, Component c) {
            Polygon p = this.getInteriorPolygon(c);
            MetalEditorTabCellRenderer mtr = (MetalEditorTabCellRenderer)c;
            if (mtr.isAttention()) {
                g.setColor(MetalEditorTabCellRenderer.ATTENTION_COLOR);
            }
            g.fillPolygon(p);
        }

        @Override
        public void getCloseButtonRectangle(JComponent jc, Rectangle rect, Rectangle bounds) {
            bounds.setBounds(-20, -20, 0, 0);
        }

        @Override
        public boolean supportsCloseButton(JComponent renderer) {
            return false;
        }
    }

    private static class MetalLeftClippedTabPainter
    implements TabPainter {
        private MetalLeftClippedTabPainter() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            MetalEditorTabCellRenderer mtr = (MetalEditorTabCellRenderer)c;
            return new Insets(mtr.isSelected() ? 3 : 5, mtr.isSelected() ? 10 : 9, 1, 0);
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        @Override
        public Polygon getInteriorPolygon(Component c) {
            MetalEditorTabCellRenderer mtr = (MetalEditorTabCellRenderer)c;
            Insets ins = this.getBorderInsets(c);
            Polygon p = new Polygon();
            int x = -1;
            int y = ins.top;
            int width = c.getWidth();
            int height = mtr.isSelected() ? c.getHeight() + 3 : c.getHeight();
            p.addPoint(x, y);
            p.addPoint(x + width, y);
            p.addPoint(x + width, y + height);
            p.addPoint(x, y + height);
            return p;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            MetalEditorTabCellRenderer mtr = (MetalEditorTabCellRenderer)c;
            g.setColor(UIManager.getColor("controlHighlight"));
            Polygon p = this.getInteriorPolygon(c);
            p.translate(0, 1);
            g.drawPolygon(p);
            p.translate(0, -1);
            g.setColor(UIManager.getColor("controlDkShadow"));
            g.drawPolygon(p);
            if (!mtr.isSelected()) {
                g.drawLine(x, y + height - 1, x + width, y + height - 1);
            }
        }

        @Override
        public void paintInterior(Graphics g, Component c) {
            Polygon p = this.getInteriorPolygon(c);
            MetalEditorTabCellRenderer mtr = (MetalEditorTabCellRenderer)c;
            if (mtr.isAttention()) {
                g.setColor(MetalEditorTabCellRenderer.ATTENTION_COLOR);
            }
            g.fillPolygon(p);
        }

        @Override
        public void getCloseButtonRectangle(JComponent jc, Rectangle rect, Rectangle bounds) {
            bounds.setBounds(-20, -20, 0, 0);
        }

        @Override
        public boolean supportsCloseButton(JComponent renderer) {
            return false;
        }
    }

    private static class MetalTabPainter
    implements TabPainter {
        private MetalTabPainter() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            MetalEditorTabCellRenderer mtr = (MetalEditorTabCellRenderer)c;
            return new Insets(mtr.isSelected() ? 3 : 5, mtr.isSelected() ? 10 : 9, 1, 0);
        }

        @Override
        public boolean supportsCloseButton(JComponent renderer) {
            return ((AbstractTabCellRenderer)renderer).isShowCloseButton();
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            MetalEditorTabCellRenderer mtr = (MetalEditorTabCellRenderer)c;
            Polygon p = this.getInteriorPolygon(c);
            g.setColor(UIManager.getColor("controlHighlight"));
            g.drawPolygon(p);
            p.translate(-1, 0);
            Insets ins = this.getBorderInsets(c);
            g.drawLine(x + 6, y + ins.top + 1, x + width - 1, y + ins.top + 1);
            if (mtr.isSelected()) {
                g.drawLine(4, ins.top + 6, 4, ins.top + 6);
                g.drawLine(2, ins.top + 8, 2, ins.top + 8);
                g.drawLine(4, ins.top + 10, 4, ins.top + 10);
                g.drawLine(2, ins.top + 12, 2, ins.top + 12);
                g.drawLine(4, ins.top + 14, 4, ins.top + 14);
                g.drawLine(2, ins.top + 16, 2, ins.top + 16);
            }
            g.setColor(UIManager.getColor("controlDkShadow"));
            g.drawPolygon(p);
            if (mtr.isSelected()) {
                g.drawLine(5, ins.top + 7, 5, ins.top + 7);
                g.drawLine(3, ins.top + 9, 3, ins.top + 9);
                g.drawLine(5, ins.top + 11, 5, ins.top + 11);
                g.drawLine(3, ins.top + 13, 3, ins.top + 13);
                g.drawLine(5, ins.top + 15, 5, ins.top + 15);
                g.drawLine(3, ins.top + 17, 3, ins.top + 17);
            }
            if (!mtr.isSelected()) {
                g.setColor(UIManager.getColor("controlDkShadow"));
                g.drawLine(x, mtr.getHeight() - 1, mtr.getWidth() - 1, mtr.getHeight() - 1);
            }
        }

        @Override
        public Polygon getInteriorPolygon(Component c) {
            MetalEditorTabCellRenderer mtr = (MetalEditorTabCellRenderer)c;
            Insets ins = this.getBorderInsets(c);
            Polygon p = new Polygon();
            int x = mtr.isLeftmost() ? 1 : 0;
            int y = 0;
            int width = mtr.isLeftmost() ? c.getWidth() - 1 : c.getWidth();
            int height = mtr.isSelected() ? c.getHeight() + 3 : c.getHeight();
            p.addPoint(x, y + ins.top + 6);
            p.addPoint(x + 6, y + ins.top);
            p.addPoint(x + width, y + ins.top);
            p.addPoint(x + width, y + height);
            p.addPoint(x, y + height);
            return p;
        }

        @Override
        public void paintInterior(Graphics g, Component c) {
            MetalEditorTabCellRenderer mtr = (MetalEditorTabCellRenderer)c;
            if (mtr.isAttention()) {
                g.setColor(MetalEditorTabCellRenderer.ATTENTION_COLOR);
            }
            Polygon p = this.getInteriorPolygon(c);
            g.fillPolygon(p);
            Rectangle r = new Rectangle();
            this.getCloseButtonRectangle(mtr, r, new Rectangle(0, 0, mtr.getWidth(), mtr.getHeight()));
            if (!g.hitClip(r.x, r.y, r.width, r.height)) {
                return;
            }
            this.paintCloseButton(g, (JComponent)c);
        }

        @Override
        public void getCloseButtonRectangle(JComponent jc, Rectangle rect, Rectangle bounds) {
            if (!((AbstractTabCellRenderer)jc).isShowCloseButton()) {
                rect.x = -100;
                rect.y = -100;
                rect.width = 0;
                rect.height = 0;
                return;
            }
            String iconPath = this.findIconPath((MetalEditorTabCellRenderer)jc);
            Icon icon = TabControlButtonFactory.getIcon(iconPath);
            int iconWidth = icon.getIconWidth();
            int iconHeight = icon.getIconHeight();
            rect.x = bounds.x + bounds.width - iconWidth - 2;
            rect.y = bounds.y + Math.max(0, bounds.height / 2 - iconHeight / 2) + 2;
            rect.width = iconWidth;
            rect.height = iconHeight;
        }

        private void paintCloseButton(Graphics g, JComponent c) {
            if (((AbstractTabCellRenderer)c).isShowCloseButton()) {
                Rectangle r = new Rectangle(0, 0, c.getWidth(), c.getHeight());
                Rectangle cbRect = new Rectangle();
                this.getCloseButtonRectangle(c, cbRect, r);
                String iconPath = this.findIconPath((MetalEditorTabCellRenderer)c);
                Icon icon = TabControlButtonFactory.getIcon(iconPath);
                icon.paintIcon(c, g, cbRect.x, cbRect.y);
            }
        }

        private String findIconPath(MetalEditorTabCellRenderer renderer) {
            if (renderer.inCloseButton() && renderer.isPressed()) {
                return "org/openide/awt/resources/metal_close_pressed.png";
            }
            if (renderer.inCloseButton()) {
                return "org/openide/awt/resources/metal_close_rollover.png";
            }
            return "org/openide/awt/resources/metal_close_enabled.png";
        }
    }

}

