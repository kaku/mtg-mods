/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.SingleSelectionModel;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.plaf.BasicScrollingTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.MetalEditorTabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.TabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;

public final class MetalEditorTabDisplayerUI
extends BasicScrollingTabDisplayerUI {
    private Rectangle scratch = new Rectangle();
    private static Map<Integer, String[]> buttonIconPaths;

    public MetalEditorTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
    }

    @Override
    protected TabCellRenderer createDefaultRenderer() {
        return new MetalEditorTabCellRenderer();
    }

    public static ComponentUI createUI(JComponent c) {
        return new MetalEditorTabDisplayerUI((TabDisplayer)c);
    }

    @Override
    public Dimension getMinimumSize(JComponent c) {
        return new Dimension(80, 28);
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        int prefHeight = 28;
        Graphics2D g = BasicScrollingTabDisplayerUI.getOffscreenGraphics();
        if (g != null) {
            FontMetrics fm = g.getFontMetrics(this.displayer.getFont());
            Insets ins = this.getTabAreaInsets();
            prefHeight = fm.getHeight() + ins.top + ins.bottom + 9;
        }
        return new Dimension(this.displayer.getWidth(), prefHeight);
    }

    @Override
    protected int createRepaintPolicy() {
        return 428;
    }

    @Override
    public Insets getTabAreaInsets() {
        Insets results = super.getTabAreaInsets();
        results.bottom += 4;
        results.right += 3;
        return results;
    }

    @Override
    public void install() {
        super.install();
        this.displayer.setBackground(UIManager.getColor("control"));
    }

    @Override
    protected void paintAfterTabs(Graphics g) {
        Rectangle r = new Rectangle();
        this.getTabsVisibleArea(r);
        r.width = this.displayer.getWidth();
        g.setColor(this.displayer.isActive() ? this.defaultRenderer.getSelectedActivatedBackground() : this.defaultRenderer.getSelectedBackground());
        Insets ins = this.getTabAreaInsets();
        g.fillRect(r.x, r.y + r.height, r.x + r.width, this.displayer.getHeight() - (r.y + r.height));
        g.setColor(UIManager.getColor("controlHighlight"));
        int selEnd = 0;
        int i = this.selectionModel.getSelectedIndex();
        if (i != -1) {
            this.getTabRect(i, this.scratch);
            if (this.scratch.width != 0) {
                if (r.x < this.scratch.x) {
                    g.drawLine(r.x, this.displayer.getHeight() - ins.bottom, this.scratch.x - 1, this.displayer.getHeight() - ins.bottom);
                }
                if (this.scratch.x + this.scratch.width < r.x + r.width) {
                    selEnd = this.scratch.x + this.scratch.width;
                    g.drawLine(selEnd, this.displayer.getHeight() - ins.bottom, r.x + r.width, this.displayer.getHeight() - ins.bottom);
                }
            } else {
                g.drawLine(0, this.displayer.getHeight() - ins.bottom, this.displayer.getWidth(), this.displayer.getHeight() - ins.bottom);
            }
            g.setColor(UIManager.getColor("controlDkShadow"));
            g.drawLine(selEnd, this.displayer.getHeight() - 5, this.displayer.getWidth(), this.displayer.getHeight() - 5);
            return;
        }
        g.drawLine(r.x, this.displayer.getHeight() - ins.bottom, r.x + r.width, this.displayer.getHeight() - ins.bottom);
        g.setColor(UIManager.getColor("controlDkShadow"));
        g.drawLine(0, this.displayer.getHeight() - 5, this.displayer.getWidth(), this.displayer.getHeight() - 5);
    }

    private static void initIcons() {
        if (null == buttonIconPaths) {
            buttonIconPaths = new HashMap<Integer, String[]>(7);
            String[] iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/metal_scrollleft_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/metal_scrollleft_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/metal_scrollleft_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/metal_scrollleft_pressed.png";
            buttonIconPaths.put(9, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/metal_scrollright_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/metal_scrollright_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/metal_scrollright_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/metal_scrollright_pressed.png";
            buttonIconPaths.put(10, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/metal_popup_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/metal_popup_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/metal_popup_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/metal_popup_pressed.png";
            buttonIconPaths.put(8, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/metal_maximize_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/metal_maximize_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/metal_maximize_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/metal_maximize_pressed.png";
            buttonIconPaths.put(3, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/metal_restore_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/metal_restore_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/metal_restore_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/metal_restore_pressed.png";
            buttonIconPaths.put(4, iconPaths);
        }
    }

    @Override
    public Icon getButtonIcon(int buttonId, int buttonState) {
        Icon res = null;
        MetalEditorTabDisplayerUI.initIcons();
        String[] paths = buttonIconPaths.get(buttonId);
        if (null != paths && buttonState >= 0 && buttonState < paths.length) {
            res = TabControlButtonFactory.getIcon(paths[buttonState]);
        }
        return res;
    }

    @Override
    protected Rectangle getControlButtonsRectangle(Container parent) {
        Component c = this.getControlButtons();
        return new Rectangle(parent.getWidth() - c.getWidth() - 3, 3, c.getWidth(), c.getHeight());
    }
}

