/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Area;
import java.awt.geom.GeneralPath;
import org.netbeans.swing.tabcontrol.plaf.ColorUtil;

class GenericGlowingChiclet {
    public static final int STATE_PRESSED = 1;
    public static final int STATE_SELECTED = 2;
    public static final int STATE_ACTIVE = 4;
    public static final int STATE_CLOSING = 8;
    public static final int STATE_ATTENTION = 16;
    static Color[] selectedActive = new Color[]{new Color(220, 238, 255), new Color(139, 187, 238), new Color(90, 143, 229), new Color(190, 247, 255)};
    static Color[] selectedPressedActive = new Color[]{selectedActive[0], new Color(50, 150, 229), new Color(80, 80, 200), selectedActive[3]};
    static Color[] inactive = new Color[]{Color.WHITE, new Color(222, 222, 227), new Color(205, 205, 205), new Color(246, 243, 249)};
    static Color[] active = new Color[]{Color.WHITE, new Color(222, 222, 227), new Color(205, 205, 205), new Color(246, 243, 249)};
    static Color[] selectedInactive = new Color[]{new Color(240, 250, 255), new Color(212, 222, 242), new Color(180, 190, 200), new Color(230, 230, 255)};
    static Color[] closing = new Color[]{new Color(255, 238, 220), new Color(238, 137, 109), new Color(255, 50, 50), new Color(255, 237, 40)};
    static Color[] attention = new Color[]{new Color(255, 255, 220), new Color(238, 237, 109), new Color(255, 255, 50), new Color(255, 237, 40)};
    private Color upperTop = selectedActive[0];
    private Color upperBottom = selectedActive[1];
    private Color lowerTop = selectedActive[2];
    private Color lowerBottom = selectedActive[3];
    private Rectangle scratch = new Rectangle();
    private float fupperLeftArc = 0.0f;
    private float fupperRightArc = 0.0f;
    private float flowerLeftArc = 0.0f;
    private float flowerRightArc = 0.0f;
    private int upperLeftArc = 0;
    private int upperRightArc = 0;
    private int lowerLeftArc = 0;
    private int lowerRightArc = 0;
    private boolean usePercentages = false;
    private boolean notchRight = false;
    private boolean notchLeft = false;
    protected boolean changed = false;
    protected int state = 6;
    public static final GenericGlowingChiclet INSTANCE = new GenericGlowingChiclet();
    private Rectangle bounds = new Rectangle();
    private boolean allowVertical = false;
    private boolean drawOutline = true;
    private Shape clip = null;

    protected GenericGlowingChiclet() {
    }

    public void setColors(Color upperTop, Color upperBottom, Color lowerTop, Color lowerBottom) {
        this.changed |= !upperTop.equals(this.getUpperTop()) || !upperBottom.equals(this.getUpperBottom()) || !lowerTop.equals(this.getLowerTop()) || !lowerBottom.equals(this.getLowerBottom());
        this.upperTop = upperTop;
        this.upperBottom = upperBottom;
        this.lowerTop = lowerTop;
        this.lowerBottom = lowerBottom;
    }

    public Color[] getColors() {
        return new Color[]{this.getUpperTop(), this.getUpperBottom(), this.getLowerTop(), this.getLowerBottom()};
    }

    protected int getState() {
        return this.state;
    }

    public void setState(int i) {
        this.changed |= this.state != i;
        if (this.state != i) {
            Color[] nue;
            if ((this.state & 1) != 0) {
                this.state |= 4;
            }
            this.state = i;
            if ((this.state & 8) != 0) {
                nue = closing;
            } else if ((this.state & 16) != 0) {
                nue = attention;
            } else {
                switch (this.state) {
                    case 5: 
                    case 7: {
                        nue = selectedPressedActive;
                        break;
                    }
                    case 6: {
                        nue = selectedActive;
                        break;
                    }
                    case 2: {
                        nue = selectedInactive;
                        break;
                    }
                    case 4: {
                        nue = active;
                        break;
                    }
                    default: {
                        nue = inactive;
                    }
                }
            }
            this.upperTop = nue[0];
            this.upperBottom = nue[1];
            this.lowerTop = nue[2];
            this.lowerBottom = nue[3];
        }
    }

    public void setBounds(int x, int y, int w, int h) {
        this.changed |= x != this.bounds.x || y != this.bounds.y || w != this.bounds.width || h != this.bounds.height;
        this.bounds.setBounds(x, y, w, h - 1);
    }

    public void setAllowVertical(boolean val) {
        if (val != this.allowVertical) {
            this.allowVertical = val;
            this.changed = true;
        }
    }

    private Rectangle getBounds() {
        this.scratch.setBounds(this.bounds);
        return this.scratch;
    }

    public void setArcs(int upperLeft, int upperRight, int lowerLeft, int lowerRight) {
        this.changed |= upperLeft != this.upperLeftArc || upperRight != this.upperRightArc || lowerLeft != this.lowerLeftArc || lowerRight != this.lowerRightArc || this.usePercentages;
        this.upperLeftArc = upperLeft;
        this.upperRightArc = upperRight;
        this.lowerLeftArc = lowerLeft;
        this.lowerRightArc = lowerRight;
        this.usePercentages = false;
    }

    public void setArcs(float upperLeft, float upperRight, float lowerLeft, float lowerRight) {
        this.changed |= upperLeft != this.fupperLeftArc || upperRight != this.fupperRightArc || lowerLeft != this.flowerLeftArc || lowerRight != this.flowerRightArc || !this.usePercentages;
        this.fupperLeftArc = upperLeft;
        this.fupperRightArc = upperRight;
        this.flowerLeftArc = lowerLeft;
        this.flowerRightArc = lowerRight;
        this.usePercentages = true;
    }

    public void setNotch(boolean right, boolean left) {
        this.changed |= right != this.notchRight || left != this.notchLeft;
        this.notchRight = right;
        this.notchLeft = left;
    }

    private int getNotchRightArc() {
        int arc = this.getUpperRightArc();
        if (arc == 0) {
            arc = this.bounds.height / 2;
        }
        return arc / 3;
    }

    private int getNotchLeftArc() {
        int arc = this.getUpperLeftArc();
        if (arc == 0) {
            arc = this.bounds.height / 2;
        }
        return arc / 3;
    }

    private int getUpperLeftArc() {
        if (!this.usePercentages) {
            return this.upperLeftArc;
        }
        return Math.round(this.fupperLeftArc * (float)Math.min(this.getBounds().height, this.getBounds().width));
    }

    private int getUpperRightArc() {
        if (!this.usePercentages) {
            return this.upperRightArc;
        }
        return Math.round(this.fupperRightArc * (float)Math.min(this.getBounds().height, this.getBounds().width));
    }

    private int getLowerLeftArc() {
        if (!this.usePercentages) {
            return this.lowerLeftArc;
        }
        return Math.round(this.flowerLeftArc * (float)Math.min(this.getBounds().height, this.getBounds().width));
    }

    private int getLowerRightArc() {
        if (!this.usePercentages) {
            return this.lowerRightArc;
        }
        return Math.round(this.flowerRightArc * (float)Math.min(this.getBounds().height, this.getBounds().width));
    }

    public void draw(Graphics2D g) {
        if (this.bounds.width == 0 || this.bounds.height == 0) {
            return;
        }
        this.drawInterior(g);
        if (this.drawOutline) {
            this.drawOutline(g);
        }
        this.changed = false;
    }

    public void setDrawOutline(boolean b) {
        this.drawOutline = b;
    }

    private void drawOutline(Graphics2D g) {
        Shape s = this.getClipShape();
        g.setColor(this.dark());
        g.setStroke(new BasicStroke(0.95f));
        Rectangle r = this.getBounds();
        ++r.height;
        Shape clip = g.getClip();
        if (clip != null) {
            Area a = new Area(clip);
            a.intersect(new Area(r));
            g.setClip(a);
        } else {
            g.setClip(r);
        }
        g.draw(s);
        g.setColor(this.getOutlineDark());
        r = this.getBounds();
        g.setStroke(new BasicStroke(0.7f));
        if (this.getLowerRightArc() != 0) {
            g.drawLine(Math.max(r.x, r.x + this.getLowerLeftArc() - 3), r.y + r.height - 1, Math.min(r.x + r.width - this.getLowerRightArc() + 3, r.x + r.width) - 1, r.y + r.height - 1);
        } else {
            g.drawLine(Math.max(r.x, r.x + this.getLowerLeftArc() - 3), r.y + r.height - 1, Math.min(r.x + r.width - this.getLowerRightArc() + 3, r.x + r.width), r.y + r.height - 1);
        }
        g.setClip(clip);
    }

    protected Color getOutlineDark() {
        return new Color(50, 50, 50);
    }

    private void drawInterior(Graphics2D g) {
        Rectangle r;
        Shape s = this.getClipShape();
        Area a = new Area(s);
        Shape clip = g.getClip();
        if (clip != null) {
            a.intersect(new Area(clip));
        }
        if (this.isVertical()) {
            r = this.getBounds();
            r.width /= 2;
            a.intersect(new Area(r));
            g.setClip(a);
            g.setPaint(this.getLeftPaint());
            g.fill(s);
            r = this.getBounds();
            r.width /= 2;
            r.x += r.width;
            a = new Area(s);
            if (clip != null) {
                a.intersect(new Area(clip));
            }
            a.intersect(new Area(r));
            g.setClip(a);
            g.setPaint(this.getRightPaint());
            g.fill(s);
        } else {
            r = this.getBounds();
            r.height /= 2;
            a.intersect(new Area(r));
            g.setClip(a);
            g.setPaint(this.getUpperPaint());
            g.fill(s);
            a = new Area(s);
            if (clip != null) {
                a.intersect(new Area(clip));
            }
            r = this.getBounds();
            r.y += r.height / 2;
            r.height -= r.height / 2;
            a.intersect(new Area(r));
            g.setClip(a);
            g.setPaint(this.getLowerPaint());
            g.fill(s);
        }
        Composite composite = g.getComposite();
        AlphaComposite comp = AlphaComposite.getInstance(3, 0.8f);
        g.setComposite(comp);
        int arc = this.getUpperLeftArc();
        r = this.getBounds();
        r.width = arc;
        r.height /= 2;
        a = new Area(s);
        if (clip != null) {
            a.intersect(new Area(clip));
        }
        a.intersect(new Area(r));
        g.setClip(a);
        g.setPaint(this.getUpperLeftPaint());
        g.fill(s);
        arc = this.getUpperRightArc();
        r = this.getBounds();
        r.x = r.x + r.width - arc;
        r.width = arc;
        r.height /= 2;
        a = new Area(s);
        if (clip != null) {
            a.intersect(new Area(clip));
        }
        a.intersect(new Area(r));
        g.setClip(a);
        g.setPaint(this.getUpperRightPaint());
        g.fill(s);
        arc = this.getLowerRightArc();
        r = this.getBounds();
        r.x = r.x + r.width - arc;
        r.width = arc;
        r.y += r.height / 2;
        r.height /= 2;
        a = new Area(s);
        if (clip != null) {
            a.intersect(new Area(clip));
        }
        a.intersect(new Area(r));
        g.setClip(a);
        g.setPaint(this.getLowerRightPaint());
        g.fill(s);
        arc = this.getLowerLeftArc();
        r = this.getBounds();
        r.width = arc;
        r.y += r.height / 2;
        r.height /= 2;
        a = new Area(s);
        if (clip != null) {
            a.intersect(new Area(clip));
        }
        a.intersect(new Area(r));
        g.setClip(a);
        g.setPaint(this.getLowerLeftPaint());
        g.fill(s);
        g.setClip(clip);
        g.setComposite(composite);
    }

    private boolean isVertical() {
        if (!this.allowVertical) {
            return false;
        }
        return this.bounds.height > this.bounds.width;
    }

    private GradientPaint getUpperPaint() {
        Rectangle r = this.getBounds();
        return ColorUtil.getGradientPaint(r.x, r.y + r.height / 9, this.getUpperTop(), r.x, r.y + r.height / 2, this.getUpperBottom(), true);
    }

    private GradientPaint getLowerPaint() {
        Rectangle r = this.getBounds();
        return ColorUtil.getGradientPaint(r.x, r.y + r.height / 2, this.getLowerTop(), r.x, r.y + r.height, this.getLowerBottom(), false);
    }

    private GradientPaint getLeftPaint() {
        Rectangle r = this.getBounds();
        return ColorUtil.getGradientPaint(r.x, r.y, this.getUpperTop(), r.x + r.width / 2, r.y, this.getUpperBottom());
    }

    private GradientPaint getRightPaint() {
        Rectangle r = this.getBounds();
        return ColorUtil.getGradientPaint(r.x + r.width / 2, r.y, this.getLowerTop(), r.x + r.width, r.y, this.getLowerBottom());
    }

    private GradientPaint getUpperLeftPaint() {
        Rectangle r = this.getBounds();
        int arc = this.getUpperLeftArc();
        if (!this.isVertical()) {
            return ColorUtil.getGradientPaint(r.x, r.y + r.height / 2, this.dark(), r.x + arc / 2, r.y + r.height / 2 - arc / 2, this.light());
        }
        return ColorUtil.getGradientPaint(r.x + r.width / 2, r.y, this.dark(), r.x + r.width / 2 - arc / 2, r.y + arc, this.light());
    }

    private GradientPaint getUpperRightPaint() {
        Rectangle r = this.getBounds();
        int arc = this.getUpperRightArc();
        if (!this.isVertical()) {
            return ColorUtil.getGradientPaint(r.x + r.width, r.y + r.height / 2, this.dark(), r.x + r.width - arc / 2, r.y + r.height / 2 - arc / 2, this.light());
        }
        return ColorUtil.getGradientPaint(r.x + r.width / 2, r.y, this.dark(), r.x + r.width / 2 + arc / 2, r.y + arc, this.light());
    }

    private GradientPaint getLowerRightPaint() {
        Rectangle r = this.getBounds();
        int arc = this.getLowerRightArc();
        if (!this.isVertical()) {
            return ColorUtil.getGradientPaint(r.x + r.width, r.y + r.height / 2, this.dark(), r.x + r.width - arc / 2, r.y + r.height / 2 + arc / 2, this.light());
        }
        return ColorUtil.getGradientPaint(r.x + r.width / 2, r.y + r.height, this.dark(), r.x + r.width / 2 + arc / 2, r.y + r.height - arc, this.light());
    }

    private GradientPaint getLowerLeftPaint() {
        Rectangle r = this.getBounds();
        int arc = this.getLowerLeftArc();
        if (!this.isVertical()) {
            return ColorUtil.getGradientPaint(r.x, r.y + r.height / 2, this.dark(), r.x + arc / 2, r.y + r.height / 2 + arc / 2, this.light());
        }
        return ColorUtil.getGradientPaint(r.x + r.width / 2, r.y + r.height, this.dark(), r.x + r.width / 2 - arc / 2, r.y + r.height - arc, this.light());
    }

    private Shape getClipShape() {
        if (this.changed) {
            this.update();
        }
        if (this.clip == null) {
            this.clip = this.createClip();
        }
        return this.clip;
    }

    protected Color dark() {
        if ((this.getState() & 2) != 0 && (this.getState() & 4) != 0) {
            return new Color(80, 80, 150);
        }
        return new Color(130, 130, 150);
    }

    private Color light() {
        Color dark = this.dark();
        return new Color(dark.getRed(), dark.getGreen(), dark.getBlue(), 0);
    }

    private void update() {
        this.clip = null;
    }

    private Shape createClip() {
        Rectangle bds = this.getBounds();
        if (!(this.notchLeft || this.notchRight || this.usePercentages || this.upperRightArc != this.lowerRightArc || this.lowerRightArc != this.lowerLeftArc || this.lowerLeftArc != this.upperLeftArc || this.upperLeftArc != 0)) {
            return new Rectangle(this.getBounds());
        }
        int upperRightArc = this.getUpperRightArc();
        int lowerRightArc = this.getLowerRightArc();
        int upperLeftArc = this.getUpperLeftArc();
        int lowerLeftArc = this.getLowerLeftArc();
        int notchR = this.getNotchRightArc();
        int notchL = this.getNotchLeftArc();
        GeneralPath gp = new GeneralPath();
        if (this.notchLeft) {
            gp.moveTo(bds.x + notchL, bds.y + bds.height / 2);
            gp.curveTo(bds.x + notchL, bds.y + bds.height / 2, bds.x + notchL, bds.y + bds.height / 2 - notchL, bds.x, bds.y + bds.height / 2 - notchL);
            if (bds.y + bds.height / 2 - notchL > bds.y + upperLeftArc) {
                gp.lineTo(bds.x, bds.y + upperLeftArc);
            }
            gp.curveTo(bds.x, Math.min(bds.y + upperLeftArc, bds.y + bds.height / 2 - notchL), bds.x, bds.y, bds.x + upperLeftArc, bds.y);
        } else {
            gp.moveTo(bds.x, bds.y + bds.height - lowerLeftArc);
            if (bds.y + bds.height - lowerLeftArc > bds.y + upperLeftArc) {
                gp.lineTo(bds.x, bds.y + upperLeftArc);
            }
            gp.curveTo(bds.x, bds.y + upperLeftArc, bds.x, bds.y, bds.x + upperLeftArc, bds.y);
        }
        if (bds.x + bds.width - upperLeftArc > bds.x + upperRightArc) {
            gp.lineTo(bds.x + bds.width - upperRightArc, bds.y);
        }
        if (this.notchRight) {
            gp.curveTo(bds.x + bds.width - upperRightArc - 1, bds.y, bds.x + bds.width - 1, bds.y, bds.x + bds.width - 1, Math.min(bds.y + upperRightArc, bds.y + bds.height / 2 - notchR));
            if (bds.y + upperRightArc < bds.y + bds.height / 2 - notchR) {
                gp.lineTo(bds.x + bds.width - 1, bds.y + bds.height / 2 - notchR);
            }
            gp.curveTo(bds.x + bds.width - 1, bds.y + bds.height / 2 - notchR, bds.x + bds.width - notchR - 1, bds.y + bds.height / 2 - notchR, bds.x + bds.width - notchR - 1, bds.y + bds.height / 2);
            gp.curveTo(bds.x + bds.width - notchR - 1, bds.y + bds.height / 2, bds.x + bds.width - notchR - 1, bds.y + bds.height / 2 + notchR, bds.x + bds.width - 1, bds.y + bds.height / 2 + notchR);
            if (bds.y + bds.height / 2 + notchR < bds.y + bds.height - lowerRightArc) {
                gp.lineTo(bds.x + bds.width - 1, bds.y + bds.height - lowerRightArc);
            }
            gp.curveTo(bds.x + bds.width - 1, Math.max(bds.y + bds.height / 2 + notchR, bds.y + bds.height - lowerRightArc), bds.x + bds.width - 1, bds.y + bds.height, bds.x + bds.width - lowerRightArc - 1, bds.y + bds.height);
        } else {
            if (upperRightArc != 0) {
                gp.curveTo(bds.x + bds.width - upperRightArc - 1, bds.y, bds.x + bds.width - 1, bds.y, bds.x + bds.width - 1, bds.y + upperRightArc);
            } else {
                gp.curveTo(bds.x + bds.width - upperRightArc, bds.y, bds.x + bds.width, bds.y, bds.x + bds.width, bds.y + upperRightArc);
            }
            if (bds.y + upperRightArc < bds.y + bds.height - lowerRightArc) {
                if (upperRightArc != 0 && lowerRightArc != 0) {
                    gp.lineTo(bds.x + bds.width - 1, bds.y + bds.height - lowerRightArc);
                } else {
                    gp.lineTo(bds.x + bds.width, bds.y + bds.height - lowerRightArc);
                }
            }
            if (lowerRightArc != 0) {
                gp.curveTo(bds.x + bds.width - 1, bds.y + bds.height - lowerRightArc, bds.x + bds.width - 1, bds.y + bds.height, bds.x + bds.width - lowerRightArc - 1, bds.y + bds.height);
            } else {
                gp.curveTo(bds.x + bds.width, bds.y + bds.height - lowerRightArc, bds.x + bds.width, bds.y + bds.height, bds.x + bds.width - lowerRightArc, bds.y + bds.height);
            }
        }
        if (bds.x + bds.width - lowerRightArc > bds.x + lowerLeftArc) {
            gp.lineTo(bds.x + lowerLeftArc, bds.y + bds.height);
        }
        if (this.notchLeft) {
            gp.curveTo(bds.x + lowerLeftArc, bds.y + bds.height, bds.x, bds.y + bds.height, bds.x, Math.max(bds.y + bds.height - lowerLeftArc, bds.y + bds.height / 2 + notchL));
            if (bds.y + bds.height - lowerLeftArc > bds.y + bds.height / 2 + notchL) {
                gp.lineTo(bds.x, bds.y + bds.height / 2 + notchL);
            }
            gp.curveTo(bds.x, bds.y + bds.height / 2 + notchL, bds.x + notchL, bds.y + bds.height / 2 + notchL, bds.x + notchL, bds.y + bds.height / 2);
        } else {
            gp.curveTo(bds.x + lowerLeftArc, bds.y + bds.height, bds.x, bds.y + bds.height, bds.x, bds.y + bds.height - lowerLeftArc);
        }
        return gp;
    }

    protected Color getUpperTop() {
        return this.upperTop;
    }

    protected Color getUpperBottom() {
        return this.upperBottom;
    }

    protected Color getLowerTop() {
        return this.lowerTop;
    }

    protected Color getLowerBottom() {
        return this.lowerBottom;
    }
}

