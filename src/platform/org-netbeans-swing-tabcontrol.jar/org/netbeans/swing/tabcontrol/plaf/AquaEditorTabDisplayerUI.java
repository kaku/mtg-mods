/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.plaf.AquaEditorTabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.BasicScrollingTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.ScrollingTabLayoutModel;
import org.netbeans.swing.tabcontrol.plaf.TabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;

public class AquaEditorTabDisplayerUI
extends BasicScrollingTabDisplayerUI {
    private static Map<Integer, String[]> buttonIconPaths;
    private Font txtFont;

    public AquaEditorTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
    }

    @Override
    protected TabCellRenderer createDefaultRenderer() {
        return new AquaEditorTabCellRenderer();
    }

    public static ComponentUI createUI(JComponent c) {
        return new AquaEditorTabDisplayerUI((TabDisplayer)c);
    }

    @Override
    protected boolean isAntialiased() {
        return true;
    }

    @Override
    protected Font createFont() {
        if (this.txtFont == null) {
            this.txtFont = (Font)UIManager.get("windowTitleFont");
            if (this.txtFont == null) {
                this.txtFont = new Font("Dialog", 0, 11);
            } else if (this.txtFont.isBold()) {
                this.txtFont = new Font(this.txtFont.getName(), 0, this.txtFont.getSize());
            }
        }
        return this.txtFont;
    }

    @Override
    protected Font getTxtFont() {
        return this.createFont();
    }

    @Override
    protected int createRepaintPolicy() {
        return 351;
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        int prefHeight = 28;
        Graphics2D g = BasicScrollingTabDisplayerUI.getOffscreenGraphics();
        if (g != null) {
            FontMetrics fm = g.getFontMetrics(this.displayer.getFont());
            Insets ins = this.getTabAreaInsets();
            prefHeight = fm.getHeight() + ins.top + ins.bottom + 7;
        }
        if (prefHeight % 2 == 0) {
            ++prefHeight;
        }
        return new Dimension(this.displayer.getWidth(), prefHeight);
    }

    @Override
    protected void paintBackground(Graphics g) {
        g.setColor(UIManager.getColor("NbTabControl.editorTabBackground"));
        g.fillRect(0, 0, this.displayer.getWidth(), this.displayer.getHeight());
    }

    @Override
    protected void paintAfterTabs(Graphics g) {
        int rightLineStart = this.getTabsAreaWidth();
        int rightLineEnd = this.displayer.getWidth();
        int y = this.displayer.getHeight();
        if (this.displayer.getModel().size() > 0 && !this.scroll().isLastTabClipped()) {
            int idx = this.scroll().getLastVisibleTab(this.displayer.getWidth());
            rightLineStart = this.scroll().getX(idx) + this.scroll().getW(idx);
        } else if (this.displayer.getModel().size() == 0) {
            rightLineStart = 6;
        }
        g.setColor(UIManager.getColor("NbTabControl.borderColor"));
        g.drawLine(rightLineStart, y - 1, rightLineEnd, y - 1);
    }

    private static void initIcons() {
        if (null == buttonIconPaths) {
            buttonIconPaths = new HashMap<Integer, String[]>(7);
            String[] iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/mac_scrollleft_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/mac_scrollleft_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/mac_scrollleft_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/mac_scrollleft_pressed.png";
            buttonIconPaths.put(9, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/mac_scrollright_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/mac_scrollright_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/mac_scrollright_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/mac_scrollright_pressed.png";
            buttonIconPaths.put(10, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/mac_popup_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/mac_popup_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/mac_popup_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/mac_popup_pressed.png";
            buttonIconPaths.put(8, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/mac_maximize_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/mac_maximize_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/mac_maximize_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/mac_maximize_pressed.png";
            buttonIconPaths.put(3, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/mac_restore_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/mac_restore_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/mac_restore_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/mac_restore_pressed.png";
            buttonIconPaths.put(4, iconPaths);
        }
    }

    @Override
    public Icon getButtonIcon(int buttonId, int buttonState) {
        Icon res = null;
        AquaEditorTabDisplayerUI.initIcons();
        String[] paths = buttonIconPaths.get(buttonId);
        if (null != paths && buttonState >= 0 && buttonState < paths.length) {
            res = TabControlButtonFactory.getIcon(paths[buttonState]);
        }
        return res;
    }
}

