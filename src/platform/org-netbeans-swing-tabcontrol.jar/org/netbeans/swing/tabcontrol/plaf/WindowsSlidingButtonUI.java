/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.JComponent;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicGraphicsUtils;
import org.netbeans.swing.tabcontrol.SlidingButton;
import org.netbeans.swing.tabcontrol.SlidingButtonUI;
import org.netbeans.swing.tabcontrol.plaf.WinClassicEditorTabCellRenderer;

public class WindowsSlidingButtonUI
extends SlidingButtonUI {
    private static final SlidingButtonUI INSTANCE = new WindowsSlidingButtonUI();
    private boolean defaults_initialized = false;
    protected Color focusColor;
    protected static int dashedRectGapX;
    protected static int dashedRectGapY;
    protected static int dashedRectGapWidth;
    protected static int dashedRectGapHeight;

    protected WindowsSlidingButtonUI() {
    }

    public static ComponentUI createUI(JComponent c) {
        return INSTANCE;
    }

    protected void installBorder(AbstractButton b) {
    }

    @Override
    public void installDefaults(AbstractButton b) {
        super.installDefaults(b);
        if (!this.defaults_initialized) {
            try {
                Integer in = (Integer)UIManager.get("Button.dashedRectGapX");
                dashedRectGapX = in == null ? 3 : in;
                in = (Integer)UIManager.get("Button.dashedRectGapY");
                dashedRectGapY = in == null ? 3 : in;
                in = (Integer)UIManager.get("Button.dashedRectGapWidth");
                dashedRectGapWidth = in == null ? 3 : in;
                in = (Integer)UIManager.get("Button.dashedRectGapHeight");
                dashedRectGapHeight = in == null ? 3 : in;
                this.focusColor = UIManager.getColor(this.getPropertyPrefix() + "focus");
                this.defaults_initialized = true;
            }
            catch (NullPointerException npe) {
                dashedRectGapX = 2;
                dashedRectGapY = 2;
                dashedRectGapWidth = 2;
                dashedRectGapHeight = 2;
            }
        }
    }

    @Override
    protected void uninstallDefaults(AbstractButton b) {
        super.uninstallDefaults(b);
        this.defaults_initialized = false;
    }

    @Override
    protected void paintBackground(Graphics2D g, AbstractButton b) {
        if (((SlidingButton)b).isBlinkState()) {
            g.setColor(WinClassicEditorTabCellRenderer.ATTENTION_COLOR);
            g.fillRect(0, 0, b.getWidth(), b.getHeight());
        } else {
            super.paintBackground(g, b);
        }
    }

    @Override
    protected void paintButtonPressed(Graphics g, AbstractButton b) {
        Color oldColor = g.getColor();
        if (((SlidingButton)b).isBlinkState()) {
            g.setColor(WinClassicEditorTabCellRenderer.ATTENTION_COLOR);
            g.fillRect(0, 0, b.getWidth(), b.getHeight());
        }
        int w = b.getWidth();
        int h = b.getHeight();
        UIDefaults table = UIManager.getLookAndFeelDefaults();
        if (b.getModel().isRollover() && !b.getModel().isPressed() && !b.getModel().isSelected()) {
            g.setColor(table.getColor("ToggleButton.highlight"));
            g.drawRect(0, 0, w - 1, h - 1);
            g.drawRect(0, 0, 0, h - 1);
            Color shade = table.getColor("ToggleButton.shadow");
            Container p = b.getParent();
            if (p != null && p.getBackground().equals(shade)) {
                shade = table.getColor("ToggleButton.darkShadow");
            }
            g.setColor(shade);
            g.drawLine(w - 1, 0, w - 1, h - 1);
            g.drawLine(0, h - 1, w - 1, h - 1);
        } else {
            Color shade = table.getColor("ToggleButton.shadow");
            Container p = b.getParent();
            if (p != null && p.getBackground().equals(shade)) {
                shade = table.getColor("ToggleButton.darkShadow");
            }
            g.setColor(shade);
            g.drawRect(0, 0, w - 1, h - 1);
            g.setColor(table.getColor("ToggleButton.highlight"));
            g.drawLine(w - 1, 0, w - 1, h - 1);
            g.drawLine(0, h - 1, w - 1, h - 1);
        }
        g.setColor(oldColor);
    }

    @Override
    protected void paintFocus(Graphics g, AbstractButton b, Rectangle viewRect, Rectangle textRect, Rectangle iconRect) {
        int width = b.getWidth();
        int height = b.getHeight();
        g.setColor(this.getFocusColor());
        BasicGraphicsUtils.drawDashedRect(g, dashedRectGapX, dashedRectGapY, width - dashedRectGapWidth, height - dashedRectGapHeight);
    }

    protected Color getFocusColor() {
        return this.focusColor;
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        Dimension d = super.getPreferredSize(c);
        AbstractButton b = (AbstractButton)c;
        if (b.isFocusPainted()) {
            if (d.width % 2 == 0) {
                ++d.width;
            }
            if (d.height % 2 == 0) {
                ++d.height;
            }
        }
        return d;
    }
}

