/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.MouseWheelEvent;
import java.awt.image.BufferedImage;
import java.lang.ref.SoftReference;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SingleSelectionModel;
import javax.swing.Timer;
import javax.swing.UIManager;
import org.netbeans.swing.tabcontrol.ComponentConverter;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.plaf.AbstractTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.BasicTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.DefaultTabLayoutModel;
import org.netbeans.swing.tabcontrol.plaf.ScrollingTabLayoutModel;
import org.netbeans.swing.tabcontrol.plaf.TabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.TabControlButton;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.netbeans.swing.tabcontrol.plaf.TabLayoutModel;
import org.netbeans.swing.tabcontrol.plaf.TabState;
import org.openide.windows.TopComponent;

public abstract class BasicScrollingTabDisplayerUI
extends BasicTabDisplayerUI {
    private Rectangle scratch = new Rectangle();
    private JPanel controlButtons;
    private TabControlButton btnScrollLeft;
    private TabControlButton btnScrollRight;
    private TabControlButton btnDropDown;
    private TabControlButton btnMaximizeRestore;
    private final Autoscroller autoscroll;
    private int lastKnownModelSize;
    static SoftReference<BufferedImage> ctx = null;

    public BasicScrollingTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
        this.autoscroll = new Autoscroller();
        this.lastKnownModelSize = Integer.MAX_VALUE;
    }

    @Override
    public Insets getAutoscrollInsets() {
        return new Insets(0, 30, 0, 30);
    }

    @Override
    public void autoscroll(Point location) {
        if (!this.displayer.getBounds().contains(location)) {
            this.autoscroll.stop();
            return;
        }
        if (location.x < 30) {
            this.autoscroll.start(true);
        } else if (location.x > this.displayer.getWidth() - 30 - this.getTabAreaInsets().right) {
            this.autoscroll.start(false);
        } else {
            this.autoscroll.stop();
        }
    }

    @Override
    protected TabLayoutModel createLayoutModel() {
        DefaultTabLayoutModel dtlm = new DefaultTabLayoutModel(this.displayer.getModel(), this.displayer);
        return new ScrollingTabLayoutModel(dtlm, this.selectionModel, this.displayer.getModel());
    }

    @Override
    protected TabState createTabState() {
        return new ScrollingTabState();
    }

    @Override
    protected HierarchyListener createHierarchyListener() {
        return new ScrollingHierarchyListener();
    }

    @Override
    public void makeTabVisible(int tab) {
        if (tab < 0) {
            return;
        }
        if (this.scroll().makeVisible(tab, this.getTabsAreaWidth())) {
            this.getTabsVisibleArea(this.scratch);
            this.displayer.repaint(this.scratch.x, this.scratch.y, this.scratch.width, this.scratch.height);
        }
        if (null == this.btnMaximizeRestore) {
            return;
        }
        TabData td = this.displayer.getModel().getTab(tab);
        Component c = td.getComponent();
        if (!(c instanceof TopComponent)) {
            return;
        }
        boolean maximizeEnabled = this.displayer.getContainerWinsysInfo().isTopComponentMaximizationEnabled((TopComponent)c);
        this.btnMaximizeRestore.setEnabled(maximizeEnabled);
    }

    protected final int getTabsAreaWidth() {
        int result = this.displayer.getWidth();
        Insets ins = this.getTabAreaInsets();
        return result - (ins.left + ins.right);
    }

    @Override
    public Insets getTabAreaInsets() {
        return new Insets(0, 0, 0, this.getControlButtons().getPreferredSize().width + 5);
    }

    @Override
    protected final int getLastVisibleTab() {
        if (this.displayer.getModel().size() == 0) {
            return -1;
        }
        return this.scroll().getLastVisibleTab(this.getTabsAreaWidth());
    }

    @Override
    protected final int getFirstVisibleTab() {
        if (this.displayer.getModel().size() == 0) {
            return -1;
        }
        return this.scroll().getFirstVisibleTab(this.getTabsAreaWidth());
    }

    @Override
    protected void install() {
        super.install();
        this.installControlButtons();
        ((ScrollingTabLayoutModel)this.layoutModel).setPixelsToAddToSelection(this.defaultRenderer.getPixelsToAddToSelection());
    }

    @Override
    protected void uninstall() {
        super.uninstall();
        this.displayer.setLayout(null);
        this.displayer.removeAll();
    }

    protected LayoutManager createLayout() {
        return new WCLayout();
    }

    protected Component getControlButtons() {
        if (null == this.controlButtons) {
            JPanel buttonsPanel = new JPanel(null);
            buttonsPanel.setOpaque(false);
            int width = 0;
            int height = 0;
            boolean isGTK = "GTK".equals(UIManager.getLookAndFeel().getID());
            Action a = this.scroll().getBackwardAction();
            a.putValue("control", this.displayer);
            this.btnScrollLeft = TabControlButtonFactory.createScrollLeftButton(this.displayer, a, isGTK);
            buttonsPanel.add(this.btnScrollLeft);
            Dimension prefDim = this.btnScrollLeft.getPreferredSize();
            this.btnScrollLeft.setBounds(width, 0, prefDim.width, prefDim.height);
            width += prefDim.width;
            height = prefDim.height;
            a = this.scroll().getForwardAction();
            a.putValue("control", this.displayer);
            this.btnScrollRight = TabControlButtonFactory.createScrollRightButton(this.displayer, a, isGTK);
            buttonsPanel.add(this.btnScrollRight);
            prefDim = this.btnScrollRight.getPreferredSize();
            this.btnScrollRight.setBounds(width += this.getScrollButtonPadding(), 0, prefDim.width, prefDim.height);
            width += prefDim.width;
            height = Math.max(height, prefDim.height);
            this.btnDropDown = TabControlButtonFactory.createDropDownButton(this.displayer, isGTK);
            buttonsPanel.add(this.btnDropDown);
            prefDim = this.btnDropDown.getPreferredSize();
            this.btnDropDown.setBounds(width += 3, 0, prefDim.width, prefDim.height);
            width += prefDim.width;
            height = Math.max(height, prefDim.height);
            if (null != this.displayer.getContainerWinsysInfo() && this.displayer.getContainerWinsysInfo().isTopComponentMaximizationEnabled()) {
                this.btnMaximizeRestore = TabControlButtonFactory.createMaximizeRestoreButton(this.displayer, isGTK);
                buttonsPanel.add(this.btnMaximizeRestore);
                prefDim = this.btnMaximizeRestore.getPreferredSize();
                this.btnMaximizeRestore.setBounds(width += 3, 0, prefDim.width, prefDim.height);
                width += prefDim.width;
                height = Math.max(height, prefDim.height);
            }
            Dimension size = new Dimension(width, height);
            buttonsPanel.setMinimumSize(size);
            buttonsPanel.setSize(size);
            buttonsPanel.setPreferredSize(size);
            buttonsPanel.setMaximumSize(size);
            this.controlButtons = buttonsPanel;
        }
        return this.controlButtons;
    }

    int getScrollButtonPadding() {
        return 0;
    }

    @Override
    protected ComponentListener createComponentListener() {
        return new ScrollingDisplayerComponentListener();
    }

    @Override
    protected void modelChanged() {
        int modelSize;
        this.scroll().clearCachedData();
        int index = this.selectionModel.getSelectedIndex();
        if (index >= this.scroll().getCachedFirstVisibleTab() && index < this.scroll().getCachedLastVisibleTab()) {
            this.makeTabVisible(this.selectionModel.getSelectedIndex());
        }
        if ((modelSize = this.displayer.getModel().size()) < this.lastKnownModelSize) {
            this.scroll().ensureAvailableSpaceUsed(true);
        }
        this.lastKnownModelSize = modelSize;
        super.modelChanged();
    }

    protected void installControlButtons() {
        this.displayer.setLayout(this.createLayout());
        this.displayer.add(this.getControlButtons());
    }

    protected final ScrollingTabLayoutModel scroll() {
        return (ScrollingTabLayoutModel)this.layoutModel;
    }

    @Override
    protected void processMouseWheelEvent(MouseWheelEvent e) {
        int i = e.getWheelRotation();
        this.tabState.clearTransientStates();
        int offset = this.scroll().getOffset();
        if (i > 0 && offset < this.displayer.getModel().size() - 1) {
            if (this.scroll().isLastTabClipped()) {
                this.scroll().setOffset(offset + 1);
            }
        } else if (i < 0) {
            if (offset >= 0) {
                this.scroll().setOffset(offset - 1);
            }
        } else {
            return;
        }
        this.displayer.repaint();
    }

    public static Graphics2D getOffscreenGraphics() {
        BufferedImage result = null;
        if (ctx != null) {
            result = ctx.get();
        }
        if (result == null) {
            result = new BufferedImage(10, 10, 1);
            ctx = new SoftReference<BufferedImage>(result);
        }
        return (Graphics2D)result.getGraphics();
    }

    protected Rectangle getControlButtonsRectangle(Container parent) {
        Component c = this.getControlButtons();
        return new Rectangle(parent.getWidth() - c.getWidth(), 0, c.getWidth(), c.getHeight());
    }

    @Override
    public Dimension getMinimumSize(JComponent c) {
        Component comp;
        int index = this.displayer.getSelectionModel().getSelectedIndex();
        TabDataModel model = this.displayer.getModel();
        if (index < 0 || index >= model.size()) {
            index = 0;
        }
        if (null == (comp = model.getTab(index).getComponent())) {
            comp = this.displayer.getComponentConverter().getComponent(model.getTab(index));
        }
        Dimension minSize = null == comp ? new Dimension(100, 10) : comp.getMinimumSize();
        minSize.width = Math.max(minSize.width, 100);
        minSize.height = Math.max(minSize.height, 10);
        return minSize;
    }

    private class Autoscroller
    implements ActionListener {
        private int direction;
        private Timer timer;

        private Autoscroller() {
            this.direction = 0;
        }

        public void start(boolean scrollLeft) {
            int newDirection;
            int n = newDirection = scrollLeft ? -1 : 1;
            if (null == this.timer || !this.timer.isRunning() || this.direction != newDirection) {
                if (null == this.timer) {
                    this.timer = new Timer(300, this);
                    this.timer.setRepeats(true);
                }
                this.direction = newDirection;
                this.timer.start();
            }
        }

        public void stop() {
            if (null != this.timer) {
                this.timer.stop();
            }
            this.direction = 0;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (this.direction < 0) {
                int offset = BasicScrollingTabDisplayerUI.this.scroll().getOffset();
                if (offset >= 0) {
                    BasicScrollingTabDisplayerUI.this.scroll().setOffset(offset - 1);
                } else {
                    this.timer.stop();
                }
            } else if (this.direction > 0) {
                int offset = BasicScrollingTabDisplayerUI.this.scroll().getOffset();
                if (offset < BasicScrollingTabDisplayerUI.this.displayer.getModel().size() - 1 && BasicScrollingTabDisplayerUI.this.scroll().isLastTabClipped()) {
                    BasicScrollingTabDisplayerUI.this.scroll().setOffset(offset + 1);
                } else {
                    this.timer.stop();
                }
            }
            BasicScrollingTabDisplayerUI.this.displayer.repaint();
        }
    }

    private class WCLayout
    implements LayoutManager {
        private WCLayout() {
        }

        @Override
        public void addLayoutComponent(String name, Component comp) {
        }

        @Override
        public void layoutContainer(Container parent) {
            Rectangle r = BasicScrollingTabDisplayerUI.this.getControlButtonsRectangle(parent);
            Component c = BasicScrollingTabDisplayerUI.this.getControlButtons();
            c.setBounds(r);
        }

        @Override
        public Dimension minimumLayoutSize(Container parent) {
            return BasicScrollingTabDisplayerUI.this.getMinimumSize((JComponent)parent);
        }

        @Override
        public Dimension preferredLayoutSize(Container parent) {
            return BasicScrollingTabDisplayerUI.this.getPreferredSize((JComponent)parent);
        }

        @Override
        public void removeLayoutComponent(Component comp) {
        }
    }

    protected class ScrollingHierarchyListener
    extends AbstractTabDisplayerUI.DisplayerHierarchyListener {
        protected ScrollingHierarchyListener() {
            super((AbstractTabDisplayerUI)BasicScrollingTabDisplayerUI.this);
        }

        @Override
        public void hierarchyChanged(HierarchyEvent e) {
            super.hierarchyChanged(e);
            if ((e.getChangeFlags() & 4) != 0 && BasicScrollingTabDisplayerUI.this.displayer.isShowing() && BasicScrollingTabDisplayerUI.this.tabState != null && BasicScrollingTabDisplayerUI.this.selectionModel != null) {
                BasicScrollingTabDisplayerUI.this.tabState.setActive(BasicScrollingTabDisplayerUI.this.displayer.isActive());
                BasicScrollingTabDisplayerUI.this.makeTabVisible(BasicScrollingTabDisplayerUI.this.selectionModel.getSelectedIndex());
            }
        }
    }

    protected class ScrollingDisplayerComponentListener
    extends ComponentAdapter {
        protected ScrollingDisplayerComponentListener() {
        }

        @Override
        public void componentResized(ComponentEvent e) {
            BasicScrollingTabDisplayerUI.this.makeTabVisible(BasicScrollingTabDisplayerUI.this.selectionModel.getSelectedIndex());
        }
    }

    protected class ScrollingTabState
    extends BasicTabDisplayerUI.BasicTabState {
        protected ScrollingTabState() {
            super(BasicScrollingTabDisplayerUI.this);
        }

        @Override
        public int getState(int tabIndex) {
            int result = super.getState(tabIndex);
            int first = BasicScrollingTabDisplayerUI.this.getFirstVisibleTab();
            int last = BasicScrollingTabDisplayerUI.this.getLastVisibleTab();
            if (tabIndex < first || tabIndex > last) {
                return 64;
            }
            if (first == last && first == tabIndex && BasicScrollingTabDisplayerUI.this.displayer.getModel().size() > 1) {
                result |= 3;
            } else if (BasicScrollingTabDisplayerUI.this.getTabsAreaWidth() < BasicScrollingTabDisplayerUI.this.scroll().getMinimumLeftClippedWidth() + BasicScrollingTabDisplayerUI.this.scroll().getMinimumRightClippedWidth() && tabIndex == first && last == first - 1 && BasicScrollingTabDisplayerUI.this.displayer.getModel().size() > 1 && BasicScrollingTabDisplayerUI.this.scroll().isLastTabClipped()) {
                result |= 2;
            } else {
                if (tabIndex == first && BasicScrollingTabDisplayerUI.this.scroll().getOffset() == first) {
                    result |= 2;
                }
                if (tabIndex == last && BasicScrollingTabDisplayerUI.this.scroll().isLastTabClipped()) {
                    result |= 1;
                }
            }
            return result;
        }
    }

}

