/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.SingleSelectionModel;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.plaf.BasicScrollingTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.ScrollingTabLayoutModel;
import org.netbeans.swing.tabcontrol.plaf.TabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.netbeans.swing.tabcontrol.plaf.WinXPEditorTabCellRenderer;

public final class WinXPEditorTabDisplayerUI
extends BasicScrollingTabDisplayerUI {
    private static final Rectangle scratch5 = new Rectangle();
    private static Map<Integer, String[]> buttonIconPaths;

    public WinXPEditorTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
    }

    public static ComponentUI createUI(JComponent c) {
        return new WinXPEditorTabDisplayerUI((TabDisplayer)c);
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        int prefHeight = 24;
        Graphics2D g = BasicScrollingTabDisplayerUI.getOffscreenGraphics();
        if (g != null) {
            FontMetrics fm = g.getFontMetrics(this.displayer.getFont());
            Insets ins = this.getTabAreaInsets();
            prefHeight = fm.getHeight() + ins.top + ins.bottom + 8;
        }
        return new Dimension(this.displayer.getWidth(), prefHeight);
    }

    @Override
    protected Font createFont() {
        return UIManager.getFont("Label.font");
    }

    @Override
    public void paintBackground(Graphics g) {
        g.setColor(this.displayer.getBackground());
        g.fillRect(0, 0, this.displayer.getWidth(), this.displayer.getHeight());
    }

    @Override
    protected void paintAfterTabs(Graphics g) {
        Rectangle r = new Rectangle();
        this.getTabsVisibleArea(r);
        r.width = this.displayer.getWidth();
        Insets ins = this.getTabAreaInsets();
        int y = this.displayer.getHeight() - 3;
        int selEnd = 0;
        int i = this.selectionModel.getSelectedIndex();
        g.setColor(WinXPEditorTabCellRenderer.getSelectedTabBottomLineColor());
        g.drawLine(0, y + 1, this.displayer.getWidth(), y + 1);
        int tabsWidth = this.getTabsAreaWidth();
        boolean needSplitLine = i != -1 && (i < this.scroll().getLastVisibleTab(tabsWidth) || i <= this.scroll().getLastVisibleTab(tabsWidth) && !this.scroll().isLastTabClipped()) && i >= this.scroll().getFirstVisibleTab(tabsWidth);
        g.setColor(UIManager.getColor("controlLtHighlight"));
        if (needSplitLine) {
            this.getTabRect(i, scratch5);
            if (WinXPEditorTabDisplayerUI.scratch5.width != 0) {
                if (r.x < WinXPEditorTabDisplayerUI.scratch5.x) {
                    g.drawLine(r.x, y, WinXPEditorTabDisplayerUI.scratch5.x + 1, y);
                }
                if (WinXPEditorTabDisplayerUI.scratch5.x + WinXPEditorTabDisplayerUI.scratch5.width < r.x + r.width) {
                    selEnd = WinXPEditorTabDisplayerUI.scratch5.x + WinXPEditorTabDisplayerUI.scratch5.width;
                    if (!this.scroll().isLastTabClipped()) {
                        --selEnd;
                    }
                    g.drawLine(selEnd, y, r.x + r.width, y);
                }
            }
        } else {
            g.drawLine(r.x, y, r.x + r.width, y);
        }
        g.setColor(WinXPEditorTabCellRenderer.getBorderColor());
        g.drawLine(0, y - 1, 0, this.displayer.getHeight());
        g.drawLine(this.displayer.getWidth() - 1, y - 1, this.displayer.getWidth() - 1, this.displayer.getHeight());
        int last = this.scroll().getLastVisibleTab(tabsWidth);
        boolean l = false;
        if (last >= 0) {
            this.getTabRect(last, scratch5);
            last = WinXPEditorTabDisplayerUI.scratch5.x + WinXPEditorTabDisplayerUI.scratch5.width;
        }
        g.drawLine(last, y - 1, this.displayer.getWidth(), y - 1);
    }

    @Override
    protected TabCellRenderer createDefaultRenderer() {
        return new WinXPEditorTabCellRenderer();
    }

    private static void initIcons() {
        if (null == buttonIconPaths) {
            buttonIconPaths = new HashMap<Integer, String[]>(7);
            String[] iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/xp_scrollleft_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/xp_scrollleft_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/xp_scrollleft_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/xp_scrollleft_pressed.png";
            buttonIconPaths.put(9, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/xp_scrollright_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/xp_scrollright_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/xp_scrollright_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/xp_scrollright_pressed.png";
            buttonIconPaths.put(10, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/xp_popup_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/xp_popup_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/xp_popup_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/xp_popup_pressed.png";
            buttonIconPaths.put(8, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/xp_maximize_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/xp_maximize_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/xp_maximize_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/xp_maximize_pressed.png";
            buttonIconPaths.put(3, iconPaths);
            iconPaths = new String[4];
            iconPaths[0] = "org/netbeans/swing/tabcontrol/resources/xp_restore_enabled.png";
            iconPaths[2] = "org/netbeans/swing/tabcontrol/resources/xp_restore_disabled.png";
            iconPaths[3] = "org/netbeans/swing/tabcontrol/resources/xp_restore_rollover.png";
            iconPaths[1] = "org/netbeans/swing/tabcontrol/resources/xp_restore_pressed.png";
            buttonIconPaths.put(4, iconPaths);
        }
    }

    @Override
    public Icon getButtonIcon(int buttonId, int buttonState) {
        Icon res = null;
        WinXPEditorTabDisplayerUI.initIcons();
        String[] paths = buttonIconPaths.get(buttonId);
        if (null != paths && buttonState >= 0 && buttonState < paths.length) {
            res = TabControlButtonFactory.getIcon(paths[buttonState]);
        }
        return res;
    }
}

