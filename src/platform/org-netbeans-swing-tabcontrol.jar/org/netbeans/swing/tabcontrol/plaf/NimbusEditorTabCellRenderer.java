/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.Painter;
import javax.swing.UIManager;
import org.netbeans.swing.tabcontrol.plaf.AbstractTabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.BusyTabsSupport;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.netbeans.swing.tabcontrol.plaf.TabPainter;

final class NimbusEditorTabCellRenderer
extends AbstractTabCellRenderer {
    private static final TabPainter leftClip = new NimbusLeftClipPainter();
    private static final TabPainter rightClip = new NimbusRightClipPainter();
    private static final TabPainter normal = new NimbusPainter();
    static final Color ATTENTION_COLOR = new Color(255, 238, 120);
    private static final Insets INSETS = new Insets(0, 4, 0, 2);

    public NimbusEditorTabCellRenderer() {
        super(leftClip, normal, rightClip, new Dimension(28, 32));
    }

    @Override
    public Color getSelectedForeground() {
        return UIManager.getColor("textText");
    }

    @Override
    public Color getForeground() {
        return this.getSelectedForeground();
    }

    @Override
    public int getPixelsToAddToSelection() {
        return 4;
    }

    @Override
    protected int getCaptionYAdjustment() {
        return -2;
    }

    @Override
    protected int getIconYAdjustment() {
        return -3;
    }

    @Override
    public Dimension getPadding() {
        Dimension d = super.getPadding();
        d.width = this.isShowCloseButton() && !Boolean.getBoolean("nb.tabs.suppressCloseButton") ? 28 : 14;
        return d;
    }

    @Override
    protected void paintIconAndText(Graphics g) {
        if (this.isBusy()) {
            this.setIcon(BusyTabsSupport.getDefault().getBusyIcon(this.isSelected()));
        }
        super.paintIconAndText(g);
    }

    private static void paintTabBackground(Graphics g, int index, Component c, int x, int y, int w, int h) {
        Shape clip = g.getClip();
        NimbusEditorTabCellRenderer ren = (NimbusEditorTabCellRenderer)c;
        ++w;
        boolean isPreviousTabSelected = ren.isPreviousTabSelected();
        if (isPreviousTabSelected) {
            g.setClip(x + 1, y, w - 1, h);
        }
        Object o = null;
        o = ren.isSelected() ? (ren.isActive() ? UIManager.get("TabbedPane:TabbedPaneTab[MouseOver+Selected].backgroundPainter") : UIManager.get("TabbedPane:TabbedPaneTab[Selected].backgroundPainter")) : UIManager.get("TabbedPane:TabbedPaneTab[Enabled].backgroundPainter");
        if (o != null && o instanceof Painter) {
            Painter painter = (Painter)o;
            BufferedImage bufIm = new BufferedImage(w, h, 1);
            Graphics2D g2d = bufIm.createGraphics();
            g2d.setBackground(UIManager.getColor("Panel.background"));
            g2d.clearRect(0, 0, w, h);
            painter.paint(g2d, null, w, h);
            g.drawImage(bufIm, x, y, null);
        }
        if (isPreviousTabSelected) {
            g.setClip(clip);
        }
    }

    private static int getHeightDifference(NimbusEditorTabCellRenderer ren) {
        return 0;
    }

    private static class NimbusRightClipPainter
    implements TabPainter {
        private NimbusRightClipPainter() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return INSETS;
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        @Override
        public Polygon getInteriorPolygon(Component c) {
            NimbusEditorTabCellRenderer ren = (NimbusEditorTabCellRenderer)c;
            Insets ins = this.getBorderInsets(c);
            Polygon p = new Polygon();
            int x = 0;
            int y = 0;
            int width = c.getWidth() + 10;
            int height = c.getHeight() - 4;
            p.addPoint(x, y);
            p.addPoint(x + width, y);
            p.addPoint(x + width, y + height);
            p.addPoint(x, y + height);
            return p;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        }

        @Override
        public void paintInterior(Graphics g, Component c) {
            NimbusEditorTabCellRenderer ren = (NimbusEditorTabCellRenderer)c;
            Polygon p = this.getInteriorPolygon(c);
            Rectangle bounds = p.getBounds();
            int yDiff = NimbusEditorTabCellRenderer.getHeightDifference(ren);
            NimbusEditorTabCellRenderer.paintTabBackground(g, 0, c, bounds.x, bounds.y + yDiff, bounds.width, bounds.height - yDiff);
        }

        @Override
        public boolean supportsCloseButton(JComponent renderer) {
            return false;
        }

        @Override
        public void getCloseButtonRectangle(JComponent jc, Rectangle rect, Rectangle bounds) {
            rect.setBounds(-20, -20, 0, 0);
        }
    }

    private static class NimbusLeftClipPainter
    implements TabPainter {
        private NimbusLeftClipPainter() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return INSETS;
        }

        @Override
        public Polygon getInteriorPolygon(Component c) {
            NimbusEditorTabCellRenderer ren = (NimbusEditorTabCellRenderer)c;
            Insets ins = this.getBorderInsets(c);
            Polygon p = new Polygon();
            int x = -3;
            int y = 0;
            int width = c.getWidth() + 3;
            int height = c.getHeight() - 4;
            p.addPoint(x, y);
            p.addPoint(x + width, y);
            p.addPoint(x + width, y + height);
            p.addPoint(x, y + height);
            return p;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        }

        @Override
        public void paintInterior(Graphics g, Component c) {
            NimbusEditorTabCellRenderer ren = (NimbusEditorTabCellRenderer)c;
            Polygon p = this.getInteriorPolygon(c);
            Rectangle bounds = p.getBounds();
            int yDiff = NimbusEditorTabCellRenderer.getHeightDifference(ren);
            NimbusEditorTabCellRenderer.paintTabBackground(g, 0, c, bounds.x, bounds.y + yDiff, bounds.width, bounds.height - yDiff);
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        @Override
        public void getCloseButtonRectangle(JComponent jc, Rectangle rect, Rectangle bounds) {
            rect.setBounds(-20, -20, 0, 0);
        }

        @Override
        public boolean supportsCloseButton(JComponent renderer) {
            return false;
        }
    }

    private static class NimbusPainter
    implements TabPainter {
        private NimbusPainter() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return INSETS;
        }

        @Override
        public Polygon getInteriorPolygon(Component c) {
            NimbusEditorTabCellRenderer ren = (NimbusEditorTabCellRenderer)c;
            Insets ins = this.getBorderInsets(c);
            Polygon p = new Polygon();
            int x = ren.isLeftmost() ? 3 : 0;
            int y = 0;
            int width = ren.isLeftmost() ? c.getWidth() - 3 : c.getWidth();
            int height = c.getHeight() - 4;
            p.addPoint(x, y);
            p.addPoint(x + width, y);
            p.addPoint(x + width, y + height);
            p.addPoint(x, y + height);
            return p;
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            NimbusEditorTabCellRenderer ren = (NimbusEditorTabCellRenderer)c;
            Polygon p = this.getInteriorPolygon(c);
        }

        @Override
        public void paintInterior(Graphics g, Component c) {
            NimbusEditorTabCellRenderer ren = (NimbusEditorTabCellRenderer)c;
            Polygon p = this.getInteriorPolygon(c);
            Rectangle bounds = p.getBounds();
            int yDiff = NimbusEditorTabCellRenderer.getHeightDifference(ren);
            NimbusEditorTabCellRenderer.paintTabBackground(g, 0, c, bounds.x, bounds.y + yDiff, bounds.width, bounds.height - yDiff);
            if (!this.supportsCloseButton((JComponent)c)) {
                return;
            }
            this.paintCloseButton(g, (JComponent)c);
        }

        @Override
        public void getCloseButtonRectangle(JComponent jc, Rectangle rect, Rectangle bounds) {
            boolean notSupported;
            boolean rightClip = ((NimbusEditorTabCellRenderer)jc).isClipRight();
            boolean leftClip = ((NimbusEditorTabCellRenderer)jc).isClipLeft();
            boolean bl = notSupported = !((NimbusEditorTabCellRenderer)jc).isShowCloseButton();
            if (leftClip || rightClip || notSupported) {
                rect.x = -100;
                rect.y = -100;
                rect.width = 0;
                rect.height = 0;
            } else {
                String iconPath = this.findIconPath((NimbusEditorTabCellRenderer)jc);
                Icon icon = TabControlButtonFactory.getIcon(iconPath);
                int iconWidth = icon.getIconWidth();
                int iconHeight = icon.getIconHeight();
                rect.x = bounds.x + bounds.width - iconWidth - 2;
                rect.y = bounds.y + Math.max(0, bounds.height / 2 - iconHeight / 2);
                rect.width = iconWidth;
                rect.height = iconHeight;
            }
        }

        private void paintCloseButton(Graphics g, JComponent c) {
            if (((AbstractTabCellRenderer)c).isShowCloseButton()) {
                Rectangle r = new Rectangle(0, 0, c.getWidth(), c.getHeight());
                Rectangle cbRect = new Rectangle();
                this.getCloseButtonRectangle(c, cbRect, r);
                String iconPath = this.findIconPath((NimbusEditorTabCellRenderer)c);
                Icon icon = TabControlButtonFactory.getIcon(iconPath);
                icon.paintIcon(c, g, cbRect.x, cbRect.y);
            }
        }

        private String findIconPath(NimbusEditorTabCellRenderer renderer) {
            if (renderer.inCloseButton() && renderer.isPressed()) {
                return "org/openide/awt/resources/gtk_close_pressed.png";
            }
            if (renderer.inCloseButton()) {
                return "org/openide/awt/resources/gtk_close_rollover.png";
            }
            return "org/openide/awt/resources/gtk_close_enabled.png";
        }

        @Override
        public boolean supportsCloseButton(JComponent renderer) {
            return ((AbstractTabCellRenderer)renderer).isShowCloseButton();
        }
    }

}

