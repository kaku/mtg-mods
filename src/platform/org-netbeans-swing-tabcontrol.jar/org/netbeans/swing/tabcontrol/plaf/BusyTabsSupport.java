/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.WeakSet
 *  org.openide.windows.TopComponent
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Component;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.customtabs.Tabbed;
import org.netbeans.swing.tabcontrol.plaf.BusyIcon;
import org.openide.util.Lookup;
import org.openide.util.WeakSet;
import org.openide.windows.TopComponent;

public abstract class BusyTabsSupport {
    private Timer animationTimer;
    private final ChangeListener modelListener;
    private final Set<Tabbed> containers;
    private final Set<Tabbed> busyContainers;

    public BusyTabsSupport() {
        this.modelListener = new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                BusyTabsSupport.this.checkBusyTabs();
            }
        };
        this.containers = new WeakSet(10);
        this.busyContainers = new WeakSet(10);
    }

    public static BusyTabsSupport getDefault() {
        return (BusyTabsSupport)Lookup.getDefault().lookup(BusyTabsSupport.class);
    }

    public final void install(Tabbed tabContainer, TabDataModel tabModel) {
        if (this.containers.contains(tabContainer)) {
            return;
        }
        tabModel.addChangeListener(this.modelListener);
        this.containers.add(tabContainer);
        this.checkBusyTabs();
    }

    public final void uninstall(Tabbed tabContainer, TabDataModel tabModel) {
        if (this.busyContainers.remove(tabContainer)) {
            this.repaintAll(tabContainer);
        }
        tabModel.removeChangeListener(this.modelListener);
        this.containers.remove(tabContainer);
        this.checkBusyTabs();
    }

    public abstract Icon getBusyIcon(boolean var1);

    public final void makeTabBusy(Tabbed tabContainer, int tabIndex, boolean busy) {
        Rectangle tabRect;
        if (!busy && null != (tabRect = tabContainer.getTabBounds(tabIndex))) {
            tabContainer.getComponent().repaint(tabRect.x, tabRect.y, tabRect.width, tabRect.height);
        }
        this.checkBusyTabs();
    }

    protected abstract int getRepaintTimerIntervalMillis();

    protected abstract void tick();

    private void startAnimationTimer() {
        if (null != this.animationTimer) {
            return;
        }
        int interval = this.getRepaintTimerIntervalMillis();
        if (interval <= 0) {
            return;
        }
        this.animationTimer = new Timer(interval, new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                BusyTabsSupport.this.checkBusyTabs();
                BusyTabsSupport.this.repaintBusyTabs();
                BusyTabsSupport.this.tick();
            }
        });
        this.animationTimer.setRepeats(true);
        this.animationTimer.start();
    }

    private void stopAnimationTimer() {
        if (null == this.animationTimer) {
            return;
        }
        this.animationTimer.stop();
        this.animationTimer = null;
        this.repaintBusyTabs();
    }

    private void checkBusyTabs() {
        this.busyContainers.clear();
        for (Tabbed tc : this.containers) {
            if (!this.hasBusyTabs(tc)) continue;
            this.busyContainers.add(tc);
        }
        if (this.busyContainers.isEmpty()) {
            this.stopAnimationTimer();
        } else {
            this.startAnimationTimer();
        }
    }

    private void repaintBusyTabs() {
        for (Tabbed tc : this.busyContainers) {
            this.repaintBusy(tc);
        }
    }

    private void repaintBusy(Tabbed tabbed) {
        for (int i = 0; i < tabbed.getTabCount(); ++i) {
            TopComponent tc = tabbed.getTopComponentAt(i);
            if (!tabbed.isBusy(tc)) continue;
            Rectangle rect = tabbed.getTabBounds(i);
            tabbed.getComponent().repaint(rect.x, rect.y, rect.width, rect.height);
        }
    }

    private void repaintAll(Tabbed tc) {
        Rectangle rect = tc.getTabsArea();
        tc.getComponent().repaint(rect.x, rect.y, rect.width, rect.height);
    }

    private boolean hasBusyTabs(Tabbed tabbedContainer) {
        boolean res = false;
        for (int tabIndex = 0; tabIndex < tabbedContainer.getTabCount(); ++tabIndex) {
            if (!tabbedContainer.isBusy(tabbedContainer.getTopComponentAt(tabIndex))) continue;
            res = true;
            break;
        }
        return res;
    }

    public static final class DefaultBusyTabsSupport
    extends BusyTabsSupport {
        private BusyIcon busyIconSelected;
        private BusyIcon busyIconDefault;

        @Override
        public Icon getBusyIcon(boolean isTabSelected) {
            if (isTabSelected) {
                if (null == this.busyIconSelected) {
                    this.busyIconSelected = BusyIcon.create(isTabSelected);
                }
                return this.busyIconSelected;
            }
            if (null == this.busyIconDefault) {
                this.busyIconDefault = BusyIcon.create(isTabSelected);
            }
            return this.busyIconDefault;
        }

        @Override
        protected void tick() {
            if (null != this.busyIconSelected) {
                this.busyIconSelected.tick();
            }
            if (null != this.busyIconDefault) {
                this.busyIconDefault.tick();
            }
        }

        @Override
        protected int getRepaintTimerIntervalMillis() {
            return 150;
        }
    }

}

