/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.plaf.AbstractViewTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.BusyTabsSupport;
import org.netbeans.swing.tabcontrol.plaf.ColorUtil;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.netbeans.swing.tabcontrol.plaf.WinClassicEditorTabCellRenderer;
import org.openide.awt.HtmlRenderer;

public final class WinClassicViewTabDisplayerUI
extends AbstractViewTabDisplayerUI {
    private static final boolean isGenericUI = !"Windows".equals(UIManager.getLookAndFeel().getID());
    private static final Color GTK_TABBED_PANE_BACKGROUND_1 = new Color(255, 255, 255);
    private static final int BUMP_X_PAD = isGenericUI ? 0 : 3;
    private static final int BUMP_WIDTH = isGenericUI ? 0 : 3;
    private static final int TXT_X_PAD = isGenericUI ? 3 : BUMP_X_PAD + BUMP_WIDTH + 5;
    private static final int TXT_Y_PAD = 3;
    private static final int ICON_X_PAD = 2;
    private static Map<Integer, String[]> buttonIconPaths;
    private Dimension prefSize = new Dimension(100, 19);
    private Rectangle tempRect = new Rectangle();

    private WinClassicViewTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
    }

    public static ComponentUI createUI(JComponent c) {
        return new WinClassicViewTabDisplayerUI((TabDisplayer)c);
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        FontMetrics fm = this.getTxtFontMetrics();
        int height = fm == null ? 19 : fm.getAscent() + 2 * fm.getDescent() + 2;
        Insets insets = c.getInsets();
        this.prefSize.height = height + insets.bottom + insets.top;
        return this.prefSize;
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        ColorUtil.setupAntialiasing(g);
        Color col = c.getBackground();
        if (col != null) {
            g.setColor(col);
            g.fillRect(0, 0, c.getWidth(), c.getHeight());
        }
        this.paintOverallBorder(g, c);
        super.paint(g, c);
    }

    protected void paintOverallBorder(Graphics g, JComponent c) {
        if (isGenericUI) {
            return;
        }
        Rectangle r = c.getBounds();
        g.setColor(UIManager.getColor("InternalFrame.borderDarkShadow"));
        g.drawLine(0, r.height - 1, r.width - 1, r.height - 1);
    }

    @Override
    protected Font getTxtFont() {
        Font result;
        if (isGenericUI && (result = UIManager.getFont("controlFont")) != null) {
            return result;
        }
        return super.getTxtFont();
    }

    @Override
    protected void paintTabContent(Graphics g, int index, String text, int x, int y, int width, int height) {
        --height;
        y -= 2;
        FontMetrics fm = this.getTxtFontMetrics();
        g.setFont(this.getTxtFont());
        int txtWidth = width;
        if (this.isSelected(index)) {
            Component buttons = this.getControlButtons();
            if (null != buttons) {
                Dimension buttonsSize = buttons.getPreferredSize();
                if (width < buttonsSize.width + 2) {
                    buttons.setVisible(false);
                } else {
                    buttons.setVisible(true);
                    txtWidth = width - (buttonsSize.width + 2 + TXT_X_PAD);
                    buttons.setLocation(x + txtWidth + TXT_X_PAD, y + (height - buttonsSize.height) / 2 + 1);
                }
            }
        } else {
            txtWidth = width - 2 * TXT_X_PAD;
        }
        if (this.isUseStretchingTabs()) {
            this.drawBump(g, index, x + 4, y + 6, BUMP_WIDTH, height - 8);
        }
        boolean slidedOut = false;
        WinsysInfoForTabbedContainer winsysInfo = this.displayer.getContainerWinsysInfo();
        if (null != winsysInfo && winsysInfo.isSlidedOutContainer()) {
            slidedOut = false;
        }
        if (this.isTabBusy(index) && !slidedOut) {
            Icon busyIcon = BusyTabsSupport.getDefault().getBusyIcon(this.isSelected(index));
            txtWidth -= busyIcon.getIconWidth() + 3 + TXT_X_PAD;
            busyIcon.paintIcon(this.displayer, g, x + TXT_X_PAD, y + (height - busyIcon.getIconHeight()) / 2 + 1);
            x += busyIcon.getIconWidth() + 3;
        }
        Color txtC = UIManager.getColor("TabbedPane.foreground");
        HtmlRenderer.renderString((String)text, (Graphics)g, (int)(x + TXT_X_PAD), (int)(y + fm.getAscent() + 3), (int)txtWidth, (int)height, (Font)this.getTxtFont(), (Color)txtC, (int)1, (boolean)true);
    }

    @Override
    protected void paintTabBorder(Graphics g, int index, int x, int y, int width, int height) {
        boolean isSelected = this.isSelected(index);
        g.translate(x, y);
        g.setColor(UIManager.getColor("InternalFrame.borderShadow"));
        g.drawLine(0, height - 1, width - 2, --height - 1);
        g.drawLine(width - 1, height - 1, width - 1, 0);
        g.setColor(isSelected ? UIManager.getColor("InternalFrame.borderHighlight") : UIManager.getColor("InternalFrame.borderLight"));
        g.drawLine(0, 0, 0, height - 1);
        g.drawLine(1, 0, width - 2, 0);
        g.translate(- x, - y);
    }

    @Override
    protected void paintTabBackground(Graphics g, int index, int x, int y, int width, int height) {
        ((Graphics2D)g).setPaint(this.getBackgroundPaint(g, index, x, y, width, --height));
        if (this.isFocused(index)) {
            g.fillRect(x, y, width, height);
        } else {
            g.fillRect(x + 1, y + 1, width - 2, height - 2);
        }
    }

    private Paint getBackgroundPaint(Graphics g, int index, int x, int y, int width, int height) {
        boolean selected = this.isSelected(index);
        boolean focused = this.isFocused(index);
        boolean attention = this.isAttention(index);
        Paint result = null;
        result = focused && !attention ? ColorUtil.getGradientPaint(x, y, WinClassicViewTabDisplayerUI.getSelGradientColor(), x + width, y, WinClassicViewTabDisplayerUI.getSelGradientColor2()) : (selected && !attention ? UIManager.getColor("TabbedPane.background") : (attention ? WinClassicEditorTabCellRenderer.ATTENTION_COLOR : UIManager.getColor("tab_unsel_fill")));
        return result;
    }

    private void drawBump(Graphics g, int index, int x, int y, int width, int height) {
        Color shadowC;
        Color bodyC;
        Color highlightC;
        if (isGenericUI) {
            return;
        }
        if (this.isFocused(index)) {
            bodyC = new Color(210, 220, 243);
            highlightC = bodyC.brighter();
            shadowC = bodyC.darker();
        } else if (this.isSelected(index)) {
            highlightC = UIManager.getColor("InternalFrame.borderHighlight");
            bodyC = UIManager.getColor("InternalFrame.borderLight");
            shadowC = UIManager.getColor("InternalFrame.borderShadow");
        } else {
            highlightC = UIManager.getColor("InternalFrame.borderLight");
            bodyC = UIManager.getColor("tab_unsel_fill");
            shadowC = UIManager.getColor("InternalFrame.borderShadow");
        }
        int i = 0;
        while (i < width / 3) {
            g.setColor(highlightC);
            g.drawLine(x, y, x, y + height - 1);
            g.drawLine(x, y, x + 1, y);
            g.setColor(bodyC);
            g.drawLine(x + 1, y + 1, x + 1, y + height - 2);
            g.setColor(shadowC);
            g.drawLine(x + 2, y, x + 2, y + height - 1);
            g.drawLine(x, y + height - 1, x + 1, y + height - 1);
            ++i;
            x += 3;
        }
    }

    private static Color getSelGradientColor() {
        if ("GTK".equals(UIManager.getLookAndFeel().getID())) {
            return GTK_TABBED_PANE_BACKGROUND_1;
        }
        return UIManager.getColor("winclassic_tab_sel_gradient");
    }

    private static Color getSelGradientColor2() {
        return UIManager.getColor("TabbedPane.background");
    }

    private static void initIcons() {
        if (null == buttonIconPaths) {
            buttonIconPaths = new HashMap<Integer, String[]>(7);
            String[] iconPaths = new String[]{"org/openide/awt/resources/win_bigclose_enabled.png", "org/openide/awt/resources/win_bigclose_pressed.png", "org/openide/awt/resources/win_bigclose_disabled.png", "org/openide/awt/resources/win_bigclose_rollover.png"};
            buttonIconPaths.put(1, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/win_slideright_enabled.png", "org/netbeans/swing/tabcontrol/resources/win_slideright_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/win_slideright_rollover.png"};
            buttonIconPaths.put(6, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/win_slideleft_enabled.png", "org/netbeans/swing/tabcontrol/resources/win_slideleft_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/win_slideleft_rollover.png"};
            buttonIconPaths.put(5, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/win_slidebottom_enabled.png", "org/netbeans/swing/tabcontrol/resources/win_slidebottom_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/win_slidebottom_rollover.png"};
            buttonIconPaths.put(7, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/win_pin_enabled.png", "org/netbeans/swing/tabcontrol/resources/win_pin_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/win_pin_rollover.png"};
            buttonIconPaths.put(2, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/win_restore_group_enabled.png", "org/netbeans/swing/tabcontrol/resources/win_restore_group_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/win_restore_group_rollover.png"};
            buttonIconPaths.put(11, iconPaths);
            iconPaths = new String[]{"org/netbeans/swing/tabcontrol/resources/win_minimize_enabled.png", "org/netbeans/swing/tabcontrol/resources/win_minimize_pressed.png", iconPaths[0], "org/netbeans/swing/tabcontrol/resources/win_minimize_rollover.png"};
            buttonIconPaths.put(12, iconPaths);
        }
    }

    @Override
    public Icon getButtonIcon(int buttonId, int buttonState) {
        Icon res = null;
        WinClassicViewTabDisplayerUI.initIcons();
        String[] paths = buttonIconPaths.get(buttonId);
        if (null != paths && buttonState >= 0 && buttonState < paths.length) {
            res = TabControlButtonFactory.getIcon(paths[buttonState]);
        }
        return res;
    }
}

