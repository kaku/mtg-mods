/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Comparator;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JToggleButton;
import javax.swing.SingleSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ButtonUI;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.plaf.AbstractTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.DefaultTabLayoutModel;
import org.netbeans.swing.tabcontrol.plaf.EqualPolygon;
import org.netbeans.swing.tabcontrol.plaf.SlidingTabDisplayerButtonUI;
import org.netbeans.swing.tabcontrol.plaf.TabLayoutModel;

public final class BasicSlidingTabDisplayerUI
extends AbstractTabDisplayerUI {
    private Rectangle scratch = new Rectangle();
    private int buttonCount = 0;
    private static final Comparator<Component> BUTTON_COMPARATOR = new IndexButtonComparator();

    public BasicSlidingTabDisplayerUI(TabDisplayer displayer) {
        super(displayer);
    }

    public static ComponentUI createUI(JComponent c) {
        return new BasicSlidingTabDisplayerUI((TabDisplayer)c);
    }

    @Override
    protected void install() {
        this.displayer.setLayout(new OrientedLayoutManager());
        this.syncButtonsWithModel();
    }

    @Override
    protected Font createFont() {
        Font f = super.createFont();
        f = new Font(f.getName(), 1, f.getSize() + 1);
        return f;
    }

    @Override
    protected void uninstall() {
        this.displayer.removeAll();
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        return this.displayer.getLayout().preferredLayoutSize(c);
    }

    @Override
    public Dimension getMinimumSize(JComponent c) {
        return this.displayer.getLayout().minimumLayoutSize(c);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean syncButtonsWithModel() {
        assert (SwingUtilities.isEventDispatchThread());
        int count = this.displayer.getModel().size();
        boolean changed = false;
        this.buttonCount = this.displayer.getComponentCount();
        if (count != this.buttonCount) {
            Object object = this.displayer.getTreeLock();
            synchronized (object) {
                while (count < this.buttonCount) {
                    if (this.buttonCount-- <= 0) continue;
                    this.displayer.remove(this.buttonCount - 1);
                    changed = true;
                }
                while (count > this.buttonCount) {
                    IndexButton ib = new IndexButton(this.buttonCount++);
                    ib.setFont(this.displayer.getFont());
                    this.displayer.add(ib);
                    changed = true;
                }
                Component[] c = this.displayer.getComponents();
                for (int i = 0; i < c.length; ++i) {
                    if (!(c[i] instanceof IndexButton)) continue;
                    changed |= ((IndexButton)c[i]).checkChanged();
                }
            }
        }
        return changed;
    }

    @Override
    protected TabLayoutModel createLayoutModel() {
        DefaultTabLayoutModel result = new DefaultTabLayoutModel(this.displayer.getModel(), this.displayer);
        result.setPadding(new Dimension(15, 2));
        return result;
    }

    @Override
    protected MouseListener createMouseListener() {
        return new MouseAdapter(){};
    }

    @Override
    public void requestAttention(int tab) {
    }

    @Override
    public void cancelRequestAttention(int tab) {
    }

    @Override
    protected ChangeListener createSelectionListener() {
        return new ChangeListener(){
            private int lastKnownSelection;

            @Override
            public void stateChanged(ChangeEvent ce) {
                int selection = BasicSlidingTabDisplayerUI.this.selectionModel.getSelectedIndex();
                if (selection != this.lastKnownSelection) {
                    IndexButton last;
                    if (this.lastKnownSelection != -1 && (last = BasicSlidingTabDisplayerUI.this.findButtonFor(this.lastKnownSelection)) != null) {
                        last.getModel().setSelected(false);
                    }
                    if (selection != -1) {
                        IndexButton current = BasicSlidingTabDisplayerUI.this.findButtonFor(selection);
                        if (BasicSlidingTabDisplayerUI.this.displayer.getComponentCount() == 0) {
                            BasicSlidingTabDisplayerUI.this.syncButtonsWithModel();
                        }
                        if (current != null) {
                            current.getModel().setSelected(true);
                        }
                    }
                }
                this.lastKnownSelection = selection;
            }
        };
    }

    @Override
    public Polygon getExactTabIndication(int index) {
        return new EqualPolygon(this.findButtonFor(index).getBounds());
    }

    @Override
    public Polygon getInsertTabIndication(int index) {
        Rectangle r = this.findButtonFor(index).getBounds();
        EqualPolygon result = new EqualPolygon(this.findButtonFor(index).getBounds());
        return result;
    }

    private IndexButton findButtonFor(int index) {
        Component[] c = this.displayer.getComponents();
        for (int i = 0; i < c.length; ++i) {
            if (!(c[i] instanceof IndexButton) || ((IndexButton)c[i]).getIndex() != index) continue;
            return (IndexButton)c[i];
        }
        return null;
    }

    @Override
    public Rectangle getTabRect(int index, Rectangle destination) {
        IndexButton ib;
        if (destination == null) {
            destination = new Rectangle();
        }
        if ((ib = this.findButtonFor(index)) != null) {
            destination.setBounds(ib.getBounds());
        } else {
            destination.setBounds(-20, -20, 0, 0);
        }
        return destination;
    }

    @Override
    public int tabForCoordinate(Point p) {
        Component[] c = this.displayer.getComponents();
        for (int i = 0; i < c.length; ++i) {
            if (!(c[i] instanceof IndexButton) || !c[i].contains(p)) continue;
            return ((IndexButton)c[i]).getIndex();
        }
        return -1;
    }

    private Object getDisplayerOrientation() {
        return this.displayer.getClientProperty("orientation");
    }

    @Override
    public Image createImageOfTab(int index) {
        TabData td = this.displayer.getModel().getTab(index);
        JLabel lbl = new JLabel(td.getText());
        int width = lbl.getFontMetrics(lbl.getFont()).stringWidth(td.getText());
        int height = lbl.getFontMetrics(lbl.getFont()).getHeight();
        width = width + td.getIcon().getIconWidth() + 6;
        height = Math.max(height, td.getIcon().getIconHeight()) + 5;
        GraphicsConfiguration config = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
        BufferedImage image = config.createCompatibleImage(width, height);
        Graphics2D g = image.createGraphics();
        g.setColor(lbl.getForeground());
        g.setFont(lbl.getFont());
        td.getIcon().paintIcon(lbl, g, 0, 0);
        g.drawString(td.getText(), 18, height / 2);
        return image;
    }

    @Override
    protected void modelChanged() {
        if (this.syncButtonsWithModel()) {
            this.displayer.validate();
        }
    }

    @Override
    public Icon getButtonIcon(int buttonId, int buttonState) {
        return null;
    }

    private final class OrientedLayoutManager
    implements LayoutManager {
        private OrientedLayoutManager() {
        }

        @Override
        public void addLayoutComponent(String name, Component comp) {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void layoutContainer(Container parent) {
            Object object = parent.getTreeLock();
            synchronized (object) {
                BasicSlidingTabDisplayerUI.this.syncButtonsWithModel();
                Component[] c = parent.getComponents();
                Arrays.sort(c, BUTTON_COMPARATOR);
                for (int i = 0; i < c.length; ++i) {
                    if (!(c[i] instanceof IndexButton)) continue;
                    this.boundsFor((IndexButton)c[i], BasicSlidingTabDisplayerUI.this.scratch);
                    c[i].setBounds(BasicSlidingTabDisplayerUI.this.scratch);
                }
            }
        }

        private void boundsFor(IndexButton b, Rectangle r) {
            Object orientation = BasicSlidingTabDisplayerUI.this.getDisplayerOrientation();
            boolean flip = orientation == TabDisplayer.ORIENTATION_EAST || orientation == TabDisplayer.ORIENTATION_WEST;
            int index = b.getIndex();
            if (index >= BasicSlidingTabDisplayerUI.this.displayer.getModel().size() || index < 0) {
                r.setBounds(-20, -20, 0, 0);
                return;
            }
            r.x = BasicSlidingTabDisplayerUI.this.layoutModel.getX(index);
            r.y = BasicSlidingTabDisplayerUI.this.layoutModel.getY(index);
            r.width = BasicSlidingTabDisplayerUI.this.layoutModel.getW(index);
            r.height = BasicSlidingTabDisplayerUI.this.layoutModel.getH(index);
            if (flip) {
                int tmp = r.x;
                r.x = r.y;
                r.y = tmp;
                tmp = r.width;
                r.width = r.height;
                r.height = tmp;
            }
        }

        @Override
        public Dimension minimumLayoutSize(Container parent) {
            return this.preferredLayoutSize(parent);
        }

        @Override
        public Dimension preferredLayoutSize(Container parent) {
            Object orientation = BasicSlidingTabDisplayerUI.this.getDisplayerOrientation();
            boolean flip = orientation == TabDisplayer.ORIENTATION_EAST || orientation == TabDisplayer.ORIENTATION_WEST;
            int max = BasicSlidingTabDisplayerUI.this.displayer.getModel().size();
            Dimension result = new Dimension();
            for (int i = 0; i < max; ++i) {
                result.height = Math.max(result.height, BasicSlidingTabDisplayerUI.this.layoutModel.getH(i));
                result.width += BasicSlidingTabDisplayerUI.this.layoutModel.getW(i);
            }
            if (flip) {
                int tmp = result.height;
                result.height = result.width;
                result.width = tmp;
            }
            return result;
        }

        @Override
        public void removeLayoutComponent(Component comp) {
        }
    }

    private static class IndexButtonComparator
    implements Comparator<Component> {
        private IndexButtonComparator() {
        }

        @Override
        public int compare(Component o1, Component o2) {
            if (o2 instanceof IndexButton && o1 instanceof IndexButton) {
                return ((IndexButton)o1).getIndex() - ((IndexButton)o2).getIndex();
            }
            return 0;
        }
    }

    public final class IndexButton
    extends JToggleButton
    implements ActionListener {
        private int index;
        private String lastKnownText;
        private Icon lastKnownIcon;
        public static final String UI_KEY = "IndexButtonUI";

        public IndexButton(int index) {
            this.lastKnownText = null;
            this.lastKnownIcon = null;
            this.index = index;
            this.addActionListener(this);
            this.setFont(BasicSlidingTabDisplayerUI.this.displayer.getFont());
            this.setFocusable(false);
        }

        @Override
        public void addNotify() {
            super.addNotify();
            ToolTipManager.sharedInstance().registerComponent(this);
        }

        @Override
        public void removeNotify() {
            super.removeNotify();
            ToolTipManager.sharedInstance().unregisterComponent(this);
        }

        public boolean isActive() {
            return BasicSlidingTabDisplayerUI.this.displayer.isActive();
        }

        @Override
        public void updateUI() {
            SlidingTabDisplayerButtonUI ui = null;
            try {
                ui = (SlidingTabDisplayerButtonUI)UIManager.getUI(this);
                this.setUI(ui);
                return;
            }
            catch (Error e) {
                System.err.println("Error getting sliding button UI: " + e.getMessage());
            }
            catch (Exception ex) {
                System.err.println("Exception getting button UI: " + ex.getMessage());
            }
            this.setUI((ButtonUI)SlidingTabDisplayerButtonUI.createUI(this));
        }

        @Override
        public String getUIClassID() {
            return "IndexButtonUI";
        }

        public Object getOrientation() {
            return BasicSlidingTabDisplayerUI.this.getDisplayerOrientation();
        }

        @Override
        public String getText() {
            if (this.index == -1) {
                return "";
            }
            if (this.index >= BasicSlidingTabDisplayerUI.this.displayer.getModel().size()) {
                return "This tab doesn't exist.";
            }
            this.lastKnownText = BasicSlidingTabDisplayerUI.this.displayer.getModel().getTab(this.index).getText();
            return this.lastKnownText;
        }

        @Override
        public String getToolTipText() {
            return BasicSlidingTabDisplayerUI.this.displayer.getModel().getTab(this.index).getTooltip();
        }

        @Override
        public final void actionPerformed(ActionEvent e) {
            if (!this.isSelected()) {
                BasicSlidingTabDisplayerUI.this.selectionModel.setSelectedIndex(-1);
            } else {
                BasicSlidingTabDisplayerUI.this.selectionModel.setSelectedIndex(this.index);
            }
        }

        public int getIndex() {
            return this.index;
        }

        @Override
        public Icon getIcon() {
            if (this.index == -1) {
                return null;
            }
            if (this.index < BasicSlidingTabDisplayerUI.this.displayer.getModel().size()) {
                this.lastKnownIcon = BasicSlidingTabDisplayerUI.this.displayer.getModel().getTab(this.index).getIcon();
            }
            return this.lastKnownIcon;
        }

        final boolean checkChanged() {
            boolean result = false;
            Icon ic = this.lastKnownIcon;
            Icon nue = this.getIcon();
            if (nue != ic) {
                this.firePropertyChange("icon", this.lastKnownIcon, nue);
                result = true;
            }
            String txt = this.lastKnownText;
            String nu = this.getText();
            if (nu != txt) {
                this.firePropertyChange("text", this.lastKnownText, this.getText());
                result = true;
            }
            if (result) {
                this.firePropertyChange("preferredSize", null, null);
            }
            return result;
        }
    }

    protected final class SlidingPropertyChangeListener
    extends AbstractTabDisplayerUI.DisplayerPropertyChangeListener {
        protected SlidingPropertyChangeListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent e) {
            super.propertyChange(e);
            if ("orientation".equals(e.getPropertyName())) {
                BasicSlidingTabDisplayerUI.this.displayer.revalidate();
            }
        }
    }

}

