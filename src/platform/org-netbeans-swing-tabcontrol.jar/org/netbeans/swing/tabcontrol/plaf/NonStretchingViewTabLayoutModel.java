/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 */
package org.netbeans.swing.tabcontrol.plaf;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.Icon;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.TabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.BasicScrollingTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.TabLayoutModel;
import org.openide.awt.HtmlRenderer;

final class NonStretchingViewTabLayoutModel
implements TabLayoutModel {
    private TabDataModel model;
    private TabDisplayer tabDisplayer;
    private static final int PADDING_RIGHT = 35;
    private static final int ICON_X_PAD = 4;
    private Dimension padding = new Dimension(10, 0);

    public NonStretchingViewTabLayoutModel(TabDataModel model, TabDisplayer tabDisplayer) {
        this.model = model;
        this.tabDisplayer = tabDisplayer;
    }

    @Override
    public int getH(int index) {
        this.checkIndex(index);
        Insets insets = this.tabDisplayer.getInsets();
        return this.tabDisplayer.getHeight() - (insets.bottom + insets.top);
    }

    @Override
    public int getW(int index) {
        this.checkIndex(index);
        return this.getXCoords()[index] - this.getX(index);
    }

    @Override
    public int getX(int index) {
        this.checkIndex(index);
        return index > 0 ? this.getXCoords()[index - 1] : this.tabDisplayer.getInsets().left;
    }

    @Override
    public int getY(int index) {
        this.checkIndex(index);
        return this.tabDisplayer.getInsets().top;
    }

    @Override
    public int indexOfPoint(int x, int y) {
        Insets insets = this.tabDisplayer.getInsets();
        int contentWidth = this.tabDisplayer.getWidth() - (insets.left + insets.right + 35);
        int contentHeight = this.tabDisplayer.getHeight() - (insets.bottom + insets.top);
        if (y < insets.top || y > contentHeight || x < insets.left || x > contentWidth) {
            return -1;
        }
        int size = this.model.size();
        for (int i = 0; i < size; ++i) {
            int diff = x - this.getX(i);
            if (diff < 0 || diff >= this.getW(i)) continue;
            return i;
        }
        return -1;
    }

    @Override
    public int dropIndexOfPoint(int x, int y) {
        Insets insets = this.tabDisplayer.getInsets();
        int contentWidth = this.tabDisplayer.getWidth() - (insets.left + insets.right + 35);
        int contentHeight = this.tabDisplayer.getHeight() - (insets.bottom + insets.top);
        if (y < insets.top || y > contentHeight || x < insets.left || x > contentWidth) {
            return -1;
        }
        int size = this.model.size();
        int[] coords = this.getXCoords();
        for (int i = 0; i < coords.length; ++i) {
            if (x >= coords[i]) continue;
            return i;
        }
        return -1;
    }

    @Override
    public void setPadding(Dimension d) {
        this.padding = new Dimension(d);
    }

    private void checkIndex(int index) {
        int size = this.model.size();
        if (index < 0 || index >= size) {
            throw new IllegalArgumentException("Index out of valid scope 0.." + (size - 1) + ": " + index);
        }
    }

    private int[] getXCoords() {
        int size = this.model.size();
        int[] tabsXCoord = new int[size];
        Insets dispInsets = this.tabDisplayer.getInsets();
        double curX = dispInsets.left;
        int maxRight = this.tabDisplayer.getWidth() - dispInsets.right - 35;
        if (maxRight - dispInsets.left / size < 5) {
            NonStretchingViewTabLayoutModel.makeEqualSized(tabsXCoord, dispInsets.left, maxRight);
            return tabsXCoord;
        }
        Icon buttonIcon = this.tabDisplayer.getUI().getButtonIcon(1, 0);
        int iconPadding = 0;
        if (null != buttonIcon) {
            iconPadding += buttonIcon.getIconWidth();
            iconPadding += 4;
        }
        int maxTabWidth = 0;
        for (int i = 0; i < size; ++i) {
            int tabIndex = i;
            String curText = this.model.getTab(tabIndex).getText();
            curX += HtmlRenderer.renderString((String)curText, (Graphics)BasicScrollingTabDisplayerUI.getOffscreenGraphics(), (int)0, (int)0, (int)Integer.MAX_VALUE, (int)Integer.MAX_VALUE, (Font)this.tabDisplayer.getFont(), (Color)Color.BLACK, (int)1, (boolean)false);
            curX += (double)this.padding.width;
            tabsXCoord[i] = (int)Math.round(curX += (double)iconPadding);
            int tabWidth = i == 0 ? tabsXCoord[i] : tabsXCoord[i] - tabsXCoord[i - 1];
            maxTabWidth = Math.max(maxTabWidth, tabWidth);
        }
        if (tabsXCoord[tabsXCoord.length - 1] > maxRight) {
            int secondMaxTabWidth = NonStretchingViewTabLayoutModel.findSecondMaxWidth(tabsXCoord, maxTabWidth);
            int widthToDistribute = tabsXCoord[tabsXCoord.length - 1] - maxRight;
            NonStretchingViewTabLayoutModel.makeShorter(tabsXCoord, secondMaxTabWidth, maxTabWidth, widthToDistribute);
            if (tabsXCoord[tabsXCoord.length - 1] > maxRight) {
                maxTabWidth = secondMaxTabWidth;
                secondMaxTabWidth = NonStretchingViewTabLayoutModel.findSecondMaxWidth(tabsXCoord, secondMaxTabWidth);
                widthToDistribute = tabsXCoord[tabsXCoord.length - 1] - maxRight;
                NonStretchingViewTabLayoutModel.makeShorter(tabsXCoord, secondMaxTabWidth, maxTabWidth, widthToDistribute);
                if (tabsXCoord[tabsXCoord.length - 1] > maxRight) {
                    NonStretchingViewTabLayoutModel.makeEqualSized(tabsXCoord, dispInsets.left, maxRight);
                }
            }
        }
        return tabsXCoord;
    }

    private static void makeEqualSized(int[] coords, int leftInsets, int maxRight) {
        int tabWidth = Math.max((maxRight - leftInsets) / coords.length, 1);
        coords[0] = leftInsets + tabWidth;
        for (int i = 1; i < coords.length; ++i) {
            coords[i] = coords[i - 1] + tabWidth;
        }
    }

    private static void makeShorter(int[] coords, int tabsWiderThan, int maxTabWidth, int widthToDistribute) {
        int tabCount = NonStretchingViewTabLayoutModel.countTabsWiderThan(coords, tabsWiderThan);
        if (0 == tabCount) {
            return;
        }
        int total = 0;
        int delta = Math.max(widthToDistribute / tabCount, 1);
        for (int i = coords.length - 1; i >= 0; --i) {
            int tabWidth = coords[i];
            if (i > 0) {
                tabWidth -= coords[i - 1];
            }
            if (tabWidth < tabsWiderThan) continue;
            int currentDelta = Math.min(tabWidth - tabsWiderThan, delta);
            int j = i;
            while (j < coords.length) {
                int[] arrn = coords;
                int n = j++;
                arrn[n] = arrn[n] - currentDelta;
            }
            if ((total += currentDelta) >= widthToDistribute) break;
        }
    }

    private static void makeShorterEqually(int[] coords, int widthToDistribute) {
        int delta = Math.max(widthToDistribute / coords.length, 1);
        int total = 0;
        for (int i = coords.length - 1; i >= 0; --i) {
            int tabWidth = coords[i];
            if (i > 0) {
                tabWidth -= coords[i - 1];
            }
            if (tabWidth < 5) continue;
            int currentDelta = Math.min(tabWidth - 5, delta);
            int j = i;
            while (j < coords.length) {
                int[] arrn = coords;
                int n = j++;
                arrn[n] = arrn[n] - currentDelta;
            }
            if ((total += currentDelta) >= widthToDistribute) break;
        }
    }

    private static int findSecondMaxWidth(int[] coords, int maxTabWidth) {
        int res = 0;
        for (int i = 0; i < coords.length; ++i) {
            int tabWidth;
            int n = tabWidth = i == 0 ? coords[i] : coords[i] - coords[i - 1];
            if (tabWidth <= res || tabWidth >= maxTabWidth) continue;
            res = tabWidth;
        }
        return res;
    }

    private static int countTabsWiderThan(int[] coords, int width) {
        int res = 0;
        for (int i = 0; i < coords.length; ++i) {
            int tabWidth;
            int n = tabWidth = i == 0 ? coords[i] : coords[i] - coords[i - 1];
            if (tabWidth <= width) continue;
            ++res;
        }
        return res;
    }
}

