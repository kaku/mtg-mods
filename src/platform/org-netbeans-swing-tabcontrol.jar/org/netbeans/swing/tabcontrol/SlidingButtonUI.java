/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicToggleButtonUI;
import javax.swing.text.View;
import org.netbeans.swing.tabcontrol.SlidingButton;

public class SlidingButtonUI
extends BasicToggleButtonUI {
    private static final BasicToggleButtonUI INSTANCE = new SlidingButtonUI();

    protected SlidingButtonUI() {
    }

    public static ComponentUI createUI(JComponent c) {
        return INSTANCE;
    }

    @Override
    protected String getPropertyPrefix() {
        return "ToggleButton.";
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        AbstractButton b = (AbstractButton)c;
        ButtonModel model = b.getModel();
        Dimension size = b.getSize();
        Font f = c.getFont();
        g.setFont(f);
        FontMetrics fm = g.getFontMetrics();
        Insets i = c.getInsets();
        Rectangle viewRect = new Rectangle(size);
        Rectangle iconRect = new Rectangle();
        Rectangle textRect = new Rectangle();
        Rectangle rotatedIconRect = new Rectangle();
        Rectangle rotatedTextRect = new Rectangle();
        SlidingButton slide = (SlidingButton)b;
        Graphics2D g2d = (Graphics2D)g;
        int orientation = slide.getOrientation();
        Rectangle rotatedViewRect = orientation == 3 || orientation == 4 ? new Rectangle(0, 0, viewRect.width, viewRect.height) : new Rectangle(0, 0, viewRect.height, viewRect.width);
        String text = SwingUtilities.layoutCompoundLabel(c, fm, b.getText(), b.getIcon(), b.getVerticalAlignment(), b.getHorizontalAlignment(), b.getVerticalTextPosition(), b.getHorizontalTextPosition(), rotatedViewRect, rotatedIconRect, rotatedTextRect, b.getText() == null ? 0 : b.getIconTextGap());
        if (orientation == 3 || orientation == 4) {
            iconRect = new Rectangle(viewRect.x + rotatedIconRect.x, viewRect.y + rotatedIconRect.y, rotatedIconRect.width, rotatedIconRect.height);
            textRect = new Rectangle(viewRect.x + rotatedTextRect.x, viewRect.y + rotatedTextRect.y, rotatedTextRect.width, rotatedTextRect.height);
        }
        if (orientation == 2) {
            iconRect = new Rectangle(viewRect.x + rotatedIconRect.y, viewRect.y + viewRect.height - rotatedIconRect.x - rotatedIconRect.width, rotatedIconRect.height, rotatedIconRect.width);
            textRect = new Rectangle(viewRect.x + rotatedTextRect.y, viewRect.y + viewRect.height - rotatedTextRect.y - rotatedTextRect.width, rotatedTextRect.height, rotatedTextRect.width);
        }
        if (orientation == 1) {
            iconRect = new Rectangle(viewRect.x + viewRect.width - rotatedIconRect.y - rotatedIconRect.height, viewRect.y + rotatedIconRect.x, rotatedIconRect.height, rotatedIconRect.width);
            textRect = new Rectangle(viewRect.x + viewRect.width - rotatedTextRect.y - rotatedTextRect.height, viewRect.y + rotatedTextRect.x, rotatedTextRect.height, rotatedTextRect.width);
        }
        g.setColor(b.getBackground());
        if (model.isArmed() && model.isPressed() || model.isSelected() || model.isRollover()) {
            this.paintButtonPressed(g, b);
        } else if (b.isOpaque()) {
            this.paintBackground(g2d, b);
        }
        if (b.getIcon() != null) {
            this.paintIcon(g, b, iconRect);
        }
        if (text != null && !text.equals("")) {
            AffineTransform saveTr = g2d.getTransform();
            if (orientation == 2) {
                g2d.rotate(-1.5707963267948966);
                g2d.translate(- c.getHeight(), 0);
            } else if (orientation == 1) {
                g2d.rotate(1.5707963267948966);
                g2d.translate(0, - c.getWidth());
            }
            View v = (View)c.getClientProperty("html");
            if (v != null) {
                v.paint(g, rotatedTextRect);
            } else {
                this.paintText(g, b, rotatedTextRect, text);
            }
            g2d.setTransform(saveTr);
        }
        if (b.isFocusPainted() && b.hasFocus()) {
            this.paintFocus(g, b, viewRect, textRect, iconRect);
        }
    }

    protected void paintBackground(Graphics2D g, AbstractButton b) {
        Dimension size = b.getSize();
        Insets insets = b.getInsets();
        Insets margin = b.getMargin();
        if (null == insets || null == margin) {
            return;
        }
        g.fillRect(insets.left - margin.left, insets.top - margin.top, size.width - (insets.left - margin.left) - (insets.right - margin.right), size.height - (insets.top - margin.top) - (insets.bottom - margin.bottom));
    }

    @Override
    public Dimension getMinimumSize(JComponent c) {
        return this.getPreferredSize(c);
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        SlidingButton b = (SlidingButton)c;
        Dimension prefSize = super.getPreferredSize(c);
        int orientation = b.getOrientation();
        if (orientation == 3 || orientation == 4) {
            return prefSize;
        }
        return new Dimension(prefSize.height, prefSize.width);
    }

    @Override
    public Dimension getMaximumSize(JComponent c) {
        return this.getPreferredSize(c);
    }
}

