/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.swing.tabcontrol;

import java.awt.AWTEvent;
import java.awt.AlphaComposite;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleRole;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.SingleSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.ComponentConverter;
import org.netbeans.swing.tabcontrol.DefaultTabDataModel;
import org.netbeans.swing.tabcontrol.LocationInformer;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabbedContainerUI;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbed;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.event.TabActionEvent;
import org.netbeans.swing.tabcontrol.plaf.DefaultTabbedContainerUI;
import org.openide.util.NbBundle;

public class TabbedContainer
extends JComponent
implements Accessible {
    public static final String TABBED_CONTAINER_UI_CLASS_ID = "TabbedContainerUI";
    public static final int TYPE_VIEW = 0;
    public static final int TYPE_EDITOR = 1;
    public static final int TYPE_SLIDING = 2;
    public static final int TYPE_TOOLBAR = 3;
    public static final String PROP_ACTIVE = "active";
    public static final String PROP_MANAGE_TAB_POSITION = "manageTabPosition";
    public static final String COMMAND_CLOSE = "close";
    public static final String COMMAND_SELECT = "select";
    public static final String COMMAND_POPUP_REQUEST = "popup";
    public static final String COMMAND_MAXIMIZE = "maximize";
    public static final String COMMAND_CLOSE_ALL = "closeAll";
    public static final String COMMAND_CLOSE_ALL_BUT_THIS = "closeAllButThis";
    public static final String COMMAND_ENABLE_AUTO_HIDE = "enableAutoHide";
    public static final String COMMAND_DISABLE_AUTO_HIDE = "disableAutoHide";
    public static final String COMMAND_TOGGLE_TRANSPARENCY = "toggleTransparency";
    public static final String COMMAND_MINIMIZE_GROUP = "minimizeGroup";
    public static final String COMMAND_RESTORE_GROUP = "restoreGroup";
    public static final String COMMAND_CLOSE_GROUP = "closeGroup";
    private TabDataModel model;
    private final int type;
    private boolean active = false;
    private boolean initialized = false;
    private transient List<ActionListener> actionListenerList;
    public static final int CONTENT_POLICY_ADD_ALL = 1;
    public static final int CONTENT_POLICY_ADD_ON_FIRST_USE = 2;
    public static final int CONTENT_POLICY_ADD_ONLY_SELECTED = 3;
    private int contentPolicy = DEFAULT_CONTENT_POLICY;
    protected static int DEFAULT_CONTENT_POLICY = 1;
    private ComponentConverter converter = null;
    private WinsysInfoForTabbed winsysInfo = null;
    private WinsysInfoForTabbedContainer containerWinsysInfo = null;
    @Deprecated
    private LocationInformer locationInformer = null;
    private static final float ALPHA_TRESHOLD = 0.1f;
    private float currentAlpha = 1.0f;
    private boolean inTransparentMode = false;
    AWTEventListener awtListener = null;

    public TabbedContainer() {
        this(null, 0);
    }

    public TabbedContainer(TabDataModel model) {
        this(model, 0);
    }

    public TabbedContainer(int type) {
        this(null, type);
    }

    public TabbedContainer(TabDataModel model, int type) {
        this(model, type, (WinsysInfoForTabbed)null);
    }

    @Deprecated
    public TabbedContainer(TabDataModel model, int type, LocationInformer locationInformer) {
        this(model, type, (WinsysInfoForTabbed)null);
        this.locationInformer = locationInformer;
    }

    @Deprecated
    public TabbedContainer(TabDataModel model, int type, WinsysInfoForTabbed winsysInfo) {
        this(model, type, WinsysInfoForTabbedContainer.getDefault(winsysInfo));
    }

    public TabbedContainer(TabDataModel model, int type, WinsysInfoForTabbedContainer winsysInfo) {
        switch (type) {
            case 0: 
            case 1: 
            case 2: 
            case 3: {
                break;
            }
            default: {
                throw new IllegalArgumentException("Unknown UI type: " + type);
            }
        }
        if (model == null) {
            model = new DefaultTabDataModel();
        }
        this.model = model;
        this.type = Boolean.getBoolean("nb.tabcontrol.alltoolbar") ? 3 : type;
        this.winsysInfo = winsysInfo;
        this.containerWinsysInfo = winsysInfo;
        this.initialized = true;
        this.updateUI();
        this.putClientProperty("viewType", new Integer(type));
    }

    @Override
    public void updateUI() {
        if (!this.initialized) {
            return;
        }
        TabbedContainerUI ui = null;
        String UIClass = (String)UIManager.get(this.getUIClassID());
        if (this.getUI() != null && this.getUI().getClass().getName().equals(UIClass) | UIClass == null && !this.getUI().shouldReplaceUI()) {
            return;
        }
        if (UIClass != null) {
            try {
                ui = (TabbedContainerUI)UIManager.getUI(this);
            }
            catch (Error e) {
                // empty catch block
            }
        }
        if (ui != null) {
            this.setUI(ui);
        } else {
            this.setUI(DefaultTabbedContainerUI.createUI(this));
        }
    }

    public final int getType() {
        return this.type;
    }

    @Override
    public String getUIClassID() {
        return "TabbedContainerUI";
    }

    public TabbedContainerUI getUI() {
        return (TabbedContainerUI)this.ui;
    }

    public final void setComponentConverter(ComponentConverter cc) {
        List<TabData> l;
        ComponentConverter old = this.converter;
        this.converter = cc;
        if (old instanceof ComponentConverter.Fixed && cc instanceof ComponentConverter.Fixed && !(l = this.getModel().getTabs()).isEmpty()) {
            TabData[] td = l.toArray(new TabData[0]);
            this.getModel().setTabs(new TabData[0]);
            this.getModel().setTabs(td);
        }
    }

    public final ComponentConverter getComponentConverter() {
        if (this.converter != null) {
            return this.converter;
        }
        return ComponentConverter.DEFAULT;
    }

    public final void setContentPolicy(int i) {
        switch (i) {
            case 1: 
            case 2: 
            case 3: {
                break;
            }
            default: {
                throw new IllegalArgumentException("Unknown content policy: " + i);
            }
        }
        if (i != this.contentPolicy) {
            int old = this.contentPolicy;
            this.contentPolicy = i;
            this.firePropertyChange("contentPolicy", old, i);
        }
    }

    public int getContentPolicy() {
        return this.contentPolicy;
    }

    @Override
    public boolean isValidateRoot() {
        return true;
    }

    @Override
    public boolean isPaintingOrigin() {
        return true;
    }

    public void setToolTipTextAt(int index, String toolTip) {
        TabData tabData = this.getModel().getTab(index);
        if (tabData != null) {
            tabData.tip = toolTip;
        }
    }

    public final TabDataModel getModel() {
        return this.model;
    }

    public final SingleSelectionModel getSelectionModel() {
        return this.getUI().getSelectionModel();
    }

    public final Rectangle getTabRect(int index, Rectangle r) {
        return this.getUI().getTabRect(index, r);
    }

    public int tabForCoordinate(Point p) {
        return this.getUI().tabForCoordinate(p);
    }

    public final void setActive(boolean active) {
        if (active != this.active) {
            this.active = active;
            this.firePropertyChange("active", !active, active);
        }
    }

    public final void requestAttention(int tab) {
        this.getUI().requestAttention(tab);
    }

    public final void cancelRequestAttention(int tab) {
        this.getUI().cancelRequestAttention(tab);
    }

    public final void setAttentionHighlight(int tab, boolean highlight) {
        this.getUI().setAttentionHighlight(tab, highlight);
    }

    public final boolean requestAttention(TabData data) {
        boolean result;
        int idx = this.getModel().indexOf(data);
        boolean bl = result = idx >= 0;
        if (result) {
            this.requestAttention(idx);
        }
        return result;
    }

    public final void cancelRequestAttention(TabData data) {
        int idx = this.getModel().indexOf(data);
        if (idx != -1) {
            this.cancelRequestAttention(idx);
        }
    }

    public final void setAttentionHighlight(TabData data, boolean highlight) {
        int idx = this.getModel().indexOf(data);
        if (idx != -1) {
            this.setAttentionHighlight(idx, highlight);
        }
    }

    public final boolean isActive() {
        return this.active;
    }

    public final synchronized void addActionListener(ActionListener listener) {
        if (this.actionListenerList == null) {
            this.actionListenerList = new ArrayList<ActionListener>();
        }
        this.actionListenerList.add(listener);
    }

    public final synchronized void removeActionListener(ActionListener listener) {
        if (this.actionListenerList != null) {
            this.actionListenerList.remove(listener);
            if (this.actionListenerList.isEmpty()) {
                this.actionListenerList = null;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final void postActionEvent(TabActionEvent event) {
        List<ActionListener> list;
        TabbedContainer tabbedContainer = this;
        synchronized (tabbedContainer) {
            if (this.actionListenerList == null) {
                return;
            }
            list = Collections.unmodifiableList(this.actionListenerList);
        }
        for (ActionListener l : list) {
            l.actionPerformed(event);
        }
    }

    public void setIconAt(int index, Icon icon) {
        this.getModel().setIcon(index, icon);
    }

    public void setTitleAt(int index, String title) {
        this.getModel().setText(index, title);
    }

    public Image createImageOfTab(int idx) {
        return this.getUI().createImageOfTab(idx);
    }

    public int getTabCount() {
        return this.getModel().size();
    }

    public final void setShowCloseButton(boolean val) {
        boolean wasShow = this.isShowCloseButton();
        if (val != wasShow) {
            this.getUI().setShowCloseButton(val);
            this.firePropertyChange("showCloseButton", wasShow, val);
        }
    }

    public final boolean isShowCloseButton() {
        return this.getUI().isShowCloseButton();
    }

    public int indexOf(Component comp) {
        int max = this.getModel().size();
        TabDataModel mdl = this.getModel();
        for (int i = 0; i < max; ++i) {
            if (this.getComponentConverter().getComponent(mdl.getTab(i)) != comp) continue;
            return i;
        }
        return -1;
    }

    public int dropIndexOfPoint(Point location) {
        return this.getUI().dropIndexOfPoint(location);
    }

    public Shape getDropIndication(Object dragged, Point location) {
        int ix = dragged instanceof Component ? this.indexOf((Component)dragged) : (dragged instanceof TabData ? this.getModel().indexOf((TabData)dragged) : -1);
        int over = this.dropIndexOfPoint(location);
        if (over == ix && ix != -1) {
            return this.getUI().getExactTabIndication(over);
        }
        return this.getUI().getInsertTabIndication(over);
    }

    @Deprecated
    public LocationInformer getLocationInformer() {
        return this.locationInformer;
    }

    @Deprecated
    public WinsysInfoForTabbed getWinsysInfo() {
        return this.winsysInfo;
    }

    public WinsysInfoForTabbedContainer getContainerWinsysInfo() {
        return this.containerWinsysInfo;
    }

    @Override
    public AccessibleContext getAccessibleContext() {
        if (null == this.accessibleContext) {
            this.accessibleContext = new JComponent.AccessibleJComponent(){

                @Override
                public AccessibleRole getAccessibleRole() {
                    return AccessibleRole.PAGE_TAB_LIST;
                }
            };
            this.accessibleContext.setAccessibleName(NbBundle.getMessage(TabbedContainer.class, (String)"ACS_TabbedContainer"));
            this.accessibleContext.setAccessibleDescription(NbBundle.getMessage(TabbedContainer.class, (String)"ACSD_TabbedContainer"));
        }
        return this.accessibleContext;
    }

    public boolean isTransparent() {
        return this.isSliding() && this.currentAlpha <= 0.1f;
    }

    public void setTransparent(boolean transparent) {
        this._setTransparent(transparent);
        this.inTransparentMode = transparent;
    }

    private void _setTransparent(boolean transparent) {
        if (this.isSliding()) {
            float oldAlpha = this.currentAlpha;
            float f = this.currentAlpha = transparent ? 0.1f : 1.0f;
            if (oldAlpha != this.currentAlpha) {
                this.repaint();
            }
        }
    }

    private AWTEventListener getAWTListener() {
        if (null == this.awtListener) {
            this.awtListener = new AWTEventListener(){

                @Override
                public void eventDispatched(AWTEvent event) {
                    if (!TabbedContainer.this.isSliding()) {
                        return;
                    }
                    if (event.getID() == 501 || event.getID() == 502 || event.getID() == 507) {
                        if (event.getSource() instanceof Component && !SwingUtilities.isDescendingFrom((Component)event.getSource(), TabbedContainer.this)) {
                            return;
                        }
                        TabbedContainer.this._setTransparent(false);
                    } else if (event.getID() == 401) {
                        KeyEvent ke = (KeyEvent)event;
                        if (ke.getKeyCode() == 96 && ke.isControlDown() && !TabbedContainer.this.inTransparentMode) {
                            TabbedContainer.this.setTransparent(true);
                            ke.consume();
                            return;
                        }
                    } else if (event.getID() == 402) {
                        TabbedContainer.this.setTransparent(false);
                        return;
                    }
                }
            };
        }
        return this.awtListener;
    }

    private boolean isSliding() {
        Component c;
        boolean res = false;
        if (this.getModel().size() == 1 && (c = this.getModel().getTab(0).getComponent()) instanceof JComponent) {
            Object val = ((JComponent)c).getClientProperty("isSliding");
            res = null != val && val instanceof Boolean && (Boolean)val != false;
        }
        return res;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        if (this.isSliding()) {
            Toolkit.getDefaultToolkit().addAWTEventListener(this.getAWTListener(), 131096);
        }
    }

    @Override
    public void removeNotify() {
        if (null != this.awtListener) {
            Toolkit.getDefaultToolkit().removeAWTEventListener(this.awtListener);
            this.awtListener = null;
        }
        super.removeNotify();
        this.currentAlpha = 1.0f;
    }

    @Override
    public void paint(Graphics g) {
        if (this.isSliding() && this.currentAlpha != 1.0f) {
            Graphics2D g2d = (Graphics2D)g;
            Composite oldComposite = g2d.getComposite();
            g2d.setComposite(AlphaComposite.getInstance(3, this.currentAlpha));
            super.paint(g);
            g2d.setComposite(oldComposite);
        } else {
            super.paint(g);
        }
    }

    static {
        String s = System.getProperty("nb.tabcontrol.contentpolicy");
        if (s != null) {
            try {
                DEFAULT_CONTENT_POLICY = Integer.parseInt(s);
                switch (DEFAULT_CONTENT_POLICY) {
                    case 1: 
                    case 2: 
                    case 3: {
                        System.err.println("Using custom content policy: " + DEFAULT_CONTENT_POLICY);
                        break;
                    }
                    default: {
                        throw new Error("Bad value for default content policy: " + s + " only values 1, 2 or 3" + "are meaningful");
                    }
                }
                System.err.println("Default content policy is " + DEFAULT_CONTENT_POLICY);
            }
            catch (Exception e) {
                System.err.println("Error parsing default content policy: \"" + s + "\"");
            }
        }
    }

}

