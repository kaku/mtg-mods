/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.swing.tabcontrol;

import java.awt.Component;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbed;
import org.openide.windows.TopComponent;

public abstract class WinsysInfoForTabbedContainer
implements WinsysInfoForTabbed {
    public boolean isTopComponentSlidingEnabled() {
        return true;
    }

    public boolean isTopComponentClosingEnabled() {
        return true;
    }

    public boolean isTopComponentMaximizationEnabled() {
        return true;
    }

    public boolean isTopComponentClosingEnabled(TopComponent tc) {
        return true;
    }

    public boolean isTopComponentSlidingEnabled(TopComponent tc) {
        return true;
    }

    public boolean isTopComponentMaximizationEnabled(TopComponent tc) {
        return true;
    }

    public boolean isModeSlidingEnabled() {
        return true;
    }

    public boolean isSlidedOutContainer() {
        return false;
    }

    public boolean isTopComponentBusy(TopComponent tc) {
        return false;
    }

    public static WinsysInfoForTabbedContainer getDefault(WinsysInfoForTabbed winsysInfo) {
        return new DefaultWinsysInfoForTabbedContainer(winsysInfo);
    }

    private static class DefaultWinsysInfoForTabbedContainer
    extends WinsysInfoForTabbedContainer {
        private WinsysInfoForTabbed delegate;

        public DefaultWinsysInfoForTabbedContainer(WinsysInfoForTabbed winsysInfo) {
            this.delegate = winsysInfo;
        }

        @Override
        public Object getOrientation(Component comp) {
            return null == this.delegate ? TabDisplayer.ORIENTATION_CENTER : this.delegate.getOrientation(comp);
        }

        @Override
        public boolean inMaximizedMode(Component comp) {
            return null == this.delegate ? false : this.delegate.inMaximizedMode(comp);
        }
    }

}

