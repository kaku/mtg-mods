/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.swing.tabcontrol.customtabs;

import java.awt.Component;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionListener;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.event.ChangeListener;
import org.openide.windows.TopComponent;

public abstract class Tabbed {
    public abstract void requestAttention(TopComponent var1);

    public abstract void cancelRequestAttention(TopComponent var1);

    public void setAttentionHighlight(TopComponent tc, boolean highlight) {
    }

    public abstract void addTopComponent(String var1, Icon var2, TopComponent var3, String var4);

    public abstract void insertComponent(String var1, Icon var2, Component var3, String var4, int var5);

    public abstract void setTopComponents(TopComponent[] var1, TopComponent var2);

    public abstract int getTabCount();

    public abstract TopComponent[] getTopComponents();

    public abstract TopComponent getTopComponentAt(int var1);

    public abstract int indexOf(Component var1);

    public abstract void removeComponent(Component var1);

    public abstract void setTitleAt(int var1, String var2);

    public abstract void setIconAt(int var1, Icon var2);

    public abstract void setToolTipTextAt(int var1, String var2);

    public abstract void setSelectedComponent(Component var1);

    public abstract TopComponent getSelectedTopComponent();

    public abstract void addChangeListener(ChangeListener var1);

    public abstract void removeChangeListener(ChangeListener var1);

    public abstract void addActionListener(ActionListener var1);

    public abstract void removeActionListener(ActionListener var1);

    public abstract void setActive(boolean var1);

    public abstract int tabForCoordinate(Point var1);

    public abstract Shape getIndicationForLocation(Point var1, TopComponent var2, Point var3, boolean var4);

    public abstract Object getConstraintForLocation(Point var1, boolean var2);

    public abstract Image createImageOfTab(int var1);

    public abstract Component getComponent();

    public abstract Action[] getPopupActions(Action[] var1, int var2);

    public abstract Rectangle getTabBounds(int var1);

    public abstract Rectangle getTabsArea();

    public abstract boolean isTransparent();

    public abstract void setTransparent(boolean var1);

    public void makeBusy(TopComponent tc, boolean busy) {
    }

    public boolean isBusy(TopComponent tc) {
        return false;
    }

    public static interface Accessor {
        public Tabbed getTabbed();
    }

}

