/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.customtabs;

import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.customtabs.Tabbed;
import org.netbeans.swing.tabcontrol.customtabs.TabbedType;

public interface TabbedComponentFactory {
    public Tabbed createTabbedComponent(TabbedType var1, WinsysInfoForTabbedContainer var2);
}

