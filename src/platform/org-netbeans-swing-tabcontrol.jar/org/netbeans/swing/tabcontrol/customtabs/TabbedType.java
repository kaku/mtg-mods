/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.customtabs;

public enum TabbedType {
    VIEW{

        @Override
        public int toInt() {
            return 0;
        }
    }
    ,
    EDITOR{

        @Override
        public int toInt() {
            return 1;
        }
    }
    ,
    SLIDING{

        @Override
        public int toInt() {
            return 2;
        }
    }
    ,
    TOOLBAR{

        @Override
        public int toInt() {
            return 3;
        }
    };
    

    private TabbedType() {
    }

    public abstract int toInt();

}

