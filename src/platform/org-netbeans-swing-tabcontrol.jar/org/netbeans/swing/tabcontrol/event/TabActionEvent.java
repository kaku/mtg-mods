/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.event;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;

public final class TabActionEvent
extends ActionEvent {
    private MouseEvent mouseEvent = null;
    private int tabIndex;
    private String groupName = null;

    public TabActionEvent(Object source, String command, int tabIndex) {
        super(source, 1001, command);
        this.tabIndex = tabIndex;
        this.consumed = false;
    }

    public TabActionEvent(Object source, String command, int tabIndex, MouseEvent mouseEvent) {
        this(source, command, tabIndex);
        this.mouseEvent = mouseEvent;
        this.consumed = false;
    }

    @Override
    public void consume() {
        this.consumed = true;
    }

    @Override
    public boolean isConsumed() {
        return super.isConsumed();
    }

    public MouseEvent getMouseEvent() {
        return this.mouseEvent;
    }

    public int getTabIndex() {
        return this.tabIndex;
    }

    public String getGroupName() {
        return this.groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public void setSource(Object source) {
        this.source = source;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("TabActionEvent:");
        sb.append("Tab " + this.tabIndex + " " + this.getActionCommand());
        return sb.toString();
    }
}

