/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.event;

import javax.swing.event.ListDataEvent;
import org.netbeans.swing.tabcontrol.TabData;

public class ComplexListDataEvent
extends ListDataEvent {
    private static final int LAST = 2;
    public static final int ITEMS_ADDED = 3;
    public static final int ITEMS_REMOVED = 4;
    static final int ITEMS_CHANGED = 5;
    private int[] indices;
    private boolean textChanged;
    private boolean componentChanged = false;
    private TabData[] affectedItems = null;

    public ComplexListDataEvent(Object source, int id, int[] indices, boolean textChanged) {
        super(source, id, -1, -1);
        this.textChanged = textChanged;
        this.indices = indices;
    }

    public ComplexListDataEvent(Object source, int id, int start, int end) {
        super(source, id, start, end);
        this.textChanged = true;
        this.indices = null;
    }

    public ComplexListDataEvent(Object source, int id, int start, int end, boolean textChanged, boolean compChange) {
        super(source, id, start, end);
        textChanged = true;
        this.indices = null;
        this.componentChanged = compChange;
    }

    public ComplexListDataEvent(Object source, int id, int start, int end, boolean textChanged) {
        this(source, id, start, end);
        this.textChanged = textChanged;
        this.indices = null;
    }

    public int[] getIndices() {
        return this.indices;
    }

    public boolean isTextChanged() {
        return this.textChanged;
    }

    public boolean isUserObjectChanged() {
        return this.componentChanged;
    }

    @Override
    public String toString() {
        String[] types = new String[]{"CONTENTS_CHANGED", "INTERVAL_ADDED", "INTERVAL_REMOVED", "ITEMS_ADDED", "ITEMS_REMOVED"};
        StringBuffer out = new StringBuffer(this.getClass().getName());
        out.append(" - " + types[this.getType()] + " - ");
        if (this.getType() <= 2) {
            out.append("start=" + this.getIndex0() + " end=" + this.getIndex1() + " ");
        } else {
            int[] ids = this.getIndices();
            if (ids != null) {
                for (int i = 0; i < ids.length; ++i) {
                    out.append(ids[i]);
                    if (i == ids.length - 1) continue;
                    out.append(',');
                }
            } else {
                out.append("null");
            }
        }
        return out.toString();
    }

    public void setAffectedItems(TabData[] td) {
        this.affectedItems = td;
    }

    public TabData[] getAffectedItems() {
        return this.affectedItems;
    }
}

