/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.event;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.netbeans.swing.tabcontrol.TabData;

public final class ArrayDiff {
    private TabData[] old;
    private TabData[] nue;
    private Set<Integer> deleted = null;
    private Set<Integer> added = null;

    private ArrayDiff(TabData[] old, TabData[] nue) {
        this.old = old;
        this.nue = nue;
        if (nue == null || old == null) {
            throw new NullPointerException(old == null && nue == null ? "Both arrays are null" : (old == null ? "Old array is null" : "New array is null"));
        }
    }

    public TabData[] getOldData() {
        return this.old;
    }

    public TabData[] getNewData() {
        return this.nue;
    }

    public static ArrayDiff createDiff(TabData[] old, TabData[] nue) {
        if (!Arrays.equals(old, nue)) {
            return new ArrayDiff(old, nue);
        }
        return null;
    }

    public Set<Integer> getDeletedIndices() {
        if (this.deleted == null) {
            HashSet<TabData> set = new HashSet<TabData>(Arrays.asList(this.nue));
            HashSet<Integer> results = new HashSet<Integer>(this.old.length);
            for (int i = 0; i < this.old.length; ++i) {
                if (set.contains(this.old[i])) continue;
                results.add(new Integer(i));
            }
            this.deleted = results;
        }
        return this.deleted;
    }

    public Set<Integer> getAddedIndices() {
        if (this.added == null) {
            HashSet<TabData> set = new HashSet<TabData>(Arrays.asList(this.old));
            HashSet<Integer> results = new HashSet<Integer>(this.nue.length);
            for (int i = 0; i < this.nue.length; ++i) {
                if (set.contains(this.nue[i])) continue;
                results.add(new Integer(i));
            }
            this.added = results;
        }
        return this.added;
    }

    public Set<Integer> getChangedIndices() {
        int max = Math.max(this.nue.length, this.old.length);
        HashSet<Integer> results = new HashSet<Integer>(max);
        for (int i = 0; i < max; ++i) {
            if (i < this.old.length && i < this.nue.length) {
                if (this.old[i].equals(this.nue[i])) continue;
                results.add(new Integer(i));
                continue;
            }
            results.add(new Integer(i));
        }
        return results;
    }

    public Set<Integer> getMovedIndices() {
        HashSet<TabData> set = new HashSet<TabData>(Arrays.asList(this.nue));
        HashSet<Integer> results = new HashSet<Integer>(this.old.length);
        for (int i = 0; i < this.old.length; ++i) {
            boolean isMoved;
            boolean isPresent = set.contains(this.old[i]);
            if (!isPresent) continue;
            boolean bl = isMoved = i < this.nue.length && !this.nue[i].equals(this.old[i]) || i >= this.nue.length;
            if (!isMoved) continue;
            results.add(new Integer(i));
        }
        return results;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("<ArrayDiff: deleted indices: [");
        sb.append(ArrayDiff.outCol(this.getDeletedIndices()));
        sb.append("] added indices: [");
        sb.append(ArrayDiff.outCol(this.getAddedIndices()));
        sb.append("] changed indices: [");
        sb.append(ArrayDiff.outCol(this.getChangedIndices()));
        sb.append("] moved indices: [");
        sb.append(ArrayDiff.outCol(this.getChangedIndices()));
        sb.append("]>");
        return sb.toString();
    }

    private static String outCol(Collection c) {
        Iterator i = c.iterator();
        StringBuffer result = new StringBuffer();
        while (i.hasNext()) {
            Object o = i.next();
            result.append(o.toString());
            if (!i.hasNext()) continue;
            result.append(",");
        }
        return result.toString();
    }

    public boolean equals(Object o) {
        if (o instanceof ArrayDiff) {
            if (o == this) {
                return true;
            }
            Object[] otherOld = ((ArrayDiff)o).getOldData();
            Object[] otherNue = ((ArrayDiff)o).getNewData();
            return Arrays.equals(this.old, otherOld) && Arrays.equals(this.nue, otherNue);
        }
        return false;
    }

    public int hashCode() {
        return ArrayDiff.arrayHashCode(this.old) ^ ArrayDiff.arrayHashCode(this.nue);
    }

    private static int arrayHashCode(Object[] o) {
        int result = 0;
        for (int i = 0; i < o.length; ++i) {
            result += o[i].hashCode() ^ i;
        }
        return result;
    }
}

