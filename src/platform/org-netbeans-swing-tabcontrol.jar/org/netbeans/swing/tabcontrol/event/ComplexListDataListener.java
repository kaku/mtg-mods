/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.event;

import javax.swing.event.ListDataListener;
import org.netbeans.swing.tabcontrol.event.ComplexListDataEvent;

public interface ComplexListDataListener
extends ListDataListener {
    public void indicesAdded(ComplexListDataEvent var1);

    public void indicesRemoved(ComplexListDataEvent var1);

    public void indicesChanged(ComplexListDataEvent var1);
}

