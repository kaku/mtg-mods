/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol.event;

import java.util.Arrays;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.event.ArrayDiff;
import org.netbeans.swing.tabcontrol.event.ComplexListDataEvent;

public final class VeryComplexListDataEvent
extends ComplexListDataEvent {
    TabData[] old;
    TabData[] nue;

    public VeryComplexListDataEvent(Object source, TabData[] old, TabData[] nue) {
        super(source, 5, -1, -1);
        this.old = old;
        this.nue = nue;
    }

    public ArrayDiff getDiff() {
        return ArrayDiff.createDiff(this.old, this.nue);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("VeryComplexListEvent - old array: ");
        sb.append(Arrays.asList(this.old));
        sb.append(" new array: ");
        sb.append(Arrays.asList(this.nue));
        sb.append(" diff: ");
        sb.append(this.getDiff());
        return sb.toString();
    }

    private static final void arr2str(Object[] o, StringBuffer sb) {
        for (int i = 0; i < o.length; ++i) {
            sb.append(o[i]);
            if (i == o.length - 1) continue;
            sb.append(",");
        }
    }
}

