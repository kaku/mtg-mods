/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ChangeSupport
 */
package org.netbeans.swing.tabcontrol;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import javax.swing.Icon;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListDataEvent;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.event.ComplexListDataEvent;
import org.netbeans.swing.tabcontrol.event.ComplexListDataListener;
import org.netbeans.swing.tabcontrol.event.VeryComplexListDataEvent;
import org.openide.util.ChangeSupport;

public class DefaultTabDataModel
implements TabDataModel {
    private transient ArrayList<ComplexListDataListener> listenerList;
    private L list;
    private final ChangeSupport cs;
    private final Object LOCK;

    public DefaultTabDataModel() {
        this.list = new L();
        this.cs = new ChangeSupport((Object)this);
        this.LOCK = new Object();
    }

    public DefaultTabDataModel(TabData[] data) {
        this.list = new L();
        this.cs = new ChangeSupport((Object)this);
        this.LOCK = new Object();
        this.list.addAll(Arrays.asList(data));
    }

    @Override
    public List<TabData> getTabs() {
        return Collections.unmodifiableList(this.list);
    }

    @Override
    public TabData getTab(int index) {
        return (TabData)this.list.get(index);
    }

    @Override
    public void setTabs(TabData[] data) {
        Object[] oldContents = new TabData[this.list.size()];
        if (Arrays.equals(data, oldContents = this.list.toArray(oldContents))) {
            return;
        }
        List<TabData> newContents = Arrays.asList(data);
        this.list.clear();
        this.list.addAll(Arrays.asList(data));
        VeryComplexListDataEvent vclde = new VeryComplexListDataEvent(this, (TabData[])oldContents, data);
        this.fireIndicesChanged(vclde);
    }

    @Override
    public void setIcon(int index, Icon i) {
        boolean[] widthChanged = new boolean[]{false};
        boolean fireChange = this._setIcon(index, i, widthChanged);
        if (fireChange) {
            ComplexListDataEvent clde = new ComplexListDataEvent(this, 0, index, index, widthChanged[0]);
            this.fireContentsChanged(clde);
        }
    }

    private boolean _setIcon(int index, Icon i, boolean[] widthChanged) {
        TabData data;
        if (i == null) {
            i = TabData.NO_ICON;
        }
        if (i != (data = this.getTab(index)).getIcon()) {
            widthChanged[0] = data.getIcon().getIconWidth() != i.getIconWidth();
            data.icon = i;
            return true;
        }
        return false;
    }

    public void setToolTipTextAt(int index, String tooltip) {
        TabData data = this.getTab(index);
        data.tip = tooltip;
    }

    @Override
    public void setText(int index, String txt) {
        boolean[] widthChanged = new boolean[]{false};
        boolean fireChange = this._setText(index, txt, widthChanged);
        if (fireChange) {
            ComplexListDataEvent clde = new ComplexListDataEvent(this, 0, index, index, widthChanged[0]);
            this.fireContentsChanged(clde);
        }
    }

    private int[] _setText(int[] indices, String[] txt, boolean[] widthChanged) {
        widthChanged[0] = false;
        boolean fireChange = false;
        boolean[] changed = new boolean[indices.length];
        int changedCount = 0;
        Arrays.fill(changed, false);
        for (int i = 0; i < indices.length; ++i) {
            boolean[] currWidthChanged = new boolean[]{false};
            fireChange |= this._setText(indices[i], txt[i], currWidthChanged);
            boolean[] arrbl = widthChanged;
            arrbl[0] = arrbl[0] | currWidthChanged[0];
            if (currWidthChanged[0]) {
                ++changedCount;
            }
            changed[i] = currWidthChanged[0];
        }
        if (widthChanged[0] || fireChange) {
            int[] toFire;
            if (changedCount == indices.length) {
                toFire = indices;
            } else {
                toFire = new int[changedCount];
                int idx = 0;
                for (int i2 = 0; i2 < indices.length; ++i2) {
                    if (!changed[i2]) continue;
                    toFire[idx] = indices[i2];
                    ++idx;
                }
            }
            return toFire;
        }
        return null;
    }

    private int[] _setIcon(int[] indices, Icon[] icons, boolean[] widthChanged) {
        widthChanged[0] = false;
        boolean fireChange = false;
        boolean[] changed = new boolean[indices.length];
        int changedCount = 0;
        Arrays.fill(changed, false);
        boolean[] currWidthChanged = new boolean[]{false};
        boolean currChanged = false;
        for (int i = 0; i < indices.length; ++i) {
            currChanged = this._setIcon(indices[i], icons[i], currWidthChanged);
            fireChange |= currChanged;
            boolean[] arrbl = widthChanged;
            arrbl[0] = arrbl[0] | currWidthChanged[0];
            if (currChanged) {
                ++changedCount;
            }
            changed[i] = currChanged;
        }
        if (widthChanged[0] || fireChange) {
            int[] toFire;
            if (changedCount == indices.length) {
                toFire = indices;
            } else {
                toFire = new int[changedCount];
                int idx = 0;
                for (int i2 = 0; i2 < indices.length; ++i2) {
                    if (!changed[i2]) continue;
                    toFire[idx] = indices[i2];
                    ++idx;
                }
            }
            return toFire;
        }
        return null;
    }

    @Override
    public void setIconsAndText(int[] indices, String[] txt, Icon[] icons) {
        boolean fire;
        boolean[] iconWidthsChanged = new boolean[]{false};
        boolean[] txtWidthsChanged = new boolean[]{false};
        int[] iconsToFire = this._setIcon(indices, icons, iconWidthsChanged);
        int[] txtToFire = this._setText(indices, txt, txtWidthsChanged);
        boolean widthChanged = iconWidthsChanged[0] || txtWidthsChanged[0];
        boolean bl = fire = widthChanged || iconsToFire != null || txtToFire != null;
        if (fire) {
            if (indices == iconsToFire && indices == txtToFire) {
                ComplexListDataEvent clde = new ComplexListDataEvent((Object)this, 0, indices, widthChanged);
                this.fireContentsChanged(clde);
            } else {
                Integer[] o;
                int size = (iconsToFire != null ? iconsToFire.length : 0) + (txtToFire != null ? txtToFire.length : 0);
                HashSet<Integer> allIndicesToFire = new HashSet<Integer>(size);
                if (iconsToFire != null) {
                    o = this.toObjectArray(iconsToFire);
                    allIndicesToFire.addAll(Arrays.asList(o));
                }
                if (txtToFire != null) {
                    o = this.toObjectArray(txtToFire);
                    allIndicesToFire.addAll(Arrays.asList(o));
                }
                Integer[] all = new Integer[allIndicesToFire.size()];
                all = allIndicesToFire.toArray(all);
                int[] allPrimitive = this.toPrimitiveArray(all);
                ComplexListDataEvent clde = new ComplexListDataEvent((Object)this, 0, allPrimitive, widthChanged);
                this.fireContentsChanged(clde);
            }
        }
    }

    @Override
    public void setIcon(int[] indices, Icon[] icons) {
        boolean[] widthChanged = new boolean[]{false};
        int[] toFire = this._setIcon(indices, icons, widthChanged);
        if (toFire != null) {
            ComplexListDataEvent clde = new ComplexListDataEvent((Object)this, 0, toFire, widthChanged[0]);
            this.fireContentsChanged(clde);
        }
    }

    @Override
    public void setText(int[] indices, String[] txt) {
        boolean[] widthChanged = new boolean[]{false};
        int[] toFire = this._setText(indices, txt, widthChanged);
        if (toFire != null) {
            ComplexListDataEvent clde = new ComplexListDataEvent((Object)this, 0, toFire, widthChanged[0]);
            this.fireContentsChanged(clde);
        }
    }

    private boolean _setText(int index, String txt, boolean[] widthChanged) {
        TabData data = this.getTab(index);
        if (txt != data.txt) {
            widthChanged[0] = data.getText() != txt;
            data.txt = txt;
            return true;
        }
        return false;
    }

    @Override
    public void setTab(int index, TabData data) {
        if (!data.equals(this.getTab(index))) {
            TabData olddata = this.getTab(index);
            boolean txtChg = data.getText().equals(olddata.getText());
            boolean compChg = data.getUserObject() != olddata.getUserObject();
            this.list.set(index, data);
            ComplexListDataEvent lde = new ComplexListDataEvent(this, 0, index, index, txtChg, compChg);
            lde.setAffectedItems(new TabData[]{data});
            this.fireContentsChanged(lde);
        }
    }

    @Override
    public void addTab(int index, TabData data) {
        this.list.add(index, data);
        ComplexListDataEvent lde = new ComplexListDataEvent(this, 1, index, index, true);
        lde.setAffectedItems(new TabData[]{data});
        this.fireIntervalAdded(lde);
    }

    @Override
    public void addTabs(int start, TabData[] data) {
        this.list.addAll(start, Arrays.asList(data));
        ComplexListDataEvent lde = new ComplexListDataEvent(this, 1, start, start + data.length - 1, true);
        lde.setAffectedItems(data);
        this.fireIntervalAdded(lde);
    }

    @Override
    public void removeTab(int index) {
        TabData[] td = new TabData[]{(TabData)this.list.get(index)};
        this.list.remove(index);
        ComplexListDataEvent lde = new ComplexListDataEvent((Object)this, 2, index, index);
        lde.setAffectedItems(td);
        this.fireIntervalRemoved(lde);
    }

    @Override
    public void removeTabs(int start, int end) {
        ArrayList affected = new ArrayList(this.list.subList(start, end));
        if (start == end) {
            this.list.remove(start);
        } else {
            this.list.removeRange(start, end);
        }
        ComplexListDataEvent lde = new ComplexListDataEvent((Object)this, 2, start, end);
        lde.setAffectedItems(affected.toArray(new TabData[0]));
        this.fireIntervalRemoved(lde);
    }

    @Override
    public void addTabs(int[] indices, TabData[] data) {
        int i;
        HashMap<Integer, TabData> m = new HashMap<Integer, TabData>(data.length);
        for (i = 0; i < data.length; ++i) {
            m.put(new Integer(indices[i]), data[i]);
        }
        Arrays.sort(indices);
        for (i = 0; i < indices.length; ++i) {
            Integer key = new Integer(indices[i]);
            TabData currData = (TabData)m.get(key);
            this.list.add(indices[i], currData);
        }
        ComplexListDataEvent clde = new ComplexListDataEvent((Object)this, 3, indices, true);
        clde.setAffectedItems(data);
        this.fireIndicesAdded(clde);
    }

    @Override
    public void removeTabs(int[] indices) {
        Arrays.sort(indices);
        TabData[] affected = new TabData[indices.length];
        for (int i = indices.length - 1; i >= 0; --i) {
            affected[i] = (TabData)this.list.remove(indices[i]);
        }
        ComplexListDataEvent clde = new ComplexListDataEvent((Object)this, 4, indices, true);
        clde.setAffectedItems(affected);
        this.fireIndicesRemoved(clde);
    }

    @Override
    public int size() {
        return this.list.size();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public synchronized void addComplexListDataListener(ComplexListDataListener listener) {
        Object object = this.LOCK;
        synchronized (object) {
            if (this.listenerList == null) {
                this.listenerList = new ArrayList();
            }
            this.listenerList.add(listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public synchronized void removeComplexListDataListener(ComplexListDataListener listener) {
        Object object = this.LOCK;
        synchronized (object) {
            this.listenerList.remove(listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fireIntervalAdded(ListDataEvent event) {
        ArrayList<ComplexListDataListener> listeners = null;
        Object object = this.LOCK;
        synchronized (object) {
            if (this.listenerList == null) {
                return;
            }
            listeners = new ArrayList<ComplexListDataListener>(this.listenerList);
        }
        for (ComplexListDataListener l : listeners) {
            l.intervalAdded(event);
        }
        this.cs.fireChange();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fireIntervalRemoved(ListDataEvent event) {
        ArrayList<ComplexListDataListener> listeners = null;
        Object object = this.LOCK;
        synchronized (object) {
            if (this.listenerList == null) {
                return;
            }
            listeners = new ArrayList<ComplexListDataListener>(this.listenerList);
        }
        for (ComplexListDataListener l : listeners) {
            l.intervalRemoved(event);
        }
        this.cs.fireChange();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fireContentsChanged(ListDataEvent event) {
        ArrayList<ComplexListDataListener> listeners = null;
        Object object = this.LOCK;
        synchronized (object) {
            if (this.listenerList == null) {
                return;
            }
            listeners = new ArrayList<ComplexListDataListener>(this.listenerList);
        }
        for (ComplexListDataListener l : listeners) {
            l.contentsChanged(event);
        }
        this.cs.fireChange();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fireIndicesAdded(ComplexListDataEvent event) {
        ArrayList<ComplexListDataListener> listeners = null;
        Object object = this.LOCK;
        synchronized (object) {
            if (this.listenerList == null) {
                return;
            }
            listeners = new ArrayList<ComplexListDataListener>(this.listenerList);
        }
        for (ComplexListDataListener l : listeners) {
            l.indicesAdded(event);
        }
        this.cs.fireChange();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fireIndicesRemoved(ComplexListDataEvent event) {
        ArrayList<ComplexListDataListener> listeners = null;
        Object object = this.LOCK;
        synchronized (object) {
            if (this.listenerList == null) {
                return;
            }
            listeners = new ArrayList<ComplexListDataListener>(this.listenerList);
        }
        for (ComplexListDataListener l : listeners) {
            l.indicesRemoved(event);
        }
        this.cs.fireChange();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fireIndicesChanged(ComplexListDataEvent event) {
        ArrayList<ComplexListDataListener> listeners = null;
        Object object = this.LOCK;
        synchronized (object) {
            if (this.listenerList == null) {
                return;
            }
            listeners = new ArrayList<ComplexListDataListener>(this.listenerList);
        }
        for (ComplexListDataListener l : listeners) {
            l.indicesChanged(event);
        }
        this.cs.fireChange();
    }

    public String toString() {
        StringBuffer out = new StringBuffer(this.getClass().getName());
        out.append(" size =");
        int max = this.size();
        out.append(max);
        out.append(" - ");
        for (int i = 0; i < max; ++i) {
            TabData td = this.getTab(i);
            out.append(td.toString());
            if (i == max - 1) continue;
            out.append(',');
        }
        return out.toString();
    }

    @Override
    public void addChangeListener(ChangeListener listener) {
        this.cs.addChangeListener(listener);
    }

    @Override
    public void removeChangeListener(ChangeListener listener) {
        this.cs.removeChangeListener(listener);
    }

    @Override
    public int indexOf(TabData td) {
        return this.list.indexOf(td);
    }

    private Integer[] toObjectArray(int[] o) {
        Integer[] result = new Integer[o.length];
        for (int i = 0; i < o.length; ++i) {
            result[i] = new Integer(o[i]);
        }
        return result;
    }

    private int[] toPrimitiveArray(Integer[] o) {
        int[] result = new int[o.length];
        for (int i = 0; i < o.length; ++i) {
            result[i] = o[i];
        }
        return result;
    }

    private class L
    extends ArrayList<TabData> {
        private L() {
        }

        @Override
        public void removeRange(int fromIndex, int toIndex) {
            ArrayList.super.removeRange(fromIndex, toIndex);
        }
    }

}

