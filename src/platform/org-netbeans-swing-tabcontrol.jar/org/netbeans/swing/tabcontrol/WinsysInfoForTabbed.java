/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol;

import java.awt.Component;

public interface WinsysInfoForTabbed {
    public Object getOrientation(Component var1);

    public boolean inMaximizedMode(Component var1);
}

