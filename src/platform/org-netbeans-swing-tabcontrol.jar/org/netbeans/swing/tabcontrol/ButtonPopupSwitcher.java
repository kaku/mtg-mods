/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.StatusDisplayer
 *  org.openide.windows.TopComponent
 */
package org.netbeans.swing.tabcontrol;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Arrays;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.ListSelectionModel;
import javax.swing.SingleSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.MouseInputListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.TableModel;
import org.netbeans.swing.popupswitcher.SwitcherTableItem;
import org.netbeans.swing.tabcontrol.DocumentSwitcherTable;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.TabDisplayerUI;
import org.netbeans.swing.tabcontrol.event.ComplexListDataEvent;
import org.netbeans.swing.tabcontrol.event.ComplexListDataListener;
import org.netbeans.swing.tabcontrol.event.TabActionEvent;
import org.openide.awt.StatusDisplayer;
import org.openide.windows.TopComponent;

final class ButtonPopupSwitcher
implements MouseInputListener,
AWTEventListener,
ListSelectionListener,
ComplexListDataListener,
PopupMenuListener {
    private static JPopupMenu popup;
    private Component invokingComponent = null;
    private long invocationTime = -1;
    private static boolean shown;
    private static ButtonPopupSwitcher currentSwitcher;
    private final DocumentSwitcherTable pTable;
    private int x;
    private int y;
    private boolean isDragging = true;
    private final TabDisplayer displayer;

    public static void showPopup(JComponent owner, TabDisplayer displayer, int x, int y) {
        ButtonPopupSwitcher switcher = new ButtonPopupSwitcher(displayer, x, y);
        switcher.doSelect(owner);
        currentSwitcher = switcher;
    }

    private ButtonPopupSwitcher(TabDisplayer displayer, int x, int y) {
        this.displayer = displayer;
        Object[] items = this.createSwitcherItems(displayer);
        Arrays.sort(items);
        this.pTable = new DocumentSwitcherTable(displayer, (SwitcherTableItem[])items, y);
        this.x = x;
        this.y = y;
    }

    private void doSelect(JComponent owner) {
        this.invokingComponent = owner;
        this.invokingComponent.addMouseListener(this);
        this.invokingComponent.addMouseMotionListener(this);
        this.pTable.addMouseListener(this);
        this.pTable.addMouseMotionListener(this);
        this.pTable.getSelectionModel().addListSelectionListener(this);
        this.displayer.getModel().addComplexListDataListener(this);
        Toolkit.getDefaultToolkit().addAWTEventListener(this, 8);
        popup = new JPopupMenu();
        popup.setBorderPainted(false);
        popup.setBorder(BorderFactory.createEmptyBorder());
        popup.add(this.pTable);
        popup.pack();
        int locationX = this.x - (int)this.pTable.getPreferredSize().getWidth();
        int locationY = this.y + 1;
        popup.setLocation(locationX, locationY);
        popup.setInvoker(this.invokingComponent);
        popup.addPopupMenuListener(this);
        popup.setVisible(true);
        shown = true;
        this.invocationTime = System.currentTimeMillis();
    }

    public static boolean isShown() {
        return shown;
    }

    static void hidePopup() {
        if (ButtonPopupSwitcher.isShown()) {
            currentSwitcher.hideCurrentPopup();
        }
    }

    private synchronized void hideCurrentPopup() {
        this.pTable.removeMouseListener(this);
        this.pTable.removeMouseMotionListener(this);
        this.pTable.getSelectionModel().removeListSelectionListener(this);
        this.displayer.getModel().removeComplexListDataListener(this);
        Toolkit.getDefaultToolkit().removeAWTEventListener(this);
        if (this.invokingComponent != null) {
            this.invokingComponent.removeMouseListener(this);
            this.invokingComponent.removeMouseMotionListener(this);
            this.invokingComponent = null;
        }
        if (popup != null) {
            popup.removePopupMenuListener(this);
            final JPopupMenu popupToHide = popup;
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    if (popupToHide.isVisible()) {
                        popupToHide.setVisible(false);
                    }
                }
            });
            popup.setVisible(false);
            popup = null;
            shown = false;
            currentSwitcher = null;
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        SwitcherTableItem item;
        StatusDisplayer.getDefault().setStatusText(null == (item = this.pTable.getSelectedItem()) ? null : item.getDescription());
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        e.consume();
        this.changeSelection(e);
        this.pTable.onMouseEvent(e);
        this.isDragging = false;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        int tabCount = this.displayer.getModel().size();
        if (this.pTable.onMouseEvent(e) && tabCount == 1) {
            this.hideCurrentPopup();
        }
        e.consume();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        long time;
        if (e.getSource() == this.invokingComponent && (time = System.currentTimeMillis()) - this.invocationTime > 500 && this.isDragging) {
            this.mouseClicked(e);
        }
        this.isDragging = false;
        e.consume();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        SwitcherTableItem item;
        Point p = e.getPoint();
        p = SwingUtilities.convertPoint((Component)e.getSource(), p, this.pTable);
        if (this.pTable.contains(p) && !this.pTable.onMouseEvent(e) && (item = this.pTable.getSelectedItem()) != null) {
            this.hideCurrentPopup();
            item.activate();
        }
        this.isDragging = false;
        e.consume();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        this.mouseDragged(e);
        e.consume();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        this.pTable.clearSelection();
        e.consume();
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        this.changeSelection(e);
        this.pTable.onMouseEvent(e);
        e.consume();
    }

    private void changeSelection(MouseEvent e) {
        Point p = e.getPoint();
        if (e.getSource() != this) {
            p = SwingUtilities.convertPoint((Component)e.getSource(), p, this.pTable);
        }
        if (this.pTable.contains(p)) {
            int row = this.pTable.rowAtPoint(p);
            int col = this.pTable.columnAtPoint(p);
            this.pTable.changeSelection(row, col, false, false);
        } else {
            this.pTable.clearSelection();
        }
    }

    private boolean onSwitcherTable(MouseEvent e) {
        Point p = e.getPoint();
        if (!(e.getSource() instanceof Component)) {
            return false;
        }
        p = SwingUtilities.convertPoint((Component)e.getSource(), p, this.pTable);
        return this.pTable.contains(p);
    }

    @Override
    public void eventDispatched(AWTEvent event) {
        if (event.getSource() == this) {
            return;
        }
        if (event instanceof KeyEvent && event.getID() == 401) {
            if (!this.changeSelection((KeyEvent)event)) {
                Toolkit.getDefaultToolkit().removeAWTEventListener(this);
                this.hideCurrentPopup();
            } else {
                ((KeyEvent)event).consume();
            }
        }
    }

    private boolean changeSelection(KeyEvent event) {
        int key = event.getKeyCode();
        int selRow = this.pTable.getSelectedRow();
        int selCol = this.pTable.getSelectedColumn();
        if (selRow < 0) {
            selRow = 0;
        }
        if (selCol < 0) {
            selCol = 0;
        }
        boolean switched = true;
        switch (key) {
            case 37: {
                if (--selCol >= 0) break;
                selCol = this.pTable.getColumnCount() - 1;
                break;
            }
            case 39: {
                if (++selCol <= this.pTable.getColumnCount() - 1) break;
                selCol = 0;
                break;
            }
            case 40: {
                if (++selRow <= this.pTable.getRowCount() - 1) break;
                selRow = 0;
                if (++selCol <= this.pTable.getColumnCount() - 1) break;
                selCol = 0;
                break;
            }
            case 38: {
                if (--selRow >= 0) break;
                selRow = this.pTable.getRowCount() - 1;
                if (--selCol >= 0) break;
                selCol = this.pTable.getColumnCount() - 1;
                break;
            }
            case 127: {
                DocumentSwitcherTable.Item item = (DocumentSwitcherTable.Item)this.pTable.getModel().getValueAt(selRow, selCol);
                if (null == item || !this.pTable.isClosable(item)) break;
                TabData tab = item.getTabData();
                int tabIndex = this.displayer.getModel().indexOf(tab);
                if (tabIndex < 0) break;
                if (this.displayer.getModel().size() == 1) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            ButtonPopupSwitcher.this.hideCurrentPopup();
                        }
                    });
                }
                TabActionEvent evt = new TabActionEvent(this.displayer, "close", tabIndex);
                this.displayer.postActionEvent(evt);
                selRow = Math.min(this.pTable.getModel().getRowCount() - 1, selRow);
                selCol = Math.min(this.pTable.getModel().getColumnCount() - 1, selCol);
                switched = true;
                break;
            }
            case 10: {
                SwitcherTableItem item = this.pTable.getSelectedItem();
                if (item == null) break;
                item.activate();
                this.hideCurrentPopup();
                break;
            }
            default: {
                switched = false;
            }
        }
        if (switched) {
            this.pTable.changeSelection(selRow, selCol, false, false);
        }
        return switched;
    }

    private void changed() {
        if (!ButtonPopupSwitcher.isShown()) {
            return;
        }
        Object[] items = this.createSwitcherItems(this.displayer);
        if (items.length == 0) {
            this.hideCurrentPopup();
            return;
        }
        Arrays.sort(items);
        this.pTable.setSwitcherItems((SwitcherTableItem[])items, this.y);
        popup.pack();
        int locationX = this.x - (int)this.pTable.getPreferredSize().getWidth();
        int locationY = this.y + 1;
        popup.setLocation(locationX, locationY);
    }

    @Override
    public void indicesAdded(ComplexListDataEvent e) {
        this.changed();
    }

    @Override
    public void indicesRemoved(ComplexListDataEvent e) {
        this.changed();
    }

    @Override
    public void indicesChanged(ComplexListDataEvent e) {
        this.changed();
    }

    @Override
    public void intervalAdded(ListDataEvent e) {
        this.changed();
    }

    @Override
    public void intervalRemoved(ListDataEvent e) {
        this.changed();
    }

    @Override
    public void contentsChanged(ListDataEvent e) {
        this.changed();
    }

    @Override
    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
    }

    @Override
    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        if (null != popup) {
            popup.removePopupMenuListener(this);
        }
        this.hideCurrentPopup();
    }

    @Override
    public void popupMenuCanceled(PopupMenuEvent e) {
        if (null != popup) {
            popup.removePopupMenuListener(this);
        }
        this.hideCurrentPopup();
    }

    private DocumentSwitcherTable.Item[] createSwitcherItems(TabDisplayer displayer) {
        List<TabData> tabs = displayer.getModel().getTabs();
        DocumentSwitcherTable.Item[] items = new DocumentSwitcherTable.Item[tabs.size()];
        int i = 0;
        int selIdx = displayer.getSelectionModel().getSelectedIndex();
        TabData selectedTab = selIdx >= 0 ? displayer.getModel().getTab(selIdx) : null;
        for (TabData tab : tabs) {
            String htmlName;
            String name;
            if (tab.getComponent() instanceof TopComponent) {
                TopComponent tabTC = (TopComponent)tab.getComponent();
                name = tabTC.getDisplayName();
                if (name == null) {
                    name = tabTC.getName();
                }
                if ((htmlName = tabTC.getHtmlDisplayName()) == null) {
                    htmlName = name;
                }
            } else {
                name = htmlName = tab.getText();
            }
            items[i++] = new DocumentSwitcherTable.Item(new ActivatableTab(tab), name, htmlName, tab, tab == selectedTab);
        }
        return items;
    }

    private class ActivatableTab
    implements SwitcherTableItem.Activatable {
        private TabData tab;

        private ActivatableTab(TabData tab) {
            this.tab = tab;
        }

        @Override
        public void activate() {
            if (this.tab != null) {
                this.selectTab(this.tab);
            }
        }

        private void selectTab(TabData tab) {
            List<TabData> tabs = ButtonPopupSwitcher.this.displayer.getModel().getTabs();
            int ind = -1;
            for (int i = 0; i < tabs.size(); ++i) {
                if (!tab.equals(tabs.get(i))) continue;
                ind = i;
                break;
            }
            if (ind != -1) {
                int old = ButtonPopupSwitcher.this.displayer.getSelectionModel().getSelectedIndex();
                ButtonPopupSwitcher.this.displayer.getSelectionModel().setSelectedIndex(ind);
                if (ButtonPopupSwitcher.this.displayer.getType() == 1 && ind >= 0 && ind == old) {
                    ButtonPopupSwitcher.this.displayer.getUI().makeTabVisible(ind);
                }
            }
        }
    }

}

