/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol;

import java.util.List;
import javax.swing.Icon;
import javax.swing.event.ChangeListener;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.event.ComplexListDataListener;

public interface TabDataModel {
    public int size();

    public TabData getTab(int var1);

    public void setTab(int var1, TabData var2);

    public void setIcon(int var1, Icon var2);

    public void setText(int var1, String var2);

    public void setIcon(int[] var1, Icon[] var2);

    public void setText(int[] var1, String[] var2);

    public void setIconsAndText(int[] var1, String[] var2, Icon[] var3);

    public void addTabs(int var1, TabData[] var2);

    public void removeTab(int var1);

    public void addTabs(int[] var1, TabData[] var2);

    public void setTabs(TabData[] var1);

    public void removeTabs(int[] var1);

    public void removeTabs(int var1, int var2);

    public void addTab(int var1, TabData var2);

    public List<TabData> getTabs();

    public int indexOf(TabData var1);

    public void addComplexListDataListener(ComplexListDataListener var1);

    public void removeComplexListDataListener(ComplexListDataListener var1);

    public void addChangeListener(ChangeListener var1);

    public void removeChangeListener(ChangeListener var1);
}

