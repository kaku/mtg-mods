/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol;

import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import javax.swing.JComponent;
import javax.swing.SingleSelectionModel;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabbedContainer;
import org.netbeans.swing.tabcontrol.event.TabActionEvent;

public abstract class TabbedContainerUI
extends ComponentUI {
    protected TabbedContainer container = null;

    public TabbedContainerUI(TabbedContainer container) {
        this.container = container;
    }

    @Override
    public void installUI(JComponent c) {
        assert (c == this.container);
    }

    protected boolean uichange() {
        return false;
    }

    final boolean shouldReplaceUI() {
        return this.uichange();
    }

    public abstract Rectangle getTabRect(int var1, Rectangle var2);

    public abstract int tabForCoordinate(Point var1);

    public abstract void makeTabVisible(int var1);

    protected final boolean shouldPerformAction(String command, int tab, MouseEvent event) {
        TabActionEvent evt = new TabActionEvent(this.container, command, tab, event);
        this.container.postActionEvent(evt);
        return !evt.isConsumed();
    }

    public abstract SingleSelectionModel getSelectionModel();

    public abstract Image createImageOfTab(int var1);

    public abstract Polygon getExactTabIndication(int var1);

    public abstract Polygon getInsertTabIndication(int var1);

    public abstract Rectangle getContentArea();

    public abstract Rectangle getTabsArea();

    public abstract int dropIndexOfPoint(Point var1);

    public abstract void setShowCloseButton(boolean var1);

    public abstract boolean isShowCloseButton();

    protected abstract void requestAttention(int var1);

    protected abstract void cancelRequestAttention(int var1);

    protected void setAttentionHighlight(int tab, boolean highlight) {
    }
}

