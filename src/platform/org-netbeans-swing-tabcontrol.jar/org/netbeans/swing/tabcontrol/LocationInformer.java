/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.tabcontrol;

import java.awt.Component;

@Deprecated
public interface LocationInformer {
    public Object getOrientation(Component var1);
}

