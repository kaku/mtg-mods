/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Task
 *  org.openide.windows.InputOutput
 */
package org.openide.execution;

import org.openide.util.Task;
import org.openide.windows.InputOutput;

public abstract class ExecutorTask
extends Task {
    protected ExecutorTask(Runnable run) {
        super(run);
    }

    public abstract void stop();

    public abstract int result();

    public abstract InputOutput getInputOutput();
}

