/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package org.openide.execution;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.Format;
import java.util.Arrays;
import java.util.Map;
import java.util.logging.Logger;
import org.openide.util.Utilities;

public final class NbProcessDescriptor
implements Serializable {
    private static final long serialVersionUID = -4535211234565221486L;
    private static Logger execLog;
    private String processName;
    private String arguments;
    private String info;

    public NbProcessDescriptor(String processName, String arguments) {
        this(processName, arguments, null);
    }

    public NbProcessDescriptor(String processName, String arguments, String info) {
        this.processName = processName;
        this.arguments = arguments;
        this.info = info;
    }

    public String getProcessName() {
        return this.processName;
    }

    public String getArguments() {
        return this.arguments;
    }

    public String getInfo() {
        return this.info;
    }

    public Process exec(Format format, String[] envp, File cwd) throws IOException {
        return this.exec(format, envp, false, cwd);
    }

    public Process exec(Format format, String[] envp, boolean appendEnv, File cwd) throws IOException {
        String stringArgs = format == null ? this.arguments : format.format(this.arguments);
        String[] args = NbProcessDescriptor.parseArguments(stringArgs);
        String[] call = null;
        envp = NbProcessDescriptor.substituteEnv(format, envp);
        call = new String[args.length + 1];
        call[0] = format == null ? this.processName : format.format(this.processName);
        System.arraycopy(args, 0, call, 1, args.length);
        NbProcessDescriptor.logArgs(call);
        ProcessBuilder pb = new ProcessBuilder(call);
        if (envp != null) {
            Map<String, String> e = pb.environment();
            if (!appendEnv) {
                e.clear();
            }
            for (int i = 0; i < envp.length; ++i) {
                String nameval = envp[i];
                int idx = nameval.indexOf(61);
                if (idx == -1) {
                    throw new IOException("No equal sign in name=value: " + nameval);
                }
                e.put(nameval.substring(0, idx), nameval.substring(idx + 1));
            }
        }
        if (cwd != null) {
            pb.directory(cwd);
        }
        return pb.start();
    }

    private static void logArgs(String[] args) {
        NbProcessDescriptor.getExecLog().fine("Running: " + Arrays.asList(args));
    }

    public Process exec(Format format, String[] envp) throws IOException {
        return this.exec(format, envp, null);
    }

    public Process exec(Format format) throws IOException {
        return this.exec(format, null);
    }

    public Process exec() throws IOException {
        return this.exec(null, null);
    }

    public int hashCode() {
        return this.processName.hashCode() + this.arguments.hashCode();
    }

    public boolean equals(Object o) {
        if (!(o instanceof NbProcessDescriptor)) {
            return false;
        }
        NbProcessDescriptor him = (NbProcessDescriptor)o;
        return this.processName.equals(him.processName) && this.arguments.equals(him.arguments);
    }

    private static String[] parseArguments(String sargs) {
        return Utilities.parseParameters((String)sargs);
    }

    private static Logger getExecLog() {
        if (execLog == null) {
            execLog = Logger.getLogger(NbProcessDescriptor.class.getName());
        }
        return execLog;
    }

    private static String[] substituteEnv(Format format, String[] envp) {
        if (envp == null || envp.length == 0 || format == null) {
            return envp;
        }
        String[] ret = new String[envp.length];
        StringBuffer adder = new StringBuffer();
        for (int i = 0; i < envp.length; ++i) {
            int idx;
            ret[i] = envp[i];
            if (ret[i] == null || (idx = ret[i].indexOf(61)) < 0) continue;
            String val = ret[i].substring(idx + 1);
            String key = ret[i].substring(0, idx);
            adder.append(key).append('=').append(format.format(val));
            ret[i] = adder.toString();
            adder.setLength(0);
        }
        return ret;
    }
}

