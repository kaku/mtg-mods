/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ServiceType
 *  org.openide.ServiceType$Registry
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 */
package org.openide.execution;

import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import org.openide.ServiceType;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public abstract class ScriptType
extends ServiceType {
    private static final long serialVersionUID = 4893207884933024341L;

    public abstract boolean acceptFileObject(FileObject var1);

    public abstract Object eval(Reader var1, Context var2) throws InvocationTargetException;

    public final Object eval(Reader r) throws InvocationTargetException {
        return this.eval(r, ScriptType.getDefaultContext());
    }

    public abstract Object eval(String var1, Context var2) throws InvocationTargetException;

    public final Object eval(String script) throws InvocationTargetException {
        return this.eval(script, ScriptType.getDefaultContext());
    }

    public abstract void exec(Reader var1, Context var2) throws InvocationTargetException;

    public final void exec(Reader r) throws InvocationTargetException {
        this.exec(r, ScriptType.getDefaultContext());
    }

    public abstract void exec(String var1, Context var2) throws InvocationTargetException;

    public final void exec(String script) throws InvocationTargetException {
        this.exec(script, ScriptType.getDefaultContext());
    }

    public abstract void addVariable(String var1, Object var2);

    public static Enumeration scriptTypes() {
        return Collections.enumeration(Lookup.getDefault().lookup(new Lookup.Template(ScriptType.class)).allInstances());
    }

    public static ScriptType find(Class clazz) {
        return (ScriptType)((Object)Lookup.getDefault().lookup(clazz));
    }

    public static ScriptType find(String name) {
        ServiceType t = ((ServiceType.Registry)Lookup.getDefault().lookup(ServiceType.Registry.class)).find(name);
        if (t instanceof ScriptType) {
            return (ScriptType)t;
        }
        return null;
    }

    public static ScriptType getDefault() {
        Enumeration en = ScriptType.scriptTypes();
        if (en.hasMoreElements()) {
            return (ScriptType)((Object)en.nextElement());
        }
        throw new RuntimeException("No script type registered.");
    }

    static Context getDefaultContext() {
        return new Context();
    }

    public static class Context {
    }

}

