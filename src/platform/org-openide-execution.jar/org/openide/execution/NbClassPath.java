/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.EnvironmentNotSupportedException
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$Environment
 *  org.openide.filesystems.FileSystemCapability
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Lookup
 */
package org.openide.execution;

import java.io.File;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.Locale;
import java.util.StringTokenizer;
import org.openide.execution.ExecutionEngine;
import org.openide.filesystems.EnvironmentNotSupportedException;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileSystemCapability;
import org.openide.filesystems.FileUtil;
import org.openide.util.Lookup;

public final class NbClassPath
implements Serializable {
    static final long serialVersionUID = -8458093409814321744L;
    private Object[] items;
    private String classpath;

    public NbClassPath(String[] classpathItems) {
        this.items = classpathItems;
    }

    public NbClassPath(File[] classpathItems) {
        this.items = classpathItems;
    }

    private NbClassPath(Object[] arr) {
        this.items = arr;
    }

    public NbClassPath(String path) {
        this.items = new Exception[0];
        this.classpath = path;
        if (path.indexOf(32) >= 0) {
            if (path.startsWith("\"")) {
                return;
            }
            StringBuffer buff = new StringBuffer(path);
            buff.insert(0, '\"');
            buff.append('\"');
            this.classpath = buff.toString();
        }
    }

    public static NbClassPath createRepositoryPath() {
        Thread.dumpStack();
        return NbClassPath.createRepositoryPath(FileSystemCapability.ALL);
    }

    public static NbClassPath createRepositoryPath(FileSystemCapability cap) {
        final class Env
        extends FileSystem.Environment {
            Env() {
            }

            public void addClassPath(String element) {
                LinkedList.this.add(element);
            }
        }
        Thread.dumpStack();
        LinkedList<EnvironmentNotSupportedException> res = new LinkedList<EnvironmentNotSupportedException>();
        Env env = res.new Env();
        Enumeration en = cap.fileSystems();
        while (en.hasMoreElements()) {
            try {
                FileSystem fs = (FileSystem)en.nextElement();
                fs.prepareEnvironment((FileSystem.Environment)env);
            }
            catch (EnvironmentNotSupportedException ex) {
                res.add(ex);
            }
        }
        return new NbClassPath(res.toArray());
    }

    public static NbClassPath createLibraryPath() {
        Thread.dumpStack();
        ExecutionEngine ee = (ExecutionEngine)Lookup.getDefault().lookup(ExecutionEngine.class);
        if (ee != null) {
            return ee.createLibraryPath();
        }
        return new NbClassPath(new File[0]);
    }

    public static NbClassPath createClassPath() {
        Thread.dumpStack();
        String cp = System.getProperty("java.class.path");
        if (cp == null || cp.length() == 0) {
            return new NbClassPath("");
        }
        StringBuffer buf = new StringBuffer(cp.length());
        StringTokenizer tok = new StringTokenizer(cp, File.pathSeparator);
        boolean appended = false;
        while (tok.hasMoreTokens()) {
            String piece = tok.nextToken();
            if (piece.endsWith("openide-compat.jar")) continue;
            if (appended) {
                buf.append(File.pathSeparatorChar);
            } else {
                appended = true;
            }
            buf.append(piece);
        }
        return new NbClassPath(buf.toString());
    }

    public static NbClassPath createBootClassPath() {
        Thread.dumpStack();
        String boot = System.getProperty("sun.boot.class.path");
        StringBuffer sb = boot != null ? new StringBuffer(boot) : new StringBuffer();
        String extensions = System.getProperty("java.ext.dirs");
        if (extensions != null) {
            StringTokenizer st = new StringTokenizer(extensions, File.pathSeparator);
            while (st.hasMoreTokens()) {
                File dir = new File(st.nextToken());
                File[] entries = dir.listFiles();
                if (entries == null) continue;
                for (int i = 0; i < entries.length; ++i) {
                    String name = entries[i].getName().toLowerCase(Locale.US);
                    if (!name.endsWith(".zip") && !name.endsWith(".jar")) continue;
                    if (sb.length() > 0) {
                        sb.append(File.pathSeparatorChar);
                    }
                    sb.append(entries[i].getPath());
                }
            }
        }
        return new NbClassPath(sb.toString());
    }

    public static File toFile(FileObject fo) {
        Thread.dumpStack();
        return FileUtil.toFile((FileObject)fo);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Exception[] getExceptions() {
        try {
            return (Exception[])this.items;
        }
        catch (ClassCastException ex) {
            NbClassPath ex = this;
            synchronized (ex) {
                this.getClassPath();
                int first = 0;
                for (int i = 0; i < this.items.length; ++i) {
                    if (this.items[i] == null) continue;
                    this.items[first++] = this.items[i];
                }
                Exception[] list = new Exception[first];
                System.arraycopy(this.items, 0, list, 0, first);
                this.items = list;
                return list;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String getClassPath() {
        if (this.classpath != null) {
            return this.classpath;
        }
        NbClassPath nbClassPath = this;
        synchronized (nbClassPath) {
            if (this.classpath != null) {
                return this.classpath;
            }
            if (this.items.length == 0) {
                this.classpath = "";
                return "";
            }
            StringBuffer sb = new StringBuffer();
            boolean haveone = false;
            for (int i = 0; i < this.items.length; ++i) {
                Object o = this.items[i];
                if (o == null || !(o instanceof String) && !(o instanceof File)) continue;
                if (haveone) {
                    sb.append(File.pathSeparatorChar);
                } else {
                    haveone = true;
                }
                sb.append(o.toString());
                this.items[i] = null;
            }
            String clsPth = sb.toString();
            if (clsPth.indexOf(32) >= 0) {
                sb.insert(0, '\"');
                sb.append('\"');
                this.classpath = sb.toString();
            } else {
                this.classpath = clsPth;
            }
            return this.classpath;
        }
    }

    public boolean equals(Object o) {
        if (!(o instanceof NbClassPath)) {
            return false;
        }
        NbClassPath him = (NbClassPath)o;
        return this.getClassPath().equals(him.getClassPath());
    }

}

