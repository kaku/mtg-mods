/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.windows.InputOutput
 */
package org.openide.execution;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLStreamHandlerFactory;
import java.security.AllPermission;
import java.security.CodeSource;
import java.security.Permission;
import java.security.PermissionCollection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.jar.Manifest;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.execution.ExecutionEngine;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.windows.InputOutput;

public class NbClassLoader
extends URLClassLoader {
    protected InputOutput inout;
    private HashMap permissionCollections;
    private PermissionCollection defaultPermissions;
    private final boolean fast;
    static ThreadLocal<Boolean> f = new ThreadLocal();

    private static ClassLoader systemClassLoader() {
        return (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
    }

    public NbClassLoader() {
        super(new URL[0], NbClassLoader.systemClassLoader());
        this.fast = false;
    }

    public NbClassLoader(InputOutput io) {
        super(new URL[0], NbClassLoader.systemClassLoader());
        this.fast = false;
        this.inout = io;
    }

    public NbClassLoader(FileObject[] roots, ClassLoader parent, InputOutput io) {
        super(NbClassLoader.createRootURLs(roots), parent);
        this.fast = NbClassLoader.canOptimize(this.getURLs());
        this.inout = io;
    }

    public NbClassLoader(FileSystem[] fileSystems) {
        super(new URL[0], NbClassLoader.systemClassLoader(), (URLStreamHandlerFactory)null);
        this.fast = false;
        Thread.dumpStack();
    }

    public NbClassLoader(FileSystem[] fileSystems, ClassLoader parent) {
        super(new URL[0], parent);
        this.fast = false;
        Thread.dumpStack();
    }

    @Override
    public URL getResource(String name) {
        return super.getResource(name.startsWith("/") ? name.substring(1) : name);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected Class findClass(String name) throws ClassNotFoundException {
        if (!this.fast && name.indexOf(46) != -1) {
            Logger.getLogger(NbClassLoader.class.getName()).log(Level.FINE, "NBFS used!");
            String pkg = name.substring(0, name.lastIndexOf(46));
            if (this.getPackage(pkg) == null) {
                String resource = name.replace('.', '/') + ".class";
                URL[] urls = this.getURLs();
                for (int i = 0; i < urls.length; ++i) {
                    FileObject root = URLMapper.findFileObject((URL)urls[i]);
                    if (root == null) continue;
                    try {
                        FileObject fo = root.getFileObject(resource);
                        if (fo == null) continue;
                        FileObject manifo = root.getFileObject("META-INF/MANIFEST.MF");
                        if (manifo == null) {
                            manifo = root.getFileObject("meta-inf/manifest.mf");
                        }
                        if (manifo == null) break;
                        Manifest mani = new Manifest();
                        InputStream is = manifo.getInputStream();
                        try {
                            mani.read(is);
                        }
                        finally {
                            is.close();
                        }
                        this.definePackage(pkg, mani, urls[i]);
                        break;
                    }
                    catch (IOException ioe) {
                        Exceptions.attachLocalizedMessage((Throwable)ioe, (String)urls[i].toString());
                        Exceptions.printStackTrace((Throwable)ioe);
                    }
                }
            }
        }
        return super.findClass(name);
    }

    public void setDefaultPermissions(PermissionCollection defaultPerms) {
        if (defaultPerms != null && !defaultPerms.isReadOnly()) {
            defaultPerms.setReadOnly();
        }
        this.defaultPermissions = defaultPerms;
    }

    @Override
    protected final synchronized PermissionCollection getPermissions(CodeSource cs) {
        PermissionCollection pc;
        if (this.permissionCollections != null && (pc = (PermissionCollection)this.permissionCollections.get(cs)) != null) {
            return pc;
        }
        return this.createPermissions(cs, this.inout);
    }

    private PermissionCollection createPermissions(CodeSource cs, InputOutput inout) {
        PermissionCollection pc;
        if (inout == null) {
            pc = this.defaultPermissions != null ? this.defaultPermissions : super.getPermissions(cs);
        } else {
            ExecutionEngine engine = ExecutionEngine.getDefault();
            pc = engine.createPermissions(cs, inout);
            if (this.defaultPermissions != null) {
                NbClassLoader.addAllPermissions(pc, this.defaultPermissions);
            } else {
                pc.add(new AllPermission());
            }
        }
        if (this.permissionCollections == null) {
            this.permissionCollections = new HashMap(7);
        }
        this.permissionCollections.put(cs, pc);
        return pc;
    }

    private static void addAllPermissions(PermissionCollection target, PermissionCollection src) {
        Enumeration<Permission> e = src.elements();
        while (e.hasMoreElements()) {
            target.add(e.nextElement());
        }
    }

    private static URL[] createRootURLs(FileObject[] roots) {
        URL[] urls = new URL[roots.length];
        for (int i = 0; i < roots.length; ++i) {
            urls[i] = roots[i].toURL();
        }
        return urls;
    }

    private static boolean canOptimize(URL[] urls) {
        assert (urls != null);
        for (int i = 0; i < urls.length; ++i) {
            URL url = urls[i];
            URL au = FileUtil.getArchiveFile((URL)url);
            if (au != null) {
                if (!url.toExternalForm().endsWith("!/")) {
                    return false;
                }
                url = au;
            }
            if ("file".equals(url.getProtocol())) continue;
            return false;
        }
        return true;
    }
}

