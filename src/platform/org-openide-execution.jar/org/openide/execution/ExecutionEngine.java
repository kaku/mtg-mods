/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.windows.InputOutput
 */
package org.openide.execution;

import java.security.AllPermission;
import java.security.CodeSource;
import java.security.Permission;
import java.security.PermissionCollection;
import java.security.Permissions;
import org.openide.execution.ExecutorTask;
import org.openide.execution.NbClassPath;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;
import org.openide.windows.InputOutput;

public abstract class ExecutionEngine {
    public abstract ExecutorTask execute(String var1, Runnable var2, InputOutput var3);

    protected abstract PermissionCollection createPermissions(CodeSource var1, InputOutput var2);

    protected abstract NbClassPath createLibraryPath();

    public static ExecutionEngine getDefault() {
        ExecutionEngine ee = (ExecutionEngine)Lookup.getDefault().lookup(ExecutionEngine.class);
        if (ee == null) {
            ee = new Trivial();
        }
        return ee;
    }

    static final class Trivial
    extends ExecutionEngine {
        @Override
        protected NbClassPath createLibraryPath() {
            return new NbClassPath(new String[0]);
        }

        @Override
        protected PermissionCollection createPermissions(CodeSource cs, InputOutput io) {
            Permissions allPerms = new Permissions();
            allPerms.add(new AllPermission());
            allPerms.setReadOnly();
            return allPerms;
        }

        @Override
        public ExecutorTask execute(String name, Runnable run, InputOutput io) {
            return new ET(run, name, io);
        }

        private static final class ET
        extends ExecutorTask {
            private RequestProcessor.Task task;
            private int resultValue;
            private final String name;
            private InputOutput io;

            public ET(Runnable run, String name, InputOutput io) {
                super(run);
                this.name = name;
                this.task = RequestProcessor.getDefault().post((Runnable)((Object)this));
            }

            @Override
            public void stop() {
                this.task.cancel();
            }

            @Override
            public int result() {
                this.waitFinished();
                return this.resultValue;
            }

            @Override
            public InputOutput getInputOutput() {
                return this.io;
            }

            public void run() {
                try {
                    super.run();
                }
                catch (RuntimeException x) {
                    x.printStackTrace();
                    this.resultValue = 1;
                }
            }
        }

    }

}

