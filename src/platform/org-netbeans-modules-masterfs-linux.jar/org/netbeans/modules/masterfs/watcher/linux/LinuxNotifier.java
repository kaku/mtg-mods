/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.jna.Function
 *  com.sun.jna.Library
 *  com.sun.jna.Native
 *  com.sun.jna.NativeLibrary
 *  org.netbeans.modules.masterfs.providers.Notifier
 */
package org.netbeans.modules.masterfs.watcher.linux;

import com.sun.jna.Function;
import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.masterfs.providers.Notifier;

public final class LinuxNotifier
extends Notifier<LKey> {
    private static final Logger LOG = Logger.getLogger(LinuxNotifier.class.getName());
    final InotifyImpl IMPL = (InotifyImpl)Native.loadLibrary((String)"c", InotifyImpl.class);
    int fd;
    private ByteBuffer buff = ByteBuffer.allocateDirect(4096);
    private Map<Integer, LKey> map = new HashMap<Integer, LKey>();

    private String getString(int maxLen) {
        if (maxLen < 1) {
            return null;
        }
        int stop = maxLen - 1;
        byte[] temp = new byte[maxLen];
        this.buff.get(temp);
        while (temp[stop] == 0) {
            --stop;
        }
        return new String(temp, 0, stop + 1);
    }

    public String nextEvent() throws IOException {
        String path;
        while ((path = this.nextEventPath()) != null && path.isEmpty()) {
        }
        return path;
    }

    public String nextEventPath() throws IOException {
        while (this.buff.remaining() < 16 || this.buff.remaining() < 16 + this.buff.getInt(this.buff.position() + 12)) {
            this.buff.compact();
            int len = this.IMPL.read(this.fd, this.buff, this.buff.remaining());
            if (len <= 0) {
                int errno = NativeLibrary.getInstance((String)"c").getFunction("errno").getInt(0);
                if (errno == 4) {
                    this.buff.flip();
                    continue;
                }
                throw new IOException("error reading from inotify: " + errno);
            }
            this.buff.position(this.buff.position() + len);
            this.buff.flip();
        }
        int wd = this.buff.getInt();
        int mask = this.buff.getInt();
        int cookie = this.buff.getInt();
        int len = this.buff.getInt();
        String name = this.getString(len);
        if ((mask & 32768) == 32768) {
            return "";
        }
        LKey key = this.map.get(wd);
        if (key == null) {
            return null;
        }
        return key.path;
    }

    protected void start() throws IOException {
        this.buff.position(this.buff.capacity());
        this.buff.order(ByteOrder.nativeOrder());
        this.fd = this.IMPL.inotify_init1(524288);
        if (this.fd < 0) {
            LOG.log(Level.INFO, "Linux kernel {0} returned {1} from inotify_init1", new Object[]{System.getProperty("os.version"), this.fd});
            this.fd = this.IMPL.inotify_init();
            LOG.log(Level.INFO, "Trying inotify_init: {0}", this.fd);
        }
        if (this.fd < 0) {
            throw new IOException("inotify_init failed: " + this.fd);
        }
    }

    public LKey addWatch(String path) throws IOException {
        int id = this.IMPL.inotify_add_watch(this.fd, path, 966);
        LOG.log(Level.FINEST, "addWatch{0} res: {1}", new Object[]{path, id});
        if (id <= 0) {
            int errno = NativeLibrary.getInstance((String)"c").getFunction("errno").getInt(0);
            throw new IOException("addWatch on " + path + " errno: " + errno);
        }
        LKey newKey = this.map.get(id);
        if (newKey == null) {
            newKey = new LKey(id, path);
            this.map.put(id, newKey);
        }
        return newKey;
    }

    public void removeWatch(LKey lkey) {
        this.map.remove(lkey.id);
        this.IMPL.inotify_rm_watch(this.fd, lkey.id);
    }

    static class LKey {
        int id;
        String path;

        public LKey(int id, String path) {
            this.id = id;
            this.path = path;
        }

        public String toString() {
            return "LKey[" + this.id + " - '" + this.path + "']";
        }
    }

    private static interface InotifyImpl
    extends Library {
        public static final int O_CLOEXEC = 524288;
        public static final int IN_ACCESS = 1;
        public static final int IN_MODIFY = 2;
        public static final int IN_ATTRIB = 4;
        public static final int IN_CLOSE_WRITE = 8;
        public static final int IN_CLOSE_NOWRITE = 16;
        public static final int IN_OPEN = 32;
        public static final int IN_MOVED_FROM = 64;
        public static final int IN_MOVED_TO = 128;
        public static final int IN_CREATE = 256;
        public static final int IN_DELETE = 512;
        public static final int IN_DELETE_SELF = 1024;
        public static final int IN_MOVE_SELF = 2048;
        public static final int IN_UNMOUNT = 8192;
        public static final int IN_Q_OVERFLOW = 16384;
        public static final int IN_IGNORED = 32768;

        public int inotify_init();

        public int inotify_init1(int var1);

        public int close(int var1);

        public int read(int var1, ByteBuffer var2, int var3);

        public int inotify_add_watch(int var1, String var2, int var3);

        public int inotify_rm_watch(int var1, int var2);
    }

}

