/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.openide.text;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class Installer
implements Runnable {
    private static Map<String, Integer> mimeTypes = new HashMap<String, Integer>();

    public static void add(String mimeType) {
        if (mimeTypes.containsKey(mimeType)) {
            Integer v = mimeTypes.get(mimeType);
            v = v + 1;
            mimeTypes.put(mimeType, v);
        } else {
            Integer v = new Integer(1);
            mimeTypes.put(mimeType, v);
        }
    }

    @Override
    public void run() {
        for (String s : mimeTypes.keySet()) {
            Logger logger = Logger.getLogger("org.netbeans.ui.metrics.editor");
            LogRecord rec = new LogRecord(Level.INFO, "USG_EDITOR_MIME_TYPE");
            rec.setParameters(new Object[]{s, mimeTypes.get(s)});
            rec.setLoggerName(logger.getName());
            logger.log(rec);
        }
    }
}

