/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.UndoRedo
 *  org.openide.awt.UndoRedo$Manager
 *  org.openide.util.Mutex
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 *  org.openide.util.UserQuestionException
 *  org.openide.util.Utilities
 */
package org.openide.text;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.openide.awt.UndoRedo;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.DocumentLine;
import org.openide.text.DocumentStatus;
import org.openide.text.Line;
import org.openide.text.NbDocument;
import org.openide.text.PositionRef;
import org.openide.text.UndoRedoManager;
import org.openide.text.UserQuestionExceptionHandler;
import org.openide.util.Mutex;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.UserQuestionException;
import org.openide.util.Utilities;

final class DocumentOpenClose {
    static final RequestProcessor RP = new RequestProcessor("org.openide.text Document Processing", 1, false, false);
    private static final int NULL_DOCUMENT_CLOSE_DELAY = 1000;
    private static final Logger LOG = CloneableEditorSupport.ERR;
    final CloneableEditorSupport ces;
    final Object lock;
    DocumentStatus documentStatus = DocumentStatus.CLOSED;
    DocumentLoad activeOpen;
    RequestProcessor.Task activeOpenTask;
    DocumentClose activeClose;
    RequestProcessor.Task activeCloseTask;
    Runnable preReloadEDT;
    DocumentLoad activeReload;
    RequestProcessor.Task activeReloadTask;
    DocumentRef docRef;
    final Object docRefLock;
    StyledDocument strongDocRef;
    boolean firingCloseDocument;
    StyledDocument docOpenedWhenFiringCloseDocument;

    static String getSimpleName(Object o) {
        return o != null ? o.getClass().getSimpleName() + "@" + System.identityHashCode(o) : "null";
    }

    DocumentOpenClose(CloneableEditorSupport ces) {
        this.ces = ces;
        this.lock = ces.getLock();
        this.docRefLock = new Object();
    }

    public DocumentStatus getDocumentStatusLA() {
        return this.documentStatus;
    }

    void setDocumentStatusLA(DocumentStatus documentStatus) {
        this.documentStatus = documentStatus;
    }

    StyledDocument getDocument() {
        return this.getRefDocument();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    StyledDocument getRefDocument() {
        Object object = this.docRefLock;
        synchronized (object) {
            return this.docRef != null ? (StyledDocument)this.docRef.get() : null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void setDocRef(StyledDocument doc) {
        Object object = this.docRefLock;
        synchronized (object) {
            this.docRef = doc != null ? new DocumentRef(doc) : null;
        }
    }

    void setDocumentStronglyReferenced(boolean stronglyReferenced) {
        StyledDocument doc;
        this.strongDocRef = stronglyReferenced ? (doc = this.getRefDocument()) : null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    StyledDocument open() throws IOException {
        DocumentLoad load;
        RequestProcessor.Task task;
        if (LOG.isLoggable(Level.FINEST)) {
            LOG.log(Level.FINEST, "open() requested by", new Exception());
        }
        Object object = this.lock;
        synchronized (object) {
            StyledDocument openDoc = this.retainExistingDocLA();
            if (openDoc != null) {
                if (LOG.isLoggable(Level.FINER)) {
                    LOG.finer("open(): Existing openDoc retained.\n");
                }
                return openDoc;
            }
            switch (this.documentStatus) {
                case OPENED: {
                    if (LOG.isLoggable(Level.FINER)) {
                        LOG.finer("open(): status OPENED but doc GCed. Schedule close task followed by possible open task\n");
                    }
                    this.closeImplLA(null, false);
                    if (this.activeOpen == null) {
                        this.initLoadTaskLA();
                    }
                    load = this.activeOpen;
                    task = this.activeOpenTask;
                    break;
                }
                case CLOSED: {
                    if (LOG.isLoggable(Level.FINER)) {
                        LOG.finer("open(): status CLOSED. Schedule a synchronous open task\n");
                    }
                    if (this.activeOpen == null) {
                        this.initLoadTaskLA();
                    }
                    load = this.activeOpen;
                    task = this.activeOpenTask;
                    break;
                }
                case RELOADING: {
                    load = this.activeReload;
                    task = this.activeReloadTask;
                    break;
                }
                case LOADING: {
                    load = this.activeOpen;
                    task = this.activeOpenTask;
                    break;
                }
                default: {
                    throw this.invalidStatus();
                }
            }
        }
        if (load == null || task == null) {
            LOG.info("load=" + load + ", task=" + (Object)task + ", this: " + this);
        }
        task.waitFinished();
        if (load.loadIOException != null) {
            throw load.loadIOException;
        }
        if (load.loadRuntimeException != null) {
            throw load.loadRuntimeException;
        }
        return load.loadDoc;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    Task openTask() {
        Object object = this.lock;
        synchronized (object) {
            final StyledDocument existingDoc = this.retainExistingDocLA();
            if (existingDoc != null) {
                Task existingDocTask = new Task(new Runnable(){
                    private final StyledDocument doc;

                    @Override
                    public void run() {
                    }
                });
                existingDocTask.run();
                return existingDocTask;
            }
            if (this.activeOpenTask != null) {
                return this.activeOpenTask;
            }
            switch (this.documentStatus) {
                case OPENED: {
                    this.closeImplLA(null, false);
                    this.initLoadTaskLA();
                    break;
                }
                case CLOSED: {
                    this.initLoadTaskLA();
                    break;
                }
                case RELOADING: {
                    return this.activeReloadTask;
                }
                case LOADING: {
                    assert (this.activeOpenTask != null);
                    break;
                }
                default: {
                    throw this.invalidStatus();
                }
            }
            return this.activeOpenTask;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void waitForCloseFinish() {
        RequestProcessor.Task closeTask;
        Object object = this.lock;
        synchronized (object) {
            closeTask = this.activeCloseTask;
        }
        if (closeTask != null) {
            closeTask.waitFinished();
        }
    }

    boolean isDocumentLoadedOrLoading() {
        this.waitForCloseFinish();
        Object object = this.lock;
        synchronized (object) {
            switch (this.documentStatus) {
                case CLOSED: {
                    return false;
                }
                case OPENED: 
                case RELOADING: 
                case LOADING: {
                    return true;
                }
            }
            throw this.invalidStatus();
        }
    }

    boolean isDocumentOpened() {
        this.waitForCloseFinish();
        Object object = this.lock;
        synchronized (object) {
            switch (this.documentStatus) {
                case CLOSED: 
                case RELOADING: 
                case LOADING: {
                    return false;
                }
                case OPENED: {
                    return true;
                }
            }
            throw this.invalidStatus();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    Task reloadTask() {
        Object object = this.lock;
        synchronized (object) {
            if (this.activeReloadTask != null) {
                return this.activeReloadTask;
            }
            return Task.EMPTY;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void reload(JEditorPane[] openedPanes) {
        DocumentLoad reloadEDTTask = null;
        Object object = this.lock;
        synchronized (object) {
            switch (this.documentStatus) {
                case CLOSED: {
                    break;
                }
                case RELOADING: {
                    break;
                }
                case OPENED: 
                case LOADING: {
                    StyledDocument reloadDoc;
                    if (this.activeClose != null || this.activeReload != null || (reloadDoc = (StyledDocument)this.docRef.get()) == null) break;
                    this.initReloadTaskLA(reloadDoc, openedPanes);
                    reloadEDTTask = this.activeReload;
                    break;
                }
                default: {
                    throw this.invalidStatus();
                }
            }
        }
        if (reloadEDTTask != null) {
            Mutex.EVENT.readAccess((Runnable)reloadEDTTask);
        }
    }

    private StyledDocument retainExistingDocLA() {
        switch (this.documentStatus) {
            case CLOSED: {
                break;
            }
            case RELOADING: 
            case LOADING: {
                this.cancelCloseLA();
                break;
            }
            case OPENED: {
                StyledDocument openDoc = this.getRefDocument();
                if (openDoc == null) break;
                this.cancelCloseLA();
                if (this.activeClose != null) break;
                return openDoc;
            }
            default: {
                throw this.invalidStatus();
            }
        }
        return null;
    }

    private void initLoadTaskLA() {
        if (this.activeOpen != null) {
            throw new IllegalStateException("Open task already inited. State:\n" + this);
        }
        if (LOG.isLoggable(Level.FINER)) {
            LOG.finer("initLoadTaskLA(): Schedule open task followed by change firing task.\n");
        }
        this.activeOpen = new DocumentLoad(this);
        this.activeOpenTask = RP.create((Runnable)this.activeOpen);
        this.activeOpenTask.schedule(0);
        RP.create((Runnable)new DocumentOpenFire(this.activeOpen)).schedule(0);
    }

    private void initReloadTaskLA(StyledDocument reloadDoc, JEditorPane[] openedPanes) {
        assert (this.activeReload == null);
        if (LOG.isLoggable(Level.FINER)) {
            LOG.finer("initLoadTaskLA(): Schedule reload task.\n");
        }
        this.activeReload = new DocumentLoad(this, reloadDoc, openedPanes);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void close() {
        Object object = this.lock;
        synchronized (object) {
            StyledDocument doc = this.getRefDocument();
            this.closeImplLA(doc, doc == null);
        }
    }

    void closeImplLA(StyledDocument doc, boolean delayedClose) {
        if (LOG.isLoggable(Level.FINEST)) {
            LOG.log(Level.FINEST, "Close requested by:\n", new Exception());
        }
        if (this.activeClose != null) {
            if (!delayedClose && this.activeClose.delayedClose) {
                this.cancelCloseLA();
                if (this.activeClose != null) {
                    if (LOG.isLoggable(Level.FINER)) {
                        LOG.finer("closeImplLA(): Delayed active close already running (can't be cancelled). Return.\n");
                    }
                    return;
                }
            } else {
                if (LOG.isLoggable(Level.FINER)) {
                    LOG.finer("closeImplLA(): Close already in progress. Return.\n");
                }
                return;
            }
        }
        assert (this.activeClose == null);
        this.activeClose = new DocumentClose(doc, delayedClose);
        this.activeCloseTask = RP.create((Runnable)this.activeClose);
        int delay = delayedClose ? 1000 : 0;
        this.activeCloseTask.schedule(delay);
        if (LOG.isLoggable(Level.FINER)) {
            LOG.finer("closeImplLA(): Scheduled close task with delay=" + delay + ".\n");
        }
    }

    void cancelCloseLA() {
        if (LOG.isLoggable(Level.FINER)) {
            if (LOG.isLoggable(Level.FINEST)) {
                LOG.log(Level.FINEST, "cancelCloseLA(): Attempt to cancel close by\n", new Exception());
            } else {
                LOG.finer("cancelCloseLA(): Attempt to cancel close.\n");
            }
        }
        if (this.activeClose != null && this.activeClose.cancel()) {
            if (LOG.isLoggable(Level.FINER)) {
                LOG.finer("cancelCloseLA(): activeClose().cancel() successful.\n");
            }
            this.activeCloseTask.cancel();
            this.activeCloseTask = null;
            this.activeClose = null;
        }
    }

    void updateLines(StyledDocument doc, boolean close) {
        Map<Line, Reference<Line>> lineMap = this.ces.findWeakHashMap();
        for (Map.Entry<Line, Reference<Line>> entry : lineMap.entrySet()) {
            Line line = entry.getKey();
            if (!(line instanceof DocumentLine)) continue;
            ((DocumentLine)line).documentOpenedClosed(doc, close);
        }
    }

    IllegalStateException invalidStatus() {
        return new IllegalStateException("Unknown documentStatus=" + (Object)((Object)this.documentStatus));
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(200);
        sb.append("DocumentOpenClose: ").append(DocumentOpenClose.getSimpleName((Object)this.ces)).append(", documentStatus=").append((Object)this.documentStatus);
        DocumentRef ref = this.docRef;
        sb.append(", docRef=");
        if (ref != null) {
            StyledDocument doc = (StyledDocument)ref.get();
            sb.append("(").append(DocumentOpenClose.getSimpleName(doc)).append(")");
        } else {
            sb.append("null");
        }
        if (this.activeOpen != null) {
            sb.append("\n  activeOpen: ").append(this.activeOpen);
        }
        if (this.activeReload != null) {
            sb.append("\n  activeReload: ").append(this.activeReload);
        }
        if (this.activeClose != null) {
            sb.append("\n  activeClose: ").append(this.activeClose);
        }
        return sb.toString();
    }

    private final class DocumentRef
    extends WeakReference<StyledDocument>
    implements Runnable {
        public DocumentRef(StyledDocument doc) {
            super(doc, Utilities.activeReferenceQueue());
            Logger.getLogger("TIMER").log(Level.FINE, "TextDocument", doc);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            Object object = DocumentOpenClose.this.lock;
            synchronized (object) {
                if (this == DocumentOpenClose.this.docRef) {
                    DocumentOpenClose.this.closeImplLA(null, true);
                }
            }
        }
    }

    private final class DocumentClose
    implements Runnable {
        final StyledDocument closeDoc;
        final boolean delayedClose;
        boolean cancelled;
        boolean started;
        boolean readLockedRun;

        public DocumentClose(StyledDocument closeDoc, boolean delayedClose) {
            this.closeDoc = closeDoc;
            this.delayedClose = delayedClose;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            if (this.readLockedRun) {
                this.readLockedRun = false;
                this.readLockedRun();
                return;
            }
            Object object = DocumentOpenClose.this.lock;
            synchronized (object) {
                if (this.cancelled) {
                    return;
                }
                this.started = true;
            }
            DocumentOpenClose.this.setDocRef(null);
            try {
                DocumentOpenClose.this.ces.setListeningOnEnv(false);
                this.readLockedRun = true;
                if (this.closeDoc != null) {
                    this.closeDoc.render(this);
                }
                DocumentOpenClose.this.ces.updateLineSet(true);
                DocumentOpenClose.this.updateLines(this.closeDoc, true);
            }
            finally {
                object = DocumentOpenClose.this.lock;
                synchronized (object) {
                    DocumentOpenClose.this.documentStatus = DocumentStatus.CLOSED;
                    DocumentOpenClose.this.activeCloseTask = null;
                    DocumentOpenClose.this.activeClose = null;
                }
                DocumentOpenClose.this.firingCloseDocument = true;
                boolean success = false;
                try {
                    DocumentOpenClose.this.ces.fireDocumentChange(this.closeDoc, true);
                    success = true;
                }
                finally {
                    DocumentOpenClose.this.firingCloseDocument = false;
                    DocumentOpenClose.this.docOpenedWhenFiringCloseDocument = null;
                    if (LOG.isLoggable(Level.FINER)) {
                        LOG.finer("documentClose(): fireDocumentChange: success=" + success + "\n");
                    }
                }
            }
        }

        void readLockedRun() {
            DocumentOpenClose.this.ces.callNotifyUnmodified();
            if (this.closeDoc != null) {
                this.closeDoc.removeUndoableEditListener((UndoableEditListener)DocumentOpenClose.this.ces.getUndoRedo());
                DocumentOpenClose.this.ces.removeDocListener(this.closeDoc);
            }
            DocumentOpenClose.this.ces.getPositionManager().documentClosed();
            DocumentOpenClose.this.ces.getUndoRedo().discardAllEdits();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        boolean cancel() {
            Object object = DocumentOpenClose.this.lock;
            synchronized (object) {
                if (this.started) {
                    return false;
                }
                this.cancelled = true;
                return true;
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(200);
            sb.append("closeDoc=").append(DocumentOpenClose.getSimpleName(this.closeDoc)).append(", delayedClose=").append(this.delayedClose).append(", cancelled=").append(this.cancelled).append(", started=").append(this.started);
            return sb.toString();
        }
    }

    private static final class DocumentOpenFire
    implements Runnable {
        final DocumentLoad documentOpen;

        public DocumentOpenFire(DocumentLoad documentOpen) {
            this.documentOpen = documentOpen;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            assert (!this.documentOpen.reload);
            if (LOG.isLoggable(Level.FINER)) {
                LOG.finer("documentLoad(): Going to fireDocumentChange...\n");
            }
            boolean success = false;
            try {
                this.documentOpen.fireDocumentChange();
                success = true;
            }
            finally {
                if (LOG.isLoggable(Level.FINER)) {
                    LOG.finer("documentLoad(): fireDocumentChange: success=" + success + "\n");
                }
            }
        }
    }

    private final class DocumentLoad
    implements Runnable {
        final boolean reload;
        StyledDocument loadDoc;
        boolean loadSuccess;
        IOException loadIOException;
        RuntimeException loadRuntimeException;
        boolean userQuestionExceptionInReload;
        boolean skipInputStreamReading;
        JEditorPane[] reloadOpenPanes;
        int[] reloadCaretOffsets;
        private boolean atomicLockedRun;
        private boolean preReloadInEDT;
        final /* synthetic */ DocumentOpenClose this$0;

        DocumentLoad(DocumentOpenClose documentOpenClose) {
            this.this$0 = documentOpenClose;
            this.reload = false;
        }

        DocumentLoad(DocumentOpenClose documentOpenClose, StyledDocument loadDoc, JEditorPane[] reloadOpenPanes) {
            this.this$0 = documentOpenClose;
            assert (loadDoc != null);
            this.reload = true;
            this.preReloadInEDT = true;
            this.loadDoc = loadDoc;
            this.reloadOpenPanes = reloadOpenPanes;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            if (this.preReloadInEDT) {
                this.preReloadInEDT = false;
                this.preReloadInEDT();
                return;
            }
            if (this.atomicLockedRun) {
                this.atomicLockedRun = false;
                this.atomicLockedRun();
                return;
            }
            try {
                Object object;
                UndoRedo.Manager undoRedoManager = this.this$0.ces.getUndoRedo();
                if (!this.userQuestionExceptionInReload) {
                    object = this.this$0.lock;
                    synchronized (object) {
                        if (this.reload) {
                            assert (this.this$0.documentStatus == DocumentStatus.OPENED);
                            this.this$0.documentStatus = DocumentStatus.RELOADING;
                        } else {
                            assert (this.this$0.documentStatus == DocumentStatus.CLOSED);
                            this.this$0.documentStatus = DocumentStatus.LOADING;
                        }
                    }
                    if (this.reload) {
                        this.loadDoc.removeUndoableEditListener((UndoableEditListener)undoRedoManager);
                        undoRedoManager.discardAllEdits();
                    } else {
                        object = this.this$0.lock;
                        synchronized (object) {
                            EditorKit kit = this.this$0.ces.createEditorKit();
                            this.loadDoc = this.this$0.ces.createStyledDocument(kit);
                            assert (this.loadDoc != null);
                        }
                    }
                }
                this.atomicLockedRun = true;
                NbDocument.runAtomic(this.loadDoc, this);
                if (this.loadIOException == null && this.loadRuntimeException == null) {
                    if (this.reload) {
                        undoRedoManager.discardAllEdits();
                    } else {
                        this.this$0.ces.setListeningOnEnv(true);
                    }
                    if (undoRedoManager instanceof UndoRedoManager) {
                        ((UndoRedoManager)undoRedoManager).markSavepoint();
                    }
                    if (this.loadDoc != null) {
                        LOG.fine("task-addUndoableEditListener");
                        this.loadDoc.addUndoableEditListener((UndoableEditListener)undoRedoManager);
                    }
                    this.this$0.ces.callNotifyUnmodified();
                    this.this$0.updateLines(this.loadDoc, false);
                    object = this.this$0.lock;
                    synchronized (object) {
                        this.this$0.documentStatus = DocumentStatus.OPENED;
                    }
                    this.loadSuccess = true;
                }
                if (this.reload && this.loadIOException instanceof UserQuestionException) {
                    this.reloadUQEThrown((UserQuestionException)this.loadIOException);
                }
            }
            catch (RuntimeException ex) {
                this.loadRuntimeException = ex;
            }
            finally {
                if (!this.userQuestionExceptionInReload) {
                    Object ex = this.this$0.lock;
                    synchronized (ex) {
                        if (!this.loadSuccess) {
                            this.this$0.documentStatus = DocumentStatus.CLOSED;
                            this.this$0.setDocRef(null);
                            this.this$0.ces.setListeningOnEnv(false);
                        }
                        this.this$0.ces.setPreventModification(false);
                        if (this.reload) {
                            this.this$0.activeReloadTask = null;
                            this.this$0.activeReload = null;
                        } else {
                            this.this$0.activeOpenTask = null;
                            this.this$0.activeOpen = null;
                        }
                        if (LOG.isLoggable(Level.FINER)) {
                            LOG.finer("documentLoad(): reload=" + this.reload + ", documentStatus=" + (Object)((Object)this.this$0.documentStatus) + ", loadSuccess=" + this.loadSuccess + "\n");
                        }
                    }
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void atomicLockedRun() {
            try {
                if (!this.userQuestionExceptionInReload) {
                    if (this.reload) {
                        this.this$0.ces.getPositionManager().documentClosed();
                        this.this$0.ces.updateLineSet(true);
                    }
                    this.this$0.ces.removeDocListener(this.loadDoc);
                    if (this.reload) {
                        LOG.fine("clearDocument");
                        try {
                            if (this.loadDoc.getLength() > 0) {
                                this.loadDoc.remove(0, this.loadDoc.getLength());
                            }
                        }
                        catch (BadLocationException ex) {
                            LOG.log(Level.INFO, null, ex);
                        }
                    }
                } else {
                    this.userQuestionExceptionInReload = false;
                }
                if (!this.skipInputStreamReading) {
                    BufferedInputStream is = new BufferedInputStream(this.this$0.ces.cesEnv().inputStream());
                    try {
                        this.this$0.ces.loadFromStreamToKit(this.loadDoc, is, this.this$0.ces.createEditorKit());
                    }
                    finally {
                        is.close();
                    }
                }
                this.this$0.setDocRef(this.loadDoc);
                if (this.reload && this.reloadOpenPanes != null) {
                    this.this$0.ces.getPositionManager().documentOpened(new WeakReference<StyledDocument>(this.loadDoc));
                }
                this.this$0.ces.updateLineSet(true);
                this.this$0.ces.updateLastSaveTime();
                this.this$0.ces.addDocListener(this.loadDoc);
                if (this.reload && this.reloadCaretOffsets != null) {
                    int docLen = this.loadDoc.getLength();
                    final Position[] caretPositions = new Position[this.reloadCaretOffsets.length];
                    for (int i = 0; i < this.reloadCaretOffsets.length; ++i) {
                        try {
                            int offset = this.reloadCaretOffsets[i];
                            offset = Math.max(Math.min(offset, docLen), 0);
                            caretPositions[i] = this.loadDoc.createPosition(offset);
                            continue;
                        }
                        catch (BadLocationException ex) {
                            caretPositions[i] = null;
                        }
                    }
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            for (int i = 0; i < DocumentLoad.this.reloadOpenPanes.length; ++i) {
                                JEditorPane pane = DocumentLoad.this.reloadOpenPanes[i];
                                if (pane.getDocument() != DocumentLoad.this.loadDoc || caretPositions[i] == null) continue;
                                DocumentLoad.this.reloadOpenPanes[i].setCaretPosition(caretPositions[i].getOffset());
                            }
                        }
                    });
                }
                this.this$0.ces.setPreventModification(true);
            }
            catch (BadLocationException ex) {
                this.loadRuntimeException = new IllegalStateException(ex);
            }
            catch (IOException ex) {
                this.loadIOException = ex;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void preReloadInEDT() {
            boolean success = false;
            try {
                this.loadDoc.render(new Runnable(){

                    @Override
                    public void run() {
                        if (DocumentLoad.this.reloadOpenPanes != null) {
                            DocumentLoad.this.reloadCaretOffsets = new int[DocumentLoad.this.reloadOpenPanes.length];
                            for (int i = 0; i < DocumentLoad.this.reloadOpenPanes.length; ++i) {
                                DocumentLoad.this.reloadCaretOffsets[i] = DocumentLoad.this.reloadOpenPanes[i].getCaretPosition();
                            }
                        }
                    }
                });
                this.this$0.activeReloadTask = DocumentOpenClose.RP.create((Runnable)this);
                this.this$0.activeReloadTask.schedule(0);
                success = true;
            }
            finally {
                if (!success) {
                    this.this$0.activeReload = null;
                    this.this$0.activeReloadTask = null;
                }
            }
        }

        void reloadUQEThrown(UserQuestionException uqe) {
            this.userQuestionExceptionInReload = true;
            UserQuestionExceptionHandler handler = new UserQuestionExceptionHandler(this.this$0.ces, (UserQuestionException)this.loadIOException){

                @Override
                protected StyledDocument openDocument() throws IOException {
                    DocumentLoad.this.loadIOException = null;
                    DocumentLoad.this.this$0.activeReloadTask.schedule(0);
                    return DocumentLoad.this.loadDoc;
                }

                @Override
                protected void openRefused() {
                    DocumentLoad.this.loadIOException = null;
                    DocumentLoad.this.skipInputStreamReading = true;
                    DocumentLoad.this.this$0.activeReloadTask.schedule(0);
                }
            };
            handler.runInEDT();
        }

        void fireDocumentChange() {
            if (this.loadSuccess) {
                this.this$0.ces.fireDocumentChange(this.loadDoc, false);
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(200);
            sb.append(DocumentOpenClose.getSimpleName(this)).append(": reload=").append(this.reload).append(", loadDoc=").append(DocumentOpenClose.getSimpleName(this.loadDoc)).append(", loadSuccess=").append(this.loadSuccess);
            if (this.reload && this.reloadOpenPanes != null) {
                sb.append(", reloadOpenPanes.length=").append(this.reloadOpenPanes.length);
            }
            return sb.toString();
        }

    }

}

