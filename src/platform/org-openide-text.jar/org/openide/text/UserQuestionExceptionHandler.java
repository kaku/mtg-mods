/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.Mutex
 *  org.openide.util.UserQuestionException
 */
package org.openide.text;

import java.io.IOException;
import javax.swing.text.StyledDocument;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.Exceptions;
import org.openide.util.Mutex;
import org.openide.util.UserQuestionException;

class UserQuestionExceptionHandler
implements Runnable {
    private final CloneableEditorSupport ces;
    private UserQuestionException uqe;
    private StyledDocument doc;

    UserQuestionExceptionHandler(CloneableEditorSupport ces, UserQuestionException uqe) {
        this.ces = ces;
        this.uqe = uqe;
    }

    void runInEDT() {
        Mutex.EVENT.readAccess((Runnable)this);
    }

    @Override
    public void run() {
        this.handleUserQuestionException();
    }

    /*
     * Exception decompiling
     */
    boolean handleUserQuestionException() {
        // This method has failed to decompile.  When submitting a bug report, please provide this stack trace, and (if you hold appropriate legal rights) the relevant class file.
        // org.benf.cfr.reader.util.ConfusedCFRException: Tried to end blocks [0[TRYBLOCK]], but top level block is 8[CATCHBLOCK]
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.processEndingBlocks(Op04StructuredStatement.java:397)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.buildNestedBlocks(Op04StructuredStatement.java:449)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op03SimpleStatement.createInitialStructuredBlock(Op03SimpleStatement.java:2877)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisInner(CodeAnalyser.java:825)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisOrWrapFail(CodeAnalyser.java:217)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysis(CodeAnalyser.java:162)
        // org.benf.cfr.reader.entities.attributes.AttributeCode.analyse(AttributeCode.java:95)
        // org.benf.cfr.reader.entities.Method.analyse(Method.java:355)
        // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:768)
        // org.benf.cfr.reader.entities.ClassFile.analyseTop(ClassFile.java:700)
        // org.benf.cfr.reader.Main.doJar(Main.java:134)
        // org.benf.cfr.reader.Main.main(Main.java:189)
        throw new IllegalStateException("Decompilation failed");
    }

    protected StyledDocument openDocument() throws IOException {
        return this.ces.openDocument();
    }

    protected void handleStart() {
    }

    protected void handleEnd() {
    }

    protected void opened(StyledDocument openDoc) {
    }

    protected void openRefused() {
    }

    protected void handleIOException(IOException ex) {
        Exceptions.printStackTrace((Throwable)ex);
    }

    protected void handleRuntimeException(RuntimeException ex) {
        Exceptions.printStackTrace((Throwable)ex);
    }

    public final StyledDocument getDocument() {
        return this.doc;
    }
}

