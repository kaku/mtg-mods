/*
 * Decompiled with CFR 0_118.
 */
package org.openide.text;

import java.awt.datatransfer.DataFlavor;
import javax.swing.text.JTextComponent;
import org.openide.text.QuietEditorPane;

public interface ActiveEditorDrop {
    public static final DataFlavor FLAVOR = QuietEditorPane.constructActiveEditorDropFlavor();

    public boolean handleTransfer(JTextComponent var1);
}

