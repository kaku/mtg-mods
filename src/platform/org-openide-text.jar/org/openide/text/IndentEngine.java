/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ServiceType
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package org.openide.text;

import java.io.Writer;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.openide.ServiceType;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

@Deprecated
public abstract class IndentEngine
extends ServiceType {
    private static final long serialVersionUID = -8548906260608507035L;
    private static Map<String, IndentEngine> map = new HashMap<String, IndentEngine>(7);
    private static IndentEngine INSTANCE = null;

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public abstract int indentLine(Document var1, int var2);

    public abstract int indentNewLine(Document var1, int var2);

    public abstract Writer createWriter(Document var1, int var2, Writer var3);

    protected boolean acceptMimeType(String mime) {
        return false;
    }

    @Deprecated
    public static synchronized void register(String mime, IndentEngine eng) {
        map.put(mime, eng);
    }

    public static Enumeration<? extends IndentEngine> indentEngines() {
        return Collections.enumeration(Lookup.getDefault().lookupAll(IndentEngine.class));
    }

    public static synchronized IndentEngine find(String mime) {
        IndentEngine eng;
        Enumeration<? extends IndentEngine> en = IndentEngine.indentEngines();
        while (en.hasMoreElements()) {
            eng = en.nextElement();
            if (!eng.acceptMimeType(mime)) continue;
            return eng;
        }
        eng = map.get(mime);
        if (eng != null) {
            return eng;
        }
        return IndentEngine.getDefault();
    }

    public static IndentEngine find(Document doc) {
        Object o = doc.getProperty("indentEngine");
        if (o instanceof IndentEngine) {
            return (IndentEngine)((Object)o);
        }
        o = doc.getProperty("mimeType");
        String s = o instanceof String ? (String)o : "text/plain";
        return IndentEngine.find(s);
    }

    public static synchronized IndentEngine getDefault() {
        if (INSTANCE == null) {
            INSTANCE = new Default();
        }
        return INSTANCE;
    }

    private static final class Default
    extends IndentEngine {
        private static final long serialVersionUID = 4493180326470838469L;

        Default() {
        }

        @Override
        public int indentLine(Document doc, int offset) {
            return offset;
        }

        @Override
        public int indentNewLine(Document doc, int offset) {
            try {
                doc.insertString(offset, "\n", null);
            }
            catch (BadLocationException ex) {
                // empty catch block
            }
            return offset + 1;
        }

        @Override
        public Writer createWriter(Document doc, int offset, Writer writer) {
            return writer;
        }

        @Override
        protected boolean acceptMimeType(String mime) {
            return true;
        }

        @Override
        public HelpCtx getHelpCtx() {
            return new HelpCtx(Default.class);
        }
    }

}

