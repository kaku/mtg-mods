/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.WeakListeners
 *  org.openide.windows.CloneableOpenSupport
 *  org.openide.windows.CloneableOpenSupportRedirector
 *  org.openide.windows.CloneableTopComponent
 */
package org.openide.text;

import java.lang.ref.Reference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.DocumentLine;
import org.openide.text.Line;
import org.openide.text.PositionRef;
import org.openide.util.Lookup;
import org.openide.util.WeakListeners;
import org.openide.windows.CloneableOpenSupport;
import org.openide.windows.CloneableOpenSupportRedirector;
import org.openide.windows.CloneableTopComponent;

final class EditorSupportLineSet
extends DocumentLine.Set {
    private CloneableEditorSupport support;

    public EditorSupportLineSet(CloneableEditorSupport support, StyledDocument doc) {
        super(doc, support);
        this.support = support;
    }

    @Override
    Map<Line, Reference<Line>> findWeakHashMap() {
        return this.support.findWeakHashMap();
    }

    @Override
    public Line createLine(int offset) {
        PositionRef ref = new PositionRef(this.support.getPositionManager(), offset, Position.Bias.Forward);
        return new SupportLine(this.support.getLookup(), ref, this.support);
    }

    private static final class COSHack {
        private static final Method redirectMethod;

        private COSHack() {
        }

        private static CloneableEditorSupport getCloneableEditorSupport(PositionRef pos) {
            CloneableEditorSupport orig = pos.getCloneableEditorSupport();
            if (orig == null || redirectMethod == null) {
                return orig;
            }
            try {
                Object result = redirectMethod.invoke(null, new Object[]{orig});
                if (result instanceof CloneableEditorSupport) {
                    return (CloneableEditorSupport)((Object)result);
                }
            }
            catch (IllegalAccessException ex) {
            }
            catch (IllegalArgumentException ex) {
            }
            catch (SecurityException ex) {
            }
            catch (InvocationTargetException ex) {
                // empty catch block
            }
            return orig;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        static {
            Method m = null;
            try {
                m = CloneableOpenSupportRedirector.class.getDeclaredMethod("findRedirect", CloneableOpenSupport.class);
                m.setAccessible(true);
            }
            catch (NoSuchMethodException ex) {}
            catch (SecurityException ex) {}
            finally {
                redirectMethod = m;
            }
        }
    }

    static class Closed
    extends Line.Set
    implements ChangeListener {
        private CloneableEditorSupport support;
        private Line.Set delegate;

        public Closed(CloneableEditorSupport support) {
            this.support = support;
            support.addChangeListener(WeakListeners.change((ChangeListener)this, (Object)((Object)support)));
        }

        @Override
        Map<Line, Reference<Line>> findWeakHashMap() {
            return this.support.findWeakHashMap();
        }

        @Override
        public List<? extends Line> getLines() {
            if (this.delegate != null) {
                return this.delegate.getLines();
            }
            return new ArrayList();
        }

        @Override
        public Line getOriginal(int line) throws IndexOutOfBoundsException {
            if (this.delegate != null) {
                return this.delegate.getOriginal(line);
            }
            return this.getCurrent(line);
        }

        @Override
        public Line getCurrent(int line) throws IndexOutOfBoundsException {
            PositionRef ref = new PositionRef(this.support.getPositionManager(), line, 0, Position.Bias.Forward);
            Lookup obj = this.support.getLookup();
            return this.registerLine(new SupportLine(obj, ref, this.support));
        }

        @Override
        public synchronized void stateChanged(ChangeEvent ev) {
            if (this.delegate == null) {
                StyledDocument doc = this.support.getDocument();
                if (doc != null) {
                    this.delegate = new EditorSupportLineSet(this.support, doc);
                }
            } else if (this.support.getDocument() == null) {
                this.delegate = null;
            }
        }
    }

    private static final class SupportLine
    extends DocumentLine {
        static final long serialVersionUID = 7282223299866986051L;

        public SupportLine(Lookup obj, PositionRef ref, CloneableEditorSupport support) {
            super(obj, ref);
        }

        @Deprecated
        @Override
        public void show(int kind, int column) {
            CloneableEditorSupport.Pane editor;
            CloneableEditorSupport support = SupportLine.getCloneableEditorSupport(this.pos);
            if (kind == 0 && !support.isDocumentLoaded()) {
                return;
            }
            if (kind == 4 || kind == 5) {
                editor = support.openReuse(this.pos, column, kind);
            } else {
                editor = support.openAt(this.pos, column);
                if (kind == 3) {
                    editor.getComponent().toFront();
                }
            }
            if (kind != 0 && kind != 1) {
                editor.getComponent().requestActive();
            }
        }

        @Override
        public void show(Line.ShowOpenType openType, Line.ShowVisibilityType visibilityType, int column) {
            CloneableEditorSupport support = SupportLine.getCloneableEditorSupport(this.pos);
            if (openType == Line.ShowOpenType.NONE && !support.isDocumentLoaded()) {
                return;
            }
            CloneableEditorSupport.Pane editor = null;
            if (openType == Line.ShowOpenType.REUSE || openType == Line.ShowOpenType.REUSE_NEW) {
                editor = support.openReuse(this.pos, column, openType);
            } else if (openType == Line.ShowOpenType.OPEN || openType == Line.ShowOpenType.NONE) {
                editor = support.openAt(this.pos, column);
            }
            if (editor != null) {
                if (visibilityType == Line.ShowVisibilityType.FRONT) {
                    editor.getComponent().requestVisible();
                    editor.getComponent().toFront();
                } else if (visibilityType == Line.ShowVisibilityType.FOCUS) {
                    editor.getComponent().requestActive();
                }
            }
        }

        @Override
        public Line.Part createPart(int column, int length) {
            DocumentLine.Part part = new DocumentLine.Part(this, new PositionRef(SupportLine.getCloneableEditorSupport(this.pos).getPositionManager(), this.pos.getOffset() + column, Position.Bias.Forward), length);
            this.addLinePart(part);
            return part;
        }

        @Override
        public String getDisplayName() {
            CloneableEditorSupport support = SupportLine.getCloneableEditorSupport(this.pos);
            return support.messageLine(this);
        }

        public String toString() {
            return "SupportLine@" + Integer.toHexString(System.identityHashCode(this)) + " at line: " + this.getLineNumber();
        }

        private static CloneableEditorSupport getCloneableEditorSupport(PositionRef pos) {
            return COSHack.getCloneableEditorSupport(pos);
        }
    }

}

