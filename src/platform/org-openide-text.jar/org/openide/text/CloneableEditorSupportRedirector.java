/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.openide.text;

import java.util.Collection;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.Map;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.Lookup;

public abstract class CloneableEditorSupportRedirector {
    private static final ThreadLocal<Map<Lookup, CloneableEditorSupport>> CHECKED = new ThreadLocal<T>();

    protected abstract CloneableEditorSupport redirect(Lookup var1);

    static CloneableEditorSupport findRedirect(CloneableEditorSupport one) {
        return CloneableEditorSupportRedirector.findRedirect(one, false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static CloneableEditorSupport findRedirect(CloneableEditorSupport one, boolean check) {
        Map<Lookup, CloneableEditorSupport> all = CHECKED.get();
        if (all == null) {
            all = new IdentityHashMap<Lookup, CloneableEditorSupport>();
            CHECKED.set(all);
        }
        Lookup lkp = one.getLookup();
        try {
            if (check && all.containsKey((Object)lkp)) {
                CloneableEditorSupport cloneableEditorSupport = null;
                return cloneableEditorSupport;
            }
            all.put(lkp, one);
            for (CloneableEditorSupportRedirector r : Lookup.getDefault().lookupAll(CloneableEditorSupportRedirector.class)) {
                CloneableEditorSupport ces = r.redirect(lkp);
                if (ces == null || ces == one) continue;
                CloneableEditorSupport cloneableEditorSupport = ces;
                return cloneableEditorSupport;
            }
            Iterator<E> i$ = null;
            return i$;
        }
        finally {
            all.remove((Object)lkp);
        }
    }
}

