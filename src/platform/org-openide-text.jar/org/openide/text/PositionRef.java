/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.windows.CloneableOpenSupport
 */
package org.openide.text;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.NbDocument;
import org.openide.util.RequestProcessor;
import org.openide.windows.CloneableOpenSupport;

public final class PositionRef
implements Serializable {
    static final long serialVersionUID = -4931337398907426948L;
    private transient Manager.Kind kind;
    private Manager manager;
    private boolean insertAfter;

    PositionRef(Manager manager, int offset, Position.Bias bias) {
        this(manager, new Manager.OffsetKind(offset, manager), bias);
    }

    PositionRef(Manager manager, int line, int column, Position.Bias bias) {
        this(manager, new Manager.LineKind(line, column, manager), bias);
    }

    private PositionRef(Manager manager, Manager.Kind kind, Position.Bias bias) {
        this.manager = manager;
        this.kind = kind;
        this.insertAfter = bias == Position.Bias.Backward;
        this.init();
    }

    private void init() {
        this.kind = this.manager.addPosition(this);
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeBoolean(this.insertAfter);
        out.writeObject(this.manager);
        this.kind.write(out);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        this.insertAfter = in.readBoolean();
        this.manager = (Manager)in.readObject();
        this.kind = this.manager.readKind(in);
        this.init();
    }

    public CloneableEditorSupport getCloneableEditorSupport() {
        return this.manager.getCloneableEditorSupport();
    }

    public Position.Bias getPositionBias() {
        return this.insertAfter ? Position.Bias.Backward : Position.Bias.Forward;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Position getPosition() throws IOException {
        StyledDocument doc = this.manager.getCloneableEditorSupport().getDocument();
        if (doc == null) {
            doc = this.manager.getCloneableEditorSupport().openDocument();
        }
        Object old = DOCUMENT.get();
        try {
            DOCUMENT.set(doc);
            Object object = this.manager.getLock();
            synchronized (object) {
                Manager.PositionKind p = this.kind.toMemory(this.insertAfter);
                Position position = p.pos;
                return position;
            }
        }
        finally {
            DOCUMENT.set(old);
        }
    }

    public int getOffset() {
        return this.kind.getOffset();
    }

    public int getLine() throws IOException {
        return this.kind.getLine();
    }

    public int getColumn() throws IOException {
        return this.kind.getColumn();
    }

    public String toString() {
        return "Pos[" + this.getOffset() + "]" + ", kind=" + this.kind;
    }

    static final class Manager
    implements Runnable,
    Serializable {
        private static ThreadLocal<Object> DOCUMENT = new ThreadLocal();
        static final long serialVersionUID = -4374030124265110801L;
        private transient ChainItem head;
        private transient ReferenceQueue<PositionRef> queue;
        private transient int counter;
        private transient RequestProcessor.Task sweepTask;
        private static final RequestProcessor RP = new RequestProcessor(PositionRef.class);
        private transient CloneableEditorSupport support;
        private transient Reference<StyledDocument> doc;
        private transient AtomicBoolean inMemory = new AtomicBoolean();

        public Manager(CloneableEditorSupport supp) {
            this.support = supp;
            this.init();
        }

        protected void init() {
            this.queue = new ReferenceQueue();
            this.head = new ChainItem(null, this.queue, null);
        }

        private Object getLock() {
            return this.support.getLock();
        }

        private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
            Object firstObject = in.readObject();
            CloneableEditorSupport.Env env = (CloneableEditorSupport.Env)firstObject;
            this.support = (CloneableEditorSupport)env.findCloneableOpenSupport();
            if (this.support == null) {
                throw new IOException();
            }
        }

        final Object readResolve() {
            return this.support.getPositionManager();
        }

        private void writeObject(ObjectOutputStream out) throws IOException {
            out.writeObject(this.support.cesEnv());
        }

        public CloneableEditorSupport getCloneableEditorSupport() {
            return this.support;
        }

        void documentOpened(Reference<StyledDocument> doc) {
            if (this.inMemory.compareAndSet(false, true)) {
                this.doc = doc;
                this.processPositions(true);
            }
        }

        void documentClosed() {
            if (this.inMemory.compareAndSet(true, false)) {
                this.processPositions(false);
                this.doc = null;
            }
        }

        private StyledDocument getDoc() {
            Object d = DOCUMENT.get();
            if (d instanceof StyledDocument) {
                return (StyledDocument)d;
            }
            if (d == this) {
                return null;
            }
            Reference<StyledDocument> w = this.doc;
            StyledDocument x = w == null ? null : w.get();
            return x;
        }

        private void processPositions(boolean toMemory) {
            while (this.queue.poll() != null) {
            }
            this.counter = 0;
            new DocumentRenderer(this, 9, toMemory).render();
        }

        private void checkQueue() {
            while (this.queue.poll() != null) {
                ++this.counter;
            }
            if (this.counter > 100) {
                this.counter = 0;
                if (this.sweepTask == null) {
                    this.sweepTask = RP.post((Runnable)this);
                } else if (this.sweepTask.isFinished()) {
                    this.sweepTask.schedule(0);
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public synchronized void run() {
            Object object = this.getLock();
            synchronized (object) {
                ChainItem previous = this.head;
                ChainItem ref = previous.next;
                while (ref != null) {
                    if (ref.get() == null) {
                        previous.next = ref.next;
                    } else {
                        previous = ref;
                    }
                    ref = ref.next;
                }
            }
        }

        Kind addPosition(PositionRef pos) {
            Kind kind = (Kind)new DocumentRenderer(this, 10, pos).renderToObject();
            this.checkQueue();
            return kind;
        }

        Kind readKind(DataInput is) throws IOException {
            int offset = is.readInt();
            int line = is.readInt();
            int column = is.readInt();
            if (offset == -1) {
                return new LineKind(line, column, this);
            }
            if (line == -1 || column == -1) {
                return new OffsetKind(offset, this);
            }
            return new OutKind(offset, line, column, this);
        }

        private static final class DocumentRenderer
        implements Runnable {
            private static final int KIND_TO_MEMORY = 0;
            private static final int POSITION_KIND_GET_LINE = 1;
            private static final int POSITION_KIND_GET_COLUMN = 2;
            private static final int POSITION_KIND_WRITE = 3;
            private static final int OUT_KIND_CONSTRUCTOR = 4;
            private static final int OFFSET_KIND_GET_LINE = 5;
            private static final int OFFSET_KIND_GET_COLUMN = 6;
            private static final int LINE_KIND_GET_OFFSET = 7;
            private static final int LINE_KIND_TO_MEMORY = 8;
            private static final int PROCESS_POSITIONS = 9;
            private static final int ADD_POSITION = 10;
            private final Manager mgr;
            private final int opCode;
            private Kind argKind;
            private boolean argInsertAfter;
            private boolean argToMemory;
            private int argInt;
            private Object retObject;
            private int retInt;
            private int argLine;
            private int argColumn;
            private PositionRef argPos;
            private StyledDocument argDoc;
            private IOException ioException;

            DocumentRenderer(Manager mgr, int opCode, Kind argKind) {
                this.mgr = mgr;
                this.opCode = opCode;
                this.argKind = argKind;
            }

            DocumentRenderer(Manager mgr, int opCode, Kind argKind, boolean argInsertAfter) {
                this(mgr, opCode, argKind);
                this.argInsertAfter = argInsertAfter;
            }

            DocumentRenderer(Manager mgr, int opCode, Kind argKind, int argInt) {
                this(mgr, opCode, argKind);
                this.argInt = argInt;
            }

            DocumentRenderer(Manager mgr, int opCode, Kind argKind, int argLine, int argColumn) {
                this(mgr, opCode, argKind);
                this.argLine = argLine;
                this.argColumn = argColumn;
            }

            DocumentRenderer(Manager mgr, int opCode, Kind argKind, int argLine, int argColumn, StyledDocument argDoc) {
                this(mgr, opCode, argKind, argLine, argColumn);
                this.argDoc = argDoc;
            }

            DocumentRenderer(Manager mgr, int opCode, Kind argKind, int argLine, int argColumn, boolean argInsertAfter) {
                this(mgr, opCode, argKind, argLine, argColumn);
                this.argInsertAfter = argInsertAfter;
            }

            DocumentRenderer(Manager mgr, int opCode, boolean toMemory) {
                this.mgr = mgr;
                this.opCode = opCode;
                this.argToMemory = toMemory;
            }

            DocumentRenderer(Manager mgr, int opCode, PositionRef argPos) {
                this.mgr = mgr;
                this.opCode = opCode;
                this.argPos = argPos;
            }

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            void render() {
                block4 : {
                    StyledDocument d = this.mgr.getDoc();
                    Object prev = DOCUMENT.get();
                    try {
                        if (d != null) {
                            DOCUMENT.set(d);
                            d.render(this);
                            break block4;
                        }
                        DOCUMENT.set(this.mgr);
                        this.run();
                    }
                    finally {
                        DOCUMENT.set(prev);
                    }
                }
            }

            Object renderToObjectIOE() throws IOException {
                Object o = this.renderToObject();
                if (this.ioException != null) {
                    throw this.ioException;
                }
                return o;
            }

            Object renderToObject() {
                this.render();
                return this.retObject;
            }

            int renderToIntIOE() throws IOException {
                int i = this.renderToInt();
                if (this.ioException != null) {
                    throw this.ioException;
                }
                return i;
            }

            int renderToInt() {
                this.render();
                return this.retInt;
            }

            int getLine() {
                return this.argLine;
            }

            int getColumn() {
                return this.argColumn;
            }

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                try {
                    switch (this.opCode) {
                        case 0: {
                            Position p;
                            int offset = this.argKind.getOffset();
                            if (this.argKind.getClass() == OutKind.class) {
                                try {
                                    int line = this.argKind.getLine();
                                    int col = this.argKind.getColumn();
                                    Element lineRoot = NbDocument.findLineRootElement(this.mgr.getDoc());
                                    if (line < lineRoot.getElementCount()) {
                                        Element lineElem = lineRoot.getElement(line);
                                        int lineStartOffset = lineElem.getStartOffset();
                                        int lineLen = lineElem.getEndOffset() - lineStartOffset;
                                        if (lineLen >= 1) {
                                            col = Math.min(col, lineLen - 1);
                                            offset = lineStartOffset + col;
                                        }
                                    }
                                }
                                catch (IOException e) {
                                    // empty catch block
                                }
                            }
                            try {
                                p = NbDocument.createPosition(this.mgr.getDoc(), offset, this.argInsertAfter ? Position.Bias.Backward : Position.Bias.Forward);
                            }
                            catch (BadLocationException e) {
                                p = this.mgr.getDoc().getEndPosition();
                            }
                            this.retObject = new PositionKind(p, this.mgr);
                            break;
                        }
                        case 1: {
                            this.retInt = NbDocument.findLineNumber(this.mgr.getDoc(), this.argKind.getOffset());
                            break;
                        }
                        case 2: {
                            this.retInt = NbDocument.findLineColumn(this.mgr.getDoc(), this.argKind.getOffset());
                            break;
                        }
                        case 3: 
                        case 4: {
                            this.retInt = this.argKind.getOffset();
                            this.argLine = this.argKind.getLine();
                            this.argColumn = this.argKind.getColumn();
                            break;
                        }
                        case 5: {
                            this.retInt = NbDocument.findLineNumber(this.mgr.getCloneableEditorSupport().openDocument(), this.argInt);
                            break;
                        }
                        case 6: {
                            this.retInt = NbDocument.findLineColumn(this.mgr.getCloneableEditorSupport().openDocument(), this.argInt);
                            break;
                        }
                        case 7: {
                            this.retInt = NbDocument.findLineOffset(this.argDoc, this.argLine) + this.argColumn;
                            break;
                        }
                        case 8: {
                            try {
                                this.retObject = NbDocument.createPosition(this.mgr.getDoc(), NbDocument.findLineOffset(this.mgr.getDoc(), this.argLine) + this.argColumn, this.argInsertAfter ? Position.Bias.Backward : Position.Bias.Forward);
                            }
                            catch (BadLocationException e) {
                                this.retObject = this.mgr.getDoc().getEndPosition();
                            }
                            catch (IndexOutOfBoundsException e) {
                                this.retObject = this.mgr.getDoc().getEndPosition();
                            }
                            break;
                        }
                        case 9: {
                            Object e = this.mgr.getLock();
                            synchronized (e) {
                                ChainItem previous = this.mgr.head;
                                ChainItem ref = previous.next;
                                while (ref != null) {
                                    PositionRef pos = (PositionRef)ref.get();
                                    if (pos == null) {
                                        previous.next = ref.next;
                                    } else {
                                        if (this.argToMemory) {
                                            pos.kind = pos.kind.toMemory(pos.insertAfter);
                                        } else {
                                            pos.kind = pos.kind.fromMemory();
                                        }
                                        previous = ref;
                                    }
                                    ref = ref.next;
                                }
                                break;
                            }
                        }
                        case 10: {
                            this.mgr.support.howToReproduceDeadlock40766(true);
                            Object e = this.mgr.getLock();
                            synchronized (e) {
                                this.mgr.support.howToReproduceDeadlock40766(false);
                                Manager.access$500((Manager)this.mgr).next = new ChainItem(this.argPos, this.mgr.queue, Manager.access$500((Manager)this.mgr).next);
                                this.retObject = this.mgr.getDoc() == null ? this.argPos.kind : this.argPos.kind.toMemory(this.argPos.insertAfter);
                                break;
                            }
                        }
                        default: {
                            throw new IllegalStateException();
                        }
                    }
                }
                catch (IOException e) {
                    this.ioException = e;
                }
            }
        }

        private static final class LineKind
        extends Kind {
            private int line;
            private int column;

            public LineKind(int line, int column, Manager mgr) {
                super(mgr);
                if (line < 0 || column < 0) {
                    throw new IndexOutOfBoundsException("Illegal LineKind[line=" + line + ",column=" + column + "] in " + mgr.getDoc() + " used by " + (Object)((Object)mgr.support) + ".");
                }
                this.line = line;
                this.column = column;
            }

            @Override
            public int getOffset() {
                try {
                    StyledDocument doc = this.mgr.getCloneableEditorSupport().getDocument();
                    if (doc == null) {
                        doc = this.mgr.getCloneableEditorSupport().openDocument();
                    }
                    try {
                        int retOffset = new DocumentRenderer(this.mgr, 7, (Kind)this, this.line, this.column, doc).renderToInt();
                        return retOffset;
                    }
                    catch (IndexOutOfBoundsException e) {
                        return doc.getEndPosition().getOffset();
                    }
                }
                catch (IOException e) {
                    return 0;
                }
            }

            @Override
            public int getLine() throws IOException {
                return this.line;
            }

            @Override
            public int getColumn() throws IOException {
                return this.column;
            }

            @Override
            public void write(DataOutput os) throws IOException {
                if (this.line < 0 || this.column < 0) {
                    throw new IOException("Illegal LineKind[line=" + this.line + ",column=" + this.column + "] in " + this.mgr.getDoc() + " used by " + (Object)((Object)this.mgr.support) + ".");
                }
                os.writeInt(-1);
                os.writeInt(this.line);
                os.writeInt(this.column);
            }

            @Override
            public PositionKind toMemory(boolean insertAfter) {
                Position p = (Position)new DocumentRenderer(this.mgr, 8, (Kind)this, this.line, this.column, insertAfter).renderToObject();
                return new PositionKind(p, this.mgr);
            }
        }

        private static final class OffsetKind
        extends Kind {
            private int offset;

            public OffsetKind(int offset, Manager mgr) {
                super(mgr);
                if (offset < 0) {
                    throw new IndexOutOfBoundsException("Illegal OffsetKind[offset=" + offset + "] in " + mgr.getDoc() + " used by " + (Object)((Object)mgr.support) + ".");
                }
                this.offset = offset;
            }

            @Override
            public int getOffset() {
                return this.offset;
            }

            @Override
            public int getLine() throws IOException {
                this.mgr.getCloneableEditorSupport().openDocument();
                return new DocumentRenderer(this.mgr, 5, (Kind)this, this.offset).renderToIntIOE();
            }

            @Override
            public int getColumn() throws IOException {
                this.mgr.getCloneableEditorSupport().openDocument();
                return new DocumentRenderer(this.mgr, 6, (Kind)this, this.offset).renderToIntIOE();
            }

            @Override
            public void write(DataOutput os) throws IOException {
                if (this.offset < 0) {
                    throw new IOException("Illegal OffsetKind[offset=" + this.offset + "] in " + this.mgr.getDoc() + " used by " + (Object)((Object)this.mgr.support) + ".");
                }
                os.writeInt(this.offset);
                os.writeInt(-1);
                os.writeInt(-1);
            }
        }

        private static final class OutKind
        extends Kind {
            private int offset;
            private int line;
            private int column;

            public OutKind(PositionKind kind, Manager mgr) {
                super(mgr);
                DocumentRenderer renderer = new DocumentRenderer(mgr, 4, kind);
                int offset = renderer.renderToInt();
                int line = renderer.getLine();
                int column = renderer.getColumn();
                if (offset < 0 || line < 0 || column < 0) {
                    throw new IndexOutOfBoundsException("Illegal OutKind[offset=" + offset + ",line=" + line + ",column=" + column + "] in " + mgr.getDoc() + " used by " + (Object)((Object)mgr.support) + ".");
                }
                this.offset = offset;
                this.line = line;
                this.column = column;
            }

            OutKind(int offset, int line, int column, Manager mgr) {
                super(mgr);
                this.offset = offset;
                this.line = line;
                this.column = column;
            }

            @Override
            public int getOffset() {
                return this.offset;
            }

            @Override
            public int getLine() {
                return this.line;
            }

            @Override
            public int getColumn() {
                return this.column;
            }

            @Override
            public void write(DataOutput os) throws IOException {
                if (this.offset < 0 || this.line < 0 || this.column < 0) {
                    throw new IOException("Illegal OutKind[offset=" + this.offset + ",line=" + this.line + ",column=" + this.column + "] in " + this.mgr.getDoc() + " used by " + (Object)((Object)this.mgr.support) + ".");
                }
                os.writeInt(this.offset);
                os.writeInt(this.line);
                os.writeInt(this.column);
            }
        }

        private static final class PositionKind
        extends Kind {
            private Position pos;

            public PositionKind(Position pos, Manager mgr) {
                super(mgr);
                this.pos = pos;
            }

            @Override
            public int getOffset() {
                return this.pos.getOffset();
            }

            @Override
            public int getLine() {
                return new DocumentRenderer(this.mgr, 1, this).renderToInt();
            }

            @Override
            public int getColumn() {
                return new DocumentRenderer(this.mgr, 2, this).renderToInt();
            }

            @Override
            public void write(DataOutput os) throws IOException {
                DocumentRenderer renderer = new DocumentRenderer(this.mgr, 3, this);
                int offset = renderer.renderToIntIOE();
                int line = renderer.getLine();
                int column = renderer.getColumn();
                if (offset < 0 || line < 0 || column < 0) {
                    throw new IOException("Illegal PositionKind: " + this.pos + "[offset=" + offset + ",line=" + line + ",column=" + column + "] in " + this.mgr.getDoc() + " used by " + (Object)((Object)this.mgr.support) + ".");
                }
                os.writeInt(offset);
                os.writeInt(line);
                os.writeInt(column);
            }

            @Override
            public PositionKind toMemory(boolean insertAfter) {
                return this;
            }

            @Override
            public Kind fromMemory() {
                return new OutKind(this, this.mgr);
            }
        }

        private static abstract class Kind {
            protected final Manager mgr;

            Kind(Manager mgr) {
                this.mgr = mgr;
            }

            public abstract int getOffset();

            public abstract int getLine() throws IOException;

            public abstract int getColumn() throws IOException;

            public abstract void write(DataOutput var1) throws IOException;

            public PositionKind toMemory(boolean insertAfter) {
                return (PositionKind)new DocumentRenderer(this.mgr, 0, this, insertAfter).renderToObject();
            }

            public Kind fromMemory() {
                return this;
            }
        }

        private static class ChainItem
        extends WeakReference<PositionRef> {
            ChainItem next;

            public ChainItem(PositionRef position, ReferenceQueue<PositionRef> queue, ChainItem next) {
                super(position, queue);
                this.next = next;
            }
        }

    }

}

