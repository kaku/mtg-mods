/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.WeakListeners
 */
package org.openide.text;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Element;
import javax.swing.text.StyledDocument;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.DocumentLine;
import org.openide.text.Line;
import org.openide.text.LineStruct;
import org.openide.text.NbDocument;
import org.openide.util.WeakListeners;

final class LineListener
implements DocumentListener {
    private int orig;
    private Reference<Element> rootRef;
    private int lines;
    private LineStruct struct = new LineStruct();
    CloneableEditorSupport support;

    public LineListener(StyledDocument doc, CloneableEditorSupport support) {
        Element root = NbDocument.findLineRootElement(doc);
        this.orig = this.lines = root.getElementCount();
        this.rootRef = new WeakReference<Element>(root);
        this.support = support;
        doc.addDocumentListener(WeakListeners.document((DocumentListener)this, (Object)doc));
    }

    public int getOriginalLineCount() {
        return this.orig;
    }

    public int getLine(int i) {
        return this.struct.convert(i, true);
    }

    public int getOld(int i) {
        return this.struct.convert(i, false);
    }

    @Override
    public void removeUpdate(DocumentEvent p0) {
        Element root = this.rootRef.get();
        int elem = root.getElementCount();
        int delta = this.lines - elem;
        this.lines = elem;
        StyledDocument doc = this.support.getDocument();
        if (doc == null) {
            return;
        }
        int lineNumber = NbDocument.findLineNumber(doc, p0.getOffset());
        if (delta > 0) {
            this.struct.deleteLines(lineNumber, delta);
        }
        if (this.support == null) {
            return;
        }
        Line.Set set = this.support.getLineSet();
        if (!(set instanceof DocumentLine.Set)) {
            return;
        }
        ((DocumentLine.Set)set).linesChanged(lineNumber, lineNumber + delta, p0);
        if (delta > 0) {
            ((DocumentLine.Set)set).linesMoved(lineNumber, elem);
        }
    }

    @Override
    public void changedUpdate(DocumentEvent p0) {
    }

    @Override
    public void insertUpdate(DocumentEvent p0) {
        Element root = this.rootRef.get();
        int elem = root.getElementCount();
        int delta = elem - this.lines;
        this.lines = elem;
        StyledDocument doc = this.support.getDocument();
        if (doc == null) {
            return;
        }
        int lineNumber = NbDocument.findLineNumber(doc, p0.getOffset());
        if (delta > 0) {
            this.struct.insertLines(lineNumber, delta);
        }
        if (this.support == null) {
            return;
        }
        Line.Set set = this.support.getLineSet();
        if (!(set instanceof DocumentLine.Set)) {
            return;
        }
        ((DocumentLine.Set)set).linesChanged(lineNumber, lineNumber, p0);
        if (delta > 0) {
            ((DocumentLine.Set)set).linesMoved(lineNumber, elem);
        }
    }
}

