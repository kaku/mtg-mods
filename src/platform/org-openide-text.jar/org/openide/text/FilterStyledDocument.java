/*
 * Decompiled with CFR 0_118.
 */
package org.openide.text;

import java.awt.Color;
import java.awt.Font;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Style;
import javax.swing.text.StyledDocument;
import org.openide.text.FilterDocument;

public class FilterStyledDocument
extends FilterDocument {
    public FilterStyledDocument(StyledDocument original) {
        super(original);
    }

    @Override
    public Style addStyle(String nm, Style parent) {
        return ((StyledDocument)this.original).addStyle(nm, parent);
    }

    @Override
    public void removeStyle(String nm) {
        ((StyledDocument)this.original).removeStyle(nm);
    }

    @Override
    public Style getStyle(String nm) {
        return ((StyledDocument)this.original).getStyle(nm);
    }

    @Override
    public void setCharacterAttributes(int offset, int length, AttributeSet s, boolean replace) {
        ((StyledDocument)this.original).setCharacterAttributes(offset, length, s, replace);
    }

    @Override
    public void setParagraphAttributes(int offset, int length, AttributeSet s, boolean replace) {
        ((StyledDocument)this.original).setParagraphAttributes(offset, length, s, replace);
    }

    @Override
    public void setLogicalStyle(int pos, Style s) {
        ((StyledDocument)this.original).setLogicalStyle(pos, s);
    }

    @Override
    public Style getLogicalStyle(int p) {
        return ((StyledDocument)this.original).getLogicalStyle(p);
    }

    @Override
    public Element getParagraphElement(int pos) {
        return ((StyledDocument)this.original).getParagraphElement(pos);
    }

    @Override
    public Element getCharacterElement(int pos) {
        return ((StyledDocument)this.original).getCharacterElement(pos);
    }

    @Override
    public Color getForeground(AttributeSet attr) {
        return ((StyledDocument)this.original).getForeground(attr);
    }

    @Override
    public Color getBackground(AttributeSet attr) {
        return ((StyledDocument)this.original).getBackground(attr);
    }

    @Override
    public Font getFont(AttributeSet attr) {
        return ((StyledDocument)this.original).getFont(attr);
    }
}

