/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.UndoRedo
 *  org.openide.awt.UndoRedo$Manager
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.windows.CloneableOpenSupport
 *  org.openide.windows.CloneableOpenSupport$Env
 *  org.openide.windows.CloneableTopComponent
 *  org.openide.windows.WindowManager
 *  org.openide.windows.Workspace
 */
package org.openide.text;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.NotSerializableException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.ObjectStreamException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.text.Caret;
import javax.swing.text.EditorKit;
import javax.swing.text.StyledDocument;
import org.netbeans.modules.openide.text.Installer;
import org.openide.awt.UndoRedo;
import org.openide.text.CloneableEditorInitializer;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.QuietEditorPane;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.windows.CloneableOpenSupport;
import org.openide.windows.CloneableTopComponent;
import org.openide.windows.WindowManager;
import org.openide.windows.Workspace;

public class CloneableEditor
extends CloneableTopComponent
implements CloneableEditorSupport.Pane {
    private static final String HELP_ID = "editing.editorwindow";
    static final long serialVersionUID = -185739563792410059L;
    protected JEditorPane pane;
    private CloneableEditorSupport support;
    private boolean componentCreated = false;
    private int cursorPosition = -1;
    private final boolean[] CLOSE_LAST_LOCK = new boolean[1];
    private Component customComponent;
    private CloneableEditorInitializer initializer;
    static final Logger LOG = Logger.getLogger("org.openide.text.CloneableEditor");

    public CloneableEditor() {
        this(null);
    }

    public CloneableEditor(CloneableEditorSupport support) {
        this(support, false);
    }

    public CloneableEditor(CloneableEditorSupport support, boolean associateLookup) {
        this.support = support;
        this.updateName();
        this._setCloseOperation();
        this.setMinimumSize(new Dimension(10, 10));
        if (associateLookup) {
            this.associateLookup(support.getLookup());
        }
    }

    private void _setCloseOperation() {
        this.setCloseOperation(0);
    }

    protected CloneableEditorSupport cloneableEditorSupport() {
        return this.support;
    }

    public int getPersistenceType() {
        return 1;
    }

    public HelpCtx getHelpCtx() {
        HelpCtx fromKit;
        EditorKit kit = this.support.cesKit();
        HelpCtx helpCtx = fromKit = kit == null ? null : HelpCtx.findHelp((Object)kit);
        if (fromKit != null) {
            return fromKit;
        }
        return new HelpCtx("editing.editorwindow");
    }

    public boolean canClose() {
        boolean result = super.canClose();
        return result;
    }

    protected void componentShowing() {
        super.componentShowing();
        this.initialize();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void initialize() {
        if (this.pane != null || this.discard()) {
            return;
        }
        QuietEditorPane tmp = new QuietEditorPane();
        tmp.getAccessibleContext().setAccessibleName(NbBundle.getMessage(CloneableEditor.class, (String)"ACS_CloneableEditor_QuietEditorPane", (Object)this.getName()));
        tmp.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CloneableEditor.class, (String)"ACSD_CloneableEditor_QuietEditorPane", (Object)this.getAccessibleContext().getAccessibleDescription()));
        tmp.putClientProperty("usedByCloneableEditor", true);
        this.pane = tmp;
        Object object = this.getInitializerLock();
        synchronized (object) {
            this.componentCreated = true;
            this.initializer = new CloneableEditorInitializer(this, this.support, this.pane);
        }
        this.initializer.start();
    }

    private Object getInitializerLock() {
        return CloneableEditorInitializer.edtRequests;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    boolean isInitializationRunning() {
        Object object = this.getInitializerLock();
        synchronized (object) {
            boolean running = this.initializer != null;
            return running;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    boolean isProvideUnfinishedPane() {
        Object object = this.getInitializerLock();
        synchronized (object) {
            return this.initializer != null && this.initializer.isProvideUnfinishedPane();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void markInitializationFinished(boolean success) {
        Object object = this.getInitializerLock();
        synchronized (object) {
            this.initializer = null;
            if (!success) {
                this.pane = null;
            }
            CloneableEditorInitializer.notifyEDTRequestsMonitor();
        }
    }

    protected final void initializeBySupport() {
        this.cloneableEditorSupport().initializeCloneableEditor(this);
    }

    void setCustomComponent(Component customComponent) {
        this.customComponent = customComponent;
    }

    int getCursorPosition() {
        return this.cursorPosition;
    }

    void setCursorPosition(int cursorPosition) {
        this.cursorPosition = cursorPosition;
    }

    protected CloneableTopComponent createClonedObject() {
        return this.support.createCloneableTopComponent();
    }

    protected void componentOpened() {
        super.componentOpened();
        CloneableEditorSupport ces = this.cloneableEditorSupport();
        if (ces != null) {
            ces.firePropertyChange("openedPanes", null, null);
            StyledDocument d = ces.getDocument();
            if (d != null) {
                String mimeType = (String)d.getProperty("mimeType");
                Installer.add(mimeType);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void componentClosed() {
        if (this.pane != null) {
            this.pane.setEditorKit(null);
            this.pane.putClientProperty("usedByCloneableEditor", false);
        }
        Object object = this.getInitializerLock();
        synchronized (object) {
            this.customComponent = null;
            this.pane = null;
            this.componentCreated = false;
        }
        super.componentClosed();
        CloneableEditorSupport ces = this.cloneableEditorSupport();
        if (ces != null) {
            ces.firePropertyChange("openedPanes", null, null);
        }
        if (ces.getAnyEditor() == null) {
            ces.close(false);
        }
    }

    protected boolean closeLast() {
        return this.closeLast(true);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final boolean closeLast(boolean ask) {
        if (ask && !this.support.canClose()) {
            return false;
        }
        boolean[] arrbl = this.CLOSE_LAST_LOCK;
        synchronized (arrbl) {
            if (this.CLOSE_LAST_LOCK[0]) {
                this.CLOSE_LAST_LOCK[0] = false;
            } else {
                this.support.notifyClosed();
            }
        }
        if (this.support.getLastSelected() == this) {
            this.support.setLastSelected(null);
        }
        return true;
    }

    public UndoRedo getUndoRedo() {
        return this.support.getUndoRedo();
    }

    public Action[] getActions() {
        ArrayList<Action> actions = new ArrayList<Action>(Arrays.asList(super.getActions()));
        actions.add(null);
        actions.addAll(Utilities.actionsForPath((String)"Editors/TabActions"));
        return actions.toArray(new Action[actions.size()]);
    }

    @Deprecated
    public void requestFocus() {
        super.requestFocus();
        if (this.pane != null) {
            if (this.customComponent != null && !SwingUtilities.isDescendingFrom(this.pane, this.customComponent)) {
                this.customComponent.requestFocus();
            } else {
                this.pane.requestFocus();
            }
        }
    }

    @Deprecated
    public boolean requestFocusInWindow() {
        super.requestFocusInWindow();
        if (this.pane != null) {
            if (this.customComponent != null && !SwingUtilities.isDescendingFrom(this.pane, this.customComponent)) {
                return this.customComponent.requestFocusInWindow();
            }
            return this.pane.requestFocusInWindow();
        }
        return false;
    }

    @Deprecated
    public boolean requestDefaultFocus() {
        if (this.customComponent != null && !SwingUtilities.isDescendingFrom(this.pane, this.customComponent)) {
            return this.customComponent.requestFocusInWindow();
        }
        if (this.pane != null) {
            return this.pane.requestFocusInWindow();
        }
        return false;
    }

    public Dimension getPreferredSize() {
        Rectangle bounds = WindowManager.getDefault().getCurrentWorkspace().getBounds();
        return new Dimension(bounds.width / 2, bounds.height / 2);
    }

    public void open() {
        boolean wasNull = this.getClientProperty((Object)"TopComponentAllowDockAnywhere") == null;
        super.open();
        if (wasNull) {
            this.putClientProperty((Object)"TopComponentAllowDockAnywhere", (Object)null);
        }
    }

    protected void componentActivated() {
        this.support.setLastSelected(this);
    }

    @Override
    public void updateName() {
        final CloneableEditorSupport ces = this.cloneableEditorSupport();
        if (ces != null) {
            Mutex.EVENT.writeAccess(new Runnable(){

                @Override
                public void run() {
                    String name = ces.messageHtmlName();
                    CloneableEditor.this.setHtmlDisplayName(name);
                    name = ces.messageName();
                    CloneableEditor.this.setDisplayName(name);
                    CloneableEditor.this.setName(name);
                    CloneableEditor.this.setToolTipText(ces.messageToolTip());
                }
            });
        }
    }

    protected String preferredID() {
        CloneableEditorSupport ces = this.cloneableEditorSupport();
        if (ces != null) {
            return ces.documentID();
        }
        return "";
    }

    public void writeExternal(ObjectOutput out) throws IOException {
        super.writeExternal(out);
        out.writeObject(this.support != null ? this.support.cesEnv() : null);
        int pos = 0;
        JEditorPane p = this.pane;
        if (p != null) {
            Caret caret = p.getCaret();
            if (caret != null) {
                pos = caret.getDot();
            } else if (p instanceof QuietEditorPane) {
                int lastPos = ((QuietEditorPane)p).getLastPosition();
                if (lastPos == -1) {
                    Logger.getLogger(CloneableEditor.class.getName()).log(Level.WARNING, null, new IllegalStateException("Pane=" + p + "was not initialized yet!"));
                } else {
                    pos = lastPos;
                }
            } else {
                StyledDocument doc;
                StyledDocument styledDocument = doc = this.support != null ? this.support.getDocument() : null;
                if (doc != null) {
                    Logger.getLogger(CloneableEditor.class.getName()).log(Level.WARNING, null, new IllegalStateException("Caret is null in editor pane=" + p + "\nsupport=" + (Object)((Object)this.support) + "\ndoc=" + doc));
                }
            }
        }
        out.writeObject(new Integer(pos));
        out.writeBoolean(this.getLookup() == this.support.getLookup());
    }

    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        boolean associate;
        super.readExternal(in);
        Object firstObject = in.readObject();
        if (firstObject instanceof CloneableOpenSupport.Env) {
            CloneableOpenSupport.Env env = (CloneableOpenSupport.Env)firstObject;
            CloneableOpenSupport os = env.findCloneableOpenSupport();
            this.support = (CloneableEditorSupport)os;
        }
        int offset = (Integer)in.readObject();
        if (!this.discard()) {
            this.cursorPosition = offset;
        }
        this.updateName();
        this.componentCreated = true;
        if (in.available() > 0 && (associate = in.readBoolean()) && this.support != null) {
            this.associateLookup(this.support.getLookup());
        }
    }

    protected Object writeReplace() throws ObjectStreamException {
        if (this.discard()) {
            throw new NotSerializableException("Serializing component is invalid: " + this);
        }
        return super.writeReplace();
    }

    protected Object readResolve() throws ObjectStreamException {
        if (this.discard()) {
            throw new InvalidObjectException("Deserialized component is invalid: " + this);
        }
        this.initializeBySupport();
        return this;
    }

    private boolean discard() {
        return this.support == null || !this.support.cesEnv().isValid();
    }

    @Override
    public CloneableTopComponent getComponent() {
        return this;
    }

    boolean isEditorPaneReady() {
        assert (SwingUtilities.isEventDispatchThread());
        return this.isEditorPaneReadyImpl();
    }

    boolean isEditorPaneReadyTest() {
        return this.isEditorPaneReadyImpl();
    }

    private boolean isEditorPaneReadyImpl() {
        return this.pane != null && !this.isInitializationRunning();
    }

    @Override
    public JEditorPane getEditorPane() {
        assert (SwingUtilities.isEventDispatchThread());
        if (!this.componentCreated) {
            return null;
        }
        if (CloneableEditorInitializer.modalDialog) {
            LOG.log(Level.WARNING, "AWT is blocked by modal dialog. Return null from CloneableEditor.getEditorPane. Please report this to IZ.");
            LOG.log(Level.WARNING, "support:" + this.support.getClass().getName());
            Exception ex = new Exception();
            StringWriter sw = new StringWriter(500);
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            LOG.log(Level.WARNING, sw.toString());
            return null;
        }
        this.initialize();
        CloneableEditorInitializer.waitForFinishedInitialization(this);
        return this.pane;
    }

    @Override
    public void ensureVisible() {
        this.open();
        this.requestVisible();
    }

}

