/*
 * Decompiled with CFR 0_118.
 */
package org.openide.text;

import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import org.openide.text.UndoRedoManager;

final class WrapUndoEdit
implements UndoableEdit {
    final UndoRedoManager undoRedoManager;
    private UndoableEdit delegate;

    WrapUndoEdit(UndoRedoManager undoRedoManager, UndoableEdit delegate) {
        assert (delegate != null);
        this.undoRedoManager = undoRedoManager;
        this.delegate = delegate;
    }

    UndoableEdit delegate() {
        return this.delegate;
    }

    void setDelegate(UndoableEdit delegate) {
        this.delegate = delegate;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void undo() throws CannotUndoException {
        this.undoRedoManager.checkLogOp("WrapUndoEdit.undo", this);
        boolean savepoint = this.undoRedoManager.isAtSavepoint();
        if (savepoint) {
            this.undoRedoManager.beforeUndoAtSavepoint(this);
        }
        boolean done = false;
        try {
            this.delegate.undo();
            done = true;
            this.undoRedoManager.afterUndoCheck(this);
        }
        finally {
            if (!done && savepoint) {
                this.undoRedoManager.delegateUndoFailedAtSavepoint(this);
            }
        }
    }

    @Override
    public boolean canUndo() {
        return this.delegate.canUndo();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void redo() throws CannotRedoException {
        this.undoRedoManager.checkLogOp("WrapUndoEdit.redo", this);
        boolean savepoint = this.undoRedoManager.isAtSavepoint();
        if (savepoint) {
            this.undoRedoManager.beforeRedoAtSavepoint(this);
        }
        boolean done = false;
        try {
            this.delegate.redo();
            done = true;
            this.undoRedoManager.afterRedoCheck(this);
        }
        finally {
            if (!done && savepoint) {
                this.undoRedoManager.delegateRedoFailedAtSavepoint(this);
            }
        }
    }

    @Override
    public boolean canRedo() {
        return this.delegate.canRedo();
    }

    @Override
    public void die() {
        this.undoRedoManager.checkLogOp("WrapUndoEdit.die", this);
        this.delegate.die();
        this.undoRedoManager.notifyWrapEditDie(this);
    }

    @Override
    public boolean addEdit(UndoableEdit anEdit) {
        if (this.undoRedoManager.isAtSavepoint()) {
            this.undoRedoManager.mergeSaveActionsToLastEdit(this);
            return false;
        }
        WrapUndoEdit wrapEdit = (WrapUndoEdit)anEdit;
        boolean added = this.delegate.addEdit(wrapEdit.delegate);
        return added;
    }

    @Override
    public boolean replaceEdit(UndoableEdit anEdit) {
        if (this.undoRedoManager.isAtSavepoint()) {
            return false;
        }
        WrapUndoEdit wrapEdit = (WrapUndoEdit)anEdit;
        boolean replaced = this.delegate.replaceEdit(wrapEdit.delegate);
        this.undoRedoManager.checkLogOp("WrapUndoEdit.replaceEdit=" + replaced, anEdit);
        if (replaced) {
            this.undoRedoManager.checkReplaceSavepointEdit(wrapEdit, this);
        }
        return replaced;
    }

    @Override
    public boolean isSignificant() {
        return this.delegate.isSignificant();
    }

    @Override
    public String getPresentationName() {
        return this.delegate.getPresentationName();
    }

    @Override
    public String getUndoPresentationName() {
        return this.delegate.getUndoPresentationName();
    }

    @Override
    public String getRedoPresentationName() {
        return this.delegate.getRedoPresentationName();
    }
}

