/*
 * Decompiled with CFR 0_118.
 */
package org.openide.text;

import java.awt.Color;
import java.awt.Font;
import java.awt.font.TextAttribute;
import java.text.AttributedCharacterIterator;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AttributedCharacters {
    protected char[] chars = new char[10];
    protected Font[] fonts = new Font[10];
    protected Color[] colors = new Color[10];
    protected int[] runStart = new int[10];
    protected int[] runLimit = new int[10];
    protected int current = -1;

    public void append(char c, Font f, Color color) {
        if (f == null) {
            return;
        }
        if (++this.current == this.chars.length) {
            char[] ctmp = new char[2 * this.chars.length];
            Font[] ftmp = new Font[2 * this.chars.length];
            Color[] cotmp = new Color[2 * this.chars.length];
            int[] rstmp = new int[2 * this.chars.length];
            int[] rltmp = new int[2 * this.chars.length];
            System.arraycopy(this.chars, 0, ctmp, 0, this.chars.length);
            System.arraycopy(this.fonts, 0, ftmp, 0, this.chars.length);
            System.arraycopy(this.colors, 0, cotmp, 0, this.chars.length);
            System.arraycopy(this.runStart, 0, rstmp, 0, this.chars.length);
            System.arraycopy(this.runLimit, 0, rltmp, 0, this.chars.length);
            this.chars = ctmp;
            this.fonts = ftmp;
            this.colors = cotmp;
            this.runStart = rstmp;
            this.runLimit = rltmp;
        }
        this.chars[this.current] = c;
        this.fonts[this.current] = f;
        this.colors[this.current] = color;
        if (this.current != 0) {
            int prev = this.current - 1;
            if (this.fonts[prev].equals(f) && this.colors[prev].equals(color)) {
                this.runStart[this.current] = this.runStart[prev];
                this.runLimit[this.runStart[this.current]] = this.current;
            } else {
                this.runLimit[this.current] = this.current;
                this.runStart[this.current] = this.current;
            }
        }
    }

    public void append(char[] a, Font f, Color color) {
        if (a == null || a.length == 0 || f == null || color == null) {
            return;
        }
        if (++this.current + a.length >= this.chars.length) {
            int size = Math.max(this.current + a.length, 2 * this.chars.length);
            char[] ctmp = new char[size];
            Font[] ftmp = new Font[size];
            Color[] cotmp = new Color[size];
            int[] rstmp = new int[size];
            int[] rltmp = new int[size];
            System.arraycopy(this.chars, 0, ctmp, 0, this.chars.length);
            System.arraycopy(this.fonts, 0, ftmp, 0, this.fonts.length);
            System.arraycopy(this.colors, 0, cotmp, 0, this.chars.length);
            System.arraycopy(this.runStart, 0, rstmp, 0, this.chars.length);
            System.arraycopy(this.runLimit, 0, rltmp, 0, this.chars.length);
            this.chars = ctmp;
            this.fonts = ftmp;
            this.colors = cotmp;
            this.runStart = rstmp;
            this.runLimit = rltmp;
        }
        System.arraycopy(a, 0, this.chars, this.current, a.length);
        for (int i = 0; i < a.length; ++i) {
            this.fonts[i + this.current] = f;
            this.colors[i + this.current] = color;
        }
        int prev = this.current - 1;
        int pseudo = this.current + a.length - 1;
        if (prev < 0) {
            this.runLimit[0] = pseudo;
        } else {
            int replace;
            if (this.fonts[prev].equals(f) && this.colors[prev].equals(color)) {
                this.runLimit[this.runStart[prev]] = pseudo;
                replace = this.runStart[prev];
            } else {
                this.runLimit[this.current] = pseudo;
                replace = this.current;
            }
            for (int i2 = this.current; i2 <= pseudo; ++i2) {
                this.runStart[i2] = replace;
            }
        }
        this.current = pseudo;
    }

    public AttributedCharacterIterator iterator() {
        int size = this.current + 1;
        char[] cs = new char[size];
        Font[] fs = new Font[size];
        Color[] colos = new Color[size];
        int[] rstmp = new int[size];
        int[] rltmp = new int[size];
        System.arraycopy(this.runStart, 0, rstmp, 0, size);
        System.arraycopy(this.runLimit, 0, rltmp, 0, size);
        System.arraycopy(this.chars, 0, cs, 0, size);
        System.arraycopy(this.fonts, 0, fs, 0, size);
        System.arraycopy(this.colors, 0, colos, 0, size);
        AttributedCharacterIteratorImpl ret = new AttributedCharacterIteratorImpl(cs, fs, colos, rstmp, rltmp);
        return ret;
    }

    public static class AttributedCharacterIteratorImpl
    implements AttributedCharacterIterator {
        protected int current;
        protected char[] chars;
        protected Font[] fonts;
        protected Color[] colors;
        protected int[] runStart;
        protected int[] runLimit;
        protected Set<AttributedCharacterIterator.Attribute> singleton;

        public AttributedCharacterIteratorImpl(char[] chars, Font[] fonts, Color[] colors, int[] rs, int[] rl) {
            this.chars = chars;
            this.fonts = fonts;
            this.colors = colors;
            this.runStart = rs;
            this.runLimit = rl;
        }

        @Override
        public Object clone() {
            try {
                return super.clone();
            }
            catch (CloneNotSupportedException e) {
                return null;
            }
        }

        @Override
        public char current() {
            if (this.current >= this.chars.length) {
                return '\uffff';
            }
            return this.chars[this.current];
        }

        @Override
        public char first() {
            this.current = 0;
            if (this.current >= this.chars.length) {
                return '\uffff';
            }
            return this.chars[this.current];
        }

        @Override
        public int getBeginIndex() {
            return 0;
        }

        @Override
        public int getEndIndex() {
            return this.chars.length;
        }

        @Override
        public int getIndex() {
            return this.current;
        }

        @Override
        public char last() {
            int end = this.getEndIndex();
            if (end == 0) {
                return '\uffff';
            }
            this.current = end - 1;
            return this.chars[this.current];
        }

        @Override
        public char next() {
            if (this.current >= this.getEndIndex() - 1) {
                return '\uffff';
            }
            return this.chars[++this.current];
        }

        @Override
        public char previous() {
            if (this.current == 0) {
                return '\uffff';
            }
            return this.chars[--this.current];
        }

        @Override
        public char setIndex(int i) {
            if (i < 0) {
                throw new IllegalArgumentException();
            }
            if (i == this.getEndIndex()) {
                this.current = this.getEndIndex();
                return '\uffff';
            }
            this.current = i;
            return this.chars[this.current];
        }

        @Override
        public Set<AttributedCharacterIterator.Attribute> getAllAttributeKeys() {
            if (this.singleton == null) {
                HashSet<TextAttribute> l = new HashSet<TextAttribute>(4);
                l.add(TextAttribute.FONT);
                l.add(TextAttribute.FOREGROUND);
                this.singleton = Collections.unmodifiableSet(l);
            }
            return this.singleton;
        }

        @Override
        public Object getAttribute(AttributedCharacterIterator.Attribute att) {
            if (att == TextAttribute.FONT) {
                return this.fonts[this.getIndex()];
            }
            if (att == TextAttribute.FOREGROUND) {
                return this.colors[this.getIndex()];
            }
            return null;
        }

        @Override
        public Map<AttributedCharacterIterator.Attribute, Object> getAttributes() {
            HashMap<AttributedCharacterIterator.Attribute, Object> m = new HashMap<AttributedCharacterIterator.Attribute, Object>(1);
            m.put(TextAttribute.FONT, this.fonts[this.getIndex()]);
            m.put(TextAttribute.FOREGROUND, this.colors[this.getIndex()]);
            return m;
        }

        @Override
        public int getRunLimit() {
            return this.runLimit[this.runStart[this.getIndex()]] + 1;
        }

        @Override
        public int getRunLimit(AttributedCharacterIterator.Attribute att) {
            if (att != TextAttribute.FONT && att != TextAttribute.FOREGROUND) {
                return this.getEndIndex();
            }
            return this.getRunLimit();
        }

        @Override
        public int getRunLimit(Set<? extends AttributedCharacterIterator.Attribute> attributes) {
            if (attributes.contains(TextAttribute.FONT) || attributes.contains(TextAttribute.FOREGROUND)) {
                return this.getRunLimit();
            }
            return this.getEndIndex();
        }

        @Override
        public int getRunStart() {
            return this.runStart[this.getIndex()];
        }

        @Override
        public int getRunStart(AttributedCharacterIterator.Attribute att) {
            if (att != TextAttribute.FONT && att != TextAttribute.FOREGROUND) {
                return 0;
            }
            return this.getRunStart();
        }

        @Override
        public int getRunStart(Set<? extends AttributedCharacterIterator.Attribute> attributes) {
            if (attributes.contains(TextAttribute.FONT) || attributes.contains(TextAttribute.FOREGROUND)) {
                return this.getRunStart();
            }
            return 0;
        }
    }

}

