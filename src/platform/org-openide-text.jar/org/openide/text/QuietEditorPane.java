/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.windows.ExternalDropHandler
 *  org.openide.windows.TopComponent
 */
package org.openide.text;

import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Rectangle;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.InputEvent;
import java.awt.im.InputContext;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.TooManyListenersException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.plaf.UIResource;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.openide.text.ActiveEditorDrop;
import org.openide.util.Lookup;
import org.openide.windows.ExternalDropHandler;
import org.openide.windows.TopComponent;

final class QuietEditorPane
extends JEditorPane {
    private static final Logger LOG = Logger.getLogger(QuietEditorPane.class.getName());
    static final int FIRE = 1;
    static final int PAINT = 2;
    static final int ALL = 3;
    private int lastPosition = -1;
    int working = 0;
    private int fontHeight;
    private int charWidth;
    private static final Set<String> EXPENSIVE_PROPERTIES = new HashSet<String>(Arrays.asList("document", "editorKit", "keymap"));

    static DataFlavor constructActiveEditorDropFlavor() {
        try {
            return new DataFlavor("text/active_editor_flavor;class=org.openide.text.ActiveEditorDrop", "Active Editor Flavor", QuietEditorPane.class.getClassLoader());
        }
        catch (ClassNotFoundException e) {
            throw new AssertionError(e);
        }
    }

    public QuietEditorPane() {
        this.setFontHeightWidth(this.getFont());
    }

    @Override
    public void setFont(Font font) {
        super.setFont(font);
        this.setFontHeightWidth(this.getFont());
    }

    private void setFontHeightWidth(Font font) {
        FontMetrics metrics = this.getFontMetrics(font);
        this.fontHeight = metrics.getHeight();
        this.charWidth = metrics.charWidth('m');
    }

    @Override
    public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
        switch (orientation) {
            case 1: {
                return this.fontHeight;
            }
            case 0: {
                return this.charWidth;
            }
        }
        throw new IllegalArgumentException("Invalid orientation: " + orientation);
    }

    @Override
    public void setDocument(Document doc) {
        super.setDocument(doc);
        if (doc != null) {
            DropTarget currDt;
            TransferHandler thn = this.getTransferHandler();
            if (!(thn instanceof DelegatingTransferHandler)) {
                DelegatingTransferHandler dth = new DelegatingTransferHandler(thn);
                this.setTransferHandler(dth);
            }
            if (!((currDt = this.getDropTarget()) instanceof DelegatingDropTarget)) {
                DelegatingDropTarget dt = new DelegatingDropTarget(currDt);
                this.setDropTarget(dt);
            }
        }
    }

    public void setWorking(int x) {
        this.working = x;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("QEP@" + Integer.toHexString(System.identityHashCode(this)) + " firing is " + ((this.working & 1) == 0 ? "OFF" : "ON"));
        }
    }

    @Override
    public void firePropertyChange(String s, Object val1, Object val2) {
        if ((this.working & 1) != 0 || s == null || !EXPENSIVE_PROPERTIES.contains(s)) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("QEP@" + Integer.toHexString(System.identityHashCode(this)) + " firing '" + s + "' change event;" + " firing is " + ((this.working & 1) == 0 ? "OFF" : "ON"));
            }
            super.firePropertyChange(s, val1, val2);
        } else if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("QEP@" + Integer.toHexString(System.identityHashCode(this)) + " suppressed '" + s + "' change event;" + " firing is OFF");
        }
    }

    @Override
    public void setCaret(Caret caret) {
        Caret oldCaret;
        if (caret == null && (oldCaret = this.getCaret()) != null) {
            this.lastPosition = oldCaret.getDot();
        }
        super.setCaret(caret);
    }

    int getLastPosition() {
        return this.lastPosition;
    }

    @Override
    public void revalidate() {
        if ((this.working & 2) != 0) {
            super.revalidate();
        }
    }

    @Override
    public void repaint() {
        if ((this.working & 2) != 0) {
            super.repaint();
        }
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        final InputContext currentInputContext = this.getInputContext();
        Runnable w = new Runnable(){

            @Override
            public void run() {
                try {
                    Class inputContext = Class.forName("sun.awt.im.InputContext", false, QuietEditorPane.class.getClassLoader());
                    Field inputMethodWindowContext = inputContext.getDeclaredField("inputMethodWindowContext");
                    Field previousInputMethod = inputContext.getDeclaredField("previousInputMethod");
                    inputMethodWindowContext.setAccessible(true);
                    previousInputMethod.setAccessible(true);
                    if (currentInputContext == inputMethodWindowContext.get(null)) {
                        inputMethodWindowContext.set(null, null);
                        previousInputMethod.set(null, null);
                    }
                    Class inputMethodContext = Class.forName("sun.awt.im.InputMethodContext", false, QuietEditorPane.class.getClassLoader());
                    Method getCompositionAreaHandler = inputMethodContext.getDeclaredMethod("getCompositionAreaHandler", Boolean.TYPE);
                    getCompositionAreaHandler.setAccessible(true);
                    getCompositionAreaHandler.invoke(currentInputContext, false);
                }
                catch (NoSuchFieldException ex) {
                    LOG.log(Level.FINE, null, ex);
                }
                catch (InvocationTargetException ex) {
                    LOG.log(Level.FINE, null, ex);
                }
                catch (NoSuchMethodException ex) {
                    LOG.log(Level.FINE, null, ex);
                }
                catch (IllegalArgumentException ex) {
                    LOG.log(Level.FINE, null, ex);
                }
                catch (IllegalAccessException ex) {
                    LOG.log(Level.FINE, null, ex);
                }
                catch (SecurityException ex) {
                    LOG.log(Level.FINE, null, ex);
                }
                catch (ClassNotFoundException ex) {
                    LOG.log(Level.FINE, null, ex);
                }
            }
        };
        if (SwingUtilities.isEventDispatchThread()) {
            w.run();
        } else {
            SwingUtilities.invokeLater(w);
        }
    }

    private class DelegatingDropTarget
    extends DropTarget
    implements UIResource {
        private final DropTarget orig;
        private boolean isDragging;

        public DelegatingDropTarget(DropTarget orig) {
            this.isDragging = false;
            this.orig = orig;
        }

        @Override
        public void addDropTargetListener(DropTargetListener dtl) throws TooManyListenersException {
            this.orig.removeDropTargetListener(dtl);
            this.orig.addDropTargetListener(dtl);
        }

        @Override
        public void removeDropTargetListener(DropTargetListener dtl) {
            this.orig.removeDropTargetListener(dtl);
        }

        @Override
        public void dragEnter(DropTargetDragEvent dtde) {
            Collection handlers = Lookup.getDefault().lookupAll(ExternalDropHandler.class);
            for (ExternalDropHandler handler : handlers) {
                if (!handler.canDrop(dtde)) continue;
                dtde.acceptDrag(1);
                this.isDragging = false;
                return;
            }
            this.orig.dragEnter(dtde);
            this.isDragging = true;
        }

        @Override
        public void dragExit(DropTargetEvent dte) {
            if (this.isDragging) {
                this.orig.dragExit(dte);
            }
            this.isDragging = false;
        }

        @Override
        public void dragOver(DropTargetDragEvent dtde) {
            Collection handlers = Lookup.getDefault().lookupAll(ExternalDropHandler.class);
            for (ExternalDropHandler handler : handlers) {
                if (!handler.canDrop(dtde)) continue;
                dtde.acceptDrag(1);
                this.isDragging = false;
                return;
            }
            this.orig.dragOver(dtde);
            this.isDragging = true;
        }

        @Override
        public void drop(DropTargetDropEvent e) {
            Collection handlers = Lookup.getDefault().lookupAll(ExternalDropHandler.class);
            for (ExternalDropHandler handler : handlers) {
                if (!handler.canDrop(e)) continue;
                e.acceptDrop(1);
                boolean dropped = handler.handleDrop(e);
                if (!dropped) continue;
                e.dropComplete(true);
                this.isDragging = false;
                return;
            }
            this.orig.drop(e);
            this.isDragging = false;
        }

        @Override
        public void dropActionChanged(DropTargetDragEvent dtde) {
            if (this.isDragging) {
                this.orig.dropActionChanged(dtde);
            }
        }
    }

    private class DelegatingTransferHandler
    extends TransferHandler {
        TransferHandler delegator;

        public DelegatingTransferHandler(TransferHandler delegator) {
            this.delegator = delegator;
        }

        @Override
        public void exportAsDrag(JComponent comp, InputEvent e, int action) {
            this.delegator.exportAsDrag(comp, e, action);
        }

        @Override
        public void exportToClipboard(JComponent comp, Clipboard clip, int action) {
            this.delegator.exportToClipboard(comp, clip, action);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public boolean importData(JComponent comp, Transferable t) {
            try {
                Object obj;
                if (t.isDataFlavorSupported(ActiveEditorDrop.FLAVOR) && (obj = t.getTransferData(ActiveEditorDrop.FLAVOR)) instanceof ActiveEditorDrop && comp instanceof JTextComponent) {
                    boolean success = false;
                    try {
                        success = ((ActiveEditorDrop)obj).handleTransfer((JTextComponent)comp);
                    }
                    finally {
                        this.requestFocus(comp);
                    }
                    return success;
                }
            }
            catch (Exception exc) {
                exc.printStackTrace();
            }
            return this.delegator.importData(comp, t);
        }

        private void requestFocus(JComponent comp) {
            Container container = SwingUtilities.getAncestorOfClass(TopComponent.class, comp);
            if (container != null) {
                ((TopComponent)container).requestActive();
            } else {
                Component f = comp;
                while (!((f = f.getParent()) instanceof Frame) && f != null) {
                }
                if (f != null) {
                    f.requestFocus();
                }
                comp.requestFocus();
            }
        }

        @Override
        public boolean canImport(JComponent comp, DataFlavor[] transferFlavors) {
            for (int i = 0; i < transferFlavors.length; ++i) {
                if (transferFlavors[i] != ActiveEditorDrop.FLAVOR) continue;
                return true;
            }
            return this.delegator.canImport(comp, transferFlavors);
        }

        @Override
        public int getSourceActions(JComponent c) {
            return this.delegator.getSourceActions(c);
        }

        @Override
        public Icon getVisualRepresentation(Transferable t) {
            return this.delegator.getVisualRepresentation(t);
        }

        @Override
        protected void exportDone(JComponent source, Transferable data, int action) {
            try {
                Method method = this.delegator.getClass().getDeclaredMethod("exportDone", JComponent.class, Transferable.class, Integer.TYPE);
                method.setAccessible(true);
                method.invoke(this.delegator, source, data, new Integer(action));
            }
            catch (NoSuchMethodException ex) {
                ex.printStackTrace();
            }
            catch (IllegalAccessException ex) {
                ex.printStackTrace();
            }
            catch (InvocationTargetException ex) {
                ex.printStackTrace();
            }
        }

        @Override
        protected Transferable createTransferable(JComponent comp) {
            try {
                Method method = this.delegator.getClass().getDeclaredMethod("createTransferable", JComponent.class);
                method.setAccessible(true);
                return (Transferable)method.invoke(this.delegator, comp);
            }
            catch (NoSuchMethodException ex) {
                ex.printStackTrace();
            }
            catch (IllegalAccessException ex) {
                ex.printStackTrace();
            }
            catch (InvocationTargetException ex) {
                ex.printStackTrace();
            }
            return null;
        }
    }

}

