/*
 * Decompiled with CFR 0_118.
 */
package org.openide.text;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import org.openide.text.Annotatable;

public abstract class Annotation {
    public static final String PROP_SHORT_DESCRIPTION = "shortDescription";
    public static final String PROP_ANNOTATION_TYPE = "annotationType";
    public static final String PROP_MOVE_TO_FRONT = "moveToFront";
    private PropertyChangeSupport support;
    private Annotatable attached;
    private boolean inDocument = false;

    public Annotation() {
        this.support = new PropertyChangeSupport(this);
    }

    public abstract String getAnnotationType();

    public abstract String getShortDescription();

    public final void attach(Annotatable anno) {
        if (this.attached != null) {
            this.detach();
        }
        this.attached = anno;
        this.attached.addAnnotation(this);
        this.notifyAttached(this.attached);
    }

    protected void notifyAttached(Annotatable toAnno) {
    }

    public final void detach() {
        if (this.attached != null) {
            this.attached.removeAnnotation(this);
            Annotatable old = this.attached;
            this.attached = null;
            this.notifyDetached(old);
        }
    }

    protected void notifyDetached(Annotatable fromAnno) {
    }

    public final Annotatable getAttachedAnnotatable() {
        return this.attached;
    }

    public final void addPropertyChangeListener(PropertyChangeListener l) {
        this.support.addPropertyChangeListener(l);
    }

    public final void removePropertyChangeListener(PropertyChangeListener l) {
        this.support.removePropertyChangeListener(l);
    }

    protected final void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        this.support.firePropertyChange(propertyName, oldValue, newValue);
    }

    public final void moveToFront() {
        this.support.firePropertyChange("moveToFront", null, null);
    }

    final boolean isInDocument() {
        return this.inDocument;
    }

    final void setInDocument(boolean b) {
        this.inDocument = b;
    }
}

