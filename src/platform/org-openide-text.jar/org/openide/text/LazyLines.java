/*
 * Decompiled with CFR 0_118.
 */
package org.openide.text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import org.openide.text.DocumentLine;
import org.openide.text.Line;
import org.openide.text.LineListener;

final class LazyLines
implements List<Line> {
    private List<Line> delegate;
    private DocumentLine.Set set;

    public LazyLines(DocumentLine.Set set) {
        this.set = set;
    }

    private List<Line> createDelegate() {
        int cnt = this.set.listener.getOriginalLineCount();
        ArrayList<Line> l = new ArrayList<Line>(cnt);
        for (int i = 0; i < cnt; ++i) {
            l.add(this.set.getOriginal(i));
        }
        return l;
    }

    private synchronized List<Line> getDelegate() {
        if (this.delegate == null) {
            this.delegate = this.createDelegate();
        }
        return this.delegate;
    }

    @Override
    public int indexOf(Object o) {
        Line find;
        int indx;
        if (o instanceof DocumentLine && (find = this.set.findLine((DocumentLine)o)) != null && this.set.getOriginal(indx = this.set.listener.getOld(find.getLineNumber())).equals(o)) {
            return indx;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        return this.indexOf(o);
    }

    @Override
    public int hashCode() {
        return this.getDelegate().hashCode();
    }

    @Override
    public boolean addAll(Collection<? extends Line> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ListIterator<Line> listIterator() {
        return this.getDelegate().listIterator();
    }

    @Override
    public Object[] toArray() {
        return this.getDelegate().toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return this.getDelegate().toArray(a);
    }

    @Override
    public ListIterator<Line> listIterator(int index) {
        return this.getDelegate().listIterator(index);
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean equals(Object obj) {
        return this.getDelegate().equals(obj);
    }

    @Override
    public boolean contains(Object o) {
        return this.getDelegate().contains(o);
    }

    @Override
    public void add(int index, Line element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        this.getDelegate().clear();
    }

    @Override
    public Line set(int index, Line element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int size() {
        return this.getDelegate().size();
    }

    @Override
    public Line get(int index) {
        return this.getDelegate().get(index);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return this.getDelegate().containsAll(c);
    }

    @Override
    public boolean add(Line o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isEmpty() {
        return this.getDelegate().isEmpty();
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Line> subList(int fromIndex, int toIndex) {
        return this.getDelegate().subList(fromIndex, toIndex);
    }

    @Override
    public Line remove(int index) {
        return this.getDelegate().remove(index);
    }

    @Override
    public Iterator<Line> iterator() {
        return this.getDelegate().iterator();
    }

    @Override
    public boolean addAll(int index, Collection<? extends Line> c) {
        throw new UnsupportedOperationException();
    }
}

