/*
 * Decompiled with CFR 0_118.
 */
package org.openide.text;

import javax.swing.event.ChangeEvent;
import javax.swing.text.StyledDocument;

final class EnhancedChangeEvent
extends ChangeEvent {
    private boolean closing;
    private StyledDocument doc;

    public EnhancedChangeEvent(Object source, StyledDocument doc, boolean closing) {
        super(source);
        this.doc = doc;
        this.closing = closing;
    }

    public boolean isClosingDocument() {
        return this.closing;
    }

    public StyledDocument getDocument() {
        return this.doc;
    }
}

