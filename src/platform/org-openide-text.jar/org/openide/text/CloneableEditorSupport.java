/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.NotifyDescriptor$Exception
 *  org.openide.awt.StatusDisplayer
 *  org.openide.awt.UndoRedo
 *  org.openide.awt.UndoRedo$Manager
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 *  org.openide.util.TaskListener
 *  org.openide.util.UserCancelException
 *  org.openide.util.UserQuestionException
 *  org.openide.util.WeakSet
 *  org.openide.windows.CloneableOpenSupport
 *  org.openide.windows.CloneableOpenSupport$Env
 *  org.openide.windows.CloneableTopComponent
 *  org.openide.windows.CloneableTopComponent$Ref
 *  org.openide.windows.TopComponent
 */
package org.openide.text;

import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.print.PageFormat;
import java.awt.print.Pageable;
import java.awt.print.Printable;
import java.awt.print.PrinterAbortException;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.EditorKit;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import javax.swing.text.WrappedPlainView;
import javax.swing.undo.UndoableEdit;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.StatusDisplayer;
import org.openide.awt.UndoRedo;
import org.openide.text.AnnotationProvider;
import org.openide.text.CloneableEditor;
import org.openide.text.CloneableEditorSupportRedirector;
import org.openide.text.DocumentOpenClose;
import org.openide.text.DocumentStatus;
import org.openide.text.EditorSupportLineSet;
import org.openide.text.EnhancedChangeEvent;
import org.openide.text.FilterDocument;
import org.openide.text.Line;
import org.openide.text.NbDocument;
import org.openide.text.PositionRef;
import org.openide.text.PrintPreferences;
import org.openide.text.UndoRedoManager;
import org.openide.text.UserQuestionExceptionHandler;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.TaskListener;
import org.openide.util.UserCancelException;
import org.openide.util.UserQuestionException;
import org.openide.util.WeakSet;
import org.openide.windows.CloneableOpenSupport;
import org.openide.windows.CloneableTopComponent;
import org.openide.windows.TopComponent;

public abstract class CloneableEditorSupport
extends CloneableOpenSupport {
    public static final String EDITOR_MODE = "editor";
    public static final UndoableEdit BEGIN_COMMIT_GROUP = UndoRedoManager.BEGIN_COMMIT_GROUP;
    public static final UndoableEdit END_COMMIT_GROUP = UndoRedoManager.END_COMMIT_GROUP;
    public static final UndoableEdit MARK_COMMIT_GROUP = UndoRedoManager.MARK_COMMIT_GROUP;
    private static final String PROP_PANE = "CloneableEditorSupport.Pane";
    private static final ThreadLocal<Boolean> LOCAL_CLOSE_DOCUMENT = new ThreadLocal<T>();
    DocumentOpenClose openClose;
    static final Logger ERR = Logger.getLogger("org.openide.text.CloneableEditorSupport");
    private EditorKit kit;
    private String mimeType;
    private Listener listener;
    private UndoRedo.Manager undoRedo;
    private Line.Set[] lineSet = new Line.Set[]{null};
    private boolean printing;
    private final Object LOCK_PRINTING = new Object();
    private PositionRef.Manager positionManager;
    private Set<ChangeListener> listeners;
    private transient Reference<Pane>[] lastSelected = new Reference[]{null};
    private long lastSaveTime;
    private boolean reloadDialogOpened;
    private PropertyChangeSupport propertyChangeSupport;
    private Lookup lookup;
    private boolean alreadyModified;
    private boolean preventModification;
    private boolean listeningOnEnv;
    private boolean inUserQuestionExceptionHandler;
    private Map<Line, Reference<Line>> lineSetWHM;
    private boolean annotationsLoaded;
    private DocFilter docFilter;
    private final Object checkModificationLock = new Object();
    private static final Set<Class<?>> warnedClasses = new WeakSet();
    private static Reference<CloneableTopComponent> lastReusable = new WeakReference<Object>(null);

    public CloneableEditorSupport(Env env) {
        this(env, Lookup.EMPTY);
    }

    public CloneableEditorSupport(Env env, Lookup l) {
        super((CloneableOpenSupport.Env)env);
        Parameters.notNull((CharSequence)"l", (Object)l);
        this.lookup = l;
        this.openClose = new DocumentOpenClose(this);
    }

    protected abstract String messageSave();

    protected abstract String messageName();

    protected String messageHtmlName() {
        return null;
    }

    protected String documentID() {
        return this.messageName();
    }

    protected abstract String messageToolTip();

    protected String messageLine(Line line) {
        return NbBundle.getMessage(Line.class, (String)"FMT_CESLineDisplayName", (Object)this.messageName(), (Object)(line.getLineNumber() + 1));
    }

    final Env cesEnv() {
        return (Env)this.env;
    }

    final EditorKit cesKit() {
        return this.createEditorKit();
    }

    protected final synchronized UndoRedo.Manager getUndoRedo() {
        CloneableEditorSupport redirect = CloneableEditorSupportRedirector.findRedirect(this);
        if (redirect != null) {
            return redirect.getUndoRedo();
        }
        if (this.undoRedo == null) {
            UndoRedo.Manager mgr;
            this.undoRedo = mgr = this.createUndoRedoManager();
        }
        return this.undoRedo;
    }

    final synchronized PositionRef.Manager getPositionManager() {
        if (this.positionManager == null) {
            this.positionManager = new PositionRef.Manager(this);
        }
        return this.positionManager;
    }

    void ensureAnnotationsLoaded() {
        if (!this.annotationsLoaded) {
            this.annotationsLoaded = true;
            Line.Set lines = this.getLineSet();
            for (AnnotationProvider act : Lookup.getDefault().lookupAll(AnnotationProvider.class)) {
                act.annotate(lines, this.lookup);
            }
        }
    }

    protected boolean asynchronousOpen() {
        Class<?> clazz = this.getClass();
        if (warnedClasses.add(clazz)) {
            ERR.warning(clazz.getName() + " should override asynchronousOpen()." + " See http://bits.netbeans.org/dev/javadoc/org-openide-text/apichanges.html#CloneableEditorSupport.asynchronousOpen");
        }
        return false;
    }

    public void open() {
        CloneableEditorSupport redirect = CloneableEditorSupportRedirector.findRedirect(this);
        if (redirect != null) {
            redirect.open();
            return;
        }
        if (this.asynchronousOpen()) {
            super.open();
        } else {
            try {
                StyledDocument doc = this.openDocument();
                super.open();
            }
            catch (UserQuestionException e) {
                new UserQuestionExceptionHandler(this, e){

                    @Override
                    protected void opened(StyledDocument openDoc) {
                        CloneableEditorSupport.this.open();
                    }
                }.runInEDT();
            }
            catch (IOException e) {
                ERR.log(Level.INFO, null, e);
            }
        }
    }

    public final void addPropertyChangeListener(PropertyChangeListener l) {
        this.getPropertyChangeSupport().addPropertyChangeListener(l);
    }

    public final void removePropertyChangeListener(PropertyChangeListener l) {
        this.getPropertyChangeSupport().removePropertyChangeListener(l);
    }

    protected final void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        this.getPropertyChangeSupport().firePropertyChange(propertyName, oldValue, newValue);
    }

    private synchronized PropertyChangeSupport getPropertyChangeSupport() {
        if (this.propertyChangeSupport == null) {
            this.propertyChangeSupport = new PropertyChangeSupport((Object)((Object)this));
        }
        return this.propertyChangeSupport;
    }

    public Task prepareDocument() {
        CloneableEditorSupport redirect = CloneableEditorSupportRedirector.findRedirect(this);
        if (redirect != null) {
            return redirect.prepareDocument();
        }
        return this.openClose.openTask();
    }

    final void addDocListener(Document d) {
        if (Boolean.TRUE.equals(d.getProperty("supportsModificationListener"))) {
            d.putProperty("modificationListener", this.getListener());
        }
        if (d instanceof AbstractDocument) {
            AbstractDocument aDoc = (AbstractDocument)d;
            DocumentFilter origFilter = aDoc.getDocumentFilter();
            this.docFilter = new DocFilter(origFilter);
            aDoc.setDocumentFilter(this.docFilter);
        } else {
            DocumentFilter origFilter = (DocumentFilter)d.getProperty(DocumentFilter.class);
            this.docFilter = new DocFilter(origFilter);
            d.putProperty(DocumentFilter.class, this.docFilter);
        }
        d.addDocumentListener(this.getListener());
    }

    final void removeDocListener(Document d) {
        if (Boolean.TRUE.equals(d.getProperty("supportsModificationListener"))) {
            d.putProperty("modificationListener", null);
        }
        if (this.docFilter != null) {
            if (d instanceof AbstractDocument) {
                AbstractDocument aDoc = (AbstractDocument)d;
                aDoc.setDocumentFilter(this.docFilter.origFilter);
            } else {
                d.putProperty(DocumentFilter.class, this.docFilter.origFilter);
            }
            this.docFilter = null;
        }
        d.removeDocumentListener(this.getListener());
    }

    public StyledDocument openDocument() throws IOException {
        CloneableEditorSupport redirect = CloneableEditorSupportRedirector.findRedirect(this);
        if (redirect != null) {
            return redirect.openDocument();
        }
        return this.openClose.open();
    }

    public StyledDocument getDocument() {
        CloneableEditorSupport redirect = CloneableEditorSupportRedirector.findRedirect(this);
        if (redirect != null) {
            return redirect.getDocument();
        }
        return this.openClose.getDocument();
    }

    public boolean isModified() {
        CloneableEditorSupport redirect = CloneableEditorSupportRedirector.findRedirect(this);
        if (redirect != null) {
            return redirect.isModified();
        }
        return this.cesEnv().isModified();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void saveDocument() throws IOException {
        long externalMod;
        CloneableEditorSupport redirect = CloneableEditorSupportRedirector.findRedirect(this);
        final boolean log = ERR.isLoggable(Level.FINE);
        if (log) {
            ERR.fine(this.documentID() + ": saveDocument() started.");
        }
        if (redirect != null) {
            if (log) {
                ERR.fine("  redirect to " + redirect.documentID());
            }
            redirect.saveDocument();
            return;
        }
        if (!this.cesEnv().isModified()) {
            if (log) {
                ERR.fine(this.documentID() + "  No save performed because cesEnv().isModified() == false");
            }
            return;
        }
        final StyledDocument myDoc = this.getDocument();
        if (myDoc == null) {
            if (log) {
                ERR.fine(this.documentID() + "  No save performed because getDocument() == null");
            }
            return;
        }
        long prevLST = this.lastSaveTime;
        if (prevLST != -1 && (externalMod = this.cesEnv().getTime().getTime()) > prevLST) {
            if (log) {
                ERR.fine(this.documentID() + ":  externalMod=" + externalMod + " > prevLST=" + prevLST + " => throw new UserQuestionException()");
            }
            throw new UserQuestionException(this.mimeType){

                public String getLocalizedMessage() {
                    return NbBundle.getMessage(CloneableEditorSupport.class, (String)"FMT_External_change_write", (Object)myDoc.getProperty("title"));
                }

                public void confirmed() throws IOException {
                    CloneableEditorSupport.this.setLastSaveTime(externalMod);
                    CloneableEditorSupport.this.saveDocument();
                }
            };
        }
        final 1MemoryOutputStream[] memoryOutputStream = new 1MemoryOutputStream[1];
        final IOException[] ioException = new IOException[1];
        final boolean[] onSaveTasksStarted = new boolean[1];
        Runnable saveToMemory = new Runnable(){

            @Override
            public void run() {
                try {
                    class MemoryOutputStream
                    extends ByteArrayOutputStream {
                        public MemoryOutputStream(int size) {
                            super(size);
                        }

                        @Override
                        public void writeTo(OutputStream os) throws IOException {
                            os.write(this.buf, 0, this.count);
                        }
                    }
                    UndoRedo.Manager urm = CloneableEditorSupport.this.getUndoRedo();
                    if (urm instanceof UndoRedoManager) {
                        UndoRedoManager urManager = (UndoRedoManager)urm;
                        if (onSaveTasksStarted[0]) {
                            urManager.endOnSaveTasks();
                        }
                        urManager.markSavepoint();
                    }
                    int byteArrayAllocSize = myDoc.getLength() * 11 / 10;
                    memoryOutputStream[0] = new MemoryOutputStream(byteArrayAllocSize);
                    EditorKit editorKit = CloneableEditorSupport.this.createEditorKit();
                    CloneableEditorSupport.this.saveFromKitToStream(myDoc, editorKit, memoryOutputStream[0]);
                    CloneableEditorSupport.this.updateLineSet(true);
                    if (log) {
                        CloneableEditorSupport.ERR.fine(CloneableEditorSupport.this.documentID() + ": Saved " + memoryOutputStream[0].size() + " bytes to memory output stream.");
                    }
                }
                catch (BadLocationException blex) {
                    Exceptions.printStackTrace((Throwable)blex);
                }
                catch (IOException ex) {
                    ioException[0] = ex;
                }
            }
        };
        Runnable beforeSaveRunnable = (Runnable)myDoc.getProperty("beforeSaveRunnable");
        if (beforeSaveRunnable != null) {
            Runnable beforeSaveStart = new Runnable(){

                @Override
                public void run() {
                    UndoRedo.Manager urm = CloneableEditorSupport.this.getUndoRedo();
                    if (urm instanceof UndoRedoManager) {
                        ((UndoRedoManager)CloneableEditorSupport.this.undoRedo).startOnSaveTasks();
                        onSaveTasksStarted[0] = true;
                    }
                }
            };
            myDoc.putProperty("beforeSaveStart", beforeSaveStart);
            myDoc.putProperty("beforeSaveEnd", saveToMemory);
            beforeSaveRunnable.run();
            if (memoryOutputStream[0] == null) {
                myDoc.render(saveToMemory);
            }
        } else {
            myDoc.render(saveToMemory);
        }
        if (ioException[0] != null) {
            if (log) {
                ERR.log(Level.FINE, this.documentID() + ": Save broken due to IOException", ioException[0]);
            }
            throw ioException[0];
        }
        OutputStream os = null;
        long oldSaveTime = this.lastSaveTime;
        try {
            this.setLastSaveTime(-1);
            os = this.cesEnv().outputStream();
            memoryOutputStream[0].writeTo(os);
            os.close();
            os = null;
            myDoc.render(new Runnable(){

                @Override
                public void run() {
                    UndoRedo.Manager urm = CloneableEditorSupport.this.getUndoRedo();
                    boolean unmodify = false;
                    if (urm instanceof UndoRedoManager) {
                        if (((UndoRedoManager)urm).isAtSavepoint()) {
                            unmodify = true;
                        }
                    } else {
                        unmodify = true;
                    }
                    if (unmodify) {
                        CloneableEditorSupport.this.callNotifyUnmodified();
                    }
                }
            });
            if (log) {
                ERR.fine(this.documentID() + ": Save to file OK, oldSaveTime: " + oldSaveTime + ", " + new Date(oldSaveTime));
            }
            this.setLastSaveTime(this.cesEnv().getTime().getTime());
        }
        finally {
            if (this.lastSaveTime == -1) {
                if (log) {
                    ERR.fine(this.documentID() + ": Save failed (lastSaveTime == -1) restoring old save time.");
                }
                this.setLastSaveTime(oldSaveTime);
                this.callNotifyModified();
            }
            if (os != null) {
                os.close();
            }
        }
    }

    public JEditorPane[] getOpenedPanes() {
        assert (SwingUtilities.isEventDispatchThread());
        CloneableEditorSupport redirect = CloneableEditorSupportRedirector.findRedirect(this);
        if (redirect != null) {
            return redirect.getOpenedPanes();
        }
        LinkedList<JEditorPane> ll = new LinkedList<JEditorPane>();
        Enumeration en = this.allEditors.getComponents();
        Pane last = this.getLastSelected();
        while (en.hasMoreElements()) {
            CloneableTopComponent ctc = (CloneableTopComponent)en.nextElement();
            Pane ed = (Pane)ctc.getClientProperty((Object)"CloneableEditorSupport.Pane");
            if (ed == null && ctc instanceof Pane) {
                ed = (Pane)ctc;
            }
            if (ed != null) {
                JEditorPane p = ed.getEditorPane();
                if (p == null) continue;
                if (last == ed || last != null && last instanceof Component && ed instanceof Container && ((Container)((Object)ed)).isAncestorOf((Component)((Object)last))) {
                    ll.addFirst(p);
                    continue;
                }
                ll.add(p);
                continue;
            }
            throw new IllegalStateException("No reference to Pane. Please file a bug against openide/text");
        }
        return ll.isEmpty() ? null : ll.toArray(new JEditorPane[ll.size()]);
    }

    JEditorPane getRecentPane() {
        assert (SwingUtilities.isEventDispatchThread());
        CloneableEditorSupport redirect = CloneableEditorSupportRedirector.findRedirect(this);
        if (redirect != null) {
            return redirect.getRecentPane();
        }
        Enumeration en = this.allEditors.getComponents();
        Pane last = this.getLastSelected();
        while (en.hasMoreElements()) {
            CloneableTopComponent ctc = (CloneableTopComponent)en.nextElement();
            Pane ed = (Pane)ctc.getClientProperty((Object)"CloneableEditorSupport.Pane");
            if (ed == null && ctc instanceof Pane) {
                ed = (Pane)ctc;
            }
            if (ed != null) {
                JEditorPane p = null;
                if (last == ed || last != null && last instanceof Component && ed instanceof Container && ((Container)((Object)ed)).isAncestorOf((Component)((Object)last))) {
                    if (ed instanceof CloneableEditor && ((CloneableEditor)ed).isEditorPaneReady()) {
                        p = ed.getEditorPane();
                    }
                    if (last instanceof CloneableEditor) {
                        if (((CloneableEditor)last).isEditorPaneReady()) {
                            p = ed.getEditorPane();
                        }
                    } else {
                        p = ed.getEditorPane();
                    }
                }
                if (p == null) continue;
                return p;
            }
            throw new IllegalStateException("No reference to Pane. Please file a bug against openide/text");
        }
        return null;
    }

    protected void afterRedirect(CloneableOpenSupport redirectedTo) {
        super.afterRedirect(redirectedTo);
        if (redirectedTo instanceof CloneableEditorSupport) {
            CloneableEditorSupport other = (CloneableEditorSupport)redirectedTo;
            this.lastSelected = other.lastSelected;
            this.openClose = other.openClose;
            this.lineSet = other.lineSet;
        }
        if (this.propertyChangeSupport != null) {
            this.propertyChangeSupport.firePropertyChange("openedPanes", null, null);
        }
    }

    final Pane getLastSelected() {
        Reference<Pane> r = this.lastSelected[0];
        return r == null ? null : r.get();
    }

    final void setLastSelected(Pane lastSelected) {
        this.lastSelected[0] = new WeakReference<Pane>(lastSelected);
    }

    public Line.Set getLineSet() {
        CloneableEditorSupport redirect = CloneableEditorSupportRedirector.findRedirect(this);
        if (redirect != null) {
            return redirect.getLineSet();
        }
        return this.updateLineSet(false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final Map<Line, Reference<Line>> findWeakHashMap() {
        Object object = this.LOCK_PRINTING;
        synchronized (object) {
            if (this.lineSetWHM != null) {
                return this.lineSetWHM;
            }
            this.lineSetWHM = new WeakHashMap<Line, Reference<Line>>();
            return this.lineSetWHM;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void print() {
        CloneableEditorSupport redirect = CloneableEditorSupportRedirector.findRedirect(this);
        if (redirect != null) {
            redirect.print();
            return;
        }
        Object object = this.LOCK_PRINTING;
        synchronized (object) {
            if (this.printing) {
                return;
            }
            this.printing = true;
        }
        try {
            PrinterJob job = PrinterJob.getPrinterJob();
            Object o = NbDocument.findPageable(this.openDocument());
            if (o instanceof Pageable) {
                job.setPageable((Pageable)o);
            } else {
                PageFormat pf = PrintPreferences.getPageFormat(job);
                job.setPrintable((Printable)o, pf);
            }
            if (job.printDialog()) {
                job.print();
            }
        }
        catch (FileNotFoundException e) {
            CloneableEditorSupport.notifyProblem(e, "CTL_Bad_File");
        }
        catch (IOException e) {
            Exceptions.printStackTrace((Throwable)e);
        }
        catch (PrinterAbortException e) {
            CloneableEditorSupport.notifyProblem(e, "CTL_Printer_Abort");
        }
        catch (PrinterException e) {
            CloneableEditorSupport.notifyProblem(e, "EXC_Printer_Problem");
        }
        finally {
            Object e = this.LOCK_PRINTING;
            synchronized (e) {
                this.printing = false;
            }
        }
    }

    private static void notifyProblem(Exception e, String key) {
        String msg = NbBundle.getMessage(CloneableEditorSupport.class, (String)key, (Object)e.getLocalizedMessage());
        Exceptions.attachLocalizedMessage((Throwable)e, (String)msg);
        DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)new NotifyDescriptor.Exception((Throwable)e));
    }

    protected CloneableTopComponent createCloneableTopComponent() {
        this.prepareDocument();
        Pane pane = this.createPane();
        pane.getComponent().putClientProperty((Object)"CloneableEditorSupport.Pane", (Object)pane);
        return pane.getComponent();
    }

    protected Pane createPane() {
        CloneableEditor ed = this.createCloneableEditor();
        this.initializeCloneableEditor(ed);
        return ed;
    }

    protected Component wrapEditorComponent(Component editorComponent) {
        return editorComponent;
    }

    protected boolean canClose() {
        if (this.cesEnv().isModified()) {
            class SafeAWTAccess
            implements Runnable {
                boolean running;
                boolean finished;
                int ret;

                SafeAWTAccess() {
                }

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    SafeAWTAccess safeAWTAccess = this;
                    synchronized (safeAWTAccess) {
                        this.running = true;
                        this.notifyAll();
                    }
                    try {
                        this.ret = CloneableEditorSupport.this.canCloseImpl();
                    }
                    finally {
                        safeAWTAccess = this;
                        synchronized (safeAWTAccess) {
                            this.finished = true;
                            this.notifyAll();
                        }
                    }
                }

                public synchronized void waitForResult() throws InterruptedException {
                    if (!this.running) {
                        this.wait(10000);
                    }
                    if (!this.running) {
                        throw new InterruptedException("Waiting 10s for AWT and nothing! Exiting to prevent deadlock");
                    }
                    while (!this.finished) {
                        this.wait();
                    }
                }
            }
            SafeAWTAccess safe = new SafeAWTAccess();
            if (SwingUtilities.isEventDispatchThread()) {
                safe.run();
            } else {
                SwingUtilities.invokeLater(safe);
                try {
                    safe.waitForResult();
                }
                catch (InterruptedException ex) {
                    ERR.log(Level.INFO, null, ex);
                    return false;
                }
            }
            if (safe.ret == 0) {
                return false;
            }
            if (safe.ret == 1) {
                try {
                    this.saveDocument();
                }
                catch (UserCancelException uce) {
                    return false;
                }
                catch (IOException e) {
                    Exceptions.printStackTrace((Throwable)e);
                    return false;
                }
            }
        }
        return true;
    }

    private int canCloseImpl() {
        String msg = this.messageSave();
        ResourceBundle bundle = NbBundle.getBundle(CloneableEditorSupport.class);
        JButton saveOption = new JButton(bundle.getString("CTL_Save"));
        saveOption.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_CTL_Save"));
        saveOption.getAccessibleContext().setAccessibleName(bundle.getString("ACSN_CTL_Save"));
        JButton discardOption = new JButton(bundle.getString("CTL_Discard"));
        discardOption.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_CTL_Discard"));
        discardOption.getAccessibleContext().setAccessibleName(bundle.getString("ACSN_CTL_Discard"));
        discardOption.setMnemonic(bundle.getString("CTL_Discard_Mnemonic").charAt(0));
        NotifyDescriptor nd = new NotifyDescriptor((Object)msg, bundle.getString("LBL_SaveFile_Title"), 1, 3, new Object[]{saveOption, discardOption, NotifyDescriptor.CANCEL_OPTION}, (Object)saveOption);
        Object ret = DialogDisplayer.getDefault().notify(nd);
        if (NotifyDescriptor.CANCEL_OPTION.equals(ret) || NotifyDescriptor.CLOSED_OPTION.equals(ret)) {
            return 0;
        }
        if (saveOption.equals(ret)) {
            return 1;
        }
        return -1;
    }

    public boolean isDocumentLoaded() {
        CloneableEditorSupport redirect = CloneableEditorSupportRedirector.findRedirect(this);
        if (redirect != null) {
            return redirect.isDocumentLoaded();
        }
        return this.openClose.isDocumentLoadedOrLoading();
    }

    boolean isDocumentReady() {
        CloneableEditorSupport redirect = CloneableEditorSupportRedirector.findRedirect(this);
        if (redirect != null) {
            return redirect.isDocumentReady();
        }
        return this.openClose.isDocumentOpened();
    }

    public void setMIMEType(String s) {
        CloneableEditorSupport redirect = CloneableEditorSupportRedirector.findRedirect(this, true);
        if (redirect != null) {
            redirect.setMIMEType(s);
            return;
        }
        this.mimeType = s;
    }

    @Deprecated
    public synchronized void addChangeListener(ChangeListener l) {
        if (this.listeners == null) {
            this.listeners = new HashSet<ChangeListener>(8);
        }
        this.listeners.add(l);
    }

    @Deprecated
    public synchronized void removeChangeListener(ChangeListener l) {
        if (this.listeners != null) {
            this.listeners.remove(l);
        }
    }

    public final PositionRef createPositionRef(int offset, Position.Bias bias) {
        return new PositionRef(this.getPositionManager(), offset, bias);
    }

    protected CloneableEditor createCloneableEditor() {
        return new CloneableEditor(this);
    }

    protected void initializeCloneableEditor(CloneableEditor editor) {
    }

    protected UndoRedo.Manager createUndoRedoManager() {
        return new UndoRedoManager(this);
    }

    public InputStream getInputStream() throws IOException {
        CloneableEditorSupport redirect = CloneableEditorSupportRedirector.findRedirect(this);
        if (redirect != null) {
            return redirect.getInputStream();
        }
        StyledDocument tmpDoc = this.getDocument();
        if (tmpDoc == null) {
            return this.cesEnv().inputStream();
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            this.saveFromKitToStream(tmpDoc, this.createEditorKit(), baos);
        }
        catch (BadLocationException e) {
            ERR.log(Level.INFO, null, e);
            throw (IllegalStateException)new IllegalStateException(e.getMessage()).initCause(e);
        }
        return new ByteArrayInputStream(baos.toByteArray());
    }

    protected void saveFromKitToStream(StyledDocument doc, EditorKit kit, OutputStream stream) throws IOException, BadLocationException {
        kit.write(stream, (Document)doc, 0, doc.getLength());
    }

    protected void loadFromStreamToKit(StyledDocument doc, InputStream stream, EditorKit kit) throws IOException, BadLocationException {
        kit.read(stream, (Document)doc, 0);
    }

    protected Task reloadDocument() {
        return this.openClose.reloadTask();
    }

    public static EditorKit getEditorKit(String mimePath) {
        Lookup lookup = MimeLookup.getLookup((MimePath)MimePath.parse((String)mimePath));
        EditorKit kit = (EditorKit)lookup.lookup(EditorKit.class);
        if (kit == null) {
            lookup = MimeLookup.getLookup((MimePath)MimePath.parse((String)"text/plain"));
            kit = (EditorKit)lookup.lookup(EditorKit.class);
        }
        return kit != null ? (EditorKit)kit.clone() : new PlainEditorKit();
    }

    protected EditorKit createEditorKit() {
        if (this.kit != null) {
            return this.kit;
        }
        if (this.mimeType != null) {
            this.kit = CloneableEditorSupport.getEditorKit(this.mimeType);
        } else {
            String defaultMIMEType = this.cesEnv().getMimeType();
            this.kit = CloneableEditorSupport.getEditorKit(defaultMIMEType);
        }
        return this.kit;
    }

    protected StyledDocument createStyledDocument(EditorKit kit) {
        StyledDocument sd = CloneableEditorSupport.createNetBeansDocument(kit.createDefaultDocument());
        sd.putProperty("mimeType", this.mimeType != null ? this.mimeType : this.cesEnv().getMimeType());
        return sd;
    }

    protected void notifyUnmodified() {
        this.env.unmarkModified();
        if (!Boolean.TRUE.equals(LOCAL_CLOSE_DOCUMENT.get())) {
            this.updateTitles();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final boolean callNotifyModified() {
        Object object = this.checkModificationLock;
        synchronized (object) {
            if (this.isAlreadyModified()) {
                return true;
            }
            if (this.preventModification) {
                return false;
            }
            this.setAlreadyModified(true);
        }
        if (!this.notifyModified()) {
            object = this.checkModificationLock;
            synchronized (object) {
                this.setAlreadyModified(false);
            }
            return false;
        }
        this.getPositionManager().documentOpened(this.openClose.docRef);
        return true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void callNotifyUnmodified() {
        Object object = this.checkModificationLock;
        synchronized (object) {
            if (!this.isAlreadyModified()) {
                return;
            }
            this.setAlreadyModified(false);
        }
        this.notifyUnmodified();
        if (this.getAnyEditor() == null) {
            this.getPositionManager().documentClosed();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected boolean notifyModified() {
        boolean locked;
        locked = true;
        try {
            this.env.markModified();
            Object object = this.checkModificationLock;
            synchronized (object) {
                if (!this.isAlreadyModified()) {
                    this.setAlreadyModified(true);
                }
            }
        }
        catch (UserQuestionException ex) {
            CloneableEditorSupport cloneableEditorSupport = this;
            synchronized (cloneableEditorSupport) {
                if (!this.inUserQuestionExceptionHandler) {
                    this.inUserQuestionExceptionHandler = true;
                    DocumentOpenClose.RP.post(new Runnable(){

                        /*
                         * WARNING - Removed try catching itself - possible behaviour change.
                         */
                        @Override
                        public void run() {
                            NotifyDescriptor.Confirmation nd = new NotifyDescriptor.Confirmation((Object)ex.getLocalizedMessage(), 0);
                            Object res = DialogDisplayer.getDefault().notify((NotifyDescriptor)nd);
                            if (NotifyDescriptor.OK_OPTION.equals(res)) {
                                try {
                                    ex.confirmed();
                                }
                                catch (IOException ex1) {
                                    Exceptions.printStackTrace((Throwable)ex1);
                                }
                            }
                            CloneableEditorSupport ex1 = CloneableEditorSupport.this;
                            synchronized (ex1) {
                                CloneableEditorSupport.this.inUserQuestionExceptionHandler = false;
                            }
                        }
                    });
                }
            }
            locked = false;
            ERR.log(Level.FINE, "Could not lock document", (Throwable)ex);
        }
        catch (IOException e) {
            ERR.log(Level.FINE, "Could not lock document", e);
            String message = null;
            message = e.getMessage() != e.getLocalizedMessage() ? e.getLocalizedMessage() : Exceptions.findLocalizedMessage((Throwable)e);
            if (message != null) {
                StatusDisplayer.getDefault().setStatusText(message);
            }
            locked = false;
        }
        if (!locked) {
            Toolkit.getDefaultToolkit().beep();
            ERR.log(Level.FINE, "notifyModified returns false");
            return false;
        }
        lastReusable.clear();
        this.updateTitles();
        if (ERR.isLoggable(Level.FINE)) {
            ERR.log(Level.FINE, "notifyModified returns true; env.isModified()=" + this.env.isModified());
        }
        return true;
    }

    protected void notifyClosed() {
        this.annotationsLoaded = false;
        this.closeDocument();
    }

    final StyledDocument getDocumentHack() {
        return this.getDoc();
    }

    final Lookup getLookup() {
        return this.lookup;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    Line.Set updateLineSet(boolean clear) {
        Object object = this.getLock();
        synchronized (object) {
            if (this.lineSet[0] != null && !clear) {
                return this.lineSet[0];
            }
            this.lineSet[0] = this.getDoc() == null || this.openClose.getDocumentStatusLA() == DocumentStatus.RELOADING ? new EditorSupportLineSet.Closed(this) : new EditorSupportLineSet(this, this.getDoc());
            return this.lineSet[0];
        }
    }

    protected boolean close(boolean ask) {
        CloneableEditorSupport redirect = CloneableEditorSupportRedirector.findRedirect(this);
        if (redirect != null) {
            return redirect.close(ask);
        }
        if (!super.close(ask)) {
            return false;
        }
        this.notifyClosed();
        return true;
    }

    private void closeDocument() {
        this.openClose.close();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void checkReload(JEditorPane[] openedPanes, boolean doReload) {
        StyledDocument d;
        Object object = this.getLock();
        synchronized (object) {
            d = this.getDoc();
        }
        if (!doReload && !this.reloadDialogOpened) {
            String msg = NbBundle.getMessage(CloneableEditorSupport.class, (String)"FMT_External_change", (Object)d.getProperty("title"));
            NotifyDescriptor.Confirmation nd = new NotifyDescriptor.Confirmation((Object)msg, 0);
            this.reloadDialogOpened = true;
            try {
                Object ret = DialogDisplayer.getDefault().notify((NotifyDescriptor)nd);
                if (NotifyDescriptor.YES_OPTION.equals(ret)) {
                    doReload = true;
                }
            }
            finally {
                this.reloadDialogOpened = false;
            }
        }
        if (doReload) {
            this.openClose.reload(openedPanes);
            this.reloadDocument();
        }
    }

    private static StyledDocument createNetBeansDocument(Document d) {
        if (d instanceof StyledDocument) {
            return (StyledDocument)d;
        }
        return new FilterDocument(d);
    }

    final void fireDocumentChange(StyledDocument document, boolean closing) {
        this.fireStateChangeEvent(document, closing);
        this.firePropertyChange("document", closing ? document : null, closing ? null : document);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private final void fireStateChangeEvent(StyledDocument document, boolean closing) {
        if (this.listeners != null) {
            ChangeListener[] ls;
            EnhancedChangeEvent event = new EnhancedChangeEvent((Object)((Object)this), document, closing);
            CloneableEditorSupport cloneableEditorSupport = this;
            synchronized (cloneableEditorSupport) {
                ls = this.listeners.toArray(new ChangeListener[this.listeners.size()]);
            }
            for (ChangeListener l : ls) {
                l.stateChanged(event);
            }
        }
    }

    protected void updateTitles() {
        Enumeration en = this.allEditors.getComponents();
        while (en.hasMoreElements()) {
            CloneableTopComponent o = (CloneableTopComponent)en.nextElement();
            Pane e = (Pane)o.getClientProperty((Object)"CloneableEditorSupport.Pane");
            if (e == null && o instanceof Pane) {
                e = (Pane)o;
            }
            if (e != null) {
                e.updateName();
                continue;
            }
            throw new IllegalStateException("No reference to Pane. Please file a bug against openide/text");
        }
    }

    private static void replaceTc(TopComponent orig, TopComponent open) {
        int pos = orig.getTabPosition();
        orig.close();
        open.openAtTabPosition(pos);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Pane openPane(boolean reuse) {
        String msg;
        Pane ce = null;
        boolean displayMsgOpened = false;
        Object object = this.getLock();
        synchronized (object) {
            ce = this.getAnyEditor();
            if (ce == null) {
                msg = this.messageOpening();
                if (msg != null) {
                    StatusDisplayer.getDefault().setStatusText(msg);
                }
                this.prepareDocument();
                ce = this.createPane();
                ce.getComponent().putClientProperty((Object)"CloneableEditorSupport.Pane", (Object)ce);
                ce.getComponent().setReference(this.allEditors);
                displayMsgOpened = true;
            }
        }
        CloneableTopComponent ctc = ce.getComponent();
        if (reuse && displayMsgOpened) {
            CloneableTopComponent last = lastReusable.get();
            if (last != null) {
                CloneableEditorSupport.replaceTc((TopComponent)last, (TopComponent)ctc);
            } else {
                ctc.open();
            }
            lastReusable = new WeakReference<CloneableTopComponent>(ctc);
        } else {
            ctc.open();
        }
        if (displayMsgOpened) {
            msg = this.messageOpened();
            if (msg == null) {
                msg = "";
            }
            StatusDisplayer.getDefault().setStatusText(msg);
        }
        return ce;
    }

    Pane getAnyEditor() {
        CloneableTopComponent ctc = this.allEditors.getArbitraryComponent();
        if (ctc == null) {
            return null;
        }
        Pane e = (Pane)ctc.getClientProperty((Object)"CloneableEditorSupport.Pane");
        if (e != null) {
            return e;
        }
        if (ctc instanceof Pane) {
            return (Pane)ctc;
        }
        Enumeration en = this.allEditors.getComponents();
        if (en.hasMoreElements()) {
            ctc = (CloneableTopComponent)en.nextElement();
            e = (Pane)ctc.getClientProperty((Object)"CloneableEditorSupport.Pane");
            if (e != null) {
                return e;
            }
            if (ctc instanceof Pane) {
                return (Pane)ctc;
            }
            throw new IllegalStateException("No reference to Pane. Please file a bug against openide/text");
        }
        return null;
    }

    @Deprecated
    final Pane openReuse(PositionRef pos, int column, int mode) {
        if (mode == 5) {
            lastReusable.clear();
        }
        return this.openAtImpl(pos, column, true);
    }

    final Pane openReuse(PositionRef pos, int column, Line.ShowOpenType mode) {
        if (mode == Line.ShowOpenType.REUSE_NEW) {
            lastReusable.clear();
        }
        return this.openAtImpl(pos, column, true);
    }

    protected final Pane openAt(PositionRef pos, int column) {
        return this.openAtImpl(pos, column, false);
    }

    private final Pane openAtImpl(PositionRef pos, int column, boolean reuse) {
        class Selector
        implements TaskListener,
        Runnable {
            private boolean documentLocked;
            final /* synthetic */ Pane val$e;
            final /* synthetic */ PositionRef val$pos;
            final /* synthetic */ int val$column;

            Selector() {
                this.val$e = var2_2;
                this.val$pos = var3_3;
                this.val$column = n;
                this.documentLocked = false;
            }

            public void taskFinished(Task t2) {
                SwingUtilities.invokeLater(this);
                t2.removeTaskListener((TaskListener)this);
            }

            @Override
            public void run() {
                JEditorPane ePane = this.val$e.getEditorPane();
                if (ePane == null) {
                    return;
                }
                StyledDocument doc = this$0.getDocument();
                if (doc == null) {
                    return;
                }
                if (!this.documentLocked) {
                    this.documentLocked = true;
                    doc.render(this);
                } else {
                    Caret caret = ePane.getCaret();
                    if (caret == null) {
                        return;
                    }
                    Document paneDoc = ePane.getDocument();
                    if (paneDoc instanceof StyledDocument && paneDoc != doc) {
                        if (CloneableEditorSupport.ERR.isLoggable(Level.FINE)) {
                            CloneableEditorSupport.ERR.fine("paneDoc=" + paneDoc + "\n !=\ndoc=" + doc);
                        }
                        doc = (StyledDocument)paneDoc;
                    }
                    Element el = NbDocument.findLineRootElement(doc);
                    int offset = (el = el.getElement(el.getElementIndex(this.val$pos.getOffset()))).getStartOffset() + Math.max(0, this.val$column);
                    if (offset > el.getEndOffset()) {
                        offset = Math.max(el.getStartOffset(), el.getEndOffset() - 1);
                    }
                    caret.setDot(offset);
                    try {
                        Rectangle r = ePane.modelToView(offset);
                        if (r != null) {
                            r.height *= 5;
                            ePane.scrollRectToVisible(r);
                        }
                    }
                    catch (BadLocationException ex) {
                        CloneableEditorSupport.ERR.log(Level.WARNING, "Can't scroll to text: pos.getOffset=" + this.val$pos.getOffset() + ", column=" + this.val$column + ", offset=" + offset + ", doc.getLength=" + doc.getLength(), ex);
                    }
                }
            }
        }
        CloneableEditorSupport redirect = CloneableEditorSupportRedirector.findRedirect(this);
        if (redirect != null) {
            return redirect.openAtImpl(pos, column, reuse);
        }
        Pane e = this.openPane(reuse);
        Task t = this.prepareDocument();
        e.ensureVisible();
        t.addTaskListener((TaskListener)new Selector(this, e, pos, column));
        return e;
    }

    final Object getLock() {
        return this.allEditors;
    }

    private Listener getListener() {
        if (this.listener == null) {
            this.listener = new Listener();
        }
        return this.listener;
    }

    void setListeningOnEnv(boolean listen) {
        if (listen != this.listeningOnEnv) {
            this.listeningOnEnv = listen;
            if (listen) {
                this.cesEnv().addPropertyChangeListener((PropertyChangeListener)this.getListener());
            } else {
                this.cesEnv().removePropertyChangeListener((PropertyChangeListener)this.getListener());
            }
        }
    }

    void howToReproduceDeadlock40766(boolean beforeLock) {
    }

    final void setLastSaveTime(long lst) {
        if (ERR.isLoggable(Level.FINE)) {
            ERR.fine(this.documentID() + ": Setting new lastSaveTime to " + lst + ", " + new Date(lst));
        }
        this.lastSaveTime = lst;
    }

    final void updateLastSaveTime() {
        this.setLastSaveTime(this.cesEnv().getTime().getTime());
    }

    final boolean isAlreadyModified() {
        return this.alreadyModified;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void setAlreadyModified(boolean alreadyModified) {
        if (alreadyModified != this.alreadyModified) {
            if (ERR.isLoggable(Level.FINE)) {
                boolean origModified;
                Object object = this.checkModificationLock;
                synchronized (object) {
                    origModified = this.isAlreadyModified();
                }
                ERR.fine(this.documentID() + ": setAlreadyModified from " + origModified + " to " + alreadyModified);
                ERR.log(Level.FINEST, null, new Exception("Setting to modified: " + alreadyModified));
            }
            this.alreadyModified = alreadyModified;
            this.openClose.setDocumentStronglyReferenced(alreadyModified);
        }
    }

    final void setPreventModification(boolean preventModification) {
        this.preventModification = preventModification;
    }

    StyledDocument getDoc() {
        return this.openClose.getRefDocument();
    }

    public String toString() {
        return "CES: " + this.openClose;
    }

    private final class DocFilter
    extends DocumentFilter {
        final DocumentFilter origFilter;

        DocFilter(DocumentFilter origFilter) {
            this.origFilter = origFilter;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void insertString(DocumentFilter.FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
            boolean origModified = this.checkModificationAllowed(offset);
            boolean success = false;
            try {
                if (this.origFilter != null) {
                    this.origFilter.insertString(fb, offset, string, attr);
                } else {
                    super.insertString(fb, offset, string, attr);
                }
                success = true;
            }
            finally {
                if (!success && !origModified) {
                    CloneableEditorSupport.this.callNotifyUnmodified();
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void remove(DocumentFilter.FilterBypass fb, int offset, int length) throws BadLocationException {
            boolean origModified = this.checkModificationAllowed(offset);
            boolean success = false;
            try {
                if (this.origFilter != null) {
                    this.origFilter.remove(fb, offset, length);
                } else {
                    super.remove(fb, offset, length);
                }
                success = true;
            }
            finally {
                if (!success && !origModified) {
                    CloneableEditorSupport.this.callNotifyUnmodified();
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void replace(DocumentFilter.FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
            boolean origModified = this.checkModificationAllowed(offset);
            boolean success = false;
            try {
                if (this.origFilter != null) {
                    this.origFilter.replace(fb, offset, length, text, attrs);
                } else {
                    super.replace(fb, offset, length, text, attrs);
                }
                success = true;
            }
            finally {
                if (!success && !origModified) {
                    CloneableEditorSupport.this.callNotifyUnmodified();
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private boolean checkModificationAllowed(int offset) throws BadLocationException {
            boolean alreadyModified;
            Object object = CloneableEditorSupport.this.checkModificationLock;
            synchronized (object) {
                alreadyModified = CloneableEditorSupport.this.isAlreadyModified();
            }
            if (!CloneableEditorSupport.this.callNotifyModified()) {
                this.modificationNotAllowed(offset);
            }
            return alreadyModified;
        }

        private void modificationNotAllowed(int offset) throws BadLocationException {
            throw new BadLocationException("Modification not allowed", offset);
        }
    }

    private final class Listener
    implements PropertyChangeListener,
    DocumentListener,
    VetoableChangeListener {
        private boolean revertModifiedFlag;

        Listener() {
        }

        @Override
        public void insertUpdate(DocumentEvent evt) {
            CloneableEditorSupport.this.callNotifyModified();
            this.revertModifiedFlag = false;
        }

        @Override
        public void removeUpdate(DocumentEvent evt) {
            CloneableEditorSupport.this.callNotifyModified();
            this.revertModifiedFlag = false;
        }

        @Override
        public void changedUpdate(DocumentEvent evt) {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
            if ("modified".equals(evt.getPropertyName())) {
                if (Boolean.TRUE.equals(evt.getNewValue())) {
                    boolean alreadyModified;
                    Object object = CloneableEditorSupport.this.checkModificationLock;
                    synchronized (object) {
                        alreadyModified = CloneableEditorSupport.this.isAlreadyModified();
                    }
                    if (!CloneableEditorSupport.this.callNotifyModified()) {
                        throw new PropertyVetoException("Not allowed", evt);
                    }
                    this.revertModifiedFlag = !alreadyModified;
                } else if (this.revertModifiedFlag) {
                    CloneableEditorSupport.this.callNotifyUnmodified();
                }
            }
        }

        @Override
        public void propertyChange(PropertyChangeEvent ev) {
            if ("expectedTime".equals(ev.getPropertyName())) {
                CloneableEditorSupport.this.lastSaveTime = ((Date)ev.getNewValue()).getTime();
            }
            if ("time".equals(ev.getPropertyName())) {
                Date time;
                CloneableEditorSupport.ERR.fine(CloneableEditorSupport.this.documentID() + ": PROP_TIME new value: " + time + ", " + ((time = (Date)ev.getNewValue()) != null ? time.getTime() : -1));
                CloneableEditorSupport.ERR.fine("       lastSaveTime: " + new Date(CloneableEditorSupport.this.lastSaveTime) + ", " + CloneableEditorSupport.this.lastSaveTime);
                boolean reload = CloneableEditorSupport.this.lastSaveTime != -1 && (time == null || time.getTime() > CloneableEditorSupport.this.lastSaveTime || time.getTime() + 10000 < CloneableEditorSupport.this.lastSaveTime);
                CloneableEditorSupport.ERR.fine("             reload: " + reload);
                if (reload) {
                    SwingUtilities.invokeLater(new Runnable(){
                        private boolean inRunAtomic;
                        private JEditorPane[] openedPanes;

                        /*
                         * WARNING - Removed try catching itself - possible behaviour change.
                         */
                        @Override
                        public void run() {
                            if (!this.inRunAtomic) {
                                this.inRunAtomic = true;
                                StyledDocument sd = CloneableEditorSupport.this.getDoc();
                                if (sd == null) {
                                    return;
                                }
                                this.openedPanes = CloneableEditorSupport.this.getOpenedPanes();
                                CloneableEditorSupport.this.preventModification = true;
                                try {
                                    NbDocument.runAtomic(sd, this);
                                }
                                finally {
                                    CloneableEditorSupport.this.preventModification = false;
                                }
                                return;
                            }
                            boolean noAsk = time == null || !CloneableEditorSupport.this.isModified();
                            CloneableEditorSupport.ERR.fine(CloneableEditorSupport.this.documentID() + ": checkReload noAsk: " + noAsk);
                            CloneableEditorSupport.this.checkReload(this.openedPanes, noAsk);
                        }
                    });
                    CloneableEditorSupport.ERR.fine(CloneableEditorSupport.this.documentID() + ": reload task posted");
                }
            }
            if ("modified".equals(ev.getPropertyName())) {
                CloneableEditorSupport.this.firePropertyChange("modified", ev.getOldValue(), ev.getNewValue());
            }
            if ("DataEditorSupport.read-only.changing".equals(ev.getPropertyName())) {
                Object o = ev.getNewValue();
                if (o == Boolean.TRUE) {
                    StyledDocument d = CloneableEditorSupport.this.getDoc();
                    d.putProperty("editable", Boolean.TRUE);
                }
                CloneableEditorSupport.this.updateTitles();
            }
        }

    }

    private static final class PlainEditorKit
    extends DefaultEditorKit
    implements ViewFactory {
        static final long serialVersionUID = -5788777967029507963L;

        PlainEditorKit() {
        }

        @Override
        public Object clone() {
            return new PlainEditorKit();
        }

        @Override
        public ViewFactory getViewFactory() {
            return this;
        }

        @Override
        public View create(Element elem) {
            return new WrappedPlainView(elem);
        }

        @Override
        public void install(JEditorPane pane) {
            super.install(pane);
            pane.setFont(new Font("Monospaced", 0, pane.getFont().getSize() + 1));
        }
    }

    public static interface Pane {
        public JEditorPane getEditorPane();

        public CloneableTopComponent getComponent();

        public void updateName();

        public void ensureVisible();
    }

    public static interface Env
    extends CloneableOpenSupport.Env {
        public static final String PROP_TIME = "time";

        public InputStream inputStream() throws IOException;

        public OutputStream outputStream() throws IOException;

        public Date getTime();

        public String getMimeType();
    }

}

