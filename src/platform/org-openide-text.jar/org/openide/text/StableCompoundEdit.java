/*
 * Decompiled with CFR 0_118.
 */
package org.openide.text;

import java.util.ArrayList;
import java.util.List;
import javax.swing.UIManager;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;

public class StableCompoundEdit
implements UndoableEdit {
    private static final int HAS_BEEN_DONE = 1;
    private static final int ALIVE = 2;
    private static final int IN_PROGRESS = 4;
    private int statusBits = 7;
    private ArrayList<UndoableEdit> edits = new ArrayList(4);

    public final List<UndoableEdit> getEdits() {
        return this.edits;
    }

    @Override
    public void die() {
        this.clearStatusBits(2);
        int size = this.edits.size();
        for (int i = size - 1; i >= 0; --i) {
            this.edits.get(i).die();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void undo() throws CannotUndoException {
        int i;
        if (!this.canUndo()) {
            throw new CannotUndoException();
        }
        try {
            for (i = this.edits.size() - 1; i >= 0; --i) {
                this.edits.get(i).undo();
            }
            this.clearStatusBits(1);
        }
        finally {
            if (i != -1) {
                int size = this.edits.size();
                while (++i < size) {
                    this.edits.get(i).redo();
                }
            }
        }
    }

    @Override
    public boolean canUndo() {
        return this.isAnyStatusBit(2) && this.isAnyStatusBit(1) && !this.isInProgress();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void redo() throws CannotRedoException {
        int i;
        if (!this.canRedo()) {
            throw new CannotRedoException();
        }
        int size = this.edits.size();
        try {
            for (i = 0; i < size; ++i) {
                this.edits.get(i).redo();
            }
            this.setStatusBits(1);
        }
        finally {
            if (i != size) {
                while (--i >= 0) {
                    this.edits.get(i).undo();
                }
            }
        }
    }

    @Override
    public boolean canRedo() {
        return this.isAnyStatusBit(2) && !this.isAnyStatusBit(1) && !this.isInProgress();
    }

    @Override
    public boolean addEdit(UndoableEdit anEdit) {
        if (!this.isInProgress()) {
            return false;
        }
        UndoableEdit last = this.lastEdit();
        if (last == null) {
            this.edits.add(anEdit);
        } else if (!last.addEdit(anEdit)) {
            if (anEdit.replaceEdit(last)) {
                this.edits.remove(this.edits.size() - 1);
            }
            this.edits.add(anEdit);
        }
        return true;
    }

    @Override
    public boolean replaceEdit(UndoableEdit anEdit) {
        return false;
    }

    @Override
    public boolean isSignificant() {
        int size = this.edits.size();
        for (int i = 0; i < size; ++i) {
            if (!this.edits.get(i).isSignificant()) continue;
            return true;
        }
        return false;
    }

    @Override
    public String getPresentationName() {
        UndoableEdit last = this.lastEdit();
        if (last != null) {
            return last.getPresentationName();
        }
        return "";
    }

    @Override
    public String getUndoPresentationName() {
        UndoableEdit last = this.lastEdit();
        if (last != null) {
            return last.getUndoPresentationName();
        }
        String name = this.getPresentationName();
        name = !"".equals(name) ? UIManager.getString("AbstractUndoableEdit.undoText") + " " + name : UIManager.getString("AbstractUndoableEdit.undoText");
        return name;
    }

    @Override
    public String getRedoPresentationName() {
        UndoableEdit last = this.lastEdit();
        if (last != null) {
            return last.getRedoPresentationName();
        }
        String name = this.getPresentationName();
        name = !"".equals(name) ? UIManager.getString("AbstractUndoableEdit.redoText") + " " + name : UIManager.getString("AbstractUndoableEdit.redoText");
        return name;
    }

    public void end() {
        this.clearStatusBits(4);
        this.edits.trimToSize();
    }

    public boolean isInProgress() {
        return this.isAnyStatusBit(4);
    }

    protected UndoableEdit lastEdit() {
        int count = this.edits.size();
        if (count > 0) {
            return this.edits.get(count - 1);
        }
        return null;
    }

    private boolean isAnyStatusBit(int bits) {
        return (this.statusBits & bits) != 0;
    }

    private void setStatusBits(int bits) {
        this.statusBits |= bits;
    }

    private void clearStatusBits(int bits) {
        this.statusBits &= ~ bits;
    }

    public String toString() {
        return super.toString() + " hasBeenDone: " + this.isAnyStatusBit(1) + " alive: " + this.isAnyStatusBit(2) + " inProgress: " + this.isInProgress() + " edits: " + this.edits;
    }
}

