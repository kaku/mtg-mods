/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.WeakListeners
 */
package org.openide.text;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.Position;
import org.openide.util.WeakListeners;

class BackwardPosition
implements Position,
DocumentListener {
    private int offset;

    private BackwardPosition(int offset) {
        this.offset = offset;
    }

    public static Position create(Document doc, int offset) {
        BackwardPosition p = new BackwardPosition(offset);
        doc.addDocumentListener(WeakListeners.document((DocumentListener)p, (Object)doc));
        return p;
    }

    @Override
    public int getOffset() {
        return this.offset;
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        if (e.getOffset() < this.offset) {
            this.offset += e.getLength();
        }
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        int o = e.getOffset();
        if (o < this.offset) {
            this.offset -= e.getLength();
            if (this.offset < o) {
                this.offset = o;
            }
        }
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }
}

