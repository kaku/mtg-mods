/*
 * Decompiled with CFR 0_118.
 */
package org.openide.text;

import java.awt.Color;
import java.awt.Font;
import javax.swing.event.DocumentListener;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.Segment;
import javax.swing.text.Style;
import javax.swing.text.StyledDocument;
import javax.swing.text.html.HTMLDocument;

public class FilterDocument
implements StyledDocument {
    private static Element leaf;
    protected Document original;

    public FilterDocument(Document original) {
        this.original = original;
    }

    @Override
    public int getLength() {
        return this.original.getLength();
    }

    @Override
    public void addDocumentListener(DocumentListener l) {
        this.original.addDocumentListener(l);
    }

    @Override
    public void removeDocumentListener(DocumentListener l) {
        this.original.removeDocumentListener(l);
    }

    @Override
    public void addUndoableEditListener(UndoableEditListener listener) {
        this.original.addUndoableEditListener(listener);
    }

    @Override
    public void removeUndoableEditListener(UndoableEditListener listener) {
        this.original.removeUndoableEditListener(listener);
    }

    @Override
    public Object getProperty(Object key) {
        if (key == DocumentFilter.class && this.original instanceof AbstractDocument) {
            return ((AbstractDocument)this.original).getDocumentFilter();
        }
        return this.original.getProperty(key);
    }

    @Override
    public void putProperty(Object key, Object value) {
        if (key == DocumentFilter.class && this.original instanceof AbstractDocument) {
            ((AbstractDocument)this.original).setDocumentFilter((DocumentFilter)value);
        } else {
            this.original.putProperty(key, value);
        }
    }

    @Override
    public void remove(int offset, int len) throws BadLocationException {
        this.original.remove(offset, len);
    }

    @Override
    public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
        this.original.insertString(offset, str, a);
    }

    @Override
    public String getText(int offset, int len) throws BadLocationException {
        return this.original.getText(offset, len);
    }

    @Override
    public void getText(int offset, int len, Segment txt) throws BadLocationException {
        this.original.getText(offset, len, txt);
    }

    @Override
    public Position getStartPosition() {
        return this.original.getStartPosition();
    }

    @Override
    public Position getEndPosition() {
        return this.original.getEndPosition();
    }

    @Override
    public Position createPosition(int offset) throws BadLocationException {
        return this.original.createPosition(offset);
    }

    @Override
    public Element[] getRootElements() {
        return this.original.getRootElements();
    }

    @Override
    public Element getDefaultRootElement() {
        return this.original.getDefaultRootElement();
    }

    @Override
    public void render(Runnable r) {
        this.original.render(r);
    }

    @Override
    public Style addStyle(String nm, Style parent) {
        return null;
    }

    @Override
    public void removeStyle(String nm) {
    }

    @Override
    public Style getStyle(String nm) {
        return null;
    }

    @Override
    public void setCharacterAttributes(int offset, int length, AttributeSet s, boolean replace) {
    }

    @Override
    public void setParagraphAttributes(int offset, int length, AttributeSet s, boolean replace) {
    }

    @Override
    public void setLogicalStyle(int pos, Style s) {
    }

    @Override
    public Style getLogicalStyle(int p) {
        return null;
    }

    @Override
    public Element getParagraphElement(int pos) {
        Element e = this.getDefaultRootElement();
        if (e != null && !e.isLeaf()) {
            int index = e.getElementIndex(pos);
            e = e.getElement(index);
        } else {
            e = FilterDocument.getLeafElement();
        }
        return e;
    }

    @Override
    public Element getCharacterElement(int pos) {
        return FilterDocument.getLeafElement();
    }

    @Override
    public Color getForeground(AttributeSet attr) {
        return Color.black;
    }

    @Override
    public Color getBackground(AttributeSet attr) {
        return Color.white;
    }

    @Override
    public Font getFont(AttributeSet attr) {
        return null;
    }

    private static Element getLeafElement() {
        HTMLDocument doc;
        if (leaf != null) {
            return leaf;
        }
        HTMLDocument hTMLDocument = doc = new HTMLDocument();
        hTMLDocument.getClass();
        leaf = new AbstractDocument.LeafElement(hTMLDocument, null, null, 0, 0);
        return leaf;
    }
}

