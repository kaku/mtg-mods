/*
 * Decompiled with CFR 0_118.
 */
package org.openide.text;

enum DocumentStatus {
    CLOSED,
    LOADING,
    OPENED,
    RELOADING;
    

    private DocumentStatus() {
    }
}

