/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 */
package org.openide.text;

import java.awt.Font;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterJob;
import java.util.prefs.Preferences;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

public final class PrintPreferences {
    private static final PrintPreferences INSTANCE = new PrintPreferences();
    private static final String PROP_WRAP = "wrap";
    private static final String PROP_HEADER_FORMAT = "headerFormat";
    private static final String PROP_FOOTER_FORMAT = "footerFormat";
    private static final String PROP_HEADER_ALIGNMENT = "headerAlignment";
    private static final String PROP_FOOTER_ALIGNMENT = "footerAlignment";
    private static final String PROP_PAGE_ORIENTATION = "pageOrientation";
    private static final String PROP_PAGE_WIDTH = "pageWidth";
    private static final String PROP_PAGE_HEIGHT = "pageHeight";
    private static final String PROP_PAGE_IMAGEABLEAREA_Y = "imageableAreaY";
    private static final String PROP_PAGE_IMAGEABLEAREA_X = "imageableAreaX";
    private static final String PROP_PAGE_IMAGEABLEAREA_WIDTH = "imageableAreaWidth";
    private static final String PROP_PAGE_IMAGEABLEAREA_HEIGHT = "imageableAreaHeight";
    private static final String PROP_HEADER_FONT_NAME = "headerFontName";
    private static final String PROP_HEADER_FONT_STYLE = "headerFontStyle";
    private static final String PROP_HEADER_FONT_SIZE = "headerFontSize";
    private static final String PROP_FOOTER_FONT_NAME = "footerFontName";
    private static final String PROP_FOOTER_FONT_STYLE = "footerFontStyle";
    private static final String PROP_FOOTER_FONT_SIZE = "footerFontSize";
    private static final String DEFAULT_FONT_NAME = "Monospaced";
    private static final int DEFAULT_FONT_STYLE = 0;
    private static final int DEFAULT_FONT_SIZE = 6;
    private static final String PROP_LINE_ASCENT_CORRECTION = "lineAscentCorrection";

    private PrintPreferences() {
    }

    private static Preferences getPreferences() {
        return NbPreferences.forModule(PrintPreferences.class);
    }

    public static PageFormat getPageFormat(PrinterJob pj) {
        PageFormat pageFormat = null;
        pageFormat = pj.defaultPage();
        Paper p = pageFormat.getPaper();
        int pageOrientation = PrintPreferences.getPreferences().getInt("pageOrientation", pageFormat.getOrientation());
        double paperWidth = PrintPreferences.getPreferences().getDouble("pageWidth", p.getWidth());
        double paperHeight = PrintPreferences.getPreferences().getDouble("pageHeight", p.getHeight());
        double iaWidth = PrintPreferences.getPreferences().getDouble("imageableAreaWidth", p.getImageableWidth());
        double iaHeight = PrintPreferences.getPreferences().getDouble("imageableAreaHeight", p.getImageableHeight());
        double iaX = PrintPreferences.getPreferences().getDouble("imageableAreaX", p.getImageableX());
        double iaY = PrintPreferences.getPreferences().getDouble("imageableAreaY", p.getImageableY());
        pageFormat.setOrientation(pageOrientation);
        p.setSize(paperWidth, paperHeight);
        p.setImageableArea(iaX, iaY, iaWidth, iaHeight);
        pageFormat.setPaper(p);
        return pageFormat;
    }

    public static void setPageFormat(PageFormat pf) {
        PrintPreferences.getPreferences().putInt("pageOrientation", pf.getOrientation());
        PrintPreferences.getPreferences().putDouble("pageWidth", pf.getPaper().getWidth());
        PrintPreferences.getPreferences().putDouble("pageHeight", pf.getPaper().getHeight());
        PrintPreferences.getPreferences().putDouble("imageableAreaWidth", pf.getPaper().getImageableWidth());
        PrintPreferences.getPreferences().putDouble("imageableAreaHeight", pf.getPaper().getImageableHeight());
        PrintPreferences.getPreferences().putDouble("imageableAreaX", pf.getPaper().getImageableX());
        PrintPreferences.getPreferences().putDouble("imageableAreaY", pf.getPaper().getImageableY());
    }

    public static boolean getWrap() {
        return PrintPreferences.getPreferences().getBoolean("wrap", true);
    }

    public static void setWrap(boolean wrap) {
        if (PrintPreferences.getWrap() == wrap) {
            return;
        }
        PrintPreferences.getPreferences().putBoolean("wrap", wrap);
    }

    public static String getHeaderFormat() {
        return PrintPreferences.getPreferences().get("headerFormat", NbBundle.getMessage(PrintPreferences.class, (String)"CTL_Header_format"));
    }

    public static void setHeaderFormat(String s) {
        if (PrintPreferences.getHeaderFormat().equals(s)) {
            return;
        }
        PrintPreferences.getPreferences().put("headerFormat", s);
    }

    public static String getFooterFormat() {
        return PrintPreferences.getPreferences().get("footerFormat", NbBundle.getMessage(PrintPreferences.class, (String)"CTL_Footer_format"));
    }

    public static void setFooterFormat(String s) {
        if (PrintPreferences.getFooterFormat().equals(s)) {
            return;
        }
        PrintPreferences.getPreferences().put("footerFormat", s);
    }

    public static Font getHeaderFont() {
        String name = PrintPreferences.getPreferences().get("headerFontName", "Monospaced");
        int style = PrintPreferences.getPreferences().getInt("headerFontStyle", 0);
        int size = PrintPreferences.getPreferences().getInt("headerFontSize", 6);
        return new Font(name, style, size);
    }

    public static void setHeaderFont(Font f) {
        if (PrintPreferences.getHeaderFont().equals(f)) {
            return;
        }
        PrintPreferences.getPreferences().put("headerFontName", f.getName());
        PrintPreferences.getPreferences().putInt("headerFontStyle", f.getStyle());
        PrintPreferences.getPreferences().putInt("headerFontSize", f.getSize());
    }

    public static Font getFooterFont() {
        String name = PrintPreferences.getPreferences().get("footerFontName", "Monospaced");
        int style = PrintPreferences.getPreferences().getInt("footerFontStyle", 0);
        int size = PrintPreferences.getPreferences().getInt("footerFontSize", 6);
        return new Font(name, style, size);
    }

    public static void setFooterFont(Font f) {
        if (PrintPreferences.getFooterFont().equals(f)) {
            return;
        }
        PrintPreferences.getPreferences().put("footerFontName", f.getName());
        PrintPreferences.getPreferences().putInt("footerFontStyle", f.getStyle());
        PrintPreferences.getPreferences().putInt("footerFontSize", f.getSize());
    }

    public static Alignment getHeaderAlignment() {
        return Alignment.valueOf(PrintPreferences.getPreferences().get("headerAlignment", Alignment.CENTER.name()));
    }

    public static void setHeaderAlignment(Alignment alignment) {
        if (PrintPreferences.getHeaderAlignment().equals((Object)alignment)) {
            return;
        }
        PrintPreferences.getPreferences().put("headerAlignment", alignment.name());
    }

    public static Alignment getFooterAlignment() {
        return Alignment.valueOf(PrintPreferences.getPreferences().get("footerAlignment", Alignment.CENTER.name()));
    }

    public static void setFooterAlignment(Alignment alignment) {
        if (PrintPreferences.getFooterAlignment().equals((Object)alignment)) {
            return;
        }
        PrintPreferences.getPreferences().put("footerAlignment", alignment.name());
    }

    public static float getLineAscentCorrection() {
        return PrintPreferences.getPreferences().getFloat("lineAscentCorrection", 1.0f);
    }

    public static void setLineAscentCorrection(float correction) {
        if (PrintPreferences.getLineAscentCorrection() == correction) {
            return;
        }
        if (correction < 0.0f) {
            throw new IllegalArgumentException();
        }
        PrintPreferences.getPreferences().putFloat("lineAscentCorrection", correction);
    }

    public static enum Alignment {
        LEFT,
        CENTER,
        RIGHT;
        

        private Alignment() {
        }
    }

}

