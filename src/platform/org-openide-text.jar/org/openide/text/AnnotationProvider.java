/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.openide.text;

import org.openide.text.Line;
import org.openide.util.Lookup;

public interface AnnotationProvider {
    public void annotate(Line.Set var1, Lookup var2);
}

