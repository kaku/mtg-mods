/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.UserQuestionException
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package org.openide.text;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.StyledDocument;
import org.openide.text.CloneableEditor;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.NbDocument;
import org.openide.text.PositionRef;
import org.openide.text.QuietEditorPane;
import org.openide.text.UserQuestionExceptionHandler;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.UserQuestionException;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

final class CloneableEditorInitializer
implements Runnable {
    private static final Logger EDITOR_LOG = CloneableEditor.LOG;
    private static final Logger LOG = Logger.getLogger(CloneableEditorInitializer.class.getName());
    private static final RequestProcessor RP = new RequestProcessor("org.openide.text Editor Initialization");
    static final Logger TIMER = Logger.getLogger("TIMER");
    static boolean modalDialog;
    static final List<Runnable> edtRequests;
    static final Runnable processPendingEDTRequestsRunnable;
    final CloneableEditor editor;
    final CloneableEditorSupport ces;
    final JEditorPane pane;
    StyledDocument doc;
    private Phase phase;
    private RequestProcessor.Task task;
    private EditorKit kit;
    private JLabel loadingLabel;
    private UserQuestionException uqe;
    boolean provideUnfinishedPane;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Converted monitor instructions to comments
     * Lifted jumps to return sites
     */
    static void waitForFinishedInitialization(CloneableEditor editor) {
        assert (SwingUtilities.isEventDispatchThread());
        do {
            List<Runnable> list = edtRequests;
            // MONITORENTER : list
            if (!editor.isInitializationRunning()) {
                // MONITOREXIT : list
                return;
            }
            // MONITOREXIT : list
            if (editor.isProvideUnfinishedPane()) {
                return;
            }
            CloneableEditorInitializer.processPendingEDTRequests();
            list = edtRequests;
            // MONITORENTER : list
            if (!editor.isInitializationRunning()) {
                // MONITOREXIT : list
                return;
            }
            try {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("CEI:Will wait() editor=" + System.identityHashCode(editor) + '\n');
                }
                edtRequests.wait(5000);
            }
            catch (InterruptedException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        } while (true);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void processPendingEDTRequests() {
        do {
            Runnable request;
            List<Runnable> list = edtRequests;
            synchronized (list) {
                if (edtRequests.isEmpty()) {
                    break;
                }
                request = edtRequests.remove(0);
            }
            if (request == null) continue;
            request.run();
        } while (true);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void addEDTRequest(Runnable request) {
        List<Runnable> list = edtRequests;
        synchronized (list) {
            edtRequests.add(request);
            CloneableEditorInitializer.notifyEDTRequestsMonitor();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void notifyEDTRequestsMonitor() {
        List<Runnable> list = edtRequests;
        synchronized (list) {
            edtRequests.notifyAll();
        }
    }

    CloneableEditorInitializer(CloneableEditor editor, CloneableEditorSupport ces, JEditorPane pane) {
        this.editor = editor;
        this.ces = ces;
        this.pane = pane;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void start() {
        boolean success = false;
        try {
            this.kit = this.ces.createEditorKit();
            this.addLoadingLabel();
            this.task = RP.create((Runnable)this);
            this.task.setPriority(3);
            this.nextPhase();
            success = true;
        }
        finally {
            if (!success) {
                this.cancelInitialization();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    boolean nextPhase() {
        if (this.phase == null) {
            this.phase = Phase.DOCUMENT_OPEN;
        } else {
            int nextOrdinal = this.phase.ordinal() + 1;
            if (nextOrdinal < Phase.values().length) {
                this.phase = Phase.values()[nextOrdinal];
            } else {
                return false;
            }
        }
        boolean success = false;
        try {
            if (this.phase.isRunInEDT()) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("CEI:addEDTRequest(): " + this + '\n');
                }
                CloneableEditorInitializer.addEDTRequest(this);
                WindowManager.getDefault().invokeWhenUIReady(processPendingEDTRequestsRunnable);
            } else {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("CEI:task.schedule(): " + this + '\n');
                }
                this.task.schedule(0);
            }
            success = true;
        }
        finally {
            if (!success) {
                this.cancelInitialization();
            }
        }
        return true;
    }

    boolean isProvideUnfinishedPane() {
        return this.provideUnfinishedPane;
    }

    void cancelInitialization() {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("CEI:cancelInitialization(): " + this + '\n');
        }
        this.editor.markInitializationFinished(false);
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                Object toClose = (TopComponent)SwingUtilities.getAncestorOfClass(TopComponent.class, (Component)((Object)CloneableEditorInitializer.this.editor));
                if (null == toClose) {
                    toClose = CloneableEditorInitializer.this.editor;
                }
                toClose.close();
            }
        });
    }

    void finishInitialization() {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("CEI:finishInitialization(): " + this + '\n');
        }
        this.editor.markInitializationFinished(true);
    }

    private void addLoadingLabel() {
        this.editor.setLayout((LayoutManager)new BorderLayout());
        this.loadingLabel = new JLabel(NbBundle.getMessage(CloneableEditor.class, (String)"LBL_EditorLoading"));
        this.loadingLabel.setOpaque(true);
        this.loadingLabel.setHorizontalAlignment(0);
        this.loadingLabel.setBorder(new EmptyBorder(new Insets(11, 11, 11, 11)));
        this.loadingLabel.setVisible(false);
        this.editor.add((Component)this.loadingLabel, (Object)"Center");
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Lifted jumps to return sites
     */
    @Override
    public void run() {
        now = System.currentTimeMillis();
        success = false;
        try {
            switch (5.$SwitchMap$org$openide$text$CloneableEditorInitializer$Phase[this.phase.ordinal()]) {
                case 1: {
                    success = this.initDocument();
                    ** break;
                }
                case 2: {
                    success = this.handleUserQuestionExceptionInEDT();
                    ** break;
                }
                case 3: {
                    success = this.initActionMapInEDT();
                    ** break;
                }
                case 4: {
                    success = this.initKit();
                    ** break;
                }
                case 5: {
                    success = this.setKitAndDocumentToPaneInEDT();
                    ** break;
                }
                case 6: {
                    success = this.initCustomEditorAndDecorationsInEDT();
                    ** break;
                }
                case 7: {
                    success = this.firePaneReadyInEDT();
                    ** break;
                }
                case 8: {
                    this.initAnnotations();
                    success = true;
                    ** break;
                }
            }
            throw new IllegalStateException("Wrong state: " + (Object)this.phase + " for " + (Object)this.ces);
        }
        catch (RuntimeException ex) {
            Exceptions.printStackTrace((Throwable)ex);
            throw ex;
        }
        finally {
            if (!success) {
                this.cancelInitialization();
                return;
            }
        }
lbl39: // 8 sources:
        success = false;
        try {
            howLong = System.currentTimeMillis() - now;
            if (CloneableEditorInitializer.TIMER.isLoggable(Level.FINE)) {
                thread = SwingUtilities.isEventDispatchThread() != false ? "EDT" : "RP";
                d = this.doc;
                v0 = who = d == null ? null : d.getProperty("stream");
                if (who == null) {
                    who = this.ces.messageName();
                }
                CloneableEditorInitializer.TIMER.log(Level.FINE, "Open Editor, phase " + (Object)this.phase + ", " + thread + " [ms]", new Object[]{who, howLong});
            }
            success = true;
        }
        finally {
            if (!success) {
                this.cancelInitialization();
            }
        }
        success = false;
        try {
            this.nextPhase();
            success = true;
            return;
        }
        finally {
            if (!success) {
                this.cancelInitialization();
            }
        }
    }

    private boolean initDocument() {
        if (EDITOR_LOG.isLoggable(Level.FINE)) {
            EDITOR_LOG.log(Level.FINE, "CloneableEditorInitializer.initDocument() Enter Time:" + System.currentTimeMillis() + " Thread:" + Thread.currentThread().getName() + " ce:[" + Integer.toHexString(System.identityHashCode(this.editor)) + "]" + " support:[" + Integer.toHexString(System.identityHashCode((Object)this.ces)) + "]" + " Name:" + this.editor.getName());
        }
        try {
            this.setDocument(this.ces.openDocument());
            this.ces.getPositionManager().documentOpened(new WeakReference<StyledDocument>(this.doc));
            assert (this.doc != null);
            return true;
        }
        catch (UserQuestionException ex) {
            this.uqe = ex;
            return true;
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex.getCause());
            return false;
        }
    }

    boolean handleUserQuestionExceptionInEDT() {
        assert (SwingUtilities.isEventDispatchThread());
        if (this.uqe != null) {
            UserQuestionExceptionHandler handler;
            if (EDITOR_LOG.isLoggable(Level.FINE)) {
                EDITOR_LOG.fine("CEI:handleUserQuestionExceptionInEDT: uqe=" + (Object)this.uqe + "\n");
            }
            if ((handler = new UserQuestionExceptionHandler(this.ces, this.uqe){

                @Override
                protected void opened(StyledDocument openDoc) {
                    CloneableEditorInitializer.this.setDocument(openDoc);
                }

                @Override
                protected void handleStart() {
                    CloneableEditorInitializer.modalDialog = true;
                }

                @Override
                protected void handleEnd() {
                    CloneableEditorInitializer.modalDialog = false;
                }
            }).handleUserQuestionException()) {
                this.uqe = null;
            } else {
                this.cancelInitialization();
            }
        }
        if (this.doc == null && this.editor.isInitializationRunning()) {
            throw new IllegalStateException("Null document for non-cancelled initialization. uqe=" + (Object)this.uqe);
        }
        return this.uqe == null;
    }

    private void setDocument(StyledDocument doc) {
        this.doc = doc;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean initActionMapInEDT() {
        ActionMap am = this.editor.getActionMap();
        ActionMap paneMap = this.pane.getActionMap();
        this.provideUnfinishedPane = true;
        try {
            am.setParent(paneMap);
        }
        finally {
            this.provideUnfinishedPane = false;
        }
        paneMap.put("cut-to-clipboard", this.getAction("cut-to-clipboard"));
        paneMap.put("copy-to-clipboard", this.getAction("copy-to-clipboard"));
        paneMap.put("delete", this.getAction("delete-next"));
        paneMap.put("paste-from-clipboard", this.getAction("paste-from-clipboard"));
        return true;
    }

    private boolean initKit() {
        if (this.kit instanceof Callable) {
            try {
                ((Callable)((Object)this.kit)).call();
            }
            catch (Exception e) {
                Exceptions.printStackTrace((Throwable)e);
            }
        }
        return true;
    }

    private Action getAction(String key) {
        if (key == null) {
            return null;
        }
        if (this.kit == null) {
            return null;
        }
        Action[] actions = this.kit.getActions();
        for (int i = 0; i < actions.length; ++i) {
            if (!key.equals(actions[i].getValue("Name"))) continue;
            return actions[i];
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void initCustomEditor() {
        if (this.doc instanceof NbDocument.CustomEditor) {
            Component customComponent;
            NbDocument.CustomEditor ce = (NbDocument.CustomEditor)((Object)this.doc);
            this.provideUnfinishedPane = true;
            try {
                customComponent = ce.createEditor(this.pane);
            }
            finally {
                this.provideUnfinishedPane = false;
            }
            if (customComponent == null) {
                throw new IllegalStateException("Document:" + this.doc + " implementing NbDocument.CustomEditor may not" + " return null component");
            }
            this.editor.setCustomComponent(customComponent);
            this.editor.add(this.ces.wrapEditorComponent(customComponent), (Object)"Center");
        } else {
            JScrollPane noBorderPane = new JScrollPane(this.pane);
            this.pane.setBorder(null);
            this.editor.add(this.ces.wrapEditorComponent(noBorderPane), (Object)"Center");
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void initDecoration() {
        if (this.doc instanceof NbDocument.CustomToolbar) {
            JToolBar customToolbar;
            NbDocument.CustomToolbar ce = (NbDocument.CustomToolbar)((Object)this.doc);
            this.provideUnfinishedPane = true;
            try {
                customToolbar = ce.createToolbar(this.pane);
            }
            finally {
                this.provideUnfinishedPane = false;
            }
            if (customToolbar == null) {
                throw new IllegalStateException("Document:" + this.doc + " implementing NbDocument.CustomToolbar may not" + " return null toolbar");
            }
            Border b = (Border)UIManager.get("Nb.Editor.Toolbar.border");
            customToolbar.setBorder(b);
            this.editor.add((Component)customToolbar, (Object)"North");
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean setKitAndDocumentToPaneInEDT() {
        this.provideUnfinishedPane = true;
        try {
            this.pane.setEditorKit(this.kit);
            ((QuietEditorPane)this.pane).setWorking(1);
            this.pane.setDocument(this.doc);
        }
        finally {
            this.provideUnfinishedPane = false;
        }
        return true;
    }

    private boolean initCustomEditorAndDecorationsInEDT() {
        Caret caret;
        this.initCustomEditor();
        this.initDecoration();
        this.editor.remove((Component)this.loadingLabel);
        ((QuietEditorPane)this.pane).setWorking(3);
        int cursorPosition = this.editor.getCursorPosition();
        if (cursorPosition != -1 && (caret = this.pane.getCaret()) != null) {
            caret.setDot(cursorPosition);
        }
        ActionMap actionMap = this.editor.getActionMap();
        ActionMap p = actionMap.getParent();
        actionMap.setParent(null);
        actionMap.setParent(p);
        if (this.shouldRequestFocus(this.pane)) {
            EDITOR_LOG.log(Level.FINE, "requestFocusInWindow {0}", this.pane);
            this.editor.requestFocusInWindow();
        }
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                CloneableEditorInitializer.this.editor.revalidate();
            }
        });
        this.finishInitialization();
        return true;
    }

    private boolean firePaneReadyInEDT() {
        this.ces.firePropertyChange("openedPanes", null, null);
        return true;
    }

    private void initAnnotations() {
        this.ces.ensureAnnotationsLoaded();
    }

    private boolean shouldRequestFocus(Component c) {
        TopComponent active = TopComponent.getRegistry().getActivated();
        while (c != null) {
            if (c == active) {
                return true;
            }
            c = c.getParent();
        }
        return false;
    }

    public String toString() {
        return "phase=" + (Object)((Object)this.phase) + ", editor=" + System.identityHashCode(this.editor);
    }

    static {
        edtRequests = new ArrayList<Runnable>(2);
        processPendingEDTRequestsRunnable = new Runnable(){

            @Override
            public void run() {
                CloneableEditorInitializer.processPendingEDTRequests();
            }
        };
    }

    static class 5 {
        static final /* synthetic */ int[] $SwitchMap$org$openide$text$CloneableEditorInitializer$Phase;

        static {
            $SwitchMap$org$openide$text$CloneableEditorInitializer$Phase = new int[Phase.values().length];
            try {
                5.$SwitchMap$org$openide$text$CloneableEditorInitializer$Phase[Phase.DOCUMENT_OPEN.ordinal()] = 1;
            }
            catch (NoSuchFieldError ex) {
                // empty catch block
            }
            try {
                5.$SwitchMap$org$openide$text$CloneableEditorInitializer$Phase[Phase.HANDLE_USER_QUESTION_EXCEPTION.ordinal()] = 2;
            }
            catch (NoSuchFieldError ex) {
                // empty catch block
            }
            try {
                5.$SwitchMap$org$openide$text$CloneableEditorInitializer$Phase[Phase.ACTION_MAP.ordinal()] = 3;
            }
            catch (NoSuchFieldError ex) {
                // empty catch block
            }
            try {
                5.$SwitchMap$org$openide$text$CloneableEditorInitializer$Phase[Phase.INIT_KIT.ordinal()] = 4;
            }
            catch (NoSuchFieldError ex) {
                // empty catch block
            }
            try {
                5.$SwitchMap$org$openide$text$CloneableEditorInitializer$Phase[Phase.KIT_AND_DOCUMENT_TO_PANE.ordinal()] = 5;
            }
            catch (NoSuchFieldError ex) {
                // empty catch block
            }
            try {
                5.$SwitchMap$org$openide$text$CloneableEditorInitializer$Phase[Phase.CUSTOM_EDITOR_AND_DECORATIONS.ordinal()] = 6;
            }
            catch (NoSuchFieldError ex) {
                // empty catch block
            }
            try {
                5.$SwitchMap$org$openide$text$CloneableEditorInitializer$Phase[Phase.FIRE_PANE_READY.ordinal()] = 7;
            }
            catch (NoSuchFieldError ex) {
                // empty catch block
            }
            try {
                5.$SwitchMap$org$openide$text$CloneableEditorInitializer$Phase[Phase.ANNOTATIONS.ordinal()] = 8;
            }
            catch (NoSuchFieldError ex) {
                // empty catch block
            }
        }
    }

    static enum Phase {
        DOCUMENT_OPEN(false),
        HANDLE_USER_QUESTION_EXCEPTION(true),
        ACTION_MAP(true),
        INIT_KIT(false),
        KIT_AND_DOCUMENT_TO_PANE(true),
        CUSTOM_EDITOR_AND_DECORATIONS(true),
        FIRE_PANE_READY(true),
        ANNOTATIONS(false);
        
        private final boolean runInEDT;

        private Phase(boolean runInEDT) {
            this.runInEDT = runInEDT;
        }

        public boolean isRunInEDT() {
            return this.runInEDT;
        }
    }

}

