/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Task
 *  org.openide.util.WeakListeners
 */
package org.openide.text;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.lang.ref.Reference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.openide.text.Annotation;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.LazyLines;
import org.openide.text.Line;
import org.openide.text.LineListener;
import org.openide.text.NbDocument;
import org.openide.text.PositionRef;
import org.openide.util.Lookup;
import org.openide.util.Task;
import org.openide.util.WeakListeners;

public abstract class DocumentLine
extends Line {
    private static Map<CloneableEditorSupport, DocumentLine[]> assigned = new WeakHashMap<CloneableEditorSupport, DocumentLine[]>(5);
    private static final long serialVersionUID = 3213776466939427487L;
    protected PositionRef pos;
    @Deprecated
    private boolean breakpoint;
    @Deprecated
    private transient boolean error;
    @Deprecated
    private transient boolean current;
    private transient LR listener;
    private transient DocumentListener docL;
    private final List<Part> lineParts = Collections.synchronizedList(new ArrayList(3));

    public DocumentLine(Lookup obj, PositionRef pos) {
        super(obj);
        this.pos = pos;
    }

    void init() {
        this.listener = new LR(this);
    }

    @Override
    public int getLineNumber() {
        try {
            return this.pos.getLine();
        }
        catch (IOException ex) {
            return 0;
        }
    }

    @Deprecated
    @Override
    public abstract void show(int var1, int var2);

    @Deprecated
    @Override
    public void setBreakpoint(boolean b) {
        if (this.breakpoint != b) {
            this.breakpoint = b;
            this.refreshState();
        }
    }

    @Deprecated
    @Override
    public boolean isBreakpoint() {
        return this.breakpoint;
    }

    @Deprecated
    @Override
    public void markError() {
        DocumentLine previous = this.registerLine(1, this);
        if (previous != null) {
            previous.error = false;
            previous.refreshState();
        }
        this.error = true;
        this.refreshState();
    }

    @Deprecated
    @Override
    public void unmarkError() {
        this.error = false;
        this.registerLine(1, null);
        this.refreshState();
    }

    @Deprecated
    @Override
    public void markCurrentLine() {
        DocumentLine previous = this.registerLine(0, this);
        if (previous != null) {
            previous.current = false;
            previous.refreshState();
        }
        this.current = true;
        this.refreshState();
    }

    @Deprecated
    @Override
    public void unmarkCurrentLine() {
        this.current = false;
        this.registerLine(0, null);
        this.refreshState();
    }

    @Deprecated
    synchronized void refreshState() {
        StyledDocument doc = this.pos.getCloneableEditorSupport().getDocument();
        if (doc != null) {
            if (this.docL != null) {
                doc.removeDocumentListener(this.docL);
            }
            if (this.error) {
                NbDocument.markError(doc, this.pos.getOffset());
                this.docL = WeakListeners.document((DocumentListener)this.listener, (Object)doc);
                doc.addDocumentListener(this.docL);
                return;
            }
            if (this.current) {
                NbDocument.markCurrent(doc, this.pos.getOffset());
                return;
            }
            if (this.breakpoint) {
                NbDocument.markBreakpoint(doc, this.pos.getOffset());
                return;
            }
            NbDocument.markNormal(doc, this.pos.getOffset());
            return;
        }
    }

    public int hashCode() {
        return this.pos.getCloneableEditorSupport().hashCode();
    }

    public boolean equals(Object o) {
        if (o instanceof DocumentLine) {
            DocumentLine dl = (DocumentLine)o;
            if (dl.pos.getCloneableEditorSupport() == this.pos.getCloneableEditorSupport()) {
                return dl.getLineNumber() == this.getLineNumber();
            }
        }
        return false;
    }

    @Deprecated
    private DocumentLine registerLine(int indx, DocumentLine line) {
        DocumentLine prev;
        CloneableEditorSupport es = this.pos.getCloneableEditorSupport();
        DocumentLine[] arr = assigned.get((Object)((Object)es));
        if (arr != null) {
            prev = arr[indx];
        } else {
            arr = new DocumentLine[2];
            assigned.put(es, arr);
            prev = null;
        }
        arr[indx] = line;
        return prev;
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.writeObject(this.pos);
        oos.writeBoolean(this.breakpoint);
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        this.pos = (PositionRef)ois.readObject();
        this.setBreakpoint(ois.readBoolean());
        this.lineParts.clear();
    }

    Object readResolve() throws ObjectStreamException {
        return this.pos.getCloneableEditorSupport().getLineSet().registerLine(this);
    }

    @Override
    protected void addAnnotation(final Annotation anno) {
        super.addAnnotation(anno);
        final StyledDocument doc = this.pos.getCloneableEditorSupport().getDocument();
        if (doc == null) {
            return;
        }
        this.pos.getCloneableEditorSupport().prepareDocument().waitFinished();
        try {
            final Position p = this.pos.getPosition();
            doc.render(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    List<? extends Annotation> list = DocumentLine.this.getAnnotations();
                    synchronized (list) {
                        if (!anno.isInDocument()) {
                            anno.setInDocument(true);
                            FindAnnotationPosition fap = new FindAnnotationPosition(doc, p);
                            doc.render(fap);
                            NbDocument.addAnnotation(doc, fap.getAnnotationPosition(), -1, anno);
                        }
                    }
                }
            });
        }
        catch (IOException ex) {
            Logger.getLogger(DocumentLine.class.getName()).log(Level.WARNING, null, ex);
        }
    }

    @Override
    protected void removeAnnotation(final Annotation anno) {
        super.removeAnnotation(anno);
        final StyledDocument doc = this.pos.getCloneableEditorSupport().getDocument();
        if (doc == null) {
            return;
        }
        this.pos.getCloneableEditorSupport().prepareDocument().waitFinished();
        doc.render(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                List<? extends Annotation> list = DocumentLine.this.getAnnotations();
                synchronized (list) {
                    if (anno.isInDocument()) {
                        anno.setInDocument(false);
                        NbDocument.removeAnnotation(doc, anno);
                    }
                }
            }
        });
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void attachDetachAnnotations(StyledDocument doc, boolean closing) {
        List<? extends Annotation> list;
        Position annoPos = null;
        if (!closing) {
            try {
                annoPos = this.pos.getPosition();
                FindAnnotationPosition fap = new FindAnnotationPosition(doc, annoPos);
                doc.render(fap);
                annoPos = fap.getAnnotationPosition();
            }
            catch (IOException ex) {
                Logger.getLogger(DocumentLine.class.getName()).log(Level.WARNING, null, ex);
            }
        }
        List list2 = list = this.getAnnotations();
        synchronized (list2) {
            for (Annotation anno : list) {
                if (!closing) {
                    if (anno.isInDocument()) continue;
                    anno.setInDocument(true);
                    NbDocument.addAnnotation(doc, annoPos, -1, anno);
                    continue;
                }
                if (!anno.isInDocument()) continue;
                anno.setInDocument(false);
                NbDocument.removeAnnotation(doc, anno);
            }
        }
        list2 = this.lineParts;
        synchronized (list2) {
            for (Part p : this.lineParts) {
                p.attachDetachAnnotations(doc, closing);
            }
        }
    }

    @Override
    public String getText() {
        final StyledDocument doc = this.pos.getCloneableEditorSupport().getDocument();
        if (doc == null) {
            return null;
        }
        final String[] retStringArray = new String[1];
        doc.render(new Runnable(){

            @Override
            public void run() {
                int lineNumber = DocumentLine.this.getLineNumber();
                int lineStart = NbDocument.findLineOffset(doc, lineNumber);
                int lineEnd = lineNumber + 1 >= NbDocument.findLineRootElement(doc).getElementCount() ? doc.getLength() : NbDocument.findLineOffset(doc, lineNumber + 1);
                try {
                    retStringArray[0] = doc.getText(lineStart, lineEnd - lineStart);
                }
                catch (BadLocationException ex) {
                    Logger.getLogger(DocumentLine.class.getName()).log(Level.WARNING, null, ex);
                    retStringArray[0] = null;
                }
            }
        });
        return retStringArray[0];
    }

    void addLinePart(Part linePart) {
        this.lineParts.add(linePart);
    }

    void moveLinePart(Part linePart, DocumentLine newLine) {
        this.lineParts.remove(linePart);
        newLine.addLinePart(linePart);
        linePart.changeLine(newLine);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void notifyChange(DocumentEvent p0, Set set, StyledDocument doc) {
        Position p;
        List<Part> list = this.lineParts;
        synchronized (list) {
            int i = 0;
            while (i < this.lineParts.size()) {
                Part part = this.lineParts.get(i);
                part.handleDocumentChange(p0);
                if (NbDocument.findLineNumber(doc, part.getOffset()) != part.getLine().getLineNumber()) {
                    DocumentLine line = (DocumentLine)set.getCurrent(NbDocument.findLineNumber(doc, part.getOffset()));
                    this.moveLinePart(part, line);
                    continue;
                }
                ++i;
            }
        }
        try {
            p = this.pos.getPosition();
        }
        catch (IOException ex) {
            Logger.getLogger(DocumentLine.class.getName()).log(Level.WARNING, null, ex);
            p = null;
        }
        if (p != null) {
            List<? extends Annotation> annos;
            int lineStartOffset = NbDocument.findLineOffset(doc, NbDocument.findLineNumber(doc, p.getOffset()));
            CloneableEditorSupport support = this.pos.getCloneableEditorSupport();
            this.pos = new PositionRef(support.getPositionManager(), lineStartOffset, Position.Bias.Forward);
            List<? extends Annotation> list2 = annos = this.getAnnotations();
            synchronized (list2) {
                if (annos.size() > 0) {
                    try {
                        p = this.pos.getPosition();
                    }
                    catch (IOException e) {
                        throw new IllegalArgumentException();
                    }
                    for (Annotation anno : annos) {
                        if (anno.isInDocument()) {
                            anno.setInDocument(false);
                            NbDocument.removeAnnotation(support.getDocument(), anno);
                        }
                        if (anno.isInDocument()) continue;
                        anno.setInDocument(true);
                        NbDocument.addAnnotation(support.getDocument(), p, -1, anno);
                    }
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void notifyMove() {
        this.updatePositionRef();
        List<Part> list = this.lineParts;
        synchronized (list) {
            for (Part p : this.lineParts) {
                p.firePropertyChange("line", null, null);
            }
        }
    }

    private void updatePositionRef() {
    }

    void documentOpenedClosed(StyledDocument doc, boolean closed) {
        this.refreshState();
        this.attachDetachAnnotations(doc, closed);
    }

    public static abstract class Set
    extends Line.Set {
        final LineListener listener;
        private List<Line> list;

        public Set(StyledDocument doc) {
            this(doc, null);
        }

        Set(StyledDocument doc, CloneableEditorSupport support) {
            this.listener = new LineListener(doc, support);
        }

        void linesChanged(int startLineNumber, int endLineNumber, DocumentEvent p0) {
            List<Line> changedLines = this.getLinesFromRange(startLineNumber, endLineNumber);
            StyledDocument doc = this.listener.support.getDocument();
            for (Line line : changedLines) {
                line.firePropertyChange("text", null, null);
                if (doc == null || !(line instanceof DocumentLine)) continue;
                ((DocumentLine)line).notifyChange(p0, this, doc);
            }
        }

        void linesMoved(int startLineNumber, int endLineNumber) {
            List<Line> movedLines = this.getLinesFromRange(startLineNumber, endLineNumber);
            for (Line line : movedLines) {
                line.firePropertyChange("lineNumber", null, null);
                if (!(line instanceof DocumentLine)) continue;
                ((DocumentLine)line).notifyMove();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private List<Line> getLinesFromRange(int startLineNumber, int endLineNumber) {
            ArrayList<Line> linesInRange = new ArrayList<Line>(10);
            Map<Line, Reference<Line>> map = this.findWeakHashMap();
            synchronized (map) {
                for (Line line : this.findWeakHashMap().keySet()) {
                    int lineNumber = line.getLineNumber();
                    if (startLineNumber > lineNumber || lineNumber > endLineNumber) continue;
                    linesInRange.add(line);
                }
            }
            return linesInRange;
        }

        @Override
        public synchronized List<? extends Line> getLines() {
            if (this.list == null) {
                this.list = new LazyLines(this);
            }
            return this.list;
        }

        @Override
        public Line getOriginal(int line) throws IndexOutOfBoundsException {
            int newLine = this.listener.getLine(line);
            return this.safelyRegisterLine(this.createLine(this.offset(newLine)));
        }

        @Override
        public int getOriginalLineNumber(final Line line) {
            final int[] ret = new int[1];
            Document d = null;
            if (line instanceof DocumentLine) {
                CloneableEditorSupport ces;
                DocumentLine dl = (DocumentLine)line;
                if (dl.pos != null && (ces = dl.pos.getCloneableEditorSupport()) != null) {
                    d = ces.getDocument();
                }
            }
            Runnable r = new Runnable(){

                @Override
                public void run() {
                    Line find = Set.this.findLine(line);
                    ret[0] = find != null ? Set.this.listener.getOld(find.getLineNumber()) : -1;
                }
            };
            if (d != null) {
                d.render(r);
            } else {
                r.run();
            }
            return ret[0];
        }

        @Override
        public Line getCurrent(int line) throws IndexOutOfBoundsException {
            return this.safelyRegisterLine(this.createLine(this.offset(line)));
        }

        private int offset(int line) {
            StyledDocument doc = this.listener.support.getDocument();
            int offset = doc == null ? 0 : NbDocument.findLineOffset(doc, line);
            return offset;
        }

        protected abstract Line createLine(int var1);

        private Line safelyRegisterLine(Line line) {
            class DocumentRenderer
            implements Runnable {
                public Line result;
                final /* synthetic */ Line val$line;

                DocumentRenderer() {
                    this.val$line = var2_2;
                }

                @Override
                public void run() {
                    this.result = this$0.registerLine(this.val$line);
                }
            }
            assert (line != null);
            StyledDocument doc = this.listener.support.getDocument();
            DocumentRenderer renderer = new DocumentRenderer(this, line);
            if (doc != null) {
                doc.render(renderer);
            } else {
                renderer.run();
            }
            return renderer.result;
        }

    }

    private final class LR
    implements Runnable,
    DocumentListener {
        private static final int UNMARK = 1;
        private int actionId;
        final /* synthetic */ DocumentLine this$0;

        public LR(DocumentLine documentLine) {
            this.this$0 = documentLine;
        }

        public LR(DocumentLine documentLine, int actionId) {
            this.this$0 = documentLine;
            this.actionId = actionId;
        }

        @Override
        public void run() {
            switch (this.actionId) {
                case 1: {
                    this.this$0.unmarkError();
                }
            }
        }

        private void invoke(int op) {
            new LR(this.this$0, op).run();
        }

        @Override
        public void removeUpdate(DocumentEvent p0) {
            this.invoke(1);
        }

        @Override
        public void insertUpdate(DocumentEvent p0) {
            this.invoke(1);
        }

        @Override
        public void changedUpdate(DocumentEvent p0) {
        }
    }

    static class Part
    extends Line.Part {
        private PositionRef position;
        private Line line;
        private int length;
        private int previousOffset;

        public Part(Line line, PositionRef position, int length) {
            this.position = position;
            this.line = line;
            this.previousOffset = position.getOffset();
            this.length = this.limitLength(length);
        }

        private int limitLength(int suggestedLength) {
            StyledDocument d = this.position.getCloneableEditorSupport().getDocument();
            if (d == null) {
                return suggestedLength;
            }
            int end = this.position.getOffset() + suggestedLength;
            if (end > d.getLength()) {
                end = d.getLength();
            }
            if (end < this.position.getOffset()) {
                return 0;
            }
            try {
                String text = d.getText(this.position.getOffset(), end - this.position.getOffset());
                int newLine = text.indexOf(10);
                return newLine == -1 ? text.length() : newLine + 1;
            }
            catch (BadLocationException ex) {
                IllegalStateException i = new IllegalStateException(ex.getMessage());
                i.initCause(ex);
                throw i;
            }
        }

        @Override
        public int getColumn() {
            try {
                return this.position.getColumn();
            }
            catch (IOException ex) {
                return 0;
            }
        }

        @Override
        public int getLength() {
            this.length = this.limitLength(this.length);
            return this.length;
        }

        @Override
        public Line getLine() {
            return this.line;
        }

        int getOffset() {
            return this.position.getOffset();
        }

        void changeLine(Line line) {
            this.line = line;
            this.firePropertyChange("lineNumber", null, line);
        }

        @Override
        protected void addAnnotation(final Annotation anno) {
            super.addAnnotation(anno);
            final StyledDocument doc = this.position.getCloneableEditorSupport().getDocument();
            if (doc == null) {
                return;
            }
            this.position.getCloneableEditorSupport().prepareDocument().waitFinished();
            doc.render(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    try {
                        List<? extends Annotation> list = Part.this.getAnnotations();
                        synchronized (list) {
                            if (!anno.isInDocument()) {
                                anno.setInDocument(true);
                                NbDocument.addAnnotation(doc, Part.this.position.getPosition(), Part.this.length, anno);
                            }
                        }
                    }
                    catch (IOException ex) {
                        Logger.getLogger(DocumentLine.class.getName()).log(Level.WARNING, null, ex);
                    }
                }
            });
        }

        @Override
        protected void removeAnnotation(final Annotation anno) {
            super.removeAnnotation(anno);
            final StyledDocument doc = this.position.getCloneableEditorSupport().getDocument();
            if (doc == null) {
                return;
            }
            this.position.getCloneableEditorSupport().prepareDocument().waitFinished();
            doc.render(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    List<? extends Annotation> list = Part.this.getAnnotations();
                    synchronized (list) {
                        if (anno.isInDocument()) {
                            anno.setInDocument(false);
                            NbDocument.removeAnnotation(doc, anno);
                        }
                    }
                }
            });
        }

        @Override
        public String getText() {
            final StyledDocument doc = this.position.getCloneableEditorSupport().getDocument();
            if (doc == null) {
                return null;
            }
            final String[] retStringArray = new String[1];
            doc.render(new Runnable(){

                @Override
                public void run() {
                    try {
                        int p = Part.this.position.getOffset();
                        retStringArray[0] = p >= doc.getLength() ? "" : doc.getText(Part.this.position.getOffset(), Part.this.getLength());
                    }
                    catch (BadLocationException ex) {
                        Logger.getLogger(DocumentLine.class.getName()).log(Level.WARNING, null, ex);
                        retStringArray[0] = null;
                    }
                }
            });
            return retStringArray[0];
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void attachDetachAnnotations(StyledDocument doc, boolean closing) {
            List<? extends Annotation> list;
            List<? extends Annotation> list2 = list = this.getAnnotations();
            synchronized (list2) {
                for (Annotation anno : list) {
                    if (!closing) {
                        try {
                            if (anno.isInDocument()) continue;
                            anno.setInDocument(true);
                            NbDocument.addAnnotation(doc, this.position.getPosition(), this.getLength(), anno);
                        }
                        catch (IOException ex) {
                            Logger.getLogger(DocumentLine.class.getName()).log(Level.WARNING, null, ex);
                        }
                        continue;
                    }
                    if (!anno.isInDocument()) continue;
                    anno.setInDocument(false);
                    NbDocument.removeAnnotation(doc, anno);
                }
            }
        }

        void handleDocumentChange(DocumentEvent p0) {
            if (p0.getType().equals(DocumentEvent.EventType.INSERT) && p0.getOffset() >= this.previousOffset && p0.getOffset() < this.previousOffset + this.getLength()) {
                this.firePropertyChange("text", null, null);
            }
            if (p0.getType().equals(DocumentEvent.EventType.REMOVE) && (p0.getOffset() >= this.previousOffset && p0.getOffset() < this.previousOffset + this.getLength() || p0.getOffset() < this.previousOffset && p0.getOffset() + p0.getLength() > this.previousOffset)) {
                this.length = this.limitLength(this.length);
                this.firePropertyChange("text", null, null);
            }
            if ((p0.getType().equals(DocumentEvent.EventType.INSERT) || p0.getType().equals(DocumentEvent.EventType.REMOVE)) && p0.getOffset() < this.previousOffset) {
                this.firePropertyChange("column", null, null);
            }
            this.previousOffset = this.position.getOffset();
        }

    }

    private static final class FindAnnotationPosition
    implements Runnable {
        private StyledDocument doc;
        private Position annoPos;

        FindAnnotationPosition(StyledDocument doc, Position pos) {
            this.doc = doc;
            this.annoPos = pos;
        }

        @Override
        public void run() {
            int lineStartOffset;
            int offset = this.annoPos.getOffset();
            if (offset != (lineStartOffset = this.doc.getParagraphElement(offset).getStartOffset())) {
                try {
                    this.annoPos = this.doc.createPosition(lineStartOffset);
                }
                catch (BadLocationException e) {
                    throw new IllegalArgumentException();
                }
            }
        }

        Position getAnnotationPosition() {
            return this.annoPos;
        }
    }

}

