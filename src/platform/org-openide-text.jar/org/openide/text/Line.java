/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.Lookups
 */
package org.openide.text;

import java.io.Serializable;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Logger;
import org.openide.text.Annotatable;
import org.openide.text.DocumentLine;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

public abstract class Line
extends Annotatable
implements Serializable {
    private static final long serialVersionUID = 9113186289600795476L;
    static final Logger LOG = Logger.getLogger(Line.class.getName());
    public static final String PROP_LINE_NUMBER = "lineNumber";
    @Deprecated
    public static final int SHOW_TRY_SHOW = 0;
    @Deprecated
    public static final int SHOW_SHOW = 1;
    @Deprecated
    public static final int SHOW_GOTO = 2;
    @Deprecated
    public static final int SHOW_TOFRONT = 3;
    @Deprecated
    public static final int SHOW_REUSE = 4;
    @Deprecated
    public static final int SHOW_REUSE_NEW = 5;
    private static final Part nullPart = new NullPart();
    private Lookup dataObject;

    public Line(Lookup context) {
        if (context == null) {
            throw new NullPointerException();
        }
        this.dataObject = context;
    }

    public Line(Object source) {
        this(source instanceof Lookup ? (Lookup)source : Lookups.singleton((Object)source));
        if (source == null) {
            throw new NullPointerException();
        }
    }

    public String getDisplayName() {
        return this.getClass().getName() + ":" + this.getLineNumber();
    }

    public final Lookup getLookup() {
        return this.dataObject;
    }

    public abstract int getLineNumber();

    @Deprecated
    public abstract void show(int var1, int var2);

    @Deprecated
    public void show(int kind) {
        this.show(kind, 0);
    }

    public void show(ShowOpenType openType, ShowVisibilityType visibilityType, int column) {
        if (openType == ShowOpenType.NONE) {
            if (visibilityType == ShowVisibilityType.NONE) {
                this.show(0, column);
            } else {
                LOG.warning("Line.show(ShowOpenType, ShowVisibilityType, int) uses unsupported combination of parameters");
                this.show(0, column);
            }
        } else if (openType == ShowOpenType.OPEN) {
            if (visibilityType == ShowVisibilityType.NONE) {
                this.show(1, column);
            } else if (visibilityType == ShowVisibilityType.FOCUS) {
                this.show(2, column);
            } else if (visibilityType == ShowVisibilityType.FRONT) {
                this.show(3, column);
            }
        } else if (openType == ShowOpenType.REUSE) {
            if (visibilityType == ShowVisibilityType.FOCUS) {
                this.show(4, column);
            } else {
                LOG.warning("Line.show(ShowOpenType, ShowVisibilityType, int) uses unsupported combination of parameters");
                this.show(4, column);
            }
        } else if (openType == ShowOpenType.REUSE_NEW) {
            if (visibilityType == ShowVisibilityType.FOCUS) {
                this.show(5, column);
            } else {
                LOG.warning("Line.show(ShowOpenType, ShowVisibilityType, int) uses unsupported combination of parameters");
                this.show(5, column);
            }
        }
    }

    public void show(ShowOpenType openType, ShowVisibilityType visibilityType) {
        this.show(openType, visibilityType, 0);
    }

    @Deprecated
    public abstract void setBreakpoint(boolean var1);

    @Deprecated
    public abstract boolean isBreakpoint();

    @Deprecated
    public abstract void markError();

    @Deprecated
    public abstract void unmarkError();

    @Deprecated
    public abstract void markCurrentLine();

    @Deprecated
    public abstract void unmarkCurrentLine();

    @Deprecated
    public boolean canBeMarkedCurrent(int action, Line previousLine) {
        return true;
    }

    public Part createPart(int column, int length) {
        return nullPart;
    }

    @Override
    public String getText() {
        return null;
    }

    public static abstract class Set {
        private Date date = new Date();
        private Map<Line, Reference<Line>> whm;

        public abstract List<? extends Line> getLines();

        public final Date getDate() {
            return this.date;
        }

        public abstract Line getOriginal(int var1) throws IndexOutOfBoundsException;

        public abstract Line getCurrent(int var1) throws IndexOutOfBoundsException;

        public int getOriginalLineNumber(Line line) {
            return Set.computeOriginal(this, line);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        Map<Line, Reference<Line>> findWeakHashMap() {
            Date date = this.date;
            synchronized (date) {
                if (this.whm != null) {
                    return this.whm;
                }
                this.whm = new WeakHashMap<Line, Reference<Line>>();
                return this.whm;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        final Line registerLine(Line line) {
            Map<Line, Reference<Line>> lines;
            if (line == null) {
                throw new NullPointerException();
            }
            Map<Line, Reference<Line>> map = lines = this.findWeakHashMap();
            synchronized (map) {
                Line in;
                Reference<Line> r = lines.get(line);
                Line line2 = in = r != null ? r.get() : null;
                if (in == null) {
                    if (line instanceof DocumentLine) {
                        ((DocumentLine)line).init();
                    }
                    lines.put(line, new WeakReference<Line>(line));
                    in = line;
                }
                return in;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        final Line findLine(Line line) {
            Map<Line, Reference<Line>> lines;
            Map<Line, Reference<Line>> map = lines = this.findWeakHashMap();
            synchronized (map) {
                Reference<Line> r = lines.get(line);
                Line in = r != null ? r.get() : null;
                return in;
            }
        }

        static int computeOriginal(Set set, Line line) {
            int n = line.getLineNumber();
            Line current = null;
            try {
                current = set.getOriginal(n);
                if (line.equals(current)) {
                    return n;
                }
            }
            catch (IndexOutOfBoundsException ex) {
                // empty catch block
            }
            if (current == null) {
                return Set.binarySearch(set, n, 0, Set.findMaxLine(set));
            }
            if (n < current.getLineNumber()) {
                return Set.binarySearch(set, n, 0, current.getLineNumber());
            }
            return Set.binarySearch(set, n, current.getLineNumber(), Set.findMaxLine(set));
        }

        private static int binarySearch(Set set, int number, int from, int to) {
            while (from < to) {
                int middle = (from + to) / 2;
                Line l = set.getOriginal(middle);
                int ln = l.getLineNumber();
                if (ln == number) {
                    return middle;
                }
                if (ln < number) {
                    from = middle + 1;
                    continue;
                }
                to = middle - 1;
            }
            return from;
        }

        private static int findMaxLine(Set set) {
            int from = 0;
            int to = 32000;
            try {
                do {
                    set.getOriginal(to);
                    from = to;
                    to *= 2;
                } while (true);
            }
            catch (IndexOutOfBoundsException ex) {
                while (from < to) {
                    int middle = (from + to + 1) / 2;
                    try {
                        set.getOriginal(middle);
                        from = middle;
                    }
                    catch (IndexOutOfBoundsException ex) {
                        to = middle - 1;
                    }
                }
                return from;
            }
        }
    }

    private static final class NullPart
    extends Part {
        NullPart() {
        }

        @Override
        public int getColumn() {
            return 0;
        }

        @Override
        public int getLength() {
            return 0;
        }

        @Override
        public Line getLine() {
            return null;
        }

        @Override
        public String getText() {
            return null;
        }
    }

    public static abstract class Part
    extends Annotatable {
        public static final String PROP_LINE = "line";
        public static final String PROP_COLUMN = "column";
        public static final String PROP_LENGTH = "length";

        public abstract int getColumn();

        public abstract int getLength();

        public abstract Line getLine();
    }

    public static enum ShowVisibilityType {
        NONE,
        FRONT,
        FOCUS;
        

        private ShowVisibilityType() {
        }
    }

    public static enum ShowOpenType {
        NONE,
        OPEN,
        REUSE,
        REUSE_NEW;
        

        private ShowOpenType() {
        }
    }

}

