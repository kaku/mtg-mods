/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 */
package org.openide.text;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextLayout;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterAbortException;
import java.awt.print.PrinterException;
import java.awt.print.PrinterGraphics;
import java.awt.print.PrinterJob;
import java.text.AttributedCharacterIterator;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.accessibility.AccessibleContext;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.text.AttributedCharacters;
import org.openide.text.NbDocument;
import org.openide.text.PrintPreferences;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

final class DefaultPrintable
implements Printable {
    private static final int ARG_SIZE = 3;
    private static Font fontInstance;
    private AttributedCharacterIterator[] styledTexts;
    private int startPage = -1;
    private List<TextLayout> textLayouts;
    private int[] pageIndices;
    private int pageIndicesSize;
    private int currentLayout;
    private int currentStyledText;
    private LineBreakMeasurer lineBreakMeasurer;
    private int maxPage;
    private List<TextLayout> startLayouts;
    private int[] lineIndices;
    private Object[] pageArgs;
    private MessageFormat header;
    private boolean printHeader;
    private MessageFormat footer;
    private boolean printFooter;
    private CancellationPanel cancellationPanel;
    private Dialog cancellationDialog;

    private DefaultPrintable(AttributedCharacterIterator[] iter, String filename) {
        if (iter == null || iter.length == 0) {
            throw new IllegalArgumentException();
        }
        DefaultPrintable.replaceEmptyIterators(iter);
        this.styledTexts = iter;
        this.textLayouts = new ArrayList<TextLayout>(100);
        this.pageIndices = new int[50];
        this.pageIndicesSize = 0;
        this.currentLayout = 0;
        this.currentStyledText = 0;
        this.lineBreakMeasurer = null;
        this.maxPage = Integer.MAX_VALUE;
        this.startLayouts = new ArrayList<TextLayout>(10);
        this.lineIndices = new int[this.pageIndices.length];
        this.pageArgs = new Object[3];
        this.pageArgs[2] = filename;
        this.pageArgs[1] = new Date(System.currentTimeMillis());
        this.header = new MessageFormat(DefaultPrintable.getHeaderFormat());
        this.printHeader = !DefaultPrintable.getHeaderFormat().equals("");
        this.footer = new MessageFormat(DefaultPrintable.getFooterFormat());
        this.printFooter = !DefaultPrintable.getFooterFormat().equals("");
    }

    public DefaultPrintable(Document doc) {
        this(DefaultPrintable.getIterators(doc), DefaultPrintable.getFilename(doc));
    }

    @Override
    public int print(Graphics g, PageFormat pf, int pageNo) throws PrinterException {
        boolean processDummy = false;
        if (this.startPage == -1) {
            processDummy = true;
            this.startPage = pageNo;
        }
        if (processDummy) {
            for (int i = 0; i < this.startPage; ++i) {
                this.printImpl(g, pf, i, false);
            }
        }
        return this.printImpl(g, pf, pageNo, true);
    }

    private int printImpl(Graphics g, PageFormat pf, int pageNo, boolean print) throws PrinterException {
        float dx;
        if (pageNo > this.maxPage) {
            this.closeDialog();
            return 1;
        }
        if (pageNo < 0) {
            this.closeDialog();
            throw new IllegalArgumentException("Illegal page number=" + pageNo);
        }
        if (g instanceof PrinterGraphics && this.isCancelled(((PrinterGraphics)((Object)g)).getPrinterJob())) {
            this.closeDialog();
            throw new PrinterAbortException();
        }
        if (this.cancellationPanel == null && g instanceof PrinterGraphics) {
            PrinterJob pJob = ((PrinterGraphics)((Object)g)).getPrinterJob();
            this.createCancellationPanel(pJob);
        }
        if (this.cancellationPanel != null) {
            int pageNumber = print ? pageNo : this.startPage;
            this.cancellationPanel.setPageno(pageNumber);
            this.packDialog();
        }
        boolean startLine = false;
        int correction = 3;
        g.setColor(Color.black);
        Graphics2D graphics = (Graphics2D)g;
        Point2D.Float pen = new Point2D.Float(this.getImageableXPatch(pf), this.getImageableYPatch(pf));
        this.pageArgs[0] = new Integer(pageNo + 1);
        float pageBreakCorrection = 0.0f;
        TextLayout headerString = null;
        TextLayout footerString = null;
        if (this.printHeader) {
            headerString = new TextLayout(this.header.format(this.pageArgs), DefaultPrintable.getHeaderFont(), graphics.getFontRenderContext());
            pageBreakCorrection += headerString.getAscent() + (headerString.getDescent() + headerString.getLeading()) * 2.0f;
        }
        if (this.printFooter) {
            footerString = new TextLayout(this.footer.format(this.pageArgs), DefaultPrintable.getFooterFont(), graphics.getFontRenderContext());
            pageBreakCorrection += footerString.getAscent() * 2.0f + footerString.getDescent() + footerString.getLeading();
        }
        float wrappingWidth = DefaultPrintable.wrap() ? (float)pf.getImageableWidth() - (float)correction : Float.MAX_VALUE;
        float pageBreak = (float)pf.getImageableHeight() + (float)pf.getImageableY() - pageBreakCorrection;
        FontRenderContext frCtx = graphics.getFontRenderContext();
        boolean pageExists = false;
        TextLayout layout = this.layoutForPage(pageNo, wrappingWidth, frCtx);
        while (pen.y < pageBreak) {
            if (layout == null) {
                this.maxPage = pageNo;
                break;
            }
            if (!pageExists) {
                if (this.printHeader && headerString != null) {
                    float dx2;
                    pen.y += headerString.getAscent();
                    float center = DefaultPrintable.computeStart(headerString.getBounds(), (float)pf.getImageableWidth(), DefaultPrintable.getHeaderAlignment());
                    float f = dx2 = headerString.isLeftToRight() ? center : wrappingWidth - headerString.getAdvance() - center;
                    if (print) {
                        headerString.draw(graphics, pen.x + dx2, pen.y);
                    }
                    pen.y += (headerString.getDescent() + headerString.getLeading()) * 2.0f;
                }
                pageExists = true;
            }
            pen.y += layout.getAscent() * DefaultPrintable.getLineAscentCorrection();
            float f = dx = layout.isLeftToRight() ? 0.0f : wrappingWidth - layout.getAdvance();
            if (print) {
                layout.draw(graphics, (float)correction + pen.x + dx, pen.y);
            }
            pen.y += (layout.getDescent() + layout.getLeading()) * DefaultPrintable.getLineAscentCorrection();
            layout = this.nextLayout(wrappingWidth, frCtx);
        }
        if (this.printFooter && pageExists && footerString != null) {
            pen.y = pageBreak;
            pen.y += footerString.getAscent() * 2.0f;
            float center = DefaultPrintable.computeStart(footerString.getBounds(), (float)pf.getImageableWidth(), DefaultPrintable.getFooterAlignment());
            float f = dx = footerString.isLeftToRight() ? 0.0f + center : wrappingWidth - footerString.getAdvance() - center;
            if (print) {
                footerString.draw(graphics, pen.x + dx, pen.y);
            }
        }
        if (g instanceof PrinterGraphics && this.isCancelled(((PrinterGraphics)((Object)g)).getPrinterJob())) {
            this.closeDialog();
            throw new PrinterAbortException();
        }
        if (!pageExists) {
            this.closeDialog();
            return 1;
        }
        return 0;
    }

    private float getImageableXPatch(PageFormat pf) {
        if (pf.getOrientation() == 0) {
            double ret = pf.getPaper().getHeight() - (pf.getImageableX() + pf.getImageableWidth());
            return Math.round(ret);
        }
        return (float)pf.getImageableX();
    }

    private float getImageableYPatch(PageFormat pf) {
        if (pf.getOrientation() == 0) {
            double ret = pf.getPaper().getWidth() - (pf.getImageableY() + pf.getImageableHeight());
            return Math.round(ret);
        }
        return (float)pf.getImageableY();
    }

    private boolean isNewline(TextLayout tl, int currentLine) {
        if (currentLine >= this.startLayouts.size()) {
            return false;
        }
        return this.startLayouts.get(currentLine) == tl;
    }

    private static float computeStart(Rectangle2D rect, float width, PrintPreferences.Alignment alignment) {
        float x = rect instanceof Rectangle2D.Float ? ((Rectangle2D.Float)rect).width : (float)((Rectangle2D.Double)rect).width;
        if (x >= width) {
            return 0.0f;
        }
        if (alignment == PrintPreferences.Alignment.LEFT) {
            return 0.0f;
        }
        if (alignment == PrintPreferences.Alignment.RIGHT) {
            return width - x;
        }
        return (width - x) / 2.0f;
    }

    private TextLayout nextLayout(float wrappingWidth, FontRenderContext frc) {
        TextLayout l;
        if (this.currentLayout == this.textLayouts.size()) {
            LineBreakMeasurer old = this.lineBreakMeasurer;
            LineBreakMeasurer measurer = this.getMeasurer(frc);
            if (measurer == null) {
                return null;
            }
            l = measurer.nextLayout(wrappingWidth);
            this.textLayouts.add(l);
            if (old != measurer) {
                this.startLayouts.add(l);
            }
        } else {
            l = this.textLayouts.get(this.currentLayout);
        }
        ++this.currentLayout;
        return l;
    }

    private TextLayout layoutForPage(int pageNo, float wrappingWidth, FontRenderContext frc) {
        if (pageNo > this.pageIndicesSize + 1) {
            throw new IllegalArgumentException("Page number " + pageNo + " is bigger than array size " + (this.pageIndicesSize + 1));
        }
        if (pageNo == this.pageIndicesSize) {
            if (this.pageIndicesSize >= this.pageIndices.length) {
                this.pageIndices = DefaultPrintable.increaseArray(this.pageIndices);
                this.lineIndices = DefaultPrintable.increaseArray(this.lineIndices);
            }
            this.pageIndices[this.pageIndicesSize] = Math.max(this.textLayouts.size() - 1, 0);
            this.lineIndices[this.pageIndicesSize++] = Math.max(this.startLayouts.size() - 1, 0);
        }
        this.currentLayout = this.pageIndices[pageNo];
        return this.nextLayout(wrappingWidth, frc);
    }

    private LineBreakMeasurer getMeasurer(FontRenderContext frc) {
        if (this.lineBreakMeasurer == null) {
            this.lineBreakMeasurer = new LineBreakMeasurer(this.styledTexts[this.currentStyledText], frc);
        } else if (this.lineBreakMeasurer.getPosition() >= this.styledTexts[this.currentStyledText].getEndIndex()) {
            if (this.currentStyledText == this.styledTexts.length - 1) {
                return null;
            }
            this.lineBreakMeasurer = new LineBreakMeasurer(this.styledTexts[++this.currentStyledText], frc);
        }
        return this.lineBreakMeasurer;
    }

    private static boolean wrap() {
        return PrintPreferences.getWrap();
    }

    private static String getHeaderFormat() {
        return PrintPreferences.getHeaderFormat();
    }

    private static String getFooterFormat() {
        return PrintPreferences.getFooterFormat();
    }

    private static Font getHeaderFont() {
        return PrintPreferences.getHeaderFont();
    }

    private static Font getFooterFont() {
        return PrintPreferences.getFooterFont();
    }

    private static PrintPreferences.Alignment getFooterAlignment() {
        return PrintPreferences.getFooterAlignment();
    }

    private static PrintPreferences.Alignment getHeaderAlignment() {
        return PrintPreferences.getHeaderAlignment();
    }

    private static float getLineAscentCorrection() {
        return PrintPreferences.getLineAscentCorrection();
    }

    private static boolean lineNumbers() {
        return false;
    }

    private static Font lineNumbersFont() {
        return new Font("Courier", 0, 6);
    }

    private static AttributedCharacterIterator[] getIterators(Document doc) {
        if (doc instanceof NbDocument.Printable) {
            return ((NbDocument.Printable)doc).createPrintIterators();
        }
        Font f = new Font("Courier", 0, 8);
        AttributedCharacters achs = null;
        char[] chars = null;
        ArrayList<AttributedCharacterIterator> iterators = new ArrayList<AttributedCharacterIterator>(300);
        try {
            String document = doc.getText(0, doc.getLength());
            int firstCharInDoc = 0;
            for (int i = 0; i < document.length(); ++i) {
                if (document.charAt(i) != '\n') continue;
                chars = new char[i - firstCharInDoc + 1];
                document.getChars(firstCharInDoc, chars.length + firstCharInDoc, chars, 0);
                achs = new AttributedCharacters();
                achs.append(chars, f, Color.black);
                iterators.add(achs.iterator());
                firstCharInDoc = i + 1;
            }
        }
        catch (BadLocationException e) {
            Exceptions.printStackTrace((Throwable)e);
        }
        AttributedCharacterIterator[] iters = new AttributedCharacterIterator[iterators.size()];
        iterators.toArray(iters);
        return iters;
    }

    private static String getFilename(Document doc) {
        String ret = (String)doc.getProperty("title");
        return ret == null ? "UNKNOWN" : ret;
    }

    private static int[] increaseArray(int[] old) {
        int[] ret = new int[2 * old.length];
        System.arraycopy(old, 0, ret, 0, old.length);
        return ret;
    }

    private void createCancellationPanel(final PrinterJob job) {
        this.cancellationPanel = new CancellationPanel(job);
        DialogDescriptor ddesc = new DialogDescriptor((Object)this.cancellationPanel, NbBundle.getMessage(PrintPreferences.class, (String)"CTL_Print_cancellation"), false, new Object[]{NbBundle.getMessage(PrintPreferences.class, (String)"CTL_Cancel")}, (Object)NbBundle.getMessage(PrintPreferences.class, (String)"CTL_Cancel"), 0, null, new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent ev) {
                DefaultPrintable.this.setCancelled(job);
                DefaultPrintable.this.closeDialog();
            }
        });
        this.setDialog(DialogDisplayer.getDefault().createDialog(ddesc));
    }

    void closeDialog() {
        if (this.cancellationDialog != null) {
            this.cancellationDialog.setVisible(false);
            this.cancellationDialog.dispose();
        }
    }

    void setDialog(Dialog d) {
        d.setVisible(true);
        d.pack();
        this.cancellationDialog = d;
    }

    void packDialog() {
        if (this.cancellationDialog != null) {
            this.cancellationDialog.pack();
        }
    }

    void setCancelled(PrinterJob job) {
        job.cancel();
    }

    boolean isCancelled(PrinterJob job) {
        return job.isCancelled();
    }

    private static void replaceEmptyIterators(AttributedCharacterIterator[] iters) {
        for (int i = 0; i < iters.length; ++i) {
            AttributedCharacterIterator achit = iters[i];
            if (achit.getBeginIndex() != achit.getEndIndex()) continue;
            AttributedCharacters at = new AttributedCharacters();
            at.append(' ', DefaultPrintable.getFontInstance(), Color.white);
            iters[i] = at.iterator();
        }
    }

    static Font getFontInstance() {
        if (fontInstance == null) {
            fontInstance = new Font("Dialog", 0, 14);
        }
        return fontInstance;
    }

    static final class CancellationPanel
    extends JPanel {
        static final long serialVersionUID = -6419253408585188541L;
        private final JLabel printProgress;
        private final MessageFormat format;
        private final Object[] msgParams;

        public CancellationPanel(PrinterJob job) {
            if (job == null) {
                throw new IllegalArgumentException();
            }
            this.format = new MessageFormat(NbBundle.getMessage(PrintPreferences.class, (String)"CTL_Print_progress"));
            this.msgParams = new Object[1];
            this.setLayout(new BorderLayout());
            this.setBorder(new EmptyBorder(12, 12, 0, 12));
            this.printProgress = new JLabel();
            this.printProgress.setHorizontalAlignment(0);
            this.add(this.printProgress);
        }

        public void setPageno(int pageno) {
            this.msgParams[0] = new Integer(pageno + 1);
            this.printProgress.setText(this.format.format(this.msgParams));
            this.getAccessibleContext().setAccessibleDescription(this.printProgress.getText());
        }
    }

}

