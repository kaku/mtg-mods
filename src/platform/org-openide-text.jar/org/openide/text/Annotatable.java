/*
 * Decompiled with CFR 0_118.
 */
package org.openide.text;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.openide.text.Annotation;

public abstract class Annotatable {
    public static final String PROP_ANNOTATION_COUNT = "annotationCount";
    public static final String PROP_DELETED = "deleted";
    public static final String PROP_TEXT = "text";
    private final PropertyChangeSupport propertyChangeSupport;
    private final List<Annotation> attachedAnnotations;
    private boolean deleted = false;

    public Annotatable() {
        this.propertyChangeSupport = new PropertyChangeSupport(this);
        this.attachedAnnotations = Collections.synchronizedList(new LinkedList());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void addAnnotation(Annotation anno) {
        int count;
        List<Annotation> list = this.attachedAnnotations;
        synchronized (list) {
            this.attachedAnnotations.add(anno);
            count = this.attachedAnnotations.size();
        }
        this.propertyChangeSupport.firePropertyChange("annotationCount", count - 1, count);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void removeAnnotation(Annotation anno) {
        int count;
        List<Annotation> list = this.attachedAnnotations;
        synchronized (list) {
            this.attachedAnnotations.remove(anno);
            count = this.attachedAnnotations.size();
        }
        this.propertyChangeSupport.firePropertyChange("annotationCount", count + 1, count);
    }

    List<? extends Annotation> getAnnotations() {
        return this.attachedAnnotations;
    }

    public final void addPropertyChangeListener(PropertyChangeListener l) {
        this.propertyChangeSupport.addPropertyChangeListener(l);
    }

    public final void removePropertyChangeListener(PropertyChangeListener l) {
        this.propertyChangeSupport.removePropertyChangeListener(l);
    }

    protected final void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        this.propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
    }

    public final boolean isDeleted() {
        return this.deleted;
    }

    public abstract String getText();

    void setDeleted(boolean deleted) {
        if (this.deleted != deleted) {
            this.deleted = deleted;
            this.propertyChangeSupport.firePropertyChange("deleted", !deleted, deleted);
        }
    }

    public final int getAnnotationCount() {
        return this.attachedAnnotations.size();
    }
}

