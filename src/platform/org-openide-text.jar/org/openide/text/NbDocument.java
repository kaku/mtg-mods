/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.actions.Openable
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.awt.UndoRedo
 *  org.openide.awt.UndoRedo$Manager
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.Mutex
 *  org.openide.util.UserQuestionException
 */
package org.openide.text;

import java.awt.Color;
import java.awt.Component;
import java.awt.print.Pageable;
import java.io.IOException;
import java.text.AttributedCharacterIterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.undo.UndoableEdit;
import org.netbeans.api.actions.Openable;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.UndoRedo;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.LineCookie;
import org.openide.text.Annotation;
import org.openide.text.BackwardPosition;
import org.openide.text.Bundle;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.DefaultPrintable;
import org.openide.text.Line;
import org.openide.text.UndoRedoManager;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.UserQuestionException;

public final class NbDocument {
    public static final Object GUARDED = new AttributeSet.CharacterAttribute(){};
    private static final SimpleAttributeSet ATTR_ADD = new SimpleAttributeSet();
    private static final SimpleAttributeSet ATTR_REMOVE = new SimpleAttributeSet();
    @Deprecated
    public static final String BREAKPOINT_STYLE_NAME = "NbBreakpointStyle";
    @Deprecated
    public static final String ERROR_STYLE_NAME = "NbErrorStyle";
    @Deprecated
    public static final String CURRENT_STYLE_NAME = "NbCurrentStyle";
    @Deprecated
    public static final String NORMAL_STYLE_NAME = "NbNormalStyle";

    private NbDocument() {
    }

    public static Element findLineRootElement(StyledDocument doc) {
        NbDocument.checkDocParameter(doc);
        Element e = doc.getParagraphElement(0).getParentElement();
        if (e == null) {
            e = doc.getDefaultRootElement();
        }
        return e;
    }

    public static int findLineNumber(StyledDocument doc, int offset) {
        return new DocumentRenderer(0, doc, offset).renderToInt();
    }

    public static int findLineColumn(StyledDocument doc, int offset) {
        return new DocumentRenderer(1, doc, offset).renderToInt();
    }

    public static int findLineOffset(StyledDocument doc, int lineNumber) {
        return new DocumentRenderer(2, doc, lineNumber).renderToInt();
    }

    public static Position createPosition(Document doc, int offset, Position.Bias bias) throws BadLocationException {
        NbDocument.checkDocParameter(doc);
        if (doc instanceof PositionBiasable) {
            return ((PositionBiasable)doc).createPosition(offset, bias);
        }
        if (bias == Position.Bias.Forward) {
            return doc.createPosition(offset);
        }
        return BackwardPosition.create(doc, offset);
    }

    public static void markGuarded(StyledDocument doc, int offset, int len) {
        NbDocument.checkDocParameter(doc);
        doc.setCharacterAttributes(offset, len, ATTR_ADD, false);
    }

    public static void unmarkGuarded(StyledDocument doc, int offset, int len) {
        NbDocument.checkDocParameter(doc);
        doc.setCharacterAttributes(offset, len, ATTR_REMOVE, false);
    }

    public static void insertGuarded(StyledDocument doc, int offset, String txt) throws BadLocationException {
        NbDocument.checkDocParameter(doc);
        doc.insertString(offset, txt, ATTR_ADD);
    }

    @Deprecated
    public static void markBreakpoint(StyledDocument doc, int offset) {
        NbDocument.checkDocParameter(doc);
        Style bp = doc.getStyle("NbBreakpointStyle");
        if (bp == null) {
            bp = doc.addStyle("NbBreakpointStyle", null);
            if (bp == null) {
                return;
            }
            bp.addAttribute(StyleConstants.ColorConstants.Background, Color.red);
            bp.addAttribute(StyleConstants.ColorConstants.Foreground, Color.white);
        }
        doc.setLogicalStyle(offset, bp);
    }

    @Deprecated
    public static void markError(StyledDocument doc, int offset) {
        NbDocument.checkDocParameter(doc);
        Style bp = doc.getStyle("NbErrorStyle");
        if (bp == null) {
            bp = doc.addStyle("NbErrorStyle", null);
            if (bp == null) {
                return;
            }
            bp.addAttribute(StyleConstants.ColorConstants.Background, Color.green);
            bp.addAttribute(StyleConstants.ColorConstants.Foreground, Color.black);
        }
        doc.setLogicalStyle(offset, bp);
    }

    @Deprecated
    public static void markCurrent(StyledDocument doc, int offset) {
        NbDocument.checkDocParameter(doc);
        Style bp = doc.getStyle("NbCurrentStyle");
        if (bp == null) {
            bp = doc.addStyle("NbCurrentStyle", null);
            if (bp == null) {
                return;
            }
            bp.addAttribute(StyleConstants.ColorConstants.Background, Color.blue);
            bp.addAttribute(StyleConstants.ColorConstants.Foreground, Color.white);
        }
        doc.setLogicalStyle(offset, bp);
    }

    @Deprecated
    public static void markNormal(StyledDocument doc, int offset) {
        NbDocument.checkDocParameter(doc);
        Style st = doc.getStyle("NbNormalStyle");
        if (st == null) {
            st = doc.addStyle("NbNormalStyle", null);
        }
        if (st != null) {
            doc.setLogicalStyle(offset, st);
        }
    }

    public static JEditorPane findRecentEditorPane(EditorCookie ec) {
        assert (SwingUtilities.isEventDispatchThread());
        if (ec instanceof CloneableEditorSupport) {
            JEditorPane pane = ((CloneableEditorSupport)((Object)ec)).getRecentPane();
            return pane;
        }
        JEditorPane[] panes = ec.getOpenedPanes();
        if (panes != null) {
            return panes[0];
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void runAtomic(StyledDocument doc, Runnable run) {
        NbDocument.checkDocParameter(doc);
        if (doc instanceof WriteLockable) {
            ((WriteLockable)((Object)doc)).runAtomic(run);
        } else {
            StyledDocument styledDocument = doc;
            synchronized (styledDocument) {
                run.run();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void runAtomicAsUser(StyledDocument doc, Runnable run) throws BadLocationException {
        NbDocument.checkDocParameter(doc);
        if (doc instanceof WriteLockable) {
            ((WriteLockable)((Object)doc)).runAtomicAsUser(run);
        } else {
            StyledDocument styledDocument = doc;
            synchronized (styledDocument) {
                run.run();
            }
        }
    }

    private static void checkDocParameter(Document doc) {
        if (doc == null) {
            throw new NullPointerException("Invalid doc parameter. Document may not be null!");
        }
    }

    public static Object findPageable(StyledDocument doc) {
        if (doc instanceof Pageable) {
            return doc;
        }
        if (doc instanceof java.awt.print.Printable) {
            return doc;
        }
        return new DefaultPrintable(doc);
    }

    public static void addAnnotation(StyledDocument doc, Position startPos, int length, Annotation annotation) {
        if (!(doc instanceof Annotatable)) {
            return;
        }
        ((Annotatable)((Object)doc)).addAnnotation(startPos, length, annotation);
    }

    public static void removeAnnotation(StyledDocument doc, Annotation annotation) {
        if (!(doc instanceof Annotatable)) {
            return;
        }
        ((Annotatable)((Object)doc)).removeAnnotation(annotation);
    }

    public static <T extends UndoableEdit> T getEditToBeUndoneOfType(EditorCookie ec, Class<T> type) {
        return NbDocument.getEditToBeUndoneRedoneOfType(ec, type, false);
    }

    public static <T extends UndoableEdit> T getEditToBeRedoneOfType(EditorCookie ec, Class<T> type) {
        return NbDocument.getEditToBeUndoneRedoneOfType(ec, type, true);
    }

    private static <T extends UndoableEdit> T getEditToBeUndoneRedoneOfType(EditorCookie ec, Class<T> type, boolean redone) {
        UndoRedo.Manager ur;
        if (ec instanceof CloneableEditorSupport && (ur = ((CloneableEditorSupport)((Object)ec)).getUndoRedo()) instanceof UndoRedoManager) {
            UndoRedoManager urManager = (UndoRedoManager)ur;
            UndoableEdit edit = urManager.editToBeUndoneRedone(redone);
            if (type.isInstance(edit)) {
                return (T)((UndoableEdit)type.cast(edit));
            }
            if (edit instanceof List) {
                List listEdit = (List)((Object)edit);
                for (int i = listEdit.size() - 1; i >= 0; --i) {
                    edit = (UndoableEdit)listEdit.get(i);
                    if (!type.isInstance(edit)) continue;
                    UndoableEdit inst = edit;
                    return (T)inst;
                }
            }
        }
        return null;
    }

    public static StyledDocument getDocument(Lookup.Provider provider) {
        block5 : {
            try {
                EditorCookie ec = (EditorCookie)provider.getLookup().lookup(EditorCookie.class);
                if (ec == null) break block5;
                StyledDocument doc = null;
                try {
                    doc = ec.openDocument();
                }
                catch (UserQuestionException uqe) {
                    Object value = DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Confirmation((Object)uqe.getLocalizedMessage(), Bundle.TXT_Question(), 0));
                    if (value != NotifyDescriptor.YES_OPTION) {
                        return null;
                    }
                    uqe.confirmed();
                    doc = ec.openDocument();
                }
                return doc;
            }
            catch (IOException ioe) {
                Logger.getLogger(NbDocument.class.getName()).log(Level.WARNING, null, ioe);
            }
        }
        return null;
    }

    public static boolean openDocument(Lookup.Provider provider, int offset, Line.ShowOpenType openType, Line.ShowVisibilityType visibilityType) {
        Openable oc;
        StyledDocument doc;
        assert (provider != null);
        LineCookie lc = (LineCookie)provider.getLookup().lookup(LineCookie.class);
        if (lc != null && offset != -1 && (doc = NbDocument.getDocument(provider)) != null) {
            int line = NbDocument.findLineNumber(doc, offset);
            int column = NbDocument.findLineColumn(doc, offset);
            Line l = null;
            try {
                l = lc.getLineSet().getCurrent(line);
            }
            catch (IndexOutOfBoundsException e) {
                l = lc.getLineSet().getCurrent(0);
            }
            if (l != null) {
                NbDocument.doShow(l, column, openType, visibilityType);
                return true;
            }
        }
        if ((oc = (Openable)provider.getLookup().lookup(Openable.class)) != null) {
            NbDocument.doOpen(oc);
            return true;
        }
        return false;
    }

    public static boolean openDocument(Lookup.Provider provider, int line, int column, Line.ShowOpenType openType, Line.ShowVisibilityType visibilityType) {
        Openable oc;
        StyledDocument doc;
        assert (provider != null);
        LineCookie lc = (LineCookie)provider.getLookup().lookup(LineCookie.class);
        if (lc != null && line >= 0 && column >= -1 && (doc = NbDocument.getDocument(provider)) != null) {
            Line l = null;
            try {
                l = lc.getLineSet().getCurrent(line);
            }
            catch (IndexOutOfBoundsException e) {
                l = lc.getLineSet().getCurrent(0);
            }
            if (l != null) {
                NbDocument.doShow(l, column, openType, visibilityType);
                return true;
            }
        }
        if ((oc = (Openable)provider.getLookup().lookup(Openable.class)) != null) {
            NbDocument.doOpen(oc);
            return true;
        }
        return false;
    }

    private static void doShow(final Line l, final int column, final Line.ShowOpenType openType, final Line.ShowVisibilityType visibilityType) {
        Mutex.EVENT.writeAccess(new Runnable(){

            @Override
            public void run() {
                l.show(openType, visibilityType, column);
            }
        });
    }

    private static void doOpen(final Openable oc) {
        Mutex.EVENT.writeAccess(new Runnable(){

            @Override
            public void run() {
                oc.open();
            }
        });
    }

    static {
        ATTR_ADD.addAttribute(GUARDED, Boolean.TRUE);
        ATTR_REMOVE.addAttribute(GUARDED, Boolean.FALSE);
    }

    private static final class DocumentRenderer
    implements Runnable {
        private static final int FIND_LINE_NUMBER = 0;
        private static final int FIND_LINE_COLUMN = 1;
        private static final int FIND_LINE_OFFSET = 2;
        private StyledDocument doc;
        private int opCode;
        private int argInt;
        private int retInt;

        DocumentRenderer(int opCode, StyledDocument doc, int argInt) {
            this.opCode = opCode;
            this.doc = doc;
            this.argInt = argInt;
        }

        int renderToInt() {
            if (this.doc != null) {
                this.doc.render(this);
            }
            return this.retInt;
        }

        @Override
        public void run() {
            switch (this.opCode) {
                case 0: {
                    Element paragraphsParent = NbDocument.findLineRootElement(this.doc);
                    this.retInt = paragraphsParent.getElementIndex(this.argInt);
                    break;
                }
                case 1: {
                    Element paragraphsParent = NbDocument.findLineRootElement(this.doc);
                    int indx = paragraphsParent.getElementIndex(this.argInt);
                    this.retInt = this.argInt - paragraphsParent.getElement(indx).getStartOffset();
                    break;
                }
                case 2: {
                    Element paragraphsParent = NbDocument.findLineRootElement(this.doc);
                    Element line = paragraphsParent.getElement(this.argInt);
                    if (line == null) {
                        throw new IndexOutOfBoundsException("Index=" + this.argInt + " is out of bounds.");
                    }
                    this.retInt = line.getStartOffset();
                    break;
                }
                default: {
                    throw new IllegalStateException();
                }
            }
        }
    }

    public static interface Annotatable
    extends Document {
        public void addAnnotation(Position var1, int var2, Annotation var3);

        public void removeAnnotation(Annotation var1);
    }

    public static interface CustomToolbar
    extends Document {
        public JToolBar createToolbar(JEditorPane var1);
    }

    public static interface CustomEditor
    extends Document {
        public Component createEditor(JEditorPane var1);
    }

    public static interface PositionBiasable
    extends Document {
        public Position createPosition(int var1, Position.Bias var2) throws BadLocationException;
    }

    public static interface Printable
    extends Document {
        public AttributedCharacterIterator[] createPrintIterators();
    }

    public static interface WriteLockable
    extends Document {
        public void runAtomic(Runnable var1);

        public void runAtomicAsUser(Runnable var1) throws BadLocationException;
    }

}

