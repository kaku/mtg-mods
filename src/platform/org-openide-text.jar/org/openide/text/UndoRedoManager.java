/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.UndoRedo
 *  org.openide.awt.UndoRedo$Manager
 */
package org.openide.text;

import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.StyledDocument;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.CompoundEdit;
import javax.swing.undo.UndoableEdit;
import org.openide.awt.UndoRedo;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.NbDocument;
import org.openide.text.StableCompoundEdit;
import org.openide.text.WrapUndoEdit;

final class UndoRedoManager
extends UndoRedo.Manager {
    private static final Logger LOG = Logger.getLogger(UndoRedoManager.class.getName());
    static final UndoableEdit SAVEPOINT = new SpecialEdit();
    static final UndoableEdit BEGIN_COMMIT_GROUP = new SpecialEdit();
    static final UndoableEdit END_COMMIT_GROUP = new SpecialEdit();
    static final UndoableEdit MARK_COMMIT_GROUP = new SpecialEdit();
    CloneableEditorSupport support;
    UndoableEdit savepointEdit;
    boolean beforeSavepoint;
    private CompoundEdit onSaveTasksEdit;
    private boolean awaitingOnSaveTasks;
    private boolean callNotifyUnmodified;
    private int buildUndoGroup;
    private CompoundEdit undoGroup;
    private int needsNestingCommit;

    public UndoRedoManager(CloneableEditorSupport support) {
        this.support = support;
        super.setLimit(1000);
    }

    void startOnSaveTasks() {
        this.commitUndoGroup();
        this.clearSaveActionsEdit();
        this.awaitingOnSaveTasks = true;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("startSaveActions() called.\n");
        }
    }

    void endOnSaveTasks() {
        if (this.onSaveTasksEdit != null) {
            this.onSaveTasksEdit.end();
        }
        this.awaitingOnSaveTasks = false;
        this.checkLogOp("    endSaveActions(): saveActionsEdit", this.onSaveTasksEdit);
    }

    void markSavepoint() {
        this.commitUndoGroup();
        this.savepointEdit = SAVEPOINT;
    }

    boolean isAtSavepoint() {
        return this.savepointEdit == SAVEPOINT;
    }

    private void markSavepointAndUnmodified() {
        this.markSavepoint();
        this.callNotifyUnmodified = true;
    }

    private void checkCallNotifyUnmodified() {
        if (this.callNotifyUnmodified) {
            this.callNotifyUnmodified = false;
            this.support.callNotifyUnmodified();
        }
    }

    void mergeSaveActionsToLastEdit(WrapUndoEdit lastWrapEdit) {
        if (this.onSaveTasksEdit != null) {
            this.checkLogOp("    mergeSaveActionsToLastEdit-lastWrapEdit", lastWrapEdit);
            StableCompoundEdit compoundEdit = new StableCompoundEdit();
            compoundEdit.addEdit(lastWrapEdit.delegate());
            compoundEdit.addEdit(this.onSaveTasksEdit);
            compoundEdit.end();
            lastWrapEdit.setDelegate(compoundEdit);
            this.onSaveTasksEdit = null;
            this.checkLogOp("    compoundEdit", compoundEdit);
        }
    }

    void beforeUndoAtSavepoint(WrapUndoEdit edit) {
        this.checkLogOp("beforeUndoAtSavepoint: undoSaveActions()", edit);
        this.undoSaveActions();
    }

    private void undoSaveActions() {
        if (this.onSaveTasksEdit != null) {
            this.checkLogOp("    saveActionsEdit.undo()", this.onSaveTasksEdit);
            this.onSaveTasksEdit.undo();
        }
    }

    void delegateUndoFailedAtSavepoint(WrapUndoEdit edit) {
        this.checkLogOp("delegateUndoFailedAtSavepoint", edit);
        this.redoSaveActions();
    }

    private void redoSaveActions() {
        if (this.onSaveTasksEdit != null) {
            this.checkLogOp("    saveActionsEdit.redo()", this.onSaveTasksEdit);
            this.onSaveTasksEdit.redo();
        }
    }

    void afterUndoCheck(WrapUndoEdit edit) {
        if (this.isAtSavepoint()) {
            this.checkLogOp("afterUndoCheck-atSavepoint", edit);
            this.beforeSavepoint = true;
            this.savepointEdit = edit;
        } else if (this.savepointEdit == edit) {
            if (this.onSaveTasksEdit != null) {
                this.checkLogOp("    saveActionsEdit.redo()", this.onSaveTasksEdit);
                this.onSaveTasksEdit.redo();
            }
            this.checkLogOp("afterUndoCheck-becomesSavepoint-markUnmodified", edit);
            assert (!this.beforeSavepoint);
            this.markSavepointAndUnmodified();
        }
    }

    void beforeRedoAtSavepoint(WrapUndoEdit edit) {
        this.checkLogOp("beforeRedoAtSavepoint", edit);
        this.undoSaveActions();
    }

    void delegateRedoFailedAtSavepoint(WrapUndoEdit edit) {
        this.checkLogOp("delegateRedoFailedAtSavepoint", edit);
        this.redoSaveActions();
    }

    void afterRedoCheck(WrapUndoEdit edit) {
        if (this.isAtSavepoint()) {
            this.checkLogOp("afterRedoCheck-atSavepoint", edit);
            this.beforeSavepoint = false;
            this.savepointEdit = edit;
        } else if (this.savepointEdit == edit) {
            if (this.onSaveTasksEdit != null) {
                this.checkLogOp("    saveActionsEdit.redo()", this.onSaveTasksEdit);
                this.onSaveTasksEdit.redo();
            }
            this.checkLogOp("afterRedoCheck-becomesSavepoint", edit);
            assert (this.beforeSavepoint);
            this.markSavepointAndUnmodified();
        }
    }

    void checkReplaceSavepointEdit(WrapUndoEdit origEdit, WrapUndoEdit newEdit) {
        if (this.savepointEdit == origEdit) {
            this.checkLogOp("checkReplaceSavepointEdit-replacedSavepointEdit", origEdit);
            this.savepointEdit = newEdit;
        }
    }

    void notifyWrapEditDie(UndoableEdit edit) {
        if (edit == this.savepointEdit) {
            this.checkLogOp("notifyWrapEditDie-savepoint-die", edit);
            this.savepointEdit = null;
            this.clearSaveActionsEdit();
        }
    }

    public synchronized boolean addEdit(UndoableEdit edit) {
        if (!this.isInProgress()) {
            return false;
        }
        if (edit == BEGIN_COMMIT_GROUP) {
            this.beginUndoGroup();
            return true;
        }
        if (edit == END_COMMIT_GROUP) {
            this.endUndoGroup();
            return true;
        }
        if (edit == MARK_COMMIT_GROUP) {
            this.commitUndoGroup();
            return true;
        }
        if (this.needsNestingCommit > 0) {
            this.commitUndoGroup();
        }
        if (!this.awaitingOnSaveTasks && this.buildUndoGroup > 0) {
            if (this.undoGroup == null) {
                this.undoGroup = new CompoundEdit();
            }
            return this.undoGroup.addEdit(edit);
        }
        return this.addEditImpl(edit);
    }

    private boolean addEditImpl(UndoableEdit edit) {
        assert (edit != null);
        if (this.awaitingOnSaveTasks) {
            this.checkLogOp("addEdit-inSaveActions", edit);
            if (this.onSaveTasksEdit == null) {
                this.onSaveTasksEdit = new CompoundEdit();
            }
            boolean added = this.onSaveTasksEdit.addEdit(edit);
            assert (added);
            return true;
        }
        WrapUndoEdit wrapEdit = new WrapUndoEdit(this, edit);
        boolean added = super.addEdit((UndoableEdit)wrapEdit);
        if (this.isAtSavepoint()) {
            this.checkLogOp("addEdit-atSavepoint", wrapEdit);
            this.beforeSavepoint = false;
            this.savepointEdit = wrapEdit;
            if (added && this.edits.size() == 1) {
                this.clearSaveActionsEdit();
            }
        } else {
            this.checkLogOp("addEdit", wrapEdit);
        }
        return added;
    }

    public void redo() throws CannotRedoException {
        StyledDocument doc = this.support.getDocument();
        if (doc == null) {
            throw new CannotRedoException();
        }
        new DocLockedRun(0, doc);
        this.checkCallNotifyUnmodified();
    }

    public void undo() throws CannotUndoException {
        StyledDocument doc = this.support.getDocument();
        if (doc == null) {
            throw new CannotUndoException();
        }
        new DocLockedRun(1, doc);
        this.checkCallNotifyUnmodified();
    }

    public boolean canRedo() {
        return (UndoRedoManager)this.new DocLockedRun((int)2, (StyledDocument)this.support.getDocument(), (int)0, (boolean)true).booleanResult;
    }

    public boolean canUndo() {
        return (UndoRedoManager)this.new DocLockedRun((int)3, (StyledDocument)this.support.getDocument(), (int)0, (boolean)true).booleanResult;
    }

    public int getLimit() {
        return (UndoRedoManager)this.new DocLockedRun((int)4, (StyledDocument)this.support.getDocument()).intResult;
    }

    public void discardAllEdits() {
        new DocLockedRun(5, this.support.getDocument(), 0, true);
    }

    private void clearSaveActionsEdit() {
        if (this.onSaveTasksEdit != null) {
            this.checkLogOp("    saveActionsEdit-die", this.onSaveTasksEdit);
            this.onSaveTasksEdit.die();
            this.onSaveTasksEdit = null;
        }
    }

    public void setLimit(int l) {
        new DocLockedRun(6, this.support.getDocument(), l);
    }

    public boolean canUndoOrRedo() {
        return (UndoRedoManager)this.new DocLockedRun((int)7, (StyledDocument)this.support.getDocument(), (int)0, (boolean)true).booleanResult;
    }

    public String getUndoOrRedoPresentationName() {
        if (this.support.isDocumentReady()) {
            return (UndoRedoManager)this.new DocLockedRun((int)8, (StyledDocument)this.support.getDocument(), (int)0, (boolean)true).stringResult;
        }
        return "";
    }

    public String getRedoPresentationName() {
        if (this.support.isDocumentReady()) {
            return (UndoRedoManager)this.new DocLockedRun((int)9, (StyledDocument)this.support.getDocument(), (int)0, (boolean)true).stringResult;
        }
        return "";
    }

    public String getUndoPresentationName() {
        if (this.support.isDocumentReady()) {
            return (UndoRedoManager)this.new DocLockedRun((int)10, (StyledDocument)this.support.getDocument(), (int)0, (boolean)true).stringResult;
        }
        return "";
    }

    public void undoOrRedo() throws CannotUndoException, CannotRedoException {
        super.undoOrRedo();
    }

    private void beginUndoGroup() {
        if (this.undoGroup != null) {
            ++this.needsNestingCommit;
        }
        LOG.log(Level.FINE, "beginUndoGroup: nesting {0}", this.buildUndoGroup);
        ++this.buildUndoGroup;
    }

    private void endUndoGroup() {
        --this.buildUndoGroup;
        LOG.log(Level.FINE, "endUndoGroup: nesting {0}", this.buildUndoGroup);
        if (this.buildUndoGroup < 0) {
            LOG.log(Level.INFO, null, new Exception("endUndoGroup without beginUndoGroup"));
            this.buildUndoGroup = 0;
        }
        if (this.needsNestingCommit <= 0) {
            this.commitUndoGroup();
        }
        if (--this.needsNestingCommit < 0) {
            this.needsNestingCommit = 0;
        }
    }

    private void commitUndoGroup() {
        if (this.undoGroup == null) {
            return;
        }
        this.needsNestingCommit = 0;
        this.undoGroup.end();
        this.addEditImpl(this.undoGroup);
        this.undoGroup = null;
    }

    UndoableEdit editToBeUndoneRedone(boolean redone) {
        WrapUndoEdit wrapEdit = (WrapUndoEdit)(redone ? this.editToBeRedone() : this.editToBeUndone());
        return wrapEdit != null ? wrapEdit.delegate() : null;
    }

    static String editToString(UndoableEdit edit) {
        if (edit instanceof WrapUndoEdit) {
            return UndoRedoManager.toStringTerse(edit) + "->" + UndoRedoManager.toStringTerse(((WrapUndoEdit)edit).delegate());
        }
        return UndoRedoManager.toStringTerse(edit);
    }

    static String toStringTerse(Object o) {
        if (o != null) {
            String clsName = o.getClass().getName();
            return clsName.substring(clsName.lastIndexOf(46) + 1) + "@" + System.identityHashCode(o);
        }
        return "null";
    }

    void checkLogOp(String op, UndoableEdit edit) {
        if (LOG.isLoggable(Level.FINE)) {
            String msg = this.thisToString() + "->" + op + ": " + UndoRedoManager.editToString(edit) + '\n';
            if (LOG.isLoggable(Level.FINEST)) {
                LOG.log(Level.FINEST, msg.substring(0, msg.length() - 1), new Exception());
            } else {
                LOG.fine(msg);
            }
        }
    }

    String thisToString() {
        String name = this.support.messageName();
        return String.valueOf(name) + ":URM@" + System.identityHashCode((Object)this);
    }

    private static class SpecialEdit
    extends CompoundEdit {
        public SpecialEdit() {
            this.end();
        }

        @Override
        public boolean canRedo() {
            return true;
        }

        @Override
        public boolean canUndo() {
            return true;
        }
    }

    private final class DocLockedRun
    implements Runnable {
        private final int type;
        boolean booleanResult;
        int intResult;
        String stringResult;

        public DocLockedRun(int type, StyledDocument doc) {
            this(type, doc, 0);
        }

        public DocLockedRun(int type, StyledDocument doc, int intValue) {
            this(type, doc, intValue, false);
        }

        public DocLockedRun(int type, StyledDocument doc, int intValue, boolean readLock) {
            this.type = type;
            this.intResult = intValue;
            if (!readLock && doc instanceof NbDocument.WriteLockable) {
                ((NbDocument.WriteLockable)((Object)doc)).runAtomic(this);
            } else if (readLock && doc != null) {
                doc.render(this);
            } else {
                this.run();
            }
        }

        @Override
        public void run() {
            switch (this.type) {
                case 0: {
                    if (UndoRedoManager.this.undoGroup != null) {
                        throw new CannotRedoException();
                    }
                    UndoRedoManager.this.redo();
                    break;
                }
                case 1: {
                    UndoRedoManager.this.commitUndoGroup();
                    UndoRedoManager.this.undo();
                    break;
                }
                case 2: {
                    this.booleanResult = UndoRedoManager.this.undoGroup != null ? false : UndoRedoManager.this.canRedo();
                    break;
                }
                case 3: {
                    this.booleanResult = UndoRedoManager.this.undoGroup != null ? true : UndoRedoManager.this.canUndo();
                    break;
                }
                case 4: {
                    this.intResult = UndoRedoManager.this.getLimit();
                    break;
                }
                case 5: {
                    if (LOG.isLoggable(Level.FINE)) {
                        int editsSize = UndoRedoManager.this.edits != null ? UndoRedoManager.this.edits.size() : 0;
                        LOG.fine("discardAllEdits(): savepoint=" + UndoRedoManager.this.isAtSavepoint() + ", editsSize=" + editsSize + "\n");
                    }
                    UndoRedoManager.this.commitUndoGroup();
                    UndoRedoManager.this.clearSaveActionsEdit();
                    UndoRedoManager.this.discardAllEdits();
                    break;
                }
                case 6: {
                    UndoRedoManager.this.setLimit(this.intResult);
                    break;
                }
                case 7: {
                    UndoRedoManager.this.canUndoOrRedo();
                    break;
                }
                case 8: {
                    this.stringResult = UndoRedoManager.this.getUndoOrRedoPresentationName();
                    break;
                }
                case 9: {
                    this.stringResult = UndoRedoManager.this.undoGroup != null ? UndoRedoManager.this.undoGroup.getRedoPresentationName() : UndoRedoManager.this.getRedoPresentationName();
                    break;
                }
                case 10: {
                    this.stringResult = UndoRedoManager.this.undoGroup != null ? UndoRedoManager.this.undoGroup.getUndoPresentationName() : UndoRedoManager.this.getUndoPresentationName();
                    break;
                }
                case 11: {
                    UndoRedoManager.this.undoOrRedo();
                    break;
                }
                default: {
                    throw new IllegalArgumentException("Unknown type: " + this.type);
                }
            }
        }
    }

}

