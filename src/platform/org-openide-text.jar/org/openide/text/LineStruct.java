/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.openide.text;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import org.openide.util.RequestProcessor;

final class LineStruct {
    private static final int MAX = 1073741823;
    private static final RequestProcessor PROCESSOR = new RequestProcessor("LineStruct Processor", 1, false, false);
    private List<Info> list = new LinkedList<Info>();

    public LineStruct() {
        this.list.add(new Info(1073741823, 1073741823));
    }

    public int convert(int line, boolean currentToOriginal) {
        class Compute
        implements Runnable {
            public int result;
            final /* synthetic */ boolean val$currentToOriginal;
            final /* synthetic */ LineStruct this$0;

            /*
             * WARNING - Possible parameter corruption
             */
            public Compute(int n) {
                this.this$0 = (LineStruct)n;
                this.val$currentToOriginal = n2;
                this.result = i;
            }

            @Override
            public void run() {
                this.result = this.val$currentToOriginal ? this.this$0.originalToCurrentImpl(this.result) : this.this$0.currentToOriginalImpl(this.result);
            }
        }
        Compute c = new Compute(this, line, currentToOriginal);
        PROCESSOR.post((Runnable)c).waitFinished();
        return c.result;
    }

    public void insertLines(final int line, final int count) {
        PROCESSOR.post(new Runnable(){

            @Override
            public void run() {
                LineStruct.this.insertLinesImpl(line, count);
            }
        });
    }

    public void deleteLines(final int line, final int count) {
        PROCESSOR.post(new Runnable(){

            @Override
            public void run() {
                LineStruct.this.deleteLinesImpl(line, count);
            }
        });
    }

    private int originalToCurrentImpl(int line) {
        Iterator<Info> it = this.list.iterator();
        int cur = 0;
        do {
            Info i = it.next();
            if (i.original > line) {
                return line > i.current ? cur + i.current : cur + line;
            }
            cur += i.current;
            line -= i.original;
        } while (true);
    }

    private int currentToOriginalImpl(int line) {
        Iterator<Info> it = this.list.iterator();
        int cur = 0;
        do {
            Info i = it.next();
            if (i.current > line) {
                return line > i.original ? cur + i.original : cur + line;
            }
            cur += i.original;
            line -= i.current;
        } while (true);
    }

    private void insertLinesImpl(int line, int count) {
        ListIterator<Info> it = this.list.listIterator();
        do {
            Info i = it.next();
            if (i.current >= line) {
                do {
                    if ((count = i.insert(line, count, it)) == 0) {
                        return;
                    }
                    i = it.next();
                    line = 0;
                } while (true);
            }
            line -= i.current;
        } while (true);
    }

    private void deleteLinesImpl(int line, int count) {
        ListIterator<Info> it = this.list.listIterator();
        do {
            Info i = it.next();
            if (i.current >= line) {
                Info stat = new Info(count, 0);
                do {
                    stat = i.delete(line, stat, it);
                    if (stat.original == 0) break;
                    i = it.next();
                    line = 0;
                } while (true);
                if (stat.current > 0 && it.hasPrevious()) {
                    Info prev = it.previous();
                    boolean hasPrev = it.hasPrevious();
                    if (hasPrev) {
                        prev = it.previous();
                    }
                    if (prev.current == 0) {
                        prev.original += stat.current;
                    } else {
                        if (hasPrev) {
                            it.next();
                        }
                        it.add(new Info(stat.current, 0));
                    }
                }
                return;
            }
            line -= i.current;
        } while (true);
    }

    private static final class Info {
        public static final int AREA_ORIGINAL = 0;
        public static final int AREA_INSERT = 1;
        public static final int AREA_REMOVE = -1;
        public int original;
        public int current;

        public Info(int o, int c) {
            this.original = o;
            this.current = c;
        }

        public int type() {
            if (this.current == this.original) {
                return 0;
            }
            if (this.current == 0) {
                return -1;
            }
            if (this.original == 0) {
                return 1;
            }
            throw new IllegalStateException("Original: " + this.original + " current: " + this.current);
        }

        public int insert(int pos, int count, ListIterator<Info> it) {
            switch (this.type()) {
                case 1: {
                    this.current += count;
                    return 0;
                }
                case 0: {
                    if (pos == this.current) {
                        return count;
                    }
                    if (pos == 0) {
                        Info ni = new Info(this.original, this.original);
                        this.original = 0;
                        this.current = count;
                        it.add(ni);
                        return 0;
                    }
                    Info ni = new Info(this.original - pos, this.original - pos);
                    this.original = this.current = pos;
                    it.add(new Info(0, count));
                    it.add(ni);
                    return 0;
                }
                case -1: {
                    if (pos != 0) {
                        throw new IllegalStateException("Pos: " + pos);
                    }
                    Info prev = it.previous();
                    if (it.hasPrevious()) {
                        prev = it.previous();
                        it.next();
                    }
                    it.next();
                    if (count < this.original) {
                        if (prev.type() == 0) {
                            prev.original += count;
                            prev.current += count;
                            this.original -= count;
                        } else {
                            Info ni = new Info(this.original - count, 0);
                            this.original = this.current = count;
                            it.add(ni);
                        }
                        return 0;
                    }
                    if (prev.type() == 0) {
                        prev.current += this.original;
                        prev.original += this.original;
                        it.remove();
                        return count - this.original;
                    }
                    this.current = this.original;
                    return count - this.current;
                }
            }
            throw new IllegalStateException("Type: " + this.type());
        }

        public Info delete(int pos, Info info, ListIterator<Info> it) {
            switch (this.type()) {
                case 0: {
                    if (pos != 0) {
                        int size = this.current - pos;
                        this.current = this.original = pos;
                        if (size >= info.original) {
                            Info ni = new Info(size, size);
                            it.add(ni);
                            info.current += info.original;
                            info.original = 0;
                            return info;
                        }
                        info.original -= size;
                        info.current += size;
                        return info;
                    }
                    if (this.current >= info.original) {
                        info.current += info.original;
                        this.current -= info.original;
                        this.original = this.current;
                        info.original = 0;
                        return info;
                    }
                    it.remove();
                    info.current += this.current;
                    info.original -= this.current;
                    return info;
                }
                case 1: {
                    if (pos != 0) {
                        int size = this.current - pos;
                        if (size >= info.original) {
                            this.current -= info.original;
                            info.original = 0;
                            return info;
                        }
                        this.current = pos;
                        info.original -= size;
                        return info;
                    }
                    if (this.current >= info.original) {
                        this.current -= info.original;
                        info.original = 0;
                        it.remove();
                        return info;
                    }
                    it.remove();
                    info.original -= this.current;
                    return info;
                }
                case -1: {
                    this.original += info.current;
                    info.current = 0;
                    return info;
                }
            }
            throw new IllegalStateException("Type: " + this.type());
        }
    }

}

