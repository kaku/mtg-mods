/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.openide.text;

import java.io.IOException;
import java.io.Serializable;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.NbDocument;
import org.openide.text.PositionRef;
import org.openide.util.NbBundle;

public final class PositionBounds
implements Serializable {
    static final long serialVersionUID = 3338509625548836633L;
    private PositionRef begin;
    private PositionRef end;

    public PositionBounds(PositionRef begin, PositionRef end) {
        this.begin = begin;
        this.end = end;
    }

    public PositionRef getBegin() {
        return this.begin;
    }

    public PositionRef getEnd() {
        return this.end;
    }

    public void setText(final String text) throws IOException, BadLocationException {
        final CloneableEditorSupport editor = this.begin.getCloneableEditorSupport();
        final StyledDocument doc = editor.openDocument();
        final BadLocationException[] hold = new BadLocationException[]{null};
        Runnable run = new Runnable(){

            @Override
            public void run() {
                try {
                    int p1 = PositionBounds.this.begin.getOffset();
                    int p2 = PositionBounds.this.end.getOffset();
                    int len = text.length();
                    if (len == 0) {
                        if (p2 > p1) {
                            doc.remove(p1, p2 - p1);
                        }
                    } else {
                        int docLen = doc.getLength();
                        if (p2 - p1 >= 2) {
                            doc.insertString(p1 + 1, text, null);
                            len = doc.getLength() - docLen;
                            doc.remove(p1 + 1 + len, p2 - p1 - 1);
                            doc.remove(p1, 1);
                        } else {
                            doc.insertString(p1, text, null);
                            len = doc.getLength() - docLen;
                            if (p2 > p1) {
                                doc.remove(p1 + len, p2 - p1);
                            }
                            if (PositionBounds.this.begin.getOffset() != p1) {
                                PositionBounds.this.begin = editor.createPositionRef(p1, PositionBounds.this.begin.getPositionBias());
                            }
                            if (PositionBounds.this.end.getOffset() - p1 != len) {
                                PositionBounds.this.end = editor.createPositionRef(p1 + len, PositionBounds.this.end.getPositionBias());
                            }
                        }
                    }
                }
                catch (BadLocationException e) {
                    hold[0] = e;
                }
            }
        };
        NbDocument.runAtomic(doc, run);
        if (hold[0] != null) {
            throw hold[0];
        }
    }

    public PositionBounds insertAfter(final String text) throws IOException, BadLocationException {
        if (text.length() == 0) {
            throw new BadLocationException(NbBundle.getBundle(PositionBounds.class).getString("MSG_Empty_string"), this.begin.getOffset());
        }
        final CloneableEditorSupport editor = this.begin.getCloneableEditorSupport();
        final StyledDocument doc = editor.openDocument();
        final Object[] hold = new Object[]{null, null};
        Runnable run = new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                Object object = editor.getLock();
                synchronized (object) {
                    try {
                        int docLen = doc.getLength();
                        int p1 = PositionBounds.this.end.getOffset();
                        doc.insertString(p1, text, null);
                        int p2 = p1 + doc.getLength() - docLen;
                        PositionBounds.this.end = editor.createPositionRef(p1, PositionBounds.this.end.getPositionBias());
                        PositionRef posBegin = editor.createPositionRef(p1, Position.Bias.Forward);
                        PositionRef posEnd = editor.createPositionRef(p2, Position.Bias.Backward);
                        hold[1] = new PositionBounds(posBegin, posEnd);
                    }
                    catch (BadLocationException e) {
                        hold[0] = e;
                    }
                }
            }
        };
        NbDocument.runAtomic(doc, run);
        if (hold[0] != null) {
            throw (BadLocationException)hold[0];
        }
        return (PositionBounds)hold[1];
    }

    public String getText() throws BadLocationException, IOException {
        StyledDocument doc = this.begin.getCloneableEditorSupport().openDocument();
        int p1 = this.begin.getOffset();
        int p2 = this.end.getOffset();
        return doc.getText(p1, p2 - p1);
    }

    public String toString() {
        StringBuffer buf = new StringBuffer("Position bounds[");
        try {
            String content = this.getText();
            buf.append(this.begin);
            buf.append(",");
            buf.append(this.end);
            buf.append(",\"");
            buf.append(content);
            buf.append("\"");
        }
        catch (IOException e) {
            buf.append("Invalid: ");
            buf.append(e.getMessage());
        }
        catch (BadLocationException e) {
            buf.append("Invalid: ");
            buf.append(e.getMessage());
        }
        buf.append("]");
        return buf.toString();
    }

}

