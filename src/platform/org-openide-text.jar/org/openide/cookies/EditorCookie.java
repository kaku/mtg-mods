/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Task
 */
package org.openide.cookies;

import java.beans.PropertyChangeListener;
import java.io.IOException;
import javax.swing.JEditorPane;
import javax.swing.text.StyledDocument;
import org.openide.cookies.LineCookie;
import org.openide.util.Task;

public interface EditorCookie
extends LineCookie {
    public void open();

    public boolean close();

    public Task prepareDocument();

    public StyledDocument openDocument() throws IOException;

    public StyledDocument getDocument();

    public void saveDocument() throws IOException;

    public boolean isModified();

    public JEditorPane[] getOpenedPanes();

    public static interface Observable
    extends EditorCookie {
        public static final String PROP_DOCUMENT = "document";
        public static final String PROP_MODIFIED = "modified";
        public static final String PROP_OPENED_PANES = "openedPanes";

        public void addPropertyChangeListener(PropertyChangeListener var1);

        public void removePropertyChangeListener(PropertyChangeListener var1);
    }

}

