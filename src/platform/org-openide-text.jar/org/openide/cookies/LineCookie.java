/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 */
package org.openide.cookies;

import org.openide.nodes.Node;
import org.openide.text.Line;

public interface LineCookie
extends Node.Cookie {
    public Line.Set getLineSet();
}

