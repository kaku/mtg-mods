/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.progress.spi;

import org.netbeans.modules.progress.spi.ProgressUIWorker;
import org.netbeans.modules.progress.spi.TaskModel;

public interface ProgressUIWorkerWithModel
extends ProgressUIWorker {
    public void setModel(TaskModel var1);

    public void showPopup();
}

