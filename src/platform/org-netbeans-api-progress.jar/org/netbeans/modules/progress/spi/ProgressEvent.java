/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.progress.spi;

import org.netbeans.modules.progress.spi.InternalHandle;

public final class ProgressEvent {
    public static final int TYPE_START = 0;
    public static final int TYPE_FINISH = 4;
    public static final int TYPE_REQUEST_STOP = 3;
    public static final int TYPE_PROGRESS = 1;
    public static final int TYPE_SWITCH = 5;
    public static final int TYPE_SILENT = 6;
    private InternalHandle source;
    private long estimatedCompletion;
    private double percentageDone;
    private int workunitsDone;
    private String message;
    private int type;
    private boolean watched;
    private boolean switched;
    private String displayName;

    public ProgressEvent(InternalHandle src, int type, boolean isWatched) {
        this.source = src;
        this.estimatedCompletion = -1;
        this.percentageDone = -1.0;
        this.workunitsDone = -1;
        this.message = null;
        this.type = type;
        this.watched = isWatched;
        this.switched = type == 5;
    }

    public ProgressEvent(InternalHandle src, int type, boolean isWatched, String msg) {
        this(src, type, isWatched);
        this.message = msg;
    }

    public ProgressEvent(InternalHandle src, String msg, int units, double percentage, long estimate, boolean isWatched) {
        this(src, 1, isWatched);
        this.workunitsDone = units;
        this.percentageDone = percentage;
        this.estimatedCompletion = estimate;
        this.message = msg;
    }

    public ProgressEvent(InternalHandle src, String msg, int units, double percentage, long estimate, boolean isWatched, String displayName) {
        this(src, msg, units, percentage, estimate, isWatched);
        this.displayName = displayName;
    }

    public InternalHandle getSource() {
        return this.source;
    }

    public long getEstimatedCompletion() {
        return this.estimatedCompletion;
    }

    public double getPercentageDone() {
        return this.percentageDone;
    }

    public int getWorkunitsDone() {
        return this.workunitsDone;
    }

    public String getMessage() {
        return this.message;
    }

    public int getType() {
        return this.type;
    }

    public boolean isWatched() {
        return this.watched;
    }

    public void copyMessageFromEarlier(ProgressEvent last) {
        if (this.message == null) {
            this.message = last.getMessage();
        }
        if (this.displayName == null) {
            this.displayName = last.getDisplayName();
        }
    }

    public void markAsSwitched() {
        this.switched = true;
    }

    public boolean isSwitched() {
        return this.switched;
    }

    public String getDisplayName() {
        return this.displayName;
    }
}

