/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.progress.spi;

import org.netbeans.modules.progress.spi.ProgressEvent;

public interface ProgressUIWorker {
    public void processProgressEvent(ProgressEvent var1);

    public void processSelectedProgressEvent(ProgressEvent var1);
}

