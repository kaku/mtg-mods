/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.progress.spi;

import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.JPanel;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressRunnable;

public interface RunOffEDTProvider {
    public void runOffEventDispatchThread(Runnable var1, String var2, AtomicBoolean var3, boolean var4, int var5, int var6);

    public static interface Progress2
    extends Progress {
        public void runOffEventThreadWithProgressDialog(Runnable var1, String var2, ProgressHandle var3, boolean var4, int var5, int var6);

        public void runOffEventThreadWithCustomDialogContent(Runnable var1, String var2, JPanel var3, int var4, int var5);
    }

    public static interface Progress
    extends RunOffEDTProvider {
        public void showProgressDialogAndRun(Runnable var1, ProgressHandle var2, boolean var3);

        public <T> T showProgressDialogAndRun(ProgressRunnable<T> var1, String var2, boolean var3);

        public <T> Future<T> showProgressDialogAndRunLater(ProgressRunnable<T> var1, ProgressHandle var2, boolean var3);
    }

}

