/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.progress.spi;

import java.awt.EventQueue;
import java.util.LinkedHashSet;
import javax.swing.DefaultListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.netbeans.modules.progress.spi.InternalHandle;

public final class TaskModel {
    private DefaultListSelectionModel selectionModel = new DefaultListSelectionModel();
    private final DefaultListModel model = new DefaultListModel();
    private InternalHandle explicit;
    private final LinkedHashSet<ListDataListener> dataListeners = new LinkedHashSet();
    private final LinkedHashSet<ListSelectionListener> selectionListeners = new LinkedHashSet();

    public TaskModel() {
        TaskListener list = new TaskListener();
        this.model.addListDataListener(list);
        this.selectionModel.addListSelectionListener(list);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addHandle(InternalHandle handle) {
        DefaultListModel defaultListModel = this.model;
        synchronized (defaultListModel) {
            this.model.addElement(handle);
        }
        this.updateSelection();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeHandle(InternalHandle handle) {
        if (this.explicit == handle) {
            this.explicit = null;
        }
        DefaultListModel defaultListModel = this.model;
        synchronized (defaultListModel) {
            this.model.removeElement(handle);
        }
        this.updateSelection();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void updateSelection() {
        if (this.explicit != null) {
            return;
        }
        InternalHandle oldSelected = this.getSelectedHandle();
        if (oldSelected != null && !oldSelected.isInSleepMode()) {
            return;
        }
        InternalHandle toSelect = null;
        DefaultListModel defaultListModel = this.model;
        synchronized (defaultListModel) {
            for (int i = 0; i < this.model.size(); ++i) {
                InternalHandle curHandle = (InternalHandle)this.model.getElementAt(i);
                if (this.getSelectionRating(curHandle) < this.getSelectionRating(toSelect)) continue;
                toSelect = curHandle;
            }
            if (toSelect != null) {
                this.selectionModel.setSelectionInterval(this.model.indexOf(toSelect), this.model.indexOf(toSelect));
            } else {
                this.selectionModel.clearSelection();
            }
        }
    }

    private int getSelectionRating(InternalHandle handle) {
        int result = 0;
        if (handle != null) {
            if (!handle.isInSleepMode()) {
                result += 4;
            }
            if (handle.isUserInitialized()) {
                result += 2;
            }
            ++result;
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void explicitlySelect(InternalHandle handle) {
        this.explicit = handle;
        DefaultListModel defaultListModel = this.model;
        synchronized (defaultListModel) {
            int index = this.model.indexOf(this.explicit);
            if (index == -1) {
                return;
            }
            this.selectionModel.setSelectionInterval(index, index);
        }
    }

    public InternalHandle getExplicitSelection() {
        return this.explicit;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public int getSize() {
        DefaultListModel defaultListModel = this.model;
        synchronized (defaultListModel) {
            return this.model.size();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public InternalHandle[] getHandles() {
        Object[] handles;
        DefaultListModel defaultListModel = this.model;
        synchronized (defaultListModel) {
            handles = new InternalHandle[this.model.size()];
            this.model.copyInto(handles);
        }
        return handles;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public InternalHandle getSelectedHandle() {
        DefaultListModel defaultListModel = this.model;
        synchronized (defaultListModel) {
            int select = this.selectionModel.getMinSelectionIndex();
            if (select != -1 && select >= 0 && select < this.model.size()) {
                return (InternalHandle)this.model.getElementAt(select);
            }
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addListSelectionListener(ListSelectionListener listener) {
        LinkedHashSet<ListSelectionListener> linkedHashSet = this.selectionListeners;
        synchronized (linkedHashSet) {
            this.selectionListeners.add(listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeListSelectionListener(ListSelectionListener listener) {
        LinkedHashSet<ListSelectionListener> linkedHashSet = this.selectionListeners;
        synchronized (linkedHashSet) {
            this.selectionListeners.remove(listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addListDataListener(ListDataListener listener) {
        LinkedHashSet<ListDataListener> linkedHashSet = this.dataListeners;
        synchronized (linkedHashSet) {
            this.dataListeners.add(listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeListDataListener(ListDataListener listener) {
        LinkedHashSet<ListDataListener> linkedHashSet = this.dataListeners;
        synchronized (linkedHashSet) {
            this.dataListeners.remove(listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private ListDataListener[] getDataListeners() {
        LinkedHashSet<ListDataListener> linkedHashSet = this.dataListeners;
        synchronized (linkedHashSet) {
            return this.dataListeners.toArray(new ListDataListener[this.dataListeners.size()]);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private ListSelectionListener[] getSelectionListeners() {
        LinkedHashSet<ListSelectionListener> linkedHashSet = this.selectionListeners;
        synchronized (linkedHashSet) {
            return this.selectionListeners.toArray(new ListSelectionListener[this.selectionListeners.size()]);
        }
    }

    private class TaskListener
    implements ListDataListener,
    ListSelectionListener {
        private TaskListener() {
        }

        @Override
        public void intervalAdded(final ListDataEvent e) {
            final ListDataListener[] lists = TaskModel.this.getDataListeners();
            EventQueue.invokeLater(new Runnable(){

                @Override
                public void run() {
                    for (ListDataListener list : lists) {
                        list.intervalAdded(e);
                    }
                }
            });
        }

        @Override
        public void intervalRemoved(final ListDataEvent e) {
            final ListDataListener[] lists = TaskModel.this.getDataListeners();
            EventQueue.invokeLater(new Runnable(){

                @Override
                public void run() {
                    for (ListDataListener list : lists) {
                        list.intervalRemoved(e);
                    }
                }
            });
        }

        @Override
        public void contentsChanged(final ListDataEvent e) {
            final ListDataListener[] lists = TaskModel.this.getDataListeners();
            EventQueue.invokeLater(new Runnable(){

                @Override
                public void run() {
                    for (ListDataListener list : lists) {
                        list.contentsChanged(e);
                    }
                }
            });
        }

        @Override
        public void valueChanged(final ListSelectionEvent e) {
            final ListSelectionListener[] lists = TaskModel.this.getSelectionListeners();
            EventQueue.invokeLater(new Runnable(){

                @Override
                public void run() {
                    for (ListSelectionListener list : lists) {
                        list.valueChanged(e);
                    }
                }
            });
        }

    }

}

