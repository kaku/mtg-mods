/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.progress.spi;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import org.netbeans.modules.progress.spi.InternalHandle;
import org.netbeans.modules.progress.spi.ProgressEvent;
import org.netbeans.modules.progress.spi.ProgressUIWorker;
import org.netbeans.modules.progress.spi.ProgressUIWorkerProvider;
import org.netbeans.modules.progress.spi.ProgressUIWorkerWithModel;
import org.netbeans.modules.progress.spi.TaskModel;
import org.netbeans.progress.module.TrivialProgressUIWorkerProvider;
import org.openide.util.Lookup;

public class Controller {
    public static Controller defaultInstance;
    private ProgressUIWorker component;
    private TaskModel model;
    private List<ProgressEvent> eventQueue;
    private boolean dispatchRunning;
    protected Timer timer;
    private long timerStart = 0;
    private static final int TIMER_QUANTUM = 400;
    public static final int INITIAL_DELAY = 500;

    public Controller(ProgressUIWorker comp) {
        this.component = comp;
        this.model = new TaskModel();
        this.eventQueue = new LinkedList<ProgressEvent>();
        this.dispatchRunning = false;
        this.timer = new Timer(400, new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Controller.this.runNow();
            }
        });
        this.timer.setRepeats(false);
    }

    public static synchronized Controller getDefault() {
        if (defaultInstance == null) {
            defaultInstance = new Controller(null);
        }
        return defaultInstance;
    }

    public Component getVisualComponent() {
        if (this.component == null) {
            this.getProgressUIWorker();
        }
        if (this.component instanceof Component) {
            return (Component)((Object)this.component);
        }
        return null;
    }

    ProgressUIWorker getProgressUIWorker() {
        if (this.component == null) {
            ProgressUIWorkerProvider prov = (ProgressUIWorkerProvider)Lookup.getDefault().lookup(ProgressUIWorkerProvider.class);
            if (prov == null) {
                Logger.getLogger(Controller.class.getName()).log(Level.CONFIG, "Using fallback trivial progress implementation");
                prov = new TrivialProgressUIWorkerProvider();
            }
            ProgressUIWorkerWithModel prgUIWorker = prov.getDefaultWorker();
            prgUIWorker.setModel(defaultInstance.getModel());
            this.component = prgUIWorker;
        }
        return this.component;
    }

    public TaskModel getModel() {
        return this.model;
    }

    void start(InternalHandle handle) {
        ProgressEvent event = new ProgressEvent(handle, 0, this.isWatched(handle));
        if (this == Controller.getDefault() && handle.getInitialDelay() > 100) {
            this.postEvent(event, true);
        } else {
            this.runImmediately(Collections.singleton(event));
        }
    }

    void finish(InternalHandle handle) {
        ProgressEvent event = new ProgressEvent(handle, 4, this.isWatched(handle));
        this.postEvent(event);
    }

    void toIndeterminate(InternalHandle handle) {
        ProgressEvent event = new ProgressEvent(handle, 5, this.isWatched(handle));
        this.model.updateSelection();
        this.postEvent(event);
    }

    void toSilent(InternalHandle handle, String message) {
        ProgressEvent event = new ProgressEvent(handle, 6, this.isWatched(handle), message);
        this.model.updateSelection();
        this.postEvent(event);
    }

    void toDeterminate(InternalHandle handle) {
        ProgressEvent event = new ProgressEvent(handle, 5, this.isWatched(handle));
        this.model.updateSelection();
        this.postEvent(event);
    }

    void progress(InternalHandle handle, String msg, int units, double percentage, long estimate) {
        ProgressEvent event = new ProgressEvent(handle, msg, units, percentage, estimate, this.isWatched(handle));
        this.postEvent(event);
    }

    ProgressEvent snapshot(InternalHandle handle, String msg, int units, double percentage, long estimate) {
        if (handle.isInSleepMode()) {
            return new ProgressEvent(handle, 6, this.isWatched(handle), msg);
        }
        return new ProgressEvent(handle, msg, units, percentage, estimate, this.isWatched(handle));
    }

    void explicitSelection(InternalHandle handle) {
        InternalHandle old = this.model.getExplicitSelection();
        this.model.explicitlySelect(handle);
        ArrayList<ProgressEvent> evnts = new ArrayList<ProgressEvent>();
        evnts.add(handle.requestStateSnapshot());
        if (old != null && old != handle) {
            evnts.add(old.requestStateSnapshot());
        }
        this.runImmediately(evnts);
    }

    void displayNameChange(InternalHandle handle, int units, double percentage, long estimate, String display) {
        ArrayList<ProgressEvent> evnts = new ArrayList<ProgressEvent>();
        evnts.add(new ProgressEvent(handle, null, units, percentage, estimate, this.isWatched(handle), display));
        this.runImmediately(evnts);
    }

    private boolean isWatched(InternalHandle hndl) {
        return this.model.getExplicitSelection() == hndl;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void runImmediately(Collection<ProgressEvent> events) {
        Controller controller = this;
        synchronized (controller) {
            this.eventQueue.addAll(events);
            this.dispatchRunning = true;
        }
        if (SwingUtilities.isEventDispatchThread()) {
            this.runNow();
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    Controller.this.runNow();
                }
            });
        }
    }

    void postEvent(ProgressEvent event) {
        this.postEvent(event, false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void postEvent(ProgressEvent event, boolean shortenPeriod) {
        Controller controller = this;
        synchronized (controller) {
            this.eventQueue.add(event);
            if (!this.dispatchRunning) {
                this.timerStart = System.currentTimeMillis();
                int delay = this.timer.getInitialDelay();
                if (shortenPeriod && this.timer.getInitialDelay() > event.getSource().getInitialDelay()) {
                    delay = event.getSource().getInitialDelay();
                }
                this.dispatchRunning = true;
                this.resetTimer(delay, true);
            } else if (shortenPeriod && System.currentTimeMillis() - this.timerStart > (long)event.getSource().getInitialDelay()) {
                this.resetTimer(event.getSource().getInitialDelay(), true);
            }
        }
    }

    protected void resetTimer(int initialDelay, boolean restart) {
        this.timer.setInitialDelay(initialDelay);
        if (restart) {
            this.timer.restart();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void runNow() {
        Object event;
        Iterator it;
        HashMap<InternalHandle, ProgressEvent> map = new HashMap<InternalHandle, ProgressEvent>();
        boolean hasShortOne = false;
        long minDiff = 400;
        InternalHandle oldSelected = this.model.getSelectedHandle();
        long stamp = System.currentTimeMillis();
        Controller controller = this;
        synchronized (controller) {
            it = this.eventQueue.iterator();
            ArrayList<InternalHandle> justStarted = new ArrayList<InternalHandle>();
            while (it.hasNext()) {
                boolean isShort;
                ProgressEvent event2 = it.next();
                boolean bl = isShort = stamp - event2.getSource().getTimeStampStarted() < (long)event2.getSource().getInitialDelay();
                if (event2.getType() == 0) {
                    if (event2.getSource().isCustomPlaced() || !isShort) {
                        this.model.addHandle(event2.getSource());
                    } else {
                        justStarted.add(event2.getSource());
                    }
                } else if (event2.getType() == 4 && !justStarted.contains(event2.getSource())) {
                    this.model.removeHandle(event2.getSource());
                }
                ProgressEvent lastEvent = (ProgressEvent)map.get(event2.getSource());
                if (lastEvent != null && event2.getType() == 4 && justStarted.contains(event2.getSource()) && isShort) {
                    map.remove(event2.getSource());
                    justStarted.remove(event2.getSource());
                } else {
                    if (lastEvent != null) {
                        event2.copyMessageFromEarlier(lastEvent);
                        if (lastEvent.isSwitched()) {
                            event2.markAsSwitched();
                        }
                    }
                    map.put(event2.getSource(), event2);
                }
                it.remove();
            }
            for (InternalHandle hndl : justStarted) {
                long diff = stamp - hndl.getTimeStampStarted();
                if (diff >= (long)hndl.getInitialDelay()) {
                    this.model.addHandle(hndl);
                    continue;
                }
                this.eventQueue.add(new ProgressEvent(hndl, 0, this.isWatched(hndl)));
                ProgressEvent evnt = (ProgressEvent)map.remove(hndl);
                if (evnt.getType() != 0) {
                    this.eventQueue.add(evnt);
                }
                hasShortOne = true;
                minDiff = Math.min(minDiff, (long)hndl.getInitialDelay() - diff);
            }
        }
        InternalHandle selected = this.model.getSelectedHandle();
        selected = selected == null ? oldSelected : selected;
        it = map.values().iterator();
        if (this.component == null) {
            this.getProgressUIWorker();
        }
        while (it.hasNext()) {
            event = (ProgressEvent)it.next();
            if (selected == event.getSource()) {
                this.component.processSelectedProgressEvent((ProgressEvent)event);
            }
            this.component.processProgressEvent((ProgressEvent)event);
        }
        event = this;
        synchronized (event) {
            this.timer.stop();
            if (hasShortOne) {
                this.timerStart = System.currentTimeMillis();
                this.resetTimer((int)Math.max(100, minDiff), true);
            } else {
                this.dispatchRunning = false;
                this.resetTimer(400, !this.eventQueue.isEmpty());
            }
        }
    }

}

