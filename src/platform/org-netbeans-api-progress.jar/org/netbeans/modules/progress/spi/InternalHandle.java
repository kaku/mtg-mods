/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Cancellable
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.progress.spi;

import java.awt.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JLabel;
import org.netbeans.modules.progress.spi.Controller;
import org.netbeans.modules.progress.spi.ExtractedProgressUIWorker;
import org.netbeans.modules.progress.spi.ProgressEvent;
import org.netbeans.modules.progress.spi.ProgressUIWorker;
import org.netbeans.modules.progress.spi.ProgressUIWorkerProvider;
import org.netbeans.progress.module.LoggingUtils;
import org.netbeans.progress.module.TrivialProgressUIWorkerProvider;
import org.openide.util.Cancellable;
import org.openide.util.Lookup;

public final class InternalHandle {
    private static final Logger LOG = Logger.getLogger(InternalHandle.class.getName());
    private String displayName;
    private boolean customPlaced1 = false;
    private boolean customPlaced2 = false;
    private boolean customPlaced3 = false;
    private int state;
    private int totalUnits;
    private int currentUnit;
    private long initialEstimate;
    private long timeStarted;
    private long timeLastProgress;
    private long timeSleepy = 0;
    private String lastMessage;
    private final Cancellable cancelable;
    private final Action viewAction;
    private final boolean userInitiated;
    private int initialDelay = 500;
    private Controller controller;
    private ExtractedProgressUIWorker component;
    public static final int STATE_INITIALIZED = 0;
    public static final int STATE_RUNNING = 1;
    public static final int STATE_FINISHED = 2;
    public static final int STATE_REQUEST_STOP = 3;
    public static final int NO_INCREASE = -2;

    public InternalHandle(String displayName, Cancellable cancel, boolean userInitiated, Action view) {
        this.displayName = displayName;
        this.userInitiated = userInitiated;
        this.state = 0;
        this.totalUnits = 0;
        this.lastMessage = null;
        this.cancelable = cancel;
        this.viewAction = view;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public synchronized int getState() {
        return this.state;
    }

    public boolean isAllowCancel() {
        return this.cancelable != null && !this.isCustomPlaced();
    }

    public boolean isAllowView() {
        return this.viewAction != null && !this.isCustomPlaced();
    }

    public boolean isCustomPlaced() {
        return this.component != null;
    }

    public boolean isUserInitialized() {
        return this.userInitiated;
    }

    private int getCurrentUnit() {
        return this.currentUnit;
    }

    public int getTotalUnits() {
        return this.totalUnits;
    }

    public void setInitialDelay(int millis) {
        if (this.state != 0) {
            LOG.log(Level.WARNING, "Setting ProgressHandle.setInitialDelay() after the task is started has no effect at {0}", LoggingUtils.findCaller());
            return;
        }
        this.initialDelay = millis;
    }

    public int getInitialDelay() {
        return this.initialDelay;
    }

    public synchronized void toSilent(String message) {
        if (this.state != 1 && this.state != 3) {
            LOG.log(Level.WARNING, "Cannot switch to silent mode when not running at {0}", LoggingUtils.findCaller());
            return;
        }
        this.timeSleepy = this.timeLastProgress = System.currentTimeMillis();
        if (message != null) {
            this.lastMessage = message;
        }
        this.controller.toSilent(this, message);
    }

    public boolean isInSleepMode() {
        return this.timeSleepy == this.timeLastProgress;
    }

    public synchronized void toIndeterminate() {
        if (this.state != 1 && this.state != 3) {
            LOG.log(Level.WARNING, "Cannot switch to indeterminate mode when not running at {0}", LoggingUtils.findCaller());
            return;
        }
        this.totalUnits = 0;
        this.currentUnit = 0;
        this.initialEstimate = -1;
        this.timeLastProgress = System.currentTimeMillis();
        this.controller.toIndeterminate(this);
    }

    public synchronized void toDeterminate(int workunits, long estimate) {
        if (this.state != 1 && this.state != 3) {
            LOG.log(Level.WARNING, "Cannot switch to determinate mode when not running at {0}", LoggingUtils.findCaller());
            return;
        }
        if (workunits < 0) {
            throw new IllegalArgumentException("number of workunits cannot be negative");
        }
        this.totalUnits = workunits;
        this.currentUnit = 0;
        this.initialEstimate = estimate;
        this.timeLastProgress = System.currentTimeMillis();
        this.controller.toDeterminate(this);
    }

    public synchronized void start(String message, int workunits, long estimate) {
        if (this.state != 0) {
            LOG.log(Level.WARNING, "Cannot call start twice on a handle at {0}", LoggingUtils.findCaller());
            return;
        }
        if (workunits < 0) {
            throw new IllegalArgumentException("number of workunits cannot be negative");
        }
        this.totalUnits = workunits;
        this.currentUnit = 0;
        if (message != null) {
            this.lastMessage = message;
        }
        if (this.controller == null) {
            this.controller = Controller.getDefault();
        }
        this.state = 1;
        this.initialEstimate = estimate;
        this.timeLastProgress = this.timeStarted = System.currentTimeMillis();
        this.controller.start(this);
    }

    public synchronized void finish() {
        if (this.state == 0) {
            LOG.log(Level.WARNING, "Cannot finish a task that was never started at {0}", LoggingUtils.findCaller());
            return;
        }
        if (this.state == 2) {
            return;
        }
        this.state = 2;
        this.currentUnit = this.totalUnits;
        this.controller.finish(this);
    }

    public synchronized void progress(String message, int workunit) {
        if (this.state != 1 && this.state != 3) {
            LOG.log(Level.WARNING, "Cannot call progress on a task that was never started at {0}", LoggingUtils.findCaller());
            return;
        }
        if (workunit != -2) {
            if (workunit < this.currentUnit) {
                throw new IllegalArgumentException("Cannot decrease processed workunit count (" + workunit + ") to lower value than before (" + this.currentUnit + ")");
            }
            if (workunit > this.totalUnits) {
                LOG.log(Level.INFO, "Cannot process more work than scheduled. Progress handle with name \"" + this.getDisplayName() + "\" has requested progress to workunit no." + workunit + " but the total number of workunits is " + this.totalUnits + ". That means the progress bar UI will not display real progress and will stay at 100%.", new IllegalArgumentException());
                workunit = this.totalUnits;
            }
            this.currentUnit = workunit;
        }
        if (message != null) {
            this.lastMessage = message;
        }
        this.timeLastProgress = System.currentTimeMillis();
        this.controller.progress(this, message, this.currentUnit, this.totalUnits > 0 ? this.getPercentageDone() : -1.0, this.initialEstimate == -1 ? -1 : this.calculateFinishEstimate());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void requestCancel() {
        if (!this.isAllowCancel()) {
            return;
        }
        InternalHandle internalHandle = this;
        synchronized (internalHandle) {
            this.state = 3;
        }
        this.cancelable.cancel();
        internalHandle = this;
        synchronized (internalHandle) {
            this.requestStateSnapshot();
        }
    }

    public void requestView() {
        if (!this.isAllowView()) {
            return;
        }
        this.viewAction.actionPerformed(new ActionEvent(this.viewAction, 1001, "performView"));
    }

    public synchronized void requestExplicitSelection() {
        if (!this.isInSleepMode()) {
            this.timeLastProgress = System.currentTimeMillis();
        }
        this.controller.explicitSelection(this);
    }

    public synchronized void requestDisplayNameChange(String newDisplayName) {
        this.displayName = newDisplayName;
        if (this.state == 0) {
            return;
        }
        this.timeLastProgress = System.currentTimeMillis();
        this.controller.displayNameChange(this, this.currentUnit, this.totalUnits > 0 ? this.getPercentageDone() : -1.0, this.initialEstimate == -1 ? -1 : this.calculateFinishEstimate(), newDisplayName);
    }

    public synchronized ProgressEvent requestStateSnapshot() {
        if (!this.isInSleepMode()) {
            this.timeLastProgress = System.currentTimeMillis();
        }
        return this.controller.snapshot(this, this.lastMessage, this.currentUnit, this.totalUnits > 0 ? this.getPercentageDone() : -1.0, this.initialEstimate == -1 ? -1 : this.calculateFinishEstimate());
    }

    private void createExtractedWorker() {
        if (this.component == null) {
            ProgressUIWorkerProvider prov = (ProgressUIWorkerProvider)Lookup.getDefault().lookup(ProgressUIWorkerProvider.class);
            if (prov == null) {
                LOG.log(Level.CONFIG, "Using fallback trivial progress implementation");
                prov = new TrivialProgressUIWorkerProvider();
            }
            this.component = prov.getExtractedComponentWorker();
            this.controller = new Controller(this.component);
        }
    }

    public synchronized JComponent extractComponent() {
        if (this.customPlaced1) {
            throw new IllegalStateException("Cannot retrieve progress component multiple times");
        }
        if (this.state != 0) {
            throw new IllegalStateException("You can request custom placement of progress component only before starting the task");
        }
        this.customPlaced1 = true;
        this.createExtractedWorker();
        return this.component.getProgressComponent();
    }

    public synchronized JLabel extractDetailLabel() {
        if (this.customPlaced2) {
            throw new IllegalStateException("Cannot retrieve progress detail label component multiple times");
        }
        if (this.state != 0) {
            throw new IllegalStateException("You can request custom placement of progress component only before starting the task");
        }
        this.customPlaced2 = true;
        this.createExtractedWorker();
        return this.component.getDetailLabelComponent();
    }

    public synchronized JLabel extractMainLabel() {
        if (this.customPlaced3) {
            throw new IllegalStateException("Cannot retrieve progress main label component multiple times");
        }
        if (this.state != 0) {
            throw new IllegalStateException("You can request custom placement of progress component only before starting the task");
        }
        this.customPlaced3 = true;
        this.createExtractedWorker();
        return this.component.getMainLabelComponent();
    }

    long calculateFinishEstimate() {
        double durationSoFar = (double)(System.currentTimeMillis() - this.timeStarted) / 1000.0;
        if (this.initialEstimate == -1) {
            return (long)(durationSoFar * (double)(this.totalUnits - this.currentUnit) / (double)this.totalUnits);
        }
        long remainingUnits = this.totalUnits - this.currentUnit;
        double remainingPortion = (double)remainingUnits / (double)this.totalUnits;
        double currentEstimate = durationSoFar / (double)this.currentUnit * (double)this.totalUnits;
        long retValue = (long)(((double)(this.initialEstimate * remainingUnits) * remainingPortion + currentEstimate * (double)remainingUnits * (1.0 - remainingPortion)) / (double)this.totalUnits);
        return retValue;
    }

    public double getPercentageDone() {
        return (double)this.currentUnit * 100.0 / (double)this.totalUnits;
    }

    public long getTimeStampStarted() {
        return this.timeStarted;
    }
}

