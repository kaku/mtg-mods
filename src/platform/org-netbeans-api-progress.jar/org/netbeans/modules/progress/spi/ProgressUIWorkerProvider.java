/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.progress.spi;

import org.netbeans.modules.progress.spi.ExtractedProgressUIWorker;
import org.netbeans.modules.progress.spi.ProgressUIWorkerWithModel;

public interface ProgressUIWorkerProvider {
    public ProgressUIWorkerWithModel getDefaultWorker();

    public ExtractedProgressUIWorker getExtractedComponentWorker();
}

