/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.progress.spi;

import javax.swing.JComponent;
import javax.swing.JLabel;
import org.netbeans.modules.progress.spi.ProgressUIWorker;

public interface ExtractedProgressUIWorker
extends ProgressUIWorker {
    public JComponent getProgressComponent();

    public JLabel getMainLabelComponent();

    public JLabel getDetailLabelComponent();
}

