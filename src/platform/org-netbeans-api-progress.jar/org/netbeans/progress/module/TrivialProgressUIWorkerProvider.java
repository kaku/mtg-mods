/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.progress.module;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.netbeans.modules.progress.spi.ExtractedProgressUIWorker;
import org.netbeans.modules.progress.spi.ProgressEvent;
import org.netbeans.modules.progress.spi.ProgressUIWorkerProvider;
import org.netbeans.modules.progress.spi.ProgressUIWorkerWithModel;
import org.netbeans.modules.progress.spi.TaskModel;

public class TrivialProgressUIWorkerProvider
implements ProgressUIWorkerProvider,
ProgressUIWorkerWithModel,
ExtractedProgressUIWorker {
    @Override
    public ProgressUIWorkerWithModel getDefaultWorker() {
        return this;
    }

    @Override
    public ExtractedProgressUIWorker getExtractedComponentWorker() {
        return this;
    }

    @Override
    public void setModel(TaskModel model) {
    }

    @Override
    public void showPopup() {
    }

    @Override
    public void processProgressEvent(ProgressEvent event) {
    }

    @Override
    public void processSelectedProgressEvent(ProgressEvent event) {
    }

    @Override
    public JComponent getProgressComponent() {
        return new JPanel();
    }

    @Override
    public JLabel getMainLabelComponent() {
        return new JLabel();
    }

    @Override
    public JLabel getDetailLabelComponent() {
        return new JLabel();
    }
}

