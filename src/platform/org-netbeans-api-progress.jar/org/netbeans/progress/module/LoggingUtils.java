/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.progress.module;

public class LoggingUtils {
    public static String findCaller() {
        for (StackTraceElement line : Thread.currentThread().getStackTrace()) {
            if (line.getClassName().matches("(java|org[.]netbeans[.](api[.]progress|modules[.]progress|progress[.]module))[.].+")) continue;
            return line.toString();
        }
        return "???";
    }

    private LoggingUtils() {
    }
}

