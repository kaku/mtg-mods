/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.StatusLineElementProvider
 */
package org.netbeans.progress.module;

import java.awt.Component;
import org.netbeans.modules.progress.spi.Controller;
import org.openide.awt.StatusLineElementProvider;

public final class ProgressVisualizerProvider
implements StatusLineElementProvider {
    public Component getStatusLineElement() {
        return Controller.getDefault().getVisualComponent();
    }
}

