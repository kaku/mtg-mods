/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Cancellable
 */
package org.netbeans.api.progress.aggregate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JLabel;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.api.progress.aggregate.ProgressContributor;
import org.netbeans.api.progress.aggregate.ProgressMonitor;
import org.openide.util.Cancellable;

public final class AggregateProgressHandle {
    private static final Logger LOG = Logger.getLogger(AggregateProgressHandle.class.getName());
    private ProgressMonitor monitor;
    private ProgressHandle handle;
    static final int WORKUNITS = 10000;
    private boolean finished;
    private Collection<ProgressContributor> contributors;
    private int current;
    final String displayName;

    AggregateProgressHandle(String displayName, ProgressContributor[] contribs, Cancellable cancellable, Action listAction, boolean systemtask) {
        this.handle = ProgressHandleFactory.createHandle(displayName, cancellable, listAction);
        this.finished = false;
        this.contributors = new ArrayList<ProgressContributor>();
        if (contribs != null) {
            for (int i = 0; i < contribs.length; ++i) {
                this.addContributor(contribs[i]);
            }
        }
        this.displayName = displayName;
    }

    public void start() {
        this.start(-1);
    }

    public synchronized void start(long estimate) {
        this.handle.start(10000, estimate);
        this.current = 0;
    }

    public synchronized void finish() {
        if (this.finished) {
            return;
        }
        this.finished = true;
        this.handle.finish();
    }

    public void suspend(String message) {
        LOG.log(Level.FINE, "{0}: {1}", new Object[]{this.displayName, message});
        this.handle.suspend(message);
    }

    public void setInitialDelay(int millis) {
        this.handle.setInitialDelay(millis);
    }

    public synchronized void addContributor(ProgressContributor contributor) {
        if (this.finished) {
            return;
        }
        int length = this.contributors.size();
        int remainingUnits = 0;
        double completedRatio = 0.0;
        if (length > 0) {
            for (ProgressContributor cont : this.contributors) {
                remainingUnits += cont.getRemainingParentWorkUnits();
                completedRatio += 1.0 - cont.getCompletedRatio();
            }
        } else {
            remainingUnits = 10000;
            completedRatio = 0.0;
        }
        int currentShare = (int)((double)remainingUnits / (completedRatio + 1.0));
        for (ProgressContributor cont : this.contributors) {
            int newshare = (int)((1.0 - cont.getCompletedRatio()) * (double)currentShare);
            remainingUnits -= newshare;
            cont.setAvailableParentWorkUnits(newshare);
        }
        contributor.setAvailableParentWorkUnits(remainingUnits);
        this.contributors.add(contributor);
        contributor.setParent(this);
    }

    int getCurrentProgress() {
        return this.current;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void processContributorStep(ProgressContributor contributor, String message, int delta) {
        AggregateProgressHandle aggregateProgressHandle = this;
        synchronized (aggregateProgressHandle) {
            if (this.finished) {
                return;
            }
            this.current += delta;
            this.handle.progress(message, this.current);
        }
        if (this.monitor != null) {
            this.monitor.progressed(contributor);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void processContributorStart(ProgressContributor contributor, String message) {
        AggregateProgressHandle aggregateProgressHandle = this;
        synchronized (aggregateProgressHandle) {
            if (this.finished) {
                return;
            }
            if (message != null) {
                this.handle.progress(message);
            }
        }
        if (this.monitor != null) {
            this.monitor.started(contributor);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void processContributorFinish(ProgressContributor contributor) {
        AggregateProgressHandle aggregateProgressHandle = this;
        synchronized (aggregateProgressHandle) {
            if (this.finished) {
                return;
            }
            this.contributors.remove(contributor);
            if (this.contributors.size() == 0) {
                this.finish();
            }
        }
        if (this.monitor != null) {
            this.monitor.finished(contributor);
        }
    }

    public void setMonitor(ProgressMonitor monitor) {
        this.monitor = monitor;
    }

    public void setDisplayName(String newDisplayName) {
        this.handle.setDisplayName(newDisplayName);
    }

    JComponent extractComponent() {
        return ProgressHandleFactory.createProgressComponent(this.handle);
    }

    JLabel extractDetailLabel() {
        return ProgressHandleFactory.createDetailLabelComponent(this.handle);
    }

    JLabel extractMainLabel() {
        return ProgressHandleFactory.createMainLabelComponent(this.handle);
    }
}

