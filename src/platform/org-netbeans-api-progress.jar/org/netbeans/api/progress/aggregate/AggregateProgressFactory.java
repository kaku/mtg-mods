/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Cancellable
 */
package org.netbeans.api.progress.aggregate;

import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JLabel;
import org.netbeans.api.progress.aggregate.AggregateProgressHandle;
import org.netbeans.api.progress.aggregate.ProgressContributor;
import org.openide.util.Cancellable;

public final class AggregateProgressFactory {
    private AggregateProgressFactory() {
    }

    public static AggregateProgressHandle createHandle(String displayName, ProgressContributor[] contributors, Cancellable allowToCancel, Action linkOutput) {
        return new AggregateProgressHandle(displayName, contributors, allowToCancel, linkOutput, false);
    }

    public static ProgressContributor createProgressContributor(String trackingId) {
        return new ProgressContributor(trackingId);
    }

    public static AggregateProgressHandle createSystemHandle(String displayName, ProgressContributor[] contributors, Cancellable allowToCancel, Action linkOutput) {
        return new AggregateProgressHandle(displayName, contributors, allowToCancel, linkOutput, true);
    }

    public static JComponent createProgressComponent(AggregateProgressHandle handle) {
        return handle.extractComponent();
    }

    public static JLabel createMainLabelComponent(AggregateProgressHandle handle) {
        return handle.extractMainLabel();
    }

    public static JLabel createDetailLabelComponent(AggregateProgressHandle handle) {
        return handle.extractDetailLabel();
    }
}

