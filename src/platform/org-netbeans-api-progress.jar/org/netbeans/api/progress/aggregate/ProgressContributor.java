/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.progress.aggregate;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.progress.aggregate.AggregateProgressHandle;
import org.netbeans.progress.module.LoggingUtils;

public final class ProgressContributor {
    private static final Logger LOG = Logger.getLogger(ProgressContributor.class.getName());
    private String id;
    private int workunits;
    private int current;
    private int parentUnits;
    private int lastParentedUnit;
    private AggregateProgressHandle parent;

    ProgressContributor(String id) {
        this.id = id;
        this.workunits = 0;
        this.current = 0;
        this.lastParentedUnit = 0;
    }

    public String getTrackingId() {
        return this.id;
    }

    void setParent(AggregateProgressHandle par) {
        this.parent = par;
    }

    int getWorkUnits() {
        return this.workunits;
    }

    int getRemainingParentWorkUnits() {
        return this.parentUnits;
    }

    void setAvailableParentWorkUnits(int newCount) {
        this.parentUnits = newCount;
    }

    double getCompletedRatio() {
        return this.workunits == 0 ? 0.0 : (double)(this.current / this.workunits);
    }

    public void start(int workunits) {
        if (this.parent == null) {
            return;
        }
        this.workunits = workunits;
        this.parent.processContributorStart(this, null);
    }

    public void finish() {
        if (this.parent == null) {
            return;
        }
        if (this.current < this.workunits) {
            this.progress(null, this.workunits);
        }
        this.parent.processContributorFinish(this);
    }

    public void progress(int workunit) {
        this.progress(null, workunit);
    }

    public void progress(String message) {
        this.progress(message, this.current);
    }

    public void progress(String message, int unit) {
        if (this.parent == null) {
            return;
        }
        if (unit < this.current || unit > this.workunits) {
            LOG.log(Level.WARNING, "logged ''{0}'' @{1} <{2} or >{3} in {4} at {5}", new Object[]{message, unit, this.current, this.workunits, this.parent.displayName, LoggingUtils.findCaller()});
            return;
        }
        if (message != null && unit == this.current) {
            this.parent.processContributorStep(this, message, 0);
            return;
        }
        this.current = unit;
        int delta = this.current - this.lastParentedUnit;
        double step = 1.0 / ((double)this.parentUnits / (double)(this.workunits - this.lastParentedUnit));
        if ((double)delta >= step) {
            boolean log;
            int count = (int)((double)delta / step);
            boolean bl = log = this.current < 0 || delta < 0 || count < 0;
            if (log) {
                LOG.log(Level.WARNING, "Progress Contributor before progress: current={0} lastParentedUnit={1}, parentUnits={2}, delta={3}, step={4}", new Object[]{this.current, this.lastParentedUnit, this.parentUnits, delta, step});
            }
            this.lastParentedUnit += (int)((double)count * step);
            this.parentUnits -= count;
            if (log) {
                LOG.log(Level.WARNING, "Progress Contributor after progress: count={0}, new parentUnits={2}, new lastParentedUnits={1}", new Object[]{count, this.lastParentedUnit, this.parentUnits});
            }
            this.parent.processContributorStep(this, message, count);
        }
    }
}

