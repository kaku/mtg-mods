/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.progress.aggregate;

import org.netbeans.api.progress.aggregate.ProgressContributor;

public interface ProgressMonitor {
    public void started(ProgressContributor var1);

    public void finished(ProgressContributor var1);

    public void progressed(ProgressContributor var1);
}

