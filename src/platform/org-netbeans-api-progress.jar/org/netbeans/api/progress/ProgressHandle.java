/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.progress;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JLabel;
import org.netbeans.modules.progress.spi.InternalHandle;

public final class ProgressHandle {
    private static final Logger LOG = Logger.getLogger(ProgressHandle.class.getName());
    private InternalHandle internal;

    ProgressHandle(InternalHandle internal) {
        LOG.fine(internal.getDisplayName());
        this.internal = internal;
    }

    public void start() {
        this.start(0, -1);
    }

    public void start(int workunits) {
        this.start(workunits, -1);
    }

    public void start(int workunits, long estimate) {
        this.internal.start("", workunits, estimate);
    }

    public void switchToIndeterminate() {
        this.internal.toIndeterminate();
    }

    public void suspend(String message) {
        LOG.log(Level.FINE, "{0}: {1}", new Object[]{this.internal.getDisplayName(), message});
        this.internal.toSilent(message);
    }

    public void switchToDeterminate(int workunits) {
        this.internal.toDeterminate(workunits, -1);
    }

    public void switchToDeterminate(int workunits, long estimate) {
        this.internal.toDeterminate(workunits, estimate);
    }

    public void finish() {
        this.internal.finish();
    }

    public void progress(int workunit) {
        this.progress(null, workunit);
    }

    public void progress(String message) {
        this.progress(message, -2);
    }

    public void progress(String message, int workunit) {
        LOG.log(Level.FINE, "{0}: {1}", new Object[]{this.internal.getDisplayName(), message});
        this.internal.progress(message, workunit);
    }

    public void setInitialDelay(int millis) {
        this.internal.setInitialDelay(millis);
    }

    public void setDisplayName(String newDisplayName) {
        LOG.fine(newDisplayName);
        this.internal.requestDisplayNameChange(newDisplayName);
    }

    JComponent extractComponent() {
        return this.internal.extractComponent();
    }

    InternalHandle getInternalHandle() {
        return this.internal;
    }

    JLabel extractDetailLabel() {
        return this.internal.extractDetailLabel();
    }

    JLabel extractMainLabel() {
        return this.internal.extractMainLabel();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    String getDisplayName() {
        InternalHandle internalHandle = this.internal;
        synchronized (internalHandle) {
            return this.internal.getDisplayName();
        }
    }
}

