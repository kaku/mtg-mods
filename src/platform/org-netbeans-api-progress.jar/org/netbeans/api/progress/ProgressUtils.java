/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Cancellable
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.api.progress;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.api.progress.ProgressRunnable;
import org.netbeans.modules.progress.spi.RunOffEDTProvider;
import org.openide.util.Cancellable;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;

public final class ProgressUtils {
    private static final RunOffEDTProvider PROVIDER = ProgressUtils.getProvider();
    private static final int DISPLAY_DIALOG_MS = 9450;
    private static final int DISPLAY_WAIT_CURSOR_MS = 50;

    private ProgressUtils() {
    }

    private static RunOffEDTProvider getProvider() {
        RunOffEDTProvider p = (RunOffEDTProvider)Lookup.getDefault().lookup(RunOffEDTProvider.class);
        return p != null ? p : new Trivial();
    }

    public static void runOffEventDispatchThread(Runnable operation, String operationDescr, AtomicBoolean cancelOperation, boolean waitForCanceled) {
        PROVIDER.runOffEventDispatchThread(operation, operationDescr, cancelOperation, waitForCanceled, 50, 9450);
    }

    public static void runOffEventDispatchThread(Runnable operation, String operationDescr, AtomicBoolean cancelOperation, boolean waitForCanceled, int waitCursorAfter, int dialogAfter) {
        PROVIDER.runOffEventDispatchThread(operation, operationDescr, cancelOperation, waitForCanceled, waitCursorAfter, dialogAfter);
    }

    public static void showProgressDialogAndRun(Runnable operation, ProgressHandle progress, boolean includeDetailLabel) {
        if (PROVIDER instanceof RunOffEDTProvider.Progress) {
            RunOffEDTProvider.Progress p = (RunOffEDTProvider.Progress)PROVIDER;
            p.showProgressDialogAndRun(operation, progress, includeDetailLabel);
        } else {
            PROVIDER.runOffEventDispatchThread(operation, progress.getDisplayName(), new AtomicBoolean(false), false, 0, 0);
        }
    }

    public static void runOffEventThreadWithProgressDialog(Runnable operation, String dialogTitle, ProgressHandle progress, boolean includeDetailLabel, int waitCursorAfter, int dialogAfter) {
        if (PROVIDER instanceof RunOffEDTProvider.Progress2) {
            RunOffEDTProvider.Progress2 p = (RunOffEDTProvider.Progress2)PROVIDER;
            p.runOffEventThreadWithProgressDialog(operation, dialogTitle, progress, includeDetailLabel, waitCursorAfter, dialogAfter);
        } else {
            PROVIDER.runOffEventDispatchThread(operation, progress.getDisplayName(), new AtomicBoolean(false), true, 50, 9450);
        }
    }

    public static void runOffEventThreadWithCustomDialogContent(Runnable operation, String dialogTitle, JPanel content, int waitCursorAfter, int dialogAfter) {
        if (PROVIDER instanceof RunOffEDTProvider.Progress2) {
            RunOffEDTProvider.Progress2 p = (RunOffEDTProvider.Progress2)PROVIDER;
            p.runOffEventThreadWithCustomDialogContent(operation, dialogTitle, content, waitCursorAfter, dialogAfter);
        } else {
            PROVIDER.runOffEventDispatchThread(operation, dialogTitle, new AtomicBoolean(false), true, 50, 9450);
        }
    }

    public static <T> T showProgressDialogAndRun(final ProgressRunnable<T> operation, final String displayName, boolean includeDetailLabel) {
        if (PROVIDER instanceof RunOffEDTProvider.Progress) {
            RunOffEDTProvider.Progress p = (RunOffEDTProvider.Progress)PROVIDER;
            return p.showProgressDialogAndRun(operation, displayName, includeDetailLabel);
        }
        final AtomicReference ref = new AtomicReference();
        PROVIDER.runOffEventDispatchThread(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                ProgressHandle handle = ProgressHandleFactory.createHandle(displayName);
                handle.start();
                handle.switchToIndeterminate();
                try {
                    ref.set(operation.run(handle));
                }
                finally {
                    handle.finish();
                }
            }
        }, displayName, new AtomicBoolean(false), true, 0, 0);
        return (T)ref.get();
    }

    public static void showProgressDialogAndRun(Runnable operation, String displayName) {
        RunnableWrapper wrapper = operation instanceof Cancellable ? new CancellableRunnableWrapper(operation) : new RunnableWrapper(operation);
        ProgressUtils.showProgressDialogAndRun(wrapper, displayName, false);
    }

    public static <T> Future<T> showProgressDialogAndRunLater(final ProgressRunnable<T> operation, final ProgressHandle handle, boolean includeDetailLabel) {
        if (PROVIDER instanceof RunOffEDTProvider.Progress) {
            RunOffEDTProvider.Progress p = (RunOffEDTProvider.Progress)PROVIDER;
            return p.showProgressDialogAndRunLater(operation, handle, includeDetailLabel);
        }
        FutureTask result = new FutureTask(new Callable<T>(){

            @Override
            public T call() throws Exception {
                return operation.run(handle);
            }
        });
        PROVIDER.runOffEventDispatchThread(result, handle.getDisplayName(), new AtomicBoolean(false), true, 0, 0);
        return result;
    }

    private static class Trivial
    implements RunOffEDTProvider {
        private static final RequestProcessor WORKER = new RequestProcessor(ProgressUtils.class.getName());

        private Trivial() {
        }

        @Override
        public void runOffEventDispatchThread(Runnable operation, String operationDescr, AtomicBoolean cancelOperation, boolean waitForCanceled, int waitCursorAfter, int dialogAfter) {
            if (SwingUtilities.isEventDispatchThread()) {
                RequestProcessor.Task t = WORKER.post(operation);
                t.waitFinished();
            } else {
                operation.run();
            }
        }
    }

    private static final class CancellableRunnableWrapper
    extends RunnableWrapper
    implements Cancellable {
        private final Cancellable cancelable;

        CancellableRunnableWrapper(Runnable toRun) {
            super(toRun);
            this.cancelable = (Cancellable)toRun;
        }

        public boolean cancel() {
            return this.cancelable.cancel();
        }
    }

    private static class RunnableWrapper
    implements ProgressRunnable<Void> {
        private final Runnable toRun;

        RunnableWrapper(Runnable toRun) {
            this.toRun = toRun;
        }

        @Override
        public Void run(ProgressHandle handle) {
            this.toRun.run();
            return null;
        }
    }

}

