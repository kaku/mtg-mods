/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Cancellable
 */
package org.netbeans.api.progress;

import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JLabel;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.modules.progress.spi.InternalHandle;
import org.openide.util.Cancellable;

public final class ProgressHandleFactory {
    private ProgressHandleFactory() {
    }

    public static ProgressHandle createHandle(String displayName) {
        return ProgressHandleFactory.createHandle(displayName, null, null);
    }

    public static ProgressHandle createHandle(String displayName, Cancellable allowToCancel) {
        return ProgressHandleFactory.createHandle(displayName, allowToCancel, null);
    }

    public static ProgressHandle createHandle(String displayName, Action linkOutput) {
        return ProgressHandleFactory.createHandle(displayName, null, linkOutput);
    }

    public static ProgressHandle createHandle(String displayName, Cancellable allowToCancel, Action linkOutput) {
        return new ProgressHandle(new InternalHandle(displayName, allowToCancel, true, linkOutput));
    }

    public static JComponent createProgressComponent(ProgressHandle handle) {
        return handle.extractComponent();
    }

    public static JLabel createMainLabelComponent(ProgressHandle handle) {
        return handle.extractMainLabel();
    }

    public static JLabel createDetailLabelComponent(ProgressHandle handle) {
        return handle.extractDetailLabel();
    }

    public static ProgressHandle createSystemHandle(String displayName) {
        return ProgressHandleFactory.createSystemHandle(displayName, null);
    }

    public static ProgressHandle createSystemHandle(String displayName, Cancellable allowToCancel) {
        return new ProgressHandle(new InternalHandle(displayName, allowToCancel, false, null));
    }

    public static ProgressHandle createSystemHandle(String displayName, Cancellable allowToCancel, Action linkOutput) {
        return new ProgressHandle(new InternalHandle(displayName, allowToCancel, false, linkOutput));
    }
}

