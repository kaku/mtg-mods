/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.progress;

import org.netbeans.api.progress.ProgressHandle;

public interface ProgressRunnable<T> {
    public T run(ProgressHandle var1);
}

