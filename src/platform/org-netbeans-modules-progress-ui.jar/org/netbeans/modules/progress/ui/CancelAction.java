/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.progress.spi.Controller
 *  org.netbeans.modules.progress.spi.InternalHandle
 *  org.netbeans.modules.progress.spi.TaskModel
 */
package org.netbeans.modules.progress.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.netbeans.modules.progress.spi.Controller;
import org.netbeans.modules.progress.spi.InternalHandle;
import org.netbeans.modules.progress.spi.TaskModel;

public final class CancelAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        InternalHandle handle = Controller.getDefault().getModel().getSelectedHandle();
        if (handle != null) {
            handle.requestCancel();
        }
    }
}

