/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.progress.spi.InternalHandle
 *  org.netbeans.modules.progress.spi.ProgressEvent
 *  org.netbeans.modules.progress.spi.ProgressUIWorkerWithModel
 *  org.netbeans.modules.progress.spi.TaskModel
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.windows.WindowManager
 */
package org.netbeans.modules.progress.ui;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.JWindow;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.plaf.ButtonUI;
import javax.swing.plaf.ProgressBarUI;
import javax.swing.plaf.basic.BasicButtonUI;
import org.netbeans.modules.progress.spi.InternalHandle;
import org.netbeans.modules.progress.spi.ProgressEvent;
import org.netbeans.modules.progress.spi.ProgressUIWorkerWithModel;
import org.netbeans.modules.progress.spi.TaskModel;
import org.netbeans.modules.progress.ui.ListComponent;
import org.netbeans.modules.progress.ui.NbProgressBar;
import org.netbeans.modules.progress.ui.PopupPane;
import org.netbeans.modules.progress.ui.ProgressListAction;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.windows.WindowManager;

public class StatusLineComponent
extends JPanel
implements ProgressUIWorkerWithModel {
    private NbProgressBar bar;
    private JLabel label;
    private JSeparator separator;
    private InternalHandle handle;
    private boolean showingPopup = false;
    private TaskModel model;
    private MouseListener mouseListener;
    private HideAWTListener hideListener;
    private JWindow popupWindow;
    private PopupPane pane;
    private Map<InternalHandle, ListComponent> handleComponentMap = new HashMap<InternalHandle, ListComponent>();
    private final int preferredHeight;
    private JButton closeButton;
    private JLabel extraLabel;

    public StatusLineComponent() {
        FlowLayout flay = new FlowLayout();
        flay.setVgap(1);
        flay.setHgap(5);
        this.setLayout(flay);
        this.mouseListener = new MListener();
        this.addMouseListener(this.mouseListener);
        this.hideListener = new HideAWTListener();
        this.createLabel();
        this.createBar();
        this.bar.setStringPainted(true);
        this.bar.setString("@@@");
        this.label.setText("@@@");
        this.preferredHeight = Math.max(this.label.getPreferredSize().height, this.bar.getPreferredSize().height) + 3;
        this.setOpaque(false);
        this.discardLabel();
        this.discardBar();
        this.pane = new PopupPane();
        this.pane.setBorder(BorderFactory.createLineBorder(UIManager.getLookAndFeelDefaults().getColor("7-border-color")));
        this.pane.getActionMap().put("HidePopup", new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                StatusLineComponent.this.hidePopup();
            }
        });
        this.pane.getInputMap().put(KeyStroke.getKeyStroke(27, 0), "HidePopup");
        this.pane.getInputMap(1).put(KeyStroke.getKeyStroke(27, 0), "HidePopup");
    }

    private void createLabel() {
        this.discardLabel();
        this.label = new JLabel();
        Color fg = UIManager.getColor("StatusLine.forground");
        if (fg != null) {
            this.label.setForeground(fg);
        }
        this.label.setCursor(Cursor.getPredefinedCursor(12));
        this.label.addMouseListener(this.mouseListener);
    }

    private void discardLabel() {
        if (this.label != null) {
            this.label.removeMouseListener(this.mouseListener);
            this.label = null;
        }
    }

    private void createExtraLabel() {
        this.discardExtraLabel();
        this.extraLabel = new JLabel();
        Color fg = UIManager.getColor("StatusLine.forground");
        if (fg != null) {
            this.extraLabel.setForeground(fg);
        }
        this.extraLabel.setCursor(Cursor.getPredefinedCursor(12));
        this.extraLabel.addMouseListener(this.mouseListener);
    }

    private void discardExtraLabel() {
        if (this.extraLabel != null) {
            this.extraLabel.removeMouseListener(this.mouseListener);
            this.extraLabel = null;
        }
    }

    private void createBar() {
        this.discardBar();
        this.bar = new NbProgressBar();
        this.bar.setUseInStatusBar(true);
        this.bar.setCursor(Cursor.getPredefinedCursor(12));
        this.bar.addMouseListener(this.mouseListener);
    }

    private void discardBar() {
        if (this.bar != null) {
            this.bar.removeMouseListener(this.mouseListener);
            this.bar.getUI().uninstallUI(this.bar);
            this.bar = null;
        }
    }

    private void createCloseButton() {
        this.discardCloseButton();
        this.closeButton = new JButton(){

            @Override
            public void updateUI() {
                this.setUI(new BasicButtonUI());
            }
        };
        this.closeButton.setBorderPainted(false);
        this.closeButton.setBorder(BorderFactory.createEmptyBorder());
        this.closeButton.setOpaque(false);
        this.closeButton.setContentAreaFilled(false);
        Image img = (Image)UIManager.get("nb.progress.cancel.icon");
        if (null != img) {
            this.closeButton.setIcon(new ImageIcon(img));
        }
        if (null != (img = (Image)UIManager.get("nb.progress.cancel.icon.mouseover"))) {
            this.closeButton.setRolloverEnabled(true);
            this.closeButton.setRolloverIcon(new ImageIcon(img));
        }
        if (null != (img = (Image)UIManager.get("nb.progress.cancel.icon.pressed"))) {
            this.closeButton.setPressedIcon(new ImageIcon(img));
        }
    }

    private void setCloseButtonNameAndTooltip() {
        this.closeButton.setName(NbBundle.getMessage(ListComponent.class, (String)"ListComponent.btnClose.name"));
        this.closeButton.setToolTipText(NbBundle.getMessage(ListComponent.class, (String)"ListComponent.btnClose.tooltip"));
    }

    private void discardCloseButton() {
        this.closeButton = null;
    }

    private void createSeparator() {
        this.discardSeparator();
        this.separator = new JSeparator(1);
        this.separator.setBorder(BorderFactory.createEmptyBorder(1, 0, 2, 0));
    }

    private void discardSeparator() {
        this.separator = null;
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension retValue = super.getPreferredSize();
        retValue.height = this.preferredHeight;
        return retValue;
    }

    @Override
    public Dimension getMinimumSize() {
        Dimension retValue = super.getMinimumSize();
        retValue.height = this.preferredHeight;
        return retValue;
    }

    @Override
    public Dimension getMaximumSize() {
        Dimension retValue = super.getMaximumSize();
        retValue.height = this.preferredHeight;
        return retValue;
    }

    public void setModel(TaskModel mod) {
        this.model = mod;
        this.model.addListDataListener((ListDataListener)new Listener());
        this.model.addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent e) {
                StatusLineComponent.this.pane.updateBoldFont(StatusLineComponent.this.model.getSelectedHandle());
            }
        });
    }

    private void setTooltipForAll() {
        int size = this.model.getSize();
        String key = "NbProgressBar.tooltip1";
        if (size == 1) {
            key = "NbProgressBar.tooltip2";
        }
        String text = NbBundle.getMessage(StatusLineComponent.class, (String)key, (Object)size);
        this.setToolTipText(text);
        if (this.label != null) {
            this.label.setToolTipText(text);
        }
        if (this.bar != null) {
            this.bar.setToolTipText(text);
        }
    }

    public void processProgressEvent(ProgressEvent event) {
        if (event.getType() == 0) {
            this.createListItem(event.getSource());
        } else if (event.getType() == 1 || event.getType() == 5 || event.getType() == 6) {
            ListComponent comp = this.handleComponentMap.get((Object)event.getSource());
            if (comp == null) {
                this.createListItem(event.getSource());
                comp = this.handleComponentMap.get((Object)event.getSource());
            }
            comp.processProgressEvent(event);
        } else if (event.getType() == 4) {
            this.removeListItem(event.getSource());
            if (this.model.getSelectedHandle() != null && this.handle != this.model.getSelectedHandle()) {
                ProgressEvent snap = this.model.getSelectedHandle().requestStateSnapshot();
                this.initializeComponent(snap);
                if (snap.getSource().isInSleepMode()) {
                    this.bar.setString(snap.getMessage());
                }
            }
            if (this.model.getSize() == 0 && event.getSource().equals((Object)this.handle)) {
                this.handle = null;
            }
        }
    }

    public void processSelectedProgressEvent(ProgressEvent event) {
        if (event.getType() == 0) {
            this.initializeComponent(event);
            return;
        }
        if (event.getType() == 4) {
            this.hidePopup();
            this.removeAll();
            this.discardSeparator();
            this.discardCloseButton();
            this.discardBar();
            this.discardLabel();
            this.discardExtraLabel();
            this.revalidate();
            this.repaint();
            return;
        }
        if (event.getSource() != this.handle || event.isSwitched() || event.getType() == 6 || event.getSource().isInSleepMode() != (this.bar.getClientProperty("sleepy") != null)) {
            this.initializeComponent(event);
        }
        if (this.bar != null) {
            if (event.getWorkunitsDone() > 0) {
                this.bar.setValue(event.getWorkunitsDone());
            }
            this.bar.setString(StatusLineComponent.getBarString(event.getPercentageDone(), event.getEstimatedCompletion()));
            if (event.getDisplayName() != null) {
                this.label.setText(event.getDisplayName());
            }
            if (event.getSource().isInSleepMode()) {
                this.bar.setString(event.getMessage());
            }
        }
    }

    static String formatEstimate(long estimate) {
        long seconds;
        long minutes;
        return "" + minutes + ((seconds = estimate - (minutes = estimate / 60) * 60) < 10 ? ":0" : ":") + seconds;
    }

    static String getBarString(double percentage, long estimatedCompletion) {
        if (estimatedCompletion != -1) {
            return StatusLineComponent.formatEstimate(estimatedCompletion);
        }
        if (percentage != -1.0) {
            int rounded = (int)Math.round(percentage);
            if (rounded > 100) {
                rounded = 100;
            }
            return "" + rounded + "%";
        }
        return "";
    }

    private void updateExtraLabel() {
        if (this.extraLabel != null) {
            if (this.handleComponentMap.size() > 1) {
                this.extraLabel.setText(NbBundle.getMessage(StatusLineComponent.class, (String)"StatusLineComponent.extra", (Object)(this.handleComponentMap.size() - 1)));
            } else {
                this.extraLabel.setText(null);
            }
        }
    }

    private void initializeComponent(ProgressEvent event) {
        this.handle = event.getSource();
        boolean toShow = false;
        if (this.label == null) {
            this.createLabel();
            this.add(this.label);
            toShow = true;
            this.label.setToolTipText(this.getToolTipText());
        }
        this.label.setText(this.handle.getDisplayName());
        if (this.bar == null) {
            this.createBar();
            this.add(this.bar);
            toShow = true;
            this.bar.setToolTipText(this.getToolTipText());
        }
        NbProgressBar.setupBar(event.getSource(), this.bar);
        if (this.closeButton == null) {
            this.createCloseButton();
            this.add(this.closeButton);
            toShow = true;
        }
        if (this.extraLabel == null) {
            this.createExtraLabel();
            this.updateExtraLabel();
            this.add(this.extraLabel);
            toShow = true;
        }
        if (this.separator == null) {
            this.createSeparator();
            this.add(this.separator);
            toShow = true;
        }
        if (this.handle.isAllowCancel()) {
            this.closeButton.setAction(new CancelAction(false));
        } else {
            this.closeButton.setAction(new EmptyCancelAction());
        }
        this.setCloseButtonNameAndTooltip();
        if (toShow) {
            this.revalidate();
            this.repaint();
        }
    }

    public void hidePopup() {
        if (GraphicsEnvironment.isHeadless()) {
            return;
        }
        if (this.popupWindow != null) {
            this.popupWindow.setVisible(false);
        }
        Toolkit.getDefaultToolkit().removeAWTEventListener(this.hideListener);
        WindowManager.getDefault().getMainWindow().removeWindowStateListener(this.hideListener);
        WindowManager.getDefault().getMainWindow().removeComponentListener(this.hideListener);
        this.showingPopup = false;
    }

    private void createListItem(InternalHandle handle) {
        ListComponent comp;
        if (this.handleComponentMap.containsKey((Object)handle)) {
            comp = this.handleComponentMap.get((Object)handle);
        } else {
            comp = new ListComponent(handle);
            this.handleComponentMap.put(handle, comp);
        }
        this.pane.addListComponent(comp);
        this.pane.updateBoldFont(this.model.getSelectedHandle());
        if (this.showingPopup) {
            this.resizePopup();
        }
        this.updateExtraLabel();
    }

    private void removeListItem(InternalHandle handle) {
        ListComponent c = this.handleComponentMap.remove((Object)handle);
        this.pane.removeListComponent(handle);
        this.pane.updateBoldFont(this.model.getSelectedHandle());
        if (this.showingPopup) {
            this.resizePopup();
        }
        if (c != null) {
            c.clearProgressBarOSX();
        }
        this.updateExtraLabel();
    }

    public void showPopup() {
        if (GraphicsEnvironment.isHeadless()) {
            return;
        }
        if (this.showingPopup) {
            return;
        }
        InternalHandle[] handles = this.model.getHandles();
        if (handles.length == 0) {
            return;
        }
        this.showingPopup = true;
        if (this.popupWindow == null) {
            this.popupWindow = new JWindow(WindowManager.getDefault().getMainWindow());
            this.popupWindow.getContentPane().add(this.pane);
        }
        Toolkit.getDefaultToolkit().addAWTEventListener(this.hideListener, 16);
        WindowManager.getDefault().getMainWindow().addWindowStateListener(this.hideListener);
        WindowManager.getDefault().getMainWindow().addComponentListener(this.hideListener);
        this.resizePopup();
        this.popupWindow.setVisible(true);
        this.pane.requestFocus();
        this.pane.repaint();
    }

    private void resizePopup() {
        this.popupWindow.pack();
        Point point = new Point(0, 0);
        SwingUtilities.convertPointToScreen(point, this);
        Dimension dim = this.popupWindow.getSize();
        Rectangle usableRect = Utilities.getUsableScreenBounds();
        int sepShift = this.separator != null ? this.separator.getSize().width : 0;
        Point loc = new Point(point.x + this.getSize().width - dim.width - sepShift - 10, point.y - dim.height - 5);
        if (loc.x < usableRect.x) {
            loc.x = Math.max(loc.x, usableRect.x);
        }
        if (loc.x + dim.width > usableRect.x + usableRect.width) {
            loc.x = usableRect.x + usableRect.width - dim.width;
        }
        if (!usableRect.contains(loc)) {
            loc = new Point(loc.x, point.y + 5 + this.getSize().height);
        }
        this.popupWindow.setLocation(loc);
    }

    private void showMenu(MouseEvent e) {
        JPopupMenu popup = new JPopupMenu();
        popup.add(new ProgressListAction(NbBundle.getMessage(StatusLineComponent.class, (String)"StatusLineComponent.ShowProcessList")));
        popup.add(new ViewAction());
        popup.add(new CancelAction(true));
        popup.show((Component)e.getSource(), e.getX(), e.getY());
    }

    private class EmptyCancelAction
    extends AbstractAction {
        public EmptyCancelAction() {
            this.setEnabled(false);
            this.putValue("SmallIcon", new Icon(StatusLineComponent.this){
                final /* synthetic */ StatusLineComponent val$this$0;

                @Override
                public int getIconHeight() {
                    return 12;
                }

                @Override
                public int getIconWidth() {
                    return 12;
                }

                @Override
                public void paintIcon(Component c, Graphics g, int x, int y) {
                }
            });
            this.putValue("Name", "");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
        }

    }

    private class ViewAction
    extends AbstractAction {
        public ViewAction() {
            this.putValue("Name", NbBundle.getMessage(StatusLineComponent.class, (String)"StatusLineComponent.View"));
            this.setEnabled(StatusLineComponent.this.handle == null ? false : StatusLineComponent.this.handle.isAllowView());
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (StatusLineComponent.this.handle != null) {
                StatusLineComponent.this.handle.requestView();
            }
        }
    }

    private class CancelAction
    extends AbstractAction {
        public CancelAction(boolean text) {
            if (text) {
                this.putValue("Name", NbBundle.getMessage(StatusLineComponent.class, (String)"StatusLineComponent.Cancel"));
            } else {
                Image icon = (Image)UIManager.get("nb.progress.cancel.icon");
                if (icon == null) {
                    this.putValue("SmallIcon", ImageUtilities.loadImageIcon((String)"org/netbeans/progress/module/resources/buton.png", (boolean)true));
                } else {
                    this.putValue("SmallIcon", new ImageIcon(icon));
                }
            }
            this.setEnabled(StatusLineComponent.this.handle == null ? false : StatusLineComponent.this.handle.isAllowCancel());
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            InternalHandle hndl = StatusLineComponent.this.handle;
            if (hndl != null && hndl.getState() == 1) {
                String message = NbBundle.getMessage(StatusLineComponent.class, (String)"Cancel_Question", (Object)StatusLineComponent.this.handle.getDisplayName());
                String title = NbBundle.getMessage(StatusLineComponent.class, (String)"Cancel_Question_Title");
                NotifyDescriptor dd = new NotifyDescriptor((Object)message, title, 0, 3, null, (Object)null);
                Object retType = DialogDisplayer.getDefault().notify(dd);
                if (retType == NotifyDescriptor.YES_OPTION && hndl.getState() == 1) {
                    hndl.requestCancel();
                }
            }
        }
    }

    private class MListener
    extends MouseAdapter {
        private MListener() {
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getButton() != 1) {
                StatusLineComponent.this.showMenu(e);
            } else if (StatusLineComponent.this.showingPopup) {
                StatusLineComponent.this.hidePopup();
            } else {
                StatusLineComponent.this.showPopup();
            }
        }
    }

    private class HideAWTListener
    extends ComponentAdapter
    implements AWTEventListener,
    WindowStateListener {
        private HideAWTListener() {
        }

        @Override
        public void eventDispatched(AWTEvent aWTEvent) {
            MouseEvent mv;
            if (aWTEvent instanceof MouseEvent && (mv = (MouseEvent)aWTEvent).getClickCount() > 0) {
                if (!(aWTEvent.getSource() instanceof Component)) {
                    return;
                }
                Component comp = (Component)aWTEvent.getSource();
                Container par = SwingUtilities.getAncestorNamed("progresspopup", comp);
                Container barpar = SwingUtilities.getAncestorOfClass(StatusLineComponent.class, comp);
                if (par == null && barpar == null) {
                    StatusLineComponent.this.hidePopup();
                }
            }
        }

        @Override
        public void windowStateChanged(WindowEvent windowEvent) {
            if (StatusLineComponent.this.showingPopup) {
                int oldState = windowEvent.getOldState();
                int newState = windowEvent.getNewState();
                if ((oldState & 1) == 0 && (newState & 1) == 1) {
                    StatusLineComponent.this.hidePopup();
                }
            }
        }

        @Override
        public void componentResized(ComponentEvent evt) {
            if (StatusLineComponent.this.showingPopup) {
                StatusLineComponent.this.resizePopup();
            }
        }

        @Override
        public void componentMoved(ComponentEvent evt) {
            if (StatusLineComponent.this.showingPopup) {
                StatusLineComponent.this.resizePopup();
            }
        }
    }

    private class Listener
    implements ListDataListener {
        private Listener() {
        }

        @Override
        public void intervalAdded(ListDataEvent e) {
            StatusLineComponent.this.setTooltipForAll();
        }

        @Override
        public void intervalRemoved(ListDataEvent e) {
            StatusLineComponent.this.setTooltipForAll();
        }

        @Override
        public void contentsChanged(ListDataEvent e) {
            StatusLineComponent.this.setTooltipForAll();
        }
    }

}

