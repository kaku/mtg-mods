/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.progress.spi.InternalHandle
 *  org.openide.util.Mutex
 */
package org.netbeans.modules.progress.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.border.Border;
import org.netbeans.modules.progress.spi.InternalHandle;
import org.netbeans.modules.progress.ui.ListComponent;
import org.openide.util.Mutex;

public class PopupPane
extends JScrollPane {
    private JPanel view = new JPanel();
    private Set<ListComponent> listComponents = new HashSet<ListComponent>();
    private ListComponent selected;

    public PopupPane() {
        GridLayout grid = new GridLayout(0, 1);
        grid.setHgap(0);
        grid.setVgap(0);
        this.view.setLayout(grid);
        this.view.setBorder(BorderFactory.createEmptyBorder());
        this.setName("progresspopup");
        this.setVerticalScrollBarPolicy(20);
        this.setViewportView(this.view);
        this.setFocusable(true);
        this.setRequestFocusEnabled(true);
        MoveDownAction down = new MoveDownAction();
        this.getActionMap().put("Move-Down", down);
        this.getInputMap().put(KeyStroke.getKeyStroke(40, 0), "Move-Down");
        this.getInputMap(1).put(KeyStroke.getKeyStroke(40, 0), "Move-Down");
        MoveUpAction up = new MoveUpAction();
        this.getActionMap().put("Move-Up", up);
        this.getInputMap().put(KeyStroke.getKeyStroke(38, 0), "Move-Up");
        this.getInputMap(1).put(KeyStroke.getKeyStroke(38, 0), "Move-Up");
        CancelAction cancel = new CancelAction();
        this.getActionMap().put("Cancel-Task", cancel);
        this.getInputMap().put(KeyStroke.getKeyStroke(127, 0), "Cancel-Task");
        this.getInputMap(1).put(KeyStroke.getKeyStroke(127, 0), "Cancel-Task");
        SelectAction select = new SelectAction();
        this.getActionMap().put("select-task", select);
        this.getInputMap().put(KeyStroke.getKeyStroke(32, 0), "select-task");
        this.getInputMap(1).put(KeyStroke.getKeyStroke(32, 0), "select-task");
        this.setHorizontalScrollBarPolicy(31);
    }

    public void addListComponent(final ListComponent lst) {
        Mutex.EVENT.readAccess(new Runnable(){

            @Override
            public void run() {
                PopupPane.this.listComponents.add(lst);
                if (PopupPane.this.view.getComponentCount() > 0) {
                    JComponent previous = (JComponent)PopupPane.this.view.getComponent(PopupPane.this.view.getComponentCount() - 1);
                    previous.setBorder(new BottomLineBorder());
                }
                lst.setBorder(BorderFactory.createEmptyBorder());
                PopupPane.this.view.add(lst);
                if (PopupPane.this.listComponents.size() > 3) {
                    PopupPane.this.setVerticalScrollBarPolicy(22);
                } else {
                    PopupPane.this.setVerticalScrollBarPolicy(21);
                }
            }
        });
    }

    public void removeListComponent(final InternalHandle handle) {
        Mutex.EVENT.readAccess(new Runnable(){

            @Override
            public void run() {
                Iterator it = PopupPane.this.listComponents.iterator();
                while (it.hasNext()) {
                    ListComponent comp = (ListComponent)it.next();
                    if (comp.getHandle() != handle) continue;
                    PopupPane.this.view.remove(comp);
                    it.remove();
                    break;
                }
                if (PopupPane.this.listComponents.size() > 3) {
                    PopupPane.this.setVerticalScrollBarPolicy(22);
                } else {
                    PopupPane.this.setVerticalScrollBarPolicy(21);
                }
            }
        });
    }

    @Override
    public Dimension getPreferredSize() {
        int count = this.view.getComponentCount();
        int height = count > 0 ? this.view.getComponent((int)0).getPreferredSize().height : 0;
        int offset = count > 3 ? height * 3 + 5 : count * height + 5;
        return new Dimension(count > 3 ? 622 : 602, offset);
    }

    public void updateBoldFont(final InternalHandle handle) {
        Mutex.EVENT.readAccess(new Runnable(){

            @Override
            public void run() {
                Iterator iterator = PopupPane.this.listComponents.iterator();
                while (iterator.hasNext()) {
                    ListComponent comp;
                    comp.markAsActive(handle == (comp = (ListComponent)iterator.next()).getHandle());
                }
            }
        });
    }

    private int findIndex(Component comp) {
        Component[] comps = this.view.getComponents();
        for (int i = 0; i < comps.length; ++i) {
            if (comps[i] != comp) continue;
            return i;
        }
        return -1;
    }

    @Override
    public void requestFocus() {
        super.requestFocus();
    }

    private class SelectAction
    extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (PopupPane.this.selected != null) {
                PopupPane.this.selected.getHandle().requestExplicitSelection();
            }
        }
    }

    private class CancelAction
    extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (PopupPane.this.selected != null) {
                Action act = PopupPane.this.selected.getCancelAction();
                if (act != null) {
                    act.actionPerformed(actionEvent);
                } else {
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        }
    }

    private class MoveUpAction
    extends AbstractAction {
        MoveUpAction() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            int index = PopupPane.this.view.getComponentCount();
            if (PopupPane.this.selected != null) {
                index = PopupPane.this.findIndex(PopupPane.this.selected);
            }
            if (--index < 0) {
                index = PopupPane.this.view.getComponentCount() - 1;
            }
            PopupPane.this.selected = (ListComponent)PopupPane.this.view.getComponent(index);
            PopupPane.this.selected.requestFocus();
        }
    }

    private class MoveDownAction
    extends AbstractAction {
        MoveDownAction() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            int index = -1;
            if (PopupPane.this.selected != null) {
                index = PopupPane.this.findIndex(PopupPane.this.selected);
            }
            if (++index >= PopupPane.this.view.getComponentCount()) {
                index = 0;
            }
            PopupPane.this.selected = (ListComponent)PopupPane.this.view.getComponent(index);
            PopupPane.this.selected.requestFocus();
        }
    }

    private static class BottomLineBorder
    implements Border {
        private Insets ins = new Insets(0, 0, 1, 0);
        private Color col = new Color(221, 229, 248);

        @Override
        public Insets getBorderInsets(Component c) {
            return this.ins;
        }

        @Override
        public boolean isBorderOpaque() {
            return false;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            Color old = g.getColor();
            g.setColor(this.col);
            g.drawRect(x, y + height - 2, width, 1);
            g.setColor(old);
        }
    }

}

