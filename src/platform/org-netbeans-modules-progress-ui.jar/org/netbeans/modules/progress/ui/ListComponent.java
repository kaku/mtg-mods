/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.progress.spi.InternalHandle
 *  org.netbeans.modules.progress.spi.ProgressEvent
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.progress.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ButtonUI;
import javax.swing.plaf.ProgressBarUI;
import javax.swing.plaf.basic.BasicButtonUI;
import org.netbeans.modules.progress.spi.InternalHandle;
import org.netbeans.modules.progress.spi.ProgressEvent;
import org.netbeans.modules.progress.ui.NbProgressBar;
import org.netbeans.modules.progress.ui.StatusLineComponent;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class ListComponent
extends JPanel {
    private NbProgressBar bar;
    private JLabel mainLabel;
    private JLabel dynaLabel;
    private JButton closeButton;
    private InternalHandle handle;
    private boolean watched;
    private Action cancelAction;
    private Color selectBgColor;
    private Color selectFgColor;
    private Color bgColor;
    private Color fgColor;
    private int mainHeight;
    private int dynaHeight;
    private static final int UPPERMARGIN = 3;
    private static final int LEFTMARGIN = 2;
    private static final int BOTTOMMARGIN = 2;
    private static final int BETWEENTEXTMARGIN = 3;
    static final int ITEM_WIDTH = 600;

    public ListComponent(InternalHandle hndl) {
        this.setFocusable(true);
        this.setRequestFocusEnabled(true);
        this.mainLabel = new JLabel();
        this.dynaLabel = new JLabel();
        this.setOpaque(true);
        this.dynaLabel.setFont(this.dynaLabel.getFont().deriveFont((float)(this.dynaLabel.getFont().getSize() - 2)));
        this.bar = new NbProgressBar();
        this.handle = hndl;
        Color bg = UIManager.getColor("nbProgressBar.popupText.background");
        if (bg != null) {
            this.setBackground(bg);
            this.mainLabel.setBackground(bg);
            this.dynaLabel.setBackground(bg);
        }
        this.bgColor = this.getBackground();
        Color dynaFg = UIManager.getColor("nbProgressBar.popupDynaText.foreground");
        if (dynaFg != null) {
            this.dynaLabel.setForeground(dynaFg);
        }
        this.fgColor = UIManager.getColor("nbProgressBar.popupText.foreground");
        if (this.fgColor != null) {
            this.mainLabel.setForeground(this.fgColor);
        }
        this.fgColor = this.mainLabel.getForeground();
        this.selectBgColor = UIManager.getColor("nbProgressBar.popupText.selectBackground");
        if (this.selectBgColor == null) {
            this.selectBgColor = UIManager.getColor("List.selectionBackground");
        }
        this.selectFgColor = UIManager.getColor("nbProgressBar.popupText.selectForeground");
        if (this.selectFgColor == null) {
            this.selectFgColor = UIManager.getColor("List.selectionForeground");
        }
        this.bar.setToolTipText(NbBundle.getMessage(ListComponent.class, (String)"ListComponent.bar.tooltip"));
        this.bar.setCursor(Cursor.getPredefinedCursor(12));
        this.mainLabel.setText("XYZ");
        this.dynaLabel.setText("XYZ");
        this.bar.setString("@@@");
        this.mainHeight = Math.max(this.mainLabel.getPreferredSize().height, this.bar.getPreferredSize().height) + 3;
        this.bar.setString(null);
        this.dynaHeight = this.dynaLabel.getPreferredSize().height;
        this.mainLabel.setText(null);
        this.dynaLabel.setText(null);
        this.setLayout(new CustomLayout());
        this.add(this.mainLabel);
        this.add(this.bar);
        MListener list = new MListener();
        if (this.handle.isAllowCancel()) {
            this.cancelAction = new CancelAction(false);
            this.closeButton = new JButton(){

                @Override
                public void updateUI() {
                    this.setUI(new BasicButtonUI());
                }
            };
            this.closeButton.setBorderPainted(false);
            this.closeButton.setBorder(BorderFactory.createEmptyBorder());
            this.closeButton.setOpaque(false);
            this.closeButton.setContentAreaFilled(false);
            this.closeButton.setFocusable(false);
            Image img = (Image)UIManager.get("nb.progress.cancel.icon2");
            if (null != img) {
                this.closeButton.setIcon(new ImageIcon(img));
            }
            if (null != (img = (Image)UIManager.get("nb.progress.cancel.icon.mouseover"))) {
                this.closeButton.setRolloverEnabled(true);
                this.closeButton.setRolloverIcon(new ImageIcon(img));
            }
            if (null != (img = (Image)UIManager.get("nb.progress.cancel.icon.pressed"))) {
                this.closeButton.setPressedIcon(new ImageIcon(img));
            }
            this.closeButton.setToolTipText(NbBundle.getMessage(ListComponent.class, (String)"ListComponent.btnClose.tooltip"));
            this.add(this.closeButton);
            if (this.handle.getState() != 1) {
                this.closeButton.setEnabled(false);
            }
        }
        this.add(this.dynaLabel);
        this.setBorder(BorderFactory.createEmptyBorder());
        this.addMouseListener(list);
        this.bar.addMouseListener(list);
        this.mainLabel.addMouseListener(list);
        this.dynaLabel.addMouseListener(list);
        if (this.handle.isAllowCancel()) {
            this.closeButton.addMouseListener(list);
        }
        this.mainLabel.setText(this.handle.getDisplayName());
        this.mainLabel.setToolTipText(this.mainLabel.getText());
        NbProgressBar.setupBar(this.handle, this.bar);
        this.addFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent e) {
                if (!e.isTemporary()) {
                    ListComponent.this.setBackground(ListComponent.this.selectBgColor);
                    ListComponent.this.mainLabel.setBackground(ListComponent.this.selectBgColor);
                    ListComponent.this.dynaLabel.setBackground(ListComponent.this.selectBgColor);
                    ListComponent.this.mainLabel.setForeground(ListComponent.this.selectFgColor);
                    ListComponent.this.scrollRectToVisible(ListComponent.this.getBounds());
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (!e.isTemporary()) {
                    ListComponent.this.setBackground(ListComponent.this.bgColor);
                    ListComponent.this.mainLabel.setBackground(ListComponent.this.bgColor);
                    ListComponent.this.dynaLabel.setBackground(ListComponent.this.bgColor);
                    ListComponent.this.mainLabel.setForeground(ListComponent.this.fgColor);
                }
            }
        });
    }

    Action getCancelAction() {
        return this.cancelAction;
    }

    InternalHandle getHandle() {
        return this.handle;
    }

    void processProgressEvent(ProgressEvent event) {
        if (event.getType() == 1 || event.getType() == 5 || event.getType() == 6) {
            if (event.getSource() != this.handle) {
                throw new IllegalStateException();
            }
            if (event.isSwitched()) {
                NbProgressBar.setupBar(event.getSource(), this.bar);
            }
            if (event.getWorkunitsDone() > 0) {
                this.bar.setValue(event.getWorkunitsDone());
            }
            this.bar.setString(StatusLineComponent.getBarString(event.getPercentageDone(), event.getEstimatedCompletion()));
            if (event.getMessage() != null) {
                this.dynaLabel.setText(event.getMessage());
            }
            if (event.getSource().getState() == 3) {
                this.closeButton.setEnabled(false);
            }
            if (event.getDisplayName() != null) {
                this.mainLabel.setText(event.getDisplayName());
            }
        } else {
            throw new IllegalStateException();
        }
    }

    void markAsActive(boolean sel) {
        if (sel == this.watched) {
            return;
        }
        this.watched = sel;
        if (sel) {
            this.mainLabel.setFont(this.mainLabel.getFont().deriveFont(1));
        } else {
            this.mainLabel.setFont(this.mainLabel.getFont().deriveFont(0));
        }
        if (this.mainLabel.isVisible()) {
            this.mainLabel.repaint();
        }
    }

    void clearProgressBarOSX() {
        if (this.bar != null) {
            this.bar.getUI().uninstallUI(this.bar);
        }
    }

    private void showMenu(MouseEvent e) {
        if (!this.isShowing()) {
            return;
        }
        JPopupMenu popup = new JPopupMenu();
        popup.setName("progresspopup");
        popup.add(new ViewAction());
        popup.add(new WatchAction());
        popup.add(new CancelAction(true));
        popup.show((Component)e.getSource(), e.getX(), e.getY());
    }

    private class CustomLayout
    implements LayoutManager {
        private CustomLayout() {
        }

        @Override
        public void addLayoutComponent(String name, Component comp) {
        }

        @Override
        public Dimension preferredLayoutSize(Container parent) {
            int height = 3 + ListComponent.this.mainHeight + 3 + ListComponent.this.dynaHeight + 2;
            return new Dimension(600, height);
        }

        @Override
        public void layoutContainer(Container parent) {
            int parentWidth = parent.getWidth();
            int parentHeight = parent.getHeight();
            int offset = parentWidth - 18;
            if (ListComponent.this.closeButton != null) {
                ListComponent.this.closeButton.setBounds(offset, 3, 18, ListComponent.this.mainHeight);
            }
            int barOffset = offset - 200;
            ListComponent.this.bar.setBounds(barOffset, 3, offset - barOffset, ListComponent.this.mainHeight);
            ListComponent.this.mainLabel.setBounds(2, 3, barOffset - 2, ListComponent.this.mainHeight);
            ListComponent.this.dynaLabel.setBounds(2, ListComponent.this.mainHeight + 3 + 3, parentWidth - 2, ListComponent.this.dynaHeight);
        }

        @Override
        public Dimension minimumLayoutSize(Container parent) {
            return this.preferredLayoutSize(parent);
        }

        @Override
        public void removeLayoutComponent(Component comp) {
        }
    }

    private class WatchAction
    extends AbstractAction {
        public WatchAction() {
            this.putValue("Name", NbBundle.getMessage(ListComponent.class, (String)"ListComponent.Watch"));
            this.putValue("AcceleratorKey", KeyStroke.getKeyStroke(32, 0));
            this.setEnabled(true);
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (ListComponent.this.handle != null) {
                ListComponent.this.handle.requestExplicitSelection();
            }
        }
    }

    private class ViewAction
    extends AbstractAction {
        public ViewAction() {
            this.putValue("Name", NbBundle.getMessage(ListComponent.class, (String)"StatusLineComponent.View"));
            this.setEnabled(ListComponent.this.handle == null ? false : ListComponent.this.handle.isAllowView());
            this.putValue("AcceleratorKey", KeyStroke.getKeyStroke(10, 0));
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (ListComponent.this.handle != null) {
                ListComponent.this.handle.requestView();
            }
        }
    }

    private class CancelAction
    extends AbstractAction {
        CancelAction(boolean text) {
            if (text) {
                this.putValue("Name", NbBundle.getMessage(ListComponent.class, (String)"StatusLineComponent.Cancel"));
                this.putValue("AcceleratorKey", KeyStroke.getKeyStroke(127, 0));
            } else {
                Image icon = (Image)UIManager.get("nb.progress.cancel.icon");
                if (icon == null) {
                    icon = ImageUtilities.loadImage((String)"org/netbeans/progress/module/resources/buton.png");
                }
                this.putValue("SmallIcon", new ImageIcon(icon));
            }
            this.setEnabled(ListComponent.this.handle == null ? false : ListComponent.this.handle.isAllowCancel());
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (ListComponent.this.handle.getState() == 1) {
                String message = NbBundle.getMessage(ListComponent.class, (String)"Cancel_Question", (Object)ListComponent.this.handle.getDisplayName());
                String title = NbBundle.getMessage(ListComponent.class, (String)"Cancel_Question_Title");
                NotifyDescriptor dd = new NotifyDescriptor((Object)message, title, 0, 3, null, (Object)null);
                Object retType = DialogDisplayer.getDefault().notify(dd);
                if (retType == NotifyDescriptor.YES_OPTION) {
                    ListComponent.this.handle.requestCancel();
                }
            }
        }
    }

    private class MListener
    extends MouseAdapter {
        private MListener() {
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getSource() == ListComponent.this.bar) {
                ListComponent.this.handle.requestExplicitSelection();
            }
            if (e.getClickCount() > 1 && (e.getSource() == ListComponent.this.mainLabel || e.getSource() == ListComponent.this.dynaLabel)) {
                ListComponent.this.handle.requestView();
            }
            if (e.getButton() != 1) {
                ListComponent.this.showMenu(e);
            } else {
                ListComponent.this.requestFocus();
            }
        }
    }

}

