/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.netbeans.api.progress.ProgressRunnable
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.modules.progress.spi.RunOffEDTProvider
 *  org.netbeans.modules.progress.spi.RunOffEDTProvider$Progress
 *  org.netbeans.modules.progress.spi.RunOffEDTProvider$Progress2
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.util.Cancellable
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.windows.WindowManager
 */
package org.netbeans.modules.progress.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.LayoutManager;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.RootPaneContainer;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.api.progress.ProgressRunnable;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.modules.progress.spi.RunOffEDTProvider;
import org.netbeans.modules.progress.ui.AbstractWindowRunner;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.Cancellable;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;
import org.openide.windows.WindowManager;

public class RunOffEDTImpl
implements RunOffEDTProvider,
RunOffEDTProvider.Progress,
RunOffEDTProvider.Progress2 {
    private static final RequestProcessor WORKER = new RequestProcessor(ProgressUtils.class.getName());
    private static final RequestProcessor TI_WORKER = new RequestProcessor("TI_" + ProgressUtils.class.getName(), 1, true);
    private static final Map<String, Long> CUMULATIVE_SPENT_TIME = new HashMap<String, Long>();
    private static final Map<String, Long> MAXIMAL_SPENT_TIME = new HashMap<String, Long>();
    private static final Map<String, Integer> INVOCATION_COUNT = new HashMap<String, Integer>();
    private static final int CANCEL_TIME = 1000;
    private static final int WARNING_TIME = Integer.getInteger("org.netbeans.modules.progress.ui.WARNING_TIME", 10000);
    private static final Logger LOG = Logger.getLogger(RunOffEDTImpl.class.getName());
    private final boolean assertionsOn;

    public void runOffEventDispatchThread(Runnable operation, String operationDescr, AtomicBoolean cancelOperation, boolean waitForCanceled, int waitCursorTime, int dlgTime) {
        Parameters.notNull((CharSequence)"operation", (Object)operation);
        Parameters.notNull((CharSequence)"cancelOperation", (Object)cancelOperation);
        if (!SwingUtilities.isEventDispatchThread()) {
            operation.run();
            return;
        }
        long startTime = System.currentTimeMillis();
        this.runOffEventDispatchThreadImpl(operation, operationDescr, cancelOperation, waitForCanceled, waitCursorTime, dlgTime);
        long elapsed = System.currentTimeMillis() - startTime;
        if (this.assertionsOn) {
            Integer count;
            String clazz = operation.getClass().getName();
            Long cumulative = CUMULATIVE_SPENT_TIME.get(clazz);
            if (cumulative == null) {
                cumulative = 0;
            }
            cumulative = cumulative + elapsed;
            CUMULATIVE_SPENT_TIME.put(clazz, cumulative);
            Long maximal = MAXIMAL_SPENT_TIME.get(clazz);
            if (maximal == null) {
                maximal = 0;
            }
            if (elapsed > maximal) {
                maximal = elapsed;
                MAXIMAL_SPENT_TIME.put(clazz, maximal);
            }
            if ((count = INVOCATION_COUNT.get(clazz)) == null) {
                count = 0;
            }
            Integer n = count;
            Integer n2 = count = Integer.valueOf(count + 1);
            INVOCATION_COUNT.put(clazz, count);
            if (elapsed > (long)WARNING_TIME) {
                LOG.log(Level.WARNING, "Lengthy operation: {0}:{1}:{2}:{3}:{4}", new Object[]{clazz, cumulative, count, maximal, String.format("%3.2f", (double)cumulative.longValue() / (double)count.intValue())});
            }
        }
    }

    private void runOffEventDispatchThreadImpl(final Runnable operation, String operationDescr, final AtomicBoolean cancelOperation, boolean waitForCanceled, int waitCursorTime, int dlgTime) {
        final CountDownLatch latch = new CountDownLatch(1);
        final AtomicReference<Dialog> d = new AtomicReference<Dialog>();
        WORKER.post(new Runnable(){

            @Override
            public void run() {
                if (cancelOperation.get()) {
                    return;
                }
                try {}
                catch (Throwable var1_1) {
                    latch.countDown();
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            Dialog dd = (Dialog)d.get();
                            if (dd != null) {
                                dd.setVisible(false);
                                dd.dispose();
                            }
                        }
                    });
                    throw var1_1;
                }
                operation.run();
                latch.countDown();
                SwingUtilities.invokeLater(new );
            }

        });
        Window window = null;
        Component glassPane = null;
        Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (focusOwner != null && (window = SwingUtilities.getWindowAncestor(focusOwner)) != null) {
            RootPaneContainer root = (RootPaneContainer)((Object)SwingUtilities.getAncestorOfClass(RootPaneContainer.class, focusOwner));
            glassPane = root.getGlassPane();
        }
        if (window == null || glassPane == null) {
            window = WindowManager.getDefault().getMainWindow();
            glassPane = ((JFrame)window).getGlassPane();
        }
        if (RunOffEDTImpl.waitMomentarily(glassPane, null, waitCursorTime, latch, window)) {
            return;
        }
        Cursor wait = Cursor.getPredefinedCursor(3);
        if (RunOffEDTImpl.waitMomentarily(glassPane, wait, dlgTime, latch, window)) {
            return;
        }
        String title = NbBundle.getMessage(RunOffEDTImpl.class, (String)"RunOffAWT.TITLE_Operation");
        String cancelButton = NbBundle.getMessage(RunOffEDTImpl.class, (String)"RunOffAWT.BTN_Cancel");
        DialogDescriptor nd = new DialogDescriptor((Object)operationDescr, title, true, new Object[]{cancelButton}, (Object)cancelButton, 0, null, new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                cancelOperation.set(true);
                ((Dialog)d.get()).setVisible(false);
                ((Dialog)d.get()).dispose();
            }
        });
        nd.setMessageType(1);
        d.set(DialogDisplayer.getDefault().createDialog(nd));
        ((Dialog)d.get()).setVisible(true);
        if (waitForCanceled) {
            try {
                if (!latch.await(1000, TimeUnit.MILLISECONDS)) {
                    throw new IllegalStateException("Canceled operation did not finish in time.");
                }
            }
            catch (InterruptedException ex) {
                LOG.log(Level.FINE, null, ex);
            }
        }
    }

    public void runOffEventThreadWithCustomDialogContent(Runnable operation, String dialogTitle, JPanel content, int waitCursorAfter, int dialogAfter) {
        this.runOffEventThreadCustomDialogImpl(operation, dialogTitle, content, waitCursorAfter, dialogAfter);
    }

    public void runOffEventThreadWithProgressDialog(Runnable operation, String operationDescr, ProgressHandle handle, boolean includeDetailLabel, int waitCursorAfter, int dialogAfter) {
        JPanel content = RunOffEDTImpl.contentPanel(handle, includeDetailLabel);
        this.runOffEventThreadCustomDialogImpl(operation, operationDescr, content, waitCursorAfter, dialogAfter);
    }

    private void runOffEventThreadCustomDialogImpl(final Runnable operation, String operationDescr, JPanel contentPanel, int waitCursorAfter, int dialogAfter) {
        if (waitCursorAfter < 0) {
            waitCursorAfter = 1000;
        }
        if (dialogAfter < 0) {
            dialogAfter = 2000;
        }
        final CountDownLatch latch = new CountDownLatch(1);
        final AtomicReference<Dialog> d = new AtomicReference<Dialog>();
        AtomicReference<RequestProcessor.Task> t = new AtomicReference<RequestProcessor.Task>();
        JDialog dialog = RunOffEDTImpl.createModalDialog(operation, operationDescr, contentPanel, d, t, operation instanceof Cancellable);
        RequestProcessor.Task rt = TI_WORKER.post(new Runnable(){

            @Override
            public void run() {
                try {}
                catch (Throwable var1_1) {
                    latch.countDown();
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            Dialog dd = (Dialog)d.get();
                            if (dd != null) {
                                dd.setVisible(false);
                                dd.dispose();
                            }
                        }
                    });
                    throw var1_1;
                }
                operation.run();
                latch.countDown();
                SwingUtilities.invokeLater(new );
            }

        });
        t.set(rt);
        Window window = null;
        Component glassPane = null;
        Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (focusOwner != null && (window = SwingUtilities.getWindowAncestor(focusOwner)) != null) {
            RootPaneContainer root = (RootPaneContainer)((Object)SwingUtilities.getAncestorOfClass(RootPaneContainer.class, focusOwner));
            glassPane = root.getGlassPane();
        }
        if (window == null || glassPane == null) {
            window = WindowManager.getDefault().getMainWindow();
            glassPane = ((JFrame)window).getGlassPane();
        }
        if (RunOffEDTImpl.waitMomentarily(glassPane, null, waitCursorAfter, latch, window)) {
            return;
        }
        Cursor wait = Cursor.getPredefinedCursor(3);
        if (RunOffEDTImpl.waitMomentarily(glassPane, wait, dialogAfter, latch, window)) {
            return;
        }
        d.set(dialog);
        if (EventQueue.isDispatchThread()) {
            EventQueue.invokeLater(new Runnable(){

                @Override
                public void run() {
                    ((Dialog)d.get()).setVisible(true);
                }
            });
        } else {
            d.get().setVisible(true);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static boolean waitMomentarily(Component glassPane, Cursor wait, int timeout, CountDownLatch l, Window window) {
        Cursor originalWindow = window.getCursor();
        Cursor originalGlass = glassPane.getCursor();
        try {
            if (wait != null) {
                window.setCursor(wait);
                glassPane.setCursor(wait);
            }
            glassPane.setVisible(true);
            try {
                boolean bl = l.await(timeout, TimeUnit.MILLISECONDS);
                return bl;
            }
            catch (InterruptedException ex) {
                LOG.log(Level.FINE, null, ex);
                boolean bl = true;
                glassPane.setVisible(false);
                window.setCursor(originalWindow);
                glassPane.setCursor(originalGlass);
                return bl;
            }
        }
        finally {
            glassPane.setVisible(false);
            window.setCursor(originalWindow);
            glassPane.setCursor(originalGlass);
        }
    }

    public RunOffEDTImpl() {
        boolean ea = false;
        if (!$assertionsDisabled) {
            ea = true;
            if (!true) {
                throw new AssertionError();
            }
        }
        this.assertionsOn = ea;
    }

    public <T> Future<T> showProgressDialogAndRunLater(ProgressRunnable<T> operation, ProgressHandle handle, boolean includeDetailLabel) {
        ProgressBackgroundRunner<T> wr = new ProgressBackgroundRunner<T>(operation, handle, includeDetailLabel, operation instanceof Cancellable);
        Future<T> result = wr.start();
        assert (EventQueue.isDispatchThread() == (result != null));
        if (result == null) {
            try {
                result = wr.waitForStart();
            }
            catch (InterruptedException ex) {
                LOG.log(Level.FINE, "Interrupted/cancelled during start {0}", operation);
                LOG.log(Level.FINER, "Interrupted/cancelled during start", ex);
                return null;
            }
        }
        return result;
    }

    public <T> T showProgressDialogAndRun(ProgressRunnable<T> toRun, String displayName, boolean includeDetailLabel) {
        try {
            return this.showProgressDialogAndRunLater(toRun, toRun instanceof Cancellable ? ProgressHandleFactory.createHandle((String)displayName, (Cancellable)((Cancellable)toRun)) : ProgressHandleFactory.createHandle((String)displayName), includeDetailLabel).get();
        }
        catch (InterruptedException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (CancellationException ex) {
            LOG.log(Level.FINER, "Cancelled " + toRun, ex);
        }
        catch (ExecutionException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        return null;
    }

    public void showProgressDialogAndRun(Runnable toRun, ProgressHandle handle, boolean includeDetailLabel) {
        boolean showCancelButton = toRun instanceof Cancellable;
        ProgressBackgroundRunner wr = new ProgressBackgroundRunner(toRun, handle, includeDetailLabel, showCancelButton);
        wr.start();
        try {
            try {
                wr.waitForStart().get();
            }
            catch (CancellationException ex) {
                LOG.log(Level.FINER, "Cancelled " + toRun, ex);
            }
            catch (ExecutionException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        catch (InterruptedException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    private static JPanel contentPanel(ProgressHandle handle, boolean includeDetail) {
        JPanel contentPanel = new JPanel(new GridBagLayout());
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = new Insets(5, 5, 0, 0);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 21;
        JLabel mainLabel = ProgressHandleFactory.createMainLabelComponent((ProgressHandle)handle);
        Font f = mainLabel.getFont();
        if (f != null) {
            mainLabel.setFont(f.deriveFont(1));
        }
        contentPanel.add((Component)mainLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = new Insets(5, 5, 0, 0);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        JComponent progressBar = ProgressHandleFactory.createProgressComponent((ProgressHandle)handle);
        contentPanel.add((Component)progressBar, gridBagConstraints);
        if (includeDetail) {
            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.insets = new Insets(5, 5, 0, 0);
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = 2;
            gridBagConstraints.anchor = 21;
            JLabel details = ProgressHandleFactory.createDetailLabelComponent((ProgressHandle)handle);
            contentPanel.add((Component)details, gridBagConstraints);
        }
        JPanel emptyPanel = new JPanel();
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = includeDetail ? 3 : 2;
        gridBagConstraints.weighty = 2.0;
        gridBagConstraints.weightx = 2.0;
        gridBagConstraints.fill = 1;
        contentPanel.add((Component)emptyPanel, gridBagConstraints);
        return contentPanel;
    }

    private static JDialog createModalDialog(final Runnable operation, String title, JPanel content, final AtomicReference<Dialog> d, final AtomicReference<RequestProcessor.Task> task, boolean cancelAvail) {
        assert (EventQueue.isDispatchThread());
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.fill = 1;
        panel.add((Component)content, gridBagConstraints);
        if (cancelAvail) {
            JPanel buttonsPanel = new JPanel();
            buttonsPanel.setLayout(new FlowLayout(2));
            String cancelButton = NbBundle.getMessage(RunOffEDTImpl.class, (String)"RunOffAWT.BTN_Cancel");
            JButton cancel = new JButton(cancelButton);
            cancel.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    if (operation instanceof Cancellable) {
                        ((Cancellable)operation).cancel();
                        ((RequestProcessor.Task)task.get()).cancel();
                        ((Dialog)d.get()).setVisible(false);
                        ((Dialog)d.get()).dispose();
                    }
                }
            });
            buttonsPanel.add(cancel);
            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = 1;
            gridBagConstraints.fill = 2;
            gridBagConstraints.weightx = 1.0;
            panel.add((Component)buttonsPanel, gridBagConstraints);
        }
        Frame mainWindow = WindowManager.getDefault().getMainWindow();
        JDialog result = new JDialog(mainWindow, title, true);
        result.setDefaultCloseOperation(0);
        result.setSize(400, 150);
        result.setContentPane(panel);
        result.setLocationRelativeTo(WindowManager.getDefault().getMainWindow());
        return result;
    }

    private static final class ProgressBackgroundRunner<T>
    extends AbstractWindowRunner<T>
    implements Cancellable {
        private final ProgressRunnable<T> toRun;

        ProgressBackgroundRunner(ProgressRunnable<T> toRun, String displayName, boolean includeDetail, boolean showCancel) {
            super(showCancel ? ProgressHandleFactory.createHandle((String)displayName, (Cancellable)((Cancellable)toRun)) : ProgressHandleFactory.createHandle((String)displayName), includeDetail, showCancel);
            this.toRun = toRun;
        }

        ProgressBackgroundRunner(ProgressRunnable<T> toRun, ProgressHandle handle, boolean includeDetail, boolean showCancel) {
            super(handle, includeDetail, showCancel);
            this.toRun = toRun;
        }

        ProgressBackgroundRunner(Runnable toRun, ProgressHandle handle, boolean includeDetail, boolean showCancel) {
            this(showCancel ? new CancellableRunnablePR(toRun) : new RunnablePR(toRun), handle, includeDetail, showCancel);
        }

        @Override
        protected T runBackground() {
            Object result;
            this.handle.start();
            this.handle.switchToIndeterminate();
            try {
                result = this.toRun.run(this.handle);
            }
            finally {
                this.handle.finish();
            }
            return (T)result;
        }

        public boolean cancel() {
            if (this.toRun instanceof Cancellable) {
                return ((Cancellable)this.toRun).cancel();
            }
            return false;
        }

        private static final class CancellableRunnablePR<T>
        extends RunnablePR<T>
        implements Cancellable {
            CancellableRunnablePR(Runnable toRun) {
                super(toRun);
            }

            public boolean cancel() {
                return ((Cancellable)this.toRun).cancel();
            }
        }

        private static class RunnablePR<T>
        implements ProgressRunnable<T> {
            protected final Runnable toRun;

            RunnablePR(Runnable toRun) {
                this.toRun = toRun;
            }

            public T run(ProgressHandle handle) {
                this.toRun.run();
                return null;
            }
        }

    }

    static final class TranslucentMask
    extends JComponent {
        private static final String PROGRESS_WINDOW_MASK_COLOR = "progress.windowMaskColor";

        TranslucentMask() {
            this.setVisible(false);
        }

        @Override
        public boolean isOpaque() {
            return false;
        }

        @Override
        public void paint(Graphics g) {
            Graphics2D g2d = (Graphics2D)g;
            Color translu = UIManager.getColor("progress.windowMaskColor");
            if (translu == null) {
                translu = new Color(180, 180, 180, 148);
            }
            g2d.setColor(translu);
            g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
        }
    }

    static class CancellableFutureTask<T>
    extends FutureTask<T>
    implements Cancellable {
        volatile RequestProcessor.Task task;
        private final Callable<T> c;

        CancellableFutureTask(Callable<T> c) {
            super(c);
            this.c = c;
        }

        public boolean cancel() {
            return this.cancel(true);
        }

        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
            boolean result = this.c instanceof Cancellable ? ((Cancellable)this.c).cancel() : false;
            return result &= FutureTask.super.cancel(mayInterruptIfRunning) & this.task.cancel();
        }

        public String toString() {
            return Object.super.toString() + "[" + this.c + "]";
        }
    }

}

