/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.progress.spi.Controller
 *  org.netbeans.modules.progress.spi.ProgressUIWorkerWithModel
 *  org.netbeans.modules.progress.spi.TaskModel
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.progress.ui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import org.netbeans.modules.progress.spi.Controller;
import org.netbeans.modules.progress.spi.ProgressUIWorkerWithModel;
import org.netbeans.modules.progress.spi.TaskModel;
import org.openide.util.NbBundle;

public class ProgressListAction
extends AbstractAction
implements ListDataListener,
Runnable {
    public ProgressListAction() {
        this(NbBundle.getMessage(ProgressListAction.class, (String)"CTL_ProcessListAction"));
    }

    public ProgressListAction(String name) {
        this.putValue("Name", name);
        Controller.getDefault().getModel().addListDataListener((ListDataListener)this);
        this.updateEnabled();
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        SwingUtilities.invokeLater(this);
    }

    @Override
    public void run() {
        ((ProgressUIWorkerWithModel)Controller.getDefault().getVisualComponent()).showPopup();
    }

    private void updateEnabled() {
        this.setEnabled(Controller.getDefault().getModel().getSize() != 0);
    }

    @Override
    public void contentsChanged(ListDataEvent listDataEvent) {
        this.updateEnabled();
    }

    @Override
    public void intervalAdded(ListDataEvent listDataEvent) {
        this.updateEnabled();
    }

    @Override
    public void intervalRemoved(ListDataEvent listDataEvent) {
        this.updateEnabled();
    }
}

