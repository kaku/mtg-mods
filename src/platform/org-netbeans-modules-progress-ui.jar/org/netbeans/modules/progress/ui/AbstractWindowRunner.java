/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.windows.WindowManager
 */
package org.netbeans.modules.progress.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ButtonUI;
import javax.swing.plaf.basic.BasicButtonUI;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.modules.progress.ui.RunOffEDTImpl;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.windows.WindowManager;

abstract class AbstractWindowRunner<T>
extends WindowAdapter
implements Runnable,
Callable<T> {
    private volatile JDialog dlg;
    private final boolean includeDetail;
    protected final ProgressHandle handle;
    private final CountDownLatch latch = new CountDownLatch(1);
    private volatile T operationResult;
    private final CountDownLatch startLatch = new CountDownLatch(1);
    private final CountDownLatch waitForTaskAssignment = new CountDownLatch(1);
    private RunOffEDTImpl.CancellableFutureTask<T> future;
    private final boolean showCancel;
    private static final RequestProcessor RP = new RequestProcessor(AbstractWindowRunner.class.getName(), 10);
    boolean isDispatchThread;
    private static final String OS_NAME = System.getProperty("os.name");
    protected static final String _OS_NAME = OS_NAME.toLowerCase();
    private static final boolean isWindows = _OS_NAME.startsWith("windows");
    private static final boolean isMac = _OS_NAME.startsWith("mac");
    private static final boolean isLinux = _OS_NAME.startsWith("linux");
    private Component oldGlassPane;

    AbstractWindowRunner(ProgressHandle handle, boolean includeDetail, boolean showCancel) {
        this.includeDetail = includeDetail;
        this.handle = handle;
        this.showCancel = showCancel;
    }

    @Override
    public T call() throws Exception {
        try {
            T t = this.runBackground();
            return t;
        }
        finally {
            EventQueue.invokeLater(this);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Future<T> task() {
        RunOffEDTImpl.CancellableFutureTask<T> result;
        ProgressHandle progressHandle = this.handle;
        synchronized (progressHandle) {
            result = this.future;
        }
        return result;
    }

    Future<T> waitForStart() throws InterruptedException {
        Future<T> result = this.task();
        if (!EventQueue.isDispatchThread() && result == null) {
            this.startLatch.await();
            result = this.task();
        }
        assert (result != null);
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public final void windowOpened(WindowEvent e) {
        this.dlg = (JDialog)e.getSource();
        if (!this.isDispatchThread) {
            this.createTask();
        }
        AbstractWindowRunner abstractWindowRunner = this;
        synchronized (abstractWindowRunner) {
            RunOffEDTImpl.CancellableFutureTask<T> f = this.future;
        }
        this.waitForTaskAssignment.countDown();
        this.grayOutMainWindow();
        f.task.schedule(0);
        this.startLatch.countDown();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Future<T> createTask() {
        RunOffEDTImpl.CancellableFutureTask ft = new RunOffEDTImpl.CancellableFutureTask(this);
        ft.task = RP.create(ft);
        ProgressHandle progressHandle = this.handle;
        synchronized (progressHandle) {
            this.future = ft;
        }
        return ft;
    }

    @Override
    public final void windowClosed(WindowEvent e) {
        this.ungrayMainWindow();
        this.latch.countDown();
    }

    final void await() throws InterruptedException {
        this.latch.await();
    }

    Future<T> start() {
        if (EventQueue.isDispatchThread()) {
            this.isDispatchThread = true;
            Future<T> task = this.createTask();
            this.dlg = this.createModalProgressDialog(this.handle, this.includeDetail);
            this.dlg.setVisible(true);
            return task;
        }
        CountDownLatch dlgLatch = new CountDownLatch(1);
        DialogCreator dc = new DialogCreator(dlgLatch);
        EventQueue.invokeLater(dc);
        try {
            dlgLatch.await();
        }
        catch (InterruptedException ex) {
            throw new IllegalStateException(ex);
        }
        return null;
    }

    protected abstract T runBackground();

    T getResult() {
        return this.operationResult;
    }

    @Override
    public void run() {
        if (!EventQueue.isDispatchThread()) {
            try {
                try {
                    this.waitForTaskAssignment.await();
                }
                catch (InterruptedException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
                this.operationResult = this.runBackground();
            }
            finally {
                EventQueue.invokeLater(this);
            }
        }
        this.dlg.setVisible(false);
        this.dlg.dispose();
    }

    private JDialog createModalProgressDialog(ProgressHandle handle, boolean includeDetail) {
        assert (EventQueue.isDispatchThread());
        int edgeGap = Utilities.isMac() ? 12 : 8;
        int compGap = Utilities.isMac() ? 9 : 5;
        JPanel panel = new JPanel(new GridLayout(includeDetail ? 3 : 2, 1, compGap, compGap));
        JLabel mainLabel = ProgressHandleFactory.createMainLabelComponent((ProgressHandle)handle);
        Font f = mainLabel.getFont();
        if (f != null) {
            mainLabel.setFont(f.deriveFont(1));
        }
        panel.add(mainLabel);
        JComponent progressBar = ProgressHandleFactory.createProgressComponent((ProgressHandle)handle);
        progressBar.setMinimumSize(new Dimension(400, 32));
        JPanel progressPanel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = 17;
        gbc.fill = 1;
        gbc.ipady = 18;
        progressPanel.add((Component)progressBar, gbc);
        if (this.showCancel) {
            final JButton closeButton = new JButton(){

                @Override
                public void updateUI() {
                    this.setUI(new BasicButtonUI());
                }
            };
            gbc.gridx = 1;
            gbc.weightx = 0.0;
            gbc.anchor = 13;
            gbc.fill = 0;
            gbc.insets = new Insets(0, Utilities.isMac() ? 12 : 5, 0, 0);
            progressPanel.add((Component)closeButton, gbc);
            closeButton.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    AbstractWindowRunner.this.task().cancel(true);
                }
            });
            closeButton.setBorderPainted(false);
            closeButton.setBorder(BorderFactory.createEmptyBorder());
            closeButton.setOpaque(false);
            closeButton.setContentAreaFilled(false);
            Image img = (Image)UIManager.get("nb.progress.cancel.icon");
            if (null != img) {
                closeButton.setIcon(ImageUtilities.image2Icon((Image)img));
            } else {
                closeButton.setText(NbBundle.getMessage(AbstractWindowRunner.class, (String)"ModalDialog.btnClose.text"));
            }
            img = (Image)UIManager.get("nb.progress.cancel.icon.mouseover");
            if (null != img) {
                closeButton.setRolloverEnabled(true);
                closeButton.setRolloverIcon(ImageUtilities.image2Icon((Image)img));
            }
            if (null != (img = (Image)UIManager.get("nb.progress.cancel.icon.pressed"))) {
                closeButton.setPressedIcon(ImageUtilities.image2Icon((Image)img));
            }
            closeButton.setToolTipText(NbBundle.getMessage(AbstractWindowRunner.class, (String)"ModalDialog.btnClose.tooltip"));
            closeButton.getAccessibleContext().setAccessibleName(NbBundle.getMessage(AbstractWindowRunner.class, (String)"ModalDialog.btnClose.accessibleName"));
            closeButton.getAccessibleContext().setAccessibleName(NbBundle.getMessage(AbstractWindowRunner.class, (String)"ModalDialog.btnClose.accessibleDescription"));
            panel.getInputMap(2).put(KeyStroke.getKeyStroke(27, 0), "cancel");
            panel.getActionMap().put("cancel", new AbstractAction(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    closeButton.doClick();
                }
            });
        }
        panel.add(progressPanel);
        if (includeDetail) {
            JLabel details = ProgressHandleFactory.createDetailLabelComponent((ProgressHandle)handle);
            details.setMinimumSize(new Dimension(300, 16));
            panel.add(details);
        }
        panel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(UIManager.getLookAndFeelDefaults().getColor("darculaMod.borderColor")), BorderFactory.createEmptyBorder(edgeGap, edgeGap, edgeGap, edgeGap)));
        panel.setMinimumSize(new Dimension(400, 100));
        Frame mainWindow = WindowManager.getDefault().getMainWindow();
        final JDialog result = new JDialog(mainWindow, true);
        result.setDefaultCloseOperation(0);
        if (!AbstractWindowRunner.useCustomLafFrameDecorations(false)) {
            result.setUndecorated(true);
        } else {
            result.getRootPane().setWindowDecorationStyle(0);
        }
        result.setSize(400, 100);
        result.getContentPane().setLayout(new BorderLayout());
        result.getContentPane().add((Component)panel, "Center");
        result.pack();
        int reqWidth = result.getWidth();
        result.setSize(Math.max(reqWidth, mainWindow instanceof JFrame ? ((JFrame)mainWindow).getContentPane().getWidth() / 3 : mainWindow.getWidth()), result.getHeight());
        result.setLocationRelativeTo(WindowManager.getDefault().getMainWindow());
        result.addWindowListener(this);
        if (EventQueue.isDispatchThread()) {
            EventQueue.invokeLater(new Runnable(){

                @Override
                public void run() {
                    result.setVisible(true);
                }
            });
        } else {
            result.setVisible(true);
        }
        return result;
    }

    public static boolean useCustomLafFrameDecorations(boolean atStartupCheck) {
        boolean value = false;
        UIDefaults laf = UIManager.getLookAndFeelDefaults();
        if (laf.getBoolean("maltego.useCustomLafFrameDecorations") && (isWindows && laf.getBoolean("maltego.includeWindowsForCustomLafFrameDecorations") || isMac && laf.getBoolean("maltego.includeMacForCustomLafFrameDecorations") || isLinux && laf.getBoolean("maltego.includeLinuxForCustomLafFrameDecorations"))) {
            return AbstractWindowRunner.useCustomLafFrameDecorationsSubCheck(laf, atStartupCheck);
        }
        return value;
    }

    private static boolean useCustomLafFrameDecorationsSubCheck(UIDefaults laf, boolean atStartupCheck) {
        boolean value = true;
        if (!(atStartupCheck && laf.getBoolean("maltego.ignoreDualScreenMonitorCheckForCustomLafFrameDecorationsAtStartup") || !atStartupCheck && laf.getBoolean("maltego.ignoreDualScreenMonitorCheckForCustomLafFrameDecorationsPerpetually"))) {
            try {
                GraphicsDevice[] screenDevices = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
                if (screenDevices.length > 1) {
                    value = false;
                }
            }
            catch (HeadlessException ex) {
                value = false;
            }
        }
        return value;
    }

    private void grayOutMainWindow() {
        Map hintsMap;
        assert (EventQueue.isDispatchThread());
        Frame f = WindowManager.getDefault().getMainWindow();
        if (f instanceof JFrame && ((hintsMap = (Map)Toolkit.getDefaultToolkit().getDesktopProperty("awt.font.desktophints")) == null || !RenderingHints.VALUE_TEXT_ANTIALIAS_OFF.equals(hintsMap.get(RenderingHints.KEY_TEXT_ANTIALIASING)))) {
            JFrame jf = (JFrame)f;
            RunOffEDTImpl.TranslucentMask mask = new RunOffEDTImpl.TranslucentMask();
            this.oldGlassPane = jf.getGlassPane();
            jf.setGlassPane(mask);
            mask.setVisible(true);
            mask.setBounds(0, 0, jf.getContentPane().getWidth(), jf.getContentPane().getHeight());
            mask.invalidate();
            mask.revalidate();
            mask.repaint();
            jf.getRootPane().paintImmediately(0, 0, jf.getRootPane().getWidth(), jf.getRootPane().getHeight());
        }
    }

    private void ungrayMainWindow() {
        if (this.oldGlassPane != null) {
            JFrame jf = (JFrame)WindowManager.getDefault().getMainWindow();
            jf.setGlassPane(this.oldGlassPane);
            jf.getGlassPane().setVisible(false);
            jf.invalidate();
            jf.repaint();
        }
    }

    private final class DialogCreator
    implements Runnable {
        private final CountDownLatch latch;

        DialogCreator(CountDownLatch latch) {
            this.latch = latch;
        }

        @Override
        public void run() {
            AbstractWindowRunner.this.createModalProgressDialog(AbstractWindowRunner.this.handle, AbstractWindowRunner.this.includeDetail);
            this.latch.countDown();
        }
    }

}

