/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.progress.spi.ExtractedProgressUIWorker
 *  org.netbeans.modules.progress.spi.ProgressUIWorkerProvider
 *  org.netbeans.modules.progress.spi.ProgressUIWorkerWithModel
 */
package org.netbeans.modules.progress.ui;

import org.netbeans.modules.progress.spi.ExtractedProgressUIWorker;
import org.netbeans.modules.progress.spi.ProgressUIWorkerProvider;
import org.netbeans.modules.progress.spi.ProgressUIWorkerWithModel;
import org.netbeans.modules.progress.ui.NbProgressBar;
import org.netbeans.modules.progress.ui.StatusLineComponent;

public class ProviderImpl
implements ProgressUIWorkerProvider {
    public ProgressUIWorkerWithModel getDefaultWorker() {
        return new StatusLineComponent();
    }

    public ExtractedProgressUIWorker getExtractedComponentWorker() {
        return new NbProgressBar();
    }
}

