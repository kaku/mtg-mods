/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.progress.spi.ExtractedProgressUIWorker
 *  org.netbeans.modules.progress.spi.InternalHandle
 *  org.netbeans.modules.progress.spi.ProgressEvent
 */
package org.netbeans.modules.progress.ui;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.UIManager;
import org.netbeans.modules.progress.spi.ExtractedProgressUIWorker;
import org.netbeans.modules.progress.spi.InternalHandle;
import org.netbeans.modules.progress.spi.ProgressEvent;
import org.netbeans.modules.progress.ui.StatusLineComponent;

public class NbProgressBar
extends JProgressBar
implements ExtractedProgressUIWorker {
    static final String SLEEPY = "sleepy";
    boolean isSetup = false;
    boolean usedInStatusBar = false;
    private JLabel detailLabel = new JLabel();
    private JLabel mainLabel = new JLabel();

    public NbProgressBar() {
        Color bg;
        this.setOrientation(0);
        this.setAlignmentX(0.5f);
        this.setAlignmentY(0.5f);
        Color fg = UIManager.getColor("nbProgressBar.Foreground");
        if (fg != null) {
            this.setForeground(fg);
        }
        if ((bg = UIManager.getColor("nbProgressBar.Background")) != null) {
            this.setBackground(bg);
        }
    }

    public void setUseInStatusBar(boolean use) {
        this.usedInStatusBar = use;
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension supers = super.getPreferredSize();
        if (this.usedInStatusBar) {
            supers.width = 200;
        }
        return supers;
    }

    public void processProgressEvent(ProgressEvent event) {
        if (event.getType() == 0 || !this.isSetup || event.isSwitched()) {
            NbProgressBar.setupBar(event.getSource(), this);
            this.mainLabel.setText(event.getSource().getDisplayName());
            this.isSetup = true;
        }
        if (event.getType() == 1) {
            if (event.getWorkunitsDone() > 0) {
                this.setValue(event.getWorkunitsDone());
            }
            this.setString(StatusLineComponent.getBarString(event.getPercentageDone(), event.getEstimatedCompletion()));
            if (event.getDisplayName() != null) {
                this.mainLabel.setText(event.getDisplayName());
            }
            if (event.getMessage() != null) {
                this.detailLabel.setText(event.getMessage());
            }
        } else if (event.getType() == 4) {
            boolean wasIndetermenite = this.isIndeterminate();
            this.setIndeterminate(false);
            this.setMaximum(event.getSource().getTotalUnits());
            this.setValue(event.getSource().getTotalUnits());
            if (wasIndetermenite) {
                this.setStringPainted(false);
            } else {
                this.setString(StatusLineComponent.getBarString(100.0, -1));
            }
        }
    }

    public void processSelectedProgressEvent(ProgressEvent event) {
    }

    static void setupBar(InternalHandle handle, NbProgressBar bar) {
        bar.putClientProperty("sleepy", null);
        int total = handle.getTotalUnits();
        if (handle.isInSleepMode()) {
            bar.setStringPainted(true);
            bar.setIndeterminate(false);
            bar.setMaximum(1);
            bar.setMinimum(0);
            bar.setValue(0);
            bar.putClientProperty("sleepy", new Object());
        } else if (total < 1) {
            bar.setValue(bar.getMaximum());
            bar.setIndeterminate(true);
            bar.setStringPainted(false);
        } else {
            bar.setStringPainted(true);
            bar.setIndeterminate(false);
            bar.setMaximum(total);
            bar.setMinimum(0);
            bar.setValue(0);
        }
        bar.setString(" ");
    }

    public JComponent getProgressComponent() {
        return this;
    }

    public JLabel getMainLabelComponent() {
        return this.mainLabel;
    }

    public JLabel getDetailLabelComponent() {
        return this.detailLabel;
    }
}

