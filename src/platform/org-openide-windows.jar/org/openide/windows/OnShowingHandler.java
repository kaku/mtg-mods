/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.lookup.Lookups
 */
package org.openide.windows;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.lookup.Lookups;
import org.openide.windows.WindowManager;

final class OnShowingHandler
implements LookupListener,
Runnable {
    private final Set<String> onShowing = new HashSet<String>();
    private final Lookup lkpShowing;
    private final WindowManager wm;
    private Lookup.Result<Runnable> resShow;

    OnShowingHandler(Lookup lkp, WindowManager wm) {
        this.lkpShowing = lkp;
        this.wm = wm;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void initialize() {
        for (Lookup.Item item : this.onShowing().allItems()) {
            Set<String> set = this.onShowing;
            synchronized (set) {
                Runnable r;
                if (this.onShowing.add(item.getId()) && (r = (Runnable)item.getInstance()) != null) {
                    this.wm.invokeWhenUIReady(r);
                }
                continue;
            }
        }
    }

    private synchronized Lookup.Result<Runnable> onShowing() {
        if (this.resShow == null) {
            Lookup lkp = this.lkpShowing != null ? this.lkpShowing : Lookups.forPath((String)"Modules/UIReady");
            this.resShow = lkp.lookupResult(Runnable.class);
            this.resShow.addLookupListener((LookupListener)this);
        }
        return this.resShow;
    }

    public void resultChanged(LookupEvent ev) {
        this.initialize();
    }

    @Override
    public void run() {
        this.initialize();
    }
}

