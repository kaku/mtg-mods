/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 */
package org.openide.windows;

import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Window;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.SwingUtilities;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.windows.DummyWindowManager;
import org.openide.windows.Mode;
import org.openide.windows.OnShowingHandler;
import org.openide.windows.TopComponent;
import org.openide.windows.TopComponentGroup;
import org.openide.windows.WindowSystemListener;
import org.openide.windows.Workspace;

public abstract class WindowManager
implements Serializable {
    @Deprecated
    public static final String PROP_WORKSPACES = "workspaces";
    @Deprecated
    public static final String PROP_CURRENT_WORKSPACE = "currentWorkspace";
    public static final String PROP_MODES = "modes";
    private static WindowManager dummyInstance;
    static final long serialVersionUID = -4133918059009277602L;
    private Reference<TopComponent> activeComponent = new WeakReference<Object>(null);
    private TopComponent.Registry registry;
    private final OnShowingHandler onShowing;

    public WindowManager() {
        this.onShowing = new OnShowingHandler(null, this);
    }

    public static final WindowManager getDefault() {
        WindowManager wmInstance = (WindowManager)Lookup.getDefault().lookup(WindowManager.class);
        return wmInstance != null ? wmInstance : WindowManager.getDummyInstance();
    }

    private static synchronized WindowManager getDummyInstance() {
        if (dummyInstance == null) {
            dummyInstance = new DummyWindowManager();
        }
        return dummyInstance;
    }

    public abstract Mode findMode(String var1);

    public abstract Mode findMode(TopComponent var1);

    public abstract Set<? extends Mode> getModes();

    public abstract Frame getMainWindow();

    public abstract void updateUI();

    protected abstract Component createTopComponentManager(TopComponent var1);

    protected TopComponent.Registry componentRegistry() {
        return (TopComponent.Registry)Lookup.getDefault().lookup(TopComponent.Registry.class);
    }

    public synchronized TopComponent.Registry getRegistry() {
        if (this.registry != null) {
            return this.registry;
        }
        this.onShowing.initialize();
        this.registry = this.componentRegistry();
        return this.registry;
    }

    @Deprecated
    public final Workspace createWorkspace(String name) {
        return this.createWorkspace(name, name);
    }

    @Deprecated
    public abstract Workspace createWorkspace(String var1, String var2);

    @Deprecated
    public abstract Workspace findWorkspace(String var1);

    @Deprecated
    public abstract Workspace[] getWorkspaces();

    @Deprecated
    public abstract void setWorkspaces(Workspace[] var1);

    @Deprecated
    public abstract Workspace getCurrentWorkspace();

    public abstract TopComponentGroup findTopComponentGroup(String var1);

    public abstract void addPropertyChangeListener(PropertyChangeListener var1);

    public abstract void removePropertyChangeListener(PropertyChangeListener var1);

    @Deprecated
    protected static final Component findComponentManager(TopComponent tc) {
        return null;
    }

    protected void activateComponent(TopComponent tc) {
        if (this.getActiveComponent() == tc) {
            return;
        }
        TopComponent old = this.getActiveComponent();
        if (old != null) {
            try {
                old.componentDeactivated();
            }
            catch (Throwable th) {
                WindowManager.logThrowable(th, "[Winsys] TopComponent " + old.getClass().getName() + " throws runtime exception from its componentDeactivated() method.\nPlease repair it!");
            }
        }
        this.setActiveComponent(tc);
        TopComponent newTC = this.getActiveComponent();
        if (newTC != null) {
            try {
                newTC.componentActivated();
            }
            catch (Throwable th) {
                WindowManager.logThrowable(th, "[Winsys] TopComponent " + newTC.getClass().getName() + " throws runtime exception from its componentActivated() method.\nPlease repair it!");
            }
        }
    }

    protected void componentOpenNotify(TopComponent tc) {
        try {
            tc.componentOpened();
        }
        catch (Throwable th) {
            WindowManager.logThrowable(th, "[Winsys] TopComponent " + tc.getClass().getName() + " throws exception/error from its componentOpened() method.\nPlease repair it!");
        }
    }

    protected void componentCloseNotify(TopComponent tc) {
        try {
            tc.componentClosed();
        }
        catch (Throwable th) {
            WindowManager.logThrowable(th, "[Winsys] TopComponent " + tc.getClass().getName() + " throws exception/error from its componentClosed() method.\nPlease repair it!");
        }
        if (tc == this.getActiveComponent()) {
            this.activateComponent(null);
        }
    }

    protected void componentShowing(TopComponent tc) {
        try {
            tc.componentShowing();
        }
        catch (Throwable th) {
            WindowManager.logThrowable(th, "[Winsys] TopComponent " + tc.getClass().getName() + " throws runtime exception from its componentShowing() method.\nPlease repair it!");
        }
    }

    protected void componentHidden(TopComponent tc) {
        try {
            tc.componentHidden();
        }
        catch (Throwable th) {
            WindowManager.logThrowable(th, "[Winsys] TopComponent " + tc.getClass().getName() + " throws runtime exception from its componentHidden() method.\nPlease repair it!");
        }
    }

    private static void logThrowable(Throwable th, String message) {
        if (th instanceof ThreadDeath || th instanceof OutOfMemoryError) {
            throw (Error)th;
        }
        Logger.getLogger(WindowManager.class.getName()).log(Level.WARNING, message, th);
    }

    protected abstract void topComponentOpen(TopComponent var1);

    protected void topComponentOpenAtTabPosition(TopComponent tc, int position) {
        this.topComponentOpen(tc);
    }

    protected int topComponentGetTabPosition(TopComponent tc) {
        Mode mode = this.findMode(tc);
        if (mode == null || !this.topComponentIsOpened(tc)) {
            return -1;
        }
        TopComponent[] tcs = mode.getTopComponents();
        for (int i = 0; i < tcs.length; ++i) {
            if (tcs[i] != tc) continue;
            return i;
        }
        return -1;
    }

    protected abstract void topComponentClose(TopComponent var1);

    protected abstract void topComponentRequestActive(TopComponent var1);

    protected abstract void topComponentRequestVisible(TopComponent var1);

    protected abstract void topComponentDisplayNameChanged(TopComponent var1, String var2);

    protected abstract void topComponentHtmlDisplayNameChanged(TopComponent var1, String var2);

    protected abstract void topComponentToolTipChanged(TopComponent var1, String var2);

    protected abstract void topComponentIconChanged(TopComponent var1, Image var2);

    protected abstract void topComponentActivatedNodesChanged(TopComponent var1, Node[] var2);

    protected abstract boolean topComponentIsOpened(TopComponent var1);

    protected abstract Action[] topComponentDefaultActions(TopComponent var1);

    protected abstract String topComponentID(TopComponent var1, String var2);

    protected void topComponentRequestAttention(TopComponent tc) {
    }

    protected void topComponentMakeBusy(TopComponent tc, boolean busy) {
    }

    protected void topComponentToFront(TopComponent tc) {
        Window parentWindow = SwingUtilities.getWindowAncestor(tc);
        if (null != parentWindow) {
            Frame parentFrame;
            int state;
            if (parentWindow instanceof Frame && ((state = (parentFrame = (Frame)parentWindow).getExtendedState()) & 1) > 0) {
                parentFrame.setExtendedState(state & -2);
            }
            parentWindow.toFront();
        }
    }

    protected void topComponentCancelRequestAttention(TopComponent tc) {
    }

    protected void topComponentAttentionHighlight(TopComponent tc, boolean highlight) {
    }

    public String findTopComponentID(TopComponent tc) {
        return this.topComponentID(tc, tc.preferredID());
    }

    public abstract TopComponent findTopComponent(String var1);

    public void invokeWhenUIReady(Runnable run) {
        EventQueue.invokeLater(run);
    }

    public boolean isEditorTopComponent(TopComponent tc) {
        return false;
    }

    public boolean isOpenedEditorTopComponent(TopComponent tc) {
        return false;
    }

    public TopComponent[] getOpenedTopComponents(Mode mode) {
        TopComponent[] allTcs = mode.getTopComponents();
        ArrayList<TopComponent> openedTcs = new ArrayList<TopComponent>(allTcs.length);
        for (TopComponent tc : allTcs) {
            if (!tc.isOpened()) continue;
            openedTcs.add(tc);
        }
        return openedTcs.toArray(new TopComponent[openedTcs.size()]);
    }

    public boolean isEditorMode(Mode mode) {
        return false;
    }

    private TopComponent getActiveComponent() {
        return this.activeComponent.get();
    }

    private void setActiveComponent(TopComponent activeComponent) {
        this.activeComponent = new WeakReference<TopComponent>(activeComponent);
    }

    public void addWindowSystemListener(WindowSystemListener listener) {
    }

    public void removeWindowSystemListener(WindowSystemListener listener) {
    }

    public void setRole(String roleName) {
    }

    public boolean isTopComponentMinimized(TopComponent tc) {
        return false;
    }

    public void setTopComponentMinimized(TopComponent tc, boolean minimize) {
    }

    public boolean isTopComponentFloating(TopComponent tc) {
        return false;
    }

    public void setTopComponentFloating(TopComponent tc, boolean floating) {
    }

    public String getRole() {
        return null;
    }

    @Deprecated
    protected static interface Component
    extends Serializable {
        @Deprecated
        public static final long serialVersionUID = 0;

        public void open();

        public void open(Workspace var1);

        public void close(Workspace var1);

        public void requestFocus();

        public void requestVisible();

        public Node[] getActivatedNodes();

        public void setActivatedNodes(Node[] var1);

        public void nameChanged();

        public void setIcon(Image var1);

        public Image getIcon();

        public Set<Workspace> whereOpened();
    }

}

