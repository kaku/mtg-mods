/*
 * Decompiled with CFR 0_118.
 */
package org.openide.windows;

import java.awt.Image;
import java.awt.Rectangle;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import org.openide.windows.TopComponent;
import org.openide.windows.Workspace;

public interface Mode
extends Serializable {
    public static final String PROP_BOUNDS = "bounds";
    public static final String PROP_NAME = "name";
    public static final String PROP_DISPLAY_NAME = "displayName";
    public static final long serialVersionUID = -2650968323666215654L;

    public String getDisplayName();

    public String getName();

    public Image getIcon();

    public boolean dockInto(TopComponent var1);

    public boolean canDock(TopComponent var1);

    public void setBounds(Rectangle var1);

    public Rectangle getBounds();

    public Workspace getWorkspace();

    public TopComponent[] getTopComponents();

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);

    public TopComponent getSelectedTopComponent();
}

