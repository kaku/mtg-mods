/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.lookup.NamedServiceDefinition
 */
package org.openide.windows;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.openide.util.lookup.NamedServiceDefinition;

@NamedServiceDefinition(path="Modules/UIReady", serviceType={Runnable.class}, position="position")
@Retention(value=RetentionPolicy.SOURCE)
@Target(value={ElementType.TYPE})
public @interface OnShowing {
    public int position() default Integer.MAX_VALUE;
}

