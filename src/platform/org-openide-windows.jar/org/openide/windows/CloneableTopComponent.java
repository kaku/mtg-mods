/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.util.io.NbMarshalledObject
 */
package org.openide.windows;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.util.io.NbMarshalledObject;
import org.openide.windows.CloneableOpenSupport;
import org.openide.windows.CloneableOpenSupportRedirector;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;
import org.openide.windows.Workspace;

public abstract class CloneableTopComponent
extends TopComponent
implements Externalizable,
TopComponent.Cloneable {
    static final long serialVersionUID = 4893753008783256289L;
    public static final Ref EMPTY = new Ref();
    private Ref ref;
    private boolean isLastActivated;

    public final Object clone() {
        return this.cloneComponent();
    }

    public final CloneableTopComponent cloneTopComponent() {
        CloneableTopComponent top = this.createClonedObject();
        top.setReference(this.getReference());
        return top;
    }

    @Override
    public final TopComponent cloneComponent() {
        return this.cloneTopComponent();
    }

    protected CloneableTopComponent createClonedObject() {
        try {
            NbMarshalledObject o = new NbMarshalledObject((Object)this);
            return (CloneableTopComponent)o.get();
        }
        catch (IOException ex) {
            throw new AssertionError(ex);
        }
        catch (ClassNotFoundException ex) {
            throw new AssertionError(ex);
        }
    }

    public final synchronized Ref getReference() {
        if (this.ref == null) {
            this.ref = new Ref(this);
        }
        return this.ref;
    }

    public final synchronized void setReference(Ref another) {
        if (another == EMPTY) {
            throw new IllegalArgumentException(NbBundle.getBundle(CloneableTopComponent.class).getString("EXC_CannotAssign"));
        }
        if (this.ref != null) {
            this.ref.removeComponent(this);
        }
        another.register(this);
        this.ref = another;
    }

    @Override
    protected void componentOpened() {
        super.componentOpened();
        this.getReference().register(this);
        CloneableOpenSupport cos = (CloneableOpenSupport)this.getLookup().lookup(CloneableOpenSupport.class);
        if (cos != null) {
            CloneableOpenSupportRedirector.notifyOpened(cos);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void componentClosed() {
        super.componentClosed();
        if (!this.isOpened()) {
            try {
                CloneableOpenSupport cos = (CloneableOpenSupport)this.getLookup().lookup(CloneableOpenSupport.class);
                if (cos != null) {
                    CloneableOpenSupportRedirector.notifyClosed(cos);
                }
            }
            finally {
                this.getReference().unregister(this);
            }
        }
    }

    @Override
    public boolean canClose() {
        if (!this.isOpened()) {
            return false;
        }
        return this.getReference().unregister(this);
    }

    @Override
    public boolean canClose(Workspace workspace, boolean last) {
        if (last) {
            return this.getReference().unregister(this);
        }
        return true;
    }

    protected boolean closeLast() {
        return true;
    }

    @Override
    public void readExternal(ObjectInput oi) throws IOException, ClassNotFoundException {
        Ref ref;
        super.readExternal(oi);
        if (this.serialVersion != 0 && (ref = (Ref)oi.readObject()) != null) {
            this.setReference(ref);
        }
    }

    @Override
    public void writeExternal(ObjectOutput oo) throws IOException {
        super.writeExternal(oo);
        oo.writeObject(this.ref);
    }

    private boolean isLastActive() {
        return this.isLastActivated;
    }

    private void setLastActivated() {
        this.isLastActivated = true;
    }

    private void unsetLastActivated() {
        this.isLastActivated = false;
    }

    public static class Ref
    implements Serializable {
        static final long serialVersionUID = 5543148876020730556L;
        private static final Object LOCK = new Object();
        private transient Set<CloneableTopComponent> componentSet = new HashSet<CloneableTopComponent>(7);
        private transient PropertyChangeListener myComponentSetListener;

        protected Ref() {
            this.myComponentSetListener = new PropertyChangeListener(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    Object activated = evt.getNewValue();
                    Object deactivated = evt.getOldValue();
                    boolean activatedInSet = false;
                    Object object = LOCK;
                    synchronized (object) {
                        activatedInSet = Ref.this.componentSet.contains(activated) && Ref.this.componentSet.contains(deactivated);
                    }
                    if (activatedInSet) {
                        ((CloneableTopComponent)activated).setLastActivated();
                        ((CloneableTopComponent)deactivated).unsetLastActivated();
                    }
                }
            };
            PropertyChangeListener listener = WeakListeners.propertyChange((PropertyChangeListener)this.myComponentSetListener, (Object)WindowManager.getDefault().getRegistry());
            WindowManager.getDefault().getRegistry().addPropertyChangeListener(listener);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private Ref(CloneableTopComponent c) {
            Object object = LOCK;
            synchronized (object) {
                this.componentSet.add(c);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Enumeration<CloneableTopComponent> getComponents() {
            HashSet<CloneableTopComponent> components;
            Object object = LOCK;
            synchronized (object) {
                components = new HashSet<CloneableTopComponent>(this.componentSet);
            }
            return Collections.enumeration(components);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public boolean isEmpty() {
            Object object = LOCK;
            synchronized (object) {
                return this.componentSet.isEmpty();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public CloneableTopComponent getAnyComponent() {
            Object object = LOCK;
            synchronized (object) {
                return this.componentSet.iterator().next();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public CloneableTopComponent getArbitraryComponent() {
            TopComponent activated = WindowManager.getDefault().getRegistry().getActivated();
            Object object = LOCK;
            synchronized (object) {
                if (this.componentSet.contains(activated)) {
                    return (CloneableTopComponent)activated;
                }
                CloneableTopComponent result = null;
                Iterator<CloneableTopComponent> i$ = this.componentSet.iterator();
                while (i$.hasNext()) {
                    CloneableTopComponent comp;
                    result = comp = i$.next();
                    if (!comp.isLastActive()) continue;
                    break;
                }
                return result;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private final void register(CloneableTopComponent c) {
            Object object = LOCK;
            synchronized (object) {
                this.componentSet.add(c);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private final boolean unregister(CloneableTopComponent c) {
            int componentCount;
            Object object = LOCK;
            synchronized (object) {
                if (!this.componentSet.contains(c)) {
                    return true;
                }
                componentCount = this.componentSet.size();
            }
            if (componentCount > 1 || c.closeLast()) {
                this.removeComponent(c);
                return true;
            }
            return false;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void removeComponent(CloneableTopComponent c) {
            Object object = LOCK;
            synchronized (object) {
                this.componentSet.remove(c);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
            in.defaultReadObject();
            Object object = LOCK;
            synchronized (object) {
                this.componentSet = new HashSet<CloneableTopComponent>(7);
            }
        }

    }

}

