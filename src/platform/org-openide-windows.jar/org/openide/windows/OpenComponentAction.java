/*
 * Decompiled with CFR 0_118.
 */
package org.openide.windows;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

final class OpenComponentAction
implements ActionListener {
    private TopComponent component;
    private final Map<?, ?> map;

    OpenComponentAction(TopComponent component) {
        assert (component != null);
        this.component = component;
        this.map = null;
    }

    OpenComponentAction(Map<?, ?> map) {
        this.map = map;
    }

    private TopComponent getTopComponent() {
        assert (EventQueue.isDispatchThread());
        if (this.component != null) {
            return this.component;
        }
        TopComponent c = null;
        Object id = this.map.get("preferredID");
        if (id instanceof String) {
            c = WindowManager.getDefault().findTopComponent((String)id);
        }
        if (c == null) {
            c = (TopComponent)this.map.get("component");
        }
        if (id != null) {
            this.component = c;
        }
        return c;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        TopComponent win = this.getTopComponent();
        if (null == win) {
            throw new IllegalStateException("Cannot find TopComponent with preferredID " + this.map.get("preferredID") + ", see IDE log for more details.");
        }
        win.open();
        win.requestActive();
    }
}

