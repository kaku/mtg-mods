/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 */
package org.openide.windows;

import java.util.Collection;
import java.util.concurrent.atomic.AtomicReference;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.windows.CloneableOpenSupport;

public abstract class CloneableOpenSupportRedirector {
    private static final Lookup.Result<CloneableOpenSupportRedirector> lkp = Lookup.getDefault().lookup(new Lookup.Template(CloneableOpenSupportRedirector.class));
    private static final AtomicReference<Collection<? extends CloneableOpenSupportRedirector>> redirectors = new AtomicReference<V>();
    private static final LookupListener listener = new LookupListener(){

        public void resultChanged(LookupEvent ev) {
            redirectors.set(lkp.allInstances());
        }
    };

    protected abstract CloneableOpenSupport redirect(CloneableOpenSupport.Env var1);

    protected abstract void opened(CloneableOpenSupport.Env var1);

    protected abstract void closed(CloneableOpenSupport.Env var1);

    static CloneableOpenSupport findRedirect(CloneableOpenSupport one) {
        CloneableOpenSupport.Env env = one.env;
        Collection<? extends CloneableOpenSupportRedirector> rlist = redirectors.get();
        if (rlist != null) {
            for (CloneableOpenSupportRedirector r : rlist) {
                CloneableOpenSupport ces = r.redirect(env);
                if (ces == null || ces == one) continue;
                return ces;
            }
        }
        return null;
    }

    static void notifyOpened(CloneableOpenSupport one) {
        CloneableOpenSupport.Env env = one.env;
        Collection<? extends CloneableOpenSupportRedirector> rlist = redirectors.get();
        if (rlist != null) {
            for (CloneableOpenSupportRedirector r : rlist) {
                r.opened(env);
            }
        }
    }

    static void notifyClosed(CloneableOpenSupport one) {
        CloneableOpenSupport.Env env = one.env;
        Collection<? extends CloneableOpenSupportRedirector> rlist = redirectors.get();
        if (rlist != null) {
            for (CloneableOpenSupportRedirector r : rlist) {
                r.closed(env);
            }
        }
    }

    static {
        lkp.addLookupListener(listener);
        listener.resultChanged(null);
    }

}

