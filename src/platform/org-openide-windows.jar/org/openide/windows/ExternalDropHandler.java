/*
 * Decompiled with CFR 0_118.
 */
package org.openide.windows;

import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;

public abstract class ExternalDropHandler {
    public abstract boolean canDrop(DropTargetDragEvent var1);

    public abstract boolean canDrop(DropTargetDropEvent var1);

    public abstract boolean handleDrop(DropTargetDropEvent var1);
}

