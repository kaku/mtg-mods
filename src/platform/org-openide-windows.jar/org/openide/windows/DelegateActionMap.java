/*
 * Decompiled with CFR 0_118.
 */
package org.openide.windows;

import java.awt.Component;
import java.awt.Container;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JComponent;
import org.netbeans.modules.openide.windows.GlobalActionContextImpl;
import org.openide.windows.TopComponent;

final class DelegateActionMap
extends ActionMap {
    private Reference<JComponent> component;
    private ActionMap delegate;

    public DelegateActionMap(JComponent c) {
        this.setComponent(c);
    }

    public DelegateActionMap(TopComponent c, ActionMap delegate) {
        this.setComponent(c);
        this.delegate = delegate;
    }

    @Override
    public int size() {
        return this.keys().length;
    }

    @Override
    public Action get(Object key) {
        Component owner;
        Action a;
        JComponent comp;
        ActionMap m = this.delegate == null ? ((comp = this.getComponent()) == null ? null : comp.getActionMap()) : this.delegate;
        if (m != null && (a = m.get(key)) != null) {
            return a;
        }
        Action found = null;
        for (owner = GlobalActionContextImpl.findFocusOwner(); owner != null && owner != this.getComponent(); owner = owner.getParent()) {
            if (found != null || !(owner instanceof JComponent) || (m = ((JComponent)owner).getActionMap()) == null) continue;
            found = m.get(key);
        }
        return owner == this.getComponent() ? found : null;
    }

    @Override
    public Object[] allKeys() {
        return this.keys(true);
    }

    @Override
    public Object[] keys() {
        return this.keys(false);
    }

    private Object[] keys(boolean all) {
        Component owner;
        JComponent comp;
        HashSet<Object> keys = new HashSet<Object>();
        ActionMap m = this.delegate == null ? ((comp = this.getComponent()) == null ? null : comp.getActionMap()) : this.delegate;
        if (m != null) {
            Object[] allKeys;
            List<Object> l = all ? (null == (allKeys = m.allKeys()) ? Collections.EMPTY_LIST : Arrays.asList(m.allKeys())) : Arrays.asList(m.keys());
            keys.addAll(l);
        }
        ArrayList<JComponent> hierarchy = new ArrayList<JComponent>();
        for (owner = GlobalActionContextImpl.findFocusOwner(); owner != null && owner != this.getComponent(); owner = owner.getParent()) {
            if (!(owner instanceof JComponent)) continue;
            hierarchy.add((JComponent)owner);
        }
        if (owner == this.getComponent()) {
            for (JComponent c : hierarchy) {
                Object[] fk;
                ActionMap am = c.getActionMap();
                if (am == null || (fk = all ? am.allKeys() : am.keys()) == null) continue;
                keys.addAll(Arrays.asList(fk));
            }
        }
        return keys.toArray();
    }

    @Override
    public void remove(Object key) {
        if (this.delegate != null) {
            this.delegate.remove(key);
        }
    }

    @Override
    public void setParent(ActionMap map) {
        if (this.delegate != null) {
            this.delegate.setParent(map);
            GlobalActionContextImpl.blickActionMap(new ActionMap());
        }
    }

    @Override
    public void clear() {
        if (this.delegate != null) {
            this.delegate.clear();
        }
    }

    @Override
    public void put(Object key, Action action) {
        if (this.delegate != null) {
            this.delegate.put(key, action);
        }
    }

    @Override
    public ActionMap getParent() {
        return this.delegate == null ? null : this.delegate.getParent();
    }

    public String toString() {
        return Object.super.toString() + " for " + this.getComponent();
    }

    JComponent getComponent() {
        return this.component.get();
    }

    private void setComponent(JComponent component) {
        this.component = new WeakReference<JComponent>(component);
    }
}

