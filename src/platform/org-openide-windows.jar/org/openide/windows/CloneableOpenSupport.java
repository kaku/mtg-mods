/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 *  org.openide.util.WeakListeners
 */
package org.openide.windows;

import java.awt.Container;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.io.IOException;
import java.io.Serializable;
import java.util.Enumeration;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Mutex;
import org.openide.util.WeakListeners;
import org.openide.windows.CloneableOpenSupportRedirector;
import org.openide.windows.CloneableTopComponent;
import org.openide.windows.TopComponent;

public abstract class CloneableOpenSupport {
    private static Container container;
    protected Env env;
    protected CloneableTopComponent.Ref allEditors;

    public CloneableOpenSupport(Env env) {
        this.env = env;
        Listener l = new Listener(env, this);
        this.allEditors = l;
        env.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)l, (Object)env));
        env.addVetoableChangeListener(WeakListeners.vetoableChange((VetoableChangeListener)l, (Object)env));
    }

    public void open() {
        CloneableOpenSupport redirect = CloneableOpenSupportRedirector.findRedirect(this);
        if (redirect != null) {
            redirect.open();
            this.afterRedirectImpl(redirect);
            return;
        }
        Mutex.EVENT.writeAccess(new Runnable(){

            @Override
            public void run() {
                CloneableTopComponent editor = CloneableOpenSupport.this.openCloneableTopComponent();
                editor.requestActive();
            }
        });
    }

    public void view() {
        this.open();
    }

    public void edit() {
        this.open();
    }

    public boolean close() {
        return this.close(true);
    }

    protected boolean close(final boolean ask) {
        if (this.allEditors.isEmpty()) {
            return true;
        }
        Boolean ret = (Boolean)Mutex.EVENT.writeAccess((Mutex.Action)new Mutex.Action<Boolean>(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            public Boolean run() {
                Object object = CloneableOpenSupport.this.getLock();
                synchronized (object) {
                    if (ask && !CloneableOpenSupport.this.canClose()) {
                        return Boolean.FALSE;
                    }
                    Enumeration<CloneableTopComponent> en = CloneableOpenSupport.this.allEditors.getComponents();
                    while (en.hasMoreElements()) {
                        TopComponent c = en.nextElement();
                        if (c.close()) continue;
                        return Boolean.FALSE;
                    }
                }
                return Boolean.TRUE;
            }
        });
        return ret;
    }

    protected boolean canClose() {
        return true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final CloneableTopComponent openCloneableTopComponent() {
        Object object = this.getLock();
        synchronized (object) {
            CloneableTopComponent ret = this.allEditors.getArbitraryComponent();
            if (ret != null) {
                ret.open();
                return ret;
            }
            String msgOpening = this.messageOpening();
            if (msgOpening != null) {
                StatusDisplayer.getDefault().setStatusText(msgOpening);
            }
            CloneableTopComponent editor = this.createCloneableTopComponent();
            editor.setReference(this.allEditors);
            editor.open();
            String msgOpened = this.messageOpened();
            if (msgOpened == null && msgOpening != null) {
                msgOpened = "";
            }
            if (msgOpened != null) {
                StatusDisplayer.getDefault().setStatusText(msgOpened);
            }
            return editor;
        }
    }

    private Object getLock() {
        if (container == null) {
            container = new Container();
        }
        return container.getTreeLock();
    }

    protected abstract CloneableTopComponent createCloneableTopComponent();

    protected abstract String messageOpening();

    protected abstract String messageOpened();

    private void afterRedirectImpl(CloneableOpenSupport redirectedTo) {
        this.allEditors = redirectedTo.allEditors;
        this.afterRedirect(redirectedTo);
    }

    protected void afterRedirect(CloneableOpenSupport redirectedTo) {
    }

    private static final class Listener
    extends CloneableTopComponent.Ref
    implements PropertyChangeListener,
    VetoableChangeListener,
    Runnable {
        static final long serialVersionUID = -1934890789745432531L;
        private final Env env;
        private final transient CloneableOpenSupport refCOS;

        public Listener(Env env, CloneableOpenSupport cos) {
            this.env = env;
            this.refCOS = cos;
        }

        private CloneableOpenSupport support() {
            return this.env.findCloneableOpenSupport();
        }

        @Override
        public void propertyChange(PropertyChangeEvent ev) {
            if ("valid".equals(ev.getPropertyName())) {
                if (Boolean.FALSE.equals(ev.getOldValue())) {
                    return;
                }
                Mutex.EVENT.readAccess((Runnable)this);
            }
        }

        @Override
        public void run() {
            CloneableOpenSupport os = this.support();
            if (os != null) {
                this.env.unmarkModified();
                os.close(false);
            }
        }

        @Override
        public void vetoableChange(PropertyChangeEvent ev) throws PropertyVetoException {
            if ("valid".equals(ev.getPropertyName())) {
                CloneableOpenSupport os;
                if (Boolean.FALSE.equals(ev.getOldValue())) {
                    return;
                }
                if (this.env.isModified() && (os = this.support()) != null && !os.canClose()) {
                    throw new PropertyVetoException("", ev);
                }
            }
        }

        public Object readResolve() {
            CloneableOpenSupport os = this.support();
            if (os == null) {
                return this;
            }
            return os.allEditors;
        }
    }

    public static interface Env
    extends Serializable {
        public static final String PROP_VALID = "valid";
        public static final String PROP_MODIFIED = "modified";

        public void addPropertyChangeListener(PropertyChangeListener var1);

        public void removePropertyChangeListener(PropertyChangeListener var1);

        public void addVetoableChangeListener(VetoableChangeListener var1);

        public void removeVetoableChangeListener(VetoableChangeListener var1);

        public boolean isValid();

        public boolean isModified();

        public void markModified() throws IOException;

        public void unmarkModified();

        public CloneableOpenSupport findCloneableOpenSupport();
    }

}

