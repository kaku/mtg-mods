/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Actions
 *  org.openide.awt.UndoRedo
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Handle
 *  org.openide.nodes.NodeAdapter
 *  org.openide.nodes.NodeListener
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.HelpCtx
 *  org.openide.util.HelpCtx$Provider
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.WeakSet
 *  org.openide.util.actions.SystemAction
 */
package org.openide.windows;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Externalizable;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EventListener;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleRole;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.Keymap;
import org.netbeans.modules.openide.windows.GlobalActionContextImpl;
import org.openide.awt.Actions;
import org.openide.awt.UndoRedo;
import org.openide.nodes.Node;
import org.openide.nodes.NodeAdapter;
import org.openide.nodes.NodeListener;
import org.openide.util.ContextAwareAction;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.WeakSet;
import org.openide.util.actions.SystemAction;
import org.openide.windows.DefaultTopComponentLookup;
import org.openide.windows.DelegateActionMap;
import org.openide.windows.Mode;
import org.openide.windows.OpenComponentAction;
import org.openide.windows.RetainLocation;
import org.openide.windows.WindowManager;
import org.openide.windows.Workspace;

public class TopComponent
extends JComponent
implements Externalizable,
Accessible,
HelpCtx.Provider,
Lookup.Provider {
    private static Logger UILOG = Logger.getLogger("org.netbeans.ui.actions");
    static final long serialVersionUID = -3022538025284122942L;
    static final Logger LOG = Logger.getLogger(TopComponent.class.getName());
    @Deprecated
    public static final int CLOSE_EACH = 0;
    @Deprecated
    public static final int CLOSE_LAST = 1;
    public static final int PERSISTENCE_ALWAYS = 0;
    public static final int PERSISTENCE_ONLY_OPENED = 1;
    public static final int PERSISTENCE_NEVER = 2;
    private static Object defaultLookupLock = new Object();
    private static final Set<Class> warnedTCPIClasses = new WeakSet();
    private static final Set<Class> warnedClasses = new WeakSet();
    private Object defaultLookupRef;
    private NodeName nodeName;
    private int closeOperation = 1;
    private transient Image icon;
    private transient Node[] activatedNodes;
    private transient String displayName;
    private String htmlDisplayName;
    short serialVersion = 1;
    private AttentionGetter attentionGetter = null;
    public static final String PROP_KEEP_PREFERRED_SIZE_WHEN_SLIDED_IN = "netbeans.winsys.tc.keep_preferred_size_when_slided_in";
    public static final String PROP_CLOSING_DISABLED = "netbeans.winsys.tc.closing_disabled";
    public static final String PROP_SLIDING_DISABLED = "netbeans.winsys.tc.sliding_disabled";
    public static final String PROP_UNDOCKING_DISABLED = "netbeans.winsys.tc.undocking_disabled";
    public static final String PROP_DRAGGING_DISABLED = "netbeans.winsys.tc.dragging_disabled";
    public static final String PROP_MAXIMIZATION_DISABLED = "netbeans.winsys.tc.maximization_disabled";
    public static final String PROP_DND_COPY_DISABLED = "netbeans.winsys.tc.draganddrop_copy_disabled";
    private transient String modeName;
    private static final String MODE_ID_PREFERENCES_KEY_INFIX = "_modeId_";

    public TopComponent() {
        this(null);
    }

    public TopComponent(Lookup lookup) {
        if (lookup != null) {
            this.setLookup(lookup, true);
        }
        this.enableEvents(8);
        this.setFocusable(false);
        this.initActionMap(lookup);
    }

    @Override
    public void addNotify() {
        Mode m;
        super.addNotify();
        if (this.isPersistLocation() && (m = WindowManager.getDefault().findMode(this)) != null) {
            this.modeName = m.getName();
            if (this.modeName == null) {
                this.modeName = this.getClass().getAnnotation(RetainLocation.class).value();
            }
            NbPreferences.forModule(this.getClass()).put(this.getModeIdKey(), this.modeName);
        }
    }

    private boolean isPersistLocation() {
        boolean result;
        boolean bl = result = this.getPersistenceType() == 2 && this.getClass().getAnnotation(RetainLocation.class) != null;
        assert (this.annotationAndPersistenceTypeAreCompatible());
        return result;
    }

    private boolean annotationAndPersistenceTypeAreCompatible() {
        if (this.getPersistenceType() != 2 && this.getClass().getAnnotation(RetainLocation.class) != null) {
            Logger.getLogger(TopComponent.class.getName()).log(Level.WARNING, "Useless to annotate a TopComponent with @RetainLocation if its persistence type is not PERSISTENCE_NEVER: {0}", new Object[]{this.getClass().getName()});
        }
        return true;
    }

    private String getModeIdKey() {
        return this.getClass().getName() + "_modeId_" + WindowManager.getDefault().findTopComponentID(this);
    }

    private void initActionMap(Lookup lookup) {
        ActionMap inner = null;
        if (lookup != null) {
            inner = (ActionMap)lookup.lookup(ActionMap.class);
        }
        if (inner == null) {
            inner = new ActionMap();
        }
        DelegateActionMap am = new DelegateActionMap(this, inner);
        if (this instanceof Cloneable) {
            am.put("cloneWindow", new CloneWindowAction(am));
        }
        am.put("closeWindow", new CloseWindowAction(am));
        this.setActionMap(am);
    }

    public static final Registry getRegistry() {
        return WindowManager.getDefault().getRegistry();
    }

    public final Node[] getActivatedNodes() {
        return this.activatedNodes;
    }

    public final void setActivatedNodes(Node[] activatedNodes) {
        assert (this.multiviewActivatedNodes());
        this.setActivatedNodesImpl(activatedNodes);
    }

    private boolean multiviewActivatedNodes() {
        if ("org.netbeans.core.multiview.MultiViewTopComponent".equals(this.getClass().getName()) || "org.netbeans.core.multiview.MultiViewCloneableTopComponent".equals(this.getClass().getName())) {
            LOG.info("Warning: You should not call setActivatedNodes() on the multiview topcomponents. Instead please manipulate the lookup of the embedded MultiViewElements. For details, please see http://www.netbeans.org/issues/show_bug.cgi?id=67257");
        }
        return true;
    }

    private void setActivatedNodesImpl(Node[] activatedNodes) {
        boolean l = LOG.isLoggable(Level.FINER);
        if (Arrays.equals(this.activatedNodes, (Object[])activatedNodes)) {
            if (l) {
                LOG.finer("No change to activatedNodes for " + this);
            }
            return;
        }
        Lookup lookup = this.getLookup(false);
        if (lookup instanceof DefaultTopComponentLookup) {
            if (l) {
                LOG.finer("Updating lookup " + (Object)lookup + " for " + this);
            }
            ((DefaultTopComponentLookup)lookup).updateLookups(activatedNodes);
        }
        Node[] old = this.activatedNodes;
        this.activatedNodes = activatedNodes;
        if (l) {
            LOG.finer("activatedNodes changed: " + (activatedNodes == null ? "" : Arrays.asList(activatedNodes).toString()));
        }
        WindowManager.getDefault().topComponentActivatedNodesChanged(this, this.activatedNodes);
        if (l) {
            LOG.finer("window manager notified: " + this);
        }
        this.firePropertyChange("activatedNodes", old, this.activatedNodes);
        if (l) {
            LOG.finer("listeners notified: " + this);
        }
    }

    public int getPersistenceType() {
        String propValue;
        Description info = this.getClass().getAnnotation(Description.class);
        if (info != null) {
            return info.persistenceType();
        }
        if (warnedClasses.add(this.getClass()) && !TopComponent.class.equals(this.getClass())) {
            Logger.getAnonymousLogger().warning("Note - " + this.getClass().getName() + " ought to override getPersistenceType()" + " rather than using the client property or accepting the default.");
        }
        if ((propValue = (String)this.getClientProperty("PersistenceType")) == null) {
            return 0;
        }
        if ("Never".equals(propValue)) {
            return 2;
        }
        if ("OnlyOpened".equals(propValue)) {
            return 1;
        }
        return 0;
    }

    public UndoRedo getUndoRedo() {
        return UndoRedo.NONE;
    }

    public void open() {
        this.open(null);
    }

    @Deprecated
    public void open(Workspace workspace) {
        if (this.isPersistLocation()) {
            Mode mode;
            this.modeName = NbPreferences.forModule(this.getClass()).get(this.getModeIdKey(), null);
            if (this.modeName == null) {
                this.modeName = this.getClass().getAnnotation(RetainLocation.class).value();
            }
            if ((mode = WindowManager.getDefault().findMode(this.modeName)) != null) {
                mode.dockInto(this);
            }
        }
        WindowManager.getDefault().topComponentOpen(this);
    }

    public final void openAtTabPosition(int position) {
        WindowManager.getDefault().topComponentOpenAtTabPosition(this, position);
    }

    public final int getTabPosition() {
        return WindowManager.getDefault().topComponentGetTabPosition(this);
    }

    public final boolean isOpened() {
        return this.isOpened(null);
    }

    @Deprecated
    public final boolean isOpened(Workspace workspace) {
        return WindowManager.getDefault().topComponentIsOpened(this);
    }

    public final boolean close() {
        return this.close(null);
    }

    @Deprecated
    public final boolean close(Workspace workspace) {
        if (!this.isOpened()) {
            return true;
        }
        WindowManager.getDefault().topComponentClose(this);
        return !this.isOpened();
    }

    public boolean canClose() {
        if (!this.isOpened()) {
            return false;
        }
        return this.canClose(null, true);
    }

    @Deprecated
    public boolean canClose(Workspace workspace, boolean last) {
        return true;
    }

    @Deprecated
    protected void openNotify() {
    }

    @Deprecated
    protected void closeNotify() {
    }

    @Deprecated
    public SystemAction[] getSystemActions() {
        return new SystemAction[0];
    }

    public Action[] getActions() {
        Action[] actions = WindowManager.getDefault().topComponentDefaultActions(this);
        SystemAction[] sysActions = this.getSystemActions();
        if (sysActions.length > 0) {
            ArrayList<Action> acs = new ArrayList<Action>(Arrays.asList(actions));
            acs.addAll(Arrays.asList(sysActions));
            return acs.toArray(new Action[0]);
        }
        return actions;
    }

    public static Action openAction(TopComponent component, String displayName, String iconBase, boolean noIconInMenu) {
        return Actions.alwaysEnabled((ActionListener)new OpenComponentAction(component), (String)displayName, (String)iconBase, (boolean)noIconInMenu);
    }

    static Action openAction(Map map) {
        return Actions.alwaysEnabled((ActionListener)new OpenComponentAction(map), (String)((String)map.get("displayName")), (String)((String)map.get("iconBase")), (boolean)Boolean.TRUE.equals(map.get("noIconInMenu")));
    }

    @Deprecated
    public final void setCloseOperation(int closeOperation) {
        if (closeOperation != 0 && closeOperation != 1) {
            throw new IllegalArgumentException(NbBundle.getBundle(TopComponent.class).getString("EXC_UnknownOperation"));
        }
        if (this.closeOperation == closeOperation) {
            return;
        }
        this.closeOperation = closeOperation;
        this.firePropertyChange("closeOperation", null, null);
    }

    @Deprecated
    public final int getCloseOperation() {
        return this.closeOperation;
    }

    protected String preferredID() {
        String name;
        Class clazz = this.getClass();
        Description id = clazz.getAnnotation(Description.class);
        if (id != null) {
            return id.preferredID();
        }
        if (this.getPersistenceType() != 2 && warnedTCPIClasses.add(clazz)) {
            Logger.getAnonymousLogger().warning(clazz.getName() + " should override preferredID()");
        }
        if ((name = this.getName()) == null) {
            int ind = clazz.getName().lastIndexOf(46);
            name = ind == -1 ? clazz.getName() : clazz.getName().substring(ind + 1);
        }
        return name;
    }

    protected void componentOpened() {
        this.openNotify();
    }

    protected void componentClosed() {
        this.closeNotify();
    }

    protected void componentShowing() {
    }

    protected void componentHidden() {
    }

    protected void componentActivated() {
    }

    protected void componentDeactivated() {
    }

    @Override
    public void requestFocus() {
        if (this.isFocusable()) {
            super.requestFocus();
        }
    }

    @Override
    public boolean requestFocusInWindow() {
        if (this.isFocusable()) {
            return super.requestFocusInWindow();
        }
        return false;
    }

    public void requestActive() {
        WindowManager.getDefault().topComponentRequestActive(this);
    }

    public void toFront() {
        WindowManager.getDefault().topComponentToFront(this);
    }

    public void requestVisible() {
        WindowManager.getDefault().topComponentRequestVisible(this);
        GlobalActionContextImpl.blickActionMap(this.getActionMap());
    }

    public final void requestAttention(final boolean brief) {
        EventQueue.invokeLater(new Runnable(){

            @Override
            public void run() {
                if (TopComponent.this.attentionGetter != null && !brief) {
                    TopComponent.this.attentionGetter.kill();
                } else if (!brief) {
                    WindowManager.getDefault().topComponentRequestAttention(TopComponent.this);
                } else if (TopComponent.this.attentionGetter != null) {
                    TopComponent.this.attentionGetter.reset();
                } else {
                    TopComponent.this.attentionGetter = new AttentionGetter();
                }
            }
        });
    }

    public final void makeBusy(final boolean busy) {
        Mutex.EVENT.readAccess(new Runnable(){

            @Override
            public void run() {
                WindowManager.getDefault().topComponentMakeBusy(TopComponent.this, busy);
            }
        });
    }

    public final void cancelRequestAttention() {
        EventQueue.invokeLater(new Runnable(){

            @Override
            public void run() {
                if (TopComponent.this.attentionGetter != null) {
                    TopComponent.this.attentionGetter.stop();
                } else {
                    WindowManager.getDefault().topComponentCancelRequestAttention(TopComponent.this);
                }
            }
        });
    }

    public final void setAttentionHighlight(final boolean highlight) {
        Mutex.EVENT.readAccess(new Runnable(){

            @Override
            public void run() {
                WindowManager.getDefault().topComponentAttentionHighlight(TopComponent.this, highlight);
            }
        });
    }

    @Override
    public void setName(String name) {
        String old = this.getName();
        if (name != null && name.equals(old)) {
            return;
        }
        super.setName(name);
        this.firePropertyChange("name", old, name);
        WindowManager.getDefault().topComponentDisplayNameChanged(this, name);
    }

    public void setDisplayName(String displayName) {
        String old = this.displayName;
        if (displayName == old || displayName != null && displayName.equals(old)) {
            return;
        }
        if (BasicHTML.isHTMLString(displayName)) {
            Logger.getAnonymousLogger().warning("Call of " + this.getClass().getName() + ".setDisplayName(\"" + displayName + "\")" + " shouldn't contain any HTML tags. Please use " + this.getClass().getName() + ".setHtmlDisplayName(String)" + "for such purpose. For details please see http://www.netbeans.org/issues/show_bug.cgi?id=66777.");
        }
        this.displayName = displayName;
        this.firePropertyChange("displayName", old, displayName);
        WindowManager.getDefault().topComponentDisplayNameChanged(this, displayName);
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public String getShortName() {
        return null;
    }

    public void setHtmlDisplayName(String htmlDisplayName) {
        String old = this.htmlDisplayName;
        if (htmlDisplayName == old || htmlDisplayName != null && htmlDisplayName.equals(old)) {
            return;
        }
        this.htmlDisplayName = htmlDisplayName;
        this.firePropertyChange("htmlDisplayName", old, htmlDisplayName);
        WindowManager.getDefault().topComponentHtmlDisplayNameChanged(this, htmlDisplayName);
    }

    public String getHtmlDisplayName() {
        return this.htmlDisplayName;
    }

    @Override
    public void setToolTipText(String toolTip) {
        if (toolTip != null && toolTip.equals(this.getToolTipText())) {
            return;
        }
        super.setToolTipText(toolTip);
        WindowManager.getDefault().topComponentToolTipChanged(this, toolTip);
    }

    public void setIcon(Image icon) {
        if (icon == this.icon) {
            return;
        }
        Image old = this.icon;
        this.icon = icon;
        WindowManager.getDefault().topComponentIconChanged(this, this.icon);
        this.firePropertyChange("icon", old, icon);
    }

    public Image getIcon() {
        Description id;
        if (this.icon == null && (id = this.getClass().getAnnotation(Description.class)) != null) {
            this.icon = ImageUtilities.loadImage((String)id.iconBase(), (boolean)true);
        }
        return this.icon;
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(TopComponent.class);
    }

    public List<Mode> availableModes(List<Mode> modes) {
        return modes;
    }

    @Override
    protected boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition, boolean pressed) {
        boolean ret = super.processKeyBinding(ks, e, condition, pressed);
        if (1 == condition && !ret && !e.isConsumed()) {
            Action action;
            Object value;
            Keymap km = (Keymap)Lookup.getDefault().lookup(Keymap.class);
            Action action2 = action = km != null ? km.getAction(ks) : null;
            if (action == null) {
                return false;
            }
            if (action instanceof ContextAwareAction) {
                Action delegate = ((ContextAwareAction)action).createContextAwareInstance(this.getLookup());
                assert (delegate != null);
                if (delegate.isEnabled() && this.getActivatedNodes() != null) {
                    action = delegate;
                }
            } else if (SwingUtilities.getWindowAncestor(e.getComponent()) instanceof Dialog && !Boolean.TRUE.equals(value = action.getValue("OpenIDE-Transmodal-Action"))) {
                return false;
            }
            if (action.isEnabled()) {
                LogRecord rec = new LogRecord(Level.FINER, "UI_ACTION_KEY_PRESS");
                rec.setParameters(new Object[]{ks, ks.toString(), action.toString(), action.getClass().getName(), action.getValue("Name")});
                rec.setResourceBundle(NbBundle.getBundle(TopComponent.class));
                rec.setLoggerName(UILOG.getName());
                UILOG.log(rec);
                ActionEvent ev = new ActionEvent(this, 1001, Utilities.keyToString((KeyStroke)ks));
                action.actionPerformed(ev);
            } else {
                Utilities.disabledActionBeep();
            }
            return true;
        }
        return ret;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(new Short(this.serialVersion));
        out.writeInt(this.closeOperation);
        out.writeObject(this.getName());
        out.writeObject(this.getToolTipText());
        if (this.getDisplayName() != null) {
            out.writeObject(this.getDisplayName());
        }
        Node n = this.nodeName == null ? null : this.nodeName.getNode();
        Node.Handle h = n == null ? null : n.getHandle();
        out.writeObject((Object)h);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        Object firstObject = in.readObject();
        if (firstObject instanceof Integer) {
            this.serialVersion = 0;
            this.closeOperation = (Integer)firstObject;
            in.readObject();
            super.setName((String)in.readObject());
            this.setToolTipText((String)in.readObject());
        } else {
            Node.Handle h;
            this.serialVersion = (Short)firstObject;
            this.closeOperation = in.readInt();
            super.setName((String)in.readObject());
            this.setToolTipText((String)in.readObject());
            Object obj = in.readObject();
            if (obj instanceof String) {
                this.setDisplayName((String)obj);
                obj = in.readObject();
            }
            if ((h = (Node.Handle)obj) != null) {
                Node n = h.getNode();
                NodeName.connect(this, n);
            }
        }
        if (this.closeOperation != 0 && this.closeOperation != 1) {
            throw new IOException("invalid closeOperation: " + this.closeOperation);
        }
    }

    protected Object writeReplace() throws ObjectStreamException {
        return new Replacer(this);
    }

    @Override
    public AccessibleContext getAccessibleContext() {
        if (this.accessibleContext == null) {
            this.accessibleContext = new JComponent.AccessibleJComponent(){

                @Override
                public AccessibleRole getAccessibleRole() {
                    return AccessibleRole.PANEL;
                }

                @Override
                public String getAccessibleName() {
                    if (this.accessibleName != null) {
                        return this.accessibleName;
                    }
                    return TopComponent.this.getName();
                }

                @Override
                public String getToolTipText() {
                    return TopComponent.this.getToolTipText();
                }
            };
        }
        return this.accessibleContext;
    }

    public Lookup getLookup() {
        return this.getLookup(true);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Lookup getLookup(boolean init) {
        Object object = defaultLookupLock;
        synchronized (object) {
            Object l;
            if (this.defaultLookupRef instanceof Lookup) {
                return (Lookup)this.defaultLookupRef;
            }
            if (this.defaultLookupRef instanceof Object[]) {
                return (Lookup)((Object[])this.defaultLookupRef)[0];
            }
            if (this.defaultLookupRef instanceof Reference && (l = ((Reference)this.defaultLookupRef).get()) instanceof Lookup) {
                return (Lookup)l;
            }
            if (!init) {
                return null;
            }
            DefaultTopComponentLookup lookup = new DefaultTopComponentLookup(this);
            this.defaultLookupRef = new WeakReference<DefaultTopComponentLookup>(lookup);
            return lookup;
        }
    }

    protected final void associateLookup(Lookup lookup) {
        this.setLookup(lookup, true);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void setLookup(Lookup lookup, boolean sync) {
        Object object = defaultLookupLock;
        synchronized (object) {
            if (this.defaultLookupRef != null) {
                throw new IllegalStateException("Trying to set lookup " + (Object)lookup + " but there already is " + this.defaultLookupRef + " for component: " + this);
            }
            this.defaultLookupRef = lookup;
            if (sync) {
                this.defaultLookupRef = new Object[]{this.defaultLookupRef, new SynchronizeNodes(lookup)};
            }
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("setLookup with " + (Object)lookup + " and sync: " + sync + " on " + this);
            }
        }
    }

    private void attachNodeName(NodeName nodeName) {
        this.nodeName = nodeName;
    }

    public SubComponent[] getSubComponents() {
        return new SubComponent[0];
    }

    private static class CloseWindowAction
    extends AbstractAction {
        DelegateActionMap am;

        public CloseWindowAction(DelegateActionMap am) {
            this.am = am;
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            TopComponent self = (TopComponent)this.am.getComponent();
            if (self != null) {
                self.close();
            }
        }
    }

    private static class CloneWindowAction
    extends AbstractAction {
        DelegateActionMap am;

        public CloneWindowAction(DelegateActionMap am) {
            this.am = am;
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            Cloneable self = (Cloneable)((Object)this.am.getComponent());
            if (self != null) {
                Mode m;
                TopComponent cloned = self.cloneComponent();
                int openIndex = -1;
                if (self instanceof TopComponent && null != (m = WindowManager.getDefault().findMode((TopComponent)((Object)self)))) {
                    TopComponent[] tcs = m.getTopComponents();
                    for (int i = 0; i < tcs.length; ++i) {
                        if (tcs[i] != self) continue;
                        openIndex = i + 1;
                        break;
                    }
                    if (openIndex >= tcs.length) {
                        openIndex = -1;
                    }
                }
                if (openIndex >= 0) {
                    cloned.openAtTabPosition(openIndex);
                } else {
                    cloned.open();
                }
                cloned.requestActive();
            }
        }
    }

    private class SynchronizeNodes
    implements LookupListener,
    Runnable {
        private Lookup.Result<Node> res;

        public SynchronizeNodes(Lookup l) {
            this.res = l.lookup(new Lookup.Template(Node.class));
            this.res.addLookupListener((LookupListener)this);
            this.resultChanged(null);
        }

        public void resultChanged(LookupEvent ev) {
            boolean l = TopComponent.LOG.isLoggable(Level.FINE);
            if (l) {
                TopComponent.LOG.fine("lookup changed for " + TopComponent.this + " is visible: " + TopComponent.this.isVisible());
            }
            if (TopComponent.this.isVisible() && EventQueue.isDispatchThread()) {
                this.run();
            } else {
                EventQueue.invokeLater(this);
            }
            if (l) {
                TopComponent.LOG.fine("lookup changed exit " + TopComponent.this);
            }
        }

        @Override
        public void run() {
            boolean l = TopComponent.LOG.isLoggable(Level.FINE);
            Collection nodes = this.res.allInstances();
            if (l) {
                TopComponent.LOG.fine("setting nodes for " + TopComponent.this + " to " + nodes);
            }
            TopComponent.this.setActivatedNodesImpl(nodes.toArray((T[])new Node[0]));
            if (l) {
                TopComponent.LOG.fine("setting nodes done for " + TopComponent.this + " to " + nodes);
            }
        }
    }

    private static final class Replacer
    implements Serializable {
        static final long serialVersionUID = -8897067133215740572L;
        transient TopComponent tc;

        public Replacer(TopComponent tc) {
            this.tc = tc;
        }

        private void writeObject(ObjectOutputStream oos) throws IOException, ClassNotFoundException {
            oos.writeObject(this.tc.getClass().getName());
            this.tc.writeExternal(oos);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
            block12 : {
                String name = (String)ois.readObject();
                name = Utilities.translate((String)name);
                try {
                    Class<?>[] result;
                    ClassLoader loader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
                    if (loader == null) {
                        loader = this.getClass().getClassLoader();
                    }
                    Class tcClass = Class.forName(name, true, loader);
                    Constructor con = tcClass.getDeclaredConstructor(new Class[0]);
                    con.setAccessible(true);
                    try {
                        this.tc = (TopComponent)con.newInstance(new Object[0]);
                    }
                    finally {
                        con.setAccessible(false);
                    }
                    this.tc.readExternal(ois);
                    Method resolveMethod = Replacer.findReadResolveMethod(tcClass);
                    if (resolveMethod == null || (result = resolveMethod.getExceptionTypes()).length != 1 || !ObjectStreamException.class.equals(result[0]) || !Object.class.equals(resolveMethod.getReturnType())) break block12;
                    resolveMethod.setAccessible(true);
                    try {
                        TopComponent unresolvedTc = this.tc;
                        this.tc = (TopComponent)resolveMethod.invoke(this.tc, new Object[0]);
                        if (this.tc == null) {
                            throw new InvalidObjectException("TopComponent.readResolve() cannot return null. See http://www.netbeans.org/issues/show_bug.cgi?id=27849 for more info. TopComponent:" + unresolvedTc);
                        }
                    }
                    finally {
                        resolveMethod.setAccessible(false);
                    }
                }
                catch (Exception exc) {
                    Throwable th = exc;
                    if (th instanceof InvocationTargetException) {
                        th = ((InvocationTargetException)th).getTargetException();
                    }
                    if (th instanceof IOException) {
                        throw (IOException)th;
                    }
                    throw (IOException)new IOException(th.toString()).initCause(th);
                }
            }
        }

        private Object readResolve() throws ObjectStreamException {
            return this.tc;
        }

        private static Method findReadResolveMethod(Class clazz) {
            Method result = null;
            Class[] params = new Class[]{};
            try {
                result = clazz.getMethod("readResolve", params);
            }
            catch (NoSuchMethodException exc) {
                for (Class i = clazz; i != null && i != TopComponent.class; i = i.getSuperclass()) {
                    try {
                        result = i.getDeclaredMethod("readResolve", params);
                        break;
                    }
                    catch (NoSuchMethodException exc2) {
                        continue;
                    }
                }
            }
            return result;
        }
    }

    @Deprecated
    public static class NodeName
    extends NodeAdapter {
        private TopComponent top;
        private Reference node;
        private NodeListener nodeL;

        public static void connect(TopComponent top, Node n) {
            new NodeName(top).attach(n);
        }

        @Deprecated
        public NodeName(TopComponent top) {
            this.top = top;
        }

        @Deprecated
        public void propertyChange(PropertyChangeEvent ev) {
            Node n;
            if (ev.getPropertyName().equals("displayName") && (n = (Node)this.node.get()) != null) {
                Mutex.EVENT.readAccess(new Runnable(){

                    @Override
                    public void run() {
                        NodeName.this.top.setName(n.getDisplayName());
                    }
                });
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void attach(Node n) {
            TopComponent topComponent = this.top;
            synchronized (topComponent) {
                this.node = new WeakReference<Node>(n);
                this.nodeL = (NodeListener)WeakListeners.create(NodeListener.class, (EventListener)((Object)this), (Object)n);
                n.addNodeListener(this.nodeL);
                this.top.attachNodeName(this);
                this.top.setActivatedNodes(new Node[]{n});
                this.top.setName(n.getDisplayName());
            }
        }

        private Node getNode() {
            return (Node)this.node.get();
        }

    }

    private class AttentionGetter
    implements ActionListener {
        Timer timer;

        public AttentionGetter() {
            this.timer = null;
            this.reset();
        }

        public void reset() {
            assert (EventQueue.isDispatchThread());
            if (this.timer != null) {
                this.timer.stop();
            }
            this.start();
            this.timer = new Timer(3500, this);
            this.timer.setRepeats(false);
            this.timer.start();
        }

        private void start() {
            WindowManager.getDefault().topComponentRequestAttention(TopComponent.this);
        }

        public void kill() {
            this.timer.stop();
            TopComponent.this.attentionGetter = null;
        }

        private void stop() {
            if (this.timer != null) {
                this.timer.stop();
            }
            TopComponent.this.attentionGetter = null;
            WindowManager.getDefault().topComponentCancelRequestAttention(TopComponent.this);
            TopComponent.this.attentionGetter = null;
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            this.stop();
        }
    }

    public static final class SubComponent {
        private final String displayName;
        private final String description;
        private final boolean active;
        private final ActionListener activator;
        private final Lookup lookup;
        private final boolean showing;

        public SubComponent(String displayName, ActionListener activator, boolean active) {
            this(displayName, null, activator, active);
        }

        public SubComponent(String displayName, String description, ActionListener activator, boolean active) {
            this(displayName, description, activator, active, Lookup.EMPTY, false);
        }

        public SubComponent(String displayName, String description, ActionListener activator, boolean active, Lookup lookup, boolean showing) {
            this.displayName = displayName;
            this.description = description;
            this.active = active;
            this.activator = activator;
            this.lookup = lookup;
            this.showing = showing;
        }

        public Lookup getLookup() {
            return this.lookup;
        }

        public boolean isShowing() {
            return this.showing;
        }

        public final boolean isActive() {
            return this.active;
        }

        public final String getDescription() {
            return this.description;
        }

        public final String getDisplayName() {
            return this.displayName;
        }

        public final void activate() {
            this.activator.actionPerformed(new ActionEvent(this, 0, "activate"));
        }
    }

    public static interface Registry {
        public static final String PROP_OPENED = "opened";
        public static final String PROP_ACTIVATED = "activated";
        public static final String PROP_CURRENT_NODES = "currentNodes";
        public static final String PROP_ACTIVATED_NODES = "activatedNodes";
        public static final String PROP_TC_OPENED = "tcOpened";
        public static final String PROP_TC_CLOSED = "tcClosed";

        public Set<TopComponent> getOpened();

        public TopComponent getActivated();

        public Node[] getCurrentNodes();

        public Node[] getActivatedNodes();

        public void addPropertyChangeListener(PropertyChangeListener var1);

        public void removePropertyChangeListener(PropertyChangeListener var1);
    }

    @Retention(value=RetentionPolicy.SOURCE)
    @Target(value={ElementType.TYPE, ElementType.METHOD})
    public static @interface OpenActionRegistration {
        public String displayName();

        public String preferredID() default "";
    }

    @Retention(value=RetentionPolicy.SOURCE)
    @Target(value={ElementType.TYPE, ElementType.METHOD})
    public static @interface Registration {
        public String mode();

        public int position() default Integer.MAX_VALUE;

        public boolean openAtStartup();

        public String[] roles() default {};
    }

    @Retention(value=RetentionPolicy.RUNTIME)
    @Target(value={ElementType.TYPE})
    public static @interface Description {
        public String preferredID();

        public String iconBase() default "";

        public int persistenceType() default 0;
    }

    public static interface Cloneable {
        public TopComponent cloneComponent();
    }

}

