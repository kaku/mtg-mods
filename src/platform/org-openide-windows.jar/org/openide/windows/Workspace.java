/*
 * Decompiled with CFR 0_118.
 */
package org.openide.windows;

import java.awt.Rectangle;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.net.URL;
import java.util.Set;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public interface Workspace
extends Serializable {
    public static final String PROP_MODES = "modes";
    public static final String PROP_NAME = "name";
    public static final String PROP_DISPLAY_NAME = "displayName";
    public static final long serialVersionUID = 2987897537843190271L;

    public String getName();

    public String getDisplayName();

    public Set<? extends Mode> getModes();

    public Rectangle getBounds();

    public void activate();

    public Mode createMode(String var1, String var2, URL var3);

    public Mode findMode(String var1);

    public Mode findMode(TopComponent var1);

    public void remove();

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

