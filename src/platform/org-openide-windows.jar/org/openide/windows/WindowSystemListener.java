/*
 * Decompiled with CFR 0_118.
 */
package org.openide.windows;

import java.util.EventListener;
import org.openide.windows.WindowSystemEvent;

public interface WindowSystemListener
extends EventListener {
    public void beforeLoad(WindowSystemEvent var1);

    public void afterLoad(WindowSystemEvent var1);

    public void beforeSave(WindowSystemEvent var1);

    public void afterSave(WindowSystemEvent var1);
}

