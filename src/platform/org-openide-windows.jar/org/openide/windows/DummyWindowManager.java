/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.util.Utilities
 *  org.openide.util.actions.SystemAction
 */
package org.openide.windows;

import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.WeakHashMap;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import org.openide.nodes.Node;
import org.openide.util.Utilities;
import org.openide.util.actions.SystemAction;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.TopComponentGroup;
import org.openide.windows.WindowManager;
import org.openide.windows.Workspace;

final class DummyWindowManager
extends WindowManager {
    private static final boolean VISIBLE = Boolean.parseBoolean(System.getProperty("org.openide.windows.DummyWindowManager.VISIBLE", "true"));
    private static final long serialVersionUID = 1;
    private static Action[] DEFAULT_ACTIONS_CLONEABLE;
    private static Action[] DEFAULT_ACTIONS_NOT_CLONEABLE;
    private final Map<String, Workspace> workspaces = new TreeMap<String, Workspace>();
    private transient Frame mw;
    private transient PropertyChangeSupport pcs;

    public DummyWindowManager() {
        this.createWorkspace("default", null).createMode("editor", "editor", null);
    }

    @Override
    public synchronized void addPropertyChangeListener(PropertyChangeListener l) {
        if (this.pcs == null) {
            this.pcs = new PropertyChangeSupport(this);
        }
        this.pcs.addPropertyChangeListener(l);
    }

    @Override
    public synchronized void removePropertyChangeListener(PropertyChangeListener l) {
        if (this.pcs != null) {
            this.pcs.removePropertyChangeListener(l);
        }
    }

    @Override
    protected TopComponent.Registry componentRegistry() {
        return new R();
    }

    private R registry() {
        return (R)this.getRegistry();
    }

    @Override
    protected WindowManager.Component createTopComponentManager(TopComponent c) {
        return null;
    }

    @Override
    public synchronized Workspace createWorkspace(String name, String displayName) {
        W w = new W(name);
        this.workspaces.put(name, w);
        if (this.pcs != null) {
            this.pcs.firePropertyChange("workspaces", null, null);
            this.pcs.firePropertyChange("currentWorkspace", null, null);
        }
        return w;
    }

    synchronized void delete(Workspace w) {
        this.workspaces.remove(w.getName());
        if (this.workspaces.isEmpty()) {
            this.createWorkspace("default", null);
        }
        if (this.pcs != null) {
            this.pcs.firePropertyChange("workspaces", null, null);
            this.pcs.firePropertyChange("currentWorkspace", null, null);
        }
    }

    @Override
    public synchronized Workspace findWorkspace(String name) {
        return this.workspaces.get(name);
    }

    @Override
    public synchronized Workspace getCurrentWorkspace() {
        return this.workspaces.values().iterator().next();
    }

    @Override
    public synchronized Workspace[] getWorkspaces() {
        return this.workspaces.values().toArray(new Workspace[0]);
    }

    @Override
    public synchronized void setWorkspaces(Workspace[] ws) {
        if (ws.length == 0) {
            throw new IllegalArgumentException();
        }
        this.workspaces.clear();
        for (int i = 0; i < ws.length; ++i) {
            this.workspaces.put(ws[i].getName(), ws[i]);
        }
        if (this.pcs != null) {
            this.pcs.firePropertyChange("workspaces", null, null);
            this.pcs.firePropertyChange("currentWorkspace", null, null);
        }
    }

    @Override
    public synchronized Frame getMainWindow() {
        if (this.mw == null) {
            this.mw = new JFrame("dummy");
        }
        return this.mw;
    }

    @Override
    public void updateUI() {
    }

    public Set<Mode> getModes() {
        HashSet<Mode> s = new HashSet<Mode>();
        for (Workspace w : new HashSet<Workspace>(this.workspaces.values())) {
            s.addAll(w.getModes());
        }
        return s;
    }

    @Override
    public Mode findMode(TopComponent tc) {
        for (Mode m : this.getModes()) {
            if (!Arrays.asList(m.getTopComponents()).contains(tc)) continue;
            return m;
        }
        return null;
    }

    @Override
    public Mode findMode(String name) {
        if (name == null) {
            return null;
        }
        for (Mode m : this.getModes()) {
            if (!name.equals(m.getName())) continue;
            return m;
        }
        return null;
    }

    @Override
    public TopComponentGroup findTopComponentGroup(String name) {
        return null;
    }

    @Override
    public TopComponent findTopComponent(String tcID) {
        return null;
    }

    @Override
    protected String topComponentID(TopComponent tc, String preferredID) {
        return preferredID;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected Action[] topComponentDefaultActions(TopComponent tc) {
        Class<DummyWindowManager> class_ = DummyWindowManager.class;
        synchronized (DummyWindowManager.class) {
            if (tc instanceof TopComponent.Cloneable) {
                if (DEFAULT_ACTIONS_CLONEABLE == null) {
                    DEFAULT_ACTIONS_CLONEABLE = DummyWindowManager.loadActions(new String[]{"Save", "CloneView", null, "CloseView"});
                }
                // ** MonitorExit[var2_2] (shouldn't be in output)
                return DEFAULT_ACTIONS_CLONEABLE;
            }
            if (DEFAULT_ACTIONS_NOT_CLONEABLE == null) {
                DEFAULT_ACTIONS_NOT_CLONEABLE = DummyWindowManager.loadActions(new String[]{"Save", null, "CloseView"});
            }
            // ** MonitorExit[var2_2] (shouldn't be in output)
            return DEFAULT_ACTIONS_NOT_CLONEABLE;
        }
    }

    private static Action[] loadActions(String[] names) {
        ArrayList<SystemAction> arr = new ArrayList<SystemAction>();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        for (int i = 0; i < names.length; ++i) {
            if (names[i] == null) {
                arr.add(null);
                continue;
            }
            try {
                Class<SystemAction> sa = Class.forName("org.openide.actions." + names[i] + "Action", true, loader).asSubclass(SystemAction.class);
                arr.add(SystemAction.get(sa));
                continue;
            }
            catch (ClassNotFoundException e) {
                // empty catch block
            }
        }
        return arr.toArray(new Action[0]);
    }

    @Override
    protected boolean topComponentIsOpened(TopComponent tc) {
        return tc.isShowing() || this.registry().opened.contains(tc);
    }

    @Override
    protected void topComponentActivatedNodesChanged(TopComponent tc, Node[] nodes) {
        this.registry().setActivatedNodes(tc, nodes);
    }

    @Override
    protected void topComponentIconChanged(TopComponent tc, Image icon) {
        JFrame f = (JFrame)SwingUtilities.getAncestorOfClass(JFrame.class, tc);
        if (f != null) {
            f.setIconImage(icon);
        }
    }

    @Override
    protected void topComponentToolTipChanged(TopComponent tc, String tooltip) {
    }

    @Override
    protected void topComponentDisplayNameChanged(TopComponent tc, String displayName) {
        JFrame f = (JFrame)SwingUtilities.getAncestorOfClass(JFrame.class, tc);
        if (f != null) {
            f.setTitle(displayName);
        }
    }

    @Override
    protected void topComponentHtmlDisplayNameChanged(TopComponent tc, String htmlDisplayName) {
    }

    @Override
    protected void topComponentOpen(TopComponent tc) {
        JFrame f = (JFrame)SwingUtilities.getAncestorOfClass(JFrame.class, tc);
        if (f == null) {
            f = new JFrame(tc.getName());
            Image icon = tc.getIcon();
            if (icon != null) {
                f.setIconImage(icon);
            }
            f.getContentPane().add(tc);
            f.pack();
            final WeakReference<TopComponent> ref = new WeakReference<TopComponent>(tc);
            f.addWindowListener(new WindowAdapter(){

                @Override
                public void windowClosing(WindowEvent ev) {
                    TopComponent tc = (TopComponent)ref.get();
                    if (tc == null) {
                        return;
                    }
                    tc.close();
                }

                @Override
                public void windowActivated(WindowEvent e) {
                    TopComponent tc = (TopComponent)ref.get();
                    if (tc == null) {
                        return;
                    }
                    tc.requestActive();
                }
            });
        }
        if (!tc.isShowing()) {
            this.componentOpenNotify(tc);
            this.componentShowing(tc);
            if (VISIBLE) {
                f.setVisible(true);
            }
            this.registry().open(tc);
        }
    }

    @Override
    protected void topComponentClose(TopComponent tc) {
        if (!tc.canClose()) {
            return;
        }
        this.componentHidden(tc);
        this.componentCloseNotify(tc);
        JFrame f = (JFrame)SwingUtilities.getAncestorOfClass(JFrame.class, tc);
        if (f != null) {
            if (VISIBLE) {
                f.setVisible(false);
            }
            tc.getParent().remove(tc);
        }
        this.registry().close(tc);
        for (W w : this.workspaces.values()) {
            w.close(tc);
        }
    }

    @Override
    protected void topComponentRequestVisible(TopComponent tc) {
        JFrame f = (JFrame)SwingUtilities.getAncestorOfClass(JFrame.class, tc);
        if (f != null && VISIBLE) {
            f.setVisible(true);
        }
    }

    @Override
    protected void topComponentRequestActive(TopComponent tc) {
        JFrame f = (JFrame)SwingUtilities.getAncestorOfClass(JFrame.class, tc);
        if (f != null) {
            f.toFront();
        }
        this.registry().setActive(tc);
        this.activateComponent(tc);
    }

    @Override
    protected void topComponentRequestAttention(TopComponent tc) {
    }

    @Override
    protected void topComponentCancelRequestAttention(TopComponent tc) {
    }

    @Override
    public boolean isEditorTopComponent(TopComponent tc) {
        Mode md = this.findMode(tc);
        if (md != null && this.isEditorMode(md)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isOpenedEditorTopComponent(TopComponent tc) {
        Mode md = this.findMode(tc);
        if (md != null && this.isEditorMode(md)) {
            return tc.isOpened();
        }
        return super.isOpenedEditorTopComponent(tc);
    }

    @Override
    public boolean isEditorMode(Mode mode) {
        if (null == mode) {
            return false;
        }
        return "editor".equals(mode.getName());
    }

    private static final class R
    implements TopComponent.Registry {
        private Reference<TopComponent> active = new WeakReference<Object>(null);
        private final Set<TopComponent> opened;
        private Node[] nodes;
        private PropertyChangeSupport pcs;

        public R() {
            this.opened = new HashSet<TopComponent>(){

                @Override
                public Iterator<TopComponent> iterator() {
                    HashSet<TopComponent> copy = new HashSet<TopComponent>();
                    Iterator it = HashSet.super.iterator();
                    while (it.hasNext()) {
                        TopComponent topComponent = (TopComponent)it.next();
                        copy.add(topComponent);
                    }
                    return copy.iterator();
                }
            };
            this.nodes = new Node[0];
        }

        @Override
        public synchronized void addPropertyChangeListener(PropertyChangeListener l) {
            if (this.pcs == null) {
                this.pcs = new PropertyChangeSupport(this);
            }
            this.pcs.addPropertyChangeListener(l);
        }

        @Override
        public synchronized void removePropertyChangeListener(PropertyChangeListener l) {
            if (this.pcs != null) {
                this.pcs.removePropertyChangeListener(l);
            }
        }

        synchronized void open(TopComponent tc) {
            this.opened.add(tc);
            if (this.pcs != null) {
                this.pcs.firePropertyChange("tcOpened", null, tc);
                this.pcs.firePropertyChange("opened", null, null);
            }
        }

        synchronized void close(TopComponent tc) {
            this.opened.remove(tc);
            if (this.pcs != null) {
                this.pcs.firePropertyChange("tcClosed", null, tc);
                this.pcs.firePropertyChange("opened", null, null);
            }
            if (this.getActive() == tc) {
                this.setActive(null);
            }
        }

        @Override
        public synchronized Set<TopComponent> getOpened() {
            return Collections.unmodifiableSet(this.opened);
        }

        synchronized void setActive(TopComponent tc) {
            Node[] _nodes;
            this.active = new WeakReference<TopComponent>(tc);
            Node[] arrnode = _nodes = tc == null ? new Node[]{} : tc.getActivatedNodes();
            if (_nodes != null) {
                this.nodes = _nodes;
                if (this.pcs != null) {
                    this.pcs.firePropertyChange("activatedNodes", null, null);
                }
            }
            if (this.pcs != null) {
                this.pcs.firePropertyChange("activated", null, null);
                this.pcs.firePropertyChange("currentNodes", null, null);
            }
        }

        synchronized void setActivatedNodes(TopComponent tc, Node[] _nodes) {
            if (tc == this.getActive()) {
                if (_nodes != null) {
                    this.nodes = _nodes;
                    if (this.pcs != null) {
                        this.pcs.firePropertyChange("activatedNodes", null, null);
                    }
                }
                if (this.pcs != null) {
                    this.pcs.firePropertyChange("currentNodes", null, null);
                }
            }
        }

        @Override
        public TopComponent getActivated() {
            return this.getActive();
        }

        @Override
        public Node[] getActivatedNodes() {
            return this.nodes;
        }

        @Override
        public synchronized Node[] getCurrentNodes() {
            if (this.getActive() != null) {
                return this.getActive().getActivatedNodes();
            }
            return null;
        }

        private TopComponent getActive() {
            return this.active.get();
        }

    }

    private final class W
    implements Workspace {
        private static final long serialVersionUID = 1;
        private final String name;
        private final Map<String, Mode> modes;
        private final Map<TopComponent, Mode> modesByComponent;
        private transient PropertyChangeSupport pcs;

        public W(String name) {
            this.modes = new HashMap<String, Mode>();
            this.modesByComponent = new WeakHashMap<TopComponent, Mode>();
            this.name = name;
        }

        @Override
        public void activate() {
        }

        @Override
        public synchronized void addPropertyChangeListener(PropertyChangeListener list) {
            if (this.pcs == null) {
                this.pcs = new PropertyChangeSupport(this);
            }
            this.pcs.addPropertyChangeListener(list);
        }

        @Override
        public synchronized void removePropertyChangeListener(PropertyChangeListener list) {
            if (this.pcs != null) {
                this.pcs.removePropertyChangeListener(list);
            }
        }

        @Override
        public void remove() {
            DummyWindowManager.this.delete(this);
        }

        @Override
        public synchronized Mode createMode(String name, String displayName, URL icon) {
            M m = new M(name);
            this.modes.put(name, m);
            if (this.pcs != null) {
                this.pcs.firePropertyChange("modes", null, null);
            }
            return m;
        }

        public synchronized Set<Mode> getModes() {
            return new HashSet<Mode>(this.modes.values());
        }

        @Override
        public synchronized Mode findMode(String name) {
            return this.modes.get(name);
        }

        @Override
        public synchronized Mode findMode(TopComponent c) {
            return this.modesByComponent.get(c);
        }

        synchronized void dock(Mode m, TopComponent c) {
            this.modesByComponent.put(c, m);
        }

        @Override
        public Rectangle getBounds() {
            return Utilities.getUsableScreenBounds();
        }

        @Override
        public String getName() {
            return this.name;
        }

        @Override
        public String getDisplayName() {
            return this.getName();
        }

        public void close(TopComponent tc) {
            for (M m : this.modes.values()) {
                m.close(tc);
            }
        }

        private final class M
        implements Mode {
            private static final long serialVersionUID = 1;
            private final String name;
            private final Set<TopComponent> components;

            public M(String name) {
                this.components = new HashSet<TopComponent>();
                this.name = name;
            }

            public void close(TopComponent tc) {
                this.components.remove(tc);
            }

            @Override
            public void addPropertyChangeListener(PropertyChangeListener l) {
            }

            @Override
            public void removePropertyChangeListener(PropertyChangeListener l) {
            }

            @Override
            public boolean canDock(TopComponent tc) {
                return true;
            }

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public synchronized boolean dockInto(TopComponent c) {
                if (this.components.add(c)) {
                    Mode old = W.this.findMode(c);
                    if (old != null && old != this && old instanceof M) {
                        Mode mode = old;
                        synchronized (mode) {
                            ((M)old).components.remove(c);
                        }
                    }
                    W.this.dock(this, c);
                }
                return true;
            }

            @Override
            public String getName() {
                return this.name;
            }

            @Override
            public String getDisplayName() {
                return this.getName();
            }

            @Override
            public Image getIcon() {
                return null;
            }

            @Override
            public synchronized TopComponent[] getTopComponents() {
                return this.components.toArray(new TopComponent[0]);
            }

            @Override
            public Workspace getWorkspace() {
                return W.this;
            }

            @Override
            public synchronized Rectangle getBounds() {
                return W.this.getBounds();
            }

            @Override
            public void setBounds(Rectangle s) {
            }

            @Override
            public TopComponent getSelectedTopComponent() {
                TopComponent[] tcs = this.components.toArray(new TopComponent[0]);
                return tcs.length > 0 ? tcs[0] : null;
            }
        }

    }

}

