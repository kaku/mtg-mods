/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.ContextGlobalProvider
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.Utilities
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.modules.openide.windows;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.KeyboardFocusManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Collection;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import org.openide.util.ContextAwareAction;
import org.openide.util.ContextGlobalProvider;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import org.openide.windows.TopComponent;

public final class GlobalActionContextImpl
implements ContextGlobalProvider,
Lookup.Provider,
PropertyChangeListener,
Runnable {
    private TopComponent.Registry registry;
    private static Reference<Component> focusOwner;
    private static volatile Lookup temporary;

    public GlobalActionContextImpl() {
        this(TopComponent.getRegistry());
    }

    public GlobalActionContextImpl(TopComponent.Registry r) {
        this.registry = r;
        if (EventQueue.isDispatchThread()) {
            this.run();
        } else {
            EventQueue.invokeLater(this);
        }
    }

    @Override
    public void run() {
        KeyboardFocusManager m = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        m.removePropertyChangeListener("permanentFocusOwner", this);
        m.addPropertyChangeListener("permanentFocusOwner", this);
        GlobalActionContextImpl.setFocusOwner(m.getPermanentFocusOwner());
    }

    public static void blickActionMap(ActionMap map) {
        GlobalActionContextImpl.blickActionMap(map, null);
    }

    private static void blickActionMap(final ActionMap map, final Component[] focus) {
        if (EventQueue.isDispatchThread()) {
            GlobalActionContextImpl.blickActionMapImpl(map, focus);
        } else {
            EventQueue.invokeLater(new Runnable(){

                @Override
                public void run() {
                    GlobalActionContextImpl.blickActionMapImpl(map, focus);
                }
            });
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void blickActionMapImpl(ActionMap map, Component[] focus) {
        assert (EventQueue.isDispatchThread());
        Object obj = Lookup.getDefault().lookup(ContextGlobalProvider.class);
        if (obj instanceof GlobalActionContextImpl) {
            GlobalActionContextImpl g = (GlobalActionContextImpl)obj;
            Lookup[] arrlookup = new Lookup[2];
            arrlookup[0] = map == null ? Lookup.EMPTY : Lookups.singleton((Object)map);
            arrlookup[1] = Lookups.exclude((Lookup)g.getLookup(), (Class[])new Class[]{ActionMap.class});
            Lookup[] arr = arrlookup;
            Lookup prev = temporary;
            try {
                temporary = new ProxyLookup(arr);
                Object q = Utilities.actionsGlobalContext().lookup(ActionMap.class);
                assert (q == map);
                if (focus != null) {
                    GlobalActionContextImpl.setFocusOwner(focus[0]);
                }
            }
            finally {
                temporary = prev;
                Utilities.actionsGlobalContext().lookup(ActionMap.class);
            }
        }
    }

    private static String dumpActionMapInfo(ActionMap map, Object q, Lookup prev, Lookup now) {
        StringBuilder sb = new StringBuilder();
        sb.append("We really get map from the lookup. Map: ").append(map).append(" returned: ").append(q);
        sb.append("\nprev: ").append((Object)(prev == null ? "null prev" : prev.lookupAll(Object.class)));
        sb.append("\nnow : ").append((Object)(now == null ? "null now" : now.lookupAll(Object.class)));
        return sb.toString();
    }

    private static void setFocusOwner(Component focus) {
        focusOwner = new WeakReference<Component>(focus);
    }

    public static Component findFocusOwner() {
        if (focusOwner == null) {
            Utilities.actionsGlobalContext();
            if (focusOwner == null) {
                GlobalActionContextImpl.setFocusOwner(null);
            }
        }
        return focusOwner.get();
    }

    public Lookup createGlobalContext() {
        this.registry.addPropertyChangeListener(this);
        return Lookups.proxy((Lookup.Provider)this);
    }

    public Lookup getLookup() {
        Lookup l = temporary;
        if (l != null) {
            return l;
        }
        TopComponent tc = this.registry.getActivated();
        return tc == null ? Lookup.EMPTY : tc.getLookup();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("activated".equals(evt.getPropertyName())) {
            Utilities.actionsGlobalContext().lookup(ActionMap.class);
        }
        if ("permanentFocusOwner".equals(evt.getPropertyName())) {
            Action a;
            Component[] arr = new Component[]{(Component)evt.getNewValue()};
            if (arr[0] instanceof AbstractButton && (a = ((AbstractButton)arr[0]).getAction()) instanceof ContextAwareAction) {
                return;
            }
            GlobalActionContextImpl.blickActionMap(null, arr);
        }
    }

}

