/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.ActionID
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 */
package org.netbeans.modules.openide.windows;

import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ExecutableType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import org.openide.awt.ActionID;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;
import org.openide.windows.TopComponent;

@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
public final class TopComponentProcessor
extends LayerGeneratingProcessor {
    public Set<String> getSupportedAnnotationTypes() {
        HashSet<String> hash = new HashSet<String>();
        hash.add(TopComponent.Registration.class.getCanonicalName());
        hash.add(TopComponent.OpenActionRegistration.class.getCanonicalName());
        hash.add(TopComponent.Description.class.getCanonicalName());
        return hash;
    }

    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        TopComponent.Description info;
        TopComponent.Registration reg;
        for (Element e2 : roundEnv.getElementsAnnotatedWith(TopComponent.Registration.class)) {
            String rootFolder;
            reg = e2.getAnnotation(TopComponent.Registration.class);
            if (reg == null) continue;
            info = this.findInfo(e2);
            if (info == null) {
                throw new LayerGenerationException("Cannot find TopComponent.Description for this element", e2, this.processingEnv, (Annotation)reg);
            }
            String id = info.preferredID();
            TopComponentProcessor.checkValidId(id, e2, this.processingEnv, info);
            String[] roles = reg.roles();
            if (roles.length == 0) {
                rootFolder = "Windows2";
                this.generateSettingsAndWstcref(e2, rootFolder, id, reg, info);
                continue;
            }
            HashSet<String> uniqueRoles = new HashSet<String>();
            for (String role : roles) {
                if (!uniqueRoles.add(role)) {
                    throw new LayerGenerationException("Duplicate role name found", e2, this.processingEnv, (Annotation)reg);
                }
                if (role.isEmpty()) {
                    throw new LayerGenerationException("Unnamed role found", e2, this.processingEnv, (Annotation)reg);
                }
                rootFolder = "Windows2/Roles/" + role;
                this.generateSettingsAndWstcref(e2, rootFolder, id, reg, info);
            }
        }
        for (Element e : roundEnv.getElementsAnnotatedWith(TopComponent.OpenActionRegistration.class)) {
            reg = e.getAnnotation(TopComponent.OpenActionRegistration.class);
            assert (reg != null);
            info = this.findInfo(e);
            ActionID aid = e.getAnnotation(ActionID.class);
            if (aid == null) continue;
            LayerBuilder.File actionFile = this.layer(new Element[]{e}).file("Actions/" + aid.category() + "/" + aid.id().replace('.', '-') + ".instance").methodvalue("instanceCreate", "org.openide.windows.TopComponent", "openAction");
            actionFile.instanceAttribute("component", TopComponent.class, (Annotation)reg, null);
            if (reg.preferredID().length() > 0) {
                actionFile.stringvalue("preferredID", reg.preferredID());
            }
            actionFile.bundlevalue("displayName", reg.displayName(), (Annotation)reg, "displayName");
            if (info != null && info.iconBase().length() > 0) {
                actionFile.stringvalue("iconBase", info.iconBase());
            }
            actionFile.write();
        }
        return true;
    }

    private void generateSettingsAndWstcref(Element e, String rootFolder, String id, TopComponent.Registration reg, TopComponent.Description info) throws LayerGenerationException {
        LayerBuilder.File settingsFile = this.layer(new Element[]{e}).file(rootFolder + "/Components/" + id + ".settings").contents(TopComponentProcessor.settingsFile(e));
        settingsFile.write();
        LayerBuilder.File modeFile = this.layer(new Element[]{e}).file(rootFolder + "/Modes/" + reg.mode() + "/" + id + ".wstcref").position(reg.position()).contents(TopComponentProcessor.modeFile(info.preferredID(), reg.openAtStartup()));
        modeFile.write();
    }

    private TopComponent.Description findInfo(Element e) throws LayerGenerationException {
        Element type;
        switch (e.asType().getKind()) {
            case DECLARED: {
                type = e;
                break;
            }
            case EXECUTABLE: {
                type = ((DeclaredType)((ExecutableType)e.asType()).getReturnType()).asElement();
                break;
            }
            default: {
                throw new LayerGenerationException("" + (Object)((Object)e.asType().getKind()), e);
            }
        }
        TopComponent.Description info = type.getAnnotation(TopComponent.Description.class);
        return info;
    }

    private static String settingsFile(Element e) throws LayerGenerationException {
        String method;
        String clazz;
        switch (e.getKind()) {
            case CLASS: {
                clazz = e.toString();
                method = null;
                break;
            }
            case METHOD: {
                clazz = e.getEnclosingElement().toString();
                method = e.getSimpleName().toString();
                break;
            }
            default: {
                throw new LayerGenerationException("Cannot work on given element", e);
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        sb.append("<!DOCTYPE settings PUBLIC \"-//NetBeans//DTD Session settings 1.0//EN\" \"http://www.netbeans.org/dtds/sessionsettings-1_0.dtd\">\n");
        sb.append("<settings version=\"1.0\">\n");
        sb.append("  <instance class=\"").append(clazz).append("\"");
        if (method != null) {
            sb.append(" method=\"").append(method).append("\"");
        }
        sb.append("/>\n");
        sb.append("</settings>\n");
        return sb.toString();
    }

    private static String modeFile(String id, boolean openAtStart) throws LayerGenerationException {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        sb.append("<!DOCTYPE tc-ref PUBLIC \"-//NetBeans//DTD Top Component in Mode Properties 2.0//EN\" \"http://www.netbeans.org/dtds/tc-ref2_0.dtd\">\n");
        sb.append("<tc-ref version=\"2.0\">\n");
        sb.append("  <tc-id id=\"").append(id).append("\"/>\n");
        sb.append("  <state opened=\"").append(openAtStart).append("\"/>\n");
        sb.append("</tc-ref>\n");
        return sb.toString();
    }

    private static void checkValidId(String id, Element e, ProcessingEnvironment processingEnv, TopComponent.Description descr) throws LayerGenerationException {
        if (null == id) {
            return;
        }
        for (char c : id.toCharArray()) {
            if ((Character.isLetterOrDigit(c) || c == '-' || c == '_') && c <= '~') continue;
            throw new LayerGenerationException("The preferred id contains invalid character '" + c + "'", e, processingEnv, (Annotation)descr);
        }
    }

}

