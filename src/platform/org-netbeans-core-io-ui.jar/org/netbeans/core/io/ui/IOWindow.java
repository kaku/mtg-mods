/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.MouseUtils
 *  org.openide.awt.MouseUtils$PopupMouseAdapter
 *  org.openide.awt.TabbedPaneFactory
 *  org.openide.awt.ToolbarWithOverflow
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.windows.IOContainer
 *  org.openide.windows.IOContainer$CallBacks
 *  org.openide.windows.IOContainer$Provider
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$SubComponent
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.io.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ObjectStreamException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.awt.MouseUtils;
import org.openide.awt.TabbedPaneFactory;
import org.openide.awt.ToolbarWithOverflow;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.windows.IOContainer;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public final class IOWindow
implements IOContainer.Provider {
    private static IOWindowImpl impl;

    IOWindowImpl impl() {
        if (impl == null) {
            impl = IOWindowImpl.findDefault();
        }
        return impl;
    }

    public void add(JComponent comp, IOContainer.CallBacks cb) {
        this.impl().addTab(comp, cb);
    }

    public JComponent getSelected() {
        return this.impl().getSelectedTab();
    }

    public boolean isActivated() {
        return this.impl().isActivated();
    }

    public void open() {
        this.impl().open();
    }

    public void remove(JComponent comp) {
        this.impl().removeTab(comp);
    }

    public void requestActive() {
        this.impl().requestActive();
    }

    public void requestVisible() {
        this.impl().requestVisible();
    }

    public void select(JComponent comp) {
        this.impl().selectTab(comp);
    }

    public void setIcon(JComponent comp, Icon icon) {
        this.impl().setIcon(comp, icon);
    }

    public void setTitle(JComponent comp, String name) {
        this.impl().setTitle(comp, name);
    }

    public void setToolTipText(JComponent comp, String text) {
        this.impl().setToolTipText(comp, text);
    }

    public void setToolbarActions(JComponent comp, Action[] toolbarActions) {
        this.impl().setToolbarActions(comp, toolbarActions);
    }

    public boolean isCloseable(JComponent comp) {
        return true;
    }

    public static final class IOWindowImpl
    extends TopComponent
    implements ChangeListener,
    PropertyChangeListener {
        public static IOWindowImpl DEFAULT;
        private static final String ICON_PROP = "tabIcon";
        private static final String TOOLBAR_ACTIONS_PROP = "toolbarActions";
        private static final String TOOLBAR_BUTTONS_PROP = "toolbarButtons";
        private static final boolean AQUA;
        private JTabbedPane pane = TabbedPaneFactory.createCloseButtonTabbedPane();
        private JComponent singleTab;
        private JToolBar toolbar;
        private JPopupMenu popupMenu;
        private Map<JComponent, IOContainer.CallBacks> tabToCb = new HashMap<JComponent, IOContainer.CallBacks>();
        boolean activated;
        JComponent lastSelTab;

        static synchronized IOWindowImpl findDefault() {
            if (DEFAULT == null) {
                TopComponent tc = WindowManager.getDefault().findTopComponent("output");
                if (tc != null) {
                    if (tc instanceof IOWindowImpl) {
                        DEFAULT = (IOWindowImpl)tc;
                    } else {
                        IllegalStateException exc = new IllegalStateException("Incorrect settings file. Unexpected class returned. Expected: " + IOWindowImpl.class.getName() + " Returned: " + tc.getClass().getName());
                        Logger.getLogger(IOWindowImpl.class.getName()).log(Level.WARNING, null, exc);
                        IOWindowImpl.getDefault();
                    }
                } else {
                    IOWindowImpl.getDefault();
                }
            }
            DEFAULT.getActionMap().remove("org.openide.actions.FindAction");
            return DEFAULT;
        }

        public static synchronized IOWindowImpl getDefault() {
            if (DEFAULT == null) {
                DEFAULT = new IOWindowImpl();
            }
            return DEFAULT;
        }

        public Object readResolve() throws ObjectStreamException {
            return IOWindowImpl.getDefault();
        }

        public IOWindowImpl() {
            this.pane.addChangeListener(this);
            this.pane.addPropertyChangeListener("close", this);
            this.setFocusable(true);
            this.toolbar = new ToolbarWithOverflow();
            this.toolbar.setOrientation(1);
            this.toolbar.setLayout(new BoxLayout(this.toolbar, 1));
            this.toolbar.setFloatable(false);
            Insets ins = this.toolbar.getMargin();
            JButton sample = new JButton();
            sample.setBorderPainted(false);
            sample.setOpaque(false);
            sample.setText(null);
            sample.setIcon(new Icon(){

                @Override
                public int getIconHeight() {
                    return 16;
                }

                @Override
                public int getIconWidth() {
                    return 16;
                }

                @Override
                public void paintIcon(Component c, Graphics g, int x, int y) {
                }
            });
            this.toolbar.add(sample);
            Dimension buttonPref = sample.getPreferredSize();
            Dimension minDim = new Dimension(buttonPref.width + ins.left + ins.right, buttonPref.height + ins.top + ins.bottom);
            this.toolbar.setMinimumSize(minDim);
            this.toolbar.setPreferredSize(minDim);
            this.toolbar.remove(sample);
            this.setLayout((LayoutManager)new BorderLayout());
            this.add((Component)this.toolbar, (Object)"West");
            this.toolbar.setBorder(new VariableRightBorder(this.pane));
            this.toolbar.setBorderPainted(true);
            this.popupMenu = new JPopupMenu();
            this.popupMenu.add(new Close());
            this.popupMenu.add(new CloseAll());
            this.popupMenu.add(new CloseOthers());
            this.pane.addMouseListener((MouseListener)new MouseUtils.PopupMouseAdapter(){

                protected void showPopup(MouseEvent evt) {
                    IOWindowImpl.this.popupMenu.show((Component)((Object)IOWindowImpl.this), evt.getX(), evt.getY());
                }
            });
            this.pane.addMouseListener(new MouseAdapter(){

                @Override
                public void mouseClicked(MouseEvent e) {
                    if (SwingUtilities.isLeftMouseButton(e)) {
                        IOWindowImpl.this.requestActive();
                    }
                }
            });
            String name = NbBundle.getMessage(IOWindow.class, (String)"LBL_IO_WINDOW");
            this.setDisplayName(name);
            this.setToolTipText(name);
            this.setName(name);
            this.putClientProperty((Object)"SlidingName", (Object)this.getDisplayName());
            if (AQUA) {
                this.setBackground(UIManager.getColor("NbExplorerView.background"));
                this.setOpaque(true);
                this.toolbar.setBackground(UIManager.getColor("NbExplorerView.background"));
                this.pane.setBackground(UIManager.getColor("NbExplorerView.background"));
                this.pane.setOpaque(true);
            }
        }

        public String getShortName() {
            return NbBundle.getMessage(IOWindow.class, (String)"LBL_IO_WINDOW");
        }

        public void open() {
            if (!this.isOpened()) {
                super.open();
            }
        }

        public void requestActive() {
            super.requestActive();
            JComponent tab = this.getSelectedTab();
            if (tab != null) {
                tab.requestFocus();
            }
        }

        public void requestVisible() {
            if (!this.isShowing()) {
                super.requestVisible();
            }
        }

        public boolean isActivated() {
            return this.activated;
        }

        public void addTab(JComponent comp, IOContainer.CallBacks cb) {
            if (cb != null) {
                this.tabToCb.put(comp, cb);
            }
            if (this.singleTab != null) {
                assert (this.pane.getParent() == null);
                assert (this.pane.getTabCount() == 0);
                this.remove((Component)this.singleTab);
                this.pane.add(this.singleTab);
                this.pane.setIconAt(0, (Icon)this.singleTab.getClientProperty("tabIcon"));
                this.pane.setToolTipTextAt(0, this.singleTab.getToolTipText());
                this.singleTab = null;
                this.pane.add(comp);
                this.add((Component)this.pane);
                this.updateWindowName(null);
            } else if (this.pane.getTabCount() > 0) {
                assert (this.pane.getParent() != null);
                assert (this.singleTab == null);
                this.pane.add(comp);
            } else {
                assert (this.pane.getParent() == null);
                assert (this.singleTab == null);
                this.setFocusable(false);
                this.singleTab = comp;
                this.add((Component)comp);
                this.updateWindowName(this.singleTab.getName());
                this.checkTabSelChange();
            }
            this.revalidate();
        }

        public void removeTab(JComponent comp) {
            if (this.singleTab != null) {
                assert (this.singleTab == comp);
                this.remove((Component)this.singleTab);
                this.singleTab = null;
                this.updateWindowName(null);
                this.checkTabSelChange();
                this.setFocusable(true);
                this.revalidate();
                this.repaint();
            } else if (this.pane.getParent() == this) {
                assert (this.pane.getTabCount() > 1);
                this.pane.remove(comp);
                if (this.pane.getTabCount() == 1) {
                    this.singleTab = (JComponent)this.pane.getComponentAt(0);
                    this.pane.remove(this.singleTab);
                    this.remove((Component)this.pane);
                    this.add((Component)this.singleTab);
                    this.updateWindowName(this.singleTab.getName());
                }
                this.revalidate();
            }
            IOContainer.CallBacks cb = this.tabToCb.remove(comp);
            if (cb != null) {
                cb.closed();
            }
        }

        public void selectTab(JComponent comp) {
            if (this.singleTab == null) {
                this.pane.setSelectedComponent(comp);
            }
            this.checkTabSelChange();
        }

        public JComponent getSelectedTab() {
            return this.singleTab != null ? this.singleTab : (JComponent)this.pane.getSelectedComponent();
        }

        public void setTitle(JComponent comp, String name) {
            comp.setName(name);
            if (this.singleTab != null) {
                assert (this.singleTab == comp);
                this.updateWindowName(name);
            } else {
                assert (this.pane.getParent() == this);
                int idx = this.pane.indexOfComponent(comp);
                assert (idx >= 0);
                this.pane.setTitleAt(idx, name);
            }
        }

        public void setToolTipText(JComponent comp, String text) {
            comp.setToolTipText(text);
            if (this.singleTab != null) {
                assert (this.singleTab == comp);
            } else {
                assert (this.pane.getParent() == this);
                int idx = this.pane.indexOfComponent(comp);
                assert (idx >= 0);
                this.pane.setToolTipTextAt(idx, text);
            }
        }

        public void setIcon(JComponent comp, Icon icon) {
            if (comp == this.singleTab) {
                comp.putClientProperty("tabIcon", icon);
                return;
            }
            int idx = this.pane.indexOfComponent(comp);
            if (idx < 0) {
                return;
            }
            comp.putClientProperty("tabIcon", icon);
            this.pane.setIconAt(idx, icon);
            this.pane.setDisabledIconAt(idx, icon);
        }

        void setToolbarActions(JComponent comp, Action[] toolbarActions) {
            if (toolbarActions != null && toolbarActions.length > 0) {
                comp.putClientProperty("toolbarActions", toolbarActions);
            }
            if (this.getSelectedTab() == comp) {
                this.updateToolbar(comp);
            }
        }

        public int getPersistenceType() {
            return 0;
        }

        public String preferredID() {
            return "output";
        }

        public void processFocusEvent(FocusEvent fe) {
            super.processFocusEvent(fe);
            if (Boolean.TRUE.equals(this.getClientProperty((Object)"isSliding"))) {
                this.repaint(200);
            }
        }

        public void paintComponent(Graphics g) {
            if (AQUA) {
                g.setColor(this.getBackground());
                g.fillRect(0, 0, this.getWidth(), this.getHeight());
            }
            super.paintComponent(g);
            if (this.hasFocus()) {
                Insets ins = this.getInsets();
                Color col = UIManager.getColor("controlShadow");
                if (col == null) {
                    col = Color.GRAY;
                }
                g.setColor(col);
                g.drawRect(ins.left + 2, ins.top + 2, this.getWidth() - (ins.left + ins.right + 4), this.getHeight() - (ins.top + ins.bottom + 4));
            }
        }

        void updateWindowName(String name) {
            String winName = NbBundle.getMessage(IOWindowImpl.class, (String)"LBL_IO_WINDOW");
            if (name != null) {
                String newName = NbBundle.getMessage(IOWindowImpl.class, (String)"FMT_IO_WINDOW", (Object[])new Object[]{winName, name});
                if (newName.indexOf("<html>") != -1) {
                    newName = "<html>" + newName.replace("<html>", "");
                    this.setHtmlDisplayName(newName);
                    this.setToolTipText(newName);
                } else {
                    this.setDisplayName(newName);
                    this.setHtmlDisplayName(null);
                    this.setToolTipText(newName);
                }
            } else {
                this.setDisplayName(winName);
                this.setToolTipText(winName);
                this.setHtmlDisplayName(null);
            }
        }

        private void updateToolbar(JComponent comp) {
            this.toolbar.removeAll();
            if (comp != null) {
                JButton[] buttons = this.getTabButtons(comp);
                for (int i = 0; i < buttons.length; ++i) {
                    this.toolbar.add(buttons[i]);
                }
            }
            this.toolbar.validate();
            this.toolbar.repaint();
        }

        JButton[] getTabButtons(JComponent comp) {
            JButton[] buttons = (JButton[])comp.getClientProperty("toolbarButtons");
            if (buttons != null) {
                return buttons;
            }
            Action[] actions = (Action[])comp.getClientProperty("toolbarActions");
            if (actions == null) {
                return new JButton[0];
            }
            buttons = new JButton[actions.length];
            for (int i = 0; i < buttons.length; ++i) {
                buttons[i] = new JButton(actions[i]);
                buttons[i].setBorderPainted(false);
                buttons[i].setOpaque(false);
                buttons[i].setText(null);
                buttons[i].putClientProperty("hideActionText", Boolean.TRUE);
                Object icon = actions[i].getValue("SmallIcon");
                if (!(icon instanceof Icon)) {
                    throw new IllegalStateException("No icon provided for " + actions[i]);
                }
                buttons[i].setDisabledIcon(ImageUtilities.createDisabledIcon((Icon)((Icon)icon)));
                String name = (String)actions[i].getValue("Name");
                String shortDescription = (String)actions[i].getValue("ShortDescription");
                String longDescription = (String)actions[i].getValue("LongDescription");
                if (name == null) {
                    name = shortDescription;
                }
                if (longDescription == null) {
                    longDescription = shortDescription;
                }
                buttons[i].getAccessibleContext().setAccessibleName(name);
                buttons[i].getAccessibleContext().setAccessibleDescription(longDescription);
            }
            return buttons;
        }

        protected void componentActivated() {
            super.componentActivated();
            this.activated = true;
            JComponent comp = this.getSelectedTab();
            IOContainer.CallBacks cb = this.tabToCb.get(comp);
            if (cb != null) {
                cb.activated();
            }
        }

        protected void componentDeactivated() {
            super.componentDeactivated();
            this.activated = false;
            JComponent comp = this.getSelectedTab();
            IOContainer.CallBacks cb = this.tabToCb.get(comp);
            if (cb != null) {
                cb.deactivated();
            }
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            this.checkTabSelChange();
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("close".equals(evt.getPropertyName())) {
                JComponent comp = (JComponent)evt.getNewValue();
                this.removeTab(comp);
            }
        }

        void checkTabSelChange() {
            JComponent sel = this.getSelectedTab();
            if (sel != this.lastSelTab) {
                this.lastSelTab = sel;
                this.updateToolbar(sel);
                this.getActionMap().setParent(sel != null ? sel.getActionMap() : null);
            }
        }

        private JComponent[] getTabs() {
            if (this.singleTab != null) {
                return new JComponent[]{this.singleTab};
            }
            JComponent[] tabs = new JComponent[this.pane.getTabCount()];
            for (int i = 0; i < this.pane.getTabCount(); ++i) {
                tabs[i] = (JComponent)this.pane.getComponentAt(i);
            }
            return tabs;
        }

        public TopComponent.SubComponent[] getSubComponents() {
            if (this.singleTab != null) {
                return new TopComponent.SubComponent[0];
            }
            JComponent[] tabs = this.getTabs();
            TopComponent.SubComponent[] res = new TopComponent.SubComponent[tabs.length];
            for (int i = 0; i < res.length; ++i) {
                final JComponent theTab = tabs[i];
                String title = this.pane.getTitleAt(i);
                res[i] = new TopComponent.SubComponent(title, new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (IOWindowImpl.this.singleTab != null || IOWindowImpl.this.pane.indexOfComponent(theTab) < 0) {
                            return;
                        }
                        IOWindowImpl.this.selectTab(theTab);
                    }
                }, theTab == this.getSelectedTab());
            }
            return res;
        }

        private void closeOtherTabs() {
            assert (this.pane.getParent() == this);
            JComponent sel = this.getSelectedTab();
            for (JComponent tab : this.getTabs()) {
                if (tab == sel) continue;
                this.removeTab(tab);
            }
        }

        private void closeAllTabs() {
            for (JComponent tab : this.getTabs()) {
                this.removeTab(tab);
            }
        }

        public HelpCtx getHelpCtx() {
            return new HelpCtx("org.netbeans.core.io.ui.IOWindow");
        }

        static {
            AQUA = "Aqua".equals(UIManager.getLookAndFeel().getID());
        }

        private class VariableRightBorder
        implements Border {
            private JTabbedPane pane;

            public VariableRightBorder(JTabbedPane pane) {
                this.pane = pane;
            }

            @Override
            public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
                if (this.pane.getParent() != IOWindowImpl.this) {
                    Color old = g.getColor();
                    g.setColor(this.getColor());
                    g.drawLine(x + width - 2, y, x + width - 2, y + height);
                    g.drawLine(x + width - 1, y, x + width - 1, y + height);
                    g.setColor(old);
                }
            }

            public Color getColor() {
                return UIManager.getLookAndFeelDefaults().getColor("Separator.foreground");
            }

            @Override
            public Insets getBorderInsets(Component c) {
                if (this.pane.getParent() == IOWindowImpl.this) {
                    return new Insets(0, 0, 0, 0);
                }
                return new Insets(0, 0, 0, 2);
            }

            @Override
            public boolean isBorderOpaque() {
                return true;
            }
        }

        private class CloseOthers
        extends AbstractAction {
            public CloseOthers() {
                super(NbBundle.getMessage(IOWindowImpl.class, (String)"LBL_CloseOthers"));
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                IOWindowImpl.this.closeOtherTabs();
            }
        }

        private class CloseAll
        extends AbstractAction {
            public CloseAll() {
                super(NbBundle.getMessage(IOWindowImpl.class, (String)"LBL_CloseAll"));
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                IOWindowImpl.this.closeAllTabs();
            }
        }

        private class Close
        extends AbstractAction {
            public Close() {
                super(NbBundle.getMessage(IOWindowImpl.class, (String)"LBL_Close"));
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                IOWindowImpl.this.removeTab(IOWindowImpl.this.getSelectedTab());
            }
        }

    }

}

