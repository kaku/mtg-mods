/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.IOContainer
 */
package org.netbeans.core.io.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.windows.IOContainer;

public final class IOWindowAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent evt) {
        IOContainer container = IOContainer.getDefault();
        container.open();
        container.requestActive();
    }
}

