/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.startup.Main
 *  org.netbeans.core.startup.ManifestSection
 *  org.netbeans.core.startup.ManifestSection$LoaderSection
 *  org.netbeans.core.startup.StartLog
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.MIMEResolver
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataLoader
 *  org.openide.loaders.DataLoaderPool
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObject$Container
 *  org.openide.loaders.FolderLookup
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.Modules
 *  org.openide.modules.SpecificationVersion
 *  org.openide.util.Enumerations
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 *  org.openide.util.TaskListener
 *  org.openide.util.TopologicalSortException
 *  org.openide.util.Utilities
 *  org.openide.util.io.NbMarshalledObject
 *  org.openide.util.io.NbObjectInputStream
 *  org.openide.util.io.NbObjectOutputStream
 */
package org.netbeans.core;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import org.netbeans.core.startup.Main;
import org.netbeans.core.startup.ManifestSection;
import org.netbeans.core.startup.StartLog;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.MIMEResolver;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataLoader;
import org.openide.loaders.DataLoaderPool;
import org.openide.loaders.DataObject;
import org.openide.loaders.FolderLookup;
import org.openide.modules.ModuleInfo;
import org.openide.modules.Modules;
import org.openide.modules.SpecificationVersion;
import org.openide.util.Enumerations;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.TaskListener;
import org.openide.util.TopologicalSortException;
import org.openide.util.Utilities;
import org.openide.util.io.NbMarshalledObject;
import org.openide.util.io.NbObjectInputStream;
import org.openide.util.io.NbObjectOutputStream;

public final class NbLoaderPool
extends DataLoaderPool
implements PropertyChangeListener,
Runnable,
LookupListener,
TaskListener {
    private static final Logger err = Logger.getLogger(NbLoaderPool.class.getName());
    private static List<DataLoader> loaders = new ArrayList<DataLoader>();
    private static Set<DataLoader> modifiedLoaders = new HashSet<DataLoader>();
    private static Map<String, DataLoader> names2Loaders = new HashMap<String, DataLoader>(200);
    private static Map<String, DataLoader> repNames2Loaders = new HashMap<String, DataLoader>(200);
    private static Map<String, String[]> installBefores = new HashMap<String, String[]>();
    private static Map<String, String[]> installAfters = new HashMap<String, String[]>();
    private static DataLoader[] loadersArray;
    private static boolean installationFinished;
    private static boolean updatingBatch;
    private static boolean updatingBatchUsed;
    private static final String LOADER_POOL_NAME = "loaders.ser";
    private static final long serialVersionUID = -8488524097175567566L;
    static boolean IN_TEST;
    private transient RequestProcessor.Task fireTask;
    private transient Lookup.Result mimeResolvers;
    private transient FileObject declarativeResolvers;
    private transient boolean listenersRegistered;
    private static RequestProcessor rp;
    private final FileChangeListener listener;

    public static synchronized void beginUpdates() {
        updatingBatch = true;
        updatingBatchUsed = false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void endUpdates() {
        NbLoaderPool pool = NbLoaderPool.getNbLoaderPool();
        Class<NbLoaderPool> class_ = NbLoaderPool.class;
        synchronized (NbLoaderPool.class) {
            if (!updatingBatch) {
                throw new IllegalStateException();
            }
            updatingBatch = false;
            if (updatingBatchUsed) {
                updatingBatchUsed = false;
                NbLoaderPool.resort(pool);
            }
            // ** MonitorExit[var1_1] (shouldn't be in output)
            return;
        }
    }

    public static void waitFinished() {
        NbLoaderPool.getNbLoaderPool().fireTask.waitFinished();
    }

    public static void add(ManifestSection.LoaderSection s) throws Exception {
        DataLoader l = (DataLoader)s.getInstance();
        NbLoaderPool.doAdd(l, s, NbLoaderPool.getNbLoaderPool());
    }

    static synchronized void doAdd(DataLoader l, ManifestSection.LoaderSection s, NbLoaderPool pool) throws Exception {
        if (err.isLoggable(Level.FINE) && s != null) {
            List<String> before = s.getInstallBefore() == null ? null : Arrays.asList(s.getInstallBefore());
            List<String> after = s.getInstallAfter() == null ? null : Arrays.asList(s.getInstallAfter());
            err.fine("add: " + (Object)l + " repclass: " + l.getRepresentationClass().getName() + " before: " + before + " after: " + after);
        }
        Iterator<DataLoader> it = loaders.iterator();
        Class c = l.getClass();
        while (it.hasNext()) {
            if (it.next().getClass() != c) continue;
            it.remove();
            break;
        }
        loaders.add(l);
        l.removePropertyChangeListener((PropertyChangeListener)pool);
        l.addPropertyChangeListener((PropertyChangeListener)pool);
        String cname = c.getName();
        names2Loaders.put(cname, l);
        repNames2Loaders.put(l.getRepresentationClassName(), l);
        if (s != null) {
            String[] ia;
            String[] ib = s.getInstallBefore();
            if (ib != null) {
                installBefores.put(cname, ib);
            }
            if ((ia = s.getInstallAfter()) != null) {
                installAfters.put(cname, ia);
            }
        }
        if (updatingBatch) {
            updatingBatchUsed = true;
        } else {
            NbLoaderPool.resort(pool);
        }
    }

    private static synchronized void resort(NbLoaderPool pool) {
        HashMap<DataLoader, List<DataLoader>> deps = new HashMap<DataLoader, List<DataLoader>>();
        NbLoaderPool.add2Deps(deps, installBefores, true);
        NbLoaderPool.add2Deps(deps, installAfters, false);
        if (err.isLoggable(Level.FINE)) {
            err.fine("Before sort: " + loaders);
        }
        try {
            loaders = Utilities.topologicalSort(loaders, deps);
            if (err.isLoggable(Level.FINE)) {
                err.fine("After sort: " + loaders);
            }
        }
        catch (TopologicalSortException ex) {
            err.log(Level.WARNING, null, (Throwable)ex);
            err.warning("Contradictory loader ordering: " + deps);
        }
        NbLoaderPool.update(pool);
    }

    private static void add2Deps(Map<DataLoader, List<DataLoader>> deps, Map orderings, boolean before) {
        for (Map.Entry e : orderings.entrySet()) {
            String loaderClassName = (String)e.getKey();
            DataLoader l = names2Loaders.get(loaderClassName);
            if (l == null) {
                throw new IllegalStateException("No such loader: " + loaderClassName);
            }
            String[] repClassNames = (String[])e.getValue();
            if (repClassNames == null) {
                throw new IllegalStateException("Null Install-" + (before ? "Before" : "After") + " for " + loaderClassName);
            }
            for (int i = 0; i < repClassNames.length; ++i) {
                String repClassName = repClassNames[i];
                DataLoader l2 = repNames2Loaders.get(repClassName);
                if (l2 != null) {
                    if (before) {
                        NbLoaderPool.addDep(deps, l, l2);
                        continue;
                    }
                    NbLoaderPool.addDep(deps, l2, l);
                    continue;
                }
                l2 = names2Loaders.get(repClassName);
                if (l2 == null) continue;
                NbLoaderPool.warn(loaderClassName, repClassName, l2.getRepresentationClassName());
            }
        }
    }

    private static void addDep(Map<DataLoader, List<DataLoader>> deps, DataLoader a, DataLoader b) {
        List<DataLoader> l = deps.get((Object)a);
        if (l == null) {
            l = new LinkedList<DataLoader>();
            deps.put(a, l);
        }
        if (!l.contains((Object)b)) {
            l.add(b);
        }
    }

    private static void warn(String yourLoader, String otherLoader, String otherRepn) {
        err.warning("Warning: a possible error in the manifest containing " + yourLoader + " was found.");
        err.warning("The loader specified an Install-{After,Before} on " + otherLoader + ", but this is a DataLoader class.");
        err.warning("Probably you wanted " + otherRepn + " which is the loader's representation class.");
    }

    static void installationFinished() {
        installationFinished = true;
        if (!modifiedLoaders.isEmpty()) {
            NbLoaderPool.getNbLoaderPool().superFireChangeEvent();
        }
    }

    static synchronized boolean isModified(DataLoader l) {
        return modifiedLoaders.contains((Object)l);
    }

    private static synchronized void writePool(ObjectOutputStream oos, NbLoaderPool pool) throws IOException {
        if (err.isLoggable(Level.FINE)) {
            err.fine("writePool");
        }
        oos.writeObject(new HashMap());
        oos.writeObject(new HashMap());
        for (DataLoader l : loaders) {
            NbMarshalledObject obj;
            if (!NbLoaderPool.isModified(l)) {
                String c = l.getClass().getName();
                if (err.isLoggable(Level.FINE)) {
                    err.fine("writing unmodified " + c);
                }
                oos.writeObject("=" + c);
                continue;
            }
            try {
                obj = new NbMarshalledObject((Object)l);
            }
            catch (IOException ex) {
                err.log(Level.WARNING, null, ex);
                obj = null;
            }
            if (obj == null) continue;
            if (err.isLoggable(Level.FINE)) {
                err.fine("writing modified " + l.getClass().getName());
            }
            boolean found = false;
            ModuleInfo m = Modules.getDefault().ownerOf(l.getClass());
            if (m != null && m.isEnabled()) {
                if (err.isLoggable(Level.FINE)) {
                    err.fine("belongs to module: " + m.getCodeNameBase());
                }
                oos.writeObject(m.getCodeNameBase());
                int r = m.getCodeNameRelease();
                oos.writeInt(r);
                SpecificationVersion v = m.getSpecificationVersion();
                if (v != null) {
                    oos.writeObject(v.toString());
                } else {
                    oos.writeObject(null);
                }
                found = true;
            }
            if (!found && err.isLoggable(Level.FINE)) {
                err.fine("does not belong to any module");
            }
            oos.writeObject((Object)obj);
        }
        if (err.isLoggable(Level.FINE)) {
            err.fine("writing null");
        }
        oos.writeObject(null);
        Enumeration e = pool.allLoaders();
        while (e.hasMoreElements()) {
            NbMarshalledObject obj;
            DataLoader l2 = (DataLoader)e.nextElement();
            if (loaders.contains((Object)l2)) continue;
            if (!NbLoaderPool.isModified(l2)) {
                String c = l2.getClass().getName();
                if (!err.isLoggable(Level.FINE)) continue;
                err.fine("skipping unmodified " + c);
                continue;
            }
            try {
                obj = new NbMarshalledObject((Object)l2);
            }
            catch (IOException ex) {
                err.log(Level.WARNING, null, ex);
                obj = null;
            }
            if (obj == null) continue;
            if (err.isLoggable(Level.FINE)) {
                err.fine("writing " + l2.getClass().getName());
            }
            oos.writeObject((Object)obj);
        }
        if (err.isLoggable(Level.FINE)) {
            err.fine("writing null");
        }
        oos.writeObject(null);
        if (err.isLoggable(Level.FINE)) {
            err.fine("done writing");
        }
    }

    private static synchronized void readPool(ObjectInputStream ois, NbLoaderPool pool) throws IOException, ClassNotFoundException {
        ois.readObject();
        ois.readObject();
        HashSet classes = new HashSet();
        LinkedList<DataLoader> l = new LinkedList<DataLoader>();
        Iterator mit = Lookup.getDefault().lookupAll(ModuleInfo.class).iterator();
        HashMap<String, ModuleInfo> modules = new HashMap<String, ModuleInfo>();
        while (mit.hasNext()) {
            ModuleInfo m = (ModuleInfo)mit.next();
            modules.put(m.getCodeNameBase(), m);
        }
        do {
            Object o1;
            NbMarshalledObject obj;
            if ((o1 = ois.readObject()) == null) {
                if (!err.isLoggable(Level.FINE)) break;
                err.fine("reading null");
                break;
            }
            if (o1 instanceof String) {
                SpecificationVersion v;
                String name = (String)o1;
                if (name.length() > 0 && name.charAt(0) == '=') {
                    String cname = name.substring(1);
                    DataLoader dl = names2Loaders.get(cname);
                    if (dl != null) {
                        if (err.isLoggable(Level.FINE)) {
                            err.fine("reading unmodified " + cname);
                        }
                        l.add(dl);
                        classes.add(dl.getClass());
                        continue;
                    }
                    if (!err.isLoggable(Level.FINE)) continue;
                    err.fine("skipping unmodified nonexistent " + cname);
                    continue;
                }
                int rel = ois.readInt();
                String spec = (String)ois.readObject();
                obj = (NbMarshalledObject)ois.readObject();
                ModuleInfo m = (ModuleInfo)modules.get(name);
                if (m == null) {
                    if (!err.isLoggable(Level.FINE)) continue;
                    err.fine("No known module " + name + ", skipping loader");
                    continue;
                }
                if (!m.isEnabled()) {
                    if (!err.isLoggable(Level.FINE)) continue;
                    err.fine("Module " + name + " is disabled, skipping loader");
                    continue;
                }
                if (m.getCodeNameRelease() < rel) {
                    if (!err.isLoggable(Level.FINE)) continue;
                    err.fine("Module " + name + " is too old (major vers.), skipping loader");
                    continue;
                }
                if (spec != null && ((v = m.getSpecificationVersion()) == null || v.compareTo((Object)new SpecificationVersion(spec)) < 0)) {
                    if (!err.isLoggable(Level.FINE)) continue;
                    err.fine("Module " + name + " is too old (spec. vers.), skipping loader");
                    continue;
                }
                if (err.isLoggable(Level.FINE)) {
                    err.fine("Module " + name + " is OK, will try to restore loader");
                }
            } else {
                obj = (NbMarshalledObject)o1;
            }
            Exception t = null;
            try {
                DataLoader loader = (DataLoader)obj.get();
                if (loader == null) continue;
                Class clazz = loader.getClass();
                if (err.isLoggable(Level.FINE)) {
                    err.fine("reading modified " + clazz.getName());
                }
                l.add(loader);
                classes.add(clazz);
            }
            catch (IOException ex) {
                t = ex;
            }
            catch (ClassNotFoundException ex) {
                t = ex;
            }
        } while (true);
        do {
            NbMarshalledObject obj;
            if ((obj = (NbMarshalledObject)ois.readObject()) == null) {
                if (!err.isLoggable(Level.FINE)) break;
                err.fine("reading null");
                break;
            }
            Exception t = null;
            try {
                DataLoader loader = (DataLoader)obj.get();
                if (!err.isLoggable(Level.FINE)) continue;
                err.fine("reading " + loader.getClass().getName());
            }
            catch (IOException ex) {
                t = ex;
            }
            catch (ClassNotFoundException ex) {
                t = ex;
            }
        } while (true);
        if (err.isLoggable(Level.FINE)) {
            err.fine("done reading");
        }
        for (DataLoader loader : loaders) {
            if (classes.contains(loader.getClass())) continue;
            l.add(loader);
        }
        if (l.size() > new HashSet(l).size()) {
            throw new IllegalStateException("Duplicates in " + l);
        }
        loaders = l;
        NbLoaderPool.resort(pool);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void store() throws IOException {
        if (modifiedLoaders.isEmpty()) {
            return;
        }
        FileObject ser = NbLoaderPool.getLoaderPoolStorage(true);
        OutputStream os = ser.getOutputStream();
        try {
            NbObjectOutputStream oos = new NbObjectOutputStream(os);
            NbObjectOutputStream.writeSafely((ObjectOutput)oos, (Object)NbLoaderPool.getNbLoaderPool());
            oos.flush();
            oos.close();
        }
        finally {
            os.close();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void load() throws IOException {
        FileObject ser = NbLoaderPool.getLoaderPoolStorage(false);
        if (ser != null) {
            try {
                NbObjectInputStream ois = new NbObjectInputStream(ser.getInputStream());
                try {
                    NbObjectInputStream.readSafely((ObjectInput)ois);
                }
                finally {
                    ois.close();
                }
            }
            catch (IOException x) {
                ser.delete();
                throw x;
            }
        }
    }

    private static FileObject getLoaderPoolStorage(boolean create) throws IOException {
        FileObject fo = FileUtil.getConfigFile((String)"loaders.ser");
        if (fo == null && create) {
            fo = FileUtil.getConfigRoot().createData("loaders.ser");
        }
        return fo;
    }

    private static synchronized void update(NbLoaderPool lp) {
        if (err.isLoggable(Level.FINE)) {
            err.fine("update");
        }
        loadersArray = null;
        if (lp != null && installationFinished) {
            lp.superFireChangeEvent();
        }
        if (lp != null) {
            Enumeration e = lp.allLoaders();
            while (e.hasMoreElements()) {
                DataLoader l = (DataLoader)e.nextElement();
                l.removePropertyChangeListener((PropertyChangeListener)lp);
                l.addPropertyChangeListener((PropertyChangeListener)lp);
            }
        }
    }

    public static synchronized boolean remove(DataLoader dl, NbLoaderPool pool) {
        if (loaders.remove((Object)dl)) {
            if (err.isLoggable(Level.FINE)) {
                err.fine("remove: " + (Object)dl);
            }
            String cname = dl.getClass().getName();
            names2Loaders.remove(cname);
            repNames2Loaders.remove(dl.getRepresentationClassName());
            installBefores.remove(cname);
            installAfters.remove(cname);
            dl.removePropertyChangeListener((PropertyChangeListener)pool);
            if (updatingBatch) {
                updatingBatchUsed = true;
            } else {
                NbLoaderPool.resort(pool);
            }
            modifiedLoaders.remove((Object)dl);
            return true;
        }
        return false;
    }

    public static NbLoaderPool getNbLoaderPool() {
        assert (!Thread.holdsLock(NbLoaderPool.class));
        return (NbLoaderPool)DataLoaderPool.getDefault();
    }

    public NbLoaderPool() {
        this.listener = new FileChangeAdapter(){

            public void fileDataCreated(FileEvent ev) {
                NbLoaderPool.this.maybeFireChangeEvent();
            }

            public void fileDeleted(FileEvent ev) {
                NbLoaderPool.this.maybeFireChangeEvent();
            }
        };
        this.fireTask = rp.create((Runnable)this, true);
    }

    private final DataFolder findServicesFolder() throws Exception {
        Method m = DataLoaderPool.class.getDeclaredMethod("getFolderLoader", new Class[0]);
        m.setAccessible(true);
        DataLoader dl = (DataLoader)m.invoke(null, new Object[0]);
        FileObject services = FileUtil.getConfigFile((String)"Services");
        return (DataFolder)dl.findDataObject(services, new HashSet());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final Lookup findServicesLookup() {
        StartLog.logProgress((String)"Got Services folder");
        try {
            this.listenersRegistered = true;
            DataFolder servicesF = this.findServicesFolder();
            FolderLookup f = new FolderLookup((DataObject.Container)servicesF, "SL[");
            f.addTaskListener((TaskListener)this);
            Lookup lookup = f.getLookup();
            return lookup;
        }
        catch (Exception e) {
            Exceptions.printStackTrace((Throwable)e);
            Lookup lookup = Lookup.EMPTY;
            return lookup;
        }
        finally {
            StartLog.logProgress((String)"created FolderLookup");
        }
    }

    public void taskFinished(Task task) {
        this.initListeners();
    }

    private void initListeners() {
        this.mimeResolvers = Lookup.getDefault().lookupResult(MIMEResolver.class);
        this.mimeResolvers.addLookupListener((LookupListener)this);
        this.listenToDeclarativeResolvers();
    }

    private void listenToDeclarativeResolvers() {
        this.declarativeResolvers = FileUtil.getConfigFile((String)"Services/MIMEResolver");
        if (this.declarativeResolvers != null) {
            this.declarativeResolvers.addFileChangeListener(this.listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected Enumeration<DataLoader> loaders() {
        if (!this.listenersRegistered) {
            this.initListeners();
        }
        Class<NbLoaderPool> class_ = NbLoaderPool.class;
        synchronized (NbLoaderPool.class) {
            if (loadersArray == null) {
                ArrayList<DataLoader> ldrs = new ArrayList<DataLoader>(loaders);
                ldrs.addAll(Lookup.getDefault().lookupAll(DataLoader.class));
                loadersArray = ldrs.toArray((T[])new DataLoader[ldrs.size()]);
            }
            Object[] arr = loadersArray;
            // ** MonitorExit[var2_1] (shouldn't be in output)
            return Enumerations.array((Object[])arr);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent ev) {
        DataLoader l = (DataLoader)ev.getSource();
        String prop = ev.getPropertyName();
        if ("actions".equals(prop) && ev.getNewValue() == null) {
            return;
        }
        modifiedLoaders.add(l);
        if (err.isLoggable(Level.FINE)) {
            err.fine("Got change in " + l.getClass().getName() + "." + prop);
        }
        if ("actions".equals(prop) || "displayName".equals(prop)) {
            return;
        }
        if (installationFinished) {
            this.superFireChangeEvent();
        }
    }

    void superFireChangeEvent() {
        err.fine("Change in loader pool scheduled");
        this.fireTask.schedule(1000);
    }

    @Override
    public void run() {
        err.fine("going to fire change in loaders");
        super.fireChangeEvent(new ChangeEvent(this));
        err.fine("change event fired");
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        NbLoaderPool.writePool(oos, NbLoaderPool.getNbLoaderPool());
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        NbLoaderPool.readPool(ois, NbLoaderPool.getNbLoaderPool());
    }

    private Object readResolve() {
        return NbLoaderPool.getNbLoaderPool();
    }

    public void resultChanged(LookupEvent ev) {
        this.maybeFireChangeEvent();
    }

    private void maybeFireChangeEvent() {
        if (IN_TEST || Main.isInitialized()) {
            this.superFireChangeEvent();
        }
    }

    static {
        installationFinished = false;
        updatingBatch = false;
        updatingBatchUsed = false;
        IN_TEST = false;
        rp = new RequestProcessor("Refresh Loader Pool");
    }

}

