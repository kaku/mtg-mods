/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.keyring.Keyring
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 */
package org.netbeans.core;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.util.prefs.Preferences;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import org.netbeans.api.keyring.Keyring;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

final class NbAuthenticatorPanel
extends JPanel {
    private String realmName;
    private final Preferences prefs;
    private final String keyringKey;
    private JPasswordField password;
    private JLabel passwordLbl;
    private JLabel realmNameLbl;
    private JTextField userName;
    private JLabel userNameLbl;

    public NbAuthenticatorPanel(String realmName) {
        this.realmName = realmName;
        this.initComponents();
        this.prefs = NbPreferences.forModule(NbAuthenticatorPanel.class).node("authentication");
        this.keyringKey = "authentication." + realmName;
        String username = this.prefs.get(realmName, null);
        if (username != null) {
            this.userName.setText(username);
            char[] pwd = Keyring.read((String)this.keyringKey);
            if (pwd != null) {
                this.password.setText(new String(pwd));
            }
        }
    }

    private void initComponents() {
        this.userNameLbl = new JLabel();
        this.userName = new JTextField();
        this.passwordLbl = new JLabel();
        this.password = new JPasswordField();
        this.realmNameLbl = new JLabel();
        this.userNameLbl.setLabelFor(this.userName);
        Mnemonics.setLocalizedText((JLabel)this.userNameLbl, (String)NbBundle.getMessage(NbAuthenticatorPanel.class, (String)"NbAuthenticatorPanel.userNameLbl.text"));
        this.passwordLbl.setLabelFor(this.password);
        Mnemonics.setLocalizedText((JLabel)this.passwordLbl, (String)NbBundle.getMessage(NbAuthenticatorPanel.class, (String)"NbAuthenticatorPanel.passwordLbl.text"));
        this.realmNameLbl.setText(this.realmName);
        this.realmNameLbl.setFocusable(false);
        this.realmNameLbl.setHorizontalTextPosition(0);
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.userNameLbl).addComponent(this.passwordLbl)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.realmNameLbl, -1, 287, 32767).addComponent(this.password, -1, 287, 32767).addComponent(this.userName, -1, 287, 32767)).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addContainerGap().addComponent(this.realmNameLbl).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.userNameLbl).addComponent(this.userName, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.passwordLbl).addComponent(this.password, -2, -1, -2)).addContainerGap()));
    }

    public char[] getPassword() {
        Keyring.save((String)this.keyringKey, (char[])this.password.getPassword(), (String)NbBundle.getMessage(NbAuthenticatorPanel.class, (String)"NbAuthenticatorPanel.password.description", (Object)this.realmName));
        return this.password.getPassword();
    }

    public String getUserName() {
        String username = this.userName.getText();
        this.prefs.put(this.realmName, username);
        return username;
    }
}

