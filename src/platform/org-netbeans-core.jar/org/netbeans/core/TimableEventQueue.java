/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.sampler.Sampler
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.Window;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;
import org.netbeans.modules.sampler.Sampler;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.windows.WindowManager;

final class TimableEventQueue
extends EventQueue
implements Runnable {
    private static final Logger LOG = Logger.getLogger(TimableEventQueue.class.getName());
    static final RequestProcessor RP = new RequestProcessor("Timeable Event Queue Watch Dog", 1, false, false);
    private static final int QUANTUM;
    private static final int REPORT;
    private static final int WAIT_CURSOR_LIMIT;
    private static final int PAUSE;
    private final RequestProcessor.Task TIMEOUT;
    private final RequestProcessor.Task WAIT_CURSOR_CHECKER;
    private volatile long ignoreTill;
    private volatile long start;
    private volatile Sampler stoppable;
    private volatile boolean isWaitCursor;
    static volatile Thread eq;
    private final Frame mainWindow;

    private TimableEventQueue(Frame f) {
        this.mainWindow = f;
        this.TIMEOUT = RP.create((Runnable)this);
        this.TIMEOUT.setPriority(1);
        this.WAIT_CURSOR_CHECKER = RP.create(new Runnable(){

            @Override
            public void run() {
                TimableEventQueue.access$076(TimableEventQueue.this, (int)TimableEventQueue.isWaitCursor() ? 1 : 0);
            }
        }, true);
        this.WAIT_CURSOR_CHECKER.setPriority(1);
        this.ignoreTill = System.currentTimeMillis() + (long)PAUSE;
    }

    static void initialize() {
        TimableEventQueue.initialize(null, true);
    }

    static void initialize(final Frame f, final boolean defaultWindow) {
        final boolean install = Boolean.valueOf(NbBundle.getMessage(TimableEventQueue.class, (String)"TimableEventQueue.install"));
        try {
            Mutex.EVENT.writeAccess((Mutex.Action)new Mutex.Action<Void>(){

                public Void run() {
                    ClassLoader scl;
                    Frame use = f;
                    if (defaultWindow && use == null) {
                        use = WindowManager.getDefault().getMainWindow();
                    }
                    if ((scl = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class)) != null) {
                        Thread.currentThread().setContextClassLoader(scl);
                    }
                    Toolkit.getDefaultToolkit().getSystemEventQueue().push(install ? new TimableEventQueue(use) : new EventQueue());
                    LOG.fine("Initialization done");
                    return null;
                }
            });
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void dispatchEvent(AWTEvent event) {
        eq = Thread.currentThread();
        boolean scheduled = false;
        try {
            scheduled = this.tick("dispatchEvent");
            super.dispatchEvent(event);
        }
        finally {
            if (scheduled) {
                this.done();
            }
        }
    }

    private void done() {
        long r;
        Sampler ss;
        this.TIMEOUT.cancel();
        this.TIMEOUT.waitFinished();
        if (!this.WAIT_CURSOR_CHECKER.cancel()) {
            this.WAIT_CURSOR_CHECKER.waitFinished();
        }
        LOG.log(Level.FINE, "isWait cursor {0}", this.isWaitCursor);
        if (this.isWaitCursor) {
            r = REPORT * 10;
            if (r > (long)WAIT_CURSOR_LIMIT) {
                r = WAIT_CURSOR_LIMIT > REPORT ? (long)WAIT_CURSOR_LIMIT : (long)REPORT;
            }
        } else {
            r = REPORT;
        }
        this.isWaitCursor = false;
        long time = System.currentTimeMillis() - this.start;
        if (time > (long)QUANTUM) {
            LOG.log(Level.FINE, "done, timer stopped, took {0}", time);
            if (time > r) {
                LOG.log(Level.WARNING, "too much time in AWT thread {0}", (Object)this.stoppable);
                this.ignoreTill = System.currentTimeMillis() + (long)PAUSE;
                TimableEventQueue.report(this.stoppable, time);
                this.stoppable = null;
            }
        } else {
            LOG.log(Level.FINEST, "done, timer stopped, took {0}", time);
        }
        if ((ss = this.stoppable) != null) {
            ss.cancel();
            this.stoppable = null;
        }
    }

    private boolean isShowing() {
        return this.mainWindow == null || this.mainWindow.isShowing();
    }

    private boolean tick(String name) {
        this.start = System.currentTimeMillis();
        if (this.start >= this.ignoreTill && this.isShowing()) {
            LOG.log(Level.FINEST, "tick, schedule a timer for {0}", name);
            this.TIMEOUT.schedule(QUANTUM);
            return true;
        }
        return false;
    }

    @Override
    public void run() {
        if (this.stoppable != null) {
            LOG.log(Level.WARNING, "Still previous controller {0}", (Object)this.stoppable);
            return;
        }
        Sampler selfSampler = TimableEventQueue.createSelfSampler();
        if (selfSampler != null) {
            selfSampler.start();
            this.stoppable = selfSampler;
        }
        this.isWaitCursor |= TimableEventQueue.isWaitCursor();
        if (!this.isWaitCursor) {
            this.WAIT_CURSOR_CHECKER.schedule(Math.max(REPORT - QUANTUM, 0));
        }
    }

    private static void report(Sampler ss, long time) {
        class R
        implements Runnable {
            final /* synthetic */ long val$time;

            R() {
                this.val$time = l;
            }

            @Override
            public void run() {
                try {
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    DataOutputStream dos = new DataOutputStream(out);
                    val$ss.stopAndWriteTo(dos);
                    dos.close();
                    if (dos.size() > 0) {
                        Object[] params = new Object[]{out.toByteArray(), this.val$time};
                        Logger.getLogger("org.netbeans.ui.performance").log(Level.CONFIG, "Slowness detected", params);
                    } else {
                        LOG.log(Level.WARNING, "no snapshot taken");
                    }
                }
                catch (Exception ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }
        if (ss == null) {
            return;
        }
        RP.post((Runnable)new R(ss, time));
    }

    private static Sampler createSelfSampler() {
        return Sampler.createSampler((String)"awt");
    }

    private static boolean isWaitCursor() {
        Component focus = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (focus != null) {
            if (focus.getCursor().getType() == 3) {
                LOG.finer("wait cursor on focus owner");
                return true;
            }
            Window w = SwingUtilities.windowForComponent(focus);
            if (w != null && TimableEventQueue.isWaitCursorOnWindow(w)) {
                LOG.finer("wait cursor on window");
                return true;
            }
        }
        for (Frame f : Frame.getFrames()) {
            if (!TimableEventQueue.isWaitCursorOnWindow(f)) continue;
            LOG.finer("wait cursor on frame");
            return true;
        }
        LOG.finest("no wait cursor");
        return false;
    }

    private static boolean isWaitCursorOnWindow(Window w) {
        Component glass;
        JRootPane root;
        if (w.getCursor().getType() == 3) {
            return true;
        }
        if (w instanceof JFrame && null != (root = ((JFrame)w).getRootPane()) && null != (glass = root.getGlassPane()) && glass.getCursor().getType() == 3) {
            return true;
        }
        return false;
    }

    static /* synthetic */ boolean access$076(TimableEventQueue x0, int x1) {
        x0.isWaitCursor = (byte)(x0.isWaitCursor | x1);
        return (boolean)x0.isWaitCursor;
    }

    static {
        int quantum = 10000;
        int report = 20000;
        if (!$assertionsDisabled) {
            quantum = 100;
            if (100 <= 0) {
                throw new AssertionError();
            }
        }
        if (!$assertionsDisabled) {
            report = 3000;
            if (3000 <= 0) {
                throw new AssertionError();
            }
        }
        QUANTUM = Integer.getInteger("org.netbeans.core.TimeableEventQueue.quantum", quantum);
        REPORT = Integer.getInteger("org.netbeans.core.TimeableEventQueue.report", report);
        WAIT_CURSOR_LIMIT = Integer.getInteger("org.netbeans.core.TimeableEventQueue.waitcursor", 15000);
        PAUSE = Integer.getInteger("org.netbeans.core.TimeableEventQueue.pause", 15000);
    }

}

