/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$Factory
 *  org.openide.cookies.InstanceCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.NbPreferences
 */
package org.netbeans.core;

import java.io.IOException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import org.netbeans.beaninfo.editors.HtmlBrowser;
import org.openide.awt.HtmlBrowser;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;

public class IDESettings {
    public static final String PROP_WWWBROWSER = "WWWBrowser";
    public static final String PROP_EXTERNAL_WWWBROWSER = "ExternalWWWBrowser";

    static Preferences getPreferences() {
        return NbPreferences.forModule(IDESettings.class);
    }

    public static HtmlBrowser.Factory getWWWBrowser() {
        return IDESettings.getBrowser("WWWBrowser", false);
    }

    public static HtmlBrowser.Factory getExternalWWWBrowser() {
        return IDESettings.getBrowser("ExternalWWWBrowser", true);
    }

    public static void setWWWBrowser(HtmlBrowser.Factory brow) {
        IDESettings.setBrowser("WWWBrowser", brow);
        if (IDESettings.isExternal(brow)) {
            IDESettings.setExternalWWWBrowser(brow);
        }
    }

    public static void setExternalWWWBrowser(HtmlBrowser.Factory brow) {
        IDESettings.setBrowser("ExternalWWWBrowser", brow);
    }

    private static void setBrowser(String prefId, HtmlBrowser.Factory brow) {
        try {
            if (brow == null) {
                IDESettings.getPreferences().put(prefId, "");
                return;
            }
            Lookup.Item item = Lookup.getDefault().lookupItem(new Lookup.Template(HtmlBrowser.Factory.class, null, (Object)brow));
            if (item != null) {
                IDESettings.getPreferences().put(prefId, item.getId());
            } else {
                Logger.getLogger(IDESettings.class.getName()).warning("IDESettings: Cannot find browser in lookup");
                IDESettings.getPreferences().put(prefId, "");
            }
        }
        catch (Exception ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    private static HtmlBrowser.Factory getBrowser(String prefId, boolean mustBeExternal) {
        try {
            String obj = IDESettings.getPreferences().get(prefId, null);
            if (obj instanceof String && !"".equals(obj)) {
                Lookup.Item item = Lookup.getDefault().lookupItem(new Lookup.Template(HtmlBrowser.Factory.class, obj, (Object)null));
                return item == null ? null : (HtmlBrowser.Factory)item.getInstance();
            }
            if (obj == null || "".equals(obj)) {
                Lookup.Result res = Lookup.getDefault().lookupResult(HtmlBrowser.Factory.class);
                for (HtmlBrowser.Factory brow : res.allInstances()) {
                    FileObject fo = FileUtil.getConfigFile((String)"Services/Browsers");
                    DataFolder folder = DataFolder.findFolder((FileObject)fo);
                    DataObject[] dobjs = folder.getChildren();
                    for (int i = 0; i < dobjs.length; ++i) {
                        Object o = null;
                        try {
                            InstanceCookie cookie;
                            if (Boolean.TRUE.equals(dobjs[i].getPrimaryFile().getAttribute("hidden")) || mustBeExternal && Boolean.TRUE.equals(dobjs[i].getPrimaryFile().getAttribute("internal")) || (cookie = (InstanceCookie)dobjs[i].getCookie(InstanceCookie.class)) == null || (o = cookie.instanceCreate()) == null || !o.equals((Object)brow)) continue;
                            return brow;
                        }
                        catch (IOException ex) {
                            Logger.getLogger(IDESettings.class.getName()).log(Level.WARNING, null, ex);
                            continue;
                        }
                        catch (ClassNotFoundException ex) {
                            Logger.getLogger(IDESettings.class.getName()).log(Level.WARNING, null, ex);
                        }
                    }
                }
                return null;
            }
        }
        catch (Exception ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        return null;
    }

    private static boolean isExternal(HtmlBrowser.Factory brow) {
        FileObject fo = FileUtil.getConfigFile((String)"Services/Browsers");
        DataFolder folder = DataFolder.findFolder((FileObject)fo);
        DataObject[] dobjs = folder.getChildren();
        for (int i = 0; i < dobjs.length; ++i) {
            Object o = null;
            try {
                InstanceCookie cookie;
                if (Boolean.TRUE.equals(dobjs[i].getPrimaryFile().getAttribute("hidden")) || !Boolean.TRUE.equals(dobjs[i].getPrimaryFile().getAttribute("internal")) || (cookie = (InstanceCookie)dobjs[i].getCookie(InstanceCookie.class)) == null || (o = cookie.instanceCreate()) == null || !o.equals((Object)brow)) continue;
                return false;
            }
            catch (IOException ex) {
                Logger.getLogger(IDESettings.class.getName()).log(Level.WARNING, null, ex);
                continue;
            }
            catch (ClassNotFoundException ex) {
                Logger.getLogger(IDESettings.class.getName()).log(Level.WARNING, null, ex);
            }
        }
        return false;
    }

    private static HtmlBrowser.FactoryEditor createHtmlBrowserFactoryEditor() {
        return new HtmlBrowser.FactoryEditor(){

            @Override
            public void setValue(Object value) {
                IDESettings.setWWWBrowser((HtmlBrowser.Factory)value);
            }

            @Override
            public Object getValue() {
                return IDESettings.getWWWBrowser();
            }
        };
    }

}

