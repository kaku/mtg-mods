/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallableSystemAction
 */
package org.netbeans.core.actions;

import java.net.MalformedURLException;
import java.net.URL;
import org.openide.awt.HtmlBrowser;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallableSystemAction;

public class HTMLViewAction
extends CallableSystemAction {
    public HTMLViewAction() {
        this.putValue("noIconInMenu", (Object)Boolean.TRUE);
    }

    protected String iconResource() {
        return "org/netbeans/core/resources/actions/htmlView.gif";
    }

    public void performAction() {
        StatusDisplayer.getDefault().setStatusText(NbBundle.getBundle(HTMLViewAction.class).getString("CTL_OpeningBrowser"));
        try {
            HtmlBrowser.URLDisplayer.getDefault().showURL(new URL(HtmlBrowser.getHomePage()));
        }
        catch (MalformedURLException e) {
            String home = HtmlBrowser.getHomePage();
            if (!home.startsWith("http://")) {
                home = "http://" + home;
            }
            try {
                HtmlBrowser.URLDisplayer.getDefault().showURL(new URL(home));
            }
            catch (MalformedURLException e1) {
                Exceptions.printStackTrace((Throwable)e1);
            }
        }
    }

    protected boolean asynchronous() {
        return false;
    }

    public String getName() {
        return NbBundle.getBundle(HTMLViewAction.class).getString("HTMLView");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(HTMLViewAction.class);
    }
}

