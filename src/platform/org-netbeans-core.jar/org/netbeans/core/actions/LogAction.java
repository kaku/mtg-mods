/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.Places
 */
package org.netbeans.core.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.actions.Bundle;
import org.netbeans.core.actions.LogViewerSupport;
import org.openide.modules.Places;

public class LogAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent evt) {
        File userDir = Places.getUserDirectory();
        if (userDir == null) {
            return;
        }
        File f = new File(userDir, "/var/log/messages.log");
        LogViewerSupport p = new LogViewerSupport(f, Bundle.MSG_ShortLogTab_name());
        try {
            p.showLogViewer();
        }
        catch (IOException e) {
            Logger.getLogger(LogAction.class.getName()).log(Level.INFO, "Showing IDE log action failed", e);
        }
    }
}

