/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallbackSystemAction
 */
package org.netbeans.core.actions;

import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallbackSystemAction;

public class PreviousViewCallbackAction
extends CallbackSystemAction {
    public String getName() {
        return NbBundle.getMessage(PreviousViewCallbackAction.class, (String)"LBL_PreviousViewCallbackAction");
    }

    public Object getActionMapKey() {
        return "PreviousViewAction";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }
}

