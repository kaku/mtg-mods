/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallbackSystemAction
 */
package org.netbeans.core.actions;

import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallbackSystemAction;

public class NextViewCallbackAction
extends CallbackSystemAction {
    public String getName() {
        return NbBundle.getMessage(NextViewCallbackAction.class, (String)"LBL_NextViewCallbackAction");
    }

    public Object getActionMapKey() {
        return "NextViewAction";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }
}

