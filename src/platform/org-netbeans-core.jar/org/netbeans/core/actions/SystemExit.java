/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.LifecycleManager
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.actions.CallableSystemAction
 */
package org.netbeans.core.actions;

import org.openide.LifecycleManager;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.actions.CallableSystemAction;

public class SystemExit
extends CallableSystemAction
implements Runnable {
    private static final RequestProcessor RP = new RequestProcessor(SystemExit.class.getName(), 3);
    private static final long serialVersionUID = 5198683109749927396L;

    public String getName() {
        return NbBundle.getMessage(SystemExit.class, (String)"Exit");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx("org.netbeans.core.actions.SystemExit");
    }

    protected boolean asynchronous() {
        return false;
    }

    public void performAction() {
        RP.post((Runnable)this);
    }

    @Override
    public void run() {
        LifecycleManager.getDefault().exit();
    }

    protected void initialize() {
        super.initialize();
        this.putValue("noIconInMenu", (Object)Boolean.TRUE);
    }
}

