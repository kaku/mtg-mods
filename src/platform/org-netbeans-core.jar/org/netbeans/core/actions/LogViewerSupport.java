/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.windows.IOProvider
 *  org.openide.windows.InputOutput
 *  org.openide.windows.OutputWriter
 */
package org.netbeans.core.actions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.RequestProcessor;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;
import org.openide.windows.OutputWriter;

public class LogViewerSupport
implements Runnable {
    private static final RequestProcessor RP = new RequestProcessor(LogViewerSupport.class);
    boolean shouldStop = false;
    FileInputStream filestream = null;
    BufferedReader ins;
    InputOutput io;
    File fileName;
    String ioName;
    int lines;
    Ring ring;
    private final RequestProcessor.Task task;

    public LogViewerSupport(File fileName, String ioName) {
        this.task = RP.create((Runnable)this);
        this.fileName = fileName;
        this.ioName = ioName;
    }

    private void init() {
        int LINES = 2000;
        int OLD_LINES = 2000;
        this.ring = new Ring(2000);
        try {
            String line;
            while ((line = this.ins.readLine()) != null) {
                this.ring.add(line);
            }
        }
        catch (IOException e) {
            Logger.getLogger(LogViewerSupport.class.getName()).log(Level.INFO, null, e);
        }
        this.lines = this.ring.output();
        this.ring.setMaxCount(2000);
    }

    @Override
    public void run() {
        int MAX_LINES = 10000;
        this.shouldStop = this.io.isClosed();
        if (!this.shouldStop) {
            try {
                String line;
                if (this.lines >= 10000) {
                    this.io.getOut().reset();
                    this.lines = this.ring.output();
                }
                while ((line = this.ins.readLine()) != null) {
                    if ((line = this.ring.add(line)) == null) continue;
                    this.io.getOut().println(line);
                    ++this.lines;
                }
            }
            catch (IOException e) {
                Logger.getLogger(LogViewerSupport.class.getName()).log(Level.INFO, null, e);
            }
            this.task.schedule(10000);
        } else {
            this.stopUpdatingLogViewer();
        }
    }

    public void showLogViewer() throws IOException {
        this.shouldStop = false;
        this.io = IOProvider.getDefault().getIO(this.ioName, false);
        this.io.getOut().reset();
        this.io.select();
        this.filestream = new FileInputStream(this.fileName);
        this.ins = new BufferedReader(new InputStreamReader(this.filestream));
        RP.post(new Runnable(){

            @Override
            public void run() {
                LogViewerSupport.this.init();
                LogViewerSupport.this.task.schedule(0);
            }
        });
    }

    public void stopUpdatingLogViewer() {
        try {
            this.ins.close();
            this.filestream.close();
            this.io.closeInputOutput();
            this.io.setOutputVisible(false);
        }
        catch (IOException e) {
            Logger.getLogger(LogViewerSupport.class.getName()).log(Level.INFO, null, e);
        }
    }

    private class Ring {
        private int maxCount;
        private int count;
        private LinkedList<String> anchor;

        public Ring(int max) {
            this.maxCount = max;
            this.count = 0;
            this.anchor = new LinkedList();
        }

        public String add(String line) {
            if (line == null || line.equals("")) {
                return null;
            }
            while (this.count >= this.maxCount) {
                this.anchor.removeFirst();
                --this.count;
            }
            this.anchor.addLast(line);
            ++this.count;
            return line;
        }

        public void setMaxCount(int newMax) {
            this.maxCount = newMax;
        }

        public int output() {
            int i = 0;
            for (String s : this.anchor) {
                LogViewerSupport.this.io.getOut().println(s);
                ++i;
            }
            return i;
        }

        public void reset() {
            this.anchor = new LinkedList();
        }
    }

}

