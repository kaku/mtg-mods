/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.actions;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String MSG_LogTab_name() {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_LogTab_name");
    }

    static String MSG_ShortLogTab_name() {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_ShortLogTab_name");
    }

    private void Bundle() {
    }
}

