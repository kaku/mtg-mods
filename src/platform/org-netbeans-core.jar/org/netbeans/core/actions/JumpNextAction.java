/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.ActionPerformer
 *  org.openide.util.actions.CallbackSystemAction
 */
package org.netbeans.core.actions;

import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.ActionPerformer;
import org.openide.util.actions.CallbackSystemAction;

public class JumpNextAction
extends CallbackSystemAction {
    protected void initialize() {
        super.initialize();
        this.setSurviveFocusChange(true);
        this.putProperty((Object)"noIconInMenu", (Object)Boolean.TRUE);
    }

    protected String iconResource() {
        return "org/netbeans/core/resources/actions/nextOutJump.gif";
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(JumpNextAction.class);
    }

    public String getName() {
        return NbBundle.getBundle(JumpNextAction.class).getString("JumpNextAction");
    }

    protected boolean asynchronous() {
        return false;
    }

    public void setActionPerformer(ActionPerformer performer) {
        throw new UnsupportedOperationException();
    }

    public Object getActionMapKey() {
        return "jumpNext";
    }
}

