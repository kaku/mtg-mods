/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.util.actions.CallableSystemAction
 */
package org.netbeans.core.actions;

import java.awt.Dialog;
import java.awt.event.ActionListener;
import javax.swing.InputMap;
import javax.swing.JDialog;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import org.netbeans.core.ui.ProductInformationPanel;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.util.actions.CallableSystemAction;

public class AboutAction
extends CallableSystemAction {
    public AboutAction() {
        this.putValue("noIconInMenu", (Object)Boolean.TRUE);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void performAction() {
        DialogDescriptor descriptor = new DialogDescriptor((Object)new ProductInformationPanel(), NbBundle.getMessage(AboutAction.class, (String)"About_title"), true, new Object[0], (Object)null, 0, null, null);
        Dialog dlg = null;
        try {
            dlg = DialogDisplayer.getDefault().createDialog(descriptor);
            if (Utilities.isMac() && dlg instanceof JDialog) {
                JDialog d = (JDialog)dlg;
                InputMap map = d.getRootPane().getInputMap(1);
                map.put(KeyStroke.getKeyStroke(87, 4), "Escape");
                d.getRootPane().putClientProperty("nb.about.dialog", Boolean.TRUE);
            }
            dlg.setResizable(false);
            dlg.setVisible(true);
        }
        finally {
            if (dlg != null) {
                dlg.dispose();
            }
        }
    }

    protected boolean asynchronous() {
        return false;
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(AboutAction.class);
    }

    public String getName() {
        return NbBundle.getMessage(AboutAction.class, (String)"About");
    }
}

