/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.CLIHandler
 *  org.netbeans.TopSecurityManager
 *  org.netbeans.core.startup.CLIOptions
 *  org.netbeans.core.startup.Main
 *  org.netbeans.core.startup.layers.SessionManager
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 */
package org.netbeans.core;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.CLIHandler;
import org.netbeans.TopSecurityManager;
import org.netbeans.core.ExitDialog;
import org.netbeans.core.NbLifecycleManager;
import org.netbeans.core.NbLoaderPool;
import org.netbeans.core.WindowSystem;
import org.netbeans.core.startup.CLIOptions;
import org.netbeans.core.startup.Main;
import org.netbeans.core.startup.layers.SessionManager;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;

final class NbLifeExit
implements Runnable {
    private static final RequestProcessor RP = new RequestProcessor("Nb Exit");
    private final int type;
    private final int status;
    private final Future<Boolean> waitFor;
    private final CountDownLatch onExit;

    NbLifeExit(int type, int status, CountDownLatch onExit) {
        this(type, status, null, onExit);
    }

    private NbLifeExit(int type, int status, Future<Boolean> waitFor, CountDownLatch onExit) {
        this.type = type;
        this.status = status;
        this.waitFor = waitFor;
        this.onExit = onExit;
        NbLifecycleManager.LOG.log(Level.FINE, "NbLifeExit({0}, {1}, {2}, {3}) = {4}", new Object[]{type, status, waitFor, onExit, this});
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void run() {
        NbLifecycleManager.LOG.log(Level.FINE, "{0}.run()", this);
        switch (this.type) {
            case 0: {
                this.doExit(this.status);
                break;
            }
            case 1: {
                this.doStopInfra(this.status);
                break;
            }
            case 2: {
                int s = 3;
                try {
                    if (this.waitFor != null && this.waitFor.get().booleanValue()) {
                        s = 4;
                    }
                }
                catch (InterruptedException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
                catch (ExecutionException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
                Mutex.EVENT.readAccess((Runnable)new NbLifeExit(s, this.status, null, this.onExit));
                break;
            }
            case 3: 
            case 4: {
                this.doApproved(this.type == 4, this.status);
                break;
            }
            case 5: {
                try {
                    boolean doExit = !Boolean.getBoolean("netbeans.close.no.exit");
                    NbLifecycleManager.LOG.log(Level.FINE, "Calling exit: {0}", doExit);
                    if (!doExit) break;
                    TopSecurityManager.exit((int)this.status);
                }
                finally {
                    NbLifecycleManager.LOG.log(Level.FINE, "After exit!");
                    this.onExit.countDown();
                }
            }
            default: {
                throw new IllegalStateException("Type: " + this.type);
            }
        }
    }

    private void doExit(int status) {
        Future res = System.getProperty("netbeans.close") != null || ExitDialog.showDialog() ? Main.getModuleSystem().shutDownAsync((Runnable)new NbLifeExit(1, status, null, this.onExit)) : null;
        RP.post((Runnable)new NbLifeExit(2, status, res, this.onExit));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void doStopInfra(int status) {
        CLIHandler.stopServer();
        WindowSystem windowSystem = (WindowSystem)Lookup.getDefault().lookup(WindowSystem.class);
        boolean gui = CLIOptions.isGui();
        if (windowSystem != null && gui) {
            windowSystem.hide();
            windowSystem.save();
        }
        if (Boolean.getBoolean("netbeans.close.when.invisible")) {
            try {
                TopSecurityManager.exit((int)status);
            }
            finally {
                this.onExit.countDown();
            }
        }
    }

    private void doApproved(boolean isApproved, int status) throws ThreadDeath {
        if (isApproved) {
            try {
                try {
                    NbLoaderPool.store();
                }
                catch (IOException ioe) {
                    Logger.getLogger(NbLifecycleManager.class.getName()).log(Level.WARNING, null, ioe);
                }
                SessionManager.getDefault().close();
            }
            catch (ThreadDeath td) {
                throw td;
            }
            catch (Throwable t) {
                Exceptions.printStackTrace((Throwable)t);
            }
            Task exitTask = new Task((Runnable)new NbLifeExit(5, status, null, this.onExit));
            RP.post((Runnable)exitTask);
        } else {
            this.onExit.countDown();
        }
    }
}

