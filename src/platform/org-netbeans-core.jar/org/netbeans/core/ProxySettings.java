/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.keyring.Keyring
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.NetworkSettings
 *  org.openide.util.NetworkSettings$ProxyCredentialsProvider
 *  org.openide.util.Parameters
 *  org.openide.util.Utilities
 */
package org.netbeans.core;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import org.netbeans.api.keyring.Keyring;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.NetworkSettings;
import org.openide.util.Parameters;
import org.openide.util.Utilities;

public class ProxySettings {
    public static final String PROXY_HTTP_HOST = "proxyHttpHost";
    public static final String PROXY_HTTP_PORT = "proxyHttpPort";
    public static final String PROXY_HTTPS_HOST = "proxyHttpsHost";
    public static final String PROXY_HTTPS_PORT = "proxyHttpsPort";
    public static final String PROXY_SOCKS_HOST = "proxySocksHost";
    public static final String PROXY_SOCKS_PORT = "proxySocksPort";
    public static final String NOT_PROXY_HOSTS = "proxyNonProxyHosts";
    public static final String PROXY_TYPE = "proxyType";
    public static final String USE_PROXY_AUTHENTICATION = "useProxyAuthentication";
    public static final String PROXY_AUTHENTICATION_USERNAME = "proxyAuthenticationUsername";
    public static final String PROXY_AUTHENTICATION_PASSWORD = "proxyAuthenticationPassword";
    public static final String USE_PROXY_ALL_PROTOCOLS = "useProxyAllProtocols";
    public static final String DIRECT = "DIRECT";
    public static final String PAC = "PAC";
    public static final String SYSTEM_PROXY_HTTP_HOST = "systemProxyHttpHost";
    public static final String SYSTEM_PROXY_HTTP_PORT = "systemProxyHttpPort";
    public static final String SYSTEM_PROXY_HTTPS_HOST = "systemProxyHttpsHost";
    public static final String SYSTEM_PROXY_HTTPS_PORT = "systemProxyHttpsPort";
    public static final String SYSTEM_PROXY_SOCKS_HOST = "systemProxySocksHost";
    public static final String SYSTEM_PROXY_SOCKS_PORT = "systemProxySocksPort";
    public static final String SYSTEM_NON_PROXY_HOSTS = "systemProxyNonProxyHosts";
    public static final String SYSTEM_PAC = "systemPAC";
    public static final String TEST_SYSTEM_PROXY_HTTP_HOST = "testSystemProxyHttpHost";
    public static final String TEST_SYSTEM_PROXY_HTTP_PORT = "testSystemProxyHttpPort";
    public static final String HTTP_CONNECTION_TEST_URL = "http://netbeans.org";
    private static String presetNonProxyHosts;
    public static final int DIRECT_CONNECTION = 0;
    public static final int AUTO_DETECT_PROXY = 1;
    public static final int MANUAL_SET_PROXY = 2;
    public static final int AUTO_DETECT_PAC = 3;
    public static final int MANUAL_SET_PAC = 4;
    private static final Logger LOGGER;

    private static Preferences getPreferences() {
        return NbPreferences.forModule(ProxySettings.class);
    }

    public static String getHttpHost() {
        return ProxySettings.normalizeProxyHost(ProxySettings.getPreferences().get("proxyHttpHost", ""));
    }

    public static String getHttpPort() {
        return ProxySettings.getPreferences().get("proxyHttpPort", "");
    }

    public static String getHttpsHost() {
        if (ProxySettings.useProxyAllProtocols()) {
            return ProxySettings.getHttpHost();
        }
        return ProxySettings.getPreferences().get("proxyHttpsHost", "");
    }

    public static String getHttpsPort() {
        if (ProxySettings.useProxyAllProtocols()) {
            return ProxySettings.getHttpPort();
        }
        return ProxySettings.getPreferences().get("proxyHttpsPort", "");
    }

    public static String getSocksHost() {
        if (ProxySettings.useProxyAllProtocols()) {
            return ProxySettings.getHttpHost();
        }
        return ProxySettings.getPreferences().get("proxySocksHost", "");
    }

    public static String getSocksPort() {
        if (ProxySettings.useProxyAllProtocols()) {
            return ProxySettings.getHttpPort();
        }
        return ProxySettings.getPreferences().get("proxySocksPort", "");
    }

    public static String getNonProxyHosts() {
        String hosts = ProxySettings.getPreferences().get("proxyNonProxyHosts", ProxySettings.getDefaultUserNonProxyHosts());
        return ProxySettings.compactNonProxyHosts(hosts);
    }

    public static int getProxyType() {
        int type = ProxySettings.getPreferences().getInt("proxyType", 1);
        if (1 == type) {
            type = ProxySettings.getSystemPac() != null ? 3 : 1;
        }
        return type;
    }

    public static String getSystemHttpHost() {
        return ProxySettings.getPreferences().get("systemProxyHttpHost", "");
    }

    public static String getSystemHttpPort() {
        return ProxySettings.getPreferences().get("systemProxyHttpPort", "");
    }

    public static String getSystemHttpsHost() {
        return ProxySettings.getPreferences().get("systemProxyHttpsHost", "");
    }

    public static String getSystemHttpsPort() {
        return ProxySettings.getPreferences().get("systemProxyHttpsPort", "");
    }

    public static String getSystemSocksHost() {
        return ProxySettings.getPreferences().get("systemProxySocksHost", "");
    }

    public static String getSystemSocksPort() {
        return ProxySettings.getPreferences().get("systemProxySocksPort", "");
    }

    public static String getSystemNonProxyHosts() {
        return ProxySettings.getPreferences().get("systemProxyNonProxyHosts", ProxySettings.getModifiedNonProxyHosts(""));
    }

    public static String getSystemPac() {
        return ProxySettings.getPreferences().get("systemPAC", null);
    }

    public static String getTestSystemHttpHost() {
        return ProxySettings.getPreferences().get("testSystemProxyHttpHost", "");
    }

    public static String getTestSystemHttpPort() {
        return ProxySettings.getPreferences().get("testSystemProxyHttpPort", "");
    }

    public static boolean useAuthentication() {
        return ProxySettings.getPreferences().getBoolean("useProxyAuthentication", false);
    }

    public static boolean useProxyAllProtocols() {
        return ProxySettings.getPreferences().getBoolean("useProxyAllProtocols", false);
    }

    public static String getAuthenticationUsername() {
        return ProxySettings.getPreferences().get("proxyAuthenticationUsername", "");
    }

    public static char[] getAuthenticationPassword() {
        char[] pwd;
        String old = ProxySettings.getPreferences().get("proxyAuthenticationPassword", null);
        if (old != null) {
            ProxySettings.getPreferences().remove("proxyAuthenticationPassword");
            ProxySettings.setAuthenticationPassword(old.toCharArray());
        }
        return (pwd = Keyring.read((String)"proxyAuthenticationPassword")) != null ? pwd : new char[]{};
    }

    public static void setAuthenticationPassword(char[] password) {
        Keyring.save((String)"proxyAuthenticationPassword", (char[])password, (String)NbBundle.getMessage(ProxySettings.class, (String)"ProxySettings.password.description"));
    }

    public static void addPreferenceChangeListener(PreferenceChangeListener l) {
        ProxySettings.getPreferences().addPreferenceChangeListener(l);
    }

    public static void removePreferenceChangeListener(PreferenceChangeListener l) {
        ProxySettings.getPreferences().removePreferenceChangeListener(l);
    }

    private static String getPresetNonProxyHosts() {
        if (presetNonProxyHosts == null) {
            presetNonProxyHosts = System.getProperty("http.nonProxyHosts", "");
        }
        return presetNonProxyHosts;
    }

    private static String getDefaultUserNonProxyHosts() {
        return ProxySettings.getModifiedNonProxyHosts(ProxySettings.getSystemNonProxyHosts());
    }

    private static /* varargs */ String concatProxies(String ... proxies) {
        StringBuilder sb = new StringBuilder();
        for (String n : proxies) {
            if (n == null || (n = n.trim()).isEmpty()) continue;
            if (sb.length() > 0 && sb.charAt(sb.length() - 1) != '|' && !n.startsWith("|")) {
                sb.append('|');
            }
            sb.append(n);
        }
        return sb.toString();
    }

    private static String getModifiedNonProxyHosts(String systemPreset) {
        String fromUser;
        String fromSystem = systemPreset.replaceAll(";", "|").replaceAll(",", "|");
        String string = fromUser = ProxySettings.getPresetNonProxyHosts() == null ? "" : ProxySettings.getPresetNonProxyHosts().replaceAll(";", "|").replaceAll(",", "|");
        if (Utilities.isWindows()) {
            fromSystem = ProxySettings.addReguralToNonProxyHosts(fromSystem);
        }
        String staticNonProxyHosts = NbBundle.getMessage(ProxySettings.class, (String)"StaticNonProxyHosts");
        String nonProxy = ProxySettings.concatProxies(fromUser, fromSystem, staticNonProxyHosts);
        try {
            String localhost = InetAddress.getLocalHost().getHostName();
            if (!"localhost".equals(localhost)) {
                nonProxy = nonProxy + "|" + localhost;
            }
        }
        catch (UnknownHostException e) {
            // empty catch block
        }
        return ProxySettings.compactNonProxyHosts(nonProxy);
    }

    private static String compactNonProxyHosts(String hosts) {
        StringTokenizer st = new StringTokenizer(hosts, ",");
        StringBuilder nonProxyHosts = new StringBuilder();
        while (st.hasMoreTokens()) {
            String h = st.nextToken().trim();
            if (h.length() == 0) continue;
            if (nonProxyHosts.length() > 0) {
                nonProxyHosts.append("|");
            }
            nonProxyHosts.append(h);
        }
        st = new StringTokenizer(nonProxyHosts.toString(), "|");
        HashSet<String> set = new HashSet<String>();
        StringBuilder compactedProxyHosts = new StringBuilder();
        while (st.hasMoreTokens()) {
            String t = st.nextToken();
            if (!set.add(t.toLowerCase(Locale.US))) continue;
            if (compactedProxyHosts.length() > 0) {
                compactedProxyHosts.append('|');
            }
            compactedProxyHosts.append(t);
        }
        return compactedProxyHosts.toString();
    }

    private static String addReguralToNonProxyHosts(String nonProxyHost) {
        StringTokenizer st = new StringTokenizer(nonProxyHost, "|");
        StringBuilder reguralProxyHosts = new StringBuilder();
        while (st.hasMoreTokens()) {
            String t = st.nextToken();
            if (t.indexOf(42) == -1) {
                t = t + '*';
            }
            if (reguralProxyHosts.length() > 0) {
                reguralProxyHosts.append('|');
            }
            reguralProxyHosts.append(t);
        }
        return reguralProxyHosts.toString();
    }

    public static String normalizeProxyHost(String proxyHost) {
        if (proxyHost.toLowerCase(Locale.US).startsWith("http://")) {
            return proxyHost.substring(7, proxyHost.length());
        }
        return proxyHost;
    }

    private static InetSocketAddress analyzeProxy(URI uri) {
        Parameters.notNull((CharSequence)"uri", (Object)uri);
        List<Proxy> proxies = ProxySelector.getDefault().select(uri);
        assert (proxies != null);
        assert (!proxies.isEmpty());
        String protocol = uri.getScheme();
        Proxy p = proxies.get(0);
        if (Proxy.Type.DIRECT == p.type()) {
            return null;
        }
        if (protocol == null || (protocol.startsWith("http") || protocol.equals("ftp")) && Proxy.Type.HTTP == p.type() || !protocol.startsWith("http") && !protocol.equals("ftp")) {
            if (p.address() instanceof InetSocketAddress) {
                return (InetSocketAddress)p.address();
            }
            LOGGER.log(Level.INFO, p.address() + " is not instanceof InetSocketAddress but " + p.address().getClass());
            return null;
        }
        return null;
    }

    public static void reload() {
        Reloader reloader = (Reloader)Lookup.getDefault().lookup(Reloader.class);
        reloader.reload();
    }

    static {
        LOGGER = Logger.getLogger(ProxySettings.class.getName());
    }

    public static abstract class Reloader {
        public abstract void reload();
    }

    public static class NbProxyCredentialsProvider
    extends NetworkSettings.ProxyCredentialsProvider {
        public String getProxyHost(URI u) {
            if (ProxySettings.getPreferences() == null) {
                return null;
            }
            InetSocketAddress sa = ProxySettings.analyzeProxy(u);
            return sa == null ? null : sa.getHostName();
        }

        public String getProxyPort(URI u) {
            if (ProxySettings.getPreferences() == null) {
                return null;
            }
            InetSocketAddress sa = ProxySettings.analyzeProxy(u);
            return sa == null ? null : Integer.toString(sa.getPort());
        }

        protected String getProxyUserName(URI u) {
            if (ProxySettings.getPreferences() == null) {
                return null;
            }
            return ProxySettings.getAuthenticationUsername();
        }

        protected char[] getProxyPassword(URI u) {
            if (ProxySettings.getPreferences() == null) {
                return null;
            }
            return ProxySettings.getAuthenticationPassword();
        }

        protected boolean isProxyAuthentication(URI u) {
            if (ProxySettings.getPreferences() == null) {
                return false;
            }
            return ProxySettings.getPreferences().getBoolean("useProxyAuthentication", false);
        }
    }

}

