/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.ui;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String LBL_description(Object product_version, Object Java_version, Object VM_version, Object OS, Object encoding, Object locale, Object user_dir, Object cache_dir, Object updates, Object font_size, Object Java_runtime) {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_description", (Object)product_version, (Object)Java_version, (Object)VM_version, (Object)OS, (Object[])new Object[]{encoding, locale, user_dir, cache_dir, updates, font_size, Java_runtime});
    }

    static String check_for_updates() {
        return NbBundle.getMessage(Bundle.class, (String)"check_for_updates");
    }

    static String to_version(Object arg0) {
        return NbBundle.getMessage(Bundle.class, (String)"to_version", (Object)arg0);
    }

    static String updates_found(Object content_description) {
        return NbBundle.getMessage(Bundle.class, (String)"updates_found", (Object)content_description);
    }

    static String updates_not_found(Object content_description) {
        return NbBundle.getMessage(Bundle.class, (String)"updates_not_found", (Object)content_description);
    }

    private void Bundle() {
    }
}

