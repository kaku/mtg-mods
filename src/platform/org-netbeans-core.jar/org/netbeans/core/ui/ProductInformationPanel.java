/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.startup.Splash
 *  org.openide.awt.CheckForUpdatesProvider
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.awt.StatusDisplayer
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.Places
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.core.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Locale;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import org.netbeans.core.actions.HTMLViewAction;
import org.netbeans.core.startup.Splash;
import org.netbeans.core.ui.Bundle;
import org.openide.awt.CheckForUpdatesProvider;
import org.openide.awt.HtmlBrowser;
import org.openide.awt.StatusDisplayer;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.modules.Places;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

public class ProductInformationPanel
extends JPanel
implements HyperlinkListener {
    URL url = null;
    Icon about;
    private static final String CHECK_FOR_UPDATES_ACTION = "check-for-updates";
    private static final int FONT_SIZE = ProductInformationPanel.getFontSize();
    private JTextPane copyright;
    private JTextPane description;
    private JLabel imageLabel;
    private JPanel imagePanel;
    private JButton jButton2;
    private JPanel jPanel1;
    private JScrollPane jScrollPane2;
    private JScrollPane jScrollPane3;

    public ProductInformationPanel() {
        this.initComponents();
        this.imageLabel.setCursor(Cursor.getPredefinedCursor(12));
        this.description.setText(Bundle.LBL_description(ProductInformationPanel.getProductVersionValue(), ProductInformationPanel.getJavaValue(), ProductInformationPanel.getVMValue(), ProductInformationPanel.getOperatingSystemValue(), ProductInformationPanel.getEncodingValue(), ProductInformationPanel.getSystemLocaleValue(), this.getUserDirValue(), Places.getCacheDirectory().getAbsolutePath(), "", FONT_SIZE, ProductInformationPanel.getJavaRuntime()));
        this.description.setCursor(Cursor.getPredefinedCursor(3));
        this.description.putClientProperty("JEditorPane.honorDisplayProperties", Boolean.TRUE);
        RequestProcessor.getDefault().post(new Runnable(){

            @Override
            public void run() {
                final String updates = ProductInformationPanel.getUpdates();
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        ProductInformationPanel.this.description.setText(Bundle.LBL_description(ProductInformationPanel.getProductVersionValue(), ProductInformationPanel.getJavaValue(), ProductInformationPanel.getVMValue(), ProductInformationPanel.getOperatingSystemValue(), ProductInformationPanel.getEncodingValue(), ProductInformationPanel.getSystemLocaleValue(), ProductInformationPanel.this.getUserDirValue(), Places.getCacheDirectory().getAbsolutePath(), updates, FONT_SIZE, ProductInformationPanel.getJavaRuntime()));
                        ProductInformationPanel.this.description.setCursor(null);
                        ProductInformationPanel.this.description.revalidate();
                    }
                });
            }

        });
        this.description.setCaretPosition(0);
        this.description.addHyperlinkListener(this);
        this.copyright.addHyperlinkListener(this);
        this.copyright.setBackground(this.getBackground());
        this.copyright.putClientProperty("JEditorPane.honorDisplayProperties", Boolean.TRUE);
        this.about = new ImageIcon(Splash.loadContent((boolean)true));
        this.imageLabel.setIcon(this.about);
        this.imageLabel.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent evt) {
                try {
                    ProductInformationPanel.this.url = new URL(NbBundle.getMessage(ProductInformationPanel.class, (String)"URL_ON_IMG"));
                    ProductInformationPanel.this.showUrl();
                }
                catch (MalformedURLException ex) {
                    // empty catch block
                }
            }
        });
        this.description.addHyperlinkListener(new HyperlinkListener(){

            @Override
            public void hyperlinkUpdate(HyperlinkEvent e) {
                if (HyperlinkEvent.EventType.ENTERED == e.getEventType()) {
                    if ("check-for-updates".equals(e.getDescription())) {
                        ProductInformationPanel.this.description.setToolTipText(Bundle.check_for_updates());
                    } else if (e.getURL() != null) {
                        ProductInformationPanel.this.description.setToolTipText(e.getURL().toExternalForm());
                    }
                } else if (HyperlinkEvent.EventType.EXITED == e.getEventType()) {
                    ProductInformationPanel.this.description.setToolTipText(null);
                } else if (HyperlinkEvent.EventType.ACTIVATED.equals(e.getEventType())) {
                    if ("check-for-updates".equals(e.getDescription())) {
                        ProductInformationPanel.checkForUpdates();
                    } else {
                        HtmlBrowser.URLDisplayer.getDefault().showURL(e.getURL());
                    }
                }
            }
        });
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.jButton2 = new JButton();
        this.jScrollPane3 = new JScrollPane();
        this.copyright = new JTextPane();
        this.jScrollPane2 = new JScrollPane();
        this.description = new JTextPane();
        this.imagePanel = new JPanel();
        this.imageLabel = new JLabel();
        this.jPanel1.setLayout(new GridBagLayout());
        this.jButton2.setMnemonic(NbBundle.getMessage(ProductInformationPanel.class, (String)"MNE_Close").charAt(0));
        this.jButton2.setText(NbBundle.getMessage(ProductInformationPanel.class, (String)"LBL_Close"));
        this.jButton2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                ProductInformationPanel.this.jButton2ActionPerformed(evt);
            }
        });
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.jPanel1.add((Component)this.jButton2, gridBagConstraints);
        this.jScrollPane3.setBorder(null);
        this.copyright.setBorder(null);
        this.copyright.setContentType("text/html");
        this.copyright.setEditable(false);
        this.copyright.setText(ProductInformationPanel.getCopyrightText());
        this.copyright.setCaretPosition(0);
        this.copyright.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent evt) {
                ProductInformationPanel.this.copyrightMouseClicked(evt);
            }
        });
        this.jScrollPane3.setViewportView(this.copyright);
        this.description.setContentType("text/html");
        this.description.setEditable(false);
        this.description.setText("<div style=\"font-size: 12pt; font-family: Verdana, 'Verdana CE',  Arial, 'Arial CE', 'Lucida Grande CE', lucida, 'Helvetica CE', sans-serif;\">\n    <b>Product Version:</b> {0}<br> <b>Java:</b> {1}; {2}<br> <b>System:</b> {3}; {4}; {5}<br><b>Userdir:</b> {6}</div>");
        this.jScrollPane2.setViewportView(this.description);
        this.imagePanel.setLayout(new BorderLayout());
        this.imageLabel.setHorizontalAlignment(0);
        this.imagePanel.add((Component)this.imageLabel, "Center");
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.imagePanel, GroupLayout.Alignment.LEADING, -1, 190, 32767).addComponent(this.jScrollPane2, GroupLayout.Alignment.LEADING, -1, 190, 32767).addComponent(this.jScrollPane3, -1, 190, 32767).addComponent(this.jPanel1, GroupLayout.Alignment.LEADING, -1, 190, 32767)).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.imagePanel, -1, 27, 32767).addGap(14, 14, 14).addComponent(this.jScrollPane3, -1, 151, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.jScrollPane2, -1, 129, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.jPanel1, -2, -1, -2).addContainerGap()));
    }

    private void copyrightMouseClicked(MouseEvent evt) {
        this.showUrl();
    }

    private void jButton2ActionPerformed(ActionEvent evt) {
        this.closeDialog();
    }

    private void closeDialog() {
        Window w = SwingUtilities.getWindowAncestor(this);
        w.setVisible(false);
        w.dispose();
    }

    private void showUrl() {
        if (this.url != null) {
            StatusDisplayer.getDefault().setStatusText(NbBundle.getBundle(HTMLViewAction.class).getString("CTL_OpeningBrowser"));
            HtmlBrowser.URLDisplayer.getDefault().showURL(this.url);
        }
    }

    public static String getProductVersionValue() {
        return MessageFormat.format(NbBundle.getBundle((String)"org.netbeans.core.startup.Bundle").getString("currentVersion"), System.getProperty("netbeans.buildnumber"));
    }

    public static String getOperatingSystemValue() {
        return NbBundle.getMessage(ProductInformationPanel.class, (String)"Format_OperatingSystem_Value", (Object)System.getProperty("os.name", "unknown"), (Object)System.getProperty("os.version", "unknown"), (Object)System.getProperty("os.arch", "unknown"));
    }

    public static String getJavaValue() {
        return System.getProperty("java.version", "unknown");
    }

    public static String getVMValue() {
        return System.getProperty("java.vm.name", "unknown") + " " + System.getProperty("java.vm.version", "");
    }

    public static String getJavaRuntime() {
        return System.getProperty("java.runtime.name", "unknown") + " " + System.getProperty("java.runtime.version", "");
    }

    public static String getSystemLocaleValue() {
        String branding;
        return Locale.getDefault().toString() + ((branding = NbBundle.getBranding()) == null ? "" : new StringBuilder().append(" (").append(branding).append(")").toString());
    }

    private String getUserDirValue() {
        return System.getProperty("netbeans.user");
    }

    public static String getEncodingValue() {
        return System.getProperty("file.encoding", "unknown");
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent event) {
        if (HyperlinkEvent.EventType.ENTERED == event.getEventType()) {
            this.url = event.getURL();
        } else if (HyperlinkEvent.EventType.EXITED == event.getEventType()) {
            this.url = null;
        }
    }

    private static String getCopyrightText() {
        FileObject[] foArray;
        String copyrighttext = NbBundle.getMessage(ProductInformationPanel.class, (String)"LBL_Copyright", (Object)FONT_SIZE);
        FileObject licenseFolder = FileUtil.getConfigFile((String)"About/Licenses");
        if (licenseFolder != null && (foArray = licenseFolder.getChildren()).length > 0) {
            boolean isSomeLicense = false;
            StringWriter sw = new StringWriter();
            for (int i = 0; i < foArray.length; ++i) {
                String curLicense = ProductInformationPanel.loadLicenseText(foArray[i]);
                if (curLicense == null) continue;
                sw.write("<br>" + MessageFormat.format(NbBundle.getBundle(ProductInformationPanel.class).getString("LBL_AddOnCopyright"), curLicense, FONT_SIZE));
                isSomeLicense = true;
            }
            if (isSomeLicense) {
                copyrighttext = copyrighttext + sw.toString();
            }
        }
        return copyrighttext;
    }

    private static String loadLicenseText(FileObject fo) {
        InputStream is = null;
        try {
            is = fo.getInputStream();
        }
        catch (FileNotFoundException ex) {
            return null;
        }
        BufferedReader in = new BufferedReader(new InputStreamReader(is));
        StringWriter result = new StringWriter();
        try {
            int curChar;
            while ((curChar = in.read()) != -1) {
                result.write(curChar);
            }
        }
        catch (IOException ex) {
            return null;
        }
        return result.toString();
    }

    private static String getUpdates() {
        assert (!EventQueue.isDispatchThread());
        CheckForUpdatesProvider checkForUpdatesProvider = (CheckForUpdatesProvider)Lookup.getDefault().lookup(CheckForUpdatesProvider.class);
        if (checkForUpdatesProvider == null) {
            return "";
        }
        String desc = checkForUpdatesProvider.getContentDescription();
        String string = desc = desc != null ? desc : "";
        if (checkForUpdatesProvider.notifyAvailableUpdates(false)) {
            return Bundle.updates_found(desc.isEmpty() ? desc : Bundle.to_version(desc));
        }
        return desc.isEmpty() ? desc : Bundle.updates_not_found(desc);
    }

    private static void checkForUpdates() {
        assert (EventQueue.isDispatchThread());
        CheckForUpdatesProvider checkForUpdatesProvider = (CheckForUpdatesProvider)Lookup.getDefault().lookup(CheckForUpdatesProvider.class);
        if (checkForUpdatesProvider != null) {
            checkForUpdatesProvider.openCheckForUpdatesWizard(true);
        }
    }

    private static int getFontSize() {
        Integer customFontSize = (Integer)UIManager.get("customFontSize");
        if (customFontSize != null) {
            return customFontSize;
        }
        return 12;
    }

}

