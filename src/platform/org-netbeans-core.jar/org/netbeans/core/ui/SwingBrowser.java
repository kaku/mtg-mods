/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$Factory
 *  org.openide.awt.HtmlBrowser$Impl
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.ui;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.HtmlBrowser;
import org.openide.util.NbBundle;

public class SwingBrowser
implements HtmlBrowser.Factory,
Serializable {
    public static final String PROP_DESCRIPTION = "description";
    protected transient PropertyChangeSupport pcs;
    private static final long serialVersionUID = -3735603646171376891L;

    public SwingBrowser() {
        this.init();
    }

    private void init() {
        this.pcs = new PropertyChangeSupport(this);
    }

    public String getDescritpion() {
        return NbBundle.getMessage(SwingBrowser.class, (String)"LBL_SwingBrowserDescription");
    }

    public HtmlBrowser.Impl createHtmlBrowserImpl() {
        try {
            Class clz = Class.forName("org.openide.awt.SwingBrowserImpl");
            Constructor con = clz.getDeclaredConstructor(new Class[0]);
            con.setAccessible(true);
            return (HtmlBrowser.Impl)con.newInstance(new Object[0]);
        }
        catch (Exception ex) {
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)NbBundle.getMessage(SwingBrowser.class, (String)"MSG_cannot_create_browser")));
            return null;
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        ois.defaultReadObject();
        this.init();
    }
}

