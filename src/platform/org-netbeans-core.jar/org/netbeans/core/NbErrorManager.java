/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.startup.TopLogging
 *  org.openide.util.UserQuestionException
 */
package org.netbeans.core;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.netbeans.core.NotifyExcPanel;
import org.netbeans.core.startup.TopLogging;
import org.openide.util.UserQuestionException;

public final class NbErrorManager
extends Handler {
    public NbErrorManager() {
        new Exc(null, null, null, null);
    }

    static Exc createExc(Throwable t, Level severity, LogRecord add) {
        LogRecord[] ann = NbErrorManager.findAnnotations(t, add);
        return new Exc(t, severity, ann, NbErrorManager.findAnnotations0(t, add, true, new HashSet<Throwable>()));
    }

    @Override
    public void publish(LogRecord record) {
        if (record.getThrown() != null) {
            Level level = record.getLevel();
            if (level.intValue() == Level.WARNING.intValue() + 1) {
                level = null;
            }
            if (level != null && level.intValue() == Level.SEVERE.intValue() + 1) {
                level = null;
            }
            Exc ex = NbErrorManager.createExc(record.getThrown(), level, record.getLevel().intValue() == 1973 ? record : null);
            NotifyExcPanel.notify(ex);
        }
    }

    @Override
    public void flush() {
    }

    @Override
    public void close() throws SecurityException {
    }

    private static final String getLocalizedMessage(LogRecord rec) {
        ResourceBundle rb = rec.getResourceBundle();
        if (rb == null) {
            return null;
        }
        String msg = rec.getMessage();
        if (msg == null) {
            return null;
        }
        String format = rb.getString(msg);
        Object[] arr = rec.getParameters();
        if (arr == null) {
            return format;
        }
        return MessageFormat.format(format, arr);
    }

    private static LogRecord[] findAnnotations(Throwable t, LogRecord add) {
        return NbErrorManager.findAnnotations0(t, add, false, new HashSet<Throwable>());
    }

    private static LogRecord[] findAnnotations0(Throwable t, LogRecord add, boolean recursively, Set<Throwable> alreadyVisited) {
        LogRecord[] arr;
        LogRecord[] extras;
        Throwable cause;
        ArrayList<LogRecord> l = new ArrayList<LogRecord>();
        for (Throwable collect = t; collect != null; collect = collect.getCause()) {
            if (!(collect instanceof Callable)) continue;
            Object res = null;
            try {
                res = ((Callable)((Object)collect)).call();
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            if (!(res instanceof LogRecord[])) continue;
            arr = res;
            l.addAll(Arrays.asList(arr));
        }
        if (add != null) {
            l.add(add);
        }
        if (recursively) {
            ArrayList<LogRecord> al = new ArrayList<LogRecord>();
            for (LogRecord ano : l) {
                Throwable t1 = ano.getThrown();
                if (t1 == null || alreadyVisited.contains(t1)) continue;
                alreadyVisited.add(t1);
                LogRecord[] tmpAnnoArray = NbErrorManager.findAnnotations0(t1, null, true, alreadyVisited);
                if (tmpAnnoArray == null || tmpAnnoArray.length <= 0) continue;
                al.addAll(Arrays.asList(tmpAnnoArray));
            }
            l.addAll(al);
        }
        if ((cause = t.getCause()) != null && (extras = NbErrorManager.findAnnotations0(cause, null, true, alreadyVisited)) != null && extras.length > 0) {
            l.addAll(Arrays.asList(extras));
        }
        arr = new LogRecord[l.size()];
        l.toArray(arr);
        return arr;
    }

    static final class Exc {
        private Throwable t;
        private Date d;
        private LogRecord[] arr;
        private LogRecord[] arrAll;
        private Level severity;

        Exc(Throwable t, Level severity, LogRecord[] arr, LogRecord[] arrAll) {
            this.t = t;
            this.severity = severity;
            this.arr = arr == null ? new LogRecord[]{} : arr;
            this.arrAll = arrAll == null ? new LogRecord[]{} : arrAll;
        }

        String getMessage() {
            String m = this.t.getMessage();
            if (m != null) {
                return m;
            }
            return (String)this.find(1);
        }

        String getFirstStacktraceLine() {
            StackTraceElement[] elems = this.t.getStackTrace();
            if (elems == null || elems.length == 0) {
                return null;
            }
            StackTraceElement elem = elems[0];
            return elem.getClassName() + "." + elem.getMethodName();
        }

        String getLocalizedMessage() {
            String m = this.t.getLocalizedMessage();
            if (m != null && !m.equals(this.t.getMessage())) {
                return m;
            }
            if (this.arrAll == null) {
                return (String)this.find(2);
            }
            for (int i = 0; i < this.arrAll.length; ++i) {
                String s = NbErrorManager.getLocalizedMessage(this.arrAll[i]);
                if (s == null) continue;
                return s;
            }
            return m;
        }

        boolean isLocalized() {
            String m = this.t.getLocalizedMessage();
            if (m != null && !m.equals(this.t.getMessage())) {
                return true;
            }
            if (this.arrAll == null) {
                return (String)this.find(2) != null;
            }
            for (int i = 0; i < this.arrAll.length; ++i) {
                String s = NbErrorManager.getLocalizedMessage(this.arrAll[i]);
                if (s == null) continue;
                return true;
            }
            return false;
        }

        final boolean isUserQuestion() {
            return this.t instanceof UserQuestionException;
        }

        final void confirm() throws IOException {
            ((UserQuestionException)this.t).confirmed();
        }

        String getClassName() {
            return (String)this.find(3);
        }

        Level getSeverity() {
            if (this.severity != null) {
                return this.severity;
            }
            LogRecord[] anns = this.arrAll != null ? this.arrAll : this.arr;
            for (int i = 0; i < anns.length; ++i) {
                Level s = anns[i].getLevel();
                if (this.severity != null && s.intValue() <= this.severity.intValue()) continue;
                this.severity = s;
            }
            if (this.severity == null || this.severity == Level.ALL) {
                this.severity = this.t instanceof Error ? Level.SEVERE : Level.WARNING;
            }
            return this.severity;
        }

        Date getDate() {
            if (this.d == null) {
                this.d = (Date)this.find(4);
            }
            return this.d;
        }

        void printStackTrace(PrintStream ps) {
            this.printStackTrace(new PrintWriter(new OutputStreamWriter(ps)));
        }

        void printStackTrace(PrintWriter pw) {
            this.printStackTrace(pw, new HashSet<Throwable>(10));
        }

        private void printStackTrace(PrintWriter pw, Set<Throwable> nestingCheck) {
            if (this.t != null && !nestingCheck.add(this.t)) {
                Logger l = Logger.getAnonymousLogger();
                l.warning("WARNING - ErrorManager detected cyclic exception nesting:");
                for (Throwable thrw : nestingCheck) {
                    l.warning("\t" + thrw);
                    LogRecord[] anns = NbErrorManager.findAnnotations(thrw, null);
                    if (anns == null) continue;
                    for (int i = 0; i < anns.length; ++i) {
                        Throwable t2 = anns[i].getThrown();
                        if (t2 == null) continue;
                        l.warning("\t=> " + t2);
                    }
                }
                l.warning("Be sure not to annotate an exception with itself, directly or indirectly.");
                return;
            }
            for (LogRecord rec : this.arr) {
                if (rec == null) continue;
                Throwable thr = rec.getThrown();
                String annotation = NbErrorManager.getLocalizedMessage(rec);
                if (annotation == null) {
                    annotation = rec.getMessage();
                }
                if (annotation == null || thr != null) continue;
                pw.println("Annotation: " + annotation);
            }
            if (this.t instanceof VirtualMachineError) {
                this.t.printStackTrace(pw);
            } else {
                TopLogging.printStackTrace((Throwable)this.t, (PrintWriter)pw);
            }
            for (int i = 0; i < this.arr.length; ++i) {
                Throwable thr;
                if (this.arr[i] == null || (thr = this.arr[i].getThrown()) == null) continue;
                LogRecord[] ans = NbErrorManager.findAnnotations(thr, null);
                Exc ex = new Exc(thr, null, ans, null);
                pw.println("==>");
                ex.printStackTrace(pw, nestingCheck);
            }
        }

        private Object find(int kind) {
            return this.find(kind, true);
        }

        private Object find(int kind, boolean def) {
            for (int i = 0; i < this.arr.length; ++i) {
                LogRecord a = this.arr[i];
                Object o = null;
                switch (kind) {
                    case 1: {
                        o = a.getMessage();
                        break;
                    }
                    case 2: {
                        o = NbErrorManager.getLocalizedMessage(a);
                        break;
                    }
                    case 3: {
                        Throwable t = a.getThrown();
                        o = t == null ? null : t.getClass().getName();
                        break;
                    }
                    case 4: {
                        o = new Date(a.getMillis());
                    }
                }
                if (o == null) continue;
                return o;
            }
            if (!def) {
                return null;
            }
            switch (kind) {
                case 1: {
                    return this.t.getMessage();
                }
                case 2: {
                    return this.t.getLocalizedMessage();
                }
                case 3: {
                    return this.t.getClass().getName();
                }
                case 4: {
                    return new Date();
                }
            }
            throw new IllegalArgumentException("Unknown " + Integer.valueOf(kind));
        }
    }

}

