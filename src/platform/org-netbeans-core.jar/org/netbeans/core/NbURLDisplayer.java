/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$Factory
 *  org.openide.awt.HtmlBrowser$Impl
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.core;

import java.awt.Component;
import java.awt.Desktop;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import javax.swing.SwingUtilities;
import org.netbeans.core.HtmlBrowserComponent;
import org.netbeans.core.IDESettings;
import org.netbeans.core.ui.SwingBrowser;
import org.openide.awt.HtmlBrowser;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.RequestProcessor;

public final class NbURLDisplayer
extends HtmlBrowser.URLDisplayer {
    private static final RequestProcessor RP = new RequestProcessor("URLDisplayer");
    private NbBrowser htmlViewer;

    public void showURL(final URL u) {
        RP.post(new Runnable(){

            @Override
            public void run() {
                NbURLDisplayer.this.warmBrowserUp(false);
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        if (NbURLDisplayer.this.htmlViewer == null) {
                            NbURLDisplayer.this.htmlViewer = new NbBrowser();
                        }
                        NbURLDisplayer.this.htmlViewer.showUrl(u);
                    }
                });
            }

        });
    }

    public void showURLExternal(final URL u) {
        RP.post(new Runnable(){

            @Override
            public void run() {
                NbURLDisplayer.this.warmBrowserUp(true);
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        if (NbURLDisplayer.this.htmlViewer == null) {
                            NbURLDisplayer.this.htmlViewer = new NbBrowser();
                        }
                        NbURLDisplayer.this.htmlViewer.showUrlExternal(u);
                    }
                });
            }

        });
    }

    private void warmBrowserUp(boolean externalBrowser) {
        if (externalBrowser && (null == this.htmlViewer || null == this.htmlViewer.externalBrowser) || !externalBrowser && (null == this.htmlViewer || null == this.htmlViewer.brComp)) {
            HtmlBrowser.Factory browserFactory;
            HtmlBrowser.Impl browserImpl;
            HtmlBrowser.Factory factory = browserFactory = externalBrowser ? IDESettings.getExternalWWWBrowser() : IDESettings.getWWWBrowser();
            if (null != browserFactory && null != (browserImpl = browserFactory.createHtmlBrowserImpl())) {
                browserImpl.getLookup();
            }
        }
    }

    private static final class DesktopBrowser
    implements HtmlBrowser.Factory {
        private final Desktop desktop;

        public DesktopBrowser(Desktop desktop) {
            this.desktop = desktop;
        }

        public HtmlBrowser.Impl createHtmlBrowserImpl() {
            return new HtmlBrowser.Impl(){
                private final PropertyChangeSupport pcs;
                private URL url;

                public void setURL(URL url) {
                    this.url = url;
                    URL url2 = DesktopBrowser.externalize(url);
                    try {
                        DesktopBrowser.this.desktop.browse(url2.toURI());
                    }
                    catch (Exception x) {
                        Logger.getLogger(NbURLDisplayer.class.getName()).log(Level.INFO, "showing: " + url2, x);
                    }
                }

                public URL getURL() {
                    return this.url;
                }

                public void reloadDocument() {
                    this.setURL(this.url);
                }

                public void addPropertyChangeListener(PropertyChangeListener l) {
                    this.pcs.addPropertyChangeListener(l);
                }

                public void removePropertyChangeListener(PropertyChangeListener l) {
                    this.pcs.removePropertyChangeListener(l);
                }

                public Component getComponent() {
                    return null;
                }

                public void stopLoading() {
                }

                public String getStatusMessage() {
                    return "";
                }

                public String getTitle() {
                    return "";
                }

                public boolean isForward() {
                    return false;
                }

                public void forward() {
                }

                public boolean isBackward() {
                    return false;
                }

                public void backward() {
                }

                public boolean isHistory() {
                    return false;
                }

                public void showHistory() {
                }
            };
        }

        private static URL externalize(URL u) {
            String proto = u.getProtocol();
            if (proto == null || proto.equals("file") || proto.equals("http") || proto.equals("https")) {
                return u;
            }
            FileObject f = URLMapper.findFileObject((URL)u);
            if (f == null) {
                return u;
            }
            URL u2 = URLMapper.findURL((FileObject)f, (int)2);
            if (u2 == null) {
                return u;
            }
            try {
                String anchor;
                String query = u.getQuery();
                if (query != null) {
                    u2 = new URL(u2, "?" + query);
                }
                if ((anchor = u.getRef()) != null) {
                    u2 = new URL(u2, "#" + anchor);
                }
            }
            catch (MalformedURLException x) {
                return u;
            }
            return u2;
        }

    }

    private static class NbBrowser {
        private HtmlBrowserComponent brComp;
        private HtmlBrowserComponent externalBrowser;
        private PreferenceChangeListener idePCL;
        private static Lookup.Result factoryResult = Lookup.getDefault().lookupResult(HtmlBrowser.Factory.class);

        public NbBrowser() {
            this.setListener();
        }

        private void showUrl(URL url) {
            if (null == this.brComp) {
                this.brComp = this.createDefaultBrowser();
            }
            this.brComp.setURLAndOpen(url);
        }

        private void showUrlExternal(URL url) {
            if (null == this.externalBrowser) {
                this.externalBrowser = this.createExternalBrowser();
            }
            this.externalBrowser.setURLAndOpen(url);
        }

        private HtmlBrowserComponent createDefaultBrowser() {
            HtmlBrowser.Factory browser = IDESettings.getWWWBrowser();
            if (browser == null) {
                browser = new SwingBrowser();
            }
            HtmlBrowserComponent res = new HtmlBrowserComponent(browser, true, true);
            res.putClientProperty((Object)"TabPolicy", (Object)"HideWhenAlone");
            return res;
        }

        private HtmlBrowserComponent createExternalBrowser() {
            HtmlBrowser.Factory browser = IDESettings.getExternalWWWBrowser();
            if (browser == null) {
                Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
                browser = desktop != null && desktop.isSupported(Desktop.Action.BROWSE) ? new DesktopBrowser(desktop) : new SwingBrowser();
            }
            return new HtmlBrowserComponent(browser, true, true);
        }

        private void setListener() {
            if (this.idePCL != null) {
                return;
            }
            try {
                this.idePCL = new PreferenceChangeListener(){

                    @Override
                    public void preferenceChange(PreferenceChangeEvent evt) {
                        if ("WWWBrowser".equals(evt.getKey())) {
                            ((NbURLDisplayer)HtmlBrowser.URLDisplayer.getDefault()).htmlViewer = null;
                            if (NbBrowser.this.idePCL != null) {
                                IDESettings.getPreferences().removePreferenceChangeListener(NbBrowser.this.idePCL);
                                NbBrowser.this.idePCL = null;
                                NbBrowser.this.brComp = null;
                                NbBrowser.this.externalBrowser = null;
                            }
                        }
                    }
                };
                IDESettings.getPreferences().addPreferenceChangeListener(this.idePCL);
            }
            catch (Exception ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }

        static {
            factoryResult.allItems();
            factoryResult.addLookupListener(new LookupListener(){

                public void resultChanged(LookupEvent ev) {
                    ((NbURLDisplayer)HtmlBrowser.URLDisplayer.getDefault()).htmlViewer = null;
                }
            });
        }

    }

}

