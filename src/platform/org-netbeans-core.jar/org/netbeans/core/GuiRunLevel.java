/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.TopSecurityManager
 *  org.netbeans.core.startup.CLIOptions
 *  org.netbeans.core.startup.MainLookup
 *  org.netbeans.core.startup.RunLevel
 *  org.netbeans.core.startup.Splash
 *  org.netbeans.core.startup.StartLog
 *  org.openide.LifecycleManager
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.datatransfer.ExClipboard
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core;

import java.awt.datatransfer.Clipboard;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import org.netbeans.TopSecurityManager;
import org.netbeans.core.NbAuthenticator;
import org.netbeans.core.NbLoaderPool;
import org.netbeans.core.TimableEventQueue;
import org.netbeans.core.WindowSystem;
import org.netbeans.core.startup.CLIOptions;
import org.netbeans.core.startup.MainLookup;
import org.netbeans.core.startup.RunLevel;
import org.netbeans.core.startup.Splash;
import org.netbeans.core.startup.StartLog;
import org.openide.LifecycleManager;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.datatransfer.ExClipboard;
import org.openide.windows.WindowManager;

public class GuiRunLevel
implements RunLevel {
    public GuiRunLevel() {
        MainLookup.started();
    }

    public void run() {
        try {
            NbLoaderPool.load();
        }
        catch (IOException ioe) {
            Logger.getLogger(GuiRunLevel.class.getName()).log(Level.INFO, null, ioe);
        }
        StartLog.logProgress((String)"LoaderPool loaded");
        Splash.getInstance().increment(10);
        NbLoaderPool.installationFinished();
        StartLog.logProgress((String)"LoaderPool notified");
        Splash.getInstance().increment(10);
        if (CLIOptions.isGui()) {
            this.initializeMainWindow();
            StartLog.logProgress((String)"Main window initialized");
            Splash.getInstance().increment(1);
        }
        if (!Boolean.getBoolean("TopSecurityManager.disable")) {
            TopSecurityManager.install();
            if (CLIOptions.isGui()) {
                TopSecurityManager.makeSwingUseSpecialClipboard((Clipboard)((Clipboard)Lookup.getDefault().lookup(ExClipboard.class)));
            }
        }
        NbAuthenticator.install();
        StartLog.logProgress((String)"Security managers installed");
        Splash.getInstance().increment(1);
    }

    private void initializeMainWindow() {
        StartLog.logStart((String)"Main window initialization");
        TimableEventQueue.initialize();
        StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(GuiRunLevel.class, (String)"MSG_MainWindowInit"));
        Timer timerInit = new Timer(0, new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent ev) {
            }
        });
        timerInit.setRepeats(false);
        timerInit.start();
        Splash.getInstance().increment(10);
        StartLog.logProgress((String)"Timer initialized");
        StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(GuiRunLevel.class, (String)"MSG_WindowShowInit"));
        WindowSystem windowSystem = (WindowSystem)Lookup.getDefault().lookup(WindowSystem.class);
        if (windowSystem != null) {
            windowSystem.init();
        }
        SwingUtilities.invokeLater(new InitWinSys(windowSystem));
        StartLog.logEnd((String)"Main window initialization");
    }

    private static void waitForMainWindowPaint() {
        Runnable r = new Runnable(){

            @Override
            public void run() {
                try {
                    Class clz = Class.forName("org.netbeans.modules.performance.guitracker.LoggingRepaintManager");
                    Method m = clz.getMethod("measureStartup", new Class[0]);
                    Object o = m.invoke(null, new Object[0]);
                    GuiRunLevel.endOfStartupMeasuring(o);
                }
                catch (ClassNotFoundException e) {
                    StartLog.logProgress((String)e.toString());
                }
                catch (NoSuchMethodException e) {
                    StartLog.logProgress((String)e.toString());
                }
                catch (IllegalAccessException e) {
                    StartLog.logProgress((String)e.toString());
                }
                catch (InvocationTargetException e) {
                    StartLog.logProgress((String)e.toString());
                }
            }
        };
        new Thread(r).start();
    }

    private static void endOfStartupMeasuring(Object o) {
        StartLog.logProgress((String)"Startup memory and time measured");
        GuiRunLevel.maybeDie(o);
    }

    private static void maybeDie(Object o) {
        if (System.getProperty("netbeans.kill") != null) {
            TopSecurityManager.exit((int)5);
        }
        if (System.getProperty("netbeans.close") != null) {
            if (Boolean.getBoolean("netbeans.warm.close")) {
                try {
                    MainLookup.warmUp((long)0).waitFinished();
                }
                catch (Exception ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            if (o != null) {
                StartLog.logMeasuredStartupTime((long)((Long)o));
            }
            LifecycleManager.getDefault().exit();
        }
    }

    private class InitWinSys
    implements Runnable {
        private final WindowSystem windowSystem;
        private int phase;

        public InitWinSys(WindowSystem windowSystem) {
            this.windowSystem = windowSystem;
        }

        @Override
        public void run() {
            StartLog.logProgress((String)"Window system initialization");
            if (this.phase == 0) {
                if (this.windowSystem != null) {
                    this.windowSystem.load();
                    StartLog.logProgress((String)"Window system loaded");
                } else {
                    Logger.getLogger(GuiRunLevel.class.getName()).log(Level.WARNING, "Module org.netbeans.core.windows missing, cannot start window system");
                }
                this.phase = 1;
                SwingUtilities.invokeLater(this);
                return;
            }
            if (this.phase == 1) {
                if (this.windowSystem != null) {
                    if (StartLog.willLog()) {
                        GuiRunLevel.waitForMainWindowPaint();
                    }
                    this.windowSystem.show();
                }
                StartLog.logProgress((String)"Window system shown");
                if (!StartLog.willLog()) {
                    GuiRunLevel.maybeDie(null);
                }
                if (System.getProperty("netbeans.warmup.skip") == null && System.getProperty("netbeans.close") == null) {
                    WindowManager.getDefault().invokeWhenUIReady(new Runnable(){

                        @Override
                        public void run() {
                            MainLookup.warmUp((long)1500);
                        }
                    });
                }
                return;
            }
            assert (false);
        }

    }

}

