/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core;

public interface WindowSystem {
    public void init();

    public void show();

    public void hide();

    public void load();

    public void save();
}

