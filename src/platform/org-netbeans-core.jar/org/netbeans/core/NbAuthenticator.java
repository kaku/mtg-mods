/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.NetworkSettings
 */
package org.netbeans.core;

import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import org.netbeans.core.GuiRunLevel;
import org.netbeans.core.NbAuthenticatorPanel;
import org.netbeans.core.ProxySettings;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.NetworkSettings;

final class NbAuthenticator
extends Authenticator {
    private static final long TIMEOUT = 3000;
    private static long lastTry = 0;

    private NbAuthenticator() {
        Preferences proxySettingsNode = NbPreferences.root().node("/org/netbeans/core");
        assert (proxySettingsNode != null);
    }

    static void install() {
        if (Boolean.valueOf(NbBundle.getMessage(GuiRunLevel.class, (String)"USE_Authentication")).booleanValue()) {
            NbAuthenticator.setDefault(new NbAuthenticator());
        }
    }

    static void install4test() {
        NbAuthenticator.setDefault(new NbAuthenticator());
    }

    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        Logger.getLogger(NbAuthenticator.class.getName()).log(Level.FINER, "Authenticator.getPasswordAuthentication() with prompt " + this.getRequestingPrompt());
        if (Authenticator.RequestorType.PROXY == this.getRequestorType() && ProxySettings.useAuthentication()) {
            Logger.getLogger(NbAuthenticator.class.getName()).log(Level.FINER, "Username set to " + ProxySettings.getAuthenticationUsername() + " while request " + this.getRequestingURL());
            return new PasswordAuthentication(ProxySettings.getAuthenticationUsername(), ProxySettings.getAuthenticationPassword());
        }
        if (System.currentTimeMillis() - lastTry > 3000) {
            if (this.getRequestingProtocol().startsWith("SOCKS") && ProxySettings.getAuthenticationUsername().length() > 0) {
                return new PasswordAuthentication(ProxySettings.getAuthenticationUsername(), ProxySettings.getAuthenticationPassword());
            }
            if (NetworkSettings.isAuthenticationDialogSuppressed()) {
                return null;
            }
            PasswordAuthentication auth = this.getAuthenticationFromURL();
            if (auth != null) {
                return auth;
            }
            NbAuthenticatorPanel ui = new NbAuthenticatorPanel(this.getRequestingPrompt());
            Object result = DialogDisplayer.getDefault().notify((NotifyDescriptor)new DialogDescriptor((Object)ui, NbBundle.getMessage(NbAuthenticator.class, (String)"CTL_Authentication")));
            if (DialogDescriptor.OK_OPTION == result) {
                lastTry = 0;
                return new PasswordAuthentication(ui.getUserName(), ui.getPassword());
            }
            lastTry = System.currentTimeMillis();
        }
        Logger.getLogger(NbAuthenticator.class.getName()).log(Level.WARNING, "No authentication set while requesting " + this.getRequestingURL());
        return null;
    }

    private PasswordAuthentication getAuthenticationFromURL() {
        String auth;
        URL u = this.getRequestingURL();
        if (u != null && (auth = u.getUserInfo()) != null) {
            int i = auth.indexOf(58);
            String user = i == -1 ? auth : auth.substring(0, i);
            String pwd = i == -1 ? "" : auth.substring(i + 1);
            return new PasswordAuthentication(user, pwd.toCharArray());
        }
        return null;
    }
}

