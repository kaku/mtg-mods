/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.startup.CoreBridge
 *  org.netbeans.core.startup.MainLookup
 *  org.netbeans.core.startup.ManifestSection
 *  org.netbeans.core.startup.ManifestSection$ActionSection
 *  org.netbeans.core.startup.ManifestSection$LoaderSection
 *  org.netbeans.swing.plaf.Startup
 *  org.openide.awt.StatusDisplayer
 *  org.openide.loaders.DataLoader
 *  org.openide.nodes.NodeOp
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 */
package org.netbeans.core;

import java.awt.EventQueue;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ProxySelector;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.SwingUtilities;
import org.netbeans.core.CLIOptions2;
import org.netbeans.core.ModuleActions;
import org.netbeans.core.NbLoaderPool;
import org.netbeans.core.startup.CoreBridge;
import org.netbeans.core.startup.MainLookup;
import org.netbeans.core.startup.ManifestSection;
import org.netbeans.swing.plaf.Startup;
import org.openide.awt.StatusDisplayer;
import org.openide.loaders.DataLoader;
import org.openide.nodes.NodeOp;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;
import org.openide.util.lookup.InstanceContent;

public final class CoreBridgeImpl
extends CoreBridge {
    private static boolean editorsRegistered = false;

    protected void attachToCategory(Object category) {
        ModuleActions.attachTo(category);
    }

    protected void loadDefaultSection(ManifestSection s, InstanceContent.Convertor<ManifestSection, Object> convertor, boolean load) {
        if (load) {
            if (convertor != null) {
                MainLookup.register((Object)s, convertor);
            } else {
                MainLookup.register((Object)s);
            }
        } else if (convertor != null) {
            MainLookup.unregister((Object)s, convertor);
        } else {
            MainLookup.unregister((Object)s);
        }
    }

    protected void loadActionSection(ManifestSection.ActionSection s, boolean load) throws Exception {
        if (load) {
            ModuleActions.add(s);
        } else {
            ModuleActions.remove(s);
        }
    }

    protected void loadLoaderSection(ManifestSection.LoaderSection s, boolean load) throws Exception {
        if (load) {
            NbLoaderPool.add(s);
        } else {
            NbLoaderPool.remove((DataLoader)s.getInstance(), NbLoaderPool.getNbLoaderPool());
        }
    }

    protected void loaderPoolTransaction(boolean begin) {
        if (begin) {
            NbLoaderPool.beginUpdates();
        } else {
            NbLoaderPool.endUpdates();
        }
    }

    public void setStatusText(String status) {
        StatusDisplayer.getDefault().setStatusText(status);
    }

    public void initializePlaf(final Class uiClass, final int uiFontSize, final URL themeURL) {
        RequestProcessor.getDefault().post(new Runnable(){

            @Override
            public void run() {
                Class uiClassToUse;
                Class class_ = uiClassToUse = null == uiClass ? CoreBridgeImpl.getPreferredUIClass() : uiClass;
                if (null != uiClass) {
                    System.setProperty("nb.laf.forced", uiClass.getName());
                }
                EventQueue.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        Startup.run((Class)uiClassToUse, (int)uiFontSize, (URL)themeURL, (ResourceBundle)NbBundle.getBundle(Startup.class));
                    }
                });
            }

        });
    }

    public Lookup lookupCacheLoad() {
        return NbLoaderPool.getNbLoaderPool().findServicesLookup();
    }

    public int cli(String[] args, InputStream inputStream, OutputStream outputStream, OutputStream errorStream, File file) {
        return CLIOptions2.INSTANCE.cli(args);
    }

    public void registerPropertyEditors() {
        CoreBridgeImpl.doRegisterPropertyEditors();
    }

    private static final void doRegisterPropertyEditors() {
        NodeOp.registerPropertyEditors();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                NodeOp.registerPropertyEditors();
            }
        });
        ProxySelector selector = (ProxySelector)Lookup.getDefault().lookup(ProxySelector.class);
        if (selector != null) {
            ProxySelector.setDefault(selector);
        }
        editorsRegistered = true;
    }

    private static Class getPreferredUIClass() {
        Preferences prefs = NbPreferences.root().node("laf");
        String uiClassName = prefs.get("laf", null);
        if (null == uiClassName) {
            return null;
        }
        ClassLoader loader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        if (null == loader) {
            loader = ClassLoader.getSystemClassLoader();
        }
        try {
            Class uiClass = loader.loadClass(uiClassName);
            return uiClass;
        }
        catch (ClassNotFoundException ex) {
            if (prefs.getBoolean("dark.themes.installed", false)) {
                prefs.remove("laf");
                prefs.remove("dark.themes.installed");
            } else {
                Logger.getLogger(CoreBridgeImpl.class.getName()).log(Level.INFO, "Cannot use look and feel class: " + uiClassName, ex);
            }
            return null;
        }
    }

}

