/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ServiceType
 *  org.openide.ServiceType$Registry
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.InstanceCookie$Of
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.loaders.InstanceDataObject
 *  org.openide.modules.ModuleInfo
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Enumerations
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Utilities
 *  org.openide.util.io.NbMarshalledObject
 */
package org.netbeans.core;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.ServiceType;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.loaders.InstanceDataObject;
import org.openide.modules.ModuleInfo;
import org.openide.nodes.Node;
import org.openide.util.Enumerations;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Utilities;
import org.openide.util.io.NbMarshalledObject;

@Deprecated
public final class Services
extends ServiceType.Registry
implements LookupListener {
    static final long serialVersionUID = -7558069607307508327L;
    private Lookup.Result<ServiceType> allTypes;
    private Map<String, ServiceType> name2Service = new HashMap<String, ServiceType>();

    public static Services getDefault() {
        return (Services)((Object)Lookup.getDefault().lookup(ServiceType.Registry.class));
    }

    public ServiceType find(Class clazz) {
        return (ServiceType)Lookup.getDefault().lookup(clazz);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public ServiceType find(String name) {
        ServiceType ret;
        Map<String, ServiceType> lookupMap;
        Map<String, ServiceType> map = lookupMap = this.name2Service;
        synchronized (map) {
            ret = lookupMap.get(name);
        }
        if (ret == null) {
            ret = super.find(name);
            map = lookupMap;
            synchronized (map) {
                lookupMap.put(name, ret);
            }
        }
        return ret;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Lookup.Result<ServiceType> getTypesResult() {
        boolean init = false;
        Services services = this;
        synchronized (services) {
            if (this.allTypes == null) {
                this.allTypes = Lookup.getDefault().lookupResult(ServiceType.class);
                this.allTypes.addLookupListener((LookupListener)this);
                init = true;
            }
        }
        if (init) {
            this.resultChanged(null);
        }
        return this.allTypes;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void resultChanged(LookupEvent ev) {
        Map<String, ServiceType> map = this.name2Service;
        synchronized (map) {
            this.name2Service.clear();
        }
    }

    public List<ServiceType> getServiceTypes() {
        return new ArrayList<ServiceType>(this.getTypesResult().allInstances());
    }

    public synchronized void setServiceTypes(List arr0) {
        DataObject parent;
        if (arr0 == null) {
            throw new NullPointerException();
        }
        List<ServiceType> arr = this.ensureSingleness(arr0);
        HashMap<ServiceType, DataObject> services = new HashMap<ServiceType, DataObject>(20);
        this.searchServices(Services.findSessionFolder("Services").getPrimaryFile(), services);
        HashMap order = new HashMap(10);
        for (ServiceType st : arr) {
            void orderedFiles22;
            DataObject dobj = services.get((Object)st);
            if (dobj != null) {
                try {
                    dobj = InstanceDataObject.create((DataFolder)dobj.getFolder(), (String)dobj.getPrimaryFile().getName(), (Object)st, (ModuleInfo)null);
                }
                catch (IOException ex) {
                    Logger.getLogger(Services.class.getName()).log(Level.WARNING, null, ex);
                }
                services.remove((Object)st);
            } else {
                dobj = this.storeNewServiceType(st);
            }
            if (dobj == null) continue;
            parent = dobj.getFolder();
            List orderedFiles22 = (List)order.get((Object)parent);
            if (orderedFiles22 == null) {
                ArrayList orderedFiles22 = new ArrayList(6);
                order.put(parent, orderedFiles22);
            }
            orderedFiles22.add(dobj);
        }
        for (Map.Entry entry : order.entrySet()) {
            parent = (DataObject)entry.getKey();
            List orderedFiles = (List)entry.getValue();
            if (orderedFiles.size() < 2) continue;
            Iterator files = orderedFiles.iterator();
            StringBuilder orderAttr = new StringBuilder(64);
            while (files.hasNext()) {
                DataObject file = (DataObject)files.next();
                orderAttr.append(file.getPrimaryFile().getNameExt()).append('/');
            }
            orderAttr.deleteCharAt(orderAttr.length() - 1);
            try {
                parent.getPrimaryFile().setAttribute("OpenIDE-Folder-Order", (Object)orderAttr.toString());
            }
            catch (IOException ex) {
                Logger.getLogger(Services.class.getName()).log(Level.WARNING, null, ex);
            }
        }
        for (DataObject dobj : services.values()) {
            try {
                dobj.delete();
            }
            catch (IOException ex) {
                Logger.getLogger(Services.class.getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    private DataObject storeNewServiceType(ServiceType st) {
        Class stype = st.getClass();
        while (stype.getSuperclass() != ServiceType.class) {
            stype = stype.getSuperclass();
        }
        try {
            String folder = Utilities.getShortClassName(stype);
            DataFolder dfServices = Services.findSessionFolder("Services");
            DataFolder dfTarget = DataFolder.create((DataFolder)dfServices, (String)folder);
            return InstanceDataObject.create((DataFolder)dfTarget, (String)null, (Object)st, (ModuleInfo)null);
        }
        catch (Exception ex) {
            Logger.getLogger(Services.class.getName()).log(Level.WARNING, null, ex);
            return null;
        }
    }

    private List<ServiceType> ensureSingleness(List<ServiceType> l) {
        ArrayList<ServiceType> newList = new ArrayList<ServiceType>(l.size());
        for (ServiceType stype : l) {
            if (newList.contains((Object)stype)) continue;
            newList.add(stype);
        }
        return newList;
    }

    private void searchServices(FileObject folder, Map<ServiceType, DataObject> services) {
        FileObject[] fobjs = folder.getChildren();
        for (int i = 0; i < fobjs.length; ++i) {
            if (!fobjs[i].isValid()) continue;
            if (fobjs[i].isFolder()) {
                this.searchServices(fobjs[i], services);
                continue;
            }
            try {
                DataObject dobj = DataObject.find((FileObject)fobjs[i]);
                InstanceCookie inst = (InstanceCookie)dobj.getCookie(InstanceCookie.class);
                if (inst == null || !Services.instanceOf(inst, ServiceType.class)) continue;
                ServiceType ser = (ServiceType)inst.instanceCreate();
                services.put(ser, dobj);
                continue;
            }
            catch (DataObjectNotFoundException ex) {
                continue;
            }
            catch (Exception ex) {
                Logger.getLogger(Services.class.getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    private static boolean instanceOf(InstanceCookie inst, Class clazz) {
        if (inst instanceof InstanceCookie.Of) {
            return ((InstanceCookie.Of)inst).instanceOf(clazz);
        }
        try {
            return clazz.isAssignableFrom(inst.instanceClass());
        }
        catch (Exception ex) {
            Logger.getLogger(Services.class.getName()).log(Level.WARNING, null, ex);
            return false;
        }
    }

    public Enumeration<ServiceType> services() {
        return Collections.enumeration(this.getServiceTypes());
    }

    public <T extends ServiceType> Enumeration<T> services(Class<T> clazz) {
        if (clazz == null) {
            return Enumerations.empty();
        }
        Collection res = Lookup.getDefault().lookupAll(clazz);
        return Collections.enumeration(res);
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        Enumeration<ServiceType> en = this.services();
        while (en.hasMoreElements()) {
            NbMarshalledObject obj;
            ServiceType s = en.nextElement();
            try {
                obj = new NbMarshalledObject((Object)s);
            }
            catch (IOException ex) {
                Logger.getLogger(Services.class.getName()).log(Level.WARNING, null, ex);
                obj = null;
            }
            if (obj == null) continue;
            oos.writeObject((Object)obj);
        }
        oos.writeObject(null);
    }

    private void readObject(ObjectInputStream oos) throws IOException, ClassNotFoundException {
        NbMarshalledObject obj;
        LinkedList<ServiceType> ll = new LinkedList<ServiceType>();
        while ((obj = (NbMarshalledObject)oos.readObject()) != null) {
            try {
                ServiceType s = (ServiceType)obj.get();
                ll.add(s);
            }
            catch (IOException ex) {
                Logger.getLogger(Services.class.getName()).log(Level.WARNING, null, ex);
            }
            catch (ClassNotFoundException ex) {
                Logger.getLogger(Services.class.getName()).log(Level.WARNING, null, ex);
            }
        }
        Services.getDefault().setServiceTypes(ll);
    }

    private Object readResolve() {
        return Services.getDefault();
    }

    private static DataFolder findSessionFolder(String name) {
        try {
            FileObject fo = FileUtil.getConfigFile((String)name);
            if (fo == null) {
                fo = FileUtil.createFolder((FileObject)FileUtil.getConfigRoot(), (String)name);
            }
            return DataFolder.findFolder((FileObject)fo);
        }
        catch (IOException ex) {
            throw (IllegalStateException)new IllegalStateException("Folder not found and cannot be created: " + name).initCause(ex);
        }
    }
}

