/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.StatusDisplayer
 *  org.openide.awt.StatusDisplayer$Message
 *  org.openide.util.ChangeSupport
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.core;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeListener;
import org.openide.awt.StatusDisplayer;
import org.openide.util.ChangeSupport;
import org.openide.util.RequestProcessor;

public final class NbStatusDisplayer
extends StatusDisplayer {
    private final ChangeSupport cs;
    private List<WeakReference<MessageImpl>> messages;
    private static int SURVIVING_TIME = Integer.getInteger("org.openide.awt.StatusDisplayer.DISPLAY_TIME", 5000);
    private static final RequestProcessor RP = new RequestProcessor("NbStatusDisplayer");

    public NbStatusDisplayer() {
        this.cs = new ChangeSupport((Object)this);
        this.messages = new ArrayList<WeakReference<MessageImpl>>(30);
    }

    public void setStatusText(String text) {
        this.add(text, 0).clear(SURVIVING_TIME);
    }

    public StatusDisplayer.Message setStatusText(String text, int importance) {
        if (importance <= 0) {
            throw new IllegalArgumentException("Invalid importance value: " + importance);
        }
        return this.add(text, importance);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public synchronized String getStatusText() {
        String text = null;
        NbStatusDisplayer nbStatusDisplayer = this;
        synchronized (nbStatusDisplayer) {
            MessageImpl msg = this.getCurrent();
            text = null == msg ? "" : msg.text;
        }
        return text;
    }

    public void addChangeListener(ChangeListener l) {
        this.cs.addChangeListener(l);
    }

    public void removeChangeListener(ChangeListener l) {
        this.cs.removeChangeListener(l);
    }

    private MessageImpl getCurrent() {
        while (!this.messages.isEmpty()) {
            WeakReference<MessageImpl> ref = this.messages.get(0);
            MessageImpl impl = ref.get();
            if (null != impl) {
                return impl;
            }
            this.messages.remove(0);
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private MessageImpl add(String text, int importance) {
        MessageImpl newMsg = new MessageImpl(text, importance);
        WeakReference<MessageImpl> newRef = new WeakReference<MessageImpl>(newMsg);
        NbStatusDisplayer nbStatusDisplayer = this;
        synchronized (nbStatusDisplayer) {
            boolean added = false;
            for (int i = 0; i < this.messages.size() && !added; ++i) {
                WeakReference<MessageImpl> ref = this.messages.get(0);
                MessageImpl impl = ref.get();
                if (impl == null) continue;
                if (impl.importance == importance) {
                    this.messages.set(i, newRef);
                    added = true;
                    continue;
                }
                if (impl.importance >= importance) continue;
                this.messages.add(i, newRef);
                added = true;
            }
            if (!added) {
                this.messages.add(newRef);
            }
        }
        this.cs.fireChange();
        Logger.getLogger(NbStatusDisplayer.class.getName()).log(Level.FINE, "Status text updated: {0}, importance: {1}", new Object[]{text, importance});
        return newMsg;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void remove(MessageImpl toRemove) {
        boolean removed = false;
        NbStatusDisplayer nbStatusDisplayer = this;
        synchronized (nbStatusDisplayer) {
            WeakReference<MessageImpl> refToRemove = null;
            for (WeakReference<MessageImpl> ref : this.messages) {
                if (toRemove != ref.get()) continue;
                refToRemove = ref;
                break;
            }
            if (null != refToRemove) {
                removed = this.messages.remove(refToRemove);
            }
        }
        if (removed) {
            this.cs.fireChange();
        }
    }

    private class MessageImpl
    implements StatusDisplayer.Message,
    Runnable {
        private final String text;
        private final int importance;

        public MessageImpl(String text, int importance) {
            this.text = text;
            this.importance = importance;
        }

        public void clear(int timeInMillis) {
            RP.post((Runnable)this, timeInMillis);
        }

        protected void finalize() throws Throwable {
            this.run();
        }

        @Override
        public void run() {
            NbStatusDisplayer.this.remove(this);
        }
    }

}

