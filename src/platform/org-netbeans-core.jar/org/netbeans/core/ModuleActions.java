/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.startup.ManifestSection
 *  org.netbeans.core.startup.ManifestSection$ActionSection
 *  org.openide.actions.ActionManager
 *  org.openide.util.Mutex
 *  org.openide.util.actions.SystemAction
 */
package org.netbeans.core;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.KeyboardFocusManager;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.RootPaneContainer;
import org.netbeans.core.startup.ManifestSection;
import org.openide.actions.ActionManager;
import org.openide.util.Mutex;
import org.openide.util.actions.SystemAction;

public class ModuleActions
extends ActionManager {
    private static SystemAction[] array;
    private static Map<Object, List<ManifestSection.ActionSection>> map;
    private static Object module;
    private Map<ActionEvent, Action> runningActions = new HashMap<ActionEvent, Action>();
    private static final Logger err;
    private static final Map<Object, Component> glassPaneUses;

    public static ModuleActions getDefaultInstance() {
        ActionManager mgr = ActionManager.getDefault();
        assert (mgr instanceof ModuleActions);
        return (ModuleActions)mgr;
    }

    public SystemAction[] getContextActions() {
        SystemAction[] a = array;
        if (a != null) {
            return a;
        }
        array = a = ModuleActions.createActions();
        return a;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void invokeAction(Action a, final ActionEvent e) {
        try {}
        catch (Throwable var3_3) {
            this.removeRunningAction(e);
            Mutex.EVENT.readAccess(new Runnable(e){
                final /* synthetic */ ActionEvent val$e;

                @Override
                public void run() {
                    ModuleActions.hideWaitCursor(this.val$e);
                }
            });
            throw var3_3;
        }
        Mutex.EVENT.readAccess(new Runnable(){

            @Override
            public void run() {
                ModuleActions.showWaitCursor(e);
            }
        });
        this.addRunningAction(a, e);
        a.actionPerformed(e);
        this.removeRunningAction(e);
        Mutex.EVENT.readAccess(new );
    }

    private void fireChange() {
        this.firePropertyChange("contextActions", (Object)null, (Object)null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void addRunningAction(Action action, ActionEvent evt) {
        Map<ActionEvent, Action> map = this.runningActions;
        synchronized (map) {
            this.runningActions.put(evt, action);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void removeRunningAction(ActionEvent evt) {
        Map<ActionEvent, Action> map = this.runningActions;
        synchronized (map) {
            this.runningActions.remove(evt);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Collection<Action> getRunningActions() {
        Map<ActionEvent, Action> map = this.runningActions;
        synchronized (map) {
            return new ArrayList<Action>(this.runningActions.values());
        }
    }

    public static synchronized void attachTo(Object m) {
        module = m;
    }

    public static synchronized void add(ManifestSection.ActionSection a) {
        List<ManifestSection.ActionSection> list = map.get(module);
        if (list == null) {
            list = new ArrayList<ManifestSection.ActionSection>();
            map.put(module, list);
        }
        list.add(a);
        array = null;
        ModuleActions.getDefaultInstance().fireChange();
    }

    public static synchronized void remove(ManifestSection.ActionSection a) {
        List<ManifestSection.ActionSection> list = map.get(module);
        if (list == null) {
            return;
        }
        list.remove((Object)a);
        if (list.isEmpty()) {
            map.remove(module);
        }
        array = null;
        ModuleActions.getDefaultInstance().fireChange();
    }

    private static synchronized SystemAction[] createActions() {
        Iterator<List<ManifestSection.ActionSection>> it = map.values().iterator();
        ArrayList<Object> arr = new ArrayList<Object>(map.size() * 5);
        while (it.hasNext()) {
            List<ManifestSection.ActionSection> l = it.next();
            for (ManifestSection.ActionSection s : l) {
                try {
                    arr.add(s.getInstance());
                }
                catch (Exception ex) {
                    Logger.getLogger(ModuleActions.class.getName()).log(Level.WARNING, null, ex);
                }
            }
            if (!it.hasNext()) continue;
            arr.add(null);
        }
        return arr.toArray((T[])new SystemAction[arr.size()]);
    }

    private static Component activeGlassPane() {
        Window w = KeyboardFocusManager.getCurrentKeyboardFocusManager().getActiveWindow();
        if (w instanceof RootPaneContainer) {
            return ((RootPaneContainer)((Object)w)).getGlassPane();
        }
        return null;
    }

    public static void showWaitCursor(Object key) {
        assert (EventQueue.isDispatchThread());
        assert (!glassPaneUses.containsKey(key));
        Component c = ModuleActions.activeGlassPane();
        if (c == null) {
            err.warning("showWaitCursor could not find a suitable glass pane; key=" + key);
            return;
        }
        if (glassPaneUses.values().contains(c)) {
            err.fine("wait cursor already displayed on " + c);
        } else {
            err.fine("wait cursor will be displayed on " + c);
            c.setCursor(Cursor.getPredefinedCursor(3));
            c.setVisible(true);
        }
        glassPaneUses.put(key, c);
    }

    public static void hideWaitCursor(Object key) {
        assert (EventQueue.isDispatchThread());
        Component c = glassPaneUses.get(key);
        if (c == null) {
            return;
        }
        glassPaneUses.remove(key);
        if (glassPaneUses.values().contains(c)) {
            err.fine("wait cursor still displayed on " + c);
        } else {
            err.fine("wait cursor will be hidden on " + c);
            c.setVisible(false);
            c.setCursor(null);
        }
    }

    static {
        map = new HashMap<Object, List<ManifestSection.ActionSection>>(8);
        module = null;
        err = Logger.getLogger("org.openide.util.actions.MouseCursorUtils");
        glassPaneUses = new HashMap<Object, Component>();
    }

}

