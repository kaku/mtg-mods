/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$Factory
 *  org.openide.awt.HtmlBrowser$Impl
 *  org.openide.awt.StatusDisplayer
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.lookup.ProxyLookup
 *  org.openide.windows.CloneableTopComponent
 */
package org.netbeans.core;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Externalizable;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.ObjectStreamException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import org.netbeans.core.IDESettings;
import org.openide.awt.HtmlBrowser;
import org.openide.awt.StatusDisplayer;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.ProxyLookup;
import org.openide.windows.CloneableTopComponent;

public class HtmlBrowserComponent
extends CloneableTopComponent
implements PropertyChangeListener {
    static final long serialVersionUID = 2912844785502987960L;
    private HtmlBrowser browserComponent;
    private HtmlBrowser.Factory browserFactory;
    private final MyLookup proxyLookup = new MyLookup();
    private static final int MAX_TITLE_LENGTH = 25;
    private String urlToLoad;
    private boolean enableHome = true;
    private boolean enableLocation = true;
    private boolean statusVisible = false;
    private boolean toolbarVisible = true;

    public HtmlBrowserComponent() {
        this(true, false);
    }

    public HtmlBrowserComponent(boolean toolbar, boolean statusLine) {
        this(IDESettings.getWWWBrowser(), toolbar, statusLine);
    }

    private HtmlBrowserComponent(boolean toolbar, boolean statusLine, URL url) {
        this(IDESettings.getWWWBrowser(), toolbar, statusLine);
        this.urlToLoad = null == url ? null : url.toExternalForm();
    }

    public HtmlBrowserComponent(HtmlBrowser.Factory fact, boolean toolbar, boolean statusLine) {
        this.setName("");
        this.setLayout((LayoutManager)new BorderLayout());
        this.browserFactory = fact;
        this.setToolTipText(NbBundle.getBundle(HtmlBrowser.class).getString("HINT_WebBrowser"));
        this.setName(NbBundle.getMessage(HtmlBrowserComponent.class, (String)"Title_WebBrowser"));
        this.setDisplayName(this.getDefaultDisplayName());
        this.putClientProperty((Object)"KeepNonPersistentTCInModelWhenClosed", (Object)Boolean.TRUE);
        this.setActivatedNodes(new Node[0]);
    }

    private String getDefaultDisplayName() {
        return NbBundle.getMessage(HtmlBrowserComponent.class, (String)"Title_WebBrowser");
    }

    public Lookup getLookup() {
        return this.proxyLookup;
    }

    public int getPersistenceType() {
        return 1;
    }

    @Override
    public void propertyChange(PropertyChangeEvent e) {
        if ("statusMessage".equals(e.getPropertyName())) {
            StatusDisplayer.getDefault().setStatusText(this.browserComponent.getBrowserImpl().getStatusMessage());
            return;
        }
        if ("title".equals(e.getPropertyName())) {
            String title = this.browserComponent.getBrowserImpl().getTitle();
            if (title == null || title.length() < 1) {
                return;
            }
            this.setToolTipText(title);
            this.setDisplayName(HtmlBrowserComponent.makeShort(title));
        } else if ("loading".equals(e.getPropertyName())) {
            boolean loading = (Boolean)e.getNewValue();
            this.makeBusy(loading);
        }
    }

    private static String makeShort(String title) {
        if (title.length() > 25) {
            title = title.substring(0, 25);
            title = title + "\u2026";
        }
        return title;
    }

    public void open() {
        if (null != this.browserComponent && this.browserComponent.getBrowserComponent() == null) {
            return;
        }
        this.setDisplayName(this.getDefaultDisplayName());
        super.open();
    }

    protected Object writeReplace() throws ObjectStreamException {
        return new BrowserReplacer(this);
    }

    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        super.readExternal(in);
        this.setStatusLineVisible(in.readBoolean());
        this.setToolbarVisible(in.readBoolean());
        this.browserComponent.setURL((URL)in.readObject());
    }

    protected CloneableTopComponent createClonedObject() {
        HtmlBrowserComponent bc = new HtmlBrowserComponent(this.browserFactory, this.isToolbarVisible(), this.isStatusLineVisible());
        bc.setURL(this.getDocumentURL());
        return bc;
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(HtmlBrowserComponent.class);
    }

    protected void componentActivated() {
        Component c;
        HtmlBrowser.Impl impl;
        if (null != this.browserComponent && null != (impl = this.browserComponent.getBrowserImpl()) && null != (c = impl.getComponent())) {
            c.requestFocusInWindow();
        }
        super.componentActivated();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                HtmlBrowserComponent.this.setEnableHome(HtmlBrowserComponent.this.enableHome);
                HtmlBrowserComponent.this.setEnableLocation(HtmlBrowserComponent.this.enableLocation);
                HtmlBrowserComponent.this.setToolbarVisible(HtmlBrowserComponent.this.toolbarVisible);
                HtmlBrowserComponent.this.setStatusLineVisible(HtmlBrowserComponent.this.statusVisible);
                if (null != HtmlBrowserComponent.this.urlToLoad) {
                    HtmlBrowserComponent.this.setURL(HtmlBrowserComponent.this.urlToLoad);
                }
                HtmlBrowserComponent.this.urlToLoad = null;
            }
        });
    }

    protected void componentClosed() {
        this.releaseBrowser();
    }

    public void recreateBrowser() {
        this.releaseBrowser();
        this.createBrowser();
    }

    private void releaseBrowser() {
        if (null != this.browserComponent) {
            this.toolbarVisible = this.isToolbarVisible();
            this.statusVisible = this.isStatusLineVisible();
            URL url = this.browserComponent.getBrowserImpl().getURL();
            this.urlToLoad = null == url ? null : url.toExternalForm();
            this.browserComponent.getBrowserImpl().removePropertyChangeListener((PropertyChangeListener)this);
            this.browserComponent.getBrowserImpl().dispose();
        }
        this.removeAll();
        this.browserComponent = null;
    }

    private void createBrowser() {
        if (null == this.browserComponent) {
            this.browserComponent = this.createBrowser(this.browserFactory, this.toolbarVisible, this.statusVisible);
            this.initBrowser();
        }
    }

    Lookup getBrowserLookup() {
        if (this.browserComponent != null) {
            return this.browserComponent.getBrowserImpl().getLookup();
        }
        return Lookup.EMPTY;
    }

    protected void componentOpened() {
        this.createBrowser();
    }

    public Image getIcon() {
        return new ImageIcon(HtmlBrowser.class.getResource("/org/openide/resources/html/htmlView.gif")).getImage();
    }

    protected HtmlBrowser createBrowser(HtmlBrowser.Factory factory, boolean showToolbar, boolean showStatus) {
        return new HtmlBrowser(factory, showToolbar, showStatus);
    }

    private void initBrowser() {
        this.add((Component)this.browserComponent, (Object)"Center");
        this.proxyLookup.setLookup(this.getBrowserLookup());
        this.browserComponent.getBrowserImpl().addPropertyChangeListener((PropertyChangeListener)this);
        if (this.browserComponent.getBrowserComponent() != null) {
            this.putClientProperty((Object)"InternalBrowser", (Object)Boolean.TRUE);
        }
        this.initStandardActions();
    }

    public void setURL(String str) {
        if (null == this.browserComponent) {
            this.urlToLoad = str;
            return;
        }
        this.urlToLoad = null;
        this.browserComponent.setURL(str);
    }

    public void setURL(URL url) {
        if (null == this.browserComponent) {
            this.urlToLoad = null == url ? null : url.toExternalForm();
            return;
        }
        this.urlToLoad = null;
        this.browserComponent.setURL(url);
    }

    public final URL getDocumentURL() {
        if (null == this.browserComponent) {
            URL url = null;
            if (null != this.urlToLoad) {
                try {
                    url = new URL(this.urlToLoad);
                }
                catch (MalformedURLException ex) {
                    // empty catch block
                }
            }
            return url;
        }
        return this.browserComponent.getDocumentURL();
    }

    public final void setEnableHome(boolean b) {
        if (null == this.browserComponent) {
            this.enableHome = b;
            return;
        }
        this.browserComponent.setEnableHome(b);
    }

    public final void setEnableLocation(boolean b) {
        if (null == this.browserComponent) {
            this.enableLocation = b;
            return;
        }
        this.browserComponent.setEnableLocation(b);
    }

    public boolean isStatusLineVisible() {
        if (null == this.browserComponent) {
            return this.statusVisible;
        }
        return this.browserComponent.isStatusLineVisible();
    }

    public void setStatusLineVisible(boolean v) {
        if (null == this.browserComponent) {
            this.statusVisible = v;
            return;
        }
        this.browserComponent.setStatusLineVisible(v);
    }

    public boolean isToolbarVisible() {
        if (null == this.browserComponent) {
            return this.toolbarVisible;
        }
        return this.browserComponent.isToolbarVisible();
    }

    public void setToolbarVisible(boolean v) {
        if (null == this.browserComponent) {
            this.toolbarVisible = v;
            return;
        }
        this.browserComponent.setToolbarVisible(v);
    }

    protected String preferredID() {
        return "HtmlBrowserComponent";
    }

    public void setURLAndOpen(URL url) {
        this.createBrowser();
        this.browserComponent.setURL(url);
        this.urlToLoad = null;
        if (null != this.browserComponent.getBrowserComponent()) {
            this.open();
            this.requestActive();
        }
    }

    public HtmlBrowser.Impl getBrowserImpl() {
        return this.browserComponent.getBrowserImpl();
    }

    private void initStandardActions() {
        String RELOAD = "RELOAD_BROWSER";
        String BACK = "NAVIGATE_BACK";
        String FORWARD = "NAVIGATE_FORWARD";
        ActionMap am = this.getActionMap();
        InputMap im = this.getInputMap(1);
        am.put("RELOAD_BROWSER", new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent e) {
                HtmlBrowserComponent.this.getBrowserImpl().reloadDocument();
            }
        });
        im.put(KeyStroke.getKeyStroke(116, 0), "RELOAD_BROWSER");
        im.put(KeyStroke.getKeyStroke(82, 128), "RELOAD_BROWSER");
        am.put("NAVIGATE_BACK", new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent e) {
                HtmlBrowserComponent.this.getBrowserImpl().backward();
            }
        });
        am.put("NAVIGATE_FORWARD", new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent e) {
                HtmlBrowserComponent.this.getBrowserImpl().forward();
            }
        });
        im.put(KeyStroke.getKeyStroke(8, 0), "NAVIGATE_BACK");
        im.put(KeyStroke.getKeyStroke(8, 64), "NAVIGATE_FORWARD");
    }

    private static class MyLookup
    extends ProxyLookup {
        public MyLookup() {
            super(new Lookup[]{Lookup.EMPTY});
        }

        public void setLookup(Lookup lkp) {
            this.setLookups(new Lookup[]{lkp});
        }
    }

    public static final class BrowserReplacer
    implements Externalizable {
        static final long serialVersionUID = 5915713034827048413L;
        private transient HtmlBrowserComponent bComp = null;
        transient boolean statLine;
        transient boolean toolbar;
        transient URL url;

        public BrowserReplacer() {
        }

        public BrowserReplacer(HtmlBrowserComponent comp) {
            this.bComp = comp;
        }

        @Override
        public void writeExternal(ObjectOutput out) throws IOException {
            out.writeBoolean(this.bComp.isStatusLineVisible());
            out.writeBoolean(this.bComp.isToolbarVisible());
            out.writeObject(this.bComp.getDocumentURL());
        }

        @Override
        public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
            this.statLine = in.readBoolean();
            this.toolbar = in.readBoolean();
            this.url = (URL)in.readObject();
        }

        private Object readResolve() throws ObjectStreamException {
            try {
                if ("http".equals(this.url.getProtocol()) && InetAddress.getByName(this.url.getHost()).equals(InetAddress.getLocalHost())) {
                    this.url.openStream();
                }
            }
            catch (UnknownHostException exc) {
            }
            catch (SecurityException exc) {
            }
            catch (NullPointerException exc) {
            }
            catch (IOException exc) {
                return null;
            }
            catch (Exception exc) {
                Logger.getLogger(HtmlBrowserComponent.class.getName()).log(Level.WARNING, null, exc);
            }
            this.bComp = new HtmlBrowserComponent(this.statLine, this.toolbar, this.url);
            return this.bComp;
        }
    }

}

