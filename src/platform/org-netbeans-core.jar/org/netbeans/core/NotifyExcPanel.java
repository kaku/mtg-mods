/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.startup.CLIOptions
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.Mnemonics
 *  org.openide.awt.Notification
 *  org.openide.awt.NotificationDisplayer
 *  org.openide.awt.NotificationDisplayer$Category
 *  org.openide.awt.NotificationDisplayer$Priority
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.Callable;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.text.Caret;
import org.netbeans.core.NbErrorManager;
import org.netbeans.core.startup.CLIOptions;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.awt.Notification;
import org.openide.awt.NotificationDisplayer;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

public final class NotifyExcPanel
extends JPanel
implements ActionListener {
    static final long serialVersionUID = 3680397500573480127L;
    private static NotifyExcPanel INSTANCE = null;
    private static final int SIZE_PREFERRED_WIDTH = 550;
    private static final int SIZE_PREFERRED_HEIGHT = 250;
    private static ArrayListPos exceptions;
    private NbErrorManager.Exc current;
    private DialogDescriptor descriptor;
    Dialog dialog;
    private JButton next;
    private JButton previous;
    private JButton details;
    private JTextPane output;
    private static boolean showDetails;
    private static Rectangle lastBounds;
    private static int extraH;
    private static int extraW;

    private NotifyExcPanel() {
        ResourceBundle bundle = NbBundle.getBundle(NotifyExcPanel.class);
        this.next = new JButton();
        Mnemonics.setLocalizedText((AbstractButton)this.next, (String)bundle.getString("CTL_NextException"));
        this.next.setDefaultCapable(false);
        this.previous = new JButton();
        Mnemonics.setLocalizedText((AbstractButton)this.previous, (String)bundle.getString("CTL_PreviousException"));
        this.previous.setDefaultCapable(false);
        this.details = new JButton();
        this.details.setDefaultCapable(false);
        this.output = new JTextPane(){

            @Override
            public boolean getScrollableTracksViewportWidth() {
                return false;
            }
        };
        this.output.setEditable(false);
        Font f = this.output.getFont();
        this.output.setFont(new Font("Monospaced", 0, null == f ? 12 : f.getSize() + 1));
        this.output.setForeground(UIManager.getColor("Label.foreground"));
        this.output.setBackground(UIManager.getColor("Label.background"));
        this.setLayout(new BorderLayout());
        this.add(new JScrollPane(this.output));
        this.setBorder(new LineBorder(UIManager.getLookAndFeelDefaults().getColor("darculaMod.borderColor")));
        this.next.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_NextException"));
        this.previous.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_PreviousException"));
        this.output.getAccessibleContext().setAccessibleName(bundle.getString("ACSN_ExceptionStackTrace"));
        this.output.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_ExceptionStackTrace"));
        this.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_NotifyExceptionPanel"));
        this.descriptor = new DialogDescriptor((Object)"", "");
        this.descriptor.setMessageType(0);
        this.descriptor.setOptions(NotifyExcPanel.computeOptions(this.previous, this.next));
        this.descriptor.setAdditionalOptions(new Object[]{this.details});
        this.descriptor.setClosingOptions(new Object[0]);
        this.descriptor.setButtonListener((ActionListener)this);
        this.descriptor.setModal(NotifyExcPanel.isModalDialogPresent() && WindowManager.getDefault().getMainWindow().isVisible());
        this.setPreferredSize(new Dimension(550 + extraW, 250 + extraH));
        this.dialog = DialogDisplayer.getDefault().createDialog(this.descriptor);
        if (null != lastBounds) {
            NotifyExcPanel.lastBounds.width = Math.max(NotifyExcPanel.lastBounds.width, 550 + extraW);
            this.dialog.setBounds(lastBounds);
        }
        this.dialog.getAccessibleContext().setAccessibleName(bundle.getString("ACN_NotifyExcPanel_Dialog"));
        this.dialog.getAccessibleContext().setAccessibleDescription(bundle.getString("ACD_NotifyExcPanel_Dialog"));
    }

    static Object[] computeOptions(Object previous, Object next) {
        ArrayList<Object> arr = new ArrayList<Object>();
        arr.add(previous);
        arr.add(next);
        extraH = 0;
        extraW = 0;
        for (Handler h : Logger.getLogger("").getHandlers()) {
            if (!(h instanceof Callable)) continue;
            boolean foundCallableForJButton = false;
            for (Type t : h.getClass().getGenericInterfaces()) {
                Type[] params;
                ParameterizedType p;
                if (!(t instanceof ParameterizedType) || (params = (p = (ParameterizedType)t).getActualTypeArguments()).length != 1 || params[0] != JButton.class) continue;
                foundCallableForJButton = true;
                break;
            }
            if (!foundCallableForJButton) continue;
            try {
                Object o = ((Callable)((Object)h)).call();
                if (o == null) continue;
                assert (o instanceof JButton);
                JButton b = (JButton)o;
                extraH += b.getPreferredSize().height;
                extraW += b.getPreferredSize().width;
                arr.add(o);
                continue;
            }
            catch (Exception ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        arr.add(NotifyDescriptor.CANCEL_OPTION);
        return arr.toArray();
    }

    private static boolean isModalDialogPresent() {
        return NotifyExcPanel.hasModalDialog(WindowManager.getDefault().getMainWindow()) || NotifyExcPanel.hasModalDialog(new JDialog().getOwner());
    }

    private static boolean hasModalDialog(Window w) {
        if (w == null) {
            return false;
        }
        Window[] ws = w.getOwnedWindows();
        for (int i = 0; i < ws.length; ++i) {
            if (ws[i] instanceof Dialog && ((Dialog)ws[i]).isModal() && ws[i].isVisible()) {
                return true;
            }
            if (!NotifyExcPanel.hasModalDialog(ws[i])) continue;
            return true;
        }
        return false;
    }

    static void cleanInstance() {
        INSTANCE = null;
    }

    static void notify(final NbErrorManager.Exc t) {
        if (!t.isUserQuestion() && !NotifyExcPanel.shallNotify(t.getSeverity(), false)) {
            return;
        }
        if (GraphicsEnvironment.isHeadless()) {
            t.printStackTrace(System.err);
            return;
        }
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                String glm = t.getLocalizedMessage();
                Level gs = t.getSeverity();
                boolean loc = t.isLocalized();
                if (t.isUserQuestion() && loc) {
                    Object ret = DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Confirmation((Object)glm, 2));
                    if (ret == NotifyDescriptor.OK_OPTION) {
                        try {
                            t.confirm();
                        }
                        catch (IOException ex) {
                            Exceptions.printStackTrace((Throwable)ex);
                        }
                    }
                    return;
                }
                if (loc) {
                    if (gs == Level.WARNING) {
                        DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)glm, 2));
                        return;
                    }
                    if (gs.intValue() == 1973) {
                        DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)glm, 1));
                        return;
                    }
                    if (gs == Level.SEVERE) {
                        DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)glm, 0));
                        return;
                    }
                }
                if (null == exceptions) {
                    exceptions = new ArrayListPos();
                }
                exceptions.add(t);
                NotifyExcPanel.access$000().position = exceptions.size() - 1;
                if (NotifyExcPanel.shallNotify(t.getSeverity(), true)) {
                    if (INSTANCE == null) {
                        INSTANCE = new NotifyExcPanel();
                    }
                    INSTANCE.updateState(t);
                } else if (null == INSTANCE) {
                    ImageIcon img1 = ImageUtilities.loadImageIcon((String)"org/netbeans/core/resources/exception.gif", (boolean)true);
                    String summary = NotifyExcPanel.getExceptionSummary(t);
                    ExceptionFlasher flash = ExceptionFlasher.notify(summary, img1);
                } else {
                    if (INSTANCE == null) {
                        INSTANCE = new NotifyExcPanel();
                    }
                    INSTANCE.updateState(t);
                }
            }
        });
    }

    private static String getExceptionSummary(NbErrorManager.Exc t) {
        String glm = t.getLocalizedMessage();
        String plainmsg = glm != null ? glm : (t.getMessage() != null ? t.getMessage() : t.getClassName());
        assert (plainmsg != null);
        return plainmsg;
    }

    private void updateState(NbErrorManager.Exc t) {
        if (!exceptions.existsNextElement()) {
            this.current = t;
            this.update();
        } else {
            this.next.setVisible(true);
            this.dialog.pack();
        }
        try {
            this.ensurePreferredSize();
            this.dialog.setVisible(true);
        }
        catch (Exception e) {
            exceptions.add(NbErrorManager.createExc(e, Level.SEVERE, null));
            this.next.setVisible(true);
        }
    }

    private void ensurePreferredSize() {
        if (null != lastBounds) {
            return;
        }
        Dimension sz = this.dialog.getSize();
        Dimension pref = this.dialog.getPreferredSize();
        if (pref.height == 0) {
            pref.height = 250;
        }
        if (pref.width == 0) {
            pref.width = 550;
        }
        if (!sz.equals(pref)) {
            this.dialog.setSize(pref.width, pref.height);
            this.dialog.validate();
            this.dialog.repaint();
        }
    }

    private void update() {
        boolean repack;
        boolean isLocalized = this.current.isLocalized();
        boolean visNext = this.next.isVisible();
        boolean visPrev = this.previous.isVisible();
        this.next.setVisible(exceptions.existsNextElement());
        this.previous.setVisible(exceptions.existsPreviousElement());
        boolean bl = repack = this.next.isVisible() != visNext || this.previous.isVisible() != visPrev;
        if (showDetails) {
            Mnemonics.setLocalizedText((AbstractButton)this.details, (String)NbBundle.getBundle(NotifyExcPanel.class).getString("CTL_Exception_Hide_Details"));
            this.details.getAccessibleContext().setAccessibleDescription(NbBundle.getBundle(NotifyExcPanel.class).getString("ACSD_Exception_Hide_Details"));
        } else {
            Mnemonics.setLocalizedText((AbstractButton)this.details, (String)NbBundle.getBundle(NotifyExcPanel.class).getString("CTL_Exception_Show_Details"));
            this.details.getAccessibleContext().setAccessibleDescription(NbBundle.getBundle(NotifyExcPanel.class).getString("ACSD_Exception_Show_Details"));
        }
        String title = NbBundle.getBundle(NotifyExcPanel.class).getString("CTL_Title_Exception");
        if (showDetails) {
            this.descriptor.setMessage((Object)this);
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    StringWriter wr = new StringWriter();
                    NotifyExcPanel.this.current.printStackTrace(new PrintWriter(wr, true));
                    NotifyExcPanel.this.output.setText(wr.toString());
                    NotifyExcPanel.this.output.getCaret().setDot(0);
                    NotifyExcPanel.this.output.requestFocus();
                }
            });
        } else if (isLocalized) {
            String msg = this.current.getLocalizedMessage();
            if (msg != null) {
                this.descriptor.setMessage((Object)msg);
            }
        } else {
            ResourceBundle curBundle = NbBundle.getBundle(NotifyExcPanel.class);
            if (this.current.getSeverity() == Level.WARNING) {
                this.descriptor.setMessage((Object)MessageFormat.format(curBundle.getString("NTF_ExceptionWarning"), this.current.getClassName()));
                title = curBundle.getString("NTF_ExceptionWarningTitle");
            } else {
                this.descriptor.setMessage((Object)MessageFormat.format(curBundle.getString("NTF_ExceptionalException"), this.current.getClassName(), CLIOptions.getLogDir()));
                title = curBundle.getString("NTF_ExceptionalExceptionTitle");
            }
        }
        this.descriptor.setTitle(title);
        if (repack) {
            this.dialog.pack();
        }
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        Object source = ev.getSource();
        if (source == this.next && exceptions.setNextElement() || source == this.previous && exceptions.setPreviousElement()) {
            this.current = exceptions.get();
            LogRecord rec = new LogRecord(Level.CONFIG, "NotifyExcPanel: " + ev.getActionCommand());
            String message = this.current.getMessage();
            String className = this.current.getClassName();
            if (message != null) {
                className = className + ": " + message;
            }
            Object[] params = new Object[]{className, this.current.getFirstStacktraceLine()};
            rec.setParameters(params);
            Logger.getLogger("org.netbeans.ui.NotifyExcPanel").log(rec);
            this.update();
            return;
        }
        if (source == this.details) {
            showDetails = !showDetails;
            lastBounds = null;
            try {
                this.update();
                this.ensurePreferredSize();
            }
            catch (Exception e) {
                exceptions.add(NbErrorManager.createExc(e, Level.SEVERE, null));
                this.next.setVisible(true);
            }
            return;
        }
        if (source == NotifyDescriptor.OK_OPTION || source == NotifyDescriptor.CLOSED_OPTION || source == NotifyDescriptor.CANCEL_OPTION) {
            LogRecord rec = new LogRecord(Level.CONFIG, "NotifyExcPanel:  close");
            rec.setParameters(null);
            Logger.getLogger("org.netbeans.ui.NotifyExcPanel").log(rec);
            try {
                if (null != exceptions) {
                    exceptions.removeAll();
                }
                lastBounds = this.dialog.getBounds();
                this.dialog.dispose();
                exceptions = null;
                INSTANCE = null;
            }
            catch (RuntimeException e) {
                throw e;
            }
            finally {
                exceptions = null;
                INSTANCE = null;
            }
        }
    }

    private static boolean shallNotify(Level level, boolean dialog) {
        int minAlert = Integer.getInteger("netbeans.exception.alert.min.level", 900);
        boolean assertionsOn = false;
        if (!$assertionsDisabled) {
            assertionsOn = true;
            if (!true) {
                throw new AssertionError();
            }
        }
        int defReport = assertionsOn ? 900 : 1001;
        int minReport = Integer.getInteger("netbeans.exception.report.min.level", defReport);
        if (dialog) {
            return level.intValue() >= minReport;
        }
        return level.intValue() >= minAlert || level.intValue() >= minReport;
    }

    static {
        extraH = 0;
        extraW = 0;
    }

    static class ArrayListPos
    extends ArrayList<NbErrorManager.Exc> {
        static final long serialVersionUID = 2;
        protected int position = 0;

        protected ArrayListPos() {
        }

        protected boolean existsElement() {
            return this.size() > 0;
        }

        protected boolean existsNextElement() {
            return this.position + 1 < this.size();
        }

        protected boolean existsPreviousElement() {
            return this.position > 0 && this.size() > 0;
        }

        protected boolean setNextElement() {
            if (!this.existsNextElement()) {
                return false;
            }
            ++this.position;
            return true;
        }

        protected boolean setPreviousElement() {
            if (!this.existsPreviousElement()) {
                return false;
            }
            --this.position;
            return true;
        }

        protected NbErrorManager.Exc get() {
            return this.existsElement() ? (NbErrorManager.Exc)this.get(this.position) : null;
        }

        protected void removeAll() {
            this.clear();
            this.position = 0;
        }
    }

    static class ExceptionFlasher
    implements ActionListener {
        static ExceptionFlasher flash;
        Notification note;
        private final Timer timer;

        private static synchronized ExceptionFlasher notify(String summary, ImageIcon icon) {
            if (flash == null) {
                flash = new ExceptionFlasher();
            } else {
                ExceptionFlasher.flash.timer.restart();
                ExceptionFlasher.flash.note.clear();
            }
            JComponent detailsPanel = ExceptionFlasher.getDetailsPanel(summary);
            JComponent bubblePanel = ExceptionFlasher.getDetailsPanel(summary);
            ExceptionFlasher.flash.note = NotificationDisplayer.getDefault().notify(NbBundle.getMessage(NotifyExcPanel.class, (String)"NTF_ExceptionalExceptionTitle"), (Icon)icon, bubblePanel, detailsPanel, NotificationDisplayer.Priority.SILENT, NotificationDisplayer.Category.ERROR);
            return flash;
        }

        public ExceptionFlasher() {
            this.timer = new Timer(300000, this);
            this.timer.setRepeats(false);
            this.timer.start();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == this.timer) {
                this.timeout();
                return;
            }
            Class<ExceptionFlasher> class_ = ExceptionFlasher.class;
            synchronized (ExceptionFlasher.class) {
                if (this.note != null) {
                    this.note.clear();
                }
                flash = null;
                // ** MonitorExit[var2_2] (shouldn't be in output)
                if (null != exceptions && exceptions.size() > 0) {
                    if (INSTANCE == null) {
                        INSTANCE = new NotifyExcPanel();
                    }
                    INSTANCE.updateState((NbErrorManager.Exc)exceptions.get(exceptions.size() - 1));
                }
                return;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void timeout() {
            Class<ExceptionFlasher> class_ = ExceptionFlasher.class;
            synchronized (ExceptionFlasher.class) {
                assert (EventQueue.isDispatchThread());
                if (null != INSTANCE) {
                    // ** MonitorExit[var1_1] (shouldn't be in output)
                    return;
                }
                if (null != exceptions) {
                    exceptions.clear();
                }
                exceptions = null;
                flash = null;
                this.timer.stop();
                if (null != this.note) {
                    this.note.clear();
                }
                // ** MonitorExit[var1_1] (shouldn't be in output)
                return;
            }
        }

        private static JComponent getDetailsPanel(String summary) {
            JPanel details = new JPanel(new GridBagLayout());
            details.setOpaque(false);
            JLabel lblMessage = new JLabel(summary);
            JButton reportLink = new JButton("<html><a href=\"_blank\">" + NbBundle.getMessage(NotifyExcPanel.class, (String)"NTF_ExceptionalExceptionReport"));
            reportLink.setFocusable(false);
            reportLink.setBorder(BorderFactory.createEmptyBorder(4, 2, 4, 2));
            reportLink.setRolloverEnabled(false);
            reportLink.setBackground(details.getBackground());
            reportLink.setBorderPainted(false);
            reportLink.setFocusPainted(false);
            reportLink.setOpaque(false);
            reportLink.setContentAreaFilled(false);
            reportLink.addActionListener(flash);
            reportLink.setCursor(Cursor.getPredefinedCursor(12));
            details.add((Component)reportLink, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, 17, 0, new Insets(3, 0, 3, 0), 0, 0));
            details.add((Component)lblMessage, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, 17, 0, new Insets(3, 0, 3, 0), 0, 0));
            return details;
        }
    }

}

