/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.startup.ModuleSystem
 *  org.openide.DialogDisplayer
 *  org.openide.LifecycleManager
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.StatusDisplayer
 *  org.openide.cookies.SaveCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 */
package org.netbeans.core;

import java.awt.EventQueue;
import java.awt.SecondaryLoop;
import java.awt.Toolkit;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.NbLifeExit;
import org.netbeans.core.startup.ModuleSystem;
import org.openide.DialogDisplayer;
import org.openide.LifecycleManager;
import org.openide.NotifyDescriptor;
import org.openide.awt.StatusDisplayer;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;

public final class NbLifecycleManager
extends LifecycleManager {
    static final Logger LOG = Logger.getLogger(NbLifecycleManager.class.getName());
    private static CountDownLatch onExit;
    private volatile SecondaryLoop sndLoop;
    private volatile boolean isExitOnEventQueue;

    public void saveAll() {
        ArrayList<DataObject> bad = new ArrayList<DataObject>();
        DataObject[] modifs = DataObject.getRegistry().getModified();
        if (modifs.length == 0) {
            return;
        }
        for (DataObject dobj : modifs) {
            try {
                SaveCookie sc = (SaveCookie)dobj.getLookup().lookup(SaveCookie.class);
                if (sc == null) continue;
                StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(NbLifecycleManager.class, (String)"CTL_FMT_SavingMessage", (Object)dobj.getName()));
                sc.save();
                continue;
            }
            catch (IOException ex) {
                Logger.getLogger(NbLifecycleManager.class.getName()).log(Level.WARNING, null, ex);
                bad.add(dobj);
            }
        }
        for (DataObject badDO : bad) {
            NotifyDescriptor.Message descriptor = new NotifyDescriptor.Message((Object)NbBundle.getMessage(NbLifecycleManager.class, (String)"CTL_Cannot_save", (Object)badDO.getPrimaryFile().getName()));
            DialogDisplayer.getDefault().notify((NotifyDescriptor)descriptor);
        }
        StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(NbLifecycleManager.class, (String)"MSG_AllSaved"));
    }

    public void exit() {
        this.exit(0);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean blockForExit(CountDownLatch[] arr) {
        Class<NbLifecycleManager> class_ = NbLifecycleManager.class;
        synchronized (NbLifecycleManager.class) {
            if (onExit != null) {
                arr[0] = onExit;
                LOG.log(Level.FINE, "blockForExit, already counting down {0}", onExit);
                // ** MonitorExit[var2_2] (shouldn't be in output)
                return true;
            }
            arr[0] = NbLifecycleManager.onExit = new CountDownLatch(1){

                @Override
                public void countDown() {
                    super.countDown();
                    SecondaryLoop d = NbLifecycleManager.this.sndLoop;
                    NbLifecycleManager.LOG.log(Level.FINE, "countDown for {0}, hiding {1}, by {2}", new Object[]{this, d, Thread.currentThread()});
                    if (d != null) {
                        while (!d.exit()) {
                            NbLifecycleManager.LOG.log(Level.FINE, "exit before enter, try again");
                        }
                    }
                }
            };
            LOG.log(Level.FINE, "blockForExit, new {0}", onExit);
            // ** MonitorExit[var2_2] (shouldn't be in output)
            return false;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Converted monitor instructions to comments
     * Lifted jumps to return sites
     */
    private void finishExitState(CountDownLatch cdl, boolean clean) {
        NbLifecycleManager.LOG.log(Level.FINE, "finishExitState {0} clean: {1}", new Object[]{Thread.currentThread(), clean});
        if (EventQueue.isDispatchThread()) {
            while (cdl.getCount() > 0) {
                prev = this.isExitOnEventQueue;
                if (!prev) {
                    this.isExitOnEventQueue = true;
                    try {
                        NbLifecycleManager.LOG.log(Level.FINE, "waiting in EDT: {0} own: {1}", new Object[]{NbLifecycleManager.onExit, cdl});
                        if (cdl.await(5, TimeUnit.SECONDS)) {
                            NbLifecycleManager.LOG.fine("wait is over, return");
                            return;
                        }
                    }
                    catch (InterruptedException ex) {
                        NbLifecycleManager.LOG.log(Level.FINE, null, ex);
                    }
                }
                sl = Toolkit.getDefaultToolkit().getSystemEventQueue().createSecondaryLoop();
                try {
                    this.sndLoop = sl;
                    NbLifecycleManager.LOG.log(Level.FINE, "Showing dialog: {0}", sl);
                    sl.enter();
                    continue;
                }
                finally {
                    NbLifecycleManager.LOG.log(Level.FINE, "Disposing dialog: {0}", this.sndLoop);
                    this.sndLoop = null;
                    this.isExitOnEventQueue = prev;
                    continue;
                }
            }
        }
        NbLifecycleManager.LOG.log(Level.FINE, "About to block on {0}", cdl);
        try {
            cdl.await();
            ** if (!clean) goto lbl-1000
        }
        catch (InterruptedException ex) {
            try {
                NbLifecycleManager.LOG.log(Level.FINE, null, ex);
                ** if (!clean) goto lbl-1000
            }
            catch (Throwable var8_10) {
                if (clean == false) throw var8_10;
                NbLifecycleManager.LOG.log(Level.FINE, "Cleaning {0} own {1}", new Object[]{NbLifecycleManager.onExit, cdl});
                var9_11 = NbLifecycleManager.class;
                // MONITORENTER : org.netbeans.core.NbLifecycleManager.class
                if (!NbLifecycleManager.$assertionsDisabled && cdl != NbLifecycleManager.onExit) {
                    throw new AssertionError();
                }
                NbLifecycleManager.onExit = null;
                // MONITOREXIT : var9_11
                throw var8_10;
            }
lbl-1000: // 1 sources:
            {
                NbLifecycleManager.LOG.log(Level.FINE, "Cleaning {0} own {1}", new Object[]{NbLifecycleManager.onExit, cdl});
                ex = NbLifecycleManager.class;
                // MONITORENTER : org.netbeans.core.NbLifecycleManager.class
                if (!NbLifecycleManager.$assertionsDisabled && cdl != NbLifecycleManager.onExit) {
                    throw new AssertionError();
                }
                NbLifecycleManager.onExit = null;
                // MONITOREXIT : ex
            }
lbl-1000: // 2 sources:
            {
            }
        }
lbl-1000: // 1 sources:
        {
            NbLifecycleManager.LOG.log(Level.FINE, "Cleaning {0} own {1}", new Object[]{NbLifecycleManager.onExit, cdl});
            prev = NbLifecycleManager.class;
            // MONITORENTER : org.netbeans.core.NbLifecycleManager.class
            if (!NbLifecycleManager.$assertionsDisabled && cdl != NbLifecycleManager.onExit) {
                throw new AssertionError();
            }
            NbLifecycleManager.onExit = null;
            // MONITOREXIT : prev
        }
lbl-1000: // 2 sources:
        {
        }
        NbLifecycleManager.LOG.fine("End of finishExitState");
    }

    public void exit(int status) {
        LOG.log(Level.FINE, "Initiating exit with status {0}", status);
        if (EventQueue.isDispatchThread()) {
            if (this.isExitOnEventQueue) {
                LOG.log(Level.FINE, "Already in process of exiting {0}, return", this.isExitOnEventQueue);
                return;
            }
            this.isExitOnEventQueue = true;
        }
        try {
            CountDownLatch[] cdl = new CountDownLatch[]{null};
            if (this.blockForExit(cdl)) {
                this.finishExitState(cdl[0], false);
                return;
            }
            NbLifeExit action = new NbLifeExit(0, status, cdl[0]);
            Mutex.EVENT.readAccess((Runnable)action);
            this.finishExitState(cdl[0], true);
        }
        catch (Error | RuntimeException ex) {
            LOG.log(Level.SEVERE, "Error during shutdown", ex);
            throw ex;
        }
        finally {
            if (EventQueue.isDispatchThread()) {
                this.isExitOnEventQueue = false;
            }
        }
    }

    public static synchronized boolean isExiting() {
        return onExit != null;
    }

    public void markForRestart() throws UnsupportedOperationException {
        ModuleSystem.markForRestart();
    }

}

