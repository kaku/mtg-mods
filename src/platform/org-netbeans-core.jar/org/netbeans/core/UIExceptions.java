/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package org.netbeans.core;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import org.openide.util.Exceptions;

public final class UIExceptions {
    private UIExceptions() {
    }

    public static void annotateUser(Throwable t, String msg, String locMsg, Throwable stackTrace, Date date) {
        AnnException ex = AnnException.findOrCreate(t, true);
        LogRecord rec = new LogRecord(OwnLevel.USER, msg);
        if (stackTrace != null) {
            rec.setThrown(stackTrace);
        }
        ex.addRecord(rec);
        if (locMsg != null) {
            Exceptions.attachLocalizedMessage((Throwable)t, (String)locMsg);
        }
    }

    private static final class AnnException
    extends Exception
    implements Callable<LogRecord[]> {
        private List<LogRecord> records;

        @Override
        public String getMessage() {
            StringBuilder sb = new StringBuilder();
            String sep = "";
            for (LogRecord r : this.records) {
                if (r.getMessage() == null) continue;
                sb.append(sep);
                sb.append(r.getMessage());
                sep = "\n";
            }
            return sb.toString();
        }

        static AnnException findOrCreate(Throwable t, boolean create) {
            if (t instanceof AnnException) {
                return (AnnException)t;
            }
            if (t.getCause() == null) {
                if (create) {
                    t.initCause(new AnnException());
                }
                return (AnnException)t.getCause();
            }
            return AnnException.findOrCreate(t.getCause(), create);
        }

        private AnnException() {
        }

        public synchronized void addRecord(LogRecord rec) {
            if (this.records == null) {
                this.records = new ArrayList<LogRecord>();
            }
            this.records.add(rec);
        }

        @Override
        public LogRecord[] call() {
            List<LogRecord> r = this.records;
            LogRecord[] empty = new LogRecord[]{};
            return r == null ? empty : r.toArray(empty);
        }

        @Override
        public void printStackTrace(PrintStream s) {
            super.printStackTrace(s);
            this.logRecords(s);
        }

        @Override
        public void printStackTrace(PrintWriter s) {
            super.printStackTrace(s);
            this.logRecords(s);
        }

        @Override
        public void printStackTrace() {
            this.printStackTrace(System.err);
        }

        private void logRecords(Appendable a) {
            List<LogRecord> r = this.records;
            if (r == null) {
                return;
            }
            try {
                for (LogRecord log : r) {
                    if (log.getMessage() != null) {
                        a.append(log.getMessage()).append("\n");
                    }
                    if (log.getThrown() == null) continue;
                    StringWriter w = new StringWriter();
                    log.getThrown().printStackTrace(new PrintWriter(w));
                    a.append(w.toString()).append("\n");
                }
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private static final class OwnLevel
    extends Level {
        public static final Level USER = new OwnLevel("USER", 1973);

        private OwnLevel(String s, int i) {
            super(s, i);
        }
    }

}

