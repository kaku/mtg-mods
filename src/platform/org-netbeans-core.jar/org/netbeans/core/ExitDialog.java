/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.actions.Savable
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.awt.Mnemonics
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.core;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Collection;
import java.util.ResourceBundle;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.netbeans.api.actions.Savable;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class ExitDialog
extends JPanel
implements ActionListener {
    private static final boolean isAqua = "Aqua".equals(UIManager.getLookAndFeel().getID());
    private static Object[] exitOptions;
    private static Dialog exitDialog;
    private static boolean result;
    JList list;
    DefaultListModel listModel;
    static final long serialVersionUID = 6039058107124767512L;

    public ExitDialog() {
        this.setLayout(new BorderLayout());
        this.listModel = new DefaultListModel();
        for (Savable obj : Savable.REGISTRY.lookupAll(Savable.class)) {
            this.listModel.addElement(obj);
        }
        this.draw();
    }

    private void draw() {
        this.list = new JList(this.listModel);
        this.list.addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent evt) {
                ExitDialog.this.updateSaveButton();
            }
        });
        if (!this.listModel.isEmpty()) {
            this.list.setSelectedIndex(0);
        } else {
            this.updateSaveButton();
        }
        JScrollPane scroll = new JScrollPane(this.list);
        this.setBorder(BorderFactory.createEmptyBorder(12, 12, 11, 12));
        this.add((Component)scroll, "Center");
        this.list.setCellRenderer(new ExitDlgListCellRenderer());
        this.list.getAccessibleContext().setAccessibleName(NbBundle.getBundle(ExitDialog.class).getString("ACSN_ListOfChangedFiles"));
        this.list.getAccessibleContext().setAccessibleDescription(NbBundle.getBundle(ExitDialog.class).getString("ACSD_ListOfChangedFiles"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getBundle(ExitDialog.class).getString("ACSD_ExitDialog"));
    }

    private void updateSaveButton() {
        ((JButton)exitOptions[0]).setEnabled(this.list.getSelectedIndex() != -1);
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension prev = super.getPreferredSize();
        return new Dimension(Math.max(300, prev.width), Math.max(150, prev.height));
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        if (exitOptions[0].equals(evt.getSource())) {
            this.save(false);
        } else if (exitOptions[1].equals(evt.getSource())) {
            this.save(true);
        } else if (exitOptions[2].equals(evt.getSource())) {
            this.theEnd();
        } else if (NotifyDescriptor.CANCEL_OPTION.equals(evt.getSource())) {
            exitDialog.setVisible(false);
        }
    }

    private void save(boolean all) {
        Object[] array = all ? this.listModel.toArray() : this.list.getSelectedValues();
        int count = array == null ? 0 : array.length;
        int index = 0;
        for (int i = 0; i < count; ++i) {
            Savable nextObject = (Savable)array[i];
            index = this.listModel.indexOf((Object)nextObject);
            this.save(nextObject);
        }
        if (this.listModel.isEmpty()) {
            this.theEnd();
        } else {
            if (index < 0) {
                index = 0;
            } else if (index > this.listModel.size() - 1) {
                index = this.listModel.size() - 1;
            }
            this.list.setSelectedIndex(index);
        }
    }

    private void save(Savable sc) {
        try {
            if (sc != null) {
                sc.save();
            }
            this.listModel.removeElement((Object)sc);
        }
        catch (IOException exc) {
            Throwable t = exc;
            if (Exceptions.findLocalizedMessage((Throwable)exc) == null) {
                t = Exceptions.attachLocalizedMessage((Throwable)exc, (String)NbBundle.getBundle(ExitDialog.class).getString("EXC_Save"));
            }
            Exceptions.printStackTrace((Throwable)t);
        }
    }

    private void theEnd() {
        result = true;
        exitDialog.setVisible(false);
        exitDialog.dispose();
    }

    public static boolean showDialog() {
        return ExitDialog.innerShowDialog();
    }

    private static boolean innerShowDialog() {
        Collection set = Savable.REGISTRY.lookupAll(Savable.class);
        if (!set.isEmpty()) {
            exitDialog = null;
            if (exitDialog == null) {
                ResourceBundle bundle = NbBundle.getBundle(ExitDialog.class);
                JButton buttonSave = new JButton();
                buttonSave.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_Save"));
                JButton buttonSaveAll = new JButton();
                buttonSaveAll.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_SaveAll"));
                JButton buttonDiscardAll = new JButton();
                buttonDiscardAll.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_DiscardAll"));
                Mnemonics.setLocalizedText((AbstractButton)buttonSave, (String)bundle.getString("CTL_Save"));
                Mnemonics.setLocalizedText((AbstractButton)buttonSaveAll, (String)bundle.getString("CTL_SaveAll"));
                Mnemonics.setLocalizedText((AbstractButton)buttonDiscardAll, (String)bundle.getString("CTL_DiscardAll"));
                exitOptions = new Object[]{buttonSave, buttonSaveAll, buttonDiscardAll};
                ExitDialog exitComponent = new ExitDialog();
                DialogDescriptor exitDlgDescriptor = new DialogDescriptor((Object)exitComponent, bundle.getString("CTL_ExitTitle"), true, exitOptions, NotifyDescriptor.CANCEL_OPTION, 1, null, (ActionListener)exitComponent);
                exitDlgDescriptor.setHelpCtx(new HelpCtx("help_on_exit_dialog"));
                exitDlgDescriptor.setAdditionalOptions(new Object[]{NotifyDescriptor.CANCEL_OPTION});
                exitDialog = DialogDisplayer.getDefault().createDialog(exitDlgDescriptor);
            }
            result = false;
            exitDialog.setVisible(true);
            return result;
        }
        return true;
    }

    static {
        result = false;
    }

    private class ExitDlgListCellRenderer
    extends JLabel
    implements ListCellRenderer {
        static final long serialVersionUID = 1877692790854373689L;
        protected Border hasFocusBorder;
        protected Border noFocusBorder;

        public ExitDlgListCellRenderer() {
            this.setOpaque(true);
            this.setBorder(this.noFocusBorder);
            this.hasFocusBorder = isAqua ? BorderFactory.createEmptyBorder(1, 1, 1, 1) : new LineBorder(UIManager.getColor("List.focusCellHighlight"));
            this.noFocusBorder = BorderFactory.createEmptyBorder(1, 1, 1, 1);
        }

        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            Savable obj = (Savable)value;
            if (obj instanceof Icon) {
                super.setIcon((Icon)obj);
            }
            this.setText(obj.toString());
            if (isSelected) {
                this.setBackground(UIManager.getColor("List.selectionBackground"));
                this.setForeground(UIManager.getColor("List.selectionForeground"));
            } else {
                this.setBackground(list.getBackground());
                this.setForeground(list.getForeground());
            }
            this.setBorder(cellHasFocus ? this.hasFocusBorder : this.noFocusBorder);
            return this;
        }
    }

}

