/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.plaf.Startup
 *  org.openide.util.Lookup
 */
package org.netbeans.core.impl;

import org.netbeans.swing.plaf.Startup;
import org.openide.util.Lookup;

public final class InitUI
implements Runnable {
    @Override
    public void run() {
        Startup.setClassLoader((ClassLoader)((ClassLoader)Lookup.getDefault().lookup(ClassLoader.class)));
    }
}

