/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.AcceleratorBinding
 *  org.openide.awt.StatusDisplayer
 *  org.openide.cookies.InstanceCookie
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.loaders.DataShadow
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.util.actions.SystemAction
 */
package org.netbeans.core;

import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;
import javax.swing.text.Keymap;
import org.openide.awt.AcceleratorBinding;
import org.openide.awt.StatusDisplayer;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.loaders.DataShadow;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.util.actions.SystemAction;

public final class NbKeymap
implements Keymap,
Comparator<KeyStroke> {
    private static final RequestProcessor RP = new RequestProcessor(NbKeymap.class);
    public static final String BINDING_REMOVED = "removed";
    public static final String SHADOW_EXT = "shadow";
    private RequestProcessor.Task refreshTask;
    private static final Action BROKEN = new AbstractAction("<broken>"){

        @Override
        public void actionPerformed(ActionEvent e) {
            Toolkit.getDefaultToolkit().beep();
        }
    };
    private Map<KeyStroke, Binding> bindings;
    private Map<String, KeyStroke> id2Stroke;
    private final Map<Action, String> action2Id = new WeakHashMap<Action, String>();
    private FileChangeListener keymapListener;
    private FileChangeListener bindingsListener;
    private static final List<KeyStroke> context = new ArrayList<KeyStroke>();
    private static final Logger LOG = Logger.getLogger(NbKeymap.class.getName());

    private void refreshBindings() {
        this.refreshTask = RP.post(new Runnable(){

            @Override
            public void run() {
                NbKeymap.this.doRefreshBindings();
            }
        });
    }

    private synchronized void doRefreshBindings() {
        this.bindings = null;
        this.bindings();
    }

    boolean waitFinished() throws InterruptedException {
        return this.refreshTask != null ? this.refreshTask.waitFinished(9999) : false;
    }

    private synchronized Map<KeyStroke, Binding> bindings() {
        if (this.bindings == null) {
            this.bindings = new HashMap<KeyStroke, Binding>();
            boolean refresh = this.id2Stroke != null;
            this.id2Stroke = new TreeMap<String, KeyStroke>();
            ArrayList<FileObject> dirs = new ArrayList<FileObject>(2);
            dirs.add(FileUtil.getConfigFile((String)"Shortcuts"));
            FileObject keymaps = FileUtil.getConfigFile((String)"Keymaps");
            if (keymaps != null) {
                String curr = (String)keymaps.getAttribute("currentKeymap");
                if (curr == null) {
                    curr = "NetBeans";
                }
                dirs.add(keymaps.getFileObject(curr));
                if (this.keymapListener == null) {
                    this.keymapListener = new FileChangeAdapter(){

                        public void fileAttributeChanged(FileAttributeEvent fe) {
                            NbKeymap.this.refreshBindings();
                        }
                    };
                    keymaps.addFileChangeListener(this.keymapListener);
                }
            }
            HashMap<String, FileObject> id2Dir = new HashMap<String, FileObject>();
            boolean processingProfile = false;
            LinkedHashMap<String, FileObject> activeShortcuts = new LinkedHashMap<String, FileObject>();
            for (FileObject dir : dirs) {
                if (dir != null) {
                    for (FileObject def : dir.getChildren()) {
                        if (!def.isData()) continue;
                        boolean removed = processingProfile && "removed".equals(def.getExt());
                        String fn = def.getName();
                        if (removed) {
                            activeShortcuts.remove(fn);
                            continue;
                        }
                        activeShortcuts.put(fn, def);
                    }
                    dir.removeFileChangeListener(this.bindingsListener);
                    dir.addFileChangeListener(this.bindingsListener);
                }
                processingProfile = true;
            }
            for (FileObject def : activeShortcuts.values()) {
                KeyStroke former;
                FileObject dir2 = def.getParent();
                if (!def.isData()) continue;
                KeyStroke[] strokes = Utilities.stringToKeys((String)def.getName());
                if (strokes == null || strokes.length == 0) {
                    LOG.log(Level.WARNING, "could not load parse name of " + def.getPath());
                    continue;
                }
                Map<KeyStroke, Binding> binder = this.bindings;
                for (int i = 0; i < strokes.length - 1; ++i) {
                    Binding sub = binder.get(strokes[i]);
                    if (sub != null && sub.nested == null) {
                        LOG.log(Level.WARNING, "conflict between " + sub.actionDefinition.getPath() + " and " + def.getPath());
                        sub = null;
                    }
                    if (sub == null) {
                        sub = new Binding();
                        binder.put(strokes[i], sub);
                    }
                    binder = sub.nested;
                }
                binder.put(strokes[strokes.length - 1], new Binding(def));
                if (strokes.length != 1) continue;
                String id = NbKeymap.idForFile(def);
                KeyStroke keyStroke = former = id2Dir.put(id, dir2) == dir2 ? this.id2Stroke.get(id) : null;
                if (former != null && this.compare(former, strokes[0]) <= 0) continue;
                this.id2Stroke.put(id, strokes[0]);
            }
            if (refresh) {
                EventQueue.invokeLater(new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        Map map = NbKeymap.this.action2Id;
                        synchronized (map) {
                            for (Map.Entry entry : NbKeymap.this.action2Id.entrySet()) {
                                ((Action)entry.getKey()).putValue("AcceleratorKey", NbKeymap.this.id2Stroke.get(entry.getValue()));
                            }
                        }
                    }
                });
            }
            if (LOG.isLoggable(Level.FINE)) {
                for (Map.Entry entry : this.id2Stroke.entrySet()) {
                    LOG.fine(entry.getValue() + " => " + (String)entry.getKey());
                }
            }
        }
        return this.bindings;
    }

    private static void resetContext() {
        context.clear();
        StatusDisplayer.getDefault().setStatusText("");
    }

    public static KeyStroke[] getContext() {
        return context.toArray(new KeyStroke[context.size()]);
    }

    private static void shiftContext(KeyStroke stroke) {
        context.add(stroke);
        StringBuilder text = new StringBuilder();
        for (KeyStroke ks : context) {
            text.append(NbKeymap.getKeyText(ks)).append(' ');
        }
        StatusDisplayer.getDefault().setStatusText(text.toString());
    }

    private static String getKeyText(KeyStroke keyStroke) {
        if (keyStroke == null) {
            return "";
        }
        String modifText = KeyEvent.getKeyModifiersText(keyStroke.getModifiers());
        if ("".equals(modifText)) {
            return KeyEvent.getKeyText(keyStroke.getKeyCode());
        }
        return modifText + "+" + KeyEvent.getKeyText(keyStroke.getKeyCode());
    }

    public NbKeymap() {
        this.bindingsListener = new FileChangeAdapter(){

            public void fileDataCreated(FileEvent fe) {
                NbKeymap.this.refreshBindings();
            }

            public void fileAttributeChanged(FileAttributeEvent fe) {
                NbKeymap.this.refreshBindings();
            }

            public void fileChanged(FileEvent fe) {
                NbKeymap.this.refreshBindings();
            }

            public void fileRenamed(FileRenameEvent fe) {
                NbKeymap.this.refreshBindings();
            }

            public void fileDeleted(FileEvent fe) {
                NbKeymap.this.refreshBindings();
            }
        };
        context.clear();
    }

    @Override
    public Action getDefaultAction() {
        return null;
    }

    @Override
    public void setDefaultAction(Action a) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getName() {
        return "Default";
    }

    @Override
    public Action getAction(final KeyStroke key) {
        switch (key.getKeyCode()) {
            case 0: 
            case 16: 
            case 17: 
            case 18: 
            case 157: 
            case 65406: 
            case 65535: {
                return null;
            }
        }
        if (key.isOnKeyRelease()) {
            return null;
        }
        LOG.log(Level.FINE, "getAction {0}", key);
        Map<KeyStroke, Binding> binder = this.bindings();
        for (KeyStroke ctx : context) {
            Binding sub = binder.get(ctx);
            if (sub == null) {
                NbKeymap.resetContext();
                return BROKEN;
            }
            binder = sub.nested;
            if (binder != null) continue;
            NbKeymap.resetContext();
            return BROKEN;
        }
        Binding b = binder.get(key);
        if (b == null) {
            NbKeymap.resetContext();
            return null;
        }
        if (b.nested == null) {
            NbKeymap.resetContext();
            return b.loadAction();
        }
        return new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent e) {
                NbKeymap.shiftContext(key);
            }
        };
    }

    @Override
    public KeyStroke[] getBoundKeyStrokes() {
        assert (false);
        return null;
    }

    @Override
    public Action[] getBoundActions() {
        assert (false);
        return null;
    }

    @Override
    public KeyStroke[] getKeyStrokesForAction(Action a) {
        return new KeyStroke[0];
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    KeyStroke keyStrokeForAction(Action a, FileObject definingFile) {
        String id = NbKeymap.idForFile(definingFile);
        this.bindings();
        Map<Action, String> map = this.action2Id;
        synchronized (map) {
            this.action2Id.put(a, id);
            KeyStroke k = this.id2Stroke.get(id);
            LOG.log(Level.FINE, "found keystroke {0} for {1} with ID {2}", new Object[]{k, NbKeymap.id(a), id});
            return k;
        }
    }

    private static String idForFile(FileObject f) {
        if (f.hasExt("shadow")) {
            String path = (String)f.getAttribute("originalFile");
            if (path != null && f.getSize() == 0) {
                f = FileUtil.getConfigFile((String)path);
                if (f == null) {
                    return path;
                }
            } else {
                try {
                    DataObject d = DataObject.find((FileObject)f);
                    if (d instanceof DataShadow) {
                        f = ((DataShadow)d).getOriginal().getPrimaryFile();
                    }
                }
                catch (DataObjectNotFoundException x) {
                    LOG.log(Level.FINE, f.getPath(), (Throwable)x);
                }
            }
        }
        if (f.hasExt("instance") && !Collections.list(f.getAttributes()).contains("instanceCreate")) {
            String clazz = (String)f.getAttribute("instanceClass");
            if (clazz != null) {
                return clazz;
            }
            return f.getName().replace('-', '.');
        }
        return f.getPath();
    }

    @Override
    public synchronized boolean isLocallyDefined(KeyStroke key) {
        assert (false);
        return false;
    }

    @Override
    public int compare(KeyStroke k1, KeyStroke k2) {
        return KeyEvent.getKeyText(k1.getKeyCode()).length() - KeyEvent.getKeyText(k2.getKeyCode()).length();
    }

    @Override
    public void addActionForKeyStroke(KeyStroke key, Action a) {
        assert (false);
    }

    @Override
    public void removeKeyStrokeBinding(KeyStroke key) {
        assert (false);
    }

    @Override
    public void removeBindings() {
        assert (false);
    }

    @Override
    public Keymap getResolveParent() {
        return null;
    }

    @Override
    public void setResolveParent(Keymap parent) {
        throw new UnsupportedOperationException();
    }

    private static Object id(Action a) {
        if (a instanceof SystemAction) {
            return a.getClass();
        }
        return a;
    }

    public static final class AcceleratorBindingImpl
    extends AcceleratorBinding {
        protected KeyStroke keyStrokeForAction(Action action, FileObject definingFile) {
            Keymap km = (Keymap)Lookup.getDefault().lookup(Keymap.class);
            if (km instanceof NbKeymap) {
                return ((NbKeymap)km).keyStrokeForAction(action, definingFile);
            }
            LOG.log(Level.WARNING, "unexpected keymap: {0}", km);
            return null;
        }
    }

    private static class Binding {
        final FileObject actionDefinition;
        private Action action;
        final Map<KeyStroke, Binding> nested;

        Binding(FileObject def) {
            this.actionDefinition = def;
            this.nested = null;
        }

        Binding() {
            this.actionDefinition = null;
            this.nested = new HashMap<KeyStroke, Binding>();
        }

        synchronized Action loadAction() {
            assert (this.actionDefinition != null);
            if (this.action == null) {
                try {
                    DataObject d = DataObject.find((FileObject)this.actionDefinition);
                    InstanceCookie ic = (InstanceCookie)d.getLookup().lookup(InstanceCookie.class);
                    if (ic == null) {
                        return null;
                    }
                    this.action = (Action)ic.instanceCreate();
                }
                catch (Exception x) {
                    LOG.log(Level.INFO, "could not load action for " + this.actionDefinition.getPath(), x);
                }
            }
            if (this.action == null) {
                this.action = BROKEN;
            }
            return this.action;
        }
    }

}

