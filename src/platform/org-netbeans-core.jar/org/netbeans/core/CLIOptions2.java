/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.CLIHandler
 *  org.netbeans.CLIHandler$Args
 *  org.openide.modules.Dependency
 *  org.openide.modules.SpecificationVersion
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core;

import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.GraphicsEnvironment;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.netbeans.CLIHandler;
import org.netbeans.core.TimableEventQueue;
import org.openide.modules.Dependency;
import org.openide.modules.SpecificationVersion;
import org.openide.util.RequestProcessor;
import org.openide.windows.WindowManager;

public class CLIOptions2
extends CLIHandler
implements Runnable {
    private int cnt;
    private static final Logger LOG = Logger.getLogger(CLIOptions2.class.getName());
    private static final int EQ_TIMEOUT = 15000;
    private final RequestProcessor.Task task;
    static CLIOptions2 INSTANCE;

    public CLIOptions2() {
        super(2);
        INSTANCE = this;
        this.task = RequestProcessor.getDefault().create((Runnable)this);
    }

    protected int cli(CLIHandler.Args arguments) {
        return this.cli(arguments.getArguments());
    }

    final int cli(String[] args) {
        if (this.cnt++ == 0) {
            return 0;
        }
        if (!GraphicsEnvironment.isHeadless()) {
            LOG.fine("CLI running");
            SwingUtilities.invokeLater(this);
            this.task.schedule(15000);
        }
        return 0;
    }

    @Override
    public void run() {
        if (!EventQueue.isDispatchThread()) {
            this.eqStuck();
            return;
        }
        LOG.fine("running in EQ");
        this.task.cancel();
        CLIOptions2.frontMainWindow();
    }

    private void eqStuck() {
        Thread eq = TimableEventQueue.eq;
        if (eq == null) {
            LOG.warning("event queue thread not determined");
            return;
        }
        StackTraceElement[] stack = Thread.getAllStackTraces().get(eq);
        if (stack == null) {
            LOG.log(Level.WARNING, "no stack trace available for {0}", eq);
            return;
        }
        LOG.log(Level.INFO, "EQ stuck in " + eq, new EQStuck(stack));
        if (Dependency.JAVA_SPEC.compareTo((Object)new SpecificationVersion("1.7")) >= 0) {
            LOG.log(Level.WARNING, "#198918: will not hard restart EQ when running on JDK 7");
            eq.interrupt();
            return;
        }
        for (StackTraceElement line : stack) {
            if (!line.getMethodName().equals("<clinit>")) continue;
            LOG.log(Level.WARNING, "Will not hard restart EQ when inside a static initializer: {0}", line);
            eq.interrupt();
            return;
        }
        eq.stop();
    }

    private static void frontMainWindow() {
        Frame f = WindowManager.getDefault().getMainWindow();
        f.setVisible(true);
        if ((f.getExtendedState() & 1) != 0) {
            f.setExtendedState(-2 & f.getExtendedState());
        }
        f.toFront();
    }

    protected void usage(PrintWriter w) {
    }

    private static class EQStuck
    extends Throwable {
        EQStuck(StackTraceElement[] stack) {
            super("GUI is not responsive");
            this.setStackTrace(stack);
        }

        @Override
        public synchronized Throwable fillInStackTrace() {
            return this;
        }
    }

}

