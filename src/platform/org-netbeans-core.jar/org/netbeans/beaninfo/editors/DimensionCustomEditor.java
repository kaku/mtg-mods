/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import javax.accessibility.AccessibleContext;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.netbeans.beaninfo.editors.DimensionEditor;
import org.netbeans.beaninfo.editors.IntegerCustomEditor;
import org.netbeans.core.UIExceptions;
import org.openide.awt.Mnemonics;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.util.NbBundle;

public class DimensionCustomEditor
extends IntegerCustomEditor {
    static final long serialVersionUID = 3718340148720193844L;
    private JTextField heightField;
    private JLabel heightLabel;
    private JPanel insidePanel;
    private JLabel jLabel1;
    private JTextField widthField;
    private JLabel widthLabel;
    private DimensionEditor editor;

    public DimensionCustomEditor(DimensionEditor editor) {
        super(editor.env);
        this.initComponents();
        this.editor = editor;
        this.editor.env.setState(PropertyEnv.STATE_NEEDS_VALIDATION);
        this.editor.env.addPropertyChangeListener((PropertyChangeListener)this);
        Dimension dimension = (Dimension)editor.getValue();
        if (dimension == null) {
            dimension = new Dimension(0, 0);
        }
        this.jLabel1.setText(NbBundle.getMessage(DimensionCustomEditor.class, (String)"CTL_Dimension"));
        Mnemonics.setLocalizedText((JLabel)this.widthLabel, (String)NbBundle.getMessage(DimensionCustomEditor.class, (String)"CTL_Width"));
        this.widthLabel.setLabelFor(this.widthField);
        Mnemonics.setLocalizedText((JLabel)this.heightLabel, (String)NbBundle.getMessage(DimensionCustomEditor.class, (String)"CTL_Height"));
        this.heightLabel.setLabelFor(this.heightField);
        this.widthField.setText(String.valueOf(dimension.width));
        this.heightField.setText(String.valueOf(dimension.height));
        this.widthField.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DimensionCustomEditor.class, (String)"ACSD_CTL_Width"));
        this.heightField.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DimensionCustomEditor.class, (String)"ACSD_CTL_Height"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DimensionCustomEditor.class, (String)"ACSD_DimensionCustomEditor"));
        this.setPanel(this.insidePanel);
        this.widthField.addKeyListener(this);
        this.heightField.addKeyListener(this);
        this.getMap().put(this.widthField, this.widthLabel);
        this.getMap().put(this.heightField, this.heightLabel);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(280, 160);
    }

    private Object getPropertyValue() throws IllegalStateException {
        try {
            int width = Integer.parseInt(this.widthField.getText());
            int height = Integer.parseInt(this.heightField.getText());
            if (width < 0 || height < 0) {
                IllegalStateException ise = new IllegalStateException();
                UIExceptions.annotateUser(ise, null, NbBundle.getMessage(DimensionCustomEditor.class, (String)"CTL_NegativeSize"), null, null);
                throw ise;
            }
            return new Dimension(width, height);
        }
        catch (NumberFormatException e) {
            IllegalStateException ise = new IllegalStateException();
            UIExceptions.annotateUser(ise, null, NbBundle.getMessage(DimensionCustomEditor.class, (String)"CTL_InvalidValue"), null, null);
            throw ise;
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("state".equals(evt.getPropertyName()) && evt.getNewValue() == PropertyEnv.STATE_VALID) {
            this.editor.setValue(this.getPropertyValue());
        }
    }

    @Override
    protected void updateValues() {
        try {
            int width = Integer.parseInt(this.widthField.getText());
            int height = Integer.parseInt(this.heightField.getText());
            this.editor.setValue(new Dimension(width, height));
        }
        catch (NumberFormatException e) {
            // empty catch block
        }
    }

    private void initComponents() {
        this.insidePanel = new JPanel();
        this.jLabel1 = new JLabel();
        this.widthLabel = new JLabel();
        this.widthField = new JTextField();
        this.heightLabel = new JLabel();
        this.heightField = new JTextField();
        this.setLayout(new GridBagLayout());
        this.insidePanel.setLayout(new GridBagLayout());
        this.jLabel1.setText("jLabel1");
        this.jLabel1.setLabelFor(this.insidePanel);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(12, 12, 0, 0);
        this.insidePanel.add((Component)this.jLabel1, gridBagConstraints);
        this.widthLabel.setText("jLabel2");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(12, 17, 0, 0);
        this.insidePanel.add((Component)this.widthLabel, gridBagConstraints);
        this.widthField.setColumns(5);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(12, 5, 0, 12);
        this.insidePanel.add((Component)this.widthField, gridBagConstraints);
        this.heightLabel.setText("jLabel3");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(5, 17, 0, 0);
        this.insidePanel.add((Component)this.heightLabel, gridBagConstraints);
        this.heightField.setColumns(5);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(5, 5, 0, 12);
        this.insidePanel.add((Component)this.heightField, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.insidePanel, gridBagConstraints);
    }
}

