/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.ExPropertyEditor
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.explorer.propertysheet.editors.XMLPropertyEditor
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.beans.PropertyEditorSupport;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.StringTokenizer;
import org.netbeans.core.UIExceptions;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.editors.XMLPropertyEditor;
import org.openide.util.NbBundle;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

abstract class ArrayOfIntSupport
extends PropertyEditorSupport
implements XMLPropertyEditor,
ExPropertyEditor {
    private static final String VALUE_FORMAT = NbBundle.getBundle(ArrayOfIntSupport.class).getString("EXC_BadFormatValue");
    private int count;
    private String className;
    PropertyEnv env;
    public static final String ATTR_VALUE = "value";

    public ArrayOfIntSupport(String className, int count) {
        this.className = className;
        this.count = count;
    }

    public void attachEnv(PropertyEnv env) {
        this.env = env;
    }

    @Override
    public String getJavaInitializationString() {
        int[] val = this.getValues();
        StringBuffer buf = new StringBuffer("new ");
        buf.append(this.className);
        buf.append("(");
        this.addArray(buf, val);
        buf.append(")");
        return buf.toString();
    }

    abstract int[] getValues();

    abstract void setValues(int[] var1);

    @Override
    public String getAsText() {
        if (this.getValue() == null) {
            return "null";
        }
        int[] val = this.getValues();
        if (val == null) {
            return null;
        }
        StringBuffer buf = new StringBuffer("[");
        this.addArray(buf, val);
        buf.append("]");
        return buf.toString();
    }

    private void addArray(StringBuffer buf, int[] arr) {
        for (int i = 0; i < this.count; ++i) {
            if (arr == null) {
                buf.append("0");
            } else {
                buf.append(arr[i]);
            }
            if (i >= this.count - 1) continue;
            buf.append(", ");
        }
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if ("null".equals(text) || "".equals(text)) {
            this.setValue(null);
            return;
        }
        int[] newVal = new int[this.count];
        int nextNumber = 0;
        StringTokenizer tuk = new StringTokenizer(text, "[] ,;", false);
        while (tuk.hasMoreTokens()) {
            String token = tuk.nextToken();
            if (nextNumber >= this.count) {
                this.badFormat(null);
            }
            try {
                newVal[nextNumber] = new Integer(token);
                ++nextNumber;
            }
            catch (NumberFormatException e) {
                this.badFormat(e);
            }
        }
        if (nextNumber != this.count && nextNumber > 0) {
            int copyValue = newVal[nextNumber - 1];
            for (int i = nextNumber; i < this.count; ++i) {
                newVal[i] = copyValue;
            }
        }
        this.setValues(newVal);
    }

    private void badFormat(Exception e) throws IllegalArgumentException {
        String msg = new MessageFormat(VALUE_FORMAT).format(new Object[]{this.className, this.getHintFormat()});
        IllegalArgumentException iae = new IllegalArgumentException(msg);
        UIExceptions.annotateUser(iae, e == null ? "" : e.getMessage(), msg, e, new Date());
        throw iae;
    }

    String getHintFormat() {
        StringBuilder buf = new StringBuilder("[");
        for (int i = 0; i < this.count; ++i) {
            buf.append("<n");
            buf.append(i);
            buf.append(">");
            if (i >= this.count - 1) continue;
            buf.append(", ");
        }
        buf.append("]");
        return buf.toString();
    }

    protected abstract String getXMLValueTag();

    public void readFromXML(Node element) throws IOException {
        if (!this.getXMLValueTag().equals(element.getNodeName())) {
            throw new IOException();
        }
        NamedNodeMap attributes = element.getAttributes();
        try {
            String value = attributes.getNamedItem("value").getNodeValue();
            this.setAsText(value);
        }
        catch (Exception e) {
            throw new IOException();
        }
    }

    public Node storeToXML(Document doc) {
        Element el = doc.createElement(this.getXMLValueTag());
        el.setAttribute("value", this.getAsText());
        return el;
    }
}

