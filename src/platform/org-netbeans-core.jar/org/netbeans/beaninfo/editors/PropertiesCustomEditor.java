/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.accessibility.AccessibleContext;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.netbeans.beaninfo.editors.PropertiesEditor;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

public class PropertiesCustomEditor
extends JPanel
implements DocumentListener {
    private PropertiesEditor editor;
    private JEditorPane editorPane;
    private JTextField warnings;

    public PropertiesCustomEditor(PropertiesEditor ed) {
        this.editor = ed;
        this.initComponents();
        Properties props = (Properties)this.editor.getValue();
        if (props == null) {
            props = new Properties();
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            props.store(baos, "");
        }
        catch (IOException e) {
            throw new AssertionError(e);
        }
        try {
            this.editorPane.setText(baos.toString("ISO-8859-1").replaceAll("(?m)^#.*" + System.getProperty("line.separator"), ""));
        }
        catch (UnsupportedEncodingException x) {
            throw new AssertionError(x);
        }
        HelpCtx.setHelpIDString((JComponent)this, (String)PropertiesCustomEditor.class.getName());
        this.editorPane.getAccessibleContext().setAccessibleName(NbBundle.getBundle(PropertiesCustomEditor.class).getString("ACS_PropertiesEditorPane"));
        this.editorPane.getAccessibleContext().setAccessibleDescription(NbBundle.getBundle(PropertiesCustomEditor.class).getString("ACSD_PropertiesEditorPane"));
        this.editorPane.getDocument().addDocumentListener(this);
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getBundle(PropertiesCustomEditor.class).getString("ACSD_CustomPropertiesEditor"));
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        this.change();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        this.change();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    private void change() {
        Properties v = new Properties();
        boolean loaded = false;
        try {
            v.load(new ByteArrayInputStream(this.editorPane.getText().getBytes("ISO-8859-1")));
            loaded = true;
        }
        catch (Exception x) {
            Color c = UIManager.getColor("nb.errorForeground");
            if (c != null) {
                this.warnings.setForeground(c);
            }
            this.warnings.setText(x.toString());
        }
        if (loaded) {
            this.editor.setValue(v);
            if (Pattern.compile("^#", 8).matcher(this.editorPane.getText()).find()) {
                Color c = UIManager.getColor("nb.warningForeground");
                if (c != null) {
                    this.warnings.setForeground(c);
                }
                this.warnings.setText(NbBundle.getMessage(PropertiesCustomEditor.class, (String)"WARN_PropertiesComments"));
            } else {
                this.warnings.setText(null);
            }
        }
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(600, 400);
    }

    private void initComponents() {
        this.setLayout(new BorderLayout());
        this.editorPane = new JEditorPane();
        this.editorPane.setContentType("text/x-properties");
        this.editorPane.setPreferredSize(new Dimension(200, 100));
        this.add((Component)new JScrollPane(this.editorPane), "Center");
        this.warnings = new JTextField(30);
        this.warnings.setEditable(false);
        this.add((Component)this.warnings, "South");
    }
}

