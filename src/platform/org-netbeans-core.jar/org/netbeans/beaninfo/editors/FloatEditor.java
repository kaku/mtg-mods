/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.beaninfo.editors;

import org.netbeans.beaninfo.editors.WrappersEditor;

public class FloatEditor
extends WrappersEditor {
    public FloatEditor() {
        super(Float.TYPE);
    }

    public String getJavaInitializationString() {
        return "new java.lang.Float(" + this.getAsText() + "F)";
    }
}

