/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.beaninfo.editors;

import java.beans.PropertyEditor;
import org.netbeans.beaninfo.editors.WrappersEditor;

public class IntegerEditor
extends WrappersEditor {
    public IntegerEditor() {
        super(Integer.TYPE);
    }

    public String getJavaInitializationString() {
        return "new java.lang.Integer(" + this.pe.getJavaInitializationString() + ")";
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        super.setAsText(text.trim());
    }
}

