/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.awt.Component;
import java.beans.PropertyEditorSupport;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import org.netbeans.beaninfo.editors.PropertiesCustomEditor;
import org.netbeans.core.UIExceptions;
import org.openide.util.NbBundle;

public class PropertiesEditor
extends PropertyEditorSupport {
    @Override
    public String getAsText() {
        Object value = this.getValue();
        if (value instanceof Properties) {
            Properties prop = (Properties)value;
            StringBuilder buff = new StringBuilder();
            Enumeration e = prop.keys();
            while (e.hasMoreElements()) {
                if (buff.length() > 0) {
                    buff.append("; ");
                }
                Object key = e.nextElement();
                buff.append(key).append('=').append(prop.get(key));
            }
            return buff.toString();
        }
        return String.valueOf(value);
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        try {
            if (text == null) {
                throw new IllegalArgumentException("Inserted value can't be null.");
            }
            Properties prop = new Properties();
            ByteArrayInputStream is = new ByteArrayInputStream(text.replace(';', '\n').getBytes("ISO8859_1"));
            prop.load(is);
            this.setValue(prop);
        }
        catch (IOException ioe) {
            IllegalArgumentException iae = new IllegalArgumentException(ioe.getMessage());
            String msg = ioe.getLocalizedMessage();
            if (msg == null) {
                msg = MessageFormat.format(NbBundle.getMessage(PropertiesEditor.class, (String)"FMT_EXC_GENERIC_BAD_VALUE"), text);
            }
            UIExceptions.annotateUser(iae, iae.getMessage(), msg, ioe, new Date());
            throw iae;
        }
    }

    @Override
    public String getJavaInitializationString() {
        return null;
    }

    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

    @Override
    public Component getCustomEditor() {
        return new PropertiesCustomEditor(this);
    }
}

