/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.beaninfo.editors;

import org.netbeans.beaninfo.editors.WrappersEditor;

public class LongEditor
extends WrappersEditor {
    public LongEditor() {
        super(Long.TYPE);
    }

    public String getJavaInitializationString() {
        return "new java.lang.Long(" + this.getAsText() + "L)";
    }
}

