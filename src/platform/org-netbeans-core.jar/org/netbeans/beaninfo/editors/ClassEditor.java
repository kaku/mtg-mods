/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.beans.PropertyEditorSupport;
import java.text.MessageFormat;
import java.util.Date;
import org.netbeans.core.UIExceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class ClassEditor
extends PropertyEditorSupport {
    @Override
    public String getJavaInitializationString() {
        Class clazz = (Class)this.getValue();
        if (clazz == null) {
            return "null";
        }
        return "Class.forName (\"" + clazz.getName() + "\")";
    }

    @Override
    public String getAsText() {
        Class clazz = (Class)this.getValue();
        if (clazz == null) {
            return "null";
        }
        return clazz.getName();
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        try {
            ClassLoader loader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
            this.setValue(loader.loadClass(text));
        }
        catch (ClassNotFoundException e) {
            IllegalArgumentException iae = new IllegalArgumentException(e.getMessage());
            String msg = MessageFormat.format(NbBundle.getMessage(ClassEditor.class, (String)"FMT_EXC_CANT_LOAD_CLASS"), text);
            UIExceptions.annotateUser(iae, e.getMessage(), msg, e, new Date());
            throw iae;
        }
    }
}

