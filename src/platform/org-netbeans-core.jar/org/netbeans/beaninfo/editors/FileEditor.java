/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.ExPropertyEditor
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.beans.PropertyEditorSupport;
import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JScrollBar;
import javax.swing.KeyStroke;
import org.netbeans.beaninfo.editors.StringCustomEditor;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

public class FileEditor
extends PropertyEditorSupport
implements ExPropertyEditor,
PropertyChangeListener {
    static final String PROPERTY_SHOW_DIRECTORIES = "directories";
    static final String PROPERTY_SHOW_FILES = "files";
    static final String PROPERTY_FILTER = "filter";
    static final String PROPERTY_CURRENT_DIR = "currentDir";
    static final String PROPERTY_BASE_DIR = "baseDir";
    static final String PROPERTY_FILE_HIDING = "file_hiding";
    private int mode = 2;
    private boolean directories = true;
    private boolean files = true;
    private boolean fileHiding = false;
    private javax.swing.filechooser.FileFilter fileFilter;
    private File currentDirectory;
    private File baseDirectory;
    static File lastCurrentDir;
    private PropertyEnv env;
    private JFileChooser chooser;
    private boolean editable = true;

    public void attachEnv(PropertyEnv env) {
        Object fil;
        Object baseDir;
        Object filter;
        this.env = env;
        this.directories = true;
        this.files = true;
        this.fileFilter = null;
        this.fileHiding = false;
        Object dirs = env.getFeatureDescriptor().getValue("directories");
        if (dirs instanceof Boolean) {
            this.directories = (Boolean)dirs;
        }
        if ((fil = env.getFeatureDescriptor().getValue("files")) instanceof Boolean) {
            this.files = (Boolean)fil;
        }
        if ((filter = env.getFeatureDescriptor().getValue("filter")) instanceof FilenameFilter) {
            this.fileFilter = new DelegatingFilenameFilter((FilenameFilter)filter);
        } else if (filter instanceof javax.swing.filechooser.FileFilter) {
            this.fileFilter = (javax.swing.filechooser.FileFilter)filter;
        } else if (filter instanceof FileFilter) {
            this.fileFilter = new DelegatingFileFilter((FileFilter)filter);
        }
        Object curDir = env.getFeatureDescriptor().getValue("currentDir");
        if (curDir instanceof File) {
            this.currentDirectory = (File)curDir;
            if (!this.currentDirectory.isDirectory()) {
                Logger.getAnonymousLogger().warning("java.io.File will not accept currentDir=" + this.currentDirectory);
                this.currentDirectory = null;
            }
        }
        if ((baseDir = env.getFeatureDescriptor().getValue("baseDir")) instanceof File) {
            this.baseDirectory = (File)baseDir;
            if (!this.baseDirectory.isDirectory() || !this.baseDirectory.isAbsolute()) {
                Logger.getAnonymousLogger().warning("java.io.File will not accept baseDir=" + this.baseDirectory);
                this.baseDirectory = null;
            }
        }
        this.mode = this.files ? (this.directories ? 2 : 0) : (this.directories ? 1 : 2);
        Object fileHide = env.getFeatureDescriptor().getValue("file_hiding");
        if (fileHide instanceof Boolean) {
            this.fileHiding = (Boolean)fileHide;
        }
        if (env.getFeatureDescriptor() instanceof Node.Property) {
            Node.Property prop = (Node.Property)env.getFeatureDescriptor();
            this.editable = prop.canWrite();
        }
    }

    @Override
    public String getAsText() {
        File file = (File)this.getValue();
        if (file == null) {
            return "";
        }
        String path = file.getPath();
        if ("".equals(path)) {
            path = ".";
        }
        return path;
    }

    @Override
    public void setAsText(String str) throws IllegalArgumentException {
        if (str == null) {
            throw new IllegalArgumentException("null");
        }
        if ("".equals(str)) {
            this.setValue(null);
            return;
        }
        if (".".equals(str)) {
            str = "";
        }
        this.setValue(new File(str));
    }

    @Override
    public Component getCustomEditor() {
        if (!this.editable) {
            String info = "";
            Object curVal = this.getValue();
            if (curVal instanceof File) {
                info = ((File)curVal).getAbsolutePath();
            }
            return new StringCustomEditor(info, false, true, null, this, this.env);
        }
        if (this.chooser == null) {
            this.chooser = FileEditor.createHackedFileChooser();
            File originalFile = (File)this.getValue();
            if (originalFile != null && !originalFile.isAbsolute() && this.baseDirectory != null) {
                originalFile = new File(this.baseDirectory, originalFile.getPath());
            }
            if (this.currentDirectory != null) {
                this.chooser.setCurrentDirectory(this.currentDirectory);
            } else if (originalFile != null && originalFile.getParentFile() != null) {
                this.chooser.setCurrentDirectory(originalFile.getParentFile());
                this.chooser.setSelectedFile(originalFile);
            } else if (lastCurrentDir != null) {
                this.chooser.setCurrentDirectory(lastCurrentDir);
            }
            this.chooser.setFileSelectionMode(this.mode);
            if (this.fileFilter != null) {
                this.chooser.setFileFilter(this.fileFilter);
            }
            switch (this.mode) {
                case 2: {
                    this.chooser.setDialogTitle(FileEditor.getString("CTL_DialogTitleFilesAndDirs"));
                    break;
                }
                case 0: {
                    this.chooser.setDialogTitle(FileEditor.getString("CTL_DialogTitleFiles"));
                    break;
                }
                case 1: {
                    this.chooser.setDialogTitle(FileEditor.getString("CTL_DialogTitleDirs"));
                }
            }
            this.chooser.setFileHidingEnabled(this.fileHiding);
            this.chooser.setControlButtonsAreShown(false);
            this.chooser.addPropertyChangeListener("SelectedFileChangedProperty", this);
            HelpCtx.setHelpIDString((JComponent)this.chooser, (String)this.getHelpCtx().getHelpID());
        }
        return this.chooser;
    }

    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

    @Override
    public String getJavaInitializationString() {
        File value = (File)this.getValue();
        if (value == null) {
            return "null";
        }
        if (this.baseDirectory != null && !value.isAbsolute()) {
            return "new java.io.File(" + FileEditor.stringify(this.baseDirectory.getPath()) + ", " + FileEditor.stringify(value.getPath()) + ")";
        }
        return "new java.io.File(" + FileEditor.stringify(value.getAbsolutePath()) + ")";
    }

    static String stringify(String in) {
        StringBuilder buf = new StringBuilder(in.length() * 2 + 2);
        buf.append('\"');
        for (int i = 0; i < in.length(); ++i) {
            char c = in.charAt(i);
            if (c == '\\' || c == '\"') {
                buf.append('\\');
            }
            buf.append(c);
        }
        buf.append('\"');
        return buf.toString();
    }

    private HelpCtx getHelpCtx() {
        return new HelpCtx(FileEditor.class);
    }

    private static String getString(String key) {
        return NbBundle.getBundle(FileEditor.class).getString(key);
    }

    static String getChildRelativePath(File baseDir, File file) {
        if (file.equals(baseDir)) {
            return "";
        }
        StringBuilder buf = new StringBuilder(file.getPath().length());
        buf.append(file.getName());
        for (File parent = file.getParentFile(); parent != null; parent = parent.getParentFile()) {
            if (parent.equals(baseDir)) {
                return buf.toString();
            }
            buf.insert(0, File.separatorChar);
            buf.insert(0, parent.getName());
        }
        return null;
    }

    @Override
    public void propertyChange(PropertyChangeEvent e) {
        String rel;
        JFileChooser chooser = (JFileChooser)e.getSource();
        File f = chooser.getSelectedFile();
        if (f == null) {
            return;
        }
        if (!this.files && f.isFile()) {
            return;
        }
        if (!this.directories && f.isDirectory()) {
            return;
        }
        if (this.baseDirectory != null && (rel = FileEditor.getChildRelativePath(this.baseDirectory, f)) != null) {
            f = new File(rel);
        }
        this.setValue(new File(f.getPath()));
        lastCurrentDir = chooser.getCurrentDirectory();
    }

    public static JFileChooser createHackedFileChooser() {
        JFileChooser chooser = new JFileChooser();
        FileEditor.hackFileChooser(chooser);
        return chooser;
    }

    public static void hackFileChooser(final JFileChooser chooser) {
        chooser.getAccessibleContext().setAccessibleDescription(FileEditor.getString("ACSD_FileEditor"));
        final Object key = chooser.getInputMap(1).get(KeyStroke.getKeyStroke(27, 0));
        AbstractAction close = new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent ae) {
                Action a;
                Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
                if (key != null && (a = chooser.getActionMap().get(key)) != null) {
                    a.actionPerformed(ae);
                }
                if (comp.getParent() == null) {
                    return;
                }
                Container c = chooser.getTopLevelAncestor();
                if (c instanceof Dialog && ((Dialog)c).isVisible()) {
                    ((Dialog)c).setVisible(false);
                    ((Dialog)c).dispose();
                }
            }
        };
        chooser.getInputMap(1).put(KeyStroke.getKeyStroke(27, 0), "close");
        chooser.getActionMap().put("close", close);
    }

    static class DelegatingFilenameFilter
    extends javax.swing.filechooser.FileFilter {
        private FilenameFilter filter;

        public DelegatingFilenameFilter(FilenameFilter f) {
            this.filter = f;
        }

        @Override
        public boolean accept(File f) {
            return this.filter.accept(f.getParentFile(), f.getName());
        }

        @Override
        public String getDescription() {
            return null;
        }
    }

    static class DelegatingFileFilter
    extends javax.swing.filechooser.FileFilter {
        private FileFilter filter;

        public DelegatingFileFilter(FileFilter f) {
            this.filter = f;
        }

        @Override
        public boolean accept(File f) {
            return this.filter.accept(f);
        }

        @Override
        public String getDescription() {
            return null;
        }
    }

    private static class ButtonHider
    implements PropertyChangeListener {
        private ButtonHider() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent pce) {
            if ("ControlButtonsAreShownChangedProperty".equals(pce.getPropertyName())) {
                JFileChooser jfc = (JFileChooser)pce.getSource();
                try {
                    this.hideShowButtons(jfc, Boolean.TRUE.equals(pce.getNewValue()));
                }
                catch (Exception e) {
                    Logger.getLogger(FileEditor.class.getName()).log(Level.WARNING, null, e);
                }
            }
        }

        private void hideShowButtons(Container cont, boolean val) {
            if (cont instanceof JComboBox || cont instanceof JScrollBar) {
                return;
            }
            Component[] c = cont.getComponents();
            for (int i = 0; i < c.length; ++i) {
                if (c[i] instanceof Container) {
                    this.hideShowButtons((Container)c[i], val);
                }
                if (!(c[i] instanceof AbstractButton)) continue;
                c[i].setVisible(val);
            }
        }
    }

}

