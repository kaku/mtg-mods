/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$Factory
 *  org.openide.cookies.InstanceCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$Status
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.beans.PropertyEditorSupport;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Set;
import org.netbeans.core.UIExceptions;
import org.openide.awt.HtmlBrowser;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class HtmlBrowser {

    public static class FactoryEditor
    extends PropertyEditorSupport {
        private static final String EA_HIDDEN = "hidden";
        private static final String BROWSER_FOLDER = "Services/Browsers";

        @Override
        public String getAsText() {
            try {
                HtmlBrowser.Factory f = (HtmlBrowser.Factory)this.getValue();
                Lookup.Item i = Lookup.getDefault().lookupItem(new Lookup.Template(HtmlBrowser.Factory.class, null, (Object)f));
                if (i != null) {
                    return i.getDisplayName();
                }
            }
            catch (Exception ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            return NbBundle.getMessage(FactoryEditor.class, (String)"CTL_UnspecifiedBrowser");
        }

        @Override
        public void setAsText(String str) throws IllegalArgumentException {
            try {
                if (NbBundle.getMessage(FactoryEditor.class, (String)"CTL_UnspecifiedBrowser").equals(str) || str == null) {
                    this.setValue(null);
                    return;
                }
                Lookup.Result r = Lookup.getDefault().lookupResult(HtmlBrowser.Factory.class);
                for (Lookup.Item i : r.allItems()) {
                    if (!str.equals(i.getDisplayName())) continue;
                    this.setValue(i.getInstance());
                    return;
                }
            }
            catch (Exception e) {
                IllegalArgumentException iae = new IllegalArgumentException(e.getMessage());
                String msg = e.getLocalizedMessage();
                if (msg == null) {
                    msg = MessageFormat.format(NbBundle.getMessage(HtmlBrowser.class, (String)"FMT_EXC_GENERIC_BAD_VALUE"), str);
                }
                UIExceptions.annotateUser(iae, str, msg, e, new Date());
                throw iae;
            }
        }

        @Override
        public String[] getTags() {
            ArrayList<String> list = new ArrayList<String>(6);
            Lookup.Result r = Lookup.getDefault().lookupResult(HtmlBrowser.Factory.class);
            for (Lookup.Item i : r.allItems()) {
                list.add(i.getDisplayName());
            }
            FileObject fo = FileUtil.getConfigFile((String)"Services/Browsers");
            if (fo != null) {
                DataFolder folder = DataFolder.findFolder((FileObject)fo);
                DataObject[] dobjs = folder.getChildren();
                for (int i2 = 0; i2 < dobjs.length; ++i2) {
                    if (!Boolean.TRUE.equals(dobjs[i2].getPrimaryFile().getAttribute("hidden")) && dobjs[i2].getCookie(InstanceCookie.class) != null) continue;
                    FileObject fo2 = dobjs[i2].getPrimaryFile();
                    String n = fo2.getName();
                    try {
                        n = fo2.getFileSystem().getStatus().annotateName(n, dobjs[i2].files());
                    }
                    catch (FileStateInvalidException e) {
                        // empty catch block
                    }
                    list.remove(n);
                }
            }
            String[] retValue = new String[list.size()];
            list.toArray(retValue);
            return retValue;
        }
    }

}

