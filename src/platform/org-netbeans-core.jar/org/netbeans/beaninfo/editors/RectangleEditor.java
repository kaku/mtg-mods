/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.awt.Component;
import java.awt.Rectangle;
import java.io.IOException;
import java.util.Date;
import org.netbeans.beaninfo.editors.ArrayOfIntSupport;
import org.netbeans.beaninfo.editors.DimensionEditor;
import org.netbeans.beaninfo.editors.RectangleCustomEditor;
import org.netbeans.core.UIExceptions;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.util.NbBundle;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class RectangleEditor
extends ArrayOfIntSupport {
    private PropertyEnv env;

    public RectangleEditor() {
        super("java.awt.Rectangle", 4);
    }

    @Override
    int[] getValues() {
        Rectangle rect = (Rectangle)this.getValue();
        return new int[]{rect.x, rect.y, rect.width, rect.height};
    }

    @Override
    void setValues(int[] val) {
        if (val[0] < 0 || val[1] < 0 || val[2] < 0 || val[3] < 0) {
            String msg = NbBundle.getMessage(DimensionEditor.class, (String)"CTL_NegativeSize");
            IllegalArgumentException iae = new IllegalArgumentException("Negative value");
            UIExceptions.annotateUser(iae, iae.getMessage(), msg, null, new Date());
            throw iae;
        }
        this.setValue(new Rectangle(val[0], val[1], val[2], val[3]));
    }

    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

    @Override
    public Component getCustomEditor() {
        return new RectangleCustomEditor(this, this.env);
    }

    @Override
    String getHintFormat() {
        return NbBundle.getMessage(RectangleEditor.class, (String)"CTL_HintFormatRE");
    }

    @Override
    protected String getXMLValueTag() {
        return "Rectangle";
    }

    @Override
    public void attachEnv(PropertyEnv env) {
        this.env = env;
    }
}

