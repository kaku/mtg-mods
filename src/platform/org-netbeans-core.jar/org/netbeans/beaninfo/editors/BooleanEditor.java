/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.beaninfo.editors;

import org.netbeans.beaninfo.editors.WrappersEditor;

public class BooleanEditor
extends WrappersEditor {
    public BooleanEditor() {
        super(Boolean.TYPE);
    }

    public String getJavaInitializationString() {
        Boolean val = (Boolean)this.getValue();
        return Boolean.TRUE.equals(val) ? "java.lang.Boolean.TRUE" : "java.lang.Boolean.FALSE";
    }
}

