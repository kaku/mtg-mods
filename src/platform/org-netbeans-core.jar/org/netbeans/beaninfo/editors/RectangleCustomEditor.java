/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.ResourceBundle;
import javax.accessibility.AccessibleContext;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import org.netbeans.beaninfo.editors.IntegerCustomEditor;
import org.netbeans.beaninfo.editors.RectangleEditor;
import org.netbeans.core.UIExceptions;
import org.openide.awt.Mnemonics;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.util.NbBundle;

public class RectangleCustomEditor
extends IntegerCustomEditor {
    static final long serialVersionUID = -9015667991684634296L;
    private JPanel jPanel2;
    private JLabel xLabel;
    private JTextField xField;
    private JLabel yLabel;
    private JTextField yField;
    private JLabel widthLabel;
    private JTextField widthField;
    private JLabel heightLabel;
    private JTextField heightField;
    private RectangleEditor editor;

    public RectangleCustomEditor(RectangleEditor editor, PropertyEnv env) {
        super(env);
        this.initComponents();
        this.editor = editor;
        Rectangle rectangle = (Rectangle)editor.getValue();
        if (rectangle == null) {
            rectangle = new Rectangle(0, 0, 0, 0);
        }
        this.xField.setText(Integer.toString(rectangle.x));
        this.yField.setText(Integer.toString(rectangle.y));
        this.widthField.setText(Integer.toString(rectangle.width));
        this.heightField.setText(Integer.toString(rectangle.height));
        ResourceBundle b = NbBundle.getBundle(RectangleCustomEditor.class);
        this.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));
        this.jPanel2.setBorder(new CompoundBorder(new TitledBorder(new EtchedBorder(), " " + b.getString("CTL_Rectangle") + " "), new EmptyBorder(new Insets(5, 5, 5, 5))));
        Mnemonics.setLocalizedText((JLabel)this.xLabel, (String)b.getString("CTL_X"));
        Mnemonics.setLocalizedText((JLabel)this.yLabel, (String)b.getString("CTL_Y"));
        Mnemonics.setLocalizedText((JLabel)this.widthLabel, (String)b.getString("CTL_Width"));
        Mnemonics.setLocalizedText((JLabel)this.heightLabel, (String)b.getString("CTL_Height"));
        this.xLabel.setLabelFor(this.xField);
        this.yLabel.setLabelFor(this.yField);
        this.widthLabel.setLabelFor(this.widthField);
        this.heightLabel.setLabelFor(this.heightField);
        this.xField.getAccessibleContext().setAccessibleDescription(b.getString("ACSD_CTL_X"));
        this.yField.getAccessibleContext().setAccessibleDescription(b.getString("ACSD_CTL_Y"));
        this.widthField.getAccessibleContext().setAccessibleDescription(b.getString("ACSD_CTL_Width"));
        this.heightField.getAccessibleContext().setAccessibleDescription(b.getString("ACSD_CTL_Height"));
        this.getAccessibleContext().setAccessibleDescription(b.getString("ACSD_CustomRectangleEditor"));
        this.setPanel(this.jPanel2);
        this.getMap().put(this.widthField, this.widthLabel);
        this.getMap().put(this.xField, this.xLabel);
        this.getMap().put(this.yField, this.yLabel);
        this.getMap().put(this.heightField, this.heightLabel);
        env.setState(PropertyEnv.STATE_NEEDS_VALIDATION);
        env.addPropertyChangeListener((PropertyChangeListener)this);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(280, 160);
    }

    private Object getPropertyValue() throws IllegalStateException {
        try {
            int x = Integer.parseInt(this.xField.getText());
            int y = Integer.parseInt(this.yField.getText());
            int width = Integer.parseInt(this.widthField.getText());
            int height = Integer.parseInt(this.heightField.getText());
            if (x < 0 || y < 0 || width < 0 || height < 0) {
                IllegalStateException ise = new IllegalStateException();
                UIExceptions.annotateUser(ise, null, NbBundle.getMessage(RectangleCustomEditor.class, (String)"CTL_NegativeSize"), null, null);
                throw ise;
            }
            return new Rectangle(x, y, width, height);
        }
        catch (NumberFormatException e) {
            IllegalStateException ise = new IllegalStateException();
            UIExceptions.annotateUser(ise, null, NbBundle.getMessage(RectangleCustomEditor.class, (String)"CTL_InvalidValue"), null, null);
            throw ise;
        }
    }

    private void initComponents() {
        this.setLayout(new BorderLayout());
        this.jPanel2 = new JPanel();
        this.jPanel2.setLayout(new GridBagLayout());
        this.xLabel = new JLabel();
        this.xLabel.setText(null);
        GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.anchor = 17;
        this.jPanel2.add((Component)this.xLabel, gridBagConstraints1);
        this.xField = new JTextField();
        this.xField.addKeyListener(this);
        gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.gridwidth = 0;
        gridBagConstraints1.fill = 2;
        gridBagConstraints1.insets = new Insets(4, 8, 4, 0);
        gridBagConstraints1.weightx = 1.0;
        this.jPanel2.add((Component)this.xField, gridBagConstraints1);
        this.yLabel = new JLabel();
        this.yLabel.setText(null);
        gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.anchor = 17;
        this.jPanel2.add((Component)this.yLabel, gridBagConstraints1);
        this.yField = new JTextField();
        this.yField.addKeyListener(this);
        gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.gridwidth = 0;
        gridBagConstraints1.fill = 2;
        gridBagConstraints1.insets = new Insets(4, 8, 4, 0);
        gridBagConstraints1.weightx = 1.0;
        this.jPanel2.add((Component)this.yField, gridBagConstraints1);
        this.widthLabel = new JLabel();
        this.widthLabel.setText(null);
        gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.anchor = 17;
        this.jPanel2.add((Component)this.widthLabel, gridBagConstraints1);
        this.widthField = new JTextField();
        this.widthField.addKeyListener(this);
        gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.gridwidth = 0;
        gridBagConstraints1.fill = 2;
        gridBagConstraints1.insets = new Insets(4, 8, 4, 0);
        gridBagConstraints1.weightx = 1.0;
        this.jPanel2.add((Component)this.widthField, gridBagConstraints1);
        this.heightLabel = new JLabel();
        this.heightLabel.setText(null);
        gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.anchor = 17;
        this.jPanel2.add((Component)this.heightLabel, gridBagConstraints1);
        this.heightField = new JTextField();
        this.heightField.addKeyListener(this);
        gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.gridwidth = 0;
        gridBagConstraints1.fill = 2;
        gridBagConstraints1.insets = new Insets(4, 8, 4, 0);
        gridBagConstraints1.weightx = 1.0;
        this.jPanel2.add((Component)this.heightField, gridBagConstraints1);
        this.add((Component)this.jPanel2, "Center");
    }

    @Override
    protected void updateValues() {
        try {
            int x = Integer.parseInt(this.xField.getText());
            int y = Integer.parseInt(this.yField.getText());
            int width = Integer.parseInt(this.widthField.getText());
            int height = Integer.parseInt(this.heightField.getText());
            this.editor.setValue(new Rectangle(x, y, width, height));
        }
        catch (NumberFormatException e) {
            // empty catch block
        }
    }

    private JLabel findLabelFor(JTextField c) {
        return this.getMap().get(c);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("state".equals(evt.getPropertyName()) && evt.getNewValue() == PropertyEnv.STATE_VALID) {
            this.editor.setValue(this.getPropertyValue());
        }
    }
}

