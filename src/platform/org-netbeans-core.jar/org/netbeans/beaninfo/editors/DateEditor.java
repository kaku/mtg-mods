/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.beaninfo.editors;

import java.beans.PropertyEditorSupport;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

public class DateEditor
extends PropertyEditorSupport {
    private static DateFormat fmt = DateFormat.getDateTimeInstance();

    @Override
    public String getAsText() {
        Date d = (Date)this.getValue();
        if (d != null) {
            return fmt.format(d);
        }
        return "";
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if ("".equals(text)) {
            this.setValue(null);
        } else {
            try {
                this.setValue(fmt.parse(text));
            }
            catch (ParseException e) {
                throw (IllegalArgumentException)new IllegalArgumentException(e.toString()).initCause(e);
            }
        }
    }

    @Override
    public String getJavaInitializationString() {
        return "new java.util.Date(" + ((Date)this.getValue()).getTime() + "L)";
    }
}

