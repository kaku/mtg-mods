/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.ExPropertyEditor
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.beaninfo.editors;

import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.BeanDescriptor;
import java.beans.FeatureDescriptor;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyEditorSupport;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import org.netbeans.core.UIExceptions;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public final class ObjectEditor
extends PropertyEditorSupport
implements ExPropertyEditor {
    private static final String PROP_SUPERCLASS = "superClass";
    static final String PROP_NULL = "nullValue";
    private static final String PROP_LOOKUP = "lookup";
    private ObjectPanel customEditor;
    private Lookup.Template<Object> template;
    private String nullValue;
    private Lookup lookup;

    public synchronized void attachEnv(PropertyEnv env) {
        Object obj = env.getFeatureDescriptor().getValue("superClass");
        if (obj instanceof Class) {
            Class clz = (Class)obj;
            this.template = new Lookup.Template(clz);
        } else {
            this.template = null;
        }
        obj = env.getFeatureDescriptor().getValue("nullValue");
        this.nullValue = Boolean.TRUE.equals(obj) ? NbBundle.getMessage(ObjectEditor.class, (String)"CTL_NullValue") : (obj instanceof String ? (String)obj : null);
        obj = env.getFeatureDescriptor().getValue("lookup");
        Lookup lookup = this.lookup = obj instanceof Lookup ? (Lookup)obj : null;
        if (this.getTags() == null || this.getTags().length <= 1) {
            env.getFeatureDescriptor().setValue("canEditAsText", Boolean.FALSE);
        }
    }

    protected Lookup lookup() {
        Lookup l = this.lookup;
        return l == null ? Lookup.getDefault() : l;
    }

    protected Lookup.Template<Object> template() {
        if (this.template == null) {
            this.template = new Lookup.Template(Object.class);
        }
        return this.template;
    }

    @Override
    public String getAsText() {
        Object value = this.getValue();
        if (value == null) {
            return this.nullValue == null ? NbBundle.getMessage(ObjectEditor.class, (String)"CTL_NullValue") : this.nullValue;
        }
        Lookup.Template t = new Lookup.Template(this.template().getType(), this.template().getId(), value);
        Lookup.Item item = this.lookup().lookupItem(t);
        if (item == null) {
            return NbBundle.getMessage(ObjectEditor.class, (String)"CTL_NullItem");
        }
        return item.getDisplayName();
    }

    @Override
    public void setAsText(String str) throws IllegalArgumentException {
        if (this.nullValue != null && this.nullValue.equals(str)) {
            this.setValue(null);
            return;
        }
        Collection allItems = this.lookup().lookup(this.template()).allItems();
        for (Lookup.Item item : allItems) {
            if (!item.getDisplayName().equals(str)) continue;
            this.setValue(item.getInstance());
            this.firePropertyChange();
            return;
        }
        IllegalArgumentException iae = new IllegalArgumentException(str);
        String msg = MessageFormat.format(NbBundle.getMessage(ObjectEditor.class, (String)"FMT_EXC_GENERIC_BAD_VALUE"), str);
        UIExceptions.annotateUser(iae, str, msg, null, new Date());
        throw iae;
    }

    @Override
    public String[] getTags() {
        Collection allItems = this.lookup().lookup(this.template()).allItems();
        if (allItems.size() <= 1) {
            return null;
        }
        ArrayList<String> list = new ArrayList<String>(allItems.size() + 1);
        if (this.nullValue != null) {
            list.add(this.nullValue);
        }
        for (Lookup.Item item : allItems) {
            list.add(item.getDisplayName());
        }
        String[] retValue = new String[list.size()];
        list.toArray(retValue);
        return retValue;
    }

    @Override
    public boolean supportsCustomEditor() {
        return this.getTags() != null && this.getTags().length > 1;
    }

    @Override
    public synchronized Component getCustomEditor() {
        ObjectPanel panel;
        if (!this.supportsCustomEditor()) {
            return null;
        }
        if (this.customEditor != null) {
            return this.customEditor;
        }
        Lookup.Result contents = this.lookup().lookup(this.template());
        this.customEditor = panel = new ObjectPanel(contents);
        return this.customEditor;
    }

    private static class ItemRadioButton
    extends JRadioButton {
        static final long serialVersionUID = 3;
        Lookup.Item item;

        public ItemRadioButton(Lookup.Item item, Font font) {
            this.item = item;
            this.setName(item.getId());
            this.setText(item.getDisplayName());
            this.setFont(font);
            this.getAccessibleContext().setAccessibleName(this.getName());
            this.getAccessibleContext().setAccessibleDescription(this.getText());
        }
    }

    private class ObjectPanel
    extends JPanel
    implements ActionListener {
        static final long serialVersionUID = 1;

        public ObjectPanel(Lookup.Result<Object> res) {
            Font bold;
            Font plain;
            this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ObjectEditor.class, (String)"ACSN_ObjectTree"));
            this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ObjectEditor.class, (String)"ACSD_ObjectTree"));
            this.setLayout(new GridBagLayout());
            GridBagConstraints gbc = new GridBagConstraints();
            int row = 0;
            ButtonGroup bg = new ButtonGroup();
            if (Utilities.isMac()) {
                bold = new Font(this.getFont().getName(), 1, this.getFont().getSize());
                plain = new Font(this.getFont().getName(), 0, this.getFont().getSize());
            } else {
                bold = this.getFont().deriveFont(1);
                plain = this.getFont().deriveFont(0);
            }
            Collection c = res.allItems();
            Lookup.Item[] items = new Lookup.Item[c.size()];
            items = c.toArray((T[])items);
            int BASE_LEFT_INSET = 7;
            for (int i = 0; i < items.length; ++i) {
                ItemRadioButton rb = new ItemRadioButton(items[i], bold);
                Object inst = items[i].getInstance();
                if (inst != null && inst.equals(ObjectEditor.this.getValue())) {
                    rb.setSelected(true);
                }
                rb.addActionListener(this);
                bg.add(rb);
                String description = this.getDescription(items[i]);
                gbc.gridx = 0;
                gbc.gridy = row;
                gbc.insets = new Insets(i == 0 ? 7 : 0, BASE_LEFT_INSET, description != null ? 1 : (i == items.length - 1 ? 7 : 4), BASE_LEFT_INSET);
                gbc.fill = 2;
                this.add((Component)rb, gbc);
                ++row;
                if (description == null) continue;
                JLabel lbl = new JLabel(description);
                lbl.setLabelFor(rb);
                lbl.setFont(plain);
                int left = rb.getIcon() != null ? rb.getIcon().getIconWidth() : 20;
                gbc.insets = new Insets(0, BASE_LEFT_INSET + left, 4, BASE_LEFT_INSET + left);
                gbc.gridx = 0;
                gbc.gridy = row++;
                this.add((Component)lbl, gbc);
            }
        }

        private String getDescription(Lookup.Item item) {
            String id = item.getId();
            String result = null;
            try {
                result = Introspector.getBeanInfo(item.getInstance().getClass()).getBeanDescriptor().getShortDescription();
            }
            catch (IntrospectionException ie) {
                // empty catch block
            }
            String toCheck = item.getInstance().getClass().getName();
            String string = toCheck = toCheck.lastIndexOf(46) != -1 ? toCheck.substring(toCheck.lastIndexOf(46) + 1) : toCheck;
            if (toCheck.equals(result)) {
                result = null;
            }
            return result;
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            Lookup.Item item = ((ItemRadioButton)ae.getSource()).item;
            Object o = item.getInstance();
            ObjectEditor.this.setValue(item.getInstance());
            ObjectEditor.this.firePropertyChange();
        }
    }

}

