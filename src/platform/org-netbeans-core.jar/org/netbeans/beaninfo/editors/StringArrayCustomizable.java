/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.beaninfo.editors;

public interface StringArrayCustomizable {
    public String[] getStringArray();

    public void setStringArray(String[] var1);
}

