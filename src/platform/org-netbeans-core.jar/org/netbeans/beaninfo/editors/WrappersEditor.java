/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.ExPropertyEditor
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.beans.PropertyEditorManager;
import java.text.MessageFormat;
import java.util.Date;
import org.netbeans.core.UIExceptions;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.util.NbBundle;

public abstract class WrappersEditor
implements ExPropertyEditor {
    protected PropertyEditor pe = null;

    public WrappersEditor(Class type) {
        this.pe = PropertyEditorManager.findEditor(type);
    }

    public void setValue(Object newValue) throws IllegalArgumentException {
        this.pe.setValue(newValue);
    }

    public Object getValue() {
        return this.pe.getValue();
    }

    public boolean isPaintable() {
        return this.pe.isPaintable();
    }

    public void paintValue(Graphics gfx, Rectangle box) {
        this.pe.paintValue(gfx, box);
    }

    public String getAsText() {
        if (this.pe.getValue() == null) {
            return "null";
        }
        return this.pe.getAsText();
    }

    public void setAsText(String text) throws IllegalArgumentException {
        if ("null".equals(text)) {
            return;
        }
        try {
            this.pe.setAsText(text);
        }
        catch (Exception e) {
            IllegalArgumentException iae = new IllegalArgumentException(e.getMessage());
            String msg = e.getLocalizedMessage();
            if (msg == null || e.getMessage().equals(msg)) {
                msg = MessageFormat.format(NbBundle.getMessage(WrappersEditor.class, (String)"FMT_EXC_GENERIC_BAD_VALUE"), text);
            }
            UIExceptions.annotateUser(iae, iae.getMessage(), msg, e, new Date());
            throw iae;
        }
    }

    public String[] getTags() {
        return this.pe.getTags();
    }

    public Component getCustomEditor() {
        return this.pe.getCustomEditor();
    }

    public boolean supportsCustomEditor() {
        return this.pe.supportsCustomEditor();
    }

    public synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pe.addPropertyChangeListener(listener);
    }

    public synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
        this.pe.removePropertyChangeListener(listener);
    }

    public void attachEnv(PropertyEnv env) {
        if (this.pe instanceof ExPropertyEditor) {
            ((ExPropertyEditor)this.pe).attachEnv(env);
        }
    }
}

