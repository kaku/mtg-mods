/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.awt.Component;
import java.awt.Insets;
import java.io.IOException;
import org.netbeans.beaninfo.editors.ArrayOfIntSupport;
import org.netbeans.beaninfo.editors.InsetsCustomEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.util.NbBundle;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class InsetsEditor
extends ArrayOfIntSupport {
    private PropertyEnv env;

    public InsetsEditor() {
        super("java.awt.Insets", 4);
    }

    @Override
    int[] getValues() {
        Insets insets = (Insets)this.getValue();
        if (insets != null) {
            return new int[]{insets.top, insets.left, insets.bottom, insets.right};
        }
        return new int[4];
    }

    @Override
    void setValues(int[] val) {
        this.setValue(new Insets(val[0], val[1], val[2], val[3]));
    }

    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

    @Override
    public Component getCustomEditor() {
        return new InsetsCustomEditor(this, this.env);
    }

    @Override
    String getHintFormat() {
        return NbBundle.getMessage(InsetsEditor.class, (String)"CTL_HintFormatIE");
    }

    @Override
    protected String getXMLValueTag() {
        return "Insets";
    }

    @Override
    public void attachEnv(PropertyEnv env) {
        this.env = env;
    }
}

