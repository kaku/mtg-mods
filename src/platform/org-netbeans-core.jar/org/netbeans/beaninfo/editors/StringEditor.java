/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.ExPropertyEditor
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.awt.Component;
import java.beans.FeatureDescriptor;
import java.beans.PropertyEditor;
import java.beans.PropertyEditorSupport;
import org.netbeans.beaninfo.editors.StringCustomEditor;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

public class StringEditor
extends PropertyEditorSupport
implements ExPropertyEditor {
    private static boolean useRaw = Boolean.getBoolean("netbeans.stringEditor.useRawCharacters");
    private boolean editable = true;
    private String instructions = null;
    private boolean oneline = false;
    private boolean customEd = false;
    private PropertyEnv env;
    private String nullValue;

    public boolean isEditable() {
        return this.editable;
    }

    @Override
    public String getAsText() {
        Object value = this.getValue();
        if (value != null) {
            return value.toString();
        }
        return this.nullValue != null ? this.nullValue : NbBundle.getMessage(StringEditor.class, (String)"CTL_NullValue");
    }

    @Override
    public void setAsText(String s) {
        if ("null".equals(s) && this.getValue() == null) {
            return;
        }
        if (this.nullValue != null && this.nullValue.equals(s)) {
            this.setValue(null);
            return;
        }
        this.setValue(s);
    }

    @Override
    public String getJavaInitializationString() {
        String s = (String)this.getValue();
        return "\"" + StringEditor.toAscii(s) + "\"";
    }

    @Override
    public boolean supportsCustomEditor() {
        return this.customEd;
    }

    @Override
    public Component getCustomEditor() {
        Object val = this.getValue();
        String s = "";
        if (val != null) {
            s = val instanceof String ? (String)val : val.toString();
        }
        return new StringCustomEditor(s, this.isEditable(), this.oneline, this.instructions, this, this.env);
    }

    private static String toAscii(String str) {
        StringBuilder buf = new StringBuilder(str.length() * 6);
        char[] chars = str.toCharArray();
        block9 : for (int i = 0; i < chars.length; ++i) {
            char c = chars[i];
            switch (c) {
                case '\b': {
                    buf.append("\\b");
                    continue block9;
                }
                case '\t': {
                    buf.append("\\t");
                    continue block9;
                }
                case '\n': {
                    buf.append("\\n");
                    continue block9;
                }
                case '\f': {
                    buf.append("\\f");
                    continue block9;
                }
                case '\r': {
                    buf.append("\\r");
                    continue block9;
                }
                case '\"': {
                    buf.append("\\\"");
                    continue block9;
                }
                case '\\': {
                    buf.append("\\\\");
                    continue block9;
                }
                default: {
                    if (c >= ' ' && (useRaw || c <= '')) {
                        buf.append(c);
                        continue block9;
                    }
                    buf.append("\\u");
                    String hex = Integer.toHexString(c);
                    for (int j = 0; j < 4 - hex.length(); ++j) {
                        buf.append('0');
                    }
                    buf.append(hex);
                }
            }
        }
        return buf.toString();
    }

    public void attachEnv(PropertyEnv env) {
        this.env = env;
        this.readEnv(env.getFeatureDescriptor());
    }

    void readEnv(FeatureDescriptor desc) {
        Object obj;
        if (desc instanceof Node.Property) {
            Node.Property prop = (Node.Property)desc;
            this.editable = prop.canWrite();
            this.instructions = (String)prop.getValue("instructions");
            this.oneline = Boolean.TRUE.equals(prop.getValue("oneline"));
            boolean bl = this.customEd = !Boolean.TRUE.equals(prop.getValue("suppressCustomEditor"));
        }
        this.nullValue = Boolean.TRUE.equals(obj = desc.getValue("nullValue")) ? NbBundle.getMessage(StringEditor.class, (String)"CTL_NullValue") : (obj instanceof String ? (String)obj : null);
    }
}

