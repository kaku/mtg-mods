/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.ExPropertyEditor
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.ImageObserver;
import java.beans.FeatureDescriptor;
import java.beans.PropertyEditorSupport;
import java.util.Date;
import org.netbeans.core.UIExceptions;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

public class ListImageEditor
extends PropertyEditorSupport
implements ExPropertyEditor {
    public static final String PROP_IMAGES = "images";
    public static final String PROP_VALUES = "values";
    public static final String PROP_DESCRIPTIONS = "descriptions";
    private boolean canWrite = true;
    private Image[] images = null;
    private Integer[] values = null;
    private String[] descriptions = null;

    public void attachEnv(PropertyEnv env) {
        FeatureDescriptor d = env.getFeatureDescriptor();
        if (d instanceof Node.Property) {
            this.canWrite = ((Node.Property)d).canWrite();
        }
        Image[] imgs = null;
        Integer[] vals = null;
        String[] descs = null;
        Object o = d.getValue("images");
        if (o instanceof Image[]) {
            imgs = (Image[])o;
        }
        if ((o = d.getValue("values")) instanceof Integer[]) {
            vals = (Integer[])o;
        }
        if ((o = d.getValue("descriptions")) instanceof String[]) {
            descs = (String[])o;
        }
        if (imgs != null && vals != null) {
            int length = imgs.length;
            if (vals.length < length) {
                length = vals.length;
            }
            if (descs != null && descs.length < length) {
                length = descs.length;
            }
            this.images = new Image[length];
            this.values = new Integer[length];
            this.descriptions = new String[length];
            for (int i = 0; i < length; ++i) {
                this.images[i] = imgs[i];
                this.values[i] = vals[i];
                this.descriptions[i] = descs == null ? vals[i].toString() : descs[i];
            }
        }
    }

    public boolean isEditable() {
        return this.canWrite;
    }

    @Override
    public String getAsText() {
        int i = this.findIndex(this.values, this.getValue());
        return (String)this.findObject(this.descriptions, i);
    }

    @Override
    public void setAsText(String str) throws IllegalArgumentException {
        int i = this.findIndex(this.descriptions, str);
        if (i == -1) {
            IllegalArgumentException iae = new IllegalArgumentException("negative: " + str);
            String msg = NbBundle.getMessage(ListImageEditor.class, (String)"CTL_NegativeSize");
            UIExceptions.annotateUser(iae, iae.getMessage(), msg, null, new Date());
            throw iae;
        }
        this.setValue(this.findObject(this.values, i));
    }

    @Override
    public String[] getTags() {
        return this.descriptions;
    }

    @Override
    public boolean isPaintable() {
        return true;
    }

    @Override
    public void paintValue(Graphics g, Rectangle rectangle) {
        Image img = (Image)this.findObject(this.images, this.findIndex(this.values, this.getValue()));
        if (img != null) {
            g.drawImage(img, rectangle.x + (rectangle.width - img.getWidth(null)) / 2, rectangle.y + (rectangle.height - img.getHeight(null)) / 2, img.getWidth(null), img.getHeight(null), null);
        }
    }

    @Override
    public String getJavaInitializationString() {
        return "new Integer(" + this.getValue() + ")";
    }

    private Object findObject(Object[] objs, int i) {
        if (objs == null || i < 0 || i >= objs.length) {
            return null;
        }
        return objs[i];
    }

    private int findIndex(Object[] objs, Object obj) {
        if (objs != null) {
            for (int i = 0; i < objs.length; ++i) {
                if (!objs[i].equals(obj)) continue;
                return i;
            }
        }
        return -1;
    }
}

