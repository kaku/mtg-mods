/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.ExPropertyEditor
 *  org.openide.explorer.propertysheet.PropertyEnv
 */
package org.netbeans.beaninfo.editors;

import java.beans.PropertyEditorSupport;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;

public abstract class ExPropertyEditorSupport
extends PropertyEditorSupport
implements ExPropertyEditor {
    protected ExPropertyEditorSupport() {
    }

    public final void attachEnv(PropertyEnv env) {
        this.attachEnvImpl(env);
        this.validateEnv(env);
    }

    protected abstract void attachEnvImpl(PropertyEnv var1);

    protected abstract void validateEnv(PropertyEnv var1);

    protected static final String arrToStr(Object[] s) {
        if (s == null) {
            return "null";
        }
        StringBuilder out = new StringBuilder(s.length * 10);
        for (int i = 0; i < s.length; ++i) {
            if (s[i] != null) {
                out.append(s[i]);
            } else {
                out.append("null");
            }
            if (i == s.length - 1) continue;
            out.append(",");
        }
        return out.toString();
    }

    public static class EnvException
    extends IllegalArgumentException {
        public EnvException(String s) {
            super(s);
        }
    }

}

