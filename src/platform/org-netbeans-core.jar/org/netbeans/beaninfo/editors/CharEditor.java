/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.editors.EnhancedPropertyEditor
 *  org.openide.nodes.PropertyEditorRegistration
 */
package org.netbeans.beaninfo.editors;

import java.awt.Component;
import java.beans.PropertyEditorSupport;
import org.openide.explorer.propertysheet.editors.EnhancedPropertyEditor;
import org.openide.nodes.PropertyEditorRegistration;

@PropertyEditorRegistration(targetType={Character.class, char.class})
public class CharEditor
extends PropertyEditorSupport
implements EnhancedPropertyEditor {
    @Override
    public String getAsText() {
        char value = ((Character)this.getValue()).charValue();
        StringBuffer buf = new StringBuffer(6);
        switch (value) {
            case '\b': {
                buf.append("\\b");
                break;
            }
            case '\t': {
                buf.append("\\t");
                break;
            }
            case '\n': {
                buf.append("\\n");
                break;
            }
            case '\f': {
                buf.append("\\f");
                break;
            }
            case '\r': {
                buf.append("\\r");
                break;
            }
            case '\\': {
                buf.append("\\\\");
                break;
            }
            default: {
                if (value >= ' ' && value <= '') {
                    buf.append(value);
                    break;
                }
                buf.append("\\u");
                String hex = Integer.toHexString(value);
                for (int j = 0; j < 4 - hex.length(); ++j) {
                    buf.append('0');
                }
                buf.append(hex);
            }
        }
        return buf.toString();
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if (text.length() < 1) {
            return;
        }
        int value = 0;
        if (text.charAt(0) == '\\') {
            int ch = text.length() >= 2 ? (int)text.charAt(1) : 92;
            switch (ch) {
                case 98: {
                    value = 8;
                    break;
                }
                case 116: {
                    value = 9;
                    break;
                }
                case 110: {
                    value = 10;
                    break;
                }
                case 102: {
                    value = 12;
                    break;
                }
                case 114: {
                    value = 13;
                    break;
                }
                case 92: {
                    value = 92;
                    break;
                }
                case 117: {
                    String num = text.substring(2, text.length());
                    if (num.length() > 4) {
                        return;
                    }
                    try {
                        int intValue = Integer.parseInt(num, 16);
                        value = (char)intValue;
                        break;
                    }
                    catch (NumberFormatException nfe) {
                        return;
                    }
                }
                default: {
                    return;
                }
            }
        } else {
            value = text.charAt(0);
        }
        this.setValue(Character.valueOf((char)value));
    }

    @Override
    public void setValue(Object newValue) throws IllegalArgumentException {
        String text;
        if (newValue instanceof Character) {
            super.setValue(newValue);
            return;
        }
        if (newValue instanceof String && (text = (String)newValue).length() >= 1) {
            super.setValue(Character.valueOf(text.charAt(0)));
            return;
        }
        if (newValue == null) {
            super.setValue(Character.valueOf('\u0000'));
            return;
        }
        throw new IllegalArgumentException();
    }

    @Override
    public String getJavaInitializationString() {
        if (((Character)this.getValue()).charValue() == '\'') {
            return "'\\''";
        }
        return "'" + this.getAsText() + "'";
    }

    public Component getInPlaceCustomEditor() {
        return null;
    }

    public boolean hasInPlaceCustomEditor() {
        return false;
    }

    public boolean supportsEditingTaggedValues() {
        return true;
    }
}

