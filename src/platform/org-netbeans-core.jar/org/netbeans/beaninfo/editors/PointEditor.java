/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.awt.Component;
import java.awt.Point;
import java.io.IOException;
import org.netbeans.beaninfo.editors.ArrayOfIntSupport;
import org.netbeans.beaninfo.editors.PointCustomEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.util.NbBundle;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class PointEditor
extends ArrayOfIntSupport {
    public PointEditor() {
        super("java.awt.Point", 2);
    }

    @Override
    int[] getValues() {
        Point p = (Point)this.getValue();
        return new int[]{p.x, p.y};
    }

    @Override
    void setValues(int[] val) {
        this.setValue(new Point(val[0], val[1]));
    }

    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

    @Override
    public Component getCustomEditor() {
        return new PointCustomEditor(this, this.env);
    }

    @Override
    String getHintFormat() {
        return NbBundle.getMessage(PointEditor.class, (String)"CTL_HintFormatPE");
    }

    @Override
    protected String getXMLValueTag() {
        return "Point";
    }
}

