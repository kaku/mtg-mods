/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.beaninfo.editors;

import org.netbeans.beaninfo.editors.WrappersEditor;

public class DoubleEditor
extends WrappersEditor {
    public DoubleEditor() {
        super(Double.TYPE);
    }

    public String getJavaInitializationString() {
        return "new java.lang.Double(" + this.getAsText() + ")";
    }
}

