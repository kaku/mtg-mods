/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.netbeans.beaninfo.editors.StringArrayCustomizable;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class StringArrayCustomEditor
extends JPanel {
    private Vector<String> itemsVector;
    private StringArrayCustomizable editor;
    static final long serialVersionUID = -4347656479280614636L;
    private JPanel editPanel;
    private JScrollPane itemListScroll;
    private JList itemList;
    private JLabel itemLabel;
    private JTextField itemField;
    private JLabel itemListLabel;
    private JPanel buttonsPanel;
    private JButton addButton;
    private JButton changeButton;
    private JButton removeButton;
    private JSeparator jSeparator1;
    private JButton moveUpButton;
    private JButton moveDownButton;
    private JPanel paddingPanel;

    public StringArrayCustomEditor(StringArrayCustomizable sac) {
        this.editor = sac;
        this.itemsVector = new Vector();
        String[] array = this.editor.getStringArray();
        if (array != null) {
            for (int i = 0; i < array.length; ++i) {
                this.itemsVector.addElement(array[i]);
            }
        }
        this.initComponents();
        this.itemList.setCellRenderer(new EmptyStringListCellRenderer());
        this.itemList.setListData(this.itemsVector);
        this.itemList.setSelectionMode(0);
        this.setBorder(new EmptyBorder(new Insets(16, 8, 8, 0)));
        this.buttonsPanel.setBorder(new EmptyBorder(new Insets(0, 5, 5, 5)));
        Mnemonics.setLocalizedText((JLabel)this.itemLabel, (String)NbBundle.getMessage(StringArrayCustomEditor.class, (String)"CTL_Item"));
        Mnemonics.setLocalizedText((JLabel)this.itemListLabel, (String)NbBundle.getMessage(StringArrayCustomEditor.class, (String)"CTL_ItemList"));
        Mnemonics.setLocalizedText((AbstractButton)this.addButton, (String)NbBundle.getMessage(StringArrayCustomEditor.class, (String)"CTL_Add_StringArrayCustomEditor"));
        Mnemonics.setLocalizedText((AbstractButton)this.changeButton, (String)NbBundle.getMessage(StringArrayCustomEditor.class, (String)"CTL_Change_StringArrayCustomEditor"));
        Mnemonics.setLocalizedText((AbstractButton)this.removeButton, (String)NbBundle.getMessage(StringArrayCustomEditor.class, (String)"CTL_Remove"));
        Mnemonics.setLocalizedText((AbstractButton)this.moveUpButton, (String)NbBundle.getMessage(StringArrayCustomEditor.class, (String)"CTL_MoveUp"));
        Mnemonics.setLocalizedText((AbstractButton)this.moveDownButton, (String)NbBundle.getMessage(StringArrayCustomEditor.class, (String)"CTL_MoveDown"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(StringArrayCustomEditor.class, (String)"ACSD_StringArrayCustomEditor"));
        this.itemField.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(StringArrayCustomEditor.class, (String)"ACSD_CTL_Item"));
        this.itemList.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(StringArrayCustomEditor.class, (String)"ACSD_CTL_ItemList"));
        this.addButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(StringArrayCustomEditor.class, (String)"ACSD_CTL_Add_StringArrayCustomEditor"));
        this.changeButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(StringArrayCustomEditor.class, (String)"ACSD_CTL_Change_StringArrayCustomEditor"));
        this.removeButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(StringArrayCustomEditor.class, (String)"ACSD_CTL_Remove"));
        this.moveUpButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(StringArrayCustomEditor.class, (String)"ACSD_CTL_MoveUp"));
        this.moveDownButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(StringArrayCustomEditor.class, (String)"ACSD_CTL_MoveDown"));
        this.updateButtons();
        this.itemField.addKeyListener(new KeyAdapter(){

            @Override
            public void keyReleased(KeyEvent event) {
                boolean containsCurrent = StringArrayCustomEditor.this.containsCurrent();
                String txt = StringArrayCustomEditor.this.itemField.getText().trim();
                boolean en = StringArrayCustomEditor.this.itemField.isEnabled() && txt.length() > 0 && !containsCurrent;
                StringArrayCustomEditor.this.addButton.setEnabled(en);
                StringArrayCustomEditor.this.changeButton.setEnabled(en && StringArrayCustomEditor.this.itemList.getSelectedIndex() != -1);
                if (containsCurrent) {
                    StringArrayCustomEditor.this.itemList.setSelectedIndex(StringArrayCustomEditor.this.idxOfCurrent());
                }
            }
        });
        this.itemField.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent ae) {
                if (StringArrayCustomEditor.this.addButton.isEnabled()) {
                    StringArrayCustomEditor.this.addButtonActionPerformed(ae);
                }
            }
        });
        this.addButton.setEnabled(false);
        this.changeButton.setEnabled(false);
        this.setMinimumSize(new Dimension(200, 400));
    }

    private boolean containsCurrent() {
        return this.idxOfCurrent() != -1;
    }

    private int idxOfCurrent() {
        String txt = this.itemField.getText().trim();
        if (txt.length() > 0) {
            int max = this.itemList.getModel().getSize();
            for (int i = 0; i < max; ++i) {
                if (!txt.equals(this.itemList.getModel().getElementAt(i))) continue;
                return i;
            }
        }
        return -1;
    }

    private void initComponents() {
        this.editPanel = new JPanel();
        this.itemListScroll = new JScrollPane();
        this.itemList = new JList();
        this.itemLabel = new JLabel();
        this.itemField = new JTextField();
        this.itemListLabel = new JLabel();
        this.buttonsPanel = new JPanel();
        this.addButton = new JButton();
        this.changeButton = new JButton();
        this.removeButton = new JButton();
        this.jSeparator1 = new JSeparator();
        this.moveUpButton = new JButton();
        this.moveDownButton = new JButton();
        this.paddingPanel = new JPanel();
        this.setLayout(new BorderLayout());
        this.editPanel.setLayout(new GridBagLayout());
        this.itemList.addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent evt) {
                StringArrayCustomEditor.this.itemListValueChanged(evt);
            }
        });
        this.itemListScroll.setViewportView(this.itemList);
        GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
        gridBagConstraints2.gridx = 0;
        gridBagConstraints2.gridy = 2;
        gridBagConstraints2.gridwidth = 2;
        gridBagConstraints2.fill = 1;
        gridBagConstraints2.weightx = 1.0;
        gridBagConstraints2.weighty = 1.0;
        this.editPanel.add((Component)this.itemListScroll, gridBagConstraints2);
        this.itemLabel.setText("item");
        this.itemLabel.setLabelFor(this.itemField);
        gridBagConstraints2 = new GridBagConstraints();
        gridBagConstraints2.insets = new Insets(0, 0, 11, 12);
        gridBagConstraints2.anchor = 17;
        this.editPanel.add((Component)this.itemLabel, gridBagConstraints2);
        gridBagConstraints2 = new GridBagConstraints();
        gridBagConstraints2.fill = 2;
        gridBagConstraints2.insets = new Insets(0, 0, 11, 0);
        this.editPanel.add((Component)this.itemField, gridBagConstraints2);
        this.itemListLabel.setText("jLabel1");
        this.itemListLabel.setLabelFor(this.itemList);
        gridBagConstraints2 = new GridBagConstraints();
        gridBagConstraints2.gridx = 0;
        gridBagConstraints2.gridy = 1;
        gridBagConstraints2.gridwidth = 2;
        gridBagConstraints2.insets = new Insets(0, 0, 2, 0);
        gridBagConstraints2.anchor = 17;
        this.editPanel.add((Component)this.itemListLabel, gridBagConstraints2);
        this.add((Component)this.editPanel, "Center");
        this.buttonsPanel.setLayout(new GridBagLayout());
        this.addButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                StringArrayCustomEditor.this.addButtonActionPerformed(evt);
            }
        });
        GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.gridwidth = 0;
        gridBagConstraints1.fill = 1;
        gridBagConstraints1.insets = new Insets(0, 8, 0, 8);
        gridBagConstraints1.weightx = 1.0;
        this.buttonsPanel.add((Component)this.addButton, gridBagConstraints1);
        this.changeButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                StringArrayCustomEditor.this.changeButtonActionPerformed(evt);
            }
        });
        gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.gridwidth = 0;
        gridBagConstraints1.fill = 1;
        gridBagConstraints1.insets = new Insets(8, 8, 0, 8);
        gridBagConstraints1.weightx = 1.0;
        this.buttonsPanel.add((Component)this.changeButton, gridBagConstraints1);
        this.removeButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                StringArrayCustomEditor.this.removeButtonActionPerformed(evt);
            }
        });
        gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.gridwidth = 0;
        gridBagConstraints1.fill = 1;
        gridBagConstraints1.insets = new Insets(8, 8, 8, 8);
        gridBagConstraints1.weightx = 1.0;
        this.buttonsPanel.add((Component)this.removeButton, gridBagConstraints1);
        gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.gridwidth = 0;
        gridBagConstraints1.fill = 2;
        gridBagConstraints1.insets = new Insets(0, 4, 0, 4);
        this.buttonsPanel.add((Component)this.jSeparator1, gridBagConstraints1);
        this.moveUpButton.setEnabled(false);
        this.moveUpButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                StringArrayCustomEditor.this.moveUpButtonActionPerformed(evt);
            }
        });
        gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.gridwidth = 0;
        gridBagConstraints1.fill = 1;
        gridBagConstraints1.insets = new Insets(8, 8, 0, 8);
        gridBagConstraints1.weightx = 1.0;
        this.buttonsPanel.add((Component)this.moveUpButton, gridBagConstraints1);
        this.moveDownButton.setEnabled(false);
        this.moveDownButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                StringArrayCustomEditor.this.moveDownButtonActionPerformed(evt);
            }
        });
        gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.gridwidth = 0;
        gridBagConstraints1.fill = 1;
        gridBagConstraints1.insets = new Insets(8, 8, 0, 8);
        gridBagConstraints1.weightx = 1.0;
        this.buttonsPanel.add((Component)this.moveDownButton, gridBagConstraints1);
        gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.weighty = 1.0;
        this.buttonsPanel.add((Component)this.paddingPanel, gridBagConstraints1);
        this.add((Component)this.buttonsPanel, "East");
    }

    private void changeButtonActionPerformed(ActionEvent evt) {
        int sel = this.itemList.getSelectedIndex();
        String s = this.itemsVector.elementAt(sel);
        this.itemsVector.removeElementAt(sel);
        this.itemsVector.insertElementAt(this.itemField.getText(), sel);
        this.itemList.setListData(this.itemsVector);
        this.itemList.setSelectedIndex(sel);
        this.itemList.repaint();
        this.updateValue();
    }

    private void moveDownButtonActionPerformed(ActionEvent evt) {
        int sel = this.itemList.getSelectedIndex();
        String s = this.itemsVector.elementAt(sel);
        this.itemsVector.removeElementAt(sel);
        this.itemsVector.insertElementAt(s, sel + 1);
        this.itemList.setListData(this.itemsVector);
        this.itemList.setSelectedIndex(sel + 1);
        this.itemList.repaint();
        this.updateValue();
    }

    private void moveUpButtonActionPerformed(ActionEvent evt) {
        int sel = this.itemList.getSelectedIndex();
        String s = this.itemsVector.elementAt(sel);
        this.itemsVector.removeElementAt(sel);
        this.itemsVector.insertElementAt(s, sel - 1);
        this.itemList.setListData(this.itemsVector);
        this.itemList.setSelectedIndex(sel - 1);
        this.itemList.repaint();
        this.updateValue();
    }

    private void removeButtonActionPerformed(ActionEvent evt) {
        int currentIndex = this.itemList.getSelectedIndex();
        this.itemsVector.removeElementAt(currentIndex);
        this.itemList.setListData(this.itemsVector);
        if (this.itemsVector.size() != 0) {
            if (currentIndex >= this.itemsVector.size()) {
                currentIndex = this.itemsVector.size() - 1;
            }
            this.itemList.setSelectedIndex(currentIndex);
        }
        this.itemList.repaint();
        this.updateValue();
    }

    private void itemListValueChanged(ListSelectionEvent evt) {
        this.updateButtons();
        int sel = this.itemList.getSelectedIndex();
        if (sel != -1) {
            this.itemField.setText(this.itemsVector.elementAt(sel));
        }
    }

    private void addButtonActionPerformed(ActionEvent evt) {
        this.itemsVector.addElement(this.itemField.getText());
        this.itemList.setListData(this.itemsVector);
        this.itemList.setSelectedIndex(this.itemsVector.size() - 1);
        this.itemList.repaint();
        this.updateValue();
    }

    @Override
    public void setEnabled(boolean val) {
        Component[] c = this.getComponents();
        super.setEnabled(val);
        this.setChildrenEnabled(this, val);
    }

    private void setChildrenEnabled(JPanel parent, boolean val) {
        Component[] c = parent.getComponents();
        for (int i = 0; i < c.length; ++i) {
            c[i].setEnabled(val);
            if (!(c[i] instanceof JPanel)) continue;
            this.setChildrenEnabled((JPanel)c[i], val);
        }
    }

    private void updateButtons() {
        int sel = this.itemList.getSelectedIndex();
        boolean enVal = this.isEnabled();
        if (sel == -1) {
            this.removeButton.setEnabled(false);
            this.moveUpButton.setEnabled(false);
            this.moveDownButton.setEnabled(false);
            this.changeButton.setEnabled(false);
        } else {
            this.removeButton.setEnabled(enVal);
            this.moveUpButton.setEnabled(enVal && sel != 0);
            this.moveDownButton.setEnabled(enVal && sel != this.itemsVector.size() - 1);
            this.changeButton.setEnabled(enVal);
        }
        this.itemField.setEnabled(enVal);
        boolean containsCurrent = this.containsCurrent();
        String txt = this.itemField.getText().trim();
        boolean en = this.itemField.isEnabled() && txt.length() > 0 && !containsCurrent;
        this.addButton.setEnabled(en);
    }

    private void updateValue() {
        Object[] value = new String[this.itemsVector.size()];
        this.itemsVector.copyInto(value);
        this.editor.setStringArray((String[])value);
    }

    static class EmptyStringListCellRenderer
    extends JLabel
    implements ListCellRenderer {
        protected static Border hasFocusBorder = new LineBorder(UIManager.getColor("List.focusCellHighlight"));
        protected static Border noFocusBorder = new EmptyBorder(1, 1, 1, 1);
        static final long serialVersionUID = 487512296465844339L;

        public EmptyStringListCellRenderer() {
            this.setOpaque(true);
            this.setBorder(noFocusBorder);
        }

        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            if (!(value instanceof String)) {
                return this;
            }
            String text = (String)value;
            if ("".equals(text)) {
                text = NbBundle.getMessage(StringArrayCustomEditor.class, (String)"CTL_Empty");
            }
            this.setText(text);
            if (isSelected) {
                this.setBackground(UIManager.getColor("List.selectionBackground"));
                this.setForeground(UIManager.getColor("List.selectionForeground"));
            } else {
                this.setBackground(list.getBackground());
                this.setForeground(list.getForeground());
            }
            this.setBorder(cellHasFocus ? hasFocusBorder : noFocusBorder);
            return this;
        }
    }

}

