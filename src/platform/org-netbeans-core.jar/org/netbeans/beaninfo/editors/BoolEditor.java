/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.nodes.PropertyEditorRegistration
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.beans.FeatureDescriptor;
import org.netbeans.beaninfo.editors.ExPropertyEditorSupport;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.nodes.PropertyEditorRegistration;
import org.openide.util.NbBundle;

@PropertyEditorRegistration(targetType={Boolean.class})
public class BoolEditor
extends ExPropertyEditorSupport {
    String[] stringValues = null;

    @Override
    protected void attachEnvImpl(PropertyEnv env) {
        this.stringValues = (String[])env.getFeatureDescriptor().getValue("stringValues");
    }

    @Override
    protected void validateEnv(PropertyEnv env) {
        if (this.stringValues != null && this.stringValues.length != 2) {
            throw new ExPropertyEditorSupport.EnvException("String value hint for boolean editor must contain exactly 2 items.  The supplied value contains " + this.stringValues.length + " items: " + BoolEditor.arrToStr(this.stringValues));
        }
    }

    private String getStringRep(boolean val) {
        if (this.stringValues != null) {
            return this.stringValues[val ? 0 : 1];
        }
        String result = val ? NbBundle.getMessage(BoolEditor.class, (String)"TRUE") : NbBundle.getMessage(BoolEditor.class, (String)"FALSE");
        return result;
    }

    private Boolean stringVal(String val) {
        String valToTest = val.trim().toUpperCase();
        String test = this.getStringRep(true).toUpperCase();
        if (test.equals(valToTest)) {
            return Boolean.TRUE;
        }
        test = this.getStringRep(false).toUpperCase();
        if (test.equals(valToTest)) {
            return Boolean.FALSE;
        }
        return null;
    }

    @Override
    public String getJavaInitializationString() {
        Boolean val = (Boolean)this.getValue();
        if (val == null) {
            return "null";
        }
        return Boolean.TRUE.equals(this.getValue()) ? "true" : "false";
    }

    @Override
    public String[] getTags() {
        return new String[]{this.getStringRep(true), this.getStringRep(false)};
    }

    @Override
    public String getAsText() {
        Boolean val = (Boolean)this.getValue();
        if (val == null) {
            return NbBundle.getMessage(BoolEditor.class, (String)"NULL");
        }
        return this.getStringRep(Boolean.TRUE.equals(this.getValue()));
    }

    @Override
    public void setAsText(String txt) {
        Boolean val = this.stringVal(txt);
        boolean newVal = val == null ? false : val;
        this.setValue(newVal ? Boolean.TRUE : Boolean.FALSE);
    }
}

