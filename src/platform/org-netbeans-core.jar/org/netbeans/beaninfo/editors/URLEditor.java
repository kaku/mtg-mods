/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.editors.XMLPropertyEditor
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.beans.PropertyEditorSupport;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Date;
import org.netbeans.core.UIExceptions;
import org.openide.explorer.propertysheet.editors.XMLPropertyEditor;
import org.openide.util.NbBundle;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class URLEditor
extends PropertyEditorSupport
implements XMLPropertyEditor {
    public static final String XML_URL = "Url";
    public static final String ATTR_VALUE = "value";

    @Override
    public void setAsText(String s) {
        if ("null".equals(s)) {
            this.setValue(null);
            return;
        }
        try {
            URL url = new URL(s);
            this.setValue(url);
        }
        catch (MalformedURLException e) {
            IllegalArgumentException iae = new IllegalArgumentException(e.getMessage());
            String msg = MessageFormat.format(NbBundle.getMessage(URLEditor.class, (String)"FMT_EXC_BAD_URL"), s);
            UIExceptions.annotateUser(iae, e.getMessage(), msg, e, new Date());
            throw iae;
        }
    }

    @Override
    public String getAsText() {
        URL url = (URL)this.getValue();
        return url != null ? url.toString() : "null";
    }

    @Override
    public String getJavaInitializationString() {
        URL url = (URL)this.getValue();
        return "new java.net.URL(\"" + url.toString() + "\")";
    }

    @Override
    public boolean supportsCustomEditor() {
        return false;
    }

    public void readFromXML(Node element) throws IOException {
        if (!"Url".equals(element.getNodeName())) {
            throw new IOException();
        }
        NamedNodeMap attributes = element.getAttributes();
        try {
            String value = attributes.getNamedItem("value").getNodeValue();
            this.setAsText(value);
        }
        catch (Exception e) {
            throw new IOException();
        }
    }

    public Node storeToXML(Document doc) {
        Element el = doc.createElement("Url");
        el.setAttribute("value", this.getAsText());
        return el;
    }
}

