/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.Mnemonics
 *  org.openide.explorer.propertysheet.editors.XMLPropertyEditor
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.beaninfo.editors;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyEditor;
import java.io.IOException;
import java.util.Arrays;
import javax.accessibility.AccessibleContext;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.netbeans.core.UIExceptions;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.explorer.propertysheet.editors.XMLPropertyEditor;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class FontEditor
implements PropertyEditor,
XMLPropertyEditor {
    static final boolean antialias = Boolean.getBoolean("nb.cellrenderer.antialiasing") || Boolean.getBoolean("swing.aatext") || FontEditor.isGTK() && FontEditor.gtkShouldAntialias() || FontEditor.isAqua();
    static final Integer[] sizes = new Integer[]{3, 5, 8, 10, 12, 14, 18, 24, 36, 48};
    static final String[] styles = new String[]{NbBundle.getMessage(FontEditor.class, (String)"CTL_Plain"), NbBundle.getMessage(FontEditor.class, (String)"CTL_Bold"), NbBundle.getMessage(FontEditor.class, (String)"CTL_Italic"), NbBundle.getMessage(FontEditor.class, (String)"CTL_BoldItalic")};
    private Font font;
    private String fontName;
    private PropertyChangeSupport support;
    private static String[] fonts;
    public static final String XML_FONT = "Font";
    public static final String ATTR_NAME = "name";
    public static final String ATTR_STYLE = "style";
    public static final String ATTR_SIZE = "size";
    private static Boolean gtkAA;

    public FontEditor() {
        this.support = new PropertyChangeSupport(this);
    }

    @Override
    public Object getValue() {
        return this.font;
    }

    @Override
    public void setValue(Object object) {
        if (this.font != null && this.font.equals(object)) {
            return;
        }
        if (this.font == null && object == null) {
            return;
        }
        if (object instanceof Font) {
            this.font = (Font)object;
        } else if (object == null) {
            this.font = null;
        } else assert (false);
        this.fontName = this.font != null ? this.font.getName() + " " + this.font.getSize() + " " + this.getStyleName(this.font.getStyle()) : null;
        this.support.firePropertyChange("", null, null);
    }

    @Override
    public String getAsText() {
        return this.fontName;
    }

    @Override
    public void setAsText(String string) {
    }

    @Override
    public String getJavaInitializationString() {
        return "new java.awt.Font(\"" + this.font.getName() + "\", " + this.font.getStyle() + ", " + this.font.getSize() + ")";
    }

    @Override
    public String[] getTags() {
        return null;
    }

    @Override
    public boolean isPaintable() {
        return true;
    }

    @Override
    public void paintValue(Graphics g, Rectangle rectangle) {
        this.paintText(g, rectangle, this.fontName == null ? "null" : this.fontName);
    }

    private void paintText(Graphics g, Rectangle rectangle, String text) {
        Font paintFont;
        if (antialias && g instanceof Graphics2D) {
            ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }
        Font originalFont = g.getFont();
        if (this.font == null) {
            this.setValue(null);
        }
        Font font = paintFont = this.font == null ? originalFont : this.font;
        assert (paintFont != null);
        FontMetrics fm = g.getFontMetrics(paintFont);
        if (fm.getHeight() > rectangle.height) {
            paintFont = Utilities.isMac() ? new Font(paintFont.getName(), paintFont.getStyle(), 12) : paintFont.deriveFont(12.0f);
            fm = g.getFontMetrics(paintFont);
        }
        g.setFont(paintFont);
        g.drawString(text, rectangle.x, rectangle.y + (rectangle.height - fm.getHeight()) / 2 + fm.getAscent());
        g.setFont(originalFont);
    }

    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

    @Override
    public Component getCustomEditor() {
        return new FontPanel();
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this.support.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this.support.removePropertyChangeListener(propertyChangeListener);
    }

    String getStyleName(int i) {
        if ((i & 1) > 0) {
            if ((i & 2) > 0) {
                return NbBundle.getMessage(FontEditor.class, (String)"CTL_BoldItalic");
            }
            return NbBundle.getMessage(FontEditor.class, (String)"CTL_Bold");
        }
        if ((i & 2) > 0) {
            return NbBundle.getMessage(FontEditor.class, (String)"CTL_Italic");
        }
        return NbBundle.getMessage(FontEditor.class, (String)"CTL_Plain");
    }

    private static String[] getFonts() {
        if (fonts == null) {
            try {
                fonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
            }
            catch (RuntimeException e) {
                fonts = new String[0];
                if (Utilities.isMac()) {
                    String msg = NbBundle.getMessage(FontEditor.class, (String)"MSG_AppleBug");
                    DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)msg));
                }
                throw e;
            }
        }
        return fonts;
    }

    public void readFromXML(Node element) throws IOException {
        if (!"Font".equals(element.getNodeName())) {
            throw new IOException();
        }
        NamedNodeMap attributes = element.getAttributes();
        try {
            String name = attributes.getNamedItem("name").getNodeValue();
            String style = attributes.getNamedItem("style").getNodeValue();
            String size = attributes.getNamedItem("size").getNodeValue();
            this.setValue(new Font(name, Integer.parseInt(style), Integer.parseInt(size)));
        }
        catch (NullPointerException e) {
            throw new IOException();
        }
    }

    public Node storeToXML(Document doc) {
        if (this.font == null) {
            IllegalArgumentException iae = new IllegalArgumentException();
            Exceptions.attachLocalizedMessage((Throwable)iae, (String)NbBundle.getMessage(FontEditor.class, (String)"MSG_FontIsNotInitialized"));
            Exceptions.printStackTrace((Throwable)iae);
            return null;
        }
        Element el = doc.createElement("Font");
        el.setAttribute("name", this.font.getName());
        el.setAttribute("style", Integer.toString(this.font.getStyle()));
        el.setAttribute("size", Integer.toString(this.font.getSize()));
        return el;
    }

    private static boolean isAqua() {
        return "Aqua".equals(UIManager.getLookAndFeel().getID());
    }

    private static boolean isGTK() {
        return "GTK".equals(UIManager.getLookAndFeel().getID());
    }

    private static boolean gtkShouldAntialias() {
        if (gtkAA == null) {
            Object o = Toolkit.getDefaultToolkit().getDesktopProperty("gnome.Xft/Antialias");
            gtkAA = new Integer(1).equals(o) ? Boolean.TRUE : Boolean.FALSE;
        }
        return gtkAA;
    }

    class FontPanel
    extends JPanel {
        JTextField tfFont;
        JTextField tfStyle;
        JTextField tfSize;
        JList lFont;
        JList lStyle;
        JList lSize;
        boolean dontSetValue;
        static final long serialVersionUID = 8377025140456676594L;

        FontPanel() {
            this.dontSetValue = false;
            this.dontSetValue = false;
            this.setLayout(new BorderLayout());
            this.setBorder(new EmptyBorder(12, 12, 0, 11));
            Font font = (Font)FontEditor.this.getValue();
            if (font == null) {
                font = FontEditor.getFonts().length > 0 ? new Font(fonts[0], 0, 10) : UIManager.getFont("Label.font");
            }
            this.lFont = new JList<String>(FontEditor.getFonts());
            this.lFont.setSelectionMode(0);
            this.lFont.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FontEditor.class, (String)"ACSD_CTL_Font"));
            this.lStyle = new JList<String>(FontEditor.styles);
            this.lStyle.setSelectionMode(0);
            this.lStyle.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FontEditor.class, (String)"ACSD_CTL_FontStyle"));
            this.lSize = new JList<Integer>(FontEditor.sizes);
            this.lSize.setSelectionMode(0);
            this.lSize.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FontEditor.class, (String)"ACSD_CTL_Size"));
            this.tfSize = new JTextField(String.valueOf(font.getSize()));
            this.tfSize.getAccessibleContext().setAccessibleDescription(this.lSize.getAccessibleContext().getAccessibleDescription());
            this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FontEditor.class, (String)"ACSD_FontCustomEditor"));
            GridBagLayout la = new GridBagLayout();
            GridBagConstraints c = new GridBagConstraints();
            this.setLayout(la);
            c.gridwidth = 1;
            c.weightx = 1.0;
            c.insets = new Insets(0, 0, 0, 0);
            c.anchor = 17;
            JLabel l = new JLabel();
            Mnemonics.setLocalizedText((JLabel)l, (String)NbBundle.getMessage(FontEditor.class, (String)"CTL_Font"));
            l.setLabelFor(this.lFont);
            la.setConstraints(l, c);
            this.add(l);
            c.insets = new Insets(0, 5, 0, 0);
            l = new JLabel();
            Mnemonics.setLocalizedText((JLabel)l, (String)NbBundle.getMessage(FontEditor.class, (String)"CTL_FontStyle"));
            l.setLabelFor(this.lStyle);
            la.setConstraints(l, c);
            this.add(l);
            c.insets = new Insets(0, 5, 0, 0);
            c.gridwidth = 0;
            l = new JLabel();
            Mnemonics.setLocalizedText((JLabel)l, (String)NbBundle.getMessage(FontEditor.class, (String)"CTL_Size"));
            l.setLabelFor(this.tfSize);
            la.setConstraints(l, c);
            this.add(l);
            c.insets = new Insets(5, 0, 0, 0);
            c.gridwidth = 1;
            c.fill = 2;
            this.tfFont = new JTextField(font.getName());
            this.tfFont.setEnabled(false);
            la.setConstraints(this.tfFont, c);
            this.add(this.tfFont);
            c.insets = new Insets(5, 5, 0, 0);
            this.tfStyle = new JTextField(FontEditor.this.getStyleName(font.getStyle()));
            this.tfStyle.setEnabled(false);
            la.setConstraints(this.tfStyle, c);
            this.add(this.tfStyle);
            c.insets = new Insets(5, 5, 0, 0);
            c.gridwidth = 0;
            this.tfSize.addKeyListener(new KeyAdapter(FontEditor.this){
                final /* synthetic */ FontEditor val$this$0;

                @Override
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == 10) {
                        FontPanel.this.setValue();
                    }
                }
            });
            this.tfSize.addFocusListener(new FocusAdapter(FontEditor.this){
                final /* synthetic */ FontEditor val$this$0;

                @Override
                public void focusLost(FocusEvent evt) {
                    if (FontPanel.this.dontSetValue) {
                        return;
                    }
                    FontPanel.this.dontSetValue = true;
                    Component c = evt.getOppositeComponent();
                    if (c != null) {
                        if (c instanceof JButton) {
                            if (((JButton)c).getText().equals(NbBundle.getMessage(FontEditor.class, (String)"CTL_OK"))) {
                                FontPanel.this.setValue();
                            }
                        } else {
                            FontPanel.this.setValue();
                        }
                    }
                }

                @Override
                public void focusGained(FocusEvent evt) {
                    FontPanel.this.dontSetValue = false;
                }
            });
            la.setConstraints(this.tfSize, c);
            this.add(this.tfSize);
            c.gridwidth = 1;
            c.insets = new Insets(5, 0, 0, 0);
            c.fill = 1;
            c.weightx = 1.0;
            c.weighty = 1.0;
            this.lFont.setVisibleRowCount(5);
            this.lFont.setSelectedValue(font.getName(), true);
            this.lFont.addListSelectionListener(new ListSelectionListener(FontEditor.this){
                final /* synthetic */ FontEditor val$this$0;

                @Override
                public void valueChanged(ListSelectionEvent e) {
                    if (!FontPanel.this.lFont.isSelectionEmpty() && FontEditor.getFonts().length > 0) {
                        int i = FontPanel.this.lFont.getSelectedIndex();
                        FontPanel.this.tfFont.setText(FontEditor.getFonts()[i]);
                        FontPanel.this.setValue();
                    }
                }
            });
            JScrollPane sp = new JScrollPane(this.lFont);
            sp.setVerticalScrollBarPolicy(22);
            la.setConstraints(sp, c);
            this.positionScrollPaneOnSelected(sp, this.lFont);
            this.add(sp);
            this.lStyle.setVisibleRowCount(5);
            this.lStyle.setSelectedValue(FontEditor.this.getStyleName(font.getStyle()), true);
            this.lStyle.addListSelectionListener(new ListSelectionListener(FontEditor.this){
                final /* synthetic */ FontEditor val$this$0;

                @Override
                public void valueChanged(ListSelectionEvent e) {
                    if (!FontPanel.this.lStyle.isSelectionEmpty()) {
                        int i = FontPanel.this.lStyle.getSelectedIndex();
                        FontPanel.this.tfStyle.setText(FontEditor.styles[i]);
                        FontPanel.this.setValue();
                    }
                }
            });
            sp = new JScrollPane(this.lStyle);
            sp.setVerticalScrollBarPolicy(22);
            c.insets = new Insets(5, 5, 0, 0);
            la.setConstraints(sp, c);
            this.positionScrollPaneOnSelected(sp, this.lStyle);
            this.add(sp);
            c.gridwidth = 0;
            this.lSize.getAccessibleContext().setAccessibleName(this.tfSize.getAccessibleContext().getAccessibleName());
            this.lSize.setVisibleRowCount(5);
            this.updateSizeList(font.getSize());
            this.lSize.addListSelectionListener(new ListSelectionListener(FontEditor.this){
                final /* synthetic */ FontEditor val$this$0;

                @Override
                public void valueChanged(ListSelectionEvent e) {
                    if (!FontPanel.this.lSize.isSelectionEmpty()) {
                        int i = FontPanel.this.lSize.getSelectedIndex();
                        FontPanel.this.tfSize.setText(String.valueOf(FontEditor.sizes[i]));
                        FontPanel.this.setValue();
                    }
                }
            });
            sp = new JScrollPane(this.lSize);
            sp.setVerticalScrollBarPolicy(22);
            c.insets = new Insets(5, 5, 0, 0);
            la.setConstraints(sp, c);
            this.positionScrollPaneOnSelected(sp, this.lSize);
            this.add(sp);
            c.gridwidth = 0;
            c.weighty = 0.0;
            JPanel p = new JPanel(new BorderLayout());
            p.setBorder(new TitledBorder(" " + NbBundle.getMessage(FontEditor.class, (String)"CTL_Preview") + " "));
            JPanel pp = new JPanel(FontEditor.this){
                final /* synthetic */ FontEditor val$this$0;

                @Override
                public Dimension getPreferredSize() {
                    return new Dimension(150, 60);
                }

                @Override
                public void paint(Graphics g) {
                    FontEditor.this.paintText(g, new Rectangle(0, 0, this.getSize().width - 1, this.getSize().height - 1), NbBundle.getMessage(FontEditor.class, (String)"MSG_Preview"));
                }
            };
            p.add("Center", pp);
            c.insets = new Insets(12, 0, 0, 0);
            la.setConstraints(p, c);
            this.add(p);
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(400, 300);
        }

        private void updateSizeList(int size) {
            if (Arrays.asList(FontEditor.sizes).contains(size)) {
                this.lSize.setSelectedValue(size, true);
            } else {
                this.lSize.clearSelection();
            }
        }

        private void setValue() {
            int size = 12;
            try {
                size = Integer.parseInt(this.tfSize.getText());
                if (size <= 0) {
                    IllegalArgumentException iae = new IllegalArgumentException();
                    UIExceptions.annotateUser(iae, null, size == 0 ? NbBundle.getMessage(FontEditor.class, (String)"CTL_InvalidValueWithParam", (Object)this.tfSize.getText()) : NbBundle.getMessage(FontEditor.class, (String)"CTL_NegativeSize"), null, null);
                    this.tfSize.setText(String.valueOf(FontEditor.this.font.getSize()));
                    throw iae;
                }
                this.updateSizeList(size);
            }
            catch (NumberFormatException e) {
                UIExceptions.annotateUser(e, null, NbBundle.getMessage(FontEditor.class, (String)"CTL_InvalidValueWithExc", (Object)e), null, null);
                this.tfSize.setText(String.valueOf(FontEditor.this.font.getSize()));
                throw e;
            }
            int i = this.lStyle.getSelectedIndex();
            int ii = 0;
            switch (i) {
                case 0: {
                    ii = 0;
                    break;
                }
                case 1: {
                    ii = 1;
                    break;
                }
                case 2: {
                    ii = 2;
                    break;
                }
                case 3: {
                    ii = 3;
                }
            }
            FontEditor.this.setValue(new Font(this.tfFont.getText(), ii, size));
            this.invalidate();
            Container p = this.getParent();
            if (p != null) {
                p.validate();
            }
            this.repaint();
        }

        private void positionScrollPaneOnSelected(JScrollPane scroll, JList list) {
            if (list.getSelectedIndex() != -1) {
                int start = list.getSelectedIndex() - list.getVisibleRowCount() / 2;
                Rectangle selected = list.getCellBounds(start < 0 ? 0 : start, list.getSelectedIndex());
                scroll.getViewport().setViewPosition(new Point(selected.x, selected.y));
            }
        }

    }

}

