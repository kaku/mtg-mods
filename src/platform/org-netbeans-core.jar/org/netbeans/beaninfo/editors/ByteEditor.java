/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.beaninfo.editors;

import org.netbeans.beaninfo.editors.WrappersEditor;

public class ByteEditor
extends WrappersEditor {
    public ByteEditor() {
        super(Byte.TYPE);
    }

    public String getJavaInitializationString() {
        return "new java.lang.Byte((byte)" + this.getAsText() + ")";
    }
}

