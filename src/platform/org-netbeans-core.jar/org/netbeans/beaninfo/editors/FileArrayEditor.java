/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.ExPropertyEditor
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.awt.Component;
import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditorSupport;
import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import org.netbeans.beaninfo.editors.FileEditor;
import org.netbeans.beaninfo.editors.StringCustomEditor;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

public class FileArrayEditor
extends PropertyEditorSupport
implements ExPropertyEditor,
PropertyChangeListener {
    private static final String ANCESTOR = "ancestor";
    private int mode = 2;
    private boolean directories = true;
    private boolean fileHiding = false;
    private boolean files = true;
    private javax.swing.filechooser.FileFilter fileFilter;
    private File currentDirectory;
    private File baseDirectory;
    private ThreadLocal<String> myPropertyFired = new ThreadLocal();
    private JFileChooser chooser;
    private boolean editable = true;

    public void attachEnv(PropertyEnv env) {
        Object fil;
        Object baseDir;
        Object filter;
        this.directories = true;
        this.files = true;
        this.fileFilter = null;
        this.fileHiding = false;
        Object dirs = env.getFeatureDescriptor().getValue("directories");
        if (dirs instanceof Boolean) {
            this.directories = (Boolean)dirs;
        }
        if ((fil = env.getFeatureDescriptor().getValue("files")) instanceof Boolean) {
            this.files = (Boolean)fil;
        }
        if ((filter = env.getFeatureDescriptor().getValue("filter")) instanceof FilenameFilter) {
            this.fileFilter = new FileEditor.DelegatingFilenameFilter((FilenameFilter)filter);
        } else if (filter instanceof javax.swing.filechooser.FileFilter) {
            this.fileFilter = (javax.swing.filechooser.FileFilter)filter;
        } else if (filter instanceof FileFilter) {
            this.fileFilter = new FileEditor.DelegatingFileFilter((FileFilter)filter);
        }
        Object curDir = env.getFeatureDescriptor().getValue("currentDir");
        if (curDir instanceof File) {
            this.currentDirectory = (File)curDir;
            if (!this.currentDirectory.isDirectory()) {
                Logger.getAnonymousLogger().warning("java.io.File will not accept currentDir=" + this.baseDirectory);
                this.currentDirectory = null;
            }
        }
        if ((baseDir = env.getFeatureDescriptor().getValue("baseDir")) instanceof File) {
            this.baseDirectory = (File)baseDir;
            if (!this.baseDirectory.isDirectory() || !this.baseDirectory.isAbsolute()) {
                Logger.getAnonymousLogger().warning("java.io.File will not accept baseDir=" + this.baseDirectory);
                this.baseDirectory = null;
            }
        }
        this.mode = this.files ? (this.directories ? 2 : 0) : (this.directories ? 1 : 2);
        Object fileHide = env.getFeatureDescriptor().getValue("file_hiding");
        if (fileHide instanceof Boolean) {
            this.fileHiding = (Boolean)fileHide;
        }
        if (env.getFeatureDescriptor() instanceof Node.Property) {
            Node.Property prop = (Node.Property)env.getFeatureDescriptor();
            this.editable = prop.canWrite();
        }
    }

    @Override
    public String getAsText() {
        File[] file = (File[])this.getValue();
        if (file == null) {
            return "";
        }
        StringBuilder path = new StringBuilder("[");
        for (int i = 0; i < file.length; ++i) {
            path.append(file[i].getPath());
        }
        if (file.length == 0) {
            path.append('.');
        }
        return path.append(']').toString();
    }

    @Override
    public void setAsText(String str) throws IllegalArgumentException {
        if (str == null) {
            throw new IllegalArgumentException("null");
        }
        if ("".equals(str)) {
            this.setValue(null);
            return;
        }
    }

    @Override
    public Component getCustomEditor() {
        if (!this.editable) {
            return new StringCustomEditor(this.getAsText(), false);
        }
        if (this.chooser == null) {
            File[] ofile;
            this.chooser = FileEditor.createHackedFileChooser();
            this.chooser.setMultiSelectionEnabled(true);
            Object vv = this.getValue();
            File originalFile = null;
            if (vv instanceof File[] && (ofile = (File[])vv).length > 0 && (originalFile = ofile[0]) != null && !originalFile.isAbsolute() && this.baseDirectory != null) {
                originalFile = new File(this.baseDirectory, originalFile.getPath());
            }
            if (this.currentDirectory != null) {
                this.chooser.setCurrentDirectory(this.currentDirectory);
            } else if (originalFile != null && originalFile.getParentFile() != null) {
                this.chooser.setCurrentDirectory(originalFile.getParentFile());
                this.chooser.setSelectedFile(originalFile);
            } else if (FileEditor.lastCurrentDir != null) {
                this.chooser.setCurrentDirectory(FileEditor.lastCurrentDir);
            }
            this.chooser.setFileSelectionMode(this.mode);
            if (this.fileFilter != null) {
                this.chooser.setFileFilter(this.fileFilter);
            }
            switch (this.mode) {
                case 2: {
                    this.chooser.setDialogTitle(FileArrayEditor.getString("CTL_DialogTitleFilesAndDirs"));
                    break;
                }
                case 0: {
                    this.chooser.setDialogTitle(FileArrayEditor.getString("CTL_DialogTitleFiles"));
                    break;
                }
                case 1: {
                    this.chooser.setDialogTitle(FileArrayEditor.getString("CTL_DialogTitleDirs"));
                }
            }
            this.chooser.setFileHidingEnabled(this.fileHiding);
            this.chooser.setControlButtonsAreShown(false);
            this.chooser.addPropertyChangeListener(this);
            HelpCtx.setHelpIDString((JComponent)this.chooser, (String)this.getHelpCtx().getHelpID());
        }
        return this.chooser;
    }

    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

    @Override
    public String getJavaInitializationString() {
        File[] value = (File[])this.getValue();
        if (value == null) {
            return "null";
        }
        StringBuilder retVal = new StringBuilder("new java.io.File[] { ");
        for (int i = 0; i < value.length; ++i) {
            if (this.baseDirectory != null && !value[i].isAbsolute()) {
                retVal.append("new java.io.File(").append(FileEditor.stringify(this.baseDirectory.getPath())).append(", ").append(FileEditor.stringify(value[i].getPath())).append("), ");
                continue;
            }
            retVal.append("new java.io.File(").append(FileEditor.stringify(value[i].getAbsolutePath())).append("), ");
        }
        return retVal.append(" }").toString();
    }

    private HelpCtx getHelpCtx() {
        return new HelpCtx(FileEditor.class);
    }

    private static String getString(String key) {
        return NbBundle.getBundle(FileArrayEditor.class).getString(key);
    }

    @Override
    public void propertyChange(PropertyChangeEvent e) {
        int i;
        if ("ancestor".equals(e.getPropertyName())) {
            this.myPropertyFired.set(null);
        }
        if (e.getSource() instanceof JFileChooser) {
            File dir;
            JFileChooser jfc = (JFileChooser)e.getSource();
            if (this.mode == 1 && "directoryChanged".equals(e.getPropertyName()) && jfc.getSelectedFile() == null && (dir = jfc.getCurrentDirectory()) != null) {
                this.setValue(new File[]{new File(dir.getAbsolutePath())});
                return;
            }
        }
        if (!"SelectedFilesChangedProperty".equals(e.getPropertyName()) && !"SelectedFileChangedProperty".equals(e.getPropertyName())) {
            return;
        }
        if (!(e.getSource() instanceof JFileChooser)) {
            return;
        }
        JFileChooser chooser = (JFileChooser)e.getSource();
        File[] f = chooser.getSelectedFiles();
        if (f == null) {
            return;
        }
        if (this.isAlreadyHandled(chooser, f, e.getPropertyName())) {
            return;
        }
        if (f.length == 0 && chooser.getSelectedFile() != null) {
            f = new File[]{chooser.getSelectedFile()};
        }
        for (i = 0; i < f.length; ++i) {
            if (!this.files && f[i].isFile()) {
                return;
            }
            if (this.directories || !f[i].isDirectory()) continue;
            return;
        }
        if (this.baseDirectory != null) {
            for (i = 0; i < f.length; ++i) {
                String rel = FileEditor.getChildRelativePath(this.baseDirectory, f[i]);
                if (rel == null) continue;
                f[i] = new File(rel);
            }
        }
        File[] nf = new File[f.length];
        for (int i2 = 0; i2 < f.length; ++i2) {
            nf[i2] = new File(f[i2].getAbsolutePath());
        }
        this.setValue(nf);
        FileEditor.lastCurrentDir = chooser.getCurrentDirectory();
    }

    private boolean isAlreadyHandled(JFileChooser chooser, File[] files, String property) {
        boolean contains;
        HashSet<File> fileSet = new HashSet<File>(Arrays.asList(files));
        File file = chooser.getSelectedFile();
        boolean bl = contains = file == null && files.length == 0 || file != null && fileSet.contains(file);
        if (!contains || property.equals(this.myPropertyFired.get())) {
            this.myPropertyFired.set(null);
            return false;
        }
        if (this.isFired()) {
            this.myPropertyFired.set(null);
            return true;
        }
        this.myPropertyFired.set(property);
        return false;
    }

    private boolean isFired() {
        return this.myPropertyFired.get() != null;
    }
}

