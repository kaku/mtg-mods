/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.editors.XMLPropertyEditor
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyEditor;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;
import javax.accessibility.AccessibleContext;
import javax.swing.Icon;
import javax.swing.JColorChooser;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.colorchooser.AbstractColorChooserPanel;
import javax.swing.colorchooser.ColorSelectionModel;
import javax.swing.colorchooser.DefaultColorSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.netbeans.core.UIExceptions;
import org.openide.explorer.propertysheet.editors.XMLPropertyEditor;
import org.openide.util.NbBundle;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public final class ColorEditor
implements PropertyEditor,
XMLPropertyEditor {
    public static final int AWT_PALETTE = 1;
    public static final int SYSTEM_PALETTE = 2;
    public static final int SWING_PALETTE = 3;
    static String[] awtColorNames;
    private static final Color[] awtColors;
    private static final String[] awtGenerate;
    private static String[] systemColorNames;
    private static final String[] systemGenerate;
    private static final Color[] systemColors;
    private static String[] swingColorNames;
    private static Color[] swingColors;
    static final boolean GTK;
    static final boolean AQUA;
    private static final boolean antialias;
    private static Boolean gtkAA;
    private static Map hintsMap;
    private SuperColor superColor;
    private PropertyChangeSupport support;
    public static final String XML_COLOR = "Color";
    public static final String ATTR_TYPE = "type";
    public static final String ATTR_RED = "red";
    public static final String ATTR_GREEN = "green";
    public static final String ATTR_BLUE = "blue";
    public static final String ATTR_ID = "id";
    public static final String ATTR_PALETTE = "palette";
    public static final String VALUE_PALETTE = "palette";
    public static final String VALUE_RGB = "rgb";
    public static final String VALUE_NULL = "null";

    public static JColorChooser getStaticChooser(ColorEditor ce) {
        JColorChooser staticChooser = new JColorChooser(new SuperColorSelectionModel()){

            @Override
            public void setColor(Color c) {
                if (c == null) {
                    return;
                }
                super.setColor(c);
            }
        };
        staticChooser.addChooserPanel(new NbColorChooserPanel(1, ColorEditor.getAWTColorNames(), awtColors, ColorEditor.getString("CTL_AWTPalette"), ce));
        ColorEditor.initSwingConstants();
        staticChooser.addChooserPanel(new NbColorChooserPanel(3, swingColorNames, swingColors, ColorEditor.getString("CTL_SwingPalette"), ce));
        staticChooser.addChooserPanel(new NbColorChooserPanel(2, ColorEditor.getSystemColorNames(), systemColors, ColorEditor.getString("CTL_SystemPalette"), ce));
        return staticChooser;
    }

    public ColorEditor() {
        this.support = new PropertyChangeSupport(this);
    }

    @Override
    public Object getValue() {
        if (this.superColor != null) {
            if (this.superColor.getID() != null) {
                return this.superColor;
            }
            return this.superColor.getColor();
        }
        return null;
    }

    @Override
    public void setValue(Object object) {
        if (object != null) {
            if (object instanceof SuperColor) {
                this.superColor = (SuperColor)object;
            } else if (object instanceof Color) {
                this.superColor = new SuperColor((Color)object);
            }
        } else {
            this.superColor = null;
        }
        this.support.firePropertyChange("", null, null);
    }

    @Override
    public String getAsText() {
        if (this.superColor == null) {
            return "null";
        }
        return this.superColor.getAsText();
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if (text == null) {
            throw new IllegalArgumentException("null parameter");
        }
        if ("null".equals(text = text.trim())) {
            this.setValue(null);
            return;
        }
        try {
            int len = text.length();
            if (len > 0) {
                int start = -1;
                int end = -1;
                char c1 = text.charAt(0);
                char c2 = text.charAt(len - 1);
                if (c1 == '[' && c2 == ']') {
                    start = 1;
                    end = len - 1;
                } else if (c1 >= '0' && c1 <= '9' && c2 >= '0' && c2 <= '9') {
                    start = 0;
                    end = len;
                }
                if (start >= 0) {
                    int index2;
                    int index1 = text.indexOf(44);
                    int n = index2 = index1 < 0 ? -1 : text.indexOf(44, index1 + 1);
                    if (index1 >= 0 && index2 >= 0) {
                        int red = Integer.parseInt(text.substring(start, index1).trim());
                        int green = Integer.parseInt(text.substring(index1 + 1, index2).trim());
                        int blue = Integer.parseInt(text.substring(index2 + 1, end).trim());
                        try {
                            this.setValue(new SuperColor(null, 0, new Color(red, green, blue)));
                            return;
                        }
                        catch (IllegalArgumentException iaE) {
                            UIExceptions.annotateUser(iaE, null, iaE.getLocalizedMessage(), null, null);
                            throw iaE;
                        }
                    }
                }
            }
        }
        catch (NumberFormatException nfe) {
            // empty catch block
        }
        int palette = 0;
        Color color = null;
        int index = ColorEditor.getIndex(awtGenerate, text);
        if (index >= 0) {
            text = ColorEditor.getAWTColorNames()[index];
        }
        if ((index = ColorEditor.getIndex(ColorEditor.getAWTColorNames(), text)) >= 0) {
            palette = 1;
            color = awtColors[index];
        }
        if (index < 0 && (index = ColorEditor.getIndex(ColorEditor.getSystemColorNames(), text)) >= 0) {
            palette = 2;
            color = systemColors[index];
        }
        if (index < 0) {
            ColorEditor.initSwingConstants();
            index = ColorEditor.getIndex(swingColorNames, text);
            if (index >= 0) {
                palette = 3;
                color = swingColors[index];
            }
        }
        if (index < 0) {
            String msg = MessageFormat.format(NbBundle.getMessage(ColorEditor.class, (String)"FMT_IllegalEntry"), text);
            IllegalArgumentException iae = new IllegalArgumentException(text);
            UIExceptions.annotateUser(iae, text, msg, null, null);
            throw iae;
        }
        this.setValue(new SuperColor(text, palette, color));
    }

    @Override
    public String getJavaInitializationString() {
        if (this.superColor == null) {
            return "null";
        }
        if (this.superColor.getID() == null) {
            return "new java.awt.Color(" + this.superColor.getRed() + ", " + this.superColor.getGreen() + ", " + this.superColor.getBlue() + ")";
        }
        switch (this.superColor.getPalette()) {
            default: {
                return "java.awt.Color." + awtGenerate[ColorEditor.getIndex(ColorEditor.getAWTColorNames(), this.superColor.getID())];
            }
            case 2: {
                return "java.awt.SystemColor." + systemGenerate[ColorEditor.getIndex(ColorEditor.getSystemColorNames(), this.superColor.getID())];
            }
            case 3: 
        }
        if (this.superColor.getID() == null) {
            return "new java.awt.Color(" + this.superColor.getRed() + ", " + this.superColor.getGreen() + ", " + this.superColor.getBlue() + ")";
        }
        return "javax.swing.UIManager.getDefaults().getColor(\"" + this.superColor.getID() + "\")";
    }

    @Override
    public String[] getTags() {
        if (this.superColor == null) {
            return ColorEditor.getAWTColorNames();
        }
        switch (this.superColor.getPalette()) {
            case 1: {
                return ColorEditor.getAWTColorNames();
            }
            case 2: {
                return ColorEditor.getSystemColorNames();
            }
            case 3: {
                ColorEditor.initSwingConstants();
                return swingColorNames;
            }
        }
        return null;
    }

    @Override
    public boolean isPaintable() {
        return true;
    }

    @Override
    public void paintValue(Graphics g, Rectangle rectangle) {
        int px;
        ((Graphics2D)g).setRenderingHints(ColorEditor.getHints());
        if (this.superColor != null) {
            Color color = g.getColor();
            g.drawRect(rectangle.x, rectangle.y + rectangle.height / 2 - 5, 10, 10);
            g.setColor(this.superColor);
            g.fillRect(rectangle.x + 1, rectangle.y + rectangle.height / 2 - 4, 9, 9);
            g.setColor(color);
            px = 18;
        } else {
            px = 0;
        }
        FontMetrics fm = g.getFontMetrics();
        g.drawString(this.getAsText(), rectangle.x + px, rectangle.y + (rectangle.height - fm.getHeight()) / 2 + fm.getAscent());
    }

    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

    @Override
    public Component getCustomEditor() {
        return new NbColorChooser(this, ColorEditor.getStaticChooser(this));
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this.support.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this.support.removePropertyChangeListener(propertyChangeListener);
    }

    private static synchronized String[] getAWTColorNames() {
        if (awtColorNames == null) {
            awtColorNames = new String[]{ColorEditor.getString("LAB_White"), ColorEditor.getString("LAB_LightGray"), ColorEditor.getString("LAB_Gray"), ColorEditor.getString("LAB_DarkGray"), ColorEditor.getString("LAB_Black"), ColorEditor.getString("LAB_Red"), ColorEditor.getString("LAB_Pink"), ColorEditor.getString("LAB_Orange"), ColorEditor.getString("LAB_Yellow"), ColorEditor.getString("LAB_Green"), ColorEditor.getString("LAB_Magenta"), ColorEditor.getString("LAB_Cyan"), ColorEditor.getString("LAB_Blue")};
        }
        return awtColorNames;
    }

    private static synchronized String[] getSystemColorNames() {
        if (systemColorNames == null) {
            systemColorNames = new String[]{ColorEditor.getString("LAB_ActiveCaption"), ColorEditor.getString("LAB_ActiveCaptionBorder"), ColorEditor.getString("LAB_ActiveCaptionText"), ColorEditor.getString("LAB_Control"), ColorEditor.getString("LAB_ControlDkShadow"), ColorEditor.getString("LAB_ControlHighlight"), ColorEditor.getString("LAB_ControlLtHighlight"), ColorEditor.getString("LAB_ControlShadow"), ColorEditor.getString("LAB_ControlText"), ColorEditor.getString("LAB_Desktop"), ColorEditor.getString("LAB_InactiveCaption"), ColorEditor.getString("LAB_InactiveCaptionBorder"), ColorEditor.getString("LAB_InactiveCaptionText"), ColorEditor.getString("LAB_Info"), ColorEditor.getString("LAB_InfoText"), ColorEditor.getString("LAB_Menu"), ColorEditor.getString("LAB_MenuText"), ColorEditor.getString("LAB_Scrollbar"), ColorEditor.getString("LAB_Text"), ColorEditor.getString("LAB_TextHighlight"), ColorEditor.getString("LAB_TextHighlightText"), ColorEditor.getString("LAB_TextInactiveText"), ColorEditor.getString("LAB_TextText"), ColorEditor.getString("LAB_Window"), ColorEditor.getString("LAB_WindowBorder"), ColorEditor.getString("LAB_WindowText")};
        }
        return systemColorNames;
    }

    private static String getString(String key) {
        return NbBundle.getBundle(ColorEditor.class).getString(key);
    }

    private static int getIndex(Object[] names, Object name) {
        for (int i = 0; i < names.length; ++i) {
            if (!name.equals(names[i])) continue;
            return i;
        }
        return -1;
    }

    private static void initSwingConstants() {
        if (swingColorNames != null) {
            return;
        }
        UIDefaults def = UIManager.getDefaults();
        Enumeration e = def.keys();
        TreeSet<String> names = new TreeSet<String>();
        while (e.hasMoreElements()) {
            Object v;
            Object k = e.nextElement();
            if (!(k instanceof String) || !((v = def.get(k)) instanceof Color)) continue;
            names.add((String)k);
        }
        swingColorNames = new String[names.size()];
        names.toArray(swingColorNames);
        swingColors = new Color[swingColorNames.length];
        int k = swingColorNames.length;
        for (int i = 0; i < k; ++i) {
            ColorEditor.swingColors[i] = (Color)def.get(swingColorNames[i]);
        }
    }

    private SuperColor getSuperColor() {
        return this.superColor;
    }

    public void readFromXML(Node element) throws IOException {
        if (!"Color".equals(element.getNodeName())) {
            throw new IOException();
        }
        NamedNodeMap attributes = element.getAttributes();
        try {
            String type = attributes.getNamedItem("type").getNodeValue();
            if ("null".equals(type)) {
                this.setValue(null);
            } else {
                String red = attributes.getNamedItem("red").getNodeValue();
                String green = attributes.getNamedItem("green").getNodeValue();
                String blue = attributes.getNamedItem("blue").getNodeValue();
                if ("palette".equals(type)) {
                    int palette = Integer.parseInt(attributes.getNamedItem("palette").getNodeValue());
                    String id = attributes.getNamedItem("id").getNodeValue();
                    if (palette == 1) {
                        int idx = ColorEditor.getIndex(awtGenerate, id);
                        id = idx >= 0 ? ColorEditor.getAWTColorNames()[idx] : (ColorEditor.getIndex(ColorEditor.getAWTColorNames(), id) >= 0 ? id : null);
                    }
                    this.setValue(new SuperColor(id, palette, new Color(Integer.parseInt(red, 16), Integer.parseInt(green, 16), Integer.parseInt(blue, 16))));
                } else {
                    this.setValue(new SuperColor(new Color(Integer.parseInt(red, 16), Integer.parseInt(green, 16), Integer.parseInt(blue, 16))));
                }
            }
        }
        catch (NullPointerException e) {
            throw new IOException();
        }
    }

    public Node storeToXML(Document doc) {
        Element el = doc.createElement("Color");
        el.setAttribute("type", this.superColor == null ? "null" : (this.superColor.getID() == null ? "rgb" : "palette"));
        if (this.superColor != null) {
            el.setAttribute("red", Integer.toHexString(this.superColor.getRed()));
            el.setAttribute("green", Integer.toHexString(this.superColor.getGreen()));
            el.setAttribute("blue", Integer.toHexString(this.superColor.getBlue()));
            if (this.superColor.getID() != null) {
                if (this.superColor.getPalette() == 1) {
                    el.setAttribute("id", awtGenerate[ColorEditor.getIndex(ColorEditor.getAWTColorNames(), this.superColor.getID())]);
                } else {
                    el.setAttribute("id", this.superColor.getID());
                }
                el.setAttribute("palette", Integer.toString(this.superColor.getPalette()));
            }
        }
        return el;
    }

    public static final boolean gtkShouldAntialias() {
        if (gtkAA == null) {
            Object o = Toolkit.getDefaultToolkit().getDesktopProperty("gnome.Xft/Antialias");
            gtkAA = Integer.valueOf(1).equals(o);
        }
        return gtkAA;
    }

    private static Map getHints() {
        if (hintsMap == null && (ColorEditor.hintsMap = (Map)Toolkit.getDefaultToolkit().getDesktopProperty("awt.font.desktophints")) == null) {
            hintsMap = new HashMap();
            if (antialias) {
                hintsMap.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            }
        }
        return hintsMap;
    }

    static {
        awtColors = new Color[]{Color.white, Color.lightGray, Color.gray, Color.darkGray, Color.black, Color.red, Color.pink, Color.orange, Color.yellow, Color.green, Color.magenta, Color.cyan, Color.blue};
        awtGenerate = new String[]{"white", "lightGray", "gray", "darkGray", "black", "red", "pink", "orange", "yellow", "green", "magenta", "cyan", "blue"};
        systemGenerate = new String[]{"activeCaption", "activeCaptionBorder", "activeCaptionText", "control", "controlDkShadow", "controlHighlight", "controlLtHighlight", "controlShadow", "controlText", "desktop", "inactiveCaption", "inactiveCaptionBorder", "inactiveCaptionText", "info", "infoText", "menu", "menuText", "scrollbar", "text", "textHighlight", "textHighlightText", "textInactiveText", "textText", "window", "windowBorder", "windowText"};
        systemColors = new Color[]{SystemColor.activeCaption, SystemColor.activeCaptionBorder, SystemColor.activeCaptionText, SystemColor.control, SystemColor.controlDkShadow, SystemColor.controlHighlight, SystemColor.controlLtHighlight, SystemColor.controlShadow, SystemColor.controlText, SystemColor.desktop, SystemColor.inactiveCaption, SystemColor.inactiveCaptionBorder, SystemColor.inactiveCaptionText, SystemColor.info, SystemColor.infoText, SystemColor.menu, SystemColor.menuText, SystemColor.scrollbar, SystemColor.text, SystemColor.textHighlight, SystemColor.textHighlightText, SystemColor.textInactiveText, SystemColor.textText, SystemColor.window, SystemColor.windowBorder, SystemColor.windowText};
        GTK = "GTK".equals(UIManager.getLookAndFeel().getID());
        AQUA = "Aqua".equals(UIManager.getLookAndFeel().getID());
        antialias = Boolean.getBoolean("nb.cellrenderer.antialiasing") || Boolean.getBoolean("swing.aatext") || GTK && ColorEditor.gtkShouldAntialias() || AQUA;
        UIManager.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                swingColorNames = null;
                swingColors = null;
            }
        });
        swingColorNames = null;
        swingColors = null;
    }

    private static final class NbColorChooserPanel
    extends AbstractColorChooserPanel
    implements ListSelectionListener {
        static final long serialVersionUID = -2792992315444428631L;
        private JList list;
        String[] names;
        Color[] colors;
        int palette;
        ColorEditor ce;
        private String displayName;

        NbColorChooserPanel(int palette, String[] names, Color[] colors, String displayName, ColorEditor ce) {
            this.names = names;
            this.colors = colors;
            this.palette = palette;
            this.displayName = displayName;
            this.ce = ce;
        }

        @Override
        protected void buildChooser() {
            this.setLayout(new BorderLayout());
            this.list = new JList<String>(this.names);
            this.add("Center", new JScrollPane(this.list));
            this.list.setCellRenderer(new MyListCellRenderer());
            this.list.addListSelectionListener(this);
            this.list.getAccessibleContext().setAccessibleName(this.displayName);
        }

        @Override
        public void updateChooser() {
            String id;
            SuperColor sc = this.ce.getSuperColor();
            if (sc != null && this.palette == sc.getPalette() && (id = sc.getID()) != null && this.names != null) {
                int i = ColorEditor.getIndex(this.names, id);
                this.list.setSelectedIndex(i);
            } else {
                this.list.clearSelection();
            }
        }

        @Override
        public String getDisplayName() {
            return this.displayName;
        }

        @Override
        public Icon getSmallDisplayIcon() {
            return null;
        }

        @Override
        public Icon getLargeDisplayIcon() {
            return null;
        }

        @Override
        public void valueChanged(ListSelectionEvent e) {
            if (!this.list.isSelectionEmpty()) {
                int i = this.list.getSelectedIndex();
                this.getColorSelectionModel().setSelectedColor(new SuperColor(this.names[i], this.palette, this.colors[i]));
            }
        }

        public void setColor(Color newColor) {
            this.getColorSelectionModel().setSelectedColor(newColor);
        }

        public Color getColor() {
            return this.getColorFromModel();
        }

        private final class MyListCellRenderer
        extends JPanel
        implements ListCellRenderer {
            private boolean selected;
            private boolean hasFocus;
            private int index;
            static final long serialVersionUID = -8877709520578055594L;

            public MyListCellRenderer() {
                this.setOpaque(true);
                this.setBorder(new EmptyBorder(1, 1, 1, 1));
            }

            @Override
            public Dimension getPreferredSize() {
                try {
                    FontMetrics fontMetrics = this.getFontMetrics(this.getFont());
                    return new Dimension(fontMetrics.stringWidth(NbColorChooserPanel.this.names[this.index]) + 30, fontMetrics.getHeight() + 4);
                }
                catch (NullPointerException e) {
                    return new Dimension(10, 10);
                }
            }

            @Override
            public void paint(Graphics g) {
                ((Graphics2D)g).setRenderingHints(ColorEditor.getHints());
                Dimension rectangle = this.getSize();
                Color color = g.getColor();
                if (this.selected) {
                    g.setColor(UIManager.getColor("List.selectionBackground"));
                } else {
                    g.setColor(UIManager.getColor("List.background"));
                }
                g.fillRect(0, 0, rectangle.width - 1, rectangle.height - 1);
                if (this.hasFocus) {
                    g.setColor(Color.black);
                    g.drawRect(0, 0, rectangle.width - 1, rectangle.height - 1);
                }
                g.setColor(Color.black);
                g.drawRect(6, rectangle.height / 2 - 5, 10, 10);
                g.setColor(NbColorChooserPanel.this.colors[this.index]);
                g.fillRect(7, rectangle.height / 2 - 4, 9, 9);
                if (this.selected) {
                    g.setColor(UIManager.getColor("List.selectionForeground"));
                } else {
                    g.setColor(UIManager.getColor("List.foreground"));
                }
                FontMetrics fm = g.getFontMetrics();
                g.drawString(NbColorChooserPanel.this.names[this.index], 22, (rectangle.height - fm.getHeight()) / 2 + fm.getAscent());
                g.setColor(color);
            }

            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                this.index = index;
                this.selected = isSelected;
                this.hasFocus = cellHasFocus;
                this.getAccessibleContext().setAccessibleName(NbColorChooserPanel.this.names[index]);
                return this;
            }
        }

    }

    static class SuperColor
    extends Color {
        static final long serialVersionUID = 6147637669184334151L;
        private String id = null;
        private int palette = 0;
        private Color color;

        SuperColor(Color color) {
            super(color.getRed(), color.getGreen(), color.getBlue());
            this.color = color;
        }

        SuperColor(String id, int palette, Color color) {
            super(color.getRed(), color.getGreen(), color.getBlue());
            this.color = color;
            this.id = id;
            this.palette = palette;
        }

        @Override
        public boolean equals(Object obj) {
            boolean superEquals = super.equals(obj);
            String objID = null;
            int objPalette = -1;
            if (!(obj instanceof SuperColor)) {
                return superEquals;
            }
            objID = ((SuperColor)obj).getID();
            objPalette = ((SuperColor)obj).getPalette();
            if (objID != null) {
                return superEquals && objID.equals(this.getID()) && objPalette == this.getPalette();
            }
            return superEquals && null == this.getID() && objPalette == this.getPalette();
        }

        private String getID() {
            return this.id;
        }

        private int getPalette() {
            return this.palette;
        }

        private Color getColor() {
            return this.color;
        }

        private String getAsText() {
            if (this.id != null) {
                return this.id;
            }
            return "[" + this.getRed() + "," + this.getGreen() + "," + this.getBlue() + "]";
        }
    }

    private static class NbColorChooser
    extends JPanel
    implements ChangeListener {
        private final ColorEditor editor;
        private final ColorSelectionModel selectionModel;
        static final long serialVersionUID = -6230228701104365037L;

        public NbColorChooser(ColorEditor editor, JColorChooser chooser) {
            this.editor = editor;
            this.selectionModel = chooser.getSelectionModel();
            this.setLayout(new BorderLayout());
            this.add((Component)chooser, "Center");
            chooser.setColor((Color)editor.getValue());
            this.selectionModel.addChangeListener(this);
            this.getAccessibleContext().setAccessibleDescription(ColorEditor.getString("ACSD_CustomColorEditor"));
        }

        @Override
        public void removeNotify() {
            super.removeNotify();
            this.selectionModel.removeChangeListener(this);
        }

        @Override
        public Dimension getPreferredSize() {
            Dimension s = super.getPreferredSize();
            return new Dimension(s.width + 50, s.height + 10);
        }

        @Override
        public void stateChanged(ChangeEvent evt) {
            this.editor.setValue(this.selectionModel.getSelectedColor());
        }
    }

    private static class SuperColorSelectionModel
    extends DefaultColorSelectionModel {
        private int reenter = -1;

        private SuperColorSelectionModel() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void setSelectedColor(Color color) {
            try {
                ++this.reenter;
                if (color instanceof SuperColor) {
                    super.setSelectedColor(color);
                } else if (color instanceof Color && this.reenter == 0) {
                    super.setSelectedColor(new SuperColor(color));
                }
            }
            finally {
                --this.reenter;
            }
        }
    }

}

