/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.beaninfo.editors;

import org.netbeans.beaninfo.editors.WrappersEditor;

public class ShortEditor
extends WrappersEditor {
    public ShortEditor() {
        super(Short.TYPE);
    }

    public String getJavaInitializationString() {
        return "new java.lang.Short((short)" + this.getAsText() + ")";
    }
}

