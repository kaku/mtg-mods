/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.beaninfo.editors;

import org.netbeans.beaninfo.editors.WrappersEditor;

public class CharacterEditor
extends WrappersEditor {
    public CharacterEditor() {
        super(Character.TYPE);
    }

    public String getJavaInitializationString() {
        if (((Character)this.getValue()).charValue() == '\'') {
            return "new java.lang.Character('\\'')";
        }
        return "new java.lang.Character('" + this.getAsText() + "')";
    }
}

