/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import javax.accessibility.AccessibleContext;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.text.JTextComponent;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.util.NbBundle;

public class StringCustomEditor
extends JPanel
implements PropertyChangeListener {
    static final long serialVersionUID = 7348579663907322425L;
    private static final int TOO_LARGE = 102400;
    boolean oneline = false;
    String instructions = null;
    private PropertyEnv env;
    private PropertyEditor editor;
    private JScrollPane textAreaScroll;
    private JTextComponent textArea;

    StringCustomEditor(String value, boolean editable, boolean oneline, String instructions, PropertyEditor editor, PropertyEnv env) {
        this.oneline = oneline;
        this.instructions = instructions;
        this.env = env;
        this.editor = editor;
        this.env.setState(PropertyEnv.STATE_NEEDS_VALIDATION);
        this.env.addPropertyChangeListener((PropertyChangeListener)this);
        this.init(value, editable);
    }

    public StringCustomEditor(String s, boolean editable) {
        this.init(s, editable);
    }

    private void init(String s, boolean editable) {
        this.setLayout(new BorderLayout());
        if (this.oneline) {
            this.textArea = new JTextField();
            this.add((Component)this.textArea, "Center");
        } else {
            this.textAreaScroll = new JScrollPane();
            this.textArea = new JTextArea();
            this.textAreaScroll.setViewportView(this.textArea);
            this.add((Component)this.textAreaScroll, "Center");
        }
        this.textArea.setEditable(editable);
        int from = 0;
        int to = 0;
        int ctn = 1;
        while ((to = s.indexOf("\n", from)) > 0) {
            ++ctn;
            from = to + 1;
        }
        this.textArea.setText(s);
        if (this.textArea instanceof JTextArea && s.length() < 102400) {
            ((JTextArea)this.textArea).setWrapStyleWord(true);
            ((JTextArea)this.textArea).setLineWrap(true);
            this.setPreferredSize(new Dimension(500, 300));
        } else {
            this.textArea.setMinimumSize(new Dimension(100, 20));
            if (this.textArea instanceof JTextArea) {
                this.setPreferredSize(new Dimension(500, 300));
            }
        }
        if (!editable) {
            // empty if block
        }
        this.setBorder(BorderFactory.createEmptyBorder(12, 12, 0, 11));
        this.textArea.getAccessibleContext().setAccessibleName(NbBundle.getBundle(StringCustomEditor.class).getString("ACS_TextArea"));
        if (this.instructions == null) {
            this.textArea.getAccessibleContext().setAccessibleDescription(NbBundle.getBundle(StringCustomEditor.class).getString("ACSD_TextArea"));
        } else {
            this.textArea.getAccessibleContext().setAccessibleDescription(this.instructions);
        }
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getBundle(StringCustomEditor.class).getString("ACSD_CustomStringEditor"));
        int prefHeight = s.length() < 102400 ? this.textArea.getPreferredSize().height + 8 : ctn * 8;
        if (this.instructions != null) {
            final JTextArea jta = new JTextArea(this.instructions);
            jta.setEditable(false);
            Color c = UIManager.getColor("control");
            if (c != null) {
                jta.setBackground(c);
            } else {
                jta.setBackground(this.getBackground());
            }
            jta.setLineWrap(true);
            jta.setWrapStyleWord(true);
            jta.setFont(this.getFont());
            this.add(jta, "North", 0);
            jta.getAccessibleContext().setAccessibleName(NbBundle.getMessage(StringCustomEditor.class, (String)"ACS_Instructions"));
            jta.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(StringCustomEditor.class, (String)"ACSD_Instructions"));
            prefHeight += jta.getPreferredSize().height;
            jta.addFocusListener(new FocusListener(){

                @Override
                public void focusGained(FocusEvent e) {
                    jta.setSelectionStart(0);
                    jta.setSelectionEnd(jta.getText().length());
                }

                @Override
                public void focusLost(FocusEvent e) {
                    jta.setSelectionStart(0);
                    jta.setSelectionEnd(0);
                }
            });
        }
        if (this.textArea instanceof JTextField) {
            this.setPreferredSize(new Dimension(300, prefHeight));
        }
    }

    @Override
    public void addNotify() {
        super.addNotify();
        if (this.isEnabled() && this.isFocusable()) {
            this.textArea.requestFocus();
        }
    }

    private Object getPropertyValue() throws IllegalStateException {
        return this.textArea.getText();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("state".equals(evt.getPropertyName()) && evt.getNewValue() == PropertyEnv.STATE_VALID) {
            this.editor.setValue(this.getPropertyValue());
        }
    }

}

