/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.ResourceBundle;
import javax.accessibility.AccessibleContext;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import org.netbeans.beaninfo.editors.ArrayOfIntSupport;
import org.netbeans.beaninfo.editors.DimensionEditor;
import org.netbeans.beaninfo.editors.IntegerCustomEditor;
import org.netbeans.beaninfo.editors.PointEditor;
import org.netbeans.core.UIExceptions;
import org.openide.awt.Mnemonics;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.util.NbBundle;

public class PointCustomEditor
extends IntegerCustomEditor {
    static final long serialVersionUID = -4067033871196801978L;
    private boolean dimensionMode = false;
    private PropertyEnv env;
    private JPanel insidePanel;
    private JTextField xField;
    private JLabel xLabel;
    private JTextField yField;
    private JLabel yLabel;
    private ArrayOfIntSupport editor;

    public PointCustomEditor(PointEditor editor, PropertyEnv env) {
        super(env);
        this.initComponents();
        this.editor = editor;
        Point point = (Point)editor.getValue();
        if (point == null) {
            point = new Point(0, 0);
        }
        this.xField.setText(Integer.toString(point.x));
        this.yField.setText(Integer.toString(point.y));
        this.xField.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(PointCustomEditor.class, (String)"ACSD_CTL_X"));
        this.yField.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(PointCustomEditor.class, (String)"ACSD_CTL_Y"));
        this.commonInit(NbBundle.getMessage(PointCustomEditor.class, (String)"CTL_Point"), env);
    }

    public PointCustomEditor(DimensionEditor editor, PropertyEnv env) {
        super(env);
        this.dimensionMode = true;
        this.initComponents();
        this.editor = editor;
        Dimension dimension = (Dimension)editor.getValue();
        if (dimension == null) {
            dimension = new Dimension(0, 0);
        }
        this.xField.setText(Integer.toString(dimension.width));
        this.yField.setText(Integer.toString(dimension.height));
        Mnemonics.setLocalizedText((JLabel)this.xLabel, (String)NbBundle.getMessage(PointCustomEditor.class, (String)"CTL_Width"));
        this.xLabel.setLabelFor(this.xField);
        Mnemonics.setLocalizedText((JLabel)this.yLabel, (String)NbBundle.getMessage(PointCustomEditor.class, (String)"CTL_Height"));
        this.yLabel.setLabelFor(this.yField);
        this.xField.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(PointCustomEditor.class, (String)"ACSD_CTL_Width"));
        this.yField.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(PointCustomEditor.class, (String)"ACSD_CTL_Height"));
        this.commonInit(NbBundle.getMessage(PointCustomEditor.class, (String)"CTL_Dimension"), env);
    }

    private void commonInit(String panelTitle, PropertyEnv env) {
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(PointCustomEditor.class, (String)"ACSD_PointCustomEditor"));
        this.setBorder(new EmptyBorder(12, 12, 0, 11));
        this.insidePanel.setBorder(new CompoundBorder(new TitledBorder(new EtchedBorder(), " " + panelTitle + " "), new EmptyBorder(new Insets(5, 5, 5, 5))));
        env.setState(PropertyEnv.STATE_NEEDS_VALIDATION);
        env.addPropertyChangeListener((PropertyChangeListener)this);
        this.setPanel(this.insidePanel);
        this.xField.addKeyListener(this);
        this.yField.addKeyListener(this);
        this.getMap().put(this.xField, this.xLabel);
        this.getMap().put(this.yField, this.yLabel);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(280, 160);
    }

    private Object getPropertyValue() throws IllegalStateException {
        try {
            int x = Integer.parseInt(this.xField.getText());
            int y = Integer.parseInt(this.yField.getText());
            if (this.dimensionMode) {
                if (x < 0 || y < 0) {
                    IllegalStateException ise = new IllegalStateException();
                    UIExceptions.annotateUser(ise, null, NbBundle.getMessage(PointCustomEditor.class, (String)"CTL_NegativeSize"), null, null);
                    throw ise;
                }
                return new Dimension(x, y);
            }
            return new Point(x, y);
        }
        catch (NumberFormatException e) {
            IllegalStateException ise = new IllegalStateException();
            UIExceptions.annotateUser(ise, null, NbBundle.getMessage(PointCustomEditor.class, (String)"CTL_InvalidValue"), null, null);
            throw ise;
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("state".equals(evt.getPropertyName()) && PropertyEnv.STATE_VALID.equals(evt.getNewValue())) {
            this.editor.setValue(this.getPropertyValue());
        }
    }

    private void initComponents() {
        this.insidePanel = new JPanel();
        this.xLabel = new JLabel();
        this.xField = new JTextField();
        this.yLabel = new JLabel();
        this.yField = new JTextField();
        this.setLayout(new BorderLayout());
        this.insidePanel.setLayout(new GridBagLayout());
        this.xLabel.setLabelFor(this.xField);
        ResourceBundle bundle = ResourceBundle.getBundle("org/netbeans/beaninfo/editors/Bundle");
        Mnemonics.setLocalizedText((JLabel)this.xLabel, (String)bundle.getString("CTL_X"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 17;
        this.insidePanel.add((Component)this.xLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(4, 8, 4, 0);
        this.insidePanel.add((Component)this.xField, gridBagConstraints);
        this.yLabel.setLabelFor(this.yField);
        Mnemonics.setLocalizedText((JLabel)this.yLabel, (String)bundle.getString("CTL_Y"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 17;
        this.insidePanel.add((Component)this.yLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(4, 8, 4, 0);
        this.insidePanel.add((Component)this.yField, gridBagConstraints);
        this.add((Component)this.insidePanel, "Center");
    }

    @Override
    protected void updateValues() {
        try {
            int x = Integer.parseInt(this.xField.getText());
            int y = Integer.parseInt(this.yField.getText());
            if (this.dimensionMode) {
                this.editor.setValue(new Dimension(x, y));
            } else {
                this.editor.setValue(new Point(x, y));
            }
        }
        catch (NumberFormatException e) {
            // empty catch block
        }
    }
}

