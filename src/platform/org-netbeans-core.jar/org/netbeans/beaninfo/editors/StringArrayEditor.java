/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.ExPropertyEditor
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.explorer.propertysheet.editors.XMLPropertyEditor
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.PropertyEditorRegistration
 */
package org.netbeans.beaninfo.editors;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.StringTokenizer;
import javax.swing.JList;
import javax.swing.JScrollPane;
import org.netbeans.beaninfo.editors.StringArrayCustomEditor;
import org.netbeans.beaninfo.editors.StringArrayCustomizable;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.editors.XMLPropertyEditor;
import org.openide.nodes.Node;
import org.openide.nodes.PropertyEditorRegistration;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

@PropertyEditorRegistration(targetType={String[].class})
public class StringArrayEditor
implements XMLPropertyEditor,
StringArrayCustomizable,
ExPropertyEditor {
    private static final String XML_STRING_ARRAY = "StringArray";
    private static final String XML_STRING_ITEM = "StringItem";
    private static final String ATTR_COUNT = "count";
    private static final String ATTR_INDEX = "index";
    private static final String ATTR_VALUE = "value";
    private String[] strings;
    private PropertyChangeSupport support;
    private boolean editable = true;
    private String separator = ",";

    public StringArrayEditor() {
        this.support = new PropertyChangeSupport(this);
    }

    public Object getValue() {
        return this.strings;
    }

    public void setValue(Object value) {
        this.strings = (String[])value;
        this.support.firePropertyChange("", null, null);
    }

    @Override
    public String[] getStringArray() {
        return (String[])this.getValue();
    }

    @Override
    public void setStringArray(String[] value) {
        this.setValue(value);
    }

    protected final String getStrings(boolean quoted) {
        if (this.strings == null) {
            return "null";
        }
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < this.strings.length; ++i) {
            if (quoted) {
                buf.append('\"').append(this.strings[i]).append('\"');
            } else {
                buf.append(this.strings[i]);
            }
            if (i == this.strings.length - 1) continue;
            buf.append(this.separator);
            buf.append(' ');
        }
        return buf.toString();
    }

    public String getAsText() {
        return this.getStrings(false);
    }

    public void setAsText(String text) {
        if ("null".equals(text)) {
            this.setValue(null);
            return;
        }
        StringTokenizer tok = new StringTokenizer(text, this.separator);
        LinkedList<String> list = new LinkedList<String>();
        while (tok.hasMoreTokens()) {
            String s = tok.nextToken();
            list.add(s.trim());
        }
        String[] a = list.toArray(new String[list.size()]);
        this.setValue(a);
    }

    public String getJavaInitializationString() {
        if (this.strings == null) {
            return "null";
        }
        StringBuilder buf = new StringBuilder("new String[] {");
        buf.append(this.getStrings(true));
        buf.append('}');
        return buf.toString();
    }

    public String[] getTags() {
        return null;
    }

    public boolean isPaintable() {
        return false;
    }

    public void paintValue(Graphics g, Rectangle rectangle) {
    }

    public boolean supportsCustomEditor() {
        if (!(this.editable || this.strings != null && this.strings.length != 0)) {
            return false;
        }
        return true;
    }

    public Component getCustomEditor() {
        if (this.editable) {
            return new StringArrayCustomEditor(this);
        }
        return new JScrollPane(new JList<String>(this.getStringArray()));
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this.support.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this.support.removePropertyChangeListener(propertyChangeListener);
    }

    public org.w3c.dom.Node storeToXML(Document doc) {
        Element arrayEl = doc.createElement("StringArray");
        int count = this.strings != null ? this.strings.length : 0;
        arrayEl.setAttribute("count", Integer.toString(count));
        for (int i = 0; i < count; ++i) {
            Element itemEl = doc.createElement("StringItem");
            itemEl.setAttribute("index", Integer.toString(i));
            itemEl.setAttribute("value", this.strings[i]);
            arrayEl.appendChild(itemEl);
        }
        return arrayEl;
    }

    public void readFromXML(org.w3c.dom.Node element) throws IOException {
        String[] stringArray;
        if (!"StringArray".equals(element.getNodeName())) {
            throw new IOException();
        }
        NamedNodeMap attributes = element.getAttributes();
        org.w3c.dom.Node countNode = null;
        int count = 0;
        countNode = attributes.getNamedItem("count");
        if (countNode != null && (count = Integer.parseInt(countNode.getNodeValue())) > 0) {
            stringArray = new String[count];
            NodeList items = element.getChildNodes();
            for (int i = 0; i < items.getLength(); ++i) {
                Element itemEl;
                int index;
                if (items.item(i).getNodeType() != 1 || !"StringItem".equals((itemEl = (Element)items.item(i)).getNodeName())) continue;
                String indexStr = itemEl.getAttribute("index");
                String valueStr = itemEl.getAttribute("value");
                if (indexStr == null || valueStr == null || (index = Integer.parseInt(indexStr)) < 0 || index >= count) continue;
                stringArray[index] = valueStr;
            }
        } else {
            stringArray = new String[]{};
        }
        this.setValue(stringArray);
    }

    public void attachEnv(PropertyEnv env) {
        FeatureDescriptor d = env.getFeatureDescriptor();
        this.readEnv(env.getFeatureDescriptor());
    }

    final void readEnv(FeatureDescriptor d) {
        this.editable = d instanceof Node.Property ? ((Node.Property)d).canWrite() : (d instanceof PropertyDescriptor ? ((PropertyDescriptor)d).getWriteMethod() != null : true);
        Object v = d.getValue("item.separator");
        if (v instanceof String) {
            this.separator = (String)v;
        }
    }
}

