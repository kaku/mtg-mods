/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.PropertyEnv
 */
package org.netbeans.beaninfo.editors;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import org.openide.explorer.propertysheet.PropertyEnv;

abstract class IntegerCustomEditor
extends JPanel
implements PropertyChangeListener,
KeyListener {
    private static final String ERROR_FOREGROUND = "nb.errorForeground";
    private JPanel myPanel;
    private PropertyEnv myEnv;
    private HashMap<JTextField, JLabel> myLabelMap = new HashMap();

    public IntegerCustomEditor(PropertyEnv env) {
        this.myEnv = env;
    }

    @Override
    public void keyPressed(KeyEvent arg0) {
    }

    @Override
    public void keyReleased(KeyEvent arg0) {
        if (this.checkValues()) {
            this.updateValues();
        }
    }

    @Override
    public void keyTyped(KeyEvent arg0) {
    }

    protected abstract void updateValues();

    protected void setPanel(JPanel panel) {
        this.myPanel = panel;
    }

    protected HashMap<JTextField, JLabel> getMap() {
        return this.myLabelMap;
    }

    protected boolean validFor(JTextField c) {
        String s = c.getText().trim();
        try {
            Integer.parseInt(s);
            this.handleValid(c);
            return true;
        }
        catch (NumberFormatException e) {
            this.handleInvalid(c);
            return false;
        }
    }

    protected void handleValid(JTextField field) {
        field.setForeground(this.getForeground());
        this.getMap().get(field).setForeground(this.getForeground());
    }

    protected void handleInvalid(JTextField field) {
        field.setForeground(this.getErrorColor());
        this.getMap().get(field).setForeground(this.getErrorColor());
    }

    private Color getErrorColor() {
        Color c = UIManager.getColor("nb.errorForeground");
        if (c == null) {
            c = Color.RED;
        }
        return c;
    }

    private PropertyEnv getEnv() {
        return this.myEnv;
    }

    private boolean checkValues() {
        Component[] components = this.myPanel.getComponents();
        boolean valid = true;
        for (int i = 0; i < components.length; ++i) {
            if (!(components[i] instanceof JTextField)) continue;
            valid &= this.validFor((JTextField)components[i]);
        }
        if (this.getEnv() != null) {
            this.getEnv().setState(valid ? PropertyEnv.STATE_VALID : PropertyEnv.STATE_INVALID);
        }
        return valid;
    }
}

