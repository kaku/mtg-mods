/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.nodes.PropertyEditorRegistration
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.beans.FeatureDescriptor;
import java.util.Arrays;
import java.util.Date;
import org.netbeans.beaninfo.editors.ExPropertyEditorSupport;
import org.netbeans.core.UIExceptions;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.nodes.PropertyEditorRegistration;
import org.openide.util.NbBundle;

@PropertyEditorRegistration(targetType={Integer.class})
public class IntEditor
extends ExPropertyEditorSupport {
    public static final String KEYS = "stringKeys";
    public static final String VALS = "intValues";
    public static final String CODE_VALS = "codeValues";
    String[] keys = null;
    String[] code = null;
    int[] values = null;

    @Override
    protected void attachEnvImpl(PropertyEnv env) {
        this.keys = (String[])env.getFeatureDescriptor().getValue("stringKeys");
        this.values = (int[])env.getFeatureDescriptor().getValue("intValues");
        this.code = (String[])env.getFeatureDescriptor().getValue("codeValues");
    }

    @Override
    protected void validateEnv(PropertyEnv env) {
        boolean valid;
        boolean bl = valid = this.keys == null && this.values == null && this.code == null;
        if (!valid) {
            boolean bl2 = valid = this.keys != null && this.values != null;
            if (!valid) {
                throw new ExPropertyEditorSupport.EnvException("You must specify both an array of keys and an array of values if you specify one. Keys=" + IntEditor.arrToStr(this.keys) + " Values=" + IntEditor.arrToStr(this.values));
            }
            boolean bl3 = valid = this.keys.length == this.values.length;
            if (valid) {
                boolean bl4 = valid = this.keys.length > 0 && this.values.length > 0;
            }
            if (!valid) {
                throw new ExPropertyEditorSupport.EnvException("The arrays of keys and values must have the same length and the length must be > 0. keys.length =" + this.keys.length + " values.length=" + this.values.length + " Keys=" + IntEditor.arrToStr(this.keys) + " Values=" + IntEditor.arrToStr(this.values));
            }
            if (this.code != null) {
                boolean bl5 = valid = this.code.length == this.keys.length;
                if (valid) {
                    boolean bl6 = valid = this.code.length > 0;
                }
                if (!valid) {
                    throw new ExPropertyEditorSupport.EnvException("The arrays of keys and values and codes must all have the same length, > 0. keys.length =" + this.keys.length + " values.length=" + this.values.length + " Code.length=" + this.code.length + " Keys=" + IntEditor.arrToStr(this.keys) + " Values=" + IntEditor.arrToStr(this.values) + " Code=" + IntEditor.arrToStr(this.code));
                }
            }
        }
    }

    private static final String arrToStr(int[] s) {
        if (s == null) {
            return "null";
        }
        StringBuilder out = new StringBuilder(s.length * 3);
        for (int i = 0; i < s.length; ++i) {
            out.append(s[i]);
            if (i == s.length - 1) continue;
            out.append(',');
        }
        return out.toString();
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    @Override
    public String getAsText() {
        Integer i = (Integer)this.getValue();
        if (i == null) return NbBundle.getMessage(IntEditor.class, (String)"NULL");
        if (this.keys == null) return this.getValue().toString();
        int intVal = i;
        int idx = -1;
        for (int j = 0; j < this.values.length; ++j) {
            if (this.values[j] != intVal) continue;
            idx = j;
            break;
        }
        if (idx == -1) throw new IllegalArgumentException("This property editor uses a set of keyed values, and the current value, " + i + ", is not specified.");
        return this.keys[idx];
    }

    private void doSetAsText(String s) {
        try {
            this.setValue(Integer.valueOf(s));
        }
        catch (NumberFormatException nfe) {
            String msg = NbBundle.getMessage(IntEditor.class, (String)"EXC_ILLEGAL_VALUE_TEXT") + s;
            IllegalArgumentException iae = new IllegalArgumentException(msg);
            UIExceptions.annotateUser(iae, msg, msg, nfe, new Date());
            throw iae;
        }
    }

    @Override
    public void setAsText(String s) {
        s = s.trim();
        if (this.keys == null) {
            this.doSetAsText(s);
        } else {
            int idx = Arrays.asList(this.keys).indexOf(s);
            if (idx == -1 || idx > this.values.length - 1) {
                StringBuilder msg = new StringBuilder();
                msg.append(NbBundle.getMessage(IntEditor.class, (String)"EXC_ILLEGAL_STRING_TEXT_FIRST"));
                msg.append(s);
                msg.append(NbBundle.getMessage(IntEditor.class, (String)"EXC_ILLEGAL_STRING_TEXT_SECOND"));
                msg.append(IntEditor.arrToStr(this.keys));
                String message = msg.toString();
                IllegalArgumentException iae = new IllegalArgumentException(message);
                UIExceptions.annotateUser(iae, message, message, iae, new Date());
                throw iae;
            }
            this.setValue(this.values[idx]);
        }
    }

    @Override
    public void setValue(Object value) {
        if (!(value instanceof Integer) && value != null) {
            throw new IllegalArgumentException("Argument to IntEditor.setValue() must be Integer, but was " + value.getClass().getName() + "(=" + value.toString() + ")");
        }
        super.setValue(value);
    }

    @Override
    public String[] getTags() {
        return this.keys;
    }

    @Override
    public String getJavaInitializationString() {
        String result = this.code == null ? this.getValue().toString() : this.code[(Integer)this.getValue()];
        return result;
    }
}

