/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.util.NbBundle
 */
package org.netbeans.beaninfo.editors;

import java.awt.Component;
import java.awt.Dimension;
import java.io.IOException;
import java.util.Date;
import org.netbeans.beaninfo.editors.ArrayOfIntSupport;
import org.netbeans.beaninfo.editors.PointCustomEditor;
import org.netbeans.core.UIExceptions;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.util.NbBundle;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class DimensionEditor
extends ArrayOfIntSupport {
    public DimensionEditor() {
        super("java.awt.Dimension", 2);
    }

    @Override
    int[] getValues() {
        Dimension d = (Dimension)this.getValue();
        return new int[]{d.width, d.height};
    }

    static String toArr(int[] ints) {
        StringBuilder sb = new StringBuilder();
        if (ints != null && ints.length > 0) {
            for (int i = 0; i < ints.length; ++i) {
                sb.append(ints[i]);
                if (i == ints.length - 1) continue;
                sb.append(',');
            }
        } else {
            return NbBundle.getMessage(DimensionEditor.class, (String)"MSG_NULL_OR_EMPTY");
        }
        return sb.toString();
    }

    @Override
    void setValues(int[] val) {
        if (val[0] < 0 || val[1] < 0) {
            String msg = NbBundle.getMessage(DimensionEditor.class, (String)"CTL_NegativeSize");
            IllegalArgumentException iae = new IllegalArgumentException("Negative value");
            UIExceptions.annotateUser(iae, iae.getMessage(), msg, null, new Date());
            throw iae;
        }
        this.setValue(new Dimension(val[0], val[1]));
    }

    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

    @Override
    public Component getCustomEditor() {
        return new PointCustomEditor(this, this.env);
    }

    @Override
    String getHintFormat() {
        return NbBundle.getMessage(DimensionEditor.class, (String)"CTL_HintFormat");
    }

    @Override
    protected String getXMLValueTag() {
        return "Dimension";
    }
}

