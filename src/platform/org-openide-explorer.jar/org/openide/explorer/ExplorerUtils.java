/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package org.openide.explorer;

import javax.swing.Action;
import javax.swing.ActionMap;
import org.netbeans.modules.openide.explorer.ExplorerActionsImpl;
import org.openide.explorer.DefaultEMLookup;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public final class ExplorerUtils {
    ExplorerUtils() {
    }

    public static Action actionCopy(ExplorerManager em) {
        return ExplorerManager.findExplorerActionsImpl(em).copyAction();
    }

    public static Action actionCut(ExplorerManager em) {
        return ExplorerManager.findExplorerActionsImpl(em).cutAction();
    }

    public static Action actionDelete(ExplorerManager em, boolean confirm) {
        ExplorerActionsImpl impl = ExplorerManager.findExplorerActionsImpl(em);
        return impl.deleteAction(confirm);
    }

    public static Action actionPaste(ExplorerManager em) {
        return ExplorerManager.findExplorerActionsImpl(em).pasteAction();
    }

    public static void activateActions(ExplorerManager em, boolean enable) {
        if (enable) {
            ExplorerManager.findExplorerActionsImpl(em).attach(em);
        } else {
            ExplorerManager.findExplorerActionsImpl(em).detach();
        }
    }

    public static Lookup createLookup(ExplorerManager em, ActionMap map) {
        return new DefaultEMLookup(em, map);
    }

    public static HelpCtx getHelpCtx(Node[] sel, HelpCtx def) {
        HelpCtx result = null;
        for (int i = 0; i < sel.length; ++i) {
            HelpCtx attempt = sel[i].getHelpCtx();
            if (attempt == null || attempt.equals((Object)HelpCtx.DEFAULT_HELP)) continue;
            if (result == null || result.equals((Object)attempt)) {
                result = attempt;
                continue;
            }
            result = null;
            break;
        }
        return result != null ? result : def;
    }
}

