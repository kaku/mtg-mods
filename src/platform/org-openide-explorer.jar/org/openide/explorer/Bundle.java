/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.openide.explorer;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String EXC_CannotHaveNullRootContext() {
        return NbBundle.getMessage(Bundle.class, (String)"EXC_CannotHaveNullRootContext");
    }

    static String EXC_ContextMustBeWithinRootContext(Object name_of_node_to_be_selected, Object name_of_node_at_root) {
        return NbBundle.getMessage(Bundle.class, (String)"EXC_ContextMustBeWithinRootContext", (Object)name_of_node_to_be_selected, (Object)name_of_node_at_root);
    }

    static String EXC_NoElementOfNodeSelectionMayBeNull() {
        return NbBundle.getMessage(Bundle.class, (String)"EXC_NoElementOfNodeSelectionMayBeNull");
    }

    static String EXC_NodeCannotBeNull() {
        return NbBundle.getMessage(Bundle.class, (String)"EXC_NodeCannotBeNull");
    }

    static String EXC_handle_failed(Object name_of_old_node) {
        return NbBundle.getMessage(Bundle.class, (String)"EXC_handle_failed", (Object)name_of_old_node);
    }

    private void Bundle() {
    }
}

