/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.WeakListeners
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 */
package org.openide.explorer;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import javax.swing.ActionMap;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.WeakListeners;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

final class DefaultEMLookup
extends ProxyLookup
implements LookupListener,
PropertyChangeListener {
    private static final Object PRESENT = new Object();
    private ExplorerManager tc;
    private LookupListener listener;
    private Map<Lookup, Lookup.Result> attachedTo;
    private Lookup actionMap;

    public DefaultEMLookup(ExplorerManager tc, ActionMap map) {
        this.tc = tc;
        this.listener = (LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this, (Object)null);
        this.actionMap = Lookups.singleton((Object)map);
        tc.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)tc));
        this.updateLookups(tc.getSelectedNodes());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void updateLookups(Node[] arr) {
        Map copy;
        if (arr == null) {
            arr = new Node[]{};
        }
        Lookup[] lookups = new Lookup[arr.length];
        DefaultEMLookup defaultEMLookup = this;
        synchronized (defaultEMLookup) {
            copy = this.attachedTo == null ? Collections.emptyMap() : new HashMap<Lookup, Lookup.Result>(this.attachedTo);
        }
        for (int i = 0; i < arr.length; ++i) {
            lookups[i] = arr[i].getLookup();
            if (copy == null) continue;
            copy.remove((Object)arr[i]);
        }
        for (Lookup.Result res : copy.values()) {
            res.removeLookupListener(this.listener);
        }
        DefaultEMLookup it = this;
        synchronized (it) {
            this.attachedTo = null;
        }
        this.setLookups(new Lookup[]{new NoNodeLookup((Lookup)new ProxyLookup(lookups), arr), Lookups.fixed((Object[])((Object[])arr)), this.actionMap});
    }

    public void resultChanged(LookupEvent ev) {
        this.updateLookups(this.tc.getSelectedNodes());
    }

    private static boolean isNodeQuery(Class<?> c) {
        return Node.class.isAssignableFrom(c) || c.isAssignableFrom(Node.class);
    }

    protected synchronized void beforeLookup(Lookup.Template<?> t) {
        if (this.attachedTo == null && DefaultEMLookup.isNodeQuery(t.getType())) {
            Lookup[] arr = this.getLookups();
            this.attachedTo = new WeakHashMap<Lookup, Lookup.Result>(arr.length * 2);
            for (int i = 0; i < arr.length - 2; ++i) {
                Lookup.Result res = arr[i].lookup(t);
                res.addLookupListener(this.listener);
                this.attachedTo.put(arr[i], res);
            }
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("selectedNodes" == evt.getPropertyName()) {
            this.updateLookups((Node[])evt.getNewValue());
        }
    }

    private static final class NoNodeLookup
    extends Lookup {
        private final Lookup delegate;
        private final Map<Node, Object> verboten;

        public NoNodeLookup(Lookup del, Node[] exclude) {
            this.delegate = del;
            this.verboten = new IdentityHashMap<Node, Object>();
            int i = 0;
            while (i < exclude.length) {
                this.verboten.put(exclude[i++], PRESENT);
            }
        }

        public <T> T lookup(Class<T> clazz) {
            if (clazz == Node.class) {
                return null;
            }
            Object o = this.delegate.lookup(clazz);
            if (this.verboten.containsKey(o)) {
                for (Object o2 : this.lookup(new Lookup.Template(clazz)).allInstances()) {
                    if (this.verboten.containsKey(o2)) continue;
                    return (T)o2;
                }
                return null;
            }
            return (T)o;
        }

        public <T> Lookup.Result<T> lookup(Lookup.Template<T> template) {
            Class clz = template.getType();
            if (clz == Node.class) {
                return Lookup.EMPTY.lookup(new Lookup.Template(clz));
            }
            return new ExclusionResult(this.delegate.lookup(template), this.verboten);
        }

        private static final class ExclusionResult<T>
        extends Lookup.Result<T>
        implements LookupListener {
            private final Lookup.Result<T> delegate;
            private final Map<Node, Object> verboten;
            private final List<LookupListener> listeners = new ArrayList<LookupListener>();

            public ExclusionResult(Lookup.Result<T> delegate, Map<Node, Object> verboten) {
                this.delegate = delegate;
                this.verboten = verboten;
            }

            public Collection<? extends T> allInstances() {
                Collection c = this.delegate.allInstances();
                ArrayList ret = new ArrayList(c.size());
                for (Object o : c) {
                    if (this.verboten.containsKey(o)) continue;
                    ret.add(o);
                }
                return ret;
            }

            public Set<Class<? extends T>> allClasses() {
                return this.delegate.allClasses();
            }

            public Collection<? extends Lookup.Item<T>> allItems() {
                Collection c = this.delegate.allItems();
                ArrayList<Lookup.Item> ret = new ArrayList<Lookup.Item>(c.size());
                for (Lookup.Item i : c) {
                    if (Node.class.isAssignableFrom(i.getType()) && this.verboten.containsKey(i.getInstance())) continue;
                    ret.add(i);
                }
                return ret;
            }

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            public void addLookupListener(LookupListener l) {
                List<LookupListener> list = this.listeners;
                synchronized (list) {
                    if (this.listeners.isEmpty()) {
                        this.delegate.addLookupListener((LookupListener)this);
                    }
                    this.listeners.add(l);
                }
            }

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            public void removeLookupListener(LookupListener l) {
                List<LookupListener> list = this.listeners;
                synchronized (list) {
                    this.listeners.remove((Object)l);
                    if (this.listeners.isEmpty()) {
                        this.delegate.removeLookupListener((LookupListener)this);
                    }
                }
            }

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            public void resultChanged(LookupEvent ev) {
                LookupListener[] ls;
                LookupEvent ev2 = new LookupEvent((Lookup.Result)this);
                List<LookupListener> list = this.listeners;
                synchronized (list) {
                    ls = this.listeners.toArray((T[])new LookupListener[this.listeners.size()]);
                }
                for (int i = 0; i < ls.length; ++i) {
                    ls[i].resultChanged(ev2);
                }
            }
        }

    }

}

