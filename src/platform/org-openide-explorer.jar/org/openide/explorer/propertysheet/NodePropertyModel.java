/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 */
package org.openide.explorer.propertysheet;

import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyEditor;
import java.lang.reflect.InvocationTargetException;
import org.openide.explorer.propertysheet.ExPropertyModel;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.ProxyNode;
import org.openide.nodes.Node;

class NodePropertyModel
implements ExPropertyModel {
    private Node.Property prop;
    private Object[] beans;
    private PropertyChangeSupport sup;
    String beanName;

    public NodePropertyModel(Node.Property property, Object[] beans) {
        this.sup = new PropertyChangeSupport(this);
        this.beanName = null;
        this.prop = property;
        this.beans = beans;
    }

    String getBeanName() {
        if (this.beans != null && this.beans.length == 1 && this.beans[0] instanceof Node.Property) {
            return ((Node.Property)this.beans[0]).getDisplayName();
        }
        return null;
    }

    @Override
    public Object getValue() throws InvocationTargetException {
        try {
            return this.prop.getValue();
        }
        catch (IllegalAccessException iae) {
            throw this.annotateException(iae);
        }
        catch (InvocationTargetException ite) {
            throw this.annotateException(ite);
        }
        catch (ProxyNode.DifferentValuesException dve) {
            return null;
        }
    }

    @Override
    public void setValue(Object v) throws InvocationTargetException {
        try {
            this.prop.setValue(v);
            this.sup.firePropertyChange("value", null, null);
        }
        catch (IllegalAccessException iae) {
            throw this.annotateException(iae);
        }
        catch (IllegalArgumentException iaae) {
            throw this.annotateException(iaae);
        }
        catch (InvocationTargetException ite) {
            throw this.annotateException(ite);
        }
    }

    private InvocationTargetException annotateException(Exception exception) {
        if (exception instanceof InvocationTargetException) {
            return (InvocationTargetException)exception;
        }
        return new InvocationTargetException(exception);
    }

    @Override
    public Class getPropertyType() {
        return this.prop.getValueType();
    }

    @Override
    public Class getPropertyEditorClass() {
        PropertyEditor ed = this.prop.getPropertyEditor();
        if (ed != null) {
            return ed.getClass();
        }
        return null;
    }

    public PropertyEditor getPropertyEditor() {
        return PropUtils.getPropertyEditor(this.prop);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.sup.addPropertyChangeListener(l);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.sup.removePropertyChangeListener(l);
    }

    @Override
    public Object[] getBeans() {
        return this.beans;
    }

    @Override
    public FeatureDescriptor getFeatureDescriptor() {
        return this.prop;
    }

    void fireValueChanged() {
        this.sup.firePropertyChange("value", null, null);
    }

    Node.Property getProperty() {
        return this.prop;
    }
}

