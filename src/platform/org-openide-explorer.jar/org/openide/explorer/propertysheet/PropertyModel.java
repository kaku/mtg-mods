/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.propertysheet;

import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;

public interface PropertyModel {
    public static final String PROP_VALUE = "value";

    public Object getValue() throws InvocationTargetException;

    public void setValue(Object var1) throws InvocationTargetException;

    public Class getPropertyType();

    public Class getPropertyEditorClass();

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

