/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.nodes.PropertySupport
 *  org.openide.nodes.PropertySupport$Reflection
 *  org.openide.util.Exceptions
 */
package org.openide.explorer.propertysheet;

import java.beans.BeanDescriptor;
import java.beans.BeanInfo;
import java.beans.FeatureDescriptor;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.beans.PropertyEditor;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.explorer.propertysheet.DefaultPropertyModel;
import org.openide.explorer.propertysheet.ExPropertyModel;
import org.openide.explorer.propertysheet.NodePropertyModel;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertyModel;
import org.openide.explorer.propertysheet.ProxyNode;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.util.Exceptions;

class ModelProperty
extends Node.Property {
    PropertyModel mdl;
    private boolean valueSet = false;

    private ModelProperty(PropertyModel pm) {
        Boolean result;
        FeatureDescriptor fd;
        super(pm.getPropertyType());
        this.mdl = pm;
        if (this.mdl instanceof ExPropertyModel && (result = (Boolean)(fd = ((ExPropertyModel)this.mdl).getFeatureDescriptor()).getValue("canEditAsText")) != null) {
            this.setValue("canEditAsText", (Object)result);
        }
    }

    Object[] getBeans() {
        if (this.mdl instanceof ExPropertyModel) {
            return ((ExPropertyModel)this.mdl).getBeans();
        }
        return null;
    }

    public PropertyEditor getPropertyEditor() {
        if (this.mdl.getPropertyEditorClass() != null) {
            try {
                Constructor c = this.mdl.getPropertyEditorClass().getConstructor(new Class[0]);
                c.setAccessible(true);
                return (PropertyEditor)c.newInstance(new Object[0]);
            }
            catch (Exception e) {
                Exceptions.printStackTrace((Throwable)e);
                return new PropUtils.NoPropertyEditorEditor();
            }
        }
        return super.getPropertyEditor();
    }

    static Node.Property toProperty(PropertyModel mdl) {
        if (mdl instanceof NodePropertyModel) {
            return ((NodePropertyModel)mdl).getProperty();
        }
        if (mdl instanceof DefaultPropertyModel) {
            return new DPMWrapper((DefaultPropertyModel)mdl);
        }
        if (mdl instanceof ExPropertyModel && ((ExPropertyModel)mdl).getFeatureDescriptor() instanceof PropertyDescriptor) {
            Object[] beans = ((ExPropertyModel)mdl).getBeans();
            if (beans.length == 1) {
                return new DPMWrapper((PropertyDescriptor)((ExPropertyModel)mdl).getFeatureDescriptor(), ((ExPropertyModel)mdl).getBeans());
            }
            return new ModelProperty(mdl);
        }
        if (mdl instanceof ExPropertyModel && ((ExPropertyModel)mdl).getFeatureDescriptor() instanceof Node.Property) {
            UnsupportedOperationException uoe = new UnsupportedOperationException("PropertyPanel now supports direct use of Node.Property objects.  Please do not use ExPropertyModel if you only need to wrap a Node.Property object.  PropertyModel will be deprecated soon.");
            Logger.getLogger(ModelProperty.class.getName()).log(Level.WARNING, null, uoe);
            return (Node.Property)((ExPropertyModel)mdl).getFeatureDescriptor();
        }
        if (mdl != null) {
            return new ModelProperty(mdl);
        }
        return new EmptyProperty();
    }

    static Node.Property toProperty(Node[] n, String name) throws ClassCastException, NullPointerException {
        Node.Property p;
        Class clazz = null;
        if (n.length == 0) {
            throw new NullPointerException("Cannot find a property in an array of 0 properties.  Looking for " + name);
        }
        for (int i = 0; i < n.length; ++i) {
            p = ModelProperty.findProperty(n[i], name);
            if (p == null) {
                throw new NullPointerException("Node " + n[i].getDisplayName() + " does not contain a property " + name);
            }
            if (clazz == null) {
                clazz = p.getValueType();
                continue;
            }
            if (p.getValueType() == clazz) continue;
            throw new ClassCastException("Found a property named " + n + " but it is of class " + p.getValueType().getName() + " not " + clazz.getName());
        }
        ProxyNode pn = new ProxyNode(n);
        p = ModelProperty.findProperty((Node)pn, name);
        if (p != null) {
            return p;
        }
        throw new NullPointerException("Found properties named " + name + " but ProxyNode did not contain one with this name.  This should " + "be impossible; probably someone has modified ProxyNode");
    }

    static Node.Property findProperty(Node n, String name) throws NullPointerException {
        Node.PropertySet[] ps = n.getPropertySets();
        for (int j = 0; j < ps.length; ++j) {
            Node.Property p = ModelProperty.findProperty(ps[j], name);
            if (p == null) continue;
            return p;
        }
        return null;
    }

    private static Node.Property findProperty(Node.PropertySet set, String name) {
        Node.Property[] p = set.getProperties();
        for (int i = 0; i < p.length; ++i) {
            if (!p[i].getName().equals(name)) continue;
            return p[i];
        }
        return null;
    }

    public boolean canRead() {
        return true;
    }

    public boolean canWrite() {
        return true;
    }

    public Object getValue() throws IllegalAccessException, InvocationTargetException {
        return this.mdl.getValue();
    }

    public void setValue(Object val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        this.mdl.setValue(val);
        this.valueSet = true;
    }

    FeatureDescriptor getFeatureDescriptor() {
        if (this.mdl instanceof ExPropertyModel) {
            return ((ExPropertyModel)this.mdl).getFeatureDescriptor();
        }
        return this;
    }

    private static String findDisplayNameFor(Object o) {
        try {
            BeanDescriptor bd;
            if (o == null) {
                return null;
            }
            if (o instanceof Node.Property) {
                return ((Node.Property)o).getDisplayName();
            }
            BeanInfo bi = Introspector.getBeanInfo(o.getClass());
            if (bi != null && (bd = bi.getBeanDescriptor()) != null) {
                return bd.getDisplayName();
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        return null;
    }

    private static class EmptyProperty
    extends Node.Property {
        public EmptyProperty() {
            super(Object.class);
        }

        public boolean canRead() {
            return true;
        }

        public boolean canWrite() {
            return false;
        }

        public Object getValue() throws IllegalAccessException, InvocationTargetException {
            return "";
        }

        public void setValue(Object val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        }

        public PropertyEditor getPropertyEditor() {
            return new PropUtils.NoPropertyEditorEditor();
        }
    }

    static class DPMWrapper
    extends PropertySupport.Reflection {
        PropertyDescriptor descriptor;
        PropertyModel mdl;
        private String beanName = null;

        public DPMWrapper(DefaultPropertyModel mdl) {
            super(mdl.bean, mdl.getPropertyType(), ((PropertyDescriptor)mdl.getFeatureDescriptor()).getReadMethod(), ((PropertyDescriptor)mdl.getFeatureDescriptor()).getWriteMethod());
            this.descriptor = (PropertyDescriptor)mdl.getFeatureDescriptor();
            this.mdl = mdl;
            this.beanName = ModelProperty.findDisplayNameFor(mdl.bean);
        }

        public DPMWrapper(PropertyDescriptor desc, Object[] instances) {
            super(instances[0], desc.getPropertyType(), desc.getReadMethod(), desc.getWriteMethod());
            this.descriptor = desc;
            if (instances != null && instances.length == 1) {
                this.beanName = ModelProperty.findDisplayNameFor(instances[0]);
            }
        }

        public String getBeanName() {
            return this.beanName;
        }

        Object[] getBeans() {
            if (this.mdl instanceof DefaultPropertyModel) {
                return ((DefaultPropertyModel)this.mdl).getBeans();
            }
            return null;
        }

        FeatureDescriptor getFeatureDescriptor() {
            return this.descriptor;
        }

        public String getDisplayName() {
            return this.descriptor.getDisplayName();
        }

        public String getShortDescription() {
            return this.descriptor.getShortDescription();
        }

        public Object getValue(String key) {
            Object result = this.descriptor.getValue(key);
            if (result == null) {
                result = super.getValue(key);
            }
            return result;
        }

        public void setValue(String key, Object val) {
            this.descriptor.setValue(key, val);
        }

        public PropertyEditor getPropertyEditor() {
            Class edClass = this.mdl != null ? this.mdl.getPropertyEditorClass() : this.descriptor.getPropertyEditorClass();
            if (edClass != null) {
                try {
                    return (PropertyEditor)edClass.newInstance();
                }
                catch (Exception e) {
                    // empty catch block
                }
            }
            return super.getPropertyEditor();
        }
    }

}

