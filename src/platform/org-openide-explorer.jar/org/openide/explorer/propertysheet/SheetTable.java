/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 *  org.openide.awt.HtmlRenderer$Renderer
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.openide.explorer.propertysheet;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.ContainerOrderFocusTraversalPolicy;
import java.awt.Dimension;
import java.awt.FocusTraversalPolicy;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.FeatureDescriptor;
import java.beans.PropertyEditor;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.io.StringBufferInputStream;
import java.io.StringReader;
import java.util.EventObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultListSelectionModel;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.TableUI;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.openide.awt.HtmlRenderer;
import org.openide.explorer.propertysheet.BaseTable;
import org.openide.explorer.propertysheet.ButtonPanel;
import org.openide.explorer.propertysheet.CleanComboUI;
import org.openide.explorer.propertysheet.ComboBoxAutoCompleteSupport;
import org.openide.explorer.propertysheet.CustomEditorAction;
import org.openide.explorer.propertysheet.IncrementPropertyValueSupport;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertySetModel;
import org.openide.explorer.propertysheet.PropertySetModelEvent;
import org.openide.explorer.propertysheet.PropertySetModelImpl;
import org.openide.explorer.propertysheet.PropertySetModelListener;
import org.openide.explorer.propertysheet.ProxyNode;
import org.openide.explorer.propertysheet.ReusablePropertyEnv;
import org.openide.explorer.propertysheet.ReusablePropertyModel;
import org.openide.explorer.propertysheet.SheetCellEditor;
import org.openide.explorer.propertysheet.SheetCellRenderer;
import org.openide.explorer.propertysheet.SheetColumnModel;
import org.openide.explorer.propertysheet.SheetTableModel;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

final class SheetTable
extends BaseTable
implements PropertySetModelListener,
CustomEditorAction.Invoker {
    private static final String ACTION_EXPAND = "expandSet";
    private static final String ACTION_COLLAPSE = "collapseSet";
    private static final String ACTION_CUSTOM_EDITOR = "invokeCustomEditor";
    private static final String ACTION_EDCLASS = "edclass";
    private static int instanceCount = 0;
    private transient boolean initialized = false;
    private FeatureDescriptor storedFd = null;
    private boolean wasEditing = false;
    private Object partialValue = null;
    private int lastSelectedRow = -1;
    private SheetCellRenderer renderer = null;
    private SheetCellEditor sheetCellEditor = null;
    private Action customEditorAction = null;
    private Action expandAction;
    private Action collapseAction;
    private Action edClassAction;
    private String beanName;
    private boolean customEditorIsOpen = false;
    private ReusablePropertyEnv reusableEnv = new ReusablePropertyEnv();
    private ReusablePropertyModel reusableModel = new ReusablePropertyModel(this.reusableEnv);
    boolean lastIncludeMargin = false;
    private HtmlRenderer.Renderer htmlrenderer = null;
    int countDown = -1;
    boolean lastFailed = false;

    public SheetTable() {
        super(new SheetTableModel(), new SheetColumnModel(), new DefaultListSelectionModel());
        this.setPropertySetModel(new PropertySetModelImpl());
        this.setRowHeight(16);
        this.setShowGrid(PropUtils.noAltBg());
        this.setShowVerticalLines(PropUtils.noAltBg());
        this.setShowHorizontalLines(PropUtils.noAltBg());
        this.setAutoResizeMode(1);
        if (!PropUtils.noAltBg()) {
            this.setIntercellSpacing(new Dimension(0, 0));
        }
        this.setGridColor(PropUtils.getSetRendererColor());
        Color c = UIManager.getColor("PropSheet.selectionBackground");
        if (c != null) {
            this.setSelectionBackground(c);
        }
        if ((c = UIManager.getColor("PropSheet.selectionForeground")) != null) {
            this.setSelectionForeground(c);
        }
        if ((c = UIManager.getColor("PropSheet.foreground")) != null) {
            this.setForeground(c);
        }
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(SheetTable.class, (String)"ACSN_SHEET_TABLE"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(SheetTable.class, (String)"ACSD_SHEET_TABLE"));
        this.setTransferHandler(new SheetTableTransferHandler());
        Color col = UIManager.getColor("netbeans.ps.background");
        if (col != null) {
            this.setBackground(col);
        }
        this.setFocusTraversalPolicy(new STPolicy());
        ++instanceCount;
    }

    protected void finalize() {
        if (--instanceCount == 0) {
            this.renderer = null;
            this.sheetCellEditor = null;
            SheetTable.cleanup();
        }
    }

    SheetCellRenderer getRenderer() {
        if (this.renderer == null) {
            this.renderer = new SheetCellRenderer(true, this.reusableEnv, this.reusableModel);
        }
        return this.renderer;
    }

    SheetCellEditor getEditor() {
        if (this.sheetCellEditor == null) {
            this.sheetCellEditor = new SheetCellEditor(this.getReusablePropertyEnv());
        }
        return this.sheetCellEditor;
    }

    private TableCellRenderer getCustomRenderer(int row) {
        FeatureDescriptor fd = this.getPropertySetModel().getFeatureDescriptor(row);
        if (fd instanceof Node.PropertySet) {
            return null;
        }
        Object res = fd.getValue("custom.cell.renderer");
        if (res instanceof TableCellRenderer) {
            this.prepareCustomEditor(res);
            return (TableCellRenderer)res;
        }
        return null;
    }

    private TableCellEditor getCustomEditor(int row) {
        FeatureDescriptor fd = this.getPropertySetModel().getFeatureDescriptor(row);
        if (fd instanceof Node.PropertySet) {
            return null;
        }
        Object res = fd.getValue("custom.cell.editor");
        if (res instanceof TableCellEditor) {
            this.prepareCustomEditor(res);
            return (TableCellEditor)res;
        }
        return null;
    }

    private void prepareCustomEditor(Object customEditorObj) {
        JComboBox comboBox = null;
        if (customEditorObj instanceof DefaultCellEditor) {
            if (((DefaultCellEditor)customEditorObj).getComponent() instanceof JComboBox) {
                comboBox = (JComboBox)((DefaultCellEditor)customEditorObj).getComponent();
            }
        } else if (customEditorObj instanceof JComboBox) {
            comboBox = (JComboBox)customEditorObj;
        }
        if (null != comboBox && !(comboBox.getUI() instanceof CleanComboUI)) {
            comboBox.setUI(new CleanComboUI(true));
            ComboBoxAutoCompleteSupport.install(comboBox);
        }
    }

    void setBeanName(String name) {
        this.beanName = name;
    }

    @Override
    public String getBeanName() {
        return this.beanName;
    }

    @Override
    public TableCellEditor getCellEditor(int row, int column) {
        TableCellEditor res;
        if (0 == column && null != (res = this.getCustomEditor(row))) {
            return res;
        }
        return this.getEditor();
    }

    @Override
    public TableCellRenderer getCellRenderer(int row, int column) {
        TableCellRenderer res;
        if (0 == column && null != (res = this.getCustomRenderer(row))) {
            return res;
        }
        return this.getRenderer();
    }

    @Override
    public void setModel(TableModel model) {
        if (this.initialized) {
            throw new UnsupportedOperationException("Changing the model of a property sheet table is not supported.  If you want to change the set of properties, ordering or other characteristings, see setPropertySetModel().");
        }
        super.setModel(model);
    }

    @Override
    public void setColumnModel(TableColumnModel model) {
        if (this.initialized) {
            throw new UnsupportedOperationException("Changing the column model of a property sheet table is not supported.  If you want to change the set of properties, ordering or other characteristings, see setPropertySetModel().");
        }
        super.setColumnModel(model);
    }

    @Override
    public void setSelectionModel(ListSelectionModel model) {
        if (this.initialized) {
            throw new UnsupportedOperationException("Changing the selection model of a property sheet table is not supported.  If you want to change the set of properties, ordering or other characteristings, see setPropertySetModel().");
        }
        super.setSelectionModel(model);
    }

    public void setPropertySetModel(PropertySetModel psm) {
        PropertySetModel old = this.getSheetModel().getPropertySetModel();
        if (old == psm) {
            return;
        }
        if (old != null) {
            old.removePropertySetModelListener(this);
        }
        this.getSheetModel().setPropertySetModel(psm);
        psm.addPropertySetModelListener(this);
    }

    PropertySetModel getPropertySetModel() {
        return this.getSheetModel().getPropertySetModel();
    }

    SheetTableModel getSheetModel() {
        return (SheetTableModel)this.getModel();
    }

    @Override
    public JTableHeader getTableHeader() {
        return null;
    }

    @Override
    protected void initKeysAndActions() {
        super.initKeysAndActions();
        this.unregisterKeyboardAction(KeyStroke.getKeyStroke(39, 0));
        this.unregisterKeyboardAction(KeyStroke.getKeyStroke(37, 0));
        this.expandAction = new ExpandAction();
        this.collapseAction = new CollapseAction();
        this.edClassAction = new EditorClassAction();
        InputMap imp = this.getInputMap();
        InputMap impAncestor = this.getInputMap(1);
        ActionMap am = this.getActionMap();
        KeyStroke ks = KeyStroke.getKeyStroke(67, 2);
        imp.put(ks, null);
        imp.put(KeyStroke.getKeyStroke(37, 0), "expandSet");
        imp.put(KeyStroke.getKeyStroke(39, 0), "collapseSet");
        if (!GraphicsEnvironment.isHeadless()) {
            imp.put(KeyStroke.getKeyStroke(36, 64 | Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "edclass");
        }
        imp.put(KeyStroke.getKeyStroke(9, 0), "next");
        imp.put(KeyStroke.getKeyStroke(9, 64), "prev");
        impAncestor.put(KeyStroke.getKeyStroke(32, 128), "invokeCustomEditor");
        impAncestor.remove(KeyStroke.getKeyStroke(39, 0));
        impAncestor.remove(KeyStroke.getKeyStroke(37, 0));
        am.put("expandSet", this.expandAction);
        am.put("collapseSet", this.collapseAction);
        am.put("invokeCustomEditor", this.getCustomEditorAction());
        am.put("edclass", this.edClassAction);
        Action defaultAction = am.get("selectNextRow");
        if (null != defaultAction) {
            am.put("selectNextRow", new IncrementAction(false, defaultAction));
        }
        if (null != (defaultAction = am.get("selectPreviousRow"))) {
            am.put("selectPreviousRow", new IncrementAction(true, defaultAction));
        }
    }

    Action getCustomEditorAction() {
        if (this.customEditorAction == null) {
            this.customEditorAction = new CustomEditorAction(this);
        }
        return this.customEditorAction;
    }

    @Override
    protected boolean matchText(Object value, String text) {
        if (value instanceof FeatureDescriptor) {
            return ((FeatureDescriptor)value).getDisplayName().toUpperCase().startsWith(text.toUpperCase());
        }
        return false;
    }

    @Override
    public void paintComponent(Graphics g) {
        boolean includeMargin = PropUtils.shouldDrawMargin(this.getPropertySetModel());
        this.getRenderer().setIncludeMargin(includeMargin);
        super.paintComponent(g);
        if (!PropUtils.noAltBg()) {
            this.paintCenterLine(g);
        }
        if (includeMargin) {
            this.paintMargin(g);
        }
        this.paintExpandableSets(g);
        this.lastIncludeMargin = includeMargin;
    }

    private void paintComponent(Graphics g, Component c, int x, int y, int w, int h) {
        c.setBounds(x, y, w, h);
        g.translate(x, y);
        c.paint(g);
        g.translate(- x, - y);
        c.setBounds(- w, - h, 0, 0);
    }

    private void paintCenterLine(Graphics g) {
        Color c = PropUtils.getAltBg();
        g.setColor(c);
        int xpos = this.getColumn(SheetColumnModel.NAMES_IDENTIFIER).getWidth() - 1;
        g.drawLine(xpos, 0, xpos, this.getHeight());
    }

    void repaintProperty(String name) {
        if (!this.isShowing()) {
            return;
        }
        if (PropUtils.isLoggable(SheetTable.class)) {
            PropUtils.log(SheetTable.class, "RepaintProperty: " + name);
        }
        PropertySetModel psm = this.getPropertySetModel();
        int min = this.getFirstVisibleRow();
        if (min == -1) {
            return;
        }
        int max = min + this.getVisibleRowCount();
        for (int i = min; i < max; ++i) {
            FeatureDescriptor fd = psm.getFeatureDescriptor(i);
            if (null == fd || !fd.getName().equals(name)) continue;
            this.paintRow(i);
            return;
        }
        if (PropUtils.isLoggable(SheetTable.class)) {
            PropUtils.log(SheetTable.class, "Property is either scrolled offscreen or property name is bogus: " + name);
        }
    }

    private void paintMargin(Graphics g) {
        g.setColor(PropUtils.getSetRendererColor());
        int w = PropUtils.getMarginWidth();
        int h = this.getHeight();
        if (g.hitClip(0, 0, w, h)) {
            g.fillRect(0, 0, w, h);
        }
    }

    @Override
    public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
        Component result = super.prepareRenderer(renderer, row, col);
        if (row < 0 || row >= this.getRowCount()) {
            return result;
        }
        Object value = this.getValueAt(row, col);
        if (result != null && value instanceof Node.Property && col == 1) {
            result.setEnabled(((Node.Property)value).canWrite());
        }
        return result;
    }

    private void paintExpandableSets(Graphics g) {
        int start = 0;
        int end = this.getRowCount();
        Insets ins = this.getInsets();
        boolean canBeSelected = this.isKnownComponent(KeyboardFocusManager.getCurrentKeyboardFocusManager().getPermanentFocusOwner());
        for (int i = 0; i < end; ++i) {
            boolean selected;
            boolean isHtml;
            int idx = start + i;
            Object value = this.getValueAt(idx, 0);
            if (!(value instanceof Node.PropertySet)) continue;
            Rectangle r = this.getCellRect(idx, 0, false);
            r.x = ins.left;
            r.width = this.getWidth() - (ins.left + ins.right);
            if (!g.hitClip(r.x, r.y, r.width, r.height)) continue;
            Node.PropertySet ps = (Node.PropertySet)value;
            String txt = ps.getHtmlDisplayName();
            boolean bl = isHtml = txt != null;
            if (!isHtml) {
                txt = ps.getDisplayName();
            }
            if (this.htmlrenderer == null) {
                this.htmlrenderer = HtmlRenderer.createRenderer();
            }
            JComponent painter = (JComponent)this.htmlrenderer.getTableCellRendererComponent((JTable)this, (Object)txt, false, false, idx, 0);
            this.htmlrenderer.setHtml(isHtml);
            this.htmlrenderer.setParentFocused(true);
            this.htmlrenderer.setIconTextGap(2);
            this.htmlrenderer.setIcon(this.getPropertySetModel().isExpanded((FeatureDescriptor)ps) ? PropUtils.getExpandedIcon() : PropUtils.getCollapsedIcon());
            boolean bl2 = selected = canBeSelected && this.getSelectedRow() == idx;
            if (!selected) {
                painter.setBackground(PropUtils.getSetRendererColor());
                painter.setForeground(PropUtils.getSetForegroundColor());
            } else {
                painter.setBackground(PropUtils.getSelectedSetRendererColor());
                painter.setForeground(PropUtils.getSelectedSetForegroundColor());
            }
            painter.setFont(painter.getFont().deriveFont(1));
            if (PropUtils.isAqua) {
                painter.setOpaque(false);
                Graphics2D g2d = (Graphics2D)g;
                Paint oldPaint = g2d.getPaint();
                g2d.setPaint(new GradientPaint(r.x, r.y, Color.white, r.x, r.y + r.height / 2, painter.getBackground()));
                g2d.fillRect(r.x, r.y, r.width, r.height);
                g2d.setPaint(oldPaint);
            } else {
                painter.setOpaque(true);
            }
            this.paintComponent(g, painter, r.x, r.y, r.width, r.height);
        }
    }

    @Override
    public void editingStopped(ChangeEvent e) {
        super.editingStopped(e);
        if (!PropUtils.psCommitOnFocusLoss && !this.getEditor().isLastUpdateSuccessful()) {
            this.countDown = 2;
        }
    }

    private void autoEdit() {
        this.editCellAt(this.getSelectedRow(), this.getSelectedColumn(), null);
        if (this.editorComp != null) {
            this.editorComp.requestFocus();
        }
        this.countDown = -1;
    }

    @Override
    public void changeSelection(int row, int col, boolean toggle, boolean extend) {
        this.countDown = -1;
        super.changeSelection(row, col, toggle, extend);
    }

    public void changeSelection(FeatureDescriptor fd, boolean toggle, boolean extend) {
        int index = this.getIndex(fd);
        if (index >= 0) {
            this.countDown = -1;
            super.changeSelection(index, 0, toggle, extend);
        }
    }

    public void paintRow(FeatureDescriptor fd) {
        int index = this.getIndex(fd);
        if (index >= 0) {
            this.paintRow(index);
        }
    }

    public int getIndex(FeatureDescriptor fd) {
        PropertySetModel psm = this.getPropertySetModel();
        int index = psm.indexOf(fd);
        return index;
    }

    @Override
    public void processFocusEvent(FocusEvent fe) {
        super.processFocusEvent(fe);
        if (fe.getID() == 1004) {
            --this.countDown;
            if (this.countDown == 0) {
                this.autoEdit();
            }
        }
        if (fe.getID() == 1004 || fe.getOppositeComponent() != null && fe.getID() == 1005 && !this.isAncestorOf(fe.getOppositeComponent())) {
            this.fireChange();
        }
    }

    @Override
    protected void focusLostCancel() {
        if (PropUtils.psCommitOnFocusLoss && this.isEditing()) {
            this.getEditor().stopCellEditing();
        } else {
            super.focusLostCancel();
        }
    }

    @Override
    public void processMouseEvent(MouseEvent me) {
        if (me.getID() == 501 && SwingUtilities.isLeftMouseButton(me)) {
            int col;
            int row;
            if (me.getClickCount() > 1) {
                FeatureDescriptor fd;
                Object mouseListener;
                row = this.rowAtPoint(me.getPoint());
                col = this.columnAtPoint(me.getPoint());
                if (col == 0 && null != (fd = this.getPropertySetModel().getFeatureDescriptor(row)) && (mouseListener = fd.getValue("nb.propertysheet.mouse.doubleclick.listener")) instanceof MouseListener) {
                    ((MouseListener)mouseListener).mouseClicked(me);
                    return;
                }
            }
            if (this.onCustomEditorButton(me) && !this.hasFocus()) {
                if (PropUtils.psCommitOnFocusLoss && this.isEditing()) {
                    this.getEditor().stopCellEditing();
                    if (this.isGoingToBeClosed()) {
                        return;
                    }
                }
                row = this.rowAtPoint(me.getPoint());
                col = this.columnAtPoint(me.getPoint());
                if (row != -1 && col != -1) {
                    this.changeSelection(row, col, false, false);
                    this.getCustomEditorAction().actionPerformed(new ActionEvent(this, 1001, "invokeCustomEditor"));
                    me.consume();
                    return;
                }
            }
        }
        super.processMouseEvent(me);
    }

    @Override
    public void setValueAt(Object o, int row, int column) {
    }

    @Override
    protected boolean isKnownComponent(Component c) {
        boolean result = super.isKnownComponent(c);
        if (result) {
            return result;
        }
        if (c == null) {
            return false;
        }
        if (c instanceof ButtonPanel) {
            return true;
        }
        InplaceEditor ie = this.getEditor().getInplaceEditor();
        if (ie != null) {
            JComponent comp = ie.getComponent();
            if (comp == c) {
                return true;
            }
            if (comp.isAncestorOf(c)) {
                return true;
            }
        }
        if (c.getParent() instanceof ButtonPanel) {
            return true;
        }
        if (this.getParent() != null && this.getParent().isAncestorOf(c)) {
            return true;
        }
        Container par = this.getParent();
        if (par != null && par.isAncestorOf(c)) {
            return true;
        }
        if (c instanceof InplaceEditor) {
            return true;
        }
        InplaceEditor ine = this.getEditor().getInplaceEditor();
        if (ine != null) {
            return ine.isKnownComponent(c);
        }
        return false;
    }

    private boolean onCustomEditorButton(MouseEvent e) {
        boolean success;
        PropertyEditor pe;
        Point pt = e.getPoint();
        int row = this.rowAtPoint(pt);
        int col = this.columnAtPoint(pt);
        FeatureDescriptor fd = this.getSheetModel().getPropertySetModel().getFeatureDescriptor(row);
        if (null == fd) {
            return false;
        }
        if (PropUtils.noCustomButtons) {
            success = false;
        } else {
            boolean bl = success = e.getX() > this.getWidth() - PropUtils.getCustomButtonWidth();
        }
        if ((e.getID() == 501 || e.getID() == 502 || e.getID() == 500) && !(success |= Boolean.FALSE.equals(fd.getValue("canEditAsText"))) && fd instanceof Node.Property && (pe = PropUtils.getPropertyEditor((Node.Property)fd)) != null && pe.supportsCustomEditor()) {
            success |= pe.isPaintable() && pe.getAsText() == null && pe.getTags() == null;
        }
        try {
            if (success && fd instanceof Node.Property && col == 1) {
                boolean supp = PropUtils.getPropertyEditor((Node.Property)fd).supportsCustomEditor();
                return supp;
            }
        }
        catch (IllegalStateException ise) {
            Logger.getLogger(SheetTable.class.getName()).log(Level.WARNING, null, ise);
        }
        return false;
    }

    @Override
    public String getToolTipText(MouseEvent e) {
        String result;
        if (this.customEditorIsOpen) {
            return null;
        }
        Point pt = e.getPoint();
        int row = this.rowAtPoint(pt);
        int col = this.columnAtPoint(pt);
        if (col == 1 && this.onCustomEditorButton(e)) {
            result = NbBundle.getMessage(SheetTable.class, (String)"CTL_EDBUTTON_TIP");
        } else {
            result = this.getSheetModel().getDescriptionFor(row, col);
            if (col == 1 && result != null && result.length() > 100) {
                result = PropUtils.createHtmlTooltip(this.getPropertySetModel().getFeatureDescriptor(row).getDisplayName(), result);
            }
        }
        if (result != null && "".equals(result.trim())) {
            result = null;
        }
        return result;
    }

    @Override
    public final FeatureDescriptor getSelection() {
        return this._getSelection();
    }

    public final FeatureDescriptor _getSelection() {
        int i = this.getSelectedRow();
        FeatureDescriptor result = i < this.getPropertySetModel().getCount() ? this.getSheetModel().getPropertySetModel().getFeatureDescriptor(this.getSelectedRow()) : null;
        return result;
    }

    public void select(FeatureDescriptor fd, boolean startEditing) {
        PropertySetModel psm = this.getPropertySetModel();
        int index = psm.indexOf(fd);
        if (index < 0) {
            return;
        }
        this.getSelectionModel().setSelectionInterval(index, index);
        if (startEditing && psm.isProperty(index)) {
            this.editCellAt(index, 1, new MouseEvent(this, 0, System.currentTimeMillis(), 0, 0, 0, 1, false));
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    InplaceEditor inplace;
                    SheetCellEditor cellEditor = SheetTable.this.getEditor();
                    if (null != cellEditor && null != (inplace = cellEditor.getInplaceEditor()) && null != inplace.getComponent()) {
                        inplace.getComponent().requestFocus();
                    }
                }
            });
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean editCellAt(int row, int column, EventObject e) {
        boolean useRadioButtons;
        assert (SwingUtilities.isEventDispatchThread());
        this.enterEditRequest();
        if (this.editingRow == row && this.isEditing()) {
            if (0 == column) {
                this.getEditor().stopCellEditing();
                this.removeEditor();
            }
            this.exitEditRequest();
            return false;
        }
        if (PropUtils.psCommitOnFocusLoss && this.isEditing()) {
            this.getEditor().stopCellEditing();
            if (this.isGoingToBeClosed()) {
                return false;
            }
        }
        if (e instanceof MouseEvent && this.onCenterLine((MouseEvent)e)) {
            this.exitEditRequest();
            return false;
        }
        if (e instanceof MouseEvent && this.onCustomEditorButton((MouseEvent)e)) {
            if (PropUtils.isLoggable(SheetTable.class)) {
                PropUtils.log(SheetTable.class, "Got a mouse click on the custom editor button");
            }
            if (this.isEditing() && this.editingRow != row) {
                this.removeEditor();
            }
            int prevSel = this.getSelectedRow();
            this.changeSelection(row, column, false, false);
            if (prevSel != -1) {
                this.paintRow(prevSel);
            }
            this.paintSelectionRow();
            this.getCustomEditorAction().actionPerformed(new ActionEvent(this, 0, null));
            this.exitEditRequest();
            return false;
        }
        FeatureDescriptor fd = this.getPropertySetModel().getFeatureDescriptor(row);
        if (fd instanceof Node.PropertySet) {
            if (this.isEditing()) {
                this.removeEditor();
                this.changeSelection(row, column, false, false);
            }
            this.maybeToggleExpanded(row, e);
            this.exitEditRequest();
            return false;
        }
        boolean bl = useRadioButtons = e instanceof MouseEvent && PropUtils.forceRadioButtons || fd != null && fd.getValue("stringValues") != null;
        if (!useRadioButtons && (column == 1 || e instanceof KeyEvent) && this.checkEditBoolean(row)) {
            this.exitEditRequest();
            return false;
        }
        boolean result = false;
        try {
            result = super.editCellAt(row, column, e);
        }
        finally {
            this.exitEditRequest();
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeEditor() {
        this.enterEditorRemoveRequest();
        try {
            super.removeEditor();
            this.getEditor().setInplaceEditor(null);
        }
        finally {
            this.exitEditorRemoveRequest();
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        boolean result;
        if (column == 0) {
            return null != this.getCustomEditor(row);
        }
        FeatureDescriptor fd = this.getPropertySetModel().getFeatureDescriptor(row);
        if (fd instanceof Node.PropertySet) {
            result = false;
        } else {
            Object val;
            Node.Property p = (Node.Property)fd;
            result = p.canWrite();
            if (result && (val = p.getValue("canEditAsText")) != null && !(result &= Boolean.TRUE.equals(val))) {
                PropertyEditor ped = PropUtils.getPropertyEditor(p);
                result |= ped.getTags() != null;
            }
        }
        return result;
    }

    private void maybeToggleExpanded(int row, EventObject e) {
        boolean doExpand = true;
        if (e instanceof MouseEvent) {
            MouseEvent me = (MouseEvent)e;
            boolean bl = doExpand = me.getClickCount() > 1;
            if (!doExpand) {
                boolean bl2 = doExpand = me.getPoint().x <= PropUtils.getMarginWidth();
            }
        }
        if (doExpand) {
            this.toggleExpanded(row);
        }
    }

    private void toggleExpanded(int index) {
        if (this.isEditing()) {
            this.getEditor().cancelCellEditing();
        }
        PropertySetModel psm = this.getSheetModel().getPropertySetModel();
        psm.toggleExpanded(index);
    }

    boolean checkEditBoolean(int row) {
        Class c;
        Node.Property p;
        FeatureDescriptor fd = this.getSheetModel().getPropertySetModel().getFeatureDescriptor(row);
        if (fd != null && fd.getValue("stringValues") != null) {
            return false;
        }
        Node.Property property = p = fd instanceof Node.Property ? (Node.Property)fd : null;
        if (p != null && ((c = p.getValueType()) == Boolean.class || c == Boolean.TYPE)) {
            if (!this.isCellEditable(row, 1)) {
                return true;
            }
            try {
                Boolean b;
                block9 : {
                    b = null;
                    try {
                        Object value = p.getValue();
                        if (value instanceof Boolean) {
                            b = (Boolean)value;
                            break block9;
                        }
                        return false;
                    }
                    catch (ProxyNode.DifferentValuesException dve) {
                        b = Boolean.FALSE;
                    }
                }
                if (this.isEditing()) {
                    this.removeEditor();
                }
                this.changeSelection(row, 1, false, false);
                Boolean newValue = b == null || Boolean.FALSE.equals(b) ? Boolean.TRUE : Boolean.FALSE;
                p.setValue((Object)newValue);
                this.paintRow(row);
                return true;
            }
            catch (Exception ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return false;
    }

    @Override
    public Component prepareEditor(TableCellEditor editor, int row, int col) {
        InplaceEditor ine;
        if (editor == null) {
            return null;
        }
        Component result = super.prepareEditor(editor, row, col);
        if (result == null) {
            return null;
        }
        if (1 == col && (ine = this.getEditor().getInplaceEditor()).supportsTextEntry()) {
            result.setBackground(PropUtils.getTextFieldBackground());
            result.setForeground(PropUtils.getTextFieldForeground());
        }
        if (result instanceof JComponent) {
            ((JComponent)result).setBorder(BorderFactory.createEmptyBorder(0, PropUtils.getTextMargin(), 0, 0));
        }
        return result;
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        boolean ed = this.isEditing();
        int n = this.lastSelectedRow = ed ? this.getEditingRow() : this.getSelectionModel().getAnchorSelectionIndex();
        if (ed) {
            this.getEditor().stopCellEditing();
        }
        super.tableChanged(e);
        this.restoreEditingState();
    }

    void saveEditingState() {
        InplaceEditor ine;
        this.storedFd = this._getSelection();
        if (this.isEditing() && (ine = this.getEditor().getInplaceEditor()) != null) {
            this.partialValue = ine.getValue();
        }
    }

    void restoreEditingState() {
        boolean canResumeEditing;
        int idx = this.indexOfLastSelected();
        boolean bl = canResumeEditing = idx != -1;
        if (!canResumeEditing) {
            idx = this.lastSelectedRow;
        }
        if (idx == -1) {
            this.clearSavedEditingState();
            return;
        }
        if (idx < this.getRowCount()) {
            this.changeSelection(idx, 1, false, false);
            if (canResumeEditing && this.wasEditing) {
                this.editCellAt(idx, 1);
                InplaceEditor ine = this.getEditor().getInplaceEditor();
                if (ine != null && this.partialValue != null) {
                    ine.setValue(this.partialValue);
                }
            }
        }
        this.clearSavedEditingState();
    }

    private void clearSavedEditingState() {
        this.storedFd = null;
        this.wasEditing = false;
        this.partialValue = null;
    }

    private int indexOfLastSelected() {
        if (this.storedFd == null) {
            return -1;
        }
        PropertySetModel mdl = this.getPropertySetModel();
        int idx = mdl.indexOf(this.storedFd);
        this.storedFd = null;
        return idx;
    }

    @Override
    public void pendingChange(PropertySetModelEvent e) {
        if (e.isReordering()) {
            this.wasEditing = this.isEditing();
            this.saveEditingState();
        } else {
            this.storedFd = null;
            this.wasEditing = false;
            this.partialValue = null;
        }
    }

    @Override
    public void boundedChange(PropertySetModelEvent e) {
    }

    @Override
    public void wholesaleChange(PropertySetModelEvent e) {
    }

    @Override
    public Component getCursorChangeComponent() {
        Container cont = this.getTopLevelAncestor();
        return cont instanceof JFrame ? ((JFrame)cont).getContentPane() : (cont instanceof JDialog ? ((JDialog)cont).getContentPane() : cont);
    }

    @Override
    public Object getPartialValue() {
        Object partialValue = null;
        if (this.isEditing() && this.editingRow == this.getSelectedRow()) {
            InplaceEditor ine = this.getEditor().getInplaceEditor();
            if (ine != null) {
                partialValue = ine.getValue();
                ine.reset();
                this.getEditor().cancelCellEditing();
            }
        } else {
            partialValue = null;
            if (this.isEditing()) {
                this.removeEditor();
            }
        }
        return partialValue;
    }

    @Override
    public void editorClosed() {
        if (this.lastFailed) {
            this.editCellAt(this.getSelectedRow(), 1, null);
        }
        this.repaint();
        this.customEditorIsOpen = false;
    }

    @Override
    public void editorOpened() {
        this.paintSelectionRow();
        this.customEditorIsOpen = true;
    }

    @Override
    public void editorOpening() {
        this.lastFailed = false;
        this.customEditorIsOpen = true;
    }

    @Override
    public void valueChanged(PropertyEditor editor) {
        this.lastFailed = false;
    }

    @Override
    public boolean allowInvoke() {
        return true;
    }

    @Override
    public void failed() {
        this.lastFailed = true;
    }

    @Override
    public boolean wantAllChanges() {
        return false;
    }

    @Override
    public ReusablePropertyEnv getReusablePropertyEnv() {
        return this.reusableEnv;
    }

    public ReusablePropertyModel getReusablePropertyModel() {
        return this.reusableModel;
    }

    private boolean isGoingToBeClosed() {
        return this.getRowCount() <= 0;
    }

    @Override
    public void setUI(TableUI ui) {
        super.setUI(ui);
        this.renderer = null;
        this.sheetCellEditor = null;
    }

    private class IncrementAction
    extends AbstractAction {
        private final boolean isIncrement;
        private final Action changeRowAction;

        private IncrementAction(boolean increment, Action defaultAction) {
            this.isIncrement = increment;
            this.changeRowAction = defaultAction;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            SheetCellEditor cellEditor;
            InplaceEditor inplaceEditor;
            if (SheetTable.this.isEditing() && (inplaceEditor = (cellEditor = SheetTable.this.getEditor()).getInplaceEditor()) instanceof IncrementPropertyValueSupport) {
                boolean consume;
                IncrementPropertyValueSupport incrementSupport = (IncrementPropertyValueSupport)((Object)inplaceEditor);
                boolean bl = consume = this.isIncrement ? incrementSupport.incrementValue() : incrementSupport.decrementValue();
                if (consume) {
                    return;
                }
            }
            this.changeRowAction.actionPerformed(e);
        }
    }

    private static class SheetTableTransferable
    implements Transferable {
        private static DataFlavor[] stringFlavors;
        private static DataFlavor[] plainFlavors;
        protected String plainData;

        public SheetTableTransferable(String plainData) {
            this.plainData = plainData;
        }

        @Override
        public DataFlavor[] getTransferDataFlavors() {
            int nPlain = this.isPlainSupported() ? plainFlavors.length : 0;
            int nString = this.isPlainSupported() ? stringFlavors.length : 0;
            int nFlavors = nPlain + nString;
            DataFlavor[] flavors = new DataFlavor[nFlavors];
            int nDone = 0;
            if (nPlain > 0) {
                System.arraycopy(plainFlavors, 0, flavors, nDone, nPlain);
                nDone += nPlain;
            }
            if (nString > 0) {
                System.arraycopy(stringFlavors, 0, flavors, nDone, nString);
                nDone += nString;
            }
            return flavors;
        }

        @Override
        public boolean isDataFlavorSupported(DataFlavor flavor) {
            DataFlavor[] flavors = this.getTransferDataFlavors();
            for (int i = 0; i < flavors.length; ++i) {
                if (!flavors[i].equals(flavor)) continue;
                return true;
            }
            return false;
        }

        @Override
        public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
            if (this.isPlainFlavor(flavor)) {
                String data = this.getPlainData();
                String string = data = data == null ? "" : data;
                if (String.class.equals(flavor.getRepresentationClass())) {
                    return data;
                }
                if (Reader.class.equals(flavor.getRepresentationClass())) {
                    return new StringReader(data);
                }
                if (InputStream.class.equals(flavor.getRepresentationClass())) {
                    return new StringBufferInputStream(data);
                }
            } else if (this.isStringFlavor(flavor)) {
                String data = this.getPlainData();
                data = data == null ? "" : data;
                return data;
            }
            throw new UnsupportedFlavorException(flavor);
        }

        protected boolean isPlainFlavor(DataFlavor flavor) {
            DataFlavor[] flavors = plainFlavors;
            for (int i = 0; i < flavors.length; ++i) {
                if (!flavors[i].equals(flavor)) continue;
                return true;
            }
            return false;
        }

        protected boolean isPlainSupported() {
            return this.plainData != null;
        }

        protected String getPlainData() {
            return this.plainData;
        }

        protected boolean isStringFlavor(DataFlavor flavor) {
            DataFlavor[] flavors = stringFlavors;
            for (int i = 0; i < flavors.length; ++i) {
                if (!flavors[i].equals(flavor)) continue;
                return true;
            }
            return false;
        }

        static {
            block2 : {
                try {
                    plainFlavors = new DataFlavor[3];
                    SheetTableTransferable.plainFlavors[0] = new DataFlavor("text/plain;class=java.lang.String");
                    SheetTableTransferable.plainFlavors[1] = new DataFlavor("text/plain;class=java.io.Reader");
                    SheetTableTransferable.plainFlavors[2] = new DataFlavor("text/plain;charset=unicode;class=java.io.InputStream");
                    stringFlavors = new DataFlavor[2];
                    SheetTableTransferable.stringFlavors[0] = new DataFlavor("application/x-java-jvm-local-objectref;class=java.lang.String");
                    SheetTableTransferable.stringFlavors[1] = DataFlavor.stringFlavor;
                }
                catch (ClassNotFoundException cle) {
                    if ($assertionsDisabled) break block2;
                    throw new AssertionError(cle);
                }
            }
        }
    }

    private static class SheetTableTransferHandler
    extends TransferHandler {
        private SheetTableTransferHandler() {
        }

        @Override
        protected Transferable createTransferable(JComponent c) {
            if (c instanceof SheetTable) {
                SheetTable table = (SheetTable)c;
                FeatureDescriptor fd = table.getSelection();
                if (fd == null) {
                    return null;
                }
                String res = fd.getDisplayName();
                if (fd instanceof Node.Property) {
                    Node.Property prop = (Node.Property)fd;
                    res = res + "\t" + PropUtils.getPropertyEditor(prop).getAsText();
                }
                return new SheetTableTransferable(res);
            }
            return null;
        }

        @Override
        public int getSourceActions(JComponent c) {
            return 1;
        }
    }

    private class STPolicy
    extends ContainerOrderFocusTraversalPolicy {
        private STPolicy() {
        }

        @Override
        public Component getComponentAfter(Container focusCycleRoot, Component aComponent) {
            if (SheetTable.this.inEditorRemoveRequest()) {
                return SheetTable.this;
            }
            Component result = super.getComponentAfter(focusCycleRoot, aComponent);
            return result;
        }

        @Override
        public Component getComponentBefore(Container focusCycleRoot, Component aComponent) {
            if (SheetTable.this.inEditorRemoveRequest()) {
                return SheetTable.this;
            }
            return super.getComponentBefore(focusCycleRoot, aComponent);
        }

        @Override
        public Component getFirstComponent(Container focusCycleRoot) {
            if (!SheetTable.this.inEditorRemoveRequest() && SheetTable.this.isEditing()) {
                return SheetTable.this.editorComp;
            }
            return SheetTable.this;
        }

        @Override
        public Component getDefaultComponent(Container focusCycleRoot) {
            if (!SheetTable.this.inEditorRemoveRequest() && SheetTable.this.isEditing() && SheetTable.this.editorComp.isShowing()) {
                return SheetTable.this.editorComp;
            }
            return SheetTable.this;
        }

        @Override
        protected boolean accept(Component aComponent) {
            InplaceEditor ine;
            if (SheetTable.this.isEditing() && SheetTable.this.inEditorRemoveRequest() && (ine = SheetTable.this.getEditor().getInplaceEditor()) != null && (aComponent == ine.getComponent() || ine.isKnownComponent(aComponent))) {
                return false;
            }
            return super.accept(aComponent) && aComponent.isShowing();
        }
    }

    private class EditorClassAction
    extends AbstractAction {
        public EditorClassAction() {
            super("edclass");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            int i = SheetTable.this.getSelectedRow();
            if (i != -1) {
                FeatureDescriptor fd = SheetTable.this.getPropertySetModel().getFeatureDescriptor(i);
                if (fd instanceof Node.Property) {
                    PropertyEditor ped = PropUtils.getPropertyEditor((Node.Property)fd);
                    System.err.println(ped.getClass().getName());
                } else {
                    System.err.println("PropertySets - no editor");
                }
            } else {
                System.err.println("No selection");
            }
        }

        @Override
        public boolean isEnabled() {
            return SheetTable.this.getSelectedRow() != -1;
        }
    }

    private class CollapseAction
    extends AbstractAction {
        public CollapseAction() {
            super("collapseSet");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            FeatureDescriptor fd = SheetTable.this._getSelection();
            if (fd instanceof Node.PropertySet) {
                int row = SheetTable.this.getSelectedRow();
                boolean b = SheetTable.this.getPropertySetModel().isExpanded(fd);
                if (!b) {
                    SheetTable.this.toggleExpanded(row);
                }
            }
        }

        @Override
        public boolean isEnabled() {
            boolean result = SheetTable.this._getSelection() instanceof Node.PropertySet;
            return result;
        }
    }

    private class ExpandAction
    extends AbstractAction {
        public ExpandAction() {
            super("expandSet");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            FeatureDescriptor fd = SheetTable.this._getSelection();
            if (fd instanceof Node.PropertySet) {
                int row = SheetTable.this.getSelectedRow();
                boolean b = SheetTable.this.getPropertySetModel().isExpanded(fd);
                if (b) {
                    SheetTable.this.toggleExpanded(row);
                }
            }
        }

        @Override
        public boolean isEnabled() {
            return SheetTable.this._getSelection() instanceof Node.PropertySet;
        }
    }

}

