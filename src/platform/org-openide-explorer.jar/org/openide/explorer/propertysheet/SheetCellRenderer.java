/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 *  org.openide.awt.HtmlRenderer$Renderer
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 */
package org.openide.explorer.propertysheet;

import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.beans.FeatureDescriptor;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;
import org.openide.awt.HtmlRenderer;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.RendererFactory;
import org.openide.explorer.propertysheet.ReusablePropertyEnv;
import org.openide.explorer.propertysheet.ReusablePropertyModel;
import org.openide.nodes.Node;

final class SheetCellRenderer
implements TableCellRenderer {
    private RendererFactory factory = null;
    private boolean tableUI;
    boolean includeMargin = false;
    private ReusablePropertyEnv reusableEnv;
    private ReusablePropertyModel reusableModel;
    boolean suppressButton = false;
    int rbMax = 0;
    private JLabel htmlLabel = HtmlRenderer.createLabel();

    SheetCellRenderer(boolean tableUI, ReusablePropertyEnv env, ReusablePropertyModel reusableModel) {
        this.tableUI = tableUI;
        this.reusableEnv = env;
        this.reusableModel = reusableModel;
    }

    void setIncludeMargin(boolean val) {
        this.includeMargin = val;
    }

    void setSuppressButton(boolean val) {
        this.suppressButton = val;
    }

    void setRadioButtonMax(int i) {
        this.rbMax = i;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean hasFocus, int row, int column) {
        JComponent result;
        FeatureDescriptor fd = (FeatureDescriptor)value;
        selected |= hasFocus && table.getSelectedRow() == row;
        if (fd == null || fd instanceof Node.PropertySet) {
            return new JLabel();
        }
        if (column == 0) {
            boolean isHtml;
            String txt = ((Node.Property)fd).getHtmlDisplayName();
            boolean bl = isHtml = txt != null;
            if (!isHtml) {
                txt = fd.getDisplayName();
            }
            JLabel lbl = this.htmlLabel;
            HtmlRenderer.Renderer ren = (HtmlRenderer.Renderer)lbl;
            ren.setHtml(isHtml);
            lbl.setText(txt);
            if (selected) {
                lbl.setBackground(table.getSelectionBackground());
                lbl.setForeground(table.getSelectionForeground());
            } else {
                lbl.setBackground(table.getBackground());
                lbl.setForeground(table.getForeground());
            }
            lbl.setOpaque(selected);
            if (this.includeMargin) {
                lbl.setBorder(BorderFactory.createMatteBorder(0, PropUtils.getMarginWidth() + 2, 0, 0, lbl.getBackground()));
            } else {
                lbl.setBorder(BorderFactory.createMatteBorder(0, PropUtils.getTextMargin(), 0, 0, lbl.getBackground()));
            }
            Object o = fd.getValue("nameIcon");
            if (o instanceof Icon) {
                lbl.setIcon((Icon)o);
            } else if (o instanceof Image) {
                lbl.setIcon(new ImageIcon((Image)o));
            } else {
                lbl.setIcon(null);
            }
            result = lbl;
        } else {
            result = this.factory().getRenderer((Node.Property)fd);
            if (selected) {
                result.setBackground(table.getSelectionBackground());
                result.setForeground(table.getSelectionForeground());
            } else {
                result.setBackground(table.getBackground());
                result.setForeground(table.getForeground());
            }
            result.setOpaque(selected);
        }
        return result;
    }

    RendererFactory factory() {
        if (this.factory == null) {
            this.factory = new RendererFactory(true, this.reusableEnv, this.reusableModel);
        }
        return this.factory;
    }
}

