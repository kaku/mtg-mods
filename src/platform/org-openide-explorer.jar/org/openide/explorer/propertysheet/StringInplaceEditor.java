/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.propertysheet;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.FocusEvent;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.beans.FeatureDescriptor;
import java.beans.PropertyEditor;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.Border;
import org.openide.explorer.propertysheet.IncrementPropertyValueSupport;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;

class StringInplaceEditor
extends JTextField
implements InplaceEditor,
IncrementPropertyValueSupport {
    protected PropertyEditor editor;
    protected PropertyEnv env;
    private boolean added;
    private String valFromEditor;
    private String valFromTextField;
    private PropertyModel pm;
    private KeyStroke[] strokes = new KeyStroke[]{KeyStroke.getKeyStroke(36, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask() | 64), KeyStroke.getKeyStroke(35, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask() | 64), KeyStroke.getKeyStroke(38, 0, false), KeyStroke.getKeyStroke(40, 0, false)};

    StringInplaceEditor() {
    }

    @Override
    public void clear() {
        this.editor = null;
        this.setEditable(true);
        this.setEnabled(true);
        this.setText("");
        this.pm = null;
        this.env = null;
        this.valFromEditor = null;
        this.valFromTextField = null;
    }

    @Override
    public void connect(PropertyEditor p, PropertyEnv env) {
        this.setActionCommand("success");
        this.env = env;
        if (PropUtils.supportsValueIncrement(env)) {
            PropUtils.wrapUpDownArrowActions(this, this);
        }
        if (this.editor == p) {
            return;
        }
        this.editor = p;
        boolean editable = PropUtils.checkEnabled(this, p, env);
        this.setEnabled(editable);
        if (p.getTags() == null && p.getAsText() == null && p.isPaintable()) {
            editable = false;
        }
        this.setEditable(editable);
        this.reset();
        this.added = false;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.added = true;
    }

    @Override
    public JComponent getComponent() {
        return this;
    }

    @Override
    public Object getValue() {
        if (this.valFromTextField != null && this.valFromTextField.equals(this.getText())) {
            return null == this.valFromEditor ? "" : this.valFromEditor;
        }
        return this.getText();
    }

    @Override
    public void reset() {
        String initialEditValue;
        String txt = this.editor.getAsText();
        if (this.editor instanceof PropUtils.DifferentValuesEditor) {
            txt = "";
        }
        this.valFromEditor = txt;
        if (this.getClass() == StringInplaceEditor.class && this.env != null && this.env.getFeatureDescriptor() != null && (initialEditValue = (String)this.env.getFeatureDescriptor().getValue("initialEditValue")) != null) {
            this.valFromEditor = txt = initialEditValue;
        }
        if (txt == null) {
            txt = "";
        }
        this.setText(txt);
        this.valFromTextField = this.getText();
        this.setSelectionStart(0);
        this.setSelectionEnd(txt.length());
    }

    @Override
    public KeyStroke[] getKeyStrokes() {
        return this.strokes;
    }

    @Override
    public PropertyEditor getPropertyEditor() {
        return this.editor;
    }

    private void handleInitialInputEvent(InputEvent e) {
        String txt = this.getText();
        if (txt.length() > 0) {
            this.setSelectionStart(0);
            this.setSelectionEnd(this.getText().length());
        }
    }

    @Override
    public void setValue(Object o) {
        if (null != o && null != this.editor && this.editor.supportsCustomEditor()) {
            this.editor.setValue(o);
            String val = this.editor.getAsText();
            if (null == val) {
                val = "";
            }
            this.setText(val);
        } else {
            this.setText(o != null ? o.toString() : "");
        }
    }

    @Override
    public boolean supportsTextEntry() {
        return true;
    }

    @Override
    public PropertyModel getPropertyModel() {
        return this.pm;
    }

    @Override
    public void setPropertyModel(PropertyModel pm) {
        this.pm = pm;
    }

    @Override
    public boolean isKnownComponent(Component c) {
        return false;
    }

    @Override
    public Dimension getPreferredSize() {
        Graphics g = PropUtils.getScratchGraphics(this);
        String s = this.getText();
        if (s.length() > 1000) {
            return new Dimension(4196, g.getFontMetrics(this.getFont()).getHeight());
        }
        FontMetrics fm = g.getFontMetrics(this.getFont());
        Dimension result = new Dimension(fm.stringWidth(s), fm.getHeight());
        result.width = Math.max(result.width, PropUtils.getMinimumPropPanelWidth());
        result.height = Math.max(result.height, PropUtils.getMinimumPropPanelHeight());
        if (this.getBorder() != null) {
            Insets i = this.getBorder().getBorderInsets(this);
            result.width += i.right + i.left;
            result.height += i.top + i.bottom;
        }
        return result;
    }

    @Override
    public void processMouseEvent(MouseEvent me) {
        super.processMouseEvent(me);
        if (this.added) {
            this.handleInitialInputEvent(me);
        }
        this.added = false;
    }

    @Override
    protected void processFocusEvent(FocusEvent fe) {
        super.processFocusEvent(fe);
        this.repaint();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void paintComponent(Graphics g) {
        if (this.editor != null && !this.hasFocus() && this.editor.isPaintable()) {
            Insets ins = this.getInsets();
            Color c = g.getColor();
            try {
                g.setColor(this.getBackground());
                g.fillRect(0, 0, this.getWidth(), this.getHeight());
            }
            finally {
                g.setColor(c);
            }
            ins.left += PropUtils.getTextMargin();
            this.editor.paintValue(g, new Rectangle(ins.left, ins.top, this.getWidth() - (ins.right + ins.left), this.getHeight() - (ins.top + ins.bottom)));
        } else {
            super.paintComponent(g);
        }
    }

    @Override
    public boolean incrementValue() {
        return this.setNextValue(true);
    }

    @Override
    public boolean decrementValue() {
        return this.setNextValue(false);
    }

    private boolean setNextValue(boolean increment) {
        if (!PropUtils.supportsValueIncrement(this.env)) {
            return false;
        }
        Object nextValue = PropUtils.getNextValue(this.env, increment);
        if (null == nextValue) {
            return true;
        }
        this.setValue(nextValue);
        return PropUtils.updateProp(this);
    }

    @Override
    public boolean isIncrementEnabled() {
        return true;
    }
}

