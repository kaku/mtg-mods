/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.propertysheet;

import org.openide.explorer.propertysheet.PropertyDisplayer;
import org.openide.explorer.propertysheet.ReusablePropertyEnv;

interface PropertyDisplayer_Inline
extends PropertyDisplayer {
    public void setShowCustomEditorButton(boolean var1);

    public boolean isShowCustomEditorButton();

    public void setTableUI(boolean var1);

    public boolean isTableUI();

    public void setRadioButtonMax(int var1);

    public int getRadioButtonMax();

    public void setUseLabels(boolean var1);

    public boolean isUseLabels();

    public boolean isTitleDisplayed();

    public boolean isRadioBoolean();

    public void setRadioBoolean(boolean var1);

    public ReusablePropertyEnv getReusablePropertyEnv();
}

