/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.NbBundle
 */
package org.openide.explorer.propertysheet;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.beans.FeatureDescriptor;
import java.beans.PropertyEditor;
import org.netbeans.modules.openide.explorer.NodeOperationImpl;
import org.openide.explorer.propertysheet.CustomEditorAction;
import org.openide.explorer.propertysheet.ProxyNode;
import org.openide.explorer.propertysheet.ReusablePropertyEnv;
import org.openide.explorer.propertysheet.ReusablePropertyModel;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

final class CustomEditorAccessorImpl
implements NodeOperationImpl.CustomEditorAccessor {
    static void register() {
        NodeOperationImpl.registerCustomEditorAccessor(new CustomEditorAccessorImpl());
    }

    private CustomEditorAccessorImpl() {
    }

    @Override
    public void showDialog(Node.Property property, Object[] beans) {
        new CustomEditorAction(new Invoker(property, beans)).actionPerformed(new ActionEvent((Object)property, 1001, "invokeCustomEditor"));
    }

    private static class Invoker
    implements CustomEditorAction.Invoker {
        private Node.Property property;
        private Object[] beans;
        private ReusablePropertyEnv propertyEnv;

        Invoker(Node.Property prop, Object[] beans) {
            this.property = prop;
            this.beans = beans;
            this.propertyEnv = new ReusablePropertyEnv();
            ReusablePropertyModel rpm = new ReusablePropertyModel(this.propertyEnv);
            rpm.setProperty(prop);
            this.propertyEnv.setNode(beans);
        }

        @Override
        public FeatureDescriptor getSelection() {
            return this.property;
        }

        @Override
        public Object getPartialValue() {
            return null;
        }

        @Override
        public Component getCursorChangeComponent() {
            return null;
        }

        @Override
        public String getBeanName() {
            if (this.beans instanceof Node[]) {
                Node[] nodes = (Node[])this.beans;
                StringBuilder name = new StringBuilder();
                String delim = NbBundle.getMessage(ProxyNode.class, (String)"CTL_List_Delimiter");
                for (int i = 0; i < nodes.length; ++i) {
                    name.append(nodes[i].getDisplayName());
                    if (i >= nodes.length - 1) continue;
                    name.append(delim);
                    if (i < 2) continue;
                    name.append(NbBundle.getMessage(ProxyNode.class, (String)"MSG_ELLIPSIS"));
                    break;
                }
                return name.toString();
            }
            return null;
        }

        @Override
        public void editorOpening() {
        }

        @Override
        public void editorOpened() {
        }

        @Override
        public void editorClosed() {
        }

        @Override
        public void valueChanged(PropertyEditor editor) {
        }

        @Override
        public boolean allowInvoke() {
            return true;
        }

        @Override
        public void failed() {
        }

        @Override
        public boolean wantAllChanges() {
            return false;
        }

        @Override
        public ReusablePropertyEnv getReusablePropertyEnv() {
            return this.propertyEnv;
        }
    }

}

