/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.WeakListeners
 */
package org.openide.explorer.propertysheet;

import java.awt.Toolkit;
import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.openide.explorer.propertysheet.CheckboxInplaceEditor;
import org.openide.explorer.propertysheet.ComboInplaceEditor;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.NodePropertyModel;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;
import org.openide.explorer.propertysheet.RadioInplaceEditor;
import org.openide.explorer.propertysheet.ReusablePropertyEnv;
import org.openide.explorer.propertysheet.StringInplaceEditor;
import org.openide.explorer.propertysheet.WrapperInplaceEditor;
import org.openide.explorer.propertysheet.editors.EnhancedPropertyEditor;
import org.openide.nodes.Node;
import org.openide.util.WeakListeners;

final class InplaceEditorFactory
implements PropertyChangeListener {
    private InplaceEditor checkbox = null;
    private InplaceEditor text = null;
    private InplaceEditor combo = null;
    private InplaceEditor radio = null;
    private ReusablePropertyEnv reusableEnv;
    private boolean tableUI;
    int radioButtonMax = -1;
    private boolean useLabels = false;
    private boolean useRadioBoolean = PropUtils.forceRadioButtons;
    private static final boolean isAqua = "Aqua".equals(UIManager.getLookAndFeel().getID());
    private static final boolean isMetal = "Metal".equals(UIManager.getLookAndFeel().getID());

    InplaceEditorFactory(boolean tableUI, ReusablePropertyEnv env) {
        this.tableUI = tableUI;
        this.reusableEnv = env;
        PropertyChangeListener weakListener = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)Toolkit.getDefaultToolkit());
        Toolkit.getDefaultToolkit().addPropertyChangeListener("win.xpstyle.themeActive", weakListener);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        this.checkbox = null;
        this.text = null;
        this.combo = null;
        this.radio = null;
    }

    void setRadioButtonMax(int i) {
        this.radioButtonMax = i;
    }

    void setUseLabels(boolean val) {
        this.useLabels = val;
    }

    void setUseRadioBoolean(boolean val) {
        this.useRadioBoolean = val;
    }

    private InplaceEditor getRadioEditor(boolean newInstance) {
        RadioInplaceEditor result;
        if (newInstance) {
            result = new RadioInplaceEditor(this.tableUI);
        } else {
            if (this.radio == null) {
                this.radio = new RadioInplaceEditor(this.tableUI);
                ((JComponent)((Object)this.radio)).setName("RadioEditor for " + this.getClass().getName() + "@" + System.identityHashCode(this));
            }
            result = (RadioInplaceEditor)this.radio;
        }
        result.setUseTitle(this.useLabels);
        return result;
    }

    private InplaceEditor getComboBoxEditor(boolean newInstance) {
        if (newInstance || isAqua || isMetal) {
            return new ComboInplaceEditor(this.tableUI);
        }
        if (this.combo == null) {
            this.combo = new ComboInplaceEditor(this.tableUI);
            ((JComponent)((Object)this.combo)).setName("ComboInplaceEditor for " + this.getClass().getName() + "@" + System.identityHashCode(this));
        }
        return this.combo;
    }

    private InplaceEditor getStringEditor(boolean newInstance) {
        if (newInstance) {
            return new StringInplaceEditor();
        }
        if (this.text == null) {
            this.text = new StringInplaceEditor();
            ((JComponent)((Object)this.text)).setName("StringEditor for " + this.getClass().getName() + "@" + System.identityHashCode(this));
        }
        return this.text;
    }

    private InplaceEditor getCheckboxEditor(boolean newInstance) {
        CheckboxInplaceEditor result;
        if (newInstance) {
            result = new CheckboxInplaceEditor();
        } else {
            if (this.checkbox == null) {
                this.checkbox = new CheckboxInplaceEditor();
                ((JComponent)((Object)this.checkbox)).setName("CheckboxEditor for " + this.getClass().getName() + "@" + System.identityHashCode(this));
            }
            result = (CheckboxInplaceEditor)this.checkbox;
        }
        result.setUseTitle(this.useLabels);
        return result;
    }

    public InplaceEditor getInplaceEditor(Node.Property p, boolean newInstance) {
        PropertyEnv env = new PropertyEnv();
        env.setBeans(this.reusableEnv.getBeans());
        return this.getInplaceEditor(p, env, newInstance);
    }

    InplaceEditor getInplaceEditor(Node.Property p, PropertyEnv env, boolean newInstance) {
        EnhancedPropertyEditor enh;
        PropertyEditor ped = PropUtils.getPropertyEditor(p);
        InplaceEditor result = (InplaceEditor)p.getValue("inplaceEditor");
        env.setFeatureDescriptor((FeatureDescriptor)p);
        env.setEditable(p.canWrite());
        if (ped instanceof ExPropertyEditor) {
            ExPropertyEditor epe = (ExPropertyEditor)ped;
            epe.attachEnv(env);
            if (result == null) {
                result = env.getInplaceEditor();
            }
        } else if (ped instanceof EnhancedPropertyEditor && (enh = (EnhancedPropertyEditor)ped).hasInPlaceCustomEditor()) {
            result = new WrapperInplaceEditor(enh);
        }
        if (result == null) {
            Class c = p.getValueType();
            if (c == Boolean.class || c == Boolean.TYPE) {
                if (ped instanceof PropUtils.NoPropertyEditorEditor) {
                    result = this.getStringEditor(newInstance);
                } else {
                    boolean useRadioButtons = this.useRadioBoolean || p.getValue("stringValues") != null;
                    result = useRadioButtons ? this.getRadioEditor(newInstance) : this.getCheckboxEditor(newInstance);
                }
            } else {
                String[] tags = ped.getTags();
                result = tags != null ? (tags.length <= this.radioButtonMax ? this.getRadioEditor(newInstance) : this.getComboBoxEditor(newInstance)) : this.getStringEditor(newInstance);
            }
        }
        if (!this.tableUI && Boolean.FALSE.equals(p.getValue("canEditAsText"))) {
            result.getComponent().setEnabled(false);
        }
        result.clear();
        result.setPropertyModel(new NodePropertyModel(p, env.getBeans()));
        result.connect(ped, env);
        if (this.tableUI) {
            if (result instanceof JTextField) {
                result.getComponent().setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 0));
            } else {
                result.getComponent().setBorder(BorderFactory.createEmptyBorder());
            }
        }
        return result;
    }
}

