/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.propertysheet;

import java.beans.FeatureDescriptor;
import org.openide.explorer.propertysheet.PropertyModel;

@Deprecated
public interface ExPropertyModel
extends PropertyModel {
    public Object[] getBeans();

    public FeatureDescriptor getFeatureDescriptor();
}

