/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ChangeSupport
 *  org.openide.util.NbBundle
 */
package org.openide.explorer.propertysheet;

import java.awt.AWTEvent;
import java.awt.AWTKeyStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FocusTraversalPolicy;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.PrintStream;
import java.util.Collections;
import java.util.EventObject;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.plaf.TableUI;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.text.JTextComponent;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.SheetTable;
import org.openide.util.ChangeSupport;
import org.openide.util.NbBundle;

abstract class BaseTable
extends JTable
implements FocusListener {
    protected static final String SYSPROP_PS_QUICK_SEARCH_DISABLED_GLOBAL = "ps.quickSearch.disabled.global";
    protected static final String ACTION_NEXT = "next";
    protected static final String ACTION_PREV = "prev";
    protected static final String ACTION_INLINE_EDITOR = "invokeInlineEditor";
    protected static final String ACTION_CANCEL_EDIT = "cancelEditing";
    protected static final String ACTION_ENTER = "enterPressed";
    protected static final String ACTION_FOCUS_NEXT = "focusNext";
    private static final int centerLineFudgeFactor = 3;
    protected static Action editAction = null;
    protected static Action cancelAction = null;
    protected static Action enterAction = null;
    protected LineDragListener dragListener;
    private final ChangeSupport cs;
    boolean needCalcRowHeight;
    private boolean inSetUI;
    private boolean allowQuickSearch;
    private int editRequests;
    private int editorRemoveRequests;
    private int editorChangeRequests;
    private boolean searchArmed;
    private transient SearchField searchField;
    private transient JPanel searchpanel;
    private transient ChangeListener viewportListener;
    private transient Point prevViewPosition;
    WL parentListener;

    public BaseTable(TableModel dm, TableColumnModel cm, ListSelectionModel sm) {
        super(dm, cm, sm);
        this.cs = new ChangeSupport((Object)this);
        this.needCalcRowHeight = true;
        this.inSetUI = false;
        this.allowQuickSearch = true;
        this.editRequests = 0;
        this.editorRemoveRequests = 0;
        this.editorChangeRequests = 0;
        this.searchArmed = false;
        this.searchField = null;
        this.searchpanel = null;
        this.prevViewPosition = null;
        this.getSelectionModel().setSelectionMode(0);
        this.setSurrendersFocusOnKeystroke(true);
        this.setCellSelectionEnabled(false);
        this.setRowSelectionAllowed(true);
        this.setAutoResizeMode(1);
        this.putClientProperty("JTable.autoStartsEdit", Boolean.FALSE);
        this.putClientProperty("terminateEditOnFocusLost", PropUtils.psCommitOnFocusLoss ? Boolean.FALSE : Boolean.TRUE);
        this.dragListener = new LineDragListener();
        this.addMouseListener(this.dragListener);
        this.addMouseMotionListener(this.dragListener);
        this.setFocusCycleRoot(true);
        this.enableEvents(4);
        if (this.getClass() != SheetTable.class) {
            throw new NoClassDefFoundError("Only SheetTable may subclass BaseTable, for good reasons");
        }
    }

    protected void initKeysAndActions() {
        this.setFocusTraversalKeys(0, Collections.emptySet());
        this.setFocusTraversalKeys(1, Collections.emptySet());
        this.unregisterKeyboardAction(KeyStroke.getKeyStroke(10, 0));
        this.unregisterKeyboardAction(KeyStroke.getKeyStroke(10, 1));
        InputMap imp = this.getInputMap();
        ActionMap am = this.getActionMap();
        if (!GraphicsEnvironment.isHeadless()) {
            imp.put(KeyStroke.getKeyStroke(9, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask() | 1, false), "focusNext");
            imp.put(KeyStroke.getKeyStroke(9, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false), "focusNext");
        }
        CTRLTabAction ctrlTab = new CTRLTabAction();
        am.put("focusNext", ctrlTab);
        imp.put(KeyStroke.getKeyStroke(9, 0), "next");
        imp.put(KeyStroke.getKeyStroke(9, 64), "prev");
        am.put("next", new NavigationAction(true));
        am.put("prev", new NavigationAction(false));
        imp.put(KeyStroke.getKeyStroke(32, 0), "invokeInlineEditor");
        imp.put(KeyStroke.getKeyStroke(113, 0), "invokeInlineEditor");
        am.put("invokeInlineEditor", BaseTable.getEditAction());
        imp.put(KeyStroke.getKeyStroke(10, 0), "enterPressed");
        am.put("enterPressed", BaseTable.getEnterAction());
        InputMap impAncestor = this.getInputMap(1);
        impAncestor.put(KeyStroke.getKeyStroke(27, 0), "cancelEditing");
        am.put("cancelEditing", new CancelAction());
        impAncestor.put(KeyStroke.getKeyStroke(27, 0), "cancelEditing");
    }

    protected static final void cleanup() {
        editAction = null;
        cancelAction = null;
        enterAction = null;
    }

    @Override
    public void setFont(Font f) {
        this.needCalcRowHeight = true;
        super.setFont(f);
    }

    private static Action getEditAction() {
        if (editAction == null) {
            editAction = new EditAction();
        }
        return editAction;
    }

    private static Action getEnterAction() {
        if (enterAction == null) {
            enterAction = new EnterAction();
        }
        return enterAction;
    }

    private void calcRowHeight(Graphics g) {
        int rowHeight;
        Integer i = (Integer)UIManager.get("netbeans.ps.rowheight");
        if (i != null) {
            rowHeight = i;
        } else {
            Font f = this.getFont();
            FontMetrics fm = g.getFontMetrics(f);
            rowHeight = Math.max(fm.getHeight() + 3, PropUtils.getSpinnerHeight());
        }
        this.needCalcRowHeight = false;
        this.setRowHeight(rowHeight);
    }

    protected int getFirstVisibleRow() {
        if (this.getParent() instanceof JViewport) {
            JViewport jvp = (JViewport)this.getParent();
            return this.rowAtPoint(jvp.getViewPosition());
        }
        Insets ins = this.getInsets();
        return this.rowAtPoint(new Point(ins.left, ins.top));
    }

    protected int getVisibleRowCount() {
        int rowCount = this.getRowCount();
        int rowHeight = this.getRowHeight();
        if (rowCount == 0 || rowHeight == 0) {
            return 0;
        }
        if (this.getParent() instanceof JViewport) {
            JViewport jvp = (JViewport)this.getParent();
            int result = Math.min(rowCount, jvp.getExtentSize().height / rowHeight + 1);
            return result;
        }
        return Math.min(rowCount, this.getHeight() / rowHeight);
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        return col != 0;
    }

    @Override
    public final void requestFocus() {
        if (this.isEditing()) {
            if (PropUtils.isLoggable(BaseTable.class)) {
                PropUtils.log(BaseTable.class, "RequestFocus on table delegating to editor component");
            }
            this.editorComp.requestFocus();
        } else if (!this.inEditorChangeRequest()) {
            if (PropUtils.isLoggable(BaseTable.class)) {
                PropUtils.log(BaseTable.class, "RequestFocus on table with no editor present");
            }
            super.requestFocus();
        }
    }

    @Override
    public final boolean requestFocusInWindow() {
        if (this.isEditing()) {
            if (PropUtils.isLoggable(BaseTable.class)) {
                PropUtils.log(BaseTable.class, "RequestFocusInWindow on table delegating to editor");
            }
            return this.editorComp.requestFocusInWindow();
        }
        if (!this.inEditorChangeRequest()) {
            if (PropUtils.isLoggable(BaseTable.class)) {
                PropUtils.log(BaseTable.class, "RequestFocusInWindow on table with no editor present");
            }
            boolean result = super.requestFocusInWindow();
            if (PropUtils.isLoggable(BaseTable.class)) {
                PropUtils.log(BaseTable.class, "  RequestFocusInWindow result " + result);
            }
            return result;
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean editCellAt(int row, int col, EventObject e) {
        boolean result;
        boolean editorChange;
        this.enterEditRequest();
        if (e instanceof MouseEvent) {
            Component focusOwner;
            if (PropUtils.isLoggable(BaseTable.class)) {
                PropUtils.log(BaseTable.class, "editCellAt " + row + "," + col + " triggered by mouse event");
            }
            if ((focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getPermanentFocusOwner()) != this && !this.requestFocusInWindow()) {
                this.requestFocus();
            }
        } else if (PropUtils.isLoggable(BaseTable.class)) {
            PropUtils.log(BaseTable.class, "editCellAt " + row + "," + col + " triggered by (null = kbd evt)" + e);
        }
        boolean wasEditing = this.isEditing();
        if (wasEditing) {
            if (PropUtils.isLoggable(BaseTable.class)) {
                PropUtils.log(BaseTable.class, "  was already editing, removing the editor");
            }
            this.removeEditor();
        }
        int prevSel = this.getSelectedRow();
        this.changeSelection(row, col, false, false);
        result = false;
        boolean bl = editorChange = wasEditing && this.isCellEditable(row, col);
        if (editorChange) {
            this.enterEditorChangeRequest();
        }
        try {
            result = super.editCellAt(row, col, e);
            if (PropUtils.isLoggable(BaseTable.class)) {
                PropUtils.log(BaseTable.class, "  Result of super.editCellAt is " + result);
            }
            if (this.editorComp != null) {
                Component c = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
                this.editorComp.addFocusListener(this);
            }
        }
        finally {
            try {
                this.exitEditRequest();
            }
            finally {
                if (editorChange) {
                    this.exitEditorChangeRequest();
                }
            }
        }
        return result;
    }

    protected final void enterEditRequest() {
        ++this.editRequests;
        if (PropUtils.isLoggable(BaseTable.class)) {
            PropUtils.log(BaseTable.class, " entering edit request");
        }
    }

    protected final void enterEditorRemoveRequest() {
        ++this.editorRemoveRequests;
        if (PropUtils.isLoggable(BaseTable.class)) {
            PropUtils.log(BaseTable.class, " entering editor remove request");
        }
    }

    protected final void enterEditorChangeRequest() {
        ++this.editorChangeRequests;
        if (PropUtils.isLoggable(BaseTable.class)) {
            PropUtils.log(BaseTable.class, " entering editor change request");
        }
    }

    protected final void exitEditRequest() {
        --this.editRequests;
        if (PropUtils.isLoggable(BaseTable.class)) {
            PropUtils.log(BaseTable.class, " exiting edit change request");
        }
        assert (this.editRequests >= 0);
    }

    protected final void exitEditorRemoveRequest() {
        --this.editorRemoveRequests;
        PropUtils.log(BaseTable.class, " exiting editor remove request");
        assert (this.editorRemoveRequests >= 0);
    }

    protected final void exitEditorChangeRequest() {
        --this.editorChangeRequests;
        if (PropUtils.isLoggable(BaseTable.class)) {
            PropUtils.log(BaseTable.class, " exiting editor change request");
        }
        assert (this.editorRemoveRequests >= 0);
    }

    protected final boolean inEditRequest() {
        return this.editRequests > 0;
    }

    protected final boolean inEditorChangeRequest() {
        return this.editorChangeRequests > 0;
    }

    protected final boolean inEditorRemoveRequest() {
        return this.editorRemoveRequests > 0;
    }

    @Override
    public Component prepareEditor(TableCellEditor editor, int row, int col) {
        Component result = editor.getTableCellEditorComponent(this, this.getValueAt(row, col), false, row, col);
        if (result != null) {
            result.setBackground(this.getSelectionBackground());
            result.setForeground(this.getSelectionForeground());
            result.setFont(this.getFont());
        }
        return result;
    }

    @Override
    public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
        Color bkColor;
        Object value = this.getValueAt(row, col);
        Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getPermanentFocusOwner();
        boolean isSelected = this.isSelected(row, focusOwner);
        Component result = renderer.getTableCellRendererComponent(this, value, isSelected, false, row, col);
        if (PropUtils.isNimbus && !isSelected && null != (bkColor = this.getBackground())) {
            result.setBackground(new Color(bkColor.getRGB()));
        }
        return result;
    }

    protected boolean isSelected(int row, Component focusOwner) {
        return (this.getSelectedRow() == row || this.editingRow == row && !this.inEditorRemoveRequest()) && (this.hasFocus() || this.isKnownComponent(focusOwner) || this.inEditRequest());
    }

    @Override
    public void setUI(TableUI ui) {
        this.needCalcRowHeight = true;
        this.inSetUI = true;
        super.setUI(ui);
        this.inSetUI = false;
    }

    @Override
    public void addFocusListener(FocusListener fl) {
        if (!this.inSetUI) {
            super.addFocusListener(fl);
        }
    }

    @Override
    public void updateUI() {
        super.updateUI();
        this.initKeysAndActions();
    }

    @Override
    public void paint(Graphics g) {
        if (this.needCalcRowHeight) {
            this.calcRowHeight(g);
            return;
        }
        super.paint(g);
    }

    protected void paintRow(int row) {
        if (row == -1) {
            return;
        }
        Rectangle dirtyRect = this.getCellRect(row, 0, false);
        dirtyRect.x = 0;
        dirtyRect.width = this.getWidth();
        this.repaint(dirtyRect);
    }

    protected void paintSelectionRow() {
        this.paintRow(this.getSelectedRow());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeEditor() {
        this.enterEditorRemoveRequest();
        try {
            int i = this.editingRow;
            if (this.editorComp != null) {
                this.editorComp.removeFocusListener(this);
            }
            if (PropUtils.isLoggable(BaseTable.class)) {
                PropUtils.log(BaseTable.class, " removing editor");
            }
            super.removeEditor();
            if (i != -1) {
                this.paintRow(i);
            }
        }
        finally {
            this.exitEditorRemoveRequest();
        }
    }

    @Override
    protected final void configureEnclosingScrollPane() {
        Container gp;
        Container p = this.getParent();
        if (p instanceof JViewport && (gp = p.getParent()) instanceof JScrollPane) {
            JScrollPane scrollPane = (JScrollPane)gp;
            JViewport viewport = scrollPane.getViewport();
            if (viewport == null || viewport.getView() != this) {
                return;
            }
            scrollPane.setColumnHeaderView(this.getTableHeader());
        }
    }

    protected final boolean onCenterLine(int pos) {
        int line = this.getColumnModel().getColumn(0).getWidth();
        return pos > line - 3 && pos < line + 3;
    }

    protected final boolean onCenterLine(MouseEvent me) {
        int pos = me.getPoint().x;
        return this.onCenterLine(pos);
    }

    @Override
    public void changeSelection(int row, int column, boolean toggle, boolean extend) {
        if (this.dragListener != null && this.dragListener.isArmed()) {
            return;
        }
        if (PropUtils.isLoggable(BaseTable.class)) {
            PropUtils.log(BaseTable.class, "ChangeSelection to " + row + "," + column);
        }
        super.changeSelection(row, column, toggle, extend);
        this.fireChange();
    }

    protected void focusLostCancel() {
        this.removeEditor();
    }

    @Override
    public void processFocusEvent(FocusEvent fe) {
        super.processFocusEvent(fe);
        if (PropUtils.isLoggable(BaseTable.class)) {
            PropUtils.log(BaseTable.class, "processFocusEvent - ");
            PropUtils.log(BaseTable.class, fe);
        }
        if ((!this.isAncestorOf(fe.getOppositeComponent()) || fe.getOppositeComponent() == null) && this.isEditing() && fe.getID() == 1005) {
            if (PropUtils.isLoggable(BaseTable.class)) {
                PropUtils.log(BaseTable.class, "ProcessFocusEvent got focus lost to unknown component, removing editor");
            }
            this.focusLostCancel();
        }
        if (!this.inEditorRemoveRequest() && !this.inEditRequest()) {
            if (fe.getOppositeComponent() == null && fe.getID() == 1005) {
                return;
            }
            this.paintSelectionRow();
        } else {
            this.paintSelectionRow();
        }
    }

    protected boolean isQuickSearchAllowed() {
        String sysPropGlobalDisable = System.getProperty("ps.quickSearch.disabled.global", "false");
        if (Boolean.parseBoolean(sysPropGlobalDisable)) {
            return false;
        }
        return this.allowQuickSearch;
    }

    protected void setQuickSearchAllowed(boolean isQuickSearchAllowed) {
        this.allowQuickSearch = isQuickSearchAllowed;
    }

    @Override
    public void processKeyEvent(KeyEvent e) {
        boolean suppressDefaultHandling;
        if (this.dragListener.isArmed()) {
            this.dragListener.setArmed(false);
        }
        boolean bl = suppressDefaultHandling = this.searchField != null && this.searchField.isShowing() && (e.getKeyCode() == 38 || e.getKeyCode() == 40);
        if (e.getKeyCode() != 9) {
            if (!suppressDefaultHandling) {
                super.processKeyEvent(e);
            }
            if (!e.isConsumed()) {
                if (e.getID() == 401 && !this.isEditing()) {
                    int modifiers = e.getModifiers();
                    int keyCode = e.getKeyCode();
                    if (modifiers > 0 && modifiers != 1 || e.isActionKey()) {
                        return;
                    }
                    char c = e.getKeyChar();
                    if (!Character.isISOControl(c) && keyCode != 16 && keyCode != 27) {
                        this.searchArmed = true;
                        e.consume();
                    }
                } else if (this.searchArmed && e.getID() == 400) {
                    this.passToSearchField(e);
                    e.consume();
                    this.searchArmed = false;
                } else {
                    this.searchArmed = false;
                }
            }
        } else {
            this.processKeyBinding(KeyStroke.getKeyStroke(9, e.getModifiersEx(), e.getID() == 402), e, 0, e.getID() == 401);
        }
    }

    void passToSearchField(KeyEvent e) {
        if (!this.isQuickSearchAllowed()) {
            return;
        }
        if (!(e.getKeyCode() != 9 && e.getKeyCode() != 10 && (e.getKeyCode() != 38 && e.getKeyCode() != 40 || this.searchField != null && this.searchField.isShowing()))) {
            return;
        }
        if (this.getRowCount() == 0) {
            return;
        }
        if (this.searchField == null || !this.searchField.isShowing()) {
            this.showSearchField();
            this.searchField.setText(String.valueOf(e.getKeyChar()));
        }
    }

    private void showSearchField() {
        Point loc;
        if (this.searchField == null) {
            this.searchField = new SearchField();
            this.searchpanel = new JPanel();
            JLabel lbl = new JLabel(NbBundle.getMessage(BaseTable.class, (String)"LBL_QUICKSEARCH"));
            this.searchpanel.setLayout(new BoxLayout(this.searchpanel, 0));
            this.searchpanel.add(lbl);
            this.searchpanel.add(this.searchField);
            lbl.setLabelFor(this.searchField);
            this.searchpanel.setBorder(BorderFactory.createRaisedBevelBorder());
            lbl.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
        }
        JLayeredPane dest = this.getRootPane().getLayeredPane();
        if (this.getParent() instanceof JViewport) {
            JViewport jvp = (JViewport)this.getParent();
            loc = jvp.getViewPosition();
            loc.x += this.getColumnModel().getColumn(0).getWidth();
            this.viewportListener = new ChangeListener(){

                @Override
                public void stateChanged(ChangeEvent e) {
                    if (null != BaseTable.this.searchField && BaseTable.this.searchField.isVisible()) {
                        if (null != BaseTable.this.prevViewPosition) {
                            BaseTable.this.repaint(0, BaseTable.access$500((BaseTable)BaseTable.this).y, BaseTable.this.getWidth(), BaseTable.this.searchpanel.getHeight());
                        }
                        assert (BaseTable.this.getParent() instanceof JViewport);
                        BaseTable.this.prevViewPosition = new Point(((JViewport)BaseTable.this.getParent()).getViewPosition());
                    }
                }
            };
            jvp.addChangeListener(this.viewportListener);
            this.prevViewPosition = new Point(loc);
        } else {
            loc = new Point(this.getColumnModel().getColumn(0).getWidth(), this.getRowHeight() / 2);
        }
        loc = SwingUtilities.convertPoint(this, loc, dest);
        int width = this.getColumnModel().getColumn(1).getWidth();
        int height = this.getRowHeight() + 5;
        if (width < 120) {
            width = 160;
            loc.x -= 160;
        }
        this.searchpanel.setBounds(loc.x, loc.y, width, height);
        dest.add(this.searchpanel);
        this.getParent().addComponentListener(this.searchField);
        this.searchpanel.setVisible(true);
        this.searchField.requestFocus();
    }

    private void hideSearchField() {
        if (this.searchField == null) {
            return;
        }
        this.searchpanel.setVisible(false);
        if (this.getParent() instanceof JViewport && null != this.viewportListener) {
            JViewport jvp = (JViewport)this.getParent();
            jvp.removeChangeListener(this.viewportListener);
            this.viewportListener = null;
        }
        this.getParent().removeComponentListener(this.searchField);
        if (this.searchpanel.getParent() != null) {
            this.searchpanel.getParent().remove(this.searchpanel);
        }
        this.paintSelectionRow();
    }

    protected boolean matchText(Object value, String text) {
        if (value != null) {
            return value.toString().startsWith(text);
        }
        return false;
    }

    @Override
    public boolean isOptimizedDrawingEnabled() {
        if (this.searchField != null && this.searchField.isShowing()) {
            return false;
        }
        return super.isOptimizedDrawingEnabled();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (this.searchField != null && this.searchField.isVisible()) {
            this.searchpanel.repaint();
        }
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        super.tableChanged(e);
        this.fireChange();
    }

    public final void addChangeListener(ChangeListener listener) {
        this.cs.addChangeListener(listener);
    }

    public final void removeChangeListener(ChangeListener listener) {
        this.cs.removeChangeListener(listener);
    }

    void fireChange() {
        if (this.cs != null) {
            this.cs.fireChange();
        }
    }

    protected boolean isKnownComponent(Component c) {
        if (c == null) {
            return false;
        }
        if (c == this) {
            return true;
        }
        if (c == this.editorComp) {
            return true;
        }
        if (c == this.searchField) {
            return true;
        }
        if (c == this.getRootPane()) {
            return true;
        }
        if (c instanceof Container && ((Container)c).isAncestorOf(this)) {
            return true;
        }
        if (this.editorComp instanceof Container && ((Container)this.editorComp).isAncestorOf(c)) {
            return true;
        }
        return false;
    }

    @Override
    public void focusGained(FocusEvent fe) {
        Component c = fe.getOppositeComponent();
        PropUtils.log(BaseTable.class, fe);
        if (!this.isKnownComponent(c)) {
            this.fireChange();
        }
        if (!this.inEditRequest() && !this.inEditorRemoveRequest() && fe.getComponent() == this) {
            this.paintSelectionRow();
        }
    }

    @Override
    public void focusLost(FocusEvent fe) {
        if (this.dragListener != null && this.dragListener.isDragging()) {
            this.dragListener.abortDrag();
        }
        PropUtils.log(BaseTable.class, fe);
        if (fe.isTemporary()) {
            return;
        }
        Component opposite = fe.getOppositeComponent();
        if (!this.isKnownComponent(opposite)) {
            this.doFocusLost(opposite);
        }
    }

    private void doFocusLost(Component opposite) {
        PropUtils.log(BaseTable.class, " removing editor due to focus change");
        if (PropUtils.psCommitOnFocusLoss && this.isEditing()) {
            this.getCellEditor().stopCellEditing();
        } else {
            this.removeEditor();
        }
        if (opposite != null) {
            this.fireChange();
        }
        this.paintSelectionRow();
    }

    @Override
    public void addNotify() {
        super.addNotify();
        Container top = this.getTopLevelAncestor();
        if (top instanceof Window) {
            this.parentListener = new WL();
            ((Window)top).addWindowListener(this.parentListener);
        }
    }

    @Override
    public void removeNotify() {
        Container top = this.getTopLevelAncestor();
        if (top instanceof Window && this.parentListener != null) {
            ((Window)top).removeWindowListener(this.parentListener);
            this.parentListener = null;
        }
        super.removeNotify();
    }

    private class CTRLTabAction
    extends AbstractAction {
        private CTRLTabAction() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            BaseTable.this.setFocusCycleRoot(false);
            try {
                Container con = BaseTable.this.getFocusCycleRootAncestor();
                if (con != null) {
                    Component to;
                    Container target = BaseTable.this;
                    if (BaseTable.this.getParent() instanceof JViewport && (target = BaseTable.this.getParent().getParent()) == con) {
                        target = BaseTable.this;
                    }
                    AWTEvent eo = EventQueue.getCurrentEvent();
                    boolean backward = false;
                    if (eo instanceof KeyEvent) {
                        backward = (((KeyEvent)eo).getModifiers() & 1) != 0 && (((KeyEvent)eo).getModifiersEx() & 64) != 0;
                    }
                    Component component = to = backward ? con.getFocusTraversalPolicy().getComponentAfter(con, BaseTable.this) : con.getFocusTraversalPolicy().getComponentAfter(con, BaseTable.this);
                    if (to == BaseTable.this) {
                        to = backward ? con.getFocusTraversalPolicy().getFirstComponent(con) : con.getFocusTraversalPolicy().getLastComponent(con);
                    }
                    to.requestFocus();
                }
            }
            finally {
                BaseTable.this.setFocusCycleRoot(true);
            }
        }
    }

    final class LineDragListener
    extends MouseAdapter
    implements MouseMotionListener {
        private long dragStartTime;
        boolean armed;
        boolean dragging;
        int pos;

        LineDragListener() {
            this.dragStartTime = -1;
            this.pos = -1;
        }

        @Override
        public void mouseExited(MouseEvent e) {
            this.setArmed(false);
        }

        @Override
        public void mousePressed(MouseEvent e) {
            if (this.isArmed() && BaseTable.this.onCenterLine(e)) {
                this.beginDrag();
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if (this.isDragging()) {
                this.finishDrag();
                this.setArmed(false);
            }
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            this.setArmed(!BaseTable.this.isEditing() && BaseTable.this.onCenterLine(e));
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            if (!this.armed && !this.dragging) {
                return;
            }
            int newPos = e.getPoint().x;
            TableColumn c0 = BaseTable.this.getColumnModel().getColumn(0);
            TableColumn c1 = BaseTable.this.getColumnModel().getColumn(1);
            int min = Math.max(c0.getMinWidth(), BaseTable.this.getWidth() - c1.getMaxWidth());
            int max = Math.min(c0.getMaxWidth(), BaseTable.this.getWidth() - c1.getMinWidth());
            if (newPos >= min && newPos <= max) {
                this.pos = newPos;
                this.update();
            }
        }

        public boolean isArmed() {
            return this.armed;
        }

        public boolean isDragging() {
            return this.dragging;
        }

        public void setArmed(boolean val) {
            if (val != this.armed) {
                this.armed = val;
                if (this.armed) {
                    BaseTable.this.setCursor(Cursor.getPredefinedCursor(11));
                } else {
                    BaseTable.this.setCursor(Cursor.getPredefinedCursor(0));
                }
            }
        }

        private void beginDrag() {
            this.dragging = true;
            this.dragStartTime = System.currentTimeMillis();
        }

        public void abortDrag() {
            this.dragging = false;
            this.setArmed(false);
            BaseTable.this.repaint();
        }

        private void finishDrag() {
            this.dragging = false;
            if (System.currentTimeMillis() - this.dragStartTime < 400) {
                this.update();
            } else {
                this.abortDrag();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void update() {
            if (this.pos < 0 || this.pos > BaseTable.this.getWidth()) {
                BaseTable.this.repaint();
                return;
            }
            int pos0 = this.pos;
            int pos1 = BaseTable.this.getWidth() - this.pos;
            Object object = BaseTable.this.getTreeLock();
            synchronized (object) {
                BaseTable.this.getColumnModel().getColumn(0).setWidth(pos0);
                BaseTable.this.getColumnModel().getColumn(1).setWidth(pos1);
                BaseTable.this.getColumnModel().getColumn(0).setPreferredWidth(pos0);
                BaseTable.this.getColumnModel().getColumn(1).setPreferredWidth(pos1);
            }
            BaseTable.this.repaint();
        }
    }

    private final class NavigationAction
    extends AbstractAction {
        private boolean direction;

        public NavigationAction(boolean direction) {
            this.direction = direction;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int next = BaseTable.this.getSelectedRow() + (this.direction ? 1 : -1);
            if (next >= BaseTable.this.getRowCount() || next < 0) {
                if (!(BaseTable.this.getTopLevelAncestor() instanceof Dialog)) {
                    next = next >= BaseTable.this.getRowCount() ? 0 : BaseTable.this.getRowCount() - 1;
                } else if (next >= BaseTable.this.getRowCount() || next < 0) {
                    Container grandcestor;
                    JRootPane rp;
                    JButton jb;
                    Component sibling;
                    Container ancestor = BaseTable.this.getFocusCycleRootAncestor();
                    Component component = sibling = this.direction ? ancestor.getFocusTraversalPolicy().getComponentAfter(ancestor, BaseTable.this.getParent()) : ancestor.getFocusTraversalPolicy().getComponentBefore(ancestor, BaseTable.this);
                    if (sibling == BaseTable.this && (grandcestor = ancestor.getFocusCycleRootAncestor()) != null) {
                        sibling = this.direction ? grandcestor.getFocusTraversalPolicy().getComponentAfter(grandcestor, ancestor) : grandcestor.getFocusTraversalPolicy().getComponentBefore(grandcestor, ancestor);
                        ancestor = grandcestor;
                    }
                    if (sibling == BaseTable.this && ancestor.getFocusTraversalPolicy().getFirstComponent(ancestor) != null) {
                        sibling = ancestor.getFocusTraversalPolicy().getFirstComponent(ancestor);
                    }
                    if (sibling == BaseTable.this && (jb = (rp = BaseTable.this.getRootPane()).getDefaultButton()) != null) {
                        sibling = jb;
                    }
                    if (sibling != null) {
                        if (sibling == BaseTable.this) {
                            BaseTable.this.changeSelection(this.direction ? 0 : BaseTable.this.getRowCount() - 1, this.direction ? 0 : BaseTable.this.getColumnCount() - 1, false, false);
                        } else {
                            sibling.requestFocus();
                        }
                        return;
                    }
                }
                BaseTable.this.changeSelection(next, BaseTable.this.getSelectedColumn(), false, false);
            }
            if (BaseTable.this.getSelectionModel().getAnchorSelectionIndex() < 0) {
                BaseTable.this.getSelectionModel().setAnchorSelectionIndex(next);
            }
            BaseTable.this.getSelectionModel().setLeadSelectionIndex(next);
        }
    }

    private static class EnterAction
    extends AbstractAction {
        private EnterAction() {
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            if (ae.getSource() instanceof BaseTable) {
                BaseTable bt = (BaseTable)ae.getSource();
                if (bt.isEditing()) {
                    return;
                }
                this.trySendEnterToDialog(bt);
            }
        }

        private void trySendEnterToDialog(BaseTable bt) {
            AWTEvent ev = EventQueue.getCurrentEvent();
            if (ev instanceof KeyEvent && ((KeyEvent)ev).getKeyCode() == 10) {
                JButton b;
                if (ev.getSource() instanceof JComboBox && ((JComboBox)ev.getSource()).isPopupVisible()) {
                    return;
                }
                if (ev.getSource() instanceof JTextComponent && ((JTextComponent)ev.getSource()).getParent() instanceof JComboBox && ((JComboBox)((JTextComponent)ev.getSource()).getParent()).isPopupVisible()) {
                    return;
                }
                JRootPane jrp = bt.getRootPane();
                if (jrp != null && (b = jrp.getDefaultButton()) != null && b.isEnabled()) {
                    b.doClick();
                }
            }
        }
    }

    private class CancelAction
    extends AbstractAction {
        private CancelAction() {
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            JTable jt = (JTable)ae.getSource();
            if (jt != null) {
                if (jt.isEditing()) {
                    TableCellEditor tce = jt.getCellEditor();
                    if (PropUtils.isLoggable(BaseTable.class)) {
                        PropUtils.log(BaseTable.class, "Cancelling edit due to keyboard event");
                    }
                    if (tce != null) {
                        jt.getCellEditor().cancelCellEditing();
                    }
                } else {
                    this.trySendEscToDialog(jt);
                }
            }
        }

        @Override
        public boolean isEnabled() {
            return BaseTable.this.isEditing();
        }

        private void trySendEscToDialog(JTable jt) {
            AWTEvent ev = EventQueue.getCurrentEvent();
            if (ev instanceof KeyEvent && ((KeyEvent)ev).getKeyCode() == 27) {
                Action a;
                if (ev.getSource() instanceof JComboBox && ((JComboBox)ev.getSource()).isPopupVisible()) {
                    return;
                }
                if (ev.getSource() instanceof JTextComponent && ((JTextComponent)ev.getSource()).getParent() instanceof JComboBox && ((JComboBox)((JTextComponent)ev.getSource()).getParent()).isPopupVisible()) {
                    return;
                }
                InputMap imp = jt.getRootPane().getInputMap(1);
                ActionMap am = jt.getRootPane().getActionMap();
                KeyStroke escape = KeyStroke.getKeyStroke(27, 0, false);
                Object key = imp.get(escape);
                if (key != null && (a = am.get(key)) != null) {
                    String commandKey;
                    if (Boolean.getBoolean("netbeans.proppanel.logDialogActions")) {
                        System.err.println("Action bound to escape key is " + a);
                    }
                    if ((commandKey = (String)a.getValue("ActionCommandKey")) == null) {
                        commandKey = "cancel";
                    }
                    a.actionPerformed(new ActionEvent(this, 1001, commandKey));
                }
            }
        }
    }

    private static class EditAction
    extends AbstractAction {
        private EditAction() {
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            JTable jt = (JTable)ae.getSource();
            int row = jt.getSelectedRow();
            int col = jt.getSelectedColumn();
            if (row != -1 && col != -1) {
                if (PropUtils.isLoggable(BaseTable.class)) {
                    PropUtils.log(BaseTable.class, "Starting edit due to key event for row " + row);
                }
                jt.editCellAt(row, 1, null);
                jt.requestFocus();
            }
        }
    }

    private class SearchField
    extends JTextField
    implements ActionListener,
    FocusListener,
    ComponentListener {
        private int selectionBeforeLastShow;

        public SearchField() {
            this.selectionBeforeLastShow = -1;
            this.addActionListener(this);
            this.addFocusListener(this);
            this.setFont(BaseTable.this.getFont());
        }

        @Override
        public void addNotify() {
            super.addNotify();
            this.selectionBeforeLastShow = BaseTable.this.getSelectedRow();
        }

        @Override
        public void processKeyEvent(KeyEvent ke) {
            if (!this.isShowing()) {
                super.processKeyEvent(ke);
                return;
            }
            if (ke.getKeyCode() == 27) {
                BaseTable.this.changeSelection(this.selectionBeforeLastShow, 0, false, false);
                BaseTable.this.requestFocus();
                ke.consume();
            } else if (ke.getKeyCode() == 38 && ke.getID() == 401) {
                this.reverseSearch(this.getText());
            } else if (ke.getKeyCode() == 40 && ke.getID() == 401) {
                this.forwardSearch(this.getText());
            } else {
                super.processKeyEvent(ke);
                if (ke.getKeyCode() != 38 && ke.getKeyCode() != 40) {
                    this.processSearchText(this.getText());
                }
            }
        }

        public void keyPressed(KeyEvent ke) {
            if (ke.getKeyCode() == 27) {
                BaseTable.this.hideSearchField();
                ke.consume();
            }
        }

        public void keyReleased(KeyEvent ke) {
            this.processSearchText(((JTextField)ke.getSource()).getText());
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.processSearchText(((JTextField)e.getSource()).getText());
            BaseTable.this.requestFocus();
        }

        @Override
        public void focusGained(FocusEvent e) {
            this.processSearchText(((JTextField)e.getSource()).getText());
            JRootPane root = this.getRootPane();
            if (root != null) {
                root.getLayeredPane().repaint();
            }
            this.setCaretPosition(this.getText().length());
        }

        @Override
        public void focusLost(FocusEvent e) {
            BaseTable.this.hideSearchField();
        }

        private void processSearchText(String txt) {
            if (txt == null || txt.length() == 0) {
                return;
            }
            int max = BaseTable.this.getRowCount();
            int pos = BaseTable.this.getSelectedRow();
            if (pos == max - 1 || pos < 0) {
                pos = 0;
            }
            for (int i = 0; i < max; ++i) {
                boolean match = BaseTable.this.matchText(BaseTable.this.getValueAt(i, 0), txt);
                if (match) {
                    BaseTable.this.changeSelection(i, 0, false, false);
                    this.getRootPane().getLayeredPane().repaint();
                    break;
                }
                if (pos++ != max - 1) continue;
                pos = 0;
            }
        }

        private void forwardSearch(String txt) {
            if (txt == null || txt.length() == 0) {
                return;
            }
            int max = BaseTable.this.getRowCount();
            int pos = BaseTable.this.getSelectedRow() + 1;
            if (pos == max - 1 || pos < 0) {
                pos = 0;
            }
            for (int i = pos; i < max; ++i) {
                boolean match = BaseTable.this.matchText(BaseTable.this.getValueAt(i, 0), txt);
                if (!match) continue;
                BaseTable.this.changeSelection(i, 0, false, false);
                this.repaint();
                break;
            }
        }

        private void reverseSearch(String txt) {
            if (txt == null || txt.length() == 0) {
                return;
            }
            int max = BaseTable.this.getRowCount();
            int pos = BaseTable.this.getSelectedRow();
            if (pos < 1) {
                pos = max - 1;
            }
            for (int i = pos - 1; i >= 0; --i) {
                boolean match = BaseTable.this.matchText(BaseTable.this.getValueAt(i, 0), txt);
                if (!match) continue;
                BaseTable.this.changeSelection(i, 0, false, false);
                this.repaint();
                break;
            }
        }

        @Override
        public void componentResized(ComponentEvent e) {
            BaseTable.this.hideSearchField();
        }

        @Override
        public void componentMoved(ComponentEvent e) {
            BaseTable.this.hideSearchField();
        }

        @Override
        public void componentShown(ComponentEvent e) {
        }

        @Override
        public void componentHidden(ComponentEvent e) {
            BaseTable.this.hideSearchField();
        }
    }

    private class WL
    extends WindowAdapter {
        private WL() {
        }

        @Override
        public void windowDeactivated(WindowEvent we) {
            BaseTable.this.doFocusLost(we.getOppositeWindow());
        }
    }

}

