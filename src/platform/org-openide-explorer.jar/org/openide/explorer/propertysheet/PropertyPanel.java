/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.openide.explorer.propertysheet;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.io.PrintStream;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleRole;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.CellEditor;
import javax.swing.CellRendererPane;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.openide.explorer.PropertyPanelBridge;
import org.netbeans.modules.openide.explorer.TTVEnvBridge;
import org.openide.explorer.propertysheet.CustomEditorDisplayer;
import org.openide.explorer.propertysheet.DefaultPropertyModel;
import org.openide.explorer.propertysheet.EditablePropertyDisplayer;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.ExPropertyModel;
import org.openide.explorer.propertysheet.ModelProperty;
import org.openide.explorer.propertysheet.NodePropertyModel;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertyDialogManager;
import org.openide.explorer.propertysheet.PropertyDisplayer;
import org.openide.explorer.propertysheet.PropertyDisplayer_Editable;
import org.openide.explorer.propertysheet.PropertyDisplayer_Inline;
import org.openide.explorer.propertysheet.PropertyDisplayer_Mutable;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;
import org.openide.explorer.propertysheet.RendererPropertyDisplayer;
import org.openide.explorer.propertysheet.ReusablePropertyEnv;
import org.openide.explorer.propertysheet.ReusablePropertyModel;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class PropertyPanel
extends JComponent
implements Accessible {
    public static final int PREF_READ_ONLY = 1;
    public static final int PREF_CUSTOM_EDITOR = 2;
    public static final int PREF_INPUT_STATE = 4;
    public static final int PREF_TABLEUI = 8;
    public static final String PROP_PREFERENCES = "preferences";
    public static final String PROP_MODEL = "model";
    @Deprecated
    public static final String PROP_PROPERTY_EDITOR = "propertyEditor";
    public static final String PROP_STATE = "state";
    private int preferences;
    private PropertyModel model;
    private boolean changeImmediate = true;
    Component inner = null;
    private Listener listener = null;
    private Node.Property prop;
    private boolean settingModel = false;
    private boolean initializing = false;
    private PropertyDisplayer displayer = null;
    Object[] beans = null;
    private ReusablePropertyEnv reusableEnv = new ReusablePropertyEnv();
    private ReusablePropertyModel reusableModel = new ReusablePropertyModel(this.reusableEnv);
    private final boolean isGtk = "GTK".equals(UIManager.getLookAndFeel().getID()) || UIManager.getLookAndFeel().getClass().getSuperclass().getName().indexOf("Synth") != -1;

    public PropertyPanel() {
        this(ModelProperty.toProperty(null), 0, null);
    }

    public PropertyPanel(Object bean, String propertyName, int preferences) {
        this(ModelProperty.toProperty(new DefaultPropertyModel(bean, propertyName)), preferences, new DefaultPropertyModel(bean, propertyName));
    }

    public PropertyPanel(PropertyModel model, int preferences) {
        this(null, preferences, model);
    }

    public PropertyPanel(Node.Property p, int preferences) {
        this(p, preferences, null);
    }

    public PropertyPanel(Node.Property p) {
        this(p, 0, null);
    }

    PropertyPanel(Node[] nodes, String propertyName) throws ClassCastException, NullPointerException {
        this(nodes.length == 1 ? ModelProperty.findProperty(nodes[0], propertyName) : ModelProperty.toProperty(nodes, propertyName));
    }

    private PropertyPanel(Node.Property p, int preferences, PropertyModel mdl) {
        this.prop = p == null ? ModelProperty.toProperty(mdl) : p;
        this.preferences = preferences;
        this.initializing = true;
        this.setModel(mdl);
        this.initializing = false;
        this.setOpaque(true);
        if (!GraphicsEnvironment.isHeadless()) {
            this.getInputMap(1).put(KeyStroke.getKeyStroke(46, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "dump");
        }
        this.getActionMap().put("dump", new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent ae) {
                System.err.println("");
                System.err.println(PropertyPanel.this);
                System.err.println("");
            }
        });
        this.getActionMap().put("invokeCustomEditor", new CustomEditorProxyAction());
        PropertyPanelBridge.register(this, new BridgeAccessor(this));
    }

    @Override
    public void setBackground(Color c) {
        if (this.inner != null) {
            this.inner.setBackground(c);
        }
        super.setBackground(c);
    }

    @Override
    public void setForeground(Color c) {
        if (this.inner != null) {
            this.inner.setForeground(c);
        }
        super.setForeground(c);
    }

    private PropertyDisplayer findPropertyDisplayer() {
        boolean isTableUI;
        PropertyDisplayer result2;
        Node.Property prop = this.getProperty();
        if (!((this.preferences & 2) != 0 || (this.preferences & 1) == 0 && this.isEnabled())) {
            return this.getRendererComponent(prop);
        }
        switch (this.preferences) {
            PropertyDisplayer result2;
            case 1: 
            case 9: {
                result2 = this.getRendererComponent(prop);
                break;
            }
            case 2: 
            case 10: {
                result2 = new CustomEditorDisplayer(prop, this.model);
                break;
            }
            case 3: 
            case 11: {
                result2 = new CustomEditorDisplayer(prop, this.model);
                break;
            }
            case 4: 
            case 12: {
                result2 = new EditablePropertyDisplayer(prop, this.model);
                break;
            }
            case 5: 
            case 13: {
                result2 = this.getRendererComponent(prop);
                break;
            }
            case 6: 
            case 14: {
                result2 = new CustomEditorDisplayer(prop, this.model);
                break;
            }
            case 7: 
            case 15: {
                result2 = new CustomEditorDisplayer(prop, this.model);
                break;
            }
            default: {
                result2 = new EditablePropertyDisplayer(prop, this.model);
            }
        }
        if (result2 instanceof PropertyDisplayer_Inline) {
            PropertyDisplayer_Inline inline = result2;
            boolean tableUI = (this.preferences & 8) != 0 || Boolean.TRUE.equals(this.getClientProperty("flat"));
            inline.setTableUI(tableUI);
            if (inline.isTableUI()) {
                inline.setUseLabels(!tableUI);
            }
        }
        boolean bl = isTableUI = (this.preferences & 8) != 0;
        if (result2 instanceof CustomEditorDisplayer) {
            ((PropertyDisplayer_Editable)result2).setUpdatePolicy(this.changeImmediate ? 1 : 2);
        } else if (result2 instanceof PropertyDisplayer_Editable) {
            ((PropertyDisplayer_Editable)result2).setUpdatePolicy(isTableUI ? 0 : 1);
        }
        if ((this.preferences & 1) != 0 && result2 instanceof CustomEditorDisplayer) {
            ((CustomEditorDisplayer)result2).setEnabled(false);
        } else if (result2 instanceof PropertyDisplayer_Editable && !this.isEnabled()) {
            ((PropertyDisplayer_Editable)result2).setEnabled(this.isEnabled());
        }
        return result2;
    }

    private RendererPropertyDisplayer getRendererComponent(Node.Property prop) {
        RendererPropertyDisplayer result;
        if (this.inner instanceof RendererPropertyDisplayer) {
            ((RendererPropertyDisplayer)this.inner).setProperty(prop);
            result = (RendererPropertyDisplayer)this.inner;
        } else {
            result = new RendererPropertyDisplayer(prop);
        }
        return result;
    }

    private PropertyDisplayer getPropertyDisplayer() {
        if (this.displayer == null) {
            this.setDisplayer(this.findPropertyDisplayer());
        }
        return this.displayer;
    }

    private boolean commit() {
        if (this.displayer instanceof PropertyDisplayer_Editable) {
            try {
                return ((PropertyDisplayer_Editable)this.displayer).commit();
            }
            catch (IllegalArgumentException iae) {
                PropertyDialogManager.notify(iae);
                return false;
            }
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void installDisplayerComponent() {
        Component focus;
        boolean hadFocus;
        PropertyDisplayer displayer = this.getPropertyDisplayer();
        Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getPermanentFocusOwner();
        boolean bl = hadFocus = focusOwner == this || this.isAncestorOf(focusOwner);
        if (hadFocus) {
            KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();
        }
        Component newInner = displayer.getComponent();
        if (!(displayer instanceof PropertyDisplayer_Editable)) {
            newInner.setEnabled(this.isEnabled() && this.getProperty().canWrite());
        }
        newInner.setForeground(this.getForeground());
        newInner.setBackground(this.getBackground());
        if (newInner != this.inner) {
            Object object = this.getTreeLock();
            synchronized (object) {
                if (this.inner != null) {
                    this.remove(this.inner);
                }
                if (newInner != null) {
                    this.add(newInner);
                    newInner.invalidate();
                    this.inner = newInner;
                }
            }
        }
        if (this.isShowing() && !(this.getParent() instanceof CellRendererPane)) {
            this.validate();
        }
        if (hadFocus && this.isEnabled() && (this.preferences & 1) == 0) {
            this.requestFocus();
        }
        if ((!this.isEnabled() || (this.preferences & 1) != 0) && ((focus = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner()) == this.inner || this.inner instanceof Container && ((Container)this.inner).isAncestorOf(focus))) {
            this.transferFocusUpCycle();
        }
    }

    @Override
    public void doLayout() {
        this.layout();
    }

    @Override
    public void layout() {
        if (this.inner != null) {
            this.inner.setBounds(0, 0, this.getWidth(), this.getHeight());
        }
    }

    @Override
    public Dimension getMinimumSize() {
        return this.getPreferredSize();
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension result = !this.isDisplayable() && (this.preferences & 2) == 0 ? this.getRendererComponent(this.getProperty()).getComponent().getPreferredSize() : (this.inner != null ? this.inner.getPreferredSize() : PropUtils.getMinimumPanelSize());
        return result;
    }

    private void setDisplayer(PropertyDisplayer nue) {
        if (this.displayer != null) {
            this.detachFromDisplayer(this.displayer);
        }
        this.displayer = nue;
        if (nue != null) {
            this.attachToDisplayer(this.displayer);
        }
    }

    private void attachToDisplayer(PropertyDisplayer displayer) {
        if (displayer instanceof PropertyDisplayer_Inline) {
            this.updateDisplayerFromClientProps();
        }
        if (displayer instanceof CustomEditorDisplayer) {
            ((CustomEditorDisplayer)displayer).setRemoteEnvListener(this.getListener());
            ((CustomEditorDisplayer)displayer).setRemoteEnvVetoListener(this.getListener());
        }
        if (displayer instanceof EditablePropertyDisplayer) {
            ((EditablePropertyDisplayer)displayer).setRemoteEnvListener(this.getListener());
            ((EditablePropertyDisplayer)displayer).setRemoteEnvVetoListener(this.getListener());
            ((EditablePropertyDisplayer)displayer).addActionListener(this.getListener());
            PropertyEnv env = ((EditablePropertyDisplayer)displayer).getPropertyEnv();
            if (env != null) {
                env.setFeatureDescriptor((FeatureDescriptor)this.getProperty());
            }
        }
    }

    private void detachFromDisplayer(PropertyDisplayer displayer) {
        if (displayer instanceof CustomEditorDisplayer) {
            ((CustomEditorDisplayer)displayer).setRemoteEnvVetoListener(null);
        }
        if (displayer instanceof EditablePropertyDisplayer) {
            ((EditablePropertyDisplayer)displayer).setRemoteEnvVetoListener(null);
            ((EditablePropertyDisplayer)displayer).removeActionListener(this.getListener());
        }
    }

    @Override
    protected void firePropertyChange(String nm, Object old, Object nue) {
        if (("flat".equals(nm) || "radioButtonMax".equals(nm) || "suppressCustomEditor".equals(nm) || "useLabels".equals(nm)) && this.displayer != null && this.displayer instanceof PropertyDisplayer_Inline) {
            this.updateDisplayerFromClientProp(nm, nue);
        }
        super.firePropertyChange(nm, old, nue);
    }

    private void updateDisplayerFromClientProp(String nm, Object val) {
        PropertyDisplayer displayer = this.getPropertyDisplayer();
        if (displayer instanceof PropertyDisplayer_Inline) {
            PropertyDisplayer_Inline inline = (PropertyDisplayer_Inline)displayer;
            if ("flat".equals(nm)) {
                inline.setTableUI(Boolean.TRUE.equals(val));
                if (Boolean.TRUE.equals(val)) {
                    inline.setUseLabels(false);
                } else if (Boolean.FALSE.equals(val) && this.getClientProperty("useLabels") == null) {
                    inline.setUseLabels(true);
                }
            } else if ("radioButtonMax".equals(nm)) {
                int max = val instanceof Integer ? (Integer)val : 0;
                inline.setRadioButtonMax(max);
            } else if ("suppressCustomEditor".equals(nm)) {
                inline.setShowCustomEditorButton(!Boolean.TRUE.equals(val));
            } else if ("useLabels".equals(nm)) {
                inline.setUseLabels(Boolean.TRUE.equals(val));
            }
        }
    }

    @Override
    public boolean isFocusable() {
        return super.isFocusable() && this.isEnabled() && (this.preferences & 1) == 0;
    }

    @Override
    public void requestFocus() {
        if (!this.isEnabled() || (this.preferences & 1) != 0) {
            return;
        }
        if (this.inner != null && this.inner.isEnabled()) {
            super.requestFocus();
            this.inner.requestFocus();
        }
    }

    private void updateDisplayerFromClientProps() {
        String[] props = new String[]{"flat", "radioButtonMax", "suppressCustomEditor", "useLabels"};
        for (int i = 0; i < props.length; ++i) {
            Object o = this.getClientProperty(props[i]);
            if (o == null) continue;
            this.updateDisplayerFromClientProp(props[i], o);
        }
    }

    @Override
    protected void processFocusEvent(FocusEvent fe) {
        super.processFocusEvent(fe);
        if (fe.getID() == 1004 && this.inner != null && this.inner.isEnabled() && this.inner.isFocusTraversable()) {
            this.inner.requestFocus();
        }
    }

    private Listener getListener() {
        if (this.listener == null) {
            this.listener = new Listener();
        }
        return this.listener;
    }

    @Override
    public void addNotify() {
        this.attachToModel();
        if (this.displayer != null) {
            this.attachToDisplayer(this.displayer);
        }
        if (this.inner == null) {
            this.installDisplayerComponent();
        }
        super.addNotify();
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.detachFromModel();
        if (this.displayer != null && !(this.displayer instanceof RendererPropertyDisplayer)) {
            this.detachFromDisplayer(this.displayer);
            this.displayer = null;
        }
        if (null != this.inner && !(this.inner instanceof RendererPropertyDisplayer)) {
            this.remove(this.inner);
            this.inner = null;
        }
    }

    public int getPreferences() {
        return this.preferences;
    }

    public void setPreferences(int preferences) {
        if (preferences != this.preferences) {
            int oldPreferences = this.preferences;
            this.preferences = preferences;
            this.hardReset();
            this.firePropertyChange("preferences", oldPreferences, preferences);
        }
    }

    public PropertyModel getModel() {
        if (this.model == null) {
            return new NodePropertyModel(this.getProperty(), null);
        }
        return this.model;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setModel(PropertyModel model) {
        block7 : {
            if (model != this.model) {
                this.settingModel = true;
                if (this.model != null && this.listener != null) {
                    this.detachFromModel();
                }
                try {
                    if (!this.initializing) {
                        this.setProperty(ModelProperty.toProperty(model));
                        this.model = model;
                        if (model != null && this.isDisplayable()) {
                            this.attachToModel();
                        }
                        break block7;
                    }
                    this.model = model;
                    this.attachToModel();
                }
                finally {
                    this.settingModel = false;
                }
            }
        }
    }

    private final void attachToModel() {
        if (this.model != null) {
            this.model.addPropertyChangeListener(this.getListener());
        }
    }

    private final void detachFromModel() {
        if (this.model != null) {
            this.model.removePropertyChangeListener(this.getListener());
        }
    }

    Object[] getBeans() {
        return this.beans;
    }

    public final void setProperty(Node.Property p) {
        TTVEnvBridge bridge;
        Object bridgeID = this.getClientProperty("beanBridgeIdentifier");
        if (bridgeID != null && (bridge = TTVEnvBridge.findInstance(bridgeID)) != null) {
            this.beans = bridge.getCurrentBeans();
            bridge.clear();
        }
        if (p != this.prop) {
            this.prop = p;
            if (!this.settingModel) {
                this.model = null;
            }
            if (this.displayer != null) {
                if (this.displayer instanceof PropertyDisplayer_Mutable) {
                    ((PropertyDisplayer_Mutable)this.displayer).setProperty(p);
                } else {
                    this.hardReset();
                }
            }
        }
    }

    final void reset() {
        if ((this.preferences & 2) != 0) {
            this.getPropertyDisplayer().refresh();
        } else {
            this.hardReset();
        }
    }

    final void hardReset() {
        this.setDisplayer(this.findPropertyDisplayer());
        if (this.isDisplayable()) {
            this.installDisplayerComponent();
        }
    }

    public final Node.Property getProperty() {
        if (this.prop == null && this.model != null) {
            this.prop = ModelProperty.toProperty(this.model);
        }
        return this.prop;
    }

    public final Object getState() {
        if (this.displayer != null && this.displayer instanceof PropertyDisplayer_Editable) {
            return ((PropertyDisplayer_Editable)this.displayer).getPropertyEnv().getState();
        }
        PropertyEditor ed = this.propertyEditor();
        if (ed instanceof ExPropertyEditor) {
            ReusablePropertyEnv env = this.reusableEnv;
            this.reusableModel.setProperty(this.prop);
            ((ExPropertyEditor)ed).attachEnv(env);
            return env.getState();
        }
        return PropertyEnv.STATE_VALID;
    }

    public void updateValue() {
        if (this.displayer != null && this.displayer instanceof PropertyDisplayer_Editable) {
            PropertyEnv env = ((PropertyDisplayer_Editable)this.displayer).getPropertyEnv();
            if (PropertyEnv.STATE_NEEDS_VALIDATION.equals(env.getState())) {
                env.setState(PropertyEnv.STATE_VALID);
            }
            if (!this.changeImmediate) {
                this.commit();
            }
        }
    }

    @Deprecated
    public PropertyEditor getPropertyEditor() {
        return this.propertyEditor();
    }

    private PropertyEditor propertyEditor() {
        PropertyEditor result = null;
        if (this.displayer != null) {
            if (this.displayer instanceof CustomEditorDisplayer) {
                result = ((CustomEditorDisplayer)this.displayer).getPropertyEditor();
            } else if (this.displayer instanceof EditablePropertyDisplayer) {
                result = ((EditablePropertyDisplayer)this.displayer).getPropertyEditor();
            }
        }
        if (result == null) {
            result = PropUtils.getPropertyEditor(this.getProperty());
        }
        return result;
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (this.inner != null) {
            PropertyDisplayer displayer = this.getPropertyDisplayer();
            if (displayer instanceof PropertyDisplayer_Editable) {
                ((PropertyDisplayer_Editable)displayer).setEnabled(enabled);
            } else {
                this.hardReset();
            }
        }
    }

    public boolean isChangeImmediate() {
        return this.changeImmediate;
    }

    public void setChangeImmediate(boolean changeImmediate) {
        PropertyDisplayer displayer;
        if (this.changeImmediate == changeImmediate) {
            return;
        }
        this.changeImmediate = changeImmediate;
        if (this.isShowing() && (displayer = this.getPropertyDisplayer()) instanceof PropertyDisplayer_Editable) {
            ((PropertyDisplayer_Editable)displayer).setUpdatePolicy(changeImmediate ? 1 : 2);
        }
        this.firePropertyChange("changeImmediate", changeImmediate ? Boolean.FALSE : Boolean.TRUE, changeImmediate ? Boolean.TRUE : Boolean.FALSE);
    }

    @Override
    public String toString() {
        if ((this.preferences & 2) != 0) {
            return super.toString() + " - " + PropertyPanel.prefsToString(this.getPreferences());
        }
        return this.getClass().getName() + System.identityHashCode(this) + PropertyPanel.prefsToString(this.getPreferences()) + " propertyRenderer: " + (this.inner == null ? " null " : this.inner.toString());
    }

    private static String prefsToString(int prefs) {
        StringBuffer sb = new StringBuffer(" prefs:");
        int[] vals = new int[]{2, 4, 1};
        String[] s = new String[]{"PREF_CUSTOM_EDITOR", "PREF_INPUT_STATE", "PREF_READ_ONLY"};
        boolean found = false;
        for (int i = 0; i < vals.length; ++i) {
            if ((vals[i] & prefs) != 0) {
                sb.append(s[i]);
            }
            if (found && i != vals.length - 1) {
                sb.append(",");
            }
            found = true;
        }
        return sb.toString();
    }

    @Override
    public void paint(Graphics g) {
        if (this.isGtk) {
            Color c = this.getBackground();
            if (c == null) {
                c = UIManager.getColor("control");
            }
            if (c == null) {
                c = Color.LIGHT_GRAY;
            }
            g.setColor(c);
            g.fillRect(0, 0, this.getWidth(), this.getHeight());
        }
        super.paint(g);
    }

    @Override
    public AccessibleContext getAccessibleContext() {
        if (this.accessibleContext == null) {
            this.accessibleContext = new AccessiblePropertyPanel();
        }
        return this.accessibleContext;
    }

    private static final class BridgeAccessor
    implements PropertyPanelBridge.Accessor {
        private final Reference<PropertyPanel> panelRef;

        public BridgeAccessor(PropertyPanel panel) {
            this.panelRef = new WeakReference<PropertyPanel>(panel);
        }

        @Override
        public boolean commit() {
            PropertyPanel panel = this.panelRef.get();
            if (panel != null) {
                return panel.commit();
            }
            return false;
        }
    }

    private class Listener
    implements PropertyChangeListener,
    VetoableChangeListener,
    ChangeListener,
    ActionListener {
        private Listener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (evt.getSource() instanceof PropertyEnv) {
                PropertyPanel.this.firePropertyChange("state", evt.getOldValue(), evt.getNewValue());
            }
            if (evt.getSource() instanceof PropertyModel) {
                if (evt.getOldValue() == null && evt.getNewValue() == null) {
                    PropertyPanel.this.hardReset();
                } else {
                    PropertyPanel.this.reset();
                }
            }
        }

        @Override
        public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
        }

        @Override
        public void stateChanged(ChangeEvent e) {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Object beanBridge;
            if (PropertyPanel.this.inner == e.getSource() && "enterPressed".equals(e.getActionCommand()) && null != (beanBridge = PropertyPanel.this.getClientProperty("beanBridgeIdentifier")) && beanBridge instanceof CellEditor) {
                ((CellEditor)beanBridge).stopCellEditing();
            }
        }
    }

    private class AccessiblePropertyPanel
    extends JComponent.AccessibleJComponent {
        AccessiblePropertyPanel() {
            super(PropertyPanel.this);
        }

        @Override
        public AccessibleRole getAccessibleRole() {
            return AccessibleRole.PANEL;
        }

        @Override
        public String getAccessibleName() {
            String name = super.getAccessibleName();
            if (name == null && PropertyPanel.this.model instanceof ExPropertyModel) {
                FeatureDescriptor fd = ((ExPropertyModel)PropertyPanel.this.model).getFeatureDescriptor();
                name = NbBundle.getMessage(PropertyPanel.class, (String)"ACS_PropertyPanel", (Object)fd.getDisplayName());
            }
            return name;
        }

        @Override
        public String getAccessibleDescription() {
            String description = super.getAccessibleDescription();
            if (description == null && PropertyPanel.this.model instanceof ExPropertyModel) {
                FeatureDescriptor fd = ((ExPropertyModel)PropertyPanel.this.model).getFeatureDescriptor();
                description = NbBundle.getMessage(PropertyPanel.class, (String)"ACSD_PropertyPanel", (Object)fd.getShortDescription());
            }
            return description;
        }
    }

    private class CustomEditorProxyAction
    extends AbstractAction {
        private CustomEditorProxyAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Action wrapped = this.getWrapped();
            if (wrapped != null) {
                wrapped.actionPerformed(e);
            } else {
                Utilities.disabledActionBeep();
            }
        }

        private Action getWrapped() {
            Node.Property p = PropertyPanel.this.getProperty();
            EditablePropertyDisplayer pd = PropertyPanel.this.getPropertyDisplayer() instanceof EditablePropertyDisplayer ? (EditablePropertyDisplayer)PropertyPanel.this.getPropertyDisplayer() : new EditablePropertyDisplayer(p);
            return pd.getCustomEditorAction();
        }

        @Override
        public boolean isEnabled() {
            Action wrapped = this.getWrapped();
            if (wrapped != null) {
                return wrapped.isEnabled();
            }
            return PropertyPanel.this.getProperty() != null;
        }
    }

}

