/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.propertysheet;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.awt.image.WritableRaster;
import java.beans.PropertyEditor;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ButtonModel;
import javax.swing.CellRendererPane;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.RepaintManager;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;

class ButtonPanel
extends JComponent
implements InplaceEditor {
    public static final Object editorActionKey = "openCustomEditor";
    private final boolean log = PropUtils.isLoggable(ButtonPanel.class);
    JComponent comp = null;
    private ConditionallyFocusableButton button;
    boolean needLayout = true;
    private InplaceEditor inplace = null;
    boolean clearing = false;

    public ButtonPanel() {
        this.createButton();
        this.setOpaque(true);
    }

    private void createButton() {
        this.button = new ConditionallyFocusableButton();
        int buttonWidth = PropUtils.getCustomButtonWidth();
        this.button.setBounds(this.getWidth() - buttonWidth, 0, buttonWidth, this.getHeight());
        this.button.setIcon(PropUtils.getCustomButtonIcon());
        this.button.setRolloverIcon(PropUtils.getCustomButtonIcon());
        this.button.setMargin(null);
        this.button.setName("Custom editor button - editor instance");
        this.button.setText(null);
        this.button.putClientProperty("hideActionText", Boolean.TRUE);
        this.add(this.button);
    }

    void setButtonAction(Action a) {
        this.button.setAction(a);
        this.button.setIcon(PropUtils.getCustomButtonIcon());
        this.button.setRolloverIcon(PropUtils.getCustomButtonIcon());
    }

    @Override
    public void setOpaque(boolean b) {
        if (this.getInplaceEditor() != null) {
            this.getInplaceEditor().getComponent().setOpaque(true);
        }
    }

    @Override
    public void setFont(Font f) {
        if (this.comp != null) {
            this.comp.setFont(f);
        }
        super.setFont(f);
    }

    public InplaceEditor getInplaceEditor() {
        return this.inplace;
    }

    public void setCustomButtonBackground(Color c) {
        this.button.setBackground(c);
    }

    public void setRolloverPoint(Point p) {
        if (p != null) {
            if (p.x < this.getWidth() - PropUtils.getCustomButtonWidth()) {
                this.button.getModel().setRollover(false);
                if (this.comp instanceof AbstractButton) {
                    ((AbstractButton)this.comp).getModel().setRollover(true);
                }
            } else {
                this.button.getModel().setRollover(true);
                if (this.comp instanceof AbstractButton) {
                    ((AbstractButton)this.comp).getModel().setRollover(false);
                }
            }
        } else {
            this.button.getModel().setRollover(false);
            if (this.comp instanceof AbstractButton) {
                ((AbstractButton)this.comp).getModel().setRollover(false);
            }
        }
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension result;
        if (this.comp != null) {
            result = new Dimension(this.comp.getPreferredSize());
            result.width += this.button.getWidth();
            result.height = Math.max(result.height, this.button.getPreferredSize().height);
        } else {
            result = new Dimension(this.button.getPreferredSize());
        }
        return result;
    }

    @Override
    public void setEnabled(boolean val) {
        super.setEnabled(val);
        if (this.comp != null) {
            this.comp.setEnabled(val);
        }
        this.button.setEnabled(true);
    }

    private void setComponent(JComponent c) {
        if (c == this.comp) {
            return;
        }
        if (this.comp != null && this.comp.getParent() == this) {
            this.remove(this.comp);
        }
        if (this.log) {
            PropUtils.log(ButtonPanel.class, "Button panel setComponent to " + c);
        }
        this.comp = c;
        if (this.comp != null) {
            this.comp.setBackground(this.getBackground());
            this.comp.setForeground(this.getForeground());
            if (this.comp.isEnabled() != this.isEnabled()) {
                this.comp.setEnabled(this.isEnabled());
            }
            this.add(this.comp);
        }
        this.needLayout = true;
    }

    @Override
    public void setBackground(Color c) {
        super.setBackground(c);
        if (this.comp != null) {
            this.comp.setBackground(c);
            Color bttn = PropUtils.getButtonColor();
            if (bttn == null) {
                this.button.setBackground(c);
            } else {
                this.button.setBackground(bttn);
            }
        }
    }

    @Override
    public void setForeground(Color c) {
        super.setForeground(c);
        if (this.comp != null) {
            this.comp.setForeground(c);
            if (PropUtils.getButtonColor() == null) {
                this.button.setForeground(c);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void paint(Graphics g) {
        Graphics cg;
        int width;
        if (this.isShowing()) {
            super.paint(g);
            return;
        }
        if (this.needLayout) {
            this.doLayout();
        }
        if ((cg = g.create(0, 0, (width = this.getWidth()) - this.button.getWidth(), this.getHeight())) instanceof Graphics2D) {
            ((Graphics2D)cg).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }
        try {
            if (this.comp instanceof InplaceEditor) {
                this.comp.paint(cg);
                if (this.comp.getParent() != this) {
                    this.add(this.comp);
                }
            }
        }
        finally {
            cg.dispose();
        }
        if ((cg = g.create(width - this.button.getWidth(), 0, this.button.getWidth(), this.getHeight())) instanceof Graphics2D) {
            ((Graphics2D)cg).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }
        try {
            this.button.paint(cg);
        }
        finally {
            cg.dispose();
        }
        if (this.getParent() instanceof CellRendererPane) {
            RepaintManager.currentManager(this).markCompletelyClean(this);
        }
    }

    @Override
    public void reshape(int x, int y, int w, int h) {
        super.reshape(x, y, w, h);
        this.needLayout = true;
    }

    @Override
    public void requestFocus() {
        if (this.comp != null) {
            this.comp.requestFocus();
        }
    }

    @Override
    public boolean requestFocusInWindow() {
        if (this.comp != null) {
            return this.comp.requestFocusInWindow();
        }
        return false;
    }

    @Override
    public void addFocusListener(FocusListener l) {
        if (this.comp != null) {
            this.button.addFocusListener(l);
            this.comp.addFocusListener(l);
        }
    }

    @Override
    public void removeFocusListener(FocusListener l) {
        if (this.comp != null) {
            this.button.removeFocusListener(l);
            this.comp.removeFocusListener(l);
        }
    }

    public void setInplaceEditor(InplaceEditor ed) {
        if (this.inplace == ed && this.isAncestorOf(this.inplace.getComponent())) {
            return;
        }
        if (this.inplace != null) {
            this.setComponent(null);
        }
        this.inplace = ed;
        this.setComponent(this.inplace.getComponent());
        this.needLayout = true;
    }

    @Override
    public void addActionListener(ActionListener al) {
        this.inplace.addActionListener(al);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void clear() {
        this.clearing = true;
        try {
            this.inplace.clear();
            this.inplace = null;
            this.setComponent(null);
        }
        finally {
            this.clearing = false;
        }
    }

    @Override
    public JComponent getComponent() {
        return this;
    }

    @Override
    public void connect(PropertyEditor pe, PropertyEnv env) {
        this.inplace.connect(pe, env);
    }

    @Override
    public KeyStroke[] getKeyStrokes() {
        return this.inplace.getKeyStrokes();
    }

    @Override
    public PropertyEditor getPropertyEditor() {
        return this.inplace.getPropertyEditor();
    }

    @Override
    public PropertyModel getPropertyModel() {
        return this.inplace.getPropertyModel();
    }

    @Override
    public Object getValue() {
        return this.inplace.getValue();
    }

    @Override
    public boolean isKnownComponent(Component c) {
        return c == this || this.inplace.isKnownComponent(c);
    }

    @Override
    public void removeActionListener(ActionListener al) {
        this.inplace.removeActionListener(al);
    }

    @Override
    public void reset() {
        this.inplace.reset();
    }

    @Override
    public void setPropertyModel(PropertyModel pm) {
        this.inplace.setPropertyModel(pm);
    }

    @Override
    public void setValue(Object o) {
        this.inplace.setValue(o);
    }

    @Override
    public boolean supportsTextEntry() {
        return this.inplace.supportsTextEntry();
    }

    @Override
    public void doLayout() {
        if (this.comp != null) {
            this.comp.setBounds(0, 0, this.getWidth() - PropUtils.getCustomButtonWidth(), this.getHeight());
            this.comp.doLayout();
        }
        this.button.setBounds(this.getWidth() - PropUtils.getCustomButtonWidth(), 0, PropUtils.getCustomButtonWidth(), this.getHeight());
        if (this.log) {
            PropUtils.log(ButtonPanel.class, "Laying out button panel.  Bounds are " + this.getBounds() + ", custom editor button bounds: " + this.button.getBounds() + " comp is " + this.comp);
        }
        this.needLayout = false;
    }

    @Override
    public Dimension getMinimumSize() {
        return this.getPreferredSize();
    }

    private class ConditionallyFocusableButton
    extends JButton {
        private AffineTransform at;
        private BufferedImage snapshot;

        public ConditionallyFocusableButton() {
            this.at = AffineTransform.getTranslateInstance(0.0, 0.0);
            this.snapshot = null;
        }

        @Override
        public boolean isFocusable() {
            return ButtonPanel.this.getParent() != null && !ButtonPanel.this.clearing;
        }

        @Override
        public void paint(Graphics g) {
            if (PropUtils.useOptimizedCustomButtonPainting() && !this.hasFocus()) {
                if (ButtonPanel.this.log) {
                    PropUtils.log(ButtonPanel.class, "Blitting custom editor button backing store for button at " + this.getBounds() + " in " + (this.getParent() == null ? " null parent" : new StringBuilder().append(this.getParent()).append("editor=").append(ButtonPanel.this.inplace).toString()));
                }
                ((Graphics2D)g).drawRenderedImage(this.getSnapshot(), this.at);
            } else {
                if (ButtonPanel.this.log) {
                    PropUtils.log(ButtonPanel.class, "Painting unoptimized custom editor button button at " + this.getBounds() + " in " + (this.getParent() == null ? " null parent" : new StringBuilder().append(this.getParent()).append("editor=").append(ButtonPanel.this.inplace).toString()));
                }
                super.paint(g);
            }
        }

        public BufferedImage getSnapshot() {
            if (this.snapshot == null) {
                this.snapshot = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage(this.getWidth(), this.getHeight());
                if (ButtonPanel.this.log) {
                    PropUtils.log(ButtonPanel.class, "Created " + this.snapshot + " custom editor button backing image");
                }
                if (this.snapshot.getAlphaRaster() == null) {
                    this.snapshot = new BufferedImage(this.getWidth(), this.getHeight(), 2);
                }
                Graphics g = this.snapshot.getGraphics();
                super.paint(g);
            }
            return this.snapshot;
        }
    }

}

