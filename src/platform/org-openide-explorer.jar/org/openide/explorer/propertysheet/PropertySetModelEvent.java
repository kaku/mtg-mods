/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.propertysheet;

import java.util.EventObject;

class PropertySetModelEvent
extends EventObject {
    public static final int TYPE_INSERT = 0;
    public static final int TYPE_REMOVE = 1;
    public static final int TYPE_WHOLESALE_CHANGE = 2;
    int type = 2;
    int start = -1;
    int end = -1;
    boolean reordering = false;

    public PropertySetModelEvent(Object source) {
        super(source);
    }

    public PropertySetModelEvent(Object source, int type, int start, int end, boolean reordering) {
        super(source);
        this.type = type;
        this.start = start;
        this.end = end;
        this.reordering = reordering;
    }

    public int getType() {
        return this.type;
    }

    public int getStartRow() {
        return this.start;
    }

    public int getEndRow() {
        return this.end;
    }

    public boolean isReordering() {
        return this.reordering;
    }
}

