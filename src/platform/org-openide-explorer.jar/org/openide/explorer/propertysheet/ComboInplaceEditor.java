/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.propertysheet;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.beans.FeatureDescriptor;
import java.beans.PropertyEditor;
import java.util.concurrent.atomic.AtomicReference;
import javax.swing.ComboBoxEditor;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.text.JTextComponent;
import org.openide.explorer.propertysheet.ComboBoxAutoCompleteSupport;
import org.openide.explorer.propertysheet.IncrementPropertyValueSupport;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;
import org.openide.explorer.propertysheet.SheetTable;
import org.openide.explorer.propertysheet.editors.EnhancedPropertyEditor;

class ComboInplaceEditor
extends JComboBox
implements InplaceEditor,
FocusListener,
AncestorListener,
IncrementPropertyValueSupport {
    static final KeyStroke[] cbKeyStrokes = new KeyStroke[]{KeyStroke.getKeyStroke(40, 0, false), KeyStroke.getKeyStroke(38, 0, false), KeyStroke.getKeyStroke(40, 0, true), KeyStroke.getKeyStroke(38, 0, true), KeyStroke.getKeyStroke(34, 0, false), KeyStroke.getKeyStroke(33, 0, false), KeyStroke.getKeyStroke(34, 0, true), KeyStroke.getKeyStroke(33, 0, true)};
    private static PopupChecker checker = null;
    protected PropertyEditor editor;
    protected PropertyEnv env;
    private ListCellRenderer originalRenderer;
    protected PropertyModel mdl;
    boolean inSetUI = false;
    private boolean tableUI;
    private boolean connecting = false;
    private boolean hasBeenEditable = false;
    private boolean needLayout = false;
    private boolean suppressFireActionEvent = false;
    private final boolean isAutoComplete;
    private boolean strictAutoCompleteMatching;
    private boolean in_setSelectedItem = false;

    public ComboInplaceEditor(boolean tableUI) {
        if (tableUI) {
            this.putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE);
        }
        if (Boolean.getBoolean("netbeans.ps.combohack")) {
            this.setLightWeightPopupEnabled(false);
        }
        if (this.getClass() == ComboInplaceEditor.class) {
            this.enableEvents(4);
        }
        this.tableUI = tableUI;
        if (tableUI) {
            this.updateUI();
        }
        this.originalRenderer = this.getRenderer();
        this.isAutoComplete = ComboBoxAutoCompleteSupport.install(this);
        String lafId = UIManager.getLookAndFeel().getID();
        if ("Aqua".equals(lafId) || "Metal".equals(lafId)) {
            UIManager.put("PopupMenu.consumeEventOnClose", Boolean.TRUE);
        }
    }

    @Override
    public void addNotify() {
        super.addNotify();
        if (this.isEditable() && this.getClass() == ComboInplaceEditor.class) {
            this.getEditor().getEditorComponent().addFocusListener(this);
        }
        this.getLayout().layoutContainer(this);
    }

    @Override
    public void setEditable(boolean val) {
        boolean hadBeenEditable = this.hasBeenEditable;
        this.hasBeenEditable |= val;
        super.setEditable(val);
        if (hadBeenEditable != this.hasBeenEditable) {
            this.log("Combo editor for " + this.editor + " setEditable (" + val + ")");
            this.needLayout = true;
        }
    }

    @Override
    public void removeNotify() {
        this.log("Combo editor for " + this.editor + " removeNotify forcing popup close");
        this.setPopupVisible(false);
        super.removeNotify();
        this.getEditor().getEditorComponent().removeFocusListener(this);
    }

    @Override
    public Insets getInsets() {
        if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
            return new Insets(0, 0, 0, 0);
        }
        return super.getInsets();
    }

    @Override
    public void clear() {
        this.editor = null;
        this.env = null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void connect(PropertyEditor pe, PropertyEnv env) {
        this.connecting = true;
        try {
            this.log("Combo editor connect to " + pe + " env=" + env);
            this.env = env;
            this.editor = pe;
            this.setModel(new DefaultComboBoxModel<String>(pe.getTags()));
            boolean editable = this.editor instanceof EnhancedPropertyEditor ? ((EnhancedPropertyEditor)this.editor).supportsEditingTaggedValues() : env != null && Boolean.TRUE.equals(env.getFeatureDescriptor().getValue("canEditAsText"));
            boolean noAutoComplete = null != env && Boolean.FALSE.equals(env.getFeatureDescriptor().getValue("canAutoComplete"));
            this.strictAutoCompleteMatching = !editable;
            this.setEditable(editable || this.isAutoComplete && !noAutoComplete);
            this.setActionCommand("success");
            Object customRendererSupport = env.getFeatureDescriptor().getValue("customListCellRendererSupport");
            if (customRendererSupport != null) {
                AtomicReference ref = (AtomicReference)customRendererSupport;
                ref.set(this.originalRenderer);
                this.setRenderer((ListCellRenderer)customRendererSupport);
            }
            if (PropUtils.supportsValueIncrement(env)) {
                PropUtils.wrapUpDownArrowActions(this, this);
                PropUtils.wrapUpDownArrowActions((JComponent)this.getEditor().getEditorComponent(), this);
            }
            this.reset();
        }
        finally {
            this.connecting = false;
        }
    }

    private void log(String s) {
        if (PropUtils.isLoggable(ComboInplaceEditor.class) && this.getClass() == ComboInplaceEditor.class) {
            PropUtils.log(ComboInplaceEditor.class, s);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setSelectedItem(Object o) {
        try {
            if (this.in_setSelectedItem) {
                this.in_setSelectedItem = false;
                if (PropUtils.supportsValueIncrement(this.env)) {
                    return;
                }
            }
            this.in_setSelectedItem = true;
            if (o == null && this.editor != null && this.editor.getTags() != null && this.editor.getTags().length > 0) {
                o = this.editor.getTags()[0];
            }
            if (o != null) {
                super.setSelectedItem(o);
            }
        }
        finally {
            this.in_setSelectedItem = false;
        }
    }

    @Override
    public void fireActionEvent() {
        if (this.connecting || this.editor == null) {
            return;
        }
        if (this.editor == null) {
            return;
        }
        if (this.suppressFireActionEvent) {
            return;
        }
        if ("comboBoxEdited".equals(this.getActionCommand())) {
            this.log("Translating comboBoxEdited action command to COMMAND_SUCCESS");
            this.setActionCommand("success");
        }
        this.log("Combo editor firing ActionPerformed command=" + this.getActionCommand());
        super.fireActionEvent();
    }

    @Override
    public void reset() {
        String initialEditValue;
        String targetValue = null;
        if (this.editor != null) {
            this.log("Combo editor reset setting selected item to " + this.editor.getAsText());
            targetValue = this.editor.getAsText();
        }
        if (this.getClass() == ComboInplaceEditor.class && this.env != null && this.env.getFeatureDescriptor() != null && (initialEditValue = (String)this.env.getFeatureDescriptor().getValue("initialEditValue")) != null) {
            targetValue = initialEditValue;
        }
        this.setSelectedItem(targetValue);
    }

    @Override
    public Object getValue() {
        if (this.isEditable()) {
            Object editorItem;
            if (this.isAutoComplete && null != (editorItem = this.getEditor().getItem())) {
                int selItem = ComboBoxAutoCompleteSupport.findMatch(this, editorItem.toString());
                if (selItem >= 0 && selItem < this.getItemCount()) {
                    return this.getItemAt(selItem);
                }
                if (this.strictAutoCompleteMatching) {
                    int selIndex = this.getSelectedIndex();
                    if (selIndex < 0) {
                        if (null != this.editor) {
                            return this.editor.getAsText();
                        }
                        return null;
                    }
                    return this.getSelectedItem();
                }
            }
            return this.getEditor().getItem();
        }
        return this.getSelectedItem();
    }

    @Override
    public PropertyEditor getPropertyEditor() {
        return this.editor;
    }

    @Override
    public PropertyModel getPropertyModel() {
        return this.mdl;
    }

    @Override
    public void setPropertyModel(PropertyModel pm) {
        this.log("Combo editor set property model to " + pm);
        this.mdl = pm;
    }

    @Override
    public JComponent getComponent() {
        return this;
    }

    @Override
    public KeyStroke[] getKeyStrokes() {
        return cbKeyStrokes;
    }

    @Override
    public void updateUI() {
        boolean useClean;
        LookAndFeel lf = UIManager.getLookAndFeel();
        String id = lf.getID();
        boolean bl = useClean = this.tableUI && (lf instanceof MetalLookAndFeel || "GTK".equals(id) || "Nimbus".equals(id) || "Aqua".equals(id) && "10.5".compareTo(System.getProperty("os.version")) <= 0 || PropUtils.isWindowsVistaLaF() || "Kunststoff".equals(id));
        if (useClean) {
            super.setUI(PropUtils.createComboUI(this, this.tableUI));
        } else {
            super.updateUI();
        }
        if (this.tableUI & this.getEditor().getEditorComponent() instanceof JComponent) {
            ((JComponent)this.getEditor().getEditorComponent()).setBorder(null);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setUI(ComboBoxUI ui) {
        this.inSetUI = true;
        try {
            super.setUI(ui);
        }
        finally {
            this.inSetUI = false;
        }
    }

    @Override
    public void showPopup() {
        try {
            this.log(" Combo editor show popup");
            super.showPopup();
        }
        catch (NullPointerException e) {
            this.log(" Combo editor show popup later due to npe");
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    ComboInplaceEditor.this.showPopup();
                }
            });
        }
    }

    private void prepareEditor() {
        Component c = this.getEditor().getEditorComponent();
        if (c instanceof JTextComponent) {
            JTextComponent jtc = (JTextComponent)c;
            String s = jtc.getText();
            if (s != null && s.length() > 0) {
                jtc.setSelectionStart(0);
                jtc.setSelectionEnd(s.length());
            }
            if (this.tableUI) {
                jtc.setBackground(this.getBackground());
            } else {
                jtc.setBackground(PropUtils.getTextFieldBackground());
            }
            if (this.tableUI) {
                jtc.requestFocus();
            }
        }
        if (this.getLayout() != null) {
            this.getLayout().layoutContainer(this);
        }
        this.repaint();
    }

    @Override
    public void processFocusEvent(FocusEvent fe) {
        if (fe.getID() == 1005 && fe.getOppositeComponent() == this.getEditor().getEditorComponent() && this.isPopupVisible()) {
            return;
        }
        super.processFocusEvent(fe);
        if (PropUtils.isLoggable(ComboInplaceEditor.class)) {
            PropUtils.log(ComboInplaceEditor.class, "Focus event on combo editor");
            PropUtils.log(ComboInplaceEditor.class, fe);
        }
        Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (this.isDisplayable() && fe.getID() == 1004 && focusOwner == this && !this.isPopupVisible()) {
            if (this.isEditable()) {
                this.prepareEditor();
                if (this.tableUI) {
                    SwingUtilities.invokeLater(new PopupChecker());
                }
            } else if (this.tableUI && (null == this.env || !PropUtils.supportsValueIncrement(this.env))) {
                this.showPopup();
                SwingUtilities.invokeLater(new PopupChecker());
            }
            this.repaint();
        } else if (fe.getID() == 1005 && this.isPopupVisible() && !this.isDisplayable()) {
            if (!PropUtils.psCommitOnFocusLoss) {
                this.setActionCommand("failure");
                this.fireActionEvent();
            }
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    if (!ComboInplaceEditor.this.isDisplayable()) {
                        ComboInplaceEditor.this.hidePopup();
                    }
                }
            });
        }
        this.repaint();
    }

    @Override
    public boolean isKnownComponent(Component c) {
        return c == this.getEditor().getEditorComponent();
    }

    @Override
    public void setValue(Object o) {
        ComboBoxAutoCompleteSupport.setIgnoreSelectionEvents(this, true);
        this.setSelectedItem(o);
        ComboBoxAutoCompleteSupport.setIgnoreSelectionEvents(this, false);
    }

    @Override
    public boolean supportsTextEntry() {
        return this.isEditable();
    }

    @Override
    protected void installAncestorListener() {
        if (this.tableUI) {
            this.addAncestorListener(this);
        } else {
            super.installAncestorListener();
        }
    }

    @Override
    public void addFocusListener(FocusListener fl) {
        if (!this.inSetUI || !this.tableUI) {
            super.addFocusListener(fl);
        }
    }

    @Override
    public void focusGained(FocusEvent e) {
        this.prepareEditor();
        if (null == this.env || !PropUtils.supportsValueIncrement(this.env)) {
            this.showPopup();
        }
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component c = e.getOppositeComponent();
        if (!this.isAncestorOf(c) && c != this.getEditor().getEditorComponent()) {
            if (c == this || c instanceof SheetTable && ((SheetTable)c).isAncestorOf(this)) {
                return;
            }
            this.setActionCommand("failure");
            this.log(" Combo editor lost focus - setting action command to failure");
            this.getEditor().getEditorComponent().removeFocusListener(this);
            if (checker == null) {
                this.log("No active popup checker, firing action event");
                this.fireActionEvent();
            }
        }
    }

    @Override
    public void firePopupMenuCanceled() {
        super.firePopupMenuCanceled();
        if (this.isEditable()) {
            Component focus = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            if (this.isDisplayable() && focus == this) {
                this.log("combo editor popup menu canceled.  Requesting focus on editor component");
                this.getEditor().getEditorComponent().requestFocus();
            }
        }
    }

    @Override
    public void processKeyEvent(KeyEvent ke) {
        super.processKeyEvent(ke);
        if (ke.getID() == 401 && ke.getKeyCode() == 27) {
            this.setActionCommand("failure");
            this.fireActionEvent();
        }
    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        checker = new PopupChecker();
        SwingUtilities.invokeLater(checker);
    }

    @Override
    public void ancestorMoved(AncestorEvent event) {
        if (this.needLayout && this.getLayout() != null) {
            this.getLayout().layoutContainer(this);
        }
    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {
    }

    @Override
    public void paintChildren(Graphics g) {
        if (this.editor != null && !this.hasFocus() && this.editor.isPaintable()) {
            return;
        }
        super.paintChildren(g);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void paintComponent(Graphics g) {
        if (this.editor != null && !this.hasFocus() && this.editor.isPaintable()) {
            Insets ins = this.getInsets();
            Color c = g.getColor();
            try {
                g.setColor(this.getBackground());
                g.fillRect(0, 0, this.getWidth(), this.getHeight());
            }
            finally {
                g.setColor(c);
            }
            ins.left += PropUtils.getTextMargin();
            this.editor.paintValue(g, new Rectangle(ins.left, ins.top, this.getWidth() - (ins.right + ins.left), this.getHeight() - (ins.top + ins.bottom)));
        } else {
            super.paintComponent(g);
        }
    }

    @Override
    public boolean incrementValue() {
        return this.setNextValue(true);
    }

    @Override
    public boolean decrementValue() {
        return this.setNextValue(false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean setNextValue(boolean increment) {
        try {
            this.suppressFireActionEvent = true;
            if (this.isPopupVisible()) {
                boolean bl = false;
                return bl;
            }
            if (!PropUtils.supportsValueIncrement(this.env)) {
                boolean bl = false;
                return bl;
            }
            Object nextValue = PropUtils.getNextValue(this.env, increment);
            if (null == nextValue) {
                boolean bl = true;
                return bl;
            }
            this.setValue(nextValue);
            boolean bl = PropUtils.updateProp(this);
            return bl;
        }
        finally {
            this.suppressFireActionEvent = false;
        }
    }

    @Override
    public boolean isIncrementEnabled() {
        return !this.isPopupVisible();
    }

    private class PopupChecker
    implements Runnable {
        private PopupChecker() {
        }

        @Override
        public void run() {
            Window w = KeyboardFocusManager.getCurrentKeyboardFocusManager().getActiveWindow();
            if (null != w && w.isAncestorOf(ComboInplaceEditor.this)) {
                if (ComboInplaceEditor.this.isShowing() && !ComboInplaceEditor.this.isPopupVisible()) {
                    ComboInplaceEditor.this.log("Popup checker ensuring editor prepared or popup visible");
                    if (ComboInplaceEditor.this.isEditable()) {
                        ComboInplaceEditor.this.prepareEditor();
                    }
                    ComboInplaceEditor.this.showPopup();
                }
                checker = null;
            }
        }
    }

}

