/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 */
package org.openide.explorer.propertysheet;

import org.openide.explorer.propertysheet.PropertyDisplayer;
import org.openide.nodes.Node;

interface PropertyDisplayer_Mutable
extends PropertyDisplayer {
    public void setProperty(Node.Property var1);
}

