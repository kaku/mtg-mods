/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.propertysheet;

import java.util.HashMap;
import java.util.Map;

class SelectionAndScrollPositionManager {
    private static Map<String, String> groupsToNodes = new HashMap<String, String>();
    private static Map<String, Integer> namesToPositions = new HashMap<String, Integer>();
    private static final Integer zero = new Integer(0);
    private String lastSelectedGroup = "";
    private String nodeName = null;

    SelectionAndScrollPositionManager() {
    }

    public void setCurrentNodeName(String name) {
        this.nodeName = name;
    }

    public String getCurrentNodeName() {
        return this.nodeName;
    }

    public String getLastSelectedGroupName() {
        return this.lastSelectedGroup;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void storeScrollPosition(int pos, String name) {
        if (pos >= 0) {
            Map<String, Integer> map = namesToPositions;
            synchronized (map) {
                namesToPositions.put(name, pos);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void storeLastSelectedGroup(String group) {
        if (this.nodeName != null) {
            Map<String, String> map = groupsToNodes;
            synchronized (map) {
                this.lastSelectedGroup = group;
                groupsToNodes.put(this.nodeName, group);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String getGroupNameForNodeName(String name) {
        String result = null;
        Map<String, String> map = groupsToNodes;
        synchronized (map) {
            result = groupsToNodes.get(name);
        }
        if (result == null) {
            result = this.lastSelectedGroup;
        }
        return result;
    }

    public int getScrollPositionForNodeName(String name) {
        Integer result = zero;
        Integer found = namesToPositions.get(name);
        if (found != null) {
            result = found;
        }
        return result;
    }
}

