/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.NodeAction
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.Lookups
 */
package org.openide.explorer.propertysheet;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.Scrollable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.openide.awt.Mnemonics;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.propertysheet.PropertyDialogManager;
import org.openide.explorer.propertysheet.PropertyPanel;
import org.openide.explorer.propertysheet.ProxyNode;
import org.openide.explorer.view.TreeTableView;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

class IndexedEditorPanel
extends JPanel
implements ExplorerManager.Provider,
PropertyChangeListener,
Lookup.Provider {
    private ExplorerManager em;
    private Lookup selectedLookup;
    private Action moveUp;
    private Action moveDown;
    private Action newAction;
    private boolean showingDetails = false;
    private Node rootNode;
    private Node.Property prop;
    private JScrollPane jScrollPane1 = new JScrollPane();
    private JPanel detailsPanel = new JPanel();
    private JButton deleteButton;
    private JButton detailsButton;
    private JButton downButton;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JButton newButton;
    private JLabel propertiesLabel;
    private JButton upButton;
    private TreeTableView treeTableView1 = new TreeTableView();
    private static final Logger LOG = Logger.getLogger(IndexedEditorPanel.class.getName());

    public IndexedEditorPanel(Node node, Node.Property[] props) {
        this.setBorder((Border)UIManager.get("Nb.ScrollPane.border"));
        this.initComponents();
        this.propertiesLabel.setLabelFor(this.treeTableView1);
        this.jPanel2.setLayout(new BorderLayout());
        this.jPanel2.add(this.treeTableView1);
        this.detailsPanel.setLayout(new BorderLayout());
        this.getExplorerManager().setRootContext(node);
        this.rootNode = node;
        this.prop = props[0];
        this.getExplorerManager().addPropertyChangeListener(this);
        this.treeTableView1.setProperties(props);
        this.treeTableView1.setRootVisible(false);
        this.treeTableView1.setDefaultActionAllowed(false);
        this.treeTableView1.setTreePreferredWidth(200);
        node.addPropertyChangeListener((PropertyChangeListener)this);
        try {
            ClassLoader l = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
            if (l == null) {
                l = Thread.currentThread().getContextClassLoader();
            }
            if (l == null) {
                l = this.getClass().getClassLoader();
            }
            this.selectedLookup = Lookups.proxy((Lookup.Provider)this);
            NodeAction globalMoveUp = (NodeAction)SystemAction.get(Class.forName("org.openide.actions.MoveUpAction", true, l).asSubclass(NodeAction.class));
            NodeAction globalMoveDown = (NodeAction)SystemAction.get(Class.forName("org.openide.actions.MoveDownAction", true, l).asSubclass(NodeAction.class));
            NodeAction globalNewAction = (NodeAction)SystemAction.get(Class.forName("org.openide.actions.NewAction", true, l).asSubclass(NodeAction.class));
            this.moveUp = globalMoveUp.createContextAwareInstance(this.selectedLookup);
            this.moveDown = globalMoveDown.createContextAwareInstance(this.selectedLookup);
            this.newAction = globalNewAction.createContextAwareInstance(this.selectedLookup);
        }
        catch (ClassNotFoundException cnfe) {
            LOG.log(Level.INFO, "Maybe missing openide.actions module?", cnfe);
        }
        ResourceBundle bundle = NbBundle.getBundle(IndexedEditorPanel.class);
        this.treeTableView1.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_Properties"));
        this.newButton.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_New"));
        this.deleteButton.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_Delete"));
        this.upButton.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_MoveUp"));
        this.downButton.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_MoveDown"));
        this.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_IndexedEditorPanel"));
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.updateButtonState();
    }

    public Lookup getLookup() {
        Node[] arr = this.getExplorerManager().getSelectedNodes();
        return arr.length == 1 ? arr[0].getLookup() : Lookup.EMPTY;
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.newButton = new JButton();
        this.deleteButton = new JButton();
        this.upButton = new JButton();
        this.downButton = new JButton();
        this.detailsButton = new JButton();
        this.propertiesLabel = new JLabel();
        this.jPanel2 = new JPanel();
        FormListener formListener = new FormListener();
        this.setLayout(new GridBagLayout());
        this.setBorder(BorderFactory.createEmptyBorder(12, 12, 0, 12));
        this.jPanel1.setLayout(new GridLayout(5, 1, 0, 5));
        Mnemonics.setLocalizedText((AbstractButton)this.newButton, (String)NbBundle.getBundle(IndexedEditorPanel.class).getString("CTL_New"));
        this.newButton.addActionListener(formListener);
        this.jPanel1.add(this.newButton);
        Mnemonics.setLocalizedText((AbstractButton)this.deleteButton, (String)NbBundle.getBundle(IndexedEditorPanel.class).getString("CTL_Delete"));
        this.deleteButton.addActionListener(formListener);
        this.jPanel1.add(this.deleteButton);
        Mnemonics.setLocalizedText((AbstractButton)this.upButton, (String)NbBundle.getBundle(IndexedEditorPanel.class).getString("CTL_MoveUp"));
        this.upButton.addActionListener(formListener);
        this.jPanel1.add(this.upButton);
        Mnemonics.setLocalizedText((AbstractButton)this.downButton, (String)NbBundle.getBundle(IndexedEditorPanel.class).getString("CTL_MoveDown"));
        this.downButton.addActionListener(formListener);
        this.jPanel1.add(this.downButton);
        Mnemonics.setLocalizedText((AbstractButton)this.detailsButton, (String)NbBundle.getBundle(IndexedEditorPanel.class).getString("CTL_HideDetails"));
        this.detailsButton.addActionListener(formListener);
        this.jPanel1.add(this.detailsButton);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.gridheight = -1;
        gridBagConstraints.anchor = 12;
        gridBagConstraints.insets = new Insets(0, 0, 11, 0);
        this.add((Component)this.jPanel1, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.propertiesLabel, (String)NbBundle.getBundle(IndexedEditorPanel.class).getString("CTL_Properties"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 0, 2, 11);
        this.add((Component)this.propertiesLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 11, 11);
        this.add((Component)this.jPanel2, gridBagConstraints);
    }

    private void detailsButtonActionPerformed(ActionEvent evt) {
        boolean bl = this.showingDetails = !this.showingDetails;
        if (this.showingDetails && !this.equals(this.detailsPanel.getParent())) {
            this.initDetails();
        }
        this.updateButtonState();
        this.updateDetailsPanel();
    }

    private void newButtonActionPerformed(ActionEvent evt) {
        try {
            this.getExplorerManager().setSelectedNodes(new Node[]{this.rootNode});
        }
        catch (PropertyVetoException pve) {
            PropertyDialogManager.notify(pve);
        }
        if (this.newAction != null && this.newAction.isEnabled()) {
            this.newAction.actionPerformed(evt);
        }
    }

    private void deleteButtonActionPerformed(ActionEvent evt) {
        Node[] sn = this.getExplorerManager().getSelectedNodes();
        if (sn == null || sn.length != 1 || sn[0] == this.rootNode) {
            return;
        }
        try {
            sn[0].destroy();
        }
        catch (IOException ioe) {
            PropertyDialogManager.notify(ioe);
        }
        this.rootNode = this.getExplorerManager().getRootContext();
    }

    private void downButtonActionPerformed(ActionEvent evt) {
        Node[] sn = this.getExplorerManager().getSelectedNodes();
        if (this.moveDown != null && this.moveDown.isEnabled()) {
            this.moveDown.actionPerformed(evt);
        }
        if (sn == null || sn.length != 1 || sn[0] == this.rootNode) {
            return;
        }
        try {
            this.getExplorerManager().setSelectedNodes(sn);
        }
        catch (PropertyVetoException pve) {
            // empty catch block
        }
    }

    private void upButtonActionPerformed(ActionEvent evt) {
        Node[] sn = this.getExplorerManager().getSelectedNodes();
        if (this.moveUp != null && this.moveUp.isEnabled()) {
            this.moveUp.actionPerformed(evt);
        }
        if (sn == null || sn.length != 1 || sn[0] == this.rootNode) {
            return;
        }
        try {
            this.getExplorerManager().setSelectedNodes(sn);
        }
        catch (PropertyVetoException pve) {
            // empty catch block
        }
    }

    @Override
    public synchronized ExplorerManager getExplorerManager() {
        if (this.em == null) {
            this.em = new ExplorerManager();
        }
        return this.em;
    }

    private void updateButtonState() {
        this.selectedLookup.lookup(Object.class);
        if (this.showingDetails) {
            this.detailsButton.setText(NbBundle.getMessage(IndexedEditorPanel.class, (String)"CTL_HideDetails"));
        } else {
            this.detailsButton.setText(NbBundle.getMessage(IndexedEditorPanel.class, (String)"CTL_ShowDetails"));
        }
        this.upButton.setEnabled(this.moveUp != null && this.moveUp.isEnabled());
        this.downButton.setEnabled(this.moveDown != null && this.moveDown.isEnabled());
        Node[] sn = this.getExplorerManager().getSelectedNodes();
        this.deleteButton.setEnabled(sn != null && sn.length == 1 && sn[0] != this.rootNode);
        this.detailsButton.setVisible(this.prop != null && this.prop.getPropertyEditor() != null && this.prop.getPropertyEditor().supportsCustomEditor());
        if (this.detailsButton.isVisible()) {
            if (this.showingDetails) {
                Mnemonics.setLocalizedText((AbstractButton)this.detailsButton, (String)NbBundle.getMessage(IndexedEditorPanel.class, (String)"CTL_HideDetails"));
                this.detailsButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(IndexedEditorPanel.class, (String)"ACSD_HideDetails"));
            } else {
                Mnemonics.setLocalizedText((AbstractButton)this.detailsButton, (String)NbBundle.getMessage(IndexedEditorPanel.class, (String)"CTL_ShowDetails"));
                this.detailsButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(IndexedEditorPanel.class, (String)"ACSD_ShowDetails"));
            }
            this.detailsButton.setEnabled(sn != null && sn.length == 1 && sn[0] != this.rootNode);
        }
    }

    private void updateDetailsPanel() {
        this.detailsPanel.removeAll();
        if (!this.showingDetails) {
            this.remove(this.detailsPanel);
            this.revalidateDetailsPanel();
            return;
        }
        Node[] selN = this.getExplorerManager().getSelectedNodes();
        if (selN == null || selN.length == 0) {
            this.revalidateDetailsPanel();
            return;
        }
        Object n = selN[0];
        if (n == this.rootNode) {
            this.revalidateDetailsPanel();
            return;
        }
        if (selN.length > 1) {
            n = new ProxyNode(selN);
        }
        Node.Property prop = n.getPropertySets()[0].getProperties()[0];
        PropertyPanel p = new PropertyPanel(prop);
        p.setPreferences(2);
        if (this.isEditorScrollable(p)) {
            this.detailsPanel.add((Component)p, "Center");
        } else {
            this.jScrollPane1.setViewportView(p);
            this.detailsPanel.add((Component)this.jScrollPane1, "Center");
        }
        this.revalidateDetailsPanel();
    }

    private void revalidateDetailsPanel() {
        this.detailsPanel.invalidate();
        this.repaint();
        if (this.detailsPanel.getParent() != null) {
            this.detailsPanel.getParent().validate();
        } else {
            this.detailsPanel.validate();
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("selectedNodes".equals(evt.getPropertyName())) {
            this.updateButtonState();
            this.updateDetailsPanel();
        }
    }

    private void initDetails() {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.gridheight = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 11;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.detailsPanel, gridBagConstraints);
    }

    private boolean isEditorScrollable(PropertyPanel p) {
        Component[] comps = p.getComponents();
        for (int i = 0; i < comps.length; ++i) {
            if (!(comps[i] instanceof Scrollable) && !IndexedEditorPanel.isInstanceOfTopComponent(comps[i])) continue;
            return true;
        }
        return false;
    }

    private static boolean isInstanceOfTopComponent(Object obj) {
        ClassLoader l = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        if (l == null) {
            l = IndexedEditorPanel.class.getClassLoader();
        }
        try {
            Class c = Class.forName("org.openide.windows.TopComponent", true, l);
            return c.isInstance(obj);
        }
        catch (Exception ex) {
            return false;
        }
    }

    private class FormListener
    implements ActionListener {
        private FormListener() {
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            if (evt.getSource() == IndexedEditorPanel.this.newButton) {
                IndexedEditorPanel.this.newButtonActionPerformed(evt);
            } else if (evt.getSource() == IndexedEditorPanel.this.deleteButton) {
                IndexedEditorPanel.this.deleteButtonActionPerformed(evt);
            } else if (evt.getSource() == IndexedEditorPanel.this.upButton) {
                IndexedEditorPanel.this.upButtonActionPerformed(evt);
            } else if (evt.getSource() == IndexedEditorPanel.this.downButton) {
                IndexedEditorPanel.this.downButtonActionPerformed(evt);
            } else if (evt.getSource() == IndexedEditorPanel.this.detailsButton) {
                IndexedEditorPanel.this.detailsButtonActionPerformed(evt);
            }
        }
    }

}

