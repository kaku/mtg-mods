/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.openide.explorer.propertysheet;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ActionMapUIResource;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;
import org.openide.util.NbBundle;

final class Boolean3WayEditor
implements ExPropertyEditor,
InplaceEditor.Factory {
    Boolean v = null;
    private transient List<PropertyChangeListener> propertyChangeListenerList;
    private Boolean3Inplace renderer = null;

    @Override
    public String getAsText() {
        if (this.v == null) {
            return NbBundle.getMessage(Boolean3WayEditor.class, (String)"CTL_Different_Values");
        }
        if (Boolean.TRUE.equals(this.v)) {
            return Boolean.TRUE.toString();
        }
        return Boolean.FALSE.toString();
    }

    @Override
    public Component getCustomEditor() {
        return null;
    }

    @Override
    public String getJavaInitializationString() {
        if (this.v == null) {
            return "null";
        }
        if (Boolean.TRUE.equals(this.v)) {
            return "Boolean.TRUE";
        }
        return "Boolean.FALSE";
    }

    @Override
    public String[] getTags() {
        return null;
    }

    @Override
    public Object getValue() {
        return this.v;
    }

    @Override
    public boolean isPaintable() {
        return true;
    }

    @Override
    public void paintValue(Graphics gfx, Rectangle box) {
        if (this.renderer == null) {
            this.renderer = new Boolean3Inplace();
        }
        this.renderer.setSize(box.width, box.height);
        this.renderer.doLayout();
        Graphics g = gfx.create(box.x, box.y, box.width, box.height);
        this.renderer.setOpaque(false);
        this.renderer.paint(g);
        g.dispose();
    }

    @Override
    public void setAsText(String text) {
        if (Boolean.TRUE.toString().compareToIgnoreCase(text) == 0) {
            this.setValue(Boolean.TRUE);
        } else {
            this.setValue(Boolean.FALSE);
        }
    }

    @Override
    public void setValue(Object value) {
        if (this.v != value) {
            this.v = (Boolean)value;
            this.firePropertyChange();
        }
    }

    @Override
    public boolean supportsCustomEditor() {
        return false;
    }

    @Override
    public void attachEnv(PropertyEnv env) {
        env.registerInplaceEditorFactory(this);
    }

    @Override
    public synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
        if (this.propertyChangeListenerList == null) {
            this.propertyChangeListenerList = new ArrayList<PropertyChangeListener>();
        }
        this.propertyChangeListenerList.add(listener);
    }

    @Override
    public synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
        if (this.propertyChangeListenerList != null) {
            this.propertyChangeListenerList.remove(listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void firePropertyChange() {
        List list;
        Boolean3WayEditor boolean3WayEditor = this;
        synchronized (boolean3WayEditor) {
            if (this.propertyChangeListenerList == null) {
                return;
            }
            list = (List)((ArrayList)this.propertyChangeListenerList).clone();
        }
        PropertyChangeEvent event = new PropertyChangeEvent(this, null, null, null);
        for (int i = 0; i < list.size(); ++i) {
            ((PropertyChangeListener)list.get(i)).propertyChange(event);
        }
    }

    @Override
    public InplaceEditor getInplaceEditor() {
        return new Boolean3Inplace();
    }

    class Boolean3Inplace
    extends JCheckBox
    implements InplaceEditor {
        private PropertyModel propertyModel;
        private final int NOT_SELECTED = 0;
        private final int SELECTED = 1;
        private final int DONT_CARE = 2;
        private final ButtonModel3Way model3way;

        Boolean3Inplace() {
            this.propertyModel = null;
            this.NOT_SELECTED = 0;
            this.SELECTED = 1;
            this.DONT_CARE = 2;
            super.addMouseListener(new MouseAdapter(Boolean3WayEditor.this){
                final /* synthetic */ Boolean3WayEditor val$this$0;

                @Override
                public void mousePressed(MouseEvent e) {
                    Boolean3Inplace.this.grabFocus();
                    Boolean3Inplace.this.model3way.nextState();
                }
            });
            ActionMapUIResource map = new ActionMapUIResource();
            map.put("pressed", new AbstractAction(Boolean3WayEditor.this){
                final /* synthetic */ Boolean3WayEditor val$this$0;

                @Override
                public void actionPerformed(ActionEvent e) {
                    Boolean3Inplace.this.grabFocus();
                    Boolean3Inplace.this.model3way.nextState();
                }
            });
            map.put("released", null);
            SwingUtilities.replaceUIActionMap(this, map);
            this.model3way = new ButtonModel3Way(this.getModel());
            this.setModel(this.model3way);
            this.setState(null == Boolean3WayEditor.this.v ? 2 : (Boolean3WayEditor.this.v != false ? 1 : 0));
        }

        @Override
        public void addMouseListener(MouseListener l) {
        }

        public void setState(int state) {
            this.model3way.setState(state);
        }

        public int getState() {
            return this.model3way.getState();
        }

        @Override
        public void setSelected(boolean b) {
            if (b) {
                this.setState(1);
            } else {
                this.setState(0);
            }
        }

        @Override
        public String getText() {
            return PropUtils.noCheckboxCaption ? "" : NbBundle.getMessage(Boolean3WayEditor.class, (String)"CTL_Different_Values");
        }

        @Override
        public void clear() {
            this.propertyModel = null;
        }

        @Override
        public void connect(PropertyEditor pe, PropertyEnv env) {
        }

        @Override
        public JComponent getComponent() {
            return this;
        }

        @Override
        public KeyStroke[] getKeyStrokes() {
            return null;
        }

        @Override
        public PropertyEditor getPropertyEditor() {
            return Boolean3WayEditor.this;
        }

        @Override
        public Object getValue() {
            return this.getState() == 2 ? null : (this.getState() == 1 ? Boolean.TRUE : Boolean.FALSE);
        }

        @Override
        public void reset() {
            this.setState(null == Boolean3WayEditor.this.v ? 2 : (Boolean3WayEditor.this.v != false ? 1 : 0));
        }

        @Override
        public void setValue(Object o) {
            this.setState(null == o ? 2 : ((Boolean)o != false ? 1 : 0));
        }

        @Override
        public boolean supportsTextEntry() {
            return false;
        }

        @Override
        public void setPropertyModel(PropertyModel pm) {
            this.propertyModel = pm;
        }

        @Override
        public PropertyModel getPropertyModel() {
            return this.propertyModel;
        }

        @Override
        public boolean isKnownComponent(Component c) {
            return false;
        }

        private class ButtonModel3Way
        implements ButtonModel {
            private final ButtonModel other;

            private ButtonModel3Way(ButtonModel other) {
                this.other = other;
            }

            private void setState(int state) {
                if (state == 0) {
                    this.other.setArmed(false);
                    this.setPressed(false);
                    this.setSelected(false);
                } else if (state == 1) {
                    this.other.setArmed(false);
                    this.setPressed(false);
                    this.setSelected(true);
                } else {
                    this.other.setArmed(true);
                    this.setPressed(true);
                    this.setSelected(true);
                }
            }

            private int getState() {
                if (this.isSelected() && !this.isArmed()) {
                    return 1;
                }
                if (this.isSelected() && this.isArmed()) {
                    return 2;
                }
                return 0;
            }

            private void nextState() {
                int current = this.getState();
                if (current == 0) {
                    this.setState(1);
                } else if (current == 1) {
                    this.setState(2);
                } else if (current == 2) {
                    this.setState(0);
                }
            }

            @Override
            public void setArmed(boolean b) {
            }

            @Override
            public void setEnabled(boolean b) {
                Boolean3Inplace.this.setFocusable(b);
                this.other.setEnabled(b);
            }

            @Override
            public boolean isArmed() {
                return this.other.isArmed();
            }

            @Override
            public boolean isSelected() {
                return this.other.isSelected();
            }

            @Override
            public boolean isEnabled() {
                return this.other.isEnabled();
            }

            @Override
            public boolean isPressed() {
                return this.other.isPressed();
            }

            @Override
            public boolean isRollover() {
                return this.other.isRollover();
            }

            @Override
            public void setSelected(boolean b) {
                this.other.setSelected(b);
            }

            @Override
            public void setPressed(boolean b) {
                this.other.setPressed(b);
            }

            @Override
            public void setRollover(boolean b) {
                this.other.setRollover(b);
            }

            @Override
            public void setMnemonic(int key) {
                this.other.setMnemonic(key);
            }

            @Override
            public int getMnemonic() {
                return this.other.getMnemonic();
            }

            @Override
            public void setActionCommand(String s) {
                this.other.setActionCommand(s);
            }

            @Override
            public String getActionCommand() {
                return this.other.getActionCommand();
            }

            @Override
            public void setGroup(ButtonGroup group) {
                this.other.setGroup(group);
            }

            @Override
            public void addActionListener(ActionListener l) {
                this.other.addActionListener(l);
            }

            @Override
            public void removeActionListener(ActionListener l) {
                this.other.removeActionListener(l);
            }

            @Override
            public void addItemListener(ItemListener l) {
                this.other.addItemListener(l);
            }

            @Override
            public void removeItemListener(ItemListener l) {
                this.other.removeItemListener(l);
            }

            @Override
            public void addChangeListener(ChangeListener l) {
                this.other.addChangeListener(l);
            }

            @Override
            public void removeChangeListener(ChangeListener l) {
                this.other.removeChangeListener(l);
            }

            @Override
            public Object[] getSelectedObjects() {
                return this.other.getSelectedObjects();
            }
        }

    }

}

