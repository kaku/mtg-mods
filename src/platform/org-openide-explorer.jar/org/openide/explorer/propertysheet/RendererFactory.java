/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 *  org.openide.awt.HtmlRenderer$Renderer
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.ImageUtilities
 *  org.openide.util.WeakListeners
 */
package org.openide.explorer.propertysheet;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentListener;
import java.awt.event.ContainerListener;
import java.awt.event.FocusEvent;
import java.awt.event.HierarchyBoundsListener;
import java.awt.event.HierarchyListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxEditor;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.LabelUI;
import javax.swing.plaf.basic.BasicGraphicsUtils;
import org.openide.awt.HtmlRenderer;
import org.openide.explorer.propertysheet.Boolean3WayEditor;
import org.openide.explorer.propertysheet.ButtonPanel;
import org.openide.explorer.propertysheet.CheckboxInplaceEditor;
import org.openide.explorer.propertysheet.ComboInplaceEditor;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.IconPanel;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.NodePropertyModel;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;
import org.openide.explorer.propertysheet.RadioInplaceEditor;
import org.openide.explorer.propertysheet.ReusablePropertyEnv;
import org.openide.explorer.propertysheet.ReusablePropertyModel;
import org.openide.explorer.propertysheet.StringInplaceEditor;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.WeakListeners;

final class RendererFactory {
    private static final int MAX_HTML_LENGTH = 1048576;
    private StringRenderer stringRenderer;
    private CheckboxRenderer checkboxRenderer;
    private ComboboxRenderer comboboxRenderer;
    private RadioRenderer radioRenderer;
    private TextFieldRenderer textFieldRenderer;
    private ButtonPanel buttonPanel;
    private IconPanel iconPanel;
    private final ReusablePropertyModel mdl;
    private final ReusablePropertyEnv env;
    private PropertyChangeListener activeThemeListener;
    private boolean tableUI;
    private boolean suppressButton;
    private int radioButtonMax = -1;
    private boolean useRadioBoolean = PropUtils.forceRadioButtons;
    private boolean useLabels;

    public RendererFactory(boolean tableUI, ReusablePropertyEnv env, ReusablePropertyModel mdl) {
        this.tableUI = tableUI;
        this.env = env;
        this.mdl = mdl;
        assert (mdl != null && env != null);
        ToolkitPropertyChangeListenerProxy tp = new ToolkitPropertyChangeListenerProxy();
        this.activeThemeListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                RendererFactory.this.stringRenderer = null;
                RendererFactory.this.checkboxRenderer = null;
                RendererFactory.this.comboboxRenderer = null;
                RendererFactory.this.radioRenderer = null;
                RendererFactory.this.textFieldRenderer = null;
                RendererFactory.this.buttonPanel = null;
                RendererFactory.this.iconPanel = null;
            }
        };
        tp.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.activeThemeListener, (Object)tp));
    }

    public void setRadioButtonMax(int i) {
        this.radioButtonMax = i;
    }

    public void setSuppressButton(boolean val) {
        this.suppressButton = val;
    }

    void setUseRadioBoolean(boolean val) {
        this.useRadioBoolean = val;
    }

    void setUseLabels(boolean val) {
        this.useLabels = val;
    }

    public JComponent getRenderer(Node.Property prop) {
        this.mdl.setProperty(prop);
        this.env.reset();
        PropertyEditor editor = this.preparePropertyEditor(this.mdl, this.env);
        if (editor instanceof ExceptionPropertyEditor) {
            return this.getExceptionRenderer((Exception)editor.getValue());
        }
        JComponent result = null;
        try {
            if (editor.isPaintable()) {
                result = this.prepareString(editor, this.env);
            } else {
                Class c = this.mdl.getPropertyType();
                if (c == Boolean.class || c == Boolean.TYPE) {
                    boolean useRadioRenderer;
                    boolean bl = useRadioRenderer = this.useRadioBoolean || this.env.getFeatureDescriptor().getValue("stringValues") != null;
                    result = useRadioRenderer ? this.prepareRadioButtons(editor, this.env) : this.prepareCheckbox(editor, this.env);
                } else if (editor.getTags() != null) {
                    String[] s = editor.getTags();
                    boolean editAsText = Boolean.TRUE.equals(prop.getValue("canEditAsText"));
                    result = s.length <= this.radioButtonMax && !editAsText ? this.prepareRadioButtons(editor, this.env) : this.prepareCombobox(editor, this.env);
                } else {
                    result = this.prepareString(editor, this.env);
                }
            }
            if (result != this.radioRenderer && result != this.textFieldRenderer) {
                if (result != this.checkboxRenderer && this.tableUI && !(result instanceof JComboBox)) {
                    result.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 0));
                } else if (result instanceof JComboBox && this.tableUI) {
                    result.setBorder(BorderFactory.createEmptyBorder());
                } else if (!(result instanceof JComboBox) && !(result instanceof JCheckBox)) {
                    result.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 0));
                }
            }
        }
        catch (Exception e) {
            result = this.getExceptionRenderer(e);
            Logger.getLogger(RendererFactory.class.getName()).log(Level.WARNING, null, e);
        }
        result.setEnabled(prop.canWrite());
        boolean propRequestsSuppressButton = Boolean.TRUE.equals(prop.getValue("suppressCustomEditor"));
        if (!(result instanceof JLabel || this.env.getState() != ReusablePropertyEnv.STATE_INVALID && prop.getValue("valueIcon") == null)) {
            result = this.prepareIconPanel(editor, this.env, (InplaceEditor)((Object)result));
        }
        if (editor.supportsCustomEditor() && !PropUtils.noCustomButtons && !this.suppressButton && !propRequestsSuppressButton) {
            ButtonPanel bp = this.buttonPanel();
            bp.setInplaceEditor((InplaceEditor)((Object)result));
            result = bp;
        }
        return result;
    }

    private IconPanel prepareIconPanel(PropertyEditor ed, PropertyEnv env, InplaceEditor inner) {
        IconPanel icp = this.iconPanel();
        icp.setInplaceEditor(inner);
        icp.connect(ed, env);
        return icp;
    }

    private PropertyEditor preparePropertyEditor(PropertyModel pm, PropertyEnv env) {
        PropertyEditor result;
        block11 : {
            try {
                if (pm instanceof NodePropertyModel) {
                    result = ((NodePropertyModel)pm).getPropertyEditor();
                    break block11;
                }
                if (pm instanceof ReusablePropertyModel) {
                    result = ((ReusablePropertyModel)pm).getPropertyEditor();
                    break block11;
                }
                Class c = pm.getPropertyEditorClass();
                if (c != null) {
                    try {
                        result = (PropertyEditor)c.newInstance();
                        Object mdlValue = pm.getValue();
                        Object edValue = result.getValue();
                        if (edValue != mdlValue) {
                            result.setValue(pm.getValue());
                        }
                        break block11;
                    }
                    catch (Exception e) {
                        result = new ExceptionPropertyEditor(e);
                    }
                    break block11;
                }
                result = PropUtils.getPropertyEditor(pm.getPropertyType());
                try {
                    result.setValue(pm.getValue());
                }
                catch (InvocationTargetException ite) {
                    result = new ExceptionPropertyEditor(ite);
                }
            }
            catch (Exception e) {
                result = new ExceptionPropertyEditor(e);
            }
        }
        if (result instanceof ExPropertyEditor) {
            ((ExPropertyEditor)result).attachEnv(env);
        }
        return result;
    }

    private JComponent getExceptionRenderer(Exception e) {
        ExceptionRenderer lbl = new ExceptionRenderer();
        lbl.setForeground(PropUtils.getErrorColor());
        lbl.setText(e.getMessage());
        return lbl;
    }

    public JComponent getStringRenderer() {
        StringRenderer result = this.stringRenderer();
        result.clear();
        result.setEnabled(true);
        return result;
    }

    private JComponent prepareRadioButtons(PropertyEditor editor, PropertyEnv env) {
        RadioRenderer ren = this.radioRenderer();
        ren.clear();
        ren.setUseTitle(this.useLabels);
        ren.connect(editor, env);
        return ren.getComponent();
    }

    private JComponent prepareCombobox(PropertyEditor editor, PropertyEnv env) {
        ComboboxRenderer ren = this.comboboxRenderer();
        ren.clear();
        ren.setEnabled(true);
        ren.connect(editor, env);
        return ren.getComponent();
    }

    private JComponent prepareString(PropertyEditor editor, PropertyEnv env) {
        StringRenderer ren = this.tableUI || editor.isPaintable() ? this.stringRenderer() : this.textFieldRenderer();
        ren.clear();
        ren.getComponent().setEnabled(true);
        ren.connect(editor, env);
        return ren.getComponent();
    }

    private JComponent prepareCheckbox(PropertyEditor editor, PropertyEnv env) {
        CheckboxRenderer ren = this.checkboxRenderer();
        ren.setUseTitle(this.useLabels);
        ren.clear();
        ren.setEnabled(true);
        ren.connect(editor, env);
        return ren.getComponent();
    }

    private ButtonPanel buttonPanel() {
        if (this.buttonPanel == null) {
            this.buttonPanel = new ButtonPanel();
        }
        this.buttonPanel.setEnabled(true);
        return this.buttonPanel;
    }

    private IconPanel iconPanel() {
        if (this.iconPanel == null) {
            this.iconPanel = new IconPanel();
        }
        this.iconPanel.setEnabled(true);
        return this.iconPanel;
    }

    private ComboboxRenderer comboboxRenderer() {
        if (this.comboboxRenderer == null) {
            this.comboboxRenderer = new ComboboxRenderer(this.tableUI);
            this.comboboxRenderer.setName("ComboboxRenderer for " + this.getClass().getName() + "@" + System.identityHashCode(this));
        }
        return this.comboboxRenderer;
    }

    private StringRenderer stringRenderer() {
        if (this.stringRenderer == null) {
            this.stringRenderer = new StringRenderer(this.tableUI);
            this.stringRenderer.setName("StringRenderer for " + this.getClass().getName() + "@" + System.identityHashCode(this));
        }
        return this.stringRenderer;
    }

    private CheckboxRenderer checkboxRenderer() {
        if (this.checkboxRenderer == null) {
            this.checkboxRenderer = new CheckboxRenderer();
            this.checkboxRenderer.setName("CheckboxRenderer for " + this.getClass().getName() + "@" + System.identityHashCode(this));
        }
        return this.checkboxRenderer;
    }

    private RadioRenderer radioRenderer() {
        if (this.radioRenderer == null) {
            this.radioRenderer = new RadioRenderer(this.tableUI);
            this.radioRenderer.setName("RadioRenderer for " + this.getClass().getName() + "@" + System.identityHashCode(this));
        }
        return this.radioRenderer;
    }

    private TextFieldRenderer textFieldRenderer() {
        if (this.textFieldRenderer == null) {
            this.textFieldRenderer = new TextFieldRenderer();
        }
        return this.textFieldRenderer;
    }

    private static String makeDisplayble(String str, Font f) {
        if (null == str) {
            return str;
        }
        if (null == f) {
            f = new JLabel().getFont();
        }
        StringBuffer buf = new StringBuffer(str.length() * 6);
        char[] chars = str.toCharArray();
        block7 : for (int i = 0; i < chars.length; ++i) {
            char c = chars[i];
            switch (c) {
                case '\t': {
                    buf.append("        ");
                    continue block7;
                }
                case '\n': {
                    continue block7;
                }
                case '\r': {
                    continue block7;
                }
                case '\b': {
                    buf.append("\\b");
                    continue block7;
                }
                case '\f': {
                    buf.append("\\f");
                    continue block7;
                }
                default: {
                    if (null == f || f.canDisplay(c)) {
                        buf.append(c);
                        continue block7;
                    }
                    buf.append("\\u");
                    String hex = Integer.toHexString(c);
                    for (int j = 0; j < 4 - hex.length(); ++j) {
                        buf.append('0');
                    }
                    buf.append(hex);
                }
            }
        }
        return buf.toString();
    }

    private class ExceptionRenderer
    extends JLabel
    implements InplaceEditor {
        private ExceptionRenderer() {
        }

        @Override
        public void addActionListener(ActionListener al) {
        }

        @Override
        public Color getForeground() {
            return PropUtils.getErrorColor();
        }

        @Override
        public void clear() {
        }

        @Override
        public void connect(PropertyEditor pe, PropertyEnv env) {
        }

        @Override
        public JComponent getComponent() {
            return this;
        }

        @Override
        public KeyStroke[] getKeyStrokes() {
            return null;
        }

        @Override
        public PropertyEditor getPropertyEditor() {
            return null;
        }

        @Override
        public PropertyModel getPropertyModel() {
            return null;
        }

        @Override
        public Object getValue() {
            return this.getText();
        }

        @Override
        public boolean isKnownComponent(Component c) {
            return c == this;
        }

        @Override
        public void removeActionListener(ActionListener al) {
        }

        @Override
        public void reset() {
        }

        @Override
        public void setPropertyModel(PropertyModel pm) {
        }

        @Override
        public void setValue(Object o) {
        }

        @Override
        public boolean supportsTextEntry() {
            return false;
        }
    }

    private static final class ExceptionPropertyEditor
    implements PropertyEditor {
        Exception e;

        public ExceptionPropertyEditor(Exception e) {
            this.e = e;
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener listener) {
        }

        @Override
        public String getAsText() {
            return this.e.getMessage();
        }

        @Override
        public Component getCustomEditor() {
            return null;
        }

        @Override
        public String getJavaInitializationString() {
            return null;
        }

        @Override
        public String[] getTags() {
            return null;
        }

        @Override
        public Object getValue() {
            return this.e;
        }

        @Override
        public boolean isPaintable() {
            return false;
        }

        @Override
        public void paintValue(Graphics gfx, Rectangle box) {
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener listener) {
        }

        @Override
        public void setAsText(String text) throws IllegalArgumentException {
        }

        @Override
        public void setValue(Object value) {
        }

        @Override
        public boolean supportsCustomEditor() {
            return false;
        }
    }

    private static final class RadioRenderer
    extends RadioInplaceEditor {
        private boolean needLayout = true;

        public RadioRenderer(boolean tableUI) {
            super(tableUI);
        }

        @Override
        public void connect(PropertyEditor pe, PropertyEnv env) {
            super.connect(pe, env);
            this.needLayout = true;
        }

        @Override
        public void paint(Graphics g) {
            if (this.needLayout) {
                this.getLayout().layoutContainer(this);
                this.needLayout = false;
            }
            super.paint(g);
            this.clear();
        }

        @Override
        protected RadioInplaceEditor.InvRadioButton createButton() {
            return new NoEventsInvRadioButton();
        }

        @Override
        protected void configureButton(RadioInplaceEditor.InvRadioButton ire, String txt) {
            if (this.editor.getTags().length == 1) {
                ire.setEnabled(false);
            } else {
                ire.setEnabled(this.isEnabled());
            }
            ire.setText(txt);
            ire.setForeground(this.getForeground());
            ire.setBackground(this.getBackground());
            ire.setFont(this.getFont());
            if (txt.equals(this.editor.getAsText())) {
                ire.setSelected(true);
            } else {
                ire.setSelected(false);
            }
        }

        @Override
        public void addContainerListener(ContainerListener cl) {
        }

        public void addChangeListener(ChangeListener cl) {
        }

        @Override
        public void addComponentListener(ComponentListener l) {
        }

        @Override
        public void addHierarchyBoundsListener(HierarchyBoundsListener hbl) {
        }

        @Override
        public void addHierarchyListener(HierarchyListener hl) {
        }

        @Override
        protected void firePropertyChange(String name, Object old, Object nue) {
        }

        @Override
        public void firePropertyChange(String name, boolean old, boolean nue) {
        }

        @Override
        public void firePropertyChange(String name, int old, int nue) {
        }

        @Override
        public void firePropertyChange(String name, byte old, byte nue) {
        }

        @Override
        public void firePropertyChange(String name, char old, char nue) {
        }

        @Override
        public void firePropertyChange(String name, double old, double nue) {
        }

        @Override
        public void firePropertyChange(String name, float old, float nue) {
        }

        @Override
        public void firePropertyChange(String name, short old, short nue) {
        }

        private class NoEventsInvRadioButton
        extends RadioInplaceEditor.InvRadioButton {
            private NoEventsInvRadioButton() {
                super((RadioInplaceEditor)RadioRenderer.this);
            }

            @Override
            public void addActionListener(ActionListener al) {
            }

            @Override
            public void addContainerListener(ContainerListener cl) {
            }

            @Override
            public void addChangeListener(ChangeListener cl) {
            }

            @Override
            public void addComponentListener(ComponentListener l) {
            }

            @Override
            public void addHierarchyBoundsListener(HierarchyBoundsListener hbl) {
            }

            @Override
            public void addHierarchyListener(HierarchyListener hl) {
            }

            @Override
            protected void fireActionPerformed(ActionEvent ae) {
            }

            @Override
            protected void fireStateChanged() {
            }

            @Override
            protected void firePropertyChange(String name, Object old, Object nue) {
            }

            @Override
            public void firePropertyChange(String name, boolean old, boolean nue) {
            }

            @Override
            public void firePropertyChange(String name, int old, int nue) {
            }

            @Override
            public void firePropertyChange(String name, byte old, byte nue) {
            }

            @Override
            public void firePropertyChange(String name, char old, char nue) {
            }

            @Override
            public void firePropertyChange(String name, double old, double nue) {
            }

            @Override
            public void firePropertyChange(String name, float old, float nue) {
            }

            @Override
            public void firePropertyChange(String name, short old, short nue) {
            }
        }

    }

    private static final class TextFieldRenderer
    extends StringInplaceEditor {
        private TextFieldRenderer() {
        }

        @Override
        public void paintComponent(Graphics g) {
            this.setEnabled(PropUtils.checkEnabled(this, this.editor, this.env));
            super.paintComponent(g);
            this.clear();
        }

        protected void fireActionPerformed(ActionEvent ae) {
        }

        protected void fireStateChanged() {
        }

        @Override
        protected void firePropertyChange(String name, Object old, Object nue) {
            boolean fire;
            boolean bl = fire = "locale".equals(name) || "document".equals(name);
            if (fire) {
                super.firePropertyChange(name, old, nue);
            }
        }
    }

    private static final class StringRenderer
    extends JLabel
    implements InplaceEditor {
        private PropertyEditor editor = null;
        private PropertyEnv env = null;
        private boolean tableUI = false;
        private boolean enabled = true;
        private JLabel htmlLabel = HtmlRenderer.createLabel();
        private JLabel noHtmlLabel = new JLabel();
        private Object value = null;
        private static final boolean isGTK = "GTK".equals(UIManager.getLookAndFeel().getID());
        private static Insets paintViewInsets = new Insets(0, 0, 0, 0);
        private static Rectangle paintIconR = new Rectangle();
        private static Rectangle paintTextR = new Rectangle();
        private static Rectangle paintViewR = new Rectangle();

        public StringRenderer(boolean tableUI) {
            this.tableUI = tableUI;
            this.setOpaque(true);
            ((HtmlRenderer.Renderer)this.htmlLabel).setRenderStyle(1);
        }

        @Override
        public void setEnabled(boolean val) {
            this.enabled = val;
        }

        @Override
        public void setText(String s) {
            if (s != null) {
                if (s.length() > 512) {
                    super.setText(RendererFactory.makeDisplayble(s.substring(0, 512), this.getFont()));
                } else {
                    super.setText(RendererFactory.makeDisplayble(s, this.getFont()));
                }
            } else {
                super.setText("");
            }
        }

        @Override
        public boolean isEnabled() {
            return this.enabled;
        }

        @Override
        protected void firePropertyChange(String name, Object old, Object nue) {
        }

        @Override
        public void validate() {
        }

        @Override
        public void invalidate() {
        }

        @Override
        public void revalidate() {
        }

        @Override
        public void repaint() {
        }

        @Override
        public void repaint(long tm, int x, int y, int w, int h) {
        }

        @Override
        public Dimension getPreferredSize() {
            if (this.getText().length() > 1024) {
                return new Dimension(4196, PropUtils.getMinimumPropPanelHeight());
            }
            Dimension result = super.getPreferredSize();
            result.width = Math.max(result.width, PropUtils.getMinimumPropPanelWidth());
            result.height = Math.max(result.height, PropUtils.getMinimumPropPanelHeight());
            return result;
        }

        @Override
        public void paint(Graphics g) {
            if (this.editor != null) {
                this.setEnabled(PropUtils.checkEnabled(this, this.editor, this.env));
            }
            if (this.editor instanceof ExceptionPropertyEditor) {
                this.setForeground(PropUtils.getErrorColor());
            }
            if (this.editor != null && this.editor.isPaintable()) {
                this.delegatedPaint(g);
            } else {
                String htmlDisplayValue = this.env == null ? null : (String)this.env.getFeatureDescriptor().getValue("htmlDisplayValue");
                boolean htmlValueUsed = htmlDisplayValue != null;
                JLabel lbl = htmlValueUsed ? this.htmlLabel : this.noHtmlLabel;
                String text = htmlValueUsed ? htmlDisplayValue : this.getText();
                text = text == null ? "" : RendererFactory.makeDisplayble(text, this.getFont());
                if (htmlValueUsed) {
                    ((HtmlRenderer.Renderer)lbl).setHtml(text.length() <= 1048576);
                }
                lbl.setFont(this.getFont());
                lbl.setEnabled(this.isEnabled());
                lbl.setText(text);
                if (!htmlValueUsed) {
                    lbl.putClientProperty("html", null);
                }
                lbl.setIcon(this.getIcon());
                lbl.setIconTextGap(this.getIconTextGap());
                lbl.setBounds(this.getBounds());
                lbl.setOpaque(true);
                lbl.setBackground(this.getBackground());
                lbl.setForeground(this.getForeground());
                lbl.setBorder(this.getBorder());
                if ((isGTK || "com.sun.java.swing.plaf.windows.WindowsLabelUI".equals(lbl.getUI().getClass().getName())) && !this.isEnabled() && !htmlValueUsed) {
                    g.setColor(lbl.getBackground());
                    g.fillRect(0, 0, lbl.getWidth(), lbl.getHeight());
                    g.setColor(lbl.getForeground());
                    Icon icon = lbl.isEnabled() ? lbl.getIcon() : lbl.getDisabledIcon();
                    FontMetrics fm = g.getFontMetrics();
                    Insets insets = lbl.getInsets(paintViewInsets);
                    StringRenderer.paintViewR.x = insets.left;
                    StringRenderer.paintViewR.y = insets.top;
                    StringRenderer.paintViewR.width = lbl.getWidth() - (insets.left + insets.right);
                    StringRenderer.paintViewR.height = lbl.getHeight() - (insets.top + insets.bottom);
                    StringRenderer.paintIconR.height = 0;
                    StringRenderer.paintIconR.width = 0;
                    StringRenderer.paintIconR.y = 0;
                    StringRenderer.paintIconR.x = 0;
                    StringRenderer.paintTextR.height = 0;
                    StringRenderer.paintTextR.width = 0;
                    StringRenderer.paintTextR.y = 0;
                    StringRenderer.paintTextR.x = 0;
                    String clippedText = SwingUtilities.layoutCompoundLabel(lbl, fm, text, icon, lbl.getVerticalAlignment(), lbl.getHorizontalAlignment(), lbl.getVerticalTextPosition(), lbl.getHorizontalTextPosition(), paintViewR, paintIconR, paintTextR, lbl.getIconTextGap());
                    if (icon != null) {
                        icon.paintIcon(lbl, g, StringRenderer.paintIconR.x, StringRenderer.paintIconR.y);
                    }
                    int textX = StringRenderer.paintTextR.x;
                    int textY = StringRenderer.paintTextR.y + fm.getAscent();
                    int mnemonicIndex = lbl.getDisplayedMnemonicIndex();
                    Color fg = lbl.getForeground();
                    if (isGTK && null == (fg = UIManager.getColor("Tree.textForeground"))) {
                        fg = Color.BLACK;
                    }
                    Color changedForeground = fg.brighter();
                    if (Color.BLACK.equals(fg)) {
                        changedForeground = Color.GRAY;
                    }
                    g.setColor(changedForeground);
                    BasicGraphicsUtils.drawStringUnderlineCharAt(g, clippedText, mnemonicIndex, textX, textY);
                } else {
                    lbl.paint(g);
                }
            }
            this.clear();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void delegatedPaint(Graphics g) {
            Color c = g.getColor();
            try {
                g.setColor(this.getBackground());
                g.fillRect(0, 0, this.getWidth(), this.getHeight());
                g.setColor(this.getForeground());
                if (!this.tableUI) {
                    Border b = BorderFactory.createBevelBorder(1);
                    b.paintBorder(this, g, 0, 0, this.getWidth(), this.getHeight());
                }
                Rectangle r = this.getBounds();
                int n = this.getWidth() > 16 ? (this.editor instanceof Boolean3WayEditor ? 0 : 3) : (r.x = 0);
                r.width -= this.getWidth() > 16 ? (this.editor instanceof Boolean3WayEditor ? 0 : 3) : 0;
                r.y = 0;
                this.editor.paintValue(g, r);
            }
            finally {
                g.setColor(c);
            }
        }

        @Override
        public void clear() {
            this.editor = null;
            this.env = null;
            this.setIcon(null);
            this.setOpaque(true);
        }

        @Override
        public void setValue(Object o) {
            this.value = o;
            this.setText(this.value instanceof String ? (String)this.value : (this.value != null ? this.value.toString() : null));
        }

        @Override
        public void connect(PropertyEditor p, PropertyEnv env) {
            this.editor = p;
            this.env = env;
            this.reset();
        }

        @Override
        public JComponent getComponent() {
            return this;
        }

        @Override
        public KeyStroke[] getKeyStrokes() {
            return null;
        }

        @Override
        public PropertyEditor getPropertyEditor() {
            return this.editor;
        }

        @Override
        public PropertyModel getPropertyModel() {
            return null;
        }

        @Override
        public Object getValue() {
            return this.getText();
        }

        public void handleInitialInputEvent(InputEvent e) {
        }

        @Override
        public boolean isKnownComponent(Component c) {
            return false;
        }

        @Override
        public void removeActionListener(ActionListener al) {
        }

        @Override
        public void reset() {
            this.setText(this.editor.getAsText());
            Image i = null;
            if (this.env != null) {
                if (this.env.getState() == PropertyEnv.STATE_INVALID) {
                    this.setForeground(PropUtils.getErrorColor());
                    i = ImageUtilities.loadImage((String)"org/openide/resources/propertysheet/invalid.gif");
                } else {
                    Object o;
                    FeatureDescriptor fd = this.env.getFeatureDescriptor();
                    Object object = o = fd == null ? null : fd.getValue("valueIcon");
                    if (o instanceof Icon) {
                        this.setIcon((Icon)o);
                    } else if (o instanceof Image) {
                        i = (Image)o;
                    }
                }
            }
            if (i != null) {
                this.setIcon(new ImageIcon(i));
            }
        }

        @Override
        public void setPropertyModel(PropertyModel pm) {
        }

        @Override
        public boolean supportsTextEntry() {
            return false;
        }

        protected void fireActionPerformed(ActionEvent ae) {
        }

        protected void fireStateChanged() {
        }

        @Override
        public void addActionListener(ActionListener al) {
        }
    }

    private static final class CheckboxRenderer
    extends CheckboxInplaceEditor {
        private CheckboxRenderer() {
        }

        @Override
        public void paintComponent(Graphics g) {
            this.setEnabled(PropUtils.checkEnabled(this, this.editor, this.env));
            super.paintComponent(g);
            this.clear();
        }

        @Override
        protected void fireActionPerformed(ActionEvent ae) {
        }

        @Override
        protected void fireStateChanged() {
        }

        @Override
        protected void firePropertyChange(String name, Object old, Object nue) {
            if ("foreground".equals(name) || "background".equals(name) || "font".equals(name)) {
                super.firePropertyChange(name, old, nue);
            }
        }

        @Override
        public void firePropertyChange(String name, boolean old, boolean nue) {
        }

        @Override
        public void firePropertyChange(String name, int old, int nue) {
        }

        @Override
        public void firePropertyChange(String name, byte old, byte nue) {
        }

        @Override
        public void firePropertyChange(String name, char old, char nue) {
        }

        @Override
        public void firePropertyChange(String name, double old, double nue) {
        }

        @Override
        public void firePropertyChange(String name, float old, float nue) {
        }

        @Override
        public void firePropertyChange(String name, short old, short nue) {
        }
    }

    static final class ComboboxRenderer
    extends ComboInplaceEditor {
        private Object item = null;
        boolean editable = false;

        public ComboboxRenderer(boolean tableUI) {
            super(tableUI);
        }

        @Override
        public boolean isEditable() {
            return false;
        }

        @Override
        public void paintComponent(Graphics g) {
            this.setEnabled(this.isEnabled() && this.env.isEditable());
            this.doLayout();
            super.paintComponent(g);
            this.clear();
        }

        @Override
        public void clear() {
            super.clear();
            this.item = null;
        }

        @Override
        public void setSelectedItem(Object o) {
            this.item = o;
            if (this.item == null && this.editor != null && this.editor.getTags().length > 0) {
                this.item = this.editor.getTags()[0];
            }
            if (this.editable) {
                this.getEditor().setItem(this.getSelectedItem());
            }
        }

        @Override
        public Object getSelectedItem() {
            return this.item;
        }

        @Override
        public void installAncestorListener() {
        }

        @Override
        public void processFocusEvent(FocusEvent fe) {
        }

        @Override
        public void processMouseEvent(MouseEvent me) {
        }

        @Override
        public void addActionListener(ActionListener ae) {
        }

        protected void fireActionPerformed(ActionEvent ae) {
        }

        protected void fireStateChanged() {
        }

        @Override
        protected void firePropertyChange(String name, Object old, Object nue) {
            super.firePropertyChange(name, old, nue);
        }
    }

    private static class ToolkitPropertyChangeListenerProxy
    implements PropertyChangeListener {
        private PropertyChangeListener l;

        public void addPropertyChangeListener(PropertyChangeListener l) {
            this.l = l;
            Toolkit.getDefaultToolkit().addPropertyChangeListener("win.xpstyle.themeActive", this);
        }

        public void removePropertyChangeListener(PropertyChangeListener l) {
            if (this.l == l) {
                Toolkit.getDefaultToolkit().removePropertyChangeListener("win.xpstyle.themeActive", this);
            }
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            this.l.propertyChange(evt);
        }
    }

}

