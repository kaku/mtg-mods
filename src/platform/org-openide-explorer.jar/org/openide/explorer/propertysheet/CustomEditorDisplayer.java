/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.openide.explorer.propertysheet;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.beans.PropertyEditorManager;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.EventListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JScrollBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import org.openide.explorer.propertysheet.EditorPropertyDisplayer;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.NodePropertyModel;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertyDialogManager;
import org.openide.explorer.propertysheet.PropertyDisplayer_Editable;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;
import org.openide.explorer.propertysheet.ProxyNode;
import org.openide.explorer.propertysheet.editors.EnhancedCustomPropertyEditor;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

final class CustomEditorDisplayer
implements PropertyDisplayer_Editable {
    private int updatePolicy = 0;
    private Node.Property prop;
    private PropertyEnv env = null;
    private PropertyEditor editor = null;
    private Component customEditor = null;
    boolean ignoreChanges = false;
    private PropertyChangeListener editorListener = null;
    private EnvListener envListener = null;
    private PropertyModel model = null;
    private Object originalValue = null;
    private EventListenerList listenerList = null;
    private boolean ignoreChanges2 = false;
    private PropertyChangeListener remoteEnvListener = null;
    private VetoableChangeListener remotevEnvListener = null;

    public CustomEditorDisplayer(Node.Property prop) {
        this.prop = prop;
    }

    public CustomEditorDisplayer(Node.Property prop, PropertyModel mdl) {
        this(prop);
        this.model = mdl;
    }

    @Override
    public void setUpdatePolicy(int i) {
        this.updatePolicy = i;
        if (this.env != null) {
            this.env.setChangeImmediate(i != 2);
        }
    }

    private Component getCustomEditor() {
        if (this.customEditor == null) {
            this.customEditor = this.getPropertyEditor().getCustomEditor();
        }
        return this.customEditor;
    }

    PropertyEditor getPropertyEditor() {
        if (this.editor == null) {
            this.setPropertyEditor(PropUtils.getPropertyEditor(this.getProperty()));
        }
        return this.editor;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void setPropertyEditor(PropertyEditor editor) {
        block8 : {
            if (this.editor != null) {
                this.detachFromPropertyEditor(this.editor);
                this.ignoreChanges = true;
            }
            this.editor = editor;
            try {
                if (editor == null) break block8;
                if (!editor.supportsCustomEditor()) {
                    Class type = this.prop.getValueType();
                    throw new IllegalArgumentException("#177688: property editor " + editor + " for property " + (Object)this.prop + " does not support a custom editor; valueType=" + type.getName() + "; PropertyEditorManager says: " + PropertyEditorManager.findEditor(type) + "; search path is: " + Arrays.toString(PropertyEditorManager.getEditorSearchPath()) + "; CCL: " + Thread.currentThread().getContextClassLoader());
                }
                try {
                    this.originalValue = editor.getValue();
                }
                catch (Exception e) {
                    // empty catch block
                }
                PropertyEnv env = new PropertyEnv();
                env.setFeatureDescriptor(EditorPropertyDisplayer.findFeatureDescriptor(this));
                this.setPropertyEnv(env);
                if (editor instanceof ExPropertyEditor) {
                    ((ExPropertyEditor)editor).attachEnv(env);
                }
                this.attachToPropertyEditor(editor);
            }
            finally {
                this.ignoreChanges = false;
            }
        }
    }

    private void setPropertyEnv(PropertyEnv env) {
        if (this.env != null) {
            this.detachFromEnv(this.env);
        }
        this.env = env;
        if (env != null) {
            env.setChangeImmediate(this.getUpdatePolicy() != 2);
            this.attachToEnv(env);
        }
    }

    private void attachToEnv(PropertyEnv env) {
        env.addPropertyChangeListener(this.getEnvListener());
        env.addVetoableChangeListener(this.getEnvListener());
        env.setBeans(EditorPropertyDisplayer.findBeans(this));
    }

    private void detachFromEnv(PropertyEnv env) {
        env.removePropertyChangeListener(this.getEnvListener());
        env.removeVetoableChangeListener(this.getEnvListener());
    }

    private void attachToPropertyEditor(PropertyEditor editor) {
        editor.addPropertyChangeListener(this.getEditorListener());
    }

    private void detachFromPropertyEditor(PropertyEditor editor) {
        editor.removePropertyChangeListener(this.getEditorListener());
    }

    private PropertyChangeListener getEditorListener() {
        if (this.editorListener == null) {
            this.editorListener = new EditorListener();
        }
        return this.editorListener;
    }

    private EnvListener getEnvListener() {
        if (this.envListener == null) {
            this.envListener = new EnvListener();
        }
        return this.envListener;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean commit() throws IllegalArgumentException {
        try {
            this.ignoreChanges = true;
            PropertyEditor editor = this.getPropertyEditor();
            Object entered = this.getEnteredValue();
            try {
                if (entered != null && entered.equals(this.getProperty().getValue())) {
                    boolean bl = false;
                    return bl;
                }
            }
            catch (Exception e) {
                Logger.getLogger(CustomEditorDisplayer.class.getName()).log(Level.WARNING, null, e);
                try {
                    if (this.getProperty().canRead()) {
                        editor.setValue(this.model.getValue());
                    }
                }
                catch (ProxyNode.DifferentValuesException dve) {
                }
                catch (Exception ex) {
                    PropertyDialogManager.notify(ex);
                }
                boolean ex = false;
                return ex;
            }
            PropertyEnv env = this.getPropertyEnv();
            Exception exception = PropUtils.updatePropertyEditor(editor, entered);
            if (exception == null && env != null && PropertyEnv.STATE_NEEDS_VALIDATION.equals(env.getState())) {
                String msg = env.silentlySetState(PropertyEnv.STATE_VALID, entered);
                System.err.println("  result of silent set state: " + msg);
                if (msg != null && !PropertyEnv.STATE_VALID.equals(env.getState())) {
                    IllegalArgumentException iae = new IllegalArgumentException("Error setting value");
                    Exceptions.attachLocalizedMessage((Throwable)iae, (String)msg);
                    if (!PropertyEnv.STATE_INVALID.equals(env.getState())) {
                        env.silentlySetState(PropertyEnv.STATE_INVALID, null);
                    }
                    throw iae;
                }
            }
            Object res = Boolean.FALSE;
            if (exception == null) {
                res = PropUtils.noDlgUpdateProp(this.getModel(), editor);
                this.originalValue = editor.getValue();
                if (res instanceof Exception && !(res instanceof ProxyNode.DifferentValuesException)) {
                    exception = (Exception)res;
                }
                if (res instanceof InvocationTargetException || res instanceof IllegalAccessException) {
                    PropertyDialogManager.notify((Exception)res);
                }
            }
            if (exception != null) {
                if (exception instanceof IllegalArgumentException) {
                    throw (IllegalArgumentException)exception;
                }
                PropertyDialogManager.notify(exception);
                IllegalArgumentException iae = new IllegalArgumentException("Error setting value");
                Exceptions.attachLocalizedMessage((Throwable)iae, (String)PropUtils.findLocalizedMessage(exception, entered, this.getProperty().getDisplayName()));
                throw iae;
            }
            boolean result = Boolean.TRUE.equals(res);
            if (result) {
                this.fireActionPerformed();
            }
            boolean bl = result;
            return bl;
        }
        finally {
            this.ignoreChanges = false;
        }
    }

    PropertyModel getModel() {
        if (this.model == null) {
            return new NodePropertyModel(this.getProperty(), null);
        }
        return this.model;
    }

    void setModel(PropertyModel mdl) {
        this.model = mdl;
    }

    @Override
    public PropertyEnv getPropertyEnv() {
        return this.env;
    }

    @Override
    public Component getComponent() {
        return this.getCustomEditor();
    }

    @Override
    public Object getEnteredValue() {
        PropertyEditor editor = this.getPropertyEditor();
        Object result = this.customEditor instanceof EnhancedCustomPropertyEditor ? ((EnhancedCustomPropertyEditor)((Object)this.customEditor)).getPropertyValue() : editor.getValue();
        return result;
    }

    @Override
    public Node.Property getProperty() {
        return this.prop;
    }

    @Override
    public int getUpdatePolicy() {
        return this.updatePolicy;
    }

    @Override
    public String isModifiedValueLegal() {
        boolean legal = true;
        String msg = null;
        PropertyEditor editor = this.getPropertyEditor();
        if (this.env != null) {
            legal = this.env.getState() != PropertyEnv.STATE_INVALID;
            System.err.println(" Attempting to validate env");
            if (legal && PropertyEnv.STATE_NEEDS_VALIDATION.equals(this.env.getState())) {
                msg = this.env.silentlySetState(PropertyEnv.STATE_VALID, this.getEnteredValue());
                legal = msg == null;
            }
        } else if (editor instanceof EnhancedCustomPropertyEditor) {
            Object entered = ((EnhancedCustomPropertyEditor)((Object)editor)).getPropertyValue();
            try {
                editor.setValue(entered);
            }
            catch (IllegalStateException ise) {
                legal = false;
                msg = PropUtils.findLocalizedMessage(ise, entered, this.getProperty().getDisplayName());
            }
        }
        if (!legal && msg == null) {
            msg = NbBundle.getMessage(CustomEditorDisplayer.class, (String)"FMT_CannotUpdateProperty", (Object)editor.getValue(), (Object)this.getProperty().getDisplayName());
        }
        return msg;
    }

    @Override
    public boolean isValueModified() {
        boolean result;
        PropertyEditor editor = this.getPropertyEditor();
        boolean bl = result = editor.getValue() != this.originalValue;
        if (!result && editor instanceof EnhancedCustomPropertyEditor) {
            Object entered = ((EnhancedCustomPropertyEditor)((Object)editor)).getPropertyValue();
            result = entered != null ? entered.equals(this.originalValue) : this.originalValue == null;
        }
        return result;
    }

    @Override
    public void refresh() {
    }

    @Override
    public void reset() {
        try {
            this.originalValue = this.getProperty().getValue();
            this.getPropertyEditor().setValue(this.originalValue);
        }
        catch (Exception e) {
            Logger.getLogger(CustomEditorDisplayer.class.getName()).log(Level.WARNING, null, e);
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        Container custEditor = (Container)this.getComponent();
        if (custEditor instanceof Container) {
            this.setEnabled(custEditor, enabled);
        }
        custEditor.setEnabled(enabled);
    }

    public void setEnabled(Container c, boolean enabled) {
        Component[] comp = c.getComponents();
        for (int i = 0; i < comp.length; ++i) {
            Boolean val;
            if (!(comp[i] instanceof JScrollBar)) {
                comp[i].setEnabled(false);
            } else {
                ((JScrollBar)comp[i]).setFocusable(enabled);
            }
            if (!(comp[i] instanceof Container)) continue;
            boolean ignore = false;
            if (comp[i] instanceof JComponent && (val = (Boolean)((JComponent)comp[i]).getClientProperty("dontEnableMe")) != null) {
                ignore = val;
            }
            if (ignore) continue;
            this.setEnabled((Container)comp[i], enabled);
        }
        c.setEnabled(enabled);
    }

    @Override
    public void setEnteredValue(Object o) {
        PropUtils.updatePropertyEditor(this.getPropertyEditor(), o);
    }

    @Override
    public void setActionCommand(String val) {
    }

    @Override
    public String getActionCommand() {
        return null;
    }

    @Override
    public synchronized void addActionListener(ActionListener listener) {
        if (this.listenerList == null) {
            this.listenerList = new EventListenerList();
        }
        this.listenerList.add(ActionListener.class, listener);
    }

    @Override
    public synchronized void removeActionListener(ActionListener listener) {
        this.listenerList.remove(ActionListener.class, listener);
    }

    private void fireActionPerformed() {
        ActionEvent event = new ActionEvent(this, 1001, "userChangedValue");
        if (this.listenerList == null) {
            return;
        }
        Object[] listeners = this.listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] != ActionListener.class) continue;
            ((ActionListener)listeners[i + 1]).actionPerformed(event);
        }
    }

    @Override
    public synchronized void addChangeListener(ChangeListener listener) {
        if (this.listenerList == null) {
            this.listenerList = new EventListenerList();
        }
        this.listenerList.add(ChangeListener.class, listener);
    }

    @Override
    public synchronized void removeChangeListener(ChangeListener listener) {
        this.listenerList.remove(ChangeListener.class, listener);
    }

    private void fireStateChanged() {
        ChangeEvent event = new ChangeEvent(this);
        if (this.listenerList == null) {
            return;
        }
        Object[] listeners = this.listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] != ChangeListener.class) continue;
            ((ChangeListener)listeners[i + 1]).stateChanged(event);
        }
    }

    void setRemoteEnvListener(PropertyChangeListener l) {
        this.remoteEnvListener = l;
    }

    void setRemoteEnvVetoListener(VetoableChangeListener vl) {
        this.remotevEnvListener = vl;
    }

    private class EditorListener
    implements PropertyChangeListener {
        private EditorListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (CustomEditorDisplayer.this.ignoreChanges) {
                return;
            }
            if ("propertyValueValid".equals(evt.getPropertyName())) {
                return;
            }
            if (CustomEditorDisplayer.this.ignoreChanges2) {
                return;
            }
            CustomEditorDisplayer.this.ignoreChanges2 = true;
            if (CustomEditorDisplayer.this.getUpdatePolicy() != 2) {
                CustomEditorDisplayer.this.commit();
            }
            CustomEditorDisplayer.this.fireStateChanged();
            CustomEditorDisplayer.this.ignoreChanges2 = false;
        }
    }

    private class EnvListener
    implements PropertyChangeListener,
    VetoableChangeListener {
        private EnvListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            CustomEditorDisplayer.this.fireStateChanged();
            if (CustomEditorDisplayer.this.remoteEnvListener != null) {
                CustomEditorDisplayer.this.remoteEnvListener.propertyChange(evt);
            }
        }

        @Override
        public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
            if (CustomEditorDisplayer.this.remotevEnvListener != null) {
                CustomEditorDisplayer.this.remotevEnvListener.vetoableChange(evt);
            }
        }
    }

}

