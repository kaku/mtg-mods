/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.openide.explorer.propertysheet;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.io.PrintStream;
import java.lang.ref.WeakReference;
import java.util.EventListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JRootPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import javax.swing.table.TableCellEditor;
import javax.swing.text.JTextComponent;
import org.openide.explorer.propertysheet.ButtonPanel;
import org.openide.explorer.propertysheet.CustomEditorAction;
import org.openide.explorer.propertysheet.EditorPropertyDisplayer;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.ExPropertyModel;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.ModelProperty;
import org.openide.explorer.propertysheet.NodePropertyModel;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertyDialogManager;
import org.openide.explorer.propertysheet.PropertyDisplayer_Editable;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;
import org.openide.explorer.propertysheet.ProxyNode;
import org.openide.explorer.propertysheet.ReusablePropertyEnv;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

class EditablePropertyDisplayer
extends EditorPropertyDisplayer
implements PropertyDisplayer_Editable {
    private static final Object NO_VALUE = new Object();
    private int updatePolicy = 0;
    private String actionCommand = "enterPressed";
    private EnvListener envListener = null;
    private EventListenerList listenerList = null;
    private int actionListenerCount = 0;
    private InplaceEditorListener ieListener = null;
    private Object cachedInitialValue = NO_VALUE;
    private Action customEditorAction = null;
    boolean customEditorIsOpening = false;
    private PropertyEditor editor = null;
    private PropertyEnv attachedEnv = null;
    private Object lastKnownState = null;
    private PropertyChangeListener remoteEnvListener = null;
    private VetoableChangeListener remotevEnvListener = null;

    public EditablePropertyDisplayer(Node.Property p) {
        super(p, null);
    }

    EditablePropertyDisplayer(Node.Property p, PropertyModel mdl) {
        super(p, mdl);
    }

    @Override
    public void setEnabled(boolean b) {
        super.setEnabled(b);
        if (this.customEditorAction != null) {
            this.customEditorAction.setEnabled(b);
        }
    }

    @Override
    public boolean commit() throws IllegalArgumentException {
        boolean result;
        try {
            result = this._commit();
        }
        catch (IllegalArgumentException iae) {
            result = false;
            if (this.getUpdatePolicy() != 2) {
                PropertyDialogManager.notify(iae);
            }
            throw iae;
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean _commit() throws IllegalArgumentException {
        InplaceEditor ine = this.getInplaceEditor();
        PropertyEditor editor = ine == null ? PropUtils.getPropertyEditor(this.getProperty()) : ine.getPropertyEditor();
        PropertyEnv env = this.getPropertyEnv();
        PropertyEnv tempEnv = null;
        if (env != null) {
            tempEnv = new PropertyEnv();
            this.detachFromEnv(env);
            tempEnv.setFeatureDescriptor(EditablePropertyDisplayer.findFeatureDescriptor(this));
            if (editor instanceof ExPropertyEditor) {
                ((ExPropertyEditor)editor).attachEnv(tempEnv);
            }
        }
        boolean success = false;
        try {
            String msg;
            Object result = PropUtils.updatePropertyEditor(this.getPropertyEditor(), this.getEnteredValue());
            if (result == null && editor instanceof ExPropertyEditor && PropertyEnv.STATE_NEEDS_VALIDATION.equals(tempEnv.getState()) && (msg = tempEnv.silentlySetState(PropertyEnv.STATE_VALID, this.getEnteredValue())) != null && !PropertyEnv.STATE_VALID.equals(env.getState())) {
                IllegalArgumentException exc = new IllegalArgumentException("Error setting value");
                Exceptions.attachLocalizedMessage((Throwable)exc, (String)msg);
                throw exc;
            }
            if (result == null) {
                result = PropUtils.noDlgUpdateProp(ine.getPropertyModel(), editor);
            }
            if (result instanceof Exception) {
                Object e = result;
                if (!(e instanceof IllegalArgumentException)) {
                    String msg2 = PropUtils.findLocalizedMessage((Throwable)e, this.getEnteredValue(), this.getProperty().getDisplayName());
                    IllegalArgumentException iae = new IllegalArgumentException(msg2);
                    Exceptions.attachMessage((Throwable)iae, (String)("Cannot set value to " + this.getEnteredValue()));
                    Exceptions.attachLocalizedMessage((Throwable)iae, (String)msg2);
                    throw iae;
                }
                IllegalArgumentException iae = (IllegalArgumentException)e;
                try {
                    editor.setValue(this.getProperty().getValue());
                }
                catch (Exception ex) {
                    // empty catch block
                }
                throw iae;
            }
            success = Boolean.TRUE.equals(result);
            if (success) {
                this.fireStateChanged();
            } else {
                InplaceEditor ed = this.getInplaceEditor();
                if (ed != null) {
                    this.getInplaceEditor().reset();
                }
            }
            boolean ed = success;
            return ed;
        }
        finally {
            if (env != null && editor != null) {
                this.attachToEnv(env);
                if (editor instanceof ExPropertyEditor) {
                    ((ExPropertyEditor)editor).attachEnv(env);
                }
            }
        }
    }

    private void cancelEditor() {
        if (this.getInplaceEditor() != null) {
            TableCellEditor tce;
            Container parent;
            for (parent = this.getParent(); parent != null && !(parent instanceof JTable); parent = parent.getParent()) {
            }
            if (parent != null && (tce = ((JTable)parent).getCellEditor()) != null) {
                tce.cancelCellEditing();
            }
        }
    }

    @Override
    public Object getEnteredValue() {
        Object result;
        if (this.getInplaceEditor() != null) {
            result = this.getInplaceEditor().getValue();
        } else if (this.cachedInitialValue != NO_VALUE) {
            result = this.cachedInitialValue;
        } else {
            PropertyEditor ed = PropUtils.getPropertyEditor(this.getProperty());
            try {
                result = ed.getAsText();
            }
            catch (ProxyNode.DifferentValuesException dve) {
                result = null;
            }
        }
        return result;
    }

    PropertyEditor getPropertyEditor() {
        if (this.editor != null) {
            return this.editor;
        }
        PropertyEditor result = this.getInplaceEditor() != null ? this.getInplaceEditor().getPropertyEditor() : PropUtils.getPropertyEditor(this.getProperty());
        this.editor = result;
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public String isModifiedValueLegal() {
        String msg;
        PropertyEditor editor = this.getPropertyEditor();
        PropertyEnv env = null;
        Object newValue = this.getEnteredValue();
        PropertyEnv myEnv = this.getPropertyEnv();
        Exception exception = null;
        Object envState = null;
        if (myEnv != null && PropertyEnv.STATE_NEEDS_VALIDATION.equals(myEnv.getState()) && (msg = myEnv.silentlySetState(PropertyEnv.STATE_VALID, newValue)) != null && !PropertyEnv.STATE_VALID.equals(myEnv.getState())) {
            return msg;
        }
        try {
            if (editor instanceof ExPropertyEditor) {
                if (myEnv != null) {
                    this.detachFromEnv(myEnv);
                }
                env = new PropertyEnv();
                env.setFeatureDescriptor(EditablePropertyDisplayer.findFeatureDescriptor(this));
                ((ExPropertyEditor)editor).attachEnv(env);
            }
            exception = PropUtils.updatePropertyEditor(editor, newValue);
            envState = env == null ? null : env.getState();
        }
        finally {
            if (editor instanceof ExPropertyEditor && myEnv != null) {
                try {
                    editor.setValue(this.getProperty().getValue());
                }
                catch (Exception e) {
                    Logger.getLogger(EditablePropertyDisplayer.class.getName()).log(Level.WARNING, null, e);
                }
                ((ExPropertyEditor)editor).attachEnv(myEnv);
                this.attachToEnv(myEnv);
            }
        }
        String result = null;
        if (exception != null) {
            result = PropUtils.findLocalizedMessage(exception, this.getEnteredValue(), this.getProperty().getDisplayName());
        } else if (PropertyEnv.STATE_INVALID.equals(envState)) {
            result = NbBundle.getMessage(EditablePropertyDisplayer.class, (String)"FMT_CannotUpdateProperty", (Object)newValue, (Object)this.getProperty().getDisplayName());
        }
        return result;
    }

    @Override
    public boolean isValueModified() {
        boolean result = false;
        PropertyEditor peditor = this.getPropertyEditor();
        Object enteredValue = this.getEnteredValue();
        Object realValue = null;
        Object editorValue = null;
        try {
            editorValue = peditor.getValue();
        }
        catch (ProxyNode.DifferentValuesException dve) {
            return false;
        }
        if (enteredValue == null != (editorValue == null)) {
            return true;
        }
        if (realValue == null) {
            realValue = editorValue;
        }
        result = realValue == null != (enteredValue == null) ? true : (realValue == enteredValue ? false : (realValue != null ? !realValue.equals(enteredValue) : false));
        return result;
    }

    @Override
    public void reset() {
        if (this.getInplaceEditor() != null) {
            this.getInplaceEditor().reset();
        }
    }

    @Override
    public void setEnteredValue(Object o) {
        if (this.getInplaceEditor() != null) {
            this.getInplaceEditor().setValue(o);
        } else {
            this.storeCachedInitialValue(o);
        }
    }

    @Override
    protected void setPropertyEnv(PropertyEnv env) {
        if (this.getPropertyEnv() != null) {
            this.detachFromEnv(this.getPropertyEnv());
        }
        super.setPropertyEnv(env);
        if (env != null) {
            env.setChangeImmediate(this.getUpdatePolicy() != 2);
            this.attachToEnv(this.getPropertyEnv());
        }
    }

    @Override
    protected void setInplaceEditor(InplaceEditor ed) {
        if (this.getInplaceEditor() != null) {
            this.detachFromInplaceEditor(this.getInplaceEditor());
        }
        super.setInplaceEditor(ed);
        if (ed == null && this.getPropertyEnv() != null) {
            this.detachFromEnv(this.getPropertyEnv());
        }
        if (this.getInplaceEditor() != null) {
            this.attachToInplaceEditor(this.getInplaceEditor());
        }
    }

    @Override
    public int getUpdatePolicy() {
        return this.updatePolicy;
    }

    @Override
    public void setUpdatePolicy(int i) {
        if (i != 1 && i != 2 && i != 0) {
            throw new IllegalArgumentException("Bad update policy: " + i);
        }
        this.updatePolicy = i;
        PropertyEnv env = this.getPropertyEnv();
        if (env != null) {
            env.setChangeImmediate(i != 2);
        }
    }

    private void trySendEscToDialog() {
        if (this.isTableUI()) {
            return;
        }
        AWTEvent ev = EventQueue.getCurrentEvent();
        if (ev instanceof KeyEvent && ((KeyEvent)ev).getKeyCode() == 27) {
            Action a;
            if (ev.getSource() instanceof JComboBox && ((JComboBox)ev.getSource()).isPopupVisible()) {
                return;
            }
            if (ev.getSource() instanceof JTextComponent && ((JTextComponent)ev.getSource()).getParent() instanceof JComboBox && ((JComboBox)((JTextComponent)ev.getSource()).getParent()).isPopupVisible()) {
                return;
            }
            InputMap imp = this.getRootPane().getInputMap(1);
            ActionMap am = this.getRootPane().getActionMap();
            KeyStroke escape = KeyStroke.getKeyStroke(27, 0, false);
            Object key = imp.get(escape);
            if (key != null && (a = am.get(key)) != null) {
                String commandKey;
                if (Boolean.getBoolean("netbeans.proppanel.logDialogActions")) {
                    System.err.println("Action bound to escape key is " + a);
                }
                if ((commandKey = (String)a.getValue("ActionCommandKey")) == null) {
                    commandKey = "cancel";
                }
                a.actionPerformed(new ActionEvent(this, 1001, commandKey));
            }
        }
    }

    private void trySendEnterToDialog() {
        AWTEvent ev = EventQueue.getCurrentEvent();
        if (ev instanceof KeyEvent && ((KeyEvent)ev).getKeyCode() == 10) {
            JButton b;
            if (ev.getSource() instanceof JComboBox && ((JComboBox)ev.getSource()).isPopupVisible()) {
                return;
            }
            if (ev.getSource() instanceof JTextComponent && ((JTextComponent)ev.getSource()).getParent() instanceof JComboBox && ((JComboBox)((JTextComponent)ev.getSource()).getParent()).isPopupVisible()) {
                return;
            }
            JRootPane jrp = this.getRootPane();
            if (jrp != null && (b = jrp.getDefaultButton()) != null && b.isEnabled()) {
                b.doClick();
            }
        }
    }

    private void attachToEnv(PropertyEnv env) {
        if (this.attachedEnv == env) {
            return;
        }
        env.addVetoableChangeListener(this.getEnvListener());
        env.addPropertyChangeListener(this.getEnvListener());
        env.setBeans(EditablePropertyDisplayer.findBeans(this));
    }

    private void detachFromEnv(PropertyEnv env) {
        env.removeVetoableChangeListener(this.getEnvListener());
        env.addPropertyChangeListener(this.getEnvListener());
        env.setBeans(null);
        this.attachedEnv = null;
    }

    private void attachToInplaceEditor(InplaceEditor ed) {
        Object o = this.fetchCachedInitialValue();
        if (o != NO_VALUE) {
            ed.setValue(o);
        }
        ed.addActionListener(this.getInplaceEditorListener());
        ed.getComponent().addFocusListener(this.getInplaceEditorListener());
    }

    private void detachFromInplaceEditor(InplaceEditor ed) {
        ed.removeActionListener(this.getInplaceEditorListener());
        ed.getComponent().removeFocusListener(this.getInplaceEditorListener());
    }

    private void storeCachedInitialValue(Object o) {
        this.cachedInitialValue = o;
    }

    private Object fetchCachedInitialValue() {
        Object result = this.cachedInitialValue;
        this.cachedInitialValue = NO_VALUE;
        return result;
    }

    private InplaceEditorListener getInplaceEditorListener() {
        if (this.ieListener == null) {
            this.ieListener = new InplaceEditorListener();
        }
        return this.ieListener;
    }

    private EnvListener getEnvListener() {
        if (this.envListener == null) {
            this.envListener = new EnvListener();
        }
        return this.envListener;
    }

    private boolean hasActionListeners() {
        return this.actionListenerCount > 0;
    }

    @Override
    public synchronized void addActionListener(ActionListener listener) {
        if (this.listenerList == null) {
            this.listenerList = new EventListenerList();
        }
        this.listenerList.add(ActionListener.class, listener);
        ++this.actionListenerCount;
    }

    @Override
    public synchronized void removeActionListener(ActionListener listener) {
        this.listenerList.remove(ActionListener.class, listener);
        this.actionListenerCount = Math.max(0, this.actionListenerCount--);
    }

    private void fireActionPerformed() {
        if (this.listenerList == null) {
            return;
        }
        ActionEvent event = new ActionEvent(this, 1001, this.getActionCommand());
        Object[] listeners = this.listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] != ActionListener.class) continue;
            ((ActionListener)listeners[i + 1]).actionPerformed(event);
        }
    }

    @Override
    public synchronized void addChangeListener(ChangeListener listener) {
        if (this.listenerList == null) {
            this.listenerList = new EventListenerList();
        }
        this.listenerList.add(ChangeListener.class, listener);
    }

    @Override
    public synchronized void removeChangeListener(ChangeListener listener) {
        this.listenerList.remove(ChangeListener.class, listener);
    }

    private void fireStateChanged() {
        if (this.listenerList == null) {
            return;
        }
        Object[] listeners = this.listenerList.getListenerList();
        ChangeEvent event = new ChangeEvent(this);
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] != ChangeListener.class) continue;
            ((ChangeListener)listeners[i + 1]).stateChanged(event);
        }
    }

    @Override
    public String getActionCommand() {
        return this.actionCommand;
    }

    @Override
    public void setActionCommand(String val) {
        this.actionCommand = val;
    }

    private boolean shouldIgnoreFocusEvents() {
        return this.customEditorIsOpening || this.inReplaceInner;
    }

    @Override
    protected void configureButtonPanel(ButtonPanel bp) {
        bp.setButtonAction(this.getCustomEditorAction());
    }

    Action getCustomEditorAction() {
        if (this.customEditorAction == null) {
            PropertyModel mdl = null;
            if (this.modelRef != null) {
                mdl = (PropertyModel)this.modelRef.get();
            }
            this.customEditorAction = new CustomEditorAction(new Invoker(), mdl);
            this.getInputMap(1).put(KeyStroke.getKeyStroke(32, 128, false), "invokeCustomEditor");
            this.getActionMap().put("invokeCustomEditor", this.customEditorAction);
        }
        return this.customEditorAction;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("Inline editor for property ");
        sb.append(this.getProperty().getDisplayName());
        sb.append(" = ");
        sb.append((Object)this.getProperty());
        sb.append(" inplace editor=");
        sb.append(this.getInplaceEditor());
        return sb.toString();
    }

    void setRemoteEnvListener(PropertyChangeListener l) {
        this.remoteEnvListener = l;
    }

    void setRemoteEnvVetoListener(VetoableChangeListener vl) {
        this.remotevEnvListener = vl;
    }

    public synchronized void dispose() {
        this.setPropertyEnv(null);
        this.setInplaceEditor(null);
        this.remotevEnvListener = null;
        this.remoteEnvListener = null;
        this.cachedInitialValue = null;
        this.editor = null;
    }

    private class EnvListener
    implements VetoableChangeListener,
    PropertyChangeListener {
        private boolean wantNextChange;

        private EnvListener() {
            this.wantNextChange = false;
        }

        @Override
        public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
            if ("state".equals(evt.getPropertyName())) {
                boolean bl = this.wantNextChange = evt.getNewValue() != EditablePropertyDisplayer.this.getPropertyEnv().getState() && EditablePropertyDisplayer.this.getPropertyEnv().getState() != null && (evt.getNewValue() != PropertyEnv.STATE_NEEDS_VALIDATION || evt.getNewValue() == PropertyEnv.STATE_NEEDS_VALIDATION && evt.getOldValue() == PropertyEnv.STATE_VALID);
            }
            if (!EditablePropertyDisplayer.this.inReplaceInner && EditablePropertyDisplayer.this.remotevEnvListener != null) {
                EditablePropertyDisplayer.this.remotevEnvListener.vetoableChange(evt);
            }
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (EditablePropertyDisplayer.this.inReplaceInner) {
                return;
            }
            if (this.wantNextChange || evt.getNewValue() == PropertyEnv.STATE_VALID && evt.getNewValue() != EditablePropertyDisplayer.this.lastKnownState) {
                this.wantNextChange = false;
                EditablePropertyDisplayer.this.replaceInner();
                EditablePropertyDisplayer.this.lastKnownState = ((PropertyEnv)evt.getSource()).getState();
            }
            if (EditablePropertyDisplayer.this.remoteEnvListener != null) {
                EditablePropertyDisplayer.this.remoteEnvListener.propertyChange(evt);
            }
        }
    }

    private class Invoker
    implements CustomEditorAction.Invoker {
        boolean failed;

        private Invoker() {
            this.failed = false;
        }

        @Override
        public boolean allowInvoke() {
            return true;
        }

        @Override
        public void editorClosed() {
            if (this.failed) {
                EditablePropertyDisplayer.this.requestFocus();
            }
            EditablePropertyDisplayer.this.customEditorIsOpening = false;
        }

        @Override
        public void editorOpened() {
            EditablePropertyDisplayer.this.customEditorIsOpening = false;
            EditablePropertyDisplayer.this.repaint();
        }

        @Override
        public void editorOpening() {
            EditablePropertyDisplayer.this.customEditorIsOpening = true;
        }

        @Override
        public void failed() {
            this.failed = true;
            if (EditablePropertyDisplayer.this.getInplaceEditor() != null) {
                EditablePropertyDisplayer.this.getInplaceEditor().reset();
            }
        }

        @Override
        public String getBeanName() {
            PropertyModel pm;
            if (EditablePropertyDisplayer.this.modelRef != null && (pm = (PropertyModel)EditablePropertyDisplayer.this.modelRef.get()) instanceof NodePropertyModel) {
                return ((NodePropertyModel)pm).getBeanName();
            }
            if (EditablePropertyDisplayer.this.getProperty() instanceof ModelProperty.DPMWrapper) {
                return ((ModelProperty.DPMWrapper)EditablePropertyDisplayer.this.getProperty()).getBeanName();
            }
            return EditorPropertyDisplayer.findFeatureDescriptor(EditablePropertyDisplayer.this).getDisplayName();
        }

        @Override
        public Component getCursorChangeComponent() {
            return EditablePropertyDisplayer.this;
        }

        @Override
        public Object getPartialValue() {
            Object pvalue = EditablePropertyDisplayer.this.getEnteredValue();
            EditablePropertyDisplayer.this.cancelEditor();
            return pvalue;
        }

        @Override
        public FeatureDescriptor getSelection() {
            return EditablePropertyDisplayer.this.getProperty();
        }

        @Override
        public void valueChanged(PropertyEditor editor) {
            this.failed = false;
            try {
                if (EditablePropertyDisplayer.this.getInplaceEditor() != null) {
                    EditablePropertyDisplayer.this.setEnteredValue(EditablePropertyDisplayer.this.getProperty().getValue());
                } else {
                    PropertyModel mdl;
                    PropertyModel propertyModel = mdl = EditablePropertyDisplayer.this.modelRef != null ? (PropertyModel)EditablePropertyDisplayer.this.modelRef.get() : null;
                    if (mdl != null) {
                        FeatureDescriptor fd = null;
                        if (mdl instanceof ExPropertyModel) {
                            fd = ((ExPropertyModel)mdl).getFeatureDescriptor();
                        }
                        String title = null;
                        if (fd != null) {
                            title = fd.getDisplayName();
                        }
                        this.failed = PropUtils.updateProp(mdl, editor, title);
                    }
                }
            }
            catch (Exception e) {
                throw (IllegalStateException)new IllegalStateException("Problem setting entered value from custom editor").initCause(e);
            }
        }

        @Override
        public boolean wantAllChanges() {
            return true;
        }

        @Override
        public ReusablePropertyEnv getReusablePropertyEnv() {
            return EditablePropertyDisplayer.this.getReusablePropertyEnv();
        }
    }

    private class InplaceEditorListener
    implements ActionListener,
    FocusListener {
        private InplaceEditorListener() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            boolean isSuccess;
            boolean bl = isSuccess = "success".equals(e.getActionCommand()) || "comboBoxEdited".equals(e.getActionCommand());
            if (isSuccess) {
                if (EditablePropertyDisplayer.this.getUpdatePolicy() == 0 || EditablePropertyDisplayer.this.getUpdatePolicy() == 1) {
                    EditablePropertyDisplayer.this.commit();
                }
                if (EditablePropertyDisplayer.this.hasActionListeners()) {
                    EditablePropertyDisplayer.this.fireActionPerformed();
                } else {
                    EditablePropertyDisplayer.this.trySendEnterToDialog();
                }
            } else if (!EditablePropertyDisplayer.this.hasActionListeners()) {
                EditablePropertyDisplayer.this.trySendEscToDialog();
            }
        }

        @Override
        public void focusGained(FocusEvent e) {
            if (EditablePropertyDisplayer.this.shouldIgnoreFocusEvents()) {
                return;
            }
        }

        @Override
        public void focusLost(FocusEvent e) {
            if (EditablePropertyDisplayer.this.shouldIgnoreFocusEvents()) {
                return;
            }
            if (!e.isTemporary() && EditablePropertyDisplayer.this.getUpdatePolicy() == 1 && !EditablePropertyDisplayer.this.getInplaceEditor().isKnownComponent(e.getOppositeComponent()) && EditablePropertyDisplayer.this.isValueModified()) {
                EditablePropertyDisplayer.this.commit();
            }
        }
    }

}

