/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package org.openide.explorer.propertysheet;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.FocusListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.CellRendererPane;
import javax.swing.ComboBoxEditor;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.basic.BasicComboBoxEditor;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.basic.ComboPopup;
import javax.swing.plaf.metal.MetalComboBoxIcon;
import org.openide.explorer.propertysheet.ComboBoxAutoCompleteSupport;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.util.Utilities;

class CleanComboUI
extends BasicComboBoxUI {
    private JButton button = null;
    private boolean tableUI;
    private ComboPopup popup = null;

    public CleanComboUI(boolean tableUI) {
        this.tableUI = tableUI;
    }

    @Override
    protected void installDefaults() {
        LookAndFeel.installColorsAndFont(this.comboBox, "ComboBox.background", "ComboBox.foreground", "ComboBox.font");
        if (this.tableUI) {
            this.comboBox.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 0));
        } else {
            this.comboBox.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(PropUtils.getShadowColor()), BorderFactory.createEmptyBorder(0, 2, 0, 0)));
        }
        CleanComboUI.installComboDefaults(this.comboBox);
    }

    @Override
    protected ComboPopup createPopup() {
        this.popup = new CleanComboPopup(this.comboBox);
        return this.popup;
    }

    @Override
    protected void installKeyboardActions() {
        super.installKeyboardActions();
        if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
            this.comboBox.getInputMap().put(KeyStroke.getKeyStroke(10, 0), "enterPressed");
        }
        if (!this.tableUI) {
            this.comboBox.getInputMap().put(KeyStroke.getKeyStroke(32, 0), "showPopup");
            this.comboBox.getActionMap().put("showPopup", new AbstractAction(){

                @Override
                public void actionPerformed(ActionEvent ae) {
                    if (!CleanComboUI.this.comboBox.isPopupVisible()) {
                        CleanComboUI.this.comboBox.showPopup();
                    }
                }
            });
        }
        if ("Aqua".equals(UIManager.getLookAndFeel().getID()) && "10.5".compareTo(System.getProperty("os.version")) <= 0) {
            AbstractAction selectPrevAction = new AbstractAction(){

                @Override
                public void actionPerformed(ActionEvent ae) {
                    CleanComboUI.this.selectPreviousPossibleValue();
                }
            };
            AbstractAction selectNextAction = new AbstractAction(){

                @Override
                public void actionPerformed(ActionEvent ae) {
                    CleanComboUI.this.selectNextPossibleValue();
                }
            };
            this.comboBox.getInputMap().put(KeyStroke.getKeyStroke(38, 0), "selectPrevious");
            this.comboBox.getInputMap().put(KeyStroke.getKeyStroke(40, 0), "selectNext");
            this.comboBox.getActionMap().put("selectPrevious", selectPrevAction);
            this.comboBox.getActionMap().put("selectNext", selectNextAction);
            JComponent editor = (JComponent)this.comboBox.getEditor().getEditorComponent();
            editor.getInputMap().put(KeyStroke.getKeyStroke(38, 0), "selectPrevious");
            editor.getInputMap().put(KeyStroke.getKeyStroke(40, 0), "selectNext");
            editor.getActionMap().put("selectPrevious", selectPrevAction);
            editor.getActionMap().put("selectNext", selectNextAction);
        }
    }

    @Override
    protected JButton createArrowButton() {
        Icon i = UIManager.getIcon("ComboBox.icon");
        if (i == null) {
            i = "Aqua".equals(UIManager.getLookAndFeel().getID()) ? new AquaComboIcon() : new MetalComboBoxIcon();
        }
        this.button = new JButton(i);
        this.button.setFocusable(false);
        this.button.setContentAreaFilled(false);
        this.button.setBorderPainted(false);
        this.button.setBorder(null);
        return this.button;
    }

    @Override
    protected Insets getInsets() {
        Insets i = super.getInsets();
        i.right += 2;
        return i;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void paint(Graphics g, JComponent c) {
        super.paint(g, c);
        if (c.hasFocus() && !this.tableUI) {
            Color prev = g.getColor();
            try {
                g.setColor(PropUtils.getShadowColor());
                g.drawRect(2, 2, c.getWidth() - 5, c.getHeight() - 5);
            }
            finally {
                g.setColor(prev);
            }
        }
    }

    @Override
    protected FocusListener createFocusListener() {
        return super.createFocusListener();
    }

    @Override
    public void paintCurrentValue(Graphics g, Rectangle bounds, boolean hasFocus) {
        ListCellRenderer<Object> renderer = this.comboBox.getRenderer();
        if (this.listBox == null || renderer == null) {
            return;
        }
        Component c = renderer.getListCellRendererComponent(this.listBox, this.comboBox.getSelectedItem(), -1, false, false);
        c.setFont(this.comboBox.getFont());
        c.setForeground(this.comboBox.isEnabled() ? this.comboBox.getForeground() : PropUtils.getDisabledForeground());
        c.setBackground(this.comboBox.getBackground());
        boolean shouldValidate = false;
        if (c instanceof JPanel) {
            shouldValidate = true;
        }
        this.currentValuePane.paintComponent(g, c, this.comboBox, bounds.x, bounds.y, bounds.width, bounds.height, shouldValidate);
    }

    @Override
    protected Rectangle rectangleForCurrentValue() {
        Rectangle r = super.rectangleForCurrentValue();
        if (this.editor != null) {
            ++r.x;
            ++r.y;
            --r.width;
            --r.height;
        }
        return r;
    }

    @Override
    protected ComboBoxEditor createEditor() {
        return new CleanComboBoxEditor();
    }

    private static void installComboDefaults(JComponent jc) {
        Font f;
        Color c = UIManager.getColor("ComboBox.background");
        if (c == null) {
            c = UIManager.getColor("text");
        }
        if (c != null) {
            jc.setBackground(c);
        }
        if ((c = UIManager.getColor("ComboBox.foreground")) == null) {
            c = UIManager.getColor("textText");
        }
        if (c != null) {
            jc.setForeground(c);
        }
        if ((f = UIManager.getFont("ComboBox.font")) != null) {
            jc.setFont(f);
        }
    }

    private static class AquaComboIcon
    implements Icon {
        private AquaComboIcon() {
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            x = (c.getWidth() - this.getIconWidth()) / 2;
            y = (c.getHeight() - this.getIconHeight()) / 2;
            g.setColor(UIManager.getColor("Button.Foreground"));
            g.drawLine(x + 3, y, x + 3, y);
            g.drawLine(x + 2, y + 1, x + 2 + 2, y + 1);
            g.drawLine(x + 1, y + 2, x + 1 + 4, y + 2);
            g.drawLine(x, y + 3, x + 6, y + 3);
            g.drawLine(x, y + 7, x + 6, y + 7);
            g.drawLine(x + 1, y + 8, x + 1 + 4, y + 8);
            g.drawLine(x + 2, y + 9, x + 2 + 2, y + 9);
            g.drawLine(x + 3, y + 10, x + 3, y + 10);
        }

        @Override
        public int getIconWidth() {
            return 7;
        }

        @Override
        public int getIconHeight() {
            return 11;
        }
    }

    static class CleanComboBoxEditor
    extends BasicComboBoxEditor {
        public CleanComboBoxEditor() {
            this.editor = new JTextField();
            Color c = UIManager.getColor("Table.selectionBackground");
            if (c == null) {
                c = Color.BLACK;
            }
            this.editor.setBorder(BorderFactory.createLineBorder(c));
        }
    }

    private static class CleanComboPopup
    extends BasicComboPopup {
        public CleanComboPopup(JComboBox box) {
            super(box);
            CleanComboUI.installComboDefaults(this);
        }

        @Override
        protected Rectangle computePopupBounds(int px, int py, int pw, int ph) {
            if (ComboBoxAutoCompleteSupport.isAutoCompleteInstalled(this.comboBox)) {
                return super.computePopupBounds(px, py, pw, ph);
            }
            Dimension d = this.list.getPreferredSize();
            Rectangle r = Utilities.getUsableScreenBounds();
            if (pw < d.width) {
                pw = Math.min(d.width, r.width - px);
            }
            if (ph < d.height) {
                ph = Math.min(r.height - py, d.height);
            }
            if (px + pw > r.width - px) {
                px -= r.width - pw;
            }
            Rectangle result = new Rectangle(px, py, pw, ph);
            return result;
        }
    }

}

