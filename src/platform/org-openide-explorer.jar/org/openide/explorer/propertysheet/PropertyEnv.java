/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.util.Exceptions
 */
package org.openide.explorer.propertysheet;

import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.explorer.propertysheet.CustomEditorAccessorImpl;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertyDialogManager;
import org.openide.util.Exceptions;

public class PropertyEnv {
    public static final String PROP_STATE = "state";
    public static final Object STATE_VALID = "valid";
    public static final Object STATE_NEEDS_VALIDATION = "needs_validation";
    public static final Object STATE_INVALID = "invalid";
    static final String PROP_CHANGE_IMMEDIATE = "changeImmediate";
    static final FeatureDescriptor dummyDescriptor = new FeatureDescriptor();
    private FeatureDescriptor featureDescriptor = dummyDescriptor;
    private Object[] beans;
    private Object state = STATE_VALID;
    private VetoableChangeSupport support;
    private PropertyChangeSupport change;
    private boolean changeImmediate = true;
    InplaceEditor.Factory factory = null;
    boolean editable = true;
    private static final Logger LOG = Logger.getLogger(PropertyEnv.class.getName());

    PropertyEnv() {
    }

    public static /* varargs */ PropertyEnv create(FeatureDescriptor fd, Object ... beans) {
        PropertyEnv env = new PropertyEnv();
        env.setFeatureDescriptor(fd);
        env.setBeans(beans);
        return env;
    }

    public Object[] getBeans() {
        return this.beans;
    }

    void setBeans(Object[] beans) {
        this.beans = beans;
    }

    public FeatureDescriptor getFeatureDescriptor() {
        return this.featureDescriptor;
    }

    void setFeatureDescriptor(FeatureDescriptor desc) {
        Object obj;
        if (desc == null) {
            throw new IllegalArgumentException("Cannot set FeatureDescriptor to null.");
        }
        this.featureDescriptor = desc;
        if (this.featureDescriptor != null && (obj = this.featureDescriptor.getValue("changeImmediate")) instanceof Boolean) {
            this.setChangeImmediate((Boolean)obj);
        }
    }

    public void setState(Object newState) {
        if (this.getState().equals(newState)) {
            return;
        }
        try {
            this.getSupport().fireVetoableChange("state", this.getState(), newState);
            this.state = newState;
            this.getChange().firePropertyChange("state", null, newState);
        }
        catch (PropertyVetoException pve) {
            String msg = Exceptions.findLocalizedMessage((Throwable)pve);
            if (msg == null) {
                msg = pve.getLocalizedMessage();
            }
            if (msg != null) {
                DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)msg, 2));
            }
            PropertyDialogManager.notify(pve);
        }
    }

    String silentlySetState(Object newState, Object newValue) {
        if (this.getState().equals(newState)) {
            return null;
        }
        try {
            this.getSupport().fireVetoableChange("state", this.getState(), newState);
            this.state = newState;
            this.getChange().firePropertyChange("state", null, newState);
        }
        catch (PropertyVetoException pve) {
            LOG.log(Level.INFO, "Cannot change property: " + pve.getPropertyChangeEvent().getPropertyName(), pve);
            String name = this.getFeatureDescriptor() == null ? null : this.getFeatureDescriptor().getDisplayName();
            return PropUtils.findLocalizedMessage(pve, newValue, name);
        }
        return null;
    }

    public Object getState() {
        return this.state;
    }

    public void addVetoableChangeListener(VetoableChangeListener l) {
        this.getSupport().addVetoableChangeListener(l);
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.getChange().addPropertyChangeListener(l);
    }

    public void removeVetoableChangeListener(VetoableChangeListener l) {
        this.getSupport().removeVetoableChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.getChange().removePropertyChangeListener(l);
    }

    boolean isChangeImmediate() {
        return this.changeImmediate;
    }

    void setChangeImmediate(boolean changeImmediate) {
        this.changeImmediate = changeImmediate;
    }

    private synchronized VetoableChangeSupport getSupport() {
        if (this.support == null) {
            this.support = new VetoableChangeSupport(this);
        }
        return this.support;
    }

    private synchronized PropertyChangeSupport getChange() {
        if (this.change == null) {
            this.change = new PropertyChangeSupport(this);
        }
        return this.change;
    }

    public void registerInplaceEditorFactory(InplaceEditor.Factory factory) {
        this.factory = factory;
    }

    InplaceEditor getInplaceEditor() {
        InplaceEditor result = this.factory != null ? this.factory.getInplaceEditor() : null;
        return result;
    }

    void setEditable(boolean editable) {
        this.editable = editable;
    }

    boolean isEditable() {
        return this.editable;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getName());
        sb.append("@");
        sb.append(System.identityHashCode(this));
        sb.append("[state=");
        sb.append(this.state == STATE_NEEDS_VALIDATION ? "STATE_NEEDS_VALIDATION" : (this.state == STATE_INVALID ? "STATE_INVALID" : "STATE_VALID"));
        sb.append(", ");
        InplaceEditor.Factory f = this.factory;
        if (f != null) {
            sb.append("InplaceEditorFactory=").append(f.getClass().getName());
            sb.append(", ");
        }
        sb.append("editable=");
        sb.append(this.editable);
        sb.append(", isChangeImmediate=");
        sb.append(this.isChangeImmediate());
        sb.append(", featureDescriptor=");
        FeatureDescriptor fd = this.getFeatureDescriptor();
        if (fd != null) {
            sb.append(fd.getDisplayName());
        } else {
            sb.append("null");
        }
        return sb.toString();
    }

    static {
        CustomEditorAccessorImpl.register();
    }
}

