/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package org.openide.explorer.propertysheet;

import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.propertysheet.PropertySheet;
import org.openide.nodes.Node;

public class PropertySheetView
extends PropertySheet {
    static final long serialVersionUID = -7568245745904766160L;
    private transient boolean guiInitialized = false;
    private transient PropertyIL managerListener;
    private transient ExplorerManager explorerManager;

    public PropertySheetView() {
        this.setPreferredSize(new Dimension(200, 300));
    }

    private void initializeGUI() {
        this.guiInitialized = true;
        this.managerListener = new PropertyIL();
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.explorerManager = ExplorerManager.find(this);
        if (!this.guiInitialized) {
            this.initializeGUI();
        }
        this.explorerManager.addPropertyChangeListener(this.managerListener);
        this.setNodes(this.explorerManager.getSelectedNodes());
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        if (this.explorerManager != null) {
            this.explorerManager.removePropertyChangeListener(this.managerListener);
            this.explorerManager = null;
            this.setNodes(new Node[0]);
        }
    }

    class PropertyIL
    implements PropertyChangeListener {
        PropertyIL() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("selectedNodes".equals(evt.getPropertyName())) {
                PropertySheetView.this.setNodes((Node[])evt.getNewValue());
            }
        }
    }

}

