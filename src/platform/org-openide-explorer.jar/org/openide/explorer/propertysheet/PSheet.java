/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$PropertySet
 */
package org.openide.explorer.propertysheet;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Arrays;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.BoundedRangeModel;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ViewportUI;
import org.netbeans.modules.openide.explorer.TabbedContainerBridge;
import org.openide.explorer.propertysheet.DescriptionComponent;
import org.openide.explorer.propertysheet.MarginViewportUI;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertySheet;
import org.openide.explorer.propertysheet.SelectionAndScrollPositionManager;
import org.openide.explorer.propertysheet.SheetTable;
import org.openide.nodes.Node;

class PSheet
extends JPanel
implements MouseListener {
    public static final int STATE_HAS_DESCRIPTION = 1;
    public static final int STATE_HAS_TABS = 2;
    private int addCount = 0;
    private String description = "";
    private String title = "";
    private SelectionAndScrollPositionManager manager = new SelectionAndScrollPositionManager();
    private boolean adjusting = false;
    private boolean helpEnabled = true;
    private boolean marginPainted = false;
    private Color marginColor = UIManager.getColor("controlShadow");
    private String emptyString = "THIS IS A BUG";
    private Boolean firstSplit = null;
    private Object[] tabbedContainerObjects = new String[]{"Hello", "World", "This", "Is", "Me"};
    private String[] tabbedContainerTitles = new String[]{"Tab 1", "Tab 2", "Tab 3", "Tab 4", "Tab 5"};
    private ChangeListener selectionListener = null;

    public PSheet() {
        this.getInputMap(1).put(KeyStroke.getKeyStroke(121, 64), "popup");
        this.getActionMap().put("popup", new PopupAction());
        this.getActionMap().put("PreviousViewAction", new SwitchTabAction(-1));
        this.getActionMap().put("NextViewAction", new SwitchTabAction(1));
        if (PropUtils.isAqua) {
            this.setBackground(UIManager.getColor("NbExplorerView.background"));
            this.setOpaque(true);
        }
    }

    SelectionAndScrollPositionManager manager() {
        return this.manager;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void adjustForName(String nodeName) {
        this.adjusting = true;
        try {
            JScrollPane jsc;
            JComponent comp = this.findTabbedContainer();
            String tabname = null;
            if (comp != null) {
                boolean success = false;
                tabname = this.manager().getLastSelectedGroupName();
                if (tabname != null && tabname.length() > 0) {
                    success = TabbedContainerBridge.getDefault().setSelectionByName(comp, tabname);
                }
                if (!success && (tabname = this.manager().getGroupNameForNodeName(nodeName)) != null && tabname.length() > 0) {
                    success = TabbedContainerBridge.getDefault().setSelectionByName(comp, tabname);
                }
                if (!success) {
                    tabname = PropUtils.basicPropsTabName();
                    success = TabbedContainerBridge.getDefault().setSelectionByName(comp, tabname);
                }
                if (success && this.selectionListener != null) {
                    ChangeEvent ce = new ChangeEvent(this);
                    this.selectionListener.stateChanged(ce);
                }
            }
            if ((jsc = this.findScrollPane()) != null) {
                String s;
                int pos;
                String string = s = tabname == null ? this.manager().getCurrentNodeName() : tabname;
                if (s != null && (pos = this.manager().getScrollPositionForNodeName(s)) >= 0 && pos < jsc.getVerticalScrollBar().getModel().getMaximum()) {
                    jsc.getVerticalScrollBar().getModel().setValue(pos);
                }
            }
        }
        finally {
            this.adjusting = false;
        }
    }

    public boolean isAdjusting() {
        return this.adjusting;
    }

    public void storeScrollAndTabInfo() {
        JComponent comp = this.findTabbedContainer();
        String tab = null;
        String node = this.manager().getCurrentNodeName();
        String lastTab = this.manager().getLastSelectedGroupName();
        if (node != null) {
            JScrollPane jsc;
            if (comp != null && (tab = TabbedContainerBridge.getDefault().getCurrentSelectedTabName(comp)) != null) {
                this.manager().storeLastSelectedGroup(tab);
            }
            if ((jsc = this.findScrollPane()) != null) {
                int pos = jsc.getVerticalScrollBar().getModel().getValue();
                String nm = lastTab != null ? lastTab : (tab != null ? tab : node);
                this.manager().storeScrollPosition(pos, nm);
            }
        }
    }

    public void setDescription(String title, String txt) {
        this.description = txt;
        this.title = title;
        DescriptionComponent desc = this.findDescriptionComponent();
        if (desc != null) {
            desc.setDescription(title, txt);
        }
    }

    public void setPropId(String id) {
        DescriptionComponent desc = this.findDescriptionComponent();
        if (desc != null) {
            desc.setPropId(id);
        }
    }

    public void setHelpEnabled(boolean val) {
        if (this.helpEnabled != val) {
            this.helpEnabled = val;
            DescriptionComponent desc = this.findDescriptionComponent();
            if (desc != null) {
                desc.setHelpEnabled(val);
            }
        }
    }

    public void setMarginPainted(boolean val) {
        if (this.marginPainted != val) {
            this.marginPainted = val;
            MarginViewportUI ui = this.findMVUI();
            if (ui != null) {
                ui.setMarginPainted(val);
            }
        }
    }

    public void setMarginColor(Color c) {
        if (!c.equals(this.marginColor)) {
            this.marginColor = c;
            MarginViewportUI ui = this.findMVUI();
            if (ui != null) {
                ui.setMarginColor(c);
            }
        }
    }

    public void setEmptyString(String s) {
        if (!s.equals(this.emptyString)) {
            this.emptyString = s;
            MarginViewportUI ui = this.findMVUI();
            if (ui != null) {
                ui.setEmptyString(s);
            }
        }
    }

    private MarginViewportUI findMVUI() {
        MarginViewportUI result = null;
        JScrollPane pane = this.findScrollPane();
        if (pane != null) {
            ViewportUI ui = pane.getViewport().getUI();
            if (ui instanceof MarginViewportUI) {
                result = (MarginViewportUI)ui;
            } else {
                result = (MarginViewportUI)MarginViewportUI.createUI(pane.getViewport());
                pane.getViewport().setUI(result);
            }
        }
        return result;
    }

    @Override
    public void doLayout() {
        Component[] c = this.getComponents();
        if (c.length > 0 && this.getWidth() >= 0 && this.getHeight() >= 0) {
            Insets ins = this.getInsets();
            c[0].setBounds(ins.left, ins.top, this.getWidth() - (ins.right + ins.left), this.getHeight() - ins.top + ins.bottom);
            if (c[0] instanceof JSplitPane && Boolean.TRUE.equals(this.firstSplit)) {
                JSplitPane pane = (JSplitPane)c[0];
                pane.setDividerLocation(0.6499999761581421);
                pane.resetToPreferredSizes();
                DescriptionComponent dc = this.findDescriptionComponent();
                if (dc != null) {
                    if (dc.getHeight() > 0) {
                        this.firstSplit = Boolean.FALSE;
                    }
                } else {
                    this.firstSplit = Boolean.FALSE;
                }
            }
            if (c.length > 1) {
                throw new IllegalStateException("Hmm, something is wrong: " + Arrays.asList(c));
            }
        }
    }

    @Override
    public void requestFocus() {
        JScrollPane jsc = this.findScrollPane();
        if (jsc != null && jsc.getViewport().getView() != null) {
            jsc.getViewport().getView().requestFocus();
        }
    }

    @Override
    public boolean requestFocusInWindow() {
        JScrollPane jsc = this.findScrollPane();
        if (jsc != null && jsc.getViewport().getView() != null) {
            return jsc.getViewport().getView().requestFocusInWindow();
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setState(int state) {
        if (state != this.getState()) {
            Object object = this.getTreeLock();
            synchronized (object) {
                switch (state) {
                    case 0: {
                        JSplitPane jsp;
                        JComponent tc = this.findTabbedContainer();
                        if (tc != null) {
                            this.remove(tc);
                        }
                        if ((jsp = this.findSplitPane()) == null) break;
                        this.remove(jsp);
                        break;
                    }
                    case 1: {
                        JSplitPane split = this.findSplitPane();
                        this.remove(this.findTabbedContainer());
                        if (split != null) {
                            this.addImpl(split, null, 0);
                            break;
                        }
                        this.addImpl(this.createDescriptionComponent(), null, 0);
                        break;
                    }
                    case 2: {
                        JSplitPane spl;
                        JScrollPane jsc = this.findScrollPane();
                        JComponent tct = this.findTabbedContainer();
                        if (tct == null) {
                            this.addImpl(this.createTabbedContainer(), null, 0);
                        }
                        if ((spl = this.findSplitPane()) != null) {
                            this.remove(spl);
                        }
                        if (jsc != null) {
                            this.setTabbedContainerInnerComponent(this.findTabbedContainer(), jsc);
                        }
                        this.adjustForName(this.manager.getCurrentNodeName());
                        break;
                    }
                    case 3: {
                        JComponent tcc = this.findTabbedContainer();
                        JSplitPane splt = this.findSplitPane();
                        JScrollPane scrl = this.findScrollPane();
                        if (tcc == null) {
                            tcc = this.createTabbedContainer();
                            this.addImpl(tcc, null, 0);
                        }
                        if (splt == null) {
                            this.addImpl(this.createDescriptionComponent(), null, 0);
                            splt = this.findSplitPane();
                        }
                        this.setTabbedContainerInnerComponent(tcc, splt);
                        if (scrl != null) {
                            splt.setLeftComponent(scrl);
                        }
                        this.adjustForName(this.manager.getCurrentNodeName());
                        break;
                    }
                    default: {
                        throw new IllegalArgumentException(Integer.toString(state));
                    }
                }
            }
        }
        this.revalidate();
        this.repaint();
    }

    public int getState() {
        int result = 0;
        if (this.findTabbedContainer() != null) {
            result |= 2;
        }
        if (this.findSplitPane() != null) {
            result |= 1;
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void addImpl(Component comp, Object constraints, int idx) {
        if (!(comp instanceof JSplitPane || comp instanceof JScrollPane || comp instanceof DescriptionComponent || comp instanceof JTable || comp instanceof JComponent && Boolean.TRUE.equals(((JComponent)comp).getClientProperty("tc")))) {
            throw new IllegalArgumentException("Unexpected component " + comp);
        }
        Object object = this.getTreeLock();
        synchronized (object) {
            block32 : {
                ++this.addCount;
                try {
                    if (!Arrays.asList(comp.getMouseListeners()).contains(this)) {
                        comp.addMouseListener(this);
                    }
                    if (comp instanceof JTable) {
                        JScrollPane jsc = this.findScrollPane();
                        if (jsc == null) {
                            jsc = this.createScrollPane(comp);
                        } else {
                            jsc.setViewportView(comp);
                        }
                        JSplitPane split = this.findSplitPane();
                        if (split != null) {
                            split.setLeftComponent(jsc);
                            split.revalidate();
                        } else {
                            JComponent tc = this.findTabbedContainer();
                            if (tc != null) {
                                this.setTabbedContainerInnerComponent(tc, split);
                            } else {
                                this.addImpl(jsc, constraints, idx);
                            }
                        }
                        break block32;
                    }
                    if (comp instanceof DescriptionComponent) {
                        JScrollPane scroll;
                        boolean hadPane;
                        JSplitPane pane = this.findSplitPane();
                        boolean bl = hadPane = pane != null;
                        if (pane == null) {
                            pane = this.createSplitPane(comp);
                        }
                        if ((scroll = this.findScrollPane()) != null) {
                            pane.setLeftComponent(scroll);
                        }
                        if (!hadPane) {
                            this.addImpl(pane, constraints, idx);
                        }
                        ((DescriptionComponent)comp).setDescription(this.title, this.description);
                        ((DescriptionComponent)comp).setHelpEnabled(this.helpEnabled);
                        break block32;
                    }
                    if (PSheet.isTabbedContainer(comp)) {
                        JSplitPane split = this.findSplitPane();
                        if (split != null) {
                            super.remove(split);
                            this.setTabbedContainerInnerComponent((JComponent)comp, split);
                        } else {
                            JScrollPane pane = this.findScrollPane();
                            if (pane != null) {
                                this.setTabbedContainerInnerComponent((JComponent)comp, pane);
                                this.remove(pane);
                            }
                        }
                        super.addImpl(comp, constraints, idx);
                        break block32;
                    }
                    if (comp instanceof JScrollPane) {
                        JSplitPane split = this.findSplitPane();
                        if (split != null) {
                            split.setLeftComponent(comp);
                            split.revalidate();
                        } else {
                            JComponent tc = this.findTabbedContainer();
                            if (tc != null) {
                                this.setTabbedContainerInnerComponent(tc, (JComponent)comp);
                            } else {
                                super.addImpl(comp, constraints, idx);
                            }
                        }
                        break block32;
                    }
                    if (comp instanceof JSplitPane) {
                        JComponent tc;
                        JScrollPane jsc = this.findScrollPane();
                        if (jsc != null) {
                            ((JSplitPane)comp).setLeftComponent(jsc);
                        }
                        if ((tc = this.findTabbedContainer()) != null) {
                            this.setTabbedContainerInnerComponent(tc, (JComponent)comp);
                        } else {
                            super.addImpl(comp, constraints, idx);
                        }
                        break block32;
                    }
                    super.addImpl(comp, constraints, idx);
                }
                finally {
                    --this.addCount;
                    this.revalidate();
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void remove(Component c) {
        if (c == null) {
            return;
        }
        c.removeMouseListener(this);
        Object object = this.getTreeLock();
        synchronized (object) {
            JSplitPane jsp;
            if (c.getParent() == this) {
                super.remove(c);
                if (this.adding()) {
                    return;
                }
            }
            if (PSheet.isTabbedContainer(c)) {
                JComponent inner = PSheet.getTabbedContainerInnerComponent((JComponent)c);
                if (inner != null) {
                    this.addImpl(inner, null, 0);
                }
            } else if (c instanceof JSplitPane) {
                Component inner;
                if (c.getParent() != null) {
                    c.getParent().remove(c);
                }
                if ((inner = ((JSplitPane)c).getLeftComponent()) != null) {
                    this.addImpl(inner, null, 0);
                }
            } else if (c instanceof DescriptionComponent && (jsp = this.findSplitPane()) != null) {
                jsp.remove(c);
                this.remove(jsp);
            }
        }
        this.revalidate();
    }

    private boolean adding() {
        return this.addCount > 0;
    }

    private DescriptionComponent createDescriptionComponent() {
        return new DescriptionComponent();
    }

    private JSplitPane createSplitPane(Component lower) {
        JSplitPane pane = new JSplitPane();
        this.firstSplit = this.firstSplit == null ? Boolean.TRUE : Boolean.FALSE;
        pane.setRightComponent(lower);
        pane.setOrientation(0);
        pane.setContinuousLayout(true);
        pane.setResizeWeight(1.0);
        pane.setDividerLocation(0.6499999761581421);
        pane.setBorder(BorderFactory.createEmptyBorder());
        pane.getActionMap().getParent().remove("toggleFocus");
        if (PropUtils.isAqua) {
            pane.setBackground(UIManager.getColor("NbExplorerView.background"));
        }
        return pane;
    }

    private JScrollPane createScrollPane(Component inner) {
        JScrollPane result = new JScrollPane(inner);
        JViewport vp = result.getViewport();
        vp.addMouseListener(this);
        MarginViewportUI ui = (MarginViewportUI)MarginViewportUI.createUI(vp);
        vp.setUI(ui);
        ui.setMarginPainted(this.marginPainted);
        ui.setMarginColor(this.marginColor);
        ui.setEmptyString(this.emptyString);
        result.setBorder(BorderFactory.createEmptyBorder());
        result.setViewportBorder(result.getBorder());
        vp.setBackground(UIManager.getColor("PropSheet.panelBg"));
        return result;
    }

    private JComponent createTabbedContainer() {
        JComponent result = TabbedContainerBridge.getDefault().createTabbedContainer();
        result.putClientProperty("tc", Boolean.TRUE);
        this.configureTabbedContainer(this.tabbedContainerObjects, this.tabbedContainerTitles, result);
        if (this.selectionListener != null) {
            TabbedContainerBridge.getDefault().attachSelectionListener(result, this.selectionListener);
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setTabbedContainerItems(Object[] o, String[] s) {
        block4 : {
            this.adjusting = true;
            try {
                this.tabbedContainerObjects = o;
                this.tabbedContainerTitles = s;
                if (o.length == 0) {
                    int newState = (this.getState() & 1) != 0 ? 1 : 0;
                    this.setState(newState);
                    break block4;
                }
                this.configureTabbedContainer(o, s, null);
            }
            finally {
                this.adjusting = false;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setTabbedContainerSelection(Object item) {
        JComponent tabbed = this.findTabbedContainer();
        if (tabbed != null) {
            this.adjusting = true;
            try {
                TabbedContainerBridge.getDefault().setSelectedItem(tabbed, item);
            }
            finally {
                this.adjusting = false;
            }
        }
    }

    public Object getTabbedContainerSelection() {
        Object o;
        JComponent tabbed = this.findTabbedContainer();
        if (tabbed != null && (o = TabbedContainerBridge.getDefault().getSelectedItem(tabbed)) instanceof Node.PropertySet[]) {
            return o;
        }
        return null;
    }

    private void configureTabbedContainer(Object[] o, String[] s, JComponent cont) {
        if (cont == null) {
            cont = this.findTabbedContainer();
        }
        if (cont != null) {
            TabbedContainerBridge.getDefault().setItems(cont, o, s);
        }
    }

    private void setTabbedContainerInnerComponent(JComponent tabbed, JComponent comp) {
        if (tabbed == null) {
            tabbed = this.findTabbedContainer();
        }
        TabbedContainerBridge.getDefault().setInnerComponent(tabbed, comp);
    }

    private static JComponent getTabbedContainerInnerComponent(JComponent c) {
        JComponent result = TabbedContainerBridge.getDefault().getInnerComponent(c);
        return result;
    }

    public void addSelectionChangeListener(ChangeListener l) {
        if (this.selectionListener != l) {
            JComponent comp = this.findTabbedContainer();
            this.selectionListener = l;
            if (comp != null) {
                TabbedContainerBridge.getDefault().attachSelectionListener(comp, l);
            }
        }
    }

    private static boolean isTabbedContainer(Component comp) {
        return comp instanceof JComponent && Boolean.TRUE.equals(((JComponent)comp).getClientProperty("tc"));
    }

    private DescriptionComponent findDescriptionComponent() {
        return (DescriptionComponent)PSheet.findChildOfClass(this.findSplitPane(), DescriptionComponent.class);
    }

    private JScrollPane findScrollPane() {
        JScrollPane result = (JScrollPane)PSheet.findChildOfClass(this, JScrollPane.class);
        if (result == null && (result = (JScrollPane)PSheet.findChildOfClass(this.findTabbedContainer(), JScrollPane.class)) == null) {
            result = (JScrollPane)PSheet.findChildOfClass(this.findSplitPane(), JScrollPane.class);
        }
        return result;
    }

    private JSplitPane findSplitPane() {
        JSplitPane result = (JSplitPane)PSheet.findChildOfClass(this, JSplitPane.class);
        if (result == null) {
            result = (JSplitPane)PSheet.findChildOfClass(this.findTabbedContainer(), JSplitPane.class);
        }
        return result;
    }

    private JComponent findTabbedContainer() {
        Component[] c = this.getComponents();
        for (int i = 0; i < c.length; ++i) {
            if (!(c[i] instanceof JComponent) || !Boolean.TRUE.equals(((JComponent)c[i]).getClientProperty("tc"))) continue;
            return (JComponent)c[i];
        }
        return null;
    }

    private static Component findChildOfClass(Container container, Class clazz) {
        if (container == null) {
            return null;
        }
        if (PSheet.isTabbedContainer((JComponent)container)) {
            JComponent c = PSheet.getTabbedContainerInnerComponent((JComponent)container);
            if (c != null && c.getClass() == clazz) {
                return c;
            }
        } else {
            Component[] c = container.getComponents();
            for (int i = 0; i < c.length; ++i) {
                if (clazz != c[i].getClass()) continue;
                return c[i];
            }
        }
        return null;
    }

    protected void popupRequested(Point p) {
        PropertySheet ps = (PropertySheet)SwingUtilities.getAncestorOfClass(PropertySheet.class, this);
        if (ps != null) {
            ps.showPopup(p);
        }
    }

    protected void helpRequested() {
        PropertySheet ps = (PropertySheet)SwingUtilities.getAncestorOfClass(PropertySheet.class, this);
        if (ps != null) {
            ps.helpAction.actionPerformed(new ActionEvent(this, 1001, "invokeHelp"));
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.isPopupTrigger()) {
            Point p = SwingUtilities.convertPoint((Component)e.getSource(), e.getPoint(), this);
            this.updateSheetTableSelection(e);
            this.popupRequested(p);
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.isPopupTrigger()) {
            Point p = SwingUtilities.convertPoint((Component)e.getSource(), e.getPoint(), this);
            this.updateSheetTableSelection(e);
            this.popupRequested(p);
        }
    }

    private void updateSheetTableSelection(MouseEvent e) {
        Component comp = (Component)e.getSource();
        if (comp instanceof SheetTable) {
            SheetTable table = (SheetTable)comp;
            table.changeSelection(table.rowAtPoint(e.getPoint()), 0, false, false);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    private class SwitchTabAction
    extends AbstractAction {
        private int increment;

        public SwitchTabAction(int increment) {
            this.increment = increment;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            JComponent tabbed = PSheet.this.findTabbedContainer();
            if (null == tabbed) {
                return;
            }
            Object o = TabbedContainerBridge.getDefault().getSelectedItem(tabbed);
            if (null == o) {
                return;
            }
            Object[] items = TabbedContainerBridge.getDefault().getItems(tabbed);
            int currentIndex = -1;
            for (int i = 0; null != items && i < items.length; ++i) {
                if (!items[i].equals(o)) continue;
                currentIndex = i;
                break;
            }
            if (currentIndex < 0) {
                return;
            }
            int newIndex = currentIndex + this.increment;
            if (newIndex < 0) {
                newIndex = items.length - 1;
            }
            if (newIndex >= items.length) {
                newIndex = 0;
            }
            TabbedContainerBridge.getDefault().setSelectedItem(tabbed, items[newIndex]);
        }
    }

    private class PopupAction
    extends AbstractAction {
        private PopupAction() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            PSheet.this.popupRequested(new Point(0, 0));
        }
    }

}

