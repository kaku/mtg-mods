/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.nodes.NodeAdapter
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 */
package org.openide.explorer.propertysheet;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeAdapter;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.Sheet;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;

final class ProxyNode
extends AbstractNode {
    private static final int MAX_NAMES = 2;
    private volatile Node[] original;
    private volatile ArrayList<Node.PropertySet[]> originalPropertySets;
    private NodeListener nl;
    private NodeListener pcl;
    String displayName = null;
    private String shortDescription = null;

    /* varargs */ ProxyNode(Node ... original) {
        super(Children.LEAF);
        this.original = original;
        this.nl = new NodeAdapterImpl(true);
        this.pcl = new NodeAdapterImpl(false);
        for (int i = 0; i < original.length; ++i) {
            original[i].addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.pcl, (Object)original[i]));
            original[i].addNodeListener((NodeListener)WeakListeners.create(NodeListener.class, (EventListener)this.nl, (Object)original[i]));
        }
    }

    public HelpCtx getHelpCtx() {
        for (int i = 0; i < this.original.length; ++i) {
            if (this.original[i].getHelpCtx() == HelpCtx.DEFAULT_HELP) continue;
            return this.original[i].getHelpCtx();
        }
        return HelpCtx.DEFAULT_HELP;
    }

    public Node cloneNode() {
        return new ProxyNode(this.original);
    }

    protected Sheet createSheet() {
        Sheet sheet = super.createSheet();
        Sheet.Set[] computedSet = this.computePropertySets();
        for (int i = 0; i < computedSet.length; ++i) {
            sheet.put(computedSet[i]);
        }
        return sheet;
    }

    Node[] getOriginalNodes() {
        return this.original;
    }

    public String getDisplayName() {
        if (this.displayName == null) {
            this.displayName = this.getConcatenatedName(2);
        }
        return this.displayName;
    }

    private String getConcatenatedName(int limit) {
        Node[] n = this.getOriginalNodes();
        StringBuffer name = new StringBuffer();
        String delim = NbBundle.getMessage(ProxyNode.class, (String)"CTL_List_Delimiter");
        for (int i = 0; i < n.length; ++i) {
            name.append(n[i].getDisplayName());
            if (i != n.length - 1) {
                name.append(delim);
            }
            if (i < limit || i == n.length - 1) continue;
            name.append(NbBundle.getMessage(ProxyNode.class, (String)"MSG_ELLIPSIS"));
            break;
        }
        return name.toString();
    }

    public String getShortDescription() {
        if (this.getOriginalNodes().length < 2) {
            return NbBundle.getMessage(ProxyNode.class, (String)"CTL_Multiple_Selection");
        }
        if (this.shortDescription == null) {
            this.shortDescription = this.getConcatenatedName(Integer.MAX_VALUE);
        }
        return this.shortDescription;
    }

    private ArrayList<Node.PropertySet[]> getOriginalPropertySets(Node[] forWhat) {
        if (null == this.originalPropertySets) {
            ArrayList<Node.PropertySet[]> arr = new ArrayList<Node.PropertySet[]>(forWhat.length);
            for (int i = 0; i < forWhat.length; ++i) {
                Node.PropertySet[] p = forWhat[i].getPropertySets();
                arr.add(p);
            }
            if (this.original == forWhat) {
                this.originalPropertySets = arr;
            }
            return arr;
        }
        return this.originalPropertySets;
    }

    private Sheet.Set[] computePropertySets() {
        Node[] copy = this.original;
        if (copy.length > 0) {
            ArrayList<Node.PropertySet[]> ops = this.getOriginalPropertySets(copy);
            if (ops.isEmpty()) {
                return new Sheet.Set[0];
            }
            Node.PropertySet[] firstSet = ops.get(0);
            HashSet<Node.PropertySet> sheets = new HashSet<Node.PropertySet>(Arrays.asList(firstSet));
            for (int i = 1; i < ops.size(); ++i) {
                sheets.retainAll(new HashSet<Object>(Arrays.asList((Object[])ops.get(i))));
            }
            ArrayList<Sheet.Set> resultSheets = new ArrayList<Sheet.Set>(sheets.size());
            for (int i2 = 0; i2 < firstSet.length; ++i2) {
                if (!sheets.contains((Object)firstSet[i2]) || firstSet[i2].isHidden()) continue;
                Node.PropertySet current = firstSet[i2];
                Sheet.Set res = new Sheet.Set();
                res.setName(current.getName());
                res.setDisplayName(current.getDisplayName());
                res.setShortDescription(current.getShortDescription());
                String tabName = (String)current.getValue("tabName");
                if (tabName != null) {
                    res.setValue("tabName", (Object)tabName);
                }
                HashSet<Node.Property> props = new HashSet<Node.Property>(Arrays.asList(current.getProperties()));
                Object propsHelpID = null;
                for (int j = 0; j < ops.size(); ++j) {
                    Node.PropertySet[] p = ops.get(j);
                    for (int k = 0; k < p.length; ++k) {
                        String pkn;
                        String cn = current == null ? null : current.getName();
                        String string = pkn = p[k] == null ? null : p[k].getName();
                        if (cn == null || !cn.equals(pkn)) continue;
                        props.retainAll(new HashSet<Node.Property>(Arrays.asList(p[k].getProperties())));
                    }
                }
                Node.Property[] p = current.getProperties();
                for (int j2 = 0; j2 < p.length; ++j2) {
                    if (!props.contains((Object)p[j2]) || p[j2].isHidden()) continue;
                    ProxyProperty pp = this.createProxyProperty(copy, p[j2].getName(), res.getName());
                    res.put((Node.Property)pp);
                }
                resultSheets.add(res);
            }
            return resultSheets.toArray((T[])new Sheet.Set[resultSheets.size()]);
        }
        return new Sheet.Set[0];
    }

    private ProxyProperty createProxyProperty(Node[] copy, String propName, String setName) {
        Node.Property[] arr = new Node.Property[copy.length];
        for (int i = 0; i < copy.length; ++i) {
            Node.PropertySet[] p = this.getOriginalPropertySets(copy).get(i);
            for (int j = 0; j < p.length; ++j) {
                if (!Utilities.compareObjects((Object)setName, (Object)p[j].getName())) continue;
                Node.Property[] np = p[j].getProperties();
                for (int k = 0; k < np.length; ++k) {
                    if (!np[k].getName().equals(propName)) continue;
                    arr[i] = np[k];
                }
            }
        }
        return new ProxyProperty(arr);
    }

    private class NodeAdapterImpl
    extends NodeAdapter {
        private final boolean nodeListener;

        public NodeAdapterImpl(boolean b) {
            this.nodeListener = b;
        }

        public void propertyChange(PropertyChangeEvent pce) {
            if (this.nodeListener) {
                this.nodePropertyChange(pce);
            } else {
                this.realPropertyChange(pce);
            }
        }

        private void nodePropertyChange(PropertyChangeEvent pce) {
            String nm = pce.getPropertyName();
            if ("cookie".equals(nm)) {
                ProxyNode.this.fireCookieChange();
            } else if ("displayName".equals(nm)) {
                ProxyNode.this.displayName = null;
                ProxyNode.this.fireDisplayNameChange((String)pce.getOldValue(), ProxyNode.this.getDisplayName());
            } else if ("icon".equals(nm)) {
                ProxyNode.this.fireIconChange();
            } else if ("openedIcon".equals(nm)) {
                ProxyNode.this.fireOpenedIconChange();
            } else if ("name".equals(nm)) {
                ProxyNode.this.fireNameChange((String)pce.getOldValue(), ProxyNode.this.getName());
            } else if ("propertySets".equals(nm)) {
                Node.PropertySet[] old = ProxyNode.this.getPropertySets();
                ProxyNode.this.setSheet(ProxyNode.this.createSheet());
                ProxyNode.this.firePropertySetsChange(old, ProxyNode.this.getPropertySets());
            } else if ("shortDescription".equals(nm)) {
                ProxyNode.this.fireShortDescriptionChange((String)pce.getOldValue(), ProxyNode.this.getShortDescription());
            } else if ("leaf".equals(nm) || "parentNode".equals(nm)) {
                // empty if block
            }
        }

        public void nodeDestroyed(NodeEvent ev) {
            int idx = Arrays.asList(ProxyNode.this.original).indexOf((Object)((Node)ev.getSource()));
            if (idx != -1) {
                HashSet<Node> set = new HashSet<Node>(Arrays.asList(ProxyNode.this.original));
                set.remove(ev.getSource());
                ProxyNode.this.original = set.toArray((T[])new Node[0]);
                if (set.size() == 0) {
                    ProxyNode.this.fireNodeDestroyed();
                }
            }
        }

        private void realPropertyChange(PropertyChangeEvent pce) {
            String nm = pce.getPropertyName();
            Node.PropertySet[] pss = ProxyNode.this.getPropertySets();
            boolean exists = false;
            for (int i = 0; i < pss.length && !exists; ++i) {
                Node.Property[] ps = pss[i].getProperties();
                for (int j = 0; j < ps.length && !exists; ++j) {
                    if (!ps[j].getName().equals(nm)) continue;
                    exists = true;
                }
            }
            if (exists) {
                ProxyNode.this.firePropertyChange(pce.getPropertyName(), pce.getOldValue(), pce.getNewValue());
            }
        }
    }

    static class DifferentValuesException
    extends RuntimeException {
        public DifferentValuesException() {
        }

        public DifferentValuesException(String message) {
            super(message);
        }
    }

    static final class ProxyProperty
    extends Node.Property {
        private Node.Property[] original;

        public ProxyProperty(Node.Property[] original) {
            super(original[0].getValueType());
            this.original = original;
            this.setName(original[0].getName());
            this.setDisplayName(original[0].getDisplayName());
            this.setShortDescription(original[0].getShortDescription());
        }

        public boolean canWrite() {
            for (int i = 0; i < this.original.length; ++i) {
                if (this.original[i].canWrite()) continue;
                return false;
            }
            return true;
        }

        public boolean canRead() {
            for (int i = 0; i < this.original.length; ++i) {
                if (this.original[i].canRead()) continue;
                return false;
            }
            return true;
        }

        public Object getValue() throws IllegalAccessException, InvocationTargetException {
            Object o = this.original[0].getValue();
            for (int i = 0; i < this.original.length; ++i) {
                if (ProxyProperty.equals(o, this.original[i].getValue())) continue;
                throw new DifferentValuesException();
            }
            return o;
        }

        static boolean equals(Object a, Object b) {
            boolean bothNull;
            boolean nullMismatch;
            boolean aIsNull = a == null;
            boolean bIsNull = b == null;
            boolean bl = bothNull = aIsNull && aIsNull == bIsNull;
            if (bothNull) {
                return true;
            }
            boolean bl2 = nullMismatch = aIsNull != bIsNull;
            if (nullMismatch) {
                return false;
            }
            return a.equals(b);
        }

        public void setValue(Object val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            for (int i = 0; i < this.original.length; ++i) {
                this.original[i].setValue(val);
            }
        }

        public Object getValue(String attributeName) {
            Object o = this.original[0].getValue(attributeName);
            if (Boolean.FALSE.equals(o)) {
                return o;
            }
            if (o == null) {
                return null;
            }
            for (int i = 1; i < this.original.length; ++i) {
                if (Boolean.FALSE.equals((Object)this.original[i])) {
                    return this.original[i];
                }
                if (o.equals(this.original[i].getValue(attributeName))) continue;
                if (Boolean.getBoolean("netbeans.ps.logDifferentValues")) {
                    Logger.getLogger(ProxyNode.class.getName()).log(Level.WARNING, null, new DifferentValuesException("Different values in attribute " + attributeName + " for proxy property " + this.getDisplayName() + "(" + (Object)((Object)this) + ") first value=" + o + " property " + i + "(" + this.original[i].getClass().getName() + " returns " + this.original[i].getValue(attributeName)));
                }
                return null;
            }
            return o;
        }

        public void setValue(String attributeName, Object value) {
            for (int i = 0; i < this.original.length; ++i) {
                this.original[i].setValue(attributeName, value);
            }
        }

        public PropertyEditor getPropertyEditor() {
            return this.original[0].getPropertyEditor();
        }

        public boolean supportsDefaultValue() {
            for (int i = 0; i < this.original.length; ++i) {
                if (this.original[i].supportsDefaultValue()) continue;
                return false;
            }
            return true;
        }

        public void restoreDefaultValue() throws IllegalAccessException, InvocationTargetException {
            for (int i = 0; i < this.original.length; ++i) {
                this.original[i].restoreDefaultValue();
            }
        }

        public String toString() {
            StringBuffer sb = new StringBuffer("Proxy property for: ");
            sb.append(this.getDisplayName());
            sb.append('[');
            for (int i = 0; i < this.original.length; ++i) {
                sb.append(this.original[i].getClass().getName());
                if (i >= this.original.length - 1) continue;
                sb.append(',');
            }
            sb.append(']');
            return sb.toString();
        }
    }

}

