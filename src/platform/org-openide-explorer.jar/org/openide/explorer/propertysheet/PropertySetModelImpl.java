/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.util.NbBundle
 */
package org.openide.explorer.propertysheet;

import java.beans.FeatureDescriptor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertySetModel;
import org.openide.explorer.propertysheet.PropertySetModelEvent;
import org.openide.explorer.propertysheet.PropertySetModelListener;
import org.openide.explorer.propertysheet.PropertySheet;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

class PropertySetModelImpl
implements PropertySetModel,
Runnable {
    private static boolean filterHiddenProperties = !Boolean.getBoolean("netbeans.ps.showHiddenProperties");
    static Set<String> closedSets = new HashSet<String>(5);
    private boolean[] expanded = null;
    private List<FeatureDescriptor> fds = new ArrayList<FeatureDescriptor>();
    private Comparator<Node.Property> comparator = null;
    private transient List<PropertySetModelListener> listenerList;
    private Node.PropertySet[] sets = null;
    private transient int setCount = 0;
    private transient PropertySetModelEvent event = null;

    public PropertySetModelImpl() {
    }

    public PropertySetModelImpl(Node.PropertySet[] ps) {
        if (ps == null) {
            ps = new Node.PropertySet[]{};
        }
        this.setPropertySets(ps);
    }

    @Override
    public int getCount() {
        return this.fds.size();
    }

    @Override
    public FeatureDescriptor getFeatureDescriptor(int index) {
        if (index == -1 || index >= this.fds.size()) {
            return null;
        }
        return this.fds.get(index);
    }

    @Override
    public int indexOf(FeatureDescriptor fd) {
        return this.fds.indexOf(fd);
    }

    @Override
    public boolean isProperty(int index) {
        return this.getFeatureDescriptor(index) instanceof Node.Property;
    }

    @Override
    public void setComparator(Comparator<Node.Property> c) {
        if (c != this.comparator) {
            this.firePendingChange(true);
            this.comparator = c;
            this.fds.clear();
            this.init();
            this.fireChange(true);
        }
    }

    @Override
    public void setPropertySets(Node.PropertySet[] sets) {
        int n = this.setCount = sets == null ? 0 : sets.length;
        if (sets == null) {
            sets = new Node.PropertySet[]{};
        }
        if (this.setCount == 0 && !SwingUtilities.isEventDispatchThread()) {
            this.sets = new Node.PropertySet[0];
            this.resetArray(sets);
            this.fds.clear();
            SwingUtilities.invokeLater(this);
            return;
        }
        this.firePendingChange(false);
        this.sets = sets;
        this.resetArray(sets);
        this.init();
        this.run();
    }

    @Override
    public void run() {
        this.fireChange(false);
    }

    private void init() {
        this.fds.clear();
        if (this.comparator == null) {
            this.initExpandable();
        } else {
            this.initPlain();
        }
    }

    private void initPlain() {
        if (this.sets == null) {
            return;
        }
        int pcount = 0;
        for (int i = 0; i < this.sets.length; ++i) {
            Node.Property[] p = this.sets[i].getProperties();
            if (p == null) {
                throw new NullPointerException("Null is not a legal return value for PropertySet.getProperties()");
            }
            pcount += p.length;
        }
        Node.Property[] props = new Node.Property[pcount];
        int l = 0;
        for (int i2 = 0; i2 < this.sets.length; ++i2) {
            Node.Property[] p = this.sets[i2].getProperties();
            System.arraycopy(p, 0, props, l, p.length);
            l += p.length;
        }
        Arrays.sort(props, this.comparator);
        this.fds.addAll(this.propsToList(props));
    }

    private void initExpandable() {
        if (this.sets == null || this.sets.length == 0) {
            return;
        }
        for (int i = 0; i < this.sets.length; ++i) {
            if (PropUtils.hideSingleExpansion) {
                if (this.sets.length > 1 || this.sets.length == 1 && !NbBundle.getMessage(PropertySetModelImpl.class, (String)"CTL_PropertiesNoMnemonic").equals(this.sets[0].getDisplayName())) {
                    this.fds.add((FeatureDescriptor)this.sets[i]);
                }
            } else if (!PropertySheet.forceTabs) {
                this.fds.add((FeatureDescriptor)this.sets[i]);
            }
            if (!this.expanded[i]) continue;
            Node.Property[] p = this.sets[i].getProperties();
            if (p == null) {
                throw new NullPointerException("Null is not a legal return value for PropertySet.getProperties()");
            }
            if (p.length > 0) {
                this.fds.addAll(this.propsToList(p));
                continue;
            }
            this.fds.remove((Object)this.sets[i]);
        }
    }

    private List<Node.Property> propsToList(Node.Property[] p) {
        List<Node.Property> result;
        if (filterHiddenProperties) {
            result = new ArrayList<Node.Property>();
            for (int i = 0; i < p.length; ++i) {
                if (p[i].isHidden()) continue;
                result.add(p[i]);
            }
        } else {
            result = Arrays.asList(p);
        }
        return result;
    }

    private void resetArray(Node.PropertySet[] sets) {
        int size = sets.length;
        if (this.expanded == null || this.expanded.length < size) {
            this.expanded = new boolean[size];
        }
        for (int i = 0; i < sets.length; ++i) {
            this.expanded[i] = !closedSets.contains(sets[i].getDisplayName());
        }
    }

    private int lookupSet(FeatureDescriptor fd) {
        if (this.sets != null) {
            List<Node.PropertySet> l = Arrays.asList(this.sets);
            return l.indexOf(fd);
        }
        return -1;
    }

    @Override
    public boolean isExpanded(FeatureDescriptor set) {
        int index = this.lookupSet(set);
        if (index == -1) {
            return false;
        }
        return this.expanded[index];
    }

    @Override
    public void toggleExpanded(int index) {
        FeatureDescriptor fd = this.getFeatureDescriptor(index);
        if (fd instanceof Node.Property) {
            throw new IllegalArgumentException("Cannot expand a property.");
        }
        int setIndex = this.lookupSet(fd);
        int eventType = this.expanded[setIndex] ? 0 : 1;
        List<Node.Property> props = this.propsToList(this.sets[setIndex].getProperties());
        int len = props.size();
        this.expanded[setIndex] = !this.expanded[setIndex];
        this.firePendingChange(eventType, index + 1, index + len, false);
        if (!this.expanded[setIndex]) {
            closedSets.add(fd.getDisplayName());
        } else {
            closedSets.remove(fd.getDisplayName());
        }
        if (this.expanded[setIndex]) {
            this.fds.addAll(index + 1, props);
        } else {
            for (int i = index + len; i > index; --i) {
                this.fds.remove(i);
            }
        }
        this.fireChange(eventType, index + 1, index + len);
        PropUtils.putSavedClosedSetNames(closedSets);
    }

    @Override
    public final void addPropertySetModelListener(PropertySetModelListener listener) {
        if (this.listenerList == null) {
            this.listenerList = new ArrayList<PropertySetModelListener>();
        }
        this.listenerList.add(listener);
    }

    @Override
    public final void removePropertySetModelListener(PropertySetModelListener listener) {
        if (this.listenerList != null) {
            this.listenerList.remove(listener);
        }
    }

    private PropertySetModelEvent getEvent() {
        if (this.event == null) {
            this.event = new PropertySetModelEvent(this);
        }
        return this.event;
    }

    private final void firePendingChange(int type, int start, int end, boolean reordering) {
        if (this.listenerList == null) {
            return;
        }
        Iterator<PropertySetModelListener> i = this.listenerList.iterator();
        this.getEvent().type = 2;
        this.event.start = start;
        this.event.end = end;
        this.event.type = type;
        this.event.reordering = reordering;
        while (i.hasNext()) {
            PropertySetModelListener curr = i.next();
            curr.pendingChange(this.event);
        }
    }

    private final void fireChange(boolean reordering) {
        if (this.listenerList == null) {
            return;
        }
        Iterator<PropertySetModelListener> i = this.listenerList.iterator();
        this.getEvent().type = 2;
        this.event.reordering = reordering;
        while (i.hasNext()) {
            PropertySetModelListener curr = i.next();
            curr.wholesaleChange(this.event);
        }
    }

    private final void firePendingChange(boolean reordering) {
        if (this.listenerList == null) {
            return;
        }
        Iterator<PropertySetModelListener> i = this.listenerList.iterator();
        this.getEvent().type = 2;
        this.event.reordering = reordering;
        while (i.hasNext()) {
            PropertySetModelListener curr = i.next();
            curr.pendingChange(this.event);
        }
    }

    private final void fireChange(int type, int start, int end) {
        if (this.listenerList == null) {
            return;
        }
        this.getEvent().start = start;
        this.event.end = end;
        this.event.type = type;
        this.event.reordering = false;
        for (PropertySetModelListener curr : this.listenerList) {
            curr.boundedChange(this.event);
        }
    }

    @Override
    public Comparator getComparator() {
        return this.comparator;
    }

    @Override
    public int getSetCount() {
        return this.setCount;
    }

    static {
        closedSets.addAll(Arrays.asList(PropUtils.getSavedClosedSetNames()));
    }
}

