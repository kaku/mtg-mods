/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.Exceptions
 */
package org.openide.explorer.propertysheet;

import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.lang.reflect.InvocationTargetException;
import javax.swing.SwingUtilities;
import org.openide.explorer.propertysheet.ExPropertyModel;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.ProxyNode;
import org.openide.explorer.propertysheet.ReusablePropertyEnv;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

class ReusablePropertyModel
implements ExPropertyModel {
    static final boolean DEBUG = Boolean.getBoolean("netbeans.reusable.strictthreads");
    private transient Node.Property PROPERTY = null;
    private final ReusablePropertyEnv env;

    public ReusablePropertyModel(ReusablePropertyEnv env) {
        this.env = env;
        env.setReusablePropertyModel(this);
    }

    void clear() {
        this.PROPERTY = null;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener l) {
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener l) {
    }

    public PropertyEditor getPropertyEditor() {
        Node.Property p;
        return PropUtils.getPropertyEditor(p, !PropUtils.isExternallyEdited(p = this.getProperty()));
    }

    @Override
    public Class getPropertyEditorClass() {
        if (DEBUG) {
            ReusablePropertyModel.checkThread();
        }
        return this.getPropertyEditor().getClass();
    }

    @Override
    public Class getPropertyType() {
        if (DEBUG) {
            ReusablePropertyModel.checkThread();
        }
        return null == this.getProperty() ? Object.class : this.getProperty().getValueType();
    }

    @Override
    public Object getValue() throws InvocationTargetException {
        if (DEBUG) {
            ReusablePropertyModel.checkThread();
        }
        try {
            return this.getProperty().getValue();
        }
        catch (IllegalAccessException iae) {
            Exceptions.printStackTrace((Throwable)iae);
            return null;
        }
    }

    @Override
    public void setValue(Object v) throws InvocationTargetException {
        if (DEBUG) {
            ReusablePropertyModel.checkThread();
        }
        try {
            this.getProperty().setValue(v);
        }
        catch (IllegalAccessException iae) {
            Exceptions.printStackTrace((Throwable)iae);
        }
    }

    @Override
    public Object[] getBeans() {
        if (DEBUG) {
            ReusablePropertyModel.checkThread();
        }
        if (this.env.getNode() instanceof ProxyNode) {
            return ((ProxyNode)((Object)this.env.getNode())).getOriginalNodes();
        }
        return new Object[]{this.env.getNode()};
    }

    @Override
    public FeatureDescriptor getFeatureDescriptor() {
        if (DEBUG) {
            ReusablePropertyModel.checkThread();
        }
        return this.getProperty();
    }

    static void checkThread() {
        if (!SwingUtilities.isEventDispatchThread()) {
            throw new IllegalStateException("Reusable property model accessed from off the AWT thread.");
        }
    }

    public Node.Property getProperty() {
        return this.PROPERTY;
    }

    public void setProperty(Node.Property PROPERTY) {
        this.PROPERTY = PROPERTY;
    }
}

