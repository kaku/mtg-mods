/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$IndexedProperty
 *  org.openide.nodes.Node$Property
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 */
package org.openide.explorer.propertysheet;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.image.BufferedImage;
import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.beans.PropertyEditorManager;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleRole;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.KeyStroke;
import javax.swing.SpinnerModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.SplitPaneUI;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import javax.swing.plaf.metal.MetalLookAndFeel;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.explorer.propertysheet.Boolean3WayEditor;
import org.openide.explorer.propertysheet.ButtonPanel;
import org.openide.explorer.propertysheet.CleanComboUI;
import org.openide.explorer.propertysheet.DefaultPropertyModel;
import org.openide.explorer.propertysheet.DescriptionComponent;
import org.openide.explorer.propertysheet.EnumPropertyEditor;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.IconPanel;
import org.openide.explorer.propertysheet.IncrementPropertyValueSupport;
import org.openide.explorer.propertysheet.IndexedPropertyEditor;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.NodePropertyModel;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;
import org.openide.explorer.propertysheet.PropertySetModel;
import org.openide.explorer.propertysheet.PropertySheet;
import org.openide.explorer.propertysheet.ProxyNode;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

final class PropUtils {
    static final boolean noCustomButtons = Boolean.getBoolean("netbeans.ps.noCustomButtons");
    static boolean forceRadioButtons = Boolean.getBoolean("netbeans.ps.forceRadioButtons");
    static final boolean noCheckboxCaption = !Boolean.getBoolean("netbeans.ps.checkboxCaption");
    static final boolean hideSingleExpansion = Boolean.getBoolean("netbeans.ps.hideSingleExpansion");
    static final boolean neverMargin = true;
    static final boolean psCommitOnFocusLoss = !Boolean.getBoolean("netbeans.ps.NoCommitOnFocusLoss");
    static final boolean psNoHelpButton = Boolean.getBoolean("netbeans.ps.noHelpButton");
    private static final String KEY_ALTBG = "Tree.altbackground";
    private static final String KEY_SETBG = "PropSheet.setBackground";
    private static final String KEY_SELSETBG = "PropSheet.selectedSetBackground";
    private static final String KEY_SETFG = "PropSheet.setForeground";
    private static final String KEY_SELSETFG = "PropSheet.selectedSetForeground";
    private static final String KEY_ICONMARGIN = "netbeans.ps.iconmargin";
    static final String KEY_ROWHEIGHT = "netbeans.ps.rowheight";
    private static final String PREF_KEY_SHOWDESCRIPTION = "showDescriptionArea";
    private static final String PREF_KEY_CLOSEDSETNAMES = "closedSetNames";
    private static final String PREF_KEY_SORTORDER = "sortOrder";
    static Color disFg = null;
    static float fsfactor = -1.0f;
    static int minW = -1;
    static int minH = -1;
    private static Color tfFg = null;
    private static Color tfBg = null;
    static Boolean noAltBg = null;
    static int marginWidth = -1;
    private static int iconMargin = -1;
    static Color selectedSetRendererColor = null;
    static Color setRendererColor = null;
    static int spinnerHeight = -1;
    static Color controlColor = null;
    static Color shadowColor = null;
    static Color altBg = null;
    private static String bptn = null;
    private static Comparator comp = null;
    private static int textMargin = -1;
    private static Color setForegroundColor = null;
    private static Color selectedSetForegroundColor = null;
    private static final Logger LOG = Logger.getLogger(PropUtils.class.getName());
    private static Boolean useOptimizedCustomButtonPainting = null;
    static final boolean isAqua = "Aqua".equals(UIManager.getLookAndFeel().getID());
    static final boolean isGtk = "GTK".equals(UIManager.getLookAndFeel().getID());
    static final boolean isNimbus = "Nimbus".equals(UIManager.getLookAndFeel().getID());
    private static Graphics scratchGraphics = null;
    private static final Comparator<Node.Property> SORTER_TYPE = new Comparator<Node.Property>(){

        @Override
        public int compare(Node.Property l, Node.Property r) {
            Class t2;
            String s2;
            Class t1 = l.getValueType();
            String s1 = t1 != null ? t1.getName() : "";
            int s = s1.compareToIgnoreCase(s2 = (t2 = r.getValueType()) != null ? t2.getName() : "");
            if (s != 0) {
                return s;
            }
            s1 = l.getDisplayName();
            s2 = r.getDisplayName();
            return s1.compareToIgnoreCase(s2);
        }

        public String toString() {
            return "Type comparator";
        }
    };
    private static final Comparator<Node.Property> SORTER_NAME = new Comparator<Node.Property>(){

        @Override
        public int compare(Node.Property l, Node.Property r) {
            String s1 = l.getDisplayName();
            String s2 = r.getDisplayName();
            return String.CASE_INSENSITIVE_ORDER.compare(s1, s2);
        }

        public String toString() {
            return "Name comparator";
        }
    };
    private static List<String> missing = null;
    private static Set<Node.Property> externallyEdited = new HashSet<Node.Property>(3);

    private static Preferences preferences() {
        return NbPreferences.forModule(PropUtils.class);
    }

    private PropUtils() {
    }

    static boolean useOptimizedCustomButtonPainting() {
        if (useOptimizedCustomButtonPainting == null) {
            useOptimizedCustomButtonPainting = "Windows".equals(UIManager.getLookAndFeel().getID()) ? Boolean.valueOf(PropUtils.isXPTheme()) : Boolean.valueOf("Aqua".equals(UIManager.getLookAndFeel().getID()));
        }
        return useOptimizedCustomButtonPainting;
    }

    static void log(Class clazz, String msg, boolean dumpstack) {
        PropUtils.log(clazz, msg);
        if (dumpstack) {
            PropUtils.dumpStack(clazz);
        }
    }

    static void log(Class clazz, String msg) {
        Logger.getLogger(clazz.getName()).fine(msg);
    }

    static void log(Class clazz, FocusEvent fe) {
        if (PropUtils.isLoggable(clazz)) {
            StringBuffer sb = new StringBuffer(30);
            PropUtils.focusEventToString(fe, sb);
            PropUtils.log(clazz, sb.toString());
        }
    }

    static boolean isLoggable(Class clazz) {
        return Logger.getLogger(clazz.getName()).isLoggable(Level.FINE);
    }

    static void logFocusOwner(Class clazz, String where) {
        if (PropUtils.isLoggable(clazz)) {
            StringBuffer sb = new StringBuffer(where);
            sb.append(" focus owner: ");
            Component owner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            PropUtils.compToString(owner, sb);
        }
    }

    static void focusEventToString(FocusEvent fe, StringBuffer sb) {
        Component target = (Component)fe.getSource();
        Component opposite = fe.getOppositeComponent();
        sb.append(" focus ");
        sb.append(fe.getID() == 1004 ? " gained by " : " lost by ");
        PropUtils.compToString(target, sb);
        sb.append(fe.getID() == 1004 ? " from " : " to ");
        PropUtils.compToString(opposite, sb);
        sb.append(" isTemporary: ");
        sb.append(fe.isTemporary());
    }

    static void compToString(Component c, StringBuffer sb) {
        if (c == null) {
            sb.append(" null ");
            return;
        }
        String name = c.getName();
        Class clazz = c.getClass();
        String classname = clazz.getName();
        int i = classname.lastIndexOf(46);
        if (i != -1 && i != classname.length() - 1) {
            classname = classname.substring(i + 1);
        }
        if (name != null) {
            sb.append("\"");
            sb.append(name);
            sb.append("\" (");
            sb.append(classname);
            sb.append(") ");
        } else {
            sb.append(' ');
            sb.append(classname);
            sb.append(' ');
        }
        if (!c.isVisible()) {
            sb.append(" [NOT VISIBLE] ");
        }
        if (!c.isDisplayable()) {
            sb.append(" [HAS NO PARENT COMPONENT] ");
        }
    }

    public static void dumpStack(Class clazz) {
        if (Logger.getLogger(clazz.getName()).isLoggable(Level.FINE)) {
            StringWriter sw = new StringWriter();
            new Throwable().printStackTrace(new PrintWriter(sw));
            PropUtils.log(clazz, sw.getBuffer().toString());
        }
    }

    static Color getButtonColor() {
        return UIManager.getColor("netbeans.ps.buttonColor");
    }

    static int getCustomButtonWidth() {
        Icon ic = PropUtils.getCustomButtonIcon();
        return ic.getIconWidth() + (isAqua ? 5 : 3);
    }

    static boolean checkEnabled(Component c, PropertyEditor editor, PropertyEnv env) {
        if (editor instanceof NoPropertyEditorEditor) {
            return false;
        }
        if (env != null) {
            Boolean canEditAsText = (Boolean)env.getFeatureDescriptor().getValue("canEditAsText");
            if (!env.isEditable() || Boolean.FALSE.equals(canEditAsText)) {
                return false;
            }
        }
        return true;
    }

    static Graphics getScratchGraphics(Component c) {
        if (scratchGraphics == null) {
            scratchGraphics = new BufferedImage(1, 1, 1).getGraphics();
        }
        return scratchGraphics;
    }

    static Color getErrorColor() {
        Color result = UIManager.getColor("nb.errorForeground");
        if (result == null) {
            result = Color.RED;
        }
        return result;
    }

    static Color getDisabledForeground() {
        if (disFg == null && (PropUtils.disFg = UIManager.getColor("textInactiveText")) == null) {
            disFg = Color.GRAY;
        }
        return disFg;
    }

    static float getFontSizeFactor() {
        if (fsfactor == -1.0f) {
            Font f = UIManager.getFont("controlFont");
            if (f == null) {
                JLabel jl = new JLabel();
                f = jl.getFont();
            }
            int baseSize = 12;
            fsfactor = baseSize / f.getSize();
        }
        return fsfactor;
    }

    static int getMinimumPropPanelWidth() {
        if (minW == -1) {
            int base = 50;
            minW = Math.round((float)base * PropUtils.getFontSizeFactor());
        }
        return minW;
    }

    static int getMinimumPropPanelHeight() {
        if (minH == -1) {
            int base = 18;
            minH = Math.round((float)base * PropUtils.getFontSizeFactor());
        }
        return minH;
    }

    static Dimension getMinimumPanelSize() {
        return new Dimension(PropUtils.getMinimumPropPanelWidth(), PropUtils.getMinimumPropPanelHeight());
    }

    static boolean updateProp(PropertyModel mdl, PropertyEditor ed, String title) {
        Object newValue = ed.getValue();
        Object o = PropUtils.noDlgUpdateProp(mdl, ed);
        if (o instanceof Exception) {
            if (o instanceof InvocationTargetException) {
                o = ((InvocationTargetException)o).getTargetException();
            }
            PropUtils.processThrowable((Throwable)o, title, newValue);
        }
        boolean result = o instanceof Boolean ? (Boolean)o : false;
        return result;
    }

    static Object noDlgUpdateProp(PropertyModel mdl, PropertyEditor ed) {
        Serializable result2;
        Object newValue = ed.getValue();
        Serializable result2 = Boolean.FALSE;
        try {
            try {
                Object[] beans;
                Object oldValue = mdl.getValue();
                int selBeans = 0;
                if (mdl instanceof NodePropertyModel && null != (beans = ((NodePropertyModel)mdl).getBeans())) {
                    selBeans = beans.length;
                }
                if (newValue != null && !newValue.equals(oldValue) || newValue == null && (oldValue != null || selBeans > 1)) {
                    mdl.setValue(newValue);
                    result2 = Boolean.TRUE;
                }
            }
            catch (ProxyNode.DifferentValuesException dve) {
                mdl.setValue(newValue);
                result2 = Boolean.TRUE;
            }
        }
        catch (Exception e) {
            result2 = e;
        }
        return result2;
    }

    static Exception updatePropertyEditor(PropertyEditor ed, Object value) {
        Exception result = null;
        try {
            if (value instanceof String) {
                try {
                    ed.setAsText((String)value);
                }
                catch (IllegalArgumentException iaE) {
                    if (null == Exceptions.findLocalizedMessage((Throwable)iaE)) {
                        Exceptions.attachLocalizedMessage((Throwable)iaE, (String)NbBundle.getMessage(PropUtils.class, (String)"MSG_SetAsText_InvalidValue", (Object)value));
                    }
                    result = iaE;
                }
            } else {
                ed.setValue(value);
            }
        }
        catch (Exception e) {
            result = e;
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static boolean updateProp(InplaceEditor ine) {
        JComponent c = ine.getComponent();
        Cursor oldCursor = c.getCursor();
        try {
            boolean result;
            String newValString;
            c.setCursor(Cursor.getPredefinedCursor(3));
            Object o = ine.getValue();
            Exception e = PropUtils.updatePropertyEditor(ine.getPropertyEditor(), o);
            String string = newValString = o == null ? NbBundle.getMessage(PropUtils.class, (String)"NULL") : o.toString();
            if (e != null) {
                String propName;
                PropertyModel pm = ine.getPropertyModel();
                if (pm instanceof NodePropertyModel) {
                    Node.Property p = ((NodePropertyModel)pm).getProperty();
                    propName = p.getDisplayName();
                } else {
                    propName = pm instanceof DefaultPropertyModel ? ((DefaultPropertyModel)pm).propertyName : NbBundle.getMessage(PropUtils.class, (String)"MSG_unknown_property_name");
                }
                PropUtils.processThrowable(e, propName, newValString);
            }
            boolean propName = result = e == null ? PropUtils.updateProp(ine.getPropertyModel(), ine.getPropertyEditor(), newValString) : false;
            return propName;
        }
        finally {
            c.setCursor(oldCursor);
        }
    }

    private static void processThrowable(Throwable throwable, String title, Object newValue) {
        String msg;
        if (throwable instanceof ThreadDeath) {
            throw (ThreadDeath)throwable;
        }
        String locMsg = Exceptions.findLocalizedMessage((Throwable)throwable);
        if (locMsg != null && throwable.getLocalizedMessage() != throwable.getMessage()) {
            msg = NbBundle.getMessage(PropUtils.class, (String)"FMT_ErrorSettingProperty", (Object)newValue, (Object)title);
            Exceptions.attachLocalizedMessage((Throwable)throwable, (String)msg);
        } else if (throwable instanceof NumberFormatException) {
            Exceptions.attachLocalizedMessage((Throwable)throwable, (String)NbBundle.getMessage(PropUtils.class, (String)"FMT_BAD_NUMBER_FORMAT", (Object)newValue));
        }
        msg = Exceptions.findLocalizedMessage((Throwable)throwable);
        if (null == msg || msg.isEmpty()) {
            msg = throwable.getMessage();
        }
        NotifyDescriptor.Message d = new NotifyDescriptor.Message((Object)msg, 1);
        DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)d);
    }

    static synchronized String findLocalizedMessage(Throwable throwable, Object newValue, String title) {
        try {
            if (throwable == null) {
                return null;
            }
            if (throwable.getLocalizedMessage() != throwable.getMessage()) {
                return throwable.getLocalizedMessage();
            }
            String msg = Exceptions.findLocalizedMessage((Throwable)throwable);
            if (msg != null) {
                return msg;
            }
            if (throwable instanceof NumberFormatException) {
                return NbBundle.getMessage(PropUtils.class, (String)"FMT_BAD_NUMBER_FORMAT", (Object)newValue);
            }
            return NbBundle.getMessage(PropUtils.class, (String)"FMT_CannotUpdateProperty", (Object)newValue, (Object)title);
        }
        catch (Exception e) {
            Exceptions.printStackTrace((Throwable)e);
            return null;
        }
    }

    static Comparator<Node.Property> getComparator(int sortingMode) {
        switch (sortingMode) {
            case 0: {
                return null;
            }
            case 1: {
                return SORTER_NAME;
            }
            case 2: {
                return SORTER_TYPE;
            }
        }
        throw new IllegalArgumentException("Unknown sorting mode: " + Integer.toString(sortingMode));
    }

    public static ComboBoxUI createComboUI(JComboBox box, boolean tableUI) {
        return new CleanComboUI(tableUI);
    }

    private static List<String> getMissing() {
        if (missing == null) {
            missing = new ArrayList<String>();
        }
        return missing;
    }

    private static PropertyEditor ignored(PropertyEditor p) {
        if (p != null && (p.getClass().getName().equals("sun.beans.editors.EnumEditor") || p.getClass().getName().equals("com.sun.beans.editors.EnumEditor"))) {
            return null;
        }
        return p;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static PropertyEditor getPropertyEditor(Class<?> c) {
        PropertyEditor result = PropUtils.ignored(PropertyEditorManager.findEditor(c));
        ClassLoader global = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        ClassLoader now = Thread.currentThread().getContextClassLoader();
        if (result == null && global != null && now != global) {
            try {
                Thread.currentThread().setContextClassLoader(global);
                result = PropertyEditorManager.findEditor(c);
            }
            finally {
                Thread.currentThread().setContextClassLoader(now);
            }
        }
        if (result == null && Enum.class.isAssignableFrom(c)) {
            result = new EnumPropertyEditor(c.asSubclass(Enum.class));
        }
        if (result == null) {
            result = new NoPropertyEditorEditor();
        }
        return result;
    }

    static PropertyEditor getPropertyEditor(Node.Property p) {
        return PropUtils.getPropertyEditor(p, true);
    }

    static PropertyEditor getPropertyEditor(Node.Property p, boolean updateEditor) {
        PropertyEditor result;
        block16 : {
            result = p.getPropertyEditor();
            if (p instanceof Node.IndexedProperty && result == null) {
                result = new IndexedPropertyEditor();
                p.setValue("changeImmediate", (Object)Boolean.FALSE);
            }
            if (result == null) {
                result = PropUtils.getPropertyEditor(p.getValueType());
            }
            if (result == null) {
                String type;
                List<String> missing = PropUtils.getMissing();
                if (!missing.contains(type = p.getValueType().getName())) {
                    Logger.getAnonymousLogger().fine("No property editor registered for type " + type);
                    missing.add(type);
                }
                result = new NoPropertyEditorEditor();
            } else if (p.canRead()) {
                try {
                    try {
                        try {
                            if ((p.getValueType() == Boolean.class || p.getValueType() == Boolean.TYPE) && p.getValue() == null) {
                                result = new Boolean3WayEditor();
                            }
                            if (updateEditor || null == result.getValue()) {
                                PropUtils.updateEdFromProp(p, result, p.getDisplayName());
                            }
                        }
                        catch (ProxyNode.DifferentValuesException dve) {
                            if (p.getValueType() == Boolean.class || p.getValueType() == Boolean.TYPE) {
                                result = new Boolean3WayEditor();
                                break block16;
                            }
                            if (result instanceof ExPropertyEditor) {
                                result = new ExDifferentValuesEditor(result);
                                break block16;
                            }
                            result = new DifferentValuesEditor(result);
                        }
                    }
                    catch (IllegalAccessException iae) {
                        throw (IllegalStateException)new IllegalStateException("Error getting property value").initCause(iae);
                    }
                }
                catch (InvocationTargetException ite) {
                    throw (IllegalStateException)new IllegalStateException("Error getting property value").initCause(ite);
                }
            }
        }
        return result;
    }

    private static void updateEdFromProp(Node.Property p, PropertyEditor ed, String title) throws ProxyNode.DifferentValuesException, IllegalAccessException, InvocationTargetException {
        Object newValue = p.getValue();
        if (newValue instanceof String && ((String)newValue).length() > 2048) {
            ed.setValue(newValue);
            return;
        }
        Object oldValue = ed.getValue();
        if (newValue == null && oldValue == null) {
            return;
        }
        if (newValue != null && !newValue.equals(oldValue) || newValue == null && oldValue != null) {
            if (oldValue instanceof Object[] && newValue instanceof Object[] && Arrays.equals((Object[])oldValue, (Object[])newValue)) {
                return;
            }
            ed.setValue(newValue);
        }
    }

    static Color getControlColor() {
        if (controlColor == null) {
            PropUtils.deriveColorsAndMargin();
        }
        return controlColor;
    }

    static Color getShadowColor() {
        if (shadowColor == null) {
            PropUtils.deriveColorsAndMargin();
        }
        return shadowColor;
    }

    static Color getAltBg() {
        if (altBg == null) {
            PropUtils.deriveColorsAndMargin();
        }
        return altBg;
    }

    static boolean noAltBg() {
        if (noAltBg == null) {
            noAltBg = UIManager.getColor("Tree.altbackground") == null ? Boolean.TRUE : Boolean.FALSE;
        }
        return noAltBg;
    }

    static Color getTextFieldBackground() {
        if (tfBg == null) {
            tfBg = UIManager.getColor("TextField.background");
            if (tfBg == null) {
                tfBg = UIManager.getColor("text");
            }
            if (tfBg == null) {
                tfBg = Color.WHITE;
            }
        }
        return tfBg;
    }

    static Color getTextFieldForeground() {
        if (tfFg == null) {
            tfFg = UIManager.getColor("TextField.foreground");
            if (tfFg == null) {
                tfFg = UIManager.getColor("textText");
            }
            if (tfFg == null) {
                tfFg = Color.BLACK;
            }
        }
        return tfFg;
    }

    private static void deriveColorsAndMargin() {
        int red;
        int blue;
        int green;
        controlColor = UIManager.getColor("control");
        if (controlColor == null) {
            controlColor = Color.LIGHT_GRAY;
        }
        boolean windows = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel".equals(UIManager.getLookAndFeel().getClass().getName());
        boolean nimbus = "Nimbus".equals(UIManager.getLookAndFeel().getID());
        boolean gtk = "GTK".equals(UIManager.getLookAndFeel().getID());
        setRendererColor = UIManager.getColor("PropSheet.setBackground");
        selectedSetRendererColor = UIManager.getColor("PropSheet.selectedSetBackground");
        if (nimbus || gtk) {
            setRendererColor = UIManager.getColor("Menu.background");
            selectedSetRendererColor = UIManager.getColor("Tree.selectionBackground");
        }
        if (setRendererColor == null && setRendererColor == null) {
            red = PropUtils.adjustColorComponent(controlColor.getRed(), -25, -25);
            green = PropUtils.adjustColorComponent(controlColor.getGreen(), -25, -25);
            blue = PropUtils.adjustColorComponent(controlColor.getBlue(), -25, -25);
            setRendererColor = new Color(red, green, blue);
        }
        if (selectedSetRendererColor == null) {
            Color col;
            Color color = col = windows ? UIManager.getColor("Table.selectionBackground") : UIManager.getColor("activeCaptionBorder");
            if (col == null) {
                col = Color.BLUE;
            }
            red = PropUtils.adjustColorComponent(col.getRed(), -25, -25);
            green = PropUtils.adjustColorComponent(col.getGreen(), -25, -25);
            blue = PropUtils.adjustColorComponent(col.getBlue(), -25, -25);
            selectedSetRendererColor = new Color(red, green, blue);
        }
        if ((PropUtils.shadowColor = UIManager.getColor("controlShadow")) == null) {
            shadowColor = Color.GRAY;
        }
        setForegroundColor = UIManager.getColor("PropSheet.setForeground");
        if (nimbus || gtk) {
            setForegroundColor = new Color(UIManager.getColor("Menu.foreground").getRGB());
        }
        if (setForegroundColor == null && (PropUtils.setForegroundColor = UIManager.getColor("Table.foreground")) == null && (PropUtils.setForegroundColor = UIManager.getColor("textText")) == null) {
            setForegroundColor = Color.BLACK;
        }
        if ((PropUtils.selectedSetForegroundColor = UIManager.getColor("PropSheet.selectedSetForeground")) == null && (PropUtils.selectedSetForegroundColor = UIManager.getColor("Table.selectionForeground")) == null) {
            selectedSetForegroundColor = Color.WHITE;
        }
        if ((PropUtils.altBg = UIManager.getColor("Tree.altbackground")) == null) {
            altBg = UIManager.getColor("Tree.background");
            if (altBg == null) {
                altBg = Color.WHITE;
            }
            noAltBg = Boolean.TRUE;
        } else {
            noAltBg = Boolean.FALSE;
        }
        Icon collapsedIcon = PropUtils.getCollapsedIcon();
        int iconSize = 9;
        if (collapsedIcon != null) {
            iconSize = collapsedIcon.getIconWidth();
            marginWidth = Math.max(14, iconSize - 2);
        } else {
            marginWidth = 13;
        }
        Integer i = (Integer)UIManager.get("netbeans.ps.iconmargin");
        iconMargin = i != null ? i : ("com.sun.java.swing.plaf.windows.WindowsLookAndFeel".equals(UIManager.getLookAndFeel().getClass().getName()) ? 4 : 0);
        i = (Integer)UIManager.get("netbeans.ps.rowheight");
        spinnerHeight = i != null ? i : iconSize;
    }

    static Icon getExpandedIcon() {
        Icon expandedIcon = UIManager.getIcon(isGtk ? "Tree.gtk_expandedIcon" : "Tree.expandedIcon2");
        if (expandedIcon == null) {
            expandedIcon = UIManager.getIcon("Tree.expandedIcon");
        }
        if (expandedIcon == null) {
            LOG.info("no Tree.expandedIcon found");
        }
        return expandedIcon;
    }

    static Icon getCollapsedIcon() {
        Icon collapsedIcon = UIManager.getIcon(isGtk ? "Tree.gtk_collapsedIcon" : "Tree.collapsedIcon2");
        if (collapsedIcon == null) {
            collapsedIcon = UIManager.getIcon("Tree.collapsedIcon");
        }
        if (collapsedIcon == null) {
            LOG.info("no Tree.collapsedIcon found");
        }
        return collapsedIcon;
    }

    static Color getSetRendererColor() {
        if (setRendererColor == null) {
            PropUtils.deriveColorsAndMargin();
        }
        return setRendererColor;
    }

    static Color getSelectedSetRendererColor() {
        if (selectedSetRendererColor == null) {
            PropUtils.deriveColorsAndMargin();
        }
        return selectedSetRendererColor;
    }

    static Color getSetForegroundColor() {
        if (setForegroundColor == null) {
            PropUtils.deriveColorsAndMargin();
        }
        return setForegroundColor;
    }

    static Color getSelectedSetForegroundColor() {
        if (selectedSetForegroundColor == null) {
            PropUtils.deriveColorsAndMargin();
        }
        return selectedSetForegroundColor;
    }

    static int getMarginWidth() {
        if (marginWidth == -1) {
            PropUtils.deriveColorsAndMargin();
        }
        return marginWidth;
    }

    static int getSpinnerHeight() {
        if (spinnerHeight == -1) {
            PropUtils.deriveColorsAndMargin();
        }
        return spinnerHeight;
    }

    static int getIconMargin() {
        if (iconMargin == -1) {
            PropUtils.deriveColorsAndMargin();
        }
        return iconMargin;
    }

    static Icon getCustomButtonIcon() {
        return new BpIcon();
    }

    private static int adjustColorComponent(int base, int adjBright, int adjDark) {
        base = base > 128 ? (base -= adjBright) : (base += adjDark);
        if (base < 0) {
            base = 0;
        }
        if (base > 255) {
            base = 255;
        }
        return base;
    }

    static String basicPropsTabName() {
        if (bptn == null) {
            bptn = NbBundle.getMessage(PropUtils.class, (String)"LBL_BasicTab");
        }
        return bptn;
    }

    static Comparator getTabListComparator() {
        if (comp == null) {
            comp = new TabListComparator();
        }
        return comp;
    }

    static SplitPaneUI createSplitPaneUI() {
        return new CleanSplitPaneUI();
    }

    static boolean shouldShowDescription() {
        return PropUtils.preferences().getBoolean("showDescriptionArea", true);
    }

    static void saveShowDescription(boolean b) {
        PropUtils.preferences().putBoolean("showDescriptionArea", b);
    }

    static String[] getSavedClosedSetNames() {
        String s = PropUtils.preferences().get("closedSetNames", null);
        if (s != null) {
            StringTokenizer tok = new StringTokenizer(s, ",");
            String[] result = new String[tok.countTokens()];
            int i = 0;
            while (tok.hasMoreElements()) {
                result[i] = tok.nextToken();
                ++i;
            }
            return result;
        }
        return new String[0];
    }

    static void putSavedClosedSetNames(Set s) {
        if (s.size() > 0) {
            StringBuffer sb = new StringBuffer(s.size() * 20);
            Iterator i = s.iterator();
            while (i.hasNext()) {
                sb.append(i.next());
                if (!i.hasNext()) continue;
                sb.append(',');
            }
            PropUtils.preferences().put("closedSetNames", sb.toString());
        } else {
            PropUtils.preferences().put("closedSetNames", "");
        }
    }

    static void putSortOrder(int i) {
        PropUtils.preferences().putInt("sortOrder", i);
    }

    static int getSavedSortOrder() {
        return PropUtils.preferences().getInt("sortOrder", 0);
    }

    static int getTextMargin() {
        if ("apple.laf.AquaLookAndFeel".equals(UIManager.getLookAndFeel().getClass().getName())) {
            return 0;
        }
        if (textMargin == -1) {
            Object o = UIManager.get("netbeans.ps.textMargin");
            textMargin = o instanceof Integer ? (Integer)o : 2;
        }
        return textMargin;
    }

    static String createHtmlTooltip(String title, String s) {
        boolean wasHtml = false;
        if (s.matches("\\<(html|HTML)\\>.*\\<\\/(html|HTML)\\>")) {
            s = s.replaceAll("\\<\\/{0,1}(html|HTML)\\>", "");
            wasHtml = true;
        }
        String token = null;
        if (s.indexOf(" ") != -1) {
            token = " ";
        } else if (s.indexOf(",") != -1) {
            token = ",";
        } else if (s.indexOf(";") != -1) {
            token = ";";
        } else if (s.indexOf("/") != -1) {
            token = "/";
        } else if (s.indexOf("\\") != -1) {
            token = "\\";
        } else {
            return s;
        }
        StringTokenizer tk = new StringTokenizer(s, token, true);
        StringBuffer sb = new StringBuffer(s.length() + 20);
        sb.append("<html>");
        sb.append("<b><u>");
        sb.append(title);
        sb.append("</u></b><br>");
        int charCount = 0;
        int lineCount = 0;
        while (tk.hasMoreTokens()) {
            String a = tk.nextToken();
            if (!wasHtml) {
                a = a.replace("&", "&amp;");
                a = a.replace("<", "&lt;");
                a = a.replace(">", "&gt;");
            }
            charCount += a.length();
            sb.append(a);
            if (tk.hasMoreTokens()) {
                ++charCount;
            }
            if (charCount <= 80) continue;
            sb.append("<br>");
            charCount = 0;
            if (++lineCount <= 10) continue;
            sb.append(NbBundle.getMessage(PropUtils.class, (String)"MSG_ELLIPSIS"));
            return sb.toString();
        }
        sb.append("</html>");
        return sb.toString();
    }

    static InplaceEditor findInnermostInplaceEditor(InplaceEditor ine) {
        while (ine instanceof IconPanel || ine instanceof ButtonPanel) {
            if (ine instanceof IconPanel) {
                ine = ((IconPanel)ine).getInplaceEditor();
                continue;
            }
            ine = ((ButtonPanel)ine).getInplaceEditor();
        }
        return ine;
    }

    static boolean shouldDrawMargin(PropertySetModel psm) {
        return false;
    }

    private static Color getIconForeground() {
        return UIManager.getColor("PropSheet.customButtonForeground");
    }

    public static boolean isXPTheme() {
        Boolean isXP = (Boolean)Toolkit.getDefaultToolkit().getDesktopProperty("win.xpstyle.themeActive");
        return isXP == null ? false : isXP;
    }

    static boolean isWindowsVistaLaF() {
        if (!"Windows".equals(UIManager.getLookAndFeel().getID())) {
            return false;
        }
        if (!PropUtils.isXPTheme()) {
            return false;
        }
        return PropUtils.isWindowsVista() || PropUtils.isWindows7() || PropUtils.isWindows8();
    }

    private static boolean isWindowsVista() {
        String osName = System.getProperty("os.name");
        return osName.indexOf("Vista") >= 0 || osName.equals("Windows NT (unknown)") && "6.0".equals(System.getProperty("os.version"));
    }

    private static boolean isWindows7() {
        String osName = System.getProperty("os.name");
        return osName.indexOf("Windows 7") >= 0 || osName.equals("Windows NT (unknown)") && "6.1".equals(System.getProperty("os.version"));
    }

    private static boolean isWindows8() {
        String osName = System.getProperty("os.name");
        return osName.indexOf("Windows 8") >= 0 || osName.equals("Windows NT (unknown)") && "6.2".equals(System.getProperty("os.version"));
    }

    static boolean shallBeRDVEnabled(FeatureDescriptor fd) {
        if (fd != null && fd instanceof Node.Property) {
            return PropUtils.shallBeRDVEnabled((Node.Property)fd);
        }
        return false;
    }

    static boolean shallBeRDVEnabled(Node.Property property) {
        if (property == null || !property.supportsDefaultValue()) {
            return false;
        }
        try {
            if (property.getClass().getMethod("isDefaultValue", new Class[0]).getDeclaringClass() == Node.Property.class) {
                return true;
            }
            return !property.isDefaultValue();
        }
        catch (NoSuchMethodException e) {
            assert (false);
            return true;
        }
    }

    static void addExternallyEdited(Node.Property p) {
        externallyEdited.add(p);
    }

    static void removeExternallyEdited(Node.Property p) {
        externallyEdited.remove((Object)p);
    }

    static boolean isExternallyEdited(Node.Property p) {
        return externallyEdited.contains((Object)p);
    }

    static void notifyEditingCancelled(PropertyEnv env) {
        if (null == env || null == env.getFeatureDescriptor()) {
            return;
        }
        Object o = env.getFeatureDescriptor().getValue("nb.property.editor.callback");
        if (o instanceof PropertyChangeListener) {
            ((PropertyChangeListener)o).propertyChange(new PropertyChangeEvent(env.getFeatureDescriptor(), "editingCancelled", null, Boolean.TRUE));
        }
    }

    static boolean supportsValueIncrement(PropertyEnv env) {
        if (null == env || null == env.getFeatureDescriptor()) {
            return false;
        }
        Object o = env.getFeatureDescriptor().getValue("valueIncrement");
        return o instanceof SpinnerModel;
    }

    static Object getNextValue(PropertyEnv env, boolean increment) {
        Object obj;
        Object res = null;
        if (null != env && null != env.getFeatureDescriptor() && (obj = env.getFeatureDescriptor().getValue("valueIncrement")) instanceof SpinnerModel) {
            SpinnerModel spinner = (SpinnerModel)obj;
            res = increment ? spinner.getNextValue() : spinner.getPreviousValue();
        }
        return res;
    }

    static void wrapUpDownArrowActions(JComponent inplaceEditor, IncrementPropertyValueSupport incrementSupport) {
        InputMap im = inplaceEditor.getInputMap(0);
        PropUtils.wrapAction(im.get(KeyStroke.getKeyStroke(38, 0)), inplaceEditor.getActionMap(), incrementSupport, true);
        PropUtils.wrapAction("selectPrevious", inplaceEditor.getActionMap(), incrementSupport, true);
        PropUtils.wrapAction("selectPrevious2", inplaceEditor.getActionMap(), incrementSupport, true);
        PropUtils.wrapAction(im.get(KeyStroke.getKeyStroke(68, 0)), inplaceEditor.getActionMap(), incrementSupport, false);
        PropUtils.wrapAction("selectNext", inplaceEditor.getActionMap(), incrementSupport, false);
        PropUtils.wrapAction("selectNext2", inplaceEditor.getActionMap(), incrementSupport, false);
    }

    private static void wrapAction(Object key, ActionMap actionMap, IncrementPropertyValueSupport incrementSupport, boolean doIncrement) {
        if (null == key) {
            return;
        }
        Action originalAction = actionMap.get(key);
        if (null != originalAction && !(originalAction instanceof IncrementValueActionWrapper)) {
            actionMap.put(key, new IncrementValueActionWrapper(originalAction, incrementSupport, doIncrement));
        }
    }

    static class BpIcon
    implements Icon {
        boolean larger;

        public BpIcon() {
            Font f = UIManager.getFont("Table.font");
            this.larger = f != null ? f.getSize() > 13 : false;
        }

        @Override
        public int getIconHeight() {
            return 12;
        }

        @Override
        public int getIconWidth() {
            return this.larger ? 16 : 12;
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            int w = c.getWidth();
            int h = c.getHeight();
            int ybase = h - 5;
            int pos2 = w / 2;
            int pos1 = pos2 - 4;
            int pos3 = pos2 + 4;
            g.setColor(PropUtils.getIconForeground() == null ? c.getForeground() : PropUtils.getIconForeground());
            this.drawDot(g, pos1 + 1, ybase, this.larger);
            this.drawDot(g, pos2, ybase, this.larger);
            this.drawDot(g, pos3 - 1, ybase, this.larger);
        }

        private void drawDot(Graphics g, int x, int y, boolean larger) {
            if (!larger) {
                g.drawLine(x, y, x, y);
            } else {
                g.drawLine(x - 1, y, x + 1, y);
                g.drawLine(x, y - 1, x, y + 1);
            }
        }
    }

    private static class SplitBorder
    implements Border {
        private SplitBorder() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            if (UIManager.getLookAndFeel() instanceof MetalLookAndFeel) {
                return new Insets(2, 0, 1, 0);
            }
            return new Insets(1, 0, 1, 0);
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            if (UIManager.getLookAndFeel() instanceof MetalLookAndFeel) {
                g.setColor(UIManager.getColor("controlShadow"));
                g.drawLine(x, y, x + width, y);
                g.setColor(UIManager.getColor("controlHighlight"));
                g.drawLine(x, y + 1, x + width, y + 1);
                g.drawLine(x, y + height - 1, x + width, y + height - 1);
                g.setColor(UIManager.getColor("controlShadow"));
                g.drawLine(x, y + height - 2, x + width, y + height - 2);
            } else {
                g.setColor(UIManager.getColor("controlHighlight"));
                g.drawLine(x, y, x + width, y);
                g.setColor(UIManager.getColor("controlShadow"));
                g.drawLine(x, y + height - 1, x + width, y + height - 1);
            }
        }
    }

    private static class CleanSplitPaneDivider
    extends BasicSplitPaneDivider
    implements Accessible {
        private AccessibleContext accessibleContext;

        public CleanSplitPaneDivider(BasicSplitPaneUI ui) {
            super(ui);
        }

        @Override
        public AccessibleContext getAccessibleContext() {
            if (null == this.accessibleContext) {
                this.accessibleContext = new Component.AccessibleAWTComponent(){

                    @Override
                    public AccessibleRole getAccessibleRole() {
                        return AccessibleRole.SPLIT_PANE;
                    }
                };
                this.accessibleContext.setAccessibleName(NbBundle.getMessage(DescriptionComponent.class, (String)"ACS_Splitter"));
                this.accessibleContext.setAccessibleDescription(NbBundle.getMessage(DescriptionComponent.class, (String)"ACSD_Splitter"));
            }
            return this.accessibleContext;
        }

    }

    private static class CleanSplitPaneUI
    extends BasicSplitPaneUI {
        private CleanSplitPaneUI() {
        }

        @Override
        protected void installDefaults() {
            super.installDefaults();
            this.divider.setBorder(new SplitBorder());
        }

        @Override
        public BasicSplitPaneDivider createDefaultDivider() {
            return new CleanSplitPaneDivider(this);
        }
    }

    private static class TabListComparator
    implements Comparator {
        private TabListComparator() {
        }

        public int compare(Object o1, Object o2) {
            String s1 = (String)o1;
            String s2 = (String)o2;
            if (s1 == s2) {
                return 0;
            }
            String bn = PropUtils.basicPropsTabName();
            if (bn.equals(s1)) {
                return -1;
            }
            if (bn.equals(s2)) {
                return 1;
            }
            return s1.compareTo(s2);
        }
    }

    static final class NoPropertyEditorEditor
    implements PropertyEditor {
        NoPropertyEditorEditor() {
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener listener) {
        }

        @Override
        public String getAsText() {
            return NbBundle.getMessage(PropertySheet.class, (String)"CTL_NoPropertyEditor");
        }

        @Override
        public Component getCustomEditor() {
            return null;
        }

        @Override
        public String getJavaInitializationString() {
            return "";
        }

        @Override
        public String[] getTags() {
            return null;
        }

        @Override
        public Object getValue() {
            return this.getAsText();
        }

        @Override
        public boolean isPaintable() {
            return false;
        }

        @Override
        public void paintValue(Graphics gfx, Rectangle box) {
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener listener) {
        }

        @Override
        public void setAsText(String text) throws IllegalArgumentException {
        }

        @Override
        public void setValue(Object value) {
        }

        @Override
        public boolean supportsCustomEditor() {
            return false;
        }
    }

    static final class ExDifferentValuesEditor
    extends DifferentValuesEditor
    implements ExPropertyEditor {
        public ExDifferentValuesEditor(PropertyEditor ed) {
            super(ed);
        }

        @Override
        public void attachEnv(PropertyEnv env) {
            ((ExPropertyEditor)this.ed).attachEnv(env);
        }
    }

    static class DifferentValuesEditor
    implements PropertyEditor {
        protected PropertyEditor ed;
        private boolean notSet = true;

        public DifferentValuesEditor(PropertyEditor ed) {
            this.ed = ed;
            this.addPropertyChangeListener(new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    DifferentValuesEditor.this.notSet = false;
                }
            });
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener listener) {
            this.ed.addPropertyChangeListener(listener);
        }

        @Override
        public String getAsText() {
            String result = this.notSet ? NbBundle.getMessage(PropUtils.class, (String)"CTL_Different_Values") : this.ed.getAsText();
            return result;
        }

        @Override
        public Component getCustomEditor() {
            return this.ed.getCustomEditor();
        }

        @Override
        public String getJavaInitializationString() {
            return this.ed.getJavaInitializationString();
        }

        @Override
        public String[] getTags() {
            return this.ed.getTags();
        }

        @Override
        public Object getValue() {
            Object result = this.notSet ? null : this.ed.getValue();
            return result;
        }

        @Override
        public boolean isPaintable() {
            return this.notSet ? false : this.ed.isPaintable();
        }

        @Override
        public void paintValue(Graphics gfx, Rectangle box) {
            if (this.isPaintable()) {
                this.ed.paintValue(gfx, box);
            }
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener listener) {
            this.ed.removePropertyChangeListener(listener);
        }

        @Override
        public void setAsText(String text) throws IllegalArgumentException {
            this.ed.setAsText(text);
            this.notSet = false;
        }

        @Override
        public void setValue(Object value) {
            this.ed.setValue(value);
            this.notSet = false;
        }

        @Override
        public boolean supportsCustomEditor() {
            return this.ed.supportsCustomEditor();
        }

    }

    private static class IncrementValueActionWrapper
    extends AbstractAction {
        private final Action originalAction;
        private final IncrementPropertyValueSupport incrementSupport;
        private final boolean increment;

        public IncrementValueActionWrapper(Action originalAction, IncrementPropertyValueSupport incrementSupport, boolean doIncrement) {
            this.originalAction = originalAction;
            this.incrementSupport = incrementSupport;
            this.increment = doIncrement;
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            boolean consume;
            boolean bl = consume = this.increment ? this.incrementSupport.incrementValue() : this.incrementSupport.decrementValue();
            if (consume) {
                return;
            }
            this.originalAction.actionPerformed(ae);
        }

        @Override
        public boolean isEnabled() {
            return this.incrementSupport.isIncrementEnabled() || this.originalAction.isEnabled();
        }
    }

}

