/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.propertysheet;

import java.beans.PropertyEditorSupport;
import java.lang.reflect.Method;

final class EnumPropertyEditor
extends PropertyEditorSupport {
    private final Class<? extends Enum> c;

    public EnumPropertyEditor(Class<? extends Enum> c) {
        this.c = c;
    }

    private Object[] getValues() {
        try {
            return (Object[])this.c.getMethod("values", new Class[0]).invoke(null, new Object[0]);
        }
        catch (Exception x) {
            throw new AssertionError(x);
        }
    }

    @Override
    public String[] getTags() {
        Object[] values = this.getValues();
        String[] tags = new String[values.length];
        for (int i = 0; i < values.length; ++i) {
            tags[i] = values[i].toString();
        }
        return tags;
    }

    @Override
    public String getAsText() {
        Object o = this.getValue();
        return o != null ? o.toString() : "";
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if (text.length() > 0) {
            Object[] values = this.getValues();
            for (int i = 0; i < values.length; ++i) {
                String p = values[i].toString();
                if (!text.equals(p)) continue;
                this.setValue(values[i]);
                return;
            }
            this.setValue(Enum.valueOf(this.c, text));
        } else {
            this.setValue(null);
        }
    }

    @Override
    public String getJavaInitializationString() {
        Enum e = (Enum)this.getValue();
        if (e == null) {
            return "null";
        }
        String name = this.c.getCanonicalName();
        if (name == null) {
            return super.getJavaInitializationString();
        }
        return name + '.' + e.name();
    }
}

