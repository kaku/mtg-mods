/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 */
package org.openide.explorer.propertysheet;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import java.beans.FeatureDescriptor;
import java.beans.PropertyEditor;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;
import org.openide.util.ImageUtilities;

class IconPanel
extends JComponent
implements InplaceEditor {
    private InplaceEditor inplaceEditor;
    private Icon icon;
    private boolean needLayout = true;
    private PropertyEnv env = null;
    private Component comp;

    public IconPanel() {
        this.setOpaque(true);
    }

    public void setInplaceEditor(InplaceEditor inplaceEditor) {
        this.inplaceEditor = inplaceEditor;
        this.setComponent(inplaceEditor.getComponent());
    }

    public InplaceEditor getInplaceEditor() {
        return this.inplaceEditor;
    }

    @Override
    public void setEnabled(boolean val) {
        if (this.comp != null) {
            this.comp.setEnabled(val);
        }
        super.setEnabled(val);
    }

    @Override
    public void setBackground(Color c) {
        if (this.comp != null) {
            this.comp.setBackground(c);
        }
        super.setBackground(c);
    }

    @Override
    public void setForeground(Color c) {
        if (this.comp != null) {
            this.comp.setForeground(c);
        }
        super.setForeground(c);
    }

    @Override
    public void setFont(Font f) {
        if (this.comp != null) {
            this.comp.setFont(f);
        }
        super.setFont(f);
    }

    private void setComponent(Component c) {
        if (this.comp != null) {
            this.remove(this.comp);
        }
        if (c != null) {
            this.add(c);
        }
        this.comp = c;
        this.needLayout = true;
    }

    public void setIcon(Icon i) {
        this.icon = i;
        this.needLayout = true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void paintComponent(Graphics g) {
        if (this.needLayout) {
            this.doLayout();
        }
        if (this.icon != null) {
            Color c = g.getColor();
            try {
                g.setColor(this.getBackground());
                int right = this.comp != null ? this.comp.getLocation().x + this.icon.getIconWidth() : this.icon.getIconWidth() + 2;
                g.fillRect(0, 0, right, this.getHeight());
                Insets ins = this.getInsets();
                int x = ins.left;
                int y = ins.top + Math.max(this.getHeight() / 2 - this.icon.getIconHeight() / 2, 0);
                this.icon.paintIcon(this, g, x, y);
            }
            finally {
                g.setColor(c);
            }
        }
        super.paintComponent(g);
    }

    @Override
    public void addActionListener(ActionListener al) {
        this.inplaceEditor.addActionListener(al);
    }

    @Override
    public void clear() {
        this.inplaceEditor.clear();
        this.setIcon(null);
        this.setComponent(null);
        this.env = null;
    }

    @Override
    public void connect(PropertyEditor pe, PropertyEnv env) {
        this.inplaceEditor.connect(pe, env);
        this.env = env;
        this.updateIcon();
    }

    private void updateIcon() {
        if (this.env != null) {
            Icon ic = null;
            FeatureDescriptor fd = this.env.getFeatureDescriptor();
            if (this.env.getState() == PropertyEnv.STATE_INVALID) {
                ic = ImageUtilities.loadImageIcon((String)"org/openide/resources/propertysheet/invalid.gif", (boolean)false);
            } else if (fd != null) {
                ic = (Icon)fd.getValue("valueIcon");
            }
            this.setIcon(ic);
            this.needLayout = true;
        }
    }

    @Override
    public void setOpaque(boolean val) {
        if (this.getInplaceEditor() != null) {
            this.getInplaceEditor().getComponent().setOpaque(true);
        }
    }

    @Override
    public JComponent getComponent() {
        return this;
    }

    @Override
    public KeyStroke[] getKeyStrokes() {
        return this.inplaceEditor.getKeyStrokes();
    }

    @Override
    public PropertyEditor getPropertyEditor() {
        return this.inplaceEditor.getPropertyEditor();
    }

    @Override
    public PropertyModel getPropertyModel() {
        return this.inplaceEditor.getPropertyModel();
    }

    @Override
    public Object getValue() {
        return this.inplaceEditor.getValue();
    }

    @Override
    public boolean isKnownComponent(Component c) {
        return c == this || this.inplaceEditor.isKnownComponent(c);
    }

    @Override
    public void removeActionListener(ActionListener al) {
        this.inplaceEditor.removeActionListener(al);
    }

    @Override
    public void reset() {
        this.inplaceEditor.reset();
        this.updateIcon();
    }

    @Override
    public void setPropertyModel(PropertyModel pm) {
        this.inplaceEditor.setPropertyModel(pm);
    }

    @Override
    public void setValue(Object o) {
        this.inplaceEditor.setValue(o);
    }

    @Override
    public boolean supportsTextEntry() {
        return this.inplaceEditor.supportsTextEntry();
    }

    @Override
    public void requestFocus() {
        this.comp.requestFocus();
    }

    @Override
    public boolean requestFocusInWindow() {
        return this.comp.requestFocusInWindow();
    }

    @Override
    public void addFocusListener(FocusListener fl) {
        if (this.comp != null) {
            this.comp.addFocusListener(fl);
        } else {
            super.addFocusListener(fl);
        }
    }

    @Override
    public void removeFocusListener(FocusListener fl) {
        if (this.comp != null) {
            this.comp.removeFocusListener(fl);
        } else {
            super.removeFocusListener(fl);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void layout() {
        Insets ins = this.getInsets();
        int iconWidth = Math.max(this.icon.getIconWidth() + PropUtils.getTextMargin(), 18);
        int x = this.icon == null ? ins.left : ins.left + iconWidth;
        int y = ins.top;
        Object object = this.getTreeLock();
        synchronized (object) {
            Component c = this.comp;
            if (c == null) {
                return;
            }
            c.setBounds(x, y, this.getWidth() - (x + ins.right), this.getHeight() - ins.bottom);
            if (c instanceof Container) {
                ((Container)c).doLayout();
            }
        }
    }

    @Override
    public Dimension getPreferredSize() {
        Insets ins = this.getInsets();
        Component c = this.comp;
        Dimension result = new Dimension(0, 0);
        if (this.icon != null) {
            result.width = this.icon.getIconWidth() + PropUtils.getTextMargin();
            result.height = this.icon.getIconHeight();
        }
        if (c != null) {
            Dimension ps = c.getPreferredSize();
            result.width += ps.width;
            result.height = Math.max(ps.height, result.height);
        }
        result.width += ins.left + ins.right;
        result.height += ins.top + ins.bottom;
        return result;
    }

    @Override
    public Dimension getMinimumSize() {
        return this.getPreferredSize();
    }
}

