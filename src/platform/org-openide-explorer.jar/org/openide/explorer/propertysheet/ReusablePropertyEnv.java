/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 */
package org.openide.explorer.propertysheet;

import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeListener;
import java.beans.VetoableChangeListener;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.ProxyNode;
import org.openide.explorer.propertysheet.ReusablePropertyModel;
import org.openide.nodes.Node;

final class ReusablePropertyEnv
extends PropertyEnv {
    private Object NODE = null;
    private ReusablePropertyModel mdl;

    public ReusablePropertyEnv() {
        this.mdl = new ReusablePropertyModel(this);
    }

    public ReusablePropertyModel getReusablePropertyModel() {
        return this.mdl;
    }

    void clear() {
        this.NODE = null;
        if (this.mdl != null) {
            this.mdl.clear();
        }
    }

    void setReusablePropertyModel(ReusablePropertyModel mdl) {
        if (mdl == null) {
            throw new NullPointerException();
        }
        this.mdl = mdl;
    }

    @Override
    public Object[] getBeans() {
        if (ReusablePropertyModel.DEBUG) {
            ReusablePropertyModel.checkThread();
        }
        if (this.getNode() instanceof ProxyNode) {
            return ((ProxyNode)((Object)this.getNode())).getOriginalNodes();
        }
        if (this.getNode() instanceof Object[]) {
            return (Object[])this.getNode();
        }
        return new Object[]{this.getNode()};
    }

    @Override
    public FeatureDescriptor getFeatureDescriptor() {
        return this.mdl.getProperty();
    }

    @Override
    public void addVetoableChangeListener(VetoableChangeListener l) {
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener l) {
    }

    @Override
    public void removeVetoableChangeListener(VetoableChangeListener l) {
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener l) {
    }

    @Override
    public boolean isEditable() {
        boolean result = this.mdl.getProperty() != null ? this.mdl.getProperty().canWrite() : true;
        return result;
    }

    public void reset() {
        this.setEditable(true);
        this.setState(STATE_NEEDS_VALIDATION);
    }

    public Object getNode() {
        return this.NODE;
    }

    public void setNode(Object NODE) {
        this.NODE = NODE;
    }
}

