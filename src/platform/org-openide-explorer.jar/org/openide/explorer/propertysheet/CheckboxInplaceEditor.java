/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.openide.explorer.propertysheet;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.beans.FeatureDescriptor;
import java.beans.PropertyEditor;
import java.text.MessageFormat;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;
import org.openide.util.NbBundle;

class CheckboxInplaceEditor
extends JCheckBox
implements InplaceEditor {
    protected PropertyEditor editor = null;
    protected PropertyEnv env = null;
    private boolean useTitle = false;
    private String text = null;
    private PropertyModel pm = null;

    public CheckboxInplaceEditor() {
        this.setActionCommand("success");
    }

    public void setUseTitle(boolean val) {
        if (this.useTitle != val) {
            this.useTitle = val;
            this.text = null;
            if (this.env != null) {
                this.setText(this.env.getFeatureDescriptor().getDisplayName());
            }
        }
    }

    @Override
    public void setSelected(boolean val) {
        boolean fire = val == this.isSelected();
        String s = this.getText();
        super.setSelected(val);
        if (fire) {
            this.firePropertyChange("text", s, this.getText());
        }
    }

    @Override
    public void connect(PropertyEditor p, PropertyEnv env) {
        this.text = null;
        if (this.editor instanceof PropUtils.NoPropertyEditorEditor) {
            this.setSelected(false);
            return;
        }
        if (this.editor == p) {
            return;
        }
        this.editor = p;
        this.setSelected(Boolean.TRUE.equals(p.getValue()));
        this.reset();
        this.env = env;
        if (env != null && this.useTitle) {
            this.setText(env.getFeatureDescriptor().getDisplayName());
        }
    }

    @Override
    public void clear() {
        this.editor = null;
        this.pm = null;
        this.env = null;
        this.text = null;
        this.getModel().setRollover(false);
    }

    @Override
    public JComponent getComponent() {
        return this;
    }

    @Override
    public Object getValue() {
        return this.isSelected() ? Boolean.TRUE : Boolean.FALSE;
    }

    @Override
    public void reset() {
        if (this.editor instanceof PropUtils.NoPropertyEditorEditor) {
            return;
        }
        if (this.editor != null) {
            Object value = this.editor.getValue();
            if (value == null) {
                this.getModel().setArmed(true);
            } else if (value instanceof Boolean) {
                this.setSelected((Boolean)value);
            }
        }
    }

    @Override
    public String getText() {
        if (this.text == null) {
            if (this.useTitle || this.editor == null || this.editor.getTags() == null) {
                this.text = super.getText();
            } else if (PropUtils.noCheckboxCaption) {
                this.text = "";
            } else {
                Boolean sel;
                String[] tags;
                String prepend = NbBundle.getMessage(CheckboxInplaceEditor.class, (String)"BOOLEAN_PREPEND");
                String append = NbBundle.getMessage(CheckboxInplaceEditor.class, (String)"BOOLEAN_APPEND");
                MessageFormat mf = new MessageFormat(NbBundle.getMessage(CheckboxInplaceEditor.class, (String)"FMT_BOOLEAN"));
                Boolean bl = sel = this.isSelected() ? Boolean.TRUE : Boolean.FALSE;
                String s = sel.equals(this.editor.getValue()) ? this.editor.getAsText() : ((tags = this.editor.getTags())[0].equals(this.editor.getAsText()) ? tags[1] : tags[0]);
                this.text = mf.format(new String[]{prepend, s, append});
            }
        }
        return this.text;
    }

    @Override
    public KeyStroke[] getKeyStrokes() {
        return null;
    }

    @Override
    public PropertyEditor getPropertyEditor() {
        return this.editor;
    }

    public void handleInitialInputEvent(InputEvent e) {
        boolean toggle = false;
        if (e instanceof MouseEvent) {
            toggle = true;
        } else if (e instanceof KeyEvent && ((KeyEvent)e).getKeyCode() == 32) {
            toggle = true;
        }
        if (toggle) {
            this.setSelected(!this.isSelected());
            this.fireActionPerformed(new ActionEvent(this, 1001, "success"));
            this.getModel().setPressed(false);
        }
    }

    @Override
    public void setValue(Object o) {
        if (o == null) {
            this.setSelected(false);
        }
        if (Boolean.TRUE.equals(o)) {
            this.setSelected(true);
        } else if (Boolean.FALSE.equals(o)) {
            this.setSelected(false);
        }
    }

    @Override
    public boolean supportsTextEntry() {
        return false;
    }

    @Override
    public PropertyModel getPropertyModel() {
        return this.pm;
    }

    @Override
    public void setPropertyModel(PropertyModel pm) {
        this.pm = pm;
    }

    @Override
    public boolean isKnownComponent(Component c) {
        return false;
    }

    @Override
    public Dimension getPreferredSize() {
        Insets ins;
        if (this.isShowing()) {
            return super.getPreferredSize();
        }
        Dimension result = PropUtils.getMinimumPanelSize();
        Graphics g = PropUtils.getScratchGraphics(this);
        g.setFont(this.getFont());
        String txt = this.getText();
        Icon i = this.getIcon();
        FontMetrics fm = g.getFontMetrics(this.getFont());
        int w = fm.stringWidth(txt);
        int h = fm.getHeight();
        if (i != null) {
            w += i.getIconWidth() + this.getIconTextGap();
            h = Math.max(h, result.height);
        }
        if ((ins = this.getInsets()) != null) {
            w += ins.left + ins.right;
            h += ins.top + ins.bottom;
        }
        result.width = Math.max(result.width, w += 22);
        result.height = Math.max(result.height, h);
        return result;
    }
}

