/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.propertysheet;

import java.util.Enumeration;
import javax.swing.DefaultListSelectionModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

final class SheetColumnModel
implements TableColumnModel {
    static final Object NAMES_IDENTIFIER = "names";
    static final Object VALUES_IDENTIFIER = "values";
    TableColumn namesColumn = new TableColumn(0);
    TableColumn valuesColumn;
    ListSelectionModel lsm = new DefaultListSelectionModel();

    public SheetColumnModel() {
        this.namesColumn.setIdentifier(NAMES_IDENTIFIER);
        this.valuesColumn = new TableColumn(1);
        this.valuesColumn.setIdentifier(VALUES_IDENTIFIER);
        this.namesColumn.setMinWidth(60);
        this.valuesColumn.setMinWidth(30);
    }

    @Override
    public void addColumn(TableColumn aColumn) {
        throw new UnsupportedOperationException("Adding columns not supported");
    }

    @Override
    public void addColumnModelListener(TableColumnModelListener x) {
    }

    @Override
    public TableColumn getColumn(int columnIndex) {
        switch (columnIndex) {
            case 0: {
                return this.namesColumn;
            }
            case 1: {
                return this.valuesColumn;
            }
        }
        throw new IllegalArgumentException("Property sheet only has 2 columns - " + Integer.toString(columnIndex));
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public int getColumnIndex(Object columnIdentifier) {
        if (columnIdentifier instanceof String) {
            if (columnIdentifier.equals(NAMES_IDENTIFIER)) {
                return 0;
            }
            if (columnIdentifier.equals(VALUES_IDENTIFIER)) {
                return 1;
            }
        }
        throw new IllegalArgumentException("Illegal value: " + columnIdentifier);
    }

    @Override
    public int getColumnIndexAtX(int xPosition) {
        int width0 = this.namesColumn.getWidth();
        if (xPosition < width0) {
            return 0;
        }
        if (xPosition < width0 + this.valuesColumn.getWidth()) {
            return 1;
        }
        return -1;
    }

    @Override
    public int getColumnMargin() {
        return 1;
    }

    @Override
    public boolean getColumnSelectionAllowed() {
        return false;
    }

    @Override
    public Enumeration<TableColumn> getColumns() {
        return new Enumeration<TableColumn>(){
            private boolean done;
            private boolean doneOne;

            @Override
            public boolean hasMoreElements() {
                return !this.done;
            }

            @Override
            public TableColumn nextElement() {
                if (this.done) {
                    return null;
                }
                if (this.doneOne) {
                    this.done = true;
                    return SheetColumnModel.this.valuesColumn;
                }
                this.doneOne = true;
                return SheetColumnModel.this.namesColumn;
            }
        };
    }

    @Override
    public int getSelectedColumnCount() {
        return 0;
    }

    @Override
    public int[] getSelectedColumns() {
        return new int[0];
    }

    @Override
    public ListSelectionModel getSelectionModel() {
        return this.lsm;
    }

    @Override
    public int getTotalColumnWidth() {
        return this.namesColumn.getWidth() + this.valuesColumn.getWidth();
    }

    @Override
    public void moveColumn(int columnIndex, int newIndex) {
    }

    @Override
    public void removeColumn(TableColumn column) {
        throw new UnsupportedOperationException("Deleting columns not supported");
    }

    @Override
    public void removeColumnModelListener(TableColumnModelListener x) {
    }

    @Override
    public void setColumnMargin(int newMargin) {
    }

    @Override
    public void setColumnSelectionAllowed(boolean flag) {
    }

    @Override
    public void setSelectionModel(ListSelectionModel newModel) {
    }

}

