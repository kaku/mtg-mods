/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package org.openide.explorer.propertysheet.editors;

import java.beans.PropertyEditor;
import org.openide.nodes.Node;

@Deprecated
public interface NodePropertyEditor
extends PropertyEditor {
    public void attach(Node[] var1);
}

