/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.propertysheet.editors;

import java.awt.Component;
import java.beans.PropertyEditor;

@Deprecated
public interface EnhancedPropertyEditor
extends PropertyEditor {
    public Component getInPlaceCustomEditor();

    public boolean hasInPlaceCustomEditor();

    public boolean supportsEditingTaggedValues();
}

