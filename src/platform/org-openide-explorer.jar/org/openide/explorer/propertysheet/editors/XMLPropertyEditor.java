/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.propertysheet.editors;

import java.beans.PropertyEditor;
import java.io.IOException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public interface XMLPropertyEditor
extends PropertyEditor {
    public void readFromXML(Node var1) throws IOException;

    public Node storeToXML(Document var1);
}

