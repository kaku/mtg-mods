/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 */
package org.openide.explorer.propertysheet;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.beans.PropertyEditor;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import org.openide.explorer.propertysheet.CheckboxInplaceEditor;
import org.openide.explorer.propertysheet.EditorPropertyDisplayer;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertyDisplayer_Inline;
import org.openide.explorer.propertysheet.PropertyDisplayer_Mutable;
import org.openide.explorer.propertysheet.RadioInplaceEditor;
import org.openide.explorer.propertysheet.RendererFactory;
import org.openide.explorer.propertysheet.ReusablePropertyEnv;
import org.openide.explorer.propertysheet.ReusablePropertyModel;
import org.openide.nodes.Node;

final class RendererPropertyDisplayer
extends JComponent
implements PropertyDisplayer_Mutable,
PropertyDisplayer_Inline {
    private boolean showCustomEditorButton = true;
    private boolean tableUI = false;
    private int radioButtonMax = 0;
    private boolean useLabels = true;
    private Node.Property prop;
    private boolean radioBoolean;
    private ReusablePropertyEnv reusableEnv = new ReusablePropertyEnv();
    private ReusablePropertyModel reusableModel = new ReusablePropertyModel(this.reusableEnv);
    boolean inGetRenderer = false;
    private Dimension prefSize = null;
    private RendererFactory rendererFactory1 = null;
    private RendererFactory rendererFactory2 = null;

    public RendererPropertyDisplayer(Node.Property p) {
        this.prop = p;
    }

    @Override
    public Component getComponent() {
        return this;
    }

    @Override
    public Node.Property getProperty() {
        return this.prop;
    }

    @Override
    public void refresh() {
        this.repaint();
    }

    @Override
    public void validate() {
        if (!this.tableUI) {
            super.validate();
        }
    }

    @Override
    public boolean isValid() {
        if (this.tableUI) {
            return true;
        }
        return super.isValid();
    }

    @Override
    public boolean isShowing() {
        if (this.tableUI) {
            return true;
        }
        return super.isShowing();
    }

    @Override
    public void setProperty(Node.Property prop) {
        if (prop == null) {
            throw new NullPointerException("Property cannot be null");
        }
        if (prop != this.prop) {
            this.prop = prop;
            this.prefSize = null;
            if (this.isShowing()) {
                this.firePropertyChange("preferredSize", null, null);
            }
            this.repaint();
        }
    }

    @Override
    public final boolean isShowCustomEditorButton() {
        Boolean explicit;
        boolean result = this.showCustomEditorButton;
        if (this.getProperty() != null && (explicit = (Boolean)this.getProperty().getValue("suppressCustomEditor")) != null) {
            result = explicit == false;
        }
        return result;
    }

    @Override
    public boolean isTableUI() {
        return this.tableUI;
    }

    @Override
    public boolean isUseLabels() {
        return this.useLabels;
    }

    @Override
    public void setUseLabels(boolean useLabels) {
        if (useLabels != this.useLabels) {
            JComponent innermost;
            Dimension oldPreferredSize = null;
            if (this.isShowing() && ((innermost = RendererPropertyDisplayer.findInnermostRenderer(this.getRenderer(this))) instanceof RadioInplaceEditor || innermost instanceof JCheckBox)) {
                oldPreferredSize = this.getPreferredSize();
            }
            this.useLabels = useLabels;
            if (oldPreferredSize != null) {
                this.firePropertyChange("preferredSize", oldPreferredSize, this.getPreferredSize());
            }
        }
    }

    @Override
    public void setShowCustomEditorButton(boolean val) {
        Boolean explicit;
        if (this.getProperty() != null && (explicit = (Boolean)this.getProperty().getValue("suppressCustomEditor")) != null) {
            val = explicit;
        }
        if (this.showCustomEditorButton != val) {
            this.prefSize = null;
            Dimension oldPreferredSize = null;
            if (this.isShowing()) {
                oldPreferredSize = this.getPreferredSize();
            }
            this.showCustomEditorButton = val;
            if (oldPreferredSize != null) {
                this.firePropertyChange("preferredSize", oldPreferredSize, this.getPreferredSize());
                this.repaint();
            }
        }
    }

    @Override
    public void setTableUI(boolean val) {
        if (val != this.tableUI) {
            this.tableUI = val;
            this.repaint();
        }
    }

    @Override
    public int getRadioButtonMax() {
        return this.radioButtonMax;
    }

    @Override
    public void setRadioButtonMax(int i) {
        if (i != this.radioButtonMax) {
            PropertyEditor ed;
            String[] tags;
            Dimension oldPreferredSize = null;
            if (this.isShowing()) {
                oldPreferredSize = this.getPreferredSize();
            }
            int old = this.radioButtonMax;
            this.radioButtonMax = i;
            if (oldPreferredSize != null && (tags = (ed = PropUtils.getPropertyEditor(this.prop)).getTags()) != null && tags.length >= i != tags.length >= old) {
                this.firePropertyChange("preferredSize", oldPreferredSize, this.getPreferredSize());
            }
        }
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("Inline editor for property ");
        sb.append(this.getProperty().getDisplayName());
        sb.append(" = ");
        sb.append((Object)this.getProperty());
        sb.append(" inplace editor=");
        if (!this.inGetRenderer) {
            try {
                sb.append(this.getRenderer(this));
            }
            catch (Exception e) {
                sb.append(e);
            }
        }
        return sb.toString();
    }

    @Override
    public void paintComponent(Graphics g) {
        this.reusableEnv.setNode(EditorPropertyDisplayer.findBeans(this));
        JComponent comp = this.getRenderer(this);
        this.prepareRenderer(comp);
        comp.setBounds(0, 0, this.getWidth(), this.getHeight());
        if (comp instanceof InplaceEditor) {
            JComponent inner = RendererPropertyDisplayer.findInnermostRenderer(comp);
            SwingUtilities.paintComponent(g, comp, this, 0, 0, this.getWidth(), this.getHeight());
            this.removeAll();
            return;
        }
        comp.paint(g);
    }

    protected void superPaintComponent(Graphics g) {
        super.paintComponent(g);
    }

    JComponent getRenderer(PropertyDisplayer_Inline inline) {
        this.inGetRenderer = true;
        JComponent result = this.rfactory(inline).getRenderer(inline.getProperty());
        if (inline.isTableUI() && null == result.getBorder()) {
            result.setBorder(BorderFactory.createEmptyBorder());
        }
        this.inGetRenderer = false;
        return result;
    }

    protected void prepareRenderer(JComponent comp) {
        comp.setBackground(this.getBackground());
        comp.setForeground(this.getForeground());
        comp.setBounds(0, 0, this.getWidth(), this.getHeight());
        JComponent innermost = RendererPropertyDisplayer.findInnermostRenderer(comp);
        if (innermost instanceof JComboBox && comp.getLayout() != null) {
            comp.getLayout().layoutContainer(comp);
        }
        if (!this.isTableUI() && ((InplaceEditor)((Object)comp)).supportsTextEntry()) {
            innermost.setBackground(PropUtils.getTextFieldBackground());
            innermost.setForeground(PropUtils.getTextFieldForeground());
        }
    }

    @Override
    public boolean isTitleDisplayed() {
        if (!this.useLabels) {
            return false;
        }
        JComponent jc = this.getRenderer(this);
        if (jc instanceof InplaceEditor) {
            InplaceEditor innermost = PropUtils.findInnermostInplaceEditor((InplaceEditor)((Object)jc));
            return innermost instanceof CheckboxInplaceEditor || innermost instanceof RadioInplaceEditor;
        }
        return false;
    }

    static JComponent findInnermostRenderer(JComponent comp) {
        if (comp instanceof InplaceEditor) {
            InplaceEditor ine = (InplaceEditor)((Object)comp);
            return PropUtils.findInnermostInplaceEditor(ine).getComponent();
        }
        return comp;
    }

    @Override
    public Dimension getPreferredSize() {
        if (this.prefSize == null) {
            JComponent jc = this.getRenderer(this);
            this.prefSize = jc.getPreferredSize();
        }
        return this.prefSize;
    }

    RendererFactory rfactory(PropertyDisplayer_Inline inline) {
        RendererFactory factory;
        if (inline.isTableUI()) {
            if (this.rendererFactory1 == null) {
                this.rendererFactory1 = new RendererFactory(inline.isTableUI(), inline.getReusablePropertyEnv(), inline.getReusablePropertyEnv().getReusablePropertyModel());
            }
            factory = this.rendererFactory1;
        } else {
            if (this.rendererFactory2 == null) {
                this.rendererFactory2 = new RendererFactory(inline.isTableUI(), inline.getReusablePropertyEnv(), inline.getReusablePropertyEnv().getReusablePropertyModel());
            }
            factory = this.rendererFactory2;
        }
        factory.setUseRadioBoolean(inline.isRadioBoolean());
        factory.setUseLabels(inline.isUseLabels());
        factory.setRadioButtonMax(inline.getRadioButtonMax());
        return factory;
    }

    @Override
    public boolean isRadioBoolean() {
        return this.radioBoolean;
    }

    @Override
    public void setRadioBoolean(boolean b) {
        this.radioBoolean = b;
    }

    @Override
    public ReusablePropertyEnv getReusablePropertyEnv() {
        return this.reusableEnv;
    }

    @Override
    public void firePropertyChange(String name, Object old, Object nue) {
    }
}

