/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.WeakListeners
 */
package org.openide.explorer.propertysheet;

import java.beans.BeanInfo;
import java.beans.FeatureDescriptor;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.explorer.propertysheet.ExPropertyModel;
import org.openide.util.WeakListeners;

@Deprecated
public class DefaultPropertyModel
implements ExPropertyModel,
PropertyChangeListener {
    Object bean;
    String propertyName;
    private PropertyChangeSupport support;
    private PropertyDescriptor prop;
    private Method readMethod;
    private Method writeMethod;
    private Class propertyTypeClass;
    private boolean donotfire;

    public DefaultPropertyModel(Object bean, String propertyName) throws IllegalArgumentException {
        this(bean, DefaultPropertyModel.findInfo(bean, propertyName));
    }

    public DefaultPropertyModel(Object bean, PropertyDescriptor descr) {
        this.donotfire = false;
        this.bean = bean;
        this.propertyName = descr.getName();
        this.support = new PropertyChangeSupport(this);
        this.prop = descr;
        this.propertyTypeClass = descr.getPropertyType();
        this.readMethod = descr.getReadMethod();
        this.writeMethod = descr.getWriteMethod();
        try {
            try {
                Method addList = bean.getClass().getMethod("addPropertyChangeListener", String.class, PropertyChangeListener.class);
                addList.invoke(bean, this.propertyName, WeakListeners.propertyChange((PropertyChangeListener)this, (Object)bean));
            }
            catch (NoSuchMethodException nsme) {
                try {
                    Method addList = bean.getClass().getMethod("addPropertyChangeListener", PropertyChangeListener.class);
                    addList.invoke(bean, WeakListeners.propertyChange((PropertyChangeListener)this, (Object)bean));
                }
                catch (NoSuchMethodException nosme) {}
            }
        }
        catch (Exception e) {
            Logger.getLogger(DefaultPropertyModel.class.getName()).log(Level.WARNING, null, e);
        }
    }

    private static PropertyDescriptor findInfo(Object bean, String name) throws IllegalArgumentException {
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(bean.getClass());
            PropertyDescriptor[] descr = beanInfo.getPropertyDescriptors();
            for (int i = 0; i < descr.length; ++i) {
                if (!descr[i].getName().equals(name)) continue;
                return descr[i];
            }
            throw new IllegalArgumentException("No property named " + name + " in class " + bean.getClass());
        }
        catch (IntrospectionException e) {
            throw (IllegalArgumentException)new IllegalArgumentException(e.toString()).initCause(e);
        }
    }

    @Override
    public Class getPropertyType() {
        return this.propertyTypeClass;
    }

    @Override
    public Object getValue() throws InvocationTargetException {
        try {
            return this.readMethod == null ? null : this.readMethod.invoke(this.bean, new Object[0]);
        }
        catch (IllegalAccessException e) {
            Logger.getLogger(DefaultPropertyModel.class.getName()).log(Level.WARNING, null, e);
            throw new InvocationTargetException(e);
        }
    }

    @Override
    public void setValue(Object v) throws InvocationTargetException {
        try {
            if (this.writeMethod != null) {
                this.donotfire = true;
                this.writeMethod.invoke(this.bean, v);
                this.donotfire = false;
            }
        }
        catch (IllegalAccessException e) {
            Logger.getLogger(DefaultPropertyModel.class.getName()).log(Level.WARNING, null, e);
            throw new InvocationTargetException(e);
        }
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.support.addPropertyChangeListener(l);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.support.removePropertyChangeListener(l);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (this.propertyName.equals(evt.getPropertyName()) && !this.donotfire) {
            this.support.firePropertyChange("value", evt.getOldValue(), evt.getNewValue());
        }
    }

    @Override
    public Class getPropertyEditorClass() {
        return this.prop.getPropertyEditorClass();
    }

    @Override
    public Object[] getBeans() {
        return new Object[]{this.bean};
    }

    @Override
    public FeatureDescriptor getFeatureDescriptor() {
        return this.prop;
    }
}

