/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.propertysheet;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.beans.PropertyEditor;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;

public interface InplaceEditor {
    public static final String COMMAND_SUCCESS = "success";
    public static final String COMMAND_FAILURE = "failure";

    public void connect(PropertyEditor var1, PropertyEnv var2);

    public JComponent getComponent();

    public void clear();

    public Object getValue();

    public void setValue(Object var1);

    public boolean supportsTextEntry();

    public void reset();

    public void addActionListener(ActionListener var1);

    public void removeActionListener(ActionListener var1);

    public KeyStroke[] getKeyStrokes();

    public PropertyEditor getPropertyEditor();

    public PropertyModel getPropertyModel();

    public void setPropertyModel(PropertyModel var1);

    public boolean isKnownComponent(Component var1);

    public static interface Factory {
        public InplaceEditor getInplaceEditor();
    }

}

