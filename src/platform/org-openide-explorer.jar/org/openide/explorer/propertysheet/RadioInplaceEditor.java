/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.WeakSet
 */
package org.openide.explorer.propertysheet;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.beans.FeatureDescriptor;
import java.beans.PropertyEditor;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import org.openide.explorer.propertysheet.AutoGridLayout;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;
import org.openide.util.WeakSet;

class RadioInplaceEditor
extends JPanel
implements InplaceEditor,
ActionListener {
    private transient List<ActionListener> actionListenerList;
    protected transient PropertyEditor editor = null;
    protected transient PropertyEnv env = null;
    protected transient PropertyModel mdl = null;
    protected transient ButtonGroup group = null;
    private boolean tableUI = false;
    boolean isFirstEvent = false;
    private WeakSet<InvRadioButton> buttonCache = new WeakSet();
    private boolean useTitle = false;

    public RadioInplaceEditor(boolean tableUI) {
        this.setLayout(new AutoGridLayout(false));
        this.tableUI = tableUI;
        this.setOpaque(true);
    }

    @Override
    public void clear() {
        this.editor = null;
        this.env = null;
        this.mdl = null;
        this.group = null;
        Component[] c = this.getComponents();
        for (int i = 0; i < c.length; ++i) {
            if (!(c[i] instanceof JRadioButton)) continue;
            ((JRadioButton)c[i]).removeActionListener(this);
        }
        this.removeAll();
        this.setEnabled(true);
    }

    @Override
    public Dimension getPreferredSize() {
        if (this.getLayout() != null) {
            return this.getLayout().preferredLayoutSize(this);
        }
        return super.getPreferredSize();
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.isFirstEvent = true;
    }

    private InvRadioButton[] getButtons(int count) {
        InvRadioButton[] result = new InvRadioButton[count];
        Iterator i = this.buttonCache.iterator();
        int idx = 0;
        while (i.hasNext() && idx < count) {
            result[idx] = (InvRadioButton)i.next();
            if (result[idx] == null) continue;
            result[idx].setEnabled(true);
            result[idx].setSelected(false);
            ++idx;
        }
        while (idx < count) {
            result[idx] = this.createButton();
            this.buttonCache.add((Object)result[idx]);
            ++idx;
        }
        return result;
    }

    @Override
    public void setEnabled(boolean val) {
        super.setEnabled(val);
        Component[] c = this.getComponents();
        for (int i = 0; i < c.length; ++i) {
            c[i].setEnabled(val);
        }
    }

    @Override
    public void setBackground(Color col) {
        super.setBackground(col);
        Component[] c = this.getComponents();
        for (int i = 0; i < c.length; ++i) {
            c[i].setBackground(col);
        }
    }

    @Override
    public void setForeground(Color col) {
        super.setForeground(col);
        Component[] c = this.getComponents();
        for (int i = 0; i < c.length; ++i) {
            c[i].setForeground(col);
        }
    }

    @Override
    public void requestFocus() {
        Component[] c = this.getComponents();
        if (c.length > 0) {
            for (int i = 0; i < c.length; ++i) {
                if (!(c[i] instanceof InvRadioButton) || !((InvRadioButton)c[i]).isSelected()) continue;
                c[i].requestFocus();
                return;
            }
            c[0].requestFocus();
        } else {
            super.requestFocus();
        }
    }

    @Override
    public boolean requestFocusInWindow() {
        Component[] c = this.getComponents();
        for (int i = 0; i < c.length; ++i) {
            if (!(c[i] instanceof InvRadioButton) || !((InvRadioButton)c[i]).isSelected()) continue;
            return c[i].requestFocusInWindow();
        }
        return super.requestFocusInWindow();
    }

    public void setUseTitle(boolean val) {
        if (this.useTitle != val) {
            this.useTitle = val;
            if (this.env != null) {
                this.setBorder(new TitledBorder(this.env.getFeatureDescriptor().getDisplayName()));
            }
        }
    }

    @Override
    public void connect(PropertyEditor pe, PropertyEnv env) {
        if (!this.tableUI && env != null && this.useTitle) {
            this.setBorder(new TitledBorder(env.getFeatureDescriptor().getDisplayName()));
        } else {
            this.setBorder(null);
        }
        this.editor = pe;
        String[] tags = this.editor.getTags();
        this.group = new ButtonGroup();
        InvRadioButton[] buttons = this.getButtons(tags.length);
        if (env != null) {
            this.setEnabled(env.isEditable());
        }
        for (int i = 0; i < tags.length; ++i) {
            InvRadioButton jr = buttons[i];
            this.configureButton(jr, tags[i]);
            this.add(jr);
        }
    }

    protected InvRadioButton createButton() {
        return new InvRadioButton();
    }

    protected void configureButton(InvRadioButton ire, String txt) {
        ire.addActionListener(this);
        if (this.editor.getTags().length == 1) {
            ire.setEnabled(false);
        } else {
            ire.setEnabled(this.isEnabled());
        }
        if (this.tableUI) {
            ire.setFocusable(false);
        } else {
            ire.setFocusable(true);
        }
        ire.setText(txt);
        if (txt.equals(this.editor.getAsText())) {
            ire.setSelected(true);
        } else {
            ire.setSelected(false);
        }
        ire.setFont(this.getFont());
        ire.setBackground(this.getBackground());
        ire.setForeground(this.getForeground());
        this.group.add(ire);
    }

    @Override
    public JComponent getComponent() {
        return this;
    }

    @Override
    public KeyStroke[] getKeyStrokes() {
        return null;
    }

    @Override
    public PropertyEditor getPropertyEditor() {
        return this.editor;
    }

    @Override
    public PropertyModel getPropertyModel() {
        return this.mdl;
    }

    @Override
    public Object getValue() {
        Component[] c = this.getComponents();
        for (int i = 0; i < c.length; ++i) {
            if (!(c[i] instanceof JRadioButton) || this.group.getSelection() != ((JRadioButton)c[i]).getModel()) continue;
            String result = ((JRadioButton)c[i]).getText();
            return result;
        }
        return null;
    }

    public void handleInitialInputEvent(InputEvent e) {
        System.err.println("HandleInitialInputEvent");
        this.getLayout().layoutContainer(this);
        if (e instanceof MouseEvent) {
            Point p = SwingUtilities.convertPoint((JComponent)e.getSource(), ((MouseEvent)e).getPoint(), this);
            Component c = this.getComponentAt(p);
            if (c instanceof JRadioButton) {
                ((JRadioButton)c).setSelected(true);
                c.requestFocus();
                this.fireActionPerformed(new ActionEvent(this, 1001, "success"));
            }
        } else {
            Component[] c = this.getComponents();
            for (int i = 0; i < c.length; ++i) {
                if (!(c[i] instanceof JRadioButton) || !((JRadioButton)c[i]).isSelected()) continue;
                c[i].requestFocusInWindow();
            }
        }
    }

    @Override
    public boolean isKnownComponent(Component c) {
        return c != null && (c == this || c instanceof InvRadioButton);
    }

    @Override
    public void reset() {
        this.setValue(this.editor.getAsText());
    }

    @Override
    public void setPropertyModel(PropertyModel pm) {
        this.mdl = pm;
    }

    @Override
    public void setValue(Object o) {
        Component[] c = this.getComponents();
        for (int i = 0; i < c.length; ++i) {
            if (!(c[i] instanceof JRadioButton)) continue;
            if (((JRadioButton)c[i]).getText().equals(o)) {
                ((JRadioButton)c[i]).setSelected(true);
                continue;
            }
            ((JRadioButton)c[i]).setSelected(false);
        }
    }

    @Override
    public boolean supportsTextEntry() {
        return false;
    }

    @Override
    public synchronized void addActionListener(ActionListener listener) {
        if (this.actionListenerList == null) {
            this.actionListenerList = new ArrayList<ActionListener>();
        }
        this.actionListenerList.add(listener);
    }

    @Override
    public synchronized void removeActionListener(ActionListener listener) {
        if (this.actionListenerList != null) {
            this.actionListenerList.remove(listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fireActionPerformed(final ActionEvent event) {
        List list;
        RadioInplaceEditor radioInplaceEditor = this;
        synchronized (radioInplaceEditor) {
            if (this.actionListenerList == null) {
                return;
            }
            list = (List)((ArrayList)this.actionListenerList).clone();
        }
        final List theList = list;
        if (this.tableUI) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    for (int i = 0; i < theList.size(); ++i) {
                        ((ActionListener)theList.get(i)).actionPerformed(event);
                    }
                }
            });
        } else {
            for (int i = 0; i < list.size(); ++i) {
                ((ActionListener)theList.get(i)).actionPerformed(event);
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ActionEvent ae = new ActionEvent(this, 1001, "success");
        this.fireActionPerformed(ae);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void paint(Graphics g) {
        if (this.isShowing()) {
            super.paint(g);
        } else {
            this.getLayout().layoutContainer(this);
            Component[] c = this.getComponents();
            Color col = g.getColor();
            try {
                g.setColor(this.getBackground());
                g.fillRect(0, 0, this.getWidth(), this.getHeight());
                for (int i = 0; i < c.length; ++i) {
                    Rectangle r = c[i].getBounds();
                    if (!g.hitClip(r.x, r.y, r.width, r.height)) continue;
                    Graphics g2 = g.create(r.x, r.y, r.width, r.height);
                    try {
                        c[i].paint(g2);
                    }
                    finally {
                        g2.dispose();
                    }
                }
                if (this.getBorder() != null) {
                    super.paintBorder(g);
                }
            }
            finally {
                g.setColor(col);
            }
        }
    }

    @Override
    public void processMouseEvent(MouseEvent me) {
        if (this.isFirstEvent) {
            this.handleInitialInputEvent(me);
            this.isFirstEvent = false;
        } else {
            super.processMouseEvent(me);
        }
    }

    @Override
    public Component getComponentAt(int x, int y) {
        this.getLayout().layoutContainer(this);
        Component result = super.getComponentAt(x, y);
        System.err.println("getComponentAt " + x + "," + y + " returning " + result.getName());
        return result;
    }

    class InvRadioButton
    extends JRadioButton {
        @Override
        public String getName() {
            return "InvRadioButton - " + this.getText();
        }

        @Override
        public void processKeyEvent(KeyEvent ke) {
            super.processKeyEvent(ke);
            if ((ke.getKeyCode() == 10 || ke.getKeyCode() == 27) && ke.getID() == 401) {
                RadioInplaceEditor.this.fireActionPerformed(new ActionEvent(this, 1001, ke.getKeyCode() == 10 ? "success" : "failure"));
            }
        }

        @Override
        public Dimension getPreferredSize() {
            int w = 0;
            int h = 0;
            Graphics g = PropUtils.getScratchGraphics(this);
            FontMetrics fm = g.getFontMetrics(this.getFont());
            if (this.getIcon() != null) {
                w = this.getIcon().getIconWidth();
                h = this.getIcon().getIconHeight();
            }
            if (this.getBorder() != null) {
                Insets ins = this.getBorder().getBorderInsets(this);
                w += ins.left + ins.right;
                h += ins.bottom + ins.top;
            }
            h = Math.max(fm.getHeight(), h) + 2;
            return new Dimension(w += fm.stringWidth(this.getText()) + 22, h);
        }
    }

}

