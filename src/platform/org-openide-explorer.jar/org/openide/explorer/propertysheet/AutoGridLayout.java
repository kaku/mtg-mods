/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.propertysheet;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.util.Arrays;
import java.util.Comparator;
import org.openide.explorer.propertysheet.PropUtils;

class AutoGridLayout
implements LayoutManager {
    int gapY = 5;
    boolean pack;

    public AutoGridLayout(boolean pack) {
        this.pack = pack;
    }

    @Override
    public void addLayoutComponent(String name, Component comp) {
    }

    @Override
    public void removeLayoutComponent(Component comp) {
    }

    private Comparator<Component> comparator() {
        return new PreferredSizeComparator(this.pack);
    }

    @Override
    public void layoutContainer(Container parent) {
        Component[] c = parent.getComponents();
        if (c.length > 3) {
            Arrays.sort(c, this.comparator());
        }
        if (c.length == 2) {
            Dimension d0 = c[0].getPreferredSize();
            Dimension d1 = c[1].getPreferredSize();
            c[0].setBounds(0, 0, d0.width, d0.height);
            c[1].setBounds(d0.width, 0, d1.width, d1.height);
            return;
        }
        Insets insets = parent.getInsets();
        int w = parent.getWidth() - insets.right;
        int h = parent.getHeight() - insets.bottom;
        int currx = insets.left;
        int curry = insets.top;
        boolean done = false;
        int cols = -1;
        for (int i = 0; i < c.length; ++i) {
            Dimension d = c[i].getPreferredSize();
            if (d.width == 0 || d.height == 0) {
                d = PropUtils.getMinimumPanelSize();
            }
            if (currx + d.width > w) {
                curry += d.height + this.gapY;
                currx = insets.left;
                if (cols == -1) {
                    cols = i;
                    break;
                }
            }
            c[i].setBounds(currx, curry, d.width, d.height);
            currx += d.width;
        }
        if (cols == -1) {
            cols = c.length;
        }
        int currCol = 0;
        for (int i2 = cols; i2 < c.length; ++i2) {
            Dimension d = c[i2].getPreferredSize();
            if (currx + d.width > w) {
                curry += d.height + this.gapY;
                currx = insets.left;
                currCol = 0;
            }
            boolean bl = done = curry + d.height > h;
            if (!done) {
                if (d.width <= w) {
                    int currColWidth;
                    for (currColWidth = c[currCol].getWidth(); currColWidth <= d.width; currColWidth += c[currCol].getWidth()) {
                        if (++currCol <= cols) continue;
                        currCol = 0;
                        curry += d.height + this.gapY;
                        currx = insets.left;
                        currColWidth = 0;
                    }
                    c[i2].setBounds(currx, curry, d.width, d.height);
                    currx += currColWidth;
                } else {
                    c[i2].setBounds(currx, curry, d.width, d.height);
                    currx += d.width;
                }
                if (currx > w) {
                    currx = insets.left;
                    curry += d.height + this.gapY;
                    currCol = 0;
                    continue;
                }
                ++currCol;
                continue;
            }
            c[i2].setBounds(0, 0, 0, 0);
        }
    }

    @Override
    public Dimension minimumLayoutSize(Container parent) {
        return this.preferredLayoutSize(parent);
    }

    @Override
    public Dimension preferredLayoutSize(Container parent) {
        Component[] c = parent.getComponents();
        if (c.length > 3) {
            Arrays.sort(c, this.comparator());
        }
        Dimension max = Toolkit.getDefaultToolkit().getScreenSize();
        max.width /= 2;
        max.height /= 2;
        Insets insets = parent.getInsets();
        int w = max.width - insets.right;
        int currx = insets.left;
        int cols = -1;
        int baseHeight = 0;
        Dimension[] dims = new Dimension[c.length];
        Dimension result = new Dimension();
        for (int i = 0; i < c.length; ++i) {
            dims[i] = c[i].getPreferredSize();
            if (dims[i].width == 0 || dims[i].height == 0) {
                dims[i] = PropUtils.getMinimumPanelSize();
            }
            baseHeight = Math.max(baseHeight, dims[i].height);
            if (cols == -1 && currx + dims[i].width > w) {
                result.width = currx;
                cols = i;
            }
            if (cols != -1) {
                result.width = Math.max(result.width, dims[i].width + insets.left + insets.right);
            }
            currx += dims[i].width;
        }
        if (cols == -1) {
            cols = c.length;
            result.width = currx;
        }
        if (!this.pack && c.length > 3) {
            int rows = c.length / cols + (c.length % cols != 0 ? 1 : 0);
            result.height = baseHeight * rows + this.gapY * rows + insets.top + insets.bottom;
            result.width += 6;
            assert (result.width >= 0 && result.height >= 0);
            return result;
        }
        int currRow = 0;
        int currCol = 0;
        currx = insets.left;
        for (int i2 = cols; i2 < c.length; ++i2) {
            int colspan = 1;
            int colwidth = dims[currCol].width;
            while (dims[i2].width > colwidth) {
                ++colspan;
                if ((colwidth += dims[++currCol].width) + currx <= max.width) continue;
                currCol = 0;
                ++currRow;
                colspan = 1;
                colwidth = dims[currCol].width;
            }
            currx += colwidth;
            if ((currCol += colspan) <= cols || i2 == c.length - 1) continue;
            currCol = 0;
            ++currRow;
            currx = insets.left;
        }
        result.height = baseHeight * currRow + insets.top + insets.bottom + this.gapY * currRow;
        return result;
    }

    private static final class PreferredSizeComparator
    implements Comparator<Component> {
        boolean smallFirst;

        public PreferredSizeComparator(boolean smallFirst) {
            this.smallFirst = smallFirst;
        }

        @Override
        public int compare(Component c1, Component c2) {
            Dimension d1 = c1.getPreferredSize();
            Dimension d2 = c2.getPreferredSize();
            return this.smallFirst ? d1.width - d2.width : d2.width - d1.width;
        }
    }

}

