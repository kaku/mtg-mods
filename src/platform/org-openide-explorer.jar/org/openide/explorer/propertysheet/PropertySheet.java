/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.nodes.NodeAdapter
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.HelpCtx$Provider
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.openide.explorer.propertysheet;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.explorer.propertysheet.PSheet;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertySetModel;
import org.openide.explorer.propertysheet.ProxyNode;
import org.openide.explorer.propertysheet.ReusablePropertyEnv;
import org.openide.explorer.propertysheet.ReusablePropertyModel;
import org.openide.explorer.propertysheet.SelectionAndScrollPositionManager;
import org.openide.explorer.propertysheet.SheetTable;
import org.openide.nodes.Node;
import org.openide.nodes.NodeAdapter;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

public class PropertySheet
extends JPanel {
    static final long serialVersionUID = -7698351033045864945L;
    public static final String PROPERTY_SELECTED_PROPERTY = "selectedFeatureDescriptor";
    @Deprecated
    public static final String PROPERTY_SORTING_MODE = "sortingMode";
    @Deprecated
    public static final String PROPERTY_VALUE_COLOR = "valueColor";
    @Deprecated
    public static final String PROPERTY_DISABLED_PROPERTY_COLOR = "disabledPropertyColor";
    @Deprecated
    public static final String PROPERTY_CURRENT_PAGE = "currentPage";
    @Deprecated
    public static final String PROPERTY_PLASTIC = "plastic";
    @Deprecated
    public static final String PROPERTY_PROPERTY_PAINTING_STYLE = "propertyPaintingStyle";
    @Deprecated
    public static final String PROPERTY_DISPLAY_WRITABLE_ONLY = "displayWritableOnly";
    @Deprecated
    public static final int ALWAYS_AS_STRING = 1;
    @Deprecated
    public static final int STRING_PREFERRED = 2;
    @Deprecated
    public static final int PAINTING_PREFERRED = 3;
    public static final int UNSORTED = 0;
    public static final int SORTED_BY_NAMES = 1;
    @Deprecated
    public static final int SORTED_BY_TYPES = 2;
    @Deprecated
    protected static Icon iNoSort;
    @Deprecated
    protected static Icon iAlphaSort;
    @Deprecated
    protected static Icon iTypeSort;
    @Deprecated
    protected static Icon iDisplayWritableOnly;
    @Deprecated
    protected static Icon iCustomize;
    private static final String ACTION_INVOKE_POPUP = "invokePopup";
    private static final String ACTION_INVOKE_HELP = "invokeHelp";
    private static final int INIT_DELAY = 70;
    private static final int MAX_DELAY = 150;
    private static final boolean neverTabs;
    static final boolean forceTabs;
    private static final RequestProcessor RP;
    private int sortingMode = 0;
    private boolean showDesc;
    private Reference<Node> storedNode;
    SheetTable table = new SheetTable();
    PSheet psheet = new PSheet();
    HelpAction helpAction;
    transient Node[] helperNodes;
    private transient RequestProcessor.Task scheduleTask;
    private transient RequestProcessor.Task initTask;
    SheetPCListener pclistener;
    private FeatureDescriptor _selectedFeature;
    private boolean popupEnabled;

    public PropertySheet() {
        this.helpAction = new HelpAction();
        this.pclistener = new SheetPCListener();
        this.popupEnabled = true;
        this.init();
        this.initActions();
    }

    public FeatureDescriptor getSelectedProperty() {
        return this._selectedFeature;
    }

    public void setSelectedProperty(FeatureDescriptor fd) {
        if (this._selectedFeature != fd) {
            FeatureDescriptor old = this._selectedFeature;
            this._selectedFeature = fd;
            this.firePropertyChange("selectedFeatureDescriptor", old, fd);
        }
    }

    private void initActions() {
        MutableAction invokePopupAction = new MutableAction(2, this);
        this.table.getInputMap().put(KeyStroke.getKeyStroke(121, 1), "invokePopup");
        this.table.getActionMap().put("invokePopup", invokePopupAction);
        this.getInputMap(1).put(KeyStroke.getKeyStroke(121, 1), "invokePopup");
        this.getActionMap().put("invokePopup", invokePopupAction);
        this.getInputMap(1).put(KeyStroke.getKeyStroke(112, 0), "invokeHelp");
        this.getActionMap().put("invokeHelp", this.helpAction);
    }

    @Override
    public void addNotify() {
        super.addNotify();
        Node oldSelection = null;
        if (this.storedNode != null) {
            oldSelection = this.storedNode.get();
        }
        if (oldSelection != null) {
            this.setCurrentNode(oldSelection);
        }
    }

    @Override
    public void updateUI() {
        UIManager.get("nb.propertysheet");
        super.updateUI();
    }

    @Override
    public void removeNotify() {
        Node lastSel = null;
        if (this.pclistener != null) {
            lastSel = this.pclistener.detach();
        }
        this.doSetNodes(null);
        if (lastSel != null) {
            this.storedNode = new WeakReference<Node>(lastSel);
        }
        super.removeNotify();
        this.table.getReusablePropertyEnv().setBeans(null);
        this.table.getReusablePropertyEnv().setNode(null);
        this.table.getReusablePropertyModel().setProperty(null);
    }

    public boolean isQuickSearchAllowed() {
        return this.table.isQuickSearchAllowed();
    }

    public void setQuickSearchAllowed(boolean isQuickSearchAllowed) {
        this.table.setQuickSearchAllowed(isQuickSearchAllowed);
    }

    private void init() {
        Font f = UIManager.getFont("controlFont");
        if (f == null) {
            f = UIManager.getFont("Tree.font");
        }
        if (f != null) {
            this.table.setFont(f);
        }
        this.showDesc = PropUtils.shouldShowDescription();
        this.setLayout(new BorderLayout());
        this.psheet.setBackground(this.table.getBackground());
        this.setBackground(this.table.getBackground());
        this.psheet.setMarginColor(PropUtils.getSetRendererColor());
        this.psheet.add(this.table);
        this.add((Component)this.psheet, "Center");
        this.table.setBorder(BorderFactory.createEmptyBorder());
        this.setDescriptionVisible(this.showDesc);
        this.setMinimumSize(new Dimension(100, 50));
        this.psheet.setEmptyString(NbBundle.getMessage(PropertySheet.class, (String)"CTL_NoProperties"));
        TabSelectionListener listener = new TabSelectionListener();
        this.psheet.addSelectionChangeListener(listener);
        this.table.addChangeListener(listener);
        try {
            this.setSortingMode(PropUtils.getSavedSortOrder());
        }
        catch (PropertyVetoException e) {
            Exceptions.printStackTrace((Throwable)e);
        }
    }

    public final void setPopupEnabled(boolean val) {
        this.popupEnabled = val;
    }

    public final void setDescriptionAreaVisible(boolean val) {
        if (this.isDescriptionVisible() != val) {
            int state = this.psheet.getState();
            if (!val) {
                int newState = (state & 2) != 0 ? 2 : 0;
                this.psheet.setState(newState);
            } else {
                int newState = (state & 2) != 0 ? 3 : 1;
                this.psheet.setState(newState);
            }
        }
    }

    void setDescriptionVisible(boolean val) {
        this.setDescriptionAreaVisible(val);
        PropUtils.saveShowDescription(val);
    }

    boolean isDescriptionVisible() {
        return (this.psheet.getState() & 1) != 0;
    }

    @Override
    public void requestFocus() {
        if (this.table.getParent() != null) {
            this.table.requestFocus();
        } else {
            super.requestFocus();
        }
    }

    @Override
    public boolean requestFocusInWindow() {
        if (this.table.getParent() != null) {
            return this.table.requestFocusInWindow();
        }
        return super.requestFocusInWindow();
    }

    private void doSetNodes(Node[] nodes) {
        if (nodes == null || nodes.length == 0) {
            this.table.getPropertySetModel().setPropertySets(null);
            this.table.getReusablePropertyEnv().clear();
            this.psheet.setTabbedContainerItems(new Object[0], new String[0]);
            return;
        }
        ProxyNode n = nodes.length == 1 ? nodes[0] : new ProxyNode(nodes);
        this.setCurrentNode((Node)n);
    }

    public synchronized void setNodes(Node[] nodes) {
        final boolean loggable = PropUtils.isLoggable(PropertySheet.class);
        if (loggable) {
            PropUtils.log(PropertySheet.class, "SetNodes " + (null == nodes ? "<null>" : Arrays.asList(nodes)));
        }
        if (nodes != null && nodes.length > 0 && this.pclistener != null) {
            if (nodes.length == 1 && nodes[0] == this.pclistener.getNode()) {
                if (loggable) {
                    PropUtils.log(PropertySheet.class, "  Same node selected as before; no redisplay needed");
                }
                return;
            }
            if (this.pclistener.getNode() instanceof ProxyNode) {
                if (loggable) {
                    PropUtils.log(PropertySheet.class, "  Selected node is a proxy node - comparing contents.");
                }
                Node[] currNodes = ((ProxyNode)this.pclistener.getNode()).getOriginalNodes();
                if (Arrays.asList(nodes).equals(Arrays.asList(currNodes))) {
                    if (loggable) {
                        PropUtils.log(PropertySheet.class, "  Proxy node represents the same nodes already showing.  Showing: " + Arrays.asList(currNodes) + " requested " + Arrays.asList(nodes));
                        HashSet<Node> currs = new HashSet<Node>(Arrays.asList(currNodes));
                        HashSet<Node> reqs = new HashSet<Node>(Arrays.asList(nodes));
                        if (currs.size() != currNodes.length) {
                            PropUtils.log(PropertySheet.class, " A hashSet of the current nodes does NOT have the same number  of elements as the array of current nodes!  Check your hashCode()/equals() contract.  One or more nodes in the array are claiming to be the same node.");
                        }
                        if (reqs.size() != nodes.length) {
                            PropUtils.log(PropertySheet.class, " A hashSet of the requested selected nodes does NOT have the same number  of elements as the array of current nodes!  Check your hashCode()/equals() contract One or more nodes in the array are claiming to be the same node.");
                        }
                    }
                    return;
                }
            }
        } else if (nodes == null || nodes.length == 0) {
            RequestProcessor.Task curTask;
            if (this.pclistener != null) {
                this.pclistener.detach();
            }
            if (!(curTask = this.getScheduleTask()).equals((Object)this.initTask)) {
                curTask.cancel();
            }
            if (EventQueue.isDispatchThread()) {
                if (loggable) {
                    PropUtils.log(PropertySheet.class, "  Nodes cleared on event queue.  Emptying model.");
                }
                this.table.getPropertySetModel().setPropertySets(null);
                this.table.getReusablePropertyEnv().clear();
                this.helperNodes = null;
                this.psheet.setTabbedContainerItems(new Object[0], new String[0]);
            } else {
                EventQueue.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        if (loggable) {
                            PropUtils.log(PropertySheet.class, "  Nodes cleared off event queue.  Empty model later on EQ.");
                        }
                        PropertySheet.this.table.getPropertySetModel().setPropertySets(null);
                        PropertySheet.this.table.getReusablePropertyEnv().clear();
                        PropertySheet.this.helperNodes = null;
                        PropertySheet.this.psheet.setTabbedContainerItems(new Object[0], new String[0]);
                    }
                });
            }
            return;
        }
        RequestProcessor.Task task = this.getScheduleTask();
        this.helperNodes = nodes;
        this.storedNode = null;
        if (task.equals((Object)this.initTask)) {
            this.scheduleTask.schedule(0);
            task.schedule(70);
        } else {
            int delay = task.getDelay() * 2;
            if (delay > 150) {
                delay = 150;
            }
            if (delay < 70) {
                delay = 70;
            }
            if (loggable) {
                PropUtils.log(PropertySheet.class, " Scheduling delayed update of selected nodes.");
            }
            task.schedule(delay);
        }
    }

    private synchronized RequestProcessor.Task getScheduleTask() {
        if (this.scheduleTask == null) {
            this.scheduleTask = RP.post(new Runnable(){

                @Override
                public void run() {
                    EventQueue.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            Node[] tmp = PropertySheet.this.helperNodes;
                            if (tmp != null) {
                                for (Node n : tmp) {
                                    n.getPropertySets();
                                }
                            }
                            boolean loggable = PropUtils.isLoggable(PropertySheet.class);
                            Node[] nodesToSet = PropertySheet.this.helperNodes;
                            if (loggable) {
                                PropUtils.log(PropertySheet.class, "Delayed updater setting nodes to " + (null == nodesToSet ? "null" : Arrays.asList(nodesToSet)));
                            }
                            PropertySheet.this.doSetNodes(nodesToSet);
                        }
                    });
                }

            });
            this.initTask = RP.post(new Runnable(){

                @Override
                public void run() {
                }
            });
        }
        if (this.initTask.isFinished() && this.scheduleTask.isFinished()) {
            return this.initTask;
        }
        return this.scheduleTask;
    }

    void setCurrentNode(Node node) {
        boolean usingTabs;
        boolean loggable;
        Node old = this.pclistener.getNode();
        if (old != node) {
            this.psheet.storeScrollAndTabInfo();
        }
        if (loggable = PropUtils.isLoggable(PropertySheet.class)) {
            PropUtils.log(PropertySheet.class, "SetCurrentNode:" + (Object)node);
        }
        PropertySetModel psm = this.table.getPropertySetModel();
        Node.PropertySet[] ps = node.getPropertySets();
        if (ps == null) {
            Logger.getAnonymousLogger().warning("Node " + (Object)node + ": getPropertySets() returns null!");
            ps = new Node.PropertySet[]{};
        }
        this.table.getReusablePropertyEnv().setNode((Object)node);
        assert (this.noNullPropertyLists(ps));
        if (this.table.isEditing()) {
            this.table.removeEditor();
        }
        if (usingTabs = PropertySheet.needTabs(node)) {
            this.psheet.setState(this.psheet.getState() | 2);
            TabInfo info = PropertySheet.getTabItems(node);
            this.psheet.setTabbedContainerItems(info.sets, info.titles);
            this.psheet.manager().setCurrentNodeName(node.getName());
            if (info.sets.length > 0) {
                psm.setPropertySets(info.getSets(0));
            }
        } else {
            psm.setPropertySets(ps);
            this.psheet.setState((this.psheet.getState() & 1) != 0 ? 1 : 0);
            this.psheet.setTabbedContainerItems(new Object[0], new String[0]);
        }
        this.psheet.adjustForName(node.getName());
        this.table.setBeanName(node.getDisplayName());
        String description = (String)node.getValue("nodeDescription");
        this.psheet.setDescription(null, null);
        this.psheet.setPropId(null);
        this.setSelectedProperty(null);
        this.pclistener.attach(node);
        if (this.isDescriptionVisible()) {
            this.helpAction.checkContext();
        }
    }

    private boolean noNullPropertyLists(Node.PropertySet[] ps) {
        boolean result = true;
        for (int i = 0; i < ps.length && (result &= ps[i] != null && ps[i].getProperties() != null); ++i) {
        }
        return result;
    }

    @Deprecated
    public void setPropertyPaintingStyle(int style) {
    }

    @Deprecated
    public int getPropertyPaintingStyle() {
        return 0;
    }

    public void setSortingMode(int sortingMode) throws PropertyVetoException {
        try {
            this.table.getPropertySetModel().setComparator(PropUtils.getComparator(sortingMode));
            this.sortingMode = sortingMode;
            this.psheet.setMarginPainted(false);
            PropUtils.putSortOrder(sortingMode);
        }
        catch (IllegalArgumentException iae) {
            throw new PropertyVetoException(NbBundle.getMessage(PropertySheet.class, (String)"EXC_Unknown_sorting_mode"), new PropertyChangeEvent(this, "sortingMode", new Integer(0), new Integer(sortingMode)));
        }
    }

    public int getSortingMode() {
        return this.sortingMode;
    }

    @Deprecated
    public void setCurrentPage(int index) {
    }

    @Deprecated
    public boolean setCurrentPage(String str) {
        return false;
    }

    @Deprecated
    public int getCurrentPage() {
        return 0;
    }

    @Deprecated
    public void setPlastic(boolean plastic) {
    }

    @Deprecated
    public boolean getPlastic() {
        return false;
    }

    @Deprecated
    public void setValueColor(Color color) {
    }

    @Deprecated
    public Color getValueColor() {
        return Color.BLACK;
    }

    @Deprecated
    public void setDisabledPropertyColor(Color color) {
    }

    @Deprecated
    public Color getDisabledPropertyColor() {
        return Color.GRAY;
    }

    @Deprecated
    public void setDisplayWritableOnly(boolean b) {
    }

    @Deprecated
    public boolean getDisplayWritableOnly() {
        return false;
    }

    final void showPopup(Point p) {
        if (!this.popupEnabled) {
            return;
        }
        JPopupMenu popup = this.createPopupMenu();
        if (null == popup) {
            JMenuItem helpItem = new JMenuItem();
            JRadioButtonMenuItem sortNamesItem = new JRadioButtonMenuItem();
            JRadioButtonMenuItem unsortedItem = new JRadioButtonMenuItem();
            JCheckBoxMenuItem descriptionItem = new JCheckBoxMenuItem();
            JMenuItem defaultValueItem = new JMenuItem();
            popup = new JPopupMenu();
            unsortedItem.setSelected(this.getSortingMode() == 0);
            sortNamesItem.setSelected(this.getSortingMode() == 1);
            this.helpAction.checkContext();
            helpItem.setAction(this.helpAction);
            sortNamesItem.setAction(new MutableAction(0, this));
            unsortedItem.setAction(new MutableAction(1, this));
            descriptionItem.setAction(new MutableAction(3, this));
            descriptionItem.setSelected(this.isDescriptionVisible());
            defaultValueItem.setAction(new MutableAction(5, this));
            FeatureDescriptor fd = this.table.getSelection();
            defaultValueItem.setEnabled(PropUtils.shallBeRDVEnabled(fd));
            popup.add(unsortedItem);
            popup.add(sortNamesItem);
            popup.add(new JSeparator());
            popup.add(descriptionItem);
            popup.add(new JSeparator());
            popup.add(defaultValueItem);
            popup.add(new JSeparator());
            popup.add(helpItem);
        }
        popup.show(this.psheet, p.x, p.y);
    }

    protected JPopupMenu createPopupMenu() {
        return null;
    }

    protected final boolean isExpanded(FeatureDescriptor fd) {
        return this.table.getPropertySetModel().isExpanded(fd);
    }

    protected final void toggleExpanded(FeatureDescriptor fd) {
        int index = this.table.getPropertySetModel().indexOf(fd);
        if (index >= 0) {
            this.table.getPropertySetModel().toggleExpanded(index);
        }
    }

    protected final FeatureDescriptor getSelection() {
        return this.table.getSelection();
    }

    public void select(FeatureDescriptor fd, boolean startEditing) {
        this.table.select(fd, startEditing);
    }

    public void changeSelection(FeatureDescriptor fd, boolean toggle, boolean extend) {
        this.table.changeSelection(fd, toggle, extend);
    }

    public void paintRow(FeatureDescriptor fd) {
        this.table.paintRow(fd);
    }

    Node[] getCurrentNodes() {
        Node n = this.pclistener.getNode();
        if (n != null) {
            if (n instanceof ProxyNode) {
                return ((ProxyNode)n).getOriginalNodes();
            }
            return new Node[]{n};
        }
        return new Node[0];
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private static final boolean needTabs(Node n) {
        boolean needTabs = true;
        if (forceTabs) {
            return true;
        }
        if (n instanceof ProxyNode) {
            Node[] nodes = ((ProxyNode)n).getOriginalNodes();
            int i = 0;
            while (i < nodes.length) {
                assert (nodes[i] != n);
                if (!(needTabs &= PropertySheet.needTabs(nodes[i]))) return needTabs;
                ++i;
            }
            return needTabs;
        }
        Node.PropertySet[] ps = n.getPropertySets();
        boolean bl = forceTabs ? ps.length > 1 : (needTabs = neverTabs ? false : false);
        if (neverTabs) return needTabs;
        int i = 0;
        while (i < ps.length) {
            if (needTabs) return needTabs;
            if (ps[i] == null) {
                throw new NullPointerException("Node " + (Object)n + " contains null in its getPropertySets() array");
            }
            needTabs |= ps[i].getValue("tabName") != null;
            ++i;
        }
        return needTabs;
    }

    private static final TabInfo getTabItems(Node n) {
        HashMap<String, ArrayList<Node.PropertySet>> titlesToContents = new HashMap<String, ArrayList<Node.PropertySet>>();
        ArrayList<String> order = new ArrayList<String>();
        Node.PropertySet[] sets = n.getPropertySets();
        for (int i = 0; i < sets.length; ++i) {
            ArrayList<Node.PropertySet> l;
            String currTab = (String)sets[i].getValue("tabName");
            if (currTab == null) {
                currTab = PropUtils.basicPropsTabName();
            }
            if ((l = (ArrayList<Node.PropertySet>)titlesToContents.get(currTab)) == null) {
                l = new ArrayList<Node.PropertySet>();
                l.add(sets[i]);
                titlesToContents.put(currTab, l);
            } else {
                l.add(sets[i]);
            }
            if (order.contains(currTab)) continue;
            order.add(currTab);
        }
        String[] titles = new String[order.size()];
        Object[] setSets = new Object[order.size()];
        int count = 0;
        for (String titles[count] : order) {
            List currSets = (List)titlesToContents.get(titles[count]);
            setSets[count] = new Node.PropertySet[currSets.size()];
            setSets[count] = currSets.toArray((T[])((Node.PropertySet[])setSets[count]));
            ++count;
        }
        return new TabInfo(titles, setSets);
    }

    @Override
    public void firePropertyChange(String propertyName, boolean oldValue, boolean newValue) {
        super.firePropertyChange(propertyName, oldValue, newValue);
        if ("MACOSX".equals(propertyName)) {
            this.table.focusLostCancel();
        }
    }

    static {
        neverTabs = Boolean.getBoolean("netbeans.ps.nevertabs");
        forceTabs = Boolean.getBoolean("nb.ps.forcetabs");
        RP = new RequestProcessor("Property Sheet");
    }

    private static final class TabInfo {
        public String[] titles;
        public Object[] sets;

        public TabInfo(String[] titles, Object[] sets) {
            this.titles = titles;
            this.sets = sets;
        }

        public Node.PropertySet[] getSets(int i) {
            return (Node.PropertySet[])this.sets[i];
        }
    }

    private final class SheetPCListener
    extends NodeAdapter {
        private PropertyChangeListener inner;
        private Node currNode;

        public SheetPCListener() {
            this.inner = new PCL();
        }

        public void attach(Node n) {
            if (this.currNode != n) {
                if (this.currNode != null) {
                    this.detach();
                }
                if (n != null) {
                    n.addPropertyChangeListener(this.inner);
                    n.addNodeListener((NodeListener)this);
                    if (PropUtils.isLoggable(PropertySheet.class)) {
                        PropUtils.log(PropertySheet.class, "Now listening for changes on " + (Object)n);
                    }
                }
                this.currNode = n;
            }
        }

        public Node getNode() {
            return this.currNode;
        }

        public Node detach() {
            Node n = this.currNode;
            if (n != null) {
                if (PropUtils.isLoggable(PropertySheet.class)) {
                    PropUtils.log(PropertySheet.class, "Detaching listeners from " + (Object)n);
                }
                n.removePropertyChangeListener(this.inner);
                n.removeNodeListener((NodeListener)this);
                this.currNode = null;
            }
            return n;
        }

        public void propertyChange(final PropertyChangeEvent evt) {
            final String nm = evt.getPropertyName();
            if ("propertySets".equals(nm)) {
                final Node n = (Node)evt.getSource();
                Mutex.EVENT.readAccess(new Runnable(){

                    @Override
                    public void run() {
                        if (SheetPCListener.this.currNode == n) {
                            PropertySheet.this.setCurrentNode(n);
                        }
                    }
                });
            } else {
                if ("cookie".equals(nm) || "icon".equals(nm) || "parentNode".equals(nm) || "openedIcon".equals(nm) || "leaf".equals(nm)) {
                    return;
                }
                Runnable runnable = new Runnable(){

                    @Override
                    public void run() {
                        if (PropertySheet.this.isDescriptionVisible() && ("displayName".equals(nm) || "shortDescription".equals(nm))) {
                            Node n = (Node)evt.getSource();
                            if (SheetPCListener.this.currNode == n) {
                                String description;
                                PropertySheet.this.psheet.setDescription(n.getDisplayName(), (description = (String)n.getValue("nodeDescription")) == null ? n.getShortDescription() : description);
                                PropertySheet.this.table.setBeanName(n.getDisplayName());
                            }
                        }
                    }
                };
                if (EventQueue.isDispatchThread()) {
                    runnable.run();
                } else {
                    EventQueue.invokeLater(runnable);
                }
            }
        }

        public void nodeDestroyed(NodeEvent ev) {
            if (ev.getNode() == this.currNode) {
                this.detach();
                Mutex.EVENT.readAccess(new Runnable(){

                    @Override
                    public void run() {
                        if (SheetPCListener.this.currNode == null) {
                            PropertySheet.this.doSetNodes(null);
                        }
                    }
                });
            }
        }

        private final class PCL
        implements PropertyChangeListener {
            private PCL() {
            }

            @Override
            public void propertyChange(final PropertyChangeEvent evt) {
                EventQueue.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        String nm = evt.getPropertyName();
                        if (nm == null) {
                            if (SheetPCListener.this.currNode != null) {
                                PropertySheet.this.setCurrentNode(SheetPCListener.this.currNode);
                            }
                        } else {
                            PropertySheet.this.table.repaintProperty(nm);
                        }
                    }
                });
            }

        }

    }

    private static class MutableAction
    extends AbstractAction {
        private static final int SORT_NAMES = 0;
        private static final int UNSORT = 1;
        private static final int INVOKE_POPUP = 2;
        private static final int SHOW_DESCRIPTION = 3;
        private static final int SHOW_HELP = 4;
        private static final int RESTORE_DEFAULT = 5;
        private final int id;
        private final PropertySheet sheet;

        public MutableAction(int id, PropertySheet sheet) {
            this.id = id;
            this.sheet = sheet;
            String nameKey = null;
            switch (id) {
                case 0: {
                    nameKey = "CTL_AlphaSort";
                    break;
                }
                case 1: {
                    nameKey = "CTL_NoSort";
                    break;
                }
                case 2: {
                    break;
                }
                case 3: {
                    nameKey = "CTL_ShowDescription";
                    break;
                }
                case 4: {
                    break;
                }
                case 5: {
                    nameKey = "CTL_RestoreDefaultValue";
                    break;
                }
                default: {
                    throw new IllegalArgumentException(Integer.toString(id));
                }
            }
            if (nameKey != null) {
                this.putValue("Name", NbBundle.getMessage(PropertySheet.class, (String)nameKey));
            }
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            switch (this.id) {
                case 0: {
                    try {
                        this.sheet.setSortingMode(1);
                    }
                    catch (PropertyVetoException pve) {}
                    break;
                }
                case 1: {
                    try {
                        this.sheet.setSortingMode(0);
                    }
                    catch (PropertyVetoException pve) {}
                    break;
                }
                case 2: {
                    this.sheet.showPopup(new Point(0, 0));
                    break;
                }
                case 3: {
                    this.sheet.setDescriptionVisible(!this.sheet.isDescriptionVisible());
                    break;
                }
                case 4: {
                    break;
                }
                case 5: {
                    try {
                        if (null == this.sheet || null == this.sheet.table || null == this.sheet.table.getSelection()) break;
                        ((Node.Property)this.sheet.table.getSelection()).restoreDefaultValue();
                        break;
                    }
                    catch (IllegalAccessException iae) {
                        throw (IllegalStateException)new IllegalStateException("Error restoring default value").initCause(iae);
                    }
                    catch (InvocationTargetException ite) {
                        throw (IllegalStateException)new IllegalStateException("Error restoring defaul value").initCause(ite);
                    }
                }
                default: {
                    throw new IllegalArgumentException(Integer.toString(this.id));
                }
            }
        }

        @Override
        public boolean isEnabled() {
            if (this.id == 2 && Boolean.TRUE.equals(this.sheet.getClientProperty("disablePopup"))) {
                return false;
            }
            return super.isEnabled();
        }
    }

    final class HelpAction
    extends AbstractAction {
        HelpCtx.Provider provider;
        private boolean wasEnabled;

        public HelpAction() {
            super(NbBundle.getMessage(PropertySheet.class, (String)"CTL_Help"));
            this.provider = null;
            this.wasEnabled = false;
            this.checkContext();
        }

        public void checkContext() {
            boolean isEnabled;
            HelpCtx ctx = this.getContext();
            boolean bl = isEnabled = ctx != null;
            if (isEnabled != this.wasEnabled) {
                this.firePropertyChange("enabled", isEnabled ? Boolean.FALSE : Boolean.TRUE, isEnabled ? Boolean.TRUE : Boolean.FALSE);
            }
            this.wasEnabled = isEnabled;
            PropertySheet.this.psheet.setHelpEnabled(isEnabled);
        }

        @Override
        public boolean isEnabled() {
            return this.getContext() != null;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            HelpCtx ctx = this.getContext();
            if (ctx == null || !ctx.display()) {
                Toolkit.getDefaultToolkit().beep();
            }
        }

        /*
         * Unable to fully structure code
         * Enabled aggressive block sorting
         * Lifted jumps to return sites
         */
        public HelpCtx getContext() {
            fd = PropertySheet.this.table.getSelection();
            id = null;
            if (fd == null) ** GOTO lbl7
            if (fd instanceof Node.Property) {
                id = (String)fd.getValue("helpID");
            }
            if (id != null) ** GOTO lbl27
lbl7: // 2 sources:
            if ((PropertySheet.this.psheet.getState() & 2) == 0) ** GOTO lbl12
            ps = (Node.PropertySet[])PropertySheet.this.psheet.getTabbedContainerSelection();
            if (ps == null || ps.length <= 0) ** GOTO lbl22
            id = (String)ps[0].getValue("helpID");
            ** GOTO lbl22
lbl12: // 1 sources:
            if (id == null && PropertySheet.this.pclistener != null) {
                n = PropertySheet.this.pclistener.getNode();
                if (n == null) {
                    return null;
                }
                ps = n.getPropertySets();
                if (fd != null && ps != null && ps.length > 0) {
                    for (i = 0; i < ps.length; ++i) {
                        if (ps[i] != fd && !Arrays.asList(ps[i].getProperties()).contains(fd)) continue;
                        id = (String)ps[i].getValue("helpID");
                        break;
                    }
                }
            }
lbl22: // 8 sources:
            if (id == null && PropertySheet.this.pclistener != null && (nodes = PropertySheet.this.getCurrentNodes()) != null && nodes.length > 0) {
                for (i = 0; i < nodes.length && (id = (String)nodes[i].getValue("propertiesHelpID")) == null; ++i) {
                    ctx = nodes[i].getHelpCtx();
                    if (ctx == null || ctx == HelpCtx.DEFAULT_HELP) continue;
                    return ctx;
                }
            }
lbl27: // 4 sources:
            if (id == null) return null;
            if (HelpCtx.DEFAULT_HELP.getHelpID().equals(id) != false) return null;
            return new HelpCtx(id);
        }
    }

    private class TabSelectionListener
    implements ChangeListener,
    FocusListener {
        private TabSelectionListener() {
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            PropertySheet.this.helpAction.checkContext();
            if (e.getSource() instanceof SheetTable) {
                SheetTable tbl = (SheetTable)e.getSource();
                FeatureDescriptor fd = tbl.getSelection();
                Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getPermanentFocusOwner();
                if (focusOwner != tbl && !tbl.isKnownComponent(focusOwner) && !PropertySheet.this.isAncestorOf(focusOwner)) {
                    fd = null;
                }
                if (fd != null) {
                    String ttl = fd.getDisplayName();
                    String desc = fd.getShortDescription();
                    PropertySheet.this.psheet.setDescription(ttl, desc);
                    if (fd instanceof Node.Property) {
                        Node.Property prop = (Node.Property)fd;
                        String name = prop.getName();
                        PropertySheet.this.psheet.setPropId("Property ID: " + name);
                        PropertySheet.this.setSelectedProperty(tbl.getSelection());
                    } else {
                        PropertySheet.this.psheet.setPropId(null);
                        PropertySheet.this.setSelectedProperty(null);
                    }
                } else {
                    Node n = PropertySheet.this.pclistener.getNode();
                    if (n != null) {
                        String ttl = n.getDisplayName();
                        String desc = (String)n.getValue("nodeDescription");
                        if (desc == null) {
                            desc = n.getShortDescription();
                        }
                    } else {
                        PropertySheet.this.psheet.setDescription(null, null);
                    }
                }
            } else {
                Node.PropertySet[] sets;
                if (!PropertySheet.this.psheet.isAdjusting()) {
                    PropertySheet.this.psheet.storeScrollAndTabInfo();
                }
                if ((sets = (Node.PropertySet[])PropertySheet.this.psheet.getTabbedContainerSelection()) != null) {
                    PropertySheet.this.table.getPropertySetModel().setPropertySets(sets);
                    if (sets.length > 0 && !PropertySheet.this.psheet.isAdjusting()) {
                        String tab = (String)sets[0].getValue("tabName");
                        tab = tab == null ? PropUtils.basicPropsTabName() : tab;
                        PropertySheet.this.psheet.manager().storeLastSelectedGroup(tab);
                        PropertySheet.this.psheet.adjustForName(tab);
                    }
                }
            }
        }

        @Override
        public void focusGained(FocusEvent e) {
            ChangeEvent ce = new ChangeEvent(PropertySheet.this.table);
            this.stateChanged(ce);
        }

        @Override
        public void focusLost(FocusEvent e) {
            this.focusGained(e);
        }
    }

}

