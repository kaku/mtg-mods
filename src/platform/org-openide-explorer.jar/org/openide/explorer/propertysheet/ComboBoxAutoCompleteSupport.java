/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.propertysheet;

import java.awt.Component;
import java.awt.Rectangle;
import javax.accessibility.Accessible;
import javax.swing.ComboBoxEditor;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.basic.ComboPopup;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;

final class ComboBoxAutoCompleteSupport {
    ComboBoxAutoCompleteSupport() {
    }

    public static boolean install(JComboBox combo) {
        boolean res = false;
        ComboBoxEditor comboEditor = combo.getEditor();
        if (comboEditor.getEditorComponent() instanceof JTextComponent) {
            JTextComponent textEditor = (JTextComponent)comboEditor.getEditorComponent();
            Document doc = textEditor.getDocument();
            doc.addDocumentListener(new AutoCompleteListener(combo));
            ComboBoxAutoCompleteSupport.setIgnoreSelectionEvents(combo, false);
            combo.setEditable(true);
            res = true;
        }
        combo.putClientProperty("nb.combo.autocomplete", res);
        return res;
    }

    static boolean isIgnoreSelectionEvents(JComboBox combo) {
        Object res = combo.getClientProperty("nb.combo.autocomplete.ignoreselection");
        return res instanceof Boolean && (Boolean)res != false;
    }

    static void setIgnoreSelectionEvents(JComboBox combo, boolean ignore) {
        combo.putClientProperty("nb.combo.autocomplete.ignoreselection", ignore);
    }

    static boolean isAutoCompleteInstalled(JComboBox combo) {
        Object res = combo.getClientProperty("nb.combo.autocomplete");
        return res instanceof Boolean && (Boolean)res != false;
    }

    static int findMatch(JComboBox combo, String editorText) {
        String item;
        int i;
        if (null == editorText || editorText.isEmpty()) {
            return -1;
        }
        for (i = 0; i < combo.getItemCount(); ++i) {
            item = combo.getItemAt(i).toString();
            if (item.toLowerCase().compareTo(editorText) != 0) continue;
            return i;
        }
        for (i = 0; i < combo.getItemCount(); ++i) {
            item = combo.getItemAt(i).toString();
            if (!item.toLowerCase().startsWith(editorText)) continue;
            return i;
        }
        return -1;
    }

    private static JList getPopupList(JComboBox combo) {
        Accessible a = combo.getUI().getAccessibleChild(combo, 0);
        if (a instanceof ComboPopup) {
            return ((ComboPopup)((Object)a)).getList();
        }
        return null;
    }

    private static class AutoCompleteListener
    implements DocumentListener {
        private final JComboBox combo;

        public AutoCompleteListener(JComboBox combo) {
            this.combo = combo;
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            this.matchSelection(e);
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            this.matchSelection(e);
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            this.matchSelection(e);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void matchSelection(DocumentEvent e) {
            block16 : {
                if (ComboBoxAutoCompleteSupport.isIgnoreSelectionEvents(this.combo)) {
                    return;
                }
                try {
                    String editorText;
                    JList list;
                    ComboBoxAutoCompleteSupport.setIgnoreSelectionEvents(this.combo, true);
                    if (!this.combo.isDisplayable()) {
                        return;
                    }
                    try {
                        editorText = e.getDocument().getText(0, e.getDocument().getLength());
                    }
                    catch (BadLocationException ex) {
                        ComboBoxAutoCompleteSupport.setIgnoreSelectionEvents(this.combo, false);
                        return;
                    }
                    if (null != this.combo.getSelectedItem() && this.combo.getSelectedItem().toString().equals(editorText)) {
                        return;
                    }
                    if (!this.combo.isPopupVisible()) {
                        this.combo.showPopup();
                    }
                    if (null == (list = ComboBoxAutoCompleteSupport.getPopupList(this.combo))) {
                        return;
                    }
                    int matchIndex = ComboBoxAutoCompleteSupport.findMatch(this.combo, editorText);
                    if (matchIndex >= 0) {
                        list.setSelectedIndex(matchIndex);
                        Rectangle rect = list.getCellBounds(matchIndex, matchIndex);
                        if (null != rect) {
                            list.scrollRectToVisible(rect);
                        }
                        break block16;
                    }
                    list.clearSelection();
                    list.scrollRectToVisible(new Rectangle(1, 1));
                }
                finally {
                    ComboBoxAutoCompleteSupport.setIgnoreSelectionEvents(this.combo, false);
                }
            }
        }
    }

}

