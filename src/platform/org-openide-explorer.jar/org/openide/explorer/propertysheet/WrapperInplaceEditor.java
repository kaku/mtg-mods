/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.propertysheet;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputEvent;
import java.beans.PropertyEditor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.ComboBoxEditor;
import javax.swing.InputMap;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.event.EventListenerList;
import javax.swing.text.JTextComponent;
import org.openide.explorer.propertysheet.ComboInplaceEditor;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;
import org.openide.explorer.propertysheet.editors.EnhancedPropertyEditor;

class WrapperInplaceEditor
extends JPanel
implements InplaceEditor,
ActionListener,
FocusListener {
    private EnhancedPropertyEditor enh;
    private PropertyModel mdl;
    private Component legacy = null;
    private transient List<ActionListener> actionListenerList;
    private boolean listenerAdded = false;
    private boolean suspendEvents = false;
    private EventListenerList listenerList = null;

    WrapperInplaceEditor(EnhancedPropertyEditor enh) {
        this.enh = enh;
        this.setLayout(new BorderLayout());
        this.getInputMap(1).put(KeyStroke.getKeyStroke(10, 0), "enter");
        this.getActionMap().put("enter", new EnterKbdAction());
    }

    @Override
    public void focusGained(FocusEvent e) {
        e.setSource(this);
        this.fireFocusGained(e);
    }

    @Override
    public void focusLost(FocusEvent e) {
        e.setSource(this);
        this.fireFocusLost(e);
    }

    @Override
    public void clear() {
        if (this.legacy != null) {
            this.removeAll();
            if (this.listenerAdded) {
                this.tryRemoveActionListener(this.legacy);
            }
            this.legacy.removeFocusListener(this);
            this.legacy = null;
        }
        this.enh = null;
        this.listenerAdded = false;
    }

    private boolean tryAddActionListener(Component comp) {
        try {
            Method m = comp.getClass().getMethod("addActionListener", ActionListener.class);
            if (m != null) {
                m.invoke(comp, this);
                return true;
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        return false;
    }

    private boolean tryRemoveActionListener(Component comp) {
        try {
            Method m = comp.getClass().getMethod("removeActionListener", ActionListener.class);
            if (m != null) {
                m.invoke(comp, this);
                return true;
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        return false;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        this.fireAction(new ActionEvent(this, 1001, "success"));
    }

    @Override
    public void connect(PropertyEditor pe, PropertyEnv env) {
        if (this.legacy != null) {
            this.clear();
        }
        if (pe != this.enh) {
            this.enh = (EnhancedPropertyEditor)pe;
        }
        Component comp = this.getLegacyInplaceEditor();
        this.add(comp, "Center");
        this.listenerAdded = this.tryAddActionListener(comp);
        comp.addFocusListener(this);
    }

    @Override
    public JComponent getComponent() {
        return this;
    }

    @Override
    public KeyStroke[] getKeyStrokes() {
        if (this.getLegacyInplaceEditor() instanceof JComboBox) {
            return ComboInplaceEditor.cbKeyStrokes;
        }
        return null;
    }

    @Override
    public PropertyEditor getPropertyEditor() {
        return this.enh;
    }

    @Override
    public PropertyModel getPropertyModel() {
        return this.mdl;
    }

    @Override
    public Object getValue() {
        return this.enh.getValue();
    }

    public void handleInitialInputEvent(InputEvent e) {
    }

    @Override
    public boolean isKnownComponent(Component c) {
        return this.isAncestorOf(c);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void reset() {
        block8 : {
            this.suspendEvents = true;
            try {
                if (this.legacy instanceof JTextComponent) {
                    ((JTextComponent)this.legacy).setText(this.enh.getAsText());
                    break block8;
                }
                if (!(this.legacy instanceof JComboBox)) break block8;
                if (((JComboBox)this.legacy).isEditable()) {
                    if (((JComboBox)this.legacy).getEditor().getEditorComponent().isShowing()) {
                        ((JComboBox)this.legacy).getEditor().setItem(this.enh.getValue());
                    }
                    break block8;
                }
                ((JComboBox)this.legacy).setSelectedItem(this.enh.getValue());
            }
            catch (Exception e) {
                Logger.getLogger(WrapperInplaceEditor.class.getName()).log(Level.WARNING, "Failure resetting legacy editor", e);
            }
            finally {
                this.suspendEvents = false;
            }
        }
    }

    @Override
    public void setPropertyModel(PropertyModel pm) {
        this.mdl = pm;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setValue(Object o) {
        block8 : {
            this.suspendEvents = true;
            try {
                if (this.legacy instanceof JTextComponent) {
                    ((JTextComponent)this.legacy).setText(o.toString());
                    break block8;
                }
                if (!(this.legacy instanceof JComboBox)) break block8;
                if (((JComboBox)this.legacy).isEditable()) {
                    if (((JComboBox)this.legacy).getEditor().getEditorComponent().isShowing()) {
                        ((JComboBox)this.legacy).getEditor().setItem(o.toString());
                    }
                    break block8;
                }
                ((JComboBox)this.legacy).setSelectedItem(o);
            }
            catch (Exception e) {
                Logger.getLogger(WrapperInplaceEditor.class.getName()).log(Level.WARNING, "Failure resetting legacy editor", e);
            }
            finally {
                this.suspendEvents = false;
            }
        }
    }

    @Override
    public boolean supportsTextEntry() {
        if (this.legacy instanceof JTextComponent) {
            return true;
        }
        if (this.legacy instanceof JComboBox && ((JComboBox)this.legacy).isEditable()) {
            return true;
        }
        return false;
    }

    private Component getLegacyInplaceEditor() {
        if (this.legacy == null) {
            this.legacy = this.enh.getInPlaceCustomEditor();
        }
        return this.legacy;
    }

    @Override
    public synchronized void addActionListener(ActionListener listener) {
        if (this.actionListenerList == null) {
            this.actionListenerList = new ArrayList<ActionListener>();
        }
        this.actionListenerList.add(listener);
    }

    @Override
    public synchronized void removeActionListener(ActionListener listener) {
        if (this.actionListenerList != null) {
            this.actionListenerList.remove(listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void fireAction(ActionEvent event) {
        List list;
        if (this.suspendEvents) {
            return;
        }
        WrapperInplaceEditor wrapperInplaceEditor = this;
        synchronized (wrapperInplaceEditor) {
            if (this.actionListenerList == null) {
                return;
            }
            list = (List)((ArrayList)this.actionListenerList).clone();
        }
        for (int i = 0; i < list.size(); ++i) {
            ((ActionListener)list.get(i)).actionPerformed(event);
        }
    }

    @Override
    public synchronized void addFocusListener(FocusListener listener) {
        if (this.listenerList == null) {
            this.listenerList = new EventListenerList();
        }
        this.listenerList.add(FocusListener.class, listener);
        super.addFocusListener(listener);
    }

    @Override
    public synchronized void removeFocusListener(FocusListener listener) {
        this.listenerList.remove(FocusListener.class, listener);
        super.removeFocusListener(listener);
    }

    private void fireFocusGained(FocusEvent event) {
        if (this.listenerList == null) {
            return;
        }
        Object[] listeners = this.listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] != FocusListener.class) continue;
            ((FocusListener)listeners[i + 1]).focusGained(event);
        }
    }

    private void fireFocusLost(FocusEvent event) {
        if (this.listenerList == null) {
            return;
        }
        Object[] listeners = this.listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] != FocusListener.class) continue;
            ((FocusListener)listeners[i + 1]).focusLost(event);
        }
    }

    private class EnterKbdAction
    extends AbstractAction {
        private EnterKbdAction() {
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            WrapperInplaceEditor.this.fireAction(new ActionEvent(WrapperInplaceEditor.this, 1001, "success"));
        }
    }

}

