/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.awt.Mnemonics
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 */
package org.openide.explorer.propertysheet;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.UIManager;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.awt.Mnemonics;
import org.openide.explorer.propertysheet.ExPropertyModel;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;
import org.openide.explorer.propertysheet.editors.EnhancedCustomPropertyEditor;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

final class PropertyDialogManager
implements VetoableChangeListener,
ActionListener {
    private static final String PROPERTY_DESCRIPTION = "description";
    private static Throwable doNotNotify;
    private PropertyChangeListener editorListener;
    private PropertyChangeListener componentListener;
    private Object oldValue;
    private PropertyEditor editor;
    private PropertyModel model;
    private Node.Property prop;
    private boolean changed = false;
    private Component component;
    private Window dialog;
    private JButton okButton;
    private Runnable errorPerformer;
    private boolean okButtonState = true;
    private Object envStateBeforeDisplay = null;
    private PropertyEnv env;
    private Object lastValueFromEditor;
    private boolean cancelled = false;
    private boolean ok = false;
    private boolean reset = false;

    public PropertyDialogManager(String title, boolean isModal, PropertyEditor editor, PropertyModel model, PropertyEnv env) {
        Object compTitle;
        Object helpID;
        this.editor = editor;
        if (env != null) {
            env.addVetoableChangeListener(this);
        }
        this.component = editor.getCustomEditor();
        if (this.component == null) {
            throw new NullPointerException("Cannot create a dialog for a null component. Offending property editor class:" + editor.getClass().getName());
        }
        this.model = model;
        this.env = env;
        HelpCtx helpCtx = null;
        if (env != null && (helpID = env.getFeatureDescriptor().getValue("helpID")) != null && helpID instanceof String && this.component != null && this.component instanceof JComponent) {
            HelpCtx.setHelpIDString((JComponent)((JComponent)this.component), (String)((String)helpID));
            helpCtx = new HelpCtx((String)helpID);
        }
        if (this.component instanceof JComponent && (compTitle = ((JComponent)this.component).getClientProperty("title")) instanceof String) {
            title = (String)compTitle;
        }
        this.createDialog(isModal, title, helpCtx);
        this.initializeListeners();
    }

    public Window getDialog() {
        return this.dialog;
    }

    @Override
    public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
        if (this.env != null && "state".equals(evt.getPropertyName())) {
            boolean bl = this.okButtonState = evt.getNewValue() != PropertyEnv.STATE_INVALID;
            if (this.okButton != null) {
                this.okButton.setEnabled(this.okButtonState);
            }
        }
    }

    private void createDialog(boolean isModal, String title, HelpCtx helpCtx) {
        Object[] options;
        JButton defaultOption;
        FeatureDescriptor fd;
        if (this.component instanceof Window) {
            this.dialog = (Window)this.component;
            this.dialog.pack();
            return;
        }
        boolean cannotWrite = false;
        boolean defaultValue = false;
        if (this.model instanceof ExPropertyModel && (fd = ((ExPropertyModel)this.model).getFeatureDescriptor()) instanceof Node.Property) {
            this.prop = (Node.Property)fd;
            cannotWrite = !this.prop.canWrite();
            defaultValue = PropUtils.shallBeRDVEnabled(this.prop);
        }
        if (this.editor == null || cannotWrite) {
            JButton closeButton = new JButton(PropertyDialogManager.getString("CTL_Close"));
            closeButton.getAccessibleContext().setAccessibleDescription(PropertyDialogManager.getString("ACSD_CTL_Close"));
            options = new Object[]{closeButton};
            defaultOption = closeButton;
        } else {
            this.okButton = new JButton(PropertyDialogManager.getString("CTL_OK"));
            this.okButton.getAccessibleContext().setAccessibleDescription(PropertyDialogManager.getString("ACSD_CTL_OK"));
            JButton cancelButton = new JButton(PropertyDialogManager.getString("CTL_Cancel"));
            cancelButton.setVerifyInputWhenFocusTarget(false);
            cancelButton.getAccessibleContext().setAccessibleDescription(PropertyDialogManager.getString("ACSD_CTL_Cancel"));
            cancelButton.setDefaultCapable(false);
            if (defaultValue) {
                JButton defaultButton = new JButton();
                String name = PropertyDialogManager.getString("CTL_Default");
                Mnemonics.setLocalizedText((AbstractButton)defaultButton, (String)name);
                defaultButton.setActionCommand(name);
                defaultButton.getAccessibleContext().setAccessibleDescription(PropertyDialogManager.getString("ACSD_CTL_Default"));
                defaultButton.setDefaultCapable(false);
                defaultButton.setVerifyInputWhenFocusTarget(false);
                options = "Aqua".equals(UIManager.getLookAndFeel().getID()) ? new Object[]{defaultButton, cancelButton, this.okButton} : new Object[]{this.okButton, defaultButton, cancelButton};
            } else {
                options = "Aqua".equals(UIManager.getLookAndFeel().getID()) ? new Object[]{cancelButton, this.okButton} : new Object[]{this.okButton, cancelButton};
            }
            defaultOption = this.okButton;
        }
        if (this.env != null && this.okButton != null) {
            boolean bl = this.okButtonState = this.env.getState() != PropertyEnv.STATE_INVALID;
            if (this.okButton != null) {
                this.okButton.setEnabled(this.okButtonState);
            }
        }
        if (this.env != null) {
            this.envStateBeforeDisplay = this.env.getState();
        }
        DialogDescriptor descriptor = new DialogDescriptor((Object)this.component, title, isModal, options, (Object)defaultOption, 0, helpCtx, (ActionListener)this);
        this.dialog = DialogDisplayer.getDefault().createDialog(descriptor);
    }

    private void initializeListeners() {
        this.dialog.addWindowListener(new WindowAdapter(){

            @Override
            public void windowClosing(WindowEvent e) {
                if (PropertyDialogManager.this.editor != null && !(PropertyDialogManager.this.component instanceof Window)) {
                    PropertyDialogManager.this.cancelled = true;
                    PropertyDialogManager.this.cancelValue();
                }
                if (PropertyDialogManager.this.env != null) {
                    PropertyDialogManager.this.env.removeVetoableChangeListener(PropertyDialogManager.this);
                }
                PropertyDialogManager.this.dialog.dispose();
            }

            @Override
            public void windowClosed(WindowEvent e) {
                if (PropertyDialogManager.this.component instanceof Window) {
                    if (PropertyDialogManager.this.component instanceof EnhancedCustomPropertyEditor) {
                        try {
                            Object newValue = ((EnhancedCustomPropertyEditor)((Object)PropertyDialogManager.this.component)).getPropertyValue();
                            PropertyDialogManager.this.model.setValue(newValue);
                        }
                        catch (InvocationTargetException ite) {
                            PropertyDialogManager.notifyUser(ite);
                        }
                        catch (IllegalStateException ise) {
                            PropertyDialogManager.notifyUser(ise);
                        }
                    } else if (PropertyDialogManager.this.env != null && !PropertyDialogManager.this.env.isChangeImmediate()) {
                        try {
                            PropertyDialogManager.this.model.setValue(PropertyDialogManager.this.lastValueFromEditor);
                        }
                        catch (InvocationTargetException ite) {
                            PropertyDialogManager.notifyUser(ite);
                        }
                        catch (IllegalStateException ise) {
                            PropertyDialogManager.notifyUser(ise);
                        }
                    }
                }
                if (PropertyDialogManager.this.editorListener != null) {
                    PropertyDialogManager.this.editor.removePropertyChangeListener(PropertyDialogManager.this.editorListener);
                }
                if (PropertyDialogManager.this.componentListener != null) {
                    PropertyDialogManager.this.component.removePropertyChangeListener(PropertyDialogManager.this.componentListener);
                }
                PropertyDialogManager.this.dialog.removeWindowListener(this);
            }
        });
        if (this.editor != null) {
            try {
                this.oldValue = this.model.getValue();
            }
            catch (Exception e) {
                // empty catch block
            }
            this.lastValueFromEditor = this.editor.getValue();
            this.editorListener = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent e) {
                    PropertyDialogManager.this.changed = true;
                    PropertyDialogManager.this.lastValueFromEditor = PropertyDialogManager.this.editor.getValue();
                    if ("propertyValueValid".equals(e.getPropertyName()) && PropertyDialogManager.this.okButton != null && e.getNewValue() instanceof Boolean) {
                        Boolean newButtonState = (Boolean)e.getNewValue();
                        PropertyDialogManager.this.okButtonState = newButtonState;
                        if (PropertyDialogManager.this.env != null) {
                            PropertyDialogManager.this.env.setState(PropertyDialogManager.this.okButtonState ? PropertyEnv.STATE_VALID : PropertyEnv.STATE_INVALID);
                        } else {
                            PropertyDialogManager.this.okButton.setEnabled(PropertyDialogManager.this.okButtonState);
                        }
                        if (e.getOldValue() instanceof Runnable) {
                            PropertyDialogManager.this.errorPerformer = (Runnable)e.getOldValue();
                        } else {
                            PropertyDialogManager.this.errorPerformer = null;
                        }
                    }
                }
            };
            this.editor.addPropertyChangeListener(this.editorListener);
            if (this.component == null) {
                throw new NullPointerException(this.editor.getClass().getName() + " returned null from getCustomEditor()");
            }
            this.componentListener = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent e) {
                    if ("helpCtx".equals(e.getPropertyName()) && PropertyDialogManager.this.dialog instanceof PropertyChangeListener) {
                        ((PropertyChangeListener)((Object)PropertyDialogManager.this.dialog)).propertyChange(e);
                    }
                }
            };
            this.component.addPropertyChangeListener(this.componentListener);
        }
    }

    private void cancelValue() {
        try {
            if (this.env != null && this.env.isEditable()) {
                this.editor.setValue(this.oldValue);
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        if (!this.changed || this.component instanceof EnhancedCustomPropertyEditor || this.env != null && !this.env.isChangeImmediate()) {
            if (this.env != null && this.envStateBeforeDisplay != null) {
                this.env.setState(this.envStateBeforeDisplay);
            }
            return;
        }
        try {
            this.model.setValue(this.oldValue);
        }
        catch (Exception e) {
            // empty catch block
        }
    }

    public boolean wasCancelled() {
        return this.cancelled;
    }

    public boolean wasOK() {
        return this.ok;
    }

    public boolean wasReset() {
        return this.reset;
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        String label = evt.getActionCommand();
        if (label.equals(PropertyDialogManager.getString("CTL_Cancel"))) {
            this.cancelled = true;
            this.cancelValue();
        }
        this.ok = false;
        this.reset = false;
        if (label.equals(PropertyDialogManager.getString("CTL_Default"))) {
            this.reset = true;
            if (this.prop != null) {
                try {
                    this.prop.restoreDefaultValue();
                }
                catch (IllegalAccessException iae) {
                    PropertyDialogManager.notifyUser(iae);
                }
                catch (InvocationTargetException ite) {
                    PropertyDialogManager.notifyUser(ite);
                }
            }
        }
        if (label.equals(PropertyDialogManager.getString("CTL_OK"))) {
            this.ok = true;
            if (this.env != null && this.env.getState() == PropertyEnv.STATE_NEEDS_VALIDATION) {
                this.env.setState(PropertyEnv.STATE_VALID);
                if (this.env.getState() != PropertyEnv.STATE_VALID) {
                    return;
                }
            }
            if (this.component instanceof EnhancedCustomPropertyEditor) {
                try {
                    Object newValue = ((EnhancedCustomPropertyEditor)((Object)this.component)).getPropertyValue();
                    this.model.setValue(newValue);
                }
                catch (InvocationTargetException ite) {
                    PropertyDialogManager.notifyUser(ite);
                    return;
                }
                catch (IllegalStateException ise) {
                    PropertyDialogManager.notifyUser(ise);
                    return;
                }
                catch (IllegalArgumentException iae) {
                    PropertyDialogManager.notifyUser(iae);
                    return;
                }
            }
            if (this.env != null && !this.env.isChangeImmediate()) {
                try {
                    this.model.setValue(this.lastValueFromEditor);
                }
                catch (InvocationTargetException ite) {
                    PropertyDialogManager.notifyUser(ite);
                    return;
                }
                catch (IllegalStateException ise) {
                    PropertyDialogManager.notifyUser(ise);
                    return;
                }
            }
            if (!this.okButtonState) {
                if (this.errorPerformer != null) {
                    this.errorPerformer.run();
                }
                return;
            }
        }
        this.changed = false;
        if (this.env != null) {
            this.env.removeVetoableChangeListener(this);
        }
        this.dialog.dispose();
    }

    private static String getString(String key) {
        return NbBundle.getMessage(PropertyDialogManager.class, (String)key);
    }

    static void doNotNotify(Throwable ex) {
        doNotNotify = ex;
    }

    static void notify(Throwable ex) {
        Throwable d = doNotNotify;
        doNotNotify = null;
        if (d == ex) {
            return;
        }
        if (ex instanceof PropertyVetoException) {
            Exceptions.attachLocalizedMessage((Throwable)ex, (String)ex.getLocalizedMessage());
        }
        Exceptions.printStackTrace((Throwable)ex);
    }

    private static void notifyUser(Exception e) {
        String userMessage = Exceptions.findLocalizedMessage((Throwable)e);
        if (userMessage != null) {
            Exceptions.printStackTrace((Throwable)e);
        } else {
            Logger.getLogger(PropertyDialogManager.class.getName()).log(Level.INFO, null, e);
        }
    }

    Component getComponent() {
        return this.component;
    }

    PropertyEditor getEditor() {
        return this.editor;
    }

}

