/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.util.NbBundle
 */
package org.openide.explorer.propertysheet;

import java.beans.FeatureDescriptor;
import java.beans.PropertyEditor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertySetModel;
import org.openide.explorer.propertysheet.PropertySetModelEvent;
import org.openide.explorer.propertysheet.PropertySetModelListener;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

final class SheetTableModel
implements TableModel,
PropertySetModelListener {
    private transient List<TableModelListener> tableModelListenerList;
    PropertySetModel model = null;

    public SheetTableModel() {
    }

    public SheetTableModel(PropertySetModel psm) {
        this.setPropertySetModel(psm);
    }

    public void setPropertySetModel(PropertySetModel mod) {
        if (this.model == mod) {
            return;
        }
        if (this.model != null) {
            this.model.removePropertySetModelListener(this);
        }
        this.model = mod;
        if (this.model == null) {
            throw new IllegalArgumentException("Model cannot be null");
        }
        mod.addPropertySetModelListener(this);
        this.model = mod;
        this.fireTableChanged(new TableModelEvent(this));
    }

    public PropertySetModel getPropertySetModel() {
        return this.model;
    }

    public Class getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0: {
                return String.class;
            }
            case 1: {
                return Object.class;
            }
        }
        throw new IllegalArgumentException("Column " + columnIndex + " does not exist.");
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int columnIndex) {
        if (columnIndex == 0) {
            return NbBundle.getMessage(SheetTableModel.class, (String)"COLUMN_NAMES");
        }
        return NbBundle.getMessage(SheetTableModel.class, (String)"COLUMN_VALUES");
    }

    @Override
    public int getRowCount() {
        if (this.model == null) {
            return 0;
        }
        return this.model.getCount();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        FeatureDescriptor result = rowIndex == -1 ? null : this.model.getFeatureDescriptor(rowIndex);
        return result;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return false;
        }
        if (columnIndex == 1) {
            FeatureDescriptor fd = this.model.getFeatureDescriptor(rowIndex);
            if (fd instanceof Node.PropertySet) {
                return false;
            }
            return ((Node.Property)fd).canWrite();
        }
        throw new IllegalArgumentException("Illegal row/column: " + Integer.toString(rowIndex) + Integer.toString(columnIndex));
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        block5 : {
            if (columnIndex == 0) {
                throw new IllegalArgumentException("Cannot set property names.");
            }
            try {
                FeatureDescriptor fd = this.model.getFeatureDescriptor(rowIndex);
                if (fd instanceof Node.Property) {
                    Node.Property p = (Node.Property)fd;
                    p.setValue(aValue);
                    break block5;
                }
                throw new IllegalArgumentException("Index " + Integer.toString(rowIndex) + Integer.toString(columnIndex) + " does not represent a property. ");
            }
            catch (IllegalAccessException iae) {
                Logger.getLogger(SheetTableModel.class.getName()).log(Level.WARNING, null, iae);
            }
            catch (InvocationTargetException ite) {
                Logger.getLogger(SheetTableModel.class.getName()).log(Level.WARNING, null, ite);
            }
        }
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Lifted jumps to return sites
     */
    public String getDescriptionFor(int row, int column) {
        if (row == -1) return "";
        if (column == -1) {
            return "";
        }
        fd = this.model.getFeatureDescriptor(row);
        p = fd instanceof Node.Property != false ? (Node.Property)fd : null;
        result = null;
        if (p != null) {
            try {
                if (column == 0) {
                    result = p.getShortDescription();
                }
                ped = PropUtils.getPropertyEditor(p);
                if (ped != null) {
                    result = ped.getAsText();
                }
                if (p.getValueType() != String.class) ** GOTO lbl26
                s = (String)p.getValue();
                if (s == null) return s;
                if (s.length() <= 2048) return s;
                return "";
            }
            catch (Exception e) {
                result = column == 0 ? p.getShortDescription() : e.toString();
            }
        } else {
            ps = (Node.PropertySet)fd;
            result = ps.getShortDescription();
        }
lbl26: // 5 sources:
        if (result != null) return result;
        return "";
    }

    @Override
    public synchronized void addTableModelListener(TableModelListener listener) {
        if (this.tableModelListenerList == null) {
            this.tableModelListenerList = new ArrayList<TableModelListener>();
        }
        this.tableModelListenerList.add(listener);
    }

    @Override
    public synchronized void removeTableModelListener(TableModelListener listener) {
        if (this.tableModelListenerList != null) {
            this.tableModelListenerList.remove(listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void fireTableChanged(TableModelEvent event) {
        List list;
        SheetTableModel sheetTableModel = this;
        synchronized (sheetTableModel) {
            if (this.tableModelListenerList == null) {
                return;
            }
            list = (List)((ArrayList)this.tableModelListenerList).clone();
        }
        for (int i = 0; i < list.size(); ++i) {
            ((TableModelListener)list.get(i)).tableChanged(event);
        }
    }

    @Override
    public void boundedChange(PropertySetModelEvent e) {
        TableModelEvent tme = new TableModelEvent(this, e.start, e.end, -1, e.type == 0 ? 1 : -1);
        this.fireTableChanged(tme);
    }

    @Override
    public void wholesaleChange(PropertySetModelEvent e) {
        this.fireTableChanged(new TableModelEvent(this));
    }

    @Override
    public void pendingChange(PropertySetModelEvent e) {
    }
}

