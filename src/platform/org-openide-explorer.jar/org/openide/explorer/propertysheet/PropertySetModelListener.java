/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.propertysheet;

import java.util.EventListener;
import org.openide.explorer.propertysheet.PropertySetModelEvent;

interface PropertySetModelListener
extends EventListener {
    public void pendingChange(PropertySetModelEvent var1);

    public void boundedChange(PropertySetModelEvent var1);

    public void wholesaleChange(PropertySetModelEvent var1);
}

