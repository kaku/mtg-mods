/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 */
package org.openide.explorer.propertysheet;

import java.beans.FeatureDescriptor;
import java.util.Comparator;
import org.openide.explorer.propertysheet.PropertySetModelListener;
import org.openide.nodes.Node;

interface PropertySetModel {
    public boolean isExpanded(FeatureDescriptor var1);

    public void toggleExpanded(int var1);

    public FeatureDescriptor getFeatureDescriptor(int var1);

    public void addPropertySetModelListener(PropertySetModelListener var1);

    public void removePropertySetModelListener(PropertySetModelListener var1);

    public void setPropertySets(Node.PropertySet[] var1);

    public boolean isProperty(int var1);

    public int getCount();

    public int indexOf(FeatureDescriptor var1);

    public void setComparator(Comparator<Node.Property> var1);

    public int getSetCount();

    public Comparator getComparator();
}

