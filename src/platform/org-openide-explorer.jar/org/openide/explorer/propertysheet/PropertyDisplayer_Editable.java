/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.propertysheet;

import java.awt.event.ActionListener;
import javax.swing.event.ChangeListener;
import org.openide.explorer.propertysheet.PropertyDisplayer;
import org.openide.explorer.propertysheet.PropertyEnv;

interface PropertyDisplayer_Editable
extends PropertyDisplayer {
    public void setEnabled(boolean var1);

    public void reset();

    public boolean isValueModified();

    public String isModifiedValueLegal();

    public boolean commit() throws IllegalArgumentException;

    public Object getEnteredValue();

    public void setEnteredValue(Object var1);

    public int getUpdatePolicy();

    public void setUpdatePolicy(int var1);

    public void setActionCommand(String var1);

    public String getActionCommand();

    public void addActionListener(ActionListener var1);

    public void removeActionListener(ActionListener var1);

    public void addChangeListener(ChangeListener var1);

    public void removeChangeListener(ChangeListener var1);

    public PropertyEnv getPropertyEnv();
}

