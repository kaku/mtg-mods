/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Index
 *  org.openide.nodes.IndexedNode
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.nodes.Node$IndexedProperty
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.nodes.PropertySupport
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.datatransfer.NewType
 */
package org.openide.explorer.propertysheet;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyEditor;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.StringTokenizer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.IndexedEditorPanel;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Index;
import org.openide.nodes.IndexedNode;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.NewType;

class IndexedPropertyEditor
implements ExPropertyEditor {
    private Object[] array;
    private PropertyEnv env;
    private PropertyChangeSupport propertySupport;
    private Node.IndexedProperty indexedProperty;
    private IndexedEditorPanel currentEditorPanel;

    public IndexedPropertyEditor() {
        this.propertySupport = new PropertyChangeSupport(this);
        this.indexedProperty = null;
    }

    @Override
    public void attachEnv(PropertyEnv env) {
        this.env = env;
        env.setChangeImmediate(false);
        FeatureDescriptor details = env.getFeatureDescriptor();
        if (!(details instanceof Node.IndexedProperty)) {
            throw new IllegalStateException("This is not an array: " + details);
        }
        this.indexedProperty = (Node.IndexedProperty)details;
    }

    @Override
    public void setValue(Object value) {
        if (value == null) {
            this.array = null;
            this.firePropertyChange();
            return;
        }
        if (!value.getClass().isArray()) {
            throw new IllegalArgumentException(this.env != null ? "Property whose value is not an array " + this.env.getFeatureDescriptor().getName() : "Unknown property - not attached yet.");
        }
        if (value.getClass().getComponentType().isPrimitive()) {
            this.array = Utilities.toObjectArray((Object)value);
        } else {
            this.array = (Object[])Array.newInstance(value.getClass().getComponentType(), ((Object[])value).length);
            System.arraycopy(value, 0, this.array, 0, this.array.length);
        }
        this.firePropertyChange();
    }

    @Override
    public Object getValue() {
        if (this.array == null) {
            return null;
        }
        if (this.indexedProperty.getElementType().isPrimitive()) {
            return Utilities.toPrimitiveArray((Object[])this.array);
        }
        return this.array;
    }

    @Override
    public boolean isPaintable() {
        return false;
    }

    @Override
    public void paintValue(Graphics gfx, Rectangle box) {
    }

    public String getJavaInitializationString(int index) {
        if (this.array[index] == null) {
            return "null";
        }
        try {
            this.indexedProperty.getIndexedPropertyEditor().setValue(this.array[index]);
            return this.indexedProperty.getIndexedPropertyEditor().getJavaInitializationString();
        }
        catch (NullPointerException e) {
            return "null";
        }
    }

    @Override
    public String getJavaInitializationString() {
        if (this.array == null) {
            return "";
        }
        StringBuilder buf = new StringBuilder("new ");
        buf.append(this.indexedProperty.getElementType().getCanonicalName());
        if (this.array.length == 0) {
            buf.append("[0]");
        } else {
            buf.append("[] {\n\t");
            for (int i = 0; i < this.array.length; ++i) {
                PropertyEditor ed = this.indexedProperty.getIndexedPropertyEditor();
                if (ed != null) {
                    ed.setValue(this.array[i]);
                    buf.append(ed.getJavaInitializationString());
                } else {
                    buf.append("???");
                }
                if (i != this.array.length - 1) {
                    buf.append(",\n\t");
                    continue;
                }
                buf.append("\n");
            }
            buf.append("}");
        }
        return buf.toString();
    }

    @Override
    public String getAsText() {
        if (this.array == null) {
            return "null";
        }
        StringBuffer buf = new StringBuffer("[");
        PropertyEditor p = null;
        if (this.indexedProperty != null) {
            p = this.indexedProperty.getIndexedPropertyEditor();
        }
        for (int i = 0; i < this.array.length; ++i) {
            if (p != null) {
                p.setValue(this.array[i]);
                buf.append(p.getAsText());
            } else {
                buf.append("null");
            }
            if (i == this.array.length - 1) continue;
            buf.append(", ");
        }
        buf.append("]");
        return buf.toString();
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if (text.equals("null")) {
            this.setValue(null);
            return;
        }
        if (text.equals("[]")) {
            this.setValue(Array.newInstance(this.indexedProperty.getElementType(), 0));
            return;
        }
        int i1 = text.indexOf(91);
        i1 = i1 < 0 || i1 + 1 >= text.length() ? 0 : ++i1;
        int i2 = text.lastIndexOf(93);
        if (i2 < 0) {
            i2 = text.length();
        }
        if (i2 < i1 || i2 > text.length()) {
            return;
        }
        try {
            PropertyEditor p = this.indexedProperty.getIndexedPropertyEditor();
            if (p == null) {
                throw new IllegalStateException("Indexed type has no property editor");
            }
            text = text.substring(i1, i2);
            StringTokenizer tok = new StringTokenizer(text, ",");
            LinkedList<Object> list = new LinkedList<Object>();
            while (tok.hasMoreTokens()) {
                String s = tok.nextToken();
                p.setAsText(s.trim());
                list.add(p.getValue());
            }
            Object[] a = list.toArray((Object[])Array.newInstance(this.getConvertedType(), list.size()));
            this.setValue(a);
        }
        catch (Exception x) {
            IllegalArgumentException iae = new IllegalArgumentException();
            Exceptions.attachLocalizedMessage((Throwable)iae, (String)IndexedPropertyEditor.getString("EXC_ErrorInIndexedSetter"));
            throw iae;
        }
    }

    @Override
    public String[] getTags() {
        return null;
    }

    @Override
    public Component getCustomEditor() {
        if (this.array == null) {
            this.array = (Object[])Array.newInstance(this.getConvertedType(), 0);
            this.firePropertyChange();
        }
        DisplayIndexedNode dummy = new DisplayIndexedNode(0);
        Node.Property prop = dummy.getPropertySets()[0].getProperties()[0];
        Node.Property[] np = new Node.Property[]{prop};
        this.currentEditorPanel = new IndexedEditorPanel(this.createRootNode(), np);
        return this.currentEditorPanel;
    }

    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.propertySupport.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.propertySupport.removePropertyChangeListener(listener);
    }

    private Node createRootNode() {
        DisplayIndexedNode[] n = new DisplayIndexedNode[this.array.length];
        for (int i = 0; i < n.length; ++i) {
            n[i] = new DisplayIndexedNode(i);
        }
        MyIndexedRootNode idr = new MyIndexedRootNode((Node[])n);
        Index ind = (Index)idr.getCookie(Index.class);
        for (int i2 = 0; i2 < n.length; ++i2) {
            ind.addChangeListener(WeakListeners.change((ChangeListener)n[i2], (Object)ind));
        }
        return idr;
    }

    private Class getConvertedType() {
        Class type = this.indexedProperty.getElementType();
        if (type.isPrimitive()) {
            type = Utilities.getObjectType((Class)type);
        }
        return type;
    }

    void firePropertyChange() {
        this.propertySupport.firePropertyChange("value", null, null);
    }

    private static String getString(String key) {
        return NbBundle.getMessage(IndexedPropertyEditor.class, (String)key);
    }

    private Object defaultValue() {
        Comparable value = null;
        if (this.indexedProperty.getElementType().isPrimitive()) {
            if (this.getConvertedType().equals(Integer.class)) {
                value = new Integer(0);
            }
            if (this.getConvertedType().equals(Boolean.class)) {
                value = Boolean.FALSE;
            }
            if (this.getConvertedType().equals(Byte.class)) {
                value = new Byte(0);
            }
            if (this.getConvertedType().equals(Character.class)) {
                value = new Character('\u0000');
            }
            if (this.getConvertedType().equals(Double.class)) {
                value = new Double(0.0);
            }
            if (this.getConvertedType().equals(Float.class)) {
                value = new Float(0.0f);
            }
            if (this.getConvertedType().equals(Long.class)) {
                value = new Long(0);
            }
            if (this.getConvertedType().equals(Short.class)) {
                value = new Short(0);
            }
        } else {
            try {
                value = (Comparable)this.getConvertedType().newInstance();
            }
            catch (Exception x) {
                // empty catch block
            }
        }
        return value;
    }

    private class MyIndexedRootNode
    extends IndexedNode {
        public MyIndexedRootNode(Node[] ch) {
            this.getChildren().add(ch);
            this.setName("IndexedRoot");
            this.setDisplayName(NbBundle.getMessage(IndexedPropertyEditor.class, (String)"CTL_Index"));
        }

        public NewType[] getNewTypes() {
            NewType nt = new NewType(){

                public void create() {
                    if (IndexedPropertyEditor.this.array != null) {
                        Object[] newArray = (Object[])Array.newInstance(IndexedPropertyEditor.this.getConvertedType(), IndexedPropertyEditor.this.array.length + 1);
                        System.arraycopy(IndexedPropertyEditor.this.array, 0, newArray, 0, IndexedPropertyEditor.this.array.length);
                        IndexedPropertyEditor.this.array = newArray;
                        IndexedPropertyEditor.access$100((IndexedPropertyEditor)IndexedPropertyEditor.this)[IndexedPropertyEditor.access$100((IndexedPropertyEditor)IndexedPropertyEditor.this).length - 1] = IndexedPropertyEditor.this.defaultValue();
                    } else {
                        IndexedPropertyEditor.this.array = (Object[])Array.newInstance(IndexedPropertyEditor.this.getConvertedType(), 1);
                        IndexedPropertyEditor.access$100((IndexedPropertyEditor)IndexedPropertyEditor.this)[0] = IndexedPropertyEditor.this.defaultValue();
                    }
                    IndexedPropertyEditor.this.firePropertyChange();
                    DisplayIndexedNode din = new DisplayIndexedNode(IndexedPropertyEditor.this.array.length - 1);
                    MyIndexedRootNode.this.getChildren().add(new Node[]{din});
                    Index i = (Index)MyIndexedRootNode.this.getCookie(Index.class);
                    i.addChangeListener(WeakListeners.change((ChangeListener)din, (Object)i));
                }
            };
            return new NewType[]{nt};
        }

    }

    class DisplayIndexedNode
    extends AbstractNode
    implements ChangeListener {
        private int index;

        public DisplayIndexedNode(int index) {
            super(Children.LEAF);
            this.index = index;
            this.setName(Integer.toString(index));
            this.setDisplayName(Integer.toString(index));
        }

        protected SystemAction[] createActions() {
            try {
                return new SystemAction[]{SystemAction.get(Class.forName("org.openide.actions.MoveUpAction").asSubclass(SystemAction.class)), SystemAction.get(Class.forName("org.openide.actions.MoveDownAction").asSubclass(SystemAction.class))};
            }
            catch (ClassNotFoundException cnfe) {
                return null;
            }
        }

        protected Sheet createSheet() {
            Sheet sheet = super.createSheet();
            Sheet.Set props = sheet.get("properties");
            if (props == null) {
                props = Sheet.createPropertiesSet();
                sheet.put(props);
            }
            props.put((Node.Property)new ValueProp());
            return sheet;
        }

        public void destroy() throws IOException {
            Object[] newArray = (Object[])Array.newInstance(IndexedPropertyEditor.this.getConvertedType(), IndexedPropertyEditor.this.array.length - 1);
            System.arraycopy(IndexedPropertyEditor.this.array, 0, newArray, 0, this.index);
            System.arraycopy(IndexedPropertyEditor.this.array, this.index + 1, newArray, this.index, IndexedPropertyEditor.this.array.length - this.index - 1);
            IndexedPropertyEditor.this.array = newArray;
            IndexedPropertyEditor.this.firePropertyChange();
            if (IndexedPropertyEditor.this.currentEditorPanel != null) {
                IndexedPropertyEditor.this.currentEditorPanel.getExplorerManager().setRootContext(IndexedPropertyEditor.this.createRootNode());
            }
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            int currentIndex;
            Node parent = this.getParentNode();
            Index i = (Index)parent.getCookie(Index.class);
            if (i != null && (currentIndex = i.indexOf((Node)this)) != this.index) {
                if (currentIndex > this.index) {
                    Object tmp = IndexedPropertyEditor.this.array[this.index];
                    IndexedPropertyEditor.access$100((IndexedPropertyEditor)IndexedPropertyEditor.this)[this.index] = IndexedPropertyEditor.this.array[currentIndex];
                    IndexedPropertyEditor.access$100((IndexedPropertyEditor)IndexedPropertyEditor.this)[currentIndex] = tmp;
                }
                this.index = currentIndex;
                this.firePropertyChange(null, (Object)null, (Object)null);
                this.setDisplayName(Integer.toString(this.index));
                IndexedPropertyEditor.this.firePropertyChange();
            }
        }

        private class ValueProp
        extends PropertySupport {
            public ValueProp() {
                super(IndexedPropertyEditor.this.indexedProperty.getName(), IndexedPropertyEditor.this.indexedProperty.getElementType(), IndexedPropertyEditor.this.indexedProperty.getDisplayName(), IndexedPropertyEditor.this.indexedProperty.getShortDescription(), IndexedPropertyEditor.this.indexedProperty.canRead(), IndexedPropertyEditor.this.indexedProperty.canWrite());
            }

            public Object getValue() {
                if (DisplayIndexedNode.this.index < IndexedPropertyEditor.this.array.length) {
                    return IndexedPropertyEditor.this.array[DisplayIndexedNode.this.index];
                }
                return null;
            }

            public void setValue(Object value) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
                Object oldVal = IndexedPropertyEditor.this.array[DisplayIndexedNode.this.index];
                IndexedPropertyEditor.access$100((IndexedPropertyEditor)IndexedPropertyEditor.this)[DisplayIndexedNode.access$500((DisplayIndexedNode)DisplayIndexedNode.this)] = value;
                DisplayIndexedNode.this.firePropertyChange(this.getName(), oldVal, value);
                IndexedPropertyEditor.this.firePropertyChange();
            }

            public PropertyEditor getPropertyEditor() {
                return IndexedPropertyEditor.this.indexedProperty.getIndexedPropertyEditor();
            }
        }

    }

}

