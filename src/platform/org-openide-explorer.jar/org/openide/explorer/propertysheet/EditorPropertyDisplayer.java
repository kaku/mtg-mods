/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.ImageUtilities
 */
package org.openide.explorer.propertysheet;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.KeyboardFocusManager;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.beans.FeatureDescriptor;
import java.beans.PropertyEditor;
import java.io.PrintStream;
import java.lang.ref.WeakReference;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.openide.explorer.propertysheet.ButtonPanel;
import org.openide.explorer.propertysheet.CheckboxInplaceEditor;
import org.openide.explorer.propertysheet.ComboInplaceEditor;
import org.openide.explorer.propertysheet.ExPropertyModel;
import org.openide.explorer.propertysheet.IconPanel;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.InplaceEditorFactory;
import org.openide.explorer.propertysheet.ModelProperty;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertyDisplayer;
import org.openide.explorer.propertysheet.PropertyDisplayer_Inline;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;
import org.openide.explorer.propertysheet.PropertyPanel;
import org.openide.explorer.propertysheet.RadioInplaceEditor;
import org.openide.explorer.propertysheet.RendererPropertyDisplayer;
import org.openide.explorer.propertysheet.ReusablePropertyEnv;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;

class EditorPropertyDisplayer
extends JComponent
implements PropertyDisplayer_Inline,
InplaceEditor.Factory {
    private Node.Property prop = null;
    private InplaceEditor inplace = null;
    private JComponent inner = null;
    private int radioButtonMax = 0;
    private boolean showCustomEditorButton = true;
    private boolean tableUI = false;
    private boolean useLabels = true;
    private PropertyEnv env = null;
    private boolean radioBoolean = false;
    protected WeakReference<PropertyModel> modelRef = null;
    protected boolean inReplaceInner = false;
    private InplaceEditorFactory factory1 = null;
    private InplaceEditorFactory factory2 = null;
    private ReusablePropertyEnv reusableEnv = new ReusablePropertyEnv();

    public EditorPropertyDisplayer(Node.Property p) {
        this(p, null);
    }

    EditorPropertyDisplayer(Node.Property p, PropertyModel mdl) {
        if (p == null) {
            throw new NullPointerException("Property may not be null");
        }
        this.prop = p;
        if (mdl != null) {
            this.modelRef = new WeakReference<PropertyModel>(mdl);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void addNotify() {
        try {
            if (this.inner == null) {
                this.replaceInner();
            }
        }
        finally {
            super.addNotify();
        }
    }

    @Override
    protected void processFocusEvent(FocusEvent fe) {
        super.processFocusEvent(fe);
        if (fe.getID() == 1004 && this.inner != null) {
            this.inner.requestFocus();
        }
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.setInplaceEditor(null);
        this.reusableEnv.clear();
    }

    @Override
    public final Component getComponent() {
        return this;
    }

    @Override
    public final Node.Property getProperty() {
        return this.prop;
    }

    @Override
    public final int getRadioButtonMax() {
        return this.radioButtonMax;
    }

    @Override
    public final boolean isShowCustomEditorButton() {
        Boolean explicit;
        boolean result = this.showCustomEditorButton;
        if (this.getProperty() != null && (explicit = (Boolean)this.getProperty().getValue("suppressCustomEditor")) != null) {
            result = explicit == false;
        }
        return result;
    }

    @Override
    public final boolean isTableUI() {
        return this.tableUI;
    }

    @Override
    public final void refresh() {
        if (this.isDisplayable()) {
            this.replaceInner();
        }
    }

    @Override
    public final boolean isUseLabels() {
        return this.useLabels;
    }

    @Override
    public final void setRadioButtonMax(int max) {
        if (max != this.radioButtonMax) {
            InplaceEditor innermost;
            int old = this.radioButtonMax;
            boolean needChange = false;
            if (this.inplace != null && ((innermost = PropUtils.findInnermostInplaceEditor(this.inplace)) instanceof JComboBox || innermost instanceof RadioInplaceEditor)) {
                PropertyEditor ped = innermost.getPropertyEditor();
                int tagCount = ped.getTags() == null ? -1 : ped.getTags().length;
                needChange = old <= tagCount != max <= tagCount;
            }
            this.radioButtonMax = max;
            if (needChange && this.inner != null) {
                this.replaceInner();
                this.firePropertyChange("preferredSize", null, null);
            }
        }
    }

    @Override
    public final void setShowCustomEditorButton(boolean val) {
        Boolean explicit;
        Node.Property p;
        if (this.getProperty() != null && (explicit = (Boolean)(p = this.getProperty()).getValue("suppressCustomEditor")) != null) {
            val = explicit;
            System.err.println("Found explicit value: " + val);
        }
        if (this.showCustomEditorButton != val) {
            this.showCustomEditorButton = val;
            this.replaceInner();
        }
    }

    @Override
    public final void setTableUI(boolean val) {
        if (val != this.tableUI) {
            this.tableUI = val;
            this.replaceInner();
        }
    }

    @Override
    public final void setUseLabels(boolean useLabels) {
        if (useLabels != this.useLabels) {
            boolean needChange = false;
            if (this.isShowing()) {
                InplaceEditor innermost = PropUtils.findInnermostInplaceEditor(this.inplace);
                needChange = innermost instanceof RadioInplaceEditor || innermost instanceof JCheckBox;
            }
            this.useLabels = useLabels;
            if (needChange && this.inner != null) {
                this.replaceInner();
            }
        }
    }

    @Override
    public final void requestFocus() {
        if (this.inner != null) {
            this.inner.requestFocus();
        } else {
            super.requestFocus();
        }
    }

    @Override
    public final Dimension getPreferredSize() {
        Dimension result = this.inner == null ? new RendererPropertyDisplayer(this.getProperty()).getRenderer(this).getPreferredSize() : this.inner.getPreferredSize();
        return result;
    }

    @Override
    public final boolean requestFocusInWindow() {
        boolean result = this.inner != null ? this.inner.requestFocusInWindow() : super.requestFocusInWindow();
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private final void installInner(JComponent c) {
        Object object = this.getTreeLock();
        synchronized (object) {
            if (this.inner != null) {
                this.remove(this.inner);
            }
            this.inner = c;
            if (this.inner != null) {
                c.setBounds(0, 0, this.getWidth(), this.getHeight());
                this.add(c);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final void replaceInner() {
        this.inReplaceInner = true;
        try {
            boolean wasComboPopup;
            Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getPermanentFocusOwner();
            boolean hadFocus = this.isEnabled() && (focusOwner == this || this.isAncestorOf(focusOwner) || this.getInplaceEditor() != null && this.getInplaceEditor().isKnownComponent(focusOwner));
            boolean bl = wasComboPopup = hadFocus && focusOwner instanceof JComboBox && ((JComboBox)focusOwner).isPopupVisible() && EventQueue.getCurrentEvent() instanceof KeyEvent && (((KeyEvent)EventQueue.getCurrentEvent()).getKeyCode() == 38 || ((KeyEvent)EventQueue.getCurrentEvent()).getKeyCode() == 40);
            if (hadFocus) {
                KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();
            }
            this.setInplaceEditor(this.createInplaceEditor());
            if (hadFocus) {
                this.requestFocus();
                if (wasComboPopup) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            InplaceEditor ied = EditorPropertyDisplayer.this.getInplaceEditor();
                            JComponent c = (ied = PropUtils.findInnermostInplaceEditor(ied)).getComponent();
                            if (c instanceof JComboBox && c.isShowing()) {
                                ((JComboBox)c).showPopup();
                            }
                        }
                    });
                }
            }
            this.revalidate();
            this.repaint();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            this.inReplaceInner = false;
        }
    }

    protected final JComponent getInner() {
        return this.inner;
    }

    protected void setInplaceEditor(InplaceEditor ed) {
        if (this.inplace != ed) {
            if (this.inplace != null) {
                this.inplace.clear();
            }
            this.inplace = ed;
            if (ed == null) {
                this.installInner(null);
            } else {
                JComponent comp = this.inplace.getComponent();
                this.prepareComponent(this.inplace);
                this.installInner(comp);
            }
        }
    }

    @Override
    public void setEnabled(boolean b) {
        if (this.inner != null) {
            this.inner.setEnabled(b);
        }
        super.setEnabled(b);
    }

    @Override
    public void setBackground(Color c) {
        super.setBackground(c);
        if (this.inner != null) {
            if (this.inplace != null && this.inplace.supportsTextEntry()) {
                this.inner.setBackground(PropUtils.getTextFieldBackground());
            } else {
                this.inner.setBackground(c);
            }
        }
    }

    @Override
    public void setForeground(Color c) {
        super.setForeground(c);
        if (this.inner != null) {
            if (this.inplace != null && this.inplace.supportsTextEntry()) {
                this.inner.setForeground(PropUtils.getTextFieldForeground());
            } else {
                this.inner.setForeground(c);
            }
        }
    }

    protected void prepareComponent(InplaceEditor inplace) {
        InplaceEditor innermost = PropUtils.findInnermostInplaceEditor(inplace);
        JComponent comp = innermost.getComponent();
        if (!this.isTableUI() && inplace.supportsTextEntry()) {
            comp.setBackground(PropUtils.getTextFieldBackground());
            comp.setForeground(PropUtils.getTextFieldForeground());
        } else {
            comp.setBackground(this.getBackground());
            if (!this.isEnabled() || !this.prop.canWrite()) {
                comp.setForeground(UIManager.getColor("textInactiveText"));
            } else {
                comp.setForeground(this.getForeground());
            }
        }
        if (comp instanceof ComboInplaceEditor) {
            comp.setEnabled(this.isEnabled() && this.getPropertyEnv().isEditable());
        } else {
            comp.setEnabled(this.isEnabled() && PropUtils.checkEnabled(this, inplace.getPropertyEditor(), this.getPropertyEnv()));
        }
    }

    @Override
    public void reshape(int x, int y, int w, int h) {
        if (this.inner != null) {
            this.inner.setBounds(0, 0, w, h);
        }
        super.reshape(x, y, w, h);
    }

    protected void setPropertyEnv(PropertyEnv env) {
        this.env = env;
    }

    public final PropertyEnv getPropertyEnv() {
        return this.env;
    }

    @Override
    public final InplaceEditor getInplaceEditor() {
        return this.inplace;
    }

    protected void configureButtonPanel(ButtonPanel bp) {
    }

    static final FeatureDescriptor findFeatureDescriptor(PropertyDisplayer pd) {
        Node.Property p;
        if (pd instanceof EditorPropertyDisplayer) {
            FeatureDescriptor fd;
            PropertyModel pm;
            EditorPropertyDisplayer epd = (EditorPropertyDisplayer)pd;
            if (epd.modelRef != null && (pm = epd.modelRef.get()) instanceof ExPropertyModel && (fd = ((ExPropertyModel)pm).getFeatureDescriptor()) != null) {
                return fd;
            }
        }
        if ((p = pd.getProperty()) instanceof ModelProperty) {
            return ((ModelProperty)p).getFeatureDescriptor();
        }
        if (p instanceof ModelProperty.DPMWrapper) {
            return ((ModelProperty.DPMWrapper)p).getFeatureDescriptor();
        }
        return p;
    }

    private InplaceEditor createInplaceEditor() {
        InplaceEditor result;
        PropertyEnv env = new PropertyEnv();
        env.setFeatureDescriptor(EditorPropertyDisplayer.findFeatureDescriptor(this));
        InplaceEditor innermost = result = this.factory(this).getInplaceEditor(this.getProperty(), env, true);
        if (this.isShowCustomEditorButton() && innermost.getPropertyEditor().supportsCustomEditor()) {
            ButtonPanel bp = new ButtonPanel();
            bp.setInplaceEditor(innermost);
            this.configureButtonPanel(bp);
            result = bp;
        }
        Icon ic = null;
        if (env.getState() == PropertyEnv.STATE_INVALID) {
            ic = ImageUtilities.loadImageIcon((String)"org/openide/resources/propertysheet/invalid.gif", (boolean)false);
        } else if (this.getProperty().getValue("valueIcon") != null) {
            Object o = this.getProperty().getValue("valueIcon");
            ic = o instanceof Image ? new ImageIcon((Image)o) : (Icon)o;
        }
        if (ic != null) {
            IconPanel iconPanel = new IconPanel();
            iconPanel.setIcon(ic);
            iconPanel.setInplaceEditor(result);
            result = iconPanel;
        }
        this.setPropertyEnv(env);
        return result;
    }

    private InplaceEditorFactory factory(PropertyDisplayer_Inline inline) {
        InplaceEditorFactory result;
        if (inline.isTableUI()) {
            if (this.factory1 == null) {
                this.factory1 = new InplaceEditorFactory(inline.isTableUI(), inline.getReusablePropertyEnv());
            }
            result = this.factory1;
        } else {
            if (this.factory2 == null) {
                this.factory2 = new InplaceEditorFactory(inline.isTableUI(), inline.getReusablePropertyEnv());
            }
            result = this.factory2;
        }
        result.setUseRadioBoolean(inline.isRadioBoolean());
        result.setRadioButtonMax(inline.getRadioButtonMax());
        result.setUseLabels(inline.isUseLabels());
        return result;
    }

    @Override
    public boolean isTitleDisplayed() {
        if (this.isUseLabels()) {
            InplaceEditor inp = null;
            inp = this.inplace != null ? this.inplace : this.createInplaceEditor();
            InplaceEditor most = PropUtils.findInnermostInplaceEditor(inp);
            return most instanceof RadioInplaceEditor || most instanceof CheckboxInplaceEditor;
        }
        return false;
    }

    @Override
    public boolean isRadioBoolean() {
        return this.radioBoolean;
    }

    @Override
    public void setRadioBoolean(boolean b) {
        this.radioBoolean = b;
    }

    @Override
    public ReusablePropertyEnv getReusablePropertyEnv() {
        return this.reusableEnv;
    }

    static final Object[] findBeans(PropertyDisplayer pd) {
        Object[] result = null;
        if (pd instanceof EditorPropertyDisplayer) {
            PropertyModel pm;
            EditorPropertyDisplayer epd = (EditorPropertyDisplayer)pd;
            if (epd.modelRef != null && (pm = epd.modelRef.get()) instanceof ExPropertyModel) {
                result = ((ExPropertyModel)pm).getBeans();
            }
        }
        if (result == null) {
            Node.Property p = pd.getProperty();
            if (p instanceof ModelProperty) {
                result = ((ModelProperty)p).getBeans();
            } else if (p instanceof ModelProperty.DPMWrapper) {
                result = ((ModelProperty.DPMWrapper)p).getBeans();
            } else if (pd instanceof EditorPropertyDisplayer && ((EditorPropertyDisplayer)pd).getParent() instanceof PropertyPanel) {
                result = ((PropertyPanel)((EditorPropertyDisplayer)pd).getParent()).getBeans();
            } else if (pd instanceof RendererPropertyDisplayer && ((RendererPropertyDisplayer)pd).getParent() instanceof PropertyPanel) {
                result = ((PropertyPanel)((RendererPropertyDisplayer)pd).getParent()).getBeans();
            }
        }
        return result;
    }

}

