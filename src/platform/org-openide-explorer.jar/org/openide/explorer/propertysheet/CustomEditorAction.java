/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.openide.explorer.propertysheet;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.lang.ref.WeakReference;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.JDialog;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.ExPropertyModel;
import org.openide.explorer.propertysheet.NodePropertyModel;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertyDialogManager;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;
import org.openide.explorer.propertysheet.ProxyNode;
import org.openide.explorer.propertysheet.ReusablePropertyEnv;
import org.openide.explorer.propertysheet.SheetTable;
import org.openide.explorer.propertysheet.editors.EnhancedCustomPropertyEditor;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

class CustomEditorAction
extends AbstractAction {
    private Invoker invoker;
    private WeakReference<PropertyModel> modelRef = null;

    public CustomEditorAction(Invoker invoker) {
        this.invoker = invoker;
        this.putValue("SmallIcon", PropUtils.getCustomButtonIcon());
    }

    public CustomEditorAction(Invoker invoker, PropertyModel mdl) {
        this(invoker);
        if (mdl != null) {
            this.modelRef = new WeakReference<PropertyModel>(mdl);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        Node.Property p;
        if (PropUtils.isLoggable(CustomEditorAction.class)) {
            PropUtils.log(CustomEditorAction.class, "CustomEditorAction invoked " + ae);
        }
        if (!this.invoker.allowInvoke()) {
            if (PropUtils.isLoggable(CustomEditorAction.class)) {
                PropUtils.log(CustomEditorAction.class, "Invoker (" + this.invoker.getClass() + " allowInvoke() returned false.  Aborting.");
            }
            return;
        }
        PropertyModel refd = this.modelRef != null ? this.modelRef.get() : null;
        FeatureDescriptor fd = this.invoker.getSelection();
        Node.Property property = p = fd instanceof Node.Property ? (Node.Property)fd : null;
        if (p == null) {
            if (PropUtils.isLoggable(CustomEditorAction.class)) {
                PropUtils.log(CustomEditorAction.class, "Cant invoke custom editor on " + fd + " it is null or not a Property." + "Aborting.");
            }
            Utilities.disabledActionBeep();
            return;
        }
        final PropertyEditor editor = PropUtils.getPropertyEditor(p);
        PropertyEnv env = null;
        if (editor instanceof ExPropertyEditor) {
            if (PropUtils.isLoggable(CustomEditorAction.class)) {
                PropUtils.log(CustomEditorAction.class, "Editor is an ExPropertyEditor, attaching a PropertyEnv");
            }
            env = new PropertyEnv();
            env.setFeatureDescriptor(fd);
            if (this.invoker instanceof SheetTable) {
                if (PropUtils.isLoggable(CustomEditorAction.class)) {
                    PropUtils.log(CustomEditorAction.class, "env.setBeans to " + this.invoker.getReusablePropertyEnv().getBeans());
                }
                env.setBeans(this.invoker.getReusablePropertyEnv().getBeans());
            }
            ((ExPropertyEditor)editor).attachEnv(env);
        }
        if (!editor.supportsCustomEditor()) {
            if (PropUtils.isLoggable(CustomEditorAction.class)) {
                PropUtils.log(CustomEditorAction.class, "Cant invoke custom editor for editor " + editor + " - it returns false " + "from supportsCustomEditor().");
            }
            Utilities.disabledActionBeep();
            return;
        }
        final Component curComp = this.invoker.getCursorChangeComponent();
        if (curComp != null) {
            Cursor cur = Cursor.getPredefinedCursor(3);
            curComp.setCursor(cur);
        }
        try {
            PropertyChangeListener pcl;
            Object partialValue = this.invoker.getPartialValue();
            if (partialValue != null) {
                try {
                    if (!(editor.getValue() != null && partialValue.toString().equals(editor.getAsText()) || editor instanceof PropUtils.DifferentValuesEditor)) {
                        editor.setAsText(partialValue.toString());
                    }
                }
                catch (ProxyNode.DifferentValuesException dve) {
                }
                catch (Exception ite) {
                    // empty catch block
                }
            }
            final PropertyModel mdl = refd == null ? new NodePropertyModel(p, null) : refd;
            String fdName = mdl instanceof ExPropertyModel && ((ExPropertyModel)mdl).getFeatureDescriptor() != null ? ((ExPropertyModel)mdl).getFeatureDescriptor().getDisplayName() : null;
            String suppliedTitle = (String)p.getValue("title");
            final String title = suppliedTitle == null ? (fd.getDisplayName() == null ? NbBundle.getMessage(CustomEditorAction.class, (String)"FMT_CUSTOM_DLG_NOPROPNAME_TITLE", (Object)(fdName == null ? this.invoker.getBeanName() : fdName)) : (fd.getDisplayName().equals(this.invoker.getBeanName()) ? this.invoker.getBeanName() : NbBundle.getMessage(CustomEditorAction.class, (String)"FMT_CUSTOM_DLG_TITLE", (Object)this.invoker.getBeanName(), (Object)fd.getDisplayName()))) : suppliedTitle;
            final PropertyDialogManager pdm = new PropertyDialogManager(NbBundle.getMessage(CustomEditorAction.class, (String)"PS_EditorTitle", (Object)(title == null ? "" : title), (Object)p.getValueType()), true, editor, mdl, env);
            boolean shouldListen = !(pdm.getComponent() instanceof EnhancedCustomPropertyEditor) && p.canWrite() && (this.invoker.wantAllChanges() || env == null || env.isChangeImmediate());
            PropertyChangeListener propertyChangeListener = pcl = !shouldListen ? null : new PropertyChangeListener(){
                private boolean updating;

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void propertyChange(PropertyChangeEvent pce) {
                    if (this.updating) {
                        return;
                    }
                    this.updating = true;
                    try {
                        boolean success = PropUtils.updateProp(mdl, editor, title);
                        if (success) {
                            CustomEditorAction.this.invoker.valueChanged(editor);
                        } else if (!pdm.wasCancelled()) {
                            CustomEditorAction.this.invoker.failed();
                        }
                    }
                    finally {
                        this.updating = false;
                    }
                }
            };
            if (pcl != null) {
                editor.addPropertyChangeListener(pcl);
            }
            final Window w = pdm.getDialog();
            WindowAdapter wl = new WindowAdapter(){
                boolean closedOption;

                @Override
                public void windowClosed(WindowEvent e) {
                    if (pdm.getComponent() instanceof EnhancedCustomPropertyEditor && !pdm.wasCancelled() && !this.closedOption && pdm.wasOK() && !pdm.wasReset()) {
                        try {
                            CustomEditorAction.this.invoker.valueChanged(pdm.getEditor());
                        }
                        catch (Exception ex) {
                            // empty catch block
                        }
                    }
                    CustomEditorAction.this.invoker.editorClosed();
                    w.removeWindowListener(this);
                    if (pcl != null) {
                        editor.removePropertyChangeListener(pcl);
                    }
                }

                @Override
                public void windowOpened(WindowEvent e) {
                    CustomEditorAction.this.invoker.editorOpened();
                    if (curComp != null) {
                        curComp.setCursor(Cursor.getPredefinedCursor(0));
                    }
                }

                @Override
                public void windowClosing(WindowEvent ev) {
                    if (PropUtils.isLoggable(CustomEditorAction.class)) {
                        PropUtils.log(CustomEditorAction.class, "CustomerEditorAction windowClosing event");
                    }
                    this.closedOption = true;
                }
            };
            if (w instanceof JDialog) {
                JDialog jd = (JDialog)w;
                jd.getAccessibleContext().setAccessibleName(title);
                if (fd.getShortDescription() != null) {
                    jd.getAccessibleContext().setAccessibleDescription(fd.getShortDescription());
                }
                w.addWindowListener(wl);
            } else if (w instanceof Frame) {
                ((Frame)w).addWindowListener(wl);
            }
            this.invoker.editorOpening();
            try {
                PropUtils.addExternallyEdited(p);
                w.setVisible(true);
                PropUtils.removeExternallyEdited(p);
            }
            catch (Exception ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        finally {
            if (curComp != null) {
                curComp.setCursor(Cursor.getPredefinedCursor(0));
            }
        }
    }

    static interface Invoker {
        public FeatureDescriptor getSelection();

        public Object getPartialValue();

        public Component getCursorChangeComponent();

        public String getBeanName();

        public void editorOpening();

        public void editorOpened();

        public void editorClosed();

        public void valueChanged(PropertyEditor var1);

        public boolean allowInvoke();

        public void failed();

        public boolean wantAllChanges();

        public ReusablePropertyEnv getReusablePropertyEnv();
    }

}

