/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.propertysheet;

import java.beans.PropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;

public interface ExPropertyEditor
extends PropertyEditor {
    public static final String PROP_VALUE_VALID = "propertyValueValid";
    public static final String PROPERTY_HELP_ID = "helpID";

    public void attachEnv(PropertyEnv var1);
}

