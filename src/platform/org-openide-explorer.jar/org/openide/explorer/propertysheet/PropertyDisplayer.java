/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 */
package org.openide.explorer.propertysheet;

import java.awt.Component;
import org.openide.nodes.Node;

interface PropertyDisplayer {
    public static final int UPDATE_ON_CONFIRMATION = 0;
    public static final int UPDATE_ON_FOCUS_LOST = 1;
    public static final int UPDATE_ON_EXPLICIT_REQUEST = 2;

    public Node.Property getProperty();

    public void refresh();

    public Component getComponent();
}

