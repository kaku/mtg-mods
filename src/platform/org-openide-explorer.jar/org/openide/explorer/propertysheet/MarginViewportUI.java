/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.propertysheet;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.JViewport;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.ViewportUI;
import org.openide.explorer.propertysheet.PropUtils;

class MarginViewportUI
extends ViewportUI
implements ComponentListener,
ContainerListener {
    private JViewport viewport;
    private int lastHeight = -1;
    private int stringWidth = -1;
    private int stringHeight = -1;
    private int ascent = -1;
    Rectangle scratch = new Rectangle();
    private String emptyString = "THIS IS A BUG";
    Color marginColor = UIManager.getColor("controlShadow");
    private int marginWidth = PropUtils.getMarginWidth();
    private boolean marginPainted = false;
    Dimension lastKnownSize = new Dimension();
    private static Map<RenderingHints.Key, Object> hintsMap = null;
    private static final boolean antialias = Boolean.getBoolean("swing.aatext") || "Aqua".equals(UIManager.getLookAndFeel().getID());

    private MarginViewportUI(JViewport jv) {
        this.viewport = jv;
    }

    public static ComponentUI createUI(JComponent c) {
        return new MarginViewportUI((JViewport)c);
    }

    @Override
    public void installUI(JComponent c) {
        super.installUI(c);
        Color fg = UIManager.getColor("controlShadow");
        if (fg == null) {
            fg = Color.LIGHT_GRAY;
        }
        c.setForeground(fg);
        Color bg = UIManager.getColor("Tree.background");
        if (bg == null) {
            bg = Color.WHITE;
        }
        c.setBackground(bg);
        Font f = UIManager.getFont("Tree.font");
        if (f == null) {
            f = UIManager.getFont("controlFont");
        }
        if (f != null) {
            c.setFont(f);
        }
        c.addContainerListener(this);
        Component[] kids = c.getComponents();
        for (int i = 0; i < kids.length; ++i) {
            kids[i].addComponentListener(this);
        }
    }

    @Override
    public void uninstallUI(JComponent vp) {
        JViewport jv = (JViewport)vp;
        Component[] c = jv.getComponents();
        for (int i = 0; i < c.length; ++i) {
            c[i].removeComponentListener(this);
        }
        jv.removeContainerListener(this);
    }

    public void setEmptyString(String s) {
        this.emptyString = s;
        this.stringWidth = -1;
        this.stringHeight = -1;
    }

    public void setMarginColor(Color c) {
        this.marginColor = c;
    }

    public void setMarginWidth(int margin) {
        this.marginWidth = margin;
    }

    public void setMarginPainted(boolean val) {
        if (this.marginPainted != val) {
            this.marginPainted = val;
            this.viewport.repaint();
        }
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        Component view = ((JViewport)c).getView();
        if (view != null) {
            this.lastKnownSize = view.getSize();
        }
        if (this.stringWidth == -1) {
            this.calcStringSizes(c.getFont(), g);
        }
        ((Graphics2D)g).addRenderingHints(MarginViewportUI.getHints());
        if (this.shouldPaintEmptyMessage()) {
            g.setFont(c.getFont());
            g.setColor(c.getForeground());
            Rectangle r = this.getEmptyMessageBounds();
            if (g.hitClip(r.x, r.y, r.width, r.height)) {
                g.drawString(this.emptyString, r.x, r.y + this.ascent);
            }
        }
    }

    static Map<RenderingHints.Key, Object> getHints() {
        if (hintsMap == null && (MarginViewportUI.hintsMap = (Map)Toolkit.getDefaultToolkit().getDesktopProperty("awt.font.desktophints")) == null) {
            hintsMap = new HashMap<RenderingHints.Key, Object>();
            if (antialias) {
                hintsMap.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                hintsMap.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            }
        }
        return hintsMap;
    }

    private void calcStringSizes(Font f, Graphics g) {
        FontMetrics fm = g.getFontMetrics(f);
        this.stringWidth = fm.stringWidth(this.emptyString);
        this.stringHeight = fm.getHeight();
        this.ascent = fm.getMaxAscent();
    }

    private Rectangle getEmptyMessageBounds() {
        Insets ins = this.viewport.getInsets();
        this.scratch.x = ins.left + ((this.viewport.getWidth() - (ins.left + ins.right)) / 2 - this.stringWidth / 2);
        this.scratch.y = ins.top + ((this.viewport.getHeight() - (ins.top + ins.bottom)) / 2 - this.stringHeight / 2);
        this.scratch.width = this.stringWidth;
        this.scratch.height = this.stringHeight;
        return this.scratch;
    }

    @Override
    public void update(Graphics g, JComponent c) {
        g.setColor(c.getBackground());
        boolean margin = this.shouldPaintMargin();
        int leftEdge = margin ? this.marginWidth : 0;
        g.fillRect(leftEdge, 0, c.getWidth() - leftEdge, c.getHeight());
        if (margin) {
            g.setColor(this.marginColor);
            g.fillRect(0, 0, this.marginWidth, c.getHeight());
        }
        this.paint(g, c);
    }

    private void scheduleRepaint(Dimension nuSize) {
        int heightDif;
        if (this.marginPainted || nuSize.height > 10 == this.lastKnownSize.height > 10) {
            // empty if block
        }
        if ((heightDif = Math.abs(nuSize.height - this.lastKnownSize.height)) == 0) {
            // empty if block
        }
        Insets ins = this.viewport.getInsets();
        this.viewport.repaint(ins.left, ins.top, this.marginWidth, this.viewport.getHeight() - (ins.top + ins.bottom));
        Rectangle r = this.getEmptyMessageBounds();
        this.viewport.repaint(r.x, r.y, r.width, r.height);
    }

    private boolean shouldPaintEmptyMessage() {
        Dimension d = this.viewport.getView().getSize();
        return d.height < 10;
    }

    private boolean shouldPaintMargin() {
        return this.marginPainted & !this.shouldPaintEmptyMessage();
    }

    @Override
    public void componentAdded(ContainerEvent e) {
        e.getChild().addComponentListener(this);
    }

    @Override
    public void componentHidden(ComponentEvent e) {
    }

    @Override
    public void componentMoved(ComponentEvent e) {
    }

    @Override
    public void componentRemoved(ContainerEvent e) {
        e.getChild().removeComponentListener(this);
    }

    @Override
    public void componentResized(ComponentEvent e) {
        this.scheduleRepaint(((Component)e.getSource()).getSize());
    }

    @Override
    public void componentShown(ComponentEvent e) {
        this.scheduleRepaint(((Component)e.getSource()).getSize());
    }
}

