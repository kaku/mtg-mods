/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 */
package org.openide.explorer.propertysheet;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.beans.FeatureDescriptor;
import java.beans.PropertyEditor;
import java.util.EventListener;
import java.util.EventObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.EventListenerList;
import javax.swing.table.TableCellEditor;
import org.openide.explorer.propertysheet.ButtonPanel;
import org.openide.explorer.propertysheet.ExPropertyModel;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.InplaceEditorFactory;
import org.openide.explorer.propertysheet.NodePropertyModel;
import org.openide.explorer.propertysheet.PropUtils;
import org.openide.explorer.propertysheet.PropertyModel;
import org.openide.explorer.propertysheet.PropertySetModel;
import org.openide.explorer.propertysheet.ReusablePropertyEnv;
import org.openide.explorer.propertysheet.SheetTable;
import org.openide.explorer.propertysheet.SheetTableModel;
import org.openide.nodes.Node;

final class SheetCellEditor
implements TableCellEditor,
ActionListener {
    ChangeEvent ce = null;
    ReusablePropertyEnv reusableEnv;
    private EventListenerList listenerList = null;
    private InplaceEditorFactory factory = null;
    private ButtonPanel buttonPanel = null;
    InplaceEditor inplaceEditor = null;
    boolean lastUpdateSuccess = true;
    private boolean inStopCellEditing = false;
    static boolean ignoreStopCellEditing = false;

    SheetCellEditor(ReusablePropertyEnv env) {
        this.reusableEnv = env;
    }

    void setInplaceEditor(InplaceEditor ie) {
        if (ie == this.inplaceEditor) {
            return;
        }
        if (PropUtils.isLoggable(SheetCellEditor.class)) {
            PropUtils.log(SheetCellEditor.class, "  SheetCellEditor.setInplaceEditor " + ie);
        }
        if (ie == null) {
            if (this.inplaceEditor != null) {
                this.inplaceEditor.clear();
            }
        } else {
            ie.addActionListener(this);
        }
        if (this.inplaceEditor != null) {
            this.inplaceEditor.removeActionListener(this);
        }
        this.inplaceEditor = ie;
    }

    PropertyEditor getPropertyEditor() {
        PropertyEditor result = this.inplaceEditor == null ? null : this.inplaceEditor.getPropertyEditor();
        return result;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        Component result = null;
        SheetTable stb = (SheetTable)table;
        this.lastUpdateSuccess = true;
        Node.Property p = (Node.Property)stb.getSheetModel().getPropertySetModel().getFeatureDescriptor(row);
        result = this.getEditorComponent(p, this, table.getForeground(), table.getBackground(), table.getSelectionBackground(), table.getSelectionForeground());
        if (result instanceof ButtonPanel) {
            ((ButtonPanel)result).setButtonAction(stb.getCustomEditorAction());
        }
        if (result != null) {
            result.setFont(stb.getFont());
        }
        return result;
    }

    private InplaceEditorFactory factory() {
        if (this.factory == null) {
            this.factory = new InplaceEditorFactory(true, this.reusableEnv);
        }
        return this.factory;
    }

    private ButtonPanel buttonPanel() {
        if (this.buttonPanel == null) {
            this.buttonPanel = new ButtonPanel();
        }
        return this.buttonPanel;
    }

    public boolean isLastUpdateSuccessful() {
        return this.lastUpdateSuccess;
    }

    public Component getEditorComponent(Node.Property p, ActionListener al, Color foreground, Color background, Color selBg, Color selFg) {
        JComponent result = null;
        InplaceEditor inplace = this.factory().getInplaceEditor(p, false);
        this.setInplaceEditor(inplace);
        PropertyEditor ped = this.inplaceEditor.getPropertyEditor();
        if (ped instanceof PropUtils.NoPropertyEditorEditor) {
            this.setInplaceEditor(null);
            return null;
        }
        boolean propRequestsSuppressButton = Boolean.TRUE.equals(p.getValue("suppressCustomEditor"));
        JComponent realEditor = null;
        if (ped.supportsCustomEditor() && !propRequestsSuppressButton) {
            realEditor = this.inplaceEditor.getComponent();
            ButtonPanel bp = this.buttonPanel();
            bp.setInplaceEditor(inplace);
            result = bp;
        } else {
            result = this.inplaceEditor.getComponent();
        }
        return result;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (PropUtils.isLoggable(SheetCellEditor.class)) {
            PropUtils.log(SheetCellEditor.class, "Editor received an action event - " + ae.getActionCommand());
        }
        if (!(ae.getSource() instanceof InplaceEditor)) {
            if (PropUtils.isLoggable(SheetCellEditor.class)) {
                PropUtils.log(SheetCellEditor.class, " Event came from an unknown object type - assuming a legacy EnhancedPropertyEditor is the cause and updating property");
            }
            if (this.inplaceEditor != null) {
                if (PropUtils.isLoggable(SheetCellEditor.class)) {
                    PropUtils.log(SheetCellEditor.class, "WRITING PROPERTY VALUE FROM EDITOR TO PROPERTY");
                }
                PropUtils.updateProp(this.inplaceEditor.getPropertyModel(), this.inplaceEditor.getPropertyEditor(), "");
            }
            this.cancelCellEditing();
        }
        if (ae.getActionCommand() == "success") {
            this.stopCellEditing();
        } else if (ae.getActionCommand() == "failure") {
            if (PropUtils.psCommitOnFocusLoss) {
                this.stopCellEditing();
            } else {
                this.cancelCellEditing();
            }
        } else {
            return;
        }
    }

    protected void fireEditingStopped() {
        if (PropUtils.isLoggable(SheetCellEditor.class)) {
            PropUtils.log(SheetCellEditor.class, "    SheetCellEditor firing editing stopped to table ");
        }
        if (this.listenerList == null) {
            return;
        }
        Object[] listeners = this.listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] != CellEditorListener.class) continue;
            if (this.ce == null) {
                this.ce = new ChangeEvent(this);
            }
            ((CellEditorListener)listeners[i + 1]).editingStopped(this.ce);
        }
    }

    protected void fireEditingCancelled() {
        if (PropUtils.isLoggable(SheetCellEditor.class)) {
            PropUtils.log(SheetCellEditor.class, "    SheetCellEditor firing editing cancelled to table ");
        }
        PropUtils.notifyEditingCancelled(this.reusableEnv);
        if (this.listenerList == null) {
            return;
        }
        Object[] listeners = this.listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] != CellEditorListener.class) continue;
            if (this.ce == null) {
                this.ce = new ChangeEvent(this);
            }
            ((CellEditorListener)listeners[i + 1]).editingCanceled(this.ce);
        }
    }

    public InplaceEditor getInplaceEditor() {
        return this.inplaceEditor;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void cancelCellEditing() {
        if (this.inplaceEditor != null) {
            try {
                if (PropUtils.isLoggable(SheetCellEditor.class)) {
                    PropUtils.log(SheetCellEditor.class, "  SheetCellEditor.cancelCellEditing ", true);
                }
                this.fireEditingCancelled();
            }
            finally {
                this.setInplaceEditor(null);
            }
        }
    }

    @Override
    public Object getCellEditorValue() {
        if (this.inplaceEditor != null) {
            return this.inplaceEditor.getValue();
        }
        return null;
    }

    @Override
    public boolean isCellEditable(EventObject anEvent) {
        return true;
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        if (anEvent instanceof MouseEvent) {
            MouseEvent e = (MouseEvent)anEvent;
            return e.getID() != 506;
        }
        return true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean stopCellEditing() {
        if (PropUtils.isLoggable(SheetCellEditor.class)) {
            PropUtils.log(SheetCellEditor.class, "SheetCellEditor.StopCellEditing", true);
        }
        if (this.inplaceEditor != null && !this.inStopCellEditing) {
            PropertyModel mdl;
            block15 : {
                this.inStopCellEditing = true;
                Component c = KeyboardFocusManager.getCurrentKeyboardFocusManager().getPermanentFocusOwner();
                if (!(PropUtils.psCommitOnFocusLoss || c instanceof JTable || this.inplaceEditor.isKnownComponent(c) || c == this.inplaceEditor.getComponent())) {
                    if (PropUtils.isLoggable(SheetCellEditor.class)) {
                        PropUtils.log(SheetCellEditor.class, "Focused component is unknown - discarding");
                    }
                    boolean bl = false;
                    return bl;
                }
                mdl = this.inplaceEditor.getPropertyModel();
                try {
                    this.lastUpdateSuccess = PropUtils.updateProp(this.inplaceEditor);
                }
                catch (NullPointerException npe) {
                    if (this.inplaceEditor == null || this.inplaceEditor.getPropertyEditor() == null) {
                        String propID = mdl instanceof NodePropertyModel ? ((NodePropertyModel)mdl).getProperty().toString() + " editor class " + (mdl.getPropertyEditorClass() != null ? mdl.getPropertyEditorClass().getName() : " unknown editor class") + " " : "";
                        Logger.getAnonymousLogger().warning("Property " + propID + "value changed *while* the property sheet was setting its value " + "but before it had been set.  This almost always means that the " + "property editor has modified the property's value itself. " + "Property editors should NEVER directly modify properties, it is " + "up to the displayer to decide if/when the property should be " + "updated.  This behavior may cause an exception in the " + "future.");
                        Logger.getAnonymousLogger().log(Level.FINE, null, npe);
                        boolean bl = false;
                        if (!ignoreStopCellEditing) {
                            this.setInplaceEditor(null);
                        }
                        this.inStopCellEditing = false;
                        return bl;
                    }
                    throw npe;
                }
                if (!PropUtils.isLoggable(SheetCellEditor.class)) break block15;
                PropUtils.log(SheetCellEditor.class, "  SheetCellEditor Firing editing stopped");
            }
            if (!ignoreStopCellEditing) {
                this.fireEditingStopped();
            }
            this.tryPostSetAction(mdl);
            finally {
                if (!ignoreStopCellEditing) {
                    this.setInplaceEditor(null);
                }
                this.inStopCellEditing = false;
            }
            return true;
        }
        return false;
    }

    void tryPostSetAction(PropertyModel mdl) {
        Action a;
        FeatureDescriptor fd;
        if (mdl instanceof ExPropertyModel && (fd = ((ExPropertyModel)mdl).getFeatureDescriptor()) != null && (a = (Action)fd.getValue("postSetAction")) != null) {
            if (PropUtils.isLoggable(SheetCellEditor.class)) {
                PropUtils.log(SheetCellEditor.class, "  Running post-set action " + a);
            }
            ActionEvent ae = new ActionEvent(this, 1001, "success");
            a.actionPerformed(ae);
        }
    }

    @Override
    public synchronized void addCellEditorListener(CellEditorListener listener) {
        if (this.listenerList == null) {
            this.listenerList = new EventListenerList();
        }
        this.listenerList.add(CellEditorListener.class, listener);
    }

    @Override
    public synchronized void removeCellEditorListener(CellEditorListener listener) {
        this.listenerList.remove(CellEditorListener.class, listener);
        if (this.listenerList.getListenerCount(CellEditorListener.class) == 0 && this.inplaceEditor != null) {
            this.inplaceEditor.clear();
        }
    }
}

