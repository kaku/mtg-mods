/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.propertysheet;

interface IncrementPropertyValueSupport {
    public static final String KEY_INCREMENT_VALUE_SUPPORT = "valueIncrement";

    public boolean incrementValue();

    public boolean decrementValue();

    public boolean isIncrementEnabled();
}

