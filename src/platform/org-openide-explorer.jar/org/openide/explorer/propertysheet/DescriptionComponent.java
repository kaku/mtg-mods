/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.openide.explorer.propertysheet;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleRole;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.text.EditorKit;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import org.openide.explorer.propertysheet.PSheet;
import org.openide.util.NbBundle;

class DescriptionComponent
extends JComponent
implements ActionListener,
MouseListener,
Accessible {
    private static int fontHeight = -1;
    private JEditorPane jep;
    private JLabel lbl;
    private JEditorPane jep2;
    private JScrollPane jsc;

    public DescriptionComponent() {
        this.init();
    }

    private void init() {
        this.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.jep = new JEditorPane();
        Color bg = UIManager.getColor("prop-sheet-bg");
        Color fg = UIManager.getColor("prop-sheet-fg");
        Color fgDisabled = UIManager.getColor("prop-sheet-disabled-fg");
        this.jep.setOpaque(true);
        this.jep.setBackground(bg);
        this.jep.setForeground(fgDisabled);
        this.jep2 = new JEditorPane();
        this.jep2.setOpaque(true);
        this.jep2.setBackground(bg);
        this.jep2.setForeground(fgDisabled);
        this.jep2.setEditable(false);
        this.jep2.putClientProperty("JEditorPane.honorDisplayProperties", Boolean.TRUE);
        this.jep.setEditable(false);
        this.jep.getAccessibleContext().setAccessibleName(NbBundle.getMessage(DescriptionComponent.class, (String)"ACS_Description"));
        this.jep.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DescriptionComponent.class, (String)"ACSD_Description"));
        this.jep.putClientProperty("JEditorPane.honorDisplayProperties", Boolean.TRUE);
        HTMLEditorKit htmlKit = new HTMLEditorKit();
        if (htmlKit.getStyleSheet().getStyleSheets() == null) {
            StyleSheet css = new StyleSheet();
            Font f = new JLabel().getFont();
            css.addRule(new StringBuffer("body { font-size: ").append(f.getSize()).append("; font-family: ").append(f.getName()).append("; }").toString());
            css.addStyleSheet(htmlKit.getStyleSheet());
            htmlKit.setStyleSheet(css);
        } else {
            this.jep.setFont(new JLabel().getFont());
            this.jep2.setFont(new JLabel().getFont());
        }
        this.jep.setEditorKit(htmlKit);
        this.jep2.setEditorKit(htmlKit);
        this.jsc = new JScrollPane(this.jep);
        this.jsc.setHorizontalScrollBarPolicy(31);
        this.jsc.setVerticalScrollBarPolicy(20);
        this.jsc.setBorder(BorderFactory.createEmptyBorder());
        this.jsc.setViewportBorder(this.jsc.getBorder());
        this.jsc.setOpaque(false);
        this.jsc.setBackground(this.getBackground());
        this.jsc.getViewport().setOpaque(false);
        this.lbl = new JLabel("Label");
        this.lbl.setFont(new Font(null, 1, this.lbl.getFont().getSize()));
        this.lbl.setForeground(fg);
        this.setLayout(new BorderLayout());
        JPanel panel = new JPanel();
        panel.setBackground(bg);
        panel.setLayout(new BorderLayout());
        panel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        JPanel propIdPanel = new JPanel(new BorderLayout());
        propIdPanel.setBackground(bg);
        propIdPanel.add((Component)this.lbl, "North");
        propIdPanel.add((Component)this.jep2, "Center");
        panel.add((Component)propIdPanel, "North");
        panel.add((Component)this.jsc, "Center");
        this.add(panel);
    }

    public void setDescription(String title, String txt) {
        if (title == null) {
            title = "";
        }
        if (txt == null) {
            txt = "";
        }
        this.lbl.setText(title);
        if (title.equals(txt)) {
            this.jep.setText("");
        } else {
            this.jep.setText(txt);
        }
    }

    public void setPropId(String id) {
        if (id == null) {
            id = "";
        }
        this.jep2.setText(id);
        this.jep2.setVisible(!"".equals(id));
    }

    public void setHelpEnabled(boolean val) {
    }

    @Override
    public void paint(Graphics g) {
        if (fontHeight == -1) {
            fontHeight = g.getFontMetrics(this.lbl.getFont()).getHeight();
        }
        super.paint(g);
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension d = new Dimension(super.getPreferredSize());
        if (fontHeight > 0) {
            Insets ins = this.getInsets();
            d.height = Math.max(50, Math.max(d.height, 4 * fontHeight + ins.top + ins.bottom + 12));
        } else {
            d.height = Math.min(d.height, 64);
        }
        return d;
    }

    @Override
    public Dimension getMinimumSize() {
        if (fontHeight < 0) {
            return super.getMinimumSize();
        }
        Dimension d = new Dimension(4 * fontHeight, 4 * fontHeight);
        return d;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        PSheet sheet = (PSheet)SwingUtilities.getAncestorOfClass(PSheet.class, this);
        if (sheet != null) {
            sheet.helpRequested();
        }
    }

    private PSheet findSheet() {
        return (PSheet)SwingUtilities.getAncestorOfClass(PSheet.class, this);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public AccessibleContext getAccessibleContext() {
        if (null == this.accessibleContext) {
            this.accessibleContext = new JComponent.AccessibleJComponent(){

                @Override
                public AccessibleRole getAccessibleRole() {
                    return AccessibleRole.SWING_COMPONENT;
                }
            };
            this.accessibleContext.setAccessibleName(NbBundle.getMessage(DescriptionComponent.class, (String)"ACS_Description"));
            this.accessibleContext.setAccessibleDescription(NbBundle.getMessage(DescriptionComponent.class, (String)"ACSD_Description"));
        }
        return this.accessibleContext;
    }

}

