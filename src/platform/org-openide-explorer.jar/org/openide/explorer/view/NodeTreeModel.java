/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.Mutex
 */
package org.openide.explorer.view;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.EventListenerList;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import org.openide.explorer.view.NodeModel;
import org.openide.explorer.view.TreeView;
import org.openide.explorer.view.Visualizer;
import org.openide.explorer.view.VisualizerChildren;
import org.openide.explorer.view.VisualizerEvent;
import org.openide.explorer.view.VisualizerNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Mutex;

public class NodeTreeModel
extends DefaultTreeModel {
    static final long serialVersionUID = 1900670294524747212L;
    private static final Logger LOG = Logger.getLogger(NodeTreeModel.class.getName());
    private transient Listener listener;
    private CopyOnWriteArrayList<TreeView> views = new CopyOnWriteArrayList();

    void addView(TreeView tw) {
        this.views.add(tw);
    }

    public NodeTreeModel() {
        super(VisualizerNode.EMPTY, true);
    }

    public NodeTreeModel(Node root) {
        super(VisualizerNode.EMPTY, true);
        this.doCallSetNode(root);
    }

    final void doCallSetNode(Node r) {
        this.setNode(r);
    }

    public void setNode(Node root) {
        this.setNode(root, null);
    }

    void setNode(final Node root, final TreeView.VisualizerHolder visHolder) {
        Mutex.EVENT.readAccess(new Runnable(){

            @Override
            public void run() {
                VisualizerNode nr;
                VisualizerNode v = (VisualizerNode)NodeTreeModel.this.getRoot();
                if (v == (nr = VisualizerNode.getVisualizer(null, root))) {
                    return;
                }
                v.removeNodeModel(NodeTreeModel.this.listener());
                nr.addNodeModel(NodeTreeModel.this.listener());
                NodeTreeModel.this.setRoot(nr);
                if (visHolder != null) {
                    visHolder.add(nr.getChildren());
                    visHolder.removeRecur(v.getChildren());
                }
            }
        });
    }

    private Listener listener() {
        if (this.listener == null) {
            this.listener = new Listener(this);
        }
        return this.listener;
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        if (path == null) {
            return;
        }
        Object o = path.getLastPathComponent();
        if (o instanceof VisualizerNode) {
            this.nodeChanged((VisualizerNode)o);
            return;
        }
        MutableTreeNode aNode = (MutableTreeNode)o;
        aNode.setUserObject(newValue);
        this.nodeChanged(aNode);
    }

    void nodesWereInsertedInternal(VisualizerEvent ev) {
        if (this.listenerList == null) {
            return;
        }
        VisualizerNode node = ev.getVisualizer();
        Object[] path = this.getPathToRoot(node);
        Object[] listeners = this.listenerList.getListenerList();
        TreeModelEventImpl e = null;
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] != TreeModelListener.class) continue;
            if (e == null) {
                e = new TreeModelEventImpl(this, path, ev);
            }
            try {
                ((TreeModelListener)listeners[i + 1]).treeNodesInserted(e);
                continue;
            }
            catch (IndexOutOfBoundsException ex) {
                LOG.log(Level.WARNING, "Visualizer: {0}", node);
                Node n = Visualizer.findNode(node);
                LOG.log(Level.WARNING, "Node: {0}", (Object)n);
                if (n != null) {
                    LOG.log(Level.WARNING, "  # children: {0}", n.getChildren().getNodesCount());
                    LOG.log(Level.WARNING, "  children: {0}", n.getChildren().getClass());
                }
                LOG.log(Level.WARNING, "Path: {0}", Arrays.toString(path));
                LOG.log(Level.WARNING, "ev.getArray: {0}", Arrays.toString(ev.getArray()));
                LOG.log(Level.WARNING, "ev.getSnapshot: {0}", ev.getSnapshot());
                throw ex;
            }
        }
    }

    static Object[] computeChildren(VisualizerEvent ev) {
        int[] childIndices = ev.getArray();
        Object[] arr = new Object[childIndices.length];
        List<Node> nodes = ev.getSnapshot();
        for (int i = 0; i < arr.length; ++i) {
            arr[i] = Visualizer.findVisualizer(nodes.get(childIndices[i]));
        }
        return arr;
    }

    private static class TreeModelEventImpl
    extends TreeModelEvent {
        private final VisualizerEvent ev;

        public TreeModelEventImpl(Object source, Object[] path, VisualizerEvent ev) {
            super(source, path, ev.getArray(), null);
            this.ev = ev;
        }

        @Override
        public Object[] getChildren() {
            if (this.children == null) {
                this.children = NodeTreeModel.computeChildren(this.ev);
            }
            return this.children;
        }
    }

    private static final class Listener
    implements NodeModel {
        private Reference<NodeTreeModel> model;

        public Listener(NodeTreeModel m) {
            this.model = new WeakReference<NodeTreeModel>(m);
        }

        private NodeTreeModel get(VisualizerEvent ev) {
            NodeTreeModel m = this.model.get();
            if (m == null && ev != null) {
                ev.getVisualizer().removeNodeModel(this);
                return null;
            }
            return m;
        }

        @Override
        public void added(VisualizerEvent.Added ev) {
            NodeTreeModel m = this.get(ev);
            if (m == null) {
                return;
            }
            m.nodesWereInsertedInternal(ev);
        }

        @Override
        public void removed(VisualizerEvent.Removed ev) {
            NodeTreeModel m = this.get(ev);
            if (m == null) {
                return;
            }
            for (TreeView tw : m.views) {
                tw.removedNodes(ev.removed);
            }
            m.nodesWereRemoved(ev.getVisualizer(), ev.getArray(), ev.removed.toArray());
        }

        @Override
        public void reordered(VisualizerEvent.Reordered ev) {
            NodeTreeModel m = this.get(ev);
            if (m == null) {
                return;
            }
            m.nodeStructureChanged(ev.getVisualizer());
        }

        @Override
        public void update(VisualizerNode v) {
            NodeTreeModel m = this.get(null);
            if (m == null) {
                return;
            }
            m.nodeChanged(v);
        }

        @Override
        public void structuralChange(VisualizerNode v) {
            NodeTreeModel m = this.get(null);
            if (m == null) {
                return;
            }
            m.nodeStructureChanged(v);
        }
    }

}

