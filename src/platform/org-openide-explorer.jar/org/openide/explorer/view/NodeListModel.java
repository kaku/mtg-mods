/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.Mutex
 */
package org.openide.explorer.view;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import javax.swing.ListModel;
import javax.swing.tree.TreeNode;
import org.openide.explorer.view.NodeModel;
import org.openide.explorer.view.VisualizerEvent;
import org.openide.explorer.view.VisualizerNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Mutex;

public class NodeListModel
extends AbstractListModel
implements ComboBoxModel {
    static final long serialVersionUID = -1926931095895356820L;
    private transient Listener listener;
    private transient VisualizerNode parent = VisualizerNode.EMPTY;
    private transient boolean showParent;
    private transient Object selectedObject = VisualizerNode.EMPTY;
    private transient int size;
    private int depth = 1;
    private Map<VisualizerNode, Info> childrenCount;

    public NodeListModel() {
        this.clearChildrenCount();
    }

    public NodeListModel(Node root) {
        this();
        this.setNode(root);
    }

    public void setNode(Node root) {
        this.setNode(root, false);
    }

    final void setNode(final Node root, final boolean showRoot) {
        Mutex.EVENT.readAccess(new Runnable(){

            @Override
            public void run() {
                if (!Children.MUTEX.isReadAccess() && !Children.MUTEX.isWriteAccess()) {
                    Children.MUTEX.readAccess((Runnable)this);
                    return;
                }
                VisualizerNode v = VisualizerNode.getVisualizer(null, root);
                if (v == NodeListModel.this.parent && NodeListModel.this.showParent == showRoot) {
                    return;
                }
                NodeListModel.this.removeAll();
                NodeListModel.this.parent.removeNodeModel(NodeListModel.this.listener());
                NodeListModel.this.showParent = showRoot;
                NodeListModel.this.parent = v;
                NodeListModel.this.selectedObject = v;
                NodeListModel.this.clearChildrenCount();
                NodeListModel.this.addAll();
                NodeListModel.this.parent.addNodeModel(NodeListModel.this.listener());
            }
        });
    }

    public void setDepth(int depth) {
        if (depth != this.depth) {
            this.depth = depth;
            this.clearChildrenCount();
            Mutex.EVENT.readAccess(new Runnable(){

                @Override
                public void run() {
                    NodeListModel.this.removeAll();
                    NodeListModel.this.addAll();
                }
            });
        }
    }

    public int getDepth() {
        return this.depth;
    }

    private Listener listener() {
        if (this.listener == null) {
            this.listener = new Listener(this);
        }
        return this.listener;
    }

    @Override
    public int getSize() {
        int s = this.findSize(this.parent, this.showParent, -1, this.depth);
        return s;
    }

    @Override
    public Object getElementAt(int i) {
        return this.findElementAt(this.parent, this.showParent, i, -1, this.depth);
    }

    public int getIndex(Object o) {
        this.getSize();
        Info i = this.childrenCount.get(o);
        return i == null ? -1 : i.index;
    }

    @Override
    public void setSelectedItem(Object anObject) {
        if (this.selectedObject != anObject) {
            this.selectedObject = anObject;
            this.fireContentsChanged(this, -1, -1);
        }
    }

    @Override
    public Object getSelectedItem() {
        return this.selectedObject;
    }

    private void clearChildrenCount() {
        this.childrenCount = new HashMap<VisualizerNode, Info>(17);
    }

    private int findSize(VisualizerNode vis, boolean includeOwnself, int index, int depth) {
        Info info = this.childrenCount.get(vis);
        if (info != null) {
            return info.childrenCount;
        }
        if (includeOwnself) {
            ++index;
        }
        int tmp = 0;
        info = new Info();
        info.depth = depth;
        info.index = index;
        if (depth-- > 0) {
            Enumeration<VisualizerNode> it = vis.children();
            while (it.hasMoreElements()) {
                VisualizerNode v = it.nextElement();
                ++tmp;
                tmp += this.findSize(v, false, index + tmp, depth);
            }
        }
        info.childrenCount = includeOwnself ? tmp + 1 : tmp;
        this.childrenCount.put(vis, info);
        return tmp;
    }

    private VisualizerNode findElementAt(VisualizerNode vis, boolean countSelf, int indx, int realIndx, int depth) {
        if (countSelf) {
            if (indx == 0) {
                return vis;
            }
            --indx;
        }
        if (--depth == 0) {
            return (VisualizerNode)vis.getChildAt(indx);
        }
        Enumeration<VisualizerNode> it = vis.children();
        while (it.hasMoreElements()) {
            int s;
            VisualizerNode v = it.nextElement();
            if (indx-- == 0) {
                return v;
            }
            if (indx < (s = this.findSize(v, false, ++realIndx, depth))) {
                return this.findElementAt(v, false, indx, realIndx, depth);
            }
            indx -= s;
            realIndx += s;
        }
        return vis;
    }

    static int findVisualizerDepth(ListModel m, VisualizerNode o) {
        if (m instanceof NodeListModel) {
            NodeListModel n = (NodeListModel)m;
            Info i = n.childrenCount.get(o);
            if (i != null) {
                return n.depth - i.depth - 1;
            }
        }
        return 0;
    }

    final void addAll() {
        this.size = this.getSize();
        if (this.size > 0) {
            this.fireIntervalAdded(this, 0, this.size - 1);
        }
    }

    final void removeAll() {
        if (this.size > 0) {
            this.fireIntervalRemoved(this, 0, this.size - 1);
        }
    }

    final void changeAll() {
        this.size = this.getSize();
        if (this.size > 0) {
            this.fireContentsChanged(this, 0, this.size - 1);
        }
        this.clearChildrenCount();
    }

    final void added(VisualizerEvent.Added ev) {
        VisualizerNode v = ev.getVisualizer();
        int[] indices = ev.getArray();
        if (this.cachedDepth(v) <= 0 || indices.length == 0) {
            return;
        }
        this.clearChildrenCount();
        this.size = this.getSize();
        int seg = this.parent == v ? 0 : this.getIndex(v);
        this.fireIntervalAdded(this, indices[0] + seg, indices[indices.length - 1] + seg);
    }

    final void removed(VisualizerEvent.Removed ev) {
        VisualizerNode v = ev.getVisualizer();
        int[] indices = ev.getArray();
        if (this.cachedDepth(v) <= 0 || indices.length == 0) {
            return;
        }
        this.clearChildrenCount();
        int seg = this.parent == v ? 0 : this.getIndex(v);
        this.fireIntervalRemoved(this, indices[0] + seg, indices[indices.length - 1] + seg);
    }

    final void update(VisualizerNode v) {
        this.getSize();
        Info i = this.childrenCount.get(v);
        if (i != null) {
            this.fireContentsChanged(this, i.index, i.index);
        }
    }

    private int cachedDepth(VisualizerNode v) {
        this.getSize();
        Info i = this.childrenCount.get(v);
        if (i != null) {
            return i.depth;
        }
        return -1;
    }

    private static final class Info {
        public int childrenCount;
        public int depth;
        public int index;

        Info() {
        }

        public String toString() {
            return "Info[childrenCount=" + this.childrenCount + ", depth=" + this.depth + ", index=" + this.index;
        }
    }

    private static final class Listener
    implements NodeModel {
        private Reference<NodeListModel> model;

        public Listener(NodeListModel m) {
            this.model = new WeakReference<NodeListModel>(m);
        }

        private NodeListModel get(VisualizerEvent ev) {
            NodeListModel m = this.model.get();
            if (m == null && ev != null) {
                ev.getVisualizer().removeNodeModel(this);
                return null;
            }
            return m;
        }

        @Override
        public void added(VisualizerEvent.Added ev) {
            NodeListModel m = this.get(ev);
            if (m == null) {
                return;
            }
            m.added(ev);
        }

        @Override
        public void removed(VisualizerEvent.Removed ev) {
            NodeListModel m = this.get(ev);
            if (m == null) {
                return;
            }
            m.removed(ev);
        }

        @Override
        public void reordered(VisualizerEvent.Reordered ev) {
            NodeListModel m = this.get(ev);
            if (m == null) {
                return;
            }
            m.changeAll();
        }

        @Override
        public void update(VisualizerNode v) {
            NodeListModel m = this.get(null);
            if (m == null) {
                return;
            }
            m.update(v);
        }

        @Override
        public void structuralChange(VisualizerNode v) {
            NodeListModel m = this.get(null);
            if (m == null) {
                return;
            }
            m.changeAll();
        }
    }

}

