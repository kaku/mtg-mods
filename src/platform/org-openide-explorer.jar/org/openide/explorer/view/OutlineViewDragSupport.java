/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.Outline
 *  org.openide.nodes.Node
 */
package org.openide.explorer.view;

import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.netbeans.swing.outline.Outline;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.DropGlassPane;
import org.openide.explorer.view.ExplorerDnDManager;
import org.openide.explorer.view.ExplorerDragSupport;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;

final class OutlineViewDragSupport
extends ExplorerDragSupport {
    protected OutlineView view;

    public OutlineViewDragSupport(OutlineView view, JComponent table) {
        this.view = view;
        this.comp = table;
    }

    @Override
    public int getAllowedDragActions() {
        return this.view.getAllowedDragActions();
    }

    @Override
    int getAllowedDropActions() {
        return this.view.getAllowedDropActions();
    }

    @Override
    public void dragGestureRecognized(DragGestureEvent dge) {
        super.dragGestureRecognized(dge);
    }

    @Override
    public void dragDropEnd(DragSourceDropEvent dsde) {
        Node[] dropedNodes = this.exDnD.getDraggedNodes();
        super.dragDropEnd(dsde);
        if (DropGlassPane.isOriginalPaneStored()) {
            DropGlassPane.putBackOriginal();
            this.exDnD.setDnDActive(false);
        }
        try {
            ExplorerManager.Provider panel;
            if (dropedNodes != null && (panel = (ExplorerManager.Provider)((Object)SwingUtilities.getAncestorOfClass(ExplorerManager.Provider.class, this.view))) != null) {
                panel.getExplorerManager().setSelectedNodes(dropedNodes);
            }
        }
        catch (Exception e) {
            // empty catch block
        }
    }

    @Override
    Node[] obtainNodes(DragGestureEvent dge) {
        int[] selRows = this.view.getOutline().getSelectedRows();
        ArrayList<Node> al = new ArrayList<Node>(selRows.length);
        for (int i = 0; i < selRows.length; ++i) {
            Node n = this.view.getNodeFromRow(selRows[i]);
            if (n == null) continue;
            al.add(n);
        }
        Node[] result = al.toArray((T[])new Node[al.size()]);
        return result;
    }
}

