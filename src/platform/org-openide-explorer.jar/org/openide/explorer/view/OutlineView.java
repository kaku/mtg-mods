/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.etable.ETable
 *  org.netbeans.swing.etable.ETableColumnModel
 *  org.netbeans.swing.etable.ETableTransferHandler
 *  org.netbeans.swing.etable.TableColumnSelector
 *  org.netbeans.swing.outline.DefaultOutlineModel
 *  org.netbeans.swing.outline.Outline
 *  org.netbeans.swing.outline.Outline$OutlineColumn
 *  org.netbeans.swing.outline.OutlineModel
 *  org.netbeans.swing.outline.RenderDataProvider
 *  org.netbeans.swing.outline.RowModel
 *  org.netbeans.swing.outline.TreePathSupport
 *  org.openide.awt.Mnemonics
 *  org.openide.awt.MouseUtils
 *  org.openide.awt.MouseUtils$PopupMouseAdapter
 *  org.openide.awt.QuickSearch
 *  org.openide.awt.QuickSearch$Callback
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.PropertySupport
 *  org.openide.nodes.PropertySupport$ReadWrite
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 */
package org.openide.explorer.view;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.EventObject;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.BoundedRangeModel;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolTip;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.MenuElement;
import javax.swing.ScrollPaneLayout;
import javax.swing.Scrollable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.tree.AbstractLayoutCache;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import org.netbeans.modules.openide.explorer.ExplorerActionsImpl;
import org.netbeans.swing.etable.ETable;
import org.netbeans.swing.etable.ETableColumnModel;
import org.netbeans.swing.etable.ETableTransferHandler;
import org.netbeans.swing.etable.TableColumnSelector;
import org.netbeans.swing.outline.DefaultOutlineModel;
import org.netbeans.swing.outline.Outline;
import org.netbeans.swing.outline.OutlineModel;
import org.netbeans.swing.outline.RenderDataProvider;
import org.netbeans.swing.outline.RowModel;
import org.netbeans.swing.outline.TreePathSupport;
import org.openide.awt.Mnemonics;
import org.openide.awt.MouseUtils;
import org.openide.awt.QuickSearch;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.propertysheet.PropertyPanel;
import org.openide.explorer.view.DragDropUtilities;
import org.openide.explorer.view.NodePopupFactory;
import org.openide.explorer.view.NodeRenderDataProvider;
import org.openide.explorer.view.NodeTreeModel;
import org.openide.explorer.view.OutlineViewDragSupport;
import org.openide.explorer.view.OutlineViewDropSupport;
import org.openide.explorer.view.PropertiesRowModel;
import org.openide.explorer.view.QuickSearchTableFilter;
import org.openide.explorer.view.SheetCell;
import org.openide.explorer.view.TableQuickSearchSupport;
import org.openide.explorer.view.TreeView;
import org.openide.explorer.view.ViewUtil;
import org.openide.explorer.view.Visualizer;
import org.openide.explorer.view.VisualizerNode;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;

public class OutlineView
extends JScrollPane {
    private static final String TREE_HORIZONTAL_SCROLLBAR = "TREE_HORIZONTAL_SCROLLBAR";
    private static RequestProcessor REVALIDATING_RP = new RequestProcessor("OutlineView", 1);
    private static final Logger logger = Logger.getLogger(OutlineView.class.getName());
    private OutlineViewOutline outline;
    ExplorerManager manager;
    private final Object managerLock = new Object();
    private PopupAdapter popupListener;
    private TableSelectionListener managerListener = null;
    private PropertyChangeListener wlpc;
    private VetoableChangeListener wlvc;
    private OutlineModel model;
    private NodeTreeModel treeModel = new NodeTreeModel();
    private PropertiesRowModel rowModel = new PropertiesRowModel();
    private NodePopupFactory popupFactory;
    private transient boolean dragActive = true;
    private transient boolean dropActive = true;
    transient OutlineViewDragSupport dragSupport;
    transient OutlineViewDropSupport dropSupport;
    transient boolean dropTargetPopupAllowed = true;
    private transient int allowedDragActions = 1073741827;
    private transient int allowedDropActions = 1073741827;
    private ActionListener defaultTreeActionListener;
    private boolean isTreeHScrollBar = false;
    private JScrollBar hScrollBar;
    private int treeHorizontalScrollBarPolicy = this.isTreeHScrollBar ? 30 : 31;
    private ScrollListener listener;
    private Selection selection = null;
    private QuickSearch quickSearch;
    private Component searchPanel;
    private final Object searchConstraints = new Object();
    private KeyListener qsKeyListener;
    private boolean horizontalScrollBarIsNeeded = false;

    public OutlineView() {
        this(null);
    }

    public OutlineView(String nodesColumnLabel) {
        this.model = this.createOutlineModel(this.treeModel, this.rowModel, nodesColumnLabel);
        this.outline = new OutlineViewOutline(this.model, this.rowModel);
        TableQuickSearchSupport tqss = this.outline.createDefaultTableQuickSearchSupport();
        this.attachQuickSearch(tqss, false, tqss.createSearchPopupMenu());
        this.qsKeyListener = new KeyListener(){

            @Override
            public void keyTyped(KeyEvent e) {
                OutlineView.this.quickSearch.processKeyEvent(e);
            }

            @Override
            public void keyPressed(KeyEvent e) {
                OutlineView.this.quickSearch.processKeyEvent(e);
            }

            @Override
            public void keyReleased(KeyEvent e) {
                OutlineView.this.quickSearch.processKeyEvent(e);
            }
        };
        this.outline.addKeyListener(this.qsKeyListener);
        this.rowModel.setOutline(this.outline);
        this.outline.setRenderDataProvider((RenderDataProvider)new NodeRenderDataProvider(this.outline));
        SheetCell.OutlineSheetCell tableCell = new SheetCell.OutlineSheetCell(this.outline);
        this.outline.setDefaultRenderer(Node.Property.class, (TableCellRenderer)tableCell);
        this.outline.setDefaultEditor(Node.Property.class, (TableCellEditor)tableCell);
        this.hScrollBar = this.createHorizontalScrollBar();
        this.hScrollBar.setUnitIncrement(10);
        this.setLayout(new OutlineScrollLayout());
        this.add(this.hScrollBar, "TREE_HORIZONTAL_SCROLLBAR");
        this.setViewportView((Component)((Object)this.outline));
        this.setPopupAllowed(true);
        this.setRequestFocusEnabled(false);
        this.outline.setRequestFocusEnabled(true);
        Color c = UIManager.getColor("Table.background1");
        if (c == null) {
            c = UIManager.getColor("Table.background");
        }
        if (c != null) {
            this.getViewport().setBackground(c);
        }
        this.getActionMap().put("org.openide.actions.PopupAction", new PopupAction());
        this.popupFactory = new OutlinePopupFactory();
        this.setDropTarget(DragDropUtilities.dragAndDropEnabled);
        this.defaultTreeActionListener = new DefaultTreeAction(this.outline);
        this.outline.registerKeyboardAction(this.defaultTreeActionListener, KeyStroke.getKeyStroke(10, 0, false), 0);
        final Color focusSelectionBackground = this.outline.getSelectionBackground();
        final Color focusSelectionForeground = this.outline.getSelectionForeground();
        this.outline.addFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent ev) {
                OutlineView.this.outline.setSelectionBackground(focusSelectionBackground);
                OutlineView.this.outline.setSelectionForeground(focusSelectionForeground);
            }

            @Override
            public void focusLost(FocusEvent ev) {
                OutlineView.this.outline.setSelectionBackground(SheetCell.getNoFocusSelectionBackground());
                OutlineView.this.outline.setSelectionForeground(SheetCell.getNoFocusSelectionForeground());
            }
        });
        this.outline.setSelectionBackground(SheetCell.getNoFocusSelectionBackground());
        this.outline.setSelectionForeground(SheetCell.getNoFocusSelectionForeground());
        TableColumnSelector tcs = (TableColumnSelector)Lookup.getDefault().lookup(TableColumnSelector.class);
        if (tcs != null) {
            this.outline.setColumnSelector(tcs);
        }
        if (DragDropUtilities.dragAndDropEnabled) {
            this.setDragSource(true);
        }
        this.setBorder(BorderFactory.createEmptyBorder());
        this.initializeTreeScrollSupport();
    }

    private void attachQuickSearch(QuickSearch.Callback callback, boolean asynchronous, JMenu popup) {
        if (this.quickSearch != null) {
            this.quickSearch.detach();
        }
        this.quickSearch = QuickSearch.attach((JComponent)this, (Object)this.searchConstraints, (QuickSearch.Callback)callback, (boolean)asynchronous, (JMenu)popup);
    }

    @Override
    public void add(Component comp, Object constraints) {
        if (constraints == this.searchConstraints) {
            this.searchPanel = comp;
            constraints = null;
        }
        super.add(comp, constraints);
    }

    @Override
    public void remove(Component comp) {
        if (comp == this.searchPanel) {
            this.searchPanel = null;
        }
        super.remove(comp);
    }

    protected OutlineModel createOutlineModel(NodeTreeModel treeModel, RowModel rowModel, String label) {
        if (label == null) {
            label = NbBundle.getMessage(OutlineView.class, (String)"NodeOutlineModel_NodesColumnLabel");
        }
        return new NodeOutlineModel(treeModel, rowModel, false, label);
    }

    private void initializeTreeScrollSupport() {
        if (UIManager.getColor("Table.background") != null) {
            this.getViewport().setBackground(UIManager.getColor("Table.background"));
        }
        this.listener = new ScrollListener();
        if (this.isTreeHScrollBar) {
            this.outline.getColumnModel().addColumnModelListener(this.listener);
        }
        if (ViewUtil.isAquaLaF) {
            this.addMouseWheelListener(new MouseWheelListener(){

                @Override
                public void mouseWheelMoved(MouseWheelEvent e) {
                    if (e.isShiftDown() && !e.isAltDown() && !e.isAltGraphDown() && !e.isControlDown() && !e.isMetaDown() && OutlineView.this.horizontalWheelScroll(e)) {
                        e.consume();
                    }
                }
            });
        }
        final RequestProcessor.Task revalidatingTask = REVALIDATING_RP.create(new Runnable(){

            @Override
            public void run() {
                if (!SwingUtilities.isEventDispatchThread()) {
                    try {
                        SwingUtilities.invokeAndWait(this);
                    }
                    catch (InterruptedException ex) {
                    }
                    catch (InvocationTargetException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                } else {
                    OutlineView.this.listener.revalidateScrollBar();
                    OutlineView.this.revalidate();
                }
            }
        });
        this.outline.setTreeWidthChangeTask(revalidatingTask);
        this.getViewport().addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                revalidatingTask.schedule(100);
            }
        });
        this.hScrollBar.addAdjustmentListener(this.listener);
        this.hScrollBar.getModel().addChangeListener(this.listener);
    }

    public int getTreeHorizontalScrollBarPolicy() {
        return this.treeHorizontalScrollBarPolicy;
    }

    public void setTreeHorizontalScrollBarPolicy(int policy) {
        if (policy == this.treeHorizontalScrollBarPolicy) {
            return;
        }
        switch (policy) {
            case 30: 
            case 31: 
            case 32: {
                break;
            }
            default: {
                throw new IllegalArgumentException("invalid treeHorizontalScrollBarPolicy");
            }
        }
        int old = this.treeHorizontalScrollBarPolicy;
        this.treeHorizontalScrollBarPolicy = policy;
        boolean wasHScrollBarVisible = this.isTreeHScrollBar;
        boolean bl = this.isTreeHScrollBar = policy != 31;
        if (wasHScrollBarVisible != this.isTreeHScrollBar) {
            if (!wasHScrollBarVisible) {
                this.outline.getColumnModel().addColumnModelListener(this.listener);
            } else {
                this.outline.getColumnModel().removeColumnModelListener(this.listener);
            }
            this.outline.setTreeHScrollingEnabled(this.isTreeHScrollBar, this.hScrollBar);
        }
        this.firePropertyChange("treeHorizontalScrollBarPolicy", old, policy);
        this.revalidate();
        this.repaint();
    }

    private void sayHorizontalScrollBarNeeded(boolean horizontalScrollBarIsNeeded) {
        this.horizontalScrollBarIsNeeded = horizontalScrollBarIsNeeded;
    }

    @Override
    public int getHorizontalScrollBarPolicy() {
        if (this.horizontalScrollBarIsNeeded) {
            return 32;
        }
        return super.getHorizontalScrollBarPolicy();
    }

    @Override
    public void requestFocus() {
        this.outline.requestFocus();
    }

    @Override
    public boolean requestFocusInWindow() {
        return this.outline.requestFocusInWindow();
    }

    public Outline getOutline() {
        return this.outline;
    }

    public boolean isPopupAllowed() {
        return this.popupListener != null;
    }

    @Deprecated
    public void setProperties(Node.Property[] newProperties) {
        this.setProperties(newProperties, true);
    }

    private void setProperties(Node.Property[] newProperties, boolean doCleanColumns) {
        TableColumnModel tcm;
        if (doCleanColumns && (tcm = this.outline.getColumnModel()) instanceof ETableColumnModel) {
            ((ETableColumnModel)tcm).clean();
        }
        this.rowModel.setProperties(newProperties);
        this.outline.tableChanged(null);
    }

    public final void addPropertyColumn(String name, String displayName) {
        this.addPropertyColumn(name, displayName, null);
    }

    public final void addPropertyColumn(String name, String displayName, String description) {
        Parameters.notNull((CharSequence)"name", (Object)name);
        Parameters.notNull((CharSequence)"displayName", (Object)displayName);
        Node.Property[] p = this.rowModel.getProperties();
        Node.Property[] nue = new Node.Property[p.length + 1];
        System.arraycopy(p, 0, nue, 0, p.length);
        nue[p.length] = new PrototypeProperty(name, displayName, description);
        this.setProperties(nue, false);
    }

    public final boolean removePropertyColumn(String name) {
        Parameters.notNull((CharSequence)"name", (Object)name);
        Node.Property[] props = this.rowModel.getProperties();
        LinkedList<Node.Property> nue = new LinkedList<Node.Property>(Arrays.asList(props));
        boolean found = false;
        Iterator<Node.Property> i = nue.iterator();
        while (i.hasNext()) {
            Node.Property p = i.next();
            if (!name.equals(p.getName())) continue;
            found = true;
            i.remove();
            break;
        }
        if (found) {
            props = nue.toArray((T[])new Node.Property[props.length - 1]);
            this.setProperties(props, false);
        }
        return found;
    }

    public final void setPropertyColumnDescription(String columnName, String description) {
        Node.Property[] props;
        Parameters.notNull((CharSequence)"columnName", (Object)columnName);
        for (Node.Property p : props = this.rowModel.getProperties()) {
            if (!columnName.equals(p.getName())) continue;
            p.setShortDescription(description);
        }
        if (columnName.equals(this.model.getColumnName(0))) {
            this.outline.setNodesColumnDescription(description);
        }
    }

    public final /* varargs */ void setPropertyColumns(String ... namesAndDisplayNames) {
        if (namesAndDisplayNames.length % 2 != 0) {
            throw new IllegalArgumentException("Odd number of names and display names: " + Arrays.asList(namesAndDisplayNames));
        }
        Node.Property[] props = new Node.Property[namesAndDisplayNames.length / 2];
        for (int i = 0; i < namesAndDisplayNames.length; i += 2) {
            props[i / 2] = new PrototypeProperty(namesAndDisplayNames[i], namesAndDisplayNames[i + 1]);
        }
        this.setProperties(props, true);
    }

    public final void setPropertyColumnAttribute(String columnName, String attributeName, Object value) throws IllegalArgumentException {
        Node.Property[] props = this.rowModel.getProperties();
        boolean found = false;
        for (Node.Property p : props) {
            if (!columnName.equals(p.getName())) continue;
            p.setValue(attributeName, value);
            found = true;
        }
        if (!found) {
            throw new IllegalArgumentException("Unknown column " + columnName);
        }
    }

    public void setPopupAllowed(boolean value) {
        if (this.popupListener == null && value) {
            this.popupListener = new PopupAdapter();
            this.outline.addMouseListener((MouseListener)((Object)this.popupListener));
            this.addMouseListener((MouseListener)((Object)this.popupListener));
            return;
        }
        if (this.popupListener != null && !value) {
            this.outline.removeMouseListener((MouseListener)((Object)this.popupListener));
            this.removeMouseListener((MouseListener)((Object)this.popupListener));
            this.popupListener = null;
            return;
        }
    }

    public void setDefaultActionAllowed(boolean defaultActionAllowed) {
        this.outline.setDefaultActionAllowed(defaultActionAllowed);
    }

    public boolean isDefaultActionAllowed() {
        return this.outline.isDefaultActionAllowed();
    }

    public void setTreeSortable(boolean treeSortable) {
        this.outline.setTreeSortable(treeSortable);
    }

    public boolean isQuickSearchAllowed() {
        return this.quickSearch.isEnabled();
    }

    public void setQuickSearchAllowed(boolean allowedQuickSearch) {
        if (allowedQuickSearch == this.isQuickSearchAllowed()) {
            return;
        }
        this.quickSearch.setEnabled(allowedQuickSearch);
        if (allowedQuickSearch) {
            this.outline.addKeyListener(this.qsKeyListener);
        } else {
            this.outline.removeKeyListener(this.qsKeyListener);
        }
    }

    public void setQuickSearchTableFilter(QuickSearchTableFilter quickSearchTableFilter, boolean asynchronous) {
        TableQuickSearchSupport tqss = this.outline.createTableQuickSearchSupport(quickSearchTableFilter);
        this.attachQuickSearch(tqss, asynchronous, tqss.createSearchPopupMenu());
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.lookupExplorerManager();
        ViewUtil.adjustBackground((JComponent)((Object)this.outline));
        ViewUtil.adjustBackground(this.getViewport());
        if (this.selection != null) {
            this.selection.setTo(this.outline.getSelectionModel());
        }
    }

    public void readSettings(Properties p, String propertyPrefix) {
        this.outline.readSettings(p, propertyPrefix);
    }

    public void writeSettings(Properties p, String propertyPrefix) {
        this.outline.writeSettings(p, propertyPrefix);
    }

    public void setNodePopupFactory(NodePopupFactory newFactory) {
        this.popupFactory = newFactory;
    }

    public NodePopupFactory getNodePopupFactory() {
        return this.popupFactory;
    }

    private void lookupExplorerManager() {
        ExplorerManager newManager;
        if (this.managerListener == null) {
            this.managerListener = new TableSelectionListener();
        }
        if ((newManager = ExplorerManager.find(this)) != this.manager) {
            if (this.manager != null) {
                this.manager.removeVetoableChangeListener(this.wlvc);
                this.manager.removePropertyChangeListener(this.wlpc);
            }
            this.manager = newManager;
            this.wlvc = WeakListeners.vetoableChange((VetoableChangeListener)this.managerListener, (Object)this.manager);
            this.manager.addVetoableChangeListener(this.wlvc);
            this.wlpc = WeakListeners.propertyChange((PropertyChangeListener)this.managerListener, (Object)this.manager);
            this.manager.addPropertyChangeListener(this.wlpc);
        }
        this.outline.setExplorerManager(newManager);
        this.synchronizeRootContext();
        this.synchronizeSelectedNodes(true, new Node[0]);
        this.outline.getSelectionModel().removeListSelectionListener(this.managerListener);
        this.outline.getSelectionModel().addListSelectionListener(this.managerListener);
    }

    final void synchronizeRootContext() {
        if (null != this.treeModel) {
            this.treeModel.setNode(this.manager.getRootContext());
        }
    }

    final /* varargs */ void synchronizeSelectedNodes(boolean scroll, Node ... nodes) {
        JViewport v;
        if (!this.needToSynchronize()) {
            return;
        }
        this.expandSelection();
        this.outline.invalidate();
        this.invalidate();
        this.validate();
        Node[] arr = this.manager.getSelectedNodes();
        this.outline.getSelectionModel().clearSelection();
        int size = this.outline.getRowCount();
        int firstSelection = -1;
        for (int i = 0; i < size; ++i) {
            Node n = this.getNodeFromRow(i);
            for (int j = 0; j < arr.length; ++j) {
                if (n == null || !n.equals((Object)arr[j])) continue;
                this.outline.getSelectionModel().addSelectionInterval(i, i);
                if (firstSelection != -1) continue;
                firstSelection = i;
                int treeColumn = this.outline.convertColumnIndexToView(0);
                this.outline.getColumnModel().getSelectionModel().setSelectionInterval(treeColumn, treeColumn);
            }
        }
        if (scroll && firstSelection >= 0 && (v = this.getViewport()) != null) {
            Rectangle rect = this.outline.getCellRect(firstSelection, 0, true);
            int h = rect.height;
            int y = rect.y;
            int extent = v.getExtentSize().height;
            int ho = this.outline.getSize().height;
            if (extent < 3 * h) {
                if (extent > h) {
                    int pad = (extent - h) / 2;
                    if ((y -= pad) < 0) {
                        y = 0;
                    }
                    if (y + extent > ho) {
                        extent = ho - y;
                    }
                } else {
                    int pad = (h - extent) / 2;
                    y += pad;
                }
                rect.height = extent;
            } else {
                if (y > 0) {
                    if ((y -= h) < 0) {
                        rect.height += - y;
                        y = 0;
                    } else {
                        rect.height += h;
                    }
                }
                if (ho > 0 && y + rect.height < ho) {
                    rect.height += h;
                    if (y + rect.height > ho) {
                        rect.height = ho - y;
                    }
                }
            }
            rect.y = y;
            this.outline.scrollRectToVisible(rect);
        }
    }

    private boolean needToSynchronize() {
        boolean doSync = false;
        Node[] arr = this.manager.getSelectedNodes();
        if (this.outline.getSelectedRows().length != arr.length) {
            doSync = true;
        } else if (arr.length > 0) {
            List<Node> nodes = Arrays.asList(arr);
            for (int idx : this.outline.getSelectedRows()) {
                Node n = this.getNodeFromRow(idx);
                if (n != null && nodes.contains((Object)n)) continue;
                doSync = true;
                break;
            }
        }
        return doSync;
    }

    private void expandSelection() {
        Node[] arr = this.manager.getSelectedNodes();
        for (int i = 0; i < arr.length; ++i) {
            TreeNode tn;
            if (arr[i].getParentNode() == null && !this.outline.isRootVisible() || (tn = Visualizer.findVisualizer(arr[i])) == null) continue;
            ArrayList<TreeNode> al = new ArrayList<TreeNode>();
            while (tn != null) {
                al.add(tn);
                tn = tn.getParent();
            }
            Collections.reverse(al);
            TreePath tp = new TreePath(al.toArray());
            ArrayDeque<TreePath> pathsStack = new ArrayDeque<TreePath>(al.size());
            while (tp != null && tp.getPathCount() > 0) {
                if ((tp = tp.getParentPath()) == null) continue;
                pathsStack.addFirst(tp);
            }
            for (TreePath etp : pathsStack) {
                this.outline.expandPath(etp);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeNotify() {
        super.removeNotify();
        this.selection = new Selection(this.outline.getSelectionModel());
        this.outline.getSelectionModel().clearSelection();
        this.outline.getSelectionModel().removeListSelectionListener(this.managerListener);
        Object object = this.managerLock;
        synchronized (object) {
            if (this.manager != null) {
                this.manager.removePropertyChangeListener(this.wlpc);
                this.manager.removeVetoableChangeListener(this.wlvc);
                this.manager = null;
            }
        }
    }

    void showPopup(int xpos, int ypos, final JPopupMenu popup) {
        if (popup != null && popup.getSubElements().length > 0) {
            PopupMenuListener p = new PopupMenuListener(){

                @Override
                public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                }

                @Override
                public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                    popup.removePopupMenuListener(this);
                    OutlineView.this.outline.requestFocus();
                }

                @Override
                public void popupMenuCanceled(PopupMenuEvent e) {
                }
            };
            popup.addPopupMenuListener(p);
            popup.show(this, xpos, ypos);
        }
    }

    private JPopupMenu createPopup(Point p) {
        JPopupMenu popup;
        int[] selRows = this.outline.getSelectedRows();
        ArrayList<Node> al = new ArrayList<Node>(selRows.length);
        for (int i = 0; i < selRows.length; ++i) {
            Node n = this.getNodeFromRow(selRows[i]);
            if (n == null) continue;
            al.add(n);
        }
        Node[] arr = al.toArray((T[])new Node[al.size()]);
        if (arr.length == 0 && this.manager.getRootContext() != null && (popup = this.manager.getRootContext().getContextMenu()) != null && popup.getSubElements().length > 0) {
            this.popupFactory.addNoFilterItem((ETable)this.outline, popup);
            return popup;
        }
        p = SwingUtilities.convertPoint(this, p, (Component)((Object)this.outline));
        int column = this.outline.columnAtPoint(p);
        int row = this.outline.rowAtPoint(p);
        return this.popupFactory.createPopupMenu(row, column, arr, (Component)((Object)this.outline));
    }

    Node getNodeFromRow(int rowIndex) {
        int row = this.outline.convertRowIndexToModel(rowIndex);
        TreePath tp = this.outline.getLayoutCache().getPathForRow(row);
        if (tp == null) {
            return null;
        }
        return Visualizer.findNode(tp.getLastPathComponent());
    }

    private Point getPositionForPopup() {
        Rectangle rect;
        int i = this.outline.getSelectionModel().getLeadSelectionIndex();
        if (i < 0) {
            return null;
        }
        int j = this.outline.getColumnModel().getSelectionModel().getLeadSelectionIndex();
        if (j < 0) {
            j = 0;
        }
        if ((rect = this.outline.getCellRect(i, j, true)) == null) {
            return null;
        }
        Point p = new Point(rect.x + rect.width / 3, rect.y + rect.height / 2);
        p = SwingUtilities.convertPoint((Component)((Object)this.outline), p, this);
        return p;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void callSelectionChanged(Node[] nodes) {
        this.manager.removePropertyChangeListener(this.wlpc);
        this.manager.removeVetoableChangeListener(this.wlvc);
        try {
            this.manager.setSelectedNodes(nodes);
        }
        catch (PropertyVetoException e) {
            this.synchronizeSelectedNodes(false, new Node[0]);
        }
        finally {
            this.manager.removePropertyChangeListener(this.wlpc);
            this.manager.removeVetoableChangeListener(this.wlvc);
            this.manager.addPropertyChangeListener(this.wlpc);
            this.manager.addVetoableChangeListener(this.wlvc);
        }
    }

    private boolean isSelectionModeBroken(Node[] nodes) {
        if (nodes.length <= 1 || this.outline.getSelectionModel().getSelectionMode() == 2) {
            return false;
        }
        if (this.outline.getSelectionModel().getSelectionMode() == 0) {
            return true;
        }
        return false;
    }

    public boolean isDragSource() {
        return this.dragActive;
    }

    public void setDragSource(boolean state) {
        if (state && this.dragSupport == null) {
            this.dragSupport = new OutlineViewDragSupport(this, (JComponent)this.getOutline());
        }
        this.dragActive = state;
        if (this.dragSupport != null) {
            this.dragSupport.activate(this.dragActive);
        }
    }

    public boolean isDropTarget() {
        return this.dropActive;
    }

    public void setDropTarget(boolean state) {
        if (this.dropActive && this.dropSupport == null) {
            this.dropSupport = new OutlineViewDropSupport(this, (JTable)((Object)this.outline), this.dropTargetPopupAllowed);
        }
        this.dropActive = state;
        if (this.dropSupport != null) {
            this.dropSupport.activate(this.dropActive);
        }
    }

    public int getAllowedDragActions() {
        return this.allowedDragActions;
    }

    public void setAllowedDragActions(int actions) {
        this.allowedDragActions = actions;
    }

    public int getAllowedDropActions() {
        return this.allowedDropActions;
    }

    protected int getAllowedDropActions(Transferable t) {
        return this.getAllowedDropActions();
    }

    public void setAllowedDropActions(int actions) {
        this.allowedDropActions = actions;
    }

    public void addTreeExpansionListener(TreeExpansionListener l) {
        TreePathSupport tps = this.getOutline().getOutlineModel().getTreePathSupport();
        if (tps != null) {
            tps.addTreeExpansionListener(l);
        }
    }

    public void removeTreeExpansionListener(TreeExpansionListener l) {
        TreePathSupport tps = this.getOutline().getOutlineModel().getTreePathSupport();
        if (tps != null) {
            tps.removeTreeExpansionListener(l);
        }
    }

    public void collapseNode(Node n) {
        if (n == null) {
            throw new IllegalArgumentException();
        }
        TreePath treePath = new TreePath(this.treeModel.getPathToRoot(VisualizerNode.getVisualizer(null, n)));
        this.getOutline().collapsePath(treePath);
    }

    public void expandNode(Node n) {
        if (n == null) {
            throw new IllegalArgumentException();
        }
        this.lookupExplorerManager();
        TreePath treePath = new TreePath(this.treeModel.getPathToRoot(VisualizerNode.getVisualizer(null, n)));
        this.getOutline().expandPath(treePath);
    }

    public boolean isExpanded(Node n) {
        TreePath treePath = new TreePath(this.treeModel.getPathToRoot(VisualizerNode.getVisualizer(null, n)));
        return this.getOutline().isExpanded(treePath);
    }

    @Override
    public Insets getInsets() {
        Insets res = this.getInnerInsets();
        res = new Insets(res.top, res.left, res.bottom, res.right);
        if (null != this.searchPanel && this.searchPanel.isVisible()) {
            res.bottom += this.searchPanel.getPreferredSize().height;
        }
        return res;
    }

    private Insets getInnerInsets() {
        Insets res = super.getInsets();
        if (null == res) {
            res = new Insets(0, 0, 0, 0);
        }
        return res;
    }

    private boolean horizontalWheelScroll(MouseWheelEvent e) {
        JScrollBar toScroll = this.hScrollBar;
        if (null == toScroll || !toScroll.isVisible()) {
            return false;
        }
        if (e.getWheelRotation() == 0) {
            return false;
        }
        int direction = e.getWheelRotation() < 0 ? -1 : 1;
        JViewport vp = this.getViewport();
        if (vp == null) {
            return false;
        }
        int units = Math.abs(e.getUnitsToScroll());
        boolean limitScroll = Math.abs(e.getWheelRotation()) == 1;
        int limit = -1;
        JScrollBar scrollbar = toScroll;
        if (limitScroll) {
            limit = direction < 0 ? scrollbar.getValue() - scrollbar.getBlockIncrement(direction) : scrollbar.getValue() + scrollbar.getBlockIncrement(direction);
        }
        for (int i = 0; i < units; ++i) {
            int delta = direction > 0 ? scrollbar.getUnitIncrement(direction) : - scrollbar.getUnitIncrement(direction);
            int oldValue = scrollbar.getValue();
            int newValue = oldValue + delta;
            if (delta > 0 && newValue < oldValue) {
                newValue = scrollbar.getMaximum();
            } else if (delta < 0 && newValue > oldValue) {
                newValue = scrollbar.getMinimum();
            }
            if (oldValue == newValue) break;
            if (limitScroll && i > 0) {
                assert (limit != -1);
                if (direction < 0 && newValue < limit || direction > 0 && newValue > limit) break;
            }
            scrollbar.setValue(newValue);
        }
        return true;
    }

    private static class Selection {
        int selectionMode;
        int anchor;
        int lead;
        List<int[]> intervals = new ArrayList<int[]>();

        public Selection(ListSelectionModel sm) {
            this.selectionMode = sm.getSelectionMode();
            this.anchor = sm.getAnchorSelectionIndex();
            this.lead = sm.getLeadSelectionIndex();
            int min = sm.getMinSelectionIndex();
            int max = sm.getMaxSelectionIndex();
            int i1 = -1;
            for (int i = min; i <= max; ++i) {
                if (sm.isSelectedIndex(i)) {
                    if (i1 != -1) continue;
                    i1 = i;
                    continue;
                }
                if (i1 == -1) continue;
                this.intervals.add(new int[]{i1, i});
                i1 = -1;
            }
            if (i1 != -1) {
                this.intervals.add(new int[]{i1, max});
            }
        }

        public void setTo(ListSelectionModel sm) {
            sm.clearSelection();
            sm.setSelectionMode(this.selectionMode);
            for (int[] itv : this.intervals) {
                sm.addSelectionInterval(itv[0], itv[1]);
            }
            sm.setAnchorSelectionIndex(this.anchor);
            sm.setLeadSelectionIndex(this.lead);
        }
    }

    private static class OutlineScrollLayout
    extends ScrollPaneLayout.UIResource {
        private JScrollBar thsb;

        @Override
        public void addLayoutComponent(String s, Component c) {
            if (s.equals("TREE_HORIZONTAL_SCROLLBAR")) {
                this.thsb = (JScrollBar)this.addSingletonComponent(this.thsb, c);
            } else {
                super.addLayoutComponent(s, c);
            }
        }

        @Override
        public Dimension preferredLayoutSize(Container parent) {
            Dimension dim = super.preferredLayoutSize(parent);
            OutlineView ov = (OutlineView)parent;
            int thsbPolicy = ov.treeHorizontalScrollBarPolicy;
            if (this.thsb != null && thsbPolicy != 31) {
                if (thsbPolicy == 32) {
                    dim.height += this.thsb.getPreferredSize().height;
                } else {
                    Dimension extentSize = null;
                    Dimension viewSize = null;
                    Component view = null;
                    if (this.viewport != null) {
                        extentSize = this.viewport.getPreferredSize();
                        viewSize = this.viewport.getViewSize();
                        view = this.viewport.getView();
                    }
                    if (viewSize != null && extentSize != null) {
                        boolean canScroll = true;
                        if (view instanceof Scrollable) {
                            boolean bl = canScroll = !((Scrollable)((Object)view)).getScrollableTracksViewportWidth();
                        }
                        if (canScroll && viewSize.width > extentSize.width) {
                            dim.height += this.thsb.getPreferredSize().height;
                        }
                    }
                }
            }
            return dim;
        }

        @Override
        public Dimension minimumLayoutSize(Container parent) {
            return super.minimumLayoutSize(parent);
        }

        @Override
        public void layoutContainer(Container parent) {
            OutlineView ov = (OutlineView)parent;
            if (ov.isTreeHScrollBar && ov.outline.getColumnModel().getColumnCount() > 0) {
                boolean hsbNeeded;
                int column = ov.outline.convertColumnIndexToView(0);
                int extentWidth = ov.outline.getColumnModel().getColumn(column).getWidth();
                int maxWidth = ov.outline.getTreePreferredWidth();
                boolean hsbvisible = this.thsb.isVisible();
                boolean hideHsb = maxWidth <= extentWidth && ov.treeHorizontalScrollBarPolicy != 32;
                JScrollBar hsbOrig = this.hsb;
                if (!hideHsb) {
                    Border viewportBorder;
                    Component view = this.viewport != null ? this.viewport.getView() : null;
                    boolean viewTracksViewportWidth = false;
                    if (view instanceof Scrollable) {
                        Scrollable sv = (Scrollable)((Object)view);
                        viewTracksViewportWidth = sv.getScrollableTracksViewportWidth();
                    }
                    Dimension viewPrefSize = view != null ? view.getPreferredSize() : new Dimension(0, 0);
                    Rectangle availR = ov.getBounds();
                    Insets insets = parent.getInsets();
                    availR.width -= insets.left + insets.right;
                    if (this.rowHead != null && this.rowHead.isVisible()) {
                        int rowHeadWidth = Math.min(availR.width, this.rowHead.getPreferredSize().width);
                        availR.width -= rowHeadWidth;
                    }
                    if ((viewportBorder = ov.getViewportBorder()) != null) {
                        Insets vpbInsets = viewportBorder.getBorderInsets(parent);
                        availR.width -= vpbInsets.left + vpbInsets.right;
                    }
                    if (availR.width < 0) {
                        boolean bl = false;
                    }
                    if (ov.getHorizontalScrollBarPolicy() == 32) {
                        hsbNeeded = true;
                    } else if (ov.getHorizontalScrollBarPolicy() == 31) {
                        hsbNeeded = false;
                    } else {
                        Dimension extentSize = this.viewport != null ? this.viewport.toViewCoordinates(availR.getSize()) : new Dimension(0, 0);
                        boolean bl = hsbNeeded = !viewTracksViewportWidth && viewPrefSize.width > extentSize.width;
                    }
                    if (hsbNeeded) {
                        this.hsb = this.createFakeHSB(hsbOrig);
                    } else {
                        ov.sayHorizontalScrollBarNeeded(true);
                    }
                } else {
                    hsbNeeded = false;
                }
                super.layoutContainer(parent);
                if (!hideHsb) {
                    if (hsbNeeded) {
                        JScrollBar fakeHsb = this.hsb;
                        fakeHsb.setVisible(false);
                        this.hsb = hsbOrig;
                        Rectangle r = fakeHsb.getBounds();
                        r.height /= 2;
                        r.y += r.height;
                        this.hsb.setBounds(r);
                        this.hsb.setVisible(true);
                    } else {
                        ov.sayHorizontalScrollBarNeeded(false);
                        this.hsbPolicy = ov.getHorizontalScrollBarPolicy();
                        this.hsb.setVisible(false);
                    }
                }
                if (hideHsb) {
                    this.thsb.setVisible(false);
                    ov.hScrollBar.setValues(0, 0, 0, 0);
                } else {
                    Rectangle vr = this.viewport.getBounds();
                    Rectangle r = new Rectangle(this.getColumnXPos(ov.outline, column), vr.y + vr.height, extentWidth, this.thsb.getPreferredSize().height);
                    this.thsb.setBounds(r);
                    if (!hsbvisible) {
                        this.thsb.setVisible(true);
                        ov.listener.revalidateScrollBar();
                    }
                }
            } else {
                super.layoutContainer(parent);
            }
            Component searchPanel = ov.searchPanel;
            if (null != searchPanel && searchPanel.isVisible()) {
                Insets innerInsets = ov.getInnerInsets();
                Dimension prefSize = searchPanel.getPreferredSize();
                searchPanel.setBounds(innerInsets.left, parent.getHeight() - innerInsets.bottom - prefSize.height, parent.getWidth() - innerInsets.left - innerInsets.right, prefSize.height);
            }
        }

        private JScrollBar createFakeHSB(final JScrollBar hsb) {
            return new JScrollBar(0){

                @Override
                public Dimension getPreferredSize() {
                    Dimension dim = hsb.getPreferredSize();
                    return new Dimension(dim.width, 2 * dim.height);
                }
            };
        }

        private int getColumnXPos(OutlineViewOutline outline, int column) {
            if (column < 0) {
                if (!outline.getComponentOrientation().isLeftToRight()) {
                    return outline.getWidth();
                }
                return 0;
            }
            if (column >= outline.getColumnCount()) {
                if (outline.getComponentOrientation().isLeftToRight()) {
                    return outline.getWidth();
                }
                return 0;
            }
            TableColumnModel cm = outline.getColumnModel();
            int x = 0;
            if (outline.getComponentOrientation().isLeftToRight()) {
                for (int i = 0; i < column; ++i) {
                    x += cm.getColumn(i).getWidth();
                }
            } else {
                for (int i = cm.getColumnCount() - 1; i > column; --i) {
                    x += cm.getColumn(i).getWidth();
                }
            }
            return x;
        }

    }

    private final class ScrollListener
    extends ComponentAdapter
    implements ChangeListener,
    TableColumnModelListener,
    AdjustmentListener {
        ScrollListener() {
        }

        @Override
        public void stateChanged(ChangeEvent evt) {
            if (evt.getSource() == OutlineView.this.hScrollBar.getModel()) {
                int value = OutlineView.this.hScrollBar.getModel().getValue();
                OutlineView.this.outline.setTreePositionX(value);
            }
        }

        private void revalidateScrollBar() {
            if (!OutlineView.this.isDisplayable()) {
                return;
            }
            if (!OutlineView.this.isTreeHScrollBar) {
                return;
            }
            if (OutlineView.this.outline.getColumnModel().getColumnCount() > 0) {
                int column = OutlineView.this.outline.convertColumnIndexToView(0);
                int extentWidth = OutlineView.this.outline.getColumnModel().getColumn(column).getWidth();
                int maxWidth = OutlineView.this.outline.getTreePreferredWidth();
                int positionX = OutlineView.this.outline.getTreePositionX();
                int value = Math.max(0, Math.min(positionX, maxWidth - extentWidth));
                OutlineView.this.hScrollBar.setValues(value, extentWidth, 0, maxWidth);
                OutlineView.this.hScrollBar.setBlockIncrement(extentWidth);
            }
        }

        @Override
        public void columnAdded(TableColumnModelEvent e) {
        }

        @Override
        public void columnRemoved(TableColumnModelEvent e) {
        }

        @Override
        public void columnMoved(TableColumnModelEvent e) {
            OutlineView.this.revalidate();
        }

        @Override
        public void columnMarginChanged(ChangeEvent e) {
            this.revalidateScrollBar();
        }

        @Override
        public void columnSelectionChanged(ListSelectionEvent e) {
        }

        @Override
        public void adjustmentValueChanged(AdjustmentEvent e) {
            int value = OutlineView.this.hScrollBar.getModel().getValue();
            OutlineView.this.outline.setTreePositionX(value);
        }
    }

    static final class PrototypeProperty
    extends PropertySupport.ReadWrite<Object> {
        PrototypeProperty(String name, String displayName) {
            this(name, displayName, null);
        }

        PrototypeProperty(String name, String displayName, String description) {
            super(name, Object.class, displayName, description);
        }

        public Object getValue() throws IllegalAccessException, InvocationTargetException {
            throw new AssertionError();
        }

        public void setValue(Object val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            throw new AssertionError();
        }

        public boolean equals(Object o) {
            return o != null && o instanceof Node.Property && this.getName().equals(((Node.Property)o).getName());
        }

        public int hashCode() {
            return this.getName().hashCode();
        }
    }

    private static class NodeOutlineModel
    extends DefaultOutlineModel {
        public NodeOutlineModel(NodeTreeModel treeModel, RowModel rowModel, boolean largeModel, String nodesColumnLabel) {
            super((TreeModel)treeModel, rowModel, largeModel, nodesColumnLabel);
        }

        public boolean isCellEditable(int rowIndex, int columnIndex) {
            if (columnIndex == 0) {
                Node treeNode = this.getNodeAt(rowIndex);
                return null != treeNode && treeNode.canRename();
            }
            return super.isCellEditable(rowIndex, columnIndex);
        }

        protected void setTreeValueAt(Object aValue, int rowIndex) {
            Node n = this.getNodeAt(rowIndex);
            if (null != n) {
                ViewUtil.nodeRename(n, aValue == null ? "" : aValue.toString());
            }
        }

        protected final Node getNodeAt(int rowIndex) {
            Node result = null;
            TreePath path = this.getLayout().getPathForRow(rowIndex);
            if (path != null) {
                result = Visualizer.findNode(path.getLastPathComponent());
            }
            return result;
        }
    }

    private static class OutlinePopupFactory
    extends NodePopupFactory {
        @Override
        public JPopupMenu createPopupMenu(int row, int column, Node[] selectedNodes, Component component) {
            if (component instanceof ETable) {
                ETable et = (ETable)component;
                int modelRowIndex = et.convertColumnIndexToModel(column);
                this.setShowQuickFilter(modelRowIndex != 0);
            }
            return super.createPopupMenu(row, column, selectedNodes, component);
        }
    }

    static class OutlineViewOutline
    extends Outline {
        private final PropertiesRowModel rowModel;
        private static final String COLUMNS_SELECTOR_HINT = "ColumnsSelectorHint";
        private static final String COPY_ACTION_DELEGATE = "Outline Copy Action Delegate ";
        private boolean treeSortable = true;
        private boolean isHScrollingEnabled;
        private JScrollBar hScrollBar;
        private TMScrollingListener tmScrollingListener;
        private int treePositionX = 0;
        private int[] rowWidths;
        private RequestProcessor.Task changeTask;
        private boolean defaultActionAllowed = true;
        private String nodesColumnDescription;
        private ExplorerManager manager;
        private TableQuickSearchSupport.QuickSearchSettings qss = new TableQuickSearchSupport.QuickSearchSettings();

        public OutlineViewOutline(OutlineModel mdl, PropertiesRowModel rowModel) {
            super(mdl);
            this.rowModel = rowModel;
            this.setSelectVisibleColumnsLabel(NbBundle.getMessage(OutlineView.class, (String)"CTL_ColumnsSelector"));
            this.removeDefaultCutCopyPaste(this.getInputMap(0));
            this.removeDefaultCutCopyPaste(this.getInputMap(1));
            KeyStroke ctrlSpace = Utilities.isMac() ? KeyStroke.getKeyStroke(32, 4, false) : KeyStroke.getKeyStroke(32, 128, false);
            Object ctrlSpaceActionBind = this.getInputMap(1).get(ctrlSpace);
            if (ctrlSpaceActionBind != null) {
                this.getInputMap(1).put(ctrlSpace, "invokeCustomEditor");
                InvokeCustomEditorAction invokeCustomEditorAction = new InvokeCustomEditorAction(ctrlSpaceActionBind);
                this.getActionMap().put("invokeCustomEditor", invokeCustomEditorAction);
            }
        }

        private void removeDefaultCutCopyPaste(InputMap map) {
            this.putActionDelegate(map, KeyStroke.getKeyStroke("control C"));
            map.put(KeyStroke.getKeyStroke("control V"), "none");
            map.put(KeyStroke.getKeyStroke("control X"), "none");
            this.putActionDelegate(map, KeyStroke.getKeyStroke("COPY"));
            map.put(KeyStroke.getKeyStroke("PASTE"), "none");
            map.put(KeyStroke.getKeyStroke("CUT"), "none");
            if (Utilities.isMac()) {
                this.putActionDelegate(map, KeyStroke.getKeyStroke(67, 4));
                map.put(KeyStroke.getKeyStroke(88, 4), "none");
                map.put(KeyStroke.getKeyStroke(86, 4), "none");
            } else {
                this.putActionDelegate(map, KeyStroke.getKeyStroke(67, 128));
                map.put(KeyStroke.getKeyStroke(88, 128), "none");
                map.put(KeyStroke.getKeyStroke(86, 128), "none");
            }
        }

        private void putActionDelegate(InputMap map, KeyStroke ks) {
            String binding = "Outline Copy Action Delegate " + ks.toString();
            Action action = this.getCopyActionDelegate(map, ks);
            if (action != null) {
                this.getActionMap().put(binding, action);
                map.put(ks, binding);
            }
        }

        private Action getCopyActionDelegate(InputMap map, KeyStroke ks) {
            ActionMap am = this.getActionMap();
            if (map != null && am != null && this.isEnabled()) {
                Action action;
                Object binding = map.get(ks);
                Action action2 = action = binding == null ? null : am.get(binding);
                if (action != null) {
                    return new CopyToClipboardAction(action);
                }
            }
            return null;
        }

        public void setColumnModel(TableColumnModel columnModel) {
            int cc;
            super.setColumnModel(columnModel);
            if (this.rowModel == null) {
                return;
            }
            List<TableColumn> columns = null;
            if (columnModel instanceof ETableColumnModel) {
                columns = OutlineViewOutline.getAllColumns((ETableColumnModel)columnModel);
            }
            if (columns == null) {
                columns = Collections.list(columnModel.getColumns());
            }
            if ((cc = columns.size()) > 0) {
                String nodesName = this.getNodesColumnName();
                Node.Property[] origProperties = this.rowModel.getProperties();
                Node.Property[] newProperties = new Node.Property[cc - 1];
                int foundOrigProperties = 0;
                int columnIndex = 0;
                for (TableColumn column : columns) {
                    String name = column.getHeaderValue().toString();
                    Node.Property origProperty = this.findProperty(origProperties, name);
                    foundOrigProperties += origProperty == null ? 0 : 1;
                    if (nodesName == null && columnIndex == 0 || nodesName != null && nodesName.equals(name)) continue;
                    newProperties[columnIndex++] = origProperty == null ? new PrototypeProperty(name, name) : origProperty;
                }
                if (foundOrigProperties != cc - 1) {
                    this.rowModel.setProperties(newProperties);
                }
            } else {
                this.rowModel.setProperties(new Node.Property[0]);
            }
        }

        private Node.Property findProperty(Node.Property[] props, String displayName) {
            if (props != null) {
                for (Node.Property property : props) {
                    if (property == null || property.getDisplayName() == null || !property.getDisplayName().equals(displayName)) continue;
                    return property;
                }
            }
            return null;
        }

        private String getNodesColumnName() {
            OutlineModel outlineModel = this.getOutlineModel();
            if (outlineModel != null && outlineModel.getColumnCount() > 0) {
                return outlineModel.getColumnName(0);
            }
            return null;
        }

        private static List<TableColumn> getAllColumns(ETableColumnModel etcm) {
            try {
                Method getAllColumnsMethod = ETableColumnModel.class.getDeclaredMethod("getAllColumns", new Class[0]);
                getAllColumnsMethod.setAccessible(true);
                return (List)getAllColumnsMethod.invoke((Object)etcm, new Object[0]);
            }
            catch (Exception ex) {
                Exceptions.printStackTrace((Throwable)ex);
                return null;
            }
        }

        public void readSettings(Properties p, String propertyPrefix) {
            super.readSettings(p, propertyPrefix);
            int numColumns = this.getColumnModel().getColumnCount();
            for (int i = 0; i < numColumns; ++i) {
                OutlineViewOutlineColumn ovoc = (OutlineViewOutlineColumn)((Object)this.getColumnModel().getColumn(i));
                ovoc.postReadSettings(p, i, propertyPrefix);
            }
        }

        private TableQuickSearchSupport createDefaultTableQuickSearchSupport() {
            return new TableQuickSearchSupport((JTable)((Object)this), new DefaultQuickSearchTableFilter(), this.qss);
        }

        private TableQuickSearchSupport createTableQuickSearchSupport(QuickSearchTableFilter quickSearchTableFilter) {
            return new TableQuickSearchSupport((JTable)((Object)this), quickSearchTableFilter, this.qss);
        }

        private void setExplorerManager(ExplorerManager manager) {
            this.manager = manager;
        }

        PropertiesRowModel getRowModel() {
            return this.rowModel;
        }

        public Object transformValue(Object value) {
            if (value instanceof OutlineViewOutlineColumn) {
                String desc;
                OutlineViewOutlineColumn c = (OutlineViewOutlineColumn)((Object)value);
                String dn = c.getRawColumnName();
                if (dn == null) {
                    dn = c.getHeaderValue().toString();
                }
                if ((desc = c.getShortDescription(null)) == null) {
                    return dn;
                }
                return NbBundle.getMessage(OutlineView.class, (String)"OutlineViewOutline_NameAndDesc", (Object)dn, (Object)desc);
            }
            if ("ColumnsSelectorHint".equals(value)) {
                return NbBundle.getMessage(OutlineView.class, (String)"ColumnsSelectorHint");
            }
            if (value instanceof AbstractButton) {
                AbstractButton b = (AbstractButton)value;
                Mnemonics.setLocalizedText((AbstractButton)b, (String)b.getText());
                return b;
            }
            if (value instanceof VisualizerNode) {
                return Visualizer.findNode(value);
            }
            return PropertiesRowModel.getValueFromProperty(value);
        }

        public String convertValueToString(Object value) {
            if (value instanceof VisualizerNode) {
                return ((VisualizerNode)value).getDisplayName();
            }
            if (value instanceof Node) {
                return ((Node)value).getDisplayName();
            }
            return super.convertValueToString(value);
        }

        void setTreeHScrollingEnabled(boolean isHScrollingEnabled, JScrollBar hScrollBar) {
            this.isHScrollingEnabled = isHScrollingEnabled;
            this.hScrollBar = hScrollBar;
            if (isHScrollingEnabled) {
                if (this.tmScrollingListener == null) {
                    this.tmScrollingListener = new TMScrollingListener();
                    this.rowWidths = new int[this.getOutlineModel().getRowCount()];
                    this.getOutlineModel().addTableModelListener((TableModelListener)this.tmScrollingListener);
                    this.getOutlineModel().addTreeModelListener((TreeModelListener)this.tmScrollingListener);
                }
            } else if (this.tmScrollingListener != null) {
                this.getOutlineModel().removeTableModelListener((TableModelListener)this.tmScrollingListener);
                this.getOutlineModel().removeTreeModelListener((TreeModelListener)this.tmScrollingListener);
                this.tmScrollingListener = null;
                this.rowWidths = null;
            }
        }

        void setTreeWidthChangeTask(RequestProcessor.Task changeTask) {
            this.changeTask = changeTask;
        }

        int getTreePositionX() {
            return this.treePositionX;
        }

        void setTreePositionX(int treePositionX) {
            if (treePositionX == this.treePositionX) {
                return;
            }
            this.treePositionX = treePositionX;
            this.tableChanged(new TableModelEvent(this.getModel(), 0, this.getRowCount(), 0));
        }

        private void setPreferredTreeWidth(int row, int width) {
            if (this.isHScrollingEnabled) {
                if (row >= this.rowWidths.length) {
                    this.rowWidths = Arrays.copyOf(this.rowWidths, row + 1);
                }
                if (this.rowWidths[row] != width) {
                    this.rowWidths[row] = width;
                    this.changeTask.schedule(100);
                }
            }
        }

        int getTreePreferredWidth() {
            int r2;
            Rectangle visibleRect = this.getVisibleRect();
            int r1 = this.rowAtPoint(new Point(0, visibleRect.y));
            if (r1 < 0) {
                return 0;
            }
            if (this.hScrollBar.isVisible()) {
                visibleRect.height += this.hScrollBar.getSize().height;
            }
            if ((r2 = this.rowAtPoint(new Point(0, visibleRect.y + visibleRect.height))) < 0) {
                r2 = this.getRowCount() - 1;
            }
            if (r2 >= this.rowWidths.length) {
                r2 = this.rowWidths.length - 1;
            }
            int width = 0;
            for (int r = r1; r <= r2; ++r) {
                if (this.rowWidths[r] <= width) continue;
                width = this.rowWidths[r];
            }
            return width += 2 * this.getIntercellSpacing().width;
        }

        void setDefaultActionAllowed(boolean defaultActionAllowed) {
            this.defaultActionAllowed = defaultActionAllowed;
        }

        boolean isDefaultActionAllowed() {
            return this.defaultActionAllowed;
        }

        public TableCellRenderer getCellRenderer(int row, int column) {
            int c;
            TableCellRenderer result = super.getCellRenderer(row, column);
            if (this.isHScrollingEnabled && (c = this.convertColumnIndexToModel(column)) == 0) {
                result = new TranslatedTableCellRenderer(this, result);
            }
            return result;
        }

        public boolean editCellAt(int row, int column, EventObject e) {
            Node.Property p;
            boolean isTreeColumn;
            Object o = this.getValueAt(row, column);
            if (o instanceof Node.Property && ((p = (Node.Property)o).getValueType() == Boolean.class || p.getValueType() == Boolean.TYPE)) {
                PropertiesRowModel.toggleBooleanProperty(p);
                Rectangle r = this.getCellRect(row, column, true);
                this.repaint(r.x, r.y, r.width, r.height);
                return false;
            }
            boolean res = false;
            boolean actionPerformed = false;
            boolean bl = isTreeColumn = this.convertColumnIndexToModel(column) == 0;
            if (isTreeColumn && row != -1 && e instanceof MouseEvent && SwingUtilities.isLeftMouseButton((MouseEvent)e)) {
                Node node;
                Action a;
                if (this.checkAt(row, column, (MouseEvent)e)) {
                    return false;
                }
                int clickCount = ((MouseEvent)e).getClickCount();
                if (clickCount > 1 && (node = Visualizer.findNode(o)) != null && this.defaultActionAllowed && (a = TreeView.takeAction(node.getPreferredAction(), node)) != null) {
                    if (a.isEnabled()) {
                        a.actionPerformed(new ActionEvent((Object)node, 1001, "", ((MouseEvent)e).getModifiers()));
                        return false;
                    }
                    Logger.getLogger(OutlineView.class.getName()).info("Action " + a + " on node " + (Object)node + " is disabled");
                }
            }
            if (e instanceof MouseEvent) {
                Action act;
                Node.Property p2;
                PropertyPanel panel;
                PropertyEditor ed;
                final Rectangle r = this.getCellRect(row, column, true);
                MouseEvent me = (MouseEvent)e;
                me.translatePoint(this.treePositionX, 0);
                int x = me.getX();
                if (x > r.x + r.width - 24 && x < r.x + r.width && o instanceof Node.Property && !isTreeColumn && !Boolean.TRUE.equals((p2 = (Node.Property)o).getValue("suppressCustomEditor")) && (ed = (panel = new PropertyPanel(p2)).getPropertyEditor()) != null && ed.supportsCustomEditor() && (act = panel.getActionMap().get("invokeCustomEditor")) != null) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            r.x = 0;
                            r.width = OutlineViewOutline.this.getWidth();
                            OutlineViewOutline.this.repaint(r);
                        }
                    });
                    act.actionPerformed(null);
                    return false;
                }
            }
            return super.editCellAt(row, column, e);
        }

        protected void processComponentKeyEvent(KeyEvent e) {
            Component editorComponent = this.getEditorComponent();
            if (editorComponent != null && editorComponent.isFocusable()) {
                editorComponent.requestFocusInWindow();
                Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(e);
            } else {
                super.processComponentKeyEvent(e);
            }
        }

        protected TableColumn createColumn(int modelIndex) {
            return new OutlineViewOutlineColumn(modelIndex);
        }

        boolean isTreeSortable() {
            return this.treeSortable;
        }

        void setTreeSortable(boolean treeSortable) {
            this.treeSortable = treeSortable;
        }

        private void setNodesColumnDescription(String description) {
            this.nodesColumnDescription = description;
        }

        private class TMScrollingListener
        implements TableModelListener,
        TreeModelListener {
            private TMScrollingListener() {
            }

            @Override
            public void tableChanged(TableModelEvent e) {
                this.updateRowWidths();
            }

            @Override
            public void treeNodesChanged(TreeModelEvent e) {
                this.updateRowWidths();
            }

            @Override
            public void treeNodesInserted(TreeModelEvent e) {
                this.updateRowWidths();
            }

            @Override
            public void treeNodesRemoved(TreeModelEvent e) {
                this.updateRowWidths();
            }

            @Override
            public void treeStructureChanged(TreeModelEvent e) {
                this.updateRowWidths();
            }

            private void updateRowWidths() {
                int rowCount = OutlineViewOutline.this.getOutlineModel().getRowCount();
                if (rowCount != OutlineViewOutline.this.rowWidths.length) {
                    OutlineViewOutline.this.rowWidths = Arrays.copyOf(OutlineViewOutline.this.rowWidths, rowCount);
                }
            }
        }

        private class TranslatedTableCellRenderer
        extends JComponent
        implements TableCellRenderer {
            private OutlineViewOutline outline;
            private TableCellRenderer delegate;
            private Component component;

            public TranslatedTableCellRenderer(OutlineViewOutline outline, TableCellRenderer delegate) {
                this.outline = outline;
                this.delegate = delegate;
            }

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                this.component = this.delegate.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                OutlineViewOutline.this.setPreferredTreeWidth(row, this.component.getPreferredSize().width);
                return this;
            }

            @Override
            public void setBounds(int x, int y, int width, int height) {
                this.component.setBounds(x, y, Math.max(width, this.outline.getTreePreferredWidth()), height);
            }

            @Override
            public Dimension getPreferredSize() {
                return this.component.getPreferredSize();
            }

            @Override
            public void paint(Graphics g) {
                if (!(this.component instanceof TranslatedTableCellRenderer)) {
                    g.translate(- this.outline.getTreePositionX(), 0);
                }
                try {
                    this.component.paint(g);
                }
                catch (NullPointerException npe) {
                    Border border = null;
                    Exceptions.printStackTrace((Throwable)Exceptions.attachMessage((Throwable)npe, (String)("Failed painting of component " + this.component + " with border " + (this.component instanceof JComponent ? (border = ((JComponent)this.component).getBorder()) : null) + (border instanceof CompoundBorder ? new StringBuilder().append(", with outsideBorder = ").append(((CompoundBorder)border).getOutsideBorder()).append(" and insideBorder = ").append(((CompoundBorder)border).getInsideBorder()).toString() : ""))));
                }
            }

            @Override
            public String getToolTipText() {
                if (this.component instanceof JComponent) {
                    return ((JComponent)this.component).getToolTipText();
                }
                return super.getToolTipText();
            }

            @Override
            public Point getToolTipLocation(MouseEvent event) {
                if (this.component instanceof JComponent) {
                    return ((JComponent)this.component).getToolTipLocation(event);
                }
                return super.getToolTipLocation(event);
            }

            @Override
            public String getToolTipText(MouseEvent event) {
                if (this.component instanceof JComponent) {
                    return ((JComponent)this.component).getToolTipText(event);
                }
                return super.getToolTipText(event);
            }

            @Override
            public JToolTip createToolTip() {
                if (this.component instanceof JComponent) {
                    return ((JComponent)this.component).createToolTip();
                }
                return super.createToolTip();
            }
        }

        class OutlineViewOutlineColumn
        extends Outline.OutlineColumn {
            private static final String PROP_PREFIX = "OutlineViewOutlineColumn-";
            private static final String PROP_SHORT_DESCRIPTION = "shortDescription";
            private String tooltip;
            private final Comparator originalNodeComparator;

            public OutlineViewOutlineColumn(int index) {
                super((Outline)OutlineViewOutline.this, index);
                this.originalNodeComparator = new NodeNestedComparator();
            }

            public boolean isSortingAllowed() {
                int index = this.getModelIndex();
                if (index <= 0) {
                    return OutlineViewOutline.this.isTreeSortable();
                }
                Object sortable = OutlineViewOutline.this.rowModel.getPropertyValue("SortableColumn", index - 1);
                if (sortable != null) {
                    return Boolean.TRUE.equals(sortable);
                }
                return super.isSortingAllowed();
            }

            public Comparator getNestedComparator() {
                if (this.getModelIndex() == 0 && super.getNestedComparator() == null) {
                    return this.originalNodeComparator;
                }
                return super.getNestedComparator();
            }

            protected TableCellRenderer createDefaultHeaderRenderer() {
                TableCellRenderer orig = super.createDefaultHeaderRenderer();
                OutlineViewOutlineHeaderRenderer ovohr = new OutlineViewOutlineHeaderRenderer(orig);
                return ovohr;
            }

            public String getShortDescription(String defaultValue) {
                TableModel model = OutlineViewOutline.this.getModel();
                if (model.getRowCount() <= 0) {
                    return null;
                }
                if (this.getModelIndex() == 0) {
                    if (OutlineViewOutline.this.nodesColumnDescription != null) {
                        return OutlineViewOutline.this.nodesColumnDescription;
                    }
                    return defaultValue;
                }
                return OutlineViewOutline.this.rowModel.getShortDescription(this.getModelIndex() - 1);
            }

            public String getRawColumnName() {
                TableModel model = OutlineViewOutline.this.getModel();
                if (model.getRowCount() <= 0) {
                    return null;
                }
                if (this.getModelIndex() == 0) {
                    return null;
                }
                return OutlineViewOutline.this.rowModel.getRawColumnName(this.getModelIndex() - 1);
            }

            void postReadSettings(Properties p, int index, String propertyPrefix) {
                String myPrefix;
                String shortDescription;
                TableModel model = OutlineViewOutline.this.getModel();
                if (model.getRowCount() > 0 && this.getModelIndex() > 0 && (shortDescription = p.getProperty((myPrefix = new StringBuilder().append(propertyPrefix).append("OutlineViewOutlineColumn-").append(Integer.toString(index)).append("-").toString()) + "shortDescription", null)) != null) {
                    OutlineViewOutline.this.rowModel.setShortDescription(this.getModelIndex() - 1, shortDescription);
                }
            }

            public void writeSettings(Properties p, int index, String propertyPrefix) {
                super.writeSettings(p, index, propertyPrefix);
                TableModel model = OutlineViewOutline.this.getModel();
                if (model.getRowCount() > 0 && this.getModelIndex() > 0) {
                    String shortDescription = OutlineViewOutline.this.rowModel.getShortDescription(this.getModelIndex() - 1);
                    String myPrefix = propertyPrefix + "OutlineViewOutlineColumn-" + Integer.toString(index) + "-";
                    p.setProperty(myPrefix + "shortDescription", shortDescription);
                }
            }

            private class NodeNestedComparator
            implements Comparator {
                private NodeNestedComparator() {
                }

                public int compare(Object o1, Object o2) {
                    assert (o1 instanceof Node);
                    assert (o2 instanceof Node);
                    return ((Node)o1).getDisplayName().compareTo(((Node)o2).getDisplayName());
                }
            }

            class OutlineViewOutlineHeaderRenderer
            implements TableCellRenderer {
                private TableCellRenderer orig;

                public OutlineViewOutlineHeaderRenderer(TableCellRenderer delegate) {
                    this.orig = delegate;
                }

                @Override
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                    Component oc = this.orig.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                    if (OutlineViewOutlineColumn.this.tooltip == null) {
                        OutlineViewOutlineColumn.this.tooltip = OutlineViewOutlineColumn.this.getShortDescription(value.toString());
                    }
                    if (OutlineViewOutlineColumn.this.tooltip != null && oc instanceof JComponent) {
                        JComponent jc = (JComponent)oc;
                        jc.setToolTipText(OutlineViewOutlineColumn.this.tooltip);
                    }
                    return oc;
                }
            }

        }

        private class InvokeCustomEditorAction
        implements Action {
            private Object delegateActionBind;

            InvokeCustomEditorAction(Object delegateActionBind) {
                this.delegateActionBind = delegateActionBind;
            }

            @Override
            public Object getValue(String key) {
                return null;
            }

            @Override
            public void putValue(String key, Object value) {
            }

            @Override
            public void setEnabled(boolean b) {
            }

            @Override
            public boolean isEnabled() {
                return true;
            }

            @Override
            public void addPropertyChangeListener(PropertyChangeListener listener) {
            }

            @Override
            public void removePropertyChangeListener(PropertyChangeListener listener) {
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                Action a;
                if (!this.openCustomEditor(e) && (a = OutlineViewOutline.this.getActionMap().get(this.delegateActionBind)) != null && a.isEnabled()) {
                    a.actionPerformed(e);
                }
            }

            private boolean openCustomEditor(ActionEvent e) {
                PropertyEditor ed;
                Action act;
                PropertyPanel panel;
                if (OutlineViewOutline.this.getSelectedRowCount() != 1 || OutlineViewOutline.this.getSelectedColumnCount() != 1) {
                    return false;
                }
                int row = OutlineViewOutline.this.getSelectedRow();
                if (row < 0) {
                    return false;
                }
                int column = OutlineViewOutline.this.getSelectedColumn();
                if (column < 0) {
                    return false;
                }
                Object o = OutlineViewOutline.this.getValueAt(row, column);
                if (!(o instanceof Node.Property)) {
                    return false;
                }
                Node.Property p = (Node.Property)o;
                if (!Boolean.TRUE.equals(p.getValue("suppressCustomEditor")) && (ed = (panel = new PropertyPanel(p)).getPropertyEditor()) != null && ed.supportsCustomEditor() && (act = panel.getActionMap().get("invokeCustomEditor")) != null) {
                    act.actionPerformed(null);
                    return true;
                }
                return false;
            }
        }

        private static class TextAddedTransferable
        implements Transferable {
            private Transferable trans;
            private Transferable ss;
            private DataFlavor[] dataFlavors;

            public TextAddedTransferable(Transferable trans, Transferable ss) {
                this.trans = trans;
                this.ss = ss;
            }

            @Override
            public synchronized DataFlavor[] getTransferDataFlavors() {
                if (this.dataFlavors == null) {
                    DataFlavor[] tf = this.trans.getTransferDataFlavors();
                    DataFlavor[] sf = this.ss.getTransferDataFlavors();
                    ArrayList<DataFlavor> dfs = new ArrayList<DataFlavor>(tf.length + sf.length);
                    for (DataFlavor df2 : tf) {
                        dfs.add(df2);
                    }
                    for (DataFlavor df2 : sf) {
                        if (dfs.contains(df2)) continue;
                        dfs.add(df2);
                    }
                    this.dataFlavors = dfs.toArray(new DataFlavor[0]);
                }
                return this.dataFlavors;
            }

            @Override
            public boolean isDataFlavorSupported(DataFlavor flavor) {
                if (this.trans.isDataFlavorSupported(flavor)) {
                    return true;
                }
                return this.ss.isDataFlavorSupported(flavor);
            }

            @Override
            public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
                if (this.trans.isDataFlavorSupported(flavor)) {
                    return this.trans.getTransferData(flavor);
                }
                return this.ss.getTransferData(flavor);
            }
        }

        private class OutlineTransferHandler
        extends ETableTransferHandler {
            private OutlineTransferHandler() {
            }

            public Transferable createOutlineTransferable() {
                return super.createTransferable((JComponent)((Object)OutlineViewOutline.this));
            }
        }

        private class CopyToClipboardAction
        implements Action {
            private Action orig;

            CopyToClipboardAction(Action orig) {
                this.orig = orig;
            }

            private Action getDelegate() {
                ExplorerManager em = OutlineViewOutline.this.manager;
                if (em == null) {
                    return this.orig;
                }
                Action a = ExplorerUtils.actionCopy(em);
                if (a.isEnabled()) {
                    return a;
                }
                return this.orig;
            }

            @Override
            public Object getValue(String key) {
                return this.getDelegate().getValue(key);
            }

            @Override
            public void putValue(String key, Object value) {
                this.getDelegate().putValue(key, value);
            }

            @Override
            public void setEnabled(boolean b) {
                throw new UnsupportedOperationException("Not supported.");
            }

            @Override
            public boolean isEnabled() {
                return this.getDelegate().isEnabled();
            }

            @Override
            public void addPropertyChangeListener(PropertyChangeListener listener) {
                this.getDelegate().addPropertyChangeListener(listener);
            }

            @Override
            public void removePropertyChangeListener(PropertyChangeListener listener) {
                this.getDelegate().removePropertyChangeListener(listener);
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                Action a;
                ExplorerManager em = OutlineViewOutline.this.manager;
                if (em != null && (a = ExplorerUtils.actionCopy(em)).isEnabled()) {
                    this.doCopy(em);
                    return;
                }
                this.getDelegate().actionPerformed(e);
            }

            private void doCopy(ExplorerManager em) {
                Clipboard clipboard;
                Node[] sel = em.getSelectedNodes();
                Transferable trans = ExplorerActionsImpl.getTransferableOwner(sel, true);
                Transferable ot = new OutlineTransferHandler().createOutlineTransferable();
                if (trans != null) {
                    if (ot != null) {
                        trans = new TextAddedTransferable(trans, ot);
                    }
                } else {
                    trans = ot;
                }
                if (trans != null && (clipboard = ExplorerActionsImpl.getClipboard()) != null) {
                    clipboard.setContents(trans, new StringSelection(""));
                }
            }
        }

        private final class DefaultQuickSearchTableFilter
        implements QuickSearchTableFilter {
            private DefaultQuickSearchTableFilter() {
            }

            @Override
            public String getStringValueAt(int row, int col) {
                String str;
                Object value = OutlineViewOutline.this.transformValue(OutlineViewOutline.this.getValueAt(row, col));
                if (value instanceof Node.Property) {
                    Node.Property p = (Node.Property)value;
                    Object v = null;
                    try {
                        v = p.getValue();
                    }
                    catch (IllegalAccessException ex) {
                    }
                    catch (InvocationTargetException ex) {
                        // empty catch block
                    }
                    str = v instanceof String ? (String)v : null;
                } else {
                    str = value instanceof VisualizerNode ? ((VisualizerNode)value).getDisplayName() : (value instanceof Node ? ((Node)value).getDisplayName() : (value instanceof String ? (String)value : null));
                }
                return str;
            }
        }

    }

    private class DefaultTreeAction
    implements ActionListener {
        private Outline outline;

        DefaultTreeAction(Outline outline) {
            this.outline = outline;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (this.outline.getSelectedColumn() != 0) {
                return;
            }
            Node[] nodes = OutlineView.this.manager.getSelectedNodes();
            TreeView.performPreferredActionOnNodes(nodes);
        }
    }

    private class TableSelectionListener
    implements VetoableChangeListener,
    ListSelectionListener,
    PropertyChangeListener {
        private TableSelectionListener() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            Object object = OutlineView.this.managerLock;
            synchronized (object) {
                if (OutlineView.this.manager == null) {
                    return;
                }
                if (evt.getPropertyName().equals("rootContext")) {
                    OutlineView.this.synchronizeRootContext();
                }
                if (evt.getPropertyName().equals("selectedNodes")) {
                    OutlineView.this.synchronizeSelectedNodes(true, new Node[0]);
                }
            }
        }

        @Override
        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            if (listSelectionEvent.getValueIsAdjusting()) {
                return;
            }
            int[] selectedRows = OutlineView.this.outline.getSelectedRows();
            ArrayList<Node> selectedNodes = new ArrayList<Node>(selectedRows.length);
            for (int i = 0; i < selectedRows.length; ++i) {
                Node n = OutlineView.this.getNodeFromRow(selectedRows[i]);
                if (n == null) continue;
                selectedNodes.add(n);
            }
            OutlineView.this.callSelectionChanged(selectedNodes.toArray((T[])new Node[selectedNodes.size()]));
        }

        @Override
        public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
            Node[] nodes;
            if (evt.getPropertyName().equals("selectedNodes") && OutlineView.this.isSelectionModeBroken(nodes = (Node[])evt.getNewValue())) {
                throw new PropertyVetoException("selection mode  broken by " + Arrays.asList(nodes), evt);
            }
        }
    }

    private class PopupAdapter
    extends MouseUtils.PopupMouseAdapter {
        PopupAdapter() {
        }

        protected void showPopup(MouseEvent e) {
            int selRow = OutlineView.this.outline.rowAtPoint(e.getPoint());
            if (selRow != -1) {
                if (!OutlineView.this.outline.getSelectionModel().isSelectedIndex(selRow)) {
                    OutlineView.this.outline.getSelectionModel().clearSelection();
                    OutlineView.this.outline.getSelectionModel().setSelectionInterval(selRow, selRow);
                }
            } else {
                OutlineView.this.outline.getSelectionModel().clearSelection();
            }
            Point p = SwingUtilities.convertPoint(e.getComponent(), e.getPoint(), OutlineView.this);
            if (OutlineView.this.isPopupAllowed()) {
                JPopupMenu pop = OutlineView.this.createPopup(p);
                OutlineView.this.showPopup(p.x, p.y, pop);
                e.consume();
            }
        }
    }

    private class PopupAction
    extends AbstractAction
    implements Runnable {
        private PopupAction() {
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            SwingUtilities.invokeLater(this);
        }

        @Override
        public void run() {
            Point p = OutlineView.this.getPositionForPopup();
            if (p == null) {
                return;
            }
            if (OutlineView.this.isPopupAllowed()) {
                JPopupMenu pop = OutlineView.this.createPopup(p);
                OutlineView.this.showPopup(p.x, p.y, pop);
            }
        }
    }

}

