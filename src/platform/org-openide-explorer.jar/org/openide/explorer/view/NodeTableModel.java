/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.awt.Mnemonics
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 */
package org.openide.explorer.view;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.table.AbstractTableModel;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.awt.Mnemonics;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

public class NodeTableModel
extends AbstractTableModel {
    private static final String ATTR_INVISIBLE = "InvisibleInTreeTableView";
    static final String ATTR_COMPARABLE_COLUMN = "ComparableColumnTTV";
    static final String ATTR_SORTING_COLUMN = "SortingColumnTTV";
    static final String ATTR_DESCENDING_ORDER = "DescendingOrderTTV";
    private static final String ATTR_ORDER_NUMBER = "OrderNumberTTV";
    private static final String ATTR_TREE_COLUMN = "TreeColumnTTV";
    private static final String ATTR_MNEMONIC_CHAR = "ColumnMnemonicCharTTV";
    private static final String ATTR_DISPLAY_NAME_WITH_MNEMONIC = "ColumnDisplayNameWithMnemonicTTV";
    ArrayColumn[] allPropertyColumns = new ArrayColumn[0];
    private int[] propertyColumns = new int[0];
    private Node[] nodeRows = new Node[0];
    private int sortColumn = -1;
    private boolean existsComparableColumn = false;
    private Node.Property treeColumnProperty = null;
    private PropertyChangeListener pcl;

    public NodeTableModel() {
        this.pcl = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                int row = NodeTableModel.this.rowForNode((Node)evt.getSource());
                if (row == -1) {
                    return;
                }
                int column = NodeTableModel.this.columnForProperty(evt.getPropertyName());
                if (column == -1) {
                    NodeTableModel.this.fireTableRowsUpdated(row, row);
                } else {
                    NodeTableModel.this.fireTableCellUpdated(row, column);
                }
            }
        };
    }

    public void setNodes(Node[] nodes) {
        int i;
        boolean asserts = false;
        if (!$assertionsDisabled) {
            asserts = true;
            if (!true) {
                throw new AssertionError();
            }
        }
        if (asserts && !EventQueue.isDispatchThread()) {
            Exceptions.printStackTrace((Throwable)new IllegalStateException("Must be called in AWT to assure data consistency."));
        }
        for (i = 0; i < this.nodeRows.length; ++i) {
            this.nodeRows[i].removePropertyChangeListener(this.pcl);
        }
        this.nodeRows = nodes;
        for (i = 0; i < this.nodeRows.length; ++i) {
            this.nodeRows[i].addPropertyChangeListener(this.pcl);
        }
        this.fireTableDataChanged();
    }

    public void setProperties(Node.Property[] props) {
        int i;
        boolean asserts = false;
        if (!$assertionsDisabled) {
            asserts = true;
            if (!true) {
                throw new AssertionError();
            }
        }
        if (asserts && !EventQueue.isDispatchThread()) {
            Exceptions.printStackTrace((Throwable)new IllegalStateException("Must be called in AWT to assure data consistency."));
        }
        int size = props.length;
        this.sortColumn = -1;
        this.treeColumnProperty = null;
        int treePosition = -1;
        for (int i2 = 0; i2 < props.length; ++i2) {
            Object o = props[i2].getValue("TreeColumnTTV");
            if (o == null || !(o instanceof Boolean) || !((Boolean)o).booleanValue()) continue;
            this.treeColumnProperty = props[i2];
            --size;
            treePosition = i2;
        }
        this.allPropertyColumns = new ArrayColumn[size];
        int visibleCount = 0;
        this.existsComparableColumn = false;
        TreeMap<Double, Integer> sort = new TreeMap<Double, Integer>();
        int ia = 0;
        for (i = 0; i < props.length; ++i) {
            Object o;
            if (i == treePosition) continue;
            this.allPropertyColumns[ia] = new ArrayColumn();
            this.allPropertyColumns[ia].setProperty(props[i]);
            if (this.isVisible(props[i])) {
                ++visibleCount;
                o = props[i].getValue("OrderNumberTTV");
                if (o != null && o instanceof Integer) {
                    sort.put(new Double(((Integer)o).doubleValue()), ia);
                } else {
                    sort.put(new Double((double)ia + 0.1), new Integer(ia));
                }
            } else {
                this.allPropertyColumns[ia].setVisibleIndex(-1);
                o = props[i].getValue("SortingColumnTTV");
                if (o != null && o instanceof Boolean) {
                    props[i].setValue("SortingColumnTTV", (Object)Boolean.FALSE);
                }
            }
            if (!this.existsComparableColumn && (o = props[i].getValue("ComparableColumnTTV")) != null && o instanceof Boolean) {
                this.existsComparableColumn = (Boolean)o;
            }
            ++ia;
        }
        this.propertyColumns = new int[visibleCount];
        int j = 0;
        Iterator it = sort.values().iterator();
        while (it.hasNext()) {
            i = (Integer)it.next();
            this.allPropertyColumns[i].setVisibleIndex(j);
            this.propertyColumns[j] = i;
            ++j;
        }
        this.fireTableStructureChanged();
    }

    private void computeVisiblePorperties(int visCount) {
        this.propertyColumns = new int[visCount];
        TreeMap<Double, Integer> sort = new TreeMap<Double, Integer>();
        for (int i = 0; i < this.allPropertyColumns.length; ++i) {
            int vi = this.allPropertyColumns[i].getVisibleIndex();
            if (vi == -1) {
                sort.put(new Double((double)i - 0.1), i);
                continue;
            }
            sort.put(new Double(vi), i);
        }
        int j = 0;
        Iterator it = sort.values().iterator();
        while (it.hasNext()) {
            int i2 = (Integer)it.next();
            Node.Property p = this.allPropertyColumns[i2].getProperty();
            if (this.isVisible(p)) {
                this.propertyColumns[j] = i2;
                this.allPropertyColumns[i2].setVisibleIndex(j);
                ++j;
                continue;
            }
            this.allPropertyColumns[i2].setVisibleIndex(-1);
            Object o = p.getValue("SortingColumnTTV");
            if (o == null || !(o instanceof Boolean) || !((Boolean)o).booleanValue()) continue;
            p.setValue("SortingColumnTTV", (Object)Boolean.FALSE);
            p.setValue("DescendingOrderTTV", (Object)Boolean.FALSE);
        }
        this.fireTableStructureChanged();
    }

    int getVisibleColumnWidth(int column) {
        return this.allPropertyColumns[this.propertyColumns[column]].getWidth();
    }

    int getArrayColumnWidth(int column) {
        return this.allPropertyColumns[column].getWidth();
    }

    void setVisibleColumnWidth(int column, int width) {
        this.allPropertyColumns[this.propertyColumns[column]].setWidth(width);
    }

    void setArrayColumnWidth(int column, int width) {
        this.allPropertyColumns[column].setWidth(width);
    }

    int getVisibleIndex(int arrayIndex) {
        return this.allPropertyColumns[arrayIndex].getVisibleIndex();
    }

    int getArrayIndex(int visibleIndex) {
        for (int i = 0; i < this.allPropertyColumns.length; ++i) {
            if (this.allPropertyColumns[i].getVisibleIndex() != visibleIndex) continue;
            return i;
        }
        return -1;
    }

    boolean isComparableColumn(int column) {
        return this.isComparableColumnEx(this.propertyColumns[column]);
    }

    boolean isComparableColumnEx(int column) {
        Node.Property p = this.allPropertyColumns[column].getProperty();
        Object o = p.getValue("ComparableColumnTTV");
        if (o != null && o instanceof Boolean) {
            return (Boolean)o;
        }
        return false;
    }

    boolean isVisibleColumnEx(int column) {
        for (int i = 0; i < this.propertyColumns.length; ++i) {
            if (column != this.propertyColumns[i]) continue;
            return true;
        }
        return false;
    }

    boolean existsComparableColumn() {
        return this.existsComparableColumn;
    }

    boolean isSortingColumnEx(int column) {
        Node.Property p = this.allPropertyColumns[column].getProperty();
        Object o = p.getValue("SortingColumnTTV");
        if (o != null && o instanceof Boolean) {
            return (Boolean)o;
        }
        return false;
    }

    void setSortingColumnEx(int column) {
        Node.Property p;
        if (this.sortColumn != -1) {
            p = this.allPropertyColumns[this.sortColumn].getProperty();
            p.setValue("SortingColumnTTV", (Object)Boolean.FALSE);
            p.setValue("DescendingOrderTTV", (Object)Boolean.FALSE);
        }
        if (column != -1) {
            this.sortColumn = column;
            p = this.allPropertyColumns[this.sortColumn].getProperty();
            p.setValue("SortingColumnTTV", (Object)Boolean.TRUE);
        } else {
            this.sortColumn = -1;
        }
    }

    int translateVisibleColumnIndex(int index) {
        if (index < 0) {
            return index;
        }
        return this.propertyColumns[index];
    }

    int getVisibleSortingColumn() {
        if (this.sortColumn == -1) {
            for (int i = 0; i < this.propertyColumns.length; ++i) {
                if (!this.isSortingColumnEx(this.propertyColumns[i])) continue;
                this.sortColumn = this.propertyColumns[i];
                return i;
            }
        } else if (this.isVisible(this.allPropertyColumns[this.sortColumn].getProperty())) {
            return this.getVisibleIndex(this.sortColumn);
        }
        return -1;
    }

    int getSortingColumn() {
        if (this.sortColumn == -1) {
            for (int i = 0; i < this.allPropertyColumns.length; ++i) {
                if (!this.isSortingColumnEx(i)) continue;
                this.sortColumn = i;
                return i;
            }
        } else {
            return this.sortColumn;
        }
        return -1;
    }

    boolean isSortOrderDescending() {
        if (this.sortColumn == -1) {
            return false;
        }
        Node.Property p = this.allPropertyColumns[this.sortColumn].getProperty();
        Object o = p.getValue("DescendingOrderTTV");
        if (o != null && o instanceof Boolean) {
            return (Boolean)o;
        }
        return false;
    }

    void setSortOrderDescending(boolean descending) {
        if (this.sortColumn != -1) {
            Node.Property p = this.allPropertyColumns[this.sortColumn].getProperty();
            p.setValue("DescendingOrderTTV", (Object)(descending ? Boolean.TRUE : Boolean.FALSE));
        }
    }

    protected Node.Property getPropertyFor(Node node, Node.Property prop) {
        Node.PropertySet[] propSets = node.getPropertySets();
        for (int i = 0; i < propSets.length; ++i) {
            Node.Property[] props = propSets[i].getProperties();
            for (int j = 0; j < props.length; ++j) {
                if (!prop.equals((Object)props[j])) continue;
                return props[j];
            }
        }
        return null;
    }

    Node nodeForRow(int row) {
        return this.nodeRows[row];
    }

    Node.Property propertyForColumn(int column) {
        if (column >= 0) {
            column = this.propertyColumns[column];
        }
        return this.propertyForColumnEx(column);
    }

    Node.Property propertyForColumnEx(int column) {
        if (column == -1) {
            return this.treeColumnProperty;
        }
        return this.allPropertyColumns[column].getProperty();
    }

    int getColumnCountEx() {
        return this.allPropertyColumns.length;
    }

    private int rowForNode(Node node) {
        for (int i = 0; i < this.nodeRows.length; ++i) {
            if (!node.equals((Object)this.nodeRows[i])) continue;
            return i;
        }
        return -1;
    }

    private int columnForProperty(String propName) {
        for (int i = 0; i < this.propertyColumns.length; ++i) {
            if (!this.allPropertyColumns[this.propertyColumns[i]].getProperty().getName().equals(propName)) continue;
            return i;
        }
        return -1;
    }

    private boolean isVisible(Node.Property p) {
        Object o = p.getValue("InvisibleInTreeTableView");
        if (o != null && o instanceof Boolean) {
            return (Boolean)o == false;
        }
        return true;
    }

    private void setVisible(Node.Property p, boolean visible) {
        p.setValue("InvisibleInTreeTableView", (Object)(!visible ? Boolean.TRUE : Boolean.FALSE));
    }

    @Override
    public int getRowCount() {
        return this.nodeRows.length;
    }

    @Override
    public int getColumnCount() {
        return this.propertyColumns.length;
    }

    @Override
    public Object getValueAt(int row, int column) {
        Node node = this.nodeRows[row];
        int pc = this.propertyColumns[column];
        ArrayColumn ac = this.allPropertyColumns[pc];
        return this.getPropertyFor(node, ac.getProperty());
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return this.getValueAt(row, column) != null;
    }

    public Class getColumnClass(int column) {
        return Node.Property.class;
    }

    @Override
    public String getColumnName(int column) {
        return this.getColumnNameEx(this.propertyColumns[column]);
    }

    String getColumnNameEx(int column) {
        return this.allPropertyColumns[column].getProperty().getDisplayName();
    }

    boolean selectVisibleColumns(String viewName, String treeColumnName, String treeColumnDesc) {
        String boxtext;
        boolean changed = false;
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        panel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(NodeTableModel.class, (String)"ACSN_ColumnDialog"));
        panel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(NodeTableModel.class, (String)"ACSD_ColumnDialog"));
        ArrayList<JCheckBox> boxes = new ArrayList<JCheckBox>(this.allPropertyColumns.length);
        boolean[] oldvalues = new boolean[this.allPropertyColumns.length];
        int[] sortpointer = new int[this.allPropertyColumns.length];
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 12, 0, 12);
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        GridBagConstraints labelConstraints = new GridBagConstraints();
        labelConstraints.gridwidth = 0;
        labelConstraints.anchor = 17;
        labelConstraints.insets = new Insets(12, 12, 0, 12);
        labelConstraints.fill = 2;
        labelConstraints.weightx = 1.0;
        JLabel desc = new JLabel(NbBundle.getMessage(NodeTableModel.class, (String)"LBL_ColumnDialogDesc"));
        panel.add((Component)desc, labelConstraints);
        GridBagConstraints firstConstraints = new GridBagConstraints();
        firstConstraints.gridwidth = 0;
        firstConstraints.anchor = 17;
        firstConstraints.insets = new Insets(12, 12, 0, 12);
        firstConstraints.fill = 2;
        firstConstraints.weightx = 1.0;
        JCheckBox first = new JCheckBox(treeColumnName + ": " + treeColumnDesc, true);
        first.setEnabled(false);
        panel.add((Component)first, firstConstraints);
        TreeMap<String, Integer> sort = new TreeMap<String, Integer>();
        for (int i = 0; i < this.allPropertyColumns.length; ++i) {
            oldvalues[i] = this.isVisible(this.allPropertyColumns[i].getProperty());
            boxtext = this.getDisplayNameWithMnemonic(this.allPropertyColumns[i].getProperty()) + ": " + this.allPropertyColumns[i].getProperty().getShortDescription();
            sort.put(boxtext, i);
        }
        Iterator it = sort.keySet().iterator();
        int j = 0;
        while (it.hasNext()) {
            boxtext = (String)it.next();
            int i2 = (Integer)sort.get(boxtext);
            JCheckBox b = new JCheckBox(boxtext, oldvalues[i2]);
            Mnemonics.setLocalizedText((AbstractButton)b, (String)boxtext);
            this.makeAccessibleCheckBox(b, this.allPropertyColumns[i2].getProperty());
            sortpointer[j] = i2;
            panel.add((Component)b, gridBagConstraints);
            boxes.add(b);
            ++j;
        }
        String title = NbBundle.getMessage(NodeTableModel.class, (String)"LBL_ColumnDialogTitle");
        if (viewName != null && viewName.length() > 0) {
            title = viewName + " - " + title;
        }
        DialogDescriptor dlg = new DialogDescriptor((Object)panel, title, true, 2, DialogDescriptor.OK_OPTION, 0, null, null);
        Dialog dialog = DialogDisplayer.getDefault().createDialog(dlg);
        dialog.setVisible(true);
        if (dlg.getValue().equals(DialogDescriptor.OK_OPTION)) {
            int num = boxes.size();
            int nv = 0;
            for (int i3 = 0; i3 < num; ++i3) {
                JCheckBox b = (JCheckBox)boxes.get(i3);
                j = sortpointer[i3];
                if (b.isSelected() != oldvalues[j]) {
                    this.setVisible(this.allPropertyColumns[j].getProperty(), b.isSelected());
                    changed = true;
                }
                if (!b.isSelected()) continue;
                ++nv;
            }
            if (changed) {
                this.computeVisiblePorperties(nv);
            }
        }
        return changed;
    }

    String getDisplayNameWithMnemonic(Node.Property p) {
        String res = null;
        Object displayNameWithMnemonic = p.getValue("ColumnDisplayNameWithMnemonicTTV");
        res = null != displayNameWithMnemonic && displayNameWithMnemonic.toString().length() > 0 ? displayNameWithMnemonic.toString() : p.getDisplayName();
        return res;
    }

    void makeAccessibleCheckBox(JCheckBox box, Node.Property p) {
        box.getAccessibleContext().setAccessibleName(p.getDisplayName());
        box.getAccessibleContext().setAccessibleDescription(p.getShortDescription());
        Object mnemonicChar = p.getValue("ColumnMnemonicCharTTV");
        if (null != mnemonicChar && mnemonicChar.toString().length() > 0) {
            box.setMnemonic(mnemonicChar.toString().charAt(0));
        }
    }

    void moveColumn(int from, int to) {
        int j;
        int i = this.propertyColumns[from];
        this.propertyColumns[from] = j = this.propertyColumns[to];
        this.propertyColumns[to] = i;
        this.allPropertyColumns[i].setVisibleIndex(to);
        this.allPropertyColumns[j].setVisibleIndex(from);
        this.sortColumn = -1;
    }

    static class ArrayColumn {
        private Node.Property property;
        private int width;

        ArrayColumn() {
        }

        public Node.Property getProperty() {
            return this.property;
        }

        public void setProperty(Node.Property property) {
            this.property = property;
        }

        public int getWidth() {
            return this.width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getVisibleIndex() {
            Integer order = (Integer)this.property.getValue("OrderNumberTTV");
            if (order == null) {
                return -1;
            }
            return order;
        }

        public void setVisibleIndex(int visibleIndex) {
            this.property.setValue("OrderNumberTTV", (Object)new Integer(visibleIndex));
        }
    }

}

