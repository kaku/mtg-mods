/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.etable.ETable
 *  org.netbeans.swing.outline.Outline
 *  org.netbeans.swing.outline.OutlineModel
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.NbBundle
 */
package org.openide.explorer.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.KeyboardFocusManager;
import java.awt.color.ColorSpace;
import java.awt.event.ComponentListener;
import java.awt.event.HierarchyListener;
import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.lang.ref.WeakReference;
import java.text.MessageFormat;
import java.util.EventObject;
import java.util.Map;
import java.util.WeakHashMap;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleRole;
import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.plaf.UIResource;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.tree.AbstractLayoutCache;
import javax.swing.tree.TreePath;
import org.netbeans.modules.openide.explorer.PropertyPanelBridge;
import org.netbeans.modules.openide.explorer.TTVEnvBridge;
import org.netbeans.swing.etable.ETable;
import org.netbeans.swing.outline.Outline;
import org.netbeans.swing.outline.OutlineModel;
import org.openide.explorer.propertysheet.ExPropertyModel;
import org.openide.explorer.propertysheet.PropertyModel;
import org.openide.explorer.propertysheet.PropertyPanel;
import org.openide.explorer.view.NodeTableModel;
import org.openide.explorer.view.OutlineView;
import org.openide.explorer.view.PropertiesRowModel;
import org.openide.explorer.view.Visualizer;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

abstract class SheetCell
extends AbstractCellEditor
implements TableModelListener,
PropertyChangeListener,
TableCellEditor,
TableCellRenderer {
    private static final int MAX_TOOLTIP_LENGTH = 1000;
    private Boolean flat;
    private Node node;
    private Node.Property prop;
    private TableCellRenderer headerRenderer = new JTableHeader().getDefaultRenderer();
    private NullPanel nullPanel;
    private Map panelCache = new WeakHashMap();
    private FocusedPropertyPanel renderer = null;
    protected PropertyPanel editor = null;
    private static Color noFocusSelectionBackground = null;
    private static Color noFocusSelectionForeground = null;

    public SheetCell() {
        this.setFlat(false);
    }

    public void setFlat(boolean f) {
        Color controlDkShadow = Color.lightGray;
        if (UIManager.getColor("controlDkShadow") != null) {
            controlDkShadow = UIManager.getColor("controlDkShadow");
        }
        Color controlLtHighlight = Color.black;
        if (UIManager.getColor("controlLtHighlight") != null) {
            controlLtHighlight = UIManager.getColor("controlLtHighlight");
        }
        Color buttonFocusColor = Color.blue;
        if (UIManager.getColor("Button.focus") != null) {
            buttonFocusColor = UIManager.getColor("Button.focus");
        }
        this.flat = f ? Boolean.TRUE : Boolean.FALSE;
    }

    @Override
    public Object getCellEditorValue() {
        return null;
    }

    public abstract Node nodeForRow(int var1);

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int r, int c) {
        this.prop = (Node.Property)value;
        this.node = this.nodeForRow(r);
        this.node.addPropertyChangeListener((PropertyChangeListener)this);
        PropertyPanel propPanel = this.getEditor(this.prop, this.node);
        propPanel.setBackground(table.getSelectionBackground());
        propPanel.setForeground(table.getSelectionForeground());
        propPanel.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, table.getSelectionBackground()));
        return propPanel;
    }

    @Override
    public boolean shouldSelectCell(EventObject ev) {
        return true;
    }

    @Override
    public boolean isCellEditable(EventObject e) {
        return true;
    }

    @Override
    public boolean stopCellEditing() {
        if (this.prop != null) {
            this.detachEditor();
        }
        return super.stopCellEditing();
    }

    @Override
    public void cancelCellEditing() {
        if (this.prop != null) {
            this.detachEditor();
        }
        super.cancelCellEditing();
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        this.cancelCellEditing();
    }

    protected void detachEditor() {
        this.node.removePropertyChangeListener((PropertyChangeListener)this);
        this.node = null;
        this.prop = null;
    }

    private FocusedPropertyPanel getRenderer(Node.Property p, Node n) {
        TTVEnvBridge bridge = TTVEnvBridge.getInstance(this);
        bridge.setCurrentBeans((Object[])new Node[]{n});
        if (this.renderer == null) {
            this.renderer = new FocusedPropertyPanel(p, 9);
            this.renderer.putClientProperty("beanBridgeIdentifier", this);
        }
        this.renderer.setProperty(p);
        this.renderer.putClientProperty("flat", Boolean.TRUE);
        return this.renderer;
    }

    public abstract String getShortDescription(int var1);

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (row == -1) {
            Component comp = this.headerRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            if (comp instanceof JComponent) {
                String tip = column > 0 ? this.getShortDescription(column) : table.getColumnName(0);
                ((JComponent)comp).setToolTipText(tip);
            }
            return comp;
        }
        Node.Property property = (Node.Property)value;
        Node n = this.nodeForRow(row);
        Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        boolean tableHasFocus = hasFocus || table == focusOwner || table.isAncestorOf(focusOwner) || focusOwner instanceof Container && ((Container)focusOwner).isAncestorOf(table);
        Component defaultRendererComponent = table.getDefaultRenderer(Object.class).getTableCellRendererComponent(table, value, isSelected, tableHasFocus, row, column);
        Color bg = this.getRealColor(defaultRendererComponent.getBackground());
        Color fg = defaultRendererComponent.getForeground();
        if (property != null) {
            FocusedPropertyPanel propPanel = this.getRenderer(property, n);
            propPanel.setFocused(hasFocus);
            Object computeTooltip = table.getClientProperty("ComputingTooltip");
            if (Boolean.TRUE.equals(computeTooltip)) {
                String toolT = null;
                PropertyEditor propEd = property.getPropertyEditor();
                Object propertyToolTipShortDescription = table.getClientProperty("PropertyToolTipShortDescription");
                if (Boolean.TRUE.equals(propertyToolTipShortDescription) && property.getShortDescription() != null) {
                    toolT = property.getShortDescription();
                } else if (propEd != null) {
                    try {
                        propEd.setValue(property.getValue());
                        toolT = propEd.getAsText();
                    }
                    catch (Exception ex) {
                        // empty catch block
                    }
                }
                if (toolT == null) {
                    Object val = null;
                    try {
                        val = property.getValue();
                    }
                    catch (Exception ex) {
                        // empty catch block
                    }
                    if (val != null) {
                        toolT = val.toString();
                    }
                }
                if (toolT != null && toolT.trim().length() > 0) {
                    if ((toolT = toolT.trim()).length() > 1000) {
                        toolT = toolT.substring(0, 1000) + "...";
                    }
                    propPanel.setToolTipText(toolT);
                } else {
                    propPanel.setToolTipText(null);
                }
            }
            propPanel.setOpaque(true);
            if (isSelected && !tableHasFocus) {
                propPanel.setBackground(SheetCell.getNoFocusSelectionBackground());
                propPanel.setForeground(SheetCell.getNoFocusSelectionForeground());
            } else {
                propPanel.setBackground(bg);
                propPanel.setForeground(fg);
            }
            if (table instanceof ETable) {
                ETable et = (ETable)table;
                et.setCellBackground((Component)propPanel, isSelected, row, column);
            }
            return propPanel;
        }
        if (this.nullPanel == null) {
            this.nullPanel = new NullPanel(n);
            this.nullPanel.setOpaque(true);
        } else {
            this.nullPanel.setNode(n);
        }
        if (isSelected && !tableHasFocus) {
            this.nullPanel.setBackground(SheetCell.getNoFocusSelectionBackground());
            this.nullPanel.setForeground(SheetCell.getNoFocusSelectionForeground());
        } else {
            this.nullPanel.setBackground(bg);
            this.nullPanel.setForeground(fg);
        }
        if (table instanceof ETable) {
            ETable et = (ETable)table;
            et.setCellBackground((Component)this.nullPanel, isSelected, row, column);
        }
        this.nullPanel.setFocused(hasFocus);
        return this.nullPanel;
    }

    private Color getRealColor(Color c) {
        if (c instanceof UIResource) {
            float[] components = c.getComponents(null);
            int cn = components.length - 1;
            float[] colorComponents = new float[cn];
            System.arraycopy(components, 0, colorComponents, 0, cn);
            c = new Color(c.getColorSpace(), colorComponents, components[cn]);
        }
        return c;
    }

    private PropertyPanel getEditor(Node.Property p, Node n) {
        int prefs = 8;
        TTVEnvBridge bridge = TTVEnvBridge.getInstance(this);
        bridge.setCurrentBeans((Object[])new Node[]{n});
        if (this.editor == null) {
            this.editor = new PropertyPanel(p, prefs);
            this.editor.putClientProperty("flat", Boolean.TRUE);
            this.editor.putClientProperty("beanBridgeIdentifier", this);
            this.editor.setProperty(p);
            return this.editor;
        }
        this.editor.setProperty(p);
        return this.editor;
    }

    private PropertyPanel obtainPanel(Node node, Node.Property prop) {
        return this.getEditor(prop, node);
    }

    private static String getString(String key) {
        return NbBundle.getMessage(SheetCell.class, (String)key);
    }

    static Color getNoFocusSelectionBackground() {
        if (noFocusSelectionBackground == null && (SheetCell.noFocusSelectionBackground = UIManager.getColor("nb.explorer.noFocusSelectionBackground")) == null) {
            noFocusSelectionBackground = UIManager.getColor("controlShadow");
            if (noFocusSelectionBackground == null) {
                noFocusSelectionBackground = Color.lightGray;
            }
            noFocusSelectionBackground = SheetCell.betterBrighter(noFocusSelectionBackground);
        }
        return noFocusSelectionBackground;
    }

    private static Color betterBrighter(Color c) {
        Color bb = c.brighter();
        if (Color.WHITE.equals(bb)) {
            bb = new Color((255 + c.getRed()) / 2, (255 + c.getGreen()) / 2, (255 + c.getBlue()) / 2);
        }
        return bb;
    }

    static Color getNoFocusSelectionForeground() {
        if (noFocusSelectionForeground == null && (SheetCell.noFocusSelectionForeground = UIManager.getColor("nb.explorer.noFocusSelectionForeground")) == null && (SheetCell.noFocusSelectionForeground = UIManager.getColor("textText")) == null) {
            noFocusSelectionForeground = Color.BLACK;
        }
        return noFocusSelectionForeground;
    }

    public static class OutlineSheetCell
    extends SheetCell {
        private Outline outline;

        public OutlineSheetCell(Outline outline) {
            this.outline = outline;
        }

        @Override
        public Node nodeForRow(int row) {
            int r = this.outline.convertRowIndexToModel(row);
            TreePath tp = this.outline.getLayoutCache().getPathForRow(r);
            return Visualizer.findNode(tp.getLastPathComponent());
        }

        @Override
        public String getShortDescription(int column) {
            return this.outline.getOutlineModel().getColumnName(column);
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            this.stopCellEditingNoCommit();
            if (SwingUtilities.isEventDispatchThread()) {
                this.outline.tableChanged(new TableModelEvent(this.outline.getModel(), 0, this.outline.getRowCount()));
            } else {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        OutlineSheetCell.this.outline.tableChanged(new TableModelEvent(OutlineSheetCell.this.outline.getModel(), 0, OutlineSheetCell.this.outline.getRowCount()));
                    }
                });
            }
        }

        @Override
        public boolean stopCellEditing() {
            PropertyPanelBridge.commit(this.editor);
            return this.stopCellEditingNoCommit();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private boolean stopCellEditingNoCommit() {
            PropertiesRowModel prm = null;
            if (this.outline instanceof OutlineView.OutlineViewOutline) {
                OutlineView.OutlineViewOutline ovo = (OutlineView.OutlineViewOutline)this.outline;
                prm = ovo.getRowModel();
            }
            if (prm != null) {
                prm.setIgnoreSetValue(true);
            }
            try {
                boolean ovo = super.stopCellEditing();
                return ovo;
            }
            finally {
                if (prm != null) {
                    prm.setIgnoreSetValue(false);
                }
            }
        }

        @Override
        protected void detachEditor() {
            super.detachEditor();
            TableModel tableModel = this.outline.getModel();
            tableModel.removeTableModelListener(this);
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int r, int c) {
            TableModel tableModel = this.outline.getModel();
            tableModel.addTableModelListener(this);
            return super.getTableCellEditorComponent(table, value, isSelected, r, c);
        }

    }

    static class TableSheetCell
    extends SheetCell {
        private NodeTableModel tableModel;
        private ETable table;

        public TableSheetCell(NodeTableModel tableModel, ETable table) {
            this.tableModel = tableModel;
            this.table = table;
        }

        @Override
        public Node nodeForRow(int row) {
            int r = this.table.convertRowIndexToModel(row);
            return this.tableModel.nodeForRow(r);
        }

        @Override
        public String getShortDescription(int column) {
            return this.tableModel.propertyForColumn(column).getShortDescription();
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            this.tableModel.fireTableDataChanged();
        }

        @Override
        protected void detachEditor() {
            super.detachEditor();
            this.tableModel.removeTableModelListener(this);
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int r, int c) {
            this.tableModel.addTableModelListener(this);
            return super.getTableCellEditorComponent(table, value, isSelected, r, c);
        }
    }

    static class FocusedPropertyPanel
    extends PropertyPanel {
        boolean focused;

        public FocusedPropertyPanel(Node.Property p, int preferences) {
            super(p, preferences);
            this.setDoubleBuffered(true);
        }

        public void setFocused(boolean focused) {
            this.focused = focused;
        }

        @Override
        public void addComponentListener(ComponentListener l) {
        }

        @Override
        public void addHierarchyListener(HierarchyListener l) {
        }

        @Override
        public void repaint(long tm, int x, int y, int width, int height) {
        }

        @Override
        public void revalidate() {
        }

        @Override
        public void firePropertyChange(String s, Object a, Object b) {
            if ("flat".equals(s)) {
                super.firePropertyChange(s, a, b);
            }
        }

        @Override
        public boolean isShowing() {
            return true;
        }

        @Override
        public void update(Graphics g) {
        }

        @Override
        public void paint(Graphics g) {
            Color c = this.getBackground();
            Color old = g.getColor();
            g.setColor(c);
            g.fillRect(0, 0, this.getWidth(), this.getHeight());
            g.setColor(old);
            super.paint(g);
            if (this.focused) {
                Color bdr = UIManager.getColor("Tree.selectionBorderColor");
                if (bdr == null) {
                    bdr = this.getForeground().equals(Color.BLACK) ? this.getBackground().darker() : this.getForeground().darker();
                }
                g.setColor(bdr);
                g.drawRect(1, 1, this.getWidth() - 3, this.getHeight() - 3);
            }
            g.setColor(old);
        }

        @Override
        public AccessibleContext getAccessibleContext() {
            if (this.accessibleContext == null) {
                this.accessibleContext = new AccessibleFocusedPropertyPanel();
            }
            return this.accessibleContext;
        }

        private class AccessibleFocusedPropertyPanel
        extends JComponent.AccessibleJComponent {
            AccessibleFocusedPropertyPanel() {
                super(FocusedPropertyPanel.this);
            }

            @Override
            public AccessibleRole getAccessibleRole() {
                return AccessibleRole.PANEL;
            }

            @Override
            public String getAccessibleName() {
                FeatureDescriptor fd = ((ExPropertyModel)FocusedPropertyPanel.this.getModel()).getFeatureDescriptor();
                PropertyEditor editor = FocusedPropertyPanel.this.getPropertyEditor();
                Object[] arrobject = new Object[2];
                arrobject[0] = fd.getDisplayName();
                arrobject[1] = editor == null ? SheetCell.getString("CTL_No_value") : editor.getAsText();
                return MessageFormat.format(SheetCell.getString("ACS_PropertyPanelRenderer"), arrobject);
            }

            @Override
            public String getAccessibleDescription() {
                FeatureDescriptor fd = ((ExPropertyModel)FocusedPropertyPanel.this.getModel()).getFeatureDescriptor();
                Node node = (Node)((ExPropertyModel)FocusedPropertyPanel.this.getModel()).getBeans()[0];
                Class clazz = FocusedPropertyPanel.this.getModel().getPropertyType();
                Object[] arrobject = new Object[3];
                arrobject[0] = fd.getShortDescription();
                arrobject[1] = clazz == null ? SheetCell.getString("CTL_No_type") : clazz.getName();
                arrobject[2] = node.getDisplayName();
                return MessageFormat.format(SheetCell.getString("ACSD_PropertyPanelRenderer"), arrobject);
            }
        }

    }

    private static class NullPanel
    extends JPanel {
        private WeakReference<Node> weakNode;
        private boolean focused = false;

        NullPanel(Node node) {
            this.weakNode = new WeakReference<Node>(node);
        }

        void setNode(Node node) {
            this.weakNode = new WeakReference<Node>(node);
        }

        @Override
        public AccessibleContext getAccessibleContext() {
            if (this.accessibleContext == null) {
                this.accessibleContext = new AccessibleNullPanel();
            }
            return this.accessibleContext;
        }

        public void setFocused(boolean val) {
            this.focused = val;
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            if (this.focused) {
                Color bdr = UIManager.getColor("Tree.selectionBorderColor");
                if (bdr == null) {
                    bdr = this.getForeground().equals(Color.BLACK) ? this.getBackground().darker() : this.getForeground().darker();
                }
                g.setColor(bdr);
                g.drawRect(1, 1, this.getWidth() - 3, this.getHeight() - 3);
                g.setColor(bdr);
            }
        }

        @Override
        public void addComponentListener(ComponentListener l) {
        }

        @Override
        public void addHierarchyListener(HierarchyListener l) {
        }

        @Override
        public void repaint() {
        }

        @Override
        public void repaint(int x, int y, int width, int height) {
        }

        @Override
        public void invalidate() {
        }

        @Override
        public void revalidate() {
        }

        @Override
        public void validate() {
        }

        @Override
        public void firePropertyChange(String s, Object a, Object b) {
        }

        private class AccessibleNullPanel
        extends JPanel.AccessibleJPanel {
            AccessibleNullPanel() {
                super(NullPanel.this);
            }

            @Override
            public String getAccessibleName() {
                String name = super.getAccessibleName();
                if (name == null) {
                    name = SheetCell.getString("ACS_NullPanel");
                }
                return name;
            }

            @Override
            public String getAccessibleDescription() {
                Node node;
                String description = super.getAccessibleDescription();
                if (description == null && (node = (Node)NullPanel.this.weakNode.get()) != null) {
                    description = MessageFormat.format(SheetCell.getString("ACSD_NullPanel"), node.getDisplayName());
                }
                return description;
            }
        }

    }

}

