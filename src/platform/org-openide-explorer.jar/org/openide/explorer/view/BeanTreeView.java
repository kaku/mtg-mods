/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package org.openide.explorer.view;

import java.awt.Rectangle;
import java.beans.PropertyVetoException;
import javax.swing.BorderFactory;
import javax.swing.JTree;
import javax.swing.border.Border;
import javax.swing.plaf.TreeUI;
import javax.swing.plaf.basic.BasicTreeUI;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.NodeTreeModel;
import org.openide.explorer.view.TreeView;
import org.openide.nodes.Node;

public class BeanTreeView
extends TreeView {
    static final long serialVersionUID = 3841322840231536380L;

    public BeanTreeView() {
        this.setBorder(BorderFactory.createEmptyBorder());
    }

    @Override
    protected NodeTreeModel createModel() {
        return new NodeTreeModel();
    }

    @Override
    protected boolean selectionAccept(Node[] nodes) {
        return true;
    }

    @Override
    protected void showSelection(TreePath[] treePaths) {
        this.tree.getSelectionModel().setSelectionPaths(treePaths);
        if (treePaths.length == 1) {
            this.showPathWithoutExpansion(treePaths[0]);
        }
    }

    @Override
    protected void selectionChanged(Node[] nodes, ExplorerManager em) throws PropertyVetoException {
        if (nodes.length > 0) {
            Node context = nodes[0].getParentNode();
            for (int i = 1; i < nodes.length; ++i) {
                if (context == nodes[i].getParentNode()) continue;
                em.setSelectedNodes(nodes);
                return;
            }
            if (em.getRootContext().getParentNode() == context) {
                em.setExploredContextAndSelection(em.getRootContext(), nodes);
            } else {
                em.setExploredContextAndSelection(context, nodes);
            }
        } else {
            em.setSelectedNodes(nodes);
        }
    }

    @Override
    protected void showPath(TreePath path) {
        this.tree.expandPath(path);
        this.showPathWithoutExpansion(path);
    }

    private void showPathWithoutExpansion(TreePath path) {
        Rectangle rect = this.tree.getPathBounds(path);
        if (rect != null) {
            TreeUI tmp = this.tree.getUI();
            int correction = 0;
            if (tmp instanceof BasicTreeUI) {
                correction = ((BasicTreeUI)tmp).getLeftChildIndent();
                correction += ((BasicTreeUI)tmp).getRightChildIndent();
            }
            rect.x = Math.max(0, rect.x - correction);
            if (rect.y >= 0) {
                this.tree.scrollRectToVisible(rect);
            }
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.tree.setEnabled(enabled);
    }

    @Override
    public boolean isEnabled() {
        if (this.tree == null) {
            return true;
        }
        return this.tree.isEnabled();
    }
}

