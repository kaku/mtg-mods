/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 *  org.openide.awt.HtmlRenderer$Renderer
 *  org.openide.awt.ListPane
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.openide.explorer.view;

import java.awt.Component;
import java.awt.Container;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.JList;
import javax.swing.JTree;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.tree.TreeCellRenderer;
import org.openide.awt.HtmlRenderer;
import org.openide.awt.ListPane;
import org.openide.explorer.view.NodeListModel;
import org.openide.explorer.view.TreeTable;
import org.openide.explorer.view.VisualizerNode;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class NodeRenderer
implements TreeCellRenderer,
ListCellRenderer {
    private static NodeRenderer instance = null;
    private static VisualizerNode draggedOver;
    private boolean bigIcons = false;
    private HtmlRenderer.Renderer renderer = HtmlRenderer.createRenderer();

    public NodeRenderer() {
    }

    @Deprecated
    public NodeRenderer(boolean bigIcons) {
        this.bigIcons = bigIcons;
    }

    @Deprecated
    public static NodeRenderer sharedInstance() {
        if (instance == null) {
            instance = new NodeRenderer();
        }
        IllegalStateException ise = new IllegalStateException("NodeRenderer.sharedInstance() is deprecated.  Create an instance of NodeRendererinstead");
        Logger.getLogger(NodeRenderer.class.getName()).log(Level.WARNING, null, ise);
        return instance;
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        String text;
        boolean isHtml;
        VisualizerNode vis = NodeRenderer.findVisualizerNode(value);
        if (vis == draggedOver) {
            sel = true;
        }
        boolean bl = isHtml = (text = vis.getHtmlDisplayName()) != null;
        if (!isHtml) {
            text = vis.getDisplayName();
        }
        Component result = this.renderer.getTreeCellRendererComponent(tree, (Object)text, sel, expanded, leaf, row, hasFocus);
        result.setEnabled(tree.isEnabled());
        this.renderer.setHtml(isHtml);
        this.configureFrom(this.renderer, tree, expanded, sel, vis);
        return result;
    }

    public Component getListCellRendererComponent(JList list, Object value, int index, boolean sel, boolean cellHasFocus) {
        int depth;
        boolean bi;
        boolean isHtml;
        VisualizerNode vis = NodeRenderer.findVisualizerNode(value);
        if (vis == draggedOver) {
            sel = true;
        }
        String text = vis.getHtmlDisplayName();
        if (list.getModel() instanceof NodeListModel && (depth = NodeListModel.findVisualizerDepth(list.getModel(), vis)) == -1) {
            text = NbBundle.getMessage(NodeRenderer.class, (String)"LBL_UP");
        }
        boolean bl = isHtml = text != null;
        if (!isHtml) {
            text = vis.getDisplayName();
        }
        Component result = this.renderer.getListCellRendererComponent(list, (Object)text, index, sel, cellHasFocus || value == draggedOver);
        this.renderer.setHtml(isHtml);
        result.setEnabled(list.isEnabled());
        int iconWidth = this.configureFrom(this.renderer, list, false, sel, vis);
        boolean bl2 = bi = this.bigIcons || list instanceof ListPane;
        if (bi) {
            this.renderer.setCentered(true);
        } else if (list.getModel() instanceof NodeListModel && ((NodeListModel)list.getModel()).getDepth() > 1) {
            int indent = iconWidth * NodeListModel.findVisualizerDepth(list.getModel(), vis);
            this.renderer.setIndent(indent);
        }
        return result;
    }

    private int configureFrom(HtmlRenderer.Renderer ren, Container target, boolean useOpenedIcon, boolean sel, VisualizerNode vis) {
        Icon icon = vis.getIcon(useOpenedIcon, this.bigIcons);
        if (icon.getIconWidth() > 0) {
            ren.setIconTextGap(24 - icon.getIconWidth());
        } else {
            ren.setIndent(26);
        }
        try {
            ren.setIcon(icon);
        }
        catch (NullPointerException ex) {
            Exceptions.attachMessage((Throwable)ex, (String)("icon: " + icon));
            Exceptions.attachMessage((Throwable)ex, (String)("vis: " + vis));
            Exceptions.attachMessage((Throwable)ex, (String)("ren: " + (Object)ren));
            throw ex;
        }
        if (target instanceof TreeTable.TreeTableCellRenderer) {
            TreeTable.TreeTableCellRenderer ttRen = (TreeTable.TreeTableCellRenderer)target;
            TreeTable tt = ttRen.getTreeTable();
            ren.setParentFocused(ttRen.treeTableHasFocus() || tt.isEditing());
        }
        return icon.getIconWidth() == 0 ? 24 : icon.getIconWidth();
    }

    private static VisualizerNode findVisualizerNode(Object value) {
        if (value instanceof Node) {
            return VisualizerNode.getVisualizer(null, (Node)value);
        }
        if (value instanceof VisualizerNode) {
            return (VisualizerNode)value;
        }
        if (value == null || " ".equals(value) || "".equals(value)) {
            return VisualizerNode.EMPTY;
        }
        throw new ClassCastException("Unexpected value: " + value);
    }

    static void dragEnter(Object dragged) {
        draggedOver = (VisualizerNode)dragged;
    }

    static void dragExit() {
        draggedOver = null;
    }
}

