/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.MouseUtils
 *  org.openide.awt.MouseUtils$PopupMouseAdapter
 *  org.openide.awt.QuickSearch
 *  org.openide.awt.QuickSearch$Callback
 *  org.openide.nodes.Children
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.FilterNode$Children
 *  org.openide.nodes.FilterNode$NodeAdapter
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeMemberEvent
 *  org.openide.nodes.NodeReorderEvent
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.openide.explorer.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.BoundedRangeModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneLayout;
import javax.swing.Scrollable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TreeExpansionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import org.openide.awt.MouseUtils;
import org.openide.awt.QuickSearch;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.BeanTreeView;
import org.openide.explorer.view.NodeTableModel;
import org.openide.explorer.view.NodeTreeModel;
import org.openide.explorer.view.QuickSearchTableFilter;
import org.openide.explorer.view.TableQuickSearchSupport;
import org.openide.explorer.view.TreeTable;
import org.openide.explorer.view.TreeView;
import org.openide.explorer.view.ViewUtil;
import org.openide.explorer.view.Visualizer;
import org.openide.explorer.view.VisualizerNode;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.ImageUtilities;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class TreeTableView
extends BeanTreeView {
    private static final String COLUMNS_ICON = "/org/netbeans/modules/openide/explorer/columns.gif";
    private static final String SORT_ASC_ICON = "org/netbeans/modules/openide/explorer/columnsSortedAsc.gif";
    private static final String SORT_DESC_ICON = "org/netbeans/modules/openide/explorer/columnsSortedDesc.gif";
    protected JTable treeTable;
    private NodeTableModel tableModel;
    private JScrollBar hScrollBar;
    private JScrollPane scrollPane;
    private ScrollListener listener;
    private boolean allowHideColumns = false;
    private boolean allowSortingByColumn = false;
    private boolean hideHScrollBar = false;
    private JButton colsButton = null;
    private SortedNodeTreeModel sortedNodeTreeModel;
    private ActionListener defaultTreeActionListener;
    private TableCellRenderer defaultHeaderRenderer = null;
    private MouseUtils.PopupMouseAdapter tableMouseListener;
    private AccessibleContext accessContext;
    private TreeColumnProperty treeColumnProperty = new TreeColumnProperty();
    private int treeColumnWidth;
    private Component treeTableParent = null;
    private QuickSearch quickSearch;
    private Component searchpanel;
    private final Object searchConstraints = new Object();

    public TreeTableView() {
        this(new NodeTableModel());
    }

    public TreeTableView(NodeTableModel ntm) {
        this.setLayout(new SearchScrollPaneLayout());
        this.tableModel = ntm;
        this.initializeTreeTable();
        this.setPopupAllowed(true);
        this.setDefaultActionAllowed(true);
        this.initializeTreeScrollSupport();
        CompoundScrollPane p = new CompoundScrollPane();
        p.setLayout(new BorderLayout());
        this.scrollPane.setViewportView(this.treeTable);
        p.add("Center", this.scrollPane);
        ImageIcon ic = new ImageIcon(ImageUtilities.loadImage((String)"org/netbeans/modules/openide/explorer/columns.gif", (boolean)true));
        this.colsButton = new JButton(ic);
        this.colsButton.setBackground(new JTableHeader().getBackground());
        this.colsButton.getAccessibleContext().setAccessibleName(NbBundle.getMessage(TreeTableView.class, (String)"ACN_ColumnsSelector"));
        this.colsButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(TreeTableView.class, (String)"ACD_ColumnsSelector"));
        this.colsButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                TreeTableView.this.selectVisibleColumns();
            }
        });
        JPanel sbp = new JPanel();
        sbp.setLayout(new FlowLayout(0, 0, 0));
        sbp.add(this.hScrollBar);
        p.add("South", sbp);
        super.setHorizontalScrollBarPolicy(31);
        super.setVerticalScrollBarPolicy(21);
        this.setViewportView(p);
        this.setBorder(BorderFactory.createEmptyBorder());
        this.setViewportBorder(BorderFactory.createEmptyBorder());
    }

    @Override
    public void setRowHeader(JViewport rowHeader) {
        rowHeader.setBorder(BorderFactory.createEmptyBorder());
        super.setRowHeader(rowHeader);
    }

    @Override
    public void setHorizontalScrollBarPolicy(int policy) {
        boolean bl = this.hideHScrollBar = policy == 31;
        if (this.hideHScrollBar) {
            this.hScrollBar.setVisible(false);
            ((TreeTable)this.treeTable).setTreeHScrollingEnabled(false);
        }
    }

    @Override
    public void setVerticalScrollBarPolicy(int policy) {
        if (this.scrollPane == null) {
            return;
        }
        boolean bl = this.allowHideColumns = policy == 22;
        if (this.allowHideColumns) {
            this.scrollPane.setCorner("UPPER_RIGHT_CORNER", this.colsButton);
        }
        this.treeTable.getTableHeader().setReorderingAllowed(this.allowHideColumns);
        this.scrollPane.setVerticalScrollBarPolicy(policy);
    }

    @Override
    protected NodeTreeModel createModel() {
        return this.getSortedNodeTreeModel();
    }

    @Override
    public void requestFocus() {
        if (this.treeTable != null) {
            this.treeTable.requestFocus();
        }
    }

    @Override
    public boolean requestFocusInWindow() {
        boolean res = super.requestFocusInWindow();
        if (null != this.treeTable) {
            this.treeTable.requestFocus();
        }
        return res;
    }

    private void setAllowSortingByColumn(boolean allow) {
        if (allow && allow != this.allowSortingByColumn) {
            this.addMouseListener(new MouseAdapter(){

                @Override
                public void mouseClicked(MouseEvent evt) {
                    int index;
                    JTableHeader h;
                    if (evt.getClickCount() == 0) {
                        return;
                    }
                    Component c = evt.getComponent();
                    if (c instanceof JTableHeader && (index = (h = (JTableHeader)c).columnAtPoint(evt.getPoint())) >= 0) {
                        TreeTableView.this.clickOnColumnAction(index - 1);
                    }
                }
            });
        }
        this.allowSortingByColumn = allow;
    }

    private void clickOnColumnAction(int index) {
        if (index == -1) {
            if (this.treeColumnProperty.isComparable()) {
                if (this.treeColumnProperty.isSortingColumn()) {
                    if (!this.treeColumnProperty.isSortOrderDescending()) {
                        this.setSortingOrder(false);
                    } else {
                        this.noSorting();
                    }
                } else {
                    int realIndex = this.tableModel.translateVisibleColumnIndex(index);
                    this.setSortingColumn(index);
                    this.setSortingOrder(true);
                }
            }
        } else if (this.tableModel.isComparableColumn(index)) {
            if (this.tableModel.isSortingColumnEx(this.tableModel.translateVisibleColumnIndex(index))) {
                if (!this.tableModel.isSortOrderDescending()) {
                    this.setSortingOrder(false);
                } else {
                    this.noSorting();
                }
            } else {
                int realIndex = this.tableModel.translateVisibleColumnIndex(index);
                this.setSortingColumn(realIndex);
                this.setSortingOrder(true);
            }
        }
    }

    private void selectVisibleColumns() {
        this.setCurrentWidths();
        String viewName = null;
        if (this.getParent() != null) {
            viewName = this.getParent().getName();
        }
        if (this.tableModel.selectVisibleColumns(viewName, this.treeTable.getColumnName(0), this.getSortedNodeTreeModel().getRootDescription())) {
            if (this.tableModel.getSortingColumn() == -1) {
                this.getSortedNodeTreeModel().setSortedByProperty(null);
            }
            this.setTreePreferredWidth(this.treeColumnWidth);
            for (int i = 0; i < this.tableModel.getColumnCount(); ++i) {
                this.setTableColumnPreferredWidth(this.tableModel.getArrayIndex(i), this.tableModel.getVisibleColumnWidth(i));
            }
        }
    }

    private void setCurrentWidths() {
        this.treeColumnWidth = this.treeTable.getColumnModel().getColumn(0).getWidth();
        for (int i = 0; i < this.tableModel.getColumnCount(); ++i) {
            int w = this.treeTable.getColumnModel().getColumn(i + 1).getWidth();
            this.tableModel.setVisibleColumnWidth(i, w);
        }
    }

    @Override
    void initializeTree() {
    }

    @Override
    public void add(Component comp, Object constraints) {
        if (constraints == this.searchConstraints) {
            this.searchpanel = comp;
            constraints = null;
        }
        super.add(comp, constraints);
    }

    @Override
    public void remove(Component comp) {
        if (comp == this.searchpanel) {
            this.searchpanel = null;
        }
        super.remove(comp);
    }

    private void initializeTreeTable() {
        this.treeModel = this.createModel();
        TreeTable tt = new TreeTable(this.treeModel, this.tableModel);
        this.treeTable = tt;
        this.tree = ((TreeTable)this.treeTable).getTree();
        TableQuickSearchSupport tqss = new TableQuickSearchSupport(tt, tt.getQuickSearchTableFilter(), tt.getQuickSearchSettings());
        this.quickSearch = QuickSearch.attach((JComponent)this, (Object)this.searchConstraints, (QuickSearch.Callback)tqss, (JMenu)tqss.createSearchPopupMenu());
        tt.addKeyListener(new KeyListener(){

            @Override
            public void keyTyped(KeyEvent e) {
                TreeTableView.this.quickSearch.processKeyEvent(e);
            }

            @Override
            public void keyPressed(KeyEvent e) {
                TreeTableView.this.quickSearch.processKeyEvent(e);
            }

            @Override
            public void keyReleased(KeyEvent e) {
                TreeTableView.this.quickSearch.processKeyEvent(e);
            }
        });
        this.defaultHeaderRenderer = this.treeTable.getTableHeader().getDefaultRenderer();
        this.treeTable.getTableHeader().setDefaultRenderer(new SortingHeaderRenderer());
        this.managerListener = new TreeView.TreePropertyListener(this);
        this.tree.addTreeExpansionListener(this.managerListener);
        this.defaultActionListener = new TreeView.PopupSupport(this);
        AbstractAction popupWrapper = new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                SwingUtilities.invokeLater(TreeTableView.this.defaultActionListener);
            }

            @Override
            public boolean isEnabled() {
                return TreeTableView.this.treeTable.isFocusOwner() || TreeTableView.this.tree.isFocusOwner();
            }
        };
        this.treeTable.getInputMap(1).put(KeyStroke.getKeyStroke(121, 64), "org.openide.actions.PopupAction");
        this.treeTable.getActionMap().put("org.openide.actions.PopupAction", popupWrapper);
        this.tree.addMouseListener(this.defaultActionListener);
        this.tableMouseListener = new MouseUtils.PopupMouseAdapter(){

            public void showPopup(MouseEvent mevt) {
                if (TreeTableView.this.isPopupAllowed()) {
                    if (mevt.getY() > TreeTableView.this.treeTable.getHeight()) {
                        TreeTableView.this.treeTable.clearSelection();
                    } else {
                        int selRow = TreeTableView.this.treeTable.rowAtPoint(mevt.getPoint());
                        boolean isAlreadySelected = false;
                        int[] currentSelection = TreeTableView.this.tree.getSelectionRows();
                        for (int i = 0; null != currentSelection && i < currentSelection.length; ++i) {
                            if (selRow != currentSelection[i]) continue;
                            isAlreadySelected = true;
                            break;
                        }
                        if (!isAlreadySelected) {
                            TreeTableView.this.tree.setSelectionRow(selRow);
                        }
                    }
                    TreeTableView.this.createPopup(mevt);
                }
            }
        };
        this.treeTable.addMouseListener((MouseListener)this.tableMouseListener);
        if (UIManager.getColor("control") != null) {
            this.treeTable.setGridColor(UIManager.getColor("control"));
        }
    }

    @Override
    public void setSelectionMode(int mode) {
        super.setSelectionMode(mode);
        if (mode == 1) {
            this.treeTable.getSelectionModel().setSelectionMode(0);
        } else if (mode == 2) {
            this.treeTable.getSelectionModel().setSelectionMode(1);
        } else if (mode == 4) {
            this.treeTable.getSelectionModel().setSelectionMode(2);
        }
    }

    @Override
    public AccessibleContext getAccessibleContext() {
        if (this.accessContext == null) {
            this.accessContext = new AccessibleTreeTableView();
        }
        return this.accessContext;
    }

    private void initializeTreeScrollSupport() {
        this.scrollPane = new JScrollPane();
        this.scrollPane.setName("TreeTableView.scrollpane");
        this.scrollPane.setBorder(BorderFactory.createEmptyBorder());
        this.scrollPane.setViewportBorder(BorderFactory.createEmptyBorder());
        if (UIManager.getColor("Table.background") != null) {
            this.scrollPane.getViewport().setBackground(UIManager.getColor("Table.background"));
        }
        this.hScrollBar = new JScrollBar(0);
        this.hScrollBar.putClientProperty("JScrollBar.isFreeStanding", Boolean.FALSE);
        this.hScrollBar.setVisible(false);
        this.listener = new ScrollListener();
        this.treeTable.addPropertyChangeListener(this.listener);
        this.scrollPane.getViewport().addComponentListener(this.listener);
        this.tree.addPropertyChangeListener(this.listener);
        this.hScrollBar.getModel().addChangeListener(this.listener);
    }

    @Override
    public void setPopupAllowed(boolean value) {
        if (this.tree == null) {
            return;
        }
        if (this.popupListener == null && value) {
            this.popupListener = new TreeView.PopupAdapter(){

                @Override
                protected void showPopup(MouseEvent e) {
                    int selRow = TreeTableView.this.tree.getClosestRowForLocation(e.getX(), e.getY());
                    if (!TreeTableView.this.tree.isRowSelected(selRow)) {
                        TreeTableView.this.tree.setSelectionRow(selRow);
                    }
                }
            };
            this.tree.addMouseListener((MouseListener)((Object)this.popupListener));
            return;
        }
        if (this.popupListener != null && !value) {
            this.tree.removeMouseListener((MouseListener)((Object)this.popupListener));
            this.popupListener = null;
            return;
        }
    }

    @Override
    public void setDefaultActionAllowed(boolean value) {
        if (this.tree == null) {
            return;
        }
        this.defaultActionEnabled = value;
        if (value) {
            this.defaultTreeActionListener = new DefaultTreeAction();
            this.treeTable.registerKeyboardAction(this.defaultTreeActionListener, KeyStroke.getKeyStroke(10, 0, false), 0);
        } else {
            this.defaultTreeActionListener = null;
            this.treeTable.unregisterKeyboardAction(KeyStroke.getKeyStroke(10, 0, false));
        }
    }

    @Override
    public boolean isQuickSearchAllowed() {
        return this.quickSearch.isEnabled();
    }

    @Override
    public void setQuickSearchAllowed(boolean allowedQuickSearch) {
        this.quickSearch.setEnabled(allowedQuickSearch);
    }

    public void setProperties(Node.Property[] props) {
        this.tableModel.setProperties(props);
        this.treeColumnProperty.setProperty(this.tableModel.propertyForColumn(-1));
        if (this.treeColumnProperty.isComparable() || this.tableModel.existsComparableColumn()) {
            this.setAllowSortingByColumn(true);
            if (this.treeColumnProperty.isSortingColumn()) {
                this.getSortedNodeTreeModel().setSortedByName(true, !this.treeColumnProperty.isSortOrderDescending());
            } else {
                int index = this.tableModel.getSortingColumn();
                if (index != -1) {
                    this.getSortedNodeTreeModel().setSortedByProperty(this.tableModel.propertyForColumnEx(index), !this.tableModel.isSortOrderDescending());
                }
            }
        }
    }

    public final void setTableAutoResizeMode(int mode) {
        this.treeTable.setAutoResizeMode(mode);
    }

    public final int getTableAutoResizeMode() {
        return this.treeTable.getAutoResizeMode();
    }

    public final void setTableColumnPreferredWidth(int index, int width) {
        if (index == -1) {
            return;
        }
        this.tableModel.setArrayColumnWidth(index, width);
        int j = this.tableModel.getVisibleIndex(index);
        if (j != -1) {
            this.treeTable.getColumnModel().getColumn(j + 1).setPreferredWidth(width);
        }
    }

    public final int getTableColumnPreferredWidth(int index) {
        int j = this.tableModel.getVisibleIndex(index);
        if (j != -1) {
            return this.treeTable.getColumnModel().getColumn(j + 1).getPreferredWidth();
        }
        return this.tableModel.getArrayColumnWidth(index);
    }

    public final void setTreePreferredWidth(int width) {
        this.treeTable.getColumnModel().getColumn(((TreeTable)this.treeTable).getTreeColumnIndex()).setPreferredWidth(width);
    }

    public final int getTreePreferredWidth() {
        return this.treeTable.getColumnModel().getColumn(((TreeTable)this.treeTable).getTreeColumnIndex()).getPreferredWidth();
    }

    @Override
    public void addNotify() {
        if (this.treeTable.getParent() != null) {
            this.treeTableParent = this.treeTable.getParent();
            this.treeTableParent.addMouseListener((MouseListener)this.tableMouseListener);
        }
        super.addNotify();
        if (this.tableModel.getRowCount() == 0) {
            Node[] nodes = new Node[this.tree.getRowCount()];
            for (int i = 0; i < this.tree.getRowCount(); ++i) {
                nodes[i] = Visualizer.findNode(this.tree.getPathForRow(i).getLastPathComponent());
            }
            this.tableModel.setNodes(nodes);
        }
        this.listener.revalidateScrollBar();
        ViewUtil.adjustBackground(this.treeTable);
        ViewUtil.adjustBackground(this.scrollPane);
        ViewUtil.adjustBackground(this.scrollPane.getViewport());
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        if (this.treeTableParent != null) {
            this.treeTableParent.removeMouseListener((MouseListener)this.tableMouseListener);
        }
        this.treeTableParent = null;
        this.tableModel.setNodes(new Node[0]);
    }

    @Override
    public void addMouseListener(MouseListener l) {
        super.addMouseListener(l);
        this.treeTable.getTableHeader().addMouseListener(l);
    }

    @Override
    public void removeMouseListener(MouseListener l) {
        super.removeMouseListener(l);
        this.treeTable.getTableHeader().removeMouseListener(l);
    }

    @Override
    public void setDragSource(boolean state) {
    }

    @Override
    public void setDropTarget(boolean state) {
    }

    @Override
    Point getPositionForPopup() {
        int row = this.treeTable.getSelectedRow();
        if (row < 0) {
            return null;
        }
        int col = this.treeTable.getSelectedColumn();
        if (col < 0) {
            col = 0;
        }
        Rectangle r = col == 0 ? this.tree.getRowBounds(row) : this.treeTable.getCellRect(row, col, true);
        Point p = SwingUtilities.convertPoint(this.treeTable, r.x, r.y, this);
        return p;
    }

    private void createPopup(MouseEvent e) {
        Point p = SwingUtilities.convertPoint(e.getComponent(), e.getX(), e.getY(), this);
        this.createPopup(p.x, p.y);
        e.consume();
    }

    @Override
    void createPopup(int xpos, int ypos) {
        int treeXpos = xpos - ((TreeTable)this.treeTable).getPositionX();
        if (this.allowHideColumns || this.allowSortingByColumn) {
            int col = this.treeTable.getColumnModel().getColumnIndexAtX(treeXpos);
            super.createExtendedPopup(xpos, ypos, this.getListMenu(col));
        } else {
            super.createPopup(xpos, ypos);
        }
    }

    private JMenu getListMenu(final int col) {
        JMenu listItem = new JMenu(NbBundle.getMessage(NodeTableModel.class, (String)"LBL_ListOptions"));
        if (this.allowHideColumns && col > 0) {
            JMenu colsItem = new JMenu(NbBundle.getMessage(NodeTableModel.class, (String)"LBL_ColsMenu"));
            boolean addColsItem = false;
            if (col > 1) {
                JMenuItem moveLItem = new JMenuItem(NbBundle.getMessage(NodeTableModel.class, (String)"LBL_MoveLeft"));
                moveLItem.addActionListener(new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        TreeTableView.this.treeTable.getColumnModel().moveColumn(col, col - 1);
                    }
                });
                colsItem.add(moveLItem);
                addColsItem = true;
            }
            if (col < this.tableModel.getColumnCount()) {
                JMenuItem moveRItem = new JMenuItem(NbBundle.getMessage(NodeTableModel.class, (String)"LBL_MoveRight"));
                moveRItem.addActionListener(new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        TreeTableView.this.treeTable.getColumnModel().moveColumn(col, col + 1);
                    }
                });
                colsItem.add(moveRItem);
                addColsItem = true;
            }
            if (addColsItem) {
                listItem.add(colsItem);
            }
        }
        if (this.allowSortingByColumn) {
            int index;
            int i;
            JRadioButtonMenuItem colItem;
            JMenu sortItem = new JMenu(NbBundle.getMessage(NodeTableModel.class, (String)"LBL_SortMenu"));
            JRadioButtonMenuItem noSortItem = new JRadioButtonMenuItem(NbBundle.getMessage(NodeTableModel.class, (String)"LBL_NoSort"), !this.getSortedNodeTreeModel().isSortingActive());
            noSortItem.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    TreeTableView.this.noSorting();
                }
            });
            sortItem.add(noSortItem);
            int visibleComparable = 0;
            if (this.treeColumnProperty.isComparable()) {
                ++visibleComparable;
                colItem = new JRadioButtonMenuItem(this.treeTable.getColumnName(0), this.treeColumnProperty.isSortingColumn());
                colItem.setHorizontalTextPosition(2);
                colItem.addActionListener(new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        TreeTableView.this.setSortingColumn(-1);
                    }
                });
                sortItem.add(colItem);
            }
            for (i = 0; i < this.tableModel.getColumnCount(); ++i) {
                if (!this.tableModel.isComparableColumn(i)) continue;
                ++visibleComparable;
                colItem = new JRadioButtonMenuItem(this.tableModel.getColumnName(i), this.tableModel.isSortingColumnEx(this.tableModel.translateVisibleColumnIndex(i)));
                colItem.setHorizontalTextPosition(2);
                index = this.tableModel.translateVisibleColumnIndex(i);
                colItem.addActionListener(new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        TreeTableView.this.setSortingColumn(index);
                    }
                });
                sortItem.add(colItem);
            }
            for (i = 0; i < this.tableModel.getColumnCountEx(); ++i) {
                if (!this.tableModel.isComparableColumnEx(i) || this.tableModel.isVisibleColumnEx(i)) continue;
                ++visibleComparable;
                colItem = new JRadioButtonMenuItem(this.tableModel.getColumnNameEx(i), this.tableModel.isSortingColumnEx(i));
                colItem.setHorizontalTextPosition(2);
                index = i;
                colItem.addActionListener(new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        TreeTableView.this.setSortingColumn(index);
                    }
                });
                sortItem.add(colItem);
            }
            if (visibleComparable > 0) {
                sortItem.addSeparator();
                boolean current_sort = this.treeColumnProperty.isSortingColumn() ? this.treeColumnProperty.isSortOrderDescending() : this.tableModel.isSortOrderDescending();
                JRadioButtonMenuItem ascItem = new JRadioButtonMenuItem(NbBundle.getMessage(NodeTableModel.class, (String)"LBL_Ascending"), !current_sort);
                ascItem.setHorizontalTextPosition(2);
                ascItem.addActionListener(new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        TreeTableView.this.setSortingOrder(true);
                    }
                });
                sortItem.add(ascItem);
                JRadioButtonMenuItem descItem = new JRadioButtonMenuItem(NbBundle.getMessage(NodeTableModel.class, (String)"LBL_Descending"), current_sort);
                descItem.setHorizontalTextPosition(2);
                descItem.addActionListener(new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        TreeTableView.this.setSortingOrder(false);
                    }
                });
                sortItem.add(descItem);
                if (!this.getSortedNodeTreeModel().isSortingActive()) {
                    ascItem.setEnabled(false);
                    descItem.setEnabled(false);
                }
                listItem.add(sortItem);
            }
        }
        if (this.allowHideColumns) {
            JMenuItem visItem = new JMenuItem(NbBundle.getMessage(NodeTableModel.class, (String)"LBL_ChangeColumns"));
            visItem.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    TreeTableView.this.selectVisibleColumns();
                }
            });
            listItem.add(visItem);
        }
        return listItem;
    }

    private void setSortingColumn(int index) {
        this.tableModel.setSortingColumnEx(index);
        if (index != -1) {
            this.getSortedNodeTreeModel().setSortedByProperty(this.tableModel.propertyForColumnEx(index), !this.tableModel.isSortOrderDescending());
            this.treeColumnProperty.setSortingColumn(false);
        } else {
            this.getSortedNodeTreeModel().setSortedByName(true, !this.treeColumnProperty.isSortOrderDescending());
            this.treeColumnProperty.setSortingColumn(true);
        }
        this.treeTable.getTableHeader().repaint();
    }

    private void noSorting() {
        this.tableModel.setSortingColumnEx(-1);
        this.getSortedNodeTreeModel().setNoSorting();
        this.treeColumnProperty.setSortingColumn(false);
        this.treeTable.getTableHeader().repaint();
    }

    private void setSortingOrder(boolean ascending) {
        if (this.treeColumnProperty.isSortingColumn()) {
            this.treeColumnProperty.setSortOrderDescending(!ascending);
        } else {
            this.tableModel.setSortOrderDescending(!ascending);
        }
        this.getSortedNodeTreeModel().setSortOrder(ascending);
        this.treeTable.getTableHeader().repaint();
    }

    private synchronized SortedNodeTreeModel getSortedNodeTreeModel() {
        if (this.sortedNodeTreeModel == null) {
            this.sortedNodeTreeModel = new SortedNodeTreeModel();
        }
        return this.sortedNodeTreeModel;
    }

    @Override
    public Insets getInsets() {
        Insets res = this.getInnerInsets();
        res = new Insets(res.top, res.left, res.bottom, res.right);
        if (null != this.searchpanel && this.searchpanel.isVisible()) {
            res.bottom += this.searchpanel.getPreferredSize().height;
        }
        return res;
    }

    private Insets getInnerInsets() {
        Insets res = super.getInsets();
        if (null == res) {
            res = new Insets(0, 0, 0, 0);
        }
        return res;
    }

    @Override
    Node getOriginalNode(Node n) {
        if (n instanceof SortedNodeTreeModel.SortedNode) {
            SortedNodeTreeModel.SortedNode sn = (SortedNodeTreeModel.SortedNode)n;
            return sn.getOriginalNode();
        }
        return n;
    }

    @Override
    protected void showSelection(TreePath[] treePaths) {
        TreePath[] modifiedTreePaths = new TreePath[treePaths.length];
        for (int i = 0; i < treePaths.length; ++i) {
            TreePath mtp;
            TreePath tp = treePaths[i];
            Node o = ((VisualizerNode)tp.getLastPathComponent()).node;
            modifiedTreePaths[i] = mtp = this.getTreePath(this.getSortedNodeFromOriginal(o));
        }
        super.showSelection(modifiedTreePaths);
    }

    @Override
    public void collapseNode(Node n) {
        super.collapseNode(this.getSortedNodeFromOriginal(n));
    }

    @Override
    public void expandNode(Node n) {
        super.expandNode(this.getSortedNodeFromOriginal(n));
    }

    @Override
    public boolean isExpanded(Node n) {
        return super.isExpanded(this.getSortedNodeFromOriginal(n));
    }

    private Node getSortedNodeFromOriginal(Node orig) {
        SortedNodeTreeModel.SortedNode sn;
        if (this.getSortedNodeTreeModel() != null && this.getSortedNodeTreeModel().original2filter != null && (sn = (SortedNodeTreeModel.SortedNode)((Object)this.getSortedNodeTreeModel().original2filter.get((Object)orig))) != null) {
            return sn;
        }
        return orig;
    }

    private static class TreeColumnProperty {
        private Node.Property p = null;

        TreeColumnProperty() {
        }

        void setProperty(Node.Property p) {
            this.p = p;
        }

        boolean isComparable() {
            if (this.p == null) {
                return false;
            }
            Object o = this.p.getValue("ComparableColumnTTV");
            if (o != null && o instanceof Boolean) {
                return (Boolean)o;
            }
            return false;
        }

        boolean isSortingColumn() {
            if (this.p == null) {
                return false;
            }
            Object o = this.p.getValue("SortingColumnTTV");
            if (o != null && o instanceof Boolean) {
                return (Boolean)o;
            }
            return false;
        }

        void setSortingColumn(boolean sorting) {
            if (this.p == null) {
                return;
            }
            this.p.setValue("SortingColumnTTV", (Object)(sorting ? Boolean.TRUE : Boolean.FALSE));
        }

        boolean isSortOrderDescending() {
            if (this.p == null) {
                return false;
            }
            Object o = this.p.getValue("DescendingOrderTTV");
            if (o != null && o instanceof Boolean) {
                return (Boolean)o;
            }
            return false;
        }

        void setSortOrderDescending(boolean descending) {
            if (this.p == null) {
                return;
            }
            this.p.setValue("DescendingOrderTTV", (Object)(descending ? Boolean.TRUE : Boolean.FALSE));
        }
    }

    private class SortingHeaderRenderer
    extends DefaultTableCellRenderer {
        SortingHeaderRenderer() {
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            Component comp = TreeTableView.this.defaultHeaderRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            if (comp instanceof JLabel) {
                if (column == 0 && TreeTableView.this.treeColumnProperty.isSortingColumn()) {
                    ((JLabel)comp).setIcon(this.getProperIcon(TreeTableView.this.treeColumnProperty.isSortOrderDescending()));
                    ((JLabel)comp).setHorizontalTextPosition(2);
                    if (Utilities.isWindows()) {
                        comp.setFont(this.getFont().deriveFont(1, this.getFont().getSize()));
                    } else {
                        comp.setFont(new Font(this.getFont().getName(), 1, this.getFont().getSize()));
                    }
                } else if (column != 0 && TreeTableView.this.tableModel.getVisibleSortingColumn() + 1 == column) {
                    ((JLabel)comp).setIcon(this.getProperIcon(TreeTableView.this.tableModel.isSortOrderDescending()));
                    ((JLabel)comp).setHorizontalTextPosition(2);
                    if (Utilities.isWindows()) {
                        comp.setFont(this.getFont().deriveFont(1, this.getFont().getSize()));
                    } else {
                        comp.setFont(new Font(this.getFont().getName(), 1, this.getFont().getSize()));
                    }
                } else {
                    ((JLabel)comp).setIcon(null);
                }
            }
            return comp;
        }

        private ImageIcon getProperIcon(boolean descending) {
            if (descending) {
                return ImageUtilities.loadImageIcon((String)"org/netbeans/modules/openide/explorer/columnsSortedDesc.gif", (boolean)false);
            }
            return ImageUtilities.loadImageIcon((String)"org/netbeans/modules/openide/explorer/columnsSortedAsc.gif", (boolean)false);
        }
    }

    private class SortedNodeTreeModel
    extends NodeTreeModel {
        private Node.Property sortedByProperty;
        private boolean sortAscending;
        private Comparator<Node> rowComparator;
        private boolean sortedByName;
        private Map<Node, SortedNode> original2filter;

        private SortedNodeTreeModel() {
            this.sortAscending = true;
            this.sortedByName = false;
            this.original2filter = new WeakHashMap<Node, SortedNode>(11);
        }

        @Override
        void setNode(Node root, TreeView.VisualizerHolder visHolder) {
            visHolder.clear();
            this.original2filter.clear();
            super.setNode((Node)new SortedNode(root), null);
        }

        void setNoSorting() {
            this.setSortedByProperty(null);
            this.setSortedByName(false);
            this.sortingChanged();
        }

        boolean isSortingActive() {
            return this.sortedByProperty != null || this.sortedByName;
        }

        void setSortedByProperty(Node.Property prop) {
            if (this.sortedByProperty == prop) {
                return;
            }
            this.sortedByProperty = prop;
            if (prop == null) {
                this.rowComparator = null;
            } else {
                this.sortedByName = false;
            }
            this.sortingChanged();
        }

        void setSortedByProperty(Node.Property prop, boolean ascending) {
            if (this.sortedByProperty == prop && ascending == this.sortAscending) {
                return;
            }
            this.sortedByProperty = prop;
            this.sortAscending = ascending;
            if (prop == null) {
                this.rowComparator = null;
            } else {
                this.sortedByName = false;
            }
            this.sortingChanged();
        }

        void setSortedByName(boolean sorted, boolean ascending) {
            if (this.sortedByName == sorted && ascending == this.sortAscending) {
                return;
            }
            this.sortedByName = sorted;
            this.sortAscending = ascending;
            if (this.sortedByName) {
                this.sortedByProperty = null;
            }
            this.sortingChanged();
        }

        void setSortedByName(boolean sorted) {
            this.sortedByName = sorted;
            if (this.sortedByName) {
                this.sortedByProperty = null;
            }
            this.sortingChanged();
        }

        void setSortOrder(boolean ascending) {
            if (ascending == this.sortAscending) {
                return;
            }
            this.sortAscending = ascending;
            this.sortingChanged();
        }

        private Node.Property getNodeProperty(Node node, Node.Property prop) {
            Node.PropertySet[] propsets = node.getPropertySets();
            int n = propsets.length;
            for (int i = 0; i < n; ++i) {
                Node.Property[] props = propsets[i].getProperties();
                int m = props.length;
                for (int j = 0; j < m; ++j) {
                    if (!props[j].equals((Object)prop)) continue;
                    return props[j];
                }
            }
            return null;
        }

        synchronized Comparator<Node> getRowComparator() {
            if (this.rowComparator == null) {
                this.rowComparator = new Comparator<Node>(){

                    @Override
                    public int compare(Node n1, Node n2) {
                        if (n1 == n2) {
                            return 0;
                        }
                        if (n1 == null && n2 == null) {
                            return 0;
                        }
                        if (n1 == null) {
                            return 1;
                        }
                        if (n2 == null) {
                            return -1;
                        }
                        if (n1.getParentNode() == null || n2.getParentNode() == null) {
                            Logger.getAnonymousLogger().warning("TTV.compare: Node " + (Object)n1 + " or " + (Object)n2 + " has no parent!");
                            return 0;
                        }
                        if (!n1.getParentNode().equals((Object)n2.getParentNode())) {
                            Logger.getAnonymousLogger().warning("TTV.compare: Nodes " + (Object)n1 + " and " + (Object)n2 + " has different parent!");
                            return 0;
                        }
                        if (SortedNodeTreeModel.this.sortedByName) {
                            int res = n1.getDisplayName().compareTo(n2.getDisplayName());
                            return SortedNodeTreeModel.this.sortAscending ? res : - res;
                        }
                        Node.Property p1 = SortedNodeTreeModel.this.getNodeProperty(n1, SortedNodeTreeModel.this.sortedByProperty);
                        Node.Property p2 = SortedNodeTreeModel.this.getNodeProperty(n2, SortedNodeTreeModel.this.sortedByProperty);
                        if (p1 == null && p2 == null) {
                            return 0;
                        }
                        try {
                            int res;
                            if (p1 == null) {
                                res = -1;
                            } else if (p2 == null) {
                                res = 1;
                            } else {
                                Object v1 = p1.getValue();
                                Object v2 = p2.getValue();
                                if (v1 == null && v2 == null) {
                                    return 0;
                                }
                                if (v1 == null) {
                                    res = -1;
                                } else if (v2 == null) {
                                    res = 1;
                                } else {
                                    if (v1.getClass() != v2.getClass() || !(v1 instanceof Comparable)) {
                                        v1 = v1.toString();
                                        v2 = v2.toString();
                                    }
                                    res = ((Comparable)v1).compareTo(v2);
                                }
                            }
                            return SortedNodeTreeModel.this.sortAscending ? res : - res;
                        }
                        catch (Exception ex) {
                            Logger.getLogger(TreeTableView.class.getName()).log(Level.WARNING, null, ex);
                            return 0;
                        }
                    }
                };
            }
            return this.rowComparator;
        }

        void sortingChanged() {
            TreeNode tn = (TreeNode)this.getRoot();
            ArrayList<TreePath> list = new ArrayList<TreePath>();
            Enumeration<TreePath> en = TreeTableView.this.tree.getExpandedDescendants(new TreePath(tn));
            while (en != null && en.hasMoreElements()) {
                TreePath path = en.nextElement();
                Node n = ((VisualizerNode)path.getLastPathComponent()).node;
                Children children = n.getChildren();
                if (!(children instanceof SortedChildren)) continue;
                ((SortedChildren)children).sortNodes();
                list.add(path);
            }
            for (int i = 0; i < list.size(); ++i) {
                TreeTableView.this.tree.expandPath((TreePath)list.get(i));
            }
        }

        String getRootDescription() {
            if (this.getRoot() instanceof VisualizerNode) {
                return ((VisualizerNode)this.getRoot()).getShortDescription();
            }
            return "";
        }

        private class SortedChildren
        extends FilterNode.Children {
            public SortedChildren(Node n) {
                super(n);
                this.sortNodes();
            }

            protected Node[] createNodes(Node key) {
                return new Node[]{new SortedNode(key)};
            }

            protected void addNotify() {
                super.addNotify();
                this.sortNodes();
            }

            protected void filterChildrenAdded(NodeMemberEvent ev) {
                super.filterChildrenAdded(ev);
                this.sortNodes();
            }

            protected void filterChildrenRemoved(NodeMemberEvent ev) {
                super.filterChildrenRemoved(ev);
                this.sortNodes();
            }

            protected void filterChildrenReordered(NodeReorderEvent ev) {
                super.filterChildrenReordered(ev);
                this.sortNodes();
            }

            private void sortNodes() {
                Object[] origNodes = this.original.getChildren().getNodes();
                if (SortedNodeTreeModel.this.isSortingActive()) {
                    Object[] sortedNodes = new Node[origNodes.length];
                    System.arraycopy(origNodes, 0, sortedNodes, 0, origNodes.length);
                    Collections.sort(Arrays.asList(sortedNodes), SortedNodeTreeModel.this.getRowComparator());
                    this.setKeys(sortedNodes);
                } else {
                    this.setKeys(origNodes);
                }
            }
        }

        private class SortedNode
        extends FilterNode {
            public SortedNode(Node original) {
                super(original, original.isLeaf() ? FilterNode.Children.LEAF : new SortedChildren(original));
                SortedNodeTreeModel.this.original2filter.put(original, this);
            }

            public Node getOriginalNode() {
                return super.getOriginal();
            }

            protected NodeListener createNodeListener() {
                return new FilterNode.NodeAdapter(this){

                    protected void propertyChange(FilterNode fn, PropertyChangeEvent ev) {
                        Node node;
                        super.propertyChange(fn, ev);
                        if (ev.getPropertyName().equals("leaf")) {
                            final Children[] newChildren = new Children[1];
                            FilterNode.Children.MUTEX.readAccess(new Runnable(){

                                @Override
                                public void run() {
                                    boolean origIsLeaf = SortedNode.this.getOriginal().isLeaf();
                                    boolean thisIsLeaf = SortedNode.this.isLeaf();
                                    if (origIsLeaf && !thisIsLeaf) {
                                        newChildren[0] = FilterNode.Children.LEAF;
                                    } else if (!origIsLeaf && thisIsLeaf) {
                                        newChildren[0] = new SortedChildren(SortedNode.this.getOriginal());
                                    }
                                }
                            });
                            if (newChildren[0] != null) {
                                FilterNode.Children.MUTEX.postWriteRequest(new Runnable(){

                                    @Override
                                    public void run() {
                                        SortedNode.this.setChildren(newChildren[0]);
                                    }
                                });
                            }
                        }
                        if (ev.getPropertyName().equals("parentNode") && (node = (Node)ev.getSource()).getParentNode() == null) {
                            SortedNodeTreeModel.this.original2filter.remove((Object)node);
                        }
                    }

                };
            }

        }

    }

    private class DefaultTreeAction
    implements ActionListener {
        DefaultTreeAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Action a;
            if (TreeTableView.this.treeTable.getSelectedColumn() != ((TreeTable)TreeTableView.this.treeTable).getTreeColumnIndex()) {
                return;
            }
            Node[] nodes = TreeTableView.this.manager.getSelectedNodes();
            if (nodes.length == 1 && (a = nodes[0].getPreferredAction()) != null) {
                if (a.isEnabled()) {
                    a.actionPerformed(new ActionEvent((Object)nodes[0], 1001, ""));
                } else {
                    Utilities.disabledActionBeep();
                }
            }
        }
    }

    private class SearchScrollPaneLayout
    extends ScrollPaneLayout {
        @Override
        public void layoutContainer(Container parent) {
            super.layoutContainer(parent);
            if (null != TreeTableView.this.searchpanel && TreeTableView.this.searchpanel.isVisible()) {
                Insets innerInsets = TreeTableView.this.getInnerInsets();
                Dimension prefSize = TreeTableView.this.searchpanel.getPreferredSize();
                TreeTableView.this.searchpanel.setBounds(innerInsets.left, parent.getHeight() - innerInsets.bottom - prefSize.height, parent.getWidth() - innerInsets.left - innerInsets.right, prefSize.height);
            }
        }
    }

    private static final class CompoundScrollPane
    extends JPanel
    implements Scrollable {
        CompoundScrollPane() {
        }

        @Override
        public void setBorder(Border b) {
        }

        @Override
        public boolean getScrollableTracksViewportWidth() {
            return true;
        }

        @Override
        public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
            return 10;
        }

        @Override
        public boolean getScrollableTracksViewportHeight() {
            return true;
        }

        @Override
        public Dimension getPreferredScrollableViewportSize() {
            return this.getPreferredSize();
        }

        @Override
        public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
            return 10;
        }
    }

    private final class ScrollListener
    extends ComponentAdapter
    implements PropertyChangeListener,
    ChangeListener {
        boolean movecorrection;

        ScrollListener() {
            this.movecorrection = false;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (((TreeTable)TreeTableView.this.treeTable).getTreeColumnIndex() == -1) {
                return;
            }
            if ("width".equals(evt.getPropertyName())) {
                if (!TreeTableView.this.treeTable.equals(evt.getSource())) {
                    Dimension dim = TreeTableView.this.hScrollBar.getPreferredSize();
                    dim.width = TreeTableView.this.treeTable.getColumnModel().getColumn(((TreeTable)TreeTableView.this.treeTable).getTreeColumnIndex()).getWidth();
                    TreeTableView.this.hScrollBar.setPreferredSize(dim);
                    TreeTableView.this.hScrollBar.revalidate();
                    TreeTableView.this.hScrollBar.repaint();
                }
                this.revalidateScrollBar();
            } else if ("positionX".equals(evt.getPropertyName())) {
                this.revalidateScrollBar();
            } else if ("treeColumnIndex".equals(evt.getPropertyName())) {
                TreeTableView.this.treeTable.getColumnModel().getColumn(((TreeTable)TreeTableView.this.treeTable).getTreeColumnIndex()).addPropertyChangeListener(TreeTableView.this.listener);
            } else if ("column_moved".equals(evt.getPropertyName())) {
                int from = (Integer)evt.getOldValue();
                int to = (Integer)evt.getNewValue();
                if (from == 0 || to == 0) {
                    if (this.movecorrection) {
                        this.movecorrection = false;
                    } else {
                        this.movecorrection = true;
                        TreeTableView.this.treeTable.getColumnModel().moveColumn(to, from);
                    }
                    return;
                }
                TreeTableView.this.treeTable.getTableHeader().getColumnModel().getColumn(from).setModelIndex(from);
                TreeTableView.this.treeTable.getTableHeader().getColumnModel().getColumn(to).setModelIndex(to);
                TreeTableView.this.tableModel.moveColumn(from - 1, to - 1);
            }
        }

        @Override
        public void componentResized(ComponentEvent e) {
            this.revalidateScrollBar();
        }

        @Override
        public void stateChanged(ChangeEvent evt) {
            int value = TreeTableView.this.hScrollBar.getModel().getValue();
            ((TreeTable)TreeTableView.this.treeTable).setPositionX(value);
        }

        private void revalidateScrollBar() {
            if (!TreeTableView.this.isDisplayable()) {
                return;
            }
            if (TreeTableView.this.treeTable.getColumnModel().getColumnCount() > 0 && ((TreeTable)TreeTableView.this.treeTable).getTreeColumnIndex() >= 0) {
                int extentWidth = TreeTableView.this.treeTable.getColumnModel().getColumn(((TreeTable)TreeTableView.this.treeTable).getTreeColumnIndex()).getWidth();
                int maxWidth = TreeTableView.this.tree.getPreferredSize().width;
                int extentHeight = TreeTableView.access$1200((TreeTableView)TreeTableView.this).getViewport().getSize().height;
                int maxHeight = TreeTableView.this.tree.getPreferredSize().height;
                int positionX = ((TreeTable)TreeTableView.this.treeTable).getPositionX();
                int value = Math.max(0, Math.min(positionX, maxWidth - extentWidth));
                boolean hsbvisible = TreeTableView.this.hScrollBar.isVisible();
                boolean vsbvisible = TreeTableView.this.scrollPane.getVerticalScrollBar().isVisible();
                int hsbheight = hsbvisible ? TreeTableView.this.hScrollBar.getHeight() : 0;
                int vsbwidth = TreeTableView.this.scrollPane.getVerticalScrollBar().getWidth();
                TreeTableView.this.hScrollBar.setValues(value, extentWidth, 0, maxWidth);
                if (TreeTableView.this.hideHScrollBar || maxWidth <= extentWidth || vsbvisible && maxHeight <= extentHeight + hsbheight && maxWidth <= extentWidth + vsbwidth) {
                    TreeTableView.this.hScrollBar.setVisible(false);
                } else {
                    TreeTableView.this.hScrollBar.setVisible(true);
                }
            }
        }
    }

    private class AccessibleTreeTableView
    extends JScrollPane.AccessibleJScrollPane {
        AccessibleTreeTableView() {
            super(TreeTableView.this);
        }

        @Override
        public void setAccessibleName(String accessibleName) {
            super.setAccessibleName(accessibleName);
            if (TreeTableView.this.treeTable != null) {
                TreeTableView.this.treeTable.getAccessibleContext().setAccessibleName(accessibleName);
            }
        }

        @Override
        public void setAccessibleDescription(String accessibleDescription) {
            super.setAccessibleDescription(accessibleDescription);
            if (TreeTableView.this.treeTable != null) {
                TreeTableView.this.treeTable.getAccessibleContext().setAccessibleDescription(accessibleDescription);
            }
        }
    }

}

