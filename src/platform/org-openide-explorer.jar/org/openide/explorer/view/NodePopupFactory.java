/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.etable.ETable
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeOp
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.openide.explorer.view;

import java.awt.Component;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import org.netbeans.swing.etable.ETable;
import org.openide.nodes.Node;
import org.openide.nodes.NodeOp;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class NodePopupFactory {
    private boolean showQuickFilter = true;

    public JPopupMenu createPopupMenu(int row, int column, Node[] selectedNodes, Component component) {
        Action[] actions = NodeOp.findActions((Node[])selectedNodes);
        JPopupMenu res = Utilities.actionsToPopup((Action[])actions, (Component)component);
        if (this.showQuickFilter && component instanceof ETable && column >= 0) {
            ETable et = (ETable)component;
            if (row >= 0) {
                Object val = et.getValueAt(row, column);
                val = et.transformValue(val);
                String s = NbBundle.getMessage(NodePopupFactory.class, (String)"LBL_QuickFilter");
                res.add(et.getQuickFilterPopup(column, val, s));
            } else if (et.getQuickFilterColumn() == column) {
                this.addNoFilterItem(et, res);
            }
        }
        return res;
    }

    void addNoFilterItem(ETable et, JPopupMenu popup) {
        if (this.showQuickFilter && et.getQuickFilterColumn() != -1) {
            String s = NbBundle.getMessage(NodePopupFactory.class, (String)"LBL_QuickFilter");
            JMenu menu = new JMenu(s);
            JMenuItem noFilterItem = et.getQuickFilterNoFilterItem(et.getQuickFilterFormatStrings()[6]);
            menu.add(noFilterItem);
            popup.add(menu);
        }
    }

    public void setShowQuickFilter(boolean show) {
        this.showQuickFilter = show;
    }
}

