/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.MouseUtils
 *  org.openide.awt.MouseUtils$PopupMouseAdapter
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeOp
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.CallbackSystemAction
 */
package org.openide.explorer.view;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.dnd.Autoscroll;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.MenuElement;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.Document;
import javax.swing.text.Position;
import org.openide.awt.MouseUtils;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.AutoscrollSupport;
import org.openide.explorer.view.DragDropUtilities;
import org.openide.explorer.view.ListViewDragSupport;
import org.openide.explorer.view.ListViewDropSupport;
import org.openide.explorer.view.NodeListModel;
import org.openide.explorer.view.NodeRenderer;
import org.openide.explorer.view.TreeView;
import org.openide.explorer.view.ViewTooltips;
import org.openide.explorer.view.Visualizer;
import org.openide.explorer.view.VisualizerNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeOp;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.actions.CallbackSystemAction;

public class ListView
extends JScrollPane
implements Externalizable {
    static final long serialVersionUID = -7540940974042262975L;
    private transient ExplorerManager manager;
    protected transient JList list;
    protected transient NodeListModel model;
    transient Listener managerListener;
    transient PropertyChangeListener wlpc;
    transient VetoableChangeListener wlvc;
    transient PopupSupport popupSupport;
    private boolean popupAllowed = true;
    private boolean traversalAllowed = true;
    private boolean showParentNode;
    private ActionListener defaultProcessor;
    transient boolean dragActive = false;
    transient boolean dropActive = false;
    transient ListViewDragSupport dragSupport;
    transient ListViewDropSupport dropSupport;
    private transient int allowedDragActions = 1073741827;
    private transient int allowedDropActions = 1073741827;
    transient boolean listenerActive;

    public ListView() {
        this.initializeList();
        this.setDropTarget(DragDropUtilities.dragAndDropEnabled);
        this.setBorder(BorderFactory.createEmptyBorder());
        this.setViewportBorder(BorderFactory.createEmptyBorder());
    }

    private void initializeList() {
        this.model = this.createModel();
        this.list = this.createList();
        this.list.setModel(this.model);
        this.setViewportView(this.list);
        AbstractAction action = new GoUpAction();
        KeyStroke key = KeyStroke.getKeyStroke(8, 0);
        this.list.registerKeyboardAction(action, key, 0);
        action = new EnterAction();
        key = KeyStroke.getKeyStroke(10, 0);
        this.list.registerKeyboardAction(action, key, 0);
        this.managerListener = new Listener();
        this.popupSupport = new PopupSupport();
        this.list.getActionMap().put("org.openide.actions.PopupAction", this.popupSupport);
        this.list.getSelectionModel().setSelectionMode(2);
        ToolTipManager.sharedInstance().registerComponent(this.list);
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(this.popupAllowed ? Boolean.TRUE : Boolean.FALSE);
        out.writeObject(this.traversalAllowed ? Boolean.TRUE : Boolean.FALSE);
        out.writeObject(new Integer(this.getSelectionMode()));
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        this.popupAllowed = (Boolean)in.readObject();
        this.traversalAllowed = (Boolean)in.readObject();
        this.setSelectionMode((Integer)in.readObject());
    }

    public boolean isPopupAllowed() {
        return this.popupAllowed;
    }

    public void setPopupAllowed(boolean value) {
        this.popupAllowed = value;
    }

    public boolean isTraversalAllowed() {
        return this.traversalAllowed;
    }

    public void setTraversalAllowed(boolean value) {
        this.traversalAllowed = value;
    }

    public boolean isShowParentNode() {
        return this.showParentNode;
    }

    public void setShowParentNode(boolean show) {
        this.showParentNode = show;
    }

    public ActionListener getDefaultProcessor() {
        return this.defaultProcessor;
    }

    public void setDefaultProcessor(ActionListener value) {
        this.defaultProcessor = value;
    }

    public void setSelectionMode(int selectionMode) {
        this.list.setSelectionMode(selectionMode);
    }

    public int getSelectionMode() {
        return this.list.getSelectionMode();
    }

    public boolean isDragSource() {
        return this.dragActive;
    }

    public void setDragSource(boolean state) {
        if (state == this.dragActive) {
            return;
        }
        this.dragActive = state;
        if (this.dragActive && this.dragSupport == null) {
            this.dragSupport = new ListViewDragSupport(this, this.list);
        }
        this.dragSupport.activate(this.dragActive);
    }

    public boolean isDropTarget() {
        return this.dropActive;
    }

    public void setDropTarget(boolean state) {
        if (state == this.dropActive) {
            return;
        }
        this.dropActive = state;
        if (this.dropActive && this.dropSupport == null) {
            this.dropSupport = new ListViewDropSupport(this, this.list);
        }
        this.dropSupport.activate(this.dropActive);
    }

    public int getAllowedDragActions() {
        return this.allowedDragActions;
    }

    public void setAllowedDragActions(int actions) {
        this.allowedDragActions = actions;
    }

    public int getAllowedDropActions() {
        return this.allowedDropActions;
    }

    public void setAllowedDropActions(int actions) {
        this.allowedDropActions = actions;
    }

    protected JList createList() {
        NbList list = new NbList();
        list.setCellRenderer(new NodeRenderer());
        return list;
    }

    protected NodeListModel createModel() {
        return new NodeListModel();
    }

    protected void selectionChanged(Node[] nodes, ExplorerManager em) throws PropertyVetoException {
        em.setSelectedNodes(nodes);
    }

    protected boolean selectionAccept(Node[] nodes) {
        if (nodes.length == 1 && this.manager.getRootContext().equals((Object)nodes[0])) {
            return true;
        }
        for (int i = 0; i < nodes.length; ++i) {
            VisualizerNode v = VisualizerNode.getVisualizer(null, nodes[i]);
            if (this.model.getIndex(v) != -1) continue;
            return false;
        }
        return true;
    }

    protected void showSelection(int[] indexes) {
        this.list.setSelectedIndices(indexes);
    }

    @Override
    public void paint(Graphics g) {
        new GuardedActions(0, g);
    }

    @Override
    protected void validateTree() {
        new GuardedActions(1, null);
    }

    @Override
    public Dimension getPreferredSize() {
        return (Dimension)(ListView)this.new GuardedActions((int)5, (Object)null).ret;
    }

    @Override
    public void doLayout() {
        new GuardedActions(2, null);
    }

    final void setNode(Node n) {
        boolean show = this.showParentNode && n != this.manager.getRootContext();
        this.model.setNode(n, show);
    }

    @Override
    public void addNotify() {
        super.addNotify();
        ExplorerManager em = ExplorerManager.find(this);
        if (em != this.manager) {
            if (this.manager != null) {
                this.manager.removeVetoableChangeListener(this.wlvc);
                this.manager.removePropertyChangeListener(this.wlpc);
            }
            this.manager = em;
            this.wlvc = WeakListeners.vetoableChange((VetoableChangeListener)this.managerListener, (Object)this.manager);
            this.manager.addVetoableChangeListener(this.wlvc);
            this.wlpc = WeakListeners.propertyChange((PropertyChangeListener)this.managerListener, (Object)this.manager);
            this.manager.addPropertyChangeListener(this.wlpc);
            this.setNode(this.manager.getExploredContext());
            this.updateSelection();
        } else if (!this.listenerActive && this.manager != null) {
            this.wlvc = WeakListeners.vetoableChange((VetoableChangeListener)this.managerListener, (Object)this.manager);
            this.manager.addVetoableChangeListener(this.wlvc);
            this.wlpc = WeakListeners.propertyChange((PropertyChangeListener)this.managerListener, (Object)this.manager);
            this.manager.addPropertyChangeListener(this.wlpc);
        }
        if (!this.listenerActive) {
            this.listenerActive = true;
            this.list.getSelectionModel().addListSelectionListener(this.managerListener);
            this.model.addListDataListener(this.managerListener);
            this.setNode(this.manager.getExploredContext());
            this.list.addMouseListener((MouseListener)((Object)this.popupSupport));
        }
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.listenerActive = false;
        this.list.getSelectionModel().removeListSelectionListener(this.managerListener);
        if (this.manager != null) {
            this.manager.removeVetoableChangeListener(this.wlvc);
            this.manager.removePropertyChangeListener(this.wlpc);
        }
        this.model.removeListDataListener(this.managerListener);
        this.list.removeMouseListener((MouseListener)((Object)this.popupSupport));
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                if (!ListView.this.isDisplayable()) {
                    ListView.this.model.setNode(Node.EMPTY);
                }
            }
        });
    }

    @Override
    public void requestFocus() {
        this.list.requestFocus();
    }

    @Override
    public boolean requestFocusInWindow() {
        return this.list.requestFocusInWindow();
    }

    final void performObjectAt(int index, int modifiers) {
        if (index < 0 || index >= this.model.getSize()) {
            return;
        }
        VisualizerNode v = (VisualizerNode)this.model.getElementAt(index);
        Node node = v.node;
        if (this.defaultProcessor != null) {
            this.defaultProcessor.actionPerformed(new ActionEvent((Object)node, 0, null, modifiers));
            return;
        }
        if (this.showParentNode && NodeListModel.findVisualizerDepth(this.model, v) == -1) {
            try {
                this.manager.setExploredContextAndSelection(node.getParentNode(), new Node[]{node});
            }
            catch (PropertyVetoException ex) {
                // empty catch block
            }
            return;
        }
        Action a = node.getPreferredAction();
        if (a != null && (modifiers & 2) == 0) {
            if ((a = TreeView.takeAction(a, node)).isEnabled()) {
                a.actionPerformed(new ActionEvent((Object)node, 1001, ""));
            } else {
                Utilities.disabledActionBeep();
            }
        } else if (this.traversalAllowed && !node.isLeaf()) {
            this.manager.setExploredContext(node, this.manager.getSelectedNodes());
        }
    }

    private void updateSelection() {
        new GuardedActions(6, null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void updateSelectionImpl() {
        Node[] sel = this.manager.getSelectedNodes();
        int[] indices = new int[sel.length];
        int firstVisible = this.list.getFirstVisibleIndex();
        int lastVisible = this.list.getLastVisibleIndex();
        boolean ensureVisible = indices.length > 0;
        for (int i = 0; i < sel.length; ++i) {
            VisualizerNode v = VisualizerNode.getVisualizer(null, sel[i]);
            indices[i] = this.model.getIndex(v);
            ensureVisible = ensureVisible && (indices[i] < firstVisible || indices[i] > lastVisible);
        }
        if (this.listenerActive) {
            this.list.getSelectionModel().removeListSelectionListener(this.managerListener);
        }
        try {
            this.showSelection(indices);
            if (ensureVisible) {
                this.list.ensureIndexIsVisible(indices[0]);
            }
        }
        finally {
            if (this.listenerActive) {
                this.list.getSelectionModel().addListSelectionListener(this.managerListener);
            }
        }
    }

    void createPopup(int xpos, int ypos, boolean contextMenu) {
        JPopupMenu popup;
        if (this.manager == null) {
            return;
        }
        if (!this.popupAllowed) {
            return;
        }
        if (!this.isShowing()) {
            return;
        }
        if (contextMenu) {
            popup = Utilities.actionsToPopup((Action[])this.manager.getExploredContext().getActions(true), (Component)this);
        } else {
            Action[] actions = NodeOp.findActions((Node[])this.manager.getSelectedNodes());
            popup = Utilities.actionsToPopup((Action[])actions, (Component)this);
        }
        if (popup != null && popup.getSubElements().length > 0) {
            Point p = this.getViewport().getViewPosition();
            p.x = xpos - p.x;
            p.y = ypos - p.y;
            SwingUtilities.convertPointToScreen(p, this);
            Dimension popupSize = popup.getPreferredSize();
            Rectangle screenBounds = Utilities.getUsableScreenBounds((GraphicsConfiguration)this.getGraphicsConfiguration());
            if (p.x + popupSize.width > screenBounds.x + screenBounds.width) {
                p.x = screenBounds.x + screenBounds.width - popupSize.width;
            }
            if (p.y + popupSize.height > screenBounds.y + screenBounds.height) {
                p.y = screenBounds.y + screenBounds.height - popupSize.height;
            }
            SwingUtilities.convertPointFromScreen(p, this);
            popup.show(this, p.x, p.y);
        }
    }

    private final class EnterAction
    extends AbstractAction {
        static final long serialVersionUID = -239805141416294016L;

        public EnterAction() {
            super("Enter");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int index = ListView.this.list.getSelectedIndex();
            ListView.this.performObjectAt(index, e.getModifiers());
        }

        @Override
        public boolean isEnabled() {
            return true;
        }
    }

    private final class GoUpAction
    extends AbstractAction {
        static final long serialVersionUID = 1599999335583246715L;

        public GoUpAction() {
            super("GoUpAction");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (ListView.this.traversalAllowed) {
                Node pan = ListView.this.manager.getExploredContext();
                if ((pan = pan.getParentNode()) != null) {
                    ListView.this.manager.setExploredContext(pan, ListView.this.manager.getSelectedNodes());
                }
            }
        }

        @Override
        public boolean isEnabled() {
            return true;
        }
    }

    private final class Listener
    implements ListDataListener,
    ListSelectionListener,
    PropertyChangeListener,
    VetoableChangeListener {
        Listener() {
        }

        @Override
        public void intervalAdded(ListDataEvent evt) {
            ListView.this.updateSelection();
        }

        @Override
        public void intervalRemoved(ListDataEvent evt) {
            ListView.this.updateSelection();
        }

        @Override
        public void contentsChanged(ListDataEvent evt) {
            ListView.this.updateSelection();
        }

        @Override
        public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
            Node[] newNodes;
            if ("selectedNodes".equals(evt.getPropertyName()) && !ListView.this.selectionAccept(newNodes = (Node[])evt.getNewValue())) {
                throw new PropertyVetoException("", evt);
            }
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("selectedNodes".equals(evt.getPropertyName())) {
                ListView.this.updateSelection();
                return;
            }
            if ("exploredContext".equals(evt.getPropertyName())) {
                ListView.this.setNode(ListView.this.manager.getExploredContext());
                return;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void valueChanged(ListSelectionEvent e) {
            int curSize = ListView.this.model.getSize();
            int[] indices = ListView.this.list.getSelectedIndices();
            ArrayList<Node> ll = new ArrayList<Node>(indices.length);
            for (int i = 0; i < indices.length; ++i) {
                if (indices[i] < curSize) {
                    Node n = Visualizer.findNode(ListView.this.model.getElementAt(indices[i]));
                    if (n != ListView.this.manager.getRootContext() && n.getParentNode() == null) continue;
                    ll.add(n);
                    continue;
                }
                ListView.this.updateSelection();
                return;
            }
            Node[] nodes = ll.toArray((T[])new Node[ll.size()]);
            ListView.this.manager.removePropertyChangeListener(ListView.this.wlpc);
            ListView.this.manager.removeVetoableChangeListener(ListView.this.wlvc);
            try {
                ListView.this.selectionChanged(nodes, ListView.this.manager);
            }
            catch (PropertyVetoException ex) {
                ListView.this.updateSelection();
            }
            finally {
                ListView.this.manager.addPropertyChangeListener(ListView.this.wlpc);
                ListView.this.manager.addVetoableChangeListener(ListView.this.wlvc);
            }
        }
    }

    private final class PopupSupport
    extends MouseUtils.PopupMouseAdapter
    implements Action,
    Runnable {
        CallbackSystemAction csa;

        public void mouseClicked(MouseEvent e) {
            if (MouseUtils.isDoubleClick((MouseEvent)e)) {
                int index = ListView.this.list.locationToIndex(e.getPoint());
                ListView.this.performObjectAt(index, e.getModifiers());
            }
        }

        protected void showPopup(MouseEvent e) {
            Rectangle r;
            Point p = new Point(e.getX(), e.getY());
            int i = ListView.this.list.locationToIndex(p);
            if (!ListView.this.list.isSelectedIndex(i)) {
                ListView.this.list.setSelectedIndex(i);
            }
            boolean contextMenu = (r = ListView.this.list.getCellBounds(i, i)) == null || !r.contains(p);
            ListView.this.createPopup(e.getX(), e.getY(), contextMenu);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            SwingUtilities.invokeLater(this);
        }

        @Override
        public void run() {
            int i;
            boolean multisel = ListView.this.list.getSelectionMode() != 0;
            int n = i = multisel ? ListView.this.list.getLeadSelectionIndex() : ListView.this.list.getSelectedIndex();
            if (i < 0) {
                return;
            }
            Point p = ListView.this.list.indexToLocation(i);
            if (p == null) {
                return;
            }
            ListView.this.createPopup(p.x, p.y, false);
        }

        @Override
        public Object getValue(String key) {
            return null;
        }

        @Override
        public void putValue(String key, Object value) {
        }

        @Override
        public void setEnabled(boolean b) {
        }

        @Override
        public boolean isEnabled() {
            return true;
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener listener) {
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener listener) {
        }
    }

    final class NbList
    extends JList
    implements Autoscroll {
        static final long serialVersionUID = -7571829536335024077L;
        AutoscrollSupport support;
        int SEARCH_FIELD_PREFERRED_SIZE;
        int SEARCH_FIELD_SPACE;
        private String maxPrefix;
        private JTextField searchTextField;
        private final int heightOfTextField;
        private int originalScrollMode;
        private JPanel searchpanel;

        NbList() {
            this.SEARCH_FIELD_PREFERRED_SIZE = 160;
            this.SEARCH_FIELD_SPACE = 3;
            this.searchTextField = new JTextField(){

                @Override
                public boolean isManagingFocus() {
                    return true;
                }

                @Override
                public void processKeyEvent(KeyEvent ke) {
                    if (ke.getKeyCode() == 27) {
                        NbList.this.removeSearchField();
                        ke.consume();
                        NbList.this.requestFocus();
                    } else {
                        super.processKeyEvent(ke);
                    }
                }
            };
            this.heightOfTextField = this.searchTextField.getPreferredSize().height;
            this.searchpanel = null;
            this.getInputMap().put(KeyStroke.getKeyStroke("control C"), "none");
            this.getInputMap().put(KeyStroke.getKeyStroke("control V"), "none");
            this.getInputMap().put(KeyStroke.getKeyStroke("control X"), "none");
            this.setupSearch();
        }

        @Override
        public void addNotify() {
            super.addNotify();
            ViewTooltips.register(this);
        }

        @Override
        public void removeNotify() {
            super.removeNotify();
            ViewTooltips.unregister(this);
        }

        @Override
        protected void processFocusEvent(FocusEvent fe) {
            super.processFocusEvent(fe);
            this.repaintSelection();
        }

        @Override
        public void paint(Graphics g) {
            new GuardedActions(0, g);
        }

        @Override
        protected void validateTree() {
            new GuardedActions(1, null);
        }

        @Override
        public Dimension getPreferredSize() {
            return (Dimension)(NbList)this.new GuardedActions((int)5, (Object)null).ret;
        }

        @Override
        public Point indexToLocation(int index) {
            return (Point)(NbList)this.new GuardedActions((int)7, (Object)Integer.valueOf((int)index)).ret;
        }

        @Override
        public int locationToIndex(Point location) {
            return (Integer)(NbList)this.new GuardedActions((int)8, (Object)location).ret;
        }

        @Override
        public Object[] getSelectedValues() {
            return (Object[])(NbList)this.new GuardedActions((int)9, (Object)null).ret;
        }

        private void repaintSelection() {
            if (Children.MUTEX.isReadAccess() || Children.MUTEX.isWriteAccess()) {
                int[] idx = this.getSelectedIndices();
                if (idx.length == 0) {
                    return;
                }
                for (int i = 0; i < idx.length; ++i) {
                    Rectangle r = this.getCellBounds(idx[i], idx[i]);
                    this.repaint(r.x, r.y, r.width, r.height);
                }
            } else {
                new GuardedActions(3, null);
            }
        }

        @Override
        public String getToolTipText(MouseEvent event) {
            return (String)(NbList)this.new GuardedActions((int)6, (Object)event).ret;
        }

        final String getToolTipTextImpl(MouseEvent event) {
            Point p;
            int row;
            if (event != null && (row = this.locationToIndex(p = event.getPoint())) >= 0) {
                VisualizerNode v = (VisualizerNode)ListView.this.model.getElementAt(row);
                String tooltip = v.getShortDescription();
                String displayName = v.getDisplayName();
                if (tooltip != null && !tooltip.equals(displayName)) {
                    return tooltip;
                }
            }
            return null;
        }

        @Override
        public void autoscroll(Point cursorLoc) {
            this.getSupport().autoscroll(cursorLoc);
        }

        @Override
        public Insets getAutoscrollInsets() {
            return this.getSupport().getAutoscrollInsets();
        }

        AutoscrollSupport getSupport() {
            if (this.support == null) {
                this.support = new AutoscrollSupport(this, new Insets(15, 10, 15, 10));
            }
            return this.support;
        }

        @Override
        public AccessibleContext getAccessibleContext() {
            if (this.accessibleContext == null) {
                this.accessibleContext = new AccessibleExplorerList();
            }
            return this.accessibleContext;
        }

        private void setupSearch() {
            KeyListener[] keyListeners = (KeyListener[])this.getListeners(KeyListener.class);
            for (int i = 0; i < keyListeners.length; ++i) {
                this.removeKeyListener(keyListeners[i]);
            }
            this.addKeyListener(new KeyAdapter(){

                @Override
                public void keyPressed(KeyEvent e) {
                    int modifiers = e.getModifiers();
                    int keyCode = e.getKeyCode();
                    if (modifiers > 0 && modifiers != 1 || e.isActionKey()) {
                        return;
                    }
                    char c = e.getKeyChar();
                    if (!Character.isISOControl(c) && keyCode != 16) {
                        NbList.this.searchTextField.setText(String.valueOf(c));
                        NbList.this.displaySearchField();
                    }
                }
            });
            SearchFieldListener searchFieldListener = new SearchFieldListener();
            this.searchTextField.addKeyListener(searchFieldListener);
            this.searchTextField.addFocusListener(searchFieldListener);
            this.searchTextField.getDocument().addDocumentListener(searchFieldListener);
        }

        private List<Integer> doSearch(String prefix) {
            ArrayList<Integer> results = new ArrayList<Integer>();
            int startIndex = this.getSelectedIndex() == -1 ? 0 : this.getSelectedIndex();
            int size = this.getModel().getSize();
            if (size == 0) {
                return results;
            }
            do {
                startIndex %= size;
                if ((startIndex = this.getNextMatch(prefix, startIndex, Position.Bias.Forward)) == -1 || results.contains(new Integer(startIndex))) break;
                results.add(startIndex);
                String elementName = this.getModel().getElementAt(startIndex).toString();
                if (this.maxPrefix == null) {
                    this.maxPrefix = elementName;
                }
                this.maxPrefix = this.findMaxPrefix(this.maxPrefix, elementName);
                ++startIndex;
            } while (true);
            return results;
        }

        private String findMaxPrefix(String str1, String str2) {
            String res = null;
            int i = 0;
            while (str1.regionMatches(true, 0, str2, 0, i)) {
                res = str1.substring(0, i);
                ++i;
            }
            return res;
        }

        private void prepareSearchPanel() {
            if (this.searchpanel == null) {
                this.searchpanel = new JPanel();
                JLabel lbl = new JLabel(NbBundle.getMessage(TreeView.class, (String)"LBL_QUICKSEARCH"));
                this.searchpanel.setLayout(new BorderLayout(5, 0));
                this.searchpanel.add((Component)lbl, "West");
                this.searchpanel.add((Component)this.searchTextField, "Center");
                lbl.setLabelFor(this.searchTextField);
                this.searchpanel.setBorder(BorderFactory.createRaisedBevelBorder());
            }
        }

        private void displaySearchField() {
            if (this.getModel().getSize() > 0 && !this.searchTextField.isDisplayable()) {
                JViewport viewport = ListView.this.getViewport();
                this.originalScrollMode = viewport.getScrollMode();
                viewport.setScrollMode(0);
                this.prepareSearchPanel();
                this.add(this.searchpanel);
                this.revalidate();
                this.repaint();
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        NbList.this.searchTextField.requestFocus();
                    }
                });
            }
        }

        @Override
        public void doLayout() {
            new GuardedActions(2, null);
        }

        final void doLayoutImpl() {
            super.doLayout();
            if (this.searchpanel != null && this.searchpanel.isDisplayable()) {
                Rectangle visibleRect = this.getVisibleRect();
                int width = Math.min(visibleRect.width - this.SEARCH_FIELD_SPACE * 2, this.SEARCH_FIELD_PREFERRED_SIZE - this.SEARCH_FIELD_SPACE);
                int height = this.heightOfTextField + this.searchpanel.getInsets().top + this.searchpanel.getInsets().bottom;
                this.searchpanel.setBounds(Math.max(this.SEARCH_FIELD_SPACE, visibleRect.x + visibleRect.width - width), visibleRect.y + this.SEARCH_FIELD_SPACE, Math.min(visibleRect.width, width) - this.SEARCH_FIELD_SPACE, height);
            }
        }

        private void removeSearchField() {
            if (this.searchpanel != null && this.searchpanel.isDisplayable()) {
                this.remove(this.searchpanel);
                ListView.this.getViewport().setScrollMode(this.originalScrollMode);
                this.repaint(this.searchpanel.getBounds());
                this.requestFocus();
            }
        }

        private class SearchFieldListener
        extends KeyAdapter
        implements DocumentListener,
        FocusListener {
            private List results;
            private int currentSelectionIndex;

            SearchFieldListener() {
                this.results = new ArrayList();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                this.searchForNode();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                this.searchForNode();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                this.searchForNode();
            }

            @Override
            public void keyPressed(KeyEvent e) {
                int keyCode = e.getKeyCode();
                if (keyCode == 27) {
                    NbList.this.removeSearchField();
                    NbList.this.requestFocus();
                } else if (keyCode == 38) {
                    --this.currentSelectionIndex;
                    this.displaySearchResult();
                    e.consume();
                } else if (keyCode == 40) {
                    ++this.currentSelectionIndex;
                    this.displaySearchResult();
                    e.consume();
                } else if (keyCode == 9) {
                    if (NbList.this.maxPrefix != null) {
                        NbList.this.searchTextField.setText(NbList.this.maxPrefix);
                    }
                    e.consume();
                } else if (keyCode == 10) {
                    NbList.this.removeSearchField();
                    NbList.this.requestFocus();
                    NbList.this.dispatchEvent(e);
                }
            }

            private void searchForNode() {
                this.currentSelectionIndex = 0;
                this.results.clear();
                NbList.this.maxPrefix = null;
                String text = NbList.this.searchTextField.getText();
                if (text.length() > 0) {
                    this.results = NbList.this.doSearch(text);
                    this.displaySearchResult();
                }
            }

            private void displaySearchResult() {
                int sz = this.results.size();
                if (sz > 0) {
                    if (this.currentSelectionIndex < 0) {
                        this.currentSelectionIndex = sz - 1;
                    } else if (this.currentSelectionIndex >= sz) {
                        this.currentSelectionIndex = 0;
                    }
                    Integer index = (Integer)this.results.get(this.currentSelectionIndex);
                    ListView.this.list.setSelectedIndex(index);
                    ListView.this.list.ensureIndexIsVisible(index);
                } else {
                    ListView.this.list.clearSelection();
                }
            }

            @Override
            public void focusGained(FocusEvent e) {
                NbList.this.searchTextField.select(1, 1);
            }

            @Override
            public void focusLost(FocusEvent e) {
                NbList.this.removeSearchField();
            }
        }

        private class AccessibleExplorerList
        extends JList.AccessibleJList {
            AccessibleExplorerList() {
                super(NbList.this);
            }

            @Override
            public String getAccessibleName() {
                return ListView.this.getAccessibleContext().getAccessibleName();
            }

            @Override
            public String getAccessibleDescription() {
                return ListView.this.getAccessibleContext().getAccessibleDescription();
            }
        }

        private class GuardedActions
        implements Mutex.Action<Object> {
            private int type;
            private Object p1;
            final Object ret;

            public GuardedActions(int type, Object p1) {
                this.type = type;
                this.p1 = p1;
                this.ret = Children.MUTEX.isReadAccess() || Children.MUTEX.isWriteAccess() ? this.run() : Children.MUTEX.readAccess((Mutex.Action)this);
            }

            public Object run() {
                switch (this.type) {
                    case 0: {
                        NbList.this.paint((Graphics)this.p1);
                        break;
                    }
                    case 1: {
                        NbList.this.validateTree();
                        break;
                    }
                    case 2: {
                        NbList.this.doLayoutImpl();
                        break;
                    }
                    case 3: {
                        NbList.this.repaintSelection();
                        break;
                    }
                    case 4: {
                        NbList.this.processEvent((AWTEvent)this.p1);
                        break;
                    }
                    case 5: {
                        return NbList.this.getPreferredSize();
                    }
                    case 6: {
                        return NbList.this.getToolTipTextImpl((MouseEvent)this.p1);
                    }
                    case 7: {
                        return NbList.this.indexToLocation((Integer)this.p1);
                    }
                    case 8: {
                        return NbList.this.locationToIndex((Point)this.p1);
                    }
                    case 9: {
                        return NbList.this.getSelectedValues();
                    }
                    case 10: {
                        Object[] arr = (Object[])this.p1;
                        return NbList.this.processKeyBinding((KeyStroke)arr[0], (KeyEvent)arr[1], (Integer)arr[2], (Boolean)arr[3]);
                    }
                    default: {
                        throw new IllegalStateException("type: " + this.type);
                    }
                }
                return null;
            }
        }

    }

    private class GuardedActions
    implements Mutex.Action<Object> {
        private int type;
        private Object p1;
        final Object ret;

        public GuardedActions(int type, Object p1) {
            this.type = type;
            this.p1 = p1;
            this.ret = Children.MUTEX.isReadAccess() || Children.MUTEX.isWriteAccess() ? this.run() : Children.MUTEX.readAccess((Mutex.Action)this);
        }

        public Object run() {
            switch (this.type) {
                case 0: {
                    ListView.this.paint((Graphics)this.p1);
                    break;
                }
                case 1: {
                    ListView.this.validateTree();
                    break;
                }
                case 2: {
                    ListView.this.doLayout();
                    break;
                }
                case 4: {
                    ListView.this.processEvent((AWTEvent)this.p1);
                    break;
                }
                case 5: {
                    return ListView.this.getPreferredSize();
                }
                case 6: {
                    ListView.this.updateSelectionImpl();
                    break;
                }
                default: {
                    throw new IllegalStateException("type: " + this.type);
                }
            }
            return null;
        }
    }

}

