/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package org.openide.explorer.view;

import java.awt.datatransfer.Transferable;
import org.openide.nodes.Node;

final class ExplorerDnDManager {
    private static ExplorerDnDManager defaultDnDManager;
    private Node[] draggedNodes;
    private Transferable draggedTransForCut;
    private Transferable draggedTransForCopy;
    private boolean isDnDActive = false;
    private int nodeAllowed = 0;
    private boolean maybeExternalDnD = false;

    private ExplorerDnDManager() {
    }

    static synchronized ExplorerDnDManager getDefault() {
        if (defaultDnDManager == null) {
            defaultDnDManager = new ExplorerDnDManager();
        }
        return defaultDnDManager;
    }

    void setDraggedNodes(Node[] n) {
        this.draggedNodes = n;
    }

    Node[] getDraggedNodes() {
        return this.draggedNodes;
    }

    void setDraggedTransferable(Transferable trans, boolean isCut) {
        if (isCut) {
            this.draggedTransForCut = trans;
        } else {
            this.draggedTransForCopy = trans;
        }
    }

    Transferable getDraggedTransferable(boolean isCut) {
        if (isCut) {
            return this.draggedTransForCut;
        }
        return this.draggedTransForCopy;
    }

    void setNodeAllowedActions(int actions) {
        this.nodeAllowed = actions;
    }

    final int getNodeAllowedActions() {
        if (!this.isDnDActive && this.maybeExternalDnD) {
            return 3;
        }
        return this.nodeAllowed;
    }

    void setDnDActive(boolean state) {
        this.isDnDActive = state;
    }

    boolean isDnDActive() {
        return this.isDnDActive || this.maybeExternalDnD;
    }

    int getAdjustedDropAction(int action, int allowed) {
        int possibleAction = action;
        if ((possibleAction & allowed) == 0) {
            possibleAction = allowed;
        }
        if ((possibleAction & this.getNodeAllowedActions()) == 0) {
            possibleAction = this.getNodeAllowedActions();
        }
        return possibleAction;
    }

    void setMaybeExternalDragAndDrop(boolean maybeExternalDnD) {
        this.maybeExternalDnD = maybeExternalDnD;
    }
}

