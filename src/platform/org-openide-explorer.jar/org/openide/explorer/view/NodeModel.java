/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.view;

import java.util.EventListener;
import org.openide.explorer.view.VisualizerEvent;
import org.openide.explorer.view.VisualizerNode;

interface NodeModel
extends EventListener {
    public void added(VisualizerEvent.Added var1);

    public void removed(VisualizerEvent.Removed var1);

    public void reordered(VisualizerEvent.Reordered var1);

    public void update(VisualizerNode var1);

    public void structuralChange(VisualizerNode var1);
}

