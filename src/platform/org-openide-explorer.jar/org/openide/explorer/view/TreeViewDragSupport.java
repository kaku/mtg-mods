/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package org.openide.explorer.view;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragSourceDropEvent;
import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreePath;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.DragDropUtilities;
import org.openide.explorer.view.DropGlassPane;
import org.openide.explorer.view.ExplorerDnDManager;
import org.openide.explorer.view.ExplorerDragSupport;
import org.openide.explorer.view.TreeView;
import org.openide.explorer.view.TreeViewCellEditor;
import org.openide.nodes.Node;

final class TreeViewDragSupport
extends ExplorerDragSupport {
    protected TreeView view;
    private JTree tree;

    public TreeViewDragSupport(TreeView view, JTree tree) {
        this.view = view;
        this.comp = tree;
        this.tree = tree;
    }

    @Override
    public int getAllowedDragActions() {
        return this.view.getAllowedDragActions();
    }

    @Override
    int getAllowedDropActions() {
        return this.view.getAllowedDropActions();
    }

    @Override
    public void dragGestureRecognized(DragGestureEvent dge) {
        TreeCellEditor tce;
        super.dragGestureRecognized(dge);
        if (this.exDnD.isDnDActive() && (tce = this.tree.getCellEditor()) instanceof TreeViewCellEditor) {
            ((TreeViewCellEditor)tce).setDnDActive(true);
        }
    }

    @Override
    public void dragDropEnd(DragSourceDropEvent dsde) {
        Node[] dropedNodes = this.exDnD.getDraggedNodes();
        super.dragDropEnd(dsde);
        if (DropGlassPane.isOriginalPaneStored()) {
            DropGlassPane.putBackOriginal();
            this.exDnD.setDnDActive(false);
        }
        try {
            ExplorerManager.Provider panel;
            if (dropedNodes != null && (panel = (ExplorerManager.Provider)((Object)SwingUtilities.getAncestorOfClass(ExplorerManager.Provider.class, this.view))) != null) {
                panel.getExplorerManager().setSelectedNodes(dropedNodes);
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        TreeCellEditor tce = this.tree.getCellEditor();
        if (tce instanceof TreeViewCellEditor) {
            ((TreeViewCellEditor)tce).setDnDActive(false);
        }
    }

    @Override
    Node[] obtainNodes(DragGestureEvent dge) {
        TreePath[] tps = this.tree.getSelectionPaths();
        if (tps == null) {
            return null;
        }
        Node[] result = new Node[tps.length];
        int cnt = 0;
        for (int i = 0; i < tps.length; ++i) {
            Rectangle r = this.tree.getPathBounds(tps[i]);
            if (r != null && r.contains(dge.getDragOrigin())) {
                ++cnt;
            }
            result[i] = DragDropUtilities.secureFindNode(tps[i].getLastPathComponent());
        }
        return cnt == 0 ? null : result;
    }
}

