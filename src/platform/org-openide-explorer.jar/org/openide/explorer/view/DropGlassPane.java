/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Parameters
 */
package org.openide.explorer.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.geom.Line2D;
import java.util.HashMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.openide.util.Parameters;

final class DropGlassPane
extends JPanel {
    private static HashMap<Integer, DropGlassPane> map = new HashMap();
    private static final int MIN_X = 5;
    private static final int MIN_Y = 3;
    private static final int MIN_WIDTH = 10;
    private static final int MIN_HEIGTH = 3;
    private static transient Component oldPane;
    private static transient JComponent originalSource;
    private static transient boolean wasVisible;
    Line2D line = null;

    private DropGlassPane() {
    }

    public static synchronized DropGlassPane getDefault(JComponent comp) {
        Integer id = new Integer(System.identityHashCode(comp));
        if (map.get(id) == null) {
            DropGlassPane dgp = new DropGlassPane();
            dgp.setOpaque(false);
            map.put(id, dgp);
        }
        return map.get(id);
    }

    static void setOriginalPane(JComponent source, Component pane, boolean visible) {
        if (oldPane != null) {
            throw new IllegalStateException("Original pane already present");
        }
        Parameters.notNull((CharSequence)"source", (Object)source);
        Parameters.notNull((CharSequence)"pane", (Object)pane);
        oldPane = pane;
        originalSource = source;
        wasVisible = visible;
    }

    static boolean isOriginalPaneStored() {
        return oldPane != null;
    }

    static void putBackOriginal() {
        if (oldPane == null) {
            throw new IllegalStateException("No original pane present");
        }
        JRootPane rp = originalSource.getRootPane();
        if (rp == null) {
            if (null != SwingUtilities.getWindowAncestor(originalSource)) {
                throw new IllegalStateException("originalSource " + originalSource + " has no root pane: " + rp);
            }
        } else {
            rp.setGlassPane(oldPane);
            oldPane.setVisible(wasVisible);
        }
        oldPane = null;
    }

    @Override
    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag);
        if (!aFlag) {
            this.setDropLine(null);
        }
    }

    public void setDropLine(Line2D line) {
        Line2D oldLine = this.line;
        this.line = line;
        if (null == oldLine && null != line || null != oldLine && null == line) {
            this.repaint();
        }
    }

    private Line2D checkLineBounds(Line2D line) {
        Rectangle bounds = this.getBounds();
        double startPointX = Math.max(line.getX1(), (double)(bounds.x + 5));
        double startPointY = Math.max(line.getY1(), (double)(bounds.y + 3));
        double endPointX = Math.min(line.getX2(), (double)(bounds.x + bounds.width - 10));
        double endPointY = Math.min(line.getY2(), (double)(bounds.y + bounds.height - 3));
        line.setLine(startPointX, startPointY, endPointX, endPointY);
        return line;
    }

    @Override
    public void paint(Graphics g) {
        if (this.line != null) {
            Color c = UIManager.getColor("Tree.dropLine");
            if (c != null) {
                g.setColor(c);
            }
            this.line = this.checkLineBounds(this.line);
            int x1 = (int)this.line.getX1();
            int x2 = (int)this.line.getX2();
            int y1 = (int)this.line.getY1();
            g.setColor(UIManager.getColor("Tree.selectionBackground"));
            g.drawLine(x1 + 2, y1, x2 - 2, y1);
            g.drawLine(x1 + 2, y1 + 1, x2 - 2, y1 + 1);
            g.drawLine(x1, y1 - 2, x1, y1 + 3);
            g.drawLine(x1 + 1, y1 - 1, x1 + 1, y1 + 2);
            g.drawLine(x2, y1 - 2, x2, y1 + 3);
            g.drawLine(x2 - 1, y1 - 1, x2 - 1, y1 + 2);
        }
    }
}

