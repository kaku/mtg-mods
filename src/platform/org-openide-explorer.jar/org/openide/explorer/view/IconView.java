/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.view;

import java.io.Externalizable;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import org.openide.explorer.view.IconPanel;
import org.openide.explorer.view.ListView;

public class IconView
extends ListView
implements Externalizable {
    static final long serialVersionUID = -9129850245819731264L;

    @Override
    protected JList createList() {
        ListView.NbList tmp = new ListView.NbList(this);
        tmp.setOpaque(false);
        tmp.setCellRenderer(new IconPanel());
        tmp.setLayoutOrientation(2);
        tmp.setVisibleRowCount(-1);
        return tmp;
    }
}

