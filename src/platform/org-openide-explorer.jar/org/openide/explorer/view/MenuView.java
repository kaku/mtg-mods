/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.JMenuPlus
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeAcceptor
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeMemberEvent
 *  org.openide.nodes.NodeOp
 *  org.openide.nodes.NodeReorderEvent
 *  org.openide.util.HelpCtx
 *  org.openide.util.HelpCtx$Provider
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.openide.explorer.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.MenuElement;
import javax.swing.MenuSelectionManager;
import javax.swing.SwingUtilities;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import org.openide.awt.JMenuPlus;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeAcceptor;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeOp;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.HelpCtx;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class MenuView
extends JPanel {
    static final long serialVersionUID = -4970665063421766904L;
    static final NodeAcceptor DEFAULT_LISTENER = new NodeAcceptor(){

        public boolean acceptNodes(Node[] nodes) {
            if (nodes == null || nodes.length != 1) {
                return false;
            }
            Node n = nodes[0];
            Action a = n.getPreferredAction();
            if (a != null && a.isEnabled()) {
                a.actionPerformed(new ActionEvent((Object)n, 0, ""));
                return true;
            }
            return false;
        }
    };
    private transient ExplorerManager explorerManager;
    private JButton root;
    private JButton current;
    private transient Listener listener;

    public MenuView() {
        this.setLayout(new FlowLayout());
        this.root = new JButton(NbBundle.getMessage(MenuView.class, (String)"MenuViewStartFromRoot"));
        this.add(this.root);
        this.current = new JButton(NbBundle.getMessage(MenuView.class, (String)"MenuViewStartFromCurrent"));
        this.add(this.current);
        this.init();
    }

    private void init() {
        this.listener = new Listener(true);
        this.root.addMouseListener(this.listener);
        this.current.addMouseListener(new Listener(false));
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        ois.defaultReadObject();
        this.init();
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.explorerManager = ExplorerManager.find(this);
        this.explorerManager.addPropertyChangeListener(this.listener);
        this.doChecks();
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.explorerManager.removePropertyChangeListener(this.listener);
        this.explorerManager = null;
    }

    private void doChecks() {
        this.current.setEnabled(this.explorerManager.getSelectedNodes().length == 1);
    }

    public static class MenuItem
    extends JMenuItem
    implements HelpCtx.Provider {
        static final long serialVersionUID = -918973978614344429L;
        protected Node node;
        protected NodeAcceptor action;

        public MenuItem(Node node) {
            this(node, MenuView.DEFAULT_LISTENER);
        }

        public MenuItem(Node node, NodeAcceptor l) {
            this(node, l, true);
        }

        @Deprecated
        public MenuItem(Node node, Acceptor action) {
            this(node, new AcceptorProxy(action), true);
        }

        @Deprecated
        public MenuItem(Node node, Acceptor action, boolean setName) {
            this(node, new AcceptorProxy(action), setName);
        }

        public MenuItem(Node node, NodeAcceptor l, boolean setName) {
            this.node = node;
            this.action = l;
            if (setName) {
                MenuItem.initialize(this, node);
            }
        }

        public HelpCtx getHelpCtx() {
            return this.node.getHelpCtx();
        }

        @Override
        public void doClick(int time) {
            this.action.acceptNodes(new Node[]{this.node});
        }

        static void initialize(JMenuItem item, Node node) {
            final class NI
            implements Runnable,
            NodeListener,
            ItemListener {
                final /* synthetic */ Node val$node;

                NI() {
                    this.val$node = var2_2;
                }

                @Override
                public void run() {
                    val$item.setIcon(new ImageIcon(this.val$node.getIcon(1)));
                    val$item.setText(this.val$node.getDisplayName());
                }

                public void childrenAdded(NodeMemberEvent ev) {
                }

                public void childrenRemoved(NodeMemberEvent ev) {
                }

                public void childrenReordered(NodeReorderEvent ev) {
                }

                public void nodeDestroyed(NodeEvent ev) {
                }

                public void propertyChange(PropertyChangeEvent ev) {
                    if ("icon".equals(ev.getPropertyName())) {
                        Mutex.EVENT.readAccess((Runnable)this);
                        return;
                    }
                    if ("displayName".equals(ev.getPropertyName())) {
                        Mutex.EVENT.readAccess((Runnable)this);
                        return;
                    }
                }

                @Override
                public void itemStateChanged(ItemEvent ev) {
                }
            }
            NI ni = new NI(item, node);
            ni.run();
            item.addItemListener(ni);
            node.addNodeListener(NodeOp.weakNodeListener((NodeListener)ni, (Object)node));
        }

    }

    private static final class AcceptorProxy
    implements NodeAcceptor {
        private Acceptor original;

        AcceptorProxy(Acceptor original) {
            this.original = original;
        }

        public boolean acceptNodes(Node[] nodes) {
            if (nodes == null || nodes.length != 1) {
                return false;
            }
            return this.original.accept(nodes[0]);
        }
    }

    public static class Menu
    extends JMenuPlus {
        static final long serialVersionUID = -1505289666675423991L;
        protected Node node;
        protected NodeAcceptor action;
        private boolean filled = false;

        public Menu(Node node) {
            this(node, MenuView.DEFAULT_LISTENER);
        }

        public Menu(Node node, NodeAcceptor action) {
            this(node, action, true);
        }

        @Deprecated
        public Menu(Node node, Acceptor action) {
            this(node, new AcceptorProxy(action), true);
        }

        @Deprecated
        public Menu(Node node, Acceptor action, boolean setName) {
            this(node, new AcceptorProxy(action), setName);
        }

        public Menu(Node node, NodeAcceptor action, boolean setName) {
            this.node = node;
            this.action = action;
            if (setName) {
                MenuItem.initialize((JMenuItem)((Object)this), node);
                HelpCtx help = node.getHelpCtx();
                if (help != null && !help.equals((Object)HelpCtx.DEFAULT_HELP) && help.getHelpID() != null) {
                    HelpCtx.setHelpIDString((JComponent)((Object)this), (String)help.getHelpID());
                }
            }
        }

        public JPopupMenu getPopupMenu() {
            JPopupMenu popup = super.getPopupMenu();
            this.fillSubmenu(popup);
            return popup;
        }

        private void fillSubmenu(JPopupMenu popup) {
            if (!this.filled) {
                this.filled = true;
                Helper h = new Helper(popup);
                Node[] nodes = this.node.getChildren().getNodes(true);
                this.removeAll();
                for (int i = 0; i < nodes.length; ++i) {
                    this.add(this.createMenuItem(nodes[i]));
                }
                if (this.getMenuComponentCount() == 0) {
                    this.add(Menu.createEmptyMenuItem());
                }
            }
        }

        public void processMouseEvent(MouseEvent e, MenuElement[] path, MenuSelectionManager manager) {
            super.processMouseEvent(e, path, manager);
            if (e.isPopupTrigger() && this.action.acceptNodes(new Node[]{this.node})) {
                MenuSelectionManager.defaultManager().clearSelectedPath();
            }
        }

        private static JMenuItem createEmptyMenuItem() {
            JMenuItem empty = new JMenuItem(NbBundle.getMessage(MenuView.class, (String)"EmptySubMenu"));
            empty.setEnabled(false);
            return empty;
        }

        protected JMenuItem createMenuItem(Node n) {
            return n.isLeaf() ? new MenuItem(n, this.action) : new Object(n, this.action);
        }

        private class Helper
        implements PopupMenuListener {
            private JPopupMenu popup;

            Helper(JPopupMenu master) {
                this.popup = master;
                this.popup.addPopupMenuListener(this);
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                Menu.this.filled = false;
                this.popup.removePopupMenuListener(this);
            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {
            }

            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
            }
        }

    }

    private class Listener
    extends MouseAdapter
    implements NodeAcceptor,
    PropertyChangeListener {
        private boolean root;

        public Listener(boolean root) {
            this.root = root;
        }

        @Override
        public void mousePressed(MouseEvent e) {
            if (e.getComponent().isEnabled()) {
                Node[] sel;
                Node context = null;
                if (!this.root && (sel = MenuView.this.explorerManager.getSelectedNodes()).length > 0) {
                    context = sel[0];
                }
                if (context == null) {
                    context = MenuView.this.explorerManager.getRootContext();
                }
                Menu menu = new Menu(context, MenuView.this.listener);
                JPopupMenu popupMenu = menu.getPopupMenu();
                Point p = new Point(e.getX(), e.getY());
                p.x = e.getX() - p.x;
                p.y = e.getY() - p.y;
                SwingUtilities.convertPointToScreen(p, e.getComponent());
                Dimension popupSize = popupMenu.getPreferredSize();
                Rectangle screenBounds = Utilities.getUsableScreenBounds((GraphicsConfiguration)MenuView.this.getGraphicsConfiguration());
                if (p.x + popupSize.width > screenBounds.x + screenBounds.width) {
                    p.x = screenBounds.x + screenBounds.width - popupSize.width;
                }
                if (p.y + popupSize.height > screenBounds.y + screenBounds.height) {
                    p.y = screenBounds.y + screenBounds.height - popupSize.height;
                }
                SwingUtilities.convertPointFromScreen(p, e.getComponent());
                popupMenu.show(e.getComponent(), p.x, p.y);
            }
        }

        public boolean acceptNodes(Node[] nodes) {
            if (nodes == null || nodes.length != 1) {
                return false;
            }
            Node n = nodes[0];
            Node parent = n.getParentNode();
            if (parent != null) {
                MenuView.this.explorerManager.setExploredContext(parent, new Node[]{n});
            }
            return true;
        }

        @Override
        public void propertyChange(PropertyChangeEvent ev) {
            if ("selectedNodes".equals(ev.getPropertyName())) {
                MenuView.this.doChecks();
            }
        }
    }

    @Deprecated
    public static interface Acceptor {
        @Deprecated
        public boolean accept(Node var1);
    }

}

