/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeMemberEvent
 *  org.openide.nodes.NodeReorderEvent
 */
package org.openide.explorer.view;

import java.util.Enumeration;
import java.util.EventObject;
import java.util.LinkedList;
import java.util.List;
import org.openide.explorer.view.VisualizerChildren;
import org.openide.explorer.view.VisualizerNode;
import org.openide.nodes.Node;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeReorderEvent;

abstract class VisualizerEvent
extends EventObject {
    int[] array;
    NodeEvent originalEvent;
    List<Node> snapshot;

    public VisualizerEvent(VisualizerChildren ch, int[] array, NodeEvent originalEvent, List<Node> snapshot) {
        super(ch);
        this.array = array;
        this.originalEvent = originalEvent;
        this.snapshot = snapshot;
    }

    public final int[] getArray() {
        return this.array;
    }

    public final VisualizerChildren getChildren() {
        return (VisualizerChildren)this.getSource();
    }

    public final VisualizerNode getVisualizer() {
        return this.getChildren().parent;
    }

    public final List<Node> getSnapshot() {
        return this.snapshot;
    }

    static final class Destroyed
    extends VisualizerEvent
    implements Runnable {
        public Destroyed(VisualizerChildren ch, NodeEvent ev) {
            super(ch, null, ev, null);
        }

        @Override
        public void run() {
            Enumeration<VisualizerNode> ch = this.getChildren().children(false);
            while (ch.hasMoreElements()) {
                VisualizerNode v = ch.nextElement();
                if (v == null) continue;
                v.nodeDestroyed(this.originalEvent);
            }
        }
    }

    static final class Reordered
    extends VisualizerEvent
    implements Runnable {
        static final long serialVersionUID = -4572356079752325870L;

        public Reordered(VisualizerChildren ch, int[] idxs, NodeReorderEvent originalEvent) {
            super(ch, idxs, (NodeEvent)originalEvent, originalEvent.getSnapshot());
        }

        @Override
        public void run() {
            super.getChildren().reordered(this);
        }
    }

    static final class Removed
    extends VisualizerEvent
    implements Runnable {
        static final long serialVersionUID = 5102881916407672392L;
        public LinkedList<VisualizerNode> removed = new LinkedList();

        public Removed(VisualizerChildren ch, int[] idxs, NodeMemberEvent originalEvent) {
            super(ch, idxs, (NodeEvent)originalEvent, originalEvent.getSnapshot());
        }

        @Override
        public void run() {
            super.getChildren().removed(this);
        }
    }

    static final class Added
    extends VisualizerEvent
    implements Runnable {
        static final long serialVersionUID = 5906423476285962043L;

        public Added(VisualizerChildren ch, int[] idxs, NodeMemberEvent originalEvent) {
            super(ch, idxs, (NodeEvent)originalEvent, originalEvent.getSnapshot());
        }

        @Override
        public void run() {
            super.getChildren().added(this);
        }
    }

}

