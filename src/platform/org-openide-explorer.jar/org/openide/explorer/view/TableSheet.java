/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.NbBundle
 */
package org.openide.explorer.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.ScrollPaneLayout;
import javax.swing.Scrollable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.MouseInputAdapter;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.openide.explorer.view.NodeTableModel;
import org.openide.explorer.view.TableSheetCell;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

class TableSheet
extends JScrollPane {
    protected transient JTable table;
    private transient NodeTableModel tableModel;
    private transient TableSheetCell tableCell;

    public TableSheet() {
        this.tableModel = new NodeTableModel();
        this.initializeView();
    }

    public TableSheet(NodeTableModel tableModel) {
        this.tableModel = tableModel;
        this.initializeView();
    }

    private void initializeView() {
        this.table = this.createTable();
        this.initializeTable();
        this.setViewportView(this.table);
        this.setRequestFocusEnabled(false);
        this.table.getAccessibleContext().setAccessibleName(NbBundle.getMessage(TableSheet.class, (String)"ACS_TableSheet"));
        this.table.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(TableSheet.class, (String)"ACSD_TableSheet"));
    }

    public void setNodes(Node[] nodes) {
        this.tableModel.setNodes(nodes);
    }

    public void setProperties(Node.Property[] props) {
        this.tableModel.setProperties(props);
    }

    public final void setAutoResizeMode(int mode) {
        this.table.setAutoResizeMode(mode);
    }

    public final int getAutoResizeMode() {
        return this.table.getAutoResizeMode();
    }

    public final void setColumnPreferredWidth(int index, int width) {
        this.table.getColumnModel().getColumn(index).setPreferredWidth(width);
        this.table.setPreferredScrollableViewportSize(this.table.getPreferredSize());
    }

    public final int getColumnPreferredWidth(int index) {
        return this.table.getColumnModel().getColumn(index).getPreferredWidth();
    }

    JTable createTable() {
        return new JTable();
    }

    private void initializeTable() {
        this.table.setModel(this.tableModel);
        this.tableCell = new TableSheetCell(this.tableModel);
        this.table.setDefaultRenderer(Node.Property.class, this.tableCell);
        this.table.setDefaultEditor(Node.Property.class, this.tableCell);
        this.table.getTableHeader().setDefaultRenderer(this.tableCell);
        this.table.setShowGrid(false);
        this.table.setIntercellSpacing(new Dimension(0, 0));
        this.table.setSelectionMode(0);
        this.table.getTableHeader().setReorderingAllowed(false);
        if (UIManager.getColor("Panel.background") != null) {
            this.table.setBackground(UIManager.getColor("Panel.background"));
            this.table.setSelectionBackground(UIManager.getColor("Panel.background"));
        }
    }

    private static String getString(String key) {
        return NbBundle.getMessage(TableSheet.class, (String)key);
    }

    @Override
    public void updateUI() {
        super.updateUI();
        if (null != this.tableCell) {
            this.tableCell.updateUI();
        }
    }

    private static final class EnablingScrollPaneLayout
    extends ScrollPaneLayout {
        JScrollPane dependentScrollPane;

        public EnablingScrollPaneLayout(JScrollPane scrollPane) {
            this.dependentScrollPane = scrollPane;
        }

        @Override
        public void layoutContainer(Container parent) {
            JScrollPane scrollPane;
            boolean hsbNeeded;
            super.layoutContainer(parent);
            Component view = this.viewport != null ? this.viewport.getView() : null;
            Dimension viewPrefSize = view != null ? view.getPreferredSize() : new Dimension(0, 0);
            Dimension extentSize = this.viewport != null ? this.viewport.toViewCoordinates(this.viewport.getSize()) : new Dimension(0, 0);
            boolean viewTracksViewportWidth = view instanceof Scrollable && ((Scrollable)((Object)view)).getScrollableTracksViewportWidth();
            boolean bl = hsbNeeded = !viewTracksViewportWidth && viewPrefSize.width > extentSize.width;
            if (this.hsb != null) {
                this.hsb.setEnabled(hsbNeeded);
            }
            if ((scrollPane = (JScrollPane)parent).getHorizontalScrollBarPolicy() != 32) {
                int newPolicy;
                int n = newPolicy = hsbNeeded ? 32 : 30;
                if (newPolicy != this.dependentScrollPane.getHorizontalScrollBarPolicy()) {
                    this.dependentScrollPane.setHorizontalScrollBarPolicy(newPolicy);
                    this.dependentScrollPane.getViewport().invalidate();
                }
            }
        }
    }

    private static class CompoundScrollPane
    extends JPanel
    implements Scrollable {
        CompoundScrollPane() {
        }

        @Override
        public boolean getScrollableTracksViewportWidth() {
            return true;
        }

        @Override
        public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
            return 10;
        }

        @Override
        public boolean getScrollableTracksViewportHeight() {
            return true;
        }

        @Override
        public Dimension getPreferredScrollableViewportSize() {
            return this.getPreferredSize();
        }

        @Override
        public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
            return 10;
        }
    }

    static class ControlledTableView
    extends TableSheet {
        JScrollPane controllingView;
        Component header;
        JPanel compoundScrollPane;

        ControlledTableView(JScrollPane contrView) {
            this.controllingView = contrView;
            this.initializeView();
        }

        ControlledTableView(JScrollPane contrView, NodeTableModel ntm) {
            super(ntm);
            this.controllingView = contrView;
            this.initializeView();
        }

        @Override
        public boolean isValidateRoot() {
            return false;
        }

        @Override
        private void initializeView() {
            Component comp = this.controllingView.getViewport().getView();
            this.controllingView.setViewportView(comp);
            if (UIManager.getColor("Table.background") != null) {
                this.getViewport().setBackground(UIManager.getColor("Table.background"));
            }
            this.setVerticalScrollBar(this.controllingView.getVerticalScrollBar());
            EnablingScrollPaneLayout spl = new EnablingScrollPaneLayout(this.controllingView);
            this.setLayout(spl);
            spl.syncWithScrollPane(this);
            spl = new EnablingScrollPaneLayout(this);
            this.controllingView.setLayout(spl);
            spl.syncWithScrollPane(this.controllingView);
            this.table.setBorder(null);
            this.header = new JTable().getTableHeader().getDefaultRenderer().getTableCellRendererComponent(null, " ", false, false, 0, 0);
            MouseDragHandler mouseHandler = new MouseDragHandler();
            this.header.addMouseListener(mouseHandler);
            this.header.addMouseMotionListener(mouseHandler);
        }

        @Override
        JTable createTable() {
            return new ATable();
        }

        JTable getTable() {
            return this.table;
        }

        @Override
        public void setBorder(Border border) {
            super.setBorder(null);
        }

        void setRowHeight(int h) {
            this.table.setRowHeight(h);
            this.getVerticalScrollBar().setUnitIncrement(h);
        }

        void setHeaderText(String text) {
            if (this.header instanceof JLabel) {
                ((JLabel)this.header).setText(text);
            }
        }

        void setControllingViewWidth(int width) {
            this.controllingView.setPreferredSize(new Dimension(width, 0));
        }

        int getControllingViewWidth() {
            return this.controllingView.getPreferredSize().width;
        }

        JComponent compoundScrollPane() {
            JPanel leftPanel = new JPanel(new BorderLayout());
            leftPanel.add(this.header, "North");
            leftPanel.add((Component)this.controllingView, "Center");
            this.compoundScrollPane = new CompoundScrollPane();
            this.compoundScrollPane.setLayout(new BorderLayout());
            this.compoundScrollPane.add((Component)leftPanel, "Center");
            this.compoundScrollPane.add((Component)this, "East");
            return this.compoundScrollPane;
        }

        private class MouseDragHandler
        extends MouseInputAdapter {
            boolean dragging;
            int lastMouseX;

            MouseDragHandler() {
                this.dragging = false;
            }

            @Override
            public void mousePressed(MouseEvent e) {
                Point p = e.getPoint();
                this.dragging = this.canResize(p);
                this.lastMouseX = p.x;
            }

            private void setCursor(Cursor c) {
                if (ControlledTableView.this.header.getCursor() != c) {
                    ControlledTableView.this.header.setCursor(c);
                }
            }

            private boolean canResize(Point mousePoint) {
                return mousePoint.x >= ControlledTableView.this.header.getSize().width - 3;
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                if (this.canResize(e.getPoint()) || this.dragging) {
                    this.setCursor(Cursor.getPredefinedCursor(11));
                } else {
                    this.setCursor(Cursor.getPredefinedCursor(0));
                }
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                int mouseX = e.getX();
                int deltaX = this.lastMouseX - mouseX;
                if (deltaX == 0) {
                    return;
                }
                if (this.dragging) {
                    Dimension size = ControlledTableView.this.table.getPreferredScrollableViewportSize();
                    int parentWidth = ControlledTableView.this.compoundScrollPane.getWidth();
                    int tableMinWidth = ControlledTableView.this.table.getMinimumSize().width;
                    int newWidth = size.width + deltaX > parentWidth - 20 ? parentWidth - 20 : (size.width + deltaX < tableMinWidth ? tableMinWidth : size.width + deltaX);
                    ControlledTableView.this.table.setPreferredScrollableViewportSize(new Dimension(newWidth, size.height));
                    this.lastMouseX -= newWidth - size.width;
                    ControlledTableView.this.table.revalidate();
                    ControlledTableView.this.table.repaint();
                } else {
                    this.lastMouseX = mouseX;
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                this.dragging = false;
            }
        }

        private class ATable
        extends JTable {
            private boolean trytorevalidate;

            public ATable() {
                this.trytorevalidate = true;
                this.getActionMap().put("cancel", new OurCancelEditingAction());
            }

            @Override
            public Dimension getPreferredScrollableViewportSize() {
                Dimension pref = super.getPreferredScrollableViewportSize();
                if (this.getAutoResizeMode() != 0 && this.getParent() != null) {
                    Insets insets = this.getParent().getInsets();
                    Dimension size = this.getParent().getSize();
                    pref.height = size.height - insets.top - insets.bottom;
                }
                return pref;
            }

            @Override
            public void setBounds(int x, int y, int width, int height) {
                super.setBounds(x, y, width, height);
                if (this.getAutoResizeMode() == 0) {
                    return;
                }
                if (this.trytorevalidate && width != this.getPreferredScrollableViewportSize().width) {
                    this.trytorevalidate = false;
                    ControlledTableView.this.compoundScrollPane.validate();
                    this.trytorevalidate = true;
                }
            }

            private class OurCancelEditingAction
            extends AbstractAction {
                OurCancelEditingAction() {
                }

                @Override
                public void actionPerformed(ActionEvent e) {
                    JTable table = (JTable)e.getSource();
                    table.removeEditor();
                }

                @Override
                public boolean isEnabled() {
                    return ATable.this.isEditing();
                }
            }

        }

    }

}

