/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.datatransfer.PasteType
 */
package org.openide.explorer.view;

import java.awt.Component;
import java.awt.Point;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import org.openide.explorer.view.DragDropUtilities;
import org.openide.explorer.view.ExplorerDnDManager;
import org.openide.explorer.view.TableView;
import org.openide.nodes.Node;
import org.openide.util.RequestProcessor;
import org.openide.util.datatransfer.PasteType;

final class TableViewDropSupport
implements DropTargetListener,
Runnable {
    boolean active = false;
    boolean dropTargetPopupAllowed;
    DropTarget dropTarget;
    int lastIndex = -1;
    protected TableView view;
    protected JTable table;

    public TableViewDropSupport(TableView view, JTable table) {
        this(view, table, true);
    }

    public TableViewDropSupport(TableView view, JTable table, boolean dropTargetPopupAllowed) {
        this.view = view;
        this.table = table;
        this.dropTargetPopupAllowed = dropTargetPopupAllowed;
    }

    public void setDropTargetPopupAllowed(boolean value) {
        this.dropTargetPopupAllowed = value;
    }

    public boolean isDropTargetPopupAllowed() {
        return this.dropTargetPopupAllowed;
    }

    @Override
    public void dragEnter(DropTargetDragEvent dtde) {
        int dropAction = ExplorerDnDManager.getDefault().getAdjustedDropAction(dtde.getDropAction(), this.view.getAllowedDropActions());
        this.lastIndex = this.indexWithCheck(dtde);
        if (this.lastIndex < 0) {
            dtde.rejectDrag();
        } else {
            dtde.acceptDrag(dropAction);
        }
    }

    @Override
    public void dragOver(DropTargetDragEvent dtde) {
        int dropAction = ExplorerDnDManager.getDefault().getAdjustedDropAction(dtde.getDropAction(), this.view.getAllowedDropActions());
        int index = this.indexWithCheck(dtde);
        if (index < 0) {
            dtde.rejectDrag();
            if (this.lastIndex >= 0) {
                this.lastIndex = -1;
            }
        } else {
            dtde.acceptDrag(dropAction);
            if (this.lastIndex != index) {
                if (this.lastIndex < 0) {
                    this.lastIndex = index;
                }
                this.lastIndex = index;
            }
        }
    }

    @Override
    public void dropActionChanged(DropTargetDragEvent dtde) {
    }

    @Override
    public void dragExit(DropTargetEvent dte) {
        if (this.lastIndex >= 0) {
            // empty if block
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void drop(DropTargetDropEvent dtde) {
        boolean dropResult = true;
        try {
            int index = this.table.rowAtPoint(dtde.getLocation());
            Node dropNode = this.view.getNodeFromRow(index);
            int dropAction = ExplorerDnDManager.getDefault().getAdjustedDropAction(dtde.getDropAction(), this.view.getAllowedDropActions());
            if (index < 0 || !this.canDrop(dropNode, dropAction)) {
                dtde.rejectDrop();
                return;
            }
            PasteType pt = null;
            if (pt == null) {
                dropResult = false;
                RequestProcessor.getDefault().post((Runnable)this, 500);
                return;
            }
            dtde.acceptDrop(dropAction);
            if (dropAction != 1073741824) {
                DragDropUtilities.performPaste(pt, null);
            }
        }
        finally {
            dtde.dropComplete(dropResult);
        }
    }

    private boolean canDrop(Node n, int dropAction) {
        Transferable trans;
        Node[] nodes;
        if (n == null) {
            return false;
        }
        if (ExplorerDnDManager.getDefault().getNodeAllowedActions() == 0) {
            return false;
        }
        if ((2 & dropAction) != 0 && (nodes = ExplorerDnDManager.getDefault().getDraggedNodes()) != null) {
            for (int i = 0; i < nodes.length; ++i) {
                if (!n.equals((Object)nodes[i].getParentNode())) continue;
                return false;
            }
        }
        if ((trans = ExplorerDnDManager.getDefault().getDraggedTransferable((2 & dropAction) != 0)) == null) {
            return false;
        }
        Object pt = null;
        return pt != null;
    }

    public void activate(boolean active) {
        if (this.active == active) {
            return;
        }
        this.active = active;
        this.getDropTarget().setActive(active);
    }

    @Override
    public void run() {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(this);
            return;
        }
        DragDropUtilities.dropNotSuccesfull();
    }

    int indexWithCheck(DropTargetDragEvent dtde) {
        int dropAction = ExplorerDnDManager.getDefault().getAdjustedDropAction(dtde.getDropAction(), this.view.getAllowedDropActions());
        if ((dropAction & this.view.getAllowedDropActions()) == 0) {
            return -1;
        }
        int index = this.table.rowAtPoint(dtde.getLocation());
        if (index == -1) {
            return -1;
        }
        Node obj = this.view.getNodeFromRow(index);
        if (index < 0) {
            return -1;
        }
        if (!(obj instanceof Node)) {
            return -1;
        }
        return index;
    }

    DropTarget getDropTarget() {
        if (this.dropTarget == null) {
            this.dropTarget = new DropTarget(this.table, this.view.getAllowedDropActions(), this, false);
        }
        return this.dropTarget;
    }
}

