/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.view;

public interface CheckableNode {
    public boolean isCheckable();

    public boolean isCheckEnabled();

    public Boolean isSelected();

    public void setSelected(Boolean var1);
}

