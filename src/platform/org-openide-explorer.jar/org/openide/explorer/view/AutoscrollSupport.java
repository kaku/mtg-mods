/*
 * Decompiled with CFR 0_118.
 */
package org.openide.explorer.view;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.dnd.Autoscroll;
import javax.swing.JViewport;

final class AutoscrollSupport
implements Autoscroll {
    Component comp;
    JViewport viewport;
    Insets insets;
    Insets scrollUnits;
    Insets autoscrollInsets;

    AutoscrollSupport(Component comp, Insets insets) {
        this.comp = comp;
        this.insets = insets;
        this.scrollUnits = insets;
    }

    @Override
    public void autoscroll(Point cursorLoc) {
        JViewport viewport = this.getViewport();
        if (viewport == null) {
            return;
        }
        Point viewPos = viewport.getViewPosition();
        int viewHeight = viewport.getExtentSize().height;
        int viewWidth = viewport.getExtentSize().width;
        if (cursorLoc.y - viewPos.y < this.insets.top) {
            viewport.setViewPosition(new Point(viewPos.x, Math.max(viewPos.y - this.scrollUnits.top, 0)));
        } else if (viewPos.y + viewHeight - cursorLoc.y < this.insets.bottom) {
            viewport.setViewPosition(new Point(viewPos.x, Math.min(viewPos.y + this.scrollUnits.bottom, this.comp.getHeight() - viewHeight)));
        } else if (cursorLoc.x - viewPos.x < this.insets.left) {
            viewport.setViewPosition(new Point(Math.max(viewPos.x - this.scrollUnits.left, 0), viewPos.y));
        } else if (viewPos.x + viewWidth - cursorLoc.x < this.insets.right) {
            viewport.setViewPosition(new Point(Math.min(viewPos.x + this.scrollUnits.right, this.comp.getWidth() - viewWidth), viewPos.y));
        }
    }

    @Override
    public Insets getAutoscrollInsets() {
        if (this.autoscrollInsets == null) {
            int height = this.comp.getHeight();
            int width = this.comp.getWidth();
            this.autoscrollInsets = new Insets(height, width, height, width);
        }
        return this.autoscrollInsets;
    }

    public Insets getInsets() {
        return this.insets;
    }

    public void setInsets(Insets insets) {
        this.insets = insets;
    }

    public Insets getScrollUnits() {
        return this.scrollUnits;
    }

    public void setScrollUnits(Insets scrollUnits) {
        this.scrollUnits = scrollUnits;
    }

    JViewport getViewport() {
        if (this.viewport == null) {
            Component curComp = this.comp;
            while (!(curComp instanceof JViewport) && curComp != null) {
                curComp = this.comp.getParent();
            }
            this.viewport = (JViewport)curComp;
        }
        return this.viewport;
    }
}

