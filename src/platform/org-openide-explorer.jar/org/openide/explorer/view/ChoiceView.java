/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package org.openide.explorer.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.ListCellRenderer;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.NodeListModel;
import org.openide.explorer.view.NodeRenderer;
import org.openide.explorer.view.Visualizer;
import org.openide.explorer.view.VisualizerNode;
import org.openide.nodes.Node;

public class ChoiceView
extends JComboBox
implements Externalizable {
    static final long serialVersionUID = 2522310031223476067L;
    private transient ExplorerManager manager;
    private transient PropertyIL iListener;
    private transient NodeListModel model;
    private boolean showExploredContext = true;

    public ChoiceView() {
        this.initializeChoice();
    }

    private void initializeChoice() {
        this.setRenderer(new NodeRenderer());
        this.model = this.createModel();
        this.setModel(this.model);
        this.iListener = new PropertyIL();
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(this.showExploredContext ? Boolean.TRUE : Boolean.FALSE);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        this.showExploredContext = (Boolean)in.readObject();
    }

    protected NodeListModel createModel() {
        return new NodeListModel();
    }

    public void setShowExploredContext(boolean b) {
        this.showExploredContext = b;
        this.updateChoice();
    }

    public boolean getShowExploredContext() {
        return this.showExploredContext;
    }

    @Override
    public void addNotify() {
        this.manager = ExplorerManager.find(this);
        this.manager.addVetoableChangeListener(this.iListener);
        this.manager.addPropertyChangeListener(this.iListener);
        this.updateChoice();
        this.addActionListener(this.iListener);
        super.addNotify();
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.removeActionListener(this.iListener);
        if (this.manager != null) {
            this.manager.removeVetoableChangeListener(this.iListener);
            this.manager.removePropertyChangeListener(this.iListener);
        }
    }

    private void updateSelection() {
        Node[] nodes = this.manager.getSelectedNodes();
        if (nodes.length > 0) {
            this.setSelectedItem(VisualizerNode.getVisualizer(null, nodes[0]));
        } else {
            this.setSelectedItem((Object)(this.showExploredContext ? this.manager.getExploredContext() : this.manager.getRootContext()));
        }
    }

    private void updateChoice() {
        if (this.manager == null) {
            return;
        }
        if (this.showExploredContext) {
            this.model.setNode(this.manager.getExploredContext());
        } else {
            this.model.setNode(this.manager.getRootContext());
        }
        this.updateSelection();
    }

    final class PropertyIL
    implements PropertyChangeListener,
    VetoableChangeListener,
    ActionListener {
        PropertyIL() {
        }

        @Override
        public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
            Node[] nodes;
            if ("selectedNodes".equals(evt.getPropertyName()) && (nodes = (Node[])evt.getNewValue()).length > 1) {
                throw new PropertyVetoException("", evt);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            ChoiceView.this.removeActionListener(this);
            try {
                if ("selectedNodes".equals(evt.getPropertyName())) {
                    Node[] selectedNodes = (Node[])evt.getNewValue();
                    ChoiceView.this.updateSelection();
                    return;
                }
                if (!ChoiceView.this.showExploredContext && "rootContext".equals(evt.getPropertyName())) {
                    ChoiceView.this.updateChoice();
                    return;
                }
                if (ChoiceView.this.showExploredContext && "exploredContext".equals(evt.getPropertyName())) {
                    ChoiceView.this.updateChoice();
                    return;
                }
            }
            finally {
                ChoiceView.this.addActionListener(this);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            int s = ChoiceView.this.getSelectedIndex();
            if (s < 0 || s >= ChoiceView.this.model.getSize()) {
                return;
            }
            Node n = Visualizer.findNode(ChoiceView.this.model.getElementAt(s));
            ChoiceView.this.manager.removeVetoableChangeListener(this);
            ChoiceView.this.manager.removePropertyChangeListener(this);
            try {
                ChoiceView.this.manager.setSelectedNodes(new Node[]{n});
            }
            catch (PropertyVetoException ex) {
                ChoiceView.this.updateChoice();
            }
            finally {
                ChoiceView.this.manager.addVetoableChangeListener(this);
                ChoiceView.this.manager.addPropertyChangeListener(this);
            }
        }
    }

}

