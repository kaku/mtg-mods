/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 */
package org.openide.explorer.view;

import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.plaf.TreeUI;
import javax.swing.table.TableModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import org.openide.explorer.view.NodeTableModel;
import org.openide.explorer.view.Visualizer;
import org.openide.nodes.Node;

class TreeTableModelAdapter
extends NodeTableModel {
    private JTree tree;
    private NodeTableModel nodeTableModel;

    public TreeTableModelAdapter(JTree t, NodeTableModel ntm) {
        this.tree = t;
        this.nodeTableModel = ntm;
        Listener listener = new Listener();
        this.tree.addTreeExpansionListener(listener);
        this.tree.getModel().addTreeModelListener(listener);
        this.nodeTableModel.addTableModelListener(listener);
    }

    @Override
    public void setNodes(Node[] nodes) {
        this.nodeTableModel.setNodes(nodes);
    }

    @Override
    public void setProperties(Node.Property[] props) {
        this.nodeTableModel.setProperties(props);
    }

    @Override
    protected Node.Property getPropertyFor(Node node, Node.Property prop) {
        return this.nodeTableModel.getPropertyFor(node, prop);
    }

    @Override
    Node nodeForRow(int row) {
        return Visualizer.findNode(this.tree.getPathForRow(row).getLastPathComponent());
    }

    @Override
    Node.Property propertyForColumn(int column) {
        return this.nodeTableModel.propertyForColumn(column - 1);
    }

    @Override
    public int getColumnCount() {
        return this.nodeTableModel.getColumnCount() + 1;
    }

    @Override
    public String getColumnName(int column) {
        return column == 0 ? Visualizer.findNode(this.tree.getModel().getRoot()).getDisplayName() : this.nodeTableModel.getColumnName(column - 1);
    }

    @Override
    public Class getColumnClass(int column) {
        return column == 0 ? TreeTableModelAdapter.class : this.nodeTableModel.getColumnClass(column - 1);
    }

    @Override
    public int getRowCount() {
        return this.tree.getRowCount();
    }

    @Override
    public Object getValueAt(int row, int column) {
        if (column == 0) {
            TreePath path = this.tree.getPathForRow(row);
            if (path == null) {
                throw new IndexOutOfBoundsException("row " + row + " vs. count " + this.tree.getRowCount() + " with UI " + this.tree.getUI());
            }
            return path.getLastPathComponent();
        }
        return this.nodeTableModel.getPropertyFor(this.nodeForRow(row), this.propertyForColumn(column));
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        if (column == 0) {
            return true;
        }
        Object o = this.getValueAt(row, column);
        if (o == null) {
            return false;
        }
        if (o instanceof Node.Property) {
            return ((Node.Property)o).canWrite();
        }
        return false;
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
    }

    class Listener
    implements TreeExpansionListener,
    TreeModelListener,
    TableModelListener,
    Runnable {
        TreePath[] tps;

        Listener() {
            this.tps = null;
        }

        @Override
        public void treeExpanded(TreeExpansionEvent event) {
            this.updateNodes();
        }

        @Override
        public void treeCollapsed(TreeExpansionEvent event) {
            this.updateNodes();
        }

        @Override
        public void treeNodesChanged(TreeModelEvent e) {
            this.delayedUpdateNodes(e);
        }

        @Override
        public void treeNodesInserted(TreeModelEvent e) {
            this.delayedUpdateNodes(e);
        }

        @Override
        public void treeNodesRemoved(TreeModelEvent e) {
            this.delayedUpdateNodes(e);
        }

        @Override
        public void treeStructureChanged(TreeModelEvent e) {
            this.tps = TreeTableModelAdapter.this.tree.getSelectionPaths();
            if (e.getPath().length == 1 && !e.getTreePath().equals(e.getPath()[0])) {
                this.tps = null;
            }
            this.delayedUpdateNodes(e);
        }

        @Override
        public void tableChanged(TableModelEvent e) {
            int c = e.getColumn();
            int column = c == -1 ? -1 : c + 1;
            TreeTableModelAdapter.this.fireTableChanged(new TableModelEvent(TreeTableModelAdapter.this, e.getFirstRow(), e.getLastRow(), column, e.getType()));
        }

        protected void delayedUpdateNodes(TreeModelEvent e) {
            SwingUtilities.invokeLater(this);
        }

        @Override
        public void run() {
            this.updateNodes();
        }

        private void updateNodes() {
            Node[] nodes = new Node[TreeTableModelAdapter.this.tree.getRowCount()];
            for (int i = 0; i < TreeTableModelAdapter.this.tree.getRowCount(); ++i) {
                nodes[i] = Visualizer.findNode(TreeTableModelAdapter.this.tree.getPathForRow(i).getLastPathComponent());
            }
            TreeTableModelAdapter.this.setNodes(nodes);
            if (this.tps != null) {
                TreeTableModelAdapter.this.tree.setSelectionPaths(this.tps);
                this.tps = null;
            }
        }
    }

}

