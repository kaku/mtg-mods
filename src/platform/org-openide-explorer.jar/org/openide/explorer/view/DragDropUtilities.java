/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.Mnemonics
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.UserCancelException
 *  org.openide.util.datatransfer.ExClipboard
 *  org.openide.util.datatransfer.ExTransferable
 *  org.openide.util.datatransfer.ExTransferable$Multi
 *  org.openide.util.datatransfer.MultiTransferObject
 *  org.openide.util.datatransfer.PasteType
 */
package org.openide.explorer.view;

import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import javax.swing.AbstractButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.tree.TreeNode;
import org.netbeans.modules.openide.explorer.ExternalDragAndDrop;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.explorer.view.TreeViewDropSupport;
import org.openide.explorer.view.Visualizer;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.UserCancelException;
import org.openide.util.datatransfer.ExClipboard;
import org.openide.util.datatransfer.ExTransferable;
import org.openide.util.datatransfer.MultiTransferObject;
import org.openide.util.datatransfer.PasteType;

final class DragDropUtilities {
    static final boolean dragAndDropEnabled = !GraphicsEnvironment.isHeadless() && Boolean.parseBoolean(System.getProperty("netbeans.dnd.enabled", "true"));
    static final int NODE_UP = -1;
    static final int NODE_CENTRAL = 0;
    static final int NODE_DOWN = 1;
    static Runnable postDropRun = null;
    static final int NoDrag = 0;
    static final int NoDrop = 1;

    private DragDropUtilities() {
    }

    static boolean checkNodeForAction(Node node, int dragAction) {
        if (node.canCut() && (dragAction == 2 || dragAction == 3)) {
            return true;
        }
        if (node.canCopy() && (dragAction == 1 || dragAction == 3 || dragAction == 1073741824 || dragAction == 1073741824)) {
            return true;
        }
        return false;
    }

    static Transferable getNodeTransferable(Node[] nodes, int dragAction) throws IOException {
        Transferable[] tArray = new Transferable[nodes.length];
        for (int i = 0; i < nodes.length; ++i) {
            tArray[i] = (dragAction & 2) != 0 ? nodes[i].clipboardCut() : nodes[i].drag();
        }
        Transferable result = tArray.length == 1 ? tArray[0] : ExternalDragAndDrop.maybeAddExternalFileDnd(new ExTransferable.Multi(tArray));
        Clipboard c = DragDropUtilities.getClipboard();
        if (c instanceof ExClipboard) {
            return ((ExClipboard)c).convert(result);
        }
        return result;
    }

    static Transferable getNodeTransferable(Node node, int dragAction) throws IOException {
        return DragDropUtilities.getNodeTransferable(new Node[]{node}, dragAction);
    }

    static void setPostDropRun(Runnable run) {
        postDropRun = run;
    }

    private static void invokePostDropRun() {
        if (postDropRun != null) {
            SwingUtilities.invokeLater(postDropRun);
            postDropRun = null;
        }
    }

    static Node[] performPaste(PasteType type, Node targetFolder) {
        try {
            if (targetFolder == null) {
                type.paste();
                return new Node[0];
            }
            Node[] preNodes = targetFolder.getChildren().getNodes(true);
            type.paste();
            Node[] postNodes = targetFolder.getChildren().getNodes(true);
            List<Node> pre = Arrays.asList(preNodes);
            List<Node> post = Arrays.asList(postNodes);
            Iterator<Node> it = post.iterator();
            ArrayList<Node> diff = new ArrayList<Node>();
            while (it.hasNext()) {
                Node n = it.next();
                if (pre.contains((Object)n)) continue;
                diff.add(n);
            }
            return diff.toArray((T[])new Node[diff.size()]);
        }
        catch (UserCancelException exc) {
            return new Node[0];
        }
        catch (IOException e) {
            Exceptions.printStackTrace((Throwable)e);
            return new Node[0];
        }
    }

    static PasteType[] getPasteTypes(Node node, Transferable trans) {
        if (!trans.isDataFlavorSupported(ExTransferable.multiFlavor)) {
            PasteType[] pt = null;
            try {
                pt = node.getPasteTypes(trans);
            }
            catch (NullPointerException npe) {
                Exceptions.printStackTrace((Throwable)npe);
            }
            return pt;
        }
        try {
            MultiTransferObject obj = (MultiTransferObject)trans.getTransferData(ExTransferable.multiFlavor);
            int count = obj.getCount();
            Transferable[] t = new Transferable[count];
            PasteType[] p = new PasteType[count];
            PasteType[] curTypes = null;
            for (int i = 0; i < count; ++i) {
                t[i] = obj.getTransferableAt(i);
                curTypes = node.getPasteTypes(t[i]);
                if (curTypes.length == 0) {
                    return curTypes;
                }
                p[i] = curTypes[0];
            }
            return new PasteType[]{new MultiPasteType(t, p)};
        }
        catch (UnsupportedFlavorException e) {
        }
        catch (IOException e) {
            // empty catch block
        }
        return new PasteType[0];
    }

    static PasteType getDropType(Node node, Transferable trans, int action, int dropIndex) {
        PasteType paste = null;
        try {
            paste = node.getDropType(trans, action, dropIndex);
            if (paste != null) {
                return paste;
            }
        }
        catch (NullPointerException npe) {
            Exceptions.printStackTrace((Throwable)npe);
        }
        if (trans.isDataFlavorSupported(ExTransferable.multiFlavor)) {
            try {
                MultiTransferObject obj = (MultiTransferObject)trans.getTransferData(ExTransferable.multiFlavor);
                int count = obj.getCount();
                Transferable[] t = new Transferable[count];
                PasteType[] p = new PasteType[count];
                PasteType pt = null;
                for (int i = 0; i < count; ++i) {
                    t[i] = obj.getTransferableAt(i);
                    pt = node.getDropType(t[i], action, dropIndex);
                    if (pt == null) {
                        return pt;
                    }
                    p[i] = pt;
                }
                return new MultiPasteType(t, p);
            }
            catch (UnsupportedFlavorException e) {
            }
            catch (IOException e) {
                // empty catch block
            }
        }
        return null;
    }

    static void dropNotSuccesfull() {
        DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)NbBundle.getMessage(TreeViewDropSupport.class, (String)"MSG_NoPasteTypes"), 2));
    }

    private static Clipboard getClipboard() {
        Clipboard c = (Clipboard)Lookup.getDefault().lookup(Clipboard.class);
        if (c == null) {
            c = Toolkit.getDefaultToolkit().getSystemClipboard();
        }
        return c;
    }

    static Node secureFindNode(Object o) {
        assert (o instanceof TreeNode);
        try {
            return Visualizer.findNode(o);
        }
        catch (ClassCastException e) {
            e.printStackTrace();
            return null;
        }
    }

    static JPopupMenu createDropFinishPopup(final TreeSet pasteTypes) {
        JPopupMenu menu = new JPopupMenu();
        final JMenuItem[] items_ = new JMenuItem[pasteTypes.size()];
        ActionListener aListener = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                JMenuItem source = (JMenuItem)e.getSource();
                Iterator it = pasteTypes.iterator();
                int i = 0;
                while (it.hasNext()) {
                    PasteType action = (PasteType)it.next();
                    if (items_[i].equals(source)) {
                        DragDropUtilities.performPaste(action, null);
                        DragDropUtilities.invokePostDropRun();
                        break;
                    }
                    ++i;
                }
            }
        };
        Iterator it = pasteTypes.iterator();
        int i = 0;
        while (it.hasNext()) {
            items_[i] = new JMenuItem();
            Mnemonics.setLocalizedText((AbstractButton)items_[i], (String)((PasteType)it.next()).getName());
            items_[i].addActionListener(aListener);
            menu.add(items_[i]);
            ++i;
        }
        menu.addSeparator();
        JMenuItem abortItem = new JMenuItem(NbBundle.getMessage(DragDropUtilities.class, (String)"MSG_ABORT"));
        menu.add(abortItem);
        return menu;
    }

    static final class MultiPasteType
    extends PasteType {
        Transferable[] t;
        PasteType[] p;

        MultiPasteType(Transferable[] t, PasteType[] p) {
            this.t = t;
            this.p = p;
        }

        public Transferable paste() throws IOException {
            int size = this.p.length;
            Transferable[] arr = new Transferable[size];
            for (int i = 0; i < size; ++i) {
                arr[i] = this.p[i].paste();
            }
            return new ExTransferable.Multi(arr);
        }
    }

}

