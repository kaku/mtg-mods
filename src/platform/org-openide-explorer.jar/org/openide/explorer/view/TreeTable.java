/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.openide.explorer.view;

import java.awt.AWTEvent;
import java.awt.AWTKeyStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.ContainerOrderFocusTraversalPolicy;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FocusTraversalPolicy;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.HierarchyListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyEditor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EventObject;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultListSelectionModel;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.MouseInputListener;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.plaf.TableUI;
import javax.swing.plaf.TreeUI;
import javax.swing.plaf.basic.BasicTableUI;
import javax.swing.plaf.basic.BasicTreeUI;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import org.openide.explorer.propertysheet.PropertyPanel;
import org.openide.explorer.view.NodeRenderer;
import org.openide.explorer.view.NodeTableModel;
import org.openide.explorer.view.NodeTreeModel;
import org.openide.explorer.view.QuickSearchTableFilter;
import org.openide.explorer.view.TableQuickSearchSupport;
import org.openide.explorer.view.TableSheetCell;
import org.openide.explorer.view.TreeTableModelAdapter;
import org.openide.explorer.view.TreeView;
import org.openide.explorer.view.ViewUtil;
import org.openide.explorer.view.Visualizer;
import org.openide.explorer.view.VisualizerNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

class TreeTable
extends JTable
implements Runnable {
    private static final String ACTION_FOCUS_NEXT = "focusNext";
    private static Color unfocusedSelBg = null;
    private static Color unfocusedSelFg = null;
    private TreeTableCellRenderer tree;
    private NodeTableModel tableModel;
    private int treeColumnIndex = -1;
    private int lastRow = -1;
    private boolean canEdit;
    private boolean ignoreScrolling = false;
    private boolean ignoreClearSelection = false;
    private int positionX;
    private boolean treeHScrollingEnabled = true;
    private final ListToTreeSelectionModelWrapper selectionWrapper;
    private boolean edCreated = false;
    boolean inSelectAll = false;
    private boolean needCalcRowHeight = true;
    boolean inEditRequest = false;
    boolean inEditorChangeRequest = false;
    int editRow = -1;
    private boolean inRemoveRequest = false;
    private TableSheetCell tableCell;
    private TableQuickSearchSupport.QuickSearchSettings qss = new TableQuickSearchSupport.QuickSearchSettings();
    private QuickSearchTableFilter qstf;

    public TreeTable(NodeTreeModel treeModel, NodeTableModel tableModel) {
        this.qstf = new DefaultQuickSearchTableFilter();
        this.setSurrendersFocusOnKeystroke(true);
        this.tree = new TreeTableCellRenderer(treeModel);
        this.tableModel = new TreeTableModelAdapter(this.tree, tableModel);
        this.tree.setCellRenderer(new NodeRenderer());
        this.setModel(this.tableModel);
        this.selectionWrapper = new ListToTreeSelectionModelWrapper();
        this.tree.setSelectionModel(this.selectionWrapper);
        this.setSelectionModel(this.selectionWrapper.getListSelectionModel());
        this.getTableHeader().setReorderingAllowed(false);
        this.setDefaultRenderer(TreeTableModelAdapter.class, this.tree);
        this.tableCell = new TableSheetCell(this.tableModel);
        this.tableCell.setFlat(true);
        this.setDefaultRenderer(Node.Property.class, this.tableCell);
        this.setDefaultEditor(Node.Property.class, this.tableCell);
        this.getTableHeader().setDefaultRenderer(this.tableCell);
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(TreeTable.class, (String)"ACSN_TreeTable"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(TreeTable.class, (String)"ACSD_TreeTable"));
        this.setFocusCycleRoot(true);
        this.setFocusTraversalPolicy(new STPolicy());
        this.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        this.putClientProperty("JTable.autoStartsEdit", Boolean.FALSE);
        this.initKeysAndActions();
    }

    private void initKeysAndActions() {
        this.setFocusTraversalKeys(0, Collections.emptySet());
        this.setFocusTraversalKeys(1, Collections.emptySet());
        this.unregisterKeyboardAction(KeyStroke.getKeyStroke(10, 0));
        this.unregisterKeyboardAction(KeyStroke.getKeyStroke(10, 1));
        InputMap imp = this.getInputMap(0);
        InputMap imp2 = this.getInputMap(1);
        ActionMap am = this.getActionMap();
        imp2.put(KeyStroke.getKeyStroke("control C"), "none");
        imp2.put(KeyStroke.getKeyStroke("control V"), "none");
        imp2.put(KeyStroke.getKeyStroke("control X"), "none");
        imp2.put(KeyStroke.getKeyStroke("COPY"), "none");
        imp2.put(KeyStroke.getKeyStroke("PASTE"), "none");
        imp2.put(KeyStroke.getKeyStroke("CUT"), "none");
        imp.put(KeyStroke.getKeyStroke(9, 3, false), "focusNext");
        imp.put(KeyStroke.getKeyStroke(9, 2, false), "focusNext");
        CTRLTabAction ctrlTab = new CTRLTabAction();
        am.put("focusNext", ctrlTab);
        this.getActionMap().put("selectNextColumn", new TreeTableAction(this.tree.getActionMap().get("selectChild"), this.getActionMap().get("selectNextColumn")));
        this.getActionMap().put("selectPreviousColumn", new TreeTableAction(this.tree.getActionMap().get("selectParent"), this.getActionMap().get("selectPreviousColumn")));
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(TreeTable.class, (String)"ACSN_TreeTable"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(TreeTable.class, (String)"ACSD_TreeTable"));
        imp.put(KeyStroke.getKeyStroke(32, 0, false), "beginEdit");
        this.getActionMap().put("beginEdit", new EditAction());
        imp2.put(KeyStroke.getKeyStroke(27, 0, false), "cancelEdit");
        this.getActionMap().put("cancelEdit", new CancelEditAction());
        imp.put(KeyStroke.getKeyStroke(10, 0, false), "enter");
        this.getActionMap().put("enter", new EnterAction());
        imp.put(KeyStroke.getKeyStroke(9, 0), "next");
        imp.put(KeyStroke.getKeyStroke(9, 64), "previous");
        am.put("next", new NavigationAction(true));
        am.put("previous", new NavigationAction(false));
    }

    public TableCellEditor getDefaultEditor(Class columnClass) {
        if (!this.edCreated && columnClass == TreeTableModelAdapter.class) {
            this.setDefaultEditor(TreeTableModelAdapter.class, new TreeTableCellEditor());
            this.edCreated = true;
        }
        return super.getDefaultEditor(columnClass);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void selectAll() {
        this.inSelectAll = true;
        try {
            super.selectAll();
        }
        finally {
            this.inSelectAll = false;
            this.selectionWrapper.updateSelectedPathsFromSelectedRows();
        }
    }

    @Override
    public void updateUI() {
        super.updateUI();
        if (this.tree != null) {
            this.tree.updateUI();
        }
        if (null != this.tableCell) {
            this.tableCell.updateUI();
        }
        LookAndFeel.installColorsAndFont(this, "Tree.background", "Tree.foreground", "Tree.font");
        if (UIManager.getColor("Table.selectionBackground") == null) {
            UIManager.put("Table.selectionBackground", new JTable().getSelectionBackground());
        }
        if (UIManager.getColor("Table.selectionForeground") == null) {
            UIManager.put("Table.selectionForeground", new JTable().getSelectionForeground());
        }
        if (UIManager.getColor("Table.gridColor") == null) {
            UIManager.put("Table.gridColor", new JTable().getGridColor());
        }
        this.setUI(new TreeTableUI());
        this.needCalcRowHeight = true;
    }

    @Override
    public int getEditingRow() {
        return this.getColumnClass(this.editingColumn) == TreeTableModelAdapter.class ? -1 : this.editingRow;
    }

    @Override
    protected final void configureEnclosingScrollPane() {
        Container gp;
        Container p = this.getParent();
        if (p instanceof JViewport && (gp = p.getParent()) instanceof JScrollPane) {
            JScrollPane scrollPane = (JScrollPane)gp;
            JViewport viewport = scrollPane.getViewport();
            if (viewport == null || viewport.getView() != this) {
                return;
            }
            JTableHeader jth = this.getTableHeader();
            if (jth != null) {
                jth.setBorder(null);
            }
            scrollPane.setColumnHeaderView(jth);
        }
    }

    TableQuickSearchSupport.QuickSearchSettings getQuickSearchSettings() {
        return this.qss;
    }

    QuickSearchTableFilter getQuickSearchTableFilter() {
        return this.qstf;
    }

    @Override
    public void paint(Graphics g) {
        new GuardedActions(0, g);
    }

    public void paintImpl(Graphics g) {
        if (this.needCalcRowHeight) {
            this.calcRowHeight(g);
            return;
        }
        super.paint(g);
    }

    @Override
    protected void validateTree() {
        new GuardedActions(1, null);
    }

    @Override
    public Dimension getPreferredSize() {
        return (Dimension)(TreeTable)this.new GuardedActions((int)5, (Object)null).ret;
    }

    @Override
    public void doLayout() {
        new GuardedActions(2, null);
    }

    @Override
    protected boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition, boolean pressed) {
        return (Boolean)(TreeTable)this.new GuardedActions((int)10, (Object)new Object[]{ks, e, Integer.valueOf((int)condition), Boolean.valueOf((boolean)pressed)}).ret;
    }

    private void calcRowHeight(Graphics g) {
        Font f = this.getFont();
        FontMetrics fm = g.getFontMetrics(f);
        int rh = fm.getHeight() + fm.getMaxDescent();
        this.needCalcRowHeight = false;
        rh = Math.max(20, rh);
        this.tree.setRowHeight(rh);
        this.setRowHeight(rh);
    }

    JTree getTree() {
        return this.tree;
    }

    int getTreeColumnIndex() {
        return this.treeColumnIndex;
    }

    void setTreeColumnIndex(int index) {
        if (this.treeColumnIndex == index) {
            return;
        }
        int old = this.treeColumnIndex;
        this.treeColumnIndex = index;
        this.firePropertyChange("treeColumnIndex", old, this.treeColumnIndex);
    }

    @Override
    public void clearSelection() {
        if (!this.ignoreClearSelection) {
            super.clearSelection();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void tableChanged(TableModelEvent e) {
        int modelColumn = this.getTreeColumnIndex();
        if (e.getFirstRow() <= 0 && modelColumn != -1 && this.getColumnCount() > 0) {
            String columnName = this.getModel().getColumnName(modelColumn);
            TableColumn aColumn = this.getColumnModel().getColumn(modelColumn);
            aColumn.setHeaderValue(columnName);
        }
        this.ignoreClearSelection = true;
        try {
            super.tableChanged(e);
            if (null != this.getTree()) {
                this.firePropertyChange("positionX", -1, this.getPositionX());
            }
        }
        finally {
            this.ignoreClearSelection = false;
        }
    }

    @Override
    public void processKeyEvent(KeyEvent e) {
        if (this.isEditing() && (e.getKeyCode() == 40 || e.getKeyCode() == 38)) {
            return;
        }
        if (!this.isEditing() || e.getKeyCode() != 9 && e.getKeyCode() != 27 || (e.getModifiers() & 2) != 0) {
            super.processKeyEvent(e);
        } else {
            this.processKeyBinding(KeyStroke.getKeyStroke(e.getKeyCode(), e.getModifiersEx(), e.getID() == 402), e, 0, e.getID() == 401);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Loose catch block
     * Enabled aggressive exception aggregation
     */
    @Override
    public boolean editCellAt(int row, int column, EventObject e) {
        boolean editable;
        MouseEvent me;
        if (e instanceof MouseEvent && column != 0 && (!SwingUtilities.isLeftMouseButton(me = (MouseEvent)e) || me.getID() != 501)) {
            return false;
        }
        if (row >= this.getRowCount() || row < 0 || column > this.getColumnCount() || column < 0) {
            return false;
        }
        this.inEditRequest = true;
        this.editRow = row;
        if (this.editingRow == row && this.editingColumn == column && this.isEditing()) {
            this.inEditRequest = false;
            return false;
        }
        if (this.isEditing()) {
            this.inEditorChangeRequest = true;
            try {
                this.removeEditor();
                this.changeSelection(row, column, false, false);
            }
            finally {
                this.inEditorChangeRequest = false;
            }
        }
        if ((editable = this.getModel().isCellEditable(row, column)) && (e == null || e instanceof KeyEvent) && column == 0) {
            editable = false;
            column = 1;
        }
        boolean columnShifted = false;
        if (!editable && (e instanceof KeyEvent || e == null)) {
            for (int i = column; i < this.getColumnCount(); ++i) {
                if (!this.getModel().isCellEditable(row, i)) continue;
                columnShifted = i != column;
                column = i;
                this.changeSelection(row, column, false, false);
                break;
            }
        }
        final Rectangle r = this.getCellRect(row, column, true);
        boolean canTryCustomEditor = !columnShifted && e instanceof MouseEvent ? ((MouseEvent)e).getX() > r.x + r.width - 24 && ((MouseEvent)e).getX() < r.x + r.width : true;
        try {
            boolean ret;
            this.canEdit = this.lastRow == row;
            Object o = this.getValueAt(row, column);
            if (o instanceof Node.Property) {
                Action act;
                PropertyEditor ed;
                PropertyPanel panel2222;
                Node.Property p = (Node.Property)o;
                if (p.canWrite() && (p.getValueType() == Boolean.class || p.getValueType() == Boolean.TYPE)) {
                    try {
                        Boolean val = (Boolean)p.getValue();
                        if (Boolean.FALSE.equals(val)) {
                            p.setValue((Object)Boolean.TRUE);
                        } else {
                            p.setValue((Object)Boolean.FALSE);
                        }
                        this.repaint(r.x, r.y, r.width, r.height);
                        boolean bl = false;
                        return bl;
                    }
                    catch (Exception e1) {
                        Logger.getLogger(TreeTable.class.getName()).log(Level.WARNING, null, e1);
                        boolean bl = false;
                        this.inEditRequest = false;
                        return bl;
                    }
                }
                if (canTryCustomEditor && !Boolean.TRUE.equals(p.getValue("suppressCustomEditor")) && (ed = (panel2222 = new PropertyPanel(p)).getPropertyEditor()) != null && ed.supportsCustomEditor() && (act = panel2222.getActionMap().get("invokeCustomEditor")) != null) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            r.x = 0;
                            r.width = TreeTable.this.getWidth();
                            TreeTable.this.repaint(r);
                        }
                    });
                    act.actionPerformed(null);
                    boolean bl = false;
                    return bl;
                }
                if (!p.canWrite()) {
                    boolean panel2222 = false;
                    return panel2222;
                }
            }
            if (ret = super.editCellAt(row, column, e)) {
                if (column == this.getTreeColumnIndex()) {
                    this.ignoreScrolling = true;
                    this.tree.scrollRectToVisible(this.tree.getRowBounds(row));
                    this.ignoreScrolling = false;
                } else {
                    SwingUtilities.invokeLater(this);
                }
            }
            boolean panel2222 = ret;
            return panel2222;
            {
                catch (Throwable throwable) {
                    throw throwable;
                }
            }
        }
        finally {
            this.inEditRequest = false;
        }
    }

    @Override
    public void run() {
        if (this.editorComp != null && this.editorComp.isShowing()) {
            this.editorComp.requestFocus();
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        this.lastRow = this.getSelectedRowCount() == 1 ? this.getSelectedRow() : -1;
        super.valueChanged(e);
    }

    @Override
    public void columnAdded(TableColumnModelEvent e) {
        super.columnAdded(e);
        this.updateTreeColumnIndex();
    }

    @Override
    public void columnRemoved(TableColumnModelEvent e) {
        super.columnRemoved(e);
        this.updateTreeColumnIndex();
    }

    @Override
    public void columnMoved(TableColumnModelEvent e) {
        super.columnMoved(e);
        this.updateTreeColumnIndex();
        int from = e.getFromIndex();
        int to = e.getToIndex();
        if (from != to) {
            this.firePropertyChange("column_moved", from, to);
        }
    }

    private void updateTreeColumnIndex() {
        for (int i = this.getColumnCount() - 1; i >= 0; --i) {
            if (this.getColumnClass(i) != TreeTableModelAdapter.class) continue;
            this.setTreeColumnIndex(i);
            return;
        }
        this.setTreeColumnIndex(-1);
    }

    public int getPositionX() {
        return this.positionX;
    }

    public void setPositionX(int x) {
        if (x == this.positionX || !this.treeHScrollingEnabled) {
            return;
        }
        int old = this.positionX;
        this.positionX = x;
        this.firePropertyChange("positionX", old, x);
        if (this.isEditing() && this.getEditingColumn() == this.getTreeColumnIndex()) {
            TableCellEditor editor = this.getCellEditor();
            if (this.ignoreScrolling && editor instanceof TreeTableCellEditor) {
                ((TreeTableCellEditor)editor).revalidateTextField();
            } else {
                this.removeEditor();
            }
        }
        this.repaint();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (this.hasFocus() && this.getSelectedColumn() == 0 && this.getSelectedRow() > 0) {
            Color bdr = UIManager.getColor("Tree.selectionBorderColor");
            if (bdr == null) {
                bdr = this.getForeground().equals(Color.BLACK) ? this.getBackground().darker() : this.getForeground().darker();
            }
            g.setColor(bdr);
            Rectangle r = this.getCellRect(this.getSelectedRow(), this.getSelectedColumn(), false);
            g.drawRect(r.x + 1, r.y + 1, r.width - 3, r.height - 3);
        }
    }

    void setTreeHScrollingEnabled(boolean enabled) {
        this.treeHScrollingEnabled = enabled;
    }

    boolean isKnownComponent(Component c) {
        if (c == null) {
            return false;
        }
        if (this.isAncestorOf(c)) {
            return true;
        }
        if (c == this.editorComp) {
            return true;
        }
        if (this.editorComp != null && this.editorComp instanceof Container && ((Container)this.editorComp).isAncestorOf(c)) {
            return true;
        }
        return false;
    }

    public boolean isValidationRoot() {
        return true;
    }

    @Override
    public void paintImmediately(int x, int y, int w, int h) {
        if (this.inEditorChangeRequest) {
            return;
        }
        super.paintImmediately(x, y, w, h);
    }

    @Override
    protected void processFocusEvent(FocusEvent fe) {
        super.processFocusEvent(fe);
        if (!(fe.getID() != 1005 || fe.isTemporary() || this.inRemoveRequest || this.inEditRequest)) {
            boolean stopEditing;
            boolean bl = stopEditing = fe.getOppositeComponent() != this.getParent() && !this.isKnownComponent(fe.getOppositeComponent()) && fe.getOppositeComponent() != null;
            if (stopEditing) {
                this.removeEditor();
            }
        }
        if (!this.inRemoveRequest && !this.inEditRequest) {
            this.repaintSelection(fe.getID() == 1004);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeEditor() {
        this.inRemoveRequest = true;
        try {
            Object object = this.getTreeLock();
            synchronized (object) {
                super.removeEditor();
            }
        }
        finally {
            this.inRemoveRequest = false;
        }
    }

    private void repaintSelection(boolean focused) {
        if (Children.MUTEX.isReadAccess() || Children.MUTEX.isWriteAccess()) {
            int start = this.getSelectionModel().getMinSelectionIndex();
            int end = this.getSelectionModel().getMaxSelectionIndex();
            if (end != -1) {
                if (end != start) {
                    Rectangle begin = this.getCellRect(start, 0, false);
                    Rectangle r = this.getCellRect(end, 0, false);
                    r.y = begin.y;
                    r.x = 0;
                    r.width = this.getWidth();
                    r.height = r.y + r.height - begin.y;
                    this.repaint(r.x, r.y, r.width, r.height);
                } else {
                    Rectangle r = this.getCellRect(start, 0, false);
                    r.width = this.getWidth();
                    r.x = 0;
                    this.repaint(r.x, r.y, r.width, r.height);
                }
            }
            if (this.isEditing() && this.editorComp != null) {
                this.editorComp.setBackground(focused ? this.getSelectionBackground() : TreeTable.getUnfocusedSelectedBackground());
                this.editorComp.setForeground(focused ? this.getSelectionForeground() : TreeTable.getUnfocusedSelectedForeground());
            }
        } else {
            new GuardedActions(3, focused);
        }
    }

    static Color getUnfocusedSelectedBackground() {
        if (unfocusedSelBg == null && (TreeTable.unfocusedSelBg = UIManager.getColor("nb.explorer.unfocusedSelBg")) == null) {
            unfocusedSelBg = UIManager.getColor("controlShadow");
            if (unfocusedSelBg == null) {
                unfocusedSelBg = Color.lightGray;
            }
            unfocusedSelBg = unfocusedSelBg.brighter();
        }
        return unfocusedSelBg;
    }

    static Color getUnfocusedSelectedForeground() {
        if (unfocusedSelFg == null && (TreeTable.unfocusedSelFg = UIManager.getColor("nb.explorer.unfocusedSelFg")) == null && (TreeTable.unfocusedSelFg = UIManager.getColor("textText")) == null) {
            unfocusedSelFg = Color.BLACK;
        }
        return unfocusedSelFg;
    }

    @Override
    protected JTableHeader createDefaultTableHeader() {
        return new TreeTableHeader(this.getColumnModel());
    }

    private class CTRLTabAction
    extends AbstractAction {
        private CTRLTabAction() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            TreeTable.this.setFocusCycleRoot(false);
            try {
                Container con = TreeTable.this.getFocusCycleRootAncestor();
                if (con != null) {
                    Component to;
                    Container target = TreeTable.this;
                    if (TreeTable.this.getParent() instanceof JViewport && (target = TreeTable.this.getParent().getParent()) == con) {
                        target = TreeTable.this;
                    }
                    AWTEvent eo = EventQueue.getCurrentEvent();
                    boolean backward = false;
                    if (eo instanceof KeyEvent) {
                        backward = (((KeyEvent)eo).getModifiers() & 1) != 0 && (((KeyEvent)eo).getModifiersEx() & 64) != 0;
                    }
                    Component component = to = backward ? con.getFocusTraversalPolicy().getComponentAfter(con, TreeTable.this) : con.getFocusTraversalPolicy().getComponentAfter(con, TreeTable.this);
                    if (to == TreeTable.this) {
                        to = backward ? con.getFocusTraversalPolicy().getFirstComponent(con) : con.getFocusTraversalPolicy().getLastComponent(con);
                    }
                    to.requestFocus();
                }
            }
            finally {
                TreeTable.this.setFocusCycleRoot(true);
            }
        }
    }

    private class EnterAction
    extends AbstractAction {
        private EnterAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            JButton b;
            JRootPane jrp = TreeTable.this.getRootPane();
            if (jrp != null && (b = TreeTable.this.getRootPane().getDefaultButton()) != null && b.isEnabled()) {
                b.doClick();
            }
        }

        @Override
        public boolean isEnabled() {
            return !TreeTable.this.isEditing() && !TreeTable.this.inRemoveRequest;
        }
    }

    private class CancelEditAction
    extends AbstractAction {
        private CancelEditAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Action a;
            if (TreeTable.this.isEditing() || TreeTable.this.editorComp != null) {
                TreeTable.this.removeEditor();
                return;
            }
            Component c = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            InputMap imp = TreeTable.this.getRootPane().getInputMap(1);
            ActionMap am = TreeTable.this.getRootPane().getActionMap();
            KeyStroke escape = KeyStroke.getKeyStroke(27, 0, false);
            Object key = imp.get(escape);
            if (key == null) {
                key = "Cancel";
            }
            if (key != null && (a = am.get(key)) != null) {
                String commandKey = (String)a.getValue("ActionCommandKey");
                if (commandKey == null) {
                    commandKey = key.toString();
                }
                a.actionPerformed(new ActionEvent(this, 1001, commandKey));
            }
        }

        @Override
        public boolean isEnabled() {
            return TreeTable.this.isEditing();
        }
    }

    private class EditAction
    extends AbstractAction {
        private EditAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int row = TreeTable.this.getSelectedRow();
            int col = TreeTable.this.getSelectedColumn();
            if (col == 0) {
                col = 1;
            }
            TreeTable.this.editCellAt(row, col, null);
        }

        @Override
        public boolean isEnabled() {
            return TreeTable.this.getSelectedRow() != -1 && TreeTable.this.getSelectedColumn() != -1 && !TreeTable.this.isEditing() && TreeTable.this.getSelectedColumn() != TreeTable.this.getTreeColumnIndex();
        }
    }

    private final class NavigationAction
    extends AbstractAction {
        private boolean direction;

        public NavigationAction(boolean direction) {
            this.direction = direction;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int targetRow;
            int targetColumn;
            if (TreeTable.this.isEditing()) {
                TreeTable.this.removeEditor();
            }
            if (this.direction) {
                if (TreeTable.this.getSelectedColumn() == TreeTable.this.getColumnCount() - 1) {
                    targetColumn = 0;
                    targetRow = TreeTable.this.getSelectedRow() + 1;
                } else {
                    targetColumn = TreeTable.this.getSelectedColumn() + 1;
                    targetRow = TreeTable.this.getSelectedRow();
                }
            } else if (TreeTable.this.getSelectedColumn() <= 0) {
                targetColumn = TreeTable.this.getColumnCount() - 1;
                targetRow = TreeTable.this.getSelectedRow() - 1;
            } else {
                targetRow = TreeTable.this.getSelectedRow();
                targetColumn = TreeTable.this.getSelectedColumn() - 1;
            }
            if (targetRow >= TreeTable.this.getRowCount() || targetRow < 0) {
                Container grandcestor;
                Component sibling;
                JRootPane rp;
                JButton jb;
                Container ancestor = TreeTable.this.getFocusCycleRootAncestor();
                Component component = sibling = this.direction ? ancestor.getFocusTraversalPolicy().getComponentAfter(ancestor, TreeTable.this.getParent()) : ancestor.getFocusTraversalPolicy().getComponentBefore(ancestor, TreeTable.this);
                if (sibling == TreeTable.this && (grandcestor = ancestor.getFocusCycleRootAncestor()) != null) {
                    sibling = this.direction ? grandcestor.getFocusTraversalPolicy().getComponentAfter(grandcestor, ancestor) : grandcestor.getFocusTraversalPolicy().getComponentBefore(grandcestor, ancestor);
                    ancestor = grandcestor;
                }
                if (sibling == TreeTable.this && ancestor.getFocusTraversalPolicy().getFirstComponent(ancestor) != null) {
                    sibling = ancestor.getFocusTraversalPolicy().getFirstComponent(ancestor);
                }
                if (sibling == TreeTable.this && (jb = (rp = TreeTable.this.getRootPane()).getDefaultButton()) != null) {
                    sibling = jb;
                }
                if (sibling != null) {
                    if (sibling == TreeTable.this) {
                        TreeTable.this.changeSelection(this.direction ? 0 : TreeTable.this.getRowCount() - 1, this.direction ? 0 : TreeTable.this.getColumnCount() - 1, false, false);
                    } else {
                        sibling.requestFocus();
                    }
                    return;
                }
            }
            TreeTable.this.changeSelection(targetRow, targetColumn, false, false);
        }
    }

    private class STPolicy
    extends ContainerOrderFocusTraversalPolicy {
        private STPolicy() {
        }

        @Override
        public Component getComponentAfter(Container focusCycleRoot, Component aComponent) {
            if (TreeTable.this.inRemoveRequest) {
                return TreeTable.this;
            }
            Component result = super.getComponentAfter(focusCycleRoot, aComponent);
            return result;
        }

        @Override
        public Component getComponentBefore(Container focusCycleRoot, Component aComponent) {
            if (TreeTable.this.inRemoveRequest) {
                return TreeTable.this;
            }
            return super.getComponentBefore(focusCycleRoot, aComponent);
        }

        @Override
        public Component getFirstComponent(Container focusCycleRoot) {
            if (!TreeTable.this.inRemoveRequest && TreeTable.this.isEditing()) {
                return TreeTable.this.editorComp;
            }
            return TreeTable.this;
        }

        @Override
        public Component getDefaultComponent(Container focusCycleRoot) {
            if (TreeTable.this.inRemoveRequest && TreeTable.this.isEditing() && TreeTable.this.editorComp.isShowing()) {
                return TreeTable.this.editorComp;
            }
            return TreeTable.this;
        }

        @Override
        protected boolean accept(Component aComponent) {
            if (TreeTable.this.isEditing() && TreeTable.this.inEditRequest) {
                return TreeTable.this.isKnownComponent(aComponent);
            }
            return super.accept(aComponent) && aComponent.isShowing();
        }
    }

    class TreeTableAction
    extends AbstractAction {
        Action treeAction;
        Action tableAction;

        TreeTableAction(Action treeAction, Action tableAction) {
            this.treeAction = treeAction;
            this.tableAction = tableAction;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (TreeTable.this.getSelectedColumn() == TreeTable.this.getTreeColumnIndex()) {
                e.setSource(TreeTable.this.getTree());
                this.treeAction.actionPerformed(e);
            } else {
                this.tableAction.actionPerformed(e);
            }
        }
    }

    class TreeTableUI
    extends BasicTableUI {
        TreeTableUI() {
        }

        @Override
        protected MouseInputListener createMouseInputListener() {
            return new TreeTableMouseInputHandler();
        }

        public class TreeTableMouseInputHandler
        extends BasicTableUI.MouseInputHandler {
            private Component dispatchComponent;

            public TreeTableMouseInputHandler() {
                super(TreeTableUI.this);
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                this.processMouseEvent(e);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                this.processMouseEvent(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (this.shouldIgnore(e)) {
                    return;
                }
                this.repostEvent(e);
                this.dispatchComponent = null;
                this.setValueIsAdjusting(false);
                if (!TreeTable.this.isEditing()) {
                    this.processMouseEvent(e);
                }
            }

            @Override
            public void mouseDragged(MouseEvent e) {
            }

            private void setDispatchComponent(MouseEvent e) {
                Component editorComponent = TreeTableUI.this.table.getEditorComponent();
                Point p = e.getPoint();
                Point p2 = SwingUtilities.convertPoint(TreeTableUI.this.table, p, editorComponent);
                this.dispatchComponent = SwingUtilities.getDeepestComponentAt(editorComponent, p2.x, p2.y);
            }

            private boolean repostEvent(MouseEvent e) {
                if (this.dispatchComponent == null) {
                    return false;
                }
                MouseEvent e2 = SwingUtilities.convertMouseEvent(TreeTableUI.this.table, e, this.dispatchComponent);
                this.dispatchComponent.dispatchEvent(e2);
                return true;
            }

            private void setValueIsAdjusting(boolean flag) {
                TreeTableUI.this.table.getSelectionModel().setValueIsAdjusting(flag);
                TreeTableUI.this.table.getColumnModel().getSelectionModel().setValueIsAdjusting(flag);
            }

            private boolean shouldIgnore(MouseEvent e) {
                return !TreeTableUI.this.table.isEnabled() || e.getButton() == 3 && e.getClickCount() == 1 && !e.isPopupTrigger();
            }

            private boolean isTreeColumn(int column) {
                return TreeTable.this.getColumnClass(column) == TreeTableModelAdapter.class;
            }

            private void processMouseEvent(MouseEvent e) {
                if (this.shouldIgnore(e)) {
                    return;
                }
                Point p = e.getPoint();
                int row = TreeTableUI.this.table.rowAtPoint(p);
                int column = TreeTableUI.this.table.columnAtPoint(p);
                if (column == -1 || row == -1) {
                    return;
                }
                if (TreeTable.this.getEditingColumn() == column && TreeTable.this.getEditingRow() == row) {
                    return;
                }
                boolean changeSelection = true;
                if (this.isTreeColumn(column)) {
                    TreePath path = TreeTable.this.tree.getPathForRow(TreeTable.this.rowAtPoint(e.getPoint()));
                    Rectangle r = TreeTable.this.tree.getPathBounds(path);
                    if (e.getX() >= r.x - TreeTable.this.positionX && e.getX() <= r.x - TreeTable.this.positionX + r.width || this.isLocationInExpandControl(path, p) || e.getID() == 502 || e.getID() == 500) {
                        changeSelection = false;
                    }
                }
                if (TreeTableUI.this.table.getSelectionModel().isSelectedIndex(row) && e.isPopupTrigger()) {
                    return;
                }
                if (TreeTableUI.this.table.editCellAt(row, column, e)) {
                    this.setDispatchComponent(e);
                    this.repostEvent(e);
                }
                if (e.getID() == 501) {
                    TreeTableUI.this.table.requestFocus();
                }
                TableCellEditor editor = TreeTableUI.this.table.getCellEditor();
                if (changeSelection && (editor == null || editor.shouldSelectCell(e))) {
                    this.setValueIsAdjusting(true);
                    TreeTableUI.this.table.changeSelection(row, column, Utilities.isMac() ? e.isMetaDown() : e.isControlDown(), e.isShiftDown());
                    this.setValueIsAdjusting(false);
                }
            }

            private boolean isLocationInExpandControl(TreePath path, Point location) {
                if (TreeTable.this.tree.getModel().isLeaf(path.getLastPathComponent())) {
                    return false;
                }
                Rectangle r = TreeTable.this.tree.getPathBounds(path);
                int boxWidth = 8;
                Insets i = TreeTable.this.tree.getInsets();
                int indent = 0;
                if (TreeTable.this.tree.getUI() instanceof BasicTreeUI) {
                    BasicTreeUI ui = (BasicTreeUI)TreeTable.this.tree.getUI();
                    if (null != ui.getExpandedIcon()) {
                        boxWidth = ui.getExpandedIcon().getIconWidth();
                    }
                    indent = ui.getLeftChildIndent();
                }
                int boxX = TreeTable.this.tree.getComponentOrientation().isLeftToRight() ? r.x - TreeTable.this.positionX - indent - boxWidth : r.x - TreeTable.this.positionX + indent + r.width;
                return location.getX() >= (double)boxX && location.getX() <= (double)(boxX + boxWidth);
            }
        }

    }

    class TreeTableSelectionModel
    extends DefaultListSelectionModel {
        TreeTableSelectionModel() {
        }

        @Override
        public void setAnchorSelectionIndex(int anchorIndex) {
            if (TreeTable.this.ignoreClearSelection) {
                return;
            }
            super.setAnchorSelectionIndex(anchorIndex);
        }

        @Override
        public void setLeadSelectionIndex(int leadIndex) {
            if (TreeTable.this.ignoreClearSelection) {
                return;
            }
            super.setLeadSelectionIndex(leadIndex);
        }
    }

    class ListToTreeSelectionModelWrapper
    extends DefaultTreeSelectionModel {
        protected boolean updatingListSelectionModel;

        public ListToTreeSelectionModelWrapper() {
            this.listSelectionModel = new TreeTableSelectionModel();
            this.getListSelectionModel().addListSelectionListener(this.createListSelectionListener());
        }

        ListSelectionModel getListSelectionModel() {
            return this.listSelectionModel;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void resetRowSelection() {
            if (!this.updatingListSelectionModel) {
                this.updatingListSelectionModel = true;
                try {
                    super.resetRowSelection();
                }
                finally {
                    this.updatingListSelectionModel = false;
                }
            }
        }

        protected ListSelectionListener createListSelectionListener() {
            return new ListSelectionHandler();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected void updateSelectedPathsFromSelectedRows() {
            block8 : {
                if (!this.updatingListSelectionModel) {
                    this.updatingListSelectionModel = true;
                    try {
                        int min = this.listSelectionModel.getMinSelectionIndex();
                        int max = this.listSelectionModel.getMaxSelectionIndex();
                        if (min == 0 && max == TreeTable.this.getRowCount()) {
                            int[] rows = new int[max];
                            int i = 0;
                            while (i < rows.length) {
                                rows[i] = i++;
                            }
                            TreeTable.this.tree.setSelectionRows(rows);
                            break block8;
                        }
                        ArrayList<Integer> list = new ArrayList<Integer>(11);
                        for (int i = min; i <= max; ++i) {
                            if (!this.listSelectionModel.isSelectedIndex(i)) continue;
                            list.add(i);
                        }
                        if (list.isEmpty()) {
                            this.clearSelection();
                            break block8;
                        }
                        int[] rows = (int[])Utilities.toPrimitiveArray((Object[])list.toArray(new Integer[list.size()]));
                        TreeTable.this.tree.setSelectionRows(rows);
                    }
                    finally {
                        this.updatingListSelectionModel = false;
                    }
                }
            }
        }

        class ListSelectionHandler
        implements ListSelectionListener {
            ListSelectionHandler() {
            }

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (TreeTable.this.inSelectAll || e.getValueIsAdjusting()) {
                    return;
                }
                ListToTreeSelectionModelWrapper.this.updateSelectedPathsFromSelectedRows();
            }
        }

    }

    static class TreeTableTextField
    extends JTextField {
        public int offset;

        TreeTableTextField() {
        }

        @Override
        public void setBounds(int x, int y, int w, int h) {
            int newX = Math.max(x, this.offset);
            super.setBounds(newX, y, w - (newX - x), h);
        }
    }

    class TreeTableCellEditor
    extends DefaultCellEditor
    implements TreeSelectionListener,
    ActionListener,
    FocusListener,
    CellEditorListener {
        protected transient int offset;
        protected transient Timer timer;

        public TreeTableCellEditor() {
            super(new TreeTableTextField());
            TreeTable.this.tree.addTreeSelectionListener(this);
            this.addCellEditorListener(this);
            super.getComponent().addFocusListener(this);
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int r, int c) {
            Component component = super.getTableCellEditorComponent(table, value, isSelected, r, c);
            this.determineOffset(value, isSelected, r);
            ((TreeTableTextField)this.getComponent()).offset = this.offset;
            return component;
        }

        @Override
        public boolean isCellEditable(EventObject e) {
            if (TreeTable.this.lastRow != -1) {
                Node n;
                TreePath tp = TreeTable.this.tree.getPathForRow(TreeTable.this.lastRow);
                Node node = n = tp != null ? Visualizer.findNode(tp.getLastPathComponent()) : null;
                if (n == null || !n.canRename()) {
                    TreeTable.this.canEdit = false;
                }
            }
            if (TreeTable.this.canEdit && e != null && e.getSource() instanceof Timer) {
                return true;
            }
            if (TreeTable.this.canEdit && this.shouldStartEditingTimer(e)) {
                this.startEditingTimer();
            } else if (this.shouldStopEditingTimer(e)) {
                this.timer.stop();
            }
            if (e instanceof MouseEvent) {
                MouseEvent me = (MouseEvent)e;
                int column = TreeTable.this.getTreeColumnIndex();
                if (SwingUtilities.isLeftMouseButton(me) && me.getClickCount() == 2) {
                    TreePath path = TreeTable.this.tree.getPathForRow(TreeTable.this.rowAtPoint(me.getPoint()));
                    Rectangle r = TreeTable.this.tree.getPathBounds(path);
                    if (me.getX() < r.x - TreeTable.this.positionX || me.getX() > r.x - TreeTable.this.positionX + r.width) {
                        me.translatePoint(r.x - me.getX(), 0);
                    }
                }
                MouseEvent newME = new MouseEvent(TreeTable.this.tree, me.getID(), me.getWhen(), me.getModifiers() + me.getModifiersEx(), me.getX() - TreeTable.this.getCellRect((int)0, (int)column, (boolean)true).x + TreeTable.this.positionX, me.getY(), me.getClickCount(), me.isPopupTrigger());
                TreeTable.this.tree.dispatchEvent(newME);
            }
            return false;
        }

        @Override
        public void valueChanged(TreeSelectionEvent e) {
            if (this.timer != null) {
                this.timer.stop();
            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (TreeTable.this.lastRow != -1) {
                TreeTable.this.editCellAt(TreeTable.this.lastRow, TreeTable.this.getTreeColumnIndex(), new EventObject(this.timer));
            }
        }

        private boolean shouldStartEditingTimer(EventObject event) {
            if (event instanceof MouseEvent && SwingUtilities.isLeftMouseButton((MouseEvent)event)) {
                MouseEvent me = (MouseEvent)event;
                return me.getID() == 501 && me.getClickCount() == 1 && this.inHitRegion(me);
            }
            return false;
        }

        private boolean shouldStopEditingTimer(EventObject event) {
            if (this.timer == null) {
                return false;
            }
            if (event instanceof MouseEvent) {
                MouseEvent me = (MouseEvent)event;
                return !SwingUtilities.isLeftMouseButton(me) || me.getClickCount() > 1;
            }
            return false;
        }

        private void startEditingTimer() {
            if (this.timer == null) {
                this.timer = new Timer(1200, this);
                this.timer.setRepeats(false);
            }
            this.timer.start();
        }

        private boolean inHitRegion(MouseEvent me) {
            this.determineOffset(me);
            if (me.getX() <= this.offset) {
                return false;
            }
            return true;
        }

        private void determineOffset(MouseEvent me) {
            int row = TreeTable.this.rowAtPoint(me.getPoint());
            if (row == -1) {
                this.offset = 0;
                return;
            }
            this.determineOffset(TreeTable.this.tree.getPathForRow(row).getLastPathComponent(), TreeTable.this.isRowSelected(row), row);
        }

        private void determineOffset(Object value, boolean isSelected, int row) {
            Object node;
            Icon icon;
            JTree t = TreeTable.this.getTree();
            boolean rv = t.isRootVisible();
            int offsetRow = row;
            if (!rv && row > 0) {
                --offsetRow;
            }
            Rectangle bounds = t.getRowBounds(offsetRow);
            this.offset = bounds.x;
            TreeCellRenderer tcr = t.getCellRenderer();
            Component comp = tcr.getTreeCellRendererComponent(t, node = t.getPathForRow(offsetRow).getLastPathComponent(), isSelected, t.isExpanded(offsetRow), t.getModel().isLeaf(node), offsetRow, false);
            if (comp instanceof JLabel && (icon = ((JLabel)comp).getIcon()) != null) {
                this.offset += ((JLabel)comp).getIconTextGap() + icon.getIconWidth();
            }
            this.offset -= TreeTable.this.positionX;
        }

        private void revalidateTextField() {
            int row = TreeTable.this.editingRow;
            if (row == -1) {
                this.offset = 0;
                return;
            }
            this.determineOffset(TreeTable.this.tree.getPathForRow(row).getLastPathComponent(), TreeTable.this.isRowSelected(row), row);
            ((TreeTableTextField)super.getComponent()).offset = this.offset;
            this.getComponent().setBounds(TreeTable.this.getCellRect(row, TreeTable.this.getTreeColumnIndex(), false));
        }

        @Override
        public void focusLost(FocusEvent evt) {
        }

        @Override
        public void focusGained(FocusEvent evt) {
            ((TreeTableTextField)super.getComponent()).selectAll();
        }

        @Override
        public void editingStopped(ChangeEvent e) {
            Node n;
            TreePath lastP = TreeTable.this.tree.getPathForRow(TreeTable.this.lastRow);
            if (lastP != null && (n = Visualizer.findNode(lastP.getLastPathComponent())) != null && n.canRename()) {
                String newStr = (String)this.getCellEditorValue();
                ViewUtil.nodeRename(n, newStr);
            }
        }

        @Override
        public void editingCanceled(ChangeEvent e) {
        }
    }

    class TreeTableCellRenderer
    extends JTree
    implements TableCellRenderer {
        protected int visibleRow;
        private int oldWidth;
        private int transY;

        public TreeTableCellRenderer(TreeModel model) {
            super(model);
            this.transY = 0;
            this.setToggleClickCount(0);
            this.putClientProperty("JTree.lineStyle", "None");
        }

        @Override
        public void validate() {
        }

        @Override
        public void repaint(long tm, int x, int y, int width, int height) {
        }

        @Override
        public void addHierarchyListener(HierarchyListener hl) {
        }

        @Override
        public void addComponentListener(ComponentListener cl) {
        }

        TreeTable getTreeTable() {
            return TreeTable.this;
        }

        @Override
        public void setRowHeight(int rowHeight) {
            if (rowHeight > 0) {
                super.setRowHeight(rowHeight);
                TreeTable.this.setRowHeight(rowHeight);
            }
        }

        @Override
        public void setBounds(int x, int y, int w, int h) {
            this.transY = - y;
            int oldW = this.getWidth();
            super.setBounds(0, 0, TreeTable.this.getColumnModel().getColumn(0).getWidth(), TreeTable.this.getHeight());
            if (oldW != w) {
                this.firePropertyChange("width", oldW, w);
            }
        }

        @Override
        public void paint(Graphics g) {
            g.translate(- TreeTable.this.getPositionX(), this.transY);
            super.paint(g);
        }

        @Override
        public Rectangle getVisibleRect() {
            Rectangle visibleRect = TreeTable.this.getVisibleRect();
            visibleRect.x = TreeTable.this.positionX;
            visibleRect.width = TreeTable.this.getColumnModel().getColumn(TreeTable.this.getTreeColumnIndex()).getWidth();
            return visibleRect;
        }

        @Override
        public void scrollRectToVisible(Rectangle aRect) {
            Rectangle rect = this.getVisibleRect();
            rect.y = aRect.y;
            rect.height = aRect.height;
            TreeTable.this.scrollRectToVisible(rect);
            int x = rect.x;
            if (aRect.width > rect.width) {
                x = aRect.x;
            } else if (aRect.x < rect.x) {
                x = aRect.x;
            } else if (aRect.x + aRect.width > rect.x + rect.width) {
                x = aRect.x + aRect.width - rect.width;
            }
            TreeTable.this.setPositionX(x);
        }

        @Override
        public String getToolTipText(MouseEvent event) {
            if (event != null) {
                Point p = event.getPoint();
                p.translate(TreeTable.this.positionX, this.visibleRow * this.getRowHeight());
                int selRow = this.getRowForLocation(p.x, p.y);
                if (selRow != -1) {
                    TreePath path = this.getPathForRow(selRow);
                    VisualizerNode v = (VisualizerNode)path.getLastPathComponent();
                    String tooltip = v.getShortDescription();
                    String displayName = v.getDisplayName();
                    if (tooltip != null && !tooltip.equals(displayName)) {
                        return tooltip;
                    }
                }
            }
            return null;
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            if (isSelected) {
                Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
                boolean tableHasFocus = focusOwner == this || focusOwner == TreeTable.this || TreeTable.this.isAncestorOf(focusOwner) || focusOwner instanceof JRootPane;
                this.setBackground(tableHasFocus ? table.getSelectionBackground() : TreeTable.getUnfocusedSelectedBackground());
                this.setForeground(tableHasFocus ? table.getSelectionForeground() : TreeTable.getUnfocusedSelectedForeground());
            } else {
                this.setBackground(table.getBackground());
                this.setForeground(table.getForeground());
            }
            this.visibleRow = row;
            return this;
        }

        @Override
        protected TreeModelListener createTreeModelListener() {
            return new JTree.TreeModelHandler(){

                @Override
                public void treeNodesRemoved(TreeModelEvent e) {
                    TreePath path;
                    if (TreeTable.this.tree.getSelectionCount() == 0 && (path = TreeView.findSiblingTreePath(e.getTreePath(), e.getChildIndices())) != null && path.getPathCount() > 0) {
                        TreeTable.this.tree.setSelectionPath(path);
                    }
                }
            };
        }

        @Override
        public void fireTreeCollapsed(TreePath path) {
            super.fireTreeCollapsed(path);
            this.firePropertyChange("width", -1, this.getWidth());
        }

        @Override
        public void fireTreeExpanded(TreePath path) {
            super.fireTreeExpanded(path);
            this.firePropertyChange("width", -1, this.getWidth());
        }

        boolean treeTableHasFocus() {
            Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            boolean tableHasFocus = focusOwner == this || focusOwner == TreeTable.this || TreeTable.this.isAncestorOf(focusOwner) || focusOwner instanceof JRootPane;
            return tableHasFocus;
        }

    }

    private static class TreeTableHeader
    extends JTableHeader {
        public TreeTableHeader(TableColumnModel columnModel) {
            super(columnModel);
        }

        @Override
        public Dimension getPreferredSize() {
            Dimension retValue = super.getPreferredSize();
            Component comp = this.getDefaultRenderer().getTableCellRendererComponent(this.getTable(), "X", false, false, -1, 0);
            int rendererHeight = comp.getPreferredSize().height;
            retValue.height = Math.max(retValue.height, rendererHeight);
            return retValue;
        }
    }

    private class GuardedActions
    implements Mutex.Action<Object> {
        private int type;
        private Object p1;
        final Object ret;

        public GuardedActions(int type, Object p1) {
            this.type = type;
            this.p1 = p1;
            this.ret = Children.MUTEX.readAccess((Mutex.Action)this);
        }

        public Object run() {
            switch (this.type) {
                case 0: {
                    TreeTable.this.paintImpl((Graphics)this.p1);
                    break;
                }
                case 1: {
                    TreeTable.this.validateTree();
                    break;
                }
                case 2: {
                    TreeTable.this.doLayout();
                    break;
                }
                case 3: {
                    TreeTable.this.repaintSelection((Boolean)this.p1);
                    break;
                }
                case 4: {
                    TreeTable.this.processEvent((AWTEvent)this.p1);
                    break;
                }
                case 5: {
                    return TreeTable.this.getPreferredSize();
                }
                case 6: 
                case 10: {
                    Object[] arr = (Object[])this.p1;
                    return TreeTable.this.processKeyBinding((KeyStroke)arr[0], (KeyEvent)arr[1], (Integer)arr[2], (Boolean)arr[3]);
                }
                default: {
                    throw new IllegalStateException("type: " + this.type);
                }
            }
            return null;
        }
    }

    private final class DefaultQuickSearchTableFilter
    implements QuickSearchTableFilter {
        private DefaultQuickSearchTableFilter() {
        }

        @Override
        public String getStringValueAt(int row, int col) {
            String str;
            Object value = TreeTable.this.getValueAt(row, col);
            if (value instanceof Node.Property) {
                Node.Property p = (Node.Property)value;
                Object v = null;
                try {
                    v = p.getValue();
                }
                catch (IllegalAccessException ex) {
                }
                catch (InvocationTargetException ex) {
                    // empty catch block
                }
                str = v instanceof String ? (String)v : null;
            } else {
                str = value instanceof VisualizerNode ? ((VisualizerNode)value).getDisplayName() : null;
            }
            return str;
        }
    }

}

