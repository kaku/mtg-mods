/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package org.openide.explorer.view;

import java.awt.Point;
import java.awt.dnd.DragGestureEvent;
import javax.swing.JComponent;
import javax.swing.JTable;
import org.openide.explorer.view.ExplorerDragSupport;
import org.openide.explorer.view.TableView;
import org.openide.nodes.Node;

class TableViewDragSupport
extends ExplorerDragSupport {
    protected TableView view;
    protected JTable table;

    public TableViewDragSupport(TableView view, JTable table) {
        this.comp = table;
        this.view = view;
        this.table = table;
    }

    @Override
    int getAllowedDropActions() {
        return this.view.getAllowedDropActions();
    }

    @Override
    public void dragGestureRecognized(DragGestureEvent dge) {
        super.dragGestureRecognized(dge);
    }

    @Override
    Node[] obtainNodes(DragGestureEvent dge) {
        Point dragOrigin = dge.getDragOrigin();
        int index = this.table.rowAtPoint(dge.getDragOrigin());
        Node n = this.view.getNodeFromRow(index);
        Node[] result = null;
        result = new Node[]{n};
        return result;
    }
}

