/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package org.openide.explorer.view;

import java.awt.Point;
import java.awt.dnd.DragGestureEvent;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ListModel;
import org.openide.explorer.view.ExplorerDragSupport;
import org.openide.explorer.view.ListView;
import org.openide.explorer.view.VisualizerNode;
import org.openide.nodes.Node;

class ListViewDragSupport
extends ExplorerDragSupport {
    protected ListView view;
    protected JList list;

    public ListViewDragSupport(ListView view, JList list) {
        this.comp = list;
        this.view = view;
        this.list = list;
    }

    @Override
    int getAllowedDropActions() {
        return this.view.getAllowedDropActions();
    }

    @Override
    protected int getAllowedDragActions() {
        return this.view.getAllowedDragActions();
    }

    @Override
    public void dragGestureRecognized(DragGestureEvent dge) {
        super.dragGestureRecognized(dge);
    }

    @Override
    Node[] obtainNodes(DragGestureEvent dge) {
        Point dragOrigin = dge.getDragOrigin();
        int index = this.list.locationToIndex(dragOrigin);
        if (index < 0) {
            return null;
        }
        Object obj = this.list.getModel().getElementAt(index);
        if (obj instanceof VisualizerNode) {
            obj = ((VisualizerNode)obj).node;
        }
        if (index < 0) {
            return null;
        }
        if (!(obj instanceof Node)) {
            return null;
        }
        Node[] result = null;
        if (this.list.isSelectedIndex(index)) {
            Object[] selected = this.list.getSelectedValues();
            result = new Node[selected.length];
            for (int i = 0; i < selected.length; ++i) {
                if (selected[i] instanceof VisualizerNode) {
                    result[i] = ((VisualizerNode)selected[i]).node;
                    continue;
                }
                if (!(selected[i] instanceof Node)) {
                    return null;
                }
                result[i] = (Node)selected[i];
            }
        } else {
            result = new Node[]{(Node)obj};
        }
        return result;
    }
}

