/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.Utilities
 */
package org.openide.explorer.view;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragGestureRecognizer;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.dnd.InvalidDnDOperationException;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.TooManyListenersException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import org.openide.explorer.view.DragDropUtilities;
import org.openide.explorer.view.ExplorerDnDManager;
import org.openide.explorer.view.NodeRenderer;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;

abstract class ExplorerDragSupport
implements DragSourceListener,
DragGestureListener {
    boolean active = false;
    DragGestureRecognizer defaultGesture;
    protected JComponent comp;
    ExplorerDnDManager exDnD = ExplorerDnDManager.getDefault();

    ExplorerDragSupport() {
    }

    abstract int getAllowedDropActions();

    @Override
    public void dragGestureRecognized(DragGestureEvent dge) {
        MouseEvent mev;
        InputEvent iev;
        if (Utilities.getOperatingSystem() == 4096 && (iev = dge.getTriggerEvent()) instanceof MouseEvent && (mev = (MouseEvent)iev).getButton() == 3) {
            return;
        }
        Node[] nodes = this.obtainNodes(dge);
        if (nodes == null || nodes.length == 0) {
            return;
        }
        int possibleNodeAction = this.getAllowedDragActions();
        for (int i = 0; i < nodes.length; ++i) {
            if ((possibleNodeAction & 2) != 0 && !nodes[i].canCut()) {
                possibleNodeAction = 1073741825;
            }
            if ((possibleNodeAction & 1) == 0 || nodes[i].canCopy()) continue;
            possibleNodeAction = 0;
        }
        this.exDnD = ExplorerDnDManager.getDefault();
        this.exDnD.setNodeAllowedActions(possibleNodeAction);
        int dragAction = dge.getDragAction();
        boolean dragStatus = this.canDrag(dragAction, possibleNodeAction);
        try {
            Transferable transferable;
            if ((possibleNodeAction & 2) != 0) {
                transferable = DragDropUtilities.getNodeTransferable(nodes, 2);
                this.exDnD.setDraggedTransferable(transferable, true);
                transferable = DragDropUtilities.getNodeTransferable(nodes, 1);
                this.exDnD.setDraggedTransferable(transferable, false);
            } else if ((possibleNodeAction & 1) != 0) {
                transferable = DragDropUtilities.getNodeTransferable(nodes, 1);
                this.exDnD.setDraggedTransferable(transferable, false);
            } else {
                transferable = Node.EMPTY.drag();
                this.exDnD.setDraggedTransferable(transferable, false);
            }
            this.exDnD.setDraggedNodes(nodes);
            this.exDnD.setDnDActive(true);
            dge.startDrag(null, transferable, this);
        }
        catch (InvalidDnDOperationException exc) {
            Logger.getLogger(ExplorerDragSupport.class.getName()).log(Level.INFO, exc.getMessage());
            Logger.getLogger(ExplorerDragSupport.class.getName()).log(Level.FINE, null, exc);
            this.exDnD.setDnDActive(false);
        }
        catch (IOException exc) {
            Exceptions.printStackTrace((Throwable)exc);
            this.exDnD.setDnDActive(false);
        }
    }

    protected int getAllowedDragActions() {
        return 0;
    }

    private boolean canDrag(int targetAction, int possibleAction) {
        return (possibleAction & targetAction) != 0;
    }

    @Override
    public void dragEnter(DragSourceDragEvent dsde) {
        this.doDragOver(dsde);
    }

    @Override
    public void dragOver(DragSourceDragEvent dsde) {
        this.doDragOver(dsde);
    }

    @Override
    public void dropActionChanged(DragSourceDragEvent dsde) {
    }

    @Override
    public void dragExit(DragSourceEvent dse) {
    }

    private void doDragOver(DragSourceDragEvent dsde) {
    }

    @Override
    public void dragDropEnd(DragSourceDropEvent dsde) {
        this.exDnD.setDraggedTransferable(null, true);
        this.exDnD.setDraggedTransferable(null, false);
        this.exDnD.setDraggedNodes(null);
        NodeRenderer.dragExit();
        this.exDnD.setDnDActive(false);
    }

    public void activate(boolean active) {
        if (this.active == active) {
            return;
        }
        this.active = active;
        DragGestureRecognizer dgr = this.getDefaultGestureRecognizer();
        if (dgr == null) {
            return;
        }
        if (active) {
            dgr.setSourceActions(this.getAllowedDragActions());
            try {
                dgr.removeDragGestureListener(this);
                dgr.addDragGestureListener(this);
            }
            catch (TooManyListenersException exc) {
                throw new IllegalStateException("Too many listeners for drag gesture.");
            }
        } else {
            dgr.removeDragGestureListener(this);
        }
    }

    DragGestureRecognizer getDefaultGestureRecognizer() {
        if (this.defaultGesture == null) {
            DragSource ds = DragSource.getDefaultDragSource();
            this.defaultGesture = ds.createDefaultDragGestureRecognizer(this.comp, this.getAllowedDragActions(), this);
        }
        return this.defaultGesture;
    }

    abstract Node[] obtainNodes(DragGestureEvent var1);
}

