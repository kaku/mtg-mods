/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.util.NbBundle
 */
package org.openide.explorer.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.EventObject;
import javax.accessibility.AccessibleContext;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.tree.DefaultTreeCellEditor;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreePath;
import org.openide.explorer.view.ViewUtil;
import org.openide.explorer.view.Visualizer;
import org.openide.explorer.view.VisualizerNode;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

class TreeViewCellEditor
extends DefaultTreeCellEditor
implements CellEditorListener,
FocusListener,
MouseMotionListener,
MouseListener {
    static final long serialVersionUID = -2171725285964032312L;
    boolean dndActive = false;
    private boolean cancelled = false;
    private boolean stopped = false;
    private boolean wasFocusOwner = true;

    public TreeViewCellEditor(JTree tree) {
        super(tree, new DefaultTreeCellRenderer());
        if (tree.getSelectionCount() == 1) {
            this.lastPath = tree.getSelectionPath();
        }
        this.addCellEditorListener(this);
    }

    @Override
    public void editingStopped(ChangeEvent e) {
        Node n;
        if (this.stopped) {
            return;
        }
        this.stopped = true;
        TreePath lastP = this.lastPath;
        if (lastP != null && (n = Visualizer.findNode(lastP.getLastPathComponent())) != null && n.canRename()) {
            String newStr = (String)this.getCellEditorValue();
            ViewUtil.nodeRename(n, newStr);
        }
    }

    @Override
    public void editingCanceled(ChangeEvent e) {
        this.cancelled = true;
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        if (evt.getSource() instanceof JTextField) {
            this.cancelled = true;
            this.cancelCellEditing();
        } else {
            super.actionPerformed(evt);
        }
    }

    @Override
    public void focusLost(FocusEvent evt) {
        if (evt.getSource() == this.tree) {
            this.abortTimer();
            return;
        }
        if (this.stopped || this.cancelled) {
            return;
        }
        if (!this.stopCellEditing()) {
            this.cancelCellEditing();
        }
    }

    @Override
    public void focusGained(FocusEvent evt) {
    }

    @Override
    protected TreeCellEditor createTreeCellEditor() {
        JTextField tf = new JTextField(){

            @Override
            public void addNotify() {
                TreeViewCellEditor.this.stopped = (TreeViewCellEditor.this.cancelled = false);
                super.addNotify();
                this.requestFocus();
            }
        };
        String laf = UIManager.getLookAndFeel().getID();
        if ("GTK".equals(laf)) {
            tf.setBorder(BorderFactory.createEmptyBorder());
        } else if ("Nimbus".equals(laf)) {
            tf.setBorder(BorderFactory.createLineBorder(new JTree().getBackground()));
        }
        tf.registerKeyboardAction(this, KeyStroke.getKeyStroke(27, 0, true), 0);
        tf.addFocusListener(this);
        Ed ed = new Ed(tf);
        ed.setClickCountToStart(1);
        ed.getComponent().getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(TreeViewCellEditor.class, (String)"ACSD_TreeViewCellEditor"));
        ed.getComponent().getAccessibleContext().setAccessibleName(NbBundle.getMessage(TreeViewCellEditor.class, (String)"ACSN_TreeViewCellEditor"));
        return ed;
    }

    @Override
    public boolean isCellEditable(EventObject event) {
        if (event != null && event instanceof MouseEvent) {
            if (!SwingUtilities.isLeftMouseButton((MouseEvent)event) || ((MouseEvent)event).isPopupTrigger()) {
                this.abortTimer();
                return false;
            }
            if (!this.wasFocusOwner) {
                this.wasFocusOwner = true;
                return false;
            }
        }
        if (this.lastPath != null) {
            Node n = Visualizer.findNode(this.lastPath.getLastPathComponent());
            if (n == null || !n.canRename()) {
                return false;
            }
        } else {
            return false;
        }
        if (this.dndActive) {
            return false;
        }
        return super.isCellEditable(event);
    }

    @Override
    protected void determineOffset(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row) {
        if (this.renderer != null) {
            this.renderer.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, true);
            this.editingIcon = this.renderer.getIcon();
            this.offset = this.editingIcon != null ? this.renderer.getIconTextGap() + this.editingIcon.getIconWidth() : 0;
        } else {
            this.editingIcon = null;
            this.offset = 0;
        }
    }

    void setDnDActive(boolean dndActive) {
        if (!dndActive) {
            this.tree.removeMouseMotionListener(this);
        }
        this.dndActive = dndActive;
    }

    @Override
    protected void setTree(JTree newTree) {
        if (newTree != this.tree && this.timer != null && this.timer.isRunning()) {
            this.tree.removeMouseMotionListener(this);
        }
        if (newTree != this.tree) {
            if (this.tree != null) {
                this.tree.removeMouseListener(this);
                this.tree.removeFocusListener(this);
            }
            if (newTree != null) {
                newTree.addMouseListener(this);
                newTree.addFocusListener(this);
            }
        }
        super.setTree(newTree);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        Point p = e.getPoint();
        boolean b = this.checkContinueTimer(p);
        if (!b) {
            this.abortTimer();
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        Point p = e.getPoint();
        boolean b = this.checkContinueTimer(p);
        if (!b) {
            this.abortTimer();
        }
    }

    void abortTimer() {
        if (this.timer != null && this.timer.isRunning()) {
            this.timer.stop();
            this.tree.removeMouseMotionListener(this);
        }
    }

    @Override
    protected void startEditingTimer() {
        this.tree.addMouseMotionListener(this);
        super.startEditingTimer();
    }

    @Override
    protected void prepareForEditing() {
        this.abortTimer();
        this.tree.removeMouseMotionListener(this);
        super.prepareForEditing();
    }

    private boolean checkContinueTimer(Point p) {
        Rectangle r = this.tree.getPathBounds(this.tree.getSelectionPath());
        if (r == null) {
            return false;
        }
        return r.contains(p);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        this.wasFocusOwner = this.tree.isFocusOwner();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    class Ed
    extends DefaultCellEditor {
        static final long serialVersionUID = -6373058702842751408L;

        public Ed(JTextField tf) {
            super(tf);
        }

        @Override
        public Component getTreeCellEditorComponent(JTree tree, Object value, boolean isSelected, boolean expanded, boolean leaf, int row) {
            Node ren = Visualizer.findNode(value);
            if (ren != null && ren.canRename()) {
                this.delegate.setValue(ren.getName());
            } else {
                this.delegate.setValue("");
            }
            TreeViewCellEditor.this.editingIcon = ((VisualizerNode)value).getIcon(expanded, false);
            ((JTextField)this.editorComponent).selectAll();
            return this.editorComponent;
        }
    }

}

