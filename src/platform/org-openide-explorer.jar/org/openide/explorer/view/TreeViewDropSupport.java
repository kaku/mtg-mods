/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Children
 *  org.openide.nodes.Index
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.datatransfer.PasteType
 */
package org.openide.explorer.view;

import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.EventListener;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JRootPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreePath;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.DragDropUtilities;
import org.openide.explorer.view.DropGlassPane;
import org.openide.explorer.view.ExplorerDnDManager;
import org.openide.explorer.view.NodeRenderer;
import org.openide.explorer.view.TreeView;
import org.openide.explorer.view.TreeViewCellEditor;
import org.openide.nodes.Children;
import org.openide.nodes.Index;
import org.openide.nodes.Node;
import org.openide.util.datatransfer.PasteType;

final class TreeViewDropSupport
implements DropTargetListener,
Runnable {
    protected static final int FUSSY_POINTING = 3;
    private static final int DELAY_TIME_FOR_EXPAND = 1000;
    private static final int SHIFT_DOWN = -1;
    private static final int SHIFT_RIGHT = 10;
    private static final int SHIFT_LEFT = 15;
    boolean active = false;
    boolean dropTargetPopupAllowed;
    DropTarget dropTarget;
    Rectangle lastNodeArea;
    private int upperNodeIdx = -1;
    private int lowerNodeIdx = -1;
    private int dropIndex = -1;
    Timer timer;
    DropGlassPane dropPane;
    private int pointAt = 0;
    protected TreeView view;
    protected JTree tree;

    public TreeViewDropSupport(TreeView view, JTree tree, boolean dropTargetPopupAllowed) {
        this.view = view;
        this.tree = tree;
        this.dropTargetPopupAllowed = dropTargetPopupAllowed;
    }

    public void setDropTargetPopupAllowed(boolean value) {
        this.dropTargetPopupAllowed = value;
    }

    public boolean isDropTargetPopupAllowed() {
        return this.dropTargetPopupAllowed;
    }

    @Override
    public void dragEnter(DropTargetDragEvent dtde) {
        this.checkStoredGlassPane();
        this.dropIndex = -1;
        this.doDragOver(dtde);
    }

    @Override
    public void dragOver(DropTargetDragEvent dtde) {
        this.checkStoredGlassPane();
        this.doDragOver(dtde);
    }

    private void checkStoredGlassPane() {
        if (!DropGlassPane.isOriginalPaneStored()) {
            Component comp = this.tree.getRootPane().getGlassPane();
            DropGlassPane.setOriginalPane(this.tree, comp, comp.isVisible());
            this.dropPane = DropGlassPane.getDefault(this.tree);
            this.tree.getRootPane().setGlassPane(this.dropPane);
            this.dropPane.revalidate();
            this.dropPane.setVisible(true);
        }
    }

    private void doDragOver(DropTargetDragEvent dtde) {
        Line2D.Double line;
        ExplorerDnDManager.getDefault().setMaybeExternalDragAndDrop(true);
        int dropAction = dtde.getDropAction();
        int allowedDropActions = this.view.getAllowedDropActions();
        dropAction = ExplorerDnDManager.getDefault().getAdjustedDropAction(dropAction, allowedDropActions);
        TreePath tp = this.getTreePath(dtde, dropAction);
        Point p = dtde.getLocation();
        if (tp == null) {
            Node dropNode = this.view.manager.getRootContext();
            if (this.canDrop(dropNode, dropAction, dtde.getTransferable())) {
                dtde.acceptDrag(dropAction);
            } else {
                dtde.rejectDrag();
            }
            return;
        }
        Node dropNode = this.getNodeForDrop(p);
        if (dropNode == null) {
            this.dropIndex = -1;
            dtde.rejectDrag();
            this.removeDropLine();
            return;
        }
        Rectangle nodeArea = this.tree.getPathBounds(tp);
        int endPointX = nodeArea.x + nodeArea.width;
        int row = this.tree.getRowForPath(tp);
        if (nodeArea != null) {
            this.pointAt = 0;
            if (p.y <= nodeArea.y + 3) {
                if (row != 0) {
                    this.pointAt = -1;
                    TreePath upPath = this.tree.getPathForRow(row - 1);
                    if (upPath != null && !upPath.equals(tp)) {
                        endPointX = Math.max(nodeArea.x + nodeArea.width, this.tree.getPathBounds((TreePath)upPath).x + this.tree.getPathBounds((TreePath)upPath).width);
                    }
                    if (dropNode.getParentNode() != null) {
                        dropNode = dropNode.getParentNode();
                        tp = null;
                    }
                }
            } else if (p.y >= nodeArea.y + nodeArea.height - 3 && !this.view.isExpanded(dropNode)) {
                this.pointAt = 1;
                TreePath downPath = this.tree.getPathForRow(row + 1);
                if (downPath != null && !downPath.equals(tp)) {
                    endPointX = Math.max(nodeArea.x + nodeArea.width, this.tree.getPathBounds((TreePath)downPath).x + this.tree.getPathBounds((TreePath)downPath).width);
                }
                if (dropNode.getParentNode() != null) {
                    dropNode = dropNode.getParentNode();
                    tp = null;
                }
            }
        }
        endPointX += 10;
        Index indexCookie = (Index)dropNode.getCookie(Index.class);
        if (indexCookie != null) {
            if (this.pointAt == -1) {
                this.lowerNodeIdx = indexCookie.indexOf(this.getNodeForDrop(p));
                this.upperNodeIdx = this.lowerNodeIdx - 1;
            } else if (this.pointAt == 1) {
                this.upperNodeIdx = indexCookie.indexOf(this.getNodeForDrop(p));
                this.lowerNodeIdx = this.upperNodeIdx + 1;
            }
            this.dropIndex = this.lowerNodeIdx;
        }
        if (dropNode == this.getNodeForDrop(p)) {
            this.dropIndex = -1;
        }
        if (!(this.timer != null && this.timer.isRunning() || dropNode == null || dropNode.isLeaf() || this.view.isExpanded(dropNode))) {
            final Node cn = dropNode;
            this.removeTimer();
            this.timer = new Timer(1000, new ActionListener(){

                @Override
                public final void actionPerformed(ActionEvent e) {
                    TreeViewDropSupport.this.view.expandNode(cn);
                }
            });
            this.timer.setRepeats(false);
            this.timer.start();
        }
        if (this.pointAt == 0) {
            if (null != this.dropPane) {
                this.dropPane.setDropLine(null);
            }
        } else if (this.pointAt == -1) {
            line = new Line2D.Double(nodeArea.x - 15, nodeArea.y + -1, endPointX, nodeArea.y + -1);
            this.convertBoundsAndSetDropLine(line);
            Rectangle lineArea = new Rectangle(nodeArea.x - 15, nodeArea.y + -1 - 3, endPointX - nodeArea.x + 15, 5);
            nodeArea = (Rectangle)nodeArea.createUnion(lineArea);
        } else {
            line = new Line2D.Double(nodeArea.x - 15, nodeArea.y + nodeArea.height + -1, endPointX, nodeArea.y + nodeArea.height + -1);
            this.convertBoundsAndSetDropLine(line);
            Rectangle lineArea = new Rectangle(nodeArea.x - 15, nodeArea.y + nodeArea.height, endPointX - nodeArea.x + 15, 2);
            nodeArea = (Rectangle)nodeArea.createUnion(lineArea);
        }
        if (this.lastNodeArea != null && !this.lastNodeArea.equals(nodeArea)) {
            NodeRenderer.dragExit();
            this.repaint(this.lastNodeArea);
        }
        if (!nodeArea.equals(this.lastNodeArea)) {
            if (tp != null) {
                NodeRenderer.dragEnter(tp.getLastPathComponent());
            }
            this.repaint(nodeArea);
            this.lastNodeArea = nodeArea;
            this.removeTimer();
        }
        if (this.canDrop(dropNode, dropAction, dtde.getTransferable())) {
            dtde.acceptDrag(dropAction);
        } else {
            Node[] draggedNodes = ExplorerDnDManager.getDefault().getDraggedNodes();
            if (null != draggedNodes && this.canReorderWhenMoving(dropNode, draggedNodes)) {
                dtde.acceptDrag(dropAction);
            } else {
                dtde.rejectDrag();
            }
        }
    }

    private void repaint(Rectangle r) {
        this.tree.repaint(r.x - 5, r.y - 5, r.width + 10, r.height + 10);
    }

    private void convertBoundsAndSetDropLine(Line2D line) {
        int x1 = (int)line.getX1();
        int x2 = (int)line.getX2();
        int y1 = (int)line.getY1();
        int y2 = (int)line.getY2();
        Point p1 = SwingUtilities.convertPoint(this.tree, x1, y1, this.tree.getRootPane());
        Point p2 = SwingUtilities.convertPoint(this.tree, x2, y2, this.tree.getRootPane());
        line.setLine(p1, p2);
        if (null != this.dropPane) {
            this.dropPane.setDropLine(line);
        }
    }

    private void removeTimer() {
        if (this.timer != null) {
            ActionListener[] l = (ActionListener[])this.timer.getListeners(ActionListener.class);
            for (int i = 0; i < l.length; ++i) {
                this.timer.removeActionListener(l[i]);
            }
            this.timer.stop();
            this.timer = null;
        }
    }

    @Override
    public void dropActionChanged(DropTargetDragEvent dtde) {
        Node[] nodes = ExplorerDnDManager.getDefault().getDraggedNodes();
        if (null != nodes) {
            int dropAction = ExplorerDnDManager.getDefault().getAdjustedDropAction(dtde.getDropAction(), this.view.getAllowedDropActions());
            for (int i = 0; i < nodes.length; ++i) {
                if ((this.view.getAllowedDropActions() & dropAction) != 0 && DragDropUtilities.checkNodeForAction(nodes[i], dropAction)) continue;
                dtde.rejectDrag();
                return;
            }
        }
    }

    @Override
    public void dragExit(DropTargetEvent dte) {
        this.dropIndex = -1;
        ExplorerDnDManager.getDefault().setMaybeExternalDragAndDrop(false);
        this.stopDragging();
    }

    private void removeDropLine() {
        if (null != this.dropPane) {
            this.dropPane.setDropLine(null);
        }
        if (this.lastNodeArea != null) {
            NodeRenderer.dragExit();
            this.repaint(this.lastNodeArea);
            this.lastNodeArea = null;
        }
    }

    private void stopDragging() {
        this.removeDropLine();
        this.removeTimer();
        if (DropGlassPane.isOriginalPaneStored()) {
            DropGlassPane.putBackOriginal();
        }
    }

    private Node getNodeForDrop(Point p) {
        if (p != null) {
            TreePath tp = this.tree.getPathForLocation(p.x, p.y);
            if (null == tp) {
                tp = this.tree.getPathForLocation(p.x, p.y - this.tree.getRowHeight() / 2);
            }
            if (tp != null) {
                return DragDropUtilities.secureFindNode(tp.getLastPathComponent());
            }
        }
        return null;
    }

    private boolean canReorderWhenMoving(Node folder, Node[] dragNodes) {
        if ((ExplorerDnDManager.getDefault().getNodeAllowedActions() & 2) == 0) {
            return false;
        }
        return this.canReorder(folder, dragNodes);
    }

    private boolean canReorder(Node folder, Node[] dragNodes) {
        if (folder == null || dragNodes.length == 0) {
            return false;
        }
        Index ic = (Index)folder.getCookie(Index.class);
        if (ic == null) {
            return false;
        }
        for (int i = 0; i < dragNodes.length; ++i) {
            if (dragNodes[i] == null) {
                return false;
            }
            if (dragNodes[i].getParentNode() == null) {
                return false;
            }
            if (dragNodes[i].getParentNode().equals((Object)folder)) continue;
            return false;
        }
        return true;
    }

    private void performReorder(Node folder, Node[] dragNodes, int lNode, int uNode) {
        block11 : {
            try {
                int i;
                Index indexCookie = (Index)folder.getCookie(Index.class);
                if (indexCookie == null) break block11;
                int[] perm = new int[indexCookie.getNodesCount()];
                int[] indexes = new int[dragNodes.length];
                int indexesLength = 0;
                for (int i2 = 0; i2 < dragNodes.length; ++i2) {
                    int idx = indexCookie.indexOf(dragNodes[i2]);
                    if (idx < 0 || idx >= perm.length) continue;
                    indexes[indexesLength++] = idx;
                }
                Arrays.sort(indexes);
                if (lNode < 0 || uNode >= perm.length || indexesLength == 0) {
                    return;
                }
                int k = 0;
                for (i = 0; i < perm.length; ++i) {
                    int j;
                    if (i <= uNode) {
                        if (!this.containsNumber(indexes, indexesLength, i)) {
                            perm[i] = k++;
                        }
                        if (i != uNode) continue;
                        for (j = 0; j < indexesLength; ++j) {
                            if (indexes[j] > uNode) continue;
                            perm[indexes[j]] = k++;
                        }
                        continue;
                    }
                    if (i == lNode) {
                        for (j = 0; j < indexesLength; ++j) {
                            if (indexes[j] < lNode) continue;
                            perm[indexes[j]] = k++;
                        }
                    }
                    if (this.containsNumber(indexes, indexesLength, i)) continue;
                    perm[i] = k++;
                }
                for (i = 0; i < perm.length; ++i) {
                    if (perm[i] == i) continue;
                    indexCookie.reorder(perm);
                    break;
                }
            }
            catch (Exception e) {
                Logger.getLogger(TreeViewDropSupport.class.getName()).log(Level.WARNING, null, e);
            }
        }
    }

    private boolean containsNumber(int[] arr, int arrLength, int n) {
        for (int i = 0; i < arrLength; ++i) {
            if (arr[i] != n) continue;
            return true;
        }
        return false;
    }

    private Node[] findDropedNodes(Node folder, Node[] dragNodes) {
        if (folder == null || dragNodes.length == 0) {
            return null;
        }
        Node[] dropNodes = new Node[dragNodes.length];
        Children children = folder.getChildren();
        for (int i = 0; i < dragNodes.length; ++i) {
            dropNodes[i] = children.findChild(dragNodes[i].getName());
        }
        return dropNodes;
    }

    private boolean canDrop(Node n, int dropAction, Transferable dndEventTransferable) {
        Node[] nodes;
        if (n == null) {
            return false;
        }
        if ((this.view.getAllowedDropActions() & dropAction) == 0) {
            return false;
        }
        if ((2 & dropAction) != 0 && (nodes = ExplorerDnDManager.getDefault().getDraggedNodes()) != null) {
            for (int i = 0; i < nodes.length; ++i) {
                if (!n.equals((Object)nodes[i].getParentNode())) continue;
                return false;
            }
        }
        Transferable trans = ExplorerDnDManager.getDefault().getDraggedTransferable((2 & dropAction) != 0);
        if (trans == null && null == (trans = dndEventTransferable)) {
            return false;
        }
        PasteType pt = DragDropUtilities.getDropType(n, trans, dropAction, this.dropIndex);
        return pt != null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void drop(DropTargetDropEvent dtde) {
        boolean dropResult = true;
        try {
            this.stopDragging();
            Node dropNode = this.getNodeForDrop(dtde.getLocation());
            if (dropNode == null) {
                dropNode = this.view.manager.getRootContext();
            } else if (this.pointAt != 0) {
                dropNode = dropNode.getParentNode();
            }
            Node[] dragNodes = ExplorerDnDManager.getDefault().getDraggedNodes();
            int dropAction = ExplorerDnDManager.getDefault().getAdjustedDropAction(dtde.getDropAction(), this.view.getAllowedDropActions());
            ExplorerDnDManager.getDefault().setMaybeExternalDragAndDrop(false);
            if (dropAction != 0) {
                dtde.acceptDrop(dropAction);
            }
            if (!this.canDrop(dropNode, dropAction, dtde.getTransferable())) {
                if (null != dragNodes && this.canReorderWhenMoving(dropNode, dragNodes)) {
                    this.performReorder(dropNode, dragNodes, this.lowerNodeIdx, this.upperNodeIdx);
                } else {
                    dropResult = false;
                }
                return;
            }
            if (1073741824 == dropAction && null != dragNodes) {
                int i;
                PasteType[] ptCut = new PasteType[]{};
                PasteType[] ptCopy = new PasteType[]{};
                if ((ExplorerDnDManager.getDefault().getNodeAllowedActions() & 2) != 0) {
                    ptCut = DragDropUtilities.getPasteTypes(dropNode, ExplorerDnDManager.getDefault().getDraggedTransferable(true));
                }
                if ((ExplorerDnDManager.getDefault().getNodeAllowedActions() & 1) != 0) {
                    ptCopy = DragDropUtilities.getPasteTypes(dropNode, ExplorerDnDManager.getDefault().getDraggedTransferable(false));
                }
                TreeSet<PasteType> setPasteTypes = new TreeSet<PasteType>(new Comparator<PasteType>(){

                    @Override
                    public int compare(PasteType obj1, PasteType obj2) {
                        return obj1.getName().compareTo(obj2.getName());
                    }
                });
                for (i = 0; i < ptCut.length; ++i) {
                    setPasteTypes.add(ptCut[i]);
                }
                for (i = 0; i < ptCopy.length; ++i) {
                    setPasteTypes.add(ptCopy[i]);
                }
                DragDropUtilities.createDropFinishPopup(setPasteTypes).show(this.tree, Math.max(dtde.getLocation().x - 5, 0), Math.max(dtde.getLocation().y - 5, 0));
                if (this.canReorder(dropNode, dragNodes)) {
                    final Node tempDropNode = dropNode;
                    final int tmpUpper = this.upperNodeIdx;
                    final int tmpLower = this.lowerNodeIdx;
                    final Node[] tempDragNodes = dragNodes;
                    DragDropUtilities.setPostDropRun(new Runnable(){

                        @Override
                        public void run() {
                            TreeViewDropSupport.this.performReorder(tempDropNode, TreeViewDropSupport.this.findDropedNodes(tempDropNode, tempDragNodes), tmpLower, tmpUpper);
                        }
                    });
                }
            } else if (dropAction != 1073741824) {
                Transferable t = ExplorerDnDManager.getDefault().getDraggedTransferable((2 & dropAction) != 0);
                if (null == t) {
                    t = dtde.getTransferable();
                }
                PasteType pt = DragDropUtilities.getDropType(dropNode, t, dropAction, this.dropIndex);
                final Node[] preNodes = dropNode.getChildren().getNodes(true);
                final Node parentNode = dropNode;
                Node[] diffNodes = DragDropUtilities.performPaste(pt, dropNode);
                if (null != ExplorerDnDManager.getDefault().getDraggedTransferable((2 & dropAction) != 0)) {
                    ExplorerDnDManager.getDefault().setDraggedNodes(diffNodes);
                }
                if (this.dropIndex != -1) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            Node[] diffNodes = TreeViewDropSupport.this.getDiffNodes(parentNode, preNodes);
                            if (TreeViewDropSupport.this.canReorder(parentNode, diffNodes)) {
                                TreeViewDropSupport.this.performReorder(parentNode, diffNodes, TreeViewDropSupport.this.lowerNodeIdx, TreeViewDropSupport.this.upperNodeIdx);
                            }
                        }
                    });
                }
            }
            TreeCellEditor tce = this.tree.getCellEditor();
            if (tce instanceof TreeViewCellEditor) {
                ((TreeViewCellEditor)tce).setDnDActive(false);
            }
        }
        finally {
            dtde.dropComplete(dropResult);
        }
    }

    private Node[] getDiffNodes(Node parent, Node[] childrenBefore) {
        Node[] childrenCurrent = parent.getChildren().getNodes(true);
        List<Node> pre = Arrays.asList(childrenBefore);
        List<Node> post = Arrays.asList(childrenCurrent);
        Iterator<Node> it = post.iterator();
        ArrayList<Node> diff = new ArrayList<Node>();
        while (it.hasNext()) {
            Node n = it.next();
            if (pre.contains((Object)n)) continue;
            diff.add(n);
        }
        return diff.toArray((T[])new Node[diff.size()]);
    }

    public void activate(boolean active) {
        if (this.active == active) {
            return;
        }
        this.active = active;
        this.getDropTarget().setActive(active);
    }

    @Override
    public void run() {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(this);
            return;
        }
        DragDropUtilities.dropNotSuccesfull();
    }

    TreePath getTreePath(DropTargetDragEvent dtde, int dropAction) {
        Point location = dtde.getLocation();
        TreePath tp = this.tree.getPathForLocation(location.x, location.y);
        if (null == tp) {
            tp = this.tree.getPathForLocation(location.x, location.y - this.tree.getRowHeight() / 2);
        }
        return tp != null && DragDropUtilities.secureFindNode(tp.getLastPathComponent()) != null ? tp : null;
    }

    DropTarget getDropTarget() {
        if (this.dropTarget == null) {
            this.dropTarget = new DropTarget(this.tree, this.view.getAllowedDropActions(), this, false);
        }
        return this.dropTarget;
    }

}

