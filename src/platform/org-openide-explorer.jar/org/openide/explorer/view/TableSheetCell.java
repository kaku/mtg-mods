/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.NbBundle
 */
package org.openide.explorer.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.KeyboardFocusManager;
import java.awt.event.ComponentListener;
import java.awt.event.HierarchyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.util.EventObject;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import org.netbeans.modules.openide.explorer.TTVEnvBridge;
import org.openide.explorer.propertysheet.PropertyPanel;
import org.openide.explorer.view.NodeTableModel;
import org.openide.explorer.view.SheetCell;
import org.openide.explorer.view.TreeTable;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

class TableSheetCell
extends AbstractCellEditor
implements TableModelListener,
PropertyChangeListener,
TableCellEditor,
TableCellRenderer {
    private NodeTableModel tableModel;
    private Boolean flat;
    private Node node;
    private Node.Property prop;
    private TableCellRenderer headerRenderer = new JTable().getTableHeader().getDefaultRenderer();
    private NullPanel nullPanel;
    private Map panelCache = new WeakHashMap();
    private SheetCell.FocusedPropertyPanel renderer = null;
    private PropertyPanel editor = null;

    public TableSheetCell(NodeTableModel tableModel) {
        this.tableModel = tableModel;
        this.setFlat(false);
    }

    public void setFlat(boolean f) {
        Color controlDkShadow = Color.lightGray;
        if (UIManager.getColor("controlDkShadow") != null) {
            controlDkShadow = UIManager.getColor("controlDkShadow");
        }
        Color controlLtHighlight = Color.black;
        if (UIManager.getColor("controlLtHighlight") != null) {
            controlLtHighlight = UIManager.getColor("controlLtHighlight");
        }
        Color buttonFocusColor = Color.blue;
        if (UIManager.getColor("Button.focus") != null) {
            buttonFocusColor = UIManager.getColor("Button.focus");
        }
        this.flat = f ? Boolean.TRUE : Boolean.FALSE;
    }

    @Override
    public Object getCellEditorValue() {
        return null;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int r, int c) {
        this.prop = (Node.Property)value;
        this.node = this.tableModel.nodeForRow(r);
        this.node.addPropertyChangeListener((PropertyChangeListener)this);
        this.tableModel.addTableModelListener(this);
        PropertyPanel propPanel = this.getEditor(this.prop, this.node);
        propPanel.setBackground(table.getSelectionBackground());
        propPanel.setForeground(table.getSelectionForeground());
        propPanel.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, table.getSelectionBackground()));
        return propPanel;
    }

    @Override
    public boolean shouldSelectCell(EventObject ev) {
        return true;
    }

    @Override
    public boolean isCellEditable(EventObject e) {
        return true;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        this.tableModel.fireTableDataChanged();
    }

    @Override
    public boolean stopCellEditing() {
        if (this.prop != null) {
            this.detachEditor();
        }
        return super.stopCellEditing();
    }

    @Override
    public void cancelCellEditing() {
        if (this.prop != null) {
            this.detachEditor();
        }
        super.cancelCellEditing();
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        this.cancelCellEditing();
    }

    private void detachEditor() {
        this.node.removePropertyChangeListener((PropertyChangeListener)this);
        this.tableModel.removeTableModelListener(this);
        this.node = null;
        this.prop = null;
    }

    private SheetCell.FocusedPropertyPanel getRenderer(Node.Property p, Node n) {
        TTVEnvBridge bridge = TTVEnvBridge.getInstance(this);
        bridge.setCurrentBeans((Object[])new Node[]{n});
        if (this.renderer == null) {
            this.renderer = new SheetCell.FocusedPropertyPanel(p, 9);
            this.renderer.putClientProperty("beanBridgeIdentifier", this);
        }
        this.renderer.setProperty(p);
        this.renderer.putClientProperty("flat", Boolean.TRUE);
        return this.renderer;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (row == -1) {
            Component comp = this.headerRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            if (comp instanceof JComponent) {
                String tip = column > 0 ? this.tableModel.propertyForColumn(column).getShortDescription() : table.getColumnName(0);
                ((JComponent)comp).setToolTipText(tip);
            }
            return comp;
        }
        Node.Property property = (Node.Property)value;
        Node n = this.tableModel.nodeForRow(row);
        if (property != null) {
            SheetCell.FocusedPropertyPanel propPanel = this.getRenderer(property, n);
            propPanel.setFocused(hasFocus);
            String tooltipText = null;
            try {
                Object tooltipValue = property.getValue();
                if (null != tooltipValue) {
                    tooltipText = tooltipValue.toString();
                }
            }
            catch (IllegalAccessException eaE) {
                Logger.getLogger(TableSheetCell.class.getName()).log(Level.WARNING, null, eaE);
            }
            catch (InvocationTargetException itE) {
                Logger.getLogger(TableSheetCell.class.getName()).log(Level.WARNING, null, itE);
            }
            propPanel.setToolTipText(TableSheetCell.createHtmlTooltip(tooltipText, propPanel.getFont()));
            propPanel.setOpaque(true);
            if (isSelected) {
                boolean tableHasFocus;
                Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
                boolean bl = tableHasFocus = table == focusOwner || table.isAncestorOf(focusOwner) || focusOwner instanceof Container && ((Container)focusOwner).isAncestorOf(table);
                if (table == focusOwner && table.isEditing()) {
                    tableHasFocus = true;
                }
                propPanel.setBackground(tableHasFocus ? table.getSelectionBackground() : TreeTable.getUnfocusedSelectedBackground());
                propPanel.setForeground(tableHasFocus ? table.getSelectionForeground() : TreeTable.getUnfocusedSelectedForeground());
            } else {
                propPanel.setBackground(table.getBackground());
                propPanel.setForeground(table.getForeground());
            }
            return propPanel;
        }
        if (this.nullPanel == null) {
            this.nullPanel = new NullPanel(n);
            this.nullPanel.setOpaque(true);
        } else {
            this.nullPanel.setNode(n);
        }
        if (isSelected) {
            Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            boolean tableHasFocus = hasFocus || table == focusOwner || table.isAncestorOf(focusOwner) || focusOwner instanceof Container && ((Container)focusOwner).isAncestorOf(table);
            this.nullPanel.setBackground(tableHasFocus ? table.getSelectionBackground() : TreeTable.getUnfocusedSelectedBackground());
            this.nullPanel.setForeground(table.getSelectionForeground().darker());
        } else {
            this.nullPanel.setBackground(table.getBackground());
            this.nullPanel.setForeground(table.getForeground());
        }
        this.nullPanel.setFocused(hasFocus);
        return this.nullPanel;
    }

    private PropertyPanel getEditor(Node.Property p, Node n) {
        int prefs = 8;
        TTVEnvBridge bridge = TTVEnvBridge.getInstance(this);
        bridge.setCurrentBeans((Object[])new Node[]{n});
        if (this.editor == null) {
            this.editor = new PropertyPanel(p, prefs);
            this.editor.putClientProperty("flat", Boolean.TRUE);
            this.editor.putClientProperty("beanBridgeIdentifier", this);
            this.editor.setProperty(p);
            return this.editor;
        }
        this.editor.setProperty(p);
        return this.editor;
    }

    void updateUI() {
        this.headerRenderer = new JTable().getTableHeader().getDefaultRenderer();
    }

    private static String getString(String key) {
        return NbBundle.getMessage(TableSheetCell.class, (String)key);
    }

    static String createHtmlTooltip(String value, Font font) {
        if (value == null) {
            return "null";
        }
        String token = null;
        if (value.indexOf(" ") != -1) {
            token = " ";
        } else if (value.indexOf(",") != -1) {
            token = ",";
        } else if (value.indexOf(";") != -1) {
            token = ";";
        } else if (value.indexOf("/") != -1) {
            token = "/";
        } else if (value.indexOf(">") != -1) {
            token = ">";
        } else if (value.indexOf("\\") != -1) {
            token = "\\";
        } else {
            return TableSheetCell.makeDisplayble(value, font);
        }
        StringTokenizer tk = new StringTokenizer(value, token, true);
        StringBuffer sb = new StringBuffer(value.length() + 20);
        sb.append("<html>");
        int charCount = 0;
        int lineCount = 0;
        while (tk.hasMoreTokens()) {
            String a = tk.nextToken();
            a = TableSheetCell.makeDisplayble(a, font);
            charCount += a.length();
            sb.append(a);
            if (tk.hasMoreTokens()) {
                ++charCount;
            }
            if (charCount <= 80) continue;
            sb.append("<br>");
            charCount = 0;
            if (++lineCount <= 10) continue;
            sb.append(NbBundle.getMessage(TableSheetCell.class, (String)"MSG_ELLIPSIS"));
            return sb.toString();
        }
        sb.append("</html>");
        return sb.toString();
    }

    private static String makeDisplayble(String str, Font f) {
        if (null == str) {
            return str;
        }
        if (null == f) {
            f = new JLabel().getFont();
        }
        StringBuffer buf = new StringBuffer((int)((double)str.length() * 1.3));
        char[] chars = str.toCharArray();
        block7 : for (int i = 0; i < chars.length; ++i) {
            char c = chars[i];
            switch (c) {
                case '\t': {
                    buf.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                    continue block7;
                }
                case '\n': {
                    continue block7;
                }
                case '\r': {
                    continue block7;
                }
                case '\b': {
                    buf.append("\\b");
                    continue block7;
                }
                case '\f': {
                    buf.append("\\f");
                    continue block7;
                }
                default: {
                    if (TableSheetCell.processHtmlEntity(buf, c)) continue block7;
                    if (null == f || f.canDisplay(c)) {
                        buf.append(c);
                        continue block7;
                    }
                    buf.append("\\u");
                    String hex = Integer.toHexString(c);
                    for (int j = 0; j < 4 - hex.length(); ++j) {
                        buf.append('0');
                    }
                    buf.append(hex);
                }
            }
        }
        return buf.toString();
    }

    private static boolean processHtmlEntity(StringBuffer buf, char c) {
        switch (c) {
            case '>': {
                buf.append("&gt;");
                break;
            }
            case '<': {
                buf.append("&lt;");
                break;
            }
            case '&': {
                buf.append("&amp;");
                break;
            }
            default: {
                return false;
            }
        }
        return true;
    }

    private static class NullPanel
    extends JPanel {
        private Reference<Node> weakNode;
        private boolean focused = false;

        NullPanel(Node node) {
            this.weakNode = new WeakReference<Node>(node);
        }

        void setNode(Node node) {
            this.weakNode = new WeakReference<Node>(node);
        }

        @Override
        public AccessibleContext getAccessibleContext() {
            if (this.accessibleContext == null) {
                this.accessibleContext = new AccessibleNullPanel();
            }
            return this.accessibleContext;
        }

        public void setFocused(boolean val) {
            this.focused = val;
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            if (this.focused) {
                Color bdr = UIManager.getColor("Tree.selectionBorderColor");
                if (bdr == null) {
                    bdr = this.getForeground().equals(Color.BLACK) ? this.getBackground().darker() : this.getForeground().darker();
                }
                g.setColor(bdr);
                g.drawRect(1, 1, this.getWidth() - 3, this.getHeight() - 3);
                g.setColor(bdr);
            }
        }

        @Override
        public void addComponentListener(ComponentListener l) {
        }

        @Override
        public void addHierarchyListener(HierarchyListener l) {
        }

        @Override
        public void repaint() {
        }

        @Override
        public void repaint(int x, int y, int width, int height) {
        }

        @Override
        public void invalidate() {
        }

        @Override
        public void revalidate() {
        }

        @Override
        public void validate() {
        }

        @Override
        public void firePropertyChange(String s, Object a, Object b) {
        }

        private class AccessibleNullPanel
        extends JPanel.AccessibleJPanel {
            AccessibleNullPanel() {
                super(NullPanel.this);
            }

            @Override
            public String getAccessibleName() {
                String name = super.getAccessibleName();
                if (name == null) {
                    name = TableSheetCell.getString("ACS_NullPanel");
                }
                return name;
            }

            @Override
            public String getAccessibleDescription() {
                Node node;
                String description = super.getAccessibleDescription();
                if (description == null && (node = (Node)NullPanel.this.weakNode.get()) != null) {
                    description = MessageFormat.format(TableSheetCell.getString("ACSD_NullPanel"), node.getDisplayName());
                }
                return description;
            }
        }

    }

}

