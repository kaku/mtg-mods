/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.Outline
 *  org.netbeans.swing.outline.RowModel
 *  org.openide.DialogDisplayer
 *  org.openide.ErrorManager
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.Mnemonics
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeMemberEvent
 *  org.openide.nodes.NodeReorderEvent
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 */
package org.openide.explorer.view;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EventListener;
import java.util.WeakHashMap;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.event.TableModelEvent;
import javax.swing.table.TableModel;
import javax.swing.tree.AbstractLayoutCache;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import org.netbeans.swing.outline.Outline;
import org.netbeans.swing.outline.RowModel;
import org.openide.DialogDisplayer;
import org.openide.ErrorManager;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.explorer.view.Visualizer;
import org.openide.nodes.Node;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;

class PropertiesRowModel
implements RowModel {
    private Node.Property[] prop = new Node.Property[0];
    private Outline outline;
    private WeakHashMap<Node, PropertyChangeListener> nodesListenersCache = new WeakHashMap();
    private String[] names = new String[this.prop.length];
    private String[] descs = new String[this.prop.length];
    private PropertyChangeListener pcl;
    OutlineTooltipUpdater otu;
    NodeListener nl;
    private boolean ignoreSetValue;

    public PropertiesRowModel() {
        this.pcl = new PropertyChangeListener(){

            @Override
            public void propertyChange(final PropertyChangeEvent evt) {
                if (!SwingUtilities.isEventDispatchThread()) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            1.this.propertyChange(evt);
                        }
                    });
                    return;
                }
                int row = PropertiesRowModel.this.rowForNode((Node)evt.getSource());
                if (row == -1) {
                    return;
                }
                int column = PropertiesRowModel.this.columnForProperty(evt.getPropertyName());
                if (column == -1) {
                    PropertiesRowModel.this.outline.tableChanged(new TableModelEvent(PropertiesRowModel.this.outline.getModel(), row, row, -1, 0));
                } else {
                    PropertiesRowModel.this.outline.tableChanged(new TableModelEvent(PropertiesRowModel.this.outline.getModel(), row, row, column + 1, 0));
                }
            }

        };
        this.otu = new OutlineTooltipUpdater();
        this.nl = new NodeListener(){

            public void childrenAdded(NodeMemberEvent ev) {
            }

            public void childrenRemoved(NodeMemberEvent ev) {
            }

            public void childrenReordered(NodeReorderEvent ev) {
            }

            public void nodeDestroyed(NodeEvent ev) {
            }

            public void propertyChange(final PropertyChangeEvent evt) {
                if (!SwingUtilities.isEventDispatchThread()) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            2.this.propertyChange(evt);
                        }
                    });
                    return;
                }
                if ("shortDescription".equals(evt.getPropertyName())) {
                    int row = PropertiesRowModel.this.rowForNode((Node)evt.getSource());
                    PropertiesRowModel.this.otu.fireToolTipChanged(PropertiesRowModel.this.outline, row);
                }
                PropertiesRowModel.this.pcl.propertyChange(evt);
            }

        };
    }

    public void setOutline(Outline outline) {
        if (this.outline != null) {
            this.outline.removeMouseListener((MouseListener)this.otu);
            this.outline.removeMouseMotionListener((MouseMotionListener)this.otu);
        }
        this.outline = outline;
        outline.addMouseListener((MouseListener)this.otu);
        outline.addMouseMotionListener((MouseMotionListener)this.otu);
    }

    private int rowForNode(Node n) {
        TreeNode tn = Visualizer.findVisualizer(n);
        if (tn != null) {
            ArrayList<TreeNode> al = new ArrayList<TreeNode>();
            while (tn != null) {
                al.add(tn);
                tn = tn.getParent();
            }
            Collections.reverse(al);
            TreePath tp = new TreePath(al.toArray());
            int row = this.outline.getLayoutCache().getRowForPath(tp);
            return row;
        }
        return -1;
    }

    public Class getColumnClass(int column) {
        return Node.Property.class;
    }

    public int getColumnCount() {
        return this.prop.length;
    }

    public String getColumnName(int column) {
        assert (column < this.prop.length);
        if (this.names[column] == null) {
            String n = this.prop[column].getDisplayName();
            JLabel l = new JLabel();
            Mnemonics.setLocalizedText((JLabel)l, (String)n);
            this.names[column] = l.getText();
        }
        return this.names[column];
    }

    public String getShortDescription(int column) {
        assert (column < this.prop.length);
        if (this.descs[column] == null) {
            String n = this.prop[column].getShortDescription();
            JLabel l = new JLabel();
            Mnemonics.setLocalizedText((JLabel)l, (String)n);
            this.descs[column] = l.getText();
        }
        return this.descs[column];
    }

    public void setShortDescription(int column, String descr) {
        assert (column < this.prop.length);
        this.prop[column].setShortDescription(descr);
        this.descs[column] = null;
    }

    public String getRawColumnName(int column) {
        return this.prop[column].getDisplayName();
    }

    public Object getValueFor(Object node, int column) {
        Node n = Visualizer.findNode(node);
        if (n == null) {
            throw new IllegalStateException("TreeNode must be VisualizerNode but was: " + node + " of class " + node.getClass().getName());
        }
        PropertyChangeListener cacheEntry = this.nodesListenersCache.get((Object)n);
        if (cacheEntry == null) {
            PropertyChangeListener p = WeakListeners.propertyChange((PropertyChangeListener)this.pcl, (Object)n);
            this.nodesListenersCache.put(n, p);
            n.addPropertyChangeListener(p);
            NodeListener l = (NodeListener)WeakListeners.create(NodeListener.class, (EventListener)this.nl, (Object)n);
            n.addNodeListener(l);
        }
        Node.Property theRealProperty = this.getPropertyFor(n, this.prop[column]);
        return theRealProperty;
    }

    public boolean isCellEditable(Object node, int column) {
        Node n = Visualizer.findNode(node);
        if (n == null) {
            throw new IllegalStateException("TreeNode must be VisualizerNode but was: " + node + " of class " + node.getClass().getName());
        }
        Node.Property theRealProperty = this.getPropertyFor(n, this.prop[column]);
        if (theRealProperty != null) {
            return theRealProperty.canWrite();
        }
        return false;
    }

    protected Node.Property getPropertyFor(Node node, Node.Property prop) {
        Node.PropertySet[] propSets = node.getPropertySets();
        for (int i = 0; i < propSets.length; ++i) {
            Node.Property[] props = propSets[i].getProperties();
            for (int j = 0; j < props.length; ++j) {
                if (!prop.equals((Object)props[j])) continue;
                return props[j];
            }
        }
        return null;
    }

    void setIgnoreSetValue(boolean ignoreSetValue) {
        this.ignoreSetValue = ignoreSetValue;
    }

    public void setValueFor(Object node, int column, Object value) {
        if (this.ignoreSetValue) {
            return;
        }
        Node n = Visualizer.findNode(node);
        if (n == null) {
            throw new IllegalStateException("TreeNode must be VisualizerNode but was: " + node + " of class " + node.getClass().getName());
        }
        Node.Property theRealProperty = this.getPropertyFor(n, this.prop[column]);
        try {
            theRealProperty.setValue(value);
        }
        catch (IllegalAccessException ex) {
            PropertiesRowModel.processThrowable(ex, theRealProperty.getDisplayName(), value);
        }
        catch (IllegalArgumentException ex) {
            PropertiesRowModel.processThrowable(ex, theRealProperty.getDisplayName(), value);
        }
        catch (InvocationTargetException ex) {
            PropertiesRowModel.processThrowable(ex.getTargetException(), theRealProperty.getDisplayName(), value);
        }
    }

    private static void processThrowable(Throwable throwable, String title, Object newValue) {
        String msg;
        if (throwable instanceof ThreadDeath) {
            throw (ThreadDeath)throwable;
        }
        String locMsg = Exceptions.findLocalizedMessage((Throwable)throwable);
        if (locMsg != null && throwable.getLocalizedMessage() != throwable.getMessage()) {
            msg = NbBundle.getMessage(PropertiesRowModel.class, (String)"FMT_ErrorSettingValue", (Object)newValue, (Object)title);
            Exceptions.attachLocalizedMessage((Throwable)throwable, (String)msg);
        } else if (throwable instanceof NumberFormatException) {
            Exceptions.attachLocalizedMessage((Throwable)throwable, (String)NbBundle.getMessage(PropertiesRowModel.class, (String)"FMT_BAD_NUMBER_FORMAT", (Object)newValue));
        }
        msg = Exceptions.findLocalizedMessage((Throwable)throwable);
        if (msg == null) {
            msg = NbBundle.getMessage(PropertiesRowModel.class, (String)"FMT_ErrorSettingValue", (Object)newValue, (Object)title);
        }
        NotifyDescriptor.Message d = new NotifyDescriptor.Message((Object)msg, 1);
        DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)d);
    }

    public void setProperties(Node.Property[] newProperties) {
        this.prop = newProperties;
        this.names = new String[this.prop.length];
        this.descs = new String[this.prop.length];
    }

    public static Object getValueFromProperty(Object property) {
        if (property instanceof Node.Property) {
            Node.Property prop = (Node.Property)property;
            try {
                return prop.getValue();
            }
            catch (Exception x) {
                ErrorManager.getDefault().getInstance(PropertiesRowModel.class.getName()).notify(1, (Throwable)x);
            }
        }
        return null;
    }

    private int columnForProperty(String propName) {
        for (int i = 0; i < this.prop.length; ++i) {
            if (!this.prop[i].getName().equals(propName)) continue;
            return i;
        }
        return -1;
    }

    Object getPropertyValue(String propName, int column) {
        return this.prop[column].getValue(propName);
    }

    final Node.Property[] getProperties() {
        return this.prop;
    }

    public static void toggleBooleanProperty(Node.Property<Boolean> p) {
        if (p.getValueType() == Boolean.class || p.getValueType() == Boolean.TYPE) {
            if (!p.canWrite()) {
                return;
            }
            try {
                Boolean val = (Boolean)p.getValue();
                if (Boolean.FALSE.equals(val)) {
                    p.setValue((Object)Boolean.TRUE);
                } else {
                    p.setValue((Object)Boolean.FALSE);
                }
            }
            catch (Exception e1) {
                ErrorManager.getDefault().notify(16, (Throwable)e1);
            }
        }
    }

    private static class OutlineTooltipUpdater
    implements MouseMotionListener,
    MouseListener {
        private MouseEvent lastMouseMovedEvent = null;

        private OutlineTooltipUpdater() {
        }

        public void fireToolTipChanged(final Outline outline, final int row) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    int r;
                    if (OutlineTooltipUpdater.this.lastMouseMovedEvent != null && (r = outline.rowAtPoint(OutlineTooltipUpdater.this.lastMouseMovedEvent.getPoint())) == row) {
                        ToolTipManager.sharedInstance().mouseMoved(OutlineTooltipUpdater.this.lastMouseMovedEvent);
                    }
                }
            });
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            this.lastMouseMovedEvent = null;
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            this.lastMouseMovedEvent = e;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            this.lastMouseMovedEvent = null;
        }

        @Override
        public void mousePressed(MouseEvent e) {
            this.lastMouseMovedEvent = null;
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            this.lastMouseMovedEvent = null;
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            this.lastMouseMovedEvent = null;
        }

        @Override
        public void mouseExited(MouseEvent e) {
            this.lastMouseMovedEvent = null;
        }

    }

}

