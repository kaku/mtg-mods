/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package org.openide.explorer.view;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Stroke;
import java.awt.image.ImageObserver;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import org.openide.explorer.view.Visualizer;
import org.openide.nodes.Node;

final class IconPanel
extends JPanel
implements ListCellRenderer {
    private Image thumbImage;
    private boolean selected;
    private boolean focused;
    private JPanel jPNImage;
    private JLabel label;

    public IconPanel() {
        this.initComponents();
    }

    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        Node node = Visualizer.findNode(value);
        this.thumbImage = node.getIcon(2);
        this.selected = isSelected;
        this.label.setOpaque(this.selected);
        if (this.selected) {
            this.label.setBackground(UIManager.getColor("List.selectionBackground"));
            this.label.setForeground(UIManager.getColor("List.selectionForeground"));
        } else {
            this.label.setBackground(UIManager.getColor("Label.background"));
            this.label.setForeground(UIManager.getColor("Label.foreground"));
        }
        this.focused = cellHasFocus;
        this.label.setText(node.getDisplayName());
        return this;
    }

    private void initComponents() {
        this.jPNImage = new Viewer();
        this.label = new JLabel();
        this.setBackground(new Color(51, 51, 51));
        this.setOpaque(false);
        this.setLayout(new BorderLayout());
        this.jPNImage.setOpaque(false);
        this.jPNImage.setLayout(null);
        this.add((Component)this.jPNImage, "Center");
        this.label.setHorizontalAlignment(0);
        this.add((Component)this.label, "South");
    }

    private class Viewer
    extends JPanel {
        public Viewer() {
            this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10), BorderFactory.createCompoundBorder(new LineBorder(new Color(255, 255, 255), 2, true), BorderFactory.createEmptyBorder(10, 10, 10, 10))));
        }

        @Override
        protected void paintComponent(Graphics graphics) {
            super.paintComponent(graphics);
            Graphics2D g = (Graphics2D)graphics;
            if (IconPanel.this.selected) {
                g.setColor(Color.BLUE.darker().darker());
                g.fillRect(12, 12, this.getWidth() - 24, this.getHeight() - 24);
            }
            if (IconPanel.this.focused) {
                g.setColor(Color.WHITE);
                g.setStroke(new BasicStroke(1.0f, 2, 0, 1.0f, new float[]{1.0f, 2.0f, 1.0f, 2.0f}, 0.0f));
                g.drawRect(18, 18, this.getWidth() - 36, this.getHeight() - 36);
                g.setStroke(new BasicStroke(1.0f));
            }
            g.drawImage(IconPanel.this.thumbImage, this.getWidth() / 2 - IconPanel.this.thumbImage.getWidth(this) / 2, this.getHeight() / 2 - IconPanel.this.thumbImage.getHeight(this) / 2, this);
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(IconPanel.this.thumbImage.getWidth(this) + this.getInsets().left + this.getInsets().right, IconPanel.this.thumbImage.getHeight(this) + this.getInsets().top + this.getInsets().bottom);
        }
    }

}

