/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.QuickSearch
 *  org.openide.awt.QuickSearch$Callback
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 */
package org.openide.explorer.view;

import java.awt.ItemSelectable;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.MenuElement;
import javax.swing.MenuSelectionManager;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.openide.awt.QuickSearch;
import org.openide.explorer.view.OutlineView;
import org.openide.explorer.view.QuickSearchTableFilter;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

class TableQuickSearchSupport
implements QuickSearch.Callback {
    private int quickSearchInitialRow = -1;
    private int quickSearchInitialColumn = -1;
    private int quickSearchLastRow = -1;
    private int quickSearchLastColumn = -1;
    private String lastSearchText;
    private JTable table;
    private QuickSearchTableFilter quickSearchTableFilter;
    private QuickSearchSettings qss;

    TableQuickSearchSupport(JTable table, QuickSearchTableFilter quickSearchTableFilter, QuickSearchSettings qss) {
        this.table = table;
        this.quickSearchTableFilter = quickSearchTableFilter;
        this.qss = qss;
    }

    public void setQuickSearchTableFilter(QuickSearchTableFilter quickSearchTableFilter, boolean asynchronous) {
        this.quickSearchTableFilter = quickSearchTableFilter;
    }

    public void quickSearchUpdate(String searchText) {
        this.lastSearchText = searchText;
        if (this.quickSearchInitialRow == -1) {
            this.quickSearchInitialRow = this.table.getSelectedRow();
            this.quickSearchInitialColumn = this.table.getSelectedColumn();
            if (this.quickSearchInitialRow == -1) {
                this.quickSearchInitialRow = 0;
            }
            if (this.quickSearchInitialColumn == -1) {
                this.quickSearchInitialColumn = 0;
            }
        }
        this.quickSearchLastRow = this.quickSearchInitialRow;
        this.quickSearchLastColumn = this.quickSearchInitialColumn;
        this.doSearch(searchText, true);
    }

    public void showNextSelection(boolean forward) {
        if (forward && ++this.quickSearchLastColumn >= this.table.getColumnCount()) {
            this.quickSearchLastColumn = 0;
            if (++this.quickSearchLastRow >= this.table.getRowCount()) {
                this.quickSearchLastRow = 0;
            }
        }
        this.doSearch(this.lastSearchText, forward);
    }

    public String findMaxPrefix(String prefix) {
        String prefixUp = this.qss.isMatchCase() ? prefix : prefix.toUpperCase();
        int row1 = 0;
        int row2 = this.table.getRowCount();
        int col1 = 0;
        int col2 = this.table.getColumnCount();
        String maxPrefix = null;
        for (int row = row1; row < row2; ++row) {
            for (int col = col1; col < col2; ++col) {
                String str = this.quickSearchTableFilter.getStringValueAt(row, col);
                String strUp = this.qss.isMatchCase() ? str : str.toUpperCase();
                if (!strUp.startsWith(prefixUp)) continue;
                maxPrefix = maxPrefix == null ? str : QuickSearch.findMaxPrefix((String)maxPrefix, (String)str, (boolean)(!this.qss.isMatchCase()));
            }
        }
        if (maxPrefix != null) {
            return maxPrefix;
        }
        return prefix;
    }

    public void quickSearchConfirmed() {
        this.quickSearchInitialRow = -1;
        this.quickSearchInitialColumn = -1;
    }

    public void quickSearchCanceled() {
        this.displaySearchResult(this.quickSearchInitialRow, this.quickSearchInitialColumn);
        this.quickSearchInitialRow = -1;
        this.quickSearchInitialColumn = -1;
    }

    private void doSearch(String searchText, boolean forward) {
        boolean bl;
        if (!this.qss.isMatchCase()) {
            searchText = searchText.toUpperCase();
        }
        int n = this.table.getRowCount();
        int row1 = this.quickSearchLastRow;
        int row2 = this.quickSearchLastRow + n;
        boolean lineStartSearch = true;
        Set<String> columnsIgnoredToSearch = this.qss.getColumnsIgnoredToSearch();
        do {
            int row;
            int col1 = this.quickSearchLastColumn;
            int col2 = forward ? this.table.getColumnCount() : 0;
            int n2 = row = forward ? row1 : row2 - 1;
            while (forward ? row < row2 : row >= row1) {
                int col = col1;
                while (forward ? col < col2 : col >= col2) {
                    String str;
                    String cName = this.table.getColumnName(col);
                    if (!columnsIgnoredToSearch.contains(cName) && (str = this.quickSearchTableFilter.getStringValueAt(row % n, col)) != null) {
                        if (!this.qss.isMatchCase()) {
                            str = str.toUpperCase();
                        }
                        if (lineStartSearch) {
                            if (str.startsWith(searchText)) {
                                this.displaySearchResult(row % n, col);
                                return;
                            }
                        } else if (str.indexOf(searchText) >= 0) {
                            this.displaySearchResult(row % n, col);
                            return;
                        }
                    }
                    col = forward ? ++col : --col;
                }
                col1 = forward ? 0 : this.table.getColumnCount() - 1;
                row = forward ? ++row : --row;
            }
            bl = !lineStartSearch;
        } while (!(lineStartSearch = bl));
        this.table.getSelectionModel().clearSelection();
        this.table.getColumnModel().getSelectionModel().clearSelection();
        this.table.scrollRectToVisible(this.table.getCellRect(this.quickSearchInitialRow, this.quickSearchInitialColumn, true));
    }

    private void displaySearchResult(int row, int column) {
        this.quickSearchLastRow = row;
        this.quickSearchLastColumn = column;
        this.table.getSelectionModel().setSelectionInterval(row, row);
        this.table.getColumnModel().getSelectionModel().setSelectionInterval(column, column);
        this.table.scrollRectToVisible(this.table.getCellRect(row, column, true));
    }

    JMenu createSearchPopupMenu() {
        return new JMenu(){

            @Override
            public JPopupMenu getPopupMenu() {
                return TableQuickSearchSupport.getSearchPopupMenu(TableQuickSearchSupport.this.qss, TableQuickSearchSupport.this.table.getColumnModel(), new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        TableQuickSearchSupport.this.doSearch(TableQuickSearchSupport.this.lastSearchText, true);
                    }
                });
            }

        };
    }

    static JPopupMenu getSearchPopupMenu(final QuickSearchSettings qss, TableColumnModel columnModel, final ActionListener doSearchAction) {
        JPopupMenu pm = new JPopupMenu();
        final JCheckBoxMenuItem matchCase = new JCheckBoxMenuItem(NbBundle.getMessage(OutlineView.class, (String)"CTL_MatchCase"), qss.isMatchCase());
        ItemListener iListener = new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent e) {
                JCheckBoxMenuItem cMenu = (JCheckBoxMenuItem)e.getItemSelectable();
                if (cMenu == matchCase) {
                    qss.setMatchCase(cMenu.isSelected());
                } else {
                    String cName = cMenu.getText();
                    if (cMenu.isSelected()) {
                        qss.addColumnToSearch(cName);
                    } else {
                        qss.removeColumnFromSearch(cName);
                    }
                }
                doSearchAction.actionPerformed(null);
            }
        };
        matchCase.addItemListener(iListener);
        pm.add(matchCase);
        pm.addSeparator();
        Enumeration<TableColumn> columns = columnModel.getColumns();
        if (columns.hasMoreElements()) {
            JMenuItem description = new JMenuItem(NbBundle.getMessage(OutlineView.class, (String)"CTL_ColumnsToSearch")){

                @Override
                public void processMouseEvent(MouseEvent e, MenuElement[] path, MenuSelectionManager manager) {
                }

                @Override
                protected void processMouseEvent(MouseEvent e) {
                }
            };
            description.setFocusable(false);
            description.setModel(new DummyButtonModel());
            pm.add(description);
        }
        while (columns.hasMoreElements()) {
            TableColumn column = columns.nextElement();
            String cName = column.getHeaderValue().toString();
            JCheckBoxMenuItem cMenu = new JCheckBoxMenuItem(cName, !qss.getColumnsIgnoredToSearch().contains(cName));
            cMenu.addItemListener(iListener);
            pm.add(cMenu);
        }
        return pm;
    }

    static final class DummyButtonModel
    implements ButtonModel {
        DummyButtonModel() {
        }

        @Override
        public boolean isArmed() {
            return false;
        }

        @Override
        public boolean isSelected() {
            return false;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }

        @Override
        public boolean isPressed() {
            return false;
        }

        @Override
        public boolean isRollover() {
            return false;
        }

        @Override
        public void setArmed(boolean b) {
        }

        @Override
        public void setSelected(boolean b) {
        }

        @Override
        public void setEnabled(boolean b) {
        }

        @Override
        public void setPressed(boolean b) {
        }

        @Override
        public void setRollover(boolean b) {
        }

        @Override
        public void setMnemonic(int key) {
        }

        @Override
        public int getMnemonic() {
            return 0;
        }

        @Override
        public void setActionCommand(String s) {
        }

        @Override
        public String getActionCommand() {
            return null;
        }

        @Override
        public void setGroup(ButtonGroup group) {
        }

        @Override
        public void addActionListener(ActionListener l) {
        }

        @Override
        public void removeActionListener(ActionListener l) {
        }

        @Override
        public void addItemListener(ItemListener l) {
        }

        @Override
        public void removeItemListener(ItemListener l) {
        }

        @Override
        public void addChangeListener(ChangeListener l) {
        }

        @Override
        public void removeChangeListener(ChangeListener l) {
        }

        @Override
        public Object[] getSelectedObjects() {
            return new Object[0];
        }
    }

    static interface StringValuedTable {
        public String getStringValueAt(int var1, int var2);
    }

    static final class QuickSearchSettings {
        private boolean matchCase = NbPreferences.forModule(QuickSearchSettings.class).getBoolean("matchCase", false);
        private Set<String> columnsIgnoredToSearch = new HashSet<String>();

        QuickSearchSettings() {
        }

        public boolean isMatchCase() {
            return this.matchCase;
        }

        public void setMatchCase(boolean matchCase) {
            this.matchCase = matchCase;
            NbPreferences.forModule(QuickSearchSettings.class).putBoolean("matchCase", matchCase);
        }

        public Set<String> getColumnsIgnoredToSearch() {
            return this.columnsIgnoredToSearch;
        }

        public void addColumnToSearch(String columnName) {
            this.columnsIgnoredToSearch.remove(columnName);
        }

        public void removeColumnFromSearch(String columnName) {
            this.columnsIgnoredToSearch.add(columnName);
        }
    }

}

