/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.datatransfer.PasteType
 */
package org.openide.explorer.view;

import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import javax.swing.JList;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import org.openide.explorer.view.DragDropUtilities;
import org.openide.explorer.view.ExplorerDnDManager;
import org.openide.explorer.view.ListView;
import org.openide.explorer.view.NodeRenderer;
import org.openide.explorer.view.VisualizerNode;
import org.openide.nodes.Node;
import org.openide.util.RequestProcessor;
import org.openide.util.datatransfer.PasteType;

final class ListViewDropSupport
implements DropTargetListener,
Runnable {
    boolean active = false;
    boolean dropTargetPopupAllowed;
    DropTarget dropTarget;
    int lastIndex = -1;
    protected ListView view;
    protected JList list;

    public ListViewDropSupport(ListView view, JList list) {
        this(view, list, true);
    }

    public ListViewDropSupport(ListView view, JList list, boolean dropTargetPopupAllowed) {
        this.view = view;
        this.list = list;
        this.dropTargetPopupAllowed = dropTargetPopupAllowed;
    }

    public void setDropTargetPopupAllowed(boolean value) {
        this.dropTargetPopupAllowed = value;
    }

    public boolean isDropTargetPopupAllowed() {
        return this.dropTargetPopupAllowed;
    }

    @Override
    public void dragEnter(DropTargetDragEvent dtde) {
        ExplorerDnDManager.getDefault().setMaybeExternalDragAndDrop(true);
        int dropAction = ExplorerDnDManager.getDefault().getAdjustedDropAction(dtde.getDropAction(), this.view.getAllowedDropActions());
        this.lastIndex = this.indexWithCheck(dtde);
        if (this.lastIndex < 0) {
            dtde.rejectDrag();
        } else {
            dtde.acceptDrag(dropAction);
            NodeRenderer.dragEnter(this.list.getModel().getElementAt(this.lastIndex));
            this.list.repaint(this.list.getCellBounds(this.lastIndex, this.lastIndex));
        }
    }

    @Override
    public void dragOver(DropTargetDragEvent dtde) {
        ExplorerDnDManager.getDefault().setMaybeExternalDragAndDrop(true);
        int dropAction = ExplorerDnDManager.getDefault().getAdjustedDropAction(dtde.getDropAction(), this.view.getAllowedDropActions());
        int index = this.indexWithCheck(dtde);
        if (index < 0) {
            dtde.rejectDrag();
            if (this.lastIndex >= 0) {
                NodeRenderer.dragExit();
                this.list.repaint(this.list.getCellBounds(this.lastIndex, this.lastIndex));
                this.lastIndex = -1;
            }
        } else {
            dtde.acceptDrag(dropAction);
            if (this.lastIndex != index) {
                if (this.lastIndex < 0) {
                    this.lastIndex = index;
                }
                NodeRenderer.dragExit();
                NodeRenderer.dragEnter(this.list.getModel().getElementAt(index));
                this.list.repaint(this.list.getCellBounds(this.lastIndex, index));
                this.lastIndex = index;
            }
        }
    }

    @Override
    public void dropActionChanged(DropTargetDragEvent dtde) {
    }

    @Override
    public void dragExit(DropTargetEvent dte) {
        ExplorerDnDManager.getDefault().setMaybeExternalDragAndDrop(false);
        if (this.lastIndex >= 0) {
            NodeRenderer.dragExit();
            this.list.repaint(this.list.getCellBounds(this.lastIndex, this.lastIndex));
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void drop(DropTargetDropEvent dtde) {
        boolean dropResult = true;
        try {
            PasteType pt;
            int index = this.list.locationToIndex(dtde.getLocation());
            Object obj = this.list.getModel().getElementAt(index);
            Node dropNode = null;
            if (obj instanceof VisualizerNode) {
                dropNode = ((VisualizerNode)obj).node;
            }
            int dropAction = ExplorerDnDManager.getDefault().getAdjustedDropAction(dtde.getDropAction(), this.view.getAllowedDropActions());
            ExplorerDnDManager.getDefault().setMaybeExternalDragAndDrop(false);
            if (index < 0 || !this.canDrop(dropNode, dropAction, dtde.getTransferable(), index)) {
                dtde.rejectDrop();
                return;
            }
            Transferable t = ExplorerDnDManager.getDefault().getDraggedTransferable((2 & dropAction) != 0);
            if (null == t) {
                t = dtde.getTransferable();
            }
            if ((pt = DragDropUtilities.getDropType(dropNode, t, dropAction, index)) == null) {
                dropResult = false;
                RequestProcessor.getDefault().post((Runnable)this, 500);
                return;
            }
            dtde.acceptDrop(dropAction);
            if (dropAction != 1073741824) {
                DragDropUtilities.performPaste(pt, null);
            }
        }
        finally {
            dtde.dropComplete(dropResult);
        }
    }

    private boolean canDrop(Node n, int dropAction, Transferable dndEventTransferable, int dropIndex) {
        Node[] nodes;
        if (n == null) {
            return false;
        }
        if (ExplorerDnDManager.getDefault().getNodeAllowedActions() == 0) {
            return false;
        }
        if ((2 & dropAction) != 0 && null != (nodes = ExplorerDnDManager.getDefault().getDraggedNodes())) {
            for (int i = 0; i < nodes.length; ++i) {
                if (!n.equals((Object)nodes[i].getParentNode())) continue;
                return false;
            }
        }
        Transferable trans = ExplorerDnDManager.getDefault().getDraggedTransferable((2 & dropAction) != 0);
        if (trans == null && (trans = dndEventTransferable) == null) {
            return false;
        }
        PasteType pt = DragDropUtilities.getDropType(n, trans, dropAction, dropIndex);
        return pt != null;
    }

    public void activate(boolean active) {
        if (this.active == active) {
            return;
        }
        this.active = active;
        this.getDropTarget().setActive(active);
    }

    @Override
    public void run() {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(this);
            return;
        }
        DragDropUtilities.dropNotSuccesfull();
    }

    int indexWithCheck(DropTargetDragEvent dtde) {
        int dropAction = ExplorerDnDManager.getDefault().getAdjustedDropAction(dtde.getDropAction(), this.view.getAllowedDropActions());
        if ((dropAction & this.view.getAllowedDropActions()) == 0) {
            return -1;
        }
        int index = this.list.locationToIndex(dtde.getLocation());
        if (index == -1) {
            return -1;
        }
        Object obj = this.list.getModel().getElementAt(index);
        if (obj instanceof VisualizerNode) {
            obj = ((VisualizerNode)obj).node;
        }
        if (index < 0) {
            return -1;
        }
        if (!(obj instanceof Node)) {
            return -1;
        }
        return index;
    }

    DropTarget getDropTarget() {
        if (this.dropTarget == null) {
            this.dropTarget = new DropTarget(this.list, this.view.getAllowedDropActions(), this, false);
        }
        return this.dropTarget;
    }
}

