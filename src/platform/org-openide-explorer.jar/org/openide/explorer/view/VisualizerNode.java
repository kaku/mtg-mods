/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeMemberEvent
 *  org.openide.nodes.NodeOp
 *  org.openide.nodes.NodeReorderEvent
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 *  org.openide.util.NbBundle
 */
package org.openide.explorer.view;

import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Enumeration;
import java.util.EventListener;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.SwingUtilities;
import javax.swing.event.EventListenerList;
import javax.swing.tree.TreeNode;
import org.openide.explorer.view.NodeModel;
import org.openide.explorer.view.VisualizerChildren;
import org.openide.explorer.view.VisualizerEvent;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeOp;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.ImageUtilities;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;

final class VisualizerNode
extends EventListenerList
implements NodeListener,
TreeNode,
Runnable {
    private static final VisualizerNode TEMPLATE = new VisualizerNode(0);
    static final Logger LOG = Logger.getLogger(VisualizerNode.class.getName());
    private static final Reference<VisualizerChildren> NO_REF = new WeakReference<Object>(null);
    private static WeakHashMap<VisualizerNode, Reference<VisualizerNode>> cache = new WeakHashMap();
    public static final VisualizerNode EMPTY = VisualizerNode.getVisualizer(null, Node.EMPTY);
    private static final QP QUEUE = new QP();
    private static final String UNKNOWN = new String();
    static final long serialVersionUID = 3726728244698316872L;
    private static final String NO_HTML_DISPLAYNAME = "noHtmlDisplayName";
    private static Icon defaultIcon;
    private static final String DEFAULT_ICON = "org/openide/nodes/defaultNode.png";
    private static final int prefetchCount;
    private Icon icon = null;
    Node node;
    private int hashCode;
    private Reference<VisualizerChildren> children = NO_REF;
    VisualizerChildren parent;
    int indexOf = -1;
    private String name;
    private String displayName;
    private String shortDescription;
    private String htmlDisplayName = null;
    private int cachedIconType = -1;

    private VisualizerNode(int hashCode) {
        this.hashCode = hashCode;
        this.node = null;
    }

    private VisualizerNode(Node n) {
        this.node = n;
        this.hashCode = System.identityHashCode((Object)this.node);
        this.node.addNodeListener(NodeOp.weakNodeListener((NodeListener)this, (Object)this.node));
        this.name = UNKNOWN;
        this.displayName = UNKNOWN;
        this.shortDescription = UNKNOWN;
    }

    public static VisualizerNode getVisualizer(VisualizerChildren ch, Node n) {
        return VisualizerNode.getVisualizer(ch, n, true);
    }

    public static synchronized VisualizerNode getVisualizer(VisualizerChildren ch, Node n, boolean create) {
        VisualizerNode v;
        VisualizerNode.TEMPLATE.hashCode = System.identityHashCode((Object)n);
        VisualizerNode.TEMPLATE.node = n;
        Reference<VisualizerNode> r = cache.get(TEMPLATE);
        VisualizerNode.TEMPLATE.hashCode = 0;
        VisualizerNode.TEMPLATE.node = null;
        VisualizerNode visualizerNode = v = r == null ? null : r.get();
        if (v == null) {
            if (!create) {
                return null;
            }
            v = new VisualizerNode(n);
            cache.put(v, new WeakReference<VisualizerNode>(v));
        }
        if (ch != null) {
            v.parent = ch;
        }
        return v;
    }

    public String getShortDescription() {
        String toolTip;
        String desc = this.shortDescription;
        if (desc == UNKNOWN) {
            this.shortDescription = desc = this.node.getShortDescription();
        }
        if ((toolTip = ImageUtilities.getImageToolTip((Image)ImageUtilities.icon2Image((Icon)(this.icon != null ? this.icon : this.getIcon(false, false))))).length() > 0) {
            StringBuilder str = new StringBuilder(128);
            desc = desc.replaceAll("</?html>", "");
            str.append("<html>").append(desc).append("<br>").append(toolTip).append("</html>");
            desc = str.toString();
        }
        return desc;
    }

    public String getDisplayName() {
        if (this.displayName == UNKNOWN) {
            this.displayName = this.node == null ? null : this.node.getDisplayName();
        }
        return this.displayName;
    }

    public String getName() {
        if (this.name == UNKNOWN) {
            this.name = this.node == null ? null : this.node.getName();
        }
        return this.name;
    }

    public VisualizerChildren getChildren() {
        return this.getChildren(true);
    }

    final VisualizerChildren getChildren(boolean create) {
        VisualizerChildren ch = this.children.get();
        if (create && ch == null && !this.node.isLeaf()) {
            Children nch = this.node.getChildren();
            int count = nch.getNodesCount();
            Node[] nodes = null;
            if (prefetchCount > 0) {
                if (count <= prefetchCount) {
                    nodes = nch.getNodes();
                } else {
                    nodes = new Node[Math.min(prefetchCount, count)];
                    for (int i = 0; i < nodes.length; ++i) {
                        nodes[i] = nch.getNodeAt(i);
                    }
                }
            }
            ch = (VisualizerChildren)Children.MUTEX.readAccess((Mutex.Action)new Mutex.Action<VisualizerChildren>(){

                public VisualizerChildren run() {
                    List snapshot = VisualizerNode.this.node.getChildren().snapshot();
                    VisualizerChildren vc = new VisualizerChildren(VisualizerNode.this, snapshot);
                    VisualizerNode.this.notifyVisualizerChildrenChange(true, vc);
                    return vc;
                }
            });
        }
        return ch == null ? VisualizerChildren.EMPTY : ch;
    }

    @Override
    public int getIndex(TreeNode p1) {
        return this.getChildren().getIndex(p1);
    }

    @Override
    public boolean getAllowsChildren() {
        return !this.isLeaf();
    }

    private LogRecord assertAccess(int index) {
        if (Children.MUTEX.isReadAccess()) {
            return null;
        }
        if (Children.MUTEX.isWriteAccess()) {
            return null;
        }
        if (!LOG.isLoggable(Level.FINE)) {
            return null;
        }
        Level level = LOG.isLoggable(Level.FINEST) ? Level.FINEST : Level.FINE;
        LogRecord rec = new LogRecord(level, "LOG_NO_READ_ACCESS");
        rec.setResourceBundle(NbBundle.getBundle(VisualizerNode.class));
        rec.setParameters(new Object[]{this, index});
        rec.setLoggerName(LOG.getName());
        if (level == Level.FINEST) {
            rec.setThrown((Throwable)((Object)new AssertionError((Object)rec.getMessage())));
        }
        return rec;
    }

    @Override
    public TreeNode getChildAt(int p1) {
        return this.getChildren().getChildAt(p1);
    }

    @Override
    public int getChildCount() {
        return this.getChildren().getChildCount();
    }

    @Override
    public Enumeration<VisualizerNode> children() {
        return this.getChildren().children(true);
    }

    @Override
    public boolean isLeaf() {
        return this.node.isLeaf();
    }

    @Override
    public TreeNode getParent() {
        Node parent = this.node.getParentNode();
        return parent == null ? null : VisualizerNode.getVisualizer(null, parent);
    }

    public void childrenAdded(NodeMemberEvent ev) {
        VisualizerChildren ch = this.children.get();
        LOG.log(Level.FINER, "childrenAdded event: {0}\n  ch: {1}", new Object[]{ev, ch});
        if (ch == null) {
            LOG.log(Level.FINER, "childrenAdded - exit");
            return;
        }
        QUEUE.runSafe(new VisualizerEvent.Added(ch, ev.getDeltaIndices(), ev));
        LOG.log(Level.FINER, "childrenAdded - end");
    }

    public void childrenRemoved(NodeMemberEvent ev) {
        VisualizerChildren ch = this.children.get();
        LOG.log(Level.FINER, "childrenRemoved event: {0}\n  ch: {1}", new Object[]{ev, ch});
        if (ch == null) {
            LOG.log(Level.FINER, "childrenRemoved - exit");
            return;
        }
        QUEUE.runSafe(new VisualizerEvent.Removed(ch, ev.getDeltaIndices(), ev));
        LOG.log(Level.FINER, "childrenRemoved - end");
    }

    public void childrenReordered(NodeReorderEvent ev) {
        VisualizerChildren ch = this.children.get();
        int[] perm = ev.getPermutation();
        LOG.log(Level.FINER, "childrenReordered {0}\n  ch: {1}", new Object[]{perm, ch});
        if (ch == null) {
            LOG.log(Level.FINER, "childrenReordered - exit");
            return;
        }
        QUEUE.runSafe(new VisualizerEvent.Reordered(ch, perm, ev));
        LOG.log(Level.FINER, "childrenReordered - end");
    }

    public void nodeDestroyed(NodeEvent ev) {
        this.node = Node.EMPTY;
        QUEUE.runSafe(new VisualizerEvent.Destroyed(this.getChildren(false), ev));
    }

    public void propertyChange(PropertyChangeEvent evt) {
        boolean isIconChange;
        String name = evt.getPropertyName();
        boolean bl = isIconChange = "icon".equals(name) || "openedIcon".equals(name);
        if ("name".equals(name) || "displayName".equals(name) || isIconChange) {
            if (isIconChange) {
                this.cachedIconType = -1;
            }
            if ("displayName".equals(name)) {
                this.htmlDisplayName = null;
            }
            QUEUE.runSafe(this);
            return;
        }
        if ("shortDescription".equals(name) && this.shortDescription != UNKNOWN) {
            QUEUE.runSafe(this);
            return;
        }
        if ("leaf".equals(name)) {
            QUEUE.runSafe(new PropLeafChange());
            return;
        }
    }

    @Override
    public void run() {
        if (!Children.MUTEX.isReadAccess()) {
            Children.MUTEX.readAccess((Runnable)this);
            return;
        }
        this.name = this.node.getName();
        this.displayName = this.node.getDisplayName();
        this.shortDescription = UNKNOWN;
        for (VisualizerNode parent = this; parent != null; parent = (VisualizerNode)parent.getParent()) {
            Object[] listeners = parent.getListenerList();
            for (int i = listeners.length - 1; i >= 0; i -= 2) {
                ((NodeModel)listeners[i]).update(this);
            }
        }
    }

    void notifyVisualizerChildrenChange(boolean strongly, VisualizerChildren ch) {
        if (strongly) {
            if (this.children.getClass() != StrongReference.class || this.children.get() != ch) {
                this.children = new StrongReference<VisualizerChildren>(ch);
            }
        } else if (this.children.getClass() != WeakReference.class || this.children.get() != ch) {
            this.children = new WeakReference<VisualizerChildren>(ch);
        }
    }

    public synchronized void addNodeModel(NodeModel l) {
        this.add(NodeModel.class, l);
    }

    public synchronized void removeNodeModel(NodeModel l) {
        this.remove(NodeModel.class, l);
    }

    public int hashCode() {
        return this.hashCode;
    }

    public boolean equals(Object o) {
        if (!(o instanceof VisualizerNode)) {
            return false;
        }
        VisualizerNode v = (VisualizerNode)o;
        return v.node == this.node;
    }

    @Override
    public String toString() {
        return this.getDisplayName();
    }

    public String toId() {
        return "'" + this.getDisplayName() + "'@" + Integer.toHexString(System.identityHashCode(this)) + " parent: " + this.parent + " indexOf: " + this.indexOf;
    }

    public String getHtmlDisplayName() {
        if (this.htmlDisplayName == null) {
            this.htmlDisplayName = this.node.getHtmlDisplayName();
            if (this.htmlDisplayName == null) {
                this.htmlDisplayName = "noHtmlDisplayName";
            }
        }
        return this.htmlDisplayName == "noHtmlDisplayName" ? null : this.htmlDisplayName;
    }

    Icon getIcon(boolean opened, boolean large) {
        int newCacheType = VisualizerNode.getCacheType(opened, large);
        if (this.cachedIconType != newCacheType) {
            Image image;
            int iconType = large ? 2 : 1;
            try {
                Image image2 = image = opened ? this.node.getOpenedIcon(iconType) : this.node.getIcon(iconType);
                if (image == null) {
                    String method = opened ? "getOpenedIcon" : "getIcon";
                    LOG.warning("Node \"" + this.node.getName() + "\" [" + this.node.getClass().getName() + "] cannot return null from " + method + "(). See Node." + method + " contract.");
                }
            }
            catch (RuntimeException x) {
                LOG.log(Level.INFO, null, x);
                image = null;
            }
            this.icon = image == null ? VisualizerNode.getDefaultIcon() : ImageUtilities.image2Icon((Image)image);
        }
        this.cachedIconType = newCacheType;
        return this.icon;
    }

    private static final int getCacheType(boolean opened, boolean large) {
        return (opened ? 2 : 0) | (large ? 1 : 0);
    }

    private static Icon getDefaultIcon() {
        if (defaultIcon == null) {
            defaultIcon = ImageUtilities.loadImageIcon((String)"org/openide/nodes/defaultNode.png", (boolean)false);
        }
        return defaultIcon;
    }

    static void runSafe(Runnable run) {
        QUEUE.runSafe(run);
    }

    VisualizerNode[] getPathToRoot() {
        return this.getPathToRoot(0);
    }

    VisualizerNode[] getPathToRoot(int depth) {
        VisualizerNode[] retNodes = this.parent == null || this.parent.parent == null ? new VisualizerNode[depth] : this.parent.parent.getPathToRoot(++depth);
        retNodes[retNodes.length - depth] = this;
        return retNodes;
    }

    static {
        prefetchCount = Math.max(Integer.getInteger("org.openide.explorer.VisualizerNode.prefetchCount", 50), 0);
    }

    private class PropLeafChange
    implements Runnable {
        @Override
        public void run() {
            if (!Children.MUTEX.isReadAccess()) {
                Children.MUTEX.readAccess((Runnable)this);
                return;
            }
            VisualizerNode.this.children = NO_REF;
            for (VisualizerNode parent = VisualizerNode.this; parent != null; parent = (VisualizerNode)parent.getParent()) {
                Object[] listeners = parent.getListenerList();
                for (int i = listeners.length - 1; i >= 0; i -= 2) {
                    ((NodeModel)listeners[i]).structuralChange(VisualizerNode.this);
                }
            }
        }
    }

    private static final class QP {
        private LinkedList<Runnable> queue = new LinkedList();

        QP() {
        }

        boolean shouldBeInvokedLater(Runnable run) {
            return run instanceof VisualizerEvent.Removed && ((VisualizerEvent)((Object)run)).getSnapshot().getClass().getName().contains("DelayedLazySnapshot");
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void runSafe(Runnable run) {
            boolean wasEmpty = false;
            QP qP = this;
            synchronized (qP) {
                if (SwingUtilities.isEventDispatchThread() && this.shouldBeInvokedLater(run)) {
                    if (!this.queue.isEmpty()) {
                        this.queue.addLast(null);
                    }
                    this.queue.addLast(run);
                    SwingUtilities.invokeLater(new ProcessQueue(Integer.MAX_VALUE));
                    return;
                }
                wasEmpty = this.queue.isEmpty();
                this.queue.addLast(run);
            }
            if (wasEmpty) {
                if (SwingUtilities.isEventDispatchThread()) {
                    this.processQueue(Integer.MAX_VALUE);
                } else {
                    SwingUtilities.invokeLater(new ProcessQueue(500));
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void processQueue(int limitMillis) {
            long until = System.currentTimeMillis() + (long)limitMillis;
            boolean isEmpty = false;
            while (!isEmpty) {
                Runnable r;
                QP qP = this;
                synchronized (qP) {
                    r = this.queue.poll();
                    if (r == null) {
                        VisualizerNode.LOG.log(Level.FINER, "Marker found, interrupting queue");
                        return;
                    }
                    isEmpty = this.queue.isEmpty();
                }
                VisualizerNode.LOG.log(Level.FINER, "Running from queue {0}", r);
                Children.MUTEX.readAccess(r);
                VisualizerNode.LOG.log(Level.FINER, "Finished {0}", r);
                if (System.currentTimeMillis() <= until) continue;
                SwingUtilities.invokeLater(new ProcessQueue(limitMillis));
                VisualizerNode.LOG.log(Level.FINER, "timeout from {0} ms", limitMillis);
                return;
            }
            VisualizerNode.LOG.log(Level.FINER, "Queue processing over");
        }

        private class ProcessQueue
        implements Runnable {
            private final int limitMillis;

            public ProcessQueue(int limitMillis) {
                this.limitMillis = limitMillis;
            }

            @Override
            public void run() {
                QP.this.processQueue(this.limitMillis);
            }
        }

    }

    private static final class StrongReference<T>
    extends WeakReference<T> {
        private T o;

        public StrongReference(T o) {
            super(null);
            this.o = o;
        }

        @Override
        public T get() {
            return this.o;
        }
    }

}

