/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package org.openide.explorer.view;

import java.awt.Rectangle;
import java.beans.PropertyVetoException;
import java.util.logging.Logger;
import javax.swing.JTree;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.NodeTreeModel;
import org.openide.explorer.view.TreeView;
import org.openide.explorer.view.Visualizer;
import org.openide.explorer.view.VisualizerEvent;
import org.openide.explorer.view.VisualizerNode;
import org.openide.nodes.Node;

public class ContextTreeView
extends TreeView {
    static final long serialVersionUID = -8282594827988436813L;
    static final Logger LOG = Logger.getLogger(ContextTreeView.class.getName());

    public ContextTreeView() {
        this.tree.getSelectionModel().setSelectionMode(1);
    }

    @Override
    protected boolean selectionAccept(Node[] nodes) {
        if (nodes.length == 0) {
            return true;
        }
        Node parent = nodes[0].getParentNode();
        for (int i = 1; i < nodes.length; ++i) {
            if (nodes[i].getParentNode() == parent) continue;
            return false;
        }
        return true;
    }

    @Override
    protected void selectionChanged(Node[] nodes, ExplorerManager man) throws PropertyVetoException {
        if (nodes.length > 0) {
            man.setExploredContext(nodes[0]);
        }
        man.setSelectedNodes(nodes);
    }

    @Override
    protected void showPath(TreePath path) {
        this.tree.makeVisible(path);
        Rectangle rect = this.tree.getPathBounds(path);
        if (rect != null) {
            rect.width += rect.x;
            rect.x = 0;
            this.tree.scrollRectToVisible(rect);
        }
        this.tree.setSelectionPath(path);
    }

    @Override
    protected void showSelection(TreePath[] paths) {
        if (paths.length == 0) {
            this.tree.setSelectionPaths(new TreePath[0]);
        } else {
            this.tree.setSelectionPath(paths[0].getParentPath());
        }
    }

    @Override
    protected boolean useExploredContextMenu() {
        return true;
    }

    @Override
    protected NodeTreeModel createModel() {
        return new NodeContextModel();
    }

    static final class NodeContextModel
    extends NodeTreeModel {
        private int[] newIndices;
        private Object[] newChildren;

        NodeContextModel() {
        }

        @Override
        public Object getChild(Object parent, int index) {
            int superCnt = super.getChildCount(parent);
            int myCnt = 0;
            for (int i = 0; i < superCnt; ++i) {
                Object origChild = super.getChild(parent, i);
                Node n = Visualizer.findNode(origChild);
                if (n.isLeaf() || myCnt++ != index) continue;
                return origChild;
            }
            return null;
        }

        @Override
        public int getChildCount(Object parent) {
            int superCnt = super.getChildCount(parent);
            int myCnt = 0;
            for (int i = 0; i < superCnt; ++i) {
                Node n = Visualizer.findNode(super.getChild(parent, i));
                if (n.isLeaf()) continue;
                ++myCnt;
            }
            return myCnt;
        }

        @Override
        public int getIndexOfChild(Object parent, Object child) {
            int superCnt = super.getChildCount(parent);
            int myCnt = 0;
            for (int i = 0; i < superCnt; ++i) {
                Object origChild = super.getChild(parent, i);
                if (child.equals(origChild)) {
                    return myCnt;
                }
                Node n = Visualizer.findNode(origChild);
                if (n.isLeaf()) continue;
                ++myCnt;
            }
            return -1;
        }

        @Override
        public boolean isLeaf(Object node) {
            return false;
        }

        private boolean filterEvent(Object[] path, int[] childIndices, Object[] children) {
            int i;
            if (path.length == 1 && path[0] == this.root && childIndices == null) {
                return true;
            }
            assert (childIndices != null && children != null);
            assert (children.length == childIndices.length);
            assert (this.newChildren == null);
            assert (this.newIndices == null);
            assert (path.length > 0);
            VisualizerNode parent = (VisualizerNode)path[path.length - 1];
            int[] filter = new int[childIndices.length];
            int accepted = 0;
            for (i = 0; i < childIndices.length; ++i) {
                VisualizerNode n = (VisualizerNode)children[i];
                if (n.isLeaf()) continue;
                filter[accepted++] = i;
            }
            if (accepted == 0) {
                return false;
            }
            this.newIndices = new int[accepted];
            this.newChildren = new Object[accepted];
            for (i = 0; i < accepted; ++i) {
                this.newChildren[i] = children[filter[i]];
                this.newIndices[i] = this.getIndexOfChild(parent, this.newChildren[i]);
            }
            return true;
        }

        private boolean removalEvent(Object[] path, int[] childIndices, Object[] children) {
            assert (childIndices != null && children != null);
            assert (children.length == childIndices.length);
            assert (this.newChildren == null);
            assert (this.newIndices == null);
            assert (path.length > 0);
            VisualizerNode parent = (VisualizerNode)path[path.length - 1];
            int[] filter = new int[childIndices.length];
            int accepted = 0;
            for (int i = 0; i < childIndices.length; ++i) {
                VisualizerNode n = (VisualizerNode)children[i];
                if (n.isLeaf()) continue;
                filter[accepted++] = i;
            }
            if (accepted == 0) {
                return false;
            }
            this.newIndices = new int[accepted];
            this.newChildren = new Object[accepted];
            int size = this.getChildCount(parent);
            int index = 0;
            int myPos = 0;
            int actualI = 0;
            int i2 = 0;
            int pos = 0;
            while (pos < accepted) {
                VisualizerNode n;
                if (childIndices[index] <= i2) {
                    n = (VisualizerNode)children[index];
                    if (!n.isLeaf()) {
                        this.newIndices[pos] = myPos++;
                        this.newChildren[pos] = n;
                        ++pos;
                    }
                    ++index;
                } else if ((n = (VisualizerNode)this.getChild(parent, actualI++)) != null && !n.isLeaf()) {
                    ++myPos;
                }
                ++i2;
            }
            return true;
        }

        @Override
        protected void fireTreeNodesRemoved(Object source, Object[] path, int[] childIndices, Object[] children) {
            ContextTreeView.LOG.fine("fireTreeNodesRemoved");
            if (!this.removalEvent(path, childIndices, children)) {
                ContextTreeView.LOG.fine("fireTreeNodesRemoved - exit");
                return;
            }
            super.fireTreeNodesRemoved(source, path, this.newIndices, this.newChildren);
            this.newIndices = null;
            this.newChildren = null;
            ContextTreeView.LOG.fine("fireTreeNodesRemoved - end");
        }

        @Override
        void nodesWereInsertedInternal(VisualizerEvent ev) {
            VisualizerNode node = ev.getVisualizer();
            int[] childIndices = ev.getArray();
            Object[] path = this.getPathToRoot(node);
            this.fireTreeNodesInserted(this, path, childIndices, NodeTreeModel.computeChildren(ev));
        }

        @Override
        protected void fireTreeNodesInserted(Object source, Object[] path, int[] childIndices, Object[] children) {
            ContextTreeView.LOG.fine("fireTreeNodesInserted");
            if (!this.filterEvent(path, childIndices, children)) {
                ContextTreeView.LOG.fine("fireTreeNodesInserted - exit");
                return;
            }
            super.fireTreeNodesInserted(source, path, this.newIndices, this.newChildren);
            this.newIndices = null;
            this.newChildren = null;
            ContextTreeView.LOG.fine("fireTreeNodesInserted - end");
        }

        @Override
        protected void fireTreeNodesChanged(Object source, Object[] path, int[] childIndices, Object[] children) {
            ContextTreeView.LOG.fine("fireTreeNodesChanged");
            if (!this.filterEvent(path, childIndices, children)) {
                ContextTreeView.LOG.fine("fireTreeNodesChanged - exit");
                return;
            }
            super.fireTreeNodesChanged(source, path, this.newIndices, this.newChildren);
            this.newIndices = null;
            this.newChildren = null;
            ContextTreeView.LOG.fine("fireTreeNodesChanged - end");
        }
    }

}

