/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.Outline
 *  org.openide.ErrorManager
 *  org.openide.nodes.Children
 *  org.openide.nodes.Index
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.datatransfer.PasteType
 */
package org.openide.explorer.view;

import java.awt.Component;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyVetoException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.EventListener;
import java.util.TreeSet;
import javax.swing.JRootPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.tree.AbstractLayoutCache;
import javax.swing.tree.TreePath;
import org.netbeans.swing.outline.Outline;
import org.openide.ErrorManager;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.DragDropUtilities;
import org.openide.explorer.view.DropGlassPane;
import org.openide.explorer.view.ExplorerDnDManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Children;
import org.openide.nodes.Index;
import org.openide.nodes.Node;
import org.openide.util.datatransfer.PasteType;

final class OutlineViewDropSupport
implements DropTargetListener,
Runnable {
    protected static final int FUSSY_POINTING = 3;
    private static final int DELAY_TIME_FOR_EXPAND = 1000;
    boolean active = false;
    boolean dropTargetPopupAllowed;
    DropTarget dropTarget;
    private DropTarget outerDropTarget;
    Rectangle lastNodeArea;
    private int upperNodeIdx = -1;
    private int lowerNodeIdx = -1;
    private int dropIndex = -1;
    Timer timer;
    DropGlassPane dropPane;
    private int pointAt = 0;
    protected OutlineView view;
    protected JTable table;
    private static ErrorManager err = ErrorManager.getDefault().getInstance(OutlineViewDropSupport.class.getName());
    private static boolean LOGABLE = err.isLoggable(1);

    public OutlineViewDropSupport(OutlineView view, JTable table, boolean dropTargetPopupAllowed) {
        this.view = view;
        this.table = table;
        this.dropTargetPopupAllowed = dropTargetPopupAllowed;
    }

    public void setDropTargetPopupAllowed(boolean value) {
        this.dropTargetPopupAllowed = value;
    }

    public boolean isDropTargetPopupAllowed() {
        return this.dropTargetPopupAllowed;
    }

    @Override
    public void dragEnter(DropTargetDragEvent dtde) {
        OutlineViewDropSupport.log("dragEnter " + dtde);
        this.checkStoredGlassPane();
        this.dropIndex = -1;
        this.doDragOver(dtde);
    }

    @Override
    public void dragOver(DropTargetDragEvent dtde) {
        OutlineViewDropSupport.log("dragOver " + dtde);
        this.checkStoredGlassPane();
        this.dropIndex = -1;
        this.doDragOver(dtde);
    }

    private void checkStoredGlassPane() {
        if (!DropGlassPane.isOriginalPaneStored() || this.dropPane == null) {
            if (DropGlassPane.isOriginalPaneStored()) {
                DropGlassPane.putBackOriginal();
            }
            Component comp = this.table.getRootPane().getGlassPane();
            DropGlassPane.setOriginalPane(this.table, comp, comp.isVisible());
            this.dropPane = DropGlassPane.getDefault(this.table);
            this.table.getRootPane().setGlassPane(this.dropPane);
            this.dropPane.revalidate();
            this.dropPane.setVisible(true);
            OutlineViewDropSupport.log("dropPane was set");
        }
    }

    private void doDragOver(DropTargetDragEvent dtde) {
        TreePath path;
        TreePath parentPath;
        ExplorerDnDManager.getDefault().setMaybeExternalDragAndDrop(true);
        int dropAction = dtde.getDropAction();
        int allowedDropActions = this.view.getAllowedDropActions(dtde.getTransferable());
        dropAction = ExplorerDnDManager.getDefault().getAdjustedDropAction(dropAction, allowedDropActions);
        Point p = dtde.getLocation();
        int row = this.view.getOutline().rowAtPoint(p);
        int column = this.view.getOutline().columnAtPoint(p);
        OutlineViewDropSupport.log("doDragOver row == " + row + " column == " + column);
        Node dropNode = null;
        if (row == -1) {
            this.dropIndex = -1;
            dropNode = this.view.manager.getRootContext();
            if (this.canDrop(dropNode, dropAction, dtde.getTransferable())) {
                dtde.acceptDrag(dropAction);
            } else {
                dtde.rejectDrag();
            }
            this.removeDropLine();
            return;
        }
        dropNode = this.getNodeForDrop(p);
        if (LOGABLE) {
            OutlineViewDropSupport.log("doDragOver dropNode == " + (Object)dropNode);
        }
        if (dropNode == null) {
            this.dropIndex = -1;
            dtde.rejectDrag();
            this.removeDropLine();
            return;
        }
        boolean isParentNodeDrop = false;
        Rectangle nodeArea = this.table.getCellRect(row, column, false);
        OutlineViewDropSupport.log("nodeArea == " + nodeArea);
        if (nodeArea != null) {
            this.pointAt = 0;
            if (p.y <= nodeArea.y + 3) {
                if (row != 0) {
                    this.pointAt = -1;
                    if (dropNode.getParentNode() != null) {
                        OutlineViewDropSupport.log("dropNode is parent 1");
                        dropNode = dropNode.getParentNode();
                        isParentNodeDrop = true;
                    }
                }
            } else if (p.y >= nodeArea.y + nodeArea.height - 3) {
                TreePath tp = this.view.getOutline().getLayoutCache().getPathForRow(this.view.getOutline().convertRowIndexToModel(row));
                if (LOGABLE) {
                    OutlineViewDropSupport.log("tp == " + tp);
                }
                if (!this.view.getOutline().getLayoutCache().isExpanded(tp)) {
                    OutlineViewDropSupport.log("tree path is not expanded");
                    this.pointAt = 1;
                    if (dropNode.getParentNode() != null) {
                        OutlineViewDropSupport.log("dropNode is parent 2");
                        dropNode = dropNode.getParentNode();
                        isParentNodeDrop = true;
                    }
                }
            }
        }
        Index indexCookie = (Index)dropNode.getCookie(Index.class);
        OutlineViewDropSupport.log("indexCookie == " + (Object)indexCookie);
        if (indexCookie != null) {
            if (this.pointAt == -1) {
                this.lowerNodeIdx = indexCookie.indexOf(this.getNodeForDrop(p));
                this.upperNodeIdx = this.lowerNodeIdx - 1;
                this.dropIndex = this.lowerNodeIdx;
            } else if (this.pointAt == 1) {
                this.upperNodeIdx = indexCookie.indexOf(this.getNodeForDrop(p));
                this.dropIndex = this.lowerNodeIdx = this.upperNodeIdx + 1;
            } else {
                this.dropIndex = indexCookie.indexOf(this.getNodeForDrop(p));
            }
        } else if (isParentNodeDrop && null != (path = this.view.getOutline().getLayoutCache().getPathForRow(this.view.getOutline().convertRowIndexToModel(row))) && null != (parentPath = path.getParentPath())) {
            int parentRow = this.view.getOutline().getLayoutCache().getRowForPath(parentPath);
            this.dropIndex = row - parentRow;
        }
        path = this.view.getOutline().getLayoutCache().getPathForRow(this.view.getOutline().convertRowIndexToModel(row));
        boolean expanded = this.view.getOutline().getLayoutCache().isExpanded(path);
        if (!(this.timer != null && this.timer.isRunning() || dropNode == null || dropNode.isLeaf() || expanded)) {
            Node cn = dropNode;
            this.removeTimer();
            this.timer = new Timer(1000, new ActionListener(){

                @Override
                public final void actionPerformed(ActionEvent e) {
                    if (LOGABLE) {
                        OutlineViewDropSupport.log("should expand " + path);
                    }
                    OutlineViewDropSupport.this.view.getOutline().expandPath(path);
                }
            });
            this.timer.setRepeats(false);
            this.timer.start();
        }
        if (this.pointAt == 0) {
            this.dropPane.setDropLine(null);
        } else if (this.pointAt == -1) {
            Line2D.Double line = new Line2D.Double(0.0, nodeArea.y, this.table.getWidth(), nodeArea.y);
            this.convertBoundsAndSetDropLine(line);
            Rectangle lineArea = new Rectangle(0, nodeArea.y - 5, this.table.getWidth(), 10);
            nodeArea = (Rectangle)nodeArea.createUnion(lineArea);
        } else {
            Line2D.Double line = new Line2D.Double(0.0, nodeArea.y + nodeArea.height, this.table.getWidth(), nodeArea.y + nodeArea.height);
            this.convertBoundsAndSetDropLine(line);
            Rectangle lineArea = new Rectangle(0, nodeArea.y + nodeArea.height - 5, this.table.getWidth(), 10);
            nodeArea = (Rectangle)nodeArea.createUnion(lineArea);
        }
        if (this.lastNodeArea != null && !this.lastNodeArea.equals(nodeArea)) {
            this.repaint(this.lastNodeArea);
        }
        if (!nodeArea.equals(this.lastNodeArea)) {
            this.repaint(nodeArea);
            this.lastNodeArea = nodeArea;
            this.removeTimer();
        }
        if (this.canDrop(dropNode, dropAction, dtde.getTransferable())) {
            dtde.acceptDrag(dropAction);
        } else if (this.canReorder(dropNode, ExplorerDnDManager.getDefault().getDraggedNodes())) {
            dtde.acceptDrag(dropAction);
        } else {
            dtde.rejectDrag();
        }
    }

    private void repaint(Rectangle r) {
        this.view.repaint(r.x - 5, r.y - 5, r.width + 10, r.height + 10);
    }

    private void convertBoundsAndSetDropLine(Line2D line) {
        int x1 = (int)line.getX1();
        int x2 = (int)line.getX2();
        int y1 = (int)line.getY1();
        int y2 = (int)line.getY2();
        Point p1 = SwingUtilities.convertPoint(this.table, x1, y1, this.table.getRootPane());
        Point p2 = SwingUtilities.convertPoint(this.table, x2, y2, this.table.getRootPane());
        line.setLine(p1, p2);
        this.dropPane.setDropLine(line);
    }

    private void removeTimer() {
        if (this.timer != null) {
            ActionListener[] l = (ActionListener[])this.timer.getListeners(ActionListener.class);
            for (int i = 0; i < l.length; ++i) {
                this.timer.removeActionListener(l[i]);
            }
            this.timer.stop();
            this.timer = null;
        }
    }

    @Override
    public void dropActionChanged(DropTargetDragEvent dtde) {
        Node[] nodes = ExplorerDnDManager.getDefault().getDraggedNodes();
        if (null == nodes) {
            return;
        }
        int dropAction = ExplorerDnDManager.getDefault().getAdjustedDropAction(dtde.getDropAction(), this.view.getAllowedDropActions(dtde.getTransferable()));
        for (int i = 0; i < nodes.length; ++i) {
            if ((this.view.getAllowedDropActions(dtde.getTransferable()) & dropAction) != 0 && DragDropUtilities.checkNodeForAction(nodes[i], dropAction)) continue;
            dtde.rejectDrag();
            return;
        }
    }

    @Override
    public void dragExit(DropTargetEvent dte) {
        ExplorerDnDManager.getDefault().setMaybeExternalDragAndDrop(false);
        this.dropIndex = -1;
        this.stopDragging();
    }

    private void removeDropLine() {
        this.dropPane.setDropLine(null);
        if (this.lastNodeArea != null) {
            this.repaint(this.lastNodeArea);
            this.lastNodeArea = null;
        }
    }

    private void stopDragging() {
        this.removeDropLine();
        this.removeTimer();
        if (DropGlassPane.isOriginalPaneStored()) {
            DropGlassPane.putBackOriginal();
        }
    }

    private Node getNodeForDrop(Point p) {
        int row = this.view.getOutline().rowAtPoint(p);
        return this.view.getNodeFromRow(row);
    }

    private boolean canReorder(Node folder, Node[] dragNodes) {
        if ((ExplorerDnDManager.getDefault().getNodeAllowedActions() & 2) == 0) {
            OutlineViewDropSupport.log("canReorder returning false 1");
            return false;
        }
        if (folder == null || dragNodes == null || dragNodes.length == 0) {
            OutlineViewDropSupport.log("canReorder returning false 2");
            return false;
        }
        Index ic = (Index)folder.getCookie(Index.class);
        if (ic == null) {
            OutlineViewDropSupport.log("canReorder returning false 3");
            return false;
        }
        for (int i = 0; i < dragNodes.length; ++i) {
            if (dragNodes[i] == null) {
                OutlineViewDropSupport.log("canReorder returning false 4");
                return false;
            }
            if (dragNodes[i].getParentNode() == null) {
                OutlineViewDropSupport.log("canReorder returning false 5");
                return false;
            }
            if (dragNodes[i].getParentNode().equals((Object)folder)) continue;
            OutlineViewDropSupport.log("canReorder returning false 6");
            return false;
        }
        OutlineViewDropSupport.log("canReorder returning true");
        return true;
    }

    private void performReorder(Node folder, Node[] dragNodes, int lNode, int uNode) {
        block11 : {
            try {
                int i;
                Index indexCookie = (Index)folder.getCookie(Index.class);
                OutlineViewDropSupport.log("performReorder indexCookie == " + (Object)indexCookie);
                if (indexCookie == null) break block11;
                int[] perm = new int[indexCookie.getNodesCount()];
                int[] indexes = new int[dragNodes.length];
                int indexesLength = 0;
                for (int i2 = 0; i2 < dragNodes.length; ++i2) {
                    int idx = indexCookie.indexOf(dragNodes[i2]);
                    if (idx < 0 || idx >= perm.length) continue;
                    indexes[indexesLength++] = idx;
                }
                Arrays.sort(indexes);
                if (lNode < 0 || uNode >= perm.length || indexesLength == 0) {
                    return;
                }
                int k = 0;
                for (i = 0; i < perm.length; ++i) {
                    int j;
                    if (i <= uNode) {
                        if (!this.containsNumber(indexes, indexesLength, i)) {
                            perm[i] = k++;
                        }
                        if (i != uNode) continue;
                        for (j = 0; j < indexesLength; ++j) {
                            if (indexes[j] > uNode) continue;
                            perm[indexes[j]] = k++;
                        }
                        continue;
                    }
                    if (i == lNode) {
                        for (j = 0; j < indexesLength; ++j) {
                            if (indexes[j] < lNode) continue;
                            perm[indexes[j]] = k++;
                        }
                    }
                    if (this.containsNumber(indexes, indexesLength, i)) continue;
                    perm[i] = k++;
                }
                for (i = 0; i < perm.length; ++i) {
                    if (perm[i] == i) continue;
                    indexCookie.reorder(perm);
                    break;
                }
            }
            catch (Exception e) {
                ErrorManager.getDefault().notify(1, (Throwable)e);
            }
        }
    }

    private boolean containsNumber(int[] arr, int arrLength, int n) {
        for (int i = 0; i < arrLength; ++i) {
            if (arr[i] != n) continue;
            return true;
        }
        return false;
    }

    private Node[] findDropedNodes(Node folder, Node[] dragNodes) {
        if (folder == null || dragNodes.length == 0) {
            return null;
        }
        Node[] dropNodes = new Node[dragNodes.length];
        Children children = folder.getChildren();
        for (int i = 0; i < dragNodes.length; ++i) {
            dropNodes[i] = children.findChild(dragNodes[i].getName());
        }
        return dropNodes;
    }

    private boolean canDrop(Node n, int dropAction, Transferable dndEventTransferable) {
        Node[] nodes;
        if (LOGABLE) {
            OutlineViewDropSupport.log("canDrop " + (Object)n);
        }
        if (n == null) {
            return false;
        }
        if ((this.view.getAllowedDropActions(dndEventTransferable) & dropAction) == 0) {
            return false;
        }
        if ((2 & dropAction) != 0 && (nodes = ExplorerDnDManager.getDefault().getDraggedNodes()) != null) {
            for (int i = 0; i < nodes.length; ++i) {
                if (!n.equals((Object)nodes[i].getParentNode())) continue;
                return false;
            }
        }
        Transferable trans = ExplorerDnDManager.getDefault().getDraggedTransferable((2 & dropAction) != 0);
        if (LOGABLE) {
            OutlineViewDropSupport.log("transferable == " + trans);
        }
        if (trans == null && null == (trans = dndEventTransferable)) {
            return false;
        }
        PasteType pt = DragDropUtilities.getDropType(n, trans, dropAction, this.dropIndex);
        return pt != null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void drop(DropTargetDropEvent dtde) {
        boolean dropResult = true;
        try {
            OutlineViewDropSupport.log("drop");
            this.stopDragging();
            Node dropNode = this.getNodeForDrop(dtde.getLocation());
            if (LOGABLE) {
                OutlineViewDropSupport.log("drop dropNode == " + (Object)dropNode);
            }
            if (dropNode == null) {
                dropNode = this.view.manager.getRootContext();
            } else if (this.pointAt != 0) {
                dropNode = dropNode.getParentNode();
            }
            Node[] dragNodes = ExplorerDnDManager.getDefault().getDraggedNodes();
            int dropAction = ExplorerDnDManager.getDefault().getAdjustedDropAction(dtde.getDropAction(), this.view.getAllowedDropActions(dtde.getTransferable()));
            if (!this.canDrop(dropNode, dropAction, dtde.getTransferable())) {
                if (this.canReorder(dropNode, dragNodes)) {
                    this.performReorder(dropNode, dragNodes, this.lowerNodeIdx, this.upperNodeIdx);
                    dtde.acceptDrop(dropAction);
                } else {
                    dtde.rejectDrop();
                }
                return;
            }
            dtde.acceptDrop(dropAction);
            if (1073741824 == dropAction) {
                int i;
                PasteType[] ptCut = new PasteType[]{};
                PasteType[] ptCopy = new PasteType[]{};
                if ((ExplorerDnDManager.getDefault().getNodeAllowedActions() & 2) != 0) {
                    ptCut = DragDropUtilities.getPasteTypes(dropNode, ExplorerDnDManager.getDefault().getDraggedTransferable(true));
                }
                if ((ExplorerDnDManager.getDefault().getNodeAllowedActions() & 1) != 0) {
                    ptCopy = DragDropUtilities.getPasteTypes(dropNode, ExplorerDnDManager.getDefault().getDraggedTransferable(false));
                }
                TreeSet<PasteType> setPasteTypes = new TreeSet<PasteType>(new Comparator<PasteType>(){

                    @Override
                    public int compare(PasteType obj1, PasteType obj2) {
                        int res = obj1.getName().compareTo(obj2.getName());
                        OutlineViewDropSupport.log("res1: " + res);
                        if (res == 0) {
                            res = System.identityHashCode((Object)obj1) - System.identityHashCode((Object)obj2);
                        }
                        OutlineViewDropSupport.log("res2: " + res);
                        return res;
                    }
                });
                for (i = 0; i < ptCut.length; ++i) {
                    if (LOGABLE) {
                        OutlineViewDropSupport.log(ptCut[i].getName() + ", " + System.identityHashCode((Object)ptCut[i]));
                    }
                    setPasteTypes.add(ptCut[i]);
                }
                for (i = 0; i < ptCopy.length; ++i) {
                    if (LOGABLE) {
                        OutlineViewDropSupport.log(ptCopy[i].getName() + ", " + System.identityHashCode((Object)ptCopy[i]));
                    }
                    setPasteTypes.add(ptCopy[i]);
                }
                DragDropUtilities.createDropFinishPopup(setPasteTypes).show(this.table, Math.max(dtde.getLocation().x - 5, 0), Math.max(dtde.getLocation().y - 5, 0));
                if (this.canReorder(dropNode, dragNodes)) {
                    final Node tempDropNode = dropNode;
                    final int tmpUpper = this.upperNodeIdx;
                    final int tmpLower = this.lowerNodeIdx;
                    final Node[] tempDragNodes = dragNodes;
                    DragDropUtilities.setPostDropRun(new Runnable(){

                        @Override
                        public void run() {
                            OutlineViewDropSupport.this.performReorder(tempDropNode, OutlineViewDropSupport.this.findDropedNodes(tempDropNode, tempDragNodes), tmpLower, tmpUpper);
                        }
                    });
                }
            } else {
                Transferable t = ExplorerDnDManager.getDefault().getDraggedTransferable((2 & dropAction) != 0);
                if (null == t) {
                    t = dtde.getTransferable();
                }
                PasteType pt = DragDropUtilities.getDropType(dropNode, t, dropAction, this.dropIndex);
                final Node[] diffNodes = DragDropUtilities.performPaste(pt, dropNode);
                if (null != ExplorerDnDManager.getDefault().getDraggedTransferable((2 & dropAction) != 0)) {
                    ExplorerDnDManager.getDefault().setDraggedNodes(diffNodes);
                }
                if (this.canReorder(dropNode, diffNodes) && this.lowerNodeIdx >= 0 && this.upperNodeIdx >= 0) {
                    this.performReorder(dropNode, diffNodes, this.lowerNodeIdx, this.upperNodeIdx);
                }
                if (diffNodes.length > 0) {
                    this.view.expandNode(dropNode);
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            try {
                                OutlineViewDropSupport.this.view.manager.setSelectedNodes(diffNodes);
                            }
                            catch (PropertyVetoException ex) {
                                // empty catch block
                            }
                        }
                    });
                }
            }
        }
        finally {
            dtde.dropComplete(dropResult);
        }
    }

    public void activate(boolean active) {
        if (this.active == active) {
            return;
        }
        this.active = active;
        if (GraphicsEnvironment.isHeadless()) {
            return;
        }
        this.getDropTarget().setActive(active);
        if (null == this.outerDropTarget) {
            this.outerDropTarget = new DropTarget(this.view.getViewport(), this.view.getAllowedDropActions(), this, false);
        }
        this.outerDropTarget.setActive(active);
    }

    @Override
    public void run() {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(this);
            return;
        }
        DragDropUtilities.dropNotSuccesfull();
    }

    DropTarget getDropTarget() {
        if (this.dropTarget == null) {
            this.dropTarget = new DropTarget(this.table, this.view.getAllowedDropActions(), this, false);
        }
        return this.dropTarget;
    }

    private static void log(String s) {
        if (LOGABLE) {
            err.log(s);
        }
    }

}

