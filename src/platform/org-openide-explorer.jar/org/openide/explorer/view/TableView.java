/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.etable.ETable
 *  org.netbeans.swing.etable.ETableColumn
 *  org.netbeans.swing.etable.TableColumnSelector
 *  org.openide.awt.Mnemonics
 *  org.openide.awt.MouseUtils
 *  org.openide.awt.MouseUtils$PopupMouseAdapter
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.Lookup
 *  org.openide.util.WeakListeners
 */
package org.openide.explorer.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.util.Arrays;
import java.util.EventObject;
import java.util.Properties;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.MenuElement;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.netbeans.swing.etable.ETable;
import org.netbeans.swing.etable.ETableColumn;
import org.netbeans.swing.etable.TableColumnSelector;
import org.openide.awt.Mnemonics;
import org.openide.awt.MouseUtils;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.NodePopupFactory;
import org.openide.explorer.view.NodeTableModel;
import org.openide.explorer.view.PropertiesRowModel;
import org.openide.explorer.view.SheetCell;
import org.openide.explorer.view.TableViewDragSupport;
import org.openide.explorer.view.TableViewDropSupport;
import org.openide.explorer.view.Visualizer;
import org.openide.explorer.view.VisualizerNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.WeakListeners;

public class TableView
extends JScrollPane {
    private ETable table = new TableViewETable();
    private ExplorerManager manager;
    transient PopupAdapter popupListener;
    transient TableSelectionListener managerListener = null;
    transient PropertyChangeListener wlpc;
    transient VetoableChangeListener wlvc;
    private NodePopupFactory popupFactory;
    private transient boolean dragActive = true;
    private transient boolean dropActive = true;
    transient TableViewDragSupport dragSupport;
    transient TableViewDropSupport dropSupport;
    transient boolean dropTargetPopupAllowed = true;
    private transient int allowedDragActions = 1073741827;
    private transient int allowedDropActions = 1073741827;

    public TableView() {
        this(new NodeTableModel());
    }

    public TableView(NodeTableModel ntm) {
        this.table.setModel((TableModel)ntm);
        SheetCell.TableSheetCell tableCell = new SheetCell.TableSheetCell(ntm, this.table);
        this.table.setDefaultRenderer(Node.Property.class, (TableCellRenderer)tableCell);
        this.table.setDefaultEditor(Node.Property.class, (TableCellEditor)tableCell);
        this.setViewportView((Component)this.table);
        this.setPopupAllowed(true);
        this.setRequestFocusEnabled(false);
        this.table.setRequestFocusEnabled(true);
        this.getActionMap().put("org.openide.actions.PopupAction", new PopupAction());
        this.popupFactory = new NodePopupFactory();
        Color c = UIManager.getColor("Table.background1");
        if (c == null) {
            c = UIManager.getColor("Table.background");
        }
        if (c != null) {
            this.getViewport().setBackground(c);
        }
        this.setDragSource(true);
        this.setDropTarget(true);
        TableColumnSelector tcs = (TableColumnSelector)Lookup.getDefault().lookup(TableColumnSelector.class);
        if (tcs != null) {
            this.table.setColumnSelector(tcs);
        }
    }

    @Override
    public void requestFocus() {
        this.table.requestFocus();
    }

    @Override
    public boolean requestFocusInWindow() {
        return this.table.requestFocusInWindow();
    }

    public ETable getTable() {
        return this.table;
    }

    public boolean isPopupAllowed() {
        return this.popupListener != null;
    }

    public void setPopupAllowed(boolean value) {
        if (this.popupListener == null && value) {
            this.popupListener = new PopupAdapter();
            this.table.addMouseListener((MouseListener)((Object)this.popupListener));
            this.addMouseListener((MouseListener)((Object)this.popupListener));
            return;
        }
        if (this.popupListener != null && !value) {
            this.table.removeMouseListener((MouseListener)((Object)this.popupListener));
            this.removeMouseListener((MouseListener)((Object)this.popupListener));
            this.popupListener = null;
            return;
        }
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.lookupExplorerManager();
    }

    public void readSettings(Properties p, String propertyPrefix) {
        this.table.readSettings(p, propertyPrefix);
    }

    public void writeSettings(Properties p, String propertyPrefix) {
        this.table.writeSettings(p, propertyPrefix);
    }

    public void setNodePopupFactory(NodePopupFactory newFactory) {
        this.popupFactory = newFactory;
    }

    public NodePopupFactory getNodePopupFactory() {
        return this.popupFactory;
    }

    private void lookupExplorerManager() {
        ExplorerManager newManager;
        if (this.managerListener == null) {
            this.managerListener = new TableSelectionListener();
        }
        if ((newManager = ExplorerManager.find(this)) != this.manager) {
            if (this.manager != null) {
                this.manager.removeVetoableChangeListener(this.wlvc);
                this.manager.removePropertyChangeListener(this.wlpc);
            }
            this.manager = newManager;
            this.wlvc = WeakListeners.vetoableChange((VetoableChangeListener)this.managerListener, (Object)this.manager);
            this.manager.addVetoableChangeListener(this.wlvc);
            this.wlpc = WeakListeners.propertyChange((PropertyChangeListener)this.managerListener, (Object)this.manager);
            this.manager.addPropertyChangeListener(this.wlpc);
            this.synchronizeRootContext();
            this.synchronizeSelectedNodes();
        }
        this.table.getSelectionModel().removeListSelectionListener(this.managerListener);
        this.table.getSelectionModel().addListSelectionListener(this.managerListener);
    }

    final void synchronizeRootContext() {
        NodeTableModel ntm = (NodeTableModel)this.table.getModel();
        ntm.setNodes(this.manager.getRootContext().getChildren().getNodes());
    }

    final void synchronizeSelectedNodes() {
        Node[] arr = this.manager.getSelectedNodes();
        this.table.getSelectionModel().clearSelection();
        NodeTableModel ntm = (NodeTableModel)this.table.getModel();
        int size = ntm.getRowCount();
        int firstSelection = -1;
        for (int i = 0; i < size; ++i) {
            Node n = this.getNodeFromRow(i);
            for (int j = 0; j < arr.length; ++j) {
                if (!n.equals((Object)arr[j])) continue;
                this.table.getSelectionModel().addSelectionInterval(i, i);
                if (firstSelection != -1) continue;
                firstSelection = i;
            }
        }
        if (firstSelection >= 0) {
            Rectangle rect = this.table.getCellRect(firstSelection, 0, true);
            if (!this.getViewport().getViewRect().contains(rect.getLocation())) {
                rect.height = Math.max(rect.height, this.getHeight() - 30);
                this.table.scrollRectToVisible(rect);
            }
        }
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.table.getSelectionModel().removeListSelectionListener(this.managerListener);
    }

    private void showPopup(int xpos, int ypos, final JPopupMenu popup) {
        if (popup != null && popup.getSubElements().length > 0) {
            PopupMenuListener p = new PopupMenuListener(){

                @Override
                public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                }

                @Override
                public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                    popup.removePopupMenuListener(this);
                    TableView.this.table.requestFocus();
                }

                @Override
                public void popupMenuCanceled(PopupMenuEvent e) {
                }
            };
            popup.addPopupMenuListener(p);
            popup.show(this, xpos, ypos);
        }
    }

    private JPopupMenu createPopup(Point p) {
        int[] selRows = this.table.getSelectedRows();
        Node[] arr = new Node[selRows.length];
        for (int i = 0; i < selRows.length; ++i) {
            arr[i] = this.getNodeFromRow(selRows[i]);
        }
        if (arr.length == 0) {
            arr = new Node[]{this.manager.getRootContext()};
        }
        p = SwingUtilities.convertPoint(this, p, (Component)this.table);
        int column = this.table.columnAtPoint(p);
        int row = this.table.rowAtPoint(p);
        return this.popupFactory.createPopupMenu(row, column, arr, (Component)this.table);
    }

    Node getNodeFromRow(int rowIndex) {
        int row = this.table.convertRowIndexToModel(rowIndex);
        TableModel tm = this.table.getModel();
        if (tm instanceof NodeTableModel) {
            NodeTableModel ntm = (NodeTableModel)tm;
            return ntm.nodeForRow(row);
        }
        return null;
    }

    private Point getPositionForPopup() {
        Rectangle rect;
        int i = this.table.getSelectionModel().getLeadSelectionIndex();
        if (i < 0) {
            return null;
        }
        int j = this.table.getColumnModel().getSelectionModel().getLeadSelectionIndex();
        if (j < 0) {
            j = 0;
        }
        if ((rect = this.table.getCellRect(i, j, true)) == null) {
            return null;
        }
        Point p = new Point(rect.x + rect.width / 3, rect.y + rect.height / 2);
        p = SwingUtilities.convertPoint((Component)this.table, p, this);
        return p;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void callSelectionChanged(Node[] nodes) {
        this.manager.removePropertyChangeListener(this.wlpc);
        this.manager.removeVetoableChangeListener(this.wlvc);
        try {
            this.manager.setSelectedNodes(nodes);
        }
        catch (PropertyVetoException e) {
            this.synchronizeSelectedNodes();
        }
        finally {
            this.manager.addPropertyChangeListener(this.wlpc);
            this.manager.addVetoableChangeListener(this.wlvc);
        }
    }

    private boolean isSelectionModeBroken(Node[] nodes) {
        if (nodes.length <= 1 || this.table.getSelectionModel().getSelectionMode() == 2) {
            return false;
        }
        if (this.table.getSelectionModel().getSelectionMode() == 0) {
            return true;
        }
        return false;
    }

    public boolean isDragSource() {
        return this.dragActive;
    }

    public void setDragSource(boolean state) {
        if (state && this.dragSupport == null) {
            this.dragSupport = new TableViewDragSupport(this, (JTable)this.table);
        }
        this.dragActive = state;
        if (this.dragSupport != null) {
            this.dragSupport.activate(this.dragActive);
        }
    }

    public boolean isDropTarget() {
        return this.dropActive;
    }

    public void setDropTarget(boolean state) {
        if (this.dropActive && this.dropSupport == null) {
            this.dropSupport = new TableViewDropSupport(this, (JTable)this.table, this.dropTargetPopupAllowed);
        }
        this.dropActive = state;
        if (this.dropSupport != null) {
            this.dropSupport.activate(this.dropActive);
        }
    }

    public int getAllowedDragActions() {
        return this.allowedDragActions;
    }

    public void setAllowedDragActions(int actions) {
        this.allowedDragActions = actions;
    }

    public int getAllowedDropActions() {
        return this.allowedDropActions;
    }

    public void setAllowedDropActions(int actions) {
        this.allowedDropActions = actions;
    }

    private static class TableViewETable
    extends ETable {
        protected TableColumn createColumn(int modelIndex) {
            return new TableViewETableColumn(modelIndex);
        }

        public Object transformValue(Object value) {
            if (value instanceof ETableColumn) {
                ETableColumn c = (ETableColumn)value;
                return c.getHeaderValue().toString();
            }
            if (value instanceof AbstractButton) {
                AbstractButton b = (AbstractButton)value;
                Mnemonics.setLocalizedText((AbstractButton)b, (String)b.getText());
                return b;
            }
            if (value instanceof VisualizerNode) {
                return Visualizer.findNode(value);
            }
            return PropertiesRowModel.getValueFromProperty(value);
        }

        public boolean editCellAt(int row, int column, EventObject e) {
            Object o = this.getValueAt(row, column);
            if (o instanceof Node.Property) {
                Node.Property p = (Node.Property)o;
                if (!p.canWrite()) {
                    return false;
                }
                if (p.getValueType() == Boolean.class || p.getValueType() == Boolean.TYPE) {
                    PropertiesRowModel.toggleBooleanProperty(p);
                    Rectangle r = this.getCellRect(row, column, true);
                    this.repaint(r.x, r.y, r.width, r.height);
                    return false;
                }
            }
            return super.editCellAt(row, column, e);
        }

        private class TableViewETableColumn
        extends ETableColumn {
            private String tooltip;

            public TableViewETableColumn(int index) {
                super(index, (ETable)TableViewETable.this);
            }

            public boolean isSortingAllowed() {
                TableModel model = TableViewETable.this.getModel();
                if (model instanceof NodeTableModel) {
                    NodeTableModel ntm = (NodeTableModel)model;
                    return ntm.isComparableColumn(this.getModelIndex());
                }
                return true;
            }

            final String getShortDescription(String defaultValue) {
                TableModel model = TableViewETable.this.getModel();
                if (model.getRowCount() <= 0) {
                    return null;
                }
                if (0 == this.getModelIndex()) {
                    return defaultValue;
                }
                if (model instanceof NodeTableModel) {
                    NodeTableModel ntm = (NodeTableModel)model;
                    Node.Property propertyForColumn = ntm.propertyForColumn(this.getModelIndex());
                    return propertyForColumn.getShortDescription();
                }
                return defaultValue;
            }

            protected TableCellRenderer createDefaultHeaderRenderer() {
                TableCellRenderer orig = super.createDefaultHeaderRenderer();
                TableViewHeaderRenderer ovohr = new TableViewHeaderRenderer(orig);
                return ovohr;
            }

            class TableViewHeaderRenderer
            implements TableCellRenderer {
                private TableCellRenderer orig;

                public TableViewHeaderRenderer(TableCellRenderer delegate) {
                    this.orig = delegate;
                }

                @Override
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                    Component oc = this.orig.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                    if (TableViewETableColumn.this.tooltip == null) {
                        TableViewETableColumn.this.tooltip = TableViewETableColumn.this.getShortDescription(value.toString());
                    }
                    if (TableViewETableColumn.this.tooltip != null && oc instanceof JComponent) {
                        JComponent jc = (JComponent)oc;
                        jc.setToolTipText(TableViewETableColumn.this.tooltip);
                    }
                    return oc;
                }
            }

        }

    }

    private class TableSelectionListener
    implements VetoableChangeListener,
    ListSelectionListener,
    PropertyChangeListener {
        private TableSelectionListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (TableView.this.manager == null) {
                return;
            }
            if (evt.getPropertyName().equals("rootContext")) {
                TableView.this.synchronizeRootContext();
            }
            if (evt.getPropertyName().equals("selectedNodes")) {
                TableView.this.synchronizeSelectedNodes();
            }
        }

        @Override
        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            int[] selectedRows = TableView.this.table.getSelectedRows();
            Node[] selectedNodes = new Node[selectedRows.length];
            for (int i = 0; i < selectedNodes.length; ++i) {
                selectedNodes[i] = TableView.this.getNodeFromRow(selectedRows[i]);
            }
            TableView.this.callSelectionChanged(selectedNodes);
        }

        @Override
        public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
            Node[] nodes;
            if (evt.getPropertyName().equals("selectedNodes") && TableView.this.isSelectionModeBroken(nodes = (Node[])evt.getNewValue())) {
                throw new PropertyVetoException("selection mode  broken by " + Arrays.asList(nodes), evt);
            }
        }
    }

    private class PopupAdapter
    extends MouseUtils.PopupMouseAdapter {
        PopupAdapter() {
        }

        protected void showPopup(MouseEvent e) {
            int selRow = TableView.this.table.rowAtPoint(e.getPoint());
            if (selRow != -1) {
                if (!TableView.this.table.getSelectionModel().isSelectedIndex(selRow)) {
                    TableView.this.table.getSelectionModel().clearSelection();
                    TableView.this.table.getSelectionModel().setSelectionInterval(selRow, selRow);
                }
            } else {
                TableView.this.table.getSelectionModel().clearSelection();
            }
            Point p = SwingUtilities.convertPoint(e.getComponent(), e.getPoint(), TableView.this);
            if (TableView.this.isPopupAllowed()) {
                JPopupMenu pop = TableView.this.createPopup(p);
                TableView.this.showPopup(p.x, p.y, pop);
                e.consume();
            }
        }
    }

    private class PopupAction
    extends AbstractAction
    implements Runnable {
        private PopupAction() {
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            SwingUtilities.invokeLater(this);
        }

        @Override
        public void run() {
            Point p = TableView.this.getPositionForPopup();
            if (p == null) {
                return;
            }
            if (TableView.this.isPopupAllowed()) {
                JPopupMenu pop = TableView.this.createPopup(p);
                TableView.this.showPopup(p.x, p.y, pop);
            }
        }
    }

}

