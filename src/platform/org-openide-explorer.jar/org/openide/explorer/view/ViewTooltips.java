/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 */
package org.openide.explorer.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.HierarchyBoundsListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.BoundedRangeModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.JViewport;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import org.openide.util.Lookup;
import org.openide.util.Utilities;

final class ViewTooltips
extends MouseAdapter
implements MouseMotionListener {
    private static final Object KEY = new Object();
    private int refcount = 0;
    private JComponent inner = null;
    private int row = -1;
    private Popup[] popups = new Popup[2];
    private ImgComp painter = new ImgComp();
    private Hider hider = null;

    private ViewTooltips() {
    }

    static synchronized void register(JComponent comp) {
        ViewTooltips vtt = (ViewTooltips)comp.getClientProperty(KEY);
        assert (vtt == null);
        vtt = new ViewTooltips();
        comp.putClientProperty(KEY, vtt);
        vtt.attachTo(comp);
    }

    static synchronized void unregister(JComponent comp) {
        ViewTooltips inst = (ViewTooltips)comp.getClientProperty(KEY);
        if (inst == null) {
            return;
        }
        int zero = inst.detachFrom(comp);
        assert (zero == 0);
        inst.hide();
        comp.putClientProperty(KEY, null);
    }

    private void attachTo(JComponent comp) {
        assert (comp instanceof JTree || comp instanceof JList);
        comp.addMouseListener(this);
        comp.addMouseMotionListener(this);
        ++this.refcount;
    }

    private int detachFrom(JComponent comp) {
        assert (comp instanceof JTree || comp instanceof JList);
        comp.removeMouseMotionListener(this);
        comp.removeMouseListener(this);
        return --this.refcount;
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        Point p = e.getPoint();
        JComponent comp = (JComponent)e.getSource();
        JScrollPane jsp = (JScrollPane)SwingUtilities.getAncestorOfClass(JScrollPane.class, comp);
        if (jsp != null) {
            p = SwingUtilities.convertPoint(comp, p, jsp);
            this.show(jsp, p);
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        this.hide();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        this.hide();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        this.hide();
    }

    void show(JScrollPane view, Point pt) {
        if (view.getViewport().getView() instanceof JTree) {
            this.showJTree(view, pt);
        } else if (view.getViewport().getView() instanceof JList) {
            this.showJList(view, pt);
        } else assert (false);
    }

    private void showJList(JScrollPane view, Point pt) {
        Point p;
        JList list = (JList)view.getViewport().getView();
        int row = list.locationToIndex(p = SwingUtilities.convertPoint(view, pt.x, pt.y, list));
        if (row == -1) {
            this.hide();
            return;
        }
        Rectangle bds = list.getCellBounds(row, row);
        ListCellRenderer ren = list.getCellRenderer();
        Dimension rendererSize = ren.getListCellRendererComponent(list, list.getModel().getElementAt(row), row, false, false).getPreferredSize();
        bds.width = rendererSize.width;
        if (bds == null || !bds.contains(p)) {
            this.hide();
            return;
        }
        if (this.setCompAndRow(list, row)) {
            Rectangle visible = this.getShowingRect(view);
            Rectangle[] rects = ViewTooltips.getRects(bds, visible);
            if (rects.length > 0) {
                this.ensureOldPopupsHidden();
                this.painter.configure(list.getModel().getElementAt(row), view, list, row);
                this.showPopups(rects, bds, visible, list, view);
            } else {
                this.hide();
            }
        }
    }

    private void showJTree(JScrollPane view, Point pt) {
        JTree tree = (JTree)view.getViewport().getView();
        Point p = SwingUtilities.convertPoint(view, pt.x, pt.y, tree);
        int row = tree.getClosestRowForLocation(p.x, p.y);
        TreePath path = tree.getClosestPathForLocation(p.x, p.y);
        Rectangle bds = tree.getPathBounds(path);
        if (bds == null || !bds.contains(p)) {
            this.hide();
            return;
        }
        if (this.setCompAndRow(tree, row)) {
            Rectangle visible = this.getShowingRect(view);
            Rectangle[] rects = ViewTooltips.getRects(bds, visible);
            if (rects.length > 0) {
                this.ensureOldPopupsHidden();
                this.painter.configure(path.getLastPathComponent(), view, tree, path, row);
                this.showPopups(rects, bds, visible, tree, view);
            } else {
                this.hide();
            }
        }
    }

    private boolean setCompAndRow(JComponent inner, int row) {
        boolean rowChanged = row != this.row;
        boolean compChanged = inner != this.inner;
        this.inner = inner;
        this.row = row;
        return rowChanged || compChanged;
    }

    void hide() {
        this.ensureOldPopupsHidden();
        if (this.painter != null) {
            this.painter.clear();
        }
        this.setHideComponent(null, null);
        this.inner = null;
        this.row = -1;
    }

    private void ensureOldPopupsHidden() {
        for (int i = 0; i < this.popups.length; ++i) {
            if (this.popups[i] == null) continue;
            this.popups[i].hide();
            this.popups[i] = null;
        }
    }

    private Rectangle getShowingRect(JScrollPane pane) {
        Insets ins1 = pane.getViewport().getInsets();
        Border inner = pane.getViewportBorder();
        Insets ins2 = inner != null ? inner.getBorderInsets(pane) : new Insets(0, 0, 0, 0);
        Insets ins3 = new Insets(0, 0, 0, 0);
        if (pane.getBorder() != null) {
            ins3 = pane.getBorder().getBorderInsets(pane);
        }
        Rectangle r = pane.getViewportBorderBounds();
        r.translate(- r.x, - r.y);
        r.width -= ins1.left + ins1.right;
        r.width -= ins2.left + ins2.right;
        r.height -= ins1.top + ins1.bottom;
        r.height -= ins2.top + ins2.bottom;
        r.x -= ins2.left;
        r.x -= ins3.left;
        Point p = pane.getViewport().getViewPosition();
        r.translate(p.x, p.y);
        r = SwingUtilities.convertRectangle(pane.getViewport(), r, pane);
        return r;
    }

    private static final Rectangle[] getRects(Rectangle bds, Rectangle vis) {
        Rectangle[] result;
        if (vis.contains(bds)) {
            result = new Rectangle[]{};
        } else if (bds.x < vis.x && bds.x + bds.width > vis.x + vis.width) {
            Rectangle a = new Rectangle(bds.x, bds.y, vis.x - bds.x, bds.height);
            Rectangle b = new Rectangle(vis.x + vis.width, bds.y, bds.x + bds.width - (vis.x + vis.width), bds.height);
            result = new Rectangle[]{a, b};
        } else {
            result = bds.x < vis.x ? new Rectangle[]{new Rectangle(bds.x, bds.y, vis.x - bds.x, bds.height)} : (bds.x + bds.width > vis.x + vis.width ? new Rectangle[]{new Rectangle(vis.x + vis.width, bds.y, bds.x + bds.width - (vis.x + vis.width), bds.height)} : new Rectangle[]{});
        }
        return result;
    }

    private void showPopups(Rectangle[] rects, Rectangle bds, Rectangle visible, JComponent comp, JScrollPane view) {
        boolean shown = false;
        for (int i = 0; i < rects.length; ++i) {
            Rectangle sect = rects[i];
            sect.translate(- bds.x, - bds.y);
            ImgComp part = this.painter.getPartial(sect, bds.x + rects[i].x < visible.x);
            Point pos = new Point(bds.x + rects[i].x, bds.y + rects[i].y);
            SwingUtilities.convertPointToScreen(pos, comp);
            if (comp instanceof JList) {
                --pos.y;
            }
            if (pos.x <= 0) continue;
            this.popups[i] = ViewTooltips.getPopupFactory().getPopup(view, part, pos.x, pos.y);
            this.popups[i].show();
            shown = true;
        }
        if (shown) {
            this.setHideComponent(comp, view);
        } else {
            this.setHideComponent(null, null);
        }
    }

    private static PopupFactory getPopupFactory() {
        if (Utilities.isMac()) {
            PopupFactory result = (PopupFactory)Lookup.getDefault().lookup(PopupFactory.class);
            return result == null ? PopupFactory.getSharedInstance() : result;
        }
        return PopupFactory.getSharedInstance();
    }

    private void setHideComponent(JComponent comp, JScrollPane parent) {
        if (this.hider != null && this.hider.isListeningTo(comp)) {
            return;
        }
        if (this.hider != null) {
            this.hider.detach();
        }
        this.hider = comp != null ? new Hider(comp, parent) : null;
    }

    private static final class Hider
    implements ChangeListener,
    PropertyChangeListener,
    TreeModelListener,
    TreeSelectionListener,
    HierarchyListener,
    HierarchyBoundsListener,
    ListSelectionListener,
    ListDataListener,
    ComponentListener {
        private final JTree tree;
        private JScrollPane pane;
        private final JList list;
        private boolean detached = false;

        public Hider(JComponent comp, JScrollPane pane) {
            if (comp instanceof JTree) {
                this.tree = (JTree)comp;
                this.list = null;
            } else {
                this.list = (JList)comp;
                this.tree = null;
            }
            assert (this.tree != null || this.list != null);
            this.pane = pane;
            this.attach();
        }

        private boolean isListeningTo(JComponent comp) {
            return !this.detached && (comp == this.list || comp == this.tree);
        }

        private void attach() {
            if (this.tree != null) {
                this.tree.getModel().addTreeModelListener(this);
                this.tree.getSelectionModel().addTreeSelectionListener(this);
                this.tree.addHierarchyBoundsListener(this);
                this.tree.addHierarchyListener(this);
                this.tree.addComponentListener(this);
            } else {
                this.list.getSelectionModel().addListSelectionListener(this);
                this.list.getModel().addListDataListener(this);
                this.list.addHierarchyBoundsListener(this);
                this.list.addHierarchyListener(this);
                this.list.addComponentListener(this);
            }
            this.pane.getHorizontalScrollBar().getModel().addChangeListener(this);
            this.pane.getVerticalScrollBar().getModel().addChangeListener(this);
            KeyboardFocusManager.getCurrentKeyboardFocusManager().addPropertyChangeListener(this);
        }

        private void detach() {
            KeyboardFocusManager.getCurrentKeyboardFocusManager().removePropertyChangeListener(this);
            if (this.tree != null) {
                this.tree.getSelectionModel().removeTreeSelectionListener(this);
                this.tree.getModel().removeTreeModelListener(this);
                this.tree.removeHierarchyBoundsListener(this);
                this.tree.removeHierarchyListener(this);
                this.tree.removeComponentListener(this);
            } else {
                this.list.getSelectionModel().removeListSelectionListener(this);
                this.list.getModel().removeListDataListener(this);
                this.list.removeHierarchyBoundsListener(this);
                this.list.removeHierarchyListener(this);
                this.list.removeComponentListener(this);
            }
            this.pane.getHorizontalScrollBar().getModel().removeChangeListener(this);
            this.pane.getVerticalScrollBar().getModel().removeChangeListener(this);
            this.detached = true;
        }

        private void change() {
            ViewTooltips vtt = (ViewTooltips)(this.tree != null ? this.tree.getClientProperty(KEY) : this.list.getClientProperty(KEY));
            if (vtt != null) {
                vtt.hide();
            }
            this.detach();
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            this.change();
        }

        @Override
        public void treeNodesChanged(TreeModelEvent e) {
            this.change();
        }

        @Override
        public void treeNodesInserted(TreeModelEvent e) {
            this.change();
        }

        @Override
        public void treeNodesRemoved(TreeModelEvent e) {
            this.change();
        }

        @Override
        public void treeStructureChanged(TreeModelEvent e) {
            this.change();
        }

        @Override
        public void hierarchyChanged(HierarchyEvent e) {
            this.change();
        }

        @Override
        public void valueChanged(TreeSelectionEvent e) {
            this.change();
        }

        @Override
        public void ancestorMoved(HierarchyEvent e) {
            this.change();
        }

        @Override
        public void ancestorResized(HierarchyEvent e) {
            this.change();
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            this.change();
        }

        @Override
        public void valueChanged(ListSelectionEvent e) {
            this.change();
        }

        @Override
        public void intervalAdded(ListDataEvent e) {
            this.change();
        }

        @Override
        public void intervalRemoved(ListDataEvent e) {
            this.change();
        }

        @Override
        public void contentsChanged(ListDataEvent e) {
            this.change();
        }

        @Override
        public void componentResized(ComponentEvent e) {
            this.change();
        }

        @Override
        public void componentMoved(ComponentEvent e) {
            this.change();
        }

        @Override
        public void componentShown(ComponentEvent e) {
            this.change();
        }

        @Override
        public void componentHidden(ComponentEvent e) {
            this.change();
        }
    }

    private static final class ImgComp
    extends JComponent {
        private BufferedImage img;
        private Dimension d = null;
        private Color bg = Color.WHITE;
        private JScrollPane comp = null;
        private Object node = null;
        private AffineTransform at = AffineTransform.getTranslateInstance(0.0, 0.0);
        boolean isRight = false;

        ImgComp() {
        }

        ImgComp(BufferedImage img, Rectangle off, boolean right) {
            this.img = img;
            this.at = AffineTransform.getTranslateInstance(- off.x, 0.0);
            this.d = new Dimension(off.width, off.height);
            this.isRight = right;
        }

        public ImgComp getPartial(Rectangle bds, boolean right) {
            assert (this.img != null);
            return new ImgComp(this.img, bds, right);
        }

        public boolean configure(Object nd, JScrollPane tv, JTree tree, TreePath path, int row) {
            boolean sameVn = this.setLastRendereredObject(nd);
            boolean sameComp = this.setLastRenderedScrollPane(tv);
            Component renderer = null;
            this.bg = tree.getBackground();
            boolean sel = tree.isSelectionEmpty() ? false : tree.getSelectionModel().isPathSelected(path);
            boolean exp = tree.isExpanded(path);
            boolean leaf = !exp && tree.getModel().isLeaf(nd);
            boolean lead = path.equals(tree.getSelectionModel().getLeadSelectionPath());
            renderer = tree.getCellRenderer().getTreeCellRendererComponent(tree, nd, sel, exp, leaf, row, lead);
            if (renderer != null) {
                this.setComponent(renderer);
            }
            return true;
        }

        public boolean configure(Object nd, JScrollPane tv, JList list, int row) {
            boolean sameVn = this.setLastRendereredObject(nd);
            boolean sameComp = this.setLastRenderedScrollPane(tv);
            Component renderer = null;
            this.bg = list.getBackground();
            boolean sel = list.isSelectionEmpty() ? false : list.getSelectionModel().isSelectedIndex(row);
            renderer = list.getCellRenderer().getListCellRendererComponent(list, nd, row, sel, false);
            if (renderer != null) {
                this.setComponent(renderer);
            }
            return true;
        }

        private boolean setLastRenderedScrollPane(JScrollPane comp) {
            boolean result = comp != this.comp;
            this.comp = comp;
            return result;
        }

        private boolean setLastRendereredObject(Object nd) {
            boolean result;
            boolean bl = result = this.node != nd;
            if (result) {
                this.node = nd;
            }
            return result;
        }

        void clear() {
            this.comp = null;
            this.node = null;
        }

        public void setComponent(Component jc) {
            Dimension d = jc.getPreferredSize();
            Rectangle currentScreenBounds = Utilities.getUsableScreenBounds();
            int width = Math.min(d.width, 4 * currentScreenBounds.width);
            BufferedImage nue = new BufferedImage(width, d.height + 2, 3);
            Graphics2D g = nue.createGraphics();
            g.setColor(this.bg);
            g.fillRect(0, 0, width, d.height + 2);
            if (jc instanceof Container && !jc.isValid()) {
                jc.setSize(width, d.height);
                jc.doLayout();
            }
            SwingUtilities.paintComponent(g, jc, this, 0, 0, width, d.height + 2);
            g.dispose();
            this.setImage(nue);
        }

        @Override
        public Rectangle getBounds() {
            Dimension dd = this.getPreferredSize();
            return new Rectangle(0, 0, dd.width, dd.height);
        }

        private void setImage(BufferedImage img) {
            this.img = img;
            this.d = null;
        }

        @Override
        public Dimension getPreferredSize() {
            if (this.d == null) {
                this.d = new Dimension(this.img.getWidth(), this.img.getHeight());
            }
            return this.d;
        }

        @Override
        public Dimension getSize() {
            return this.getPreferredSize();
        }

        @Override
        public void paint(Graphics g) {
            Graphics2D g2d = (Graphics2D)g;
            g2d.drawRenderedImage(this.img, this.at);
            g.setColor(Color.GRAY);
            g.drawLine(0, 0, this.d.width, 0);
            g.drawLine(0, this.d.height - 1, this.d.width, this.d.height - 1);
            if (this.isRight) {
                g.drawLine(0, 0, 0, this.d.height - 1);
            } else {
                g.drawLine(this.d.width - 1, 0, this.d.width - 1, this.d.height - 1);
            }
        }

        @Override
        public void firePropertyChange(String s, Object a, Object b) {
        }

        @Override
        public void invalidate() {
        }

        @Override
        public void validate() {
        }

        @Override
        public void revalidate() {
        }
    }

}

