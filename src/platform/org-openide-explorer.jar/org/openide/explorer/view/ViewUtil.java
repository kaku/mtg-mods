/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.openide.explorer.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.plaf.UIResource;
import org.openide.explorer.view.TreeViewCellEditor;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

class ViewUtil {
    public static final boolean isAquaLaF = "Aqua".equals(UIManager.getLookAndFeel().getID());
    private static final boolean useDefaultBackground = Boolean.getBoolean("nb.explorerview.aqua.defaultbackground");
    private static final RequestProcessor RP = new RequestProcessor("Explorer Views");

    private ViewUtil() {
    }

    static RequestProcessor uiProcessor() {
        return RP;
    }

    static void adjustBackground(JComponent c) {
        if (!isAquaLaF || useDefaultBackground) {
            return;
        }
        if (!ViewUtil.isInTabbedContainer(c)) {
            return;
        }
        Color currentBackground = c.getBackground();
        if (currentBackground instanceof UIResource) {
            c.setBackground(UIManager.getColor("NbExplorerView.background"));
        }
    }

    private static boolean isInTabbedContainer(Component c) {
        for (Container parent = c.getParent(); null != parent; parent = parent.getParent()) {
            if (!(parent instanceof JComponent) || !"TabbedContainerUI".equals(((JComponent)parent).getUIClassID())) continue;
            return true;
        }
        return false;
    }

    static void nodeRename(final Node n, final String newStr) {
        if (n.getName().equals(newStr)) {
            return;
        }
        if (EventQueue.isDispatchThread() && Boolean.TRUE.equals(n.getValue("slowRename"))) {
            RP.post(new Runnable(){

                @Override
                public void run() {
                    ViewUtil.nodeRename(n, newStr);
                }
            });
            return;
        }
        try {
            n.setName(newStr);
        }
        catch (IllegalArgumentException exc) {
            boolean needToAnnotate;
            boolean bl = needToAnnotate = Exceptions.findLocalizedMessage((Throwable)exc) == null;
            if (needToAnnotate) {
                String msg = NbBundle.getMessage(TreeViewCellEditor.class, (String)"RenameFailed", (Object)n.getName(), (Object)newStr);
                Exceptions.attachLocalizedMessage((Throwable)exc, (String)msg);
            }
            Exceptions.printStackTrace((Throwable)exc);
        }
    }

}

