/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.CheckRenderDataProvider
 *  org.netbeans.swing.outline.Outline
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 */
package org.openide.explorer.view;

import java.awt.Color;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.tree.AbstractLayoutCache;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import org.netbeans.swing.outline.CheckRenderDataProvider;
import org.netbeans.swing.outline.Outline;
import org.openide.explorer.view.CheckableNode;
import org.openide.explorer.view.Visualizer;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

class NodeRenderDataProvider
implements CheckRenderDataProvider {
    private Outline table;

    public NodeRenderDataProvider(Outline table) {
        this.table = table;
    }

    public Color getBackground(Object o) {
        return null;
    }

    public String getDisplayName(Object o) {
        Node n = Visualizer.findNode(o);
        if (n == null) {
            throw new IllegalStateException("TreeNode must be VisualizerNode but was: " + o + " of class " + o.getClass().getName());
        }
        String text = n.getHtmlDisplayName();
        if (null == text) {
            text = n.getDisplayName();
        }
        return text;
    }

    public Color getForeground(Object o) {
        return null;
    }

    public Icon getIcon(Object o) {
        Node n = Visualizer.findNode(o);
        if (n == null) {
            throw new IllegalStateException("TreeNode must be VisualizerNode but was: " + o + " of class " + o.getClass().getName());
        }
        boolean expanded = false;
        if (o instanceof TreeNode) {
            ArrayList<TreeNode> al = new ArrayList<TreeNode>();
            for (TreeNode tn = (TreeNode)o; tn != null; tn = tn.getParent()) {
                al.add(tn);
            }
            Collections.reverse(al);
            TreePath tp = new TreePath(al.toArray());
            AbstractLayoutCache layout = this.table.getLayoutCache();
            expanded = layout.isExpanded(tp);
        }
        Image image = null;
        image = expanded ? n.getOpenedIcon(1) : n.getIcon(1);
        return new ImageIcon(image);
    }

    public String getTooltipText(Object o) {
        Node n = Visualizer.findNode(o);
        if (n == null) {
            throw new IllegalStateException("TreeNode must be VisualizerNode but was: " + o + " of class " + o.getClass().getName());
        }
        return n.getShortDescription();
    }

    public boolean isHtmlDisplayName(Object o) {
        Node n = Visualizer.findNode(o);
        if (n == null) {
            throw new IllegalStateException("TreeNode must be VisualizerNode but was: " + o + " of class " + o.getClass().getName());
        }
        return null != n.getHtmlDisplayName();
    }

    private CheckableNode getCheckCookie(Object o) {
        Node n = Visualizer.findNode(o);
        if (n == null) {
            throw new IllegalStateException("TreeNode must be VisualizerNode but was: " + o + " of class " + o.getClass().getName());
        }
        return (CheckableNode)n.getLookup().lookup(CheckableNode.class);
    }

    public boolean isCheckable(Object o) {
        CheckableNode c = this.getCheckCookie(o);
        return c != null && c.isCheckable();
    }

    public boolean isCheckEnabled(Object o) {
        CheckableNode c = this.getCheckCookie(o);
        return c != null && c.isCheckEnabled();
    }

    public Boolean isSelected(Object o) {
        CheckableNode c = this.getCheckCookie(o);
        if (c != null) {
            return c.isSelected();
        }
        return null;
    }

    public void setSelected(Object o, Boolean selected) {
        CheckableNode c = this.getCheckCookie(o);
        if (c != null) {
            c.setSelected(selected);
        }
    }
}

