/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeEvent
 */
package org.openide.explorer.view;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.tree.TreeNode;
import org.openide.explorer.view.NodeModel;
import org.openide.explorer.view.VisualizerEvent;
import org.openide.explorer.view.VisualizerNode;
import org.openide.nodes.Node;
import org.openide.nodes.NodeEvent;

final class VisualizerChildren {
    public static final VisualizerChildren EMPTY = new VisualizerChildren();
    private static final Logger LOG = Logger.getLogger(VisualizerChildren.class.getName());
    public final VisualizerNode parent;
    private final List<VisualizerNode> visNodes;
    private List<Node> snapshot;

    private VisualizerChildren() {
        this.visNodes = Collections.emptyList();
        this.snapshot = Collections.emptyList();
        this.parent = null;
    }

    public VisualizerChildren(VisualizerNode parent, List<Node> snapshot) {
        this.parent = parent;
        int size = snapshot.size();
        this.visNodes = new ArrayList<VisualizerNode>(size);
        for (int i = 0; i < size; ++i) {
            this.visNodes.add(null);
        }
        this.snapshot = snapshot;
    }

    private void recomputeIndexes(VisualizerNode tn) {
        int i;
        List<VisualizerNode> vn = this.getVisNodes(true);
        assert (vn.size() == this.snapshot.size());
        for (i = 0; i < vn.size(); ++i) {
            VisualizerNode node = vn.get(i);
            if (node == null) continue;
            node.indexOf = i;
        }
        if (tn != null && tn.indexOf == -1) {
            i = 0;
            while (i < vn.size()) {
                VisualizerNode visNode = (VisualizerNode)this.getChildAt(i);
                visNode.indexOf = i++;
                if (visNode != tn) continue;
                return;
            }
        }
    }

    public TreeNode getChildAt(int pos) {
        List<VisualizerNode> vn = this.getVisNodes(false);
        if (pos >= vn.size()) {
            return VisualizerNode.EMPTY;
        }
        VisualizerNode visNode = vn.get(pos);
        if (visNode == null) {
            Node node = this.snapshot.get(pos);
            if (node == null) {
                throw new NullPointerException("snapshot: " + this.snapshot + " pos: " + pos + " parent: " + this.parent);
            }
            visNode = VisualizerNode.getVisualizer(this, node);
            visNode.indexOf = pos;
            vn.set(pos, visNode);
            this.parent.notifyVisualizerChildrenChange(false, this);
        }
        return visNode;
    }

    public int getChildCount() {
        return this.getVisNodes(false).size();
    }

    public Enumeration<VisualizerNode> children(final boolean create) {
        return new Enumeration<VisualizerNode>(){
            private int index;

            @Override
            public boolean hasMoreElements() {
                return this.index < VisualizerChildren.this.getVisNodes(false).size();
            }

            @Override
            public VisualizerNode nextElement() {
                VisualizerNode visualizerNode = create ? (VisualizerNode)VisualizerChildren.this.getChildAt(this.index++) : VisualizerChildren.this.getVisNodes(false).get(this.index++);
                return visualizerNode;
            }
        };
    }

    public int getIndex(TreeNode p1) {
        VisualizerNode visNode = (VisualizerNode)p1;
        if (visNode.indexOf != -1) {
            List<VisualizerNode> vn = this.getVisNodes(false);
            if (visNode.indexOf >= vn.size() || vn.get(visNode.indexOf) != visNode) {
                return -1;
            }
        } else {
            this.recomputeIndexes(visNode);
        }
        return visNode.indexOf;
    }

    final String dumpIndexes(VisualizerNode visNode) {
        StringBuilder sb = new StringBuilder();
        sb.append("EMPTY: ").append(visNode == VisualizerNode.EMPTY).append(", Lazy: ").append(this.snapshot.getClass().getName().endsWith("LazySnapshot"));
        sb.append("\nSeeking for: ").append(visNode.toId());
        sb.append("\nwith parent: ").append((VisualizerNode)visNode.getParent() != null ? ((VisualizerNode)visNode.getParent()).toId() : "null");
        sb.append("\nSeeking in : ").append(this.parent != null ? this.parent.toId() : "null").append("\n");
        this.addVisNodesInfo(sb);
        return sb.toString();
    }

    private void addVisNodesInfo(StringBuilder sb) {
        List<VisualizerNode> vn = this.getVisNodes(false);
        for (int i = 0; i < vn.size(); ++i) {
            VisualizerNode node = vn.get(i);
            sb.append("  ").append(i);
            if (node != null) {
                sb.append(" = ").append(node.toId());
            } else {
                sb.append(" = null");
            }
            sb.append('\n');
        }
    }

    final String dumpEventInfo(VisualizerEvent ev) {
        StringBuilder sb = new StringBuilder();
        sb.append("\nEvent: ").append(ev.getClass().getName());
        sb.append("\nOriginal event: ").append(ev.originalEvent.getClass().getName());
        sb.append("\ncurrent vis. nodes:");
        this.addVisNodesInfo(sb);
        sb.append("\nIndexes: ");
        int[] arr = ev.getArray();
        for (int i = 0; i < arr.length; ++i) {
            sb.append(Integer.toString(arr[i]));
            sb.append(" ");
        }
        sb.append("\n");
        sb.append(ev.originalEvent.toString());
        return sb.toString();
    }

    public void added(VisualizerEvent.Added ev) {
        if (this != this.parent.getChildren()) {
            return;
        }
        this.snapshot = ev.getSnapshot();
        ListIterator<VisualizerNode> it = this.getVisNodes(true).listIterator();
        int[] indxs = ev.getArray();
        int current = 0;
        for (int inIndxs = 0; inIndxs < indxs.length; ++inIndxs) {
            while (current++ < indxs[inIndxs]) {
                it.next();
            }
            it.add(null);
        }
        this.recomputeIndexes(null);
        for (VisualizerNode p = this.parent; p != null; p = (VisualizerNode)p.getParent()) {
            Object[] listeners = p.getListenerList();
            for (int i = listeners.length - 1; i >= 0; i -= 2) {
                ((NodeModel)listeners[i]).added(ev);
            }
        }
    }

    public void removed(VisualizerEvent.Removed ev) {
        if (this != this.parent.getChildren()) {
            return;
        }
        this.snapshot = ev.getSnapshot();
        int[] idxs = ev.getArray();
        if (idxs.length == 0) {
            return;
        }
        List<VisualizerNode> vn = this.getVisNodes(true);
        if (vn.isEmpty()) {
            return;
        }
        assert (vn.size() > idxs[idxs.length - 1]);
        int prev = Integer.MAX_VALUE;
        for (int i = idxs.length - 1; i >= 0; --i) {
            if (vn.isEmpty()) continue;
            assert (idxs[i] < prev);
            VisualizerNode visNode = vn.remove(idxs[i]);
            ev.removed.add(visNode != null ? visNode : VisualizerNode.EMPTY);
        }
        this.recomputeIndexes(null);
        for (VisualizerNode p = this.parent; p != null; p = (VisualizerNode)p.getParent()) {
            Object[] listeners = p.getListenerList();
            for (int i2 = listeners.length - 1; i2 >= 0; i2 -= 2) {
                ((NodeModel)listeners[i2]).removed(ev);
            }
        }
        if (vn.isEmpty()) {
            this.parent.notifyVisualizerChildrenChange(true, this);
        }
    }

    public void reordered(VisualizerEvent.Reordered ev) {
        if (this != this.parent.getChildren()) {
            return;
        }
        this.snapshot = ev.getSnapshot();
        int[] indxs = ev.getArray();
        List<VisualizerNode> vn = this.getVisNodes(true);
        VisualizerNode[] old = vn.toArray(new VisualizerNode[vn.size()]);
        VisualizerNode[] arr = new VisualizerNode[old.length];
        int s = indxs.length;
        try {
            for (int i = 0; i < s; ++i) {
                VisualizerNode old_i = old[i];
                int indxs_i = indxs[i];
                if (arr[indxs_i] != null) {
                    LOG.log(Level.WARNING, "Writing to this index for the second time: {0}", indxs_i);
                    LOG.log(Level.WARNING, "Length of indxs array: {0}", indxs.length);
                    LOG.log(Level.WARNING, "Length of actual array: {0}", old.length);
                    LOG.warning("Indices of reorder event:");
                    int j = 0;
                    while (i < indxs.length) {
                        LOG.log(Level.WARNING, "\t{0}", indxs[j]);
                        ++j;
                    }
                    LOG.log(Level.WARNING, "Who", new Exception());
                    return;
                }
                arr[indxs_i] = old_i;
            }
        }
        catch (ArrayIndexOutOfBoundsException e) {
            LOG.log(Level.WARNING, "Length of actual array: " + old.length, e);
            LOG.warning("Indices of reorder event:");
            for (int i = 0; i < indxs.length; ++i) {
                LOG.log(Level.WARNING, "\t{0}", indxs[i]);
            }
            return;
        }
        vn.clear();
        vn.addAll(Arrays.asList(arr));
        this.recomputeIndexes(null);
        for (VisualizerNode p = this.parent; p != null; p = (VisualizerNode)p.getParent()) {
            Object[] listeners = p.getListenerList();
            for (int i = listeners.length - 1; i >= 0; i -= 2) {
                ((NodeModel)listeners[i]).reordered(ev);
            }
        }
    }

    public String toString() {
        String str = "";
        if (this.parent != null) {
            str = "Parent: " + this.parent + " ";
        }
        str = str + "[";
        for (VisualizerNode vn : this.getVisNodes(false)) {
            VisualizerChildren vch;
            str = str + vn;
            if (vn != null && (vch = vn.getChildren(false)) != EMPTY) {
                str = str + vch;
            }
            str = str + " ";
        }
        str = str + " {" + this.snapshot + "}";
        str = str + "]";
        return str;
    }

    final List<VisualizerNode> getVisNodes(boolean guardAccess) {
        if (guardAccess && !$assertionsDisabled && !EventQueue.isDispatchThread()) {
            throw new AssertionError();
        }
        return this.visNodes;
    }

}

