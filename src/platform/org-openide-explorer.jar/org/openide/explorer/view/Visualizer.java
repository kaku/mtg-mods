/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package org.openide.explorer.view;

import javax.swing.tree.TreeNode;
import org.openide.explorer.view.VisualizerNode;
import org.openide.nodes.Node;

public class Visualizer {
    private Visualizer() {
    }

    public static TreeNode findVisualizer(Node node) {
        return VisualizerNode.getVisualizer(null, node);
    }

    public static Node findNode(Object visualizer) {
        if (visualizer instanceof Node) {
            return (Node)visualizer;
        }
        return ((VisualizerNode)visualizer).node;
    }
}

