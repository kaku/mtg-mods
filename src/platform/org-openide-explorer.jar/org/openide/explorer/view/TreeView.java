/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.MouseUtils
 *  org.openide.awt.MouseUtils$PopupMouseAdapter
 *  org.openide.awt.QuickSearch
 *  org.openide.awt.QuickSearch$Callback
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeOp
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 */
package org.openide.explorer.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.Autoscroll;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.MenuElement;
import javax.swing.ScrollPaneLayout;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.TransferHandler;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.plaf.TreeUI;
import javax.swing.plaf.UIResource;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.RowMapper;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import org.openide.awt.MouseUtils;
import org.openide.awt.QuickSearch;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.AutoscrollSupport;
import org.openide.explorer.view.DragDropUtilities;
import org.openide.explorer.view.ExplorerDnDManager;
import org.openide.explorer.view.NodeRenderer;
import org.openide.explorer.view.NodeTreeModel;
import org.openide.explorer.view.TreeViewCellEditor;
import org.openide.explorer.view.TreeViewDragSupport;
import org.openide.explorer.view.TreeViewDropSupport;
import org.openide.explorer.view.ViewTooltips;
import org.openide.explorer.view.ViewUtil;
import org.openide.explorer.view.Visualizer;
import org.openide.explorer.view.VisualizerChildren;
import org.openide.explorer.view.VisualizerNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeOp;
import org.openide.util.ContextAwareAction;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

public abstract class TreeView
extends JScrollPane {
    static final Logger LOG;
    static final long serialVersionUID = -1639001987693376168L;
    private static final int TIME_TO_COLLAPSE;
    private static final int MIN_TREEVIEW_WIDTH = 400;
    private static final int MIN_TREEVIEW_HEIGHT = 400;
    protected transient JTree tree;
    transient NodeTreeModel treeModel;
    transient ExplorerManager manager;
    transient PopupSupport defaultActionListener;
    transient boolean defaultActionEnabled;
    transient PopupAdapter popupListener;
    transient TreePropertyListener managerListener = null;
    transient PropertyChangeListener wlpc;
    transient VetoableChangeListener wlvc;
    private transient boolean dragActive = true;
    private transient boolean dropActive = true;
    transient TreeViewDragSupport dragSupport;
    transient TreeViewDropSupport dropSupport;
    transient boolean dropTargetPopupAllowed = true;
    private transient int allowedDragActions = 1073741827;
    private transient int allowedDropActions = 1073741827;
    private transient QuickSearch qs;
    private transient boolean autoWaitCursor = true;
    private final VisualizerHolder visHolder = new VisualizerHolder();
    private static Reference<TreeView> lastSearchField;
    private transient boolean removedNodeWasSelected = false;
    TreePath[] origSelectionPaths = null;
    private Component searchPanel = null;
    private final Object searchConstraints = new Object();

    public TreeView() {
        this(true, true);
    }

    public TreeView(boolean defaultAction, boolean popupAllowed) {
        this.setLayout(new ExplorerScrollPaneLayout());
        this.initializeTree();
        this.setDropTarget(DragDropUtilities.dragAndDropEnabled);
        this.setPopupAllowed(popupAllowed);
        this.setDefaultActionAllowed(defaultAction);
        Dimension dim = null;
        try {
            dim = this.getPreferredSize();
            if (dim == null) {
                dim = new Dimension(400, 400);
            }
        }
        catch (NullPointerException npe) {
            dim = new Dimension(400, 400);
        }
        if (dim.width < 400) {
            dim.width = 400;
        }
        if (dim.height < 400) {
            dim.height = 400;
        }
        this.setPreferredSize(dim);
    }

    @Override
    public void updateUI() {
        VisualizerHolder tmp = this.visHolder;
        if (tmp != null) {
            tmp.clear();
        }
        super.updateUI();
        this.setViewportBorder(BorderFactory.createEmptyBorder());
        this.setBorder(BorderFactory.createEmptyBorder());
    }

    void initializeTree() {
        this.treeModel = this.createModel();
        this.treeModel.addView(this);
        this.tree = new ExplorerTree(this.treeModel);
        if (GraphicsEnvironment.isHeadless()) {
            return;
        }
        NodeRenderer rend = new NodeRenderer();
        this.tree.setCellRenderer(rend);
        this.tree.putClientProperty("JTree.lineStyle", "Angled");
        this.setViewportView(this.tree);
        this.tree.setCellEditor(new TreeViewCellEditor(this.tree));
        this.tree.setEditable(true);
        this.tree.setInvokesStopCellEditing(true);
        int rowHeight = rend.getTreeCellRendererComponent((JTree)this.tree, (Object)null, (boolean)false, (boolean)false, (boolean)false, (int)0, (boolean)true).getPreferredSize().height;
        this.tree.setRowHeight(rowHeight);
        this.tree.setLargeModel(true);
        this.setSelectionMode(4);
        ToolTipManager.sharedInstance().registerComponent(this.tree);
        this.managerListener = new TreePropertyListener();
        this.tree.addTreeExpansionListener(this.managerListener);
        this.tree.addTreeWillExpandListener(this.managerListener);
        this.setRequestFocusEnabled(false);
        this.defaultActionListener = new PopupSupport();
        this.getInputMap(0).put(KeyStroke.getKeyStroke(121, 64), "org.openide.actions.PopupAction");
        this.getActionMap().put("org.openide.actions.PopupAction", this.defaultActionListener.popup);
        this.tree.addFocusListener(this.defaultActionListener);
        this.tree.addMouseListener(this.defaultActionListener);
    }

    public boolean isPopupAllowed() {
        return this.popupListener != null && this.isShowing() && this.isDisplayable();
    }

    public void setPopupAllowed(boolean value) {
        if (this.popupListener == null && value) {
            this.popupListener = new PopupAdapter();
            this.tree.addMouseListener((MouseListener)((Object)this.popupListener));
            return;
        }
        if (this.popupListener != null && !value) {
            this.tree.removeMouseListener((MouseListener)((Object)this.popupListener));
            this.popupListener = null;
            return;
        }
    }

    void setDropTargetPopupAllowed(boolean value) {
        this.dropTargetPopupAllowed = value;
        if (this.dropSupport != null) {
            this.dropSupport.setDropTargetPopupAllowed(value);
        }
    }

    boolean isDropTargetPopupAllowed() {
        return this.dropSupport != null ? this.dropSupport.isDropTargetPopupAllowed() : this.dropTargetPopupAllowed;
    }

    public boolean isDefaultActionEnabled() {
        return this.defaultActionEnabled;
    }

    @Override
    public void requestFocus() {
        this.tree.requestFocus();
    }

    @Override
    public boolean requestFocusInWindow() {
        return this.tree.requestFocusInWindow();
    }

    public void setDefaultActionAllowed(boolean value) {
        this.defaultActionEnabled = value;
        if (value) {
            this.tree.registerKeyboardAction(this.defaultActionListener, KeyStroke.getKeyStroke(10, 0, false), 0);
        } else {
            this.tree.unregisterKeyboardAction(KeyStroke.getKeyStroke(10, 0, false));
        }
    }

    public boolean isRootVisible() {
        return this.tree.isRootVisible();
    }

    public void setRootVisible(boolean visible) {
        this.tree.setRootVisible(visible);
        this.tree.setShowsRootHandles(!visible);
    }

    public boolean isQuickSearchAllowed() {
        return this.qs.isEnabled();
    }

    public void setQuickSearchAllowed(boolean allowedQuickSearch) {
        this.qs.setEnabled(allowedQuickSearch);
    }

    @Deprecated
    public void setUseSubstringInQuickSearch(boolean useSubstring) {
    }

    public boolean isDragSource() {
        return this.dragActive;
    }

    public void setDragSource(boolean state) {
        if (state && this.dragSupport == null) {
            this.dragSupport = new TreeViewDragSupport(this, this.tree);
        }
        this.dragActive = state;
        if (this.dragSupport != null && !GraphicsEnvironment.isHeadless()) {
            this.dragSupport.activate(this.dragActive);
        }
    }

    public boolean isDropTarget() {
        return this.dropActive;
    }

    public void setDropTarget(boolean state) {
        if (this.dropActive && this.dropSupport == null) {
            this.dropSupport = new TreeViewDropSupport(this, this.tree, this.dropTargetPopupAllowed);
        }
        this.dropActive = state;
        if (this.dropSupport != null && !GraphicsEnvironment.isHeadless()) {
            this.dropSupport.activate(this.dropActive);
        }
    }

    public int getAllowedDragActions() {
        return this.allowedDragActions;
    }

    public void setAllowedDragActions(int actions) {
        this.allowedDragActions = actions;
    }

    public int getAllowedDropActions() {
        return this.allowedDropActions;
    }

    public void setAllowedDropActions(int actions) {
        this.allowedDropActions = actions;
    }

    public void collapseNode(final Node n) {
        if (n == null) {
            throw new IllegalArgumentException();
        }
        VisualizerNode.runSafe(new Runnable(){

            @Override
            public void run() {
                TreePath path = TreeView.this.getTreePath(n);
                TreeView.LOG.log(Level.FINE, "collapseNode: {0} {1}", new Object[]{n, path});
                TreeView.this.tree.collapsePath(path);
                TreeView.LOG.fine("collapsePath done");
            }
        });
    }

    public void expandNode(final Node n) {
        if (n == null) {
            throw new IllegalArgumentException();
        }
        this.lookupExplorerManager();
        final List prepare = n.getChildren().snapshot();
        VisualizerNode.runSafe(new Runnable(){

            @Override
            public void run() {
                TreeView.LOG.log(Level.FINEST, "Just print the variable so it is not GCed: {0}", prepare);
                TreePath p = TreeView.this.getTreePath(n);
                TreeView.LOG.log(Level.FINE, "expandNode: {0} {1}", new Object[]{n, p});
                TreeView.this.tree.expandPath(p);
                TreeView.LOG.fine("expandPath done");
            }
        });
    }

    public boolean isExpanded(Node n) {
        return this.tree.isExpanded(this.getTreePath(n));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void expandAll() {
        TreeUI treeUI = this.tree.getUI();
        try {
            this.tree.setUI(null);
            TreeNode root = (TreeNode)this.tree.getModel().getRoot();
            this.expandOrCollapseAll(new TreePath(root), true);
        }
        finally {
            this.tree.setUI(treeUI);
            this.tree.updateUI();
        }
    }

    private void expandOrCollapseAll(TreePath parent, boolean expand) {
        TreeNode node = (TreeNode)parent.getLastPathComponent();
        if (node.getChildCount() > 0) {
            Enumeration e = node.children();
            while (e.hasMoreElements()) {
                TreeNode n = (TreeNode)e.nextElement();
                TreePath path = parent.pathByAddingChild(n);
                this.expandOrCollapseAll(path, expand);
            }
        }
        if (expand) {
            this.tree.expandPath(parent);
        } else {
            this.tree.collapsePath(parent);
        }
    }

    @Override
    public void validate() {
        Children.MUTEX.readAccess(new Runnable(){

            @Override
            public void run() {
                TreeView.this.validate();
            }
        });
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.lookupExplorerManager();
    }

    private void lookupExplorerManager() {
        ExplorerManager newManager = ExplorerManager.find(this);
        if (newManager != this.manager) {
            if (this.manager != null) {
                this.manager.removeVetoableChangeListener(this.wlvc);
                this.manager.removePropertyChangeListener(this.wlpc);
            }
            this.manager = newManager;
            this.wlvc = WeakListeners.vetoableChange((VetoableChangeListener)this.managerListener, (Object)this.manager);
            this.manager.addVetoableChangeListener(this.wlvc);
            this.wlpc = WeakListeners.propertyChange((PropertyChangeListener)this.managerListener, (Object)this.manager);
            this.manager.addPropertyChangeListener(this.wlpc);
            this.synchronizeRootContext();
            this.synchronizeExploredContext();
            this.synchronizeSelectedNodes();
        }
        this.tree.getSelectionModel().removeTreeSelectionListener(this.managerListener);
        this.tree.getSelectionModel().addTreeSelectionListener(this.managerListener);
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.tree.getSelectionModel().removeTreeSelectionListener(this.managerListener);
    }

    protected abstract NodeTreeModel createModel();

    protected abstract void selectionChanged(Node[] var1, ExplorerManager var2) throws PropertyVetoException;

    protected abstract boolean selectionAccept(Node[] var1);

    protected abstract void showPath(TreePath var1);

    protected abstract void showSelection(TreePath[] var1);

    protected boolean useExploredContextMenu() {
        return false;
    }

    private boolean isSelectionModeBroken(Node[] nodes) {
        if (nodes.length <= 1 || this.getSelectionMode() == 4) {
            return false;
        }
        if (this.getSelectionMode() == 1) {
            return true;
        }
        TreePath[] paths = new TreePath[nodes.length];
        RowMapper rowMapper = this.tree.getSelectionModel().getRowMapper();
        if (rowMapper == null) {
            return false;
        }
        ArrayList<Node> toBeExpaned = new ArrayList<Node>(3);
        for (int i = 0; i < nodes.length; ++i) {
            toBeExpaned.clear();
            Node n = nodes[i];
            while (n.getParentNode() != null) {
                if (!this.isExpanded(n)) {
                    toBeExpaned.add(n);
                }
                n = n.getParentNode();
            }
            for (int j = toBeExpaned.size() - 1; j >= 0; --j) {
                this.expandNode((Node)toBeExpaned.get(j));
            }
            paths[i] = this.getTreePath(nodes[i]);
        }
        int[] rows = rowMapper.getRowsForPaths(paths);
        Arrays.sort(rows);
        for (int i2 = 1; i2 < rows.length; ++i2) {
            if (rows[i2] == rows[i2 - 1] + 1) continue;
            return true;
        }
        return false;
    }

    TreePath getTreePath(Node node) {
        return new TreePath(this.treeModel.getPathToRoot(VisualizerNode.getVisualizer(null, node)));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void callSelectionChanged(Node[] nodes) {
        this.manager.removePropertyChangeListener(this.wlpc);
        this.manager.removeVetoableChangeListener(this.wlvc);
        try {
            this.selectionChanged(nodes, this.manager);
        }
        catch (PropertyVetoException e) {
            this.synchronizeSelectedNodes();
        }
        finally {
            this.manager.addPropertyChangeListener(this.wlpc);
            this.manager.addVetoableChangeListener(this.wlvc);
        }
    }

    final void synchronizeRootContext() {
        Mutex.EVENT.readAccess(new Runnable(){

            @Override
            public void run() {
                TreeCellEditor cellEditor = TreeView.this.tree.getCellEditor();
                if (cellEditor instanceof TreeViewCellEditor) {
                    ((TreeViewCellEditor)cellEditor).abortTimer();
                }
                TreeView.this.tree.cancelEditing();
                Node rc = TreeView.this.manager.getRootContext();
                TreeView.LOG.log(Level.FINE, "synchronizeRootContext {0}", (Object)rc);
                TreeView.this.treeModel.setNode(rc, TreeView.this.visHolder);
            }
        });
    }

    final void synchronizeExploredContext() {
        final Node n = this.manager.getExploredContext();
        if (n == null) {
            return;
        }
        VisualizerNode.runSafe(new Runnable(){

            @Override
            public void run() {
                TreePath tp = TreeView.this.getTreePath(n);
                TreeView.LOG.log(Level.FINE, "synchronizeExploredContext {0} path {1}", new Object[]{n, tp});
                TreeView.this.showPath(tp);
            }
        });
    }

    public void setSelectionMode(int mode) {
        this.tree.getSelectionModel().setSelectionMode(mode);
    }

    public int getSelectionMode() {
        return this.tree.getSelectionModel().getSelectionMode();
    }

    public void setAutoWaitCursor(boolean enable) {
        this.autoWaitCursor = enable;
    }

    private void showWaitCursor(boolean show) {
        JRootPane rPane = this.getRootPane();
        if (rPane == null) {
            return;
        }
        if (SwingUtilities.isEventDispatchThread()) {
            TreeView.doShowWaitCursor(rPane.getGlassPane(), show);
        } else {
            SwingUtilities.invokeLater(new CursorR(rPane.getGlassPane(), show));
        }
    }

    private static void doShowWaitCursor(Component glassPane, boolean show) {
        if (show) {
            glassPane.setCursor(Cursor.getPredefinedCursor(3));
            glassPane.setVisible(true);
        } else {
            glassPane.setVisible(false);
            glassPane.setCursor(null);
        }
    }

    private void prepareWaitCursor(final Node node) {
        if (node == null || !this.autoWaitCursor) {
            return;
        }
        this.showWaitCursor(true);
        ViewUtil.uiProcessor().post(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                try {
                    node.getChildren().getNodesCount(true);
                }
                catch (Exception e) {
                    TreeView.LOG.log(Level.WARNING, null, e);
                }
                finally {
                    TreeView.this.showWaitCursor(false);
                }
            }
        });
    }

    final void synchronizeSelectedNodes() {
        VisualizerNode.runSafe(new Runnable(){

            @Override
            public void run() {
                Node[] arr = TreeView.this.manager.getSelectedNodes();
                TreePath[] paths = new TreePath[arr.length];
                boolean log = TreeView.LOG.isLoggable(Level.FINE);
                if (log) {
                    TreeView.LOG.log(Level.FINE, "synchronizeSelectedNodes: {0}", arr.length);
                }
                for (int i = 0; i < arr.length; ++i) {
                    paths[i] = TreeView.this.getTreePath(arr[i]);
                    if (!log) continue;
                    TreeView.LOG.log(Level.FINE, "paths[{0}] = {1} node: {2}", new Object[]{i, paths[i], arr[i]});
                }
                TreeView.this.tree.getSelectionModel().removeTreeSelectionListener(TreeView.this.managerListener);
                TreeView.this.showSelection(paths);
                TreeView.this.tree.getSelectionModel().addTreeSelectionListener(TreeView.this.managerListener);
                if (log) {
                    TreeView.LOG.fine("synchronizeSelectedNodes done");
                }
            }
        });
    }

    void scrollTreeToVisible(TreePath path, TreeNode child) {
        Rectangle base = this.tree.getVisibleRect();
        Rectangle b1 = this.tree.getPathBounds(path);
        Rectangle b2 = this.tree.getPathBounds(new TreePath(this.treeModel.getPathToRoot(child)));
        if (base != null && b1 != null && b2 != null) {
            this.tree.scrollRectToVisible(new Rectangle(base.x, b1.y, 1, b2.y - b1.y + b2.height));
        }
    }

    private void createPopup(int xpos, int ypos, JPopupMenu popup) {
        if (popup.getSubElements().length > 0) {
            popup.show(this, xpos, ypos);
        }
    }

    void createPopup(int xpos, int ypos) {
        if (this.isPopupAllowed()) {
            JPopupMenu popup;
            Node[] selNodes = this.manager.getSelectedNodes();
            if (selNodes.length > 0) {
                Action[] actions = NodeOp.findActions((Node[])selNodes);
                if (actions.length > 0) {
                    this.createPopup(xpos, ypos, Utilities.actionsToPopup((Action[])actions, (Component)this));
                }
            } else if (this.manager.getRootContext() != null && (popup = this.manager.getRootContext().getContextMenu()) != null) {
                this.createPopup(xpos, ypos, popup);
            }
        }
    }

    void createExtendedPopup(int xpos, int ypos, JMenu newMenu) {
        Node[] ns = this.manager.getSelectedNodes();
        JPopupMenu popup = null;
        if (ns.length > 0) {
            Action[] actions = NodeOp.findActions((Node[])ns);
            popup = Utilities.actionsToPopup((Action[])actions, (Component)this);
        } else if (this.manager.getRootContext() != null) {
            popup = this.manager.getRootContext().getContextMenu();
        }
        boolean cnt = false;
        if (popup == null) {
            popup = SystemAction.createPopupMenu((SystemAction[])new SystemAction[0]);
        }
        popup.add(newMenu);
        this.createPopup(xpos, ypos, popup);
    }

    Point getPositionForPopup() {
        int i = this.tree.getLeadSelectionRow();
        if (i < 0) {
            return null;
        }
        Rectangle rect = this.tree.getRowBounds(i);
        if (rect == null) {
            return null;
        }
        Point p = new Point(rect.x, rect.y);
        p = SwingUtilities.convertPoint(this.tree, p, this);
        return p;
    }

    static /* varargs */ Action takeAction(Action action, Node ... nodes) {
        if (action instanceof ContextAwareAction) {
            Lookup contextLookup = TreeView.getLookupFor(nodes);
            Action contextInstance = ((ContextAwareAction)action).createContextAwareInstance(contextLookup);
            assert (contextInstance != action);
            action = contextInstance;
        }
        return action;
    }

    private static /* varargs */ Lookup getLookupFor(Node ... nodes) {
        if (nodes.length == 1) {
            Lookup contextLookup = nodes[0].getLookup();
            Object o = contextLookup.lookup(nodes[0].getClass());
            if (!nodes[0].equals(o)) {
                contextLookup = new ProxyLookup(new Lookup[]{Lookups.singleton((Object)nodes[0]), contextLookup});
            }
            return contextLookup;
        }
        Lookup[] lkps = new Lookup[nodes.length];
        for (int i = 0; i < nodes.length; ++i) {
            lkps[i] = nodes[i].getLookup();
        }
        ProxyLookup contextLookup = new ProxyLookup(lkps);
        HashSet<Node> toAdd = new HashSet<Node>(Arrays.asList(nodes));
        toAdd.removeAll(contextLookup.lookupAll(Node.class));
        if (!toAdd.isEmpty()) {
            contextLookup = new ProxyLookup(new Lookup[]{contextLookup, Lookups.fixed((Object[])((Object[])toAdd.toArray((T[])new Node[toAdd.size()])))});
        }
        return contextLookup;
    }

    static TreePath findSiblingTreePath(TreePath parentPath, int[] childIndices) {
        TreePath newSelection;
        if (childIndices == null) {
            throw new IllegalArgumentException("Indexes of changed children are null.");
        }
        if (parentPath == null) {
            throw new IllegalArgumentException("The tree path to parent is null.");
        }
        if (childIndices.length == 0) {
            return null;
        }
        TreeNode parent = (TreeNode)parentPath.getLastPathComponent();
        Object[] parentPaths = parentPath.getPath();
        int childCount = parent.getChildCount();
        if (childCount > 0) {
            int childPathLength = parentPaths.length + 1;
            Object[] childPath = new Object[childPathLength];
            System.arraycopy(parentPaths, 0, childPath, 0, parentPaths.length);
            int selectedChild = Math.min(childIndices[0], childCount - 1);
            childPath[childPathLength - 1] = parent.getChildAt(selectedChild);
            newSelection = new TreePath(childPath);
        } else {
            newSelection = new TreePath(parentPaths);
        }
        return newSelection;
    }

    void removedNodes(List<VisualizerNode> removed) {
        TreeSelectionModel sm = this.tree.getSelectionModel();
        TreePath[] selPaths = sm != null ? sm.getSelectionPaths() : null;
        ArrayList<TreePath> remSel = null;
        for (VisualizerNode vn : removed) {
            this.visHolder.removeRecur(vn.getChildren(false));
            if (selPaths == null) continue;
            TreePath path = new TreePath(vn.getPathToRoot());
            for (TreePath tp : selPaths) {
                if (!path.isDescendant(tp)) continue;
                if (remSel == null) {
                    remSel = new ArrayList<TreePath>();
                }
                remSel.add(tp);
            }
        }
        boolean bl = this.removedNodeWasSelected = remSel != null;
        if (remSel != null) {
            try {
                sm.removeSelectionPaths(remSel.toArray(new TreePath[remSel.size()]));
            }
            catch (NullPointerException e) {
                // empty catch block
            }
        }
    }

    Node getOriginalNode(Node n) {
        return n;
    }

    static void performPreferredActionOnNodes(Node[] nodes) {
        if (nodes.length > 0) {
            Action a = nodes[0].getPreferredAction();
            if (a == null) {
                return;
            }
            for (int i = 1; i < nodes.length; ++i) {
                Action ai = nodes[i].getPreferredAction();
                if (ai != null && ai.equals(a)) continue;
                return;
            }
            if ((a = TreeView.takeAction(a, (Node[])nodes)) != null && a.isEnabled()) {
                a.actionPerformed(new ActionEvent((Object)(nodes.length == 1 ? nodes[0] : nodes), 1001, ""));
            } else {
                Utilities.disabledActionBeep();
            }
        }
    }

    @Override
    public void add(Component comp, Object constraints) {
        if (constraints == this.searchConstraints) {
            this.searchPanel = comp;
            constraints = null;
        }
        super.add(comp, constraints);
    }

    @Override
    public void remove(Component comp) {
        if (comp == this.searchPanel) {
            this.searchPanel = null;
        }
        super.remove(comp);
    }

    @Override
    public Insets getInsets() {
        Insets res = this.getInnerInsets();
        res = new Insets(res.top, res.left, res.bottom, res.right);
        if (null != this.searchPanel && this.searchPanel.isVisible()) {
            res.bottom += this.searchPanel.getPreferredSize().height;
        }
        return res;
    }

    private Insets getInnerInsets() {
        Insets res = super.getInsets();
        if (null == res) {
            res = new Insets(0, 0, 0, 0);
        }
        return res;
    }

    Component getSearchPanel() {
        return this.searchPanel;
    }

    static {
        UIManager.put("Tree.scrollsHorizontallyAndVertically", Boolean.TRUE);
        LOG = Logger.getLogger(TreeView.class.getName());
        TIME_TO_COLLAPSE = System.getProperty("netbeans.debug.heap") != null ? 0 : 15000;
        lastSearchField = new WeakReference<Object>(null);
    }

    static class VisualizerHolder
    extends HashSet<VisualizerChildren> {
        VisualizerHolder() {
        }

        void removeRecur(VisualizerChildren visChildren) {
            Enumeration<VisualizerNode> vnodes = visChildren.children(false);
            while (vnodes.hasMoreElements()) {
                VisualizerNode vn = vnodes.nextElement();
                if (vn == null) continue;
                this.removeRecur(vn.getChildren(false));
            }
            this.remove(visChildren);
        }
    }

    private static class DummyTransferHandler
    extends TransferHandler {
        private DummyTransferHandler() {
        }

        @Override
        public void exportAsDrag(JComponent comp, InputEvent e, int action) {
        }

        @Override
        public void exportToClipboard(JComponent comp, Clipboard clip, int action) throws IllegalStateException {
        }

        @Override
        public boolean canImport(JComponent comp, DataFlavor[] transferFlavors) {
            return false;
        }

        @Override
        public boolean importData(JComponent comp, Transferable t) {
            return false;
        }

        @Override
        public int getSourceActions(JComponent c) {
            return 3;
        }
    }

    private final class ExplorerTree
    extends JTree
    implements Autoscroll,
    QuickSearch.Callback {
        AutoscrollSupport support;
        private String maxPrefix;
        int SEARCH_FIELD_SPACE;
        private boolean firstPaint;
        private List<TreePath> searchResults;
        private int currentSelectionIndex;
        private String lastSearchText;
        private boolean registered;

        ExplorerTree(TreeModel model) {
            super(model);
            this.SEARCH_FIELD_SPACE = 3;
            this.firstPaint = true;
            this.searchResults = new ArrayList<TreePath>();
            this.registered = false;
            this.toggleClickCount = 0;
            this.getInputMap().put(KeyStroke.getKeyStroke("control C"), "none");
            this.getInputMap().put(KeyStroke.getKeyStroke("control V"), "none");
            this.getInputMap().put(KeyStroke.getKeyStroke("control X"), "none");
            this.getInputMap().put(KeyStroke.getKeyStroke("COPY"), "none");
            this.getInputMap().put(KeyStroke.getKeyStroke("PASTE"), "none");
            this.getInputMap().put(KeyStroke.getKeyStroke("CUT"), "none");
            if (Utilities.isMac()) {
                this.getInputMap().put(KeyStroke.getKeyStroke(67, 4), "none");
                this.getInputMap().put(KeyStroke.getKeyStroke(88, 4), "none");
                this.getInputMap().put(KeyStroke.getKeyStroke(86, 4), "none");
            }
            this.setupSearch();
            if (!GraphicsEnvironment.isHeadless()) {
                this.setDragEnabled(true);
            }
        }

        @Override
        public void addNotify() {
            super.addNotify();
            if (!this.registered) {
                ViewTooltips.register(this);
                this.registered = true;
            }
            ViewUtil.adjustBackground(this);
        }

        @Override
        public void removeNotify() {
            super.removeNotify();
            if (this.registered) {
                ViewTooltips.unregister(this);
                this.registered = false;
            }
        }

        @Override
        public void updateUI() {
            super.updateUI();
            this.setBorder(BorderFactory.createEmptyBorder());
            if (this.getTransferHandler() != null && this.getTransferHandler() instanceof UIResource) {
                this.setTransferHandler(new DummyTransferHandler());
            }
        }

        private void calcRowHeight(Graphics g) {
            int height = Math.max(18, 2 + g.getFontMetrics(this.getFont()).getHeight());
            String s = System.getProperty("nb.cellrenderer.fixedheight");
            if (s != null) {
                try {
                    height = Integer.parseInt(s);
                }
                catch (Exception e) {
                    // empty catch block
                }
            }
            if (this.getRowHeight() != height) {
                this.setRowHeight(height);
            } else {
                this.revalidate();
                this.repaint();
            }
        }

        @Override
        public Rectangle getRowBounds(int row) {
            Rectangle r = super.getRowBounds(row);
            if (r == null) {
                TreeView.LOG.log(Level.WARNING, "No bounds for row {0} in three view: {1}", new Object[]{row, this});
                return new Rectangle();
            }
            return r;
        }

        @Override
        public void paint(Graphics g) {
            new GuardedActions(0, g);
        }

        @Override
        protected void validateTree() {
            new GuardedActions(1, null);
        }

        @Override
        public void doLayout() {
            new GuardedActions(2, null);
        }

        private void guardedPaint(Graphics g) {
            if (this.firstPaint) {
                this.firstPaint = false;
                this.calcRowHeight(g);
                g.setColor(this.getBackground());
                g.fillRect(0, 0, this.getWidth(), this.getHeight());
                return;
            }
            try {
                this.paint(g);
            }
            catch (NullPointerException ex) {
                TreeView.LOG.log(Level.INFO, "Problems while painting", ex);
            }
        }

        private void guardedValidateTree() {
            super.validateTree();
        }

        private void guardedDoLayout() {
            super.doLayout();
        }

        @Override
        public void setFont(Font f) {
            if (f != this.getFont()) {
                this.firstPaint = true;
                super.setFont(f);
            }
        }

        @Override
        protected void processFocusEvent(FocusEvent fe) {
            new GuardedActions(3, fe);
        }

        @Override
        protected void processKeyEvent(KeyEvent e) {
            TreeView.this.qs.processKeyEvent(e);
            if (!e.isConsumed()) {
                super.processKeyEvent(e);
            }
        }

        private void repaintSelection() {
            int first = this.getSelectionModel().getMinSelectionRow();
            int last = this.getSelectionModel().getMaxSelectionRow();
            if (first != -1) {
                if (first == last) {
                    Rectangle r = this.getRowBounds(first);
                    if (r == null) {
                        this.repaint();
                        return;
                    }
                    this.repaint(r.x, r.y, r.width, r.height);
                } else {
                    Rectangle top = this.getRowBounds(first);
                    Rectangle bottom = this.getRowBounds(last);
                    if (top == null || bottom == null) {
                        this.repaint();
                        return;
                    }
                    Rectangle r = new Rectangle();
                    r.x = Math.min(top.x, bottom.x);
                    r.y = top.y;
                    r.width = this.getWidth();
                    r.height = bottom.y + bottom.height - top.y;
                    this.repaint(r.x, r.y, r.width, r.height);
                }
            }
        }

        private void setupSearch() {
            TreeView.this.qs = QuickSearch.attach((JComponent)TreeView.this, (Object)TreeView.this.searchConstraints, (QuickSearch.Callback)this);
        }

        public void quickSearchUpdate(String searchText) {
            this.lastSearchText = searchText;
            this.currentSelectionIndex = 0;
            this.searchResults.clear();
            this.maxPrefix = null;
            String text = searchText.toUpperCase();
            TreeView.this.origSelectionPaths = this.getSelectionPaths();
            if (TreeView.this.origSelectionPaths != null && TreeView.this.origSelectionPaths.length == 0) {
                TreeView.this.origSelectionPaths = null;
            }
            if (text.length() > 0) {
                this.searchResults = this.doSearch(text);
            }
            this.displaySearchResult();
        }

        public void showNextSelection(boolean forward) {
            this.currentSelectionIndex = forward ? ++this.currentSelectionIndex : --this.currentSelectionIndex;
            this.displaySearchResult();
        }

        public String findMaxPrefix(String prefix) {
            return this.maxPrefix;
        }

        public void quickSearchConfirmed() {
            TreePath selectedTPath = this.getSelectionPath();
            if (selectedTPath != null) {
                TreeNode selectedTNode = (TreeNode)selectedTPath.getLastPathComponent();
                Node selectedNode = Visualizer.findNode(selectedTNode);
                TreeView.performPreferredActionOnNodes(new Node[]{selectedNode});
            }
            TreeView.this.origSelectionPaths = null;
            this.searchResults.clear();
            this.lastSearchText = null;
        }

        public void quickSearchCanceled() {
            TreeView.this.origSelectionPaths = null;
            this.searchResults.clear();
            this.lastSearchText = null;
        }

        private List<TreePath> doSearch(String prefix) {
            ArrayList<TreePath> results = new ArrayList<TreePath>();
            HashSet<TreePath> resSet = new HashSet<TreePath>();
            int startOfSubstringMatches = 0;
            int startIndex = TreeView.this.origSelectionPaths != null ? Math.max(0, this.getRowForPath(TreeView.this.origSelectionPaths[0])) : 0;
            int size = this.getRowCount();
            if (size == 0) {
                return results;
            }
            do {
                TreePath path;
                SubstringSearchResult substringSearchResult;
                TreePath treePath = path = (substringSearchResult = this.getNextSubstringMatch(prefix, startIndex %= size, true)) != null ? substringSearchResult.treePath : null;
                if (path == null || resSet.contains(path)) break;
                startIndex = TreeView.this.tree.getRowForPath(path);
                boolean isPrefixMatch = true;
                if (substringSearchResult.index == 0) {
                    results.add(startOfSubstringMatches++, path);
                } else {
                    isPrefixMatch = false;
                    results.add(path);
                }
                resSet.add(path);
                if (isPrefixMatch) {
                    String elementName = ((VisualizerNode)path.getLastPathComponent()).getDisplayName();
                    if (this.maxPrefix == null) {
                        this.maxPrefix = elementName;
                    }
                    this.maxPrefix = QuickSearch.findMaxPrefix((String)this.maxPrefix, (String)elementName, (boolean)true);
                }
                ++startIndex;
            } while (true);
            return results;
        }

        private SubstringSearchResult getNextSubstringMatch(String substring, int startingRow, boolean forward) {
            int max = this.getRowCount();
            if (substring == null) {
                throw new IllegalArgumentException("Substring is null");
            }
            if (startingRow < 0 || startingRow >= max) {
                throw new IllegalArgumentException("startingRow = " + startingRow + " rowCount = " + max);
            }
            substring = substring.toUpperCase();
            int increment = forward ? 1 : -1;
            int row = startingRow;
            do {
                TreePath path;
                int index;
                String text;
                if ((index = (text = this.convertValueToText((path = this.getPathForRow(row)).getLastPathComponent(), this.isRowSelected(row), this.isExpanded(row), true, row, false)).toUpperCase().indexOf(substring)) < 0) continue;
                return new SubstringSearchResult(path, index);
            } while ((row = (row + increment + max) % max) != startingRow);
            return null;
        }

        private void displaySearchResult() {
            int sz = this.searchResults.size();
            if (sz > 0) {
                if (this.currentSelectionIndex < 0) {
                    this.currentSelectionIndex = sz - 1;
                } else if (this.currentSelectionIndex >= sz) {
                    this.currentSelectionIndex = 0;
                }
                TreePath path = this.searchResults.get(this.currentSelectionIndex);
                this.setSelectionPath(path);
                this.scrollPathToVisible(path);
            } else if (this.lastSearchText.isEmpty() && TreeView.this.origSelectionPaths != null) {
                this.setSelectionPaths(TreeView.this.origSelectionPaths);
                this.scrollPathToVisible(TreeView.this.origSelectionPaths[0]);
            } else {
                this.clearSelection();
            }
        }

        @Override
        public void autoscroll(Point cursorLoc) {
            this.getSupport().autoscroll(cursorLoc);
        }

        @Override
        public Insets getAutoscrollInsets() {
            return this.getSupport().getAutoscrollInsets();
        }

        AutoscrollSupport getSupport() {
            if (this.support == null) {
                this.support = new AutoscrollSupport(this, new Insets(15, 10, 15, 10));
            }
            return this.support;
        }

        @Override
        public String getToolTipText(MouseEvent event) {
            if (event != null) {
                Point p = event.getPoint();
                int selRow = this.getRowForLocation(p.x, p.y);
                if (selRow != -1) {
                    TreePath path = this.getPathForRow(selRow);
                    VisualizerNode v = (VisualizerNode)path.getLastPathComponent();
                    String tooltip = v.getShortDescription();
                    String displayName = v.getDisplayName();
                    if (tooltip != null && !tooltip.equals(displayName)) {
                        return tooltip;
                    }
                }
            }
            return null;
        }

        @Override
        protected TreeModelListener createTreeModelListener() {
            return new ModelHandler();
        }

        @Override
        public AccessibleContext getAccessibleContext() {
            if (this.accessibleContext == null) {
                this.accessibleContext = new AccessibleExplorerTree();
            }
            return this.accessibleContext;
        }

        private class SubstringSearchResult {
            TreePath treePath;
            int index;

            public SubstringSearchResult(TreePath treePath, int index) {
                this.treePath = treePath;
                this.index = index;
            }
        }

        private class ModelHandler
        extends JTree.TreeModelHandler {
            ModelHandler() {
                super(ExplorerTree.this);
            }

            @Override
            public void treeStructureChanged(TreeModelEvent e) {
                TreePath[] selectionPaths = ExplorerTree.this.getSelectionPaths();
                Enumeration<TreePath> expanded = ExplorerTree.this.getExpandedDescendants(e.getTreePath());
                super.treeStructureChanged(e);
                if (expanded != null) {
                    while (expanded.hasMoreElements()) {
                        ExplorerTree.this.expandPath(expanded.nextElement());
                    }
                }
                if (selectionPaths != null && selectionPaths.length > 0) {
                    boolean wasSelected = ExplorerTree.this.isPathSelected(selectionPaths[0]);
                    ExplorerTree.this.setSelectionPaths(selectionPaths);
                    if (!wasSelected) {
                        ExplorerTree.this.scrollPathToVisible(selectionPaths[0]);
                    }
                }
            }

            @Override
            public void treeNodesRemoved(TreeModelEvent e) {
                super.treeNodesRemoved(e);
                boolean wasSelected = TreeView.this.removedNodeWasSelected;
                TreeView.this.removedNodeWasSelected = false;
                if (ExplorerDnDManager.getDefault().isDnDActive()) {
                    return;
                }
                if (wasSelected && TreeView.this.tree.getSelectionCount() == 0) {
                    TreePath path = TreeView.findSiblingTreePath(e.getTreePath(), e.getChildIndices());
                    if (path == null || e.getChildIndices().length == 0) {
                        return;
                    }
                    if (path.getPathCount() > 0) {
                        TreeView.this.tree.setSelectionPath(path);
                    }
                }
            }
        }

        private class AccessibleExplorerTree
        extends JTree.AccessibleJTree {
            AccessibleExplorerTree() {
                super(ExplorerTree.this);
            }

            @Override
            public String getAccessibleName() {
                return TreeView.this.getAccessibleContext().getAccessibleName();
            }

            @Override
            public String getAccessibleDescription() {
                return TreeView.this.getAccessibleContext().getAccessibleDescription();
            }
        }

        private class GuardedActions
        implements Mutex.Action<Object> {
            private int type;
            private Object p1;
            final Object ret;

            public GuardedActions(int type, Object p1) {
                this.type = type;
                this.p1 = p1;
                this.ret = Children.MUTEX.isReadAccess() || Children.MUTEX.isWriteAccess() ? this.run() : Children.MUTEX.readAccess((Mutex.Action)this);
            }

            public Object run() {
                switch (this.type) {
                    case 0: {
                        ExplorerTree.this.guardedPaint((Graphics)this.p1);
                        break;
                    }
                    case 1: {
                        ExplorerTree.this.guardedValidateTree();
                        break;
                    }
                    case 2: {
                        ExplorerTree.this.guardedDoLayout();
                        break;
                    }
                    case 3: {
                        ExplorerTree.this.processFocusEvent((FocusEvent)this.p1);
                        ExplorerTree.this.repaintSelection();
                        break;
                    }
                    default: {
                        throw new IllegalStateException("type: " + this.type);
                    }
                }
                return null;
            }
        }

    }

    private class ExplorerScrollPaneLayout
    extends ScrollPaneLayout {
        private ExplorerScrollPaneLayout() {
        }

        @Override
        public void layoutContainer(Container parent) {
            super.layoutContainer(parent);
            if (null != TreeView.this.searchPanel && TreeView.this.searchPanel.isVisible()) {
                Insets innerInsets = TreeView.this.getInnerInsets();
                Dimension prefSize = TreeView.this.searchPanel.getPreferredSize();
                TreeView.this.searchPanel.setBounds(innerInsets.left, parent.getHeight() - innerInsets.bottom - prefSize.height, parent.getWidth() - innerInsets.left - innerInsets.right, prefSize.height);
            }
        }
    }

    final class PopupSupport
    extends MouseAdapter
    implements Runnable,
    FocusListener,
    ActionListener {
        public final Action popup;

        PopupSupport() {
            this.popup = new AbstractAction(){

                @Override
                public void actionPerformed(ActionEvent evt) {
                    SwingUtilities.invokeLater(PopupSupport.this);
                }

                @Override
                public boolean isEnabled() {
                    return TreeView.this.isFocusOwner() || TreeView.this.tree.isFocusOwner();
                }
            };
        }

        @Override
        public void run() {
            Point p = TreeView.this.getPositionForPopup();
            if (p == null) {
                p = new Point(0, 0);
            }
            TreeView.this.createPopup(p.x, p.y);
        }

        @Override
        public void focusGained(FocusEvent ev) {
            ev.getComponent().removeFocusListener(this);
            if (DragDropUtilities.dragAndDropEnabled && TreeView.this.dragActive) {
                TreeView.this.setDragSource(true);
            }
        }

        @Override
        public void focusLost(FocusEvent ev) {
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            TreeView.this.tree.stopEditing();
            int selRow = TreeView.this.tree.getRowForLocation(e.getX(), e.getY());
            if (selRow != -1 && SwingUtilities.isLeftMouseButton(e) && MouseUtils.isDoubleClick((MouseEvent)e)) {
                TreePath selPath;
                Node node;
                Action a;
                if (TreeView.this.defaultActionEnabled && (a = TreeView.takeAction((node = Visualizer.findNode((selPath = TreeView.this.tree.getPathForLocation(e.getX(), e.getY())).getLastPathComponent())).getPreferredAction(), node)) != null) {
                    if (a.isEnabled()) {
                        a.actionPerformed(new ActionEvent((Object)node, 1001, ""));
                    } else {
                        Utilities.disabledActionBeep();
                    }
                    e.consume();
                    return;
                }
                if (TreeView.this.tree.isExpanded(selRow)) {
                    TreeView.this.tree.collapseRow(selRow);
                } else {
                    TreeView.this.tree.expandRow(selRow);
                }
            }
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            Node[] nodes = TreeView.this.manager.getSelectedNodes();
            TreeView.performPreferredActionOnNodes(nodes);
        }

    }

    class PopupAdapter
    extends MouseUtils.PopupMouseAdapter {
        PopupAdapter() {
        }

        protected void showPopup(MouseEvent e) {
            TreeView.this.tree.cancelEditing();
            int selRow = TreeView.this.tree.getRowForLocation(e.getX(), e.getY());
            if (selRow == -1 && !TreeView.this.isRootVisible()) {
                try {
                    TreeView.this.manager.setSelectedNodes(new Node[0]);
                }
                catch (PropertyVetoException exc) {
                    assert (false);
                }
            } else if (!TreeView.this.tree.isRowSelected(selRow)) {
                TreeView.this.tree.setSelectionRow(selRow);
            }
            if (selRow != -1 || !TreeView.this.isRootVisible()) {
                Point p = SwingUtilities.convertPoint(e.getComponent(), e.getX(), e.getY(), TreeView.this);
                TreeView.this.createPopup((int)p.getX(), (int)p.getY());
            }
        }
    }

    class TreePropertyListener
    implements VetoableChangeListener,
    PropertyChangeListener,
    TreeExpansionListener,
    TreeWillExpandListener,
    TreeSelectionListener,
    Runnable {
        private RequestProcessor.Task scheduled;
        private TreePath[] readAccessPaths;

        TreePropertyListener() {
        }

        @Override
        public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
            if (evt.getPropertyName().equals("selectedNodes")) {
                Node[] nodes = (Node[])evt.getNewValue();
                if (TreeView.this.isSelectionModeBroken(nodes)) {
                    throw new PropertyVetoException("selection mode " + TreeView.this.getSelectionMode() + " broken by " + Arrays.asList(nodes), evt);
                }
                if (!TreeView.this.selectionAccept(nodes)) {
                    throw new PropertyVetoException("selection " + Arrays.asList(nodes) + " rejected", evt);
                }
            }
        }

        @Override
        public final void propertyChange(PropertyChangeEvent evt) {
            if (TreeView.this.manager == null) {
                return;
            }
            final String prop = evt.getPropertyName();
            if (!(prop.equals("rootContext") || prop.equals("exploredContext") || prop.equals("selectedNodes"))) {
                return;
            }
            Children.MUTEX.readAccess(new Runnable(){

                @Override
                public void run() {
                    if (prop.equals("rootContext")) {
                        TreeView.this.synchronizeRootContext();
                    }
                    if (prop.equals("exploredContext")) {
                        TreeView.this.synchronizeExploredContext();
                    }
                    if (prop.equals("selectedNodes")) {
                        TreeView.this.synchronizeSelectedNodes();
                    }
                }
            });
        }

        @Override
        public synchronized void treeExpanded(TreeExpansionEvent ev) {
            class Request
            implements Runnable {
                private TreePath path;

                public Request(TreePath path) {
                    this.path = path;
                }

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    if (!SwingUtilities.isEventDispatchThread()) {
                        SwingUtilities.invokeLater(this);
                        return;
                    }
                    if (!Children.MUTEX.isReadAccess() && !Children.MUTEX.isWriteAccess()) {
                        Children.MUTEX.readAccess((Runnable)this);
                        return;
                    }
                    try {
                        if (!TreeView.this.tree.isVisible(this.path)) {
                            return;
                        }
                        if (TreeView.this.treeModel == null) {
                            return;
                        }
                        TreeNode myNode = (TreeNode)this.path.getLastPathComponent();
                        if (TreeView.this.treeModel.getPathToRoot(myNode)[0] != TreeView.this.treeModel.getRoot()) {
                            return;
                        }
                        int lastChildIndex = myNode.getChildCount() - 1;
                        if (lastChildIndex >= 0) {
                            TreeNode lastChild = myNode.getChildAt(lastChildIndex);
                            Rectangle base = TreeView.this.tree.getVisibleRect();
                            Rectangle b1 = TreeView.this.tree.getPathBounds(this.path);
                            Rectangle b2 = TreeView.this.tree.getPathBounds(new TreePath(TreeView.this.treeModel.getPathToRoot(lastChild)));
                            if (base != null && b1 != null && b2 != null) {
                                TreeView.this.tree.scrollRectToVisible(new Rectangle(base.x, b1.y, 1, b2.y - b1.y + b2.height));
                            }
                        }
                    }
                    finally {
                        this.path = null;
                    }
                }
            }
            VisualizerNode vn = (VisualizerNode)ev.getPath().getLastPathComponent();
            TreeView.this.visHolder.add(vn.getChildren());
            if (!TreeView.this.tree.getScrollsOnExpand()) {
                return;
            }
            RequestProcessor.Task t = this.scheduled;
            if (t != null) {
                t.cancel();
            }
            this.scheduled = ViewUtil.uiProcessor().post((Runnable)new Request(ev.getPath()), 250);
        }

        @Override
        public synchronized void treeCollapsed(TreeExpansionEvent ev) {
            class Request
            implements Runnable {
                private TreePath path;

                public Request(TreePath path) {
                    this.path = path;
                }

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    if (!SwingUtilities.isEventDispatchThread()) {
                        SwingUtilities.invokeLater(this);
                        return;
                    }
                    boolean expanded = true;
                    try {
                        expanded = TreeView.this.tree.isExpanded(this.path);
                        if (expanded) {
                            return;
                        }
                        if (!TreeView.this.tree.isVisible(this.path)) {
                            return;
                        }
                        if (TreeView.this.treeModel == null) {
                            return;
                        }
                        TreeNode myNode = (TreeNode)this.path.getLastPathComponent();
                        if (TreeView.this.treeModel.getPathToRoot(myNode)[0] != TreeView.this.treeModel.getRoot()) {
                            return;
                        }
                        TreeView.this.treeModel.nodeStructureChanged(myNode);
                    }
                    finally {
                        if (!expanded) {
                            VisualizerNode vn = (VisualizerNode)this.path.getLastPathComponent();
                            TreeView.this.visHolder.removeRecur(vn.getChildren(false));
                        }
                        this.path = null;
                    }
                }
            }
            ViewUtil.uiProcessor().post((Runnable)new Request(ev.getPath()), TIME_TO_COLLAPSE);
        }

        @Override
        public void valueChanged(TreeSelectionEvent ev) {
            TreePath[] paths = TreeView.this.tree.getSelectionPaths();
            if (paths == null) {
                if (ExplorerDnDManager.getDefault().isDnDActive()) {
                    return;
                }
                TreeView.this.callSelectionChanged(new Node[0]);
            } else {
                this.readAccessPaths = paths;
                Children.MUTEX.postReadRequest((Runnable)this);
            }
        }

        @Override
        public void run() {
            if (this.readAccessPaths == null) {
                return;
            }
            TreePath[] paths = this.readAccessPaths;
            this.readAccessPaths = null;
            ArrayList<Node> ll = new ArrayList<Node>(paths.length);
            for (int i = 0; i < paths.length; ++i) {
                Node n = Visualizer.findNode(paths[i].getLastPathComponent());
                n = TreeView.this.getOriginalNode(n);
                if (!this.isUnderRoot(TreeView.this.manager.getRootContext(), n)) continue;
                ll.add(n);
            }
            TreeView.this.callSelectionChanged(ll.toArray((T[])new Node[ll.size()]));
        }

        private boolean isUnderRoot(Node rootContext, Node node) {
            while (node != null) {
                if (node.equals((Object)rootContext)) {
                    return true;
                }
                node = node.getParentNode();
            }
            return false;
        }

        @Override
        public void treeWillCollapse(TreeExpansionEvent event) throws ExpandVetoException {
        }

        @Override
        public void treeWillExpand(TreeExpansionEvent event) throws ExpandVetoException {
            TreePath path = event.getPath();
            TreeView.this.prepareWaitCursor(DragDropUtilities.secureFindNode(path.getLastPathComponent()));
        }

    }

    private static class CursorR
    implements Runnable {
        private Component glassPane;
        private boolean show;

        private CursorR(Component cont, boolean show) {
            this.glassPane = cont;
            this.show = show;
        }

        @Override
        public void run() {
            TreeView.doShowWaitCursor(this.glassPane, this.show);
        }
    }

}

