/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.WeakListeners
 */
package org.openide.explorer.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.table.TableColumnModel;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.ListView;
import org.openide.explorer.view.NodeTableModel;
import org.openide.explorer.view.TableSheet;
import org.openide.explorer.view.Visualizer;
import org.openide.nodes.Node;
import org.openide.util.WeakListeners;

@Deprecated
public class ListTableView
extends ListView {
    private boolean tableChanging = false;
    private ExplorerManager manager;
    private PropertyChangeListener pchl;
    private TableSheet.ControlledTableView controlledTableView;
    private Listener listener;
    private Dimension prefSize;
    private JTable table;

    public ListTableView() {
        this(null);
    }

    public ListTableView(NodeTableModel ntm) {
        this.setVerticalScrollBarPolicy(21);
        this.setHorizontalScrollBarPolicy(31);
        this.setViewportView(null);
        JScrollPane listView = new JScrollPane(this.list);
        listView.setVerticalScrollBarPolicy(21);
        listView.setBorder(null);
        this.controlledTableView = ntm == null ? new TableSheet.ControlledTableView(listView) : new TableSheet.ControlledTableView(listView, ntm);
        this.setViewportView(this.controlledTableView.compoundScrollPane());
        this.listener = new Listener();
        this.delayedFireTableDataChanged();
        this.setPreferredSize(new Dimension(400, 400));
        this.table = this.controlledTableView.getTable();
    }

    public void setProperties(Node.Property[] props) {
        this.controlledTableView.setProperties(props);
    }

    public final void setTableAutoResizeMode(int mode) {
        this.controlledTableView.setAutoResizeMode(mode);
    }

    public final int getTableAutoResizeMode() {
        return this.controlledTableView.getAutoResizeMode();
    }

    public final void setTableColumnPreferredWidth(int index, int width) {
        this.controlledTableView.setColumnPreferredWidth(index, width);
    }

    public final int getTableColumnPreferredWidth(int index) {
        return this.controlledTableView.getColumnPreferredWidth(index);
    }

    public void setListPreferredWidth(int width) {
        this.controlledTableView.setControllingViewWidth(width);
        Dimension dim = this.getPreferredSize();
        this.table.setPreferredScrollableViewportSize(new Dimension(dim.width - width, dim.height));
    }

    public final int getListPreferredWidth() {
        return this.controlledTableView.getControllingViewWidth();
    }

    @Override
    public void setPreferredSize(Dimension dim) {
        super.setPreferredSize(dim);
        this.prefSize = dim;
    }

    @Override
    public Dimension getPreferredSize() {
        return this.prefSize;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        ExplorerManager newManager = ExplorerManager.find(this);
        if (newManager != this.manager) {
            if (this.manager != null) {
                this.manager.removePropertyChangeListener(this.pchl);
            }
            this.manager = newManager;
            this.pchl = WeakListeners.propertyChange((PropertyChangeListener)this.listener, (Object)this.manager);
            this.manager.addPropertyChangeListener(this.pchl);
            this.controlledTableView.setHeaderText(this.manager.getExploredContext().getDisplayName());
        }
        this.list.getModel().addListDataListener(this.listener);
        this.list.addFocusListener(this.listener);
        this.delayedFireTableDataChanged();
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.list.getModel().removeListDataListener(this.listener);
        this.list.removeFocusListener(this.listener);
        this.controlledTableView.setNodes(new Node[0]);
    }

    private void delayedFireTableDataChanged() {
        if (this.tableChanging) {
            return;
        }
        this.tableChanging = true;
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                if (ListTableView.this.list.getCellBounds(0, 0) != null) {
                    ListTableView.this.controlledTableView.setRowHeight(ListTableView.this.list.getCellBounds((int)0, (int)0).height);
                }
                ListTableView.this.changeTableModel();
                ListTableView.this.tableChanging = false;
            }
        });
    }

    private void changeTableModel() {
        Node[] nodes = new Node[this.list.getModel().getSize()];
        for (int i = 0; i < this.list.getModel().getSize(); ++i) {
            nodes[i] = Visualizer.findNode(this.list.getModel().getElementAt(i));
        }
        this.controlledTableView.setNodes(nodes);
    }

    private class Listener
    implements PropertyChangeListener,
    FocusListener,
    ListDataListener {
        Listener() {
        }

        @Override
        public void intervalAdded(ListDataEvent e) {
            ListTableView.this.delayedFireTableDataChanged();
        }

        @Override
        public void intervalRemoved(ListDataEvent e) {
            ListTableView.this.delayedFireTableDataChanged();
        }

        @Override
        public void contentsChanged(ListDataEvent e) {
            ListTableView.this.delayedFireTableDataChanged();
        }

        @Override
        public void focusGained(FocusEvent evt) {
        }

        @Override
        public void focusLost(FocusEvent evt) {
            if (evt.isTemporary()) {
                return;
            }
            int selectedRow = ListTableView.this.list.getSelectedIndex();
            ListTableView.this.table.getSelectionModel().setAnchorSelectionIndex(selectedRow);
            ListTableView.this.table.getColumnModel().getSelectionModel().setAnchorSelectionIndex(0);
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("exploredContext".equals(evt.getPropertyName())) {
                ListTableView.this.controlledTableView.setHeaderText(ListTableView.this.manager.getExploredContext().getDisplayName());
            }
        }
    }

}

