/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package org.openide.explorer;

import java.io.IOException;
import org.openide.nodes.Node;

public interface ExtendedDelete {
    public boolean delete(Node[] var1) throws IOException;
}

