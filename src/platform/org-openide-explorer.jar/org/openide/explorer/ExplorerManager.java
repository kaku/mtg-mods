/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Handle
 *  org.openide.nodes.NodeAdapter
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeNotFoundException
 *  org.openide.nodes.NodeOp
 *  org.openide.util.Exceptions
 *  org.openide.util.Mutex
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.util.io.SafeException
 */
package org.openide.explorer;

import java.awt.Component;
import java.awt.Container;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamField;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.netbeans.modules.openide.explorer.ExplorerActionsImpl;
import org.openide.explorer.Bundle;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeAdapter;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeNotFoundException;
import org.openide.nodes.NodeOp;
import org.openide.util.Exceptions;
import org.openide.util.Mutex;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.util.io.SafeException;

public final class ExplorerManager
implements Serializable,
Cloneable {
    static final long serialVersionUID = -4330330689803575792L;
    public static final String PROP_ROOT_CONTEXT = "rootContext";
    public static final String PROP_EXPLORED_CONTEXT = "exploredContext";
    public static final String PROP_SELECTED_NODES = "selectedNodes";
    public static final String PROP_NODE_CHANGE = "nodeChange";
    static RequestProcessor selectionProcessor;
    private static final int SELECTION_SYNC_DELAY = 200;
    private static final ObjectStreamField[] serialPersistentFields;
    private transient VetoableChangeSupport vetoableSupport;
    private transient PropertyChangeSupport propertySupport;
    private Node rootContext;
    private Node exploredContext;
    private Node[] selectedNodes;
    private transient Listener listener;
    private transient NodeListener weakListener;
    private RequestProcessor.Task selectionSyncTask;
    private ExplorerActionsImpl actions;
    private transient Object LOCK;
    static boolean SCHEDULE_REMOVE_ASYNCH;

    public ExplorerManager() {
        this.init();
    }

    private void init() {
        this.exploredContext = this.rootContext = Node.EMPTY;
        this.selectedNodes = new Node[0];
        this.listener = new Listener();
        this.weakListener = NodeOp.weakNodeListener((NodeListener)this.listener, (Object)null);
        this.LOCK = new Object();
    }

    public ExplorerManager clone() {
        ExplorerManager em = new ExplorerManager();
        em.rootContext = this.rootContext;
        em.exploredContext = this.exploredContext;
        em.selectedNodes = this.selectedNodes;
        return em;
    }

    public Node[] getSelectedNodes() {
        return this.selectedNodes;
    }

    private boolean equalNodes(Node[] arr1, Node[] arr2) {
        int i;
        if (!Arrays.equals((Object[])arr1, (Object[])arr2)) {
            return false;
        }
        if (arr1 == null || arr1.length == 0) {
            return true;
        }
        for (i = 0; i < arr1.length && Arrays.equals(NodeOp.createPath((Node)arr1[i], (Node)null), NodeOp.createPath((Node)arr2[i], (Node)null)); ++i) {
        }
        return i == arr1.length;
    }

    public final void setSelectedNodes(Node[] value) throws PropertyVetoException {
        class AtomicSetSelectedNodes
        implements Runnable {
            public PropertyVetoException veto;
            private boolean doFire;
            private Node[] oldValue;
            private Node[] newValue;
            final /* synthetic */ Node[] val$value;

            AtomicSetSelectedNodes() {
                this.val$value = var2_2;
            }

            private void checkAndSet() {
                if (this.val$value == null) {
                    throw new IllegalArgumentException(Bundle.EXC_NodeCannotBeNull());
                }
                if (this$0.equalNodes(this.val$value, this$0.selectedNodes)) {
                    return;
                }
                ArrayList<Node> validNodes = null;
                for (int i = 0; i < this.val$value.length; ++i) {
                    if (this.val$value[i] == null) {
                        throw new IllegalArgumentException(Bundle.EXC_NoElementOfNodeSelectionMayBeNull());
                    }
                    if (!this$0.isUnderRoot(this.val$value[i])) {
                        if (validNodes != null) continue;
                        validNodes = new ArrayList<Node>(this.val$value.length);
                        for (int j = 0; j < i; ++j) {
                            validNodes.add(this.val$value[i]);
                        }
                        continue;
                    }
                    if (validNodes == null) continue;
                    validNodes.add(this.val$value[i]);
                }
                if (validNodes != null) {
                    this.newValue = validNodes.toArray((T[])new Node[validNodes.size()]);
                    if (this$0.equalNodes(this.newValue, this$0.selectedNodes)) {
                        return;
                    }
                } else {
                    this.newValue = this.val$value;
                }
                if (this.newValue.length != 0 && this$0.vetoableSupport != null) {
                    try {
                        this$0.vetoableSupport.fireVetoableChange("selectedNodes", this$0.selectedNodes, this.newValue);
                    }
                    catch (PropertyVetoException ex) {
                        this.veto = ex;
                        return;
                    }
                }
                this.updateSelection();
            }

            private void updateSelection() {
                this.oldValue = this$0.selectedNodes;
                this$0.addRemoveListeners(false);
                this$0.selectedNodes = this.newValue;
                this$0.addRemoveListeners(true);
                this.doFire = true;
            }

            public void fire() {
                if (this.doFire) {
                    this$0.fireInAWT("selectedNodes", this.oldValue, this$0.selectedNodes);
                }
            }

            @Override
            public void run() {
                this.checkAndSet();
            }
        }
        AtomicSetSelectedNodes setNodes = new AtomicSetSelectedNodes(this, value);
        Children.MUTEX.readAccess((Runnable)setNodes);
        setNodes.fire();
        if (setNodes.veto != null) {
            throw setNodes.veto;
        }
    }

    public final Node getExploredContext() {
        return this.exploredContext;
    }

    public final void setExploredContext(Node value) {
        this.setExploredContext(value, new Node[0]);
    }

    public final void setExploredContext(Node value, Node[] selection) {
        class SetExploredContext
        implements Runnable {
            boolean doFire;
            Node oldValue;
            final /* synthetic */ Node val$value;
            final /* synthetic */ Node[] val$selection;

            SetExploredContext() {
                this.val$value = var2_2;
                this.val$selection = var3_3;
            }

            @Override
            public void run() {
                if (Utilities.compareObjects((Object)this.val$value, (Object)this$0.exploredContext)) {
                    this$0.setSelectedNodes0(this.val$selection);
                    return;
                }
                this$0.checkUnderRoot(this.val$value);
                this$0.setSelectedNodes0(this.val$selection);
                this.oldValue = this$0.exploredContext;
                this$0.addRemoveListeners(false);
                this$0.exploredContext = this.val$value;
                this$0.addRemoveListeners(true);
                this.doFire = true;
            }

            public void fire() {
                if (this.doFire) {
                    this$0.fireInAWT("exploredContext", (Object)this.oldValue, (Object)this.val$value);
                }
            }
        }
        SetExploredContext set = new SetExploredContext(this, value, selection);
        Children.MUTEX.readAccess((Runnable)set);
        set.fire();
    }

    public final void setExploredContextAndSelection(Node value, Node[] selection) throws PropertyVetoException {
        class SetExploredContextAndSelection
        implements Runnable {
            public PropertyVetoException veto;
            private boolean doFire;
            private Object oldValue;
            final /* synthetic */ Node val$value;
            final /* synthetic */ Node[] val$selection;

            SetExploredContextAndSelection() {
                this.val$value = var2_2;
                this.val$selection = var3_3;
            }

            @Override
            public void run() {
                try {
                    if (Utilities.compareObjects((Object)this.val$value, (Object)this$0.exploredContext)) {
                        this$0.setSelectedNodes(this.val$selection);
                        return;
                    }
                    this$0.checkUnderRoot(this.val$value);
                    this$0.setSelectedNodes(this.val$selection);
                    this.oldValue = this$0.exploredContext;
                    this$0.addRemoveListeners(false);
                    this$0.exploredContext = this.val$value;
                    this$0.addRemoveListeners(true);
                    this.doFire = true;
                }
                catch (PropertyVetoException ex) {
                    this.veto = ex;
                }
            }

            public void fire() {
                if (this.doFire) {
                    this$0.fireInAWT("exploredContext", this.oldValue, (Object)this$0.exploredContext);
                }
            }
        }
        SetExploredContextAndSelection set = new SetExploredContextAndSelection(this, value, selection);
        Children.MUTEX.readAccess((Runnable)set);
        set.fire();
        if (set.veto != null) {
            throw set.veto;
        }
    }

    final void addRemoveListeners(boolean add) {
        IdentityHashMap collect = new IdentityHashMap(333);
        this.collectNodes(this.exploredContext, collect);
        for (Node n : this.selectedNodes) {
            this.collectNodes(n, collect);
        }
        for (Node n2 : collect.keySet()) {
            if (add) {
                n2.addNodeListener(this.weakListener);
                continue;
            }
            n2.removeNodeListener(this.weakListener);
        }
    }

    private final void collectNodes(Node n, Map<Node, ?> collect) {
        assert (Children.MUTEX.isReadAccess());
        while (n != null && n != this.rootContext) {
            collect.put(n, null);
            n = n.getParentNode();
        }
    }

    final void setSelectedNodes0(Node[] nodes) {
        try {
            this.setSelectedNodes(nodes);
        }
        catch (PropertyVetoException e) {
            // empty catch block
        }
    }

    public final Node getRootContext() {
        return this.rootContext;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final void setRootContext(Node value) {
        class SetRootContext
        implements Runnable {
            final /* synthetic */ Node val$value;

            SetRootContext() {
                this.val$value = var2_2;
            }

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                Object object = this$0.LOCK;
                synchronized (object) {
                    if (this$0.rootContext.equals((Object)this.val$value)) {
                        return;
                    }
                    this$0.addRemoveListeners(false);
                    Node oldValue = this$0.rootContext;
                    this$0.rootContext = this.val$value;
                    oldValue.removeNodeListener(this$0.weakListener);
                    this$0.rootContext.addNodeListener(this$0.weakListener);
                    this$0.fireInAWT("rootContext", (Object)oldValue, (Object)this$0.rootContext);
                    Node[] newselection = this$0.getSelectedNodes();
                    if (!this$0.areUnderTarget(newselection, this$0.rootContext)) {
                        newselection = new Node[]{};
                    }
                    this$0.setExploredContext(this$0.rootContext, newselection);
                }
            }
        }
        if (value == null) {
            throw new IllegalArgumentException(Bundle.EXC_CannotHaveNullRootContext());
        }
        Object object = this.LOCK;
        synchronized (object) {
            if (this.rootContext.equals((Object)value)) {
                return;
            }
        }
        SetRootContext run = new SetRootContext(this, value);
        Children.MUTEX.readAccess((Runnable)run);
    }

    private boolean areUnderTarget(Node[] nodes, Node target) {
        block0 : for (int i = 0; i < nodes.length; ++i) {
            for (Node node = nodes[i]; node != null; node = node.getParentNode()) {
                if (node.equals((Object)target)) continue block0;
            }
            return false;
        }
        return true;
    }

    public synchronized void addPropertyChangeListener(PropertyChangeListener l) {
        if (this.propertySupport == null) {
            this.propertySupport = new PropertyChangeSupport(this);
        }
        this.propertySupport.addPropertyChangeListener(l);
    }

    public synchronized void removePropertyChangeListener(PropertyChangeListener l) {
        if (this.propertySupport != null) {
            this.propertySupport.removePropertyChangeListener(l);
        }
    }

    public synchronized void addVetoableChangeListener(VetoableChangeListener l) {
        if (this.vetoableSupport == null) {
            this.vetoableSupport = new VetoableChangeSupport(this);
        }
        this.vetoableSupport.addVetoableChangeListener(l);
    }

    public synchronized void removeVetoableChangeListener(VetoableChangeListener l) {
        if (this.vetoableSupport != null) {
            this.vetoableSupport.removeVetoableChangeListener(l);
        }
    }

    private boolean isUnderRoot(Node node) {
        while (node != null) {
            if (node.equals((Object)this.rootContext)) {
                return true;
            }
            node = node.getParentNode();
        }
        return false;
    }

    private void checkUnderRoot(Node value) {
        if (value != null && !this.isUnderRoot(value)) {
            throw new IllegalArgumentException(Bundle.EXC_ContextMustBeWithinRootContext(value.getDisplayName(), this.rootContext.getDisplayName()));
        }
    }

    final void waitFinished() {
        if (this.selectionSyncTask != null) {
            this.selectionSyncTask.waitFinished();
        }
    }

    private void writeObject(ObjectOutputStream os) throws IOException {
        os.writeObject(this);
        ObjectOutputStream.PutField fields = os.putFields();
        Node.Handle rCH = this.rootContext.getHandle();
        fields.put("root", (Object)rCH);
        fields.put("rootName", this.rootContext.getDisplayName());
        if (rCH != null) {
            String[] explored = this.exploredContext == null ? null : (this.isUnderRoot(this.exploredContext) ? NodeOp.createPath((Node)this.exploredContext, (Node)this.rootContext) : null);
            fields.put("explored", explored);
            LinkedList<String[]> selected = new LinkedList<String[]>();
            for (int i = 0; i < this.selectedNodes.length; ++i) {
                if (!this.isUnderRoot(this.selectedNodes[i])) continue;
                selected.add(NodeOp.createPath((Node)this.selectedNodes[i], (Node)this.rootContext));
            }
            fields.put("selected", selected.toArray());
        }
        os.writeFields();
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        this.init();
        Object firstObject = ois.readObject();
        if (firstObject != this) {
            this.readObjectOld((Node.Handle)firstObject, ois);
            return;
        }
        ObjectInputStream.GetField fields = ois.readFields();
        Node.Handle h = (Node.Handle)fields.get("root", null);
        String rootName = (String)fields.get("rootName", null);
        if (h == null) {
            throw new SafeException((Exception)new IOException("Could not restore Explorer window; the root node \"" + rootName + "\" is not persistent; override Node.getHandle to fix"));
        }
        String[] exploredCtx = (String[])fields.get("explored", null);
        Object[] selPaths = (Object[])fields.get("selected", null);
        try {
            Node root = h.getNode();
            if (root == null) {
                throw new IOException("Node.Handle.getNode (for " + rootName + ") should not return null");
            }
            this.restoreSelection(root, exploredCtx, Arrays.asList(selPaths));
        }
        catch (IOException ioe) {
            SafeException safe = new SafeException((Exception)ioe);
            if (!Utilities.compareObjects((Object)ioe.getMessage(), (Object)ioe.getLocalizedMessage())) {
                Exceptions.attachLocalizedMessage((Throwable)safe, (String)Bundle.EXC_handle_failed(rootName));
            }
            throw safe;
        }
    }

    private void readObjectOld(Node.Handle h, ObjectInputStream ois) throws IOException, ClassNotFoundException {
        String[] path;
        if (h == null) {
            return;
        }
        String[] rootCtx = (String[])ois.readObject();
        String[] exploredCtx = (String[])ois.readObject();
        LinkedList<String[]> ll = new LinkedList<String[]>();
        while ((path = (String[])ois.readObject()) != null) {
            ll.add(path);
        }
        Node root = ExplorerManager.findPath(h.getNode(), rootCtx);
        this.restoreSelection(root, exploredCtx, ll);
    }

    private void restoreSelection(final Node root, final String[] exploredCtx, final List<?> selectedPaths) {
        this.setRootContext(root);
        Mutex.EVENT.readAccess(new Runnable(){

            @Override
            public void run() {
                ArrayList<Node> selNodes = new ArrayList<Node>(selectedPaths.size());
                for (Object path : selectedPaths) {
                    selNodes.add(ExplorerManager.findPath(root, (String[])path));
                }
                Node[] newSelection = selNodes.toArray((T[])new Node[selNodes.size()]);
                if (exploredCtx != null) {
                    ExplorerManager.this.setExploredContext(ExplorerManager.findPath(root, exploredCtx), newSelection);
                } else {
                    ExplorerManager.this.setSelectedNodes0(newSelection);
                }
            }
        });
    }

    public static ExplorerManager find(Component comp) {
        do {
            if ((comp = comp.getParent()) != null) continue;
            return new ExplorerManager();
        } while (!(comp instanceof Provider));
        return ((Provider)((Object)comp)).getExplorerManager();
    }

    static Node findPath(Node r, String[] path) {
        try {
            return NodeOp.findPath((Node)r, (String[])path);
        }
        catch (NodeNotFoundException ex) {
            return ex.getClosestNode();
        }
    }

    static synchronized RequestProcessor getSelectionProcessor() {
        if (selectionProcessor == null) {
            selectionProcessor = new RequestProcessor("ExplorerManager-selection");
        }
        return selectionProcessor;
    }

    static synchronized ExplorerActionsImpl findExplorerActionsImpl(ExplorerManager em) {
        assert (em != null);
        if (em.actions == null) {
            em.actions = new ExplorerActionsImpl();
            em.actions.attach(em);
        }
        return em.actions;
    }

    final void fireInAWT(final String propName, final Object oldVal, final Object newVal) {
        if (this.propertySupport != null) {
            Mutex.EVENT.readAccess(new Runnable(){

                @Override
                public void run() {
                    ExplorerManager.this.propertySupport.firePropertyChange(propName, oldVal, newVal);
                }
            });
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void waitActionsFinished() {
        Class<ExplorerManager> class_ = ExplorerManager.class;
        synchronized (ExplorerManager.class) {
            ExplorerActionsImpl a = this.actions;
            // ** MonitorExit[var2_1] (shouldn't be in output)
            if (a != null) {
                a.waitFinished();
            }
            return;
        }
    }

    static {
        serialPersistentFields = new ObjectStreamField[]{new ObjectStreamField("root", Node.Handle.class), new ObjectStreamField("rootName", String.class), new ObjectStreamField("explored", String[].class), new ObjectStreamField("selected", Object[].class)};
        SCHEDULE_REMOVE_ASYNCH = true;
    }

    private class Listener
    extends NodeAdapter
    implements Runnable {
        Collection<Node> removeList;

        Listener() {
            this.removeList = new HashSet<Node>();
        }

        public void nodeDestroyed(NodeEvent ev) {
            if (ev.getNode().equals((Object)ExplorerManager.this.getRootContext())) {
                ExplorerManager.this.setRootContext(Node.EMPTY);
            } else {
                final Node n = ev.getNode();
                Runnable r = new Runnable(){

                    @Override
                    public void run() {
                        Listener.this.scheduleRemove(n);
                    }
                };
                if (ExplorerManager.SCHEDULE_REMOVE_ASYNCH) {
                    SwingUtilities.invokeLater(r);
                } else {
                    r.run();
                }
            }
        }

        public void propertyChange(PropertyChangeEvent ev) {
            ExplorerManager.this.fireInAWT("nodeChange", null, null);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void scheduleRemove(Node n) {
            Object object = ExplorerManager.this;
            synchronized (object) {
                if (ExplorerManager.this.selectionSyncTask == null) {
                    ExplorerManager.this.selectionSyncTask = ExplorerManager.getSelectionProcessor().create((Runnable)this);
                } else {
                    ExplorerManager.this.selectionSyncTask.cancel();
                }
            }
            object = this;
            synchronized (object) {
                this.removeList.add(n);
            }
            ExplorerManager.this.selectionSyncTask.schedule(200);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            Collection<Node> remove;
            if (!Children.MUTEX.isReadAccess()) {
                Children.MUTEX.readAccess((Runnable)this);
                return;
            }
            Listener listener = this;
            synchronized (listener) {
                remove = this.removeList;
                this.removeList = new HashSet<Node>();
            }
            if (!ExplorerManager.this.isUnderRoot(ExplorerManager.this.exploredContext)) {
                ExplorerManager.this.setExploredContext(ExplorerManager.this.rootContext);
                return;
            }
            LinkedList<Node> newSel = new LinkedList<Node>(Arrays.asList(ExplorerManager.this.getSelectedNodes()));
            Iterator<Node> it = remove.iterator();
            while (it.hasNext()) {
                Node n_remove = it.next();
                if (!newSel.contains((Object)n_remove)) continue;
                Node n_selection = newSel.get(newSel.indexOf((Object)n_remove));
                if (Arrays.equals(NodeOp.createPath((Node)n_remove, (Node)null), NodeOp.createPath((Node)n_selection, (Node)null)) && !this.isInParentChildren(n_remove)) continue;
                it.remove();
            }
            newSel.removeAll(remove);
            Iterator<Node> i = newSel.iterator();
            while (i.hasNext()) {
                Node n = i.next();
                if (ExplorerManager.this.isUnderRoot(n)) continue;
                i.remove();
            }
            Node[] selNodes = newSel.toArray((T[])new Node[newSel.size()]);
            ExplorerManager.this.setSelectedNodes0(selNodes);
        }

        private boolean isInParentChildren(Node node) {
            Node parent = node.getParentNode();
            return parent != null && Arrays.asList(parent.getChildren().getNodes()).contains((Object)node);
        }

    }

    public static interface Provider {
        public ExplorerManager getExplorerManager();
    }

}

