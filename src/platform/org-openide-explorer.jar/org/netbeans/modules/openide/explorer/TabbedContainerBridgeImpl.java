/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.ComponentConverter
 *  org.netbeans.swing.tabcontrol.ComponentConverter$Fixed
 *  org.netbeans.swing.tabcontrol.TabData
 *  org.netbeans.swing.tabcontrol.TabDataModel
 *  org.netbeans.swing.tabcontrol.TabbedContainer
 */
package org.netbeans.modules.openide.explorer;

import java.awt.Component;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.SingleSelectionModel;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.openide.explorer.TabbedContainerBridge;
import org.netbeans.swing.tabcontrol.ComponentConverter;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabbedContainer;

class TabbedContainerBridgeImpl
extends TabbedContainerBridge {
    @Override
    public void attachSelectionListener(JComponent jc, ChangeListener listener) {
        TabbedContainer cont = (TabbedContainer)jc;
        cont.getSelectionModel().addChangeListener(listener);
    }

    @Override
    public JComponent createTabbedContainer() {
        return new TabbedContainer(3);
    }

    @Override
    public void detachSelectionListener(JComponent jc, ChangeListener listener) {
        TabbedContainer cont = (TabbedContainer)jc;
        cont.getSelectionModel().removeChangeListener(listener);
    }

    @Override
    public Object[] getItems(JComponent jc) {
        TabbedContainer cont = (TabbedContainer)jc;
        List l = cont.getModel().getTabs();
        Object[] items = new Object[l.size()];
        for (int i = 0; i < items.length; ++i) {
            items[i] = ((TabData)l.get(i)).getUserObject();
        }
        return items;
    }

    @Override
    public Object getSelectedItem(JComponent jc) {
        Object result = null;
        TabbedContainer cont = (TabbedContainer)jc;
        int i = cont.getSelectionModel().getSelectedIndex();
        if (i != -1) {
            result = cont.getModel().getTab(i).getUserObject();
        }
        return result;
    }

    @Override
    public void setSelectedItem(JComponent jc, Object selection) {
        TabbedContainer cont = (TabbedContainer)jc;
        TabDataModel mdl = cont.getModel();
        int max = mdl.size();
        for (int i = 0; i < max; ++i) {
            TabData td = mdl.getTab(i);
            if (td.getUserObject() != selection) continue;
            cont.getSelectionModel().setSelectedIndex(i);
            break;
        }
    }

    @Override
    public boolean setSelectionByName(JComponent jc, String tabname) {
        TabbedContainer cont = (TabbedContainer)jc;
        TabDataModel mdl = cont.getModel();
        int max = mdl.size();
        for (int i = 0; i < max; ++i) {
            TabData td = mdl.getTab(i);
            if (!tabname.equals(td.getText())) continue;
            cont.getSelectionModel().setSelectedIndex(i);
            return true;
        }
        return false;
    }

    @Override
    public String getCurrentSelectedTabName(JComponent jc) {
        TabbedContainer cont = (TabbedContainer)jc;
        int sel = cont.getSelectionModel().getSelectedIndex();
        if (sel != -1) {
            TabData td = cont.getModel().getTab(sel);
            return td.getText();
        }
        return null;
    }

    @Override
    public void setInnerComponent(JComponent jc, JComponent inner) {
        TabbedContainer cont = (TabbedContainer)jc;
        ComponentConverter.Fixed cc = new ComponentConverter.Fixed((Component)inner);
        cont.setComponentConverter((ComponentConverter)cc);
    }

    @Override
    public JComponent getInnerComponent(JComponent jc) {
        TabbedContainer cont = (TabbedContainer)jc;
        return (JComponent)cont.getComponentConverter().getComponent(null);
    }

    @Override
    public void setItems(JComponent jc, Object[] objects, String[] titles) {
        TabbedContainer cont = (TabbedContainer)jc;
        assert (objects.length == titles.length);
        TabData[] td = new TabData[objects.length];
        for (int i = 0; i < objects.length; ++i) {
            td[i] = new TabData(objects[i], null, titles[i], null);
        }
        cont.getModel().setTabs(td);
    }
}

