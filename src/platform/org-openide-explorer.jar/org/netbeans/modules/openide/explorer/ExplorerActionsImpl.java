/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.datatransfer.ExTransferable
 *  org.openide.util.datatransfer.ExTransferable$Multi
 *  org.openide.util.datatransfer.MultiTransferObject
 *  org.openide.util.datatransfer.PasteType
 */
package org.netbeans.modules.openide.explorer;

import java.awt.EventQueue;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.FlavorEvent;
import java.awt.datatransfer.FlavorListener;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.netbeans.modules.openide.explorer.Bundle;
import org.netbeans.modules.openide.explorer.ExternalDragAndDrop;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExtendedDelete;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.datatransfer.ExTransferable;
import org.openide.util.datatransfer.MultiTransferObject;
import org.openide.util.datatransfer.PasteType;

public final class ExplorerActionsImpl {
    private static final RequestProcessor RP = new RequestProcessor("Explorer Actions");
    private static final Logger LOG = Logger.getLogger(ExplorerActionsImpl.class.getName());
    private final CopyCutActionPerformer copyActionPerformer;
    private final CopyCutActionPerformer cutActionPerformer;
    private final DeleteActionPerformer deleteActionPerformerConfirm;
    private final DeleteActionPerformer deleteActionPerformerNoConfirm;
    private final OwnPaste pasteActionPerformer;
    private ActionStateUpdater actionStateUpdater;
    private ExplorerManager manager;

    public ExplorerActionsImpl() {
        this.copyActionPerformer = new CopyCutActionPerformer(true);
        this.cutActionPerformer = new CopyCutActionPerformer(false);
        this.deleteActionPerformerConfirm = new DeleteActionPerformer(true);
        this.deleteActionPerformerNoConfirm = new DeleteActionPerformer(false);
        this.pasteActionPerformer = new OwnPaste();
    }

    public Action copyAction() {
        return this.copyActionPerformer;
    }

    public Action cutAction() {
        return this.cutActionPerformer;
    }

    public Action deleteAction(boolean confirm) {
        return confirm ? this.deleteActionPerformerConfirm : this.deleteActionPerformerNoConfirm;
    }

    public Action pasteAction() {
        return this.pasteActionPerformer;
    }

    public synchronized void attach(ExplorerManager m) {
        if (this.manager != null) {
            this.detach();
        }
        this.manager = m;
        this.actionStateUpdater = new ActionStateUpdater(this.manager);
        this.actionStateUpdater.schedule();
    }

    public synchronized void detach() {
        if (this.manager == null || this.actionStateUpdater == null) {
            return;
        }
        this.actionStateUpdater.unlisten(this.manager);
        this.actionStateUpdater = null;
        this.stopActions();
        this.manager = null;
    }

    final void stopActions() {
        assert (EventQueue.isDispatchThread());
        if (this.copyActionPerformer != null) {
            this.copyActionPerformer.setEnabled(false);
            this.cutActionPerformer.setEnabled(false);
            this.deleteActionPerformerConfirm.setEnabled(false);
            this.deleteActionPerformerNoConfirm.setEnabled(false);
            this.pasteActionPerformer.setEnabled(false);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void updateActions(boolean updatePasteAction) {
        ExplorerManager m;
        int k;
        assert (!EventQueue.isDispatchThread());
        ExplorerActionsImpl explorerActionsImpl = this;
        synchronized (explorerActionsImpl) {
            m = this.manager;
        }
        if (m == null) {
            return;
        }
        Node[] path = m.getSelectedNodes();
        int n = k = path != null ? path.length : 0;
        if (k > 0) {
            int i;
            boolean incest = false;
            if (k > 1) {
                HashMap<Node, Object> allNodes = new HashMap<Node, Object>(101);
                for (i = 0; i < k; ++i) {
                    if (this.checkParents(path[i], allNodes)) continue;
                    incest = true;
                    break;
                }
            }
            for (i = 0; i < k; ++i) {
                if (!incest && path[i].canCopy()) continue;
                this.copyActionPerformer.toEnabled(false);
                break;
            }
            if (i == k) {
                this.copyActionPerformer.toEnabled(true);
            }
            for (i = 0; i < k; ++i) {
                if (!incest && path[i].canCut()) continue;
                this.cutActionPerformer.toEnabled(false);
                break;
            }
            if (i == k) {
                this.cutActionPerformer.toEnabled(true);
            }
            for (i = 0; i < k; ++i) {
                if (!incest && path[i].canDestroy()) continue;
                this.deleteActionPerformerConfirm.toEnabled(false);
                this.deleteActionPerformerNoConfirm.toEnabled(false);
                break;
            }
            if (i == k) {
                this.deleteActionPerformerConfirm.toEnabled(true);
                this.deleteActionPerformerNoConfirm.toEnabled(true);
            }
        } else {
            this.copyActionPerformer.toEnabled(false);
            this.cutActionPerformer.toEnabled(false);
            this.deleteActionPerformerConfirm.toEnabled(false);
            this.deleteActionPerformerNoConfirm.toEnabled(false);
        }
        if (updatePasteAction) {
            this.updatePasteAction(path);
        }
    }

    private boolean checkParents(Node node, HashMap<Node, Object> set) {
        if (set.get((Object)node) != null) {
            return false;
        }
        set.put(node, this);
        do {
            if ((node = node.getParentNode()) != null) continue;
            return true;
        } while (set.put(node, (Object)node) != this);
        return false;
    }

    private void updatePasteAction(Node[] path) {
        ExplorerManager man = this.manager;
        if (man == null) {
            this.pasteActionPerformer.setPasteTypes(null);
            return;
        }
        if (path != null && path.length > 1) {
            this.pasteActionPerformer.setPasteTypes(null);
            return;
        }
        Node node = man.getExploredContext();
        Node[] selectedNodes = man.getSelectedNodes();
        if (selectedNodes != null && selectedNodes.length == 1) {
            node = selectedNodes[0];
        }
        if (node != null) {
            if (this.actionStateUpdater != null) {
                Transferable trans = this.actionStateUpdater.getTransferable();
                if (trans != null) {
                    this.updatePasteTypes(trans, node);
                }
            } else {
                LOG.fine("#126145: caused by http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6322854");
            }
        }
    }

    private void updatePasteTypes(Transferable trans, Node pan) {
        if (trans != null) {
            PasteType[] pasteTypes;
            PasteType[] arrpasteType = pasteTypes = pan == null ? new PasteType[]{} : pan.getPasteTypes(trans);
            if (pasteTypes.length != 0) {
                this.pasteActionPerformer.setPasteTypes(pasteTypes);
                return;
            }
            if (trans.isDataFlavorSupported(ExTransferable.multiFlavor)) {
                try {
                    MultiTransferObject obj = (MultiTransferObject)trans.getTransferData(ExTransferable.multiFlavor);
                    int count = obj.getCount();
                    boolean ok = true;
                    Transferable[] t = new Transferable[count];
                    PasteType[] p = new PasteType[count];
                    for (int i = 0; i < count; ++i) {
                        t[i] = obj.getTransferableAt(i);
                        PasteType[] arrpasteType2 = pasteTypes = pan == null ? new PasteType[]{} : pan.getPasteTypes(t[i]);
                        if (pasteTypes.length == 0) {
                            ok = false;
                            break;
                        }
                        p[i] = pasteTypes[0];
                    }
                    if (ok) {
                        PasteType[] arrOfPaste = new PasteType[]{new MultiPasteType(t, p)};
                        this.pasteActionPerformer.setPasteTypes(arrOfPaste);
                        return;
                    }
                }
                catch (UnsupportedFlavorException e) {
                }
                catch (IOException e) {
                    // empty catch block
                }
            }
        }
        this.pasteActionPerformer.setPasteTypes(null);
    }

    private static Transferable getTransferableOwner(Node node, boolean copyCut) {
        try {
            return copyCut ? node.clipboardCopy() : node.clipboardCut();
        }
        catch (IOException e) {
            LOG.log(Level.WARNING, null, e);
            return null;
        }
    }

    public static Transferable getTransferableOwner(Node[] sel, boolean copyCut) {
        Transferable trans;
        if (sel.length != 1) {
            Transferable[] arrayTrans = new Transferable[sel.length];
            for (int i = 0; i < sel.length; ++i) {
                arrayTrans[i] = ExplorerActionsImpl.getTransferableOwner(sel[i], copyCut);
                if (arrayTrans[i] != null) continue;
                return null;
            }
            trans = ExternalDragAndDrop.maybeAddExternalFileDnd(new ExTransferable.Multi(arrayTrans));
        } else {
            trans = ExplorerActionsImpl.getTransferableOwner(sel[0], copyCut);
        }
        return trans;
    }

    public static Clipboard getClipboard() {
        if (GraphicsEnvironment.isHeadless()) {
            return null;
        }
        Clipboard c = (Clipboard)Lookup.getDefault().lookup(Clipboard.class);
        if (c == null) {
            c = Toolkit.getDefaultToolkit().getSystemClipboard();
        }
        return c;
    }

    final void syncActions() {
        this.copyActionPerformer.syncEnable();
        this.cutActionPerformer.syncEnable();
        this.deleteActionPerformerConfirm.syncEnable();
        this.deleteActionPerformerNoConfirm.syncEnable();
        this.pasteActionPerformer.syncEnable();
    }

    private boolean actionsUpdateScheduled() {
        ActionStateUpdater asu = this.actionStateUpdater;
        return asu != null ? asu.updateScheduled() : false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final void waitFinished() {
        ActionStateUpdater u = this.actionStateUpdater;
        ExplorerActionsImpl explorerActionsImpl = this;
        synchronized (explorerActionsImpl) {
            u = this.actionStateUpdater;
        }
        if (u == null) {
            return;
        }
        u.waitFinished();
        if (EventQueue.isDispatchThread()) {
            u.run();
        } else {
            try {
                EventQueue.invokeAndWait(u);
            }
            catch (Exception ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
    }

    private class ActionStateUpdater
    implements PropertyChangeListener,
    FlavorListener,
    Runnable {
        private final RequestProcessor.Task timer;
        private final PropertyChangeListener weakL;
        private FlavorListener flavL;
        private Transferable trans;

        ActionStateUpdater(ExplorerManager m) {
            this.timer = RP.create((Runnable)this);
            this.weakL = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)m);
            m.addPropertyChangeListener(this.weakL);
        }

        void unlisten(ExplorerManager m) {
            m.removePropertyChangeListener(this.weakL);
        }

        boolean updateScheduled() {
            return this.timer.getDelay() > 0;
        }

        @Override
        public synchronized void propertyChange(PropertyChangeEvent e) {
            this.schedule();
        }

        @Override
        public void flavorsChanged(FlavorEvent ev) {
            this.schedule();
        }

        @Override
        public void run() {
            if (EventQueue.isDispatchThread()) {
                ExplorerActionsImpl.this.syncActions();
            } else {
                ExplorerActionsImpl.this.updateActions(false);
                EventQueue.invokeLater(this);
                this.registerListener();
                this.updateTrans();
                ExplorerActionsImpl.this.updateActions(true);
                EventQueue.invokeLater(this);
            }
        }

        private void registerListener() {
            Clipboard c;
            if (this.flavL == null && (c = ExplorerActionsImpl.getClipboard()) != null) {
                this.flavL = (FlavorListener)WeakListeners.create(FlavorListener.class, (EventListener)this, (Object)c);
                c.addFlavorListener(this.flavL);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void updateTrans() {
            Clipboard clipboard = ExplorerActionsImpl.getClipboard();
            if (clipboard == null) {
                return;
            }
            Transferable t = clipboard.getContents(ExplorerActionsImpl.this);
            ActionStateUpdater actionStateUpdater = this;
            synchronized (actionStateUpdater) {
                this.trans = t;
            }
        }

        final Transferable getTransferable() {
            return this.trans;
        }

        public void update() {
            if (EventQueue.isDispatchThread()) {
                try {
                    this.timer.waitFinished(100);
                }
                catch (InterruptedException ex) {
                    LOG.log(Level.FINE, null, ex);
                }
            } else {
                this.timer.waitFinished();
            }
        }

        private void schedule() {
            ExplorerActionsImpl.this.copyActionPerformer.toEnabled(false);
            ExplorerActionsImpl.this.cutActionPerformer.toEnabled(false);
            ExplorerActionsImpl.this.deleteActionPerformerConfirm.toEnabled(false);
            ExplorerActionsImpl.this.deleteActionPerformerNoConfirm.toEnabled(false);
            ExplorerActionsImpl.this.pasteActionPerformer.toEnabled(false);
            EventQueue.invokeLater(this);
            this.timer.schedule(0);
        }

        final void waitFinished() {
            this.timer.waitFinished();
        }
    }

    private class DeleteActionPerformer
    extends BaseAction
    implements Runnable {
        private boolean confirmDelete;

        DeleteActionPerformer(boolean confirmDelete) {
            super();
            this.confirmDelete = confirmDelete;
        }

        @Override
        public boolean isEnabled() {
            return super.isEnabled();
        }

        @Override
        public void actionPerformed(ActionEvent ev) {
            ExplorerManager em = ExplorerActionsImpl.this.manager;
            if (em == null) {
                return;
            }
            Node[] sel = em.getSelectedNodes();
            if (sel == null || sel.length == 0) {
                return;
            }
            for (ExtendedDelete del : Lookup.getDefault().lookupAll(ExtendedDelete.class)) {
                try {
                    if (!del.delete(sel)) continue;
                    return;
                }
                catch (IOException e) {
                    Exceptions.printStackTrace((Throwable)e);
                    return;
                }
            }
            if (!this.confirmDelete || this.doConfirm(sel)) {
                try {
                    em.setSelectedNodes(new Node[0]);
                }
                catch (PropertyVetoException e) {
                    // empty catch block
                }
                this.doDestroy(sel);
                Mutex.EVENT.readAccess((Runnable)this);
            }
        }

        @Override
        public void run() {
            assert (EventQueue.isDispatchThread());
            this.setEnabled(false);
        }

        private boolean doConfirm(Node[] sel) {
            String message;
            String title;
            boolean customDelete = true;
            for (int i = 0; i < sel.length; ++i) {
                if (Boolean.TRUE.equals(sel[i].getValue("customDelete"))) continue;
                customDelete = false;
                break;
            }
            if (customDelete) {
                return true;
            }
            if (sel.length == 1) {
                message = Bundle.MSG_ConfirmDeleteObject(sel[0].getDisplayName());
                title = Bundle.MSG_ConfirmDeleteObjectTitle();
            } else {
                message = Bundle.MSG_ConfirmDeleteObjects(sel.length);
                title = Bundle.MSG_ConfirmDeleteObjectsTitle();
            }
            NotifyDescriptor.Confirmation desc = new NotifyDescriptor.Confirmation((Object)message, title, 0);
            return NotifyDescriptor.YES_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)desc));
        }

        private void doDestroy(Node[] sel) {
            for (int i = 0; i < sel.length; ++i) {
                try {
                    sel[i].destroy();
                    continue;
                }
                catch (IOException e) {
                    Exceptions.printStackTrace((Throwable)e);
                }
            }
        }
    }

    private class CopyCutActionPerformer
    extends BaseAction {
        private boolean copyCut;

        public CopyCutActionPerformer(boolean b) {
            super();
            this.copyCut = b;
        }

        @Override
        public boolean isEnabled() {
            return super.isEnabled();
        }

        @Override
        public void actionPerformed(ActionEvent ev) {
            Clipboard clipboard;
            ExplorerManager em = ExplorerActionsImpl.this.manager;
            if (em == null) {
                return;
            }
            Node[] sel = em.getSelectedNodes();
            Transferable trans = ExplorerActionsImpl.getTransferableOwner(sel, this.copyCut);
            if (trans != null && (clipboard = ExplorerActionsImpl.getClipboard()) != null) {
                clipboard.setContents(trans, new StringSelection(""));
            }
        }
    }

    private static abstract class BaseAction
    extends AbstractAction {
        private volatile Boolean toEnable;

        private BaseAction() {
        }

        public void toEnabled(boolean e) {
            this.toEnable = e;
        }

        public void syncEnable() {
            assert (EventQueue.isDispatchThread());
            if (this.toEnable != null) {
                this.setEnabled(this.toEnable);
                this.toEnable = null;
            }
        }
    }

    private class OwnPaste
    extends BaseAction {
        private PasteType[] pasteTypes;

        OwnPaste() {
        }

        @Override
        public boolean isEnabled() {
            return super.isEnabled();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void setPasteTypes(PasteType[] arr) {
            OwnPaste ownPaste = this;
            synchronized (ownPaste) {
                this.pasteTypes = arr;
            }
            LOG.log(Level.FINER, "setPasteTypes for {0}", Arrays.toString((Object[])arr));
            this.toEnabled(arr != null);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            PasteType[] arr;
            OwnPaste ownPaste = this;
            synchronized (ownPaste) {
                arr = this.pasteTypes;
            }
            if (arr != null && arr.length > 0) {
                try {
                    arr[0].paste();
                    return;
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            Utilities.disabledActionBeep();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public Object getValue(String s) {
            if ("delegates".equals(s)) {
                ActionStateUpdater asu;
                Object now;
                String prev = "";
                if (LOG.isLoggable(Level.FINE)) {
                    OwnPaste ownPaste = this;
                    synchronized (ownPaste) {
                        prev = Arrays.toString(this.pasteTypes);
                    }
                }
                if ((asu = ExplorerActionsImpl.this.actionStateUpdater) != null) {
                    asu.update();
                }
                if (LOG.isLoggable(Level.FINE)) {
                    OwnPaste ownPaste = this;
                    synchronized (ownPaste) {
                        now = Arrays.toString(this.pasteTypes);
                    }
                    if (now == null) {
                        now = "";
                    }
                    if (prev.equals(now)) {
                        LOG.log(Level.FINER, "getDelegates {0}", now);
                    } else {
                        LOG.log(Level.FINE, "Delegates updated. Before: {0}", prev);
                        LOG.log(Level.FINE, "Delegates updated. After : {0}", now);
                    }
                }
                now = this;
                synchronized (now) {
                    return this.pasteTypes;
                }
            }
            return super.getValue(s);
        }
    }

    private static class MultiPasteType
    extends PasteType {
        Transferable[] t;
        PasteType[] p;

        MultiPasteType(Transferable[] t, PasteType[] p) {
            this.t = t;
            this.p = p;
        }

        public Transferable paste() throws IOException {
            int size = this.p.length;
            Transferable[] arr = new Transferable[size];
            for (int i = 0; i < size; ++i) {
                Transferable newTransferable = this.p[i].paste();
                arr[i] = newTransferable != null ? newTransferable : this.t[i];
            }
            return new ExTransferable.Multi(arr);
        }
    }

}

