/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.NodeAcceptor
 *  org.openide.nodes.NodeOperation
 *  org.openide.util.UserCancelException
 */
package org.netbeans.modules.openide.explorer;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.LayoutManager;
import javax.swing.JDialog;
import javax.swing.JPanel;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertySheet;
import org.openide.explorer.view.BeanTreeView;
import org.openide.nodes.Node;
import org.openide.nodes.NodeAcceptor;
import org.openide.nodes.NodeOperation;
import org.openide.util.UserCancelException;

public final class NodeOperationImpl
extends NodeOperation {
    private static CustomEditorAccessor accessor;

    public boolean customize(Node node) {
        Component customizer = node.getCustomizer();
        if (customizer == null) {
            return false;
        }
        JDialog d = new JDialog();
        d.setModal(false);
        d.setTitle(node.getDisplayName());
        d.getContentPane().setLayout(new BorderLayout());
        d.getContentPane().add(customizer, "Center");
        d.setDefaultCloseOperation(2);
        d.pack();
        d.setVisible(true);
        return true;
    }

    public void explore(Node n) {
        JDialog d = new JDialog();
        d.setTitle(n.getDisplayName());
        d.setModal(false);
        d.getContentPane().setLayout(new BorderLayout());
        EP p = new EP();
        p.getExplorerManager().setRootContext(n);
        p.setLayout(new BorderLayout());
        p.add((Component)new BeanTreeView(), "Center");
        d.getContentPane().add((Component)p, "Center");
        d.setDefaultCloseOperation(2);
        d.pack();
        d.setVisible(true);
    }

    public Node[] select(String title, String rootTitle, Node root, NodeAcceptor acceptor, Component top) throws UserCancelException {
        JDialog d = new JDialog();
        d.setTitle(title);
        d.setModal(true);
        d.getContentPane().setLayout(new BorderLayout());
        EP p = new EP();
        p.getExplorerManager().setRootContext(root);
        p.setLayout(new BorderLayout());
        p.add((Component)new BeanTreeView(), "Center");
        d.getContentPane().add((Component)p, "Center");
        if (top != null) {
            d.getContentPane().add(top, "North");
        }
        d.pack();
        d.setVisible(true);
        Node[] nodes = p.getExplorerManager().getSelectedNodes();
        d.dispose();
        return nodes;
    }

    public void showProperties(Node n) {
        this.showProperties(new Node[]{n});
    }

    public void showProperties(Node[] nodes) {
        PropertySheet ps = new PropertySheet();
        ps.setNodes(nodes);
        JDialog d = new JDialog();
        d.setTitle("Properties");
        d.setModal(true);
        d.getContentPane().setLayout(new BorderLayout());
        d.getContentPane().add((Component)ps, "Center");
        d.pack();
        d.setVisible(true);
        d.dispose();
    }

    public /* varargs */ void showCustomEditorDialog(Node.Property<?> property, Object ... beans) {
        if (!EventQueue.isDispatchThread()) {
            throw new IllegalStateException();
        }
        if (accessor == null) {
            try {
                Class.forName(PropertyEnv.class.getName(), true, this.getClass().getClassLoader());
            }
            catch (ClassNotFoundException ex) {
                // empty catch block
            }
        }
        accessor.showDialog(property, beans);
    }

    public static void registerCustomEditorAccessor(CustomEditorAccessor acc) {
        accessor = acc;
    }

    public static interface CustomEditorAccessor {
        public void showDialog(Node.Property var1, Object[] var2);
    }

    private static final class EP
    extends JPanel
    implements ExplorerManager.Provider {
        private ExplorerManager em = new ExplorerManager();

        private EP() {
        }

        @Override
        public ExplorerManager getExplorerManager() {
            return this.em;
        }
    }

}

