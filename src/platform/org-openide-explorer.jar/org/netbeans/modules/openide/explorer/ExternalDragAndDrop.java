/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.datatransfer.ExTransferable
 *  org.openide.util.datatransfer.ExTransferable$Multi
 *  org.openide.util.datatransfer.ExTransferable$Single
 *  org.openide.util.datatransfer.MultiTransferObject
 */
package org.netbeans.modules.openide.explorer;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.datatransfer.ExTransferable;
import org.openide.util.datatransfer.MultiTransferObject;

public class ExternalDragAndDrop {
    private ExternalDragAndDrop() {
    }

    public static Transferable maybeAddExternalFileDnd(ExTransferable.Multi multi) {
        ExTransferable.Multi res = multi;
        try {
            MultiTransferObject mto = (MultiTransferObject)multi.getTransferData(ExTransferable.multiFlavor);
            final ArrayList fileList = new ArrayList(mto.getCount());
            for (int i = 0; i < mto.getCount(); ++i) {
                if (!mto.isDataFlavorSupported(i, DataFlavor.javaFileListFlavor)) continue;
                List list = (List)mto.getTransferData(i, DataFlavor.javaFileListFlavor);
                fileList.addAll(list);
            }
            if (!fileList.isEmpty()) {
                ExTransferable fixed = ExTransferable.create((Transferable)multi);
                fixed.put(new ExTransferable.Single(DataFlavor.javaFileListFlavor){

                    protected Object getData() throws IOException, UnsupportedFlavorException {
                        return fileList;
                    }
                });
                res = fixed;
            }
        }
        catch (UnsupportedFlavorException ex) {
            Logger.getLogger(ExternalDragAndDrop.class.getName()).log(Level.INFO, null, ex);
        }
        catch (IOException ex) {
            Logger.getLogger(ExternalDragAndDrop.class.getName()).log(Level.INFO, null, ex);
        }
        return res;
    }

}

