/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.openide.explorer;

import java.util.Map;
import java.util.WeakHashMap;

public class TTVEnvBridge {
    private static Map<Object, TTVEnvBridge> bridges = new WeakHashMap<Object, TTVEnvBridge>();
    Object[] beans = null;

    private TTVEnvBridge() {
    }

    public static TTVEnvBridge getInstance(Object identifier) {
        TTVEnvBridge result = bridges.get(identifier);
        if (result == null) {
            result = new TTVEnvBridge();
            bridges.put(identifier, result);
        }
        return result;
    }

    public static TTVEnvBridge findInstance(Object identifier) {
        return bridges.get(identifier);
    }

    public void setCurrentBeans(Object[] o) {
        this.beans = o;
    }

    public void clear() {
        this.beans = null;
    }

    public Object[] getCurrentBeans() {
        if (this.beans == null) {
            return new Object[0];
        }
        return this.beans;
    }
}

