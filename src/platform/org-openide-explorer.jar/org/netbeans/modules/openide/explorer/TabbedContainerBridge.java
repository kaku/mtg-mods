/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.openide.explorer;

import javax.swing.JComponent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.openide.explorer.TabbedContainerBridgeImpl;

public abstract class TabbedContainerBridge {
    protected TabbedContainerBridge() {
    }

    public static TabbedContainerBridge getDefault() {
        return new TabbedContainerBridgeImpl();
    }

    public abstract JComponent createTabbedContainer();

    public abstract void setInnerComponent(JComponent var1, JComponent var2);

    public abstract JComponent getInnerComponent(JComponent var1);

    public abstract Object[] getItems(JComponent var1);

    public abstract void setItems(JComponent var1, Object[] var2, String[] var3);

    public abstract void attachSelectionListener(JComponent var1, ChangeListener var2);

    public abstract void detachSelectionListener(JComponent var1, ChangeListener var2);

    public abstract Object getSelectedItem(JComponent var1);

    public abstract void setSelectedItem(JComponent var1, Object var2);

    public abstract boolean setSelectionByName(JComponent var1, String var2);

    public abstract String getCurrentSelectedTabName(JComponent var1);
}

