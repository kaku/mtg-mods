/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.openide.explorer;

import java.util.Map;
import java.util.WeakHashMap;
import org.openide.explorer.propertysheet.PropertyPanel;

public class PropertyPanelBridge {
    private static final Map<PropertyPanel, Accessor> accessors = new WeakHashMap<PropertyPanel, Accessor>();

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void register(PropertyPanel panel, Accessor accessor) {
        Map<PropertyPanel, Accessor> map = accessors;
        synchronized (map) {
            accessors.put(panel, accessor);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static boolean commit(PropertyPanel panel) {
        Accessor a;
        Map<PropertyPanel, Accessor> map = accessors;
        synchronized (map) {
            a = accessors.get(panel);
        }
        if (a == null) {
            return false;
        }
        return a.commit();
    }

    public static interface Accessor {
        public boolean commit();
    }

}

