/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.apple.eawt.Application
 *  com.apple.eawt.ApplicationEvent
 *  com.apple.eawt.ApplicationListener
 *  com.apple.eawt.FullScreenUtilities
 *  org.openide.ErrorManager
 *  org.openide.awt.Actions
 *  org.openide.cookies.EditCookie
 *  org.openide.cookies.OpenCookie
 *  org.openide.cookies.ViewCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.util.Lookup
 *  org.openide.windows.WindowManager
 *  org.openide.windows.WindowSystemEvent
 *  org.openide.windows.WindowSystemListener
 */
package org.netbeans.modules.applemenu;

import com.apple.eawt.Application;
import com.apple.eawt.ApplicationEvent;
import com.apple.eawt.ApplicationListener;
import com.apple.eawt.FullScreenUtilities;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.beans.Beans;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JDialog;
import javax.swing.JRootPane;
import org.openide.ErrorManager;
import org.openide.awt.Actions;
import org.openide.cookies.EditCookie;
import org.openide.cookies.OpenCookie;
import org.openide.cookies.ViewCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Lookup;
import org.openide.windows.WindowManager;
import org.openide.windows.WindowSystemEvent;
import org.openide.windows.WindowSystemListener;

class NbApplicationAdapter
implements ApplicationListener {
    private static ApplicationListener al = null;

    private NbApplicationAdapter() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void install() {
        boolean wasDesignTime = Beans.isDesignTime();
        try {
            Beans.setDesignTime(false);
            al = new NbApplicationAdapter();
            Application.getApplication().addApplicationListener(al);
            Application.getApplication().setEnabledAboutMenu(true);
            Application.getApplication().setEnabledPreferencesMenu(true);
        }
        finally {
            Beans.setDesignTime(wasDesignTime);
        }
        WindowManager.getDefault().addWindowSystemListener(new WindowSystemListener(){

            public void beforeLoad(WindowSystemEvent event) {
                WindowManager.getDefault().removeWindowSystemListener((WindowSystemListener)this);
                try {
                    FullScreenUtilities.setWindowCanFullScreen((Window)WindowManager.getDefault().getMainWindow(), (boolean)true);
                }
                catch (ThreadDeath td) {
                    throw td;
                }
                catch (Throwable e) {
                    Logger.getLogger(NbApplicationAdapter.class.getName()).log(Level.FINE, "Error while setting up full screen support.", e);
                }
            }

            public void afterLoad(WindowSystemEvent event) {
            }

            public void beforeSave(WindowSystemEvent event) {
            }

            public void afterSave(WindowSystemEvent event) {
            }
        });
    }

    static void uninstall() {
        if (al != null) {
            Application.getApplication().removeApplicationListener(al);
            al = null;
        }
    }

    public void handleAbout(ApplicationEvent e) {
        Window[] windows = Dialog.getWindows();
        if (null != windows) {
            for (Window w : windows) {
                JDialog dlg;
                if (!(w instanceof JDialog) || !Boolean.TRUE.equals((dlg = (JDialog)w).getRootPane().getClientProperty("nb.about.dialog")) || !dlg.isVisible()) continue;
                dlg.toFront();
                e.setHandled(true);
                return;
            }
        }
        e.setHandled(this.performAction("Help", "org.netbeans.core.actions.AboutAction"));
    }

    public void handleOpenApplication(ApplicationEvent e) {
    }

    public void handleOpenFile(ApplicationEvent e) {
        FileObject obj;
        boolean result = false;
        String fname = e.getFilename();
        File f = new File(fname);
        if (f.exists() && !f.isDirectory() && (obj = FileUtil.toFileObject((File)f)) != null) {
            try {
                DataObject dob = DataObject.find((FileObject)obj);
                OpenCookie oc = (OpenCookie)dob.getLookup().lookup(OpenCookie.class);
                result = oc != null;
                if (result) {
                    oc.open();
                } else {
                    EditCookie ec = (EditCookie)dob.getLookup().lookup(EditCookie.class);
                    result = ec != null;
                    if (result) {
                        ec.edit();
                    } else {
                        ViewCookie v = (ViewCookie)dob.getLookup().lookup(ViewCookie.class);
                        result = v != null;
                        if (result) {
                            v.view();
                        }
                    }
                }
            }
            catch (DataObjectNotFoundException ex) {
                Logger.getLogger(NbApplicationAdapter.class.getName()).log(Level.INFO, fname, (Throwable)ex);
            }
        }
        e.setHandled(result);
    }

    public void handlePreferences(ApplicationEvent e) {
        e.setHandled(this.performAction("Window", "org.netbeans.modules.options.OptionsWindowAction"));
    }

    public void handlePrintFile(ApplicationEvent e) {
    }

    public void handleQuit(ApplicationEvent e) {
        e.setHandled(!this.performAction("System", "org.netbeans.core.actions.SystemExit"));
    }

    public void handleReOpenApplication(ApplicationEvent e) {
    }

    private boolean performAction(String category, String id) {
        Action a = Actions.forID((String)category, (String)id);
        if (a == null) {
            return false;
        }
        ActionEvent ae = new ActionEvent(this, 1001, "whatever");
        try {
            a.actionPerformed(ae);
            return true;
        }
        catch (Exception e) {
            ErrorManager.getDefault().notify(16, (Throwable)e);
            return false;
        }
    }

}

