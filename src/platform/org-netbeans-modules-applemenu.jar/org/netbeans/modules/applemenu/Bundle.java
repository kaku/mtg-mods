/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.applemenu;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String MSG_MinimizeWindowAction() {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_MinimizeWindowAction");
    }

    private void Bundle() {
    }
}

