/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.applemenu;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.event.AWTEventListener;
import java.awt.event.FocusEvent;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import javax.swing.text.Caret;
import javax.swing.text.JTextComponent;

public class CtrlClickHack
implements AWTEventListener {
    private Reference<JTextComponent> lastFocusedTextComponent = null;

    @Override
    public void eventDispatched(AWTEvent e) {
        if (!(e instanceof MouseEvent) && !(e instanceof FocusEvent)) {
            return;
        }
        if (e instanceof FocusEvent) {
            FocusEvent fe = (FocusEvent)e;
            if (fe.getID() == 1004) {
                Caret caret;
                JTextComponent jtc;
                if (fe.getOppositeComponent() instanceof JTextComponent) {
                    jtc = (JTextComponent)fe.getOppositeComponent();
                    if (null != jtc && null != (caret = jtc.getCaret())) {
                        caret.setVisible(false);
                    }
                } else {
                    JTextComponent jTextComponent = jtc = this.lastFocusedTextComponent == null ? null : this.lastFocusedTextComponent.get();
                    if (null != jtc && null != (caret = jtc.getCaret())) {
                        caret.setVisible(false);
                    }
                }
                if (fe.getComponent() instanceof JTextComponent) {
                    jtc = (JTextComponent)fe.getComponent();
                    this.lastFocusedTextComponent = new WeakReference<JTextComponent>(jtc);
                    if (null != jtc && null != (caret = jtc.getCaret())) {
                        caret.setVisible(true);
                    }
                }
            }
            return;
        }
        MouseEvent evt = (MouseEvent)e;
        if (evt.getModifiers() != 18) {
            return;
        }
        try {
            Field f1 = InputEvent.class.getDeclaredField("modifiers");
            Field f2 = MouseEvent.class.getDeclaredField("button");
            Method m = MouseEvent.class.getDeclaredMethod("setNewModifiers", new Class[0]);
            f1.setAccessible(true);
            f1.setInt(evt, 4);
            f2.setAccessible(true);
            f2.setInt(evt, 3);
            m.setAccessible(true);
            m.invoke(evt, new Object[0]);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

