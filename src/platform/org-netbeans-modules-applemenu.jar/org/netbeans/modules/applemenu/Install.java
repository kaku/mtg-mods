/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.ModuleInstall
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.applemenu;

import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.lang.reflect.Method;
import org.netbeans.modules.applemenu.CtrlClickHack;
import org.openide.modules.ModuleInstall;
import org.openide.util.Utilities;

public class Install
extends ModuleInstall {
    private CtrlClickHack listener;

    public void restored() {
        this.listener = new CtrlClickHack();
        Toolkit.getDefaultToolkit().addAWTEventListener(this.listener, 20);
        if (Utilities.isMac()) {
            try {
                Class adapter = Class.forName("org.netbeans.modules.applemenu.NbApplicationAdapter");
                Method m = adapter.getDeclaredMethod("install", new Class[0]);
                m.invoke(adapter, new Object[0]);
            }
            catch (NoClassDefFoundError e) {
            }
            catch (ClassNotFoundException e) {
            }
            catch (Exception e) {
                // empty catch block
            }
            String pn = "apple.laf.useScreenMenuBar";
            if (System.getProperty(pn) == null) {
                System.setProperty(pn, "true");
            }
        }
    }

    public void uninstalled() {
        if (this.listener != null) {
            Toolkit.getDefaultToolkit().removeAWTEventListener(this.listener);
            this.listener = null;
        }
        if (System.getProperty("mrj.version") != null) {
            try {
                Class adapter = Class.forName("org.netbeans.modules.applemenu.NbApplicationAdapter");
                Method m = adapter.getDeclaredMethod("uninstall", new Class[0]);
                m.invoke(adapter, new Object[0]);
            }
            catch (NoClassDefFoundError e) {
            }
            catch (ClassNotFoundException e) {
            }
            catch (Exception e) {
                // empty catch block
            }
        }
    }
}

