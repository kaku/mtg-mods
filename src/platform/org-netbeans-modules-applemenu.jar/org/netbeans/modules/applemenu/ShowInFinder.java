/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.StatusDisplayer
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 */
package org.netbeans.modules.applemenu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.awt.StatusDisplayer;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

public final class ShowInFinder
implements ActionListener {
    private static final Logger LOG = Logger.getLogger(ShowInFinder.class.getName());
    private final DataObject context;
    private static final RequestProcessor RP = new RequestProcessor("ShowInFinder", 1);

    public ShowInFinder(DataObject context) {
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        RP.execute(new Runnable(){

            @Override
            public void run() {
                FileObject fobj = ShowInFinder.this.context.getPrimaryFile();
                if (fobj == null) {
                    return;
                }
                LOG.log(Level.FINE, "Selected file: {0}", FileUtil.getFileDisplayName((FileObject)fobj));
                if (FileUtil.getArchiveFile((FileObject)fobj) != null) {
                    fobj = FileUtil.getArchiveFile((FileObject)fobj);
                }
                LOG.log(Level.FINE, "File to select in Finder: {0}", FileUtil.getFileDisplayName((FileObject)fobj));
                File file = FileUtil.toFile((FileObject)fobj);
                if (file == null) {
                    LOG.log(Level.INFO, "Ignoring non local file: {0}", FileUtil.getFileDisplayName((FileObject)fobj));
                    StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(ShowInFinder.class, (String)"TXT_NoLocalFile"));
                    return;
                }
                try {
                    Class fmClz = Class.forName("com.apple.eio.FileManager");
                    Method m = fmClz.getDeclaredMethod("revealInFinder", File.class);
                    m.invoke(null, file);
                }
                catch (ClassNotFoundException e) {
                    LOG.log(Level.FINE, "Cannot load com.apple.eio.FileManager class.");
                }
                catch (NoSuchMethodException e) {
                    LOG.log(Level.FINE, "No method revealInFinder(java.io.File) in the com.apple.eio.FileManager class.");
                }
                catch (InvocationTargetException e) {
                    LOG.log(Level.FINE, "Cannot invoke method com.apple.eio.FileManager.revealInFinder(java.io.File).");
                }
                catch (IllegalAccessException e) {
                    LOG.log(Level.FINE, "The method com.apple.eio.FileManager.revealInFinder(java.io.File) is not accessible");
                }
            }
        });
    }

}

