/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 */
package org.netbeans.modules.applemenu;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.peer.ComponentPeer;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JWindow;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.SwingUtilities;
import org.openide.ErrorManager;

public class ApplePopupFactory
extends PopupFactory {
    private static final boolean APPLE_HEAVYWEIGHT = Boolean.getBoolean("nb.explorer.hw.completions");
    private static final boolean APPLE_COCOA_HACK = APPLE_HEAVYWEIGHT && Boolean.getBoolean("nb.explorer.hw.cocoahack");
    private static Set<Reference<JWindow>> windowPool = new HashSet<Reference<JWindow>>();
    private static int ct = 0;
    private static boolean hackBroken = false;
    private static boolean warned = false;

    @Override
    public Popup getPopup(Component owner, Component contents, int x, int y) throws IllegalArgumentException {
        assert (owner instanceof JComponent);
        Dimension d = contents.getPreferredSize();
        Container c = ((JComponent)owner).getTopLevelAncestor();
        if (c == null) {
            throw new IllegalArgumentException("Not onscreen: " + owner);
        }
        Point p = new Point(x, y);
        SwingUtilities.convertPointFromScreen(p, c);
        Rectangle r = new Rectangle(p.x, p.y, d.width, d.height);
        if (c.getBounds().contains(r)) {
            return new LWPopup(owner, contents, x, y);
        }
        return APPLE_HEAVYWEIGHT ? new HWPopup(owner, contents, x, y) : new NullPopup();
    }

    private static JWindow checkOutWindow() {
        if (windowPool != null && !windowPool.isEmpty()) {
            Iterator<Reference<JWindow>> i = windowPool.iterator();
            while (i.hasNext()) {
                Reference<JWindow> ref = i.next();
                JWindow win = ref.get();
                i.remove();
                if (win == null) continue;
                assert (!win.isShowing());
                win.setBounds(0, 0, 1, 1);
                win.getContentPane().removeAll();
                win.setBackground(new Color(255, 255, 255, 0));
                return win;
            }
        }
        Window nue = APPLE_COCOA_HACK ? new HackedJWindow() : new JWindow();
        nue.setBackground(new Color(255, 255, 255, 0));
        return nue;
    }

    private static void checkInWindow(JWindow win) {
        if (!APPLE_COCOA_HACK) {
            win.dispose();
        }
        windowPool.add(new SoftReference<JWindow>(win));
    }

    static boolean broken() {
        return hackBroken;
    }

    private static final class HackedJWindow
    extends JWindow {
        private String title = "none";

        HackedJWindow() {
        }

        @Override
        public void addNotify() {
            super.addNotify();
            this.hackTitle();
            this.hackNativeWindow();
        }

        private void hackTitle() {
            if (!hackBroken) {
                try {
                    ComponentPeer o = this.getPeer();
                    if (o != null) {
                        Method m = o.getClass().getDeclaredMethod("setTitle", String.class);
                        m.setAccessible(true);
                        this.title = "hw popup" + ct++;
                        m.invoke(o, this.title);
                    }
                }
                catch (Exception e) {
                    this.warn(e);
                }
            }
        }

        private void hackNativeWindow() {
            if (!hackBroken) {
                try {
                    Class c = Class.forName("com.apple.cocoa.application.NSApplication");
                    Method m = c.getDeclaredMethod("sharedApplication", new Class[0]);
                    Object nsapplication = m.invoke(null, new Object[0]);
                    m = nsapplication.getClass().getMethod("windows", new Class[0]);
                    Object nsarray_of_nswindows = m.invoke(nsapplication, new Object[0]);
                    m = nsarray_of_nswindows.getClass().getMethod("count", new Class[0]);
                    int arrSize = (Integer)m.invoke(nsarray_of_nswindows, new Object[0]);
                    Object[] windows = new Object[arrSize];
                    m = nsarray_of_nswindows.getClass().getMethod("getObjects", Object[].class);
                    m.invoke(nsarray_of_nswindows, windows);
                    if (windows.length > 0) {
                        c = windows[0].getClass();
                        Method titleMethod = c.getMethod("title", new Class[0]);
                        Method setHasShadowMethod = c.getMethod("setHasShadow", Boolean.TYPE);
                        for (int i = 0; i < windows.length; ++i) {
                            String ttl = (String)titleMethod.invoke(windows[i], new Object[0]);
                            if (!this.title.equals(ttl)) continue;
                            setHasShadowMethod.invoke(windows[i], Boolean.FALSE);
                        }
                    }
                }
                catch (Exception e) {
                    this.warn(e);
                }
            }
        }

        private void warn(Exception e) {
            hackBroken = true;
            if (!warned) {
                warned = true;
                ErrorManager.getDefault().log(1, "Cannot turn off popup drop shadow, reverting to standard swing popup factory");
                ErrorManager.getDefault().notify(1, (Throwable)e);
                e.printStackTrace();
            }
        }
    }

    private static class HWPopup
    extends OurPopup {
        private JWindow window = null;

        public HWPopup(Component owner, Component contents, int x, int y) {
            super(owner, contents, x, y);
        }

        @Override
        public boolean isShowing() {
            return this.window != null && this.window.isShowing();
        }

        @Override
        void dispose() {
            if (this.window != null) {
                ApplePopupFactory.checkInWindow(this.window);
                this.window = null;
            }
            super.dispose();
        }

        @Override
        protected void prepareResources() {
            this.window = ApplePopupFactory.checkOutWindow();
            this.window.getContentPane().add(this.contents);
            this.window.setLocation(new Point(this.x, this.y));
            this.window.pack();
            this.window.setBackground(new Color(255, 255, 255, 0));
        }

        @Override
        protected void doShow() {
            this.window.setVisible(true);
        }

        @Override
        protected void doHide() {
            if (this.window != null) {
                this.window.setVisible(false);
                this.window.getContentPane().remove(this.contents);
                this.dispose();
            }
        }
    }

    private static class LWPopup
    extends OurPopup {
        private Rectangle bounds = null;

        public LWPopup(Component owner, Component contents, int x, int y) {
            super(owner, contents, x, y);
        }

        @Override
        protected void prepareResources() {
            JComponent jc = (JComponent)this.owner;
            Container w = jc.getTopLevelAncestor();
            JComponent pane = null;
            if (w instanceof JFrame) {
                pane = (JComponent)((JFrame)w).getGlassPane();
            } else if (w instanceof JDialog) {
                pane = (JComponent)((JDialog)w).getGlassPane();
            } else if (w instanceof JWindow) {
                pane = (JComponent)((JWindow)w).getGlassPane();
            }
            if (w == null) {
                throw new IllegalArgumentException("Not a JFrame/JWindow/JDialog: " + this.owner);
            }
            Point p = new Point(this.x, this.y);
            SwingUtilities.convertPointFromScreen(p, pane);
            if (pane.getLayout() != null) {
                pane.setLayout(null);
            }
            pane.setVisible(true);
            this.contents.setVisible(false);
            Dimension d = this.contents.getPreferredSize();
            pane.add(this.contents);
            this.bounds = new Rectangle(p.x, p.y, d.width, d.height);
            this.contents.setBounds(p.x, p.y, d.width, d.height);
        }

        @Override
        protected void doShow() {
            this.contents.setVisible(true);
        }

        @Override
        public boolean isShowing() {
            return this.contents != null && this.contents.isShowing();
        }

        @Override
        protected void doHide() {
            Container parent = this.contents.getParent();
            if (parent != null) {
                this.contents.getParent().remove(this.contents);
                parent.repaint(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height);
                parent.setVisible(false);
            }
            this.contents.setVisible(true);
        }
    }

    private static abstract class OurPopup
    extends Popup {
        protected Component owner = null;
        protected Component contents = null;
        protected int x = -1;
        protected int y = -1;
        private boolean canReuse = false;

        public OurPopup(Component owner, Component contents, int x, int y) {
            this.configure(owner, contents, x, y);
        }

        final void configure(Component owner, Component contents, int x, int y) {
            this.owner = owner;
            this.contents = contents;
            this.x = x;
            this.y = y;
        }

        protected abstract void prepareResources();

        protected abstract void doShow();

        public abstract boolean isShowing();

        protected abstract void doHide();

        @Override
        public final void show() {
            this.prepareResources();
            this.doShow();
        }

        @Override
        public final void hide() {
            this.doHide();
        }

        void dispose() {
            this.owner = null;
            this.contents = null;
            this.x = -1;
            this.y = -1;
        }

        public final void clear() {
            this.canReuse = true;
            this.dispose();
        }

        boolean isInUse() {
            return this.canReuse;
        }
    }

    private static final class NullPopup
    extends Popup {
        private NullPopup() {
        }

        @Override
        public void show() {
        }

        @Override
        public void hide() {
        }
    }

}

