/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.WindowManager
 */
package org.netbeans.modules.applemenu;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.windows.WindowManager;

public final class MinimizeWindowAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent arg0) {
        WindowManager.getDefault().getMainWindow().setExtendedState(1);
    }
}

