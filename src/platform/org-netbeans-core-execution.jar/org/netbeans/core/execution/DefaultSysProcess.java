/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.execution.ExecutorTask
 *  org.openide.util.Exceptions
 *  org.openide.windows.InputOutput
 */
package org.netbeans.core.execution;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.execution.ExecutionEngine;
import org.netbeans.core.execution.RunClassThread;
import org.netbeans.core.execution.TaskThreadGroup;
import org.openide.execution.ExecutorTask;
import org.openide.util.Exceptions;
import org.openide.windows.InputOutput;

final class DefaultSysProcess
extends ExecutorTask {
    static int processCount;
    private final TaskThreadGroup group;
    private boolean destroyed = false;
    private final InputOutput io;
    private final String name;

    public DefaultSysProcess(Runnable run, TaskThreadGroup grp, InputOutput io, String name) {
        super(run);
        this.group = grp;
        this.io = io;
        this.name = name;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public synchronized void stop() {
        if (this.destroyed) {
            return;
        }
        this.destroyed = true;
        try {
            this.group.interrupt();
            RunClassThread runClassThread = this.group.getRunClassThread();
            if (runClassThread != null) {
                runClassThread.waitForEnd(3000);
            }
        }
        catch (InterruptedException e) {
            Logger.getLogger(DefaultSysProcess.class.getName()).log(Level.FINE, null, e);
        }
        finally {
            this.group.setRunClassThread(null);
        }
        ExecutionEngine.closeGroup(this.group);
        this.group.kill();
        this.notifyFinished();
    }

    public int result() {
        try {
            this.group.waitFor();
        }
        catch (InterruptedException e) {
            return 4;
        }
        this.notifyFinished();
        return 0;
    }

    public InputOutput getInputOutput() {
        return this.io;
    }

    public void run() {
    }

    public String getName() {
        return this.name;
    }

    void destroyThreadGroup(ThreadGroup base) {
        new Thread(base, new Destroyer(this.group)).start();
    }

    private static class Destroyer
    implements Runnable {
        private final ThreadGroup group;

        Destroyer(ThreadGroup group) {
            this.group = group;
        }

        @Override
        public void run() {
            try {
                while (this.group.activeCount() > 0) {
                    Thread.sleep(1000);
                }
            }
            catch (InterruptedException e) {
                Exceptions.printStackTrace((Throwable)e);
            }
            if (!this.group.isDestroyed()) {
                try {
                    this.group.destroy();
                }
                catch (IllegalThreadStateException x) {
                    // empty catch block
                }
            }
        }
    }

}

