/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.execution;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import org.netbeans.core.execution.ExecutionEngine;

final class SysIn
extends InputStream {
    @Override
    public int read() throws IOException {
        return ExecutionEngine.getTaskIOs().getIn().read();
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        char[] b2 = new char[len];
        int ret = ExecutionEngine.getTaskIOs().getIn().read(b2, 0, len);
        for (int i = 0; i < len; ++i) {
            b[off + i] = (byte)b2[i];
        }
        return ret;
    }

    @Override
    public void close() throws IOException {
        ExecutionEngine.getTaskIOs().getIn().close();
    }

    @Override
    public void mark(int x) {
        try {
            ExecutionEngine.getTaskIOs().getIn().mark(x);
        }
        catch (IOException e) {
            // empty catch block
        }
    }

    @Override
    public void reset() throws IOException {
        ExecutionEngine.getTaskIOs().getIn().reset();
    }

    @Override
    public boolean markSupported() {
        return ExecutionEngine.getTaskIOs().getIn().markSupported();
    }

    @Override
    public long skip(long l) throws IOException {
        return ExecutionEngine.getTaskIOs().getIn().skip(l);
    }
}

