/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.execution.NbClassPath
 *  org.openide.explorer.propertysheet.ExPropertyEditor
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 */
package org.netbeans.core.execution.beaninfo.editors;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyEditor;
import org.netbeans.core.execution.beaninfo.editors.NbClassPathCustomEditor;
import org.openide.execution.NbClassPath;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.nodes.Node;

public class NbClassPathEditor
implements ExPropertyEditor {
    private NbClassPath pd;
    private PropertyChangeSupport support;
    private boolean editable = true;

    public NbClassPathEditor() {
        this.support = new PropertyChangeSupport(this);
    }

    public Object getValue() {
        return this.pd;
    }

    public void setValue(Object value) {
        NbClassPath old = this.pd;
        this.pd = (NbClassPath)value;
        this.support.firePropertyChange("value", (Object)old, (Object)this.pd);
    }

    public String getAsText() {
        if (this.pd != null) {
            return this.pd.getClassPath();
        }
        return "null";
    }

    public void setAsText(String string) {
        if (!"null".equals(string)) {
            this.setValue((Object)new NbClassPath(string));
        }
    }

    public String getJavaInitializationString() {
        return "new NbClassPath (" + this.getAsText() + ")";
    }

    public String[] getTags() {
        return null;
    }

    public boolean isPaintable() {
        return false;
    }

    public void paintValue(Graphics g, Rectangle rectangle) {
    }

    public boolean supportsCustomEditor() {
        return true;
    }

    public Component getCustomEditor() {
        return new NbClassPathCustomEditor((PropertyEditor)((Object)this));
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this.support.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this.support.removePropertyChangeListener(propertyChangeListener);
    }

    public boolean isEditable() {
        return this.editable;
    }

    public void attachEnv(PropertyEnv env) {
        FeatureDescriptor desc = env.getFeatureDescriptor();
        if (desc instanceof Node.Property) {
            Node.Property prop = (Node.Property)desc;
            this.editable = prop.canWrite();
        }
    }
}

