/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.beaninfo.editors.FileEditor
 *  org.openide.awt.Mnemonics
 *  org.openide.execution.NbProcessDescriptor
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.execution.beaninfo.editors;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import org.netbeans.beaninfo.editors.FileEditor;
import org.netbeans.core.execution.beaninfo.editors.NbProcessDescriptorEditor;
import org.openide.awt.Mnemonics;
import org.openide.execution.NbProcessDescriptor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

public class NbProcessDescriptorCustomEditor
extends JPanel
implements PropertyChangeListener {
    private NbProcessDescriptorEditor editor;
    private static int DEFAULT_WIDTH = 400;
    private static int DEFAULT_HEIGHT = 250;
    static final long serialVersionUID = -2766277953540349247L;
    private JLabel argumentKeyLabel;
    private JTextArea argumentsArea;
    private JLabel argumentsLabel;
    private JScrollPane argumentsScrollPane;
    private JTextArea hintArea;
    private JButton jButton1;
    private JPanel jPanel1;
    private JScrollPane jScrollPane1;
    private JTextField processField;
    private JLabel processLabel;

    public NbProcessDescriptorCustomEditor(NbProcessDescriptorEditor editor, PropertyEnv env) {
        this.editor = editor;
        this.initComponents();
        if (editor.pd != null) {
            this.processField.setText(editor.pd.getProcessName());
            this.argumentsArea.setText(editor.pd.getArguments());
            this.hintArea.setText(editor.pd.getInfo());
        }
        this.processField.getAccessibleContext().setAccessibleDescription(NbProcessDescriptorCustomEditor.getString("ACSD_NbProcessDescriptorCustomEditor.processLabel"));
        this.argumentsArea.getAccessibleContext().setAccessibleDescription(NbProcessDescriptorCustomEditor.getString("ACSD_NbProcessDescriptorCustomEditor.argumentsLabel"));
        this.hintArea.getAccessibleContext().setAccessibleDescription(NbProcessDescriptorCustomEditor.getString("ACSD_NbProcessDescriptorCustomEditor.argumentKeyLabel"));
        this.jButton1.getAccessibleContext().setAccessibleDescription(NbProcessDescriptorCustomEditor.getString("ACSD_NbProcessDescriptorCustomEditor.jButton1"));
        this.getAccessibleContext().setAccessibleDescription(NbProcessDescriptorCustomEditor.getString("ACSD_CustomNbProcessDescriptorEditor"));
        HelpCtx.setHelpIDString((JComponent)this, (String)NbProcessDescriptorCustomEditor.class.getName());
        env.setState(PropertyEnv.STATE_NEEDS_VALIDATION);
        env.addPropertyChangeListener((PropertyChangeListener)this);
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension inh = super.getPreferredSize();
        return new Dimension(DEFAULT_WIDTH, Math.max(inh.height, DEFAULT_HEIGHT));
    }

    private Object getPropertyValue() throws IllegalStateException {
        if (this.editor.pd == null) {
            return new NbProcessDescriptor(this.processField.getText(), this.argumentsArea.getText());
        }
        return new NbProcessDescriptor(this.processField.getText(), this.argumentsArea.getText(), this.editor.pd.getInfo());
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("state".equals(evt.getPropertyName()) && evt.getNewValue() == PropertyEnv.STATE_VALID) {
            this.editor.setValue(this.getPropertyValue());
        }
    }

    private void initComponents() {
        this.processLabel = new JLabel();
        this.processField = new JTextField();
        this.jButton1 = new JButton();
        this.argumentsLabel = new JLabel();
        this.argumentsScrollPane = new JScrollPane();
        this.argumentsArea = new JTextArea();
        this.jPanel1 = new JPanel();
        this.argumentKeyLabel = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this.hintArea = new JTextArea();
        this.setLayout(new GridBagLayout());
        this.processLabel.setLabelFor(this.processField);
        Mnemonics.setLocalizedText((JLabel)this.processLabel, (String)NbProcessDescriptorCustomEditor.getString("CTL_NbProcessDescriptorCustomEditor.processLabel.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 0, 5, 12);
        this.add((Component)this.processLabel, gridBagConstraints);
        this.processLabel.getAccessibleContext().setAccessibleDescription("Process");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = -1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 3.0;
        gridBagConstraints.insets = new Insets(0, 0, 5, 5);
        this.add((Component)this.processField, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.jButton1, (String)NbProcessDescriptorCustomEditor.getString("CTL_NbProcessDescriptorCustomEditor.jButton1.text"));
        this.jButton1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                NbProcessDescriptorCustomEditor.this.jButton1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.insets = new Insets(0, 0, 5, 0);
        this.add((Component)this.jButton1, gridBagConstraints);
        this.jButton1.getAccessibleContext().setAccessibleDescription("Browse");
        this.argumentsLabel.setLabelFor(this.argumentsArea);
        Mnemonics.setLocalizedText((JLabel)this.argumentsLabel, (String)NbBundle.getMessage(NbProcessDescriptorCustomEditor.class, (String)"CTL_NbProcessDescriptorCustomEditor.argumentsLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 0, 11, 12);
        this.add((Component)this.argumentsLabel, gridBagConstraints);
        this.argumentsLabel.getAccessibleContext().setAccessibleDescription("Process arguments");
        this.argumentsScrollPane.setMinimumSize(new Dimension(22, 35));
        this.argumentsScrollPane.setViewportView(this.argumentsArea);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 11, 0);
        this.add((Component)this.argumentsScrollPane, gridBagConstraints);
        this.jPanel1.setLayout(new BorderLayout(0, 2));
        this.argumentKeyLabel.setLabelFor(this.hintArea);
        Mnemonics.setLocalizedText((JLabel)this.argumentKeyLabel, (String)NbProcessDescriptorCustomEditor.getString("CTL_NbProcessDescriptorCustomEditor.argumentKeyLabel.text"));
        this.jPanel1.add((Component)this.argumentKeyLabel, "North");
        this.argumentKeyLabel.getAccessibleContext().setAccessibleDescription("Arguments hint");
        this.hintArea.setBackground((Color)UIManager.getDefaults().get("Label.background"));
        this.hintArea.setEditable(false);
        this.hintArea.setLineWrap(true);
        this.jScrollPane1.setViewportView(this.hintArea);
        this.jPanel1.add((Component)this.jScrollPane1, "Center");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 7.0;
        gridBagConstraints.weighty = 7.0;
        this.add((Component)this.jPanel1, gridBagConstraints);
    }

    private void jButton1ActionPerformed(ActionEvent evt) {
        int retVal;
        JFileChooser chooser = FileEditor.createHackedFileChooser();
        chooser.setMultiSelectionEnabled(false);
        File init = new File(this.processField.getText());
        if (init.isFile()) {
            chooser.setCurrentDirectory(init.getParentFile());
            chooser.setSelectedFile(init);
        }
        if ((retVal = chooser.showOpenDialog(this)) == 0) {
            String absolute_name = chooser.getSelectedFile().getAbsolutePath();
            this.processField.setText(absolute_name);
        }
    }

    private static final String getString(String s) {
        return NbBundle.getMessage(NbProcessDescriptorCustomEditor.class, (String)s);
    }

}

