/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.beaninfo.editors.FileEditor
 *  org.openide.awt.Mnemonics
 *  org.openide.execution.NbClassPath
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.NbCollections
 */
package org.netbeans.core.execution.beaninfo.editors;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyEditor;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.StringTokenizer;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;
import org.netbeans.beaninfo.editors.FileEditor;
import org.netbeans.core.execution.beaninfo.editors.NbClassPathEditor;
import org.openide.awt.Mnemonics;
import org.openide.execution.NbClassPath;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.NbCollections;

class NbClassPathCustomEditor
extends JPanel {
    private static File lastDirFolder = null;
    private static File lastJarFolder = null;
    private PropertyEditor editor;
    private DefaultListModel listModel = new DefaultListModel();
    private boolean editable = true;
    private JButton addDirButton;
    private JButton addJarButton;
    private JButton downButton;
    private JPanel innerPanel;
    private JList pathList;
    private JScrollPane pathScrollPane;
    private JButton removeButton;
    private JButton upButton;

    public NbClassPathCustomEditor() {
        this.initComponents();
        this.pathList.setModel(this.listModel);
        this.pathScrollPane.setViewportView(this.pathList);
        this.setMinimumSize(new Dimension(400, 200));
        this.setPreferredSize(new Dimension(400, 200));
        this.pathList.getAccessibleContext().setAccessibleDescription(NbClassPathCustomEditor.getString("ACSD_PathList"));
        this.addDirButton.getAccessibleContext().setAccessibleDescription(NbClassPathCustomEditor.getString("ACSD_AddDirectory"));
        this.addJarButton.getAccessibleContext().setAccessibleDescription(NbClassPathCustomEditor.getString("ACSD_AddJAR"));
        this.upButton.getAccessibleContext().setAccessibleDescription(NbClassPathCustomEditor.getString("ACSD_MoveUp"));
        this.downButton.getAccessibleContext().setAccessibleDescription(NbClassPathCustomEditor.getString("ACSD_MoveDown"));
        this.removeButton.getAccessibleContext().setAccessibleDescription(NbClassPathCustomEditor.getString("ACSD_Remove"));
        this.getAccessibleContext().setAccessibleDescription(NbClassPathCustomEditor.getString("ACSD_CustomNbClassPathEditor"));
    }

    NbClassPathCustomEditor(PropertyEditor propEd) {
        this();
        this.editor = propEd;
        Object value = propEd.getValue();
        if (value instanceof NbClassPath) {
            this.setClassPath(((NbClassPath)value).getClassPath());
        }
        if (this.editor instanceof NbClassPathEditor && !((NbClassPathEditor)((Object)this.editor)).isEditable()) {
            this.editable = false;
            this.addDirButton.setEnabled(false);
            this.addJarButton.setEnabled(false);
        }
    }

    private void initComponents() {
        this.innerPanel = new JPanel();
        this.addDirButton = new JButton();
        this.addJarButton = new JButton();
        this.upButton = new JButton();
        this.downButton = new JButton();
        this.removeButton = new JButton();
        this.pathScrollPane = new JScrollPane();
        this.pathList = new JList();
        this.setLayout(new BorderLayout());
        this.innerPanel.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((AbstractButton)this.addDirButton, (String)NbClassPathCustomEditor.getString("CTL_AddDirectory"));
        this.addDirButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                NbClassPathCustomEditor.this.addDirButtonActionPerformed(evt);
            }
        });
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 11;
        gridBagConstraints.insets = new Insets(0, 0, 5, 0);
        this.innerPanel.add((Component)this.addDirButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.addJarButton, (String)NbClassPathCustomEditor.getString("CTL_AddJAR"));
        this.addJarButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                NbClassPathCustomEditor.this.addJarButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 11;
        gridBagConstraints.insets = new Insets(0, 0, 5, 0);
        this.innerPanel.add((Component)this.addJarButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.upButton, (String)NbClassPathCustomEditor.getString("CTL_MoveUp"));
        this.upButton.setEnabled(false);
        this.upButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                NbClassPathCustomEditor.this.upButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 11;
        gridBagConstraints.insets = new Insets(0, 0, 5, 0);
        this.innerPanel.add((Component)this.upButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.downButton, (String)NbClassPathCustomEditor.getString("CTL_MoveDown"));
        this.downButton.setEnabled(false);
        this.downButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                NbClassPathCustomEditor.this.downButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 11;
        this.innerPanel.add((Component)this.downButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.removeButton, (String)NbClassPathCustomEditor.getString("CTL_Remove"));
        this.removeButton.setEnabled(false);
        this.removeButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                NbClassPathCustomEditor.this.removeButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 11;
        gridBagConstraints.insets = new Insets(0, 0, 11, 0);
        this.innerPanel.add((Component)this.removeButton, gridBagConstraints);
        this.pathList.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent evt) {
                NbClassPathCustomEditor.this.pathListMouseClicked(evt);
            }
        });
        this.pathList.addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent evt) {
                NbClassPathCustomEditor.this.pathListValueChanged(evt);
            }
        });
        this.pathScrollPane.setViewportView(this.pathList);
        this.pathList.getAccessibleContext().setAccessibleName(NbBundle.getMessage(NbClassPathCustomEditor.class, (String)"NbClassPathCustomEditor.pathList.AccessibleContext.accessibleName"));
        this.pathList.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(NbClassPathCustomEditor.class, (String)"NbClassPathCustomEditor.pathList.AccessibleContext.accessibleDescription"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 5;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 0, 11);
        this.innerPanel.add((Component)this.pathScrollPane, gridBagConstraints);
        this.add((Component)this.innerPanel, "Center");
    }

    private void pathListMouseClicked(MouseEvent evt) {
        if (evt.getClickCount() != 2) {
            return;
        }
        this.triggerEdit(this.pathList.getSelectedIndex());
    }

    private void pathListValueChanged(ListSelectionEvent evt) {
        this.enableButtons();
    }

    private void removeButtonActionPerformed(ActionEvent evt) {
        int index = this.pathList.getSelectedIndex();
        Object[] selectedValues = this.pathList.getSelectedValues();
        for (int i = 0; i < selectedValues.length; ++i) {
            this.listModel.removeElement(selectedValues[i]);
            this.fireValueChanged();
        }
        int size = this.listModel.getSize();
        if (index >= 0 && size > 0) {
            if (size == index) {
                this.pathList.setSelectedIndex(index - 1);
            } else if (size > index) {
                this.pathList.setSelectedIndex(index);
            } else {
                this.pathList.setSelectedIndex(0);
            }
        }
        this.enableButtons();
    }

    private void downButtonActionPerformed(ActionEvent evt) {
        int i = this.pathList.getSelectedIndex();
        this.swap(i);
        this.pathList.setSelectedIndex(i + 1);
    }

    private void upButtonActionPerformed(ActionEvent evt) {
        int i = this.pathList.getSelectedIndex();
        this.swap(i - 1);
        this.pathList.setSelectedIndex(i - 1);
    }

    private void addJarButtonActionPerformed(ActionEvent evt) {
        JFileChooser chooser = FileEditor.createHackedFileChooser();
        chooser.setFileHidingEnabled(false);
        this.setHelpToChooser(chooser);
        chooser.setFileFilter(new FileFilter(){

            @Override
            public boolean accept(File f) {
                return f.isDirectory() || f.getName().endsWith(".jar") || f.getName().endsWith(".zip");
            }

            @Override
            public String getDescription() {
                return NbClassPathCustomEditor.getString("CTL_JarArchivesMask");
            }
        });
        if (lastJarFolder != null) {
            chooser.setCurrentDirectory(lastJarFolder);
        }
        chooser.setDialogTitle(NbClassPathCustomEditor.getString("CTL_FileSystemPanel.Jar_Dialog_Title"));
        chooser.setMultiSelectionEnabled(true);
        if (chooser.showDialog(this, NbClassPathCustomEditor.getString("CTL_Approve_Button_Title")) == 0) {
            File[] files = chooser.getSelectedFiles();
            boolean found = false;
            for (int i = 0; i < files.length; ++i) {
                if (files[i] == null || !files[i].isFile()) continue;
                found = true;
                String path = files[i].getAbsolutePath();
                if (this.listModel.contains(path)) continue;
                this.listModel.addElement(path);
            }
            if (found) {
                lastJarFolder = chooser.getCurrentDirectory();
                this.fireValueChanged();
            }
            this.pathList.setSelectedIndex(this.listModel.size() - 1);
        }
    }

    private void addDirButtonActionPerformed(ActionEvent evt) {
        File f;
        JFileChooser chooser = FileEditor.createHackedFileChooser();
        chooser.setFileHidingEnabled(false);
        this.setHelpToChooser(chooser);
        chooser.setFileSelectionMode(1);
        chooser.setDialogTitle(NbClassPathCustomEditor.getString("CTL_FileSystemPanel.Local_Dialog_Title"));
        if (lastDirFolder != null) {
            chooser.setCurrentDirectory(lastDirFolder);
        }
        if (chooser.showDialog(this, NbClassPathCustomEditor.getString("CTL_Approve_Button_Title")) == 0 && (f = chooser.getSelectedFile()) != null && f.isDirectory()) {
            lastDirFolder = f.getParentFile();
            String path = f.getAbsolutePath();
            if (!this.listModel.contains(path)) {
                this.listModel.addElement(path);
            }
            this.fireValueChanged();
            this.pathList.setSelectedIndex(this.listModel.size() - 1);
        }
    }

    private void fireValueChanged() {
        if (this.editor != null) {
            this.editor.setValue(this.getPropertyValue());
        }
    }

    private void triggerEdit(int index) {
        if (index < 0) {
            return;
        }
        String selectedItem = (String)this.listModel.elementAt(index);
        File selectedF = new File(selectedItem);
        if (selectedF.isDirectory()) {
            File f;
            JFileChooser chooser = FileEditor.createHackedFileChooser();
            this.setHelpToChooser(chooser);
            chooser.setFileSelectionMode(1);
            chooser.setDialogTitle(NbClassPathCustomEditor.getString("CTL_Edit_Local_Dialog_Title"));
            if (selectedF.getParentFile() != null) {
                chooser.setCurrentDirectory(selectedF.getParentFile());
                chooser.setSelectedFile(selectedF);
            }
            if (chooser.showDialog(this, NbClassPathCustomEditor.getString("CTL_Approve_Button_Title")) == 0 && (f = chooser.getSelectedFile()) != null && f.isDirectory()) {
                lastDirFolder = chooser.getCurrentDirectory();
                try {
                    f = f.getCanonicalFile();
                }
                catch (IOException ioe) {
                    // empty catch block
                }
                this.listModel.set(index, f.getAbsolutePath());
                this.fireValueChanged();
            }
        } else if (selectedF.isFile()) {
            File f;
            JFileChooser chooser = FileEditor.createHackedFileChooser();
            this.setHelpToChooser(chooser);
            chooser.setFileFilter(new FileFilter(){

                @Override
                public boolean accept(File f) {
                    return f.isDirectory() || f.getName().endsWith(".jar") || f.getName().endsWith(".zip");
                }

                @Override
                public String getDescription() {
                    return NbClassPathCustomEditor.getString("CTL_JarArchivesMask");
                }
            });
            chooser.setCurrentDirectory(selectedF.getParentFile());
            chooser.setSelectedFile(selectedF);
            chooser.setDialogTitle(NbClassPathCustomEditor.getString("CTL_Edit_Jar_Dialog_Title"));
            if (chooser.showDialog(this, NbClassPathCustomEditor.getString("CTL_Approve_Button_Title")) == 0 && (f = chooser.getSelectedFile()) != null && f.isFile()) {
                lastJarFolder = chooser.getCurrentDirectory();
                this.listModel.set(index, f.getAbsolutePath());
                this.fireValueChanged();
            }
        }
    }

    private void swap(int index) {
        if (index < 0 || index >= this.listModel.size() - 1) {
            return;
        }
        Object value = this.listModel.elementAt(index);
        this.listModel.removeElement(value);
        this.listModel.add(index + 1, value);
        this.fireValueChanged();
        this.enableButtons();
    }

    private void enableButtons() {
        if (!this.editable) {
            return;
        }
        this.removeButton.setEnabled(this.pathList.getSelectedIndices().length > 0);
        if (this.pathList.getSelectedIndices().length == 1) {
            this.downButton.setEnabled(this.pathList.getSelectedIndices()[0] < this.pathList.getModel().getSize() - 1);
            this.upButton.setEnabled(this.pathList.getSelectedIndices()[0] > 0);
        } else {
            this.downButton.setEnabled(false);
            this.upButton.setEnabled(false);
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (enabled) {
            this.enableButtons();
        }
    }

    private void setClassPath(String classPath) {
        StringTokenizer tok = new StringTokenizer(classPath, File.pathSeparator);
        while (tok.hasMoreTokens()) {
            String s = tok.nextToken();
            if (s.startsWith("\"")) {
                s = s.substring(1);
            }
            if (s.endsWith("\"")) {
                s = s.substring(0, s.length() - 1);
            }
            if (this.listModel.contains(s)) continue;
            this.listModel.addElement(s);
        }
    }

    public Object getPropertyValue() throws IllegalStateException {
        ArrayList<String> list = Collections.list(NbCollections.checkedEnumerationByFilter(this.listModel.elements(), String.class, (boolean)true));
        String[] arr = list.toArray(new String[list.size()]);
        return new NbClassPath(arr);
    }

    private static final String getString(String s) {
        return NbBundle.getMessage(NbClassPathCustomEditor.class, (String)s);
    }

    private void setHelpToChooser(JFileChooser chooser) {
        HelpCtx help = HelpCtx.findHelp((Component)this);
        if (help != null) {
            HelpCtx.setHelpIDString((JComponent)chooser, (String)help.getHelpID());
        }
    }

}

