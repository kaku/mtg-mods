/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.execution.NbProcessDescriptor
 *  org.openide.explorer.propertysheet.ExPropertyEditor
 *  org.openide.explorer.propertysheet.PropertyEnv
 */
package org.netbeans.core.execution.beaninfo.editors;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import org.netbeans.core.execution.beaninfo.editors.NbProcessDescriptorCustomEditor;
import org.openide.execution.NbProcessDescriptor;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;

public class NbProcessDescriptorEditor
implements ExPropertyEditor {
    private PropertyEnv env;
    NbProcessDescriptor pd;
    private PropertyChangeSupport support;

    public NbProcessDescriptorEditor() {
        this.support = new PropertyChangeSupport(this);
    }

    public Object getValue() {
        return this.pd;
    }

    public void setValue(Object value) {
        this.pd = (NbProcessDescriptor)value;
        this.support.firePropertyChange("", null, null);
    }

    public String getAsText() {
        if (this.pd == null) {
            return "null";
        }
        return this.pd.getProcessName() + " " + this.pd.getArguments();
    }

    public void setAsText(String string) {
        String args;
        String prg;
        int indx = (string = string.trim()).indexOf(32);
        if (indx == -1 || new File(string).exists()) {
            prg = string;
            args = "";
        } else {
            prg = string.substring(0, indx);
            args = string.substring(indx + 1);
        }
        NbProcessDescriptor newPD = null;
        newPD = this.pd == null ? new NbProcessDescriptor(prg, args) : new NbProcessDescriptor(prg, args, this.pd.getInfo());
        this.setValue((Object)newPD);
    }

    public String getJavaInitializationString() {
        return null;
    }

    public String[] getTags() {
        return null;
    }

    public boolean isPaintable() {
        return false;
    }

    public void paintValue(Graphics g, Rectangle rectangle) {
    }

    public boolean supportsCustomEditor() {
        return true;
    }

    public Component getCustomEditor() {
        return new NbProcessDescriptorCustomEditor(this, this.env);
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this.support.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this.support.removePropertyChangeListener(propertyChangeListener);
    }

    public void attachEnv(PropertyEnv env) {
        this.env = env;
    }
}

