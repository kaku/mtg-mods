/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.execution;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Writer;
import org.netbeans.core.execution.ExecutionEngine;

final class WriterPrintStream
extends PrintStream {
    private boolean stdOut;
    private static String newLine;

    public WriterPrintStream(OutputStream out, boolean stdOut) {
        super(out, true);
        this.stdOut = stdOut;
    }

    @Override
    public void close() {
    }

    @Override
    public void flush() {
        try {
            if (this.stdOut) {
                ExecutionEngine.getTaskIOs().getOut().flush();
            } else {
                ExecutionEngine.getTaskIOs().getErr().flush();
            }
        }
        catch (IOException e) {
            this.setError();
        }
    }

    private void write(String s) {
        try {
            if (this.stdOut) {
                ExecutionEngine.getTaskIOs().getOut().write(s);
            } else {
                ExecutionEngine.getTaskIOs().getErr().write(s);
            }
        }
        catch (IOException e) {
            this.setError();
        }
    }

    @Override
    public void print(boolean b) {
        this.write(b ? "true" : "false");
    }

    @Override
    public void print(char c) {
        try {
            if (this.stdOut) {
                ExecutionEngine.getTaskIOs().getOut().write(c);
            } else {
                ExecutionEngine.getTaskIOs().getErr().write(c);
            }
        }
        catch (IOException e) {
            this.setError();
        }
    }

    @Override
    public void print(int i) {
        this.write(String.valueOf(i));
    }

    @Override
    public void print(long l) {
        this.write(String.valueOf(l));
    }

    @Override
    public void print(float f) {
        this.write(String.valueOf(f));
    }

    @Override
    public void print(double d) {
        this.write(String.valueOf(d));
    }

    @Override
    public void print(char[] s) {
        try {
            if (this.stdOut) {
                ExecutionEngine.getTaskIOs().getOut().write(s);
            } else {
                ExecutionEngine.getTaskIOs().getErr().write(s);
            }
        }
        catch (IOException e) {
            this.setError();
        }
    }

    @Override
    public void print(String s) {
        if (s == null) {
            s = "null";
        }
        this.write(s);
    }

    @Override
    public void print(Object obj) {
        this.write(String.valueOf(obj));
    }

    @Override
    public void println() {
        this.print(WriterPrintStream.getNewLine());
    }

    @Override
    public void println(boolean x) {
        String out = x ? "true" : "false";
        this.write(out.concat(WriterPrintStream.getNewLine()));
    }

    @Override
    public void println(char x) {
        String nline = WriterPrintStream.getNewLine();
        int nlinelen = nline.length();
        char[] tmp = new char[nlinelen + 1];
        tmp[0] = x;
        for (int i = 0; i < nlinelen; ++i) {
            tmp[i + 1] = nline.charAt(i);
        }
        try {
            if (this.stdOut) {
                ExecutionEngine.getTaskIOs().getOut().write(tmp);
            } else {
                ExecutionEngine.getTaskIOs().getErr().write(tmp);
            }
        }
        catch (IOException e) {
            this.setError();
        }
    }

    @Override
    public void println(int x) {
        this.write(String.valueOf(x).concat(WriterPrintStream.getNewLine()));
    }

    @Override
    public void println(long x) {
        this.write(String.valueOf(x).concat(WriterPrintStream.getNewLine()));
    }

    @Override
    public void println(float x) {
        this.write(String.valueOf(x).concat(WriterPrintStream.getNewLine()));
    }

    @Override
    public void println(double x) {
        this.write(String.valueOf(x).concat(WriterPrintStream.getNewLine()));
    }

    @Override
    public void println(char[] x) {
        String nline = WriterPrintStream.getNewLine();
        int nlinelen = nline.length();
        char[] tmp = new char[x.length + nlinelen];
        System.arraycopy(x, 0, tmp, 0, x.length);
        for (int i = 0; i < nlinelen; ++i) {
            tmp[x.length + i] = nline.charAt(i);
        }
        x = null;
        try {
            if (this.stdOut) {
                ExecutionEngine.getTaskIOs().getOut().write(tmp);
            } else {
                ExecutionEngine.getTaskIOs().getErr().write(tmp);
            }
        }
        catch (IOException e) {
            this.setError();
        }
    }

    @Override
    public void println(String x) {
        if (x == null) {
            x = "null";
        }
        this.print(x.concat(WriterPrintStream.getNewLine()));
    }

    @Override
    public void println(Object x) {
        if (x == null) {
            this.print("null".concat(WriterPrintStream.getNewLine()));
        } else {
            String s = x.toString();
            if (s == null) {
                this.print("<null>".concat(WriterPrintStream.getNewLine()));
            } else {
                this.print(s.concat(WriterPrintStream.getNewLine()));
            }
        }
    }

    private static String getNewLine() {
        if (newLine == null) {
            newLine = System.getProperty("line.separator");
        }
        return newLine;
    }
}

