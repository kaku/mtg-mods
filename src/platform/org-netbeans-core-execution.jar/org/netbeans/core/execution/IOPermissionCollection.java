/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.InputOutput
 */
package org.netbeans.core.execution;

import java.io.Serializable;
import java.security.Permission;
import java.security.PermissionCollection;
import java.util.Enumeration;
import org.netbeans.core.execution.TaskThreadGroup;
import org.openide.windows.InputOutput;

final class IOPermissionCollection
extends PermissionCollection
implements Serializable {
    private InputOutput io;
    private PermissionCollection delegated;
    final TaskThreadGroup grp;
    static final long serialVersionUID = 2046381622544740109L;

    protected IOPermissionCollection(InputOutput io, PermissionCollection delegated, TaskThreadGroup grp) {
        this.io = io;
        this.delegated = delegated;
        this.grp = grp;
    }

    @Override
    public boolean implies(Permission p) {
        return this.delegated.implies(p);
    }

    @Override
    public Enumeration<Permission> elements() {
        return this.delegated.elements();
    }

    @Override
    public void add(Permission perm) {
        this.delegated.add(perm);
    }

    public InputOutput getIO() {
        return this.io;
    }

    public void setIO(InputOutput io) {
        this.io = io;
    }

    @Override
    public String toString() {
        return this.delegated.toString();
    }
}

