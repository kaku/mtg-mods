/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.windows.IOProvider
 *  org.openide.windows.InputOutput
 */
package org.netbeans.core.execution;

import org.netbeans.core.execution.DefaultSysProcess;
import org.netbeans.core.execution.ExecutionEngine;
import org.netbeans.core.execution.ExecutionEvent;
import org.netbeans.core.execution.ExecutorTaskImpl;
import org.netbeans.core.execution.IOThreadIfc;
import org.netbeans.core.execution.TaskIO;
import org.netbeans.core.execution.TaskThreadGroup;
import org.openide.util.NbBundle;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;

final class RunClassThread
extends Thread
implements IOThreadIfc {
    private InputOutput io;
    String allName;
    private final ExecutionEngine engine;
    private final ExecutorTaskImpl task;
    private Runnable run;
    static int number = 0;
    TaskThreadGroup mygroup;
    private boolean finalized;

    public RunClassThread(TaskThreadGroup base, String name, int number, InputOutput io, ExecutionEngine engine, ExecutorTaskImpl task, Runnable run) {
        super((ThreadGroup)base, "exec_" + name + "_" + number);
        this.mygroup = base;
        this.mygroup.setRunClassThread(this);
        this.allName = name;
        this.io = io;
        this.engine = engine;
        this.task = task;
        this.run = run;
        this.setDaemon(false);
        this.start();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void run() {
        DefaultSysProcess def;
        TaskIO tIO;
        this.mygroup.setFinalizable();
        boolean fire = true;
        if (this.allName == null) {
            this.allName = RunClassThread.generateName();
            fire = false;
        }
        String ioname = NbBundle.getMessage(RunClassThread.class, (String)"CTL_ProgramIO", (Object)this.allName);
        if (this.io != null) {
            def = new DefaultSysProcess(this, this.mygroup, this.io, this.allName);
            tIO = new TaskIO(this.io, ioname, true);
            this.io.select();
            ExecutionEngine.getTaskIOs().put(this.io, tIO);
        } else {
            tIO = null;
            tIO = ExecutionEngine.getTaskIOs().getTaskIO(ioname);
            if (tIO == null) {
                this.io = IOProvider.getDefault().getIO(ioname, true);
                tIO = new TaskIO(this.io, ioname);
            } else {
                this.io = tIO.getInout();
            }
            this.io.select();
            this.io.setFocusTaken(true);
            ExecutionEngine.getTaskIOs().put(this.io, tIO);
            def = new DefaultSysProcess(this, this.mygroup, this.io, this.allName);
        }
        ExecutionEvent ev = null;
        try {
            ev = new ExecutionEvent((Object)this.engine, def);
            if (fire) {
                this.engine.fireExecutionStarted(ev);
            }
            Object object = this.task.lock;
            synchronized (object) {
                this.task.proc = def;
                this.task.lock.notifyAll();
            }
            this.run.run();
            this.run = null;
            int result = 2;
            try {
                result = def.result();
            }
            catch (ThreadDeath err) {
            }
            catch (IllegalMonitorStateException e) {
                // empty catch block
            }
            this.task.result = result;
        }
        finally {
            Thread.interrupted();
            if (ev != null && fire) {
                this.engine.fireExecutionFinished(ev);
            }
            ExecutionEngine.closeGroup(this.mygroup);
            this.task.finished();
            ExecutionEngine.getTaskIOs().free(this.mygroup, this.io);
            this.mygroup = null;
            this.io = null;
            RunClassThread result = this;
            synchronized (result) {
                this.finalized = true;
                this.notifyAll();
            }
        }
    }

    @Override
    public InputOutput getInputOutput() {
        return this.io;
    }

    public synchronized void waitForEnd(long timeout) throws InterruptedException {
        if (!this.finalized) {
            this.wait(timeout);
        }
    }

    static String generateName() {
        return NbBundle.getMessage(RunClassThread.class, (String)"CTL_GeneratedName", (Object)number++);
    }
}

