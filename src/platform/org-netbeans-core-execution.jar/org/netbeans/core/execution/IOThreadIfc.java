/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.InputOutput
 */
package org.netbeans.core.execution;

import org.openide.windows.InputOutput;

interface IOThreadIfc {
    public InputOutput getInputOutput();
}

