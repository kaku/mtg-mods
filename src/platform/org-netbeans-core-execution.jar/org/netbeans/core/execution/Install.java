/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.TopSecurityManager
 *  org.netbeans.core.ModuleActions
 *  org.netbeans.modules.progress.spi.Controller
 *  org.netbeans.modules.progress.spi.InternalHandle
 *  org.netbeans.modules.progress.spi.TaskModel
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.awt.Actions
 *  org.openide.awt.Mnemonics
 *  org.openide.execution.ExecutorTask
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.ExplorerUtils
 *  org.openide.explorer.view.ListView
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeMemberEvent
 *  org.openide.nodes.NodeReorderEvent
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.SystemAction
 */
package org.netbeans.core.execution;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import org.netbeans.TopSecurityManager;
import org.netbeans.core.ModuleActions;
import org.netbeans.core.execution.ExecutionEngine;
import org.netbeans.core.execution.ExecutionEvent;
import org.netbeans.core.execution.ExecutionListener;
import org.netbeans.core.execution.SecMan;
import org.netbeans.modules.progress.spi.Controller;
import org.netbeans.modules.progress.spi.InternalHandle;
import org.netbeans.modules.progress.spi.TaskModel;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.awt.Actions;
import org.openide.awt.Mnemonics;
import org.openide.execution.ExecutorTask;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.ListView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;

public class Install
implements Runnable {
    private static final Logger LOG = Logger.getLogger(Install.class.getName());

    @Override
    public void run() {
        TopSecurityManager.register((SecurityManager)SecMan.DEFAULT);
    }

    private static boolean showPendingTasks() {
        if (Boolean.getBoolean("netbeans.full.hack") || Install.getPendingTasks().isEmpty()) {
            return true;
        }
        EM panel = new EM();
        Dialog[] dialog = new Dialog[1];
        AbstractNode root = new AbstractNode((Children)new PendingChildren());
        JButton exitOption = new JButton();
        Mnemonics.setLocalizedText((AbstractButton)exitOption, (String)NbBundle.getMessage(Install.class, (String)"LAB_EndTasks"));
        exitOption.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(Install.class, (String)"ACSD_EndTasks"));
        PendingDialogCloser closer = new PendingDialogCloser(dialog, exitOption);
        panel.getExplorerManager().setRootContext((Node)root);
        panel.getExplorerManager().addPropertyChangeListener((PropertyChangeListener)closer);
        DialogDescriptor dd = new DialogDescriptor((Object)panel, NbBundle.getMessage(Install.class, (String)"CTL_PendingTitle"), true, new Object[]{exitOption, DialogDescriptor.CANCEL_OPTION}, (Object)exitOption, 0, null, (ActionListener)closer);
        dd.setHelpCtx(null);
        if (!Install.getPendingTasks().isEmpty()) {
            root.addNodeListener((NodeListener)closer);
            dialog[0] = DialogDisplayer.getDefault().createDialog(dd);
            dialog[0].addWindowListener(closer);
            dialog[0].setVisible(true);
            dialog[0].dispose();
            if (dd.getValue() == DialogDescriptor.CANCEL_OPTION || dd.getValue() == DialogDescriptor.CLOSED_OPTION) {
                return false;
            }
        }
        return true;
    }

    static Collection<?> getPendingTasks() {
        ArrayList<Object> pendingTasks = new ArrayList<Object>(10);
        pendingTasks.addAll(ModuleActions.getDefaultInstance().getRunningActions());
        ExecutionEngine ee = ExecutionEngine.getExecutionEngine();
        if (ee != null) {
            pendingTasks.addAll(ee.getRunningTasks());
        }
        pendingTasks.addAll(Arrays.asList(Controller.getDefault().getModel().getHandles()));
        return pendingTasks;
    }

    private static void killPendingTasks() {
        LogRecord r = new LogRecord(Level.INFO, "KILL_PENDING_TASKS");
        r.setLoggerName(LOG.getName());
        LOG.log(r);
        for (InternalHandle h : Controller.getDefault().getModel().getHandles()) {
            h.requestCancel();
        }
        Install.killRunningExecutors();
    }

    private static void killRunningExecutors() {
        ExecutionEngine ee = ExecutionEngine.getExecutionEngine();
        if (ee == null) {
            return;
        }
        for (ExecutorTask et : new ArrayList<ExecutorTask>(ee.getRunningTasks())) {
            if (et.isFinished()) continue;
            et.stop();
        }
    }

    private static class NoActionNode
    extends AbstractNode {
        private Image img;

        public NoActionNode(Icon icon, String name, String display) {
            super(Children.LEAF);
            if (icon != null) {
                this.img = ImageUtilities.icon2Image((Icon)icon);
            }
            this.setName(name);
            if (display != null) {
                this.setDisplayName(display);
            }
        }

        public Action[] getActions(boolean context) {
            return new Action[0];
        }

        public Image getIcon(int type) {
            return this.img == null ? super.getIcon(type) : this.img;
        }
    }

    static class PendingChildren
    extends Children.Keys<Object>
    implements ExecutionListener,
    ListDataListener {
        public PendingChildren() {
            ExecutionEngine ee = ExecutionEngine.getExecutionEngine();
            if (ee != null) {
                ee.addExecutionListener(this);
            }
            Controller.getDefault().getModel().addListDataListener((ListDataListener)this);
        }

        protected Node[] createNodes(Object key) {
            Node[] arrnode;
            NoActionNode n = null;
            if (key instanceof Action) {
                Action action = (Action)key;
                Icon icon = action instanceof SystemAction ? ((SystemAction)action).getIcon() : null;
                String actionName = (String)action.getValue("Name");
                if (actionName == null) {
                    actionName = "";
                }
                actionName = Actions.cutAmpersand((String)actionName);
                n = new NoActionNode(icon, actionName, NbBundle.getMessage(Install.class, (String)"CTL_ActionInProgress", (Object)actionName));
            } else if (key instanceof ExecutorTask) {
                n = new NoActionNode(null, key.toString(), NbBundle.getMessage(Install.class, (String)"CTL_PendingExternalProcess2", (Object)ExecutionEngine.getExecutionEngine().getRunningTaskName((ExecutorTask)key)));
            } else if (key instanceof InternalHandle) {
                n = new NoActionNode(null, ((InternalHandle)key).getDisplayName(), null);
            }
            if (n == null) {
                arrnode = null;
            } else {
                Node[] arrnode2 = new Node[1];
                arrnode = arrnode2;
                arrnode2[0] = n;
            }
            return arrnode;
        }

        protected void addNotify() {
            this.setKeys(Install.getPendingTasks());
            Children.Keys.super.addNotify();
        }

        protected void removeNotify() {
            this.setKeys(Collections.emptySet());
            Children.Keys.super.removeNotify();
            ExecutionEngine ee = ExecutionEngine.getExecutionEngine();
            if (ee != null) {
                ee.removeExecutionListener(this);
            }
            Controller.getDefault().getModel().removeListDataListener((ListDataListener)this);
        }

        @Override
        public void startedExecution(ExecutionEvent ev) {
            this.setKeys(Install.getPendingTasks());
        }

        @Override
        public void finishedExecution(ExecutionEvent ev) {
            this.setKeys(Install.getPendingTasks());
        }

        @Override
        public void intervalAdded(ListDataEvent e) {
            this.setKeys(Install.getPendingTasks());
        }

        @Override
        public void intervalRemoved(ListDataEvent e) {
            this.setKeys(Install.getPendingTasks());
        }

        @Override
        public void contentsChanged(ListDataEvent e) {
            this.setKeys(Install.getPendingTasks());
        }
    }

    private static class EM
    extends JPanel
    implements ExplorerManager.Provider {
        private ExplorerManager manager = new ExplorerManager();
        private Lookup lookup;

        public EM() {
            ActionMap map = this.getActionMap();
            map.put("copy-to-clipboard", ExplorerUtils.actionCopy((ExplorerManager)this.manager));
            map.put("cut-to-clipboard", ExplorerUtils.actionCut((ExplorerManager)this.manager));
            map.put("paste-from-clipboard", ExplorerUtils.actionPaste((ExplorerManager)this.manager));
            map.put("delete", ExplorerUtils.actionDelete((ExplorerManager)this.manager, (boolean)true));
            this.lookup = ExplorerUtils.createLookup((ExplorerManager)this.manager, (ActionMap)map);
            this.initComponent();
        }

        private void initComponent() {
            this.setLayout(new GridBagLayout());
            GridBagConstraints cons = new GridBagConstraints();
            cons.gridx = 0;
            cons.gridy = 0;
            cons.weightx = 1.0;
            cons.fill = 2;
            cons.insets = new Insets(11, 11, 0, 12);
            JLabel label = new JLabel();
            Mnemonics.setLocalizedText((JLabel)label, (String)NbBundle.getMessage(Install.class, (String)"LAB_PendingTasks"));
            this.add((Component)label, cons);
            cons.gridy = 1;
            cons.weighty = 1.0;
            cons.fill = 1;
            cons.insets = new Insets(2, 11, 0, 12);
            ListView view = new ListView();
            view.setBorder(UIManager.getBorder("Nb.ScrollPane.border"));
            label.setLabelFor((Component)view);
            this.add((Component)view, cons);
            view.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(Install.class, (String)"ACSD_PendingTasks"));
            this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(Install.class, (String)"ACSD_PendingTitle"));
            Dimension origSize = this.getPreferredSize();
            this.setPreferredSize(new Dimension(origSize.width * 5 / 4, origSize.height));
        }

        public ExplorerManager getExplorerManager() {
            return this.manager;
        }

        public Lookup getLookup() {
            return this.lookup;
        }

        @Override
        public void addNotify() {
            super.addNotify();
            ExplorerUtils.activateActions((ExplorerManager)this.manager, (boolean)true);
        }

        @Override
        public void removeNotify() {
            ExplorerUtils.activateActions((ExplorerManager)this.manager, (boolean)false);
            super.removeNotify();
        }
    }

    private static class PendingDialogCloser
    extends WindowAdapter
    implements Runnable,
    PropertyChangeListener,
    ActionListener,
    NodeListener {
        private Dialog[] dialogHolder;
        private Object exitOption;

        PendingDialogCloser(Dialog[] holder, Object exit) {
            this.dialogHolder = holder;
            this.exitOption = exit;
        }

        @Override
        public void run() {
            this.dialogHolder[0].setVisible(false);
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("exploredContext".equals(evt.getPropertyName())) {
                this.checkClose();
            }
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            if (evt.getSource() == this.exitOption) {
                Install.killPendingTasks();
                Mutex.EVENT.readAccess((Runnable)this);
            }
        }

        public void childrenRemoved(NodeMemberEvent evt) {
            this.checkClose();
        }

        @Override
        public void windowOpened(WindowEvent evt) {
            this.checkClose();
        }

        private void checkClose() {
            if (this.dialogHolder[0] != null && Install.getPendingTasks().isEmpty()) {
                Mutex.EVENT.readAccess((Runnable)this);
            }
        }

        public void childrenAdded(NodeMemberEvent ev) {
        }

        public void childrenReordered(NodeReorderEvent ev) {
        }

        public void nodeDestroyed(NodeEvent ev) {
        }
    }

    public static final class Closing
    implements Callable {
        public Boolean call() throws Exception {
            return Install.showPendingTasks();
        }
    }

    public static final class Down
    implements Runnable {
        @Override
        public void run() {
            Install.showPendingTasks();
            TopSecurityManager.unregister((SecurityManager)SecMan.DEFAULT);
        }
    }

}

