/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.execution;

import org.netbeans.core.execution.ExecutionEvent;

public interface ExecutionListener {
    public void startedExecution(ExecutionEvent var1);

    public void finishedExecution(ExecutionEvent var1);
}

