/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.execution;

import java.lang.reflect.Field;
import java.security.AccessControlContext;
import java.security.AccessController;
import java.security.PermissionCollection;
import java.security.ProtectionDomain;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.netbeans.core.execution.IOPermissionCollection;

class AccController {
    static Field context;

    AccController() {
    }

    static void init() {
    }

    static Field getContextField() throws Exception {
        if (context == null) {
            Field ctx;
            try {
                ctx = AccessControlContext.class.getDeclaredField("context");
            }
            catch (NoSuchFieldException nsfe) {
                ctx = AccessControlContext.class.getDeclaredField("domainsArray");
            }
            ctx.setAccessible(true);
            context = ctx;
        }
        return context;
    }

    static ProtectionDomain[] getDomains(AccessControlContext acc) throws Exception {
        Object o = AccController.getContextField().get(acc);
        if (o.getClass() == Object[].class) {
            Object[] array = (Object[])o;
            ProtectionDomain[] domains = new ProtectionDomain[array.length];
            for (int i = 0; i < array.length; ++i) {
                domains[i] = (ProtectionDomain)array[i];
            }
            return domains;
        }
        return (ProtectionDomain[])o;
    }

    static IOPermissionCollection getIOPermissionCollection() {
        return AccController.getIOPermissionCollection(AccessController.getContext());
    }

    static IOPermissionCollection getIOPermissionCollection(AccessControlContext acc) {
        try {
            ProtectionDomain[] pds = AccController.getDomains(acc);
            for (int i = 0; i < pds.length; ++i) {
                PermissionCollection pc = pds[i].getPermissions();
                if (!(pc instanceof IOPermissionCollection)) continue;
                return (IOPermissionCollection)pc;
            }
            return null;
        }
        catch (Exception e) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    Logger.getLogger(AccController.class.getName()).log(Level.WARNING, null, e);
                }
            });
            return null;
        }
    }

}

