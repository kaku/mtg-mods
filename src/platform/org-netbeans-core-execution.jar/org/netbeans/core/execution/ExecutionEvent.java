/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.execution;

import java.util.EventObject;
import org.netbeans.core.execution.DefaultSysProcess;

public class ExecutionEvent
extends EventObject {
    static final long serialVersionUID = -9181112840849353114L;
    private DefaultSysProcess proc;

    public ExecutionEvent(Object source, DefaultSysProcess proc) {
        super(source);
        this.proc = proc;
    }

    public DefaultSysProcess getProcess() {
        return this.proc;
    }
}

