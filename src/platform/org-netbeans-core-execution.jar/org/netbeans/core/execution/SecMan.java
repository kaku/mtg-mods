/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.execution.NbClassLoader
 *  org.openide.windows.InputOutput
 */
package org.netbeans.core.execution;

import java.awt.Window;
import java.security.AccessControlContext;
import java.security.AccessController;
import java.security.CodeSource;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.security.ProtectionDomain;
import org.netbeans.core.execution.AccController;
import org.netbeans.core.execution.ExecutionEngine;
import org.netbeans.core.execution.ExitSecurityException;
import org.netbeans.core.execution.IOPermissionCollection;
import org.netbeans.core.execution.TaskThreadGroup;
import org.openide.execution.NbClassLoader;
import org.openide.windows.InputOutput;

public class SecMan
extends SecurityManager {
    public static SecurityManager DEFAULT = new SecMan();
    private static Class nbClassLoaderClass = NbClassLoader.class;
    private ThreadGroup base = ExecutionEngine.base;

    public SecMan() {
        PrivilegedCheck.init();
        AccController.init();
    }

    @Override
    public void checkExit(int status) throws SecurityException {
        PrivilegedCheck.checkExit(status, this);
    }

    final void checkExitImpl(int status, AccessControlContext acc) throws SecurityException {
        IOPermissionCollection iopc = AccController.getIOPermissionCollection(acc);
        if (iopc != null && iopc.grp != null) {
            ExecutionEngine.getTaskIOs().free(iopc.grp, iopc.getIO());
            ExecutionEngine.closeGroup(iopc.grp);
            this.stopTaskThreadGroup(iopc.grp);
            throw new ExitSecurityException("Exit from within execution engine, normal");
        }
        ThreadGroup g = Thread.currentThread().getThreadGroup();
        if (g instanceof TaskThreadGroup) {
            throw new ExitSecurityException("Exit from within execution engine, normal");
        }
        if (this.isNbClassLoader()) {
            throw new ExitSecurityException("Exit from within user-loaded code");
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void stopTaskThreadGroup(TaskThreadGroup old) {
        TaskThreadGroup taskThreadGroup = old;
        synchronized (taskThreadGroup) {
            int count = old.activeCount();
            int icurrent = -1;
            Thread current = Thread.currentThread();
            Thread[] thrs = new Thread[count];
            old.enumerate(thrs, true);
            for (int i = 0; i < thrs.length && thrs[i] != null; ++i) {
                if (thrs[i] == current) {
                    icurrent = i;
                    continue;
                }
                thrs[i].stop();
            }
            if (icurrent != -1) {
                thrs[icurrent].stop();
            }
        }
    }

    @Override
    public boolean checkTopLevelWindow(Object window) {
        IOPermissionCollection iopc = AccController.getIOPermissionCollection();
        if (iopc != null && iopc.grp != null && window instanceof Window) {
            ExecutionEngine.putWindow((Window)window, iopc.grp);
        }
        return true;
    }

    protected boolean isNbClassLoader() {
        Class[] ctx = this.getClassContext();
        for (int i = 0; i < ctx.length; ++i) {
            if (!nbClassLoaderClass.isInstance(ctx[i].getClassLoader()) || ctx[i].getProtectionDomain().getCodeSource() == null) continue;
            return true;
        }
        return false;
    }

    private static final class PrivilegedCheck
    implements PrivilegedExceptionAction<Object> {
        private final int action;
        private final SecMan sm;
        private int status;
        private AccessControlContext acc;

        public PrivilegedCheck(int action, SecMan sm) {
            this.action = action;
            this.sm = sm;
            if (action == 0) {
                this.acc = AccessController.getContext();
            }
        }

        static void init() {
        }

        @Override
        public Object run() throws Exception {
            switch (this.action) {
                case 0: {
                    this.sm.checkExitImpl(this.status, this.acc);
                    break;
                }
            }
            return null;
        }

        static void checkExit(int status, SecMan sm) {
            PrivilegedCheck pea = new PrivilegedCheck(0, sm);
            pea.status = status;
            PrivilegedCheck.check(pea);
        }

        private static void check(PrivilegedCheck action) {
            try {
                AccessController.doPrivileged(action);
            }
            catch (PrivilegedActionException e) {
                Exception orig = e.getException();
                if (orig instanceof RuntimeException) {
                    throw (RuntimeException)orig;
                }
                orig.printStackTrace();
            }
        }
    }

}

