/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.InputOutput
 *  org.openide.windows.OutputWriter
 */
package org.netbeans.core.execution;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.HashMap;
import java.util.Hashtable;
import org.netbeans.core.execution.AccController;
import org.netbeans.core.execution.IOPermissionCollection;
import org.netbeans.core.execution.IOThreadIfc;
import org.netbeans.core.execution.TaskIO;
import org.openide.windows.InputOutput;
import org.openide.windows.OutputWriter;

final class IOTable
extends Hashtable<InputOutput, TaskIO> {
    static final long serialVersionUID = 9096333712401558521L;
    private ThreadGroup base;
    private TaskIO systemIO;
    private HashMap<String, TaskIO> freeTaskIOs;
    private boolean searchingIO = false;

    public IOTable(ThreadGroup base, TaskIO systemIO) {
        this.base = base;
        this.systemIO = systemIO;
        this.freeTaskIOs = new HashMap(16);
    }

    ThreadGroup findGroup() {
        ThreadGroup g;
        ThreadGroup old = null;
        for (g = Thread.currentThread().getThreadGroup(); g != null && g != this.base; g = g.getParent()) {
            old = g;
        }
        return g == null ? null : old;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private synchronized TaskIO getIO() {
        TaskIO io;
        if (this.searchingIO) {
            return this.systemIO;
        }
        InputOutput inout = null;
        if (Thread.currentThread() instanceof IOThreadIfc) {
            inout = ((IOThreadIfc)((Object)Thread.currentThread())).getInputOutput();
        }
        IOPermissionCollection iopc = null;
        if (inout == null) {
            try {
                this.searchingIO = true;
                iopc = AccController.getIOPermissionCollection();
            }
            finally {
                this.searchingIO = false;
            }
            if (iopc == null) {
                return this.systemIO;
            }
            inout = iopc.getIO();
        }
        if ((io = (TaskIO)this.get((Object)inout)) == null) {
            return new TaskIO(inout);
        }
        return io;
    }

    synchronized TaskIO getTaskIO(String name) {
        TaskIO ret;
        if (this.reuseTaskIO() && (ret = this.getFreeTaskIO(name)) != null) {
            return ret;
        }
        return null;
    }

    private boolean reuseTaskIO() {
        return true;
    }

    private boolean clearTaskIO() {
        return true;
    }

    private TaskIO getFreeTaskIO(String name) {
        TaskIO t = this.freeTaskIOs.get(name);
        if (t == null) {
            return null;
        }
        if (this.clearTaskIO()) {
            try {
                t.getInout().getOut().reset();
                t.getInout().getErr().reset();
            }
            catch (IOException e) {
                // empty catch block
            }
        }
        t.in = null;
        t.getInout().flushReader();
        this.freeTaskIOs.remove(name);
        return t;
    }

    synchronized void free(ThreadGroup grp, InputOutput io) {
        TaskIO t = (TaskIO)this.get((Object)io);
        if (t == null) {
            return;
        }
        if (t.foreign) {
            return;
        }
        if (t.getName() != "noname" && (t = this.freeTaskIOs.put(t.getName(), t)) != null) {
            t.getInout().closeInputOutput();
        }
        this.remove((Object)io);
    }

    public Reader getIn() {
        TaskIO io = this.getIO();
        if (io.in == null) {
            io.initIn();
        }
        return io.in;
    }

    public Writer getOut() {
        TaskIO io = this.getIO();
        if (io.out == null) {
            io.initOut();
        }
        return io.out;
    }

    public Writer getErr() {
        TaskIO io = this.getIO();
        if (io.err == null) {
            io.initErr();
        }
        return io.err;
    }
}

