/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.startup.Main
 *  org.openide.execution.ExecutionEngine
 *  org.openide.execution.ExecutorTask
 *  org.openide.execution.NbClassPath
 *  org.openide.util.Lookup
 *  org.openide.windows.InputOutput
 */
package org.netbeans.core.execution;

import java.awt.Window;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.io.Writer;
import java.security.CodeSource;
import java.security.PermissionCollection;
import java.security.Policy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import org.netbeans.core.execution.DefaultSysProcess;
import org.netbeans.core.execution.ExecutionEvent;
import org.netbeans.core.execution.ExecutionListener;
import org.netbeans.core.execution.ExecutorTaskImpl;
import org.netbeans.core.execution.IOPermissionCollection;
import org.netbeans.core.execution.IOTable;
import org.netbeans.core.execution.OutputStreamWriter;
import org.netbeans.core.execution.RunClassThread;
import org.netbeans.core.execution.SysIn;
import org.netbeans.core.execution.TaskIO;
import org.netbeans.core.execution.TaskThreadGroup;
import org.netbeans.core.execution.WindowTable;
import org.netbeans.core.execution.WriterPrintStream;
import org.netbeans.core.startup.Main;
import org.openide.execution.ExecutorTask;
import org.openide.execution.NbClassPath;
import org.openide.util.Lookup;
import org.openide.windows.InputOutput;

public final class ExecutionEngine
extends org.openide.execution.ExecutionEngine {
    public static final ThreadGroup base = new ThreadGroup("base");
    private int number = 1;
    public static final TaskIO systemIO = new TaskIO();
    private static final IOTable taskIOs = new IOTable(base, systemIO);
    private static WindowTable wtable = new WindowTable();
    private HashSet<ExecutionListener> executionListeners = new HashSet();
    private List<ExecutorTask> runningTasks = Collections.synchronizedList(new ArrayList(5));
    static final long serialVersionUID = 9072488605180080803L;

    public ExecutionEngine() {
        System.setIn(new SysIn());
        System.setOut(ExecutionEngine.createPrintStream(true));
        System.setErr(ExecutionEngine.createPrintStream(false));
    }

    public static ExecutionEngine getExecutionEngine() {
        ExecutionEngine ee = (ExecutionEngine)((Object)Lookup.getDefault().lookup(ExecutionEngine.class));
        if (ee != null) {
            return ee;
        }
        org.openide.execution.ExecutionEngine ee2 = (org.openide.execution.ExecutionEngine)Lookup.getDefault().lookup(org.openide.execution.ExecutionEngine.class);
        if (ee2 instanceof ExecutionEngine) {
            return (ExecutionEngine)ee2;
        }
        return null;
    }

    public Collection<ExecutorTask> getRunningTasks() {
        return Arrays.asList(this.runningTasks.toArray((T[])new ExecutorTask[0]));
    }

    public String getRunningTaskName(ExecutorTask task) {
        if (!this.runningTasks.contains((Object)task) || !(task instanceof DefaultSysProcess)) {
            return null;
        }
        return ((DefaultSysProcess)task).getName();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public ExecutorTask execute(String name, Runnable run, final InputOutput inout) {
        TaskThreadGroup g = new TaskThreadGroup(base, "exec_" + name + "_" + this.number);
        g.setDaemon(true);
        ExecutorTaskImpl task = new ExecutorTaskImpl();
        Object object = task.lock;
        synchronized (object) {
            try {
                new RunClassThread(g, name, this.number++, inout, this, task, run);
                task.lock.wait();
            }
            catch (InterruptedException e) {
                inout.closeInputOutput();
                return new ExecutorTask(null){

                    public void stop() {
                    }

                    public int result() {
                        return 2;
                    }

                    public InputOutput getInputOutput() {
                        return inout;
                    }
                };
            }
        }
        return task;
    }

    protected NbClassPath createLibraryPath() {
        List l = Main.getModuleSystem().getModuleJars();
        return new NbClassPath(l.toArray(new File[l.size()]));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final void addExecutionListener(ExecutionListener l) {
        HashSet<ExecutionListener> hashSet = this.executionListeners;
        synchronized (hashSet) {
            this.executionListeners.add(l);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final void removeExecutionListener(ExecutionListener l) {
        HashSet<ExecutionListener> hashSet = this.executionListeners;
        synchronized (hashSet) {
            this.executionListeners.remove(l);
        }
    }

    protected final PermissionCollection createPermissions(CodeSource cs, InputOutput io) {
        PermissionCollection pc = Policy.getPolicy().getPermissions(cs);
        ThreadGroup grp = Thread.currentThread().getThreadGroup();
        return new IOPermissionCollection(io, pc, grp instanceof TaskThreadGroup ? (TaskThreadGroup)grp : null);
    }

    protected final void fireExecutionStarted(ExecutionEvent ev) {
        this.runningTasks.add(ev.getProcess());
        for (ExecutionListener l : (HashSet)this.executionListeners.clone()) {
            l.startedExecution(ev);
        }
    }

    protected final void fireExecutionFinished(ExecutionEvent ev) {
        this.runningTasks.remove((Object)ev.getProcess());
        for (ExecutionListener l : (HashSet)this.executionListeners.clone()) {
            l.finishedExecution(ev);
        }
        ev.getProcess().destroyThreadGroup(base);
    }

    static void putWindow(Window w, TaskThreadGroup tg) {
        wtable.putTaskWindow(w, tg);
    }

    static void closeGroup(ThreadGroup tg) {
        wtable.closeGroup(tg);
    }

    static boolean hasWindows(ThreadGroup tg) {
        return wtable.hasWindows(tg);
    }

    static IOTable getTaskIOs() {
        return taskIOs;
    }

    public static ThreadGroup findGroup() {
        ThreadGroup g;
        ThreadGroup old = null;
        for (g = Thread.currentThread().getThreadGroup(); g != null && g != base; g = g.getParent()) {
            old = g;
        }
        return g == null ? null : old;
    }

    static PrintStream createPrintStream(boolean stdOut) {
        return new WriterPrintStream(new SysOut(stdOut), stdOut);
    }

    static {
        ExecutionEngine.systemIO.out = new OutputStreamWriter(System.out);
        ExecutionEngine.systemIO.err = new OutputStreamWriter(System.err);
        ExecutionEngine.systemIO.in = new InputStreamReader(System.in);
    }

    static class SysOut
    extends OutputStream {
        boolean std;

        SysOut(boolean std) {
            this.std = std;
        }

        @Override
        public void write(int b) throws IOException {
            if (this.std) {
                ExecutionEngine.getTaskIOs().getOut().write(b);
            } else {
                ExecutionEngine.getTaskIOs().getErr().write(b);
            }
        }

        @Override
        public void write(byte[] buff, int off, int len) throws IOException {
            String s = new String(buff, off, len);
            if (this.std) {
                ExecutionEngine.getTaskIOs().getOut().write(s.toCharArray(), 0, s.length());
            } else {
                ExecutionEngine.getTaskIOs().getErr().write(s.toCharArray(), 0, s.length());
            }
        }

        @Override
        public void flush() throws IOException {
            if (this.std) {
                ExecutionEngine.getTaskIOs().getOut().flush();
            } else {
                ExecutionEngine.getTaskIOs().getErr().flush();
            }
        }

        @Override
        public void close() throws IOException {
            if (this.std) {
                ExecutionEngine.getTaskIOs().getOut().close();
            } else {
                ExecutionEngine.getTaskIOs().getErr().close();
            }
        }
    }

}

