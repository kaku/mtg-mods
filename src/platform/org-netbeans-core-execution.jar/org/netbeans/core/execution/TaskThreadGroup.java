/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.execution;

import org.netbeans.core.execution.ExecutionEngine;
import org.netbeans.core.execution.RunClassThread;

final class TaskThreadGroup
extends ThreadGroup {
    private Object TIMER = new Object();
    private boolean runClassThreadOut = false;
    private boolean finalizable;
    private boolean dead = false;
    private RunClassThread runClassThread;

    TaskThreadGroup(ThreadGroup parent, String name) {
        super(parent, name);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean isProcessDead(boolean inside) {
        TaskThreadGroup taskThreadGroup = this;
        synchronized (taskThreadGroup) {
            if (this.dead) {
                return true;
            }
            if (!this.finalizable) {
                return false;
            }
            int count = this.activeCount();
            if (count > 1) {
                int active = 0;
                Thread[] threads = new Thread[count];
                this.enumerate(threads);
                int i = threads.length;
                while (--i >= 0) {
                    if (threads[i] == null || !threads[i].isAlive() || threads[i].isDaemon() || ++active <= 1) continue;
                    return false;
                }
                count = active;
            }
            if (ExecutionEngine.hasWindows(this)) {
                return false;
            }
            if (count == 0) {
                return true;
            }
            if (inside) {
                return true;
            }
            return false;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void waitFor() throws InterruptedException {
        boolean inside = Thread.currentThread() instanceof RunClassThread;
        Object object = this.TIMER;
        synchronized (object) {
            try {
                while (!this.isProcessDead(inside)) {
                    this.TIMER.wait(1000);
                }
            }
            finally {
                this.TIMER.notifyAll();
                TaskThreadGroup taskThreadGroup = this;
                synchronized (taskThreadGroup) {
                    this.dead = true;
                }
            }
        }
    }

    synchronized void setFinalizable() {
        this.finalizable = true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void kill() {
        Object object = this.TIMER;
        synchronized (object) {
            if (!this.dead) {
                this.dead = true;
                this.TIMER.notifyAll();
                try {
                    this.TIMER.wait(3000);
                }
                catch (InterruptedException e) {
                    // empty catch block
                }
            }
        }
    }

    public void setRunClassThread(RunClassThread t) {
        this.runClassThread = t;
    }

    public RunClassThread getRunClassThread() {
        return this.runClassThread;
    }
}

