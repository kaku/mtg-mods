/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.execution;

class ExitSecurityException
extends SecurityException {
    static final long serialVersionUID = -8973677308554045785L;

    public ExitSecurityException() {
    }

    public ExitSecurityException(String s) {
        super(s);
    }
}

