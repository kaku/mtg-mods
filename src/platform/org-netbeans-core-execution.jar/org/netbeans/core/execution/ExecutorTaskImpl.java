/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.execution.ExecutorTask
 *  org.openide.windows.InputOutput
 */
package org.netbeans.core.execution;

import org.netbeans.core.execution.DefaultSysProcess;
import org.openide.execution.ExecutorTask;
import org.openide.windows.InputOutput;

final class ExecutorTaskImpl
extends ExecutorTask {
    int result = -1;
    DefaultSysProcess proc;
    Object lock;

    ExecutorTaskImpl() {
        super(new Runnable(){

            @Override
            public void run() {
            }
        });
        this.lock = this;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void stop() {
        try {
            Object object = this.lock;
            synchronized (object) {
                while (this.proc == null) {
                    this.lock.wait();
                }
                this.proc.stop();
            }
        }
        catch (InterruptedException e) {
            // empty catch block
        }
    }

    public int result() {
        this.waitFinished();
        this.proc.stop();
        return this.result;
    }

    final void finished() {
        this.notifyFinished();
    }

    public void run() {
        this.waitFinished();
    }

    public InputOutput getInputOutput() {
        return this.proc.getInputOutput();
    }

}

