/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.execution;

import java.io.IOException;
import java.io.PrintStream;
import java.io.Writer;

final class OutputStreamWriter
extends Writer {
    private PrintStream out;

    public OutputStreamWriter(PrintStream out) {
        super(out);
        if (out == null) {
            throw new NullPointerException();
        }
        this.out = out;
    }

    private void ensureOpen() throws IOException {
        if (this.out == null) {
            throw new IOException("Stream closed");
        }
    }

    @Override
    public void write(int c) throws IOException {
        char[] cbuf = new char[]{(char)c};
        this.write(cbuf, 0, 1);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void write(char[] cbuf, int off, int len) throws IOException {
        Object object = this.lock;
        synchronized (object) {
            this.ensureOpen();
            if (off == 0 && len == cbuf.length) {
                this.out.print(cbuf);
            } else {
                char[] chars = new char[len];
                System.arraycopy(cbuf, off, chars, 0, len);
                this.out.print(chars);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void write(String str, int off, int len) throws IOException {
        Object object = this.lock;
        synchronized (object) {
            this.ensureOpen();
            if (off == 0 && len == str.length()) {
                this.out.print(str);
            } else {
                char[] chars = new char[len];
                str.getChars(off, off + len, chars, 0);
                this.out.print(chars);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void flush() {
        Object object = this.lock;
        synchronized (object) {
            if (this.out == null) {
                return;
            }
            this.out.flush();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void close() {
        Object object = this.lock;
        synchronized (object) {
            if (this.out == null) {
                return;
            }
            this.flush();
            this.out.close();
            this.out = null;
        }
    }
}

