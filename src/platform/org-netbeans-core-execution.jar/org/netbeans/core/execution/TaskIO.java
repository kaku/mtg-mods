/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.InputOutput
 *  org.openide.windows.OutputWriter
 */
package org.netbeans.core.execution;

import java.io.Reader;
import java.io.Writer;
import org.openide.windows.InputOutput;
import org.openide.windows.OutputWriter;

class TaskIO {
    static final String VOID = "noname";
    Writer out;
    Writer err;
    Reader in;
    InputOutput inout;
    private String name;
    boolean foreign;

    TaskIO() {
        this.name = "noname";
    }

    TaskIO(InputOutput inout) {
        this(inout, "noname");
    }

    TaskIO(InputOutput inout, String name) {
        this.inout = inout;
        this.name = name;
    }

    TaskIO(InputOutput inout, String name, boolean foreign) {
        this.inout = inout;
        this.name = name;
        this.foreign = foreign;
    }

    void initOut() {
        if (this.out == null) {
            this.out = this.inout.getOut();
        }
    }

    void initErr() {
        if (this.err == null) {
            this.err = this.inout.getErr();
        }
    }

    void initIn() {
        if (this.in == null) {
            this.in = this.inout.getIn();
        }
    }

    String getName() {
        return this.name;
    }

    InputOutput getInout() {
        return this.inout;
    }
}

