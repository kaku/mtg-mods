/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.execution;

import java.awt.Frame;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import org.netbeans.core.execution.TaskThreadGroup;

final class WindowTable
extends HashMap<Window, TaskThreadGroup> {
    static final long serialVersionUID = -1494996298725028533L;
    private WindowListener winListener;
    private HashMap<ThreadGroup, ArrayList<Window>> windowMap = new HashMap(16);
    private static Frame shOwnerFrame;

    public WindowTable() {
        super(16);
        this.winListener = new WindowAdapter(){

            @Override
            public void windowClosed(WindowEvent ev) {
                Window win = (Window)ev.getSource();
                WindowTable.this.removeWindow(win);
                win.removeWindowListener(this);
            }
        };
    }

    public synchronized void putTaskWindow(Window win, TaskThreadGroup grp) {
        ArrayList vec = this.windowMap.get(grp);
        if (vec == null) {
            vec = new ArrayList();
            this.windowMap.put(grp, vec);
        }
        vec.add(win);
        win.addWindowListener(this.winListener);
        HashMap.super.put(win, grp);
    }

    public TaskThreadGroup getThreadGroup(Window win) {
        return (TaskThreadGroup)HashMap.super.get(win);
    }

    void closeGroup(ThreadGroup grp) {
        ArrayList<Window> vec = this.windowMap.get(grp);
        if (vec == null) {
            return;
        }
        for (Window win : vec) {
            win.setVisible(false);
            this.remove(win);
            if (win == WindowTable.getSharedOwnerFrame()) continue;
            win.dispose();
        }
        this.windowMap.remove(grp);
    }

    private static Frame getSharedOwnerFrame() {
        if (shOwnerFrame != null) {
            return shOwnerFrame;
        }
        try {
            Class swUtil = Class.forName("javax.swing.SwingUtilities");
            Method getter = swUtil.getDeclaredMethod("getSharedOwnerFrame", new Class[0]);
            getter.setAccessible(true);
            shOwnerFrame = (Frame)getter.invoke(null, new Object[0]);
        }
        catch (Exception e) {
            // empty catch block
        }
        return shOwnerFrame;
    }

    boolean hasWindows(ThreadGroup grp) {
        ArrayList<Window> vec = this.windowMap.get(grp);
        if (vec == null || vec.size() == 0 || this.hiddenWindows(vec)) {
            return false;
        }
        return true;
    }

    private boolean hiddenWindows(ArrayList<Window> vec) {
        for (Window win : vec) {
            if (!win.isVisible()) continue;
            return false;
        }
        return true;
    }

    private void removeWindow(Window win) {
        Object obj = this.get(win);
        if (obj == null) {
            return;
        }
        this.remove(win);
        ArrayList<Window> vec = this.windowMap.get(obj);
        if (vec == null) {
            return;
        }
        vec.remove(win);
    }

}

