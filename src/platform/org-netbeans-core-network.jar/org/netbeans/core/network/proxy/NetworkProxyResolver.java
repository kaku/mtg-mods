/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.network.proxy;

import org.netbeans.core.network.proxy.NetworkProxySettings;

public interface NetworkProxyResolver {
    public NetworkProxySettings getNetworkProxySettings();
}

