/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.jna.Library
 *  com.sun.jna.Native
 *  com.sun.jna.Pointer
 */
package org.netbeans.core.network.proxy.mac;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

public interface MacNetworkProxyLibrary
extends Library {
    public static final MacNetworkProxyLibrary LIBRARY = (MacNetworkProxyLibrary)Native.loadLibrary((String)"CoreServices", MacNetworkProxyLibrary.class);

    public Pointer CFNetworkCopySystemProxySettings();
}

