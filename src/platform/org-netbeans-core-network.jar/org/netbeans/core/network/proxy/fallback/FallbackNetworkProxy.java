/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.network.proxy.fallback;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.network.proxy.NetworkProxyResolver;
import org.netbeans.core.network.proxy.NetworkProxySettings;
import org.openide.util.NbBundle;

public class FallbackNetworkProxy
implements NetworkProxyResolver {
    private static final Logger LOGGER = Logger.getLogger(FallbackNetworkProxy.class.getName());
    private static final String AT = "@";
    private static final String COMMA = ",";
    private static final String SLASH = "/";
    private static final String PROTOCOL_PREXIF_SEPARATOR = "://";
    private static final String EMPTY_STRING = "";
    private static final String HTTP_PROXY_SYS_PROPERTY = "http_proxy";
    private static final String HTTPS_PROXY_SYS_PROPERTY = "https_proxy";
    private static final String SOCKS_PROXY_SYS_PROPERTY = "socks_proxy";
    private static final String NO_PROXY_SYS_PROPERTY = "no_proxy";
    private static final String DEFAULT_NO_PROXY_HOSTS = NbBundle.getMessage(FallbackNetworkProxy.class, (String)"DefaulNoProxyHosts");

    @Override
    public NetworkProxySettings getNetworkProxySettings() {
        LOGGER.log(Level.FINE, "Fallback system proxy resolver started.");
        String httpProxyRaw = System.getenv("http_proxy");
        if (httpProxyRaw != null && !httpProxyRaw.isEmpty()) {
            String[] noProxyHosts;
            String httpsProxyRaw = System.getenv("https_proxy");
            String socksProxyRaw = System.getenv("socks_proxy");
            String noProxyRaw = System.getenv("no_proxy");
            LOGGER.log(Level.INFO, "Fallback system proxy resolver: http_proxy={0}", httpProxyRaw);
            LOGGER.log(Level.INFO, "Fallback system proxy resolver: https_proxy={0}", httpsProxyRaw);
            LOGGER.log(Level.INFO, "Fallback system proxy resolver: socks_proxy={0}", socksProxyRaw);
            LOGGER.log(Level.INFO, "Fallback system proxy resolver: no_proxy={0}", noProxyRaw);
            String httpProxy = this.prepareVariable(httpProxyRaw);
            String httpsProxy = this.prepareVariable(httpsProxyRaw);
            String socksProxy = this.prepareVariable(socksProxyRaw);
            if (noProxyRaw == null) {
                noProxyHosts = DEFAULT_NO_PROXY_HOSTS.split(",");
                LOGGER.log(Level.INFO, "Fallback system proxy resolver: no proxy set to default");
            } else {
                noProxyHosts = noProxyRaw.split(",");
            }
            return new NetworkProxySettings(httpProxy, httpsProxy, socksProxy, noProxyHosts);
        }
        LOGGER.log(Level.INFO, "Fallback system proxy resolver: no http_proxy variable found");
        return new NetworkProxySettings();
    }

    private String prepareVariable(String variable) {
        if (variable == null) {
            return "";
        }
        if (variable.endsWith("/")) {
            variable = variable.substring(0, variable.length() - 1);
        }
        if (variable.contains("@")) {
            variable = variable.substring(variable.lastIndexOf("@") + 1);
        }
        if (variable.contains("://")) {
            variable = variable.substring(variable.indexOf("://") + 3);
        }
        return variable;
    }
}

