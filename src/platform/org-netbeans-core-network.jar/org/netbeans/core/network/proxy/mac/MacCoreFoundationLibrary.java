/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.jna.Library
 *  com.sun.jna.Native
 *  com.sun.jna.Pointer
 */
package org.netbeans.core.network.proxy.mac;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

public interface MacCoreFoundationLibrary
extends Library {
    public static final MacCoreFoundationLibrary LIBRARY = (MacCoreFoundationLibrary)Native.loadLibrary((String)"CoreFoundation", MacCoreFoundationLibrary.class);

    public boolean CFDictionaryGetValueIfPresent(Pointer var1, Pointer var2, Pointer[] var3);

    public Pointer CFDictionaryGetValue(Pointer var1, Pointer var2);

    public Pointer CFStringCreateWithCString(Pointer var1, byte[] var2, Pointer var3);

    public long CFStringGetLength(Pointer var1);

    public long CFStringGetMaximumSizeForEncoding(long var1, int var3);

    public boolean CFStringGetCString(Pointer var1, Pointer var2, long var3, int var5);

    public Pointer CFNumberGetType(Pointer var1);

    public boolean CFNumberGetValue(Pointer var1, Pointer var2, Pointer var3);

    public long CFNumberGetByteSize(Pointer var1);

    public long CFArrayGetCount(Pointer var1);

    public Pointer CFArrayGetValueAtIndex(Pointer var1, Pointer var2);
}

