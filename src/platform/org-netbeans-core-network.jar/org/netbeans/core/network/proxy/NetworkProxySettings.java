/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.network.proxy;

import java.util.logging.Level;
import java.util.logging.Logger;

public final class NetworkProxySettings {
    private static final Logger LOGGER = Logger.getLogger(NetworkProxySettings.class.getName());
    private static final String COLON = ":";
    private static final String SLASH = "/";
    private static final String EMPTY_STRING = "";
    private final boolean resolved;
    private final ProxyMode proxyMode;
    private final String httpProxyHost;
    private final String httpProxyPort;
    private final String httpsProxyHost;
    private final String httpsProxyPort;
    private final String socksProxyHost;
    private final String socksProxyPort;
    private final String pacFileUrl;
    private final String[] noProxyHosts;

    public NetworkProxySettings() {
        this.resolved = true;
        this.proxyMode = ProxyMode.DIRECT;
        this.pacFileUrl = null;
        this.httpProxyHost = null;
        this.httpProxyPort = null;
        this.httpsProxyHost = null;
        this.httpsProxyPort = null;
        this.socksProxyHost = null;
        this.socksProxyPort = null;
        this.noProxyHosts = new String[0];
    }

    public NetworkProxySettings(String httpProxy, String[] noProxyHosts) {
        String httpProxyHostChecked = this.getHost(httpProxy);
        String httpProxyPortChecked = this.getPort(httpProxy);
        this.resolved = true;
        this.proxyMode = ProxyMode.MANUAL;
        this.pacFileUrl = null;
        this.httpProxyHost = httpProxyHostChecked;
        this.httpProxyPort = httpProxyPortChecked;
        this.httpsProxyHost = httpProxyHostChecked;
        this.httpsProxyPort = httpProxyPortChecked;
        this.socksProxyHost = httpProxyHostChecked;
        this.socksProxyPort = httpProxyPortChecked;
        this.noProxyHosts = this.checkArray(noProxyHosts);
    }

    public NetworkProxySettings(String httpProxy, String httpsProxy, String socksProxy, String[] noProxyHosts) {
        this.resolved = true;
        this.proxyMode = ProxyMode.MANUAL;
        this.pacFileUrl = null;
        this.httpProxyHost = this.getHost(httpProxy);
        this.httpProxyPort = this.getPort(httpProxy);
        this.httpsProxyHost = this.getHost(httpsProxy);
        this.httpsProxyPort = this.getPort(httpsProxy);
        this.socksProxyHost = this.getHost(socksProxy);
        this.socksProxyPort = this.getPort(socksProxy);
        this.noProxyHosts = this.checkArray(noProxyHosts);
    }

    public NetworkProxySettings(String httpProxyHost, String httpProxyPort, String[] noProxyHosts) {
        String httpProxyHostChecked = this.checkNull(httpProxyHost);
        String httpProxyPortChecked = this.checkNumber(httpProxyPort);
        this.resolved = true;
        this.proxyMode = ProxyMode.MANUAL;
        this.pacFileUrl = null;
        this.httpProxyHost = httpProxyHostChecked;
        this.httpProxyPort = httpProxyPortChecked;
        this.httpsProxyHost = httpProxyHostChecked;
        this.httpsProxyPort = httpProxyPortChecked;
        this.socksProxyHost = httpProxyHostChecked;
        this.socksProxyPort = httpProxyPortChecked;
        this.noProxyHosts = this.checkArray(noProxyHosts);
    }

    public NetworkProxySettings(String httpProxyHost, String httpProxyPort, String httpsProxyHost, String httpsProxyPort, String socksProxyHost, String socksProxyPort, String[] noProxyHosts) {
        this.resolved = true;
        this.proxyMode = ProxyMode.MANUAL;
        this.pacFileUrl = null;
        this.httpProxyHost = this.checkNull(httpProxyHost);
        this.httpProxyPort = this.checkNumber(httpProxyPort);
        this.httpsProxyHost = this.checkNull(httpsProxyHost);
        this.httpsProxyPort = this.checkNumber(httpsProxyPort);
        this.socksProxyHost = this.checkNull(socksProxyHost);
        this.socksProxyPort = this.checkNumber(socksProxyPort);
        this.noProxyHosts = this.checkArray(noProxyHosts);
    }

    public NetworkProxySettings(String pacFileUrl) {
        this.resolved = true;
        this.proxyMode = ProxyMode.AUTO;
        this.pacFileUrl = this.checkNull(pacFileUrl);
        this.httpProxyHost = null;
        this.httpProxyPort = null;
        this.httpsProxyHost = null;
        this.httpsProxyPort = null;
        this.socksProxyHost = null;
        this.socksProxyPort = null;
        this.noProxyHosts = new String[0];
    }

    public NetworkProxySettings(boolean resolved) {
        this.resolved = resolved;
        this.proxyMode = ProxyMode.DIRECT;
        this.pacFileUrl = null;
        this.httpProxyHost = null;
        this.httpProxyPort = null;
        this.httpsProxyHost = null;
        this.httpsProxyPort = null;
        this.socksProxyHost = null;
        this.socksProxyPort = null;
        this.noProxyHosts = new String[0];
    }

    private String getHost(String string) {
        if (string == null) {
            return "";
        }
        if (string.contains(":")) {
            return string.substring(0, string.lastIndexOf(":"));
        }
        return string;
    }

    private String getPort(String string) {
        if (string == null) {
            return "";
        }
        if (string.endsWith("/")) {
            string = string.substring(string.length() - 1, string.length());
        }
        if (string.contains(":")) {
            return string.substring(string.lastIndexOf(":") + 1);
        }
        return "";
    }

    private String checkNull(String string) {
        return string == null ? "" : string;
    }

    private String checkNumber(String string) {
        if (string != null) {
            try {
                Integer.parseInt(string);
                return string;
            }
            catch (NumberFormatException nfe) {
                LOGGER.log(Level.SEVERE, "Cannot parse number {0}", string);
            }
        }
        return "";
    }

    private String[] checkArray(String[] array) {
        return array == null ? new String[]{} : array;
    }

    public boolean isResolved() {
        return this.resolved;
    }

    public ProxyMode getProxyMode() {
        return this.proxyMode;
    }

    public String getHttpProxyHost() {
        return this.httpProxyHost;
    }

    public String getHttpProxyPort() {
        return this.httpProxyPort;
    }

    public String getHttpsProxyHost() {
        return this.httpsProxyHost;
    }

    public String getHttpsProxyPort() {
        return this.httpsProxyPort;
    }

    public String getSocksProxyHost() {
        return this.socksProxyHost;
    }

    public String getSocksProxyPort() {
        return this.socksProxyPort;
    }

    public String getPacFileUrl() {
        return this.pacFileUrl;
    }

    public String[] getNoProxyHosts() {
        return this.noProxyHosts;
    }

    public static enum ProxyMode {
        DIRECT,
        AUTO,
        MANUAL;
        

        private ProxyMode() {
        }
    }

}

