/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.jna.Memory
 *  com.sun.jna.NativeLibrary
 *  com.sun.jna.Pointer
 */
package org.netbeans.core.network.proxy.mac;

import com.sun.jna.Memory;
import com.sun.jna.NativeLibrary;
import com.sun.jna.Pointer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.network.proxy.NetworkProxyResolver;
import org.netbeans.core.network.proxy.NetworkProxySettings;
import org.netbeans.core.network.proxy.mac.MacCoreFoundationLibrary;
import org.netbeans.core.network.proxy.mac.MacNetworkProxyLibrary;

public class MacNetworkProxy
implements NetworkProxyResolver {
    private static final Logger LOGGER = Logger.getLogger(MacNetworkProxy.class.getName());
    private static final MacNetworkProxyLibrary cfNetworkLibrary = MacNetworkProxyLibrary.LIBRARY;
    private static final MacCoreFoundationLibrary cfLibrary = MacCoreFoundationLibrary.LIBRARY;
    private static final String COMMA = ",";
    private static final NativeLibrary NETWORK_LIBRARY = NativeLibrary.getInstance((String)"CoreServices");
    private static final String KEY_AUTO_DISCOVERY_ENABLE = "kCFNetworkProxiesProxyAutoDiscoveryEnable";
    private static final String KEY_PAC_ENABLE = "kCFNetworkProxiesProxyAutoConfigEnable";
    private static final String KEY_PAC_URL = "kCFNetworkProxiesProxyAutoConfigURLString";
    private static final String KEY_HTTP_ENABLE = "kCFNetworkProxiesHTTPEnable";
    private static final String KEY_HTTP_HOST = "kCFNetworkProxiesHTTPProxy";
    private static final String KEY_HTTP_PORT = "kCFNetworkProxiesHTTPPort";
    private static final String KEY_HTTPS_ENABLE = "kCFNetworkProxiesHTTPSEnable";
    private static final String KEY_HTTPS_HOST = "kCFNetworkProxiesHTTPSProxy";
    private static final String KEY_HTTPS_PORT = "kCFNetworkProxiesHTTPSPort";
    private static final String KEY_SOCKS_ENABLE = "kCFNetworkProxiesSOCKSEnable";
    private static final String KEY_SOCKS_HOST = "kCFNetworkProxiesSOCKSProxy";
    private static final String KEY_SOCKS_PORT = "kCFNetworkProxiesSOCKSPort";
    private static final String KEY_EXCEPTIONS_LIST = "kCFNetworkProxiesExceptionsList";

    @Override
    public NetworkProxySettings getNetworkProxySettings() {
        Pointer pacEnable;
        boolean resolved = false;
        LOGGER.log(Level.FINE, "Mac system proxy resolver started.");
        Pointer settingsDictionary = cfNetworkLibrary.CFNetworkCopySystemProxySettings();
        Pointer autoDiscoveryEnable = cfLibrary.CFDictionaryGetValue(settingsDictionary, this.getKeyCFStringRef("kCFNetworkProxiesProxyAutoDiscoveryEnable"));
        if (this.getIntFromCFNumberRef(autoDiscoveryEnable) != 0) {
            LOGGER.log(Level.INFO, "Mac system proxy resolver: auto detect");
            resolved = true;
        }
        if (this.getIntFromCFNumberRef(pacEnable = cfLibrary.CFDictionaryGetValue(settingsDictionary, this.getKeyCFStringRef("kCFNetworkProxiesProxyAutoConfigEnable"))) != 0) {
            Pointer[] pacUrlPointer = new Pointer[1];
            if (cfLibrary.CFDictionaryGetValueIfPresent(settingsDictionary, this.getKeyCFStringRef("kCFNetworkProxiesProxyAutoConfigURLString"), pacUrlPointer)) {
                String pacUrl = this.getStringFromCFStringRef(pacUrlPointer[0]);
                LOGGER.log(Level.INFO, "Mac system proxy resolver: auto - PAC ({0})", pacUrl);
                return new NetworkProxySettings(pacUrl);
            }
        }
        Pointer httpEnable = cfLibrary.CFDictionaryGetValue(settingsDictionary, this.getKeyCFStringRef("kCFNetworkProxiesHTTPEnable"));
        Pointer httpsEnable = cfLibrary.CFDictionaryGetValue(settingsDictionary, this.getKeyCFStringRef("kCFNetworkProxiesHTTPSEnable"));
        Pointer socksEnable = cfLibrary.CFDictionaryGetValue(settingsDictionary, this.getKeyCFStringRef("kCFNetworkProxiesSOCKSEnable"));
        if (this.getIntFromCFNumberRef(httpEnable) != 0 || this.getIntFromCFNumberRef(httpsEnable) != 0 || this.getIntFromCFNumberRef(socksEnable) != 0) {
            String httpHost = this.getStringFromCFStringRef(this.getValueIfExists(settingsDictionary, "kCFNetworkProxiesHTTPProxy"));
            String httpPort = this.getStringFromCFNumberRef(this.getValueIfExists(settingsDictionary, "kCFNetworkProxiesHTTPPort"));
            String httpsHost = this.getStringFromCFStringRef(this.getValueIfExists(settingsDictionary, "kCFNetworkProxiesHTTPSProxy"));
            String httpsPort = this.getStringFromCFNumberRef(this.getValueIfExists(settingsDictionary, "kCFNetworkProxiesHTTPSPort"));
            String socksHost = this.getStringFromCFStringRef(this.getValueIfExists(settingsDictionary, "kCFNetworkProxiesSOCKSProxy"));
            String socksPort = this.getStringFromCFNumberRef(this.getValueIfExists(settingsDictionary, "kCFNetworkProxiesSOCKSPort"));
            String[] noProxyHosts = this.getNoProxyHosts(this.getValueIfExists(settingsDictionary, "kCFNetworkProxiesExceptionsList"));
            LOGGER.log(Level.INFO, "Mac system proxy resolver: manual - http host ({0})", httpHost);
            LOGGER.log(Level.INFO, "Mac system proxy resolver: manual - http port ({0})", httpPort);
            LOGGER.log(Level.INFO, "Mac system proxy resolver: manual - https host ({0})", httpsHost);
            LOGGER.log(Level.INFO, "Mac system proxy resolver: manual - https port ({0})", httpsPort);
            LOGGER.log(Level.INFO, "Mac system proxy resolver: manual - socks host ({0})", socksHost);
            LOGGER.log(Level.INFO, "Mac system proxy resolver: manual - socks port ({0})", socksPort);
            LOGGER.log(Level.INFO, "Mac system proxy resolver: manual - no proxy hosts ({0})", MacNetworkProxy.getStringFromArray(noProxyHosts));
            return new NetworkProxySettings(httpHost, httpPort, httpsHost, httpsPort, socksHost, socksPort, noProxyHosts);
        }
        return new NetworkProxySettings(resolved);
    }

    private Pointer getKeyCFStringRef(String key) {
        return NETWORK_LIBRARY.getGlobalVariableAddress(key).getPointer(0);
    }

    private String getStringFromCFStringRef(Pointer cfStringPointer) {
        long lenght;
        long maxSize;
        Memory buffer;
        if (cfStringPointer != null && cfLibrary.CFStringGetCString(cfStringPointer, (Pointer)(buffer = new Memory(maxSize = cfLibrary.CFStringGetMaximumSizeForEncoding(lenght = cfLibrary.CFStringGetLength(cfStringPointer), 134217984))), maxSize, 134217984)) {
            return buffer.getString(0);
        }
        return null;
    }

    private int getIntFromCFNumberRef(Pointer cfNumberPointer) {
        long numberSize;
        Pointer cfNumberType;
        Memory numberValue;
        if (cfNumberPointer != null && cfLibrary.CFNumberGetValue(cfNumberPointer, cfNumberType = cfLibrary.CFNumberGetType(cfNumberPointer), (Pointer)(numberValue = new Memory(numberSize = cfLibrary.CFNumberGetByteSize(cfNumberPointer))))) {
            return numberValue.getInt(0);
        }
        return 0;
    }

    private String getStringFromCFNumberRef(Pointer cfNumberPointer) {
        long numberSize;
        Pointer cfNumberType;
        Memory numberValue;
        if (cfNumberPointer != null && cfLibrary.CFNumberGetValue(cfNumberPointer, cfNumberType = cfLibrary.CFNumberGetType(cfNumberPointer), (Pointer)(numberValue = new Memory(numberSize = cfLibrary.CFNumberGetByteSize(cfNumberPointer))))) {
            return String.valueOf(numberValue.getInt(0));
        }
        return null;
    }

    private String[] getNoProxyHosts(Pointer noProxyHostsPointer) {
        if (noProxyHostsPointer != null) {
            long arrayLenght = cfLibrary.CFArrayGetCount(noProxyHostsPointer);
            String[] noProxyHosts = new String[(int)arrayLenght];
            for (long i = 0; i < arrayLenght; ++i) {
                String noProxyHost;
                Pointer value = cfLibrary.CFArrayGetValueAtIndex(noProxyHostsPointer, new Pointer(i));
                noProxyHosts[(int)i] = noProxyHost = this.getStringFromCFStringRef(value);
            }
            return noProxyHosts;
        }
        return new String[0];
    }

    private Pointer getValueIfExists(Pointer settingsDictionary, String key) {
        Pointer[] returnValue = new Pointer[1];
        if (cfLibrary.CFDictionaryGetValueIfPresent(settingsDictionary, this.getKeyCFStringRef(key), returnValue)) {
            return returnValue[0];
        }
        return null;
    }

    private static String getStringFromArray(String[] stringArray) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < stringArray.length; ++i) {
            sb.append(stringArray[i]);
            if (i != stringArray.length - 1) continue;
            sb.append(",");
        }
        return sb.toString();
    }
}

