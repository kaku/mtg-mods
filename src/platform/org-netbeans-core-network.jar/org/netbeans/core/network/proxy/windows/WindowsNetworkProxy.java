/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.jna.Pointer
 */
package org.netbeans.core.network.proxy.windows;

import com.sun.jna.Pointer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.network.proxy.NetworkProxyResolver;
import org.netbeans.core.network.proxy.NetworkProxySettings;
import org.netbeans.core.network.proxy.windows.WindowsNetworkProxyLibrary;

public class WindowsNetworkProxy
implements NetworkProxyResolver {
    private static final Logger LOGGER = Logger.getLogger(WindowsNetworkProxy.class.getName());
    private static final String HTTP_PROPERTY_NAME = "http=";
    private static final String HTTPS_PROPERTY_NAME = "https=";
    private static final String SOCKS_PROPERTY_NAME = "socks=";
    private static final String SPACE = " ";
    private static final String COLON = ":";
    private static final String SEMI_COLON = ";";

    @Override
    public NetworkProxySettings getNetworkProxySettings() {
        LOGGER.log(Level.FINE, "Windows system proxy resolver started.");
        WindowsNetworkProxyLibrary.ProxyConfig.ByReference prxCnf = new WindowsNetworkProxyLibrary.ProxyConfig.ByReference();
        boolean result = WindowsNetworkProxyLibrary.LIBRARY.WinHttpGetIEProxyConfigForCurrentUser(prxCnf);
        if (result) {
            Pointer pacFilePointer;
            LOGGER.log(Level.FINE, "Windows system proxy resolver successfully retrieved proxy settings.");
            if (prxCnf.autoDetect) {
                LOGGER.log(Level.INFO, "Windows system proxy resolver: auto detect");
            }
            if ((pacFilePointer = prxCnf.pacFile) != null) {
                String pacFileUrl = pacFilePointer.getString(0, true);
                LOGGER.log(Level.INFO, "Windows system proxy resolver: auto - PAC ({0})", pacFileUrl);
                return new NetworkProxySettings(pacFileUrl);
            }
            Pointer proxyPointer = prxCnf.proxy;
            Pointer proxyBypassPointer = prxCnf.proxyBypass;
            if (proxyPointer != null) {
                String[] noProxyHosts;
                String proxyString = proxyPointer.getString(0, true);
                LOGGER.log(Level.INFO, "Windows system proxy resolver: manual ({0})", proxyString);
                String httpProxy = null;
                String httpsProxy = null;
                String socksProxy = null;
                if (proxyString != null) {
                    proxyString = proxyString.toLowerCase();
                }
                if (proxyString.contains(";")) {
                    String[] proxies;
                    for (String singleProxy : proxies = proxyString.split(";")) {
                        if (singleProxy.startsWith("http=")) {
                            httpProxy = singleProxy.substring("http=".length());
                            continue;
                        }
                        if (singleProxy.startsWith("https=")) {
                            httpsProxy = singleProxy.substring("https=".length());
                            continue;
                        }
                        if (!singleProxy.startsWith("socks=")) continue;
                        socksProxy = singleProxy.substring("socks=".length());
                    }
                } else if (proxyString.startsWith("http=")) {
                    proxyString = proxyString.substring("http=".length());
                    httpProxy = proxyString.replace(" ", ":");
                } else if (proxyString.startsWith("https=")) {
                    proxyString = proxyString.substring("https=".length());
                    httpsProxy = proxyString.replace(" ", ":");
                } else if (proxyString.startsWith("socks=")) {
                    proxyString = proxyString.substring("socks=".length());
                    socksProxy = proxyString.replace(" ", ":");
                } else {
                    httpProxy = proxyString;
                    httpsProxy = proxyString;
                }
                if (proxyBypassPointer != null) {
                    String proxyBypass = proxyBypassPointer.getString(0, true);
                    LOGGER.log(Level.INFO, "Windows system proxy resolver: manual - no proxy hosts ({0})", proxyBypass);
                    noProxyHosts = proxyBypass.split(";");
                } else {
                    noProxyHosts = new String[]{};
                }
                return new NetworkProxySettings(httpProxy, httpsProxy, socksProxy, noProxyHosts);
            }
            LOGGER.log(Level.FINE, "Windows system proxy resolver: no proxy");
            return new NetworkProxySettings();
        }
        LOGGER.log(Level.SEVERE, "Windows system proxy resolver cannot retrieve proxy settings from Windows API!");
        return new NetworkProxySettings(false);
    }
}

