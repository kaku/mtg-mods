/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.ProxySettings
 *  org.netbeans.core.ProxySettings$Reloader
 *  org.openide.util.NbPreferences
 *  org.openide.util.Utilities
 */
package org.netbeans.core.network.proxy;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import org.netbeans.core.ProxySettings;
import org.netbeans.core.network.proxy.NetworkProxyResolver;
import org.netbeans.core.network.proxy.NetworkProxySettings;
import org.netbeans.core.network.proxy.ProxyAutoConfig;
import org.netbeans.core.network.proxy.fallback.FallbackNetworkProxy;
import org.netbeans.core.network.proxy.gnome.GnomeNetworkProxy;
import org.netbeans.core.network.proxy.kde.KdeNetworkProxy;
import org.netbeans.core.network.proxy.mac.MacNetworkProxy;
import org.netbeans.core.network.proxy.windows.WindowsNetworkProxy;
import org.openide.util.NbPreferences;
import org.openide.util.Utilities;

public class NetworkProxyReloader
extends ProxySettings.Reloader {
    private static final Logger LOGGER = Logger.getLogger(NetworkProxyReloader.class.getName());
    private static final String EMPTY_STRING = "";
    private static final String NON_PROXY_HOSTS_DELIMITER = "|";
    private static final String GNOME = "gnome";
    private static final String KDE = "kde";
    private static final String RUNNING_ENV_SYS_PROPERTY = "netbeans.running.environment";
    private static final NetworkProxyResolver NETWORK_PROXY_RESOLVER = NetworkProxyReloader.getNetworkProxyResolver();
    private static final NetworkProxyResolver FALLBACK_NETWORK_PROXY_RESOLVER = NetworkProxyReloader.getFallbackProxyResolver();

    public static void reloadNetworkProxy() {
        LOGGER.log(Level.FINE, "System network proxy reloading started.");
        NetworkProxySettings networkProxySettings = NETWORK_PROXY_RESOLVER.getNetworkProxySettings();
        if (!networkProxySettings.isResolved()) {
            LOGGER.log(Level.INFO, "System network proxy reloading failed! Trying fallback resolver.");
            NetworkProxySettings fallbackNetworkProxySettings = FALLBACK_NETWORK_PROXY_RESOLVER.getNetworkProxySettings();
            if (fallbackNetworkProxySettings.isResolved()) {
                LOGGER.log(Level.INFO, "System network proxy reloading succeeded. Fallback provider was successful.");
                networkProxySettings = fallbackNetworkProxySettings;
            } else {
                LOGGER.log(Level.INFO, "System network proxy reloading failed! Fallback provider was unsuccessful.");
            }
        } else {
            LOGGER.log(Level.INFO, "System network proxy reloading succeeded.");
        }
        switch (networkProxySettings.getProxyMode()) {
            case AUTO: {
                String testHttpProxyHost;
                String testHttpProxyPort;
                ProxyAutoConfig pacForTest = ProxyAutoConfig.get(networkProxySettings.getPacFileUrl());
                List<Proxy> testHttpProxy = null;
                try {
                    testHttpProxy = pacForTest.findProxyForURL(new URI("http://netbeans.org"));
                }
                catch (URISyntaxException ex) {
                    LOGGER.log(Level.WARNING, "Cannot create URI from: http://netbeans.org", ex);
                }
                if (testHttpProxy != null && !testHttpProxy.isEmpty() && testHttpProxy.get(0).address() != null) {
                    testHttpProxyHost = ((InetSocketAddress)testHttpProxy.get(0).address()).getHostName();
                    testHttpProxyPort = Integer.toString(((InetSocketAddress)testHttpProxy.get(0).address()).getPort());
                } else {
                    testHttpProxyHost = "";
                    testHttpProxyPort = Integer.toString(0);
                }
                LOGGER.log(Level.INFO, "System network proxy - mode: auto");
                LOGGER.log(Level.INFO, "System network proxy - pac url: {0}", networkProxySettings.getPacFileUrl());
                LOGGER.log(Level.INFO, "System network proxy TEST - http host: {0}", testHttpProxyHost);
                LOGGER.log(Level.INFO, "System network proxy TEST - http port: {0}", testHttpProxyPort);
                NetworkProxyReloader.getPreferences().remove("systemProxyHttpHost");
                NetworkProxyReloader.getPreferences().remove("systemProxyHttpPort");
                NetworkProxyReloader.getPreferences().remove("systemProxyHttpsHost");
                NetworkProxyReloader.getPreferences().remove("systemProxyHttpsPort");
                NetworkProxyReloader.getPreferences().remove("systemProxySocksHost");
                NetworkProxyReloader.getPreferences().remove("systemProxySocksPort");
                NetworkProxyReloader.getPreferences().remove("systemProxyNonProxyHosts");
                NetworkProxyReloader.getPreferences().put("testSystemProxyHttpHost", testHttpProxyHost);
                NetworkProxyReloader.getPreferences().put("testSystemProxyHttpPort", testHttpProxyPort);
                NetworkProxyReloader.getPreferences().put("systemPAC", networkProxySettings.getPacFileUrl());
                break;
            }
            case MANUAL: {
                LOGGER.log(Level.INFO, "System network proxy - mode: manual");
                LOGGER.log(Level.INFO, "System network proxy - http host: {0}", networkProxySettings.getHttpProxyHost());
                LOGGER.log(Level.INFO, "System network proxy - http port: {0}", networkProxySettings.getHttpProxyPort());
                LOGGER.log(Level.INFO, "System network proxy - https host: {0}", networkProxySettings.getHttpsProxyHost());
                LOGGER.log(Level.INFO, "System network proxy - https port: {0}", networkProxySettings.getHttpsProxyPort());
                LOGGER.log(Level.INFO, "System network proxy - socks host: {0}", networkProxySettings.getSocksProxyHost());
                LOGGER.log(Level.INFO, "System network proxy - socks port: {0}", networkProxySettings.getSocksProxyPort());
                LOGGER.log(Level.INFO, "System network proxy - no proxy hosts: {0}", NetworkProxyReloader.getStringFromArray(networkProxySettings.getNoProxyHosts()));
                LOGGER.log(Level.INFO, "System network proxy TEST - http host: {0}", networkProxySettings.getHttpProxyHost());
                LOGGER.log(Level.INFO, "System network proxy TEST - http port: {0}", networkProxySettings.getHttpProxyPort());
                NetworkProxyReloader.getPreferences().put("systemProxyHttpHost", networkProxySettings.getHttpProxyHost());
                NetworkProxyReloader.getPreferences().put("systemProxyHttpPort", networkProxySettings.getHttpProxyPort());
                NetworkProxyReloader.getPreferences().put("systemProxyHttpsHost", networkProxySettings.getHttpsProxyHost());
                NetworkProxyReloader.getPreferences().put("systemProxyHttpsPort", networkProxySettings.getHttpsProxyPort());
                NetworkProxyReloader.getPreferences().put("systemProxySocksHost", networkProxySettings.getSocksProxyHost());
                NetworkProxyReloader.getPreferences().put("systemProxySocksPort", networkProxySettings.getSocksProxyPort());
                NetworkProxyReloader.getPreferences().put("systemProxyNonProxyHosts", NetworkProxyReloader.getStringFromArray(networkProxySettings.getNoProxyHosts()));
                NetworkProxyReloader.getPreferences().put("testSystemProxyHttpHost", networkProxySettings.getHttpProxyHost());
                NetworkProxyReloader.getPreferences().put("testSystemProxyHttpPort", networkProxySettings.getHttpProxyPort());
                NetworkProxyReloader.getPreferences().remove("systemPAC");
                break;
            }
            case DIRECT: {
                LOGGER.log(Level.INFO, "System network proxy - mode: direct");
            }
            default: {
                LOGGER.log(Level.INFO, "System network proxy: fell to default (correct if direct mode went before)");
                NetworkProxyReloader.getPreferences().remove("systemProxyHttpHost");
                NetworkProxyReloader.getPreferences().remove("systemProxyHttpPort");
                NetworkProxyReloader.getPreferences().remove("systemProxyHttpsHost");
                NetworkProxyReloader.getPreferences().remove("systemProxyHttpsPort");
                NetworkProxyReloader.getPreferences().remove("systemProxySocksHost");
                NetworkProxyReloader.getPreferences().remove("systemProxySocksPort");
                NetworkProxyReloader.getPreferences().remove("systemProxyNonProxyHosts");
                NetworkProxyReloader.getPreferences().remove("systemPAC");
                NetworkProxyReloader.getPreferences().remove("testSystemProxyHttpHost");
                NetworkProxyReloader.getPreferences().remove("testSystemProxyHttpPort");
            }
        }
        LOGGER.log(Level.FINE, "System network proxy reloading fineshed.");
    }

    private static String getStringFromArray(String[] stringArray) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < stringArray.length; ++i) {
            sb.append(stringArray[i]);
            if (i >= stringArray.length - 1) continue;
            sb.append("|");
        }
        return sb.toString();
    }

    private static Preferences getPreferences() {
        return NbPreferences.forModule(ProxySettings.class);
    }

    private static NetworkProxyResolver getNetworkProxyResolver() {
        if (NETWORK_PROXY_RESOLVER == null) {
            String env;
            if (Utilities.isWindows()) {
                LOGGER.log(Level.INFO, "System network proxy resolver: Windows");
                return new WindowsNetworkProxy();
            }
            if (Utilities.isMac()) {
                LOGGER.log(Level.INFO, "System network proxy resolver: Mac");
                return new MacNetworkProxy();
            }
            if (Utilities.isUnix() && (env = System.getProperty("netbeans.running.environment")) != null) {
                if (env.toLowerCase().equals("gnome")) {
                    LOGGER.log(Level.INFO, "System network proxy resolver: Gnome");
                    return new GnomeNetworkProxy();
                }
                if (env.toLowerCase().equals("kde")) {
                    LOGGER.log(Level.INFO, "System network proxy resolver: KDE");
                    return new KdeNetworkProxy();
                }
            }
            LOGGER.log(Level.INFO, "System network proxy resolver: no suitable found, using fallback.");
            return new FallbackNetworkProxy();
        }
        return NETWORK_PROXY_RESOLVER;
    }

    private static NetworkProxyResolver getFallbackProxyResolver() {
        if (FALLBACK_NETWORK_PROXY_RESOLVER == null) {
            return new FallbackNetworkProxy();
        }
        return FALLBACK_NETWORK_PROXY_RESOLVER;
    }

    public void reload() {
        NetworkProxyReloader.reloadNetworkProxy();
    }

}

