/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.network.proxy.kde;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.network.proxy.NetworkProxyResolver;
import org.netbeans.core.network.proxy.NetworkProxySettings;

public class KdeNetworkProxy
implements NetworkProxyResolver {
    private static final Logger LOGGER = Logger.getLogger(KdeNetworkProxy.class.getName());
    private static final String EMPTY_STRING = "";
    private static final String SPACE = " ";
    private static final String EQUALS = "=";
    private static final String COLON = ":";
    private static final String COMMA = ",";
    private static final String SQ_BRACKET_LEFT = "[";
    private static final String HOME = "HOME";
    private static final String KIOSLAVERC_PROXY_SETTINGS_GROUP = "[Proxy Settings]";
    private static final String KIOSLAVERC_PROXY_TYPE = "ProxyType";
    private static final String KIOSLAVERC_PROXY_CONFIG_SCRIPT = "Proxy Config Script";
    private static final String KIOSLAVERC_HTTP_PROXY = "httpProxy";
    private static final String KIOSLAVERC_HTTPS_PROXY = "httpsProxy";
    private static final String KIOSLAVERC_SOCKS_PROXY = "socksProxy";
    private static final String KIOSLAVERC_NO_PROXY_FOR = "NoProxyFor";
    private static final String KIOSLAVERC_PROXY_TYPE_NONE = "0";
    private static final String KIOSLAVERC_PROXY_TYPE_MANUAL = "1";
    private static final String KIOSLAVERC_PROXY_TYPE_PAC = "2";
    private static final String KIOSLAVERC_PROXY_TYPE_AUTO = "3";
    private static final String KIOSLAVERC_PROXY_TYPE_SYSTEM = "4";
    private static final String KIOSLAVERC_PATH_IN_HOME = ".kde/share/config/kioslaverc";
    private final String KIOSLAVERC_PATH;

    public KdeNetworkProxy() {
        this.KIOSLAVERC_PATH = this.getKioslavercPath();
    }

    @Override
    public NetworkProxySettings getNetworkProxySettings() {
        LOGGER.log(Level.FINE, "KDE system proxy resolver started.");
        Map<String, String> kioslavercMap = this.getKioslavercMap();
        String proxyType = kioslavercMap.get("ProxyType");
        if (proxyType == null) {
            LOGGER.log(Level.WARNING, "KDE system proxy resolver: The kioslaverc key not found ({0})", "ProxyType");
            return new NetworkProxySettings(false);
        }
        if (proxyType.equals("0") || proxyType.equals("3")) {
            LOGGER.log(Level.INFO, "KDE system proxy resolver: direct (proxy type: {0})", proxyType);
            return new NetworkProxySettings();
        }
        if (proxyType.equals("2")) {
            LOGGER.log(Level.INFO, "KDE system proxy resolver: auto - PAC");
            String pacFileUrl = kioslavercMap.get("Proxy Config Script");
            if (pacFileUrl != null) {
                LOGGER.log(Level.INFO, "KDE system proxy resolver: PAC URL ({0})", pacFileUrl);
                return new NetworkProxySettings(pacFileUrl);
            }
            LOGGER.log(Level.INFO, "KDE system proxy resolver: PAC URL null value");
            return new NetworkProxySettings(false);
        }
        if (proxyType.equals("1") || proxyType.equals("4")) {
            LOGGER.log(Level.INFO, "KDE system proxy resolver: manual (proxy type: {0})", proxyType);
            String httpProxy = kioslavercMap.get("httpProxy");
            String httpsProxy = kioslavercMap.get("httpsProxy");
            String socksProxy = kioslavercMap.get("socksProxy");
            String noProxyFor = kioslavercMap.get("NoProxyFor");
            LOGGER.log(Level.INFO, "KDE system proxy resolver: http proxy ({0})", httpProxy);
            LOGGER.log(Level.INFO, "KDE system proxy resolver: https proxy ({0})", httpsProxy);
            LOGGER.log(Level.INFO, "KDE system proxy resolver: socks proxy ({0})", socksProxy);
            LOGGER.log(Level.INFO, "KDE system proxy resolver: no proxy ({0})", noProxyFor);
            if (proxyType.equals("1")) {
                httpProxy = httpProxy == null ? "" : httpProxy.trim().replaceAll(" ", ":");
                httpsProxy = httpsProxy == null ? "" : httpsProxy.trim().replaceAll(" ", ":");
                socksProxy = socksProxy == null ? "" : socksProxy.trim().replaceAll(" ", ":");
            }
            String[] noProxyHosts = KdeNetworkProxy.getNoProxyHosts(noProxyFor);
            return new NetworkProxySettings(httpProxy, httpsProxy, socksProxy, noProxyHosts);
        }
        return new NetworkProxySettings(false);
    }

    private Map<String, String> getKioslavercMap() {
        File kioslavercFile = new File(this.KIOSLAVERC_PATH);
        HashMap<String, String> map = new HashMap<String, String>();
        if (kioslavercFile.exists()) {
            try {
                String line;
                FileInputStream fis = new FileInputStream(kioslavercFile);
                DataInputStream dis = new DataInputStream(fis);
                BufferedReader br = new BufferedReader(new InputStreamReader(dis));
                boolean inGroup = false;
                while ((line = br.readLine()) != null) {
                    if (inGroup) {
                        if (line.contains("=")) {
                            int indexOfEquals = line.indexOf("=");
                            String key = line.substring(0, indexOfEquals);
                            String value = line.substring(indexOfEquals + 1);
                            map.put(key, value);
                            continue;
                        }
                        if (!line.startsWith("[")) continue;
                        break;
                    }
                    if (!line.startsWith("[Proxy Settings]")) continue;
                    inGroup = true;
                }
                dis.close();
            }
            catch (FileNotFoundException fnfe) {
                LOGGER.log(Level.SEVERE, "Cannot read file: ", fnfe);
            }
            catch (IOException ioe) {
                LOGGER.log(Level.SEVERE, "Cannot read file: ", ioe);
            }
        } else {
            LOGGER.log(Level.WARNING, "KDE system proxy resolver: The kioslaverc file not found ({0})", this.KIOSLAVERC_PATH);
        }
        return map;
    }

    private String getKioslavercPath() {
        String homePath = System.getenv("HOME");
        if (homePath != null) {
            return homePath + File.separator + ".kde/share/config/kioslaverc";
        }
        return "";
    }

    private static String[] getNoProxyHosts(String noProxyHostsString) {
        if (noProxyHostsString != null && !noProxyHostsString.isEmpty()) {
            return noProxyHostsString.split(",");
        }
        return new String[0];
    }
}

