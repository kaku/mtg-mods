/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.ProxySettings
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 */
package org.netbeans.core.network.proxy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import org.netbeans.core.ProxySettings;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;

public class ProxyAutoConfig {
    private static final Map<String, ProxyAutoConfig> file2pac = new HashMap<String, ProxyAutoConfig>(2);
    private static final RequestProcessor RP = new RequestProcessor(ProxyAutoConfig.class);
    private static final String NS_PROXY_AUTO_CONFIG_URL = "nbinst://org.netbeans.core/modules/ext/nsProxyAutoConfig.js";
    private static final String PROTO_FILE = "file://";
    private static final Logger LOGGER = Logger.getLogger(ProxyAutoConfig.class.getName());
    private Invocable inv = null;
    private final RequestProcessor.Task initTask;
    private final URI pacURI;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static synchronized ProxyAutoConfig get(String pacFile) {
        if (file2pac.get(pacFile) == null) {
            LOGGER.fine("Init ProxyAutoConfig for " + pacFile);
            ProxyAutoConfig instance = null;
            try {
                instance = new ProxyAutoConfig(pacFile);
            }
            catch (URISyntaxException ex) {
                Logger.getLogger(ProxyAutoConfig.class.getName()).warning("Parsing " + pacFile + " to URI throws " + ex);
            }
            finally {
                file2pac.put(pacFile, instance);
            }
        }
        return file2pac.get(pacFile);
    }

    private ProxyAutoConfig(String pacURL) throws URISyntaxException {
        assert (file2pac.get(pacURL) == null);
        String normPAC = this.normalizePAC(pacURL);
        this.pacURI = new URI(normPAC);
        this.initTask = RP.post(new Runnable(){

            @Override
            public void run() {
                ProxyAutoConfig.this.initEngine();
            }
        });
    }

    public URI getPacURI() {
        return this.pacURI;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void initEngine() {
        ScriptEngine eng;
        InputStream pacIS;
        try {
            pacIS = this.pacURI.isAbsolute() ? ProxyAutoConfig.downloadPAC(this.pacURI.toURL()) : null;
        }
        catch (IOException ex) {
            LOGGER.log(Level.INFO, "InputStream for " + this.pacURI + " throws " + ex, ex);
            return;
        }
        if (pacIS == null) {
            return;
        }
        String utils = ProxyAutoConfig.downloadUtils();
        try {
            eng = ProxyAutoConfig.evalPAC(pacIS, utils);
        }
        catch (FileNotFoundException ex) {
            LOGGER.log(Level.FINE, "While constructing ProxyAutoConfig thrown " + ex, ex);
            return;
        }
        catch (ScriptException ex) {
            LOGGER.log(Level.FINE, "While constructing ProxyAutoConfig thrown " + ex, ex);
            return;
        }
        finally {
            if (pacIS != null) {
                try {
                    pacIS.close();
                }
                catch (IOException ex) {
                    LOGGER.log(Level.FINE, "While closing PAC input stream thrown " + ex, ex);
                }
            }
        }
        assert (eng != null);
        if (eng == null) {
            LOGGER.log(Level.WARNING, "JavaScript engine cannot be null");
            return;
        }
        this.inv = (Invocable)((Object)eng);
    }

    public List<Proxy> findProxyForURL(URI u) {
        assert (this.initTask != null);
        if (!this.initTask.isFinished()) {
            while (!this.initTask.isFinished()) {
                try {
                    RP.post(new Runnable(){

                        @Override
                        public void run() {
                        }
                    }).waitFinished(100);
                }
                catch (InterruptedException ex) {
                    LOGGER.log(Level.FINEST, ex.getMessage(), ex);
                }
            }
        }
        if (this.inv == null) {
            return Collections.singletonList(Proxy.NO_PROXY);
        }
        Object proxies = null;
        try {
            proxies = this.inv.invokeFunction("FindProxyForURL", u.toString(), u.getHost());
        }
        catch (ScriptException ex) {
            LOGGER.log(Level.FINE, "While invoking FindProxyForURL(" + u + ", " + u.getHost() + " thrown " + ex, ex);
        }
        catch (NoSuchMethodException ex) {
            LOGGER.log(Level.FINE, "While invoking FindProxyForURL(" + u + ", " + u.getHost() + " thrown " + ex, ex);
        }
        List res = this.analyzeResult(u, proxies);
        if (res == null) {
            LOGGER.info("findProxyForURL(" + u + ") returns null.");
            res = Collections.emptyList();
        }
        LOGGER.fine("findProxyForURL(" + u + ") returns " + Arrays.asList(res));
        return res;
    }

    private static InputStream downloadPAC(URL pacURL) throws IOException {
        URLConnection conn = pacURL.openConnection(Proxy.NO_PROXY);
        InputStream is = conn.getInputStream();
        return is;
    }

    private static ScriptEngine evalPAC(InputStream is, String utils) throws FileNotFoundException, ScriptException {
        ScriptEngineManager factory = new ScriptEngineManager();
        ScriptEngine engine = factory.getEngineByName("JavaScript");
        InputStreamReader pacReader = new InputStreamReader(is);
        engine.eval(pacReader);
        engine.eval(utils);
        return engine;
    }

    private List<Proxy> analyzeResult(URI uri, Object proxiesString) {
        if (proxiesString == null) {
            LOGGER.fine("Null result for " + uri);
            return null;
        }
        String protocol = uri.getScheme();
        assert (protocol != null);
        if (protocol == null) {
            return null;
        }
        Proxy.Type proxyType = "http".equals(protocol) || "https".equals(protocol) ? Proxy.Type.HTTP : Proxy.Type.SOCKS;
        StringTokenizer st = new StringTokenizer(proxiesString.toString(), ";");
        LinkedList<Proxy> proxies = new LinkedList<Proxy>();
        while (st.hasMoreTokens()) {
            String proxy = st.nextToken();
            if ("DIRECT".equals(proxy.trim())) {
                proxies.add(Proxy.NO_PROXY);
                continue;
            }
            String host = ProxyAutoConfig.getHost(proxy);
            Integer port = ProxyAutoConfig.getPort(proxy);
            if (host == null || port == null) continue;
            proxies.add(new Proxy(proxyType, new InetSocketAddress(host, (int)port)));
        }
        return proxies;
    }

    private static String getHost(String proxy) {
        int i;
        if (proxy.startsWith("PROXY ")) {
            proxy = proxy.substring(6);
        }
        if ((i = proxy.lastIndexOf(":")) <= 0 || i >= proxy.length() - 1) {
            LOGGER.info("No port in " + proxy);
            return null;
        }
        String host = proxy.substring(0, i);
        return ProxySettings.normalizeProxyHost((String)host);
    }

    private static Integer getPort(String proxy) {
        int i;
        if (proxy.startsWith("PROXY ")) {
            proxy = proxy.substring(6);
        }
        if ((i = proxy.lastIndexOf(":")) <= 0 || i >= proxy.length() - 1) {
            LOGGER.info("No port in " + proxy);
            return null;
        }
        String port = proxy.substring(i + 1);
        if (port.indexOf(47) >= 0) {
            port = port.substring(0, port.indexOf(47));
        }
        try {
            return Integer.parseInt(port);
        }
        catch (NumberFormatException ex) {
            LOGGER.log(Level.INFO, ex.getLocalizedMessage(), ex);
            return null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static String downloadUtils() {
        StringBuilder builder;
        builder = new StringBuilder();
        BufferedReader reader = null;
        FileObject fo = null;
        try {
            try {
                fo = URLMapper.findFileObject((URL)new URL("nbinst://org.netbeans.core/modules/ext/nsProxyAutoConfig.js"));
            }
            catch (MalformedURLException ex) {
                LOGGER.log(Level.INFO, ex.getMessage(), ex);
                return "";
            }
            reader = new BufferedReader(new FileReader(FileUtil.toFile((FileObject)fo)));
        }
        catch (FileNotFoundException ex) {
            LOGGER.log(Level.INFO, ex.getMessage(), ex);
        }
        try {
            String line;
            boolean doAppend = false;
            while ((line = reader.readLine()) != null) {
                if ((line = line.trim()).startsWith("var pacUtils =")) {
                    doAppend = true;
                    continue;
                }
                if (!doAppend) continue;
                if (line.endsWith("+")) {
                    line = line.substring(0, line.length() - 1);
                }
                builder.append(line.replaceAll("\"", "").replaceAll("\\\\n", "").replaceAll("\\\\\\\\", "\\\\"));
                builder.append(System.getProperty("line.separator"));
            }
        }
        catch (IOException ex) {
            LOGGER.log(Level.INFO, "While downloading nsProxyAutoConfig.js thrown " + ex.getMessage(), ex);
        }
        finally {
            if (reader != null) {
                try {
                    reader.close();
                }
                catch (IOException ex) {
                    LOGGER.log(Level.FINE, ex.getMessage(), ex);
                }
            }
        }
        return builder.toString();
    }

    private String normalizePAC(String pacURL) {
        String fileLocation;
        File f;
        int index = pacURL.indexOf("\n");
        if (index != -1) {
            pacURL = pacURL.substring(0, index);
        }
        if ((index = pacURL.indexOf("\r")) != -1) {
            pacURL = pacURL.substring(0, index);
        }
        if ((fileLocation = pacURL).startsWith("file://")) {
            fileLocation = fileLocation.substring("file://".length());
        }
        pacURL = (f = new File(fileLocation)).canRead() ? Utilities.toURI((File)f).toString() : pacURL.replaceAll("\\\\", "/");
        index = pacURL.indexOf(" ");
        if (index != -1) {
            pacURL = pacURL.substring(0, index);
        }
        return pacURL.trim();
    }

}

