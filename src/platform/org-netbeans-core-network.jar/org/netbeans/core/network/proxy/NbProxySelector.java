/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.ProxySettings
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.core.network.proxy;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.netbeans.core.ProxySettings;
import org.netbeans.core.network.proxy.NetworkProxyReloader;
import org.netbeans.core.network.proxy.ProxyAutoConfig;
import org.openide.util.RequestProcessor;

public final class NbProxySelector
extends ProxySelector {
    private final ProxySelector original = ProxySelector.getDefault();
    private static final Logger LOG = Logger.getLogger(NbProxySelector.class.getName());
    private static Object useSystemProxies;
    private static final String DEFAULT_PROXY_SELECTOR_CLASS_NAME = "sun.net.spi.DefaultProxySelector";
    private static final RequestProcessor RP;
    private static final int DNS_TIMEOUT = 10000;

    public NbProxySelector() {
        LOG.log(Level.FINE, "java.net.useSystemProxies has been set to {0}", NbProxySelector.useSystemProxies());
        if (this.original.getClass().getName().equals("sun.net.spi.DefaultProxySelector") || this.original == null) {
            NetworkProxyReloader.reloadNetworkProxy();
        }
        ProxySettings.addPreferenceChangeListener((PreferenceChangeListener)new ProxySettingsListener());
        this.copySettingsToSystem();
    }

    @Override
    public List<Proxy> select(URI uri) {
        List res = new ArrayList();
        int proxyType = ProxySettings.getProxyType();
        switch (proxyType) {
            case 0: {
                res = Collections.singletonList(Proxy.NO_PROXY);
                break;
            }
            case 1: {
                if (NbProxySelector.useSystemProxies()) {
                    if (this.original == null) break;
                    res = this.original.select(uri);
                    break;
                }
                String protocol = uri.getScheme();
                assert (protocol != null);
                if (NbProxySelector.dontUseProxy(ProxySettings.getSystemNonProxyHosts(), uri.getHost())) {
                    res.add(Proxy.NO_PROXY);
                    break;
                }
                if (protocol.toLowerCase(Locale.US).startsWith("http")) {
                    String ports = ProxySettings.getSystemHttpPort();
                    if (ports != null && ports.length() > 0 && ProxySettings.getSystemHttpHost().length() > 0) {
                        int porti = Integer.parseInt(ports);
                        Proxy p = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(ProxySettings.getSystemHttpHost(), porti));
                        res.add(p);
                    }
                } else {
                    String ports = ProxySettings.getSystemSocksPort();
                    String hosts = ProxySettings.getSystemSocksHost();
                    if (ports != null && ports.length() > 0 && hosts.length() > 0) {
                        int porti = Integer.parseInt(ports);
                        Proxy p = new Proxy(Proxy.Type.SOCKS, new InetSocketAddress(hosts, porti));
                        res.add(p);
                    }
                }
                if (this.original == null) break;
                res.addAll(this.original.select(uri));
                break;
            }
            case 2: {
                String protocol = uri.getScheme();
                assert (protocol != null);
                if (NbProxySelector.dontUseProxy(ProxySettings.getNonProxyHosts(), uri.getHost())) {
                    res.add(Proxy.NO_PROXY);
                    break;
                }
                if (protocol.toLowerCase(Locale.US).startsWith("http")) {
                    String hosts = ProxySettings.getHttpHost();
                    String ports = ProxySettings.getHttpPort();
                    if (ports != null && ports.length() > 0 && hosts.length() > 0) {
                        int porti = Integer.parseInt(ports);
                        Proxy p = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(hosts, porti));
                        res.add(p);
                    } else {
                        LOG.log(Level.FINE, "Incomplete HTTP Proxy [{0}/{1}] found in ProxySelector[Type: {2}] for uri {3}. ", new Object[]{hosts, ports, ProxySettings.getProxyType(), uri});
                        if (this.original != null) {
                            LOG.log(Level.FINEST, "Fallback to the default ProxySelector which returns {0}", this.original.select(uri));
                            res.addAll(this.original.select(uri));
                        }
                    }
                } else {
                    String ports = ProxySettings.getSocksPort();
                    String hosts = ProxySettings.getSocksHost();
                    if (ports != null && ports.length() > 0 && hosts.length() > 0) {
                        int porti = Integer.parseInt(ports);
                        Proxy p = new Proxy(Proxy.Type.SOCKS, new InetSocketAddress(hosts, porti));
                        res.add(p);
                    } else {
                        LOG.log(Level.FINE, "Incomplete SOCKS Server [{0}/{1}] found in ProxySelector[Type: {2}] for uri {3}. ", new Object[]{hosts, ports, ProxySettings.getProxyType(), uri});
                        if (this.original != null) {
                            LOG.log(Level.FINEST, "Fallback to the default ProxySelector which returns {0}", this.original.select(uri));
                            res.addAll(this.original.select(uri));
                        }
                    }
                }
                res.add(Proxy.NO_PROXY);
                break;
            }
            case 3: {
                if (NbProxySelector.useSystemProxies()) {
                    if (this.original != null) {
                        res = this.original.select(uri);
                    }
                } else {
                    ProxyAutoConfig pac = ProxyAutoConfig.get(NbProxySelector.getPacFile());
                    assert (pac != null);
                    if (pac == null) {
                        LOG.log(Level.FINEST, "No instance of ProxyAutoConfig({0}) for URI {1}", new Object[]{NbProxySelector.getPacFile(), uri});
                        res.add(Proxy.NO_PROXY);
                    }
                    if (pac.getPacURI().getHost() == null) {
                        LOG.log(Level.FINEST, "Identifying proxy for URI {0}---{1}, PAC LOCAL URI: {2}", new Object[]{uri.toString(), uri.getHost(), pac.getPacURI().toString()});
                        res.addAll(pac.findProxyForURL(uri));
                    } else if (pac.getPacURI().getHost().equals(uri.getHost())) {
                        res.add(Proxy.NO_PROXY);
                    } else {
                        LOG.log(Level.FINEST, "Identifying proxy for URI {0}---{1}, PAC URI: {2}---{3}", new Object[]{uri.toString(), uri.getHost(), pac.getPacURI().toString(), pac.getPacURI().getHost()});
                        res.addAll(pac.findProxyForURL(uri));
                    }
                }
                if (this.original != null) {
                    res.addAll(this.original.select(uri));
                }
                res.add(Proxy.NO_PROXY);
                break;
            }
            case 4: {
                ProxyAutoConfig pac = ProxyAutoConfig.get(NbProxySelector.getPacFile());
                assert (pac != null);
                if (pac == null) {
                    LOG.log(Level.FINEST, "No instance of ProxyAutoConfig({0}) for URI {1}", new Object[]{NbProxySelector.getPacFile(), uri});
                    res.add(Proxy.NO_PROXY);
                }
                if (pac.getPacURI().getHost() == null) {
                    LOG.log(Level.FINEST, "Identifying proxy for URI {0}---{1}, PAC LOCAL URI: {2}", new Object[]{uri.toString(), uri.getHost(), pac.getPacURI().toString()});
                    res.addAll(pac.findProxyForURL(uri));
                } else if (pac.getPacURI().getHost().equals(uri.getHost())) {
                    res.add(Proxy.NO_PROXY);
                } else {
                    LOG.log(Level.FINEST, "Identifying proxy for URI {0}---{1}, PAC URI: {2}---{3}", new Object[]{uri.toString(), uri.getHost(), pac.getPacURI().toString(), pac.getPacURI().getHost()});
                    res.addAll(pac.findProxyForURL(uri));
                }
                res.add(Proxy.NO_PROXY);
                break;
            }
            default: {
                assert (false);
                break;
            }
        }
        LOG.log(Level.FINEST, "NbProxySelector[Type: {0}, Use HTTP for all protocols: {1}] returns {2} for URI {3}", new Object[]{ProxySettings.getProxyType(), ProxySettings.useProxyAllProtocols(), res, uri});
        return res;
    }

    @Override
    public void connectFailed(URI arg0, SocketAddress arg1, IOException arg2) {
        LOG.log(Level.INFO, "connectionFailed(" + arg0 + ", " + arg1 + ")", arg2);
    }

    private void copySettingsToSystem() {
        String host = null;
        String port = null;
        String nonProxyHosts = null;
        String socksHost = null;
        String socksPort = null;
        String httpsHost = null;
        String httpsPort = null;
        int proxyType = ProxySettings.getProxyType();
        switch (proxyType) {
            case 0: {
                host = null;
                port = null;
                httpsHost = null;
                httpsPort = null;
                nonProxyHosts = null;
                socksHost = null;
                socksPort = null;
                break;
            }
            case 1: {
                host = ProxySettings.getSystemHttpHost();
                port = ProxySettings.getSystemHttpPort();
                httpsHost = ProxySettings.getSystemHttpsHost();
                httpsPort = ProxySettings.getSystemHttpsPort();
                socksHost = ProxySettings.getSystemSocksHost();
                socksPort = ProxySettings.getSystemSocksPort();
                nonProxyHosts = ProxySettings.getSystemNonProxyHosts();
                break;
            }
            case 2: {
                host = ProxySettings.getHttpHost();
                port = ProxySettings.getHttpPort();
                httpsHost = ProxySettings.getHttpsHost();
                httpsPort = ProxySettings.getHttpsPort();
                nonProxyHosts = ProxySettings.getNonProxyHosts();
                socksHost = ProxySettings.getSocksHost();
                socksPort = ProxySettings.getSocksPort();
                break;
            }
            case 3: {
                host = null;
                port = null;
                httpsHost = null;
                httpsPort = null;
                nonProxyHosts = null;
                socksHost = null;
                socksPort = null;
                break;
            }
            case 4: {
                host = null;
                port = null;
                httpsHost = null;
                httpsPort = null;
                nonProxyHosts = ProxySettings.getNonProxyHosts();
                socksHost = null;
                socksPort = null;
                break;
            }
            default: {
                assert (false);
                break;
            }
        }
        this.setOrClearProperty("http.proxyHost", host, false);
        this.setOrClearProperty("http.proxyPort", port, true);
        this.setOrClearProperty("http.nonProxyHosts", nonProxyHosts, false);
        this.setOrClearProperty("https.proxyHost", httpsHost, false);
        this.setOrClearProperty("https.proxyPort", httpsPort, true);
        this.setOrClearProperty("https.nonProxyHosts", nonProxyHosts, false);
        this.setOrClearProperty("socksProxyHost", socksHost, false);
        this.setOrClearProperty("socksProxyPort", socksPort, true);
        LOG.log(Level.FINE, "Set System''s http.proxyHost/Port/NonProxyHost to {0}/{1}/{2}", new Object[]{host, port, nonProxyHosts});
        LOG.log(Level.FINE, "Set System''s https.proxyHost/Port to {0}/{1}", new Object[]{httpsHost, httpsPort});
        LOG.log(Level.FINE, "Set System''s socksProxyHost/Port to {0}/{1}", new Object[]{socksHost, socksPort});
    }

    private void setOrClearProperty(String key, String value, boolean isInteger) {
        assert (key != null);
        if (value == null || value.length() == 0) {
            System.clearProperty(key);
        } else {
            if (isInteger) {
                try {
                    Integer.parseInt(value);
                }
                catch (NumberFormatException nfe) {
                    LOG.log(Level.INFO, nfe.getMessage(), nfe);
                }
            }
            System.setProperty(key, value);
        }
    }

    static boolean dontUseProxy(String nonProxyHosts, String host) {
        if (host == null) {
            return false;
        }
        if (NbProxySelector.dontUseIp(nonProxyHosts, host)) {
            return true;
        }
        return NbProxySelector.dontUseHostName(nonProxyHosts, host);
    }

    private static boolean dontUseHostName(String nonProxyHosts, String host) {
        if (host == null) {
            return false;
        }
        boolean dontUseProxy = false;
        StringTokenizer st = new StringTokenizer(nonProxyHosts, "|", false);
        while (st.hasMoreTokens() && !dontUseProxy) {
            String token = st.nextToken().trim();
            int star = token.indexOf("*");
            if (star == -1) {
                dontUseProxy = token.equals(host);
                if (!dontUseProxy) continue;
                LOG.log(Level.FINEST, "NbProxySelector[Type: {0}]. Host {1} found in nonProxyHosts: {2}", new Object[]{ProxySettings.getProxyType(), host, nonProxyHosts});
                continue;
            }
            String start = token.substring(0, star - 1 < 0 ? 0 : star - 1);
            String end = token.substring(star + 1 > token.length() ? token.length() : star + 1);
            boolean compareStart = star > 0;
            boolean compareEnd = star < token.length() - 1;
            dontUseProxy = compareStart && host.startsWith(start) || compareEnd && host.endsWith(end);
            if (!dontUseProxy) continue;
            LOG.log(Level.FINEST, "NbProxySelector[Type: {0}]. Host {1} found in nonProxyHosts: {2}", new Object[]{ProxySettings.getProxyType(), host, nonProxyHosts});
        }
        return dontUseProxy;
    }

    private static boolean dontUseIp(String nonProxyHosts, String host) {
        if (host == null) {
            return false;
        }
        DnsTimeoutTask dns = new DnsTimeoutTask(host);
        RequestProcessor.Task create = RP.post((Runnable)dns);
        try {
            create.waitFinished(10000);
        }
        catch (InterruptedException ex) {
            LOG.log(Level.INFO, "Timeout when waiting for DNS response. ({0})", host);
        }
        String ip = dns.getIp();
        if (ip == null) {
            return false;
        }
        boolean dontUseProxy = false;
        StringTokenizer st = new StringTokenizer(nonProxyHosts, "|", false);
        while (st.hasMoreTokens() && !dontUseProxy) {
            String nonProxyHost = st.nextToken().trim();
            int star = nonProxyHost.indexOf("*");
            if (star == -1) {
                dontUseProxy = nonProxyHost.equals(ip);
                if (!dontUseProxy) continue;
                LOG.log(Level.FINEST, "NbProxySelector[Type: {0}]. Host''s IP {1} found in nonProxyHosts: {2}", new Object[]{ProxySettings.getProxyType(), ip, nonProxyHosts});
                continue;
            }
            try {
                dontUseProxy = Pattern.matches(nonProxyHost, ip);
                if (!dontUseProxy) continue;
                LOG.log(Level.FINEST, "NbProxySelector[Type: {0}]. Host''s IP{1} found in nonProxyHosts: {2}", new Object[]{ProxySettings.getProxyType(), ip, nonProxyHosts});
            }
            catch (PatternSyntaxException pse) {}
        }
        return dontUseProxy;
    }

    static boolean useSystemProxies() {
        if (useSystemProxies == null) {
            try {
                Class clazz = Class.forName("sun.net.NetProperties");
                Method getBoolean = clazz.getMethod("getBoolean", String.class);
                useSystemProxies = getBoolean.invoke(null, "java.net.useSystemProxies");
            }
            catch (Exception x) {
                LOG.log(Level.FINEST, "Cannot get value of java.net.useSystemProxies bacause " + x.getMessage(), x);
            }
        }
        return useSystemProxies != null && "true".equalsIgnoreCase(useSystemProxies.toString());
    }

    static boolean usePAC() {
        String pacFile = ProxySettings.getSystemPac();
        return pacFile != null;
    }

    private static String getPacFile() {
        return ProxySettings.getSystemPac();
    }

    static {
        RP = new RequestProcessor(NbProxySelector.class.getName(), 5);
    }

    private static class DnsTimeoutTask
    implements Runnable {
        private final String host;
        private String ip = null;

        public DnsTimeoutTask(String host) {
            this.host = host;
        }

        @Override
        public void run() {
            try {
                this.ip = InetAddress.getByName(this.host).getHostAddress();
            }
            catch (UnknownHostException ex) {
                LOG.log(Level.FINE, ex.getLocalizedMessage(), ex);
            }
        }

        public String getIp() {
            return this.ip;
        }
    }

    private class ProxySettingsListener
    implements PreferenceChangeListener {
        private ProxySettingsListener() {
        }

        @Override
        public void preferenceChange(PreferenceChangeEvent evt) {
            if (evt.getKey().startsWith("proxy") || evt.getKey().startsWith("useProxy")) {
                NbProxySelector.this.copySettingsToSystem();
            }
        }
    }

}

