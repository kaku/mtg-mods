/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.network.proxy.gnome;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.network.proxy.NetworkProxySettings;
import org.netbeans.core.network.proxy.gnome.GnomeNetworkProxy;

public class GsettingsNetworkProxy {
    private static final Logger LOGGER = Logger.getLogger(GsettingsNetworkProxy.class.getName());
    private static final String EMPTY_STRING = "";
    private static final String SPACE = " ";
    private static final String DOT = ".";
    private static final String COLON = ",";
    private static final String SINGLE_QUOTE = "'";
    private static final String SQ_BRACKET_LEFT = "[";
    private static final String SQ_BRACKET_RIGHT = "]";
    protected static final String GSETTINGS_PATH = "/usr/bin/gsettings";
    private static final String GSETTINGS_ARGUMENT_LIST_RECURSIVELY = " list-recursively ";
    private static final String GSETTINGS_PROXY_SCHEMA = "org.gnome.system.proxy";
    private static final String GSETTINGS_KEY_MODE = "org.gnome.system.proxy.mode";
    private static final String GSETTINGS_KEY_PAC_URL = "org.gnome.system.proxy.autoconfig-url";
    private static final String GSETTINGS_KEY_HTTP_ALL = "org.gnome.system.proxy.http.use-same-proxy";
    private static final String GSETTINGS_KEY_HTTP_HOST = "org.gnome.system.proxy.http.host";
    private static final String GSETTINGS_KEY_HTTP_PORT = "org.gnome.system.proxy.http.port";
    private static final String GSETTINGS_KEY_HTTPS_HOST = "org.gnome.system.proxy.https.host";
    private static final String GSETTINGS_KEY_HTTPS_PORT = "org.gnome.system.proxy.https.port";
    private static final String GSETTINGS_KEY_SOCKS_HOST = "org.gnome.system.proxy.socks.host";
    private static final String GSETTINGS_KEY_SOCKS_PORT = "org.gnome.system.proxy.socks.port";
    private static final String GSETTINGS_KEY_IGNORE_HOSTS = "org.gnome.system.proxy.ignore-hosts";
    private static final String GSETTINGS_VALUE_NONE = "none";
    private static final String GSETTINGS_VALUE_AUTO = "auto";
    private static final String GSETTINGS_VALUE_MANUAL = "manual";

    protected static NetworkProxySettings getNetworkProxySettings() {
        LOGGER.log(Level.FINE, "GSettings system proxy resolver started.");
        Map<String, String> proxyProperties = GsettingsNetworkProxy.getGsettingsMap();
        String proxyMode = proxyProperties.get("org.gnome.system.proxy.mode");
        if (proxyMode == null) {
            LOGGER.log(Level.SEVERE, "GSettings proxy mode is null.");
            return new NetworkProxySettings(false);
        }
        if (proxyMode.equals("none")) {
            LOGGER.log(Level.INFO, "GSettings system proxy resolver: direct connection");
            return new NetworkProxySettings();
        }
        if (proxyMode.equals("auto")) {
            String pacUrl = proxyProperties.get("org.gnome.system.proxy.autoconfig-url");
            LOGGER.log(Level.INFO, "GSettings system proxy resolver: auto - PAC ({0})", pacUrl);
            if (pacUrl != null) {
                return new NetworkProxySettings(pacUrl);
            }
            return new NetworkProxySettings("");
        }
        if (proxyMode.equals("manual")) {
            String httpProxyAll = proxyProperties.get("org.gnome.system.proxy.http.use-same-proxy");
            String httpProxyHost = proxyProperties.get("org.gnome.system.proxy.http.host");
            String httpProxyPort = proxyProperties.get("org.gnome.system.proxy.http.port");
            String noProxyHosts = proxyProperties.get("org.gnome.system.proxy.ignore-hosts");
            LOGGER.log(Level.INFO, "GSettings system proxy resolver: manual - http for all ({0})", httpProxyAll);
            LOGGER.log(Level.INFO, "GSettings system proxy resolver: manual - http host ({0})", httpProxyHost);
            LOGGER.log(Level.INFO, "GSettings system proxy resolver: manual - http port ({0})", httpProxyPort);
            LOGGER.log(Level.INFO, "GSettings system proxy resolver: manual - ho proxy hosts ({0})", noProxyHosts);
            if (httpProxyAll != null && Boolean.parseBoolean(httpProxyAll)) {
                return new NetworkProxySettings(httpProxyHost, httpProxyPort, GsettingsNetworkProxy.getNoProxyHosts(noProxyHosts));
            }
            String httpsProxyHost = proxyProperties.get("org.gnome.system.proxy.https.host");
            String httpsProxyPort = proxyProperties.get("org.gnome.system.proxy.https.port");
            String socksProxyHost = proxyProperties.get("org.gnome.system.proxy.socks.host");
            String socksProxyPort = proxyProperties.get("org.gnome.system.proxy.socks.port");
            LOGGER.log(Level.INFO, "GSettings system proxy resolver: manual - https host ({0})", httpsProxyHost);
            LOGGER.log(Level.INFO, "GSettings system proxy resolver: manual - https port ({0})", httpsProxyPort);
            LOGGER.log(Level.INFO, "GSettings system proxy resolver: manual - socks host ({0})", socksProxyHost);
            LOGGER.log(Level.INFO, "GSettings system proxy resolver: manual - socks port ({0})", socksProxyPort);
            return new NetworkProxySettings(httpProxyHost, httpProxyPort, httpsProxyHost, httpsProxyPort, socksProxyHost, socksProxyPort, GsettingsNetworkProxy.getNoProxyHosts(noProxyHosts));
        }
        return new NetworkProxySettings(false);
    }

    protected static boolean isGsettingsValid() {
        String command = "/usr/bin/gsettings list-recursively org.gnome.system.proxy";
        try {
            BufferedReader reader = GnomeNetworkProxy.executeCommand(command);
            if (reader.ready()) {
                return true;
            }
        }
        catch (IOException ioe) {
            LOGGER.log(Level.SEVERE, "Cannot read line: " + command, ioe);
        }
        LOGGER.log(Level.INFO, "GSettings return empty list");
        return false;
    }

    private static Map<String, String> getGsettingsMap() {
        HashMap<String, String> map = new HashMap<String, String>();
        String command = "/usr/bin/gsettings list-recursively org.gnome.system.proxy";
        try {
            BufferedReader reader = GnomeNetworkProxy.executeCommand(command);
            String line = reader.readLine();
            while (line != null) {
                String key = GsettingsNetworkProxy.getKey(line).toLowerCase();
                if (key != null && !key.isEmpty()) {
                    String value = GsettingsNetworkProxy.getValue(line);
                    map.put(key, value);
                }
                line = reader.readLine();
            }
        }
        catch (IOException ioe) {
            LOGGER.log(Level.SEVERE, "Cannot read line: " + command, ioe);
        }
        return map;
    }

    private static String getKey(String line) {
        String[] splittedLine = line.split(" ");
        if (splittedLine.length >= 2) {
            return splittedLine[0] + "." + splittedLine[1];
        }
        return null;
    }

    private static String getValue(String line) {
        String[] splittedLine = line.split(" ");
        if (splittedLine.length > 2) {
            StringBuilder value = new StringBuilder();
            for (int i = 2; i < splittedLine.length; ++i) {
                value.append(splittedLine[i]);
            }
            return value.toString().replaceAll("'", "");
        }
        return null;
    }

    private static String[] getNoProxyHosts(String noProxyHostsString) {
        if (noProxyHostsString != null && !noProxyHostsString.isEmpty()) {
            if (noProxyHostsString.startsWith("[")) {
                noProxyHostsString = noProxyHostsString.substring(1);
            }
            if (noProxyHostsString.endsWith("]")) {
                noProxyHostsString = noProxyHostsString.substring(0, noProxyHostsString.length() - 1);
            }
            return noProxyHostsString.split(",");
        }
        return new String[0];
    }
}

