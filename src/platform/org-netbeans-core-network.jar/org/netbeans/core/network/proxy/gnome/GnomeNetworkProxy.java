/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.network.proxy.gnome;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.network.proxy.NetworkProxyResolver;
import org.netbeans.core.network.proxy.NetworkProxySettings;
import org.netbeans.core.network.proxy.gnome.GconfNetworkProxy;
import org.netbeans.core.network.proxy.gnome.GsettingsNetworkProxy;

public class GnomeNetworkProxy
implements NetworkProxyResolver {
    private static final Logger LOGGER = Logger.getLogger(GnomeNetworkProxy.class.getName());

    @Override
    public NetworkProxySettings getNetworkProxySettings() {
        if (new File("/usr/bin/gsettings").exists() && GsettingsNetworkProxy.isGsettingsValid()) {
            return GsettingsNetworkProxy.getNetworkProxySettings();
        }
        if (new File("/usr/bin/gconftool-2").exists() && GconfNetworkProxy.isGconfValid()) {
            return GconfNetworkProxy.getNetworkProxySettings();
        }
        return new NetworkProxySettings(false);
    }

    protected static BufferedReader executeCommand(String command) {
        BufferedReader reader = null;
        try {
            Process p = Runtime.getRuntime().exec(command);
            p.waitFor();
            reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        }
        catch (IOException ioe) {
            LOGGER.log(Level.SEVERE, "Cannot execute command: " + command, ioe);
        }
        catch (InterruptedException ie) {
            LOGGER.log(Level.SEVERE, "Cannot execute command: " + command, ie);
        }
        return reader;
    }
}

