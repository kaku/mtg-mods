/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.network.proxy.gnome;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.network.proxy.NetworkProxySettings;
import org.netbeans.core.network.proxy.gnome.GnomeNetworkProxy;

public class GconfNetworkProxy {
    private static final Logger LOGGER = Logger.getLogger(GconfNetworkProxy.class.getName());
    private static final String EQUALS = "=";
    private static final String COLON = ",";
    private static final String SQ_BRACKET_LEFT = "[";
    private static final String SQ_BRACKET_RIGHT = "]";
    protected static final String GCONF_PATH = "/usr/bin/gconftool-2";
    private static final String GCONF_ARGUMENT_LIST_RECURSIVELY = " -R ";
    private static final String GCONF_NODE_PROXY = "/system/proxy";
    private static final String GCONF_NODE_HTTP_PROXY = "/system/http_proxy";
    private static final String GCONF_KEY_MODE = "mode";
    private static final String GCONF_KEY_PAC_URL = "autoconfig_url";
    private static final String GCONF_KEY_HTTP_ALL = "use_http_proxy";
    private static final String GCONF_KEY_HTTP_HOST = "host";
    private static final String GCONF_KEY_HTTP_PORT = "port";
    private static final String GCONF_KEY_HTTPS_HOST = "secure_host";
    private static final String GCONF_KEY_HTTPS_PORT = "secure_port";
    private static final String GCONF_KEY_SOCKS_HOST = "socks_host";
    private static final String GCONF_KEY_SOCKS_PORT = "socks_port";
    private static final String GCONF_KEY_IGNORE_HOSTS = "ignore_hosts";
    private static final String GCONF_VALUE_NONE = "none";
    private static final String GCONF_VALUE_AUTO = "auto";
    private static final String GCONF_VALUE_MANUAL = "manual";

    protected static NetworkProxySettings getNetworkProxySettings() {
        LOGGER.log(Level.FINE, "GConf system proxy resolver started.");
        Map<String, String> proxyProperties = GconfNetworkProxy.getGconfMap("/system/proxy");
        String proxyMode = proxyProperties.get("mode");
        if (proxyMode == null) {
            LOGGER.log(Level.SEVERE, "GConf proxy mode is null.");
            return new NetworkProxySettings(false);
        }
        if (proxyMode.equals("none")) {
            LOGGER.log(Level.INFO, "GConf system proxy resolver: direct connection");
            return new NetworkProxySettings();
        }
        if (proxyMode.equals("auto")) {
            String pacUrl = proxyProperties.get("autoconfig_url");
            LOGGER.log(Level.INFO, "GConf system proxy resolver: auto - PAC ({0})", pacUrl);
            if (pacUrl != null) {
                return new NetworkProxySettings(pacUrl);
            }
            return new NetworkProxySettings("");
        }
        if (proxyMode.equals("manual")) {
            proxyProperties.putAll(GconfNetworkProxy.getGconfMap("/system/http_proxy"));
            String httpProxyAll = proxyProperties.get("use_http_proxy");
            String httpProxyHost = proxyProperties.get("host");
            String httpProxyPort = proxyProperties.get("port");
            String noProxyHosts = proxyProperties.get("ignore_hosts");
            LOGGER.log(Level.INFO, "GConf system proxy resolver: manual - http for all ({0})", httpProxyAll);
            LOGGER.log(Level.INFO, "GConf system proxy resolver: manual - http host ({0})", httpProxyHost);
            LOGGER.log(Level.INFO, "GConf system proxy resolver: manual - http port ({0})", httpProxyPort);
            LOGGER.log(Level.INFO, "GConf system proxy resolver: manual - no proxy hosts ({0})", noProxyHosts);
            if (httpProxyAll != null && Boolean.parseBoolean(httpProxyAll)) {
                return new NetworkProxySettings(httpProxyHost, httpProxyPort, GconfNetworkProxy.getNoProxyHosts(noProxyHosts));
            }
            String httpsProxyHost = proxyProperties.get("secure_host");
            String httpsProxyPort = proxyProperties.get("secure_port");
            String socksProxyHost = proxyProperties.get("socks_host");
            String socksProxyPort = proxyProperties.get("socks_port");
            LOGGER.log(Level.INFO, "GConf system proxy resolver: manual - https host ({0})", httpsProxyHost);
            LOGGER.log(Level.INFO, "GConf system proxy resolver: manual - https port ({0})", httpsProxyPort);
            LOGGER.log(Level.INFO, "GConf system proxy resolver: manual - socks host ({0})", socksProxyHost);
            LOGGER.log(Level.INFO, "GConf system proxy resolver: manual - socks port ({0})", socksProxyPort);
            return new NetworkProxySettings(httpProxyHost, httpProxyPort, httpsProxyHost, httpsProxyPort, socksProxyHost, socksProxyPort, GconfNetworkProxy.getNoProxyHosts(noProxyHosts));
        }
        return new NetworkProxySettings(false);
    }

    protected static boolean isGconfValid() {
        String command = "/usr/bin/gconftool-2 -R /system/proxy";
        try {
            BufferedReader reader = GnomeNetworkProxy.executeCommand(command);
            if (reader.ready()) {
                return true;
            }
        }
        catch (IOException ioe) {
            LOGGER.log(Level.SEVERE, "Cannot read line: " + command, ioe);
        }
        LOGGER.log(Level.WARNING, "GConf return empty list");
        return false;
    }

    private static Map<String, String> getGconfMap(String gconfNode) {
        HashMap<String, String> map;
        block5 : {
            map = new HashMap<String, String>();
            String command = "/usr/bin/gconftool-2 -R " + gconfNode;
            try {
                BufferedReader reader = GnomeNetworkProxy.executeCommand(command);
                if (reader != null) {
                    String line = reader.readLine();
                    while (line != null) {
                        String key = GconfNetworkProxy.getKey(line).toLowerCase();
                        if (key != null && !key.isEmpty()) {
                            String value = GconfNetworkProxy.getValue(line);
                            map.put(key, value);
                        }
                        line = reader.readLine();
                    }
                    break block5;
                }
                return map;
            }
            catch (IOException ioe) {
                LOGGER.log(Level.SEVERE, "Cannot read line: " + command, ioe);
            }
        }
        return map;
    }

    private static String getKey(String line) {
        return line.substring(0, line.indexOf("=")).trim();
    }

    private static String getValue(String line) {
        return line.substring(line.indexOf("=") + 1).trim();
    }

    private static String[] getNoProxyHosts(String noProxyHostsString) {
        if (noProxyHostsString != null && !noProxyHostsString.isEmpty()) {
            if (noProxyHostsString.startsWith("[")) {
                noProxyHostsString = noProxyHostsString.substring(1);
            }
            if (noProxyHostsString.endsWith("]")) {
                noProxyHostsString = noProxyHostsString.substring(0, noProxyHostsString.length() - 1);
            }
            return noProxyHostsString.split(",");
        }
        return new String[0];
    }
}

