/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.jna.Library
 *  com.sun.jna.Native
 *  com.sun.jna.Pointer
 *  com.sun.jna.Structure
 *  com.sun.jna.Structure$ByReference
 */
package org.netbeans.core.network.proxy.windows;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import java.util.Arrays;
import java.util.List;

public interface WindowsNetworkProxyLibrary
extends Library {
    public static final WindowsNetworkProxyLibrary LIBRARY = (WindowsNetworkProxyLibrary)Native.loadLibrary((String)"winhttp.dll", WindowsNetworkProxyLibrary.class);

    public boolean WinHttpGetIEProxyConfigForCurrentUser(ProxyConfig var1);

    public static class ProxyConfig
    extends Structure {
        public boolean autoDetect;
        public Pointer pacFile;
        public Pointer proxy;
        public Pointer proxyBypass;

        protected List getFieldOrder() {
            return Arrays.asList("autoDetect", "pacFile", "proxy", "proxyBypass");
        }

        public static class ByReference
        extends ProxyConfig
        implements Structure.ByReference {
        }

    }

}

