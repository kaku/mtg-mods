/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.settings;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(value=RetentionPolicy.SOURCE)
@Target(value={ElementType.TYPE})
public @interface ConvertAsProperties {
    public static final String IGNORE_ALL_CHANGES = "all";

    public String dtd();

    public boolean autostore() default 1;

    public String[] ignoreChanges() default {};
}

