/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.settings;

import java.lang.annotation.Annotation;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(value=RetentionPolicy.SOURCE)
@Target(value={ElementType.TYPE})
@Documented
public @interface ConvertAsJavaBean {
    public boolean subclasses() default 1;
}

