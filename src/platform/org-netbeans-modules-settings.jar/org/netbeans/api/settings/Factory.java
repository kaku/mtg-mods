/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.api.settings;

import org.netbeans.modules.settings.Env;
import org.netbeans.modules.settings.convertors.XMLPropertiesConvertor;
import org.openide.filesystems.FileObject;

final class Factory {
    private Factory() {
    }

    private static Object properties(FileObject fo) {
        return XMLPropertiesConvertor.create(fo);
    }

    private static Object create(FileObject fo) {
        return Env.create(fo);
    }
}

