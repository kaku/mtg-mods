/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 *  org.openide.xml.EntityCatalog
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.spi.settings;

import java.io.ByteArrayOutputStream;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.modules.settings.ContextProvider;
import org.netbeans.spi.settings.Convertor;
import org.netbeans.spi.settings.ConvertorResolver;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;
import org.openide.xml.EntityCatalog;
import org.openide.xml.XMLUtil;
import org.w3c.dom.CDATASection;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public abstract class DOMConvertor
extends Convertor {
    private static final String ATTR_PUBLIC_ID = "dtd_public_id";
    private static final String ATTR_ID = "id";
    private static final String ATTR_IDREF = "idref";
    private static final String ELM_DELEGATE = "domconvertor";
    private static final Map<Document, Map<Object, CacheRec>> refsCache = new HashMap<Document, Map<Object, CacheRec>>();
    private static final Map<Document, Lookup> ctxCache = new HashMap<Document, Lookup>();
    private String publicID;
    private String systemID;
    private String rootElement;

    protected DOMConvertor(String publicID, String systemID, String rootElement) {
        this.publicID = publicID;
        this.systemID = systemID;
        this.rootElement = rootElement;
        if (publicID == null) {
            throw new NullPointerException("publicID");
        }
        if (systemID == null) {
            throw new NullPointerException("systemID");
        }
        if (rootElement == null) {
            throw new NullPointerException("rootElement");
        }
    }

    @Override
    public final Object read(Reader r) throws IOException, ClassNotFoundException {
        Document doc = null;
        try {
            InputSource is = new InputSource(r);
            doc = XMLUtil.parse((InputSource)is, (boolean)false, (boolean)false, (ErrorHandler)null, (EntityResolver)EntityCatalog.getDefault());
            DOMConvertor.setDocumentContext(doc, DOMConvertor.findContext(r));
            Object object = this.readElement(doc.getDocumentElement());
            return object;
        }
        catch (SAXException ex) {
            IOException ioe = new IOException(ex.getLocalizedMessage());
            ioe.initCause(ex);
            throw ioe;
        }
        finally {
            if (doc != null) {
                DOMConvertor.clearCashesForDocument(doc);
            }
        }
    }

    @Override
    public final void write(Writer w, Object inst) throws IOException {
        Document doc = null;
        try {
            doc = XMLUtil.createDocument((String)this.rootElement, (String)null, (String)this.publicID, (String)this.systemID);
            DOMConvertor.setDocumentContext(doc, DOMConvertor.findContext(w));
            this.writeElement(doc, doc.getDocumentElement(), inst);
            ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
            XMLUtil.write((Document)doc, (OutputStream)baos, (String)"UTF-8");
            w.write(baos.toString("UTF-8"));
        }
        catch (DOMException ex) {
            IOException e = new IOException(ex.getLocalizedMessage());
            e.initCause(ex);
            throw e;
        }
        finally {
            if (doc != null) {
                DOMConvertor.clearCashesForDocument(doc);
            }
        }
    }

    protected abstract Object readElement(Element var1) throws IOException, ClassNotFoundException;

    protected abstract void writeElement(Document var1, Element var2, Object var3) throws IOException, DOMException;

    protected static final Object delegateRead(Element element) throws IOException, ClassNotFoundException {
        Object obj;
        String idref = element.getAttribute("idref");
        if (idref.length() != 0) {
            Object obj2 = DOMConvertor.getCache(element.getOwnerDocument(), idref.intern());
            if (obj2 != null) {
                return obj2;
            }
            throw new IOException("broken reference: " + element + ", idref=" + idref);
        }
        String publicId = element.getAttribute("dtd_public_id");
        Convertor c = ConvertorResolver.getDefault().getConvertor(publicId);
        if (c == null) {
            throw new IOException("Convertor not found. publicId: " + publicId);
        }
        if (element.getTagName().equals("domconvertor")) {
            NodeList children = element.getChildNodes();
            String content = null;
            int size = children.getLength();
            for (int i = 0; i < size; ++i) {
                String text;
                Node n = children.item(i);
                if (n.getNodeType() == 4) {
                    content = n.getNodeValue();
                    break;
                }
                if (n.getNodeType() != 3 || (text = n.getNodeValue().trim()).length() <= 0) continue;
                content = text;
                break;
            }
            if (content == null) {
                throw new IOException("Expected CDATA block under: " + element.getTagName());
            }
            obj = DOMConvertor.readFromString(c, content, DOMConvertor.findContext(element.getOwnerDocument()));
        } else if (c instanceof DOMConvertor) {
            DOMConvertor dc = (DOMConvertor)c;
            obj = dc.readElement(element);
        } else {
            throw new IOException("Missing DOMConvertor for publicId: " + publicId);
        }
        String id = element.getAttribute("id");
        if (id.length() != 0) {
            DOMConvertor.setCache(element.getOwnerDocument(), id, obj);
        }
        return obj;
    }

    protected static final Element delegateWrite(Document doc, Object obj) throws IOException, DOMException {
        Class<?> clazz;
        Element el;
        CacheRec cache = DOMConvertor.setCache(doc, obj);
        if (cache.used) {
            return DOMConvertor.writeReference(doc, cache);
        }
        ConvertorResolver res = ConvertorResolver.getDefault();
        Convertor c = res.getConvertor(clazz = obj.getClass());
        if (c == null) {
            throw new IOException("Convertor not found for object: " + obj);
        }
        if (c instanceof DOMConvertor) {
            DOMConvertor dc = (DOMConvertor)c;
            el = doc.createElement(dc.rootElement);
            dc.writeElement(doc, el, obj);
            if (el.getAttribute("dtd_public_id").length() == 0) {
                el.setAttribute("dtd_public_id", res.getPublicID(clazz));
            }
        } else {
            el = doc.createElement("domconvertor");
            el.setAttribute("dtd_public_id", res.getPublicID(clazz));
            el.appendChild(doc.createCDATASection(DOMConvertor.writeToString(c, obj, DOMConvertor.findContext(doc))));
        }
        cache.elm = el;
        return el;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected static Lookup findContext(Document doc) {
        Map<Document, Lookup> map = ctxCache;
        synchronized (map) {
            Lookup ctx = ctxCache.get(doc);
            return ctx == null ? Lookup.EMPTY : ctx;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void setDocumentContext(Document doc, Lookup ctx) {
        Map<Document, Lookup> map = ctxCache;
        synchronized (map) {
            ctxCache.put(doc, ctx);
        }
    }

    private static String writeToString(Convertor c, Object obj, Lookup ctx) throws IOException {
        CharArrayWriter caw = new CharArrayWriter(1024);
        Writer w = caw;
        FileObject fo = (FileObject)ctx.lookup(FileObject.class);
        if (fo != null) {
            w = ContextProvider.createWriterContextProvider(caw, fo);
        }
        c.write(w, obj);
        w.close();
        return caw.toString();
    }

    private static Object readFromString(Convertor c, String s, Lookup ctx) throws IOException, ClassNotFoundException {
        Reader r = new StringReader(s);
        FileObject fo = (FileObject)ctx.lookup(FileObject.class);
        if (fo != null) {
            r = ContextProvider.createReaderContextProvider(r, fo);
        }
        return c.read(r);
    }

    private static Element writeReference(Document doc, CacheRec cache) throws DOMException {
        Element el = doc.createElement(cache.elm.getTagName());
        el.setAttribute("idref", (String)cache.value);
        cache.elm.setAttribute("id", (String)cache.value);
        return el;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static CacheRec setCache(Document key, Object obj) {
        Map<Document, Map<Object, CacheRec>> map = refsCache;
        synchronized (map) {
            CacheRec cr;
            Map<Object, CacheRec> refs = refsCache.get(key);
            if (refs == null) {
                refs = new HashMap<Object, CacheRec>();
                refsCache.put(key, refs);
            }
            if ((cr = refs.get(obj)) == null) {
                cr = new CacheRec();
                cr.key = obj;
                cr.value = "ID_" + String.valueOf(refs.size());
                refs.put(obj, cr);
            }
            cr.used = cr.elm != null;
            return cr;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static CacheRec setCache(Document key, Object id, Object obj) {
        Map<Document, Map<Object, CacheRec>> map = refsCache;
        synchronized (map) {
            CacheRec cr;
            Map<Object, CacheRec> refs = refsCache.get(key);
            if (refs == null) {
                refs = new HashMap<Object, CacheRec>();
                refsCache.put(key, refs);
            }
            if ((cr = refs.get(id)) == null) {
                cr = new CacheRec();
                cr.key = id;
                cr.value = obj;
                refs.put(id, cr);
            }
            return cr;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static Object getCache(Document key, Object idref) {
        Map<Document, Map<Object, CacheRec>> map = refsCache;
        synchronized (map) {
            Map<Object, CacheRec> refs = refsCache.get(key);
            if (refs == null) {
                return null;
            }
            CacheRec cr = refs.get(idref);
            return cr.value;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void clearCashesForDocument(Document doc) {
        Map<Document, Map<Object, CacheRec>> map = refsCache;
        synchronized (map) {
            refsCache.remove(doc);
        }
        map = ctxCache;
        synchronized (map) {
            ctxCache.remove(doc);
        }
    }

    private static class CacheRec {
        Object key;
        Element elm;
        Object value;
        boolean used;

        CacheRec() {
        }
    }

}

