/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.settings;

import java.io.IOException;

public interface Saver {
    public void markDirty();

    public void requestSave() throws IOException;
}

