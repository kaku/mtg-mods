/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 */
package org.netbeans.spi.settings;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import org.netbeans.spi.settings.Saver;
import org.openide.util.Lookup;

public abstract class Convertor {
    public abstract void write(Writer var1, Object var2) throws IOException;

    public abstract Object read(Reader var1) throws IOException, ClassNotFoundException;

    public abstract void registerSaver(Object var1, Saver var2);

    public abstract void unregisterSaver(Object var1, Saver var2);

    protected static Lookup findContext(Reader r) {
        if (r instanceof Lookup.Provider) {
            return ((Lookup.Provider)r).getLookup();
        }
        return Lookup.EMPTY;
    }

    protected static Lookup findContext(Writer w) {
        if (w instanceof Lookup.Provider) {
            return ((Lookup.Provider)w).getLookup();
        }
        return Lookup.EMPTY;
    }
}

