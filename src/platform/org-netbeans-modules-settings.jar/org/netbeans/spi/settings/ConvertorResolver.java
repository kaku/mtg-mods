/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package org.netbeans.spi.settings;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.settings.Env;
import org.netbeans.spi.settings.Convertor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

final class ConvertorResolver {
    private static final String LOOKUP_PREFIX = "/xml/lookups";
    private static final ConvertorResolver DEFAULT = new ConvertorResolver();

    private ConvertorResolver() {
    }

    protected static ConvertorResolver getDefault() {
        return DEFAULT;
    }

    protected Convertor getConvertor(Class clazz) {
        try {
            FileObject fo = Env.findProvider(clazz);
            if (fo == null) {
                fo = Env.findProvider(Object.class);
            }
            return this.getConvertor(fo);
        }
        catch (IOException ex) {
            Logger.getLogger(ConvertorResolver.class.getName()).log(Level.WARNING, null, ex);
            return null;
        }
    }

    String getPublicID(Class clazz) {
        try {
            Object attrib;
            FileObject fo = Env.findProvider(clazz);
            if (fo == null) {
                fo = Env.findProvider(Object.class);
            }
            return (attrib = (fo = Env.findEntityRegistration(fo)).getAttribute("hint.originalPublicID")) == null || !(attrib instanceof String) ? null : (String)attrib;
        }
        catch (IOException ex) {
            Logger.getLogger(ConvertorResolver.class.getName()).log(Level.WARNING, null, ex);
            return null;
        }
    }

    protected Convertor getConvertor(String publicID) {
        StringBuffer sb = new StringBuffer(200);
        sb.append("/xml/lookups");
        sb.append(ConvertorResolver.convertPublicId(publicID));
        sb.append(".instance");
        FileObject fo = FileUtil.getConfigFile((String)sb.toString());
        return fo == null ? null : this.getConvertor(fo);
    }

    private Convertor getConvertor(FileObject fo) {
        Object attrb = fo.getAttribute("settings.convertor");
        return attrb == null || !(attrb instanceof Convertor) ? null : (Convertor)attrb;
    }

    /*
     * Unable to fully structure code
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private static String convertPublicId(String publicID) {
        arr = publicID.toCharArray();
        numberofslashes = 0;
        state = 0;
        write = 0;
        i = 0;
        while (i < arr.length) {
            ch = arr[i];
            switch (state) {
                case 0: {
                    if (ch == 43 || ch == 45 || ch == 73 || ch == 83 || ch == 79) break;
                    state = 1;
                }
                case 1: {
                    if (ch == 47) {
                        state = 2;
                        if (++numberofslashes == 3) return new String(arr, 0, write);
                        arr[write++] = 47;
                        break;
                    }
                    ** GOTO lbl-1000
                }
                case 2: {
                    if (ch == 47) break;
                    state = 1;
                }
                default: lbl-1000: // 2 sources:
                {
                    arr[write++] = ch >= 65 && ch <= 90 || ch >= 97 && ch <= 122 || ch >= 48 && ch <= 57 ? ch : 95;
                }
            }
            ++i;
        }
        return new String(arr, 0, write);
    }
}

