/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.ModuleManager
 *  org.netbeans.core.startup.Main
 *  org.netbeans.core.startup.ModuleSystem
 *  org.openide.cookies.InstanceCookie
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObject$Container
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.loaders.FolderLookup
 *  org.openide.util.Lookup
 *  org.openide.util.WeakListeners
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 *  org.openide.util.lookup.implspi.NamedServicesProvider
 */
package org.netbeans.modules.settings;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.ModuleManager;
import org.netbeans.core.startup.Main;
import org.netbeans.core.startup.ModuleSystem;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.loaders.FolderLookup;
import org.openide.util.Lookup;
import org.openide.util.WeakListeners;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import org.openide.util.lookup.implspi.NamedServicesProvider;

public final class RecognizeInstanceObjects
extends NamedServicesProvider {
    private static final Logger LOG = Logger.getLogger(RecognizeInstanceObjects.class.getName());

    public <T> T lookupObject(String path, Class<T> type) {
        FileObject fo = FileUtil.getConfigFile((String)path);
        if (fo != null) {
            try {
                InstanceCookie ic = (InstanceCookie)DataObject.find((FileObject)fo).getLookup().lookup(InstanceCookie.class);
                Object obj = ic != null ? ic.instanceCreate() : null;
                return type.isInstance(obj) ? (T)type.cast(obj) : null;
            }
            catch (IOException ex) {
                LOG.log(Level.INFO, "Cannot create instance for " + path, ex);
            }
            catch (ClassNotFoundException ex) {
                LOG.log(Level.INFO, "Cannot create instance for " + path, ex);
            }
        }
        return null;
    }

    public Lookup create(String path) {
        return new OverObjects(path);
    }

    protected Lookup lookupFor(Object obj) {
        if (obj instanceof FileObject) {
            try {
                return DataObject.find((FileObject)((FileObject)obj)).getLookup();
            }
            catch (DataObjectNotFoundException ex) {
                LOG.log(Level.INFO, "Can't find DataObject for " + obj, (Throwable)ex);
            }
        }
        return null;
    }

    private static final class OverObjects
    extends ProxyLookup
    implements PropertyChangeListener,
    FileChangeListener {
        private final String path;

        public OverObjects(String path) {
            block6 : {
                super(OverObjects.delegates(null, path));
                this.path = path;
                try {
                    ModuleSystem ms = Main.getModuleSystem((boolean)false);
                    if (ms != null) {
                        ModuleManager man = ms.getManager();
                        man.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)man));
                    } else {
                        LOG.log(Level.WARNING, "Not listening on module system");
                    }
                }
                catch (Throwable e) {
                    LOG.log(Level.WARNING, "Can't listen on module system", e);
                }
                try {
                    FileSystem sfs = FileUtil.getConfigRoot().getFileSystem();
                    sfs.addFileChangeListener(FileUtil.weakFileChangeListener((FileChangeListener)this, (Object)sfs));
                }
                catch (FileStateInvalidException x) {
                    if ($assertionsDisabled) break block6;
                    throw new AssertionError((Object)x);
                }
            }
        }

        private static Lookup[] delegates(Lookup prevFolderLkp, String path) {
            ClassLoader loader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
            LOG.log(Level.FINEST, "lkp loader: {0}", loader);
            if (loader == null) {
                loader = Thread.currentThread().getContextClassLoader();
                LOG.log(Level.FINEST, "ccl: {0}", loader);
            }
            if (loader == null) {
                loader = RecognizeInstanceObjects.class.getClassLoader();
            }
            LOG.log(Level.FINER, "metaInfServices for {0}", loader);
            Lookup base = Lookups.metaInfServices((ClassLoader)loader, (String)("META-INF/namedservices/" + path));
            FileObject fo = FileUtil.getConfigFile((String)path);
            if (fo == null) {
                return new Lookup[]{base};
            }
            String s = path.endsWith("/") ? path.substring(0, path.length() - 1) : path;
            if (prevFolderLkp == null) {
                prevFolderLkp = new FolderLookup((DataObject.Container)DataFolder.findFolder((FileObject)fo), s).getLookup();
            }
            return new Lookup[]{prevFolderLkp, base};
        }

        Lookup extractFolderLkp() {
            Lookup[] arr = this.getLookups();
            return arr.length == 2 ? arr[0] : null;
        }

        @Override
        public void propertyChange(PropertyChangeEvent ev) {
            this.setLookups(OverObjects.delegates(this.extractFolderLkp(), this.path));
        }

        public void fileFolderCreated(FileEvent fe) {
            this.ch(fe);
        }

        public void fileDataCreated(FileEvent fe) {
            this.ch(fe);
        }

        public void fileChanged(FileEvent fe) {
            this.ch(fe);
        }

        public void fileDeleted(FileEvent fe) {
            this.ch(fe);
        }

        public void fileRenamed(FileRenameEvent fe) {
            this.ch((FileEvent)fe);
        }

        public void fileAttributeChanged(FileAttributeEvent fe) {
            this.ch((FileEvent)fe);
        }

        private void ch(FileEvent e) {
            if ((e.getFile().getPath() + "/").startsWith(this.path)) {
                this.setLookups(OverObjects.delegates(this.extractFolderLkp(), this.path));
            }
        }
    }

}

