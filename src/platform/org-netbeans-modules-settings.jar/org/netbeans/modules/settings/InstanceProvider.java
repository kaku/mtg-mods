/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.InstanceCookie$Of
 *  org.openide.cookies.SaveCookie
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 */
package org.netbeans.modules.settings;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.ref.SoftReference;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.settings.ContextProvider;
import org.netbeans.modules.settings.Env;
import org.netbeans.modules.settings.SaveSupport;
import org.netbeans.modules.settings.ScheduledRequest;
import org.netbeans.modules.settings.convertors.SerialDataNode;
import org.netbeans.spi.settings.Convertor;
import org.openide.cookies.InstanceCookie;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

final class InstanceProvider
extends FileChangeAdapter
implements PropertyChangeListener,
FileSystem.AtomicAction {
    private static final Logger LOG = Logger.getLogger(InstanceProvider.class.getName());
    private final InstanceContent lkpContent;
    private final Lookup lookup;
    private final DataObject dobj;
    private final FileObject settingFO;
    private final FileObject providerFO;
    private final NodeConvertor node;
    private SaveSupport saver;
    private SaveCookie scCache;
    private boolean wasReportedProblem = false;
    private Set<String> instanceOfSet;
    private String instanceClassName;
    final Object READWRITE_LOCK = new Object();
    private Convertor convertor;
    private ScheduledRequest request;

    public InstanceProvider(DataObject dobj, FileObject providerFO) {
        this.settingFO = dobj.getPrimaryFile();
        this.providerFO = providerFO;
        this.dobj = dobj;
        this.settingFO.addFileChangeListener(FileUtil.weakFileChangeListener((FileChangeListener)this, (Object)this.settingFO));
        this.lkpContent = new InstanceContent();
        this.lkpContent.add((Object)this.createInstance(null));
        this.node = new NodeConvertor();
        this.lkpContent.add((Object)this, (InstanceContent.Convertor)this.node);
        this.lookup = new AbstractLookup((AbstractLookup.Content)this.lkpContent);
    }

    public Lookup getLookup() {
        return this.lookup;
    }

    FileObject getProvider() {
        return this.providerFO;
    }

    FileObject getFile() {
        return this.settingFO;
    }

    DataObject getDataObject() {
        return this.dobj;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt == null) {
            return;
        }
        String name = evt.getPropertyName();
        if (name == null) {
            return;
        }
        if (name == "savecookie") {
            this.provideSaveCookie();
        } else if (name == "fileChanged") {
            InstanceProvider instanceProvider = this;
            synchronized (instanceProvider) {
                this.instanceOfSet = null;
            }
            this.instanceCookieChanged(null);
        }
    }

    public void fileChanged(FileEvent fe) {
        if (this.saver != null && fe.firedFrom((FileSystem.AtomicAction)this.saver.getSaveCookie())) {
            return;
        }
        this.propertyChange(new PropertyChangeEvent(this, "fileChanged", null, null));
    }

    public void fileDeleted(FileEvent fe) {
        if (this.saver != null && fe.firedFrom((FileSystem.AtomicAction)this.saver.getSaveCookie())) {
            return;
        }
        this.releaseInstance();
    }

    private synchronized void attachToInstance(Object inst) {
        if (this.saver != null) {
            this.saver.removePropertyChangeListener(this);
            this.getScheduledRequest().forceToFinish();
        }
        this.saver = this.createSaveSupport(inst);
        this.saver.addPropertyChangeListener(this);
    }

    private InstanceCookie.Of createInstance(Object inst) {
        return new InstanceCookieImpl(inst);
    }

    private SaveSupport createSaveSupport(Object inst) {
        return new SaveSupport(this, inst);
    }

    private void provideSaveCookie() {
        SaveCookie scNew = this.saver.getSaveCookie();
        if (this.scCache != null) {
            if (!this.saver.isChanged()) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("remove save cookie: " + (Object)this.dobj);
                }
                this.lkpContent.remove((Object)this.scCache);
                this.scCache = null;
                return;
            }
        } else if (this.saver.isChanged()) {
            this.scCache = scNew;
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("add save cookie: " + (Object)this.dobj + " cookie: " + (Object)scNew);
            }
            this.lkpContent.add((Object)scNew);
            return;
        }
    }

    private void releaseInstance() {
        SaveSupport _saver = this.saver;
        if (_saver != null) {
            _saver.removePropertyChangeListener(this);
        }
        if (this.scCache != null) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("release instance and remove save cookie: " + (Object)this.dobj);
            }
            this.lkpContent.remove((Object)this.scCache);
            this.getScheduledRequest().cancel();
            this.scCache = null;
        }
        this.lkpContent.remove((Object)this, (InstanceContent.Convertor)this.node);
    }

    private void instanceCookieChanged(Object inst) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("instanceCookieChanged: " + (Object)this.dobj + " inst: " + inst);
        }
        this.releaseInstance();
        this.lkpContent.add((Object)this, (InstanceContent.Convertor)this.node);
        Object ic = this.lookup.lookup(InstanceCookie.class);
        this.lkpContent.remove(ic);
        InstanceCookie.Of newCookie = this.createInstance(inst);
        this.lkpContent.add((Object)newCookie);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("cookie replaced: " + (Object)this.dobj + " old: " + ic + " new: " + (Object)newCookie);
        }
    }

    Convertor getConvertor() throws IOException {
        if (this.convertor == null) {
            Object attrb = this.providerFO.getAttribute("settings.convertor");
            if (attrb == null || !(attrb instanceof Convertor)) {
                throw new IOException("cannot create convertor: " + attrb + ", provider:" + (Object)this.providerFO);
            }
            this.convertor = (Convertor)attrb;
        }
        return this.convertor;
    }

    private synchronized String getInstanceClassName() {
        if (this.instanceClassName == null) {
            Object name = this.providerFO.getAttribute("settings.instanceClass");
            this.instanceClassName = name != null && name instanceof String ? Utilities.translate((String)((String)name)) : null;
        }
        return this.instanceClassName;
    }

    public String toString() {
        return this.getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + '[' + (Object)this.getDataObject() + ", " + (Object)this.getProvider() + ']';
    }

    public void run() throws IOException {
        this.saver.writeDown();
    }

    synchronized ScheduledRequest getScheduledRequest() {
        if (this.request == null) {
            this.request = new ScheduledRequest(this.settingFO, this);
        }
        return this.request;
    }

    private static final class NodeConvertor
    implements InstanceContent.Convertor<InstanceProvider, Node> {
        NodeConvertor() {
        }

        public Node convert(InstanceProvider o) {
            return new SerialDataNode(o.getDataObject());
        }

        public Class<Node> type(InstanceProvider o) {
            return Node.class;
        }

        public String id(InstanceProvider o) {
            return o.toString();
        }

        public String displayName(InstanceProvider o) {
            return o.toString();
        }
    }

    final class InstanceCookieImpl
    implements InstanceCookie.Of {
        private SoftReference<Object> cachedInstance;

        public InstanceCookieImpl(Object inst) {
            this.setCachedInstance(inst);
        }

        public Class instanceClass() throws IOException, ClassNotFoundException {
            String name = InstanceProvider.this.getInstanceClassName();
            if (name == null) {
                Object instanceCreate = this.instanceCreate();
                if (instanceCreate != null) {
                    return instanceCreate.getClass();
                }
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "instance could not be created for: {0}", InstanceProvider.this.getInstanceClassName());
                }
                return null;
            }
            return ((ClassLoader)Lookup.getDefault().lookup(ClassLoader.class)).loadClass(name);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Object instanceCreate() throws IOException, ClassNotFoundException {
            Object inst;
            Object object = this;
            synchronized (object) {
                inst = this.getCachedInstance();
                if (inst != null) {
                    return inst;
                }
            }
            try {
                object = InstanceProvider.this.READWRITE_LOCK;
                synchronized (object) {
                    Reader r = ContextProvider.createReaderContextProvider(new InputStreamReader(InstanceProvider.this.settingFO.getInputStream(), "UTF-8"), InstanceProvider.this.getFile());
                    inst = InstanceProvider.this.getConvertor().read(r);
                }
            }
            catch (IOException ex) {
                throw (IOException)Exceptions.attachLocalizedMessage((Throwable)ex, (String)InstanceProvider.this.toString());
            }
            catch (ClassNotFoundException ex) {
                throw (ClassNotFoundException)Exceptions.attachLocalizedMessage((Throwable)ex, (String)InstanceProvider.this.toString());
            }
            InstanceCookieImpl ex = this;
            synchronized (ex) {
                Object existing = this.getCachedInstance();
                if (existing != null) {
                    return existing;
                }
                this.setCachedInstance(inst);
            }
            InstanceProvider.this.attachToInstance(inst);
            return inst;
        }

        public String instanceName() {
            String name = InstanceProvider.this.getInstanceClassName();
            if (name != null) {
                return name;
            }
            Exception e = null;
            try {
                Class instanceClass = this.instanceClass();
                if (instanceClass != null) {
                    return instanceClass.getName();
                }
            }
            catch (IOException ex) {
                e = ex;
            }
            catch (ClassNotFoundException ex) {
                e = ex;
            }
            if (e != null && !InstanceProvider.this.wasReportedProblem) {
                InstanceProvider.this.wasReportedProblem = true;
                Exceptions.attachLocalizedMessage((Throwable)e, (String)InstanceProvider.this.dobj.toString());
                Logger.getLogger(InstanceProvider.class.getName()).log(Level.WARNING, null, e);
            }
            return "Unknown";
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public boolean instanceOf(Class<?> type) {
            InstanceProvider instanceProvider = InstanceProvider.this;
            synchronized (instanceProvider) {
                if (InstanceProvider.this.instanceOfSet == null) {
                    InstanceProvider.this.instanceOfSet = Env.parseAttribute(InstanceProvider.this.providerFO.getAttribute("settings.instanceOf"));
                    Iterator it = InstanceProvider.this.instanceOfSet.iterator();
                    InstanceProvider.this.instanceOfSet = new HashSet(InstanceProvider.this.instanceOfSet.size() * 5 / 4);
                    while (it.hasNext()) {
                        InstanceProvider.this.instanceOfSet.add(Utilities.translate((String)((String)it.next())));
                    }
                }
            }
            if (InstanceProvider.this.instanceOfSet.isEmpty()) {
                Exception e = null;
                try {
                    Class instanceClass = this.instanceClass();
                    if (instanceClass != null) {
                        return type.isAssignableFrom(instanceClass);
                    }
                }
                catch (IOException ex) {
                    e = ex;
                }
                catch (ClassNotFoundException ex) {
                    e = ex;
                }
                if (e != null && !InstanceProvider.this.wasReportedProblem) {
                    InstanceProvider.this.wasReportedProblem = true;
                    Exceptions.attachLocalizedMessage((Throwable)e, (String)InstanceProvider.this.dobj.toString());
                    Logger.getLogger(InstanceProvider.class.getName()).log(Level.WARNING, null, e);
                }
                return false;
            }
            return InstanceProvider.this.instanceOfSet.contains(type.getName());
        }

        public void setInstance(Object inst, boolean save) throws IOException {
            InstanceProvider.this.instanceCookieChanged(inst);
            if (inst != null) {
                InstanceProvider.this.attachToInstance(inst);
                if (save) {
                    InstanceProvider.this.getScheduledRequest().runAndWait();
                }
            }
        }

        private Object getCachedInstance() {
            return this.cachedInstance.get();
        }

        private void setCachedInstance(Object inst) {
            this.cachedInstance = new SoftReference<Object>(inst);
        }
    }

}

