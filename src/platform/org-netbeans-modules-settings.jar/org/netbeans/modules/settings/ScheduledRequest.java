/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.util.Exceptions
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.settings;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;

public final class ScheduledRequest
implements Runnable {
    private static final RequestProcessor PROCESSOR = new RequestProcessor("Settings Processor");
    private static final int DELAY = 2000;
    private Object inst;
    private RequestProcessor.Task task;
    private FileLock lock;
    private final FileObject fobj;
    private final FileSystem.AtomicAction run;
    private boolean running = false;
    private int counter = 0;

    public ScheduledRequest(FileObject fobj, FileSystem.AtomicAction run) {
        this.fobj = fobj;
        this.run = run;
    }

    public boolean isRunning() {
        return this.running;
    }

    public FileLock getFileLock() {
        return this.lock;
    }

    public synchronized void schedule(Object obj) {
        this.schedule(obj, 2000);
    }

    private void schedule(Object obj, int delay) {
        if (obj == null) {
            return;
        }
        if (this.inst != null && this.inst != obj) {
            throw new IllegalStateException("Inconsistant state! File: " + (Object)this.fobj);
        }
        try {
            if (this.lock == null) {
                this.lock = this.fobj.lock();
            }
        }
        catch (IOException ex) {
            Logger.getLogger(ScheduledRequest.class.getName()).log(Level.WARNING, null, ex);
            return;
        }
        ++this.counter;
        if (this.task == null) {
            this.task = PROCESSOR.post((Runnable)this, delay);
        } else {
            this.task.schedule(delay);
        }
        this.inst = obj;
    }

    public synchronized void runAndWait() throws IOException {
        if (this.task != null) {
            this.task.cancel();
            this.counter = 0;
            this.releaseResources();
        }
        this.lock = this.fobj.lock();
        this.performRequest();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void forceToFinish() {
        RequestProcessor.Task t = null;
        ScheduledRequest scheduledRequest = this;
        synchronized (scheduledRequest) {
            if (this.task != null) {
                t = this.task;
                this.task.schedule(0);
            }
        }
        if (t != null) {
            t.waitFinished();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void cancel() {
        RequestProcessor.Task t = this.task;
        if (t == null) {
            return;
        }
        if (this.isRunning()) {
            t.waitFinished();
        } else {
            ScheduledRequest scheduledRequest = this;
            synchronized (scheduledRequest) {
                t.cancel();
                this.counter = 0;
                this.releaseResources();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void run() {
        ScheduledRequest scheduledRequest = this;
        synchronized (scheduledRequest) {
            this.counter = 0;
        }
        try {
            this.performRequest();
        }
        catch (IOException ex) {
            Exceptions.attachLocalizedMessage((Throwable)ex, (String)this.fobj.toString());
            Logger.getLogger(ScheduledRequest.class.getName()).log(Level.WARNING, null, ex);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void performRequest() throws IOException {
        RequestProcessor.Task runningTask;
        boolean isAlreadyRunning = false;
        ScheduledRequest scheduledRequest = this;
        synchronized (scheduledRequest) {
            isAlreadyRunning = this.running;
            runningTask = this.task;
            this.running = true;
        }
        if (isAlreadyRunning) {
            runningTask.waitFinished();
            return;
        }
        try {
            this.run.run();
        }
        finally {
            scheduledRequest = this;
            synchronized (scheduledRequest) {
                this.running = false;
                if (this.task == null || this.counter == 0) {
                    this.releaseResources();
                }
            }
        }
    }

    private void releaseResources() {
        if (this.lock != null) {
            this.lock.releaseLock();
            this.lock = null;
        }
        this.inst = null;
        this.task = null;
    }
}

