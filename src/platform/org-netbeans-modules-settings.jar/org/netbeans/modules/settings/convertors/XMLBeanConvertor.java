/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.io.ReaderInputStream
 */
package org.netbeans.modules.settings.convertors;

import java.beans.ExceptionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.Buffer;
import java.nio.CharBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.modules.settings.convertors.XMLSettingsSupport;
import org.netbeans.spi.settings.Convertor;
import org.netbeans.spi.settings.Saver;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.io.ReaderInputStream;

public final class XMLBeanConvertor
extends Convertor
implements PropertyChangeListener {
    private Saver saver;

    public static Convertor create() {
        return new XMLBeanConvertor();
    }

    @Override
    public Object read(Reader r) throws IOException, ClassNotFoundException {
        BufferedReader buf = new BufferedReader(r, 4096);
        CharBuffer arr = CharBuffer.allocate(2048);
        buf.mark(arr.capacity());
        buf.read(arr);
        arr.flip();
        Matcher m = Pattern.compile("<java").matcher(arr);
        if (m.find()) {
            buf.reset();
            buf.skip(m.start());
        } else {
            buf.reset();
        }
        XMLDecoder d = new XMLDecoder((InputStream)new ReaderInputStream((Reader)buf, "UTF-8"));
        return d.readObject();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void write(Writer w, final Object inst) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        XMLEncoder e = new XMLEncoder(out);
        e.setExceptionListener(new ExceptionListener(){

            @Override
            public void exceptionThrown(Exception x) {
                Logger.getLogger(XMLBeanConvertor.class.getName()).log(Level.INFO, "Problem writing " + inst, x);
            }
        });
        ClassLoader ccl = Thread.currentThread().getContextClassLoader();
        try {
            ClassLoader ccl2 = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
            if (ccl2 != null) {
                Thread.currentThread().setContextClassLoader(ccl2);
            }
            e.writeObject(inst);
        }
        finally {
            Thread.currentThread().setContextClassLoader(ccl);
        }
        e.close();
        String data = new String(out.toByteArray(), "UTF-8");
        data = data.replaceFirst("<java", "<!DOCTYPE xmlbeans PUBLIC \"-//NetBeans//DTD XML beans 1.0//EN\" \"http://www.netbeans.org/dtds/xml-beans-1_0.dtd\">\n<java");
        w.write(data);
    }

    @Override
    public void registerSaver(Object inst, Saver s) {
        if (this.saver != null) {
            XMLSettingsSupport.err.warning("[Warning] Saver already registered");
            return;
        }
        try {
            Method method = inst.getClass().getMethod("addPropertyChangeListener", PropertyChangeListener.class);
            method.invoke(inst, this);
            this.saver = s;
        }
        catch (NoSuchMethodException ex) {
            XMLSettingsSupport.err.warning("ObjectChangesNotifier: NoSuchMethodException: " + inst.getClass().getName() + ".addPropertyChangeListener");
        }
        catch (IllegalAccessException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (InvocationTargetException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    @Override
    public void unregisterSaver(Object inst, Saver s) {
        if (this.saver == null) {
            return;
        }
        if (this.saver != s) {
            XMLSettingsSupport.err.warning("[Warning] trying unregistered unknown Saver");
            return;
        }
        try {
            Method method = inst.getClass().getMethod("removePropertyChangeListener", PropertyChangeListener.class);
            method.invoke(inst, this);
            this.saver = null;
        }
        catch (NoSuchMethodException ex) {
            XMLSettingsSupport.err.fine("ObjectChangesNotifier: NoSuchMethodException: " + inst.getClass().getName() + ".removePropertyChangeListener");
        }
        catch (IllegalAccessException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (InvocationTargetException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (this.saver == null) {
            return;
        }
        if (this.acceptSave()) {
            try {
                this.saver.requestSave();
            }
            catch (IOException ex) {
                Logger.getLogger(XMLBeanConvertor.class.getName()).log(Level.WARNING, null, ex);
            }
        } else {
            this.saver.markDirty();
        }
    }

    private boolean acceptSave() {
        return true;
    }

}

