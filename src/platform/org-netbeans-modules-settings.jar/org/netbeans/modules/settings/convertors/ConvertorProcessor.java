/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 */
package org.netbeans.modules.settings.convertors;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import org.netbeans.api.settings.ConvertAsJavaBean;
import org.netbeans.api.settings.ConvertAsProperties;
import org.netbeans.api.settings.FactoryMethod;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;

@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
public class ConvertorProcessor
extends LayerGeneratingProcessor {
    public Set<String> getSupportedAnnotationTypes() {
        return new HashSet<String>(Arrays.asList(ConvertAsProperties.class.getCanonicalName(), ConvertAsJavaBean.class.getCanonicalName(), FactoryMethod.class.getCanonicalName()));
    }

    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment env) throws LayerGenerationException {
        String convElem;
        if (env.processingOver()) {
            return false;
        }
        for (Element e22 : env.getElementsAnnotatedWith(ConvertAsProperties.class)) {
            ConvertAsProperties reg = e22.getAnnotation(ConvertAsProperties.class);
            convElem = this.instantiableClassOrMethod(e22, true, reg);
            String dtd = reg.dtd();
            String dtdCode = ConvertorProcessor.convertPublicId(dtd);
            this.layer(new Element[]{e22}).file("xml/entities" + dtdCode).url("nbres:/org/netbeans/modules/settings/resources/properties-1_0.dtd").stringvalue("hint.originalPublicID", dtd).write();
            this.layer(new Element[]{e22}).file("xml/memory/" + convElem.replace('.', '/')).stringvalue("settings.providerPath", "xml/lookups/" + dtdCode + ".instance").write();
            LayerBuilder.File f = this.layer(new Element[]{e22}).file("xml/lookups" + dtdCode + ".instance").methodvalue("instanceCreate", "org.netbeans.api.settings.Factory", "create").methodvalue("settings.convertor", "org.netbeans.api.settings.Factory", "properties").stringvalue("settings.instanceClass", convElem).stringvalue("settings.instanceOf", convElem).boolvalue("xmlproperties.preventStoring", !reg.autostore());
            ConvertorProcessor.commaSeparated(f, reg.ignoreChanges()).write();
        }
        for (Element e : env.getElementsAnnotatedWith(ConvertAsJavaBean.class)) {
            ConvertAsJavaBean reg = e.getAnnotation(ConvertAsJavaBean.class);
            if (reg == null) continue;
            convElem = this.instantiableClassOrMethod(e, false, reg);
            LayerBuilder.File f = this.layer(new Element[]{e}).file("xml/memory/" + convElem.replace('.', '/'));
            f.stringvalue("settings.providerPath", "xml/lookups/NetBeans/DTD_XML_beans_1_0.instance");
            if (reg.subclasses()) {
                f.boolvalue("settings.subclasses", true);
            }
            f.write();
        }
        for (Element e2 : env.getElementsAnnotatedWith(FactoryMethod.class)) {
            FactoryMethod m = e2.getAnnotation(FactoryMethod.class);
            if (m == null) continue;
            boolean found = false;
            for (Element ch : e2.getEnclosedElements()) {
                if (ch.getKind() != ElementKind.METHOD || !m.value().equals(ch.getSimpleName().toString())) continue;
                ExecutableElement ee = (ExecutableElement)ch;
                if (ee.getParameters().size() > 0) {
                    throw new LayerGenerationException("Factory method " + m.value() + " must have no parameters", (Element)ee);
                }
                if (!ee.getModifiers().contains((Object)Modifier.STATIC)) {
                    throw new LayerGenerationException("Factory method " + m.value() + " has to be static", (Element)ee);
                }
                if (!this.processingEnv.getTypeUtils().isSameType(ee.getReturnType(), e2.asType())) {
                    throw new LayerGenerationException("Factory method " + m.value() + " must return " + e2.getSimpleName(), (Element)ee);
                }
                found = true;
            }
            if (found) continue;
            throw new LayerGenerationException("Method named " + m.value() + " was not found in this class", e2);
        }
        return true;
    }

    /*
     * Unable to fully structure code
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private static String convertPublicId(String publicID) {
        arr = publicID.toCharArray();
        numberofslashes = 0;
        state = 0;
        write = 0;
        i = 0;
        while (i < arr.length) {
            ch = arr[i];
            switch (state) {
                case 0: {
                    if (ch == 43 || ch == 45 || ch == 73 || ch == 83 || ch == 79) break;
                    state = 1;
                }
                case 1: {
                    if (ch == 47) {
                        state = 2;
                        if (++numberofslashes == 3) return new String(arr, 0, write);
                        arr[write++] = 47;
                        break;
                    }
                    ** GOTO lbl-1000
                }
                case 2: {
                    if (ch == 47) break;
                    state = 1;
                }
                default: lbl-1000: // 2 sources:
                {
                    arr[write++] = ch >= 65 && ch <= 90 || ch >= 97 && ch <= 122 || ch >= 48 && ch <= 57 ? ch : 95;
                }
            }
            ++i;
        }
        return new String(arr, 0, write);
    }

    private static LayerBuilder.File commaSeparated(LayerBuilder.File f, String[] arr) {
        if (arr.length == 0) {
            return f;
        }
        StringBuilder sb = new StringBuilder();
        String sep = "";
        for (String s : arr) {
            sb.append(sep);
            sb.append(s);
            sep = ",";
        }
        return f.stringvalue("xmlproperties.ignoreChanges", sb.toString());
    }

    private String instantiableClassOrMethod(Element e, boolean checkMethods, Annotation r) throws IllegalArgumentException, LayerGenerationException {
        switch (e.getKind()) {
            case CLASS: {
                String clazz = this.processingEnv.getElementUtils().getBinaryName((TypeElement)e).toString();
                if (e.getModifiers().contains((Object)Modifier.ABSTRACT)) {
                    throw new LayerGenerationException(clazz + " must not be abstract", e, this.processingEnv, r);
                }
                boolean hasDefaultCtor = false;
                for (ExecutableElement constructor : ElementFilter.constructorsIn(e.getEnclosedElements())) {
                    if (!constructor.getParameters().isEmpty()) continue;
                    hasDefaultCtor = true;
                    break;
                }
                if (!hasDefaultCtor) {
                    throw new LayerGenerationException(clazz + " must have a no-argument constructor", e, this.processingEnv, r);
                }
                if (checkMethods) {
                    TypeMirror propType = this.processingEnv.getElementUtils().getTypeElement("java.util.Properties").asType();
                    boolean hasRead = false;
                    boolean hasWrite = false;
                    for (ExecutableElement m : ElementFilter.methodsIn(e.getEnclosedElements())) {
                        if (m.getParameters().size() == 1 && m.getSimpleName().contentEquals("readProperties") && m.getParameters().get(0).asType().equals(propType)) {
                            hasRead = true;
                        }
                        if (m.getParameters().size() != 1 || !m.getSimpleName().contentEquals("writeProperties") || !m.getParameters().get(0).asType().equals(propType) || m.getReturnType().getKind() != TypeKind.VOID) continue;
                        hasWrite = true;
                    }
                    if (!hasRead) {
                        throw new LayerGenerationException(clazz + " must have proper readProperties method", e, this.processingEnv, r);
                    }
                    if (!hasWrite) {
                        throw new LayerGenerationException(clazz + " must have proper writeProperties method", e, this.processingEnv, r);
                    }
                }
                return clazz;
            }
        }
        throw new LayerGenerationException("Annotated element is not loadable as an instance", e, this.processingEnv, r);
    }

}

