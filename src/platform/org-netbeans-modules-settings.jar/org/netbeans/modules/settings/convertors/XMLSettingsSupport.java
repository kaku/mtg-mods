/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ServiceType
 *  org.openide.filesystems.FileObject
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.Modules
 *  org.openide.modules.SpecificationVersion
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.SharedClassObject
 *  org.openide.util.Utilities
 *  org.openide.util.io.NbObjectOutputStream
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.modules.settings.convertors;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.settings.FactoryMethod;
import org.netbeans.spi.settings.Saver;
import org.openide.ServiceType;
import org.openide.filesystems.FileObject;
import org.openide.modules.ModuleInfo;
import org.openide.modules.Modules;
import org.openide.modules.SpecificationVersion;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.SharedClassObject;
import org.openide.util.Utilities;
import org.openide.util.io.NbObjectOutputStream;
import org.openide.xml.XMLUtil;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

final class XMLSettingsSupport {
    public static final String INSTANCE_DTD_ID = "-//NetBeans//DTD Session settings 1.0//EN";
    public static final String INSTANCE_DTD_WWW = "http://www.netbeans.org/dtds/sessionsettings-1_0.dtd";
    public static final String XML_EXT = "settings";
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");
    static final Logger err = Logger.getLogger(XMLSettingsSupport.class.getName());
    private static final char[] HEXDIGITS = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', '0', '1', '2', '3', '4', '5', '6', '7'};
    private static final int INDENT = 8;
    private static final int BLOCK = 100;
    private static final int BUFFSIZE = 108;

    XMLSettingsSupport() {
    }

    static Object newInstance(Class<?> clazz) throws IllegalArgumentException, InstantiationException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        FactoryMethod fm = clazz.getAnnotation(FactoryMethod.class);
        if (fm != null) {
            Method method;
            try {
                method = clazz.getMethod(fm.value(), new Class[0]);
            }
            catch (NoSuchMethodException ex) {
                method = clazz.getDeclaredMethod(fm.value(), new Class[0]);
            }
            method.setAccessible(true);
            return method.invoke(null, new Object[0]);
        }
        Constructor c = clazz.getDeclaredConstructor(new Class[0]);
        c.setAccessible(true);
        return c.newInstance(new Object[0]);
    }

    private static void storeInstanceOf(Set<Class> classes, PrintWriter pw) throws IOException {
        TreeSet<String> clazzNames = new TreeSet<String>();
        for (Class clz : classes) {
            clazzNames.add(clz.getName());
        }
        StringBuilder sb = new StringBuilder(200);
        for (String s : clazzNames) {
            sb.append("    <instanceof class=\"").append(s).append("\"/>").append(LINE_SEPARATOR);
        }
        pw.print(sb.toString());
    }

    public static void storeToXML10(Object inst, Writer os, ModuleInfo mi) throws IOException {
        PrintWriter pw = new PrintWriter(os);
        pw.println("<?xml version=\"1.0\"?>");
        pw.println("<!DOCTYPE settings PUBLIC \"-//NetBeans//DTD Session settings 1.0//EN\" \"http://www.netbeans.org/dtds/sessionsettings-1_0.dtd\">");
        pw.println("<settings version=\"1.0\">");
        XMLSettingsSupport.storeModule(mi, pw);
        XMLSettingsSupport.storeInstanceOf(XMLSettingsSupport.getSuperClasses(inst.getClass(), null), pw);
        XMLSettingsSupport.storeSerialData(inst, pw);
        pw.println("</settings>");
        pw.flush();
    }

    private static void storeToXML10(Class clazz, Writer os, ModuleInfo mi) throws IOException {
        PrintWriter pw = new PrintWriter(os);
        pw.println("<?xml version=\"1.0\"?>");
        pw.println("<!DOCTYPE settings PUBLIC \"-//NetBeans//DTD Session settings 1.0//EN\" \"http://www.netbeans.org/dtds/sessionsettings-1_0.dtd\">");
        pw.println("<settings version=\"1.0\">");
        XMLSettingsSupport.storeModule(mi, pw);
        XMLSettingsSupport.storeInstanceOf(XMLSettingsSupport.getSuperClasses(clazz, null), pw);
        pw.println("    <instance class=\"" + clazz.getName() + "\"/>");
        pw.println("</settings>");
        pw.flush();
    }

    private static void storeModule(ModuleInfo mi, PrintWriter pw) throws IOException {
        if (mi == null) {
            return;
        }
        String modulName = mi.getCodeName();
        SpecificationVersion spec = mi.getSpecificationVersion();
        StringBuffer sb = new StringBuffer(80);
        sb.append("    <module");
        if (modulName != null && modulName.length() != 0) {
            sb.append(" name=\"").append(modulName).append('\"');
        }
        if (spec != null) {
            sb.append(" spec=\"").append(spec.toString()).append('\"');
        }
        sb.append("/>");
        pw.println(sb.toString());
    }

    private static void storeSerialData(Object inst, PrintWriter pw) throws IOException {
        int i;
        pw.println("    <serialdata class=\"" + inst.getClass().getName() + "\">");
        ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
        SpecialObjectOutputStream oo = new SpecialObjectOutputStream(baos);
        oo.writeObject(inst);
        byte[] bdata = baos.toByteArray();
        char[] cdata = new char[108];
        for (i = 0; i < 8; ++i) {
            cdata[i] = 32;
        }
        i = 0;
        int blen = bdata.length;
        while (i < blen) {
            int j;
            int mark = 8 + Math.min(2 * (blen - i), 100);
            for (j = 8; j < mark; j += 2) {
                int b = bdata[i++] + 256;
                cdata[j] = HEXDIGITS[b >> 4];
                cdata[j + 1] = HEXDIGITS[b & 15];
            }
            pw.write(cdata, 0, j);
            pw.println();
        }
        pw.println("    </serialdata>");
        pw.flush();
    }

    private static Set<Class> getSuperClasses(Class clazz, Set<Class> classes) {
        if (classes == null) {
            classes = new HashSet<Class>();
        }
        if (clazz == null || !classes.add(clazz)) {
            return classes;
        }
        Class<?>[] cs = clazz.getInterfaces();
        if (cs != null) {
            for (int i = 0; i < cs.length; ++i) {
                XMLSettingsSupport.getSuperClasses(cs[i], classes);
            }
        } else {
            err.severe("Error: if you encounter this message, please attach the class name to the issue http://www.netbeans.org/issues/show_bug.cgi?id=16257. Class.getInterfaces() == null for the class: " + clazz);
        }
        return XMLSettingsSupport.getSuperClasses(clazz.getSuperclass(), classes);
    }

    private static Class getServiceTypeClass(Class type) {
        if (!ServiceType.class.isAssignableFrom(type)) {
            throw new IllegalArgumentException();
        }
        while (type.getSuperclass() != ServiceType.class) {
            type = type.getSuperclass();
        }
        return type;
    }

    public static final class Convertor
    extends org.netbeans.spi.settings.Convertor {
        @Override
        public Object read(Reader r) throws IOException, ClassNotFoundException {
            SettingsRecognizer rec = new SettingsRecognizer(false, null);
            rec.parse(r);
            return rec.instanceCreate();
        }

        @Override
        public void registerSaver(Object inst, Saver s) {
        }

        @Override
        public void unregisterSaver(Object inst, Saver s) {
        }

        @Override
        public void write(Writer w, Object inst) throws IOException {
            XMLSettingsSupport.storeToXML10(inst, w, Modules.getDefault().ownerOf(inst.getClass()));
        }
    }

    static final class StopSAXException
    extends SAXException {
        public StopSAXException() {
            super("Parser stopped");
        }
    }

    static final class SettingsRecognizer
    extends DefaultHandler {
        private static final String ELM_SETTING = "settings";
        private static final String ATR_SETTING_VERSION = "version";
        private static final String ELM_MODULE = "module";
        private static final String ATR_MODULE_NAME = "name";
        private static final String ATR_MODULE_SPEC = "spec";
        private static final String ATR_MODULE_IMPL = "impl";
        private static final String ELM_INSTANCE = "instance";
        private static final String ATR_INSTANCE_CLASS = "class";
        private static final String ATR_INSTANCE_METHOD = "method";
        private static final String ELM_INSTANCEOF = "instanceof";
        private static final String ATR_INSTANCEOF_CLASS = "class";
        private static final String ELM_SERIALDATA = "serialdata";
        private static final String ATR_SERIALDATA_CLASS = "class";
        private boolean header;
        private Stack<String> stack;
        private String version;
        private String instanceClass;
        private String instanceMethod;
        private Set<String> instanceOf = new HashSet<String>();
        private byte[] serialdata;
        private CharArrayWriter chaos = null;
        private String codeName;
        private String codeNameBase;
        private int codeNameRelease;
        private SpecificationVersion moduleSpec;
        private String moduleImpl;
        private final FileObject source;
        private static final byte[] MODULE_SETTINGS_INTRO = "<?xml version=\"1.0\"?> <!DOCTYPE settings PUBLIC \"-//NetBeans//DTD Session settings 1.0//EN\" \"http://www.netbeans.org/dtds/sessionsettings-1_0.dtd\"> <settings version=\"".getBytes();
        private static final byte[] MODULE_SETTINGS_INTRO_END = "> <".getBytes();
        private static final byte[] MODULE_SETTINGS_MODULE_NAME = "odule name=\"".getBytes();
        private static final byte[] MODULE_SETTINGS_MODULE_SPEC = "spec=\"".getBytes();
        private static final byte[] MODULE_SETTINGS_MODULE_IMPL = "impl=\"".getBytes();
        private static final byte[] MODULE_SETTINGS_TAG_END = "> <".getBytes();
        private static final byte[] MODULE_SETTINGS_INSTANCE = "nstance".getBytes();
        private static final byte[] MODULE_SETTINGS_INSTANCE_CLZ = "class=\"".getBytes();
        private static final byte[] MODULE_SETTINGS_INSTANCE_MTD = "method=\"".getBytes();
        private static final byte[] MODULE_SETTINGS_OF = "f class=\"".getBytes();
        private static final byte[] MODULE_SETTINGS_SERIAL = "erialdata class=\"".getBytes();
        private static final byte[] MODULE_SETTINGS_END = "settings>".getBytes();

        public SettingsRecognizer(boolean header, FileObject source) {
            this.header = header;
            this.source = source;
        }

        public boolean isAllRead() {
            return !this.header;
        }

        public void setAllRead(boolean all) {
            if (!this.header) {
                return;
            }
            this.header = all;
        }

        public String getSettingsVerison() {
            return this.version;
        }

        public String getCodeName() {
            return this.codeName;
        }

        public String getCodeNameBase() {
            return this.codeNameBase;
        }

        public int getCodeNameRelease() {
            return this.codeNameRelease;
        }

        public SpecificationVersion getSpecificationVersion() {
            return this.moduleSpec;
        }

        public String getModuleImpl() {
            return this.moduleImpl;
        }

        public Set<String> getInstanceOf() {
            return this.instanceOf;
        }

        public String getMethodName() {
            return this.instanceMethod;
        }

        private InputStream getSerializedInstance() {
            if (this.serialdata == null) {
                return null;
            }
            return new ByteArrayInputStream(this.serialdata);
        }

        @Override
        public InputSource resolveEntity(String publicId, String systemId) throws SAXException {
            if ("-//NetBeans//DTD Session settings 1.0//EN".equals(publicId) || "-//NetBeans//DTD XML beans 1.0//EN".equals(publicId)) {
                return new InputSource(new ByteArrayInputStream(new byte[0]));
            }
            return null;
        }

        @Override
        public void characters(char[] values, int start, int length) throws SAXException {
            if (this.header) {
                return;
            }
            String element = this.stack.peek();
            if ("serialdata".equals(element)) {
                if (this.chaos == null) {
                    this.chaos = new CharArrayWriter(length);
                }
                this.chaos.write(values, start, length);
            }
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attribs) throws SAXException {
            this.stack.push(qName);
            if ("settings".equals(qName)) {
                this.version = attribs.getValue("version");
            } else if ("module".equals(qName)) {
                this.codeName = attribs.getValue("name");
                this.resolveModuleElm(this.codeName);
                this.moduleImpl = attribs.getValue("impl");
                try {
                    String spec = attribs.getValue("spec");
                    this.moduleSpec = spec == null ? null : new SpecificationVersion(spec);
                }
                catch (NumberFormatException nfe) {
                    throw new SAXException(nfe);
                }
            } else if ("instanceof".equals(qName)) {
                String instanceOfName = Utilities.translate((String)attribs.getValue("class"));
                if (instanceOfName.length() > 0) {
                    this.instanceOf.add(instanceOfName);
                }
            } else if ("instance".equals(qName)) {
                this.instanceClass = attribs.getValue("class");
                if (this.instanceClass == null) {
                    System.err.println("Hint: NPE is caused by broken settings file: " + (Object)this.source);
                }
                this.instanceClass = Utilities.translate((String)this.instanceClass);
                this.instanceMethod = attribs.getValue("method");
            } else if ("serialdata".equals(qName)) {
                this.instanceClass = attribs.getValue("class");
                this.instanceClass = Utilities.translate((String)this.instanceClass);
                if (this.header) {
                    throw new StopSAXException();
                }
            }
        }

        private void resolveModuleElm(String codeName) {
            if (codeName != null) {
                int slash = codeName.indexOf("/");
                if (slash == -1) {
                    this.codeNameBase = codeName;
                    this.codeNameRelease = -1;
                } else {
                    this.codeNameBase = codeName.substring(0, slash);
                    try {
                        this.codeNameRelease = Integer.parseInt(codeName.substring(slash + 1));
                    }
                    catch (NumberFormatException ex) {
                        Exceptions.attachLocalizedMessage((Throwable)ex, (String)("Content: \n" + SettingsRecognizer.getFileContent(this.source)));
                        Exceptions.attachLocalizedMessage((Throwable)ex, (String)("Source: " + (Object)this.source));
                        Logger.getLogger(XMLSettingsSupport.class.getName()).log(Level.WARNING, null, ex);
                        this.codeNameRelease = -1;
                    }
                }
            } else {
                this.codeNameBase = null;
                this.codeNameRelease = -1;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            String element = this.stack.pop();
            if ("serialdata".equals(element) && this.chaos != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream(this.chaos.size() >> 1);
                try {
                    this.chars2Bytes(baos, this.chaos.toCharArray(), 0, this.chaos.size());
                    this.serialdata = baos.toByteArray();
                }
                catch (IOException ex) {
                    Logger.getLogger(XMLSettingsSupport.class.getName()).log(Level.WARNING, null, ex);
                }
                finally {
                    this.chaos = null;
                    try {
                        baos.close();
                    }
                    catch (IOException ex) {}
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private Object readSerial(InputStream is) throws IOException, ClassNotFoundException {
            Object object;
            if (is == null) {
                return null;
            }
            SpecialObjectInputStream oi = new SpecialObjectInputStream(is);
            try {
                Object o;
                object = o = oi.readObject();
            }
            catch (Throwable var5_7) {
                try {
                    oi.close();
                    throw var5_7;
                }
                catch (IOException ex) {
                    Exceptions.attachLocalizedMessage((Throwable)ex, (String)("Content: \n" + SettingsRecognizer.getFileContent(this.source)));
                    Exceptions.attachLocalizedMessage((Throwable)ex, (String)("Source: " + (Object)this.source));
                    Exceptions.attachLocalizedMessage((Throwable)ex, (String)("Cannot read class: " + this.instanceClass));
                    throw ex;
                }
                catch (ClassNotFoundException ex) {
                    Exceptions.attachLocalizedMessage((Throwable)ex, (String)("Content: \n" + SettingsRecognizer.getFileContent(this.source)));
                    Exceptions.attachLocalizedMessage((Throwable)ex, (String)("Source: " + (Object)this.source));
                    throw ex;
                }
            }
            oi.close();
            return object;
        }

        public Object instanceCreate() throws IOException, ClassNotFoundException {
            Object inst = null;
            inst = this.readSerial(this.getSerializedInstance());
            if (inst == null) {
                if (this.instanceMethod != null) {
                    inst = this.createFromMethod(this.instanceClass, this.instanceMethod);
                } else {
                    Class clazz = this.instanceClass();
                    if (SharedClassObject.class.isAssignableFrom(clazz)) {
                        inst = SharedClassObject.findObject(clazz.asSubclass(SharedClassObject.class), (boolean)false);
                        if (null != inst) {
                            try {
                                Method method = SharedClassObject.class.getDeclaredMethod("reset", new Class[0]);
                                method.setAccessible(true);
                                method.invoke(inst, new Object[0]);
                            }
                            catch (Exception e) {
                                Exceptions.printStackTrace((Throwable)e);
                            }
                        } else {
                            inst = SharedClassObject.findObject(clazz.asSubclass(SharedClassObject.class), (boolean)true);
                        }
                    } else {
                        try {
                            inst = XMLSettingsSupport.newInstance(clazz);
                        }
                        catch (Exception ex) {
                            IOException ioe = new IOException();
                            ioe.initCause(ex);
                            Exceptions.attachMessage((Throwable)ioe, (String)("Content: \n" + SettingsRecognizer.getFileContent(this.source)));
                            Exceptions.attachMessage((Throwable)ioe, (String)("Class: " + clazz));
                            Exceptions.attachMessage((Throwable)ioe, (String)("Source: " + (Object)this.source));
                            throw ioe;
                        }
                    }
                }
            }
            return inst;
        }

        private static String getFileContent(FileObject fo) {
            try {
                int length;
                InputStreamReader isr = new InputStreamReader(fo.getInputStream());
                char[] cbuf = new char[1024];
                StringBuffer sbuf = new StringBuffer(1024);
                while ((length = isr.read(cbuf)) > 0) {
                    sbuf.append(cbuf, 0, length);
                }
                return sbuf.toString();
            }
            catch (Exception ex) {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                return sw.toString();
            }
        }

        private Object createFromMethod(String srcClazz, String srcMethod) throws ClassNotFoundException, IOException {
            String targetClass;
            String targetMethod;
            int dotIndex = this.instanceMethod.lastIndexOf(46);
            if (dotIndex > 0) {
                targetClass = srcMethod.substring(0, dotIndex);
                targetMethod = srcMethod.substring(dotIndex + 1);
            } else {
                targetClass = srcClazz;
                targetMethod = srcMethod;
            }
            Class clazz = this.loadClass(targetClass);
            try {
                Object instance;
                try {
                    Method method = clazz.getDeclaredMethod(targetMethod, FileObject.class);
                    method.setAccessible(true);
                    instance = method.invoke(null, new Object[]{this.source});
                }
                catch (NoSuchMethodException ex) {
                    Method method = clazz.getDeclaredMethod(targetMethod, new Class[0]);
                    method.setAccessible(true);
                    instance = method.invoke(null, new Object[0]);
                }
                if (instance == null) {
                    throw new IOException("Null return not permitted from " + targetClass + "." + targetMethod);
                }
                return instance;
            }
            catch (Exception ex) {
                IOException ioe = new IOException("Error reading " + (Object)this.source + ": " + ex);
                ioe.initCause(ex);
                Exceptions.attachMessage((Throwable)ioe, (String)("Content:\n" + SettingsRecognizer.getFileContent(this.source)));
                Exceptions.attachMessage((Throwable)ioe, (String)("Method: " + srcMethod));
                Exceptions.attachMessage((Throwable)ioe, (String)("Class: " + clazz));
                throw ioe;
            }
        }

        public Class instanceClass() throws IOException, ClassNotFoundException {
            if (this.instanceClass == null) {
                throw new ClassNotFoundException((Object)this.source + ": missing 'class' attribute in 'instance' element");
            }
            return this.loadClass(this.instanceClass);
        }

        private Class<?> loadClass(String clazz) throws ClassNotFoundException {
            return ((ClassLoader)Lookup.getDefault().lookup(ClassLoader.class)).loadClass(clazz);
        }

        public String instanceName() {
            if (this.instanceClass == null) {
                return "";
            }
            return this.instanceClass;
        }

        private int tr(char c) {
            if (c >= '0' && c <= '9') {
                return c - 48;
            }
            if (c >= 'A' && c <= 'F') {
                return c - 65 + 10;
            }
            if (c >= 'a' && c <= 'f') {
                return c - 97 + 10;
            }
            return -1;
        }

        private void chars2Bytes(OutputStream os, char[] chars, int off, int length) throws IOException {
            int i = off;
            block0 : while (i < length) {
                int read;
                if ((read = this.tr(chars[i++])) < 0) continue;
                byte rbyte = (byte)(read << 4);
                while (i < length) {
                    if ((read = this.tr(chars[i++])) < 0) continue;
                    rbyte = (byte)(rbyte + (byte)read);
                    os.write(rbyte);
                    continue block0;
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         * Unable to fully structure code
         * Enabled aggressive block sorting
         * Enabled unnecessary exception pruning
         * Enabled aggressive exception aggregation
         * Lifted jumps to return sites
         */
        public void parse() throws IOException {
            block23 : {
                in = null;
                try {
                    if (this.header) {
                        if (XMLSettingsSupport.err.isLoggable(Level.FINE) && this.source.getSize() < 12000) {
                            arr = new byte[(int)this.source.getSize()];
                            temp = this.source.getInputStream();
                            len = temp.read(arr);
                            if (len != arr.length) {
                                throw new IOException("Could not read " + arr.length + " bytes from " + (Object)this.source + " just " + len);
                            }
                            XMLSettingsSupport.err.finer("Parsing:" + new String(arr));
                            temp.close();
                            in = new ByteArrayInputStream(arr);
                        } else {
                            in = this.source.getInputStream();
                        }
                        iofs = this.quickParse(new BufferedInputStream(in));
                        if (iofs != null) {
                            this.instanceOf = iofs;
                            return;
                        }
                    }
                    break block23;
                }
                catch (IOException ioe) {}
                ** GOTO lbl26
                finally {
                    if (in != null) {
                        in.close();
                    }
                }
            }
            this.stack = new Stack<E>();
            in = this.source.getInputStream();
            reader = XMLUtil.createXMLReader();
            reader.setContentHandler(this);
            reader.setErrorHandler(this);
            reader.setEntityResolver(this);
            reader.parse(new InputSource(new BufferedInputStream(in)));
            this.stack = null;
            try {
                if (in == null) return;
                in.close();
                return;
            }
            catch (IOException ex) {
                return;
            }
            catch (StopSAXException ex) {
                this.stack = null;
                try {
                    if (in == null) return;
                    in.close();
                    return;
                }
                catch (IOException ex) {
                    return;
                }
                catch (SAXException ex2) {
                    try {
                        ioe = new IOException(this.source.toString());
                        ioe.initCause(ex2);
                        Exceptions.attachMessage((Throwable)ioe, (String)("Content: \n" + SettingsRecognizer.getFileContent(this.source)));
                        Exceptions.attachMessage((Throwable)ioe, (String)("Source: " + (Object)this.source));
                        throw ioe;
                    }
                    catch (Throwable throwable) {
                        this.stack = null;
                        try {
                            if (in == null) throw throwable;
                            in.close();
                            throw throwable;
                        }
                        catch (IOException ex) {
                            // empty catch block
                        }
                        throw throwable;
                    }
                }
            }
        }

        public void parse(Reader source) throws IOException {
            this.stack = new Stack();
            try {
                XMLReader reader = XMLUtil.createXMLReader();
                reader.setContentHandler(this);
                reader.setErrorHandler(this);
                reader.setEntityResolver(this);
                reader.parse(new InputSource(source));
            }
            catch (StopSAXException ex) {}
            catch (SAXException ex) {
                IOException ioe = new IOException(source.toString());
                ioe.initCause(ex);
                throw ioe;
            }
            finally {
                this.stack = null;
            }
        }

        /*
         * Unable to fully structure code
         * Enabled aggressive block sorting
         * Enabled unnecessary exception pruning
         * Enabled aggressive exception aggregation
         * Lifted jumps to return sites
         */
        private Set<String> quickParse(InputStream is) throws IOException {
            iofs = new HashSet<String>();
            if (!this.expect(is, SettingsRecognizer.MODULE_SETTINGS_INTRO)) {
                XMLSettingsSupport.err.fine("Could not read intro " + (Object)this.source);
                return null;
            }
            this.version = this.readTo(is, '\"');
            if (this.version == null) {
                XMLSettingsSupport.err.fine("Could not read version " + (Object)this.source);
                return null;
            }
            if (!this.expect(is, SettingsRecognizer.MODULE_SETTINGS_INTRO_END)) {
                XMLSettingsSupport.err.fine("Could not read stuff after cnb " + (Object)this.source);
                return null;
            }
            block8 : do lbl-1000: // 7 sources:
            {
                c = is.read();
                switch (c) {
                    case 109: {
                        if (!this.expect(is, SettingsRecognizer.MODULE_SETTINGS_MODULE_NAME)) {
                            XMLSettingsSupport.err.fine("Could not read up to <module name=\" " + (Object)this.source);
                            return null;
                        }
                        codeName = this.readTo(is, '\"');
                        if (codeName == null) {
                            XMLSettingsSupport.err.fine("Could not read module name value " + (Object)this.source);
                            return null;
                        }
                        codeName = codeName.intern();
                        this.resolveModuleElm(codeName);
                        c = is.read();
                        if (c != 47) ** GOTO lbl30
                        if (this.expect(is, SettingsRecognizer.MODULE_SETTINGS_TAG_END)) ** GOTO lbl-1000
                        XMLSettingsSupport.err.fine("Could not read up to end of module tag " + (Object)this.source);
                        return null;
lbl30: // 1 sources:
                        if (c != 32) {
                            XMLSettingsSupport.err.fine("Could not space after module name " + (Object)this.source);
                            return null;
                        }
                        if (!this.expect(is, SettingsRecognizer.MODULE_SETTINGS_MODULE_SPEC)) {
                            XMLSettingsSupport.err.fine("Could not read up to spec=\" " + (Object)this.source);
                            return null;
                        }
                        mspec = this.readTo(is, '\"');
                        if (mspec == null) {
                            XMLSettingsSupport.err.fine("Could not read module spec value " + (Object)this.source);
                            return null;
                        }
                        try {
                            this.moduleSpec = new SpecificationVersion(mspec);
                        }
                        catch (NumberFormatException nfe) {
                            return null;
                        }
                        c = is.read();
                        if (c != 47) ** GOTO lbl50
                        if (this.expect(is, SettingsRecognizer.MODULE_SETTINGS_TAG_END)) ** GOTO lbl-1000
                        XMLSettingsSupport.err.fine("Could not read up to end of <module name spec/> tag " + (Object)this.source);
                        return null;
lbl50: // 1 sources:
                        if (c != 32) {
                            XMLSettingsSupport.err.fine("Could not read space after module name " + (Object)this.source);
                            return null;
                        }
                        if (!this.expect(is, SettingsRecognizer.MODULE_SETTINGS_MODULE_IMPL)) {
                            XMLSettingsSupport.err.fine("Could not read up to impl=\" " + (Object)this.source);
                            return null;
                        }
                        this.moduleImpl = this.readTo(is, '\"');
                        if (this.moduleImpl == null) {
                            XMLSettingsSupport.err.fine("Could not read module impl value " + (Object)this.source);
                            return null;
                        }
                        this.moduleImpl = this.moduleImpl.intern();
                        if (this.expect(is, SettingsRecognizer.MODULE_SETTINGS_TAG_END)) ** GOTO lbl-1000
                        XMLSettingsSupport.err.fine("Could not read up to /> < " + (Object)this.source);
                        return null;
                    }
                    case 105: {
                        if (!this.expect(is, SettingsRecognizer.MODULE_SETTINGS_INSTANCE)) {
                            XMLSettingsSupport.err.fine("Could not read up to instance " + (Object)this.source);
                            return null;
                        }
                        c = is.read();
                        if (c != 111) ** GOTO lbl85
                        if (!this.expect(is, SettingsRecognizer.MODULE_SETTINGS_OF)) {
                            XMLSettingsSupport.err.fine("Could not read up to instance");
                            return null;
                        }
                        iof = this.readTo(is, '\"');
                        if (iof == null) {
                            XMLSettingsSupport.err.fine("Could not read instanceof value " + (Object)this.source);
                            return null;
                        }
                        iof = Utilities.translate((String)iof).intern();
                        iofs.add(iof);
                        if (is.read() != 47) {
                            XMLSettingsSupport.err.fine("No / at end of <instanceof> " + iof + " " + (Object)this.source);
                            return null;
                        }
                        if (this.expect(is, SettingsRecognizer.MODULE_SETTINGS_TAG_END)) ** GOTO lbl-1000
                        XMLSettingsSupport.err.fine("Could not read up to next tag after <instanceof> " + iof + " " + (Object)this.source);
                        return null;
lbl85: // 1 sources:
                        if (c != 32) {
                            XMLSettingsSupport.err.fine("Could not read after to instance " + (Object)this.source);
                            return null;
                        }
                        if (!this.expect(is, SettingsRecognizer.MODULE_SETTINGS_INSTANCE_CLZ)) {
                            XMLSettingsSupport.err.fine("Could not read up to class=\" " + (Object)this.source);
                            return null;
                        }
                        this.instanceClass = this.readTo(is, '\"');
                        if (this.instanceClass == null) {
                            XMLSettingsSupport.err.fine("Could not read instance class value " + (Object)this.source);
                            return null;
                        }
                        this.instanceClass = Utilities.translate((String)this.instanceClass).intern();
                        c = is.read();
                        if (c != 47) ** GOTO lbl101
                        if (this.expect(is, SettingsRecognizer.MODULE_SETTINGS_TAG_END)) ** GOTO lbl-1000
                        XMLSettingsSupport.err.fine("Could not read up to end of instance tag " + (Object)this.source);
                        return null;
lbl101: // 1 sources:
                        if (c != 32) {
                            XMLSettingsSupport.err.fine("Could not space after instance class " + (Object)this.source);
                            return null;
                        }
                        if (!this.expect(is, SettingsRecognizer.MODULE_SETTINGS_INSTANCE_MTD)) {
                            XMLSettingsSupport.err.fine("Could not read up to method=\" " + (Object)this.source);
                            return null;
                        }
                        this.instanceMethod = this.readTo(is, '\"');
                        if (this.instanceMethod == null) {
                            XMLSettingsSupport.err.fine("Could not read method value " + (Object)this.source);
                            return null;
                        }
                        this.instanceMethod = this.instanceMethod.intern();
                        c = is.read();
                        if (c == 47) continue block8;
                        XMLSettingsSupport.err.fine("Strange stuff after method attribute " + (Object)this.source);
                        return null;
                        if (this.expect(is, SettingsRecognizer.MODULE_SETTINGS_TAG_END)) continue block8;
                        XMLSettingsSupport.err.fine("Could not read up to end of instance tag " + (Object)this.source);
                        return null;
                    }
                    case 115: {
                        if (!this.expect(is, SettingsRecognizer.MODULE_SETTINGS_SERIAL)) {
                            XMLSettingsSupport.err.fine("Could not read up to <serialdata class=\" " + (Object)this.source);
                            return null;
                        }
                        this.instanceClass = this.readTo(is, '\"');
                        if (this.instanceClass == null) {
                            XMLSettingsSupport.err.fine("Could not read serialdata class value " + (Object)this.source);
                            return null;
                        }
                        this.instanceClass = Utilities.translate((String)this.instanceClass).intern();
                        c = is.read();
                        if (c == 62) break block8;
                        XMLSettingsSupport.err.fine("Could not read up to end of serialdata tag " + (Object)this.source);
                        return null;
                    }
                    case 47: {
                        if (this.expect(is, SettingsRecognizer.MODULE_SETTINGS_END)) break block8;
                        XMLSettingsSupport.err.fine("Could not read up to end of settings tag " + (Object)this.source);
                        return null;
                    }
                    default: {
                        XMLSettingsSupport.err.fine("Strange stuff after <" + (char)c + " " + (Object)this.source);
                        return null;
                    }
                }
                break;
            } while (true);
            if (this.instanceClass == null) return null;
            if (iofs.isEmpty() != false) return null;
            return iofs;
        }

        private boolean expect(InputStream is, byte[] stuff) throws IOException {
            int len = stuff.length;
            boolean inWhitespace = false;
            int i = 0;
            while (i < len) {
                int c = is.read();
                if (c == 10 || c == 13 || c == 32 || c == 9) {
                    if (inWhitespace) continue;
                    inWhitespace = true;
                    c = 32;
                } else {
                    inWhitespace = false;
                }
                if (c == stuff[i++]) continue;
                return false;
            }
            if (stuff[len - 1] == 10) {
                if (!is.markSupported()) {
                    throw new IOException("Mark not supported");
                }
                is.mark(1);
                int c = is.read();
                if (c != -1 && c != 10 && c != 13) {
                    is.reset();
                }
            }
            return true;
        }

        private String readTo(InputStream is, char delim) throws IOException {
            if (delim == '\n') {
                throw new IOException("Not implemented");
            }
            CharArrayWriter caw = new CharArrayWriter(100);
            boolean inNewline = false;
            int c;
            while ((c = is.read()) != -1) {
                if (c > 126) {
                    return null;
                }
                if (c == 10 || c == 13) {
                    if (inNewline) continue;
                    inNewline = true;
                    c = 10;
                } else {
                    if (c < 32 && c != 9) {
                        return null;
                    }
                    inNewline = false;
                }
                if (c == delim) {
                    return caw.toString();
                }
                caw.write(c);
            }
            return null;
        }
    }

    private static class SpecialObjectInputStream
    extends ObjectInputStream {
        public SpecialObjectInputStream(InputStream is) throws IOException {
            super(is);
            try {
                this.enableResolveObject(true);
            }
            catch (SecurityException ex) {
                throw new IOException(ex.toString());
            }
        }

        protected Class resolveClass(ObjectStreamClass v) throws IOException, ClassNotFoundException {
            ClassLoader cl = SpecialObjectInputStream.getNBClassLoader();
            try {
                return Class.forName(v.getName(), false, cl);
            }
            catch (ClassNotFoundException cnfe) {
                String msg = "Offending classloader: " + cl;
                Exceptions.attachMessage((Throwable)cnfe, (String)msg);
                throw cnfe;
            }
        }

        @Override
        protected ObjectStreamClass readClassDescriptor() throws IOException, ClassNotFoundException {
            String newN;
            ObjectStreamClass ose = super.readClassDescriptor();
            String name = ose.getName();
            if (name == (newN = Utilities.translate((String)name))) {
                return ose;
            }
            ClassLoader cl = SpecialObjectInputStream.getNBClassLoader();
            try {
                Class origCl = Class.forName(name, false, cl);
                return ObjectStreamClass.lookup(origCl);
            }
            catch (ClassNotFoundException ex) {
                Class clazz = Class.forName(newN, false, cl);
                ObjectStreamClass newOse = ObjectStreamClass.lookup(clazz);
                if (newOse == null) {
                    throw new NotSerializableException(newN);
                }
                return newOse;
            }
        }

        private static ClassLoader getNBClassLoader() {
            ClassLoader c = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
            return c != null ? c : ClassLoader.getSystemClassLoader();
        }
    }

    private static class SpecialObjectOutputStream
    extends NbObjectOutputStream {
        private boolean first = true;

        public SpecialObjectOutputStream(OutputStream os) throws IOException {
            super(os);
        }

        public Object replaceObject(Object obj) throws IOException {
            if (this.first) {
                if (obj == null) {
                    throw new NotSerializableException();
                }
                this.first = false;
            }
            return super.replaceObject(obj);
        }
    }

}

