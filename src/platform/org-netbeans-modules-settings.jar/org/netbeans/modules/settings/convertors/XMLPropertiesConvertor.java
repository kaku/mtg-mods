/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.modules.settings.convertors;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.settings.Env;
import org.netbeans.modules.settings.convertors.XMLSettingsSupport;
import org.netbeans.spi.settings.Convertor;
import org.netbeans.spi.settings.Saver;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.xml.XMLUtil;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.DefaultHandler;

public final class XMLPropertiesConvertor
extends Convertor
implements PropertyChangeListener {
    public static final String EA_PREVENT_STORING = "xmlproperties.preventStoring";
    public static final String EA_IGNORE_CHANGES = "xmlproperties.ignoreChanges";
    private FileObject providerFO;
    private Set ignoreProperites;
    private Saver saver;
    private static final String INDENT = "    ";
    private String instanceClass = null;

    public static Convertor create(FileObject providerFO) {
        return new XMLPropertiesConvertor(providerFO);
    }

    public XMLPropertiesConvertor(FileObject fo) {
        this.providerFO = fo;
    }

    @Override
    public Object read(java.io.Reader r) throws IOException, ClassNotFoundException {
        Object def = this.defaultInstanceCreate();
        return this.readSetting(r, def);
    }

    @Override
    public void write(Writer w, Object inst) throws IOException {
        Object publicId;
        w.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + XMLSettingsSupport.LINE_SEPARATOR);
        w.write("<!DOCTYPE properties PUBLIC \"");
        FileObject foEntity = Env.findEntityRegistration(this.providerFO);
        if (foEntity == null) {
            foEntity = this.providerFO;
        }
        if ((publicId = foEntity.getAttribute("hint.originalPublicID")) == null || !(publicId instanceof String)) {
            throw new IOException("missing or invalid attribute: hint.originalPublicID, provider: " + (Object)foEntity);
        }
        w.write((String)publicId);
        w.write("\" \"http://www.netbeans.org/dtds/properties-1_0.dtd\">" + XMLSettingsSupport.LINE_SEPARATOR);
        w.write("<properties>" + XMLSettingsSupport.LINE_SEPARATOR);
        Properties p = XMLPropertiesConvertor.getProperties(inst);
        if (p != null && !p.isEmpty()) {
            XMLPropertiesConvertor.writeProperties(w, p);
        }
        w.write("</properties>" + XMLSettingsSupport.LINE_SEPARATOR);
    }

    @Override
    public void registerSaver(Object inst, Saver s) {
        if (this.saver != null) {
            XMLSettingsSupport.err.log(Level.WARNING, "Already registered Saver: {0} for settings object: {1}", new Object[]{s.getClass().getCanonicalName(), inst.getClass().getCanonicalName()});
            return;
        }
        try {
            Method method = inst.getClass().getMethod("addPropertyChangeListener", PropertyChangeListener.class);
            method.invoke(inst, this);
            this.saver = s;
        }
        catch (NoSuchMethodException ex) {
            XMLSettingsSupport.err.warning("ObjectChangesNotifier: NoSuchMethodException: " + inst.getClass().getName() + ".addPropertyChangeListener");
        }
        catch (IllegalAccessException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (InvocationTargetException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    @Override
    public void unregisterSaver(Object inst, Saver s) {
        if (this.saver == null) {
            return;
        }
        if (this.saver != s) {
            XMLSettingsSupport.err.log(Level.WARNING, "Unregistering unknown Saver: {0} for settings object: {1}", new Object[]{s.getClass().getCanonicalName(), inst.getClass().getCanonicalName()});
            return;
        }
        try {
            Method method = inst.getClass().getMethod("removePropertyChangeListener", PropertyChangeListener.class);
            method.invoke(inst, this);
            this.saver = null;
        }
        catch (NoSuchMethodException ex) {
            XMLSettingsSupport.err.fine("ObjectChangesNotifier: NoSuchMethodException: " + inst.getClass().getName() + ".removePropertyChangeListener");
        }
        catch (IllegalAccessException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (InvocationTargetException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (this.saver == null || this.ignoreChange(evt)) {
            return;
        }
        if (this.acceptSave()) {
            try {
                this.saver.requestSave();
            }
            catch (IOException ex) {
                Logger.getLogger(XMLPropertiesConvertor.class.getName()).log(Level.WARNING, null, ex);
            }
        } else {
            this.saver.markDirty();
        }
    }

    private boolean ignoreChange(PropertyChangeEvent pce) {
        if (pce == null || pce.getPropertyName() == null) {
            return true;
        }
        if (this.ignoreProperites == null) {
            this.ignoreProperites = Env.parseAttribute(this.providerFO.getAttribute("xmlproperties.ignoreChanges"));
        }
        if (this.ignoreProperites.contains(pce.getPropertyName())) {
            return true;
        }
        return this.ignoreProperites.contains("all");
    }

    private boolean acceptSave() {
        Object storing = this.providerFO.getAttribute("xmlproperties.preventStoring");
        if (storing == null) {
            return true;
        }
        if (storing instanceof Boolean) {
            return (Boolean)storing == false;
        }
        if (storing instanceof String) {
            return Boolean.valueOf((String)storing) == false;
        }
        return true;
    }

    private Object defaultInstanceCreate() throws IOException, ClassNotFoundException {
        Object instanceCreate = this.providerFO.getAttribute("settings.instanceCreate");
        if (instanceCreate != null) {
            return instanceCreate;
        }
        Class c = this.getInstanceClass();
        try {
            return XMLSettingsSupport.newInstance(c);
        }
        catch (Exception ex) {
            IOException ioe = new IOException("Cannot create instance of " + c.getName());
            ioe.initCause(ex);
            throw ioe;
        }
    }

    private Class getInstanceClass() throws IOException, ClassNotFoundException {
        if (this.instanceClass == null) {
            Object name = this.providerFO.getAttribute("settings.instanceClass");
            if (name == null || !(name instanceof String)) {
                throw new IllegalStateException("missing or invalid ea attribute: settings.instanceClass");
            }
            this.instanceClass = (String)name;
        }
        return ((ClassLoader)Lookup.getDefault().lookup(ClassLoader.class)).loadClass(this.instanceClass);
    }

    private Object readSetting(java.io.Reader input, Object inst) throws IOException {
        try {
            Method m = inst.getClass().getDeclaredMethod("readProperties", Properties.class);
            m.setAccessible(true);
            Reader r = new Reader();
            r.parse(input);
            m.setAccessible(true);
            Object ret = m.invoke(inst, r.getProperties());
            if (ret == null) {
                ret = inst;
            }
            return ret;
        }
        catch (NoSuchMethodException ex) {
            IOException ioe = new IOException(ex.getMessage());
            ioe.initCause(ex);
            throw ioe;
        }
        catch (IllegalAccessException ex) {
            IOException ioe = new IOException(ex.getMessage());
            ioe.initCause(ex);
            throw ioe;
        }
        catch (InvocationTargetException ex) {
            Throwable t = ex.getTargetException();
            IOException ioe = new IOException(ex.getMessage());
            ioe.initCause(t);
            throw ioe;
        }
    }

    private static void writeProperties(Writer w, Properties p) throws IOException {
        for (String key : p.keySet()) {
            w.write("    ");
            w.write("<property name=\"");
            w.write(key);
            w.write("\" value=\"");
            w.write(XMLUtil.toAttributeValue((String)p.getProperty(key)));
            w.write("\"/>" + XMLSettingsSupport.LINE_SEPARATOR);
        }
    }

    private static Properties getProperties(Object inst) throws IOException {
        try {
            Method m = inst.getClass().getDeclaredMethod("writeProperties", Properties.class);
            m.setAccessible(true);
            Properties prop = new Properties();
            m.invoke(inst, prop);
            return prop;
        }
        catch (NoSuchMethodException ex) {
            IOException ioe = new IOException(ex.getMessage());
            ioe.initCause(ex);
            throw ioe;
        }
        catch (IllegalAccessException ex) {
            IOException ioe = new IOException(ex.getMessage());
            ioe.initCause(ex);
            throw ioe;
        }
        catch (InvocationTargetException ex) {
            Throwable t = ex.getTargetException();
            IOException ioe = new IOException(ex.getMessage());
            ioe.initCause(t);
            throw ioe;
        }
    }

    private static class Reader
    extends DefaultHandler
    implements LexicalHandler {
        private static final String ELM_PROPERTY = "property";
        private static final String ATR_PROPERTY_NAME = "name";
        private static final String ATR_PROPERTY_VALUE = "value";
        private Properties props = new Properties();
        private String publicId;

        Reader() {
        }

        @Override
        public InputSource resolveEntity(String publicId, String systemId) throws SAXException {
            if (this.publicId != null && this.publicId.equals(publicId)) {
                return new InputSource(new ByteArrayInputStream(new byte[0]));
            }
            return null;
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attribs) throws SAXException {
            if ("property".equals(qName)) {
                String propertyName = attribs.getValue("name");
                String propertyValue = attribs.getValue("value");
                this.props.setProperty(propertyName, propertyValue);
            }
        }

        public void parse(java.io.Reader src) throws IOException {
            try {
                XMLReader reader = XMLUtil.createXMLReader((boolean)false, (boolean)false);
                reader.setContentHandler(this);
                reader.setEntityResolver(this);
                InputSource is = new InputSource(src);
                try {
                    reader.setProperty("http://xml.org/sax/properties/lexical-handler", this);
                }
                catch (SAXException sex) {
                    XMLSettingsSupport.err.warning("Warning: XML parser does not support lexical-handler feature.");
                }
                reader.parse(is);
            }
            catch (SAXException ex) {
                IOException ioe = new IOException();
                ioe.initCause(ex);
                throw ioe;
            }
        }

        public Properties getProperties() {
            return this.props;
        }

        public String getPublicID() {
            return this.publicId;
        }

        @Override
        public void startDTD(String name, String publicId, String systemId) throws SAXException {
            this.publicId = publicId;
        }

        @Override
        public void endDTD() throws SAXException {
        }

        @Override
        public void startEntity(String str) throws SAXException {
        }

        @Override
        public void endEntity(String str) throws SAXException {
        }

        @Override
        public void comment(char[] values, int param, int param2) throws SAXException {
        }

        @Override
        public void startCDATA() throws SAXException {
        }

        @Override
        public void endCDATA() throws SAXException {
        }
    }

}

