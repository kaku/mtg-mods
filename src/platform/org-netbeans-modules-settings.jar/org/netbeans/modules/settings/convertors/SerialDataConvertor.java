/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.InstanceCookie$Of
 *  org.openide.cookies.SaveCookie
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.Environment
 *  org.openide.loaders.Environment$Provider
 *  org.openide.loaders.InstanceDataObject
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.Modules
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.SharedClassObject
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.settings.convertors;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import org.netbeans.modules.settings.Env;
import org.netbeans.modules.settings.ScheduledRequest;
import org.netbeans.modules.settings.convertors.ModuleInfoManager;
import org.netbeans.modules.settings.convertors.SerialDataNode;
import org.netbeans.modules.settings.convertors.XMLSettingsSupport;
import org.netbeans.spi.settings.Convertor;
import org.netbeans.spi.settings.Saver;
import org.openide.cookies.InstanceCookie;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.Environment;
import org.openide.loaders.InstanceDataObject;
import org.openide.modules.ModuleInfo;
import org.openide.modules.Modules;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.SharedClassObject;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.windows.TopComponent;

public final class SerialDataConvertor
extends FileChangeAdapter
implements PropertyChangeListener,
FileSystem.AtomicAction {
    static final String EA_NAME = "name";
    final Object READWRITE_LOCK = new Object();
    private final InstanceContent lkpContent;
    private final Lookup lookup;
    private final DataObject dobj;
    private final FileObject provider;
    private final NodeConvertor node;
    private SettingsInstance instance;
    private SaveSupport saver;
    private String moduleCodeBase = null;
    private boolean miUnInitialized = true;
    private boolean moduleMissing;
    private ScheduledRequest request;

    public SerialDataConvertor(DataObject dobj, FileObject provider) {
        this.dobj = dobj;
        this.provider = provider;
        this.lkpContent = new InstanceContent();
        FileObject fo = dobj.getPrimaryFile();
        fo.addFileChangeListener(FileUtil.weakFileChangeListener((FileChangeListener)this, (Object)fo));
        SettingsInstance si = this.createInstance(null);
        if (this.isModuleEnabled(si) && si.instanceName().length() > 0) {
            this.instance = si;
            this.lkpContent.add((Object)this.instance);
        }
        this.lkpContent.add((Object)this);
        this.node = new NodeConvertor();
        this.lkpContent.add((Object)this, (InstanceContent.Convertor)this.node);
        this.lookup = new AbstractLookup((AbstractLookup.Content)this.lkpContent);
    }

    public void write(Writer w, Object inst) throws IOException {
        XMLSettingsSupport.storeToXML10(inst, w, Modules.getDefault().ownerOf(inst.getClass()));
    }

    void handleUnfiredChange() {
        this.saver.propertyChange(null);
    }

    DataObject getDataObject() {
        return this.dobj;
    }

    FileObject getProvider() {
        return this.provider;
    }

    public final Lookup getLookup() {
        return this.lookup;
    }

    private SettingsInstance createInstance(Object inst) {
        return new SettingsInstance(inst);
    }

    private SaveSupport createSaveSupport(Object inst) {
        return new SaveSupport(inst);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void attachToInstance(Object inst) {
        SaveSupport _saver = null;
        SerialDataConvertor serialDataConvertor = this;
        synchronized (serialDataConvertor) {
            if (this.saver != null) {
                this.saver.removePropertyChangeListener(this);
                _saver = this.saver;
            }
        }
        if (_saver != null) {
            _saver.flush();
        }
        serialDataConvertor = this;
        synchronized (serialDataConvertor) {
            this.saver = this.createSaveSupport(inst);
            this.saver.addPropertyChangeListener(this);
        }
    }

    private void provideSaveCookie() {
        if (this.saver.isChanged()) {
            this.lkpContent.add((Object)this.saver);
        } else {
            this.lkpContent.remove((Object)this.saver);
        }
    }

    private void instanceCookieChanged(Object inst) {
        if (XMLSettingsSupport.err.isLoggable(Level.FINE)) {
            XMLSettingsSupport.err.fine("instanceCookieChanged: " + (Object)this.dobj);
        }
        if (this.saver != null) {
            if (XMLSettingsSupport.err.isLoggable(Level.FINE)) {
                XMLSettingsSupport.err.fine("canceling saver: " + (Object)this.dobj);
            }
            this.saver.removePropertyChangeListener(this);
            this.getScheduledRequest().cancel();
            this.saver = null;
        }
        SettingsInstance si = this.createInstance(inst);
        boolean recreate = false;
        if (this.instance != null && this.instance.getCachedInstance() != null && SerialDataConvertor.isSystemOption(this.instance.getCachedInstance())) {
            recreate = true;
        }
        if (XMLSettingsSupport.err.isLoggable(Level.FINE)) {
            XMLSettingsSupport.err.fine("need recreate: " + recreate + " for " + (Object)this.dobj);
        }
        if (this.isModuleEnabled(si) && si.instanceName().length() > 0) {
            this.instance = si;
            this.lkpContent.set(Arrays.asList(this, si), null);
            if (XMLSettingsSupport.err.isLoggable(Level.FINE)) {
                XMLSettingsSupport.err.fine("module enabled: " + (Object)this.dobj);
            }
        } else {
            this.lkpContent.set(Collections.singleton(this), null);
            this.instance = null;
            if (XMLSettingsSupport.err.isLoggable(Level.FINE)) {
                XMLSettingsSupport.err.fine("module disabled: " + (Object)this.dobj + "  or instanceName is empty: " + si.instanceName());
            }
        }
        this.lkpContent.add((Object)this, (InstanceContent.Convertor)this.node);
        if (this.isModuleEnabled(si) && recreate) {
            if (XMLSettingsSupport.err.isLoggable(Level.FINE)) {
                XMLSettingsSupport.err.fine("recreating: " + (Object)this.dobj);
            }
            try {
                if (this.instance != null) {
                    this.instance.instanceCreate();
                }
            }
            catch (Exception ex) {
                XMLSettingsSupport.err.log(Level.WARNING, null, ex);
            }
        }
        if (XMLSettingsSupport.err.isLoggable(Level.FINE)) {
            XMLSettingsSupport.err.fine("done: " + (Object)this.dobj);
        }
    }

    private static boolean isSystemOption(Object obj) {
        boolean b = false;
        if (obj != null && obj instanceof SharedClassObject) {
            for (Class c = obj.getClass(); !b && c != null; c = c.getSuperclass()) {
                b = "org.openide.options.SystemOption".equals(c.getName());
            }
        }
        return b;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt == null) {
            return;
        }
        String name = evt.getPropertyName();
        if (name == null) {
            return;
        }
        if (name == "savecookie") {
            this.provideSaveCookie();
        } else if (name == "fileChanged") {
            this.miUnInitialized = true;
            if (this.moduleCodeBase != null) {
                ModuleInfo mi = ModuleInfoManager.getDefault().getModule(this.moduleCodeBase);
                ModuleInfoManager.getDefault().unregisterPropertyChangeListener(this, mi);
            }
            this.instanceCookieChanged(null);
        } else if ("enabled".equals(evt.getPropertyName())) {
            this.instanceCookieChanged(null);
        }
    }

    public void fileChanged(FileEvent fe) {
        if (this.saver != null && fe.firedFrom((FileSystem.AtomicAction)this.saver)) {
            return;
        }
        this.propertyChange(new PropertyChangeEvent(this, "fileChanged", null, null));
    }

    public void fileDeleted(FileEvent fe) {
        if (this.saver != null && fe.firedFrom((FileSystem.AtomicAction)this.saver)) {
            return;
        }
        if (this.saver != null) {
            this.saver.removePropertyChangeListener(this);
            this.getScheduledRequest().cancel();
            this.saver = null;
        }
    }

    private boolean isModuleEnabled(SettingsInstance si) {
        ModuleInfo mi = null;
        if (this.miUnInitialized) {
            this.moduleCodeBase = this.getModuleCodeNameBase(si);
            this.miUnInitialized = false;
            if (this.moduleCodeBase != null) {
                mi = ModuleInfoManager.getDefault().getModule(this.moduleCodeBase);
                boolean bl = this.moduleMissing = mi == null;
                if (mi != null) {
                    ModuleInfoManager.getDefault().registerPropertyChangeListener(this, mi);
                } else {
                    XMLSettingsSupport.err.warning("Warning: unknown module code base: " + this.moduleCodeBase + " in " + (Object)this.getDataObject().getPrimaryFile());
                }
            } else {
                this.moduleMissing = false;
            }
        } else {
            mi = ModuleInfoManager.getDefault().getModule(this.moduleCodeBase);
        }
        return !this.moduleMissing && (mi == null || mi.isEnabled());
    }

    private String getModuleCodeNameBase(SettingsInstance si) {
        try {
            String module = si.getSettings(true).getCodeNameBase();
            return module;
        }
        catch (IOException ex) {
            XMLSettingsSupport.err.log(Level.WARNING, null, ex);
            return null;
        }
    }

    static void inform(Throwable t) {
        XMLSettingsSupport.err.log(Level.WARNING, null, t);
    }

    public void run() throws IOException {
        if (this.saver != null) {
            this.saver.writeDown();
        }
    }

    private synchronized ScheduledRequest getScheduledRequest() {
        if (this.request == null) {
            this.request = new ScheduledRequest(this.getDataObject().getPrimaryFile(), this);
        }
        return this.request;
    }

    private static final class NodeConvertor
    implements InstanceContent.Convertor<SerialDataConvertor, Node> {
        NodeConvertor() {
        }

        public Node convert(SerialDataConvertor o) {
            return new SerialDataNode(o);
        }

        public Class<Node> type(SerialDataConvertor o) {
            return Node.class;
        }

        public String id(SerialDataConvertor o) {
            return o.toString();
        }

        public String displayName(SerialDataConvertor o) {
            return o.toString();
        }
    }

    public static final class Provider
    implements Environment.Provider {
        private final FileObject providerFO;

        public static Environment.Provider create(FileObject fo) {
            return new Provider(fo);
        }

        private Provider(FileObject fo) {
            this.providerFO = fo;
        }

        public Lookup getEnvironment(DataObject dobj) {
            if (!(dobj instanceof InstanceDataObject)) {
                return Lookup.EMPTY;
            }
            return new SerialDataConvertor(dobj, this.providerFO).getLookup();
        }
    }

    private final class SaveSupport
    implements FileSystem.AtomicAction,
    SaveCookie,
    PropertyChangeListener,
    Saver {
        public static final String PROP_SAVE = "savecookie";
        public static final String PROP_FILE_CHANGED = "fileChanged";
        private PropertyChangeSupport changeSupport;
        private int propertyChangeListenerCount;
        private boolean isChanged;
        private final FileObject file;
        private final WeakReference<Object> instance;
        private Boolean knownToBeTemplate;
        private boolean isWriting;
        private Convertor convertor;
        private ByteArrayOutputStream buf;

        public SaveSupport(Object inst) {
            this.propertyChangeListenerCount = 0;
            this.isChanged = false;
            this.knownToBeTemplate = null;
            this.isWriting = false;
            this.instance = new WeakReference<Object>(inst);
            this.file = SerialDataConvertor.this.getDataObject().getPrimaryFile();
        }

        public final boolean isChanged() {
            return this.isChanged;
        }

        private boolean acceptSave() {
            Object inst = this.instance.get();
            if (inst == null || !(inst instanceof Serializable) || inst instanceof TopComponent) {
                return false;
            }
            return true;
        }

        private boolean ignoreChange(PropertyChangeEvent pce) {
            if (this.isChanged || this.isWriting || !SerialDataConvertor.this.getDataObject().isValid()) {
                return true;
            }
            if (pce != null && Boolean.FALSE.equals(pce.getPropagationId())) {
                return true;
            }
            if (this.knownToBeTemplate == null) {
                this.knownToBeTemplate = SerialDataConvertor.this.getDataObject().isTemplate() ? Boolean.TRUE : Boolean.FALSE;
            }
            return this.knownToBeTemplate;
        }

        private Convertor getConvertor() {
            return this.convertor;
        }

        private Convertor initConvertor() {
            Object inst = this.instance.get();
            if (inst == null) {
                throw new IllegalStateException("setting object cannot be null: " + (Object)SerialDataConvertor.this.getDataObject());
            }
            try {
                FileObject newProviderFO = Env.findProvider(inst.getClass());
                if (newProviderFO != null) {
                    Object attrb;
                    FileObject foEntity = Env.findEntityRegistration(newProviderFO);
                    if (foEntity == null) {
                        foEntity = newProviderFO;
                    }
                    if ((attrb = foEntity.getAttribute("hint.originalPublicID")) == null || !(attrb instanceof String)) {
                        throw new IOException("missing or invalid attribute: hint.originalPublicID, provider: " + (Object)foEntity);
                    }
                    if ("-//NetBeans//DTD Session settings 1.0//EN".equals(attrb)) {
                        this.convertor = null;
                        return this.convertor;
                    }
                    attrb = newProviderFO.getAttribute("settings.convertor");
                    if (attrb == null || !(attrb instanceof Convertor)) {
                        throw new IOException("cannot create convertor: " + attrb + ", provider: " + (Object)newProviderFO);
                    }
                    this.convertor = (Convertor)attrb;
                    return this.convertor;
                }
            }
            catch (IOException ex) {
                SerialDataConvertor.inform(ex);
            }
            return this.convertor;
        }

        public synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
            if (this.changeSupport == null || this.propertyChangeListenerCount <= 0) {
                Convertor conv;
                Object inst = this.instance.get();
                if (inst == null) {
                    return;
                }
                if (this.changeSupport == null) {
                    this.changeSupport = new PropertyChangeSupport(this);
                    this.propertyChangeListenerCount = 0;
                }
                if ((conv = this.initConvertor()) != null) {
                    conv.registerSaver(inst, this);
                } else {
                    this.registerPropertyChangeListener(inst);
                }
            }
            ++this.propertyChangeListenerCount;
            this.changeSupport.addPropertyChangeListener(listener);
        }

        public synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
            if (this.changeSupport == null) {
                return;
            }
            --this.propertyChangeListenerCount;
            this.changeSupport.removePropertyChangeListener(listener);
            if (this.propertyChangeListenerCount == 0) {
                Object inst = this.instance.get();
                if (inst == null) {
                    return;
                }
                Convertor conv = this.getConvertor();
                if (conv != null) {
                    conv.unregisterSaver(inst, this);
                } else {
                    this.unregisterPropertyChangeListener(inst);
                }
            }
        }

        private void registerPropertyChangeListener(Object inst) {
            if (inst instanceof SharedClassObject) {
                ((SharedClassObject)inst).addPropertyChangeListener((PropertyChangeListener)this);
            } else if (inst instanceof JComponent) {
                ((JComponent)inst).addPropertyChangeListener(this);
            } else {
                try {
                    Method method = inst.getClass().getMethod("addPropertyChangeListener", PropertyChangeListener.class);
                    method.invoke(inst, this);
                }
                catch (NoSuchMethodException ex) {
                    if (XMLSettingsSupport.err.isLoggable(Level.FINE)) {
                        XMLSettingsSupport.err.warning("NoSuchMethodException: " + inst.getClass().getName() + ".addPropertyChangeListener");
                    }
                }
                catch (IllegalAccessException ex) {
                    XMLSettingsSupport.err.warning("Instance: " + inst);
                    XMLSettingsSupport.err.log(Level.WARNING, null, ex);
                }
                catch (InvocationTargetException ex) {
                    XMLSettingsSupport.err.log(Level.WARNING, null, ex.getTargetException());
                }
            }
        }

        private void unregisterPropertyChangeListener(Object inst) {
            try {
                Method method = inst.getClass().getMethod("removePropertyChangeListener", PropertyChangeListener.class);
                method.invoke(inst, this);
            }
            catch (NoSuchMethodException ex) {
                if (XMLSettingsSupport.err.isLoggable(Level.FINE)) {
                    XMLSettingsSupport.err.log(Level.FINE, "NoSuchMethodException: " + inst.getClass().getName() + ".removePropertyChangeListener");
                }
            }
            catch (IllegalAccessException ex) {
                XMLSettingsSupport.err.log(Level.WARNING, "Instance: " + inst);
                XMLSettingsSupport.err.log(Level.WARNING, null, ex);
            }
            catch (InvocationTargetException ex) {
                XMLSettingsSupport.err.log(Level.WARNING, null, ex.getTargetException());
            }
        }

        private void firePropertyChange(String name) {
            if (this.changeSupport != null) {
                this.changeSupport.firePropertyChange(name, null, null);
            }
        }

        public void flush() {
            SerialDataConvertor.this.getScheduledRequest().forceToFinish();
        }

        @Override
        public final void propertyChange(PropertyChangeEvent pce) {
            if (this.ignoreChange(pce)) {
                return;
            }
            this.isChanged = true;
            this.firePropertyChange("savecookie");
            if (this.acceptSave()) {
                SerialDataConvertor.this.getScheduledRequest().schedule(this.instance.get());
            }
        }

        @Override
        public void markDirty() {
            if (this.ignoreChange(null)) {
                return;
            }
            this.isChanged = true;
            this.firePropertyChange("savecookie");
        }

        @Override
        public void requestSave() throws IOException {
            if (this.ignoreChange(null)) {
                return;
            }
            this.isChanged = true;
            this.firePropertyChange("savecookie");
            SerialDataConvertor.this.getScheduledRequest().schedule(this.instance.get());
        }

        public void run() throws IOException {
            if (!SerialDataConvertor.this.getDataObject().isValid()) {
                if (XMLSettingsSupport.err.isLoggable(Level.FINE)) {
                    XMLSettingsSupport.err.fine("invalid data object cannot be used for storing " + (Object)SerialDataConvertor.this.getDataObject());
                }
                return;
            }
            try {
                this.try2run();
            }
            catch (IOException ex) {
                if (SerialDataConvertor.this.getDataObject().isValid()) {
                    throw ex;
                }
                return;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void try2run() throws IOException {
            Object object = SerialDataConvertor.this.READWRITE_LOCK;
            synchronized (object) {
                FileLock lock;
                if (XMLSettingsSupport.err.isLoggable(Level.FINER)) {
                    XMLSettingsSupport.err.finer("saving " + (Object)SerialDataConvertor.this.getDataObject());
                }
                if ((lock = SerialDataConvertor.this.getScheduledRequest().getFileLock()) == null) {
                    return;
                }
                OutputStream los = this.file.getOutputStream(lock);
                BufferedOutputStream os = new BufferedOutputStream(los, 1024);
                try {
                    this.buf.writeTo(os);
                    if (XMLSettingsSupport.err.isLoggable(Level.FINER)) {
                        XMLSettingsSupport.err.finer("saved " + (Object)SerialDataConvertor.this.dobj);
                    }
                }
                finally {
                    os.close();
                }
            }
        }

        public void save() throws IOException {
            if (!this.isChanged) {
                return;
            }
            SerialDataConvertor.this.getScheduledRequest().runAndWait();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void writeDown() throws IOException {
            ByteArrayOutputStream b;
            block6 : {
                Object inst = this.instance.get();
                if (inst == null) {
                    return;
                }
                b = new ByteArrayOutputStream(1024);
                OutputStreamWriter w = new OutputStreamWriter((OutputStream)b, "UTF-8");
                try {
                    this.isWriting = true;
                    Convertor conv = this.getConvertor();
                    if (conv != null) {
                        conv.write(w, inst);
                        break block6;
                    }
                    SerialDataConvertor.this.write(w, inst);
                }
                finally {
                    w.close();
                    this.isWriting = false;
                }
            }
            this.isChanged = false;
            this.buf = b;
            this.file.getFileSystem().runAtomicAction((FileSystem.AtomicAction)this);
            this.buf = null;
            if (!this.isChanged) {
                this.firePropertyChange("savecookie");
            }
        }
    }

    private final class SettingsInstance
    implements InstanceCookie.Of {
        private SoftReference<Object> inst;
        private XMLSettingsSupport.SettingsRecognizer settings;

        public SettingsInstance(Object instance) {
            this.settings = null;
            this.setCachedInstance(instance);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private XMLSettingsSupport.SettingsRecognizer getSettings(boolean header) throws IOException {
            SettingsInstance settingsInstance = this;
            synchronized (settingsInstance) {
                if (this.settings == null) {
                    Object object = SerialDataConvertor.this.READWRITE_LOCK;
                    synchronized (object) {
                        this.settings = new XMLSettingsSupport.SettingsRecognizer(header, SerialDataConvertor.this.getDataObject().getPrimaryFile());
                        this.settings.parse();
                    }
                    return this.settings;
                }
                if (!header && !this.settings.isAllRead()) {
                    this.settings.setAllRead(false);
                    this.settings.parse();
                }
                return this.settings;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Object instanceCreate() throws IOException, ClassNotFoundException {
            Object inst;
            SettingsInstance settingsInstance = this;
            synchronized (settingsInstance) {
                inst = this.getCachedInstance();
                if (inst != null) {
                    if (XMLSettingsSupport.err.isLoggable(Level.FINE)) {
                        XMLSettingsSupport.err.fine("Cached instance1: " + inst);
                    }
                    return inst;
                }
            }
            XMLSettingsSupport.SettingsRecognizer recog = this.getSettings(false);
            inst = recog.instanceCreate();
            settingsInstance = this;
            synchronized (settingsInstance) {
                Object existing = this.getCachedInstance();
                if (existing != null) {
                    if (XMLSettingsSupport.err.isLoggable(Level.FINER)) {
                        XMLSettingsSupport.err.finer("Cached instance2: " + existing);
                    }
                    return existing;
                }
                this.setCachedInstance(inst);
            }
            if (XMLSettingsSupport.err.isLoggable(Level.FINER)) {
                XMLSettingsSupport.err.finer("Attached to instance: " + inst);
            }
            SerialDataConvertor.this.attachToInstance(inst);
            return inst;
        }

        public Class instanceClass() throws IOException, ClassNotFoundException {
            Object inst = this.getCachedInstance();
            if (inst != null) {
                return inst.getClass();
            }
            XMLSettingsSupport.SettingsRecognizer recog = this.getSettings(false);
            return recog.instanceClass();
        }

        public boolean instanceOf(Class<?> type) {
            try {
                if (SerialDataConvertor.this.moduleCodeBase != null && ModuleInfoManager.getDefault().isReloaded(SerialDataConvertor.this.moduleCodeBase) && type.getClassLoader() != ClassLoader.getSystemClassLoader() && type.getClassLoader() != null) {
                    ModuleInfo info = ModuleInfoManager.getDefault().getModule(SerialDataConvertor.this.moduleCodeBase);
                    if (info == null || !info.isEnabled()) {
                        return false;
                    }
                    Class instanceType = this.instanceClass();
                    return type.isAssignableFrom(instanceType);
                }
                Object inst = this.getCachedInstance();
                if (inst != null) {
                    return type.isInstance(inst);
                }
                return this.getSettings(true).getInstanceOf().contains(type.getName());
            }
            catch (ClassNotFoundException ex) {
                XMLSettingsSupport.err.log(Level.WARNING, SerialDataConvertor.this.getDataObject().getPrimaryFile().toString());
                SerialDataConvertor.inform(ex);
            }
            catch (IOException ex) {
                XMLSettingsSupport.err.log(Level.WARNING, SerialDataConvertor.this.getDataObject().getPrimaryFile().toString());
                SerialDataConvertor.inform(ex);
            }
            return false;
        }

        public String instanceName() {
            Object inst = this.getCachedInstance();
            if (inst != null) {
                return inst.getClass().getName();
            }
            try {
                return this.getSettings(true).instanceName();
            }
            catch (IOException ex) {
                XMLSettingsSupport.err.warning(SerialDataConvertor.this.getDataObject().getPrimaryFile().toString());
                SerialDataConvertor.inform(ex);
                return "";
            }
        }

        private Object getCachedInstance() {
            return this.inst.get();
        }

        private void setCachedInstance(Object o) {
            this.inst = new SoftReference<Object>(o);
            this.settings = null;
        }

        public void setInstance(Object inst, boolean save) throws IOException {
            SerialDataConvertor.this.instanceCookieChanged(inst);
            if (inst != null) {
                SerialDataConvertor.this.attachToInstance(inst);
                if (save) {
                    SerialDataConvertor.this.getScheduledRequest().runAndWait();
                }
            }
        }
    }

}

