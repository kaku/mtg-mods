/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.actions.ToolsAction
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.InstanceCookie$Of
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$Status
 *  org.openide.loaders.DataNode
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.InstanceDataObject
 *  org.openide.nodes.BeanChildren
 *  org.openide.nodes.BeanChildren$Factory
 *  org.openide.nodes.BeanNode
 *  org.openide.nodes.BeanNode$Descriptor
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.nodes.Node$IndexedProperty
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.util.SharedClassObject
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.SystemAction
 */
package org.netbeans.modules.settings.convertors;

import java.awt.Image;
import java.beans.BeanDescriptor;
import java.beans.BeanInfo;
import java.beans.EventSetDescriptor;
import java.beans.IntrospectionException;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.beans.beancontext.BeanContext;
import java.beans.beancontext.BeanContextChild;
import java.beans.beancontext.BeanContextMembershipEvent;
import java.beans.beancontext.BeanContextMembershipListener;
import java.beans.beancontext.BeanContextProxy;
import java.beans.beancontext.BeanContextSupport;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.Icon;
import org.netbeans.modules.settings.convertors.SerialDataConvertor;
import org.netbeans.modules.settings.convertors.XMLSettingsSupport;
import org.openide.actions.ToolsAction;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.loaders.InstanceDataObject;
import org.openide.nodes.BeanChildren;
import org.openide.nodes.BeanNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.SharedClassObject;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.actions.SystemAction;

public final class SerialDataNode
extends DataNode {
    private PropL propertyChangeListener = null;
    private boolean noBeanInfo = false;
    private final SerialDataConvertor convertor;
    private WeakReference<Object> settingInstance = new WeakReference<Object>(null);
    private boolean isNameChanged = false;
    private final Boolean isAfterNodeConstruction;
    static final String ATTR_DISPLAY_NAME = "SerialDataNode_DisplayName";
    private boolean notifyResolvePropertyChange = true;

    public SerialDataNode(DataObject dobj) {
        this(null, dobj, Boolean.FALSE.equals(dobj.getPrimaryFile().getAttribute("beaninfo")));
    }

    public SerialDataNode(SerialDataConvertor conv) {
        this(conv, conv.getDataObject(), Boolean.FALSE.equals(conv.getDataObject().getPrimaryFile().getAttribute("beaninfo")));
    }

    private SerialDataNode(SerialDataConvertor conv, DataObject dobj, boolean noBeanInfo) {
        super(dobj, SerialDataNode.getChildren(dobj, noBeanInfo));
        this.convertor = conv;
        this.noBeanInfo = noBeanInfo;
        this.isAfterNodeConstruction = Boolean.TRUE;
    }

    private static Children getChildren(DataObject dobj, boolean noBeanInfo) {
        if (noBeanInfo) {
            return Children.LEAF;
        }
        InstanceCookie inst = (InstanceCookie)dobj.getCookie(InstanceCookie.class);
        if (inst == null) {
            return Children.LEAF;
        }
        try {
            Class clazz = inst.instanceClass();
            if (BeanContext.class.isAssignableFrom(clazz) || BeanContextProxy.class.isAssignableFrom(clazz)) {
                return new InstanceChildren();
            }
            return Children.LEAF;
        }
        catch (Exception ex) {
            return Children.LEAF;
        }
    }

    private InstanceDataObject i() {
        return (InstanceDataObject)this.getDataObject();
    }

    private InstanceCookie.Of ic() {
        return (InstanceCookie.Of)this.getDataObject().getCookie(InstanceCookie.Of.class);
    }

    private SerialDataConvertor getConvertor() {
        return this.convertor;
    }

    private Object getSettingInstance() {
        return this.settingInstance.get();
    }

    private void setSettingsInstance(Object obj) {
        if (obj == this.settingInstance.get()) {
            return;
        }
        this.isNameChanged = false;
        this.settingInstance = new WeakReference<Object>(obj);
    }

    public Image getIcon(int type) {
        if (this.noBeanInfo) {
            return super.getIcon(type);
        }
        Image img = this.initIcon(type);
        if (img == null) {
            img = super.getIcon(type);
        }
        try {
            DataObject dobj = this.getDataObject();
            return dobj.getPrimaryFile().getFileSystem().getStatus().annotateIcon(img, type, dobj.files());
        }
        catch (FileStateInvalidException e) {
            return img;
        }
    }

    public Image getOpenedIcon(int type) {
        return this.getIcon(type);
    }

    public String getHtmlDisplayName() {
        return null;
    }

    private void resolvePropertyChange() {
        SerialDataConvertor c;
        if (this.propertyChangeListener != null && this.propertyChangeListener.getChangeAndReset()) {
            return;
        }
        if (this.notifyResolvePropertyChange && this.propertyChangeListener != null) {
            this.notifyResolvePropertyChange = false;
            XMLSettingsSupport.err.warning("Warning: no PropertyChangeEvent fired from settings stored in " + (Object)this.getDataObject());
        }
        if ((c = this.getConvertor()) != null) {
            c.handleUnfiredChange();
        }
    }

    private void initPList(Object bean, BeanInfo bInfo, BeanNode.Descriptor descr) {
        EventSetDescriptor[] descs = bInfo.getEventSetDescriptors();
        try {
            Method setter = null;
            for (int i = 0; descs != null && i < descs.length; ++i) {
                setter = descs[i].getAddListenerMethod();
                if (setter == null || !setter.getName().equals("addPropertyChangeListener")) continue;
                this.propertyChangeListener = new PropL(this.createSupportedPropertyNames(descr));
                setter.invoke(bean, WeakListeners.propertyChange((PropertyChangeListener)this.propertyChangeListener, (Object)bean));
                this.setSettingsInstance(bean);
            }
        }
        catch (Exception ex) {
            // empty catch block
        }
    }

    private Collection<String> createSupportedPropertyNames(BeanNode.Descriptor descr) {
        int i;
        Node.Property property;
        ArrayList<String> supportedPropertyNames = new ArrayList<String>();
        if (descr.property != null) {
            for (i = 0; i < descr.property.length; ++i) {
                property = descr.property[i];
                supportedPropertyNames.add(property.getName());
            }
        }
        if (descr.expert != null) {
            for (i = 0; i < descr.expert.length; ++i) {
                property = descr.property[i];
                supportedPropertyNames.add(property.getName());
            }
        }
        return supportedPropertyNames;
    }

    private Image initIcon(int type) {
        Image beanInfoIcon = null;
        try {
            BeanInfo bi;
            InstanceCookie.Of ic = this.ic();
            if (ic == null) {
                return null;
            }
            Class clazz = ic.instanceClass();
            String className = clazz.getName();
            if (className.equals("javax.swing.JSeparator") || className.equals("javax.swing.JToolBar$Separator")) {
                Class clazzTmp = Class.forName("javax.swing.JSeparator");
                bi = Utilities.getBeanInfo(clazzTmp);
            } else {
                bi = Utilities.getBeanInfo((Class)clazz);
            }
            if (bi != null) {
                beanInfoIcon = bi.getIcon(type);
            }
            if (SystemAction.class.isAssignableFrom(clazz)) {
                Icon icon;
                SystemAction action = SystemAction.get(clazz.asSubclass(SystemAction.class));
                if (beanInfoIcon == null && (icon = action.getIcon()) != null) {
                    beanInfoIcon = ImageUtilities.icon2Image((Icon)icon);
                }
            }
        }
        catch (Exception e) {
            Logger.getLogger(SerialDataNode.class.getName()).log(Level.WARNING, null, e);
        }
        return beanInfoIcon;
    }

    private String getNameForBean() {
        try {
            Method nameGetter;
            InstanceCookie.Of ic = this.ic();
            if (ic == null) {
                return NbBundle.getMessage(SerialDataNode.class, (String)"LBL_BrokenSettings");
            }
            Class clazz = ic.instanceClass();
            Class[] param = new Class[]{};
            try {
                nameGetter = clazz.getMethod("getName", param);
                if (nameGetter.getReturnType() != String.class) {
                    throw new NoSuchMethodException();
                }
            }
            catch (NoSuchMethodException e) {
                try {
                    nameGetter = clazz.getMethod("getDisplayName", param);
                    if (nameGetter.getReturnType() != String.class) {
                        throw new NoSuchMethodException();
                    }
                }
                catch (NoSuchMethodException ee) {
                    return null;
                }
            }
            Object bean = ic.instanceCreate();
            this.setSettingsInstance(bean);
            return (String)nameGetter.invoke(bean, new Object[0]);
        }
        catch (Exception ex) {
            return null;
        }
    }

    private Method getDeclaredSetter() {
        Method nameSetter = null;
        try {
            InstanceCookie.Of ic = this.ic();
            if (ic == null) {
                return null;
            }
            Class clazz = ic.instanceClass();
            Class[] param = new Class[]{String.class};
            try {
                nameSetter = clazz.getMethod("setName", param);
            }
            catch (NoSuchMethodException e) {
                nameSetter = clazz.getMethod("setDisplayName", param);
            }
            if (!Modifier.isPublic(nameSetter.getModifiers())) {
                nameSetter = null;
            }
        }
        catch (Exception ex) {
            // empty catch block
        }
        return nameSetter;
    }

    public void setName(String name) {
        String old = this.getName();
        if (old != null && old.equals(name)) {
            return;
        }
        InstanceCookie.Of ic = this.ic();
        if (ic == null) {
            return;
        }
        Method nameSetter = this.getDeclaredSetter();
        if (nameSetter != null) {
            try {
                Object bean = ic.instanceCreate();
                this.setSettingsInstance(bean);
                nameSetter.invoke(bean, name);
                this.isNameChanged = true;
                this.getDataObject().getPrimaryFile().setAttribute("SerialDataNode_DisplayName", (Object)name);
                this.resolvePropertyChange();
                return;
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            catch (ClassNotFoundException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            catch (IllegalAccessException ex) {
                Exceptions.attachLocalizedMessage((Throwable)ex, (String)this.getDataObject().toString());
                Exceptions.printStackTrace((Throwable)ex);
            }
            catch (IllegalArgumentException ex) {
                Exceptions.attachLocalizedMessage((Throwable)ex, (String)this.getDataObject().toString());
                Exceptions.printStackTrace((Throwable)ex);
            }
            catch (InvocationTargetException ex) {
                Exceptions.attachLocalizedMessage((Throwable)ex, (String)this.getDataObject().toString());
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
    }

    public String getName() {
        if (this.isAfterNodeConstruction == null) {
            return super.getName();
        }
        return this.getDisplayName();
    }

    public String getDisplayName() {
        String name;
        Object setting = this.getSettingInstance();
        if (setting != null && this.isNameChanged && (name = this.getNameForBean()) != null) {
            return name;
        }
        name = (String)this.getDataObject().getPrimaryFile().getAttribute("SerialDataNode_DisplayName");
        if (name == null) {
            try {
                String def = "\b";
                FileSystem.Status fsStatus = this.getDataObject().getPrimaryFile().getFileSystem().getStatus();
                name = fsStatus.annotateName(def, this.getDataObject().files());
                if (name.indexOf(def) < 0) {
                    return name;
                }
                name = this.getNameForBean();
                name = name != null ? fsStatus.annotateName(name, this.getDataObject().files()) : fsStatus.annotateName(this.getDataObject().getName(), this.getDataObject().files());
            }
            catch (FileStateInvalidException e) {
                // empty catch block
            }
        }
        return name;
    }

    protected Sheet createSheet() {
        Sheet retVal = new Sheet();
        InstanceCookie.Of ic = this.ic();
        if (ic != null) {
            try {
                Class instanceClass = ic.instanceClass();
                Object bean = ic.instanceCreate();
                BeanInfo beanInfo = Utilities.getBeanInfo((Class)instanceClass);
                BeanNode.Descriptor descr = BeanNode.computeProperties((Object)bean, (BeanInfo)beanInfo);
                BeanDescriptor bd = beanInfo.getBeanDescriptor();
                this.initPList(bean, beanInfo, descr);
                retVal.put(this.createPropertiesSet(descr, bd));
                if (descr.expert != null && descr.expert.length != 0) {
                    retVal.put(this.createExpertSet(descr, bd));
                }
            }
            catch (ClassNotFoundException ex) {
                Logger.getLogger(SerialDataNode.class.getName()).log(Level.WARNING, null, ex);
            }
            catch (IOException ex) {
                Logger.getLogger(SerialDataNode.class.getName()).log(Level.WARNING, null, ex);
            }
            catch (IntrospectionException ex) {
                Logger.getLogger(SerialDataNode.class.getName()).log(Level.WARNING, null, ex);
            }
        }
        return retVal;
    }

    private Sheet.Set createExpertSet(BeanNode.Descriptor descr, BeanDescriptor bd) {
        Object helpID;
        Sheet.Set p = Sheet.createExpertSet();
        SerialDataNode.convertProps(p, descr.expert, this);
        if (bd != null && (helpID = bd.getValue("expertHelpID")) != null && helpID instanceof String) {
            p.setValue("helpID", helpID);
        }
        return p;
    }

    private Sheet.Set createPropertiesSet(BeanNode.Descriptor descr, BeanDescriptor bd) {
        Object helpID;
        Sheet.Set props = Sheet.createPropertiesSet();
        if (descr.property != null) {
            SerialDataNode.convertProps(props, descr.property, this);
        }
        if (bd != null && (helpID = bd.getValue("propertiesHelpID")) != null && helpID instanceof String) {
            props.setValue("helpID", helpID);
        }
        return props;
    }

    private static final void convertProps(Sheet.Set set, Node.Property<?>[] arr, SerialDataNode ido) {
        for (int i = 0; i < arr.length; ++i) {
            if (arr[i] instanceof Node.IndexedProperty) {
                set.put((Node.Property)new I((Node.IndexedProperty)arr[i], ido));
                continue;
            }
            set.put((Node.Property)new P(arr[i], ido));
        }
    }

    public boolean canRename() {
        return this.getDeclaredSetter() != null;
    }

    public boolean canDestroy() {
        try {
            InstanceCookie.Of ic = this.ic();
            if (ic == null) {
                return true;
            }
            Class clazz = ic.instanceClass();
            return !SharedClassObject.class.isAssignableFrom(clazz);
        }
        catch (Exception ex) {
            return true;
        }
    }

    public boolean canCut() {
        try {
            InstanceCookie.Of ic = this.ic();
            if (ic == null) {
                return false;
            }
            Class clazz = ic.instanceClass();
            return !SharedClassObject.class.isAssignableFrom(clazz);
        }
        catch (Exception ex) {
            return false;
        }
    }

    public boolean canCopy() {
        try {
            InstanceCookie.Of ic = this.ic();
            if (ic == null) {
                return false;
            }
            Class clazz = ic.instanceClass();
            return !SharedClassObject.class.isAssignableFrom(clazz);
        }
        catch (Exception ex) {
            return false;
        }
    }

    public String getShortDescription() {
        if (this.noBeanInfo) {
            return super.getShortDescription();
        }
        try {
            InstanceCookie.Of ic = this.ic();
            if (ic == null) {
                return this.getDataObject().getPrimaryFile().toString();
            }
            Class clazz = ic.instanceClass();
            BeanDescriptor bd = Utilities.getBeanInfo((Class)clazz).getBeanDescriptor();
            String desc = bd.getShortDescription();
            return desc.equals(bd.getDisplayName()) ? this.getDisplayName() : desc;
        }
        catch (Exception ex) {
            return super.getShortDescription();
        }
    }

    public Action getPreferredAction() {
        return null;
    }

    private final class PropL
    implements PropertyChangeListener {
        private Collection<String> supportedPropertyNames;
        private boolean isChanged;

        PropL(Collection<String> supportedPropertyNames) {
            this.isChanged = false;
            this.supportedPropertyNames = supportedPropertyNames;
        }

        @Override
        public void propertyChange(PropertyChangeEvent e) {
            this.isChanged = true;
            String name = e.getPropertyName();
            if (this.isSupportedName(name)) {
                SerialDataNode.this.firePropertyChange(name, e.getOldValue(), e.getNewValue());
                if (name.equals("name")) {
                    SerialDataNode.this.isNameChanged = true;
                    SerialDataNode.this.fireDisplayNameChange(null, null);
                } else if (name.equals("displayName")) {
                    SerialDataNode.this.isNameChanged = true;
                }
            }
        }

        private boolean isSupportedName(String name) {
            return name != null && this.supportedPropertyNames.contains(name);
        }

        public boolean getChangeAndReset() {
            boolean wasChanged = this.isChanged;
            this.isChanged = false;
            return wasChanged;
        }
    }

    private static class BeanContextNode
    extends BeanNode<Object> {
        public BeanContextNode(Object bean, SerialDataNode task) throws IntrospectionException {
            super(bean, BeanContextNode.getChildren(bean, task));
            this.changeSheet(this.getSheet(), task);
        }

        private void changeSheet(Sheet orig, SerialDataNode task) {
            Sheet.Set props = orig.get("properties");
            if (props != null) {
                SerialDataNode.convertProps(props, props.getProperties(), task);
            }
            if ((props = orig.get("expert")) != null) {
                SerialDataNode.convertProps(props, props.getProperties(), task);
            }
        }

        private static Children getChildren(Object bean, SerialDataNode task) {
            BeanContextChild bch;
            if (bean instanceof BeanContext) {
                return new BeanChildren((BeanContext)bean, (BeanChildren.Factory)new BeanFactoryImpl(task));
            }
            if (bean instanceof BeanContextProxy && (bch = ((BeanContextProxy)bean).getBeanContextProxy()) instanceof BeanContext) {
                return new BeanChildren((BeanContext)bch, (BeanChildren.Factory)new BeanFactoryImpl(task));
            }
            return Children.LEAF;
        }

        public boolean canDestroy() {
            return false;
        }

        public Action[] getActions(boolean context) {
            return BeanContextNode.removeActions(BeanNode.super.getActions(context), new Action[]{SystemAction.get(ToolsAction.class)});
        }

        private static Action[] removeActions(Action[] allActions, Action[] toDeleteActions) {
            Action[] retVal = allActions;
            List<Action> actions = Arrays.asList(allActions);
            for (int i = 0; i < toDeleteActions.length; ++i) {
                Action a = toDeleteActions[i];
                if (!actions.contains(a)) continue;
                actions = new ArrayList<Action>(actions);
                actions.remove(a);
                retVal = actions.toArray(new Action[0]);
            }
            return retVal;
        }
    }

    private static class BeanFactoryImpl
    implements BeanChildren.Factory {
        SerialDataNode task;

        public BeanFactoryImpl(SerialDataNode task) {
            this.task = task;
        }

        public Node createNode(Object bean) throws IntrospectionException {
            return new BeanContextNode(bean, this.task);
        }
    }

    private static final class InstanceChildren
    extends Children.Keys {
        SerialDataNode task;
        DataObject dobj;
        Object bean;
        ContextL contextL = null;

        protected void addNotify() {
            super.addNotify();
            this.task = (SerialDataNode)this.getNode();
            this.dobj = this.task.getDataObject();
            this.contextL = new ContextL(this);
            this.init();
        }

        protected void removeNotify() {
            if (this.contextL != null && this.bean != null) {
                ((BeanContext)this.bean).removeBeanContextMembershipListener(this.contextL);
            }
            this.contextL = null;
            this.setKeys(Collections.emptySet());
        }

        private void init() {
            try {
                InstanceCookie ic = (InstanceCookie)this.dobj.getCookie(InstanceCookie.class);
                if (ic == null) {
                    this.bean = null;
                    return;
                }
                Class clazz = ic.instanceClass();
                this.bean = BeanContext.class.isAssignableFrom(clazz) ? ic.instanceCreate() : (BeanContextProxy.class.isAssignableFrom(clazz) ? ((BeanContextProxy)ic.instanceCreate()).getBeanContextProxy() : null);
            }
            catch (Exception ex) {
                this.bean = null;
                Exceptions.printStackTrace((Throwable)ex);
            }
            if (this.bean != null) {
                ((BeanContext)this.bean).addBeanContextMembershipListener(this.contextL);
            }
            this.updateKeys();
        }

        private void updateKeys() {
            if (this.bean == null) {
                this.setKeys(Collections.emptySet());
            } else {
                this.setKeys(((BeanContext)this.bean).toArray());
            }
        }

        protected Node[] createNodes(Object key) {
            Object ctx = this.bean;
            if (this.bean == null) {
                return new Node[0];
            }
            try {
                BeanContextSupport bcs;
                if (key instanceof BeanContextSupport && ((BeanContext)ctx).contains((bcs = (BeanContextSupport)key).getBeanContextPeer())) {
                    return new Node[0];
                }
                return new Node[]{new BeanContextNode(key, this.task)};
            }
            catch (IntrospectionException ex) {
                return new Node[0];
            }
        }

        private static final class ContextL
        implements BeanContextMembershipListener {
            private final Reference<InstanceChildren> ref;

            ContextL(InstanceChildren bc) {
                this.ref = new WeakReference<InstanceChildren>(bc);
            }

            @Override
            public void childrenAdded(BeanContextMembershipEvent bcme) {
                InstanceChildren bc = this.ref.get();
                if (bc != null) {
                    bc.updateKeys();
                }
            }

            @Override
            public void childrenRemoved(BeanContextMembershipEvent bcme) {
                InstanceChildren bc = this.ref.get();
                if (bc != null) {
                    bc.updateKeys();
                }
            }
        }

    }

    private static final class I
    extends Node.IndexedProperty {
        private Node.IndexedProperty del;
        private SerialDataNode t;

        public I(Node.IndexedProperty del, SerialDataNode t) {
            super(del.getValueType(), del.getElementType());
            this.del = del;
            this.t = t;
        }

        public void setName(String str) {
            this.del.setName(str);
        }

        public void restoreDefaultValue() throws IllegalAccessException, InvocationTargetException {
            this.del.restoreDefaultValue();
        }

        public void setValue(String str, Object obj) {
            this.del.setValue(str, obj);
        }

        public boolean supportsDefaultValue() {
            return this.del.supportsDefaultValue();
        }

        public boolean canRead() {
            return this.del.canRead();
        }

        public PropertyEditor getPropertyEditor() {
            return this.del.getPropertyEditor();
        }

        public boolean isHidden() {
            return this.del.isHidden();
        }

        public Object getValue() throws IllegalAccessException, InvocationTargetException {
            return this.del.getValue();
        }

        public void setExpert(boolean param) {
            this.del.setExpert(param);
        }

        public void setValue(Object val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            this.del.setValue(val);
            this.t.resolvePropertyChange();
        }

        public void setShortDescription(String str) {
            this.del.setShortDescription(str);
        }

        public boolean isExpert() {
            return this.del.isExpert();
        }

        public boolean canWrite() {
            return this.del.canWrite();
        }

        public Class getValueType() {
            return this.del.getValueType();
        }

        public String getDisplayName() {
            return this.del.getDisplayName();
        }

        public Enumeration<String> attributeNames() {
            return this.del.attributeNames();
        }

        public String getShortDescription() {
            return this.del.getShortDescription();
        }

        public String getName() {
            return this.del.getName();
        }

        public void setHidden(boolean param) {
            this.del.setHidden(param);
        }

        public void setDisplayName(String str) {
            this.del.setDisplayName(str);
        }

        public boolean isPreferred() {
            return this.del.isPreferred();
        }

        public Object getValue(String str) {
            return this.del.getValue(str);
        }

        public void setPreferred(boolean param) {
            this.del.setPreferred(param);
        }

        public boolean canIndexedRead() {
            return this.del.canIndexedRead();
        }

        public Class getElementType() {
            return this.del.getElementType();
        }

        public Object getIndexedValue(int index) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            return this.del.getIndexedValue(index);
        }

        public boolean canIndexedWrite() {
            return this.del.canIndexedWrite();
        }

        public void setIndexedValue(int indx, Object val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            this.del.setIndexedValue(indx, val);
            this.t.resolvePropertyChange();
        }

        public PropertyEditor getIndexedPropertyEditor() {
            return this.del.getIndexedPropertyEditor();
        }
    }

    private static final class P
    extends Node.Property {
        private Node.Property del;
        private SerialDataNode t;

        public P(Node.Property del, SerialDataNode t) {
            super(del.getValueType());
            this.del = del;
            this.t = t;
        }

        public void setName(String str) {
            this.del.setName(str);
        }

        public void restoreDefaultValue() throws IllegalAccessException, InvocationTargetException {
            this.del.restoreDefaultValue();
        }

        public void setValue(String str, Object obj) {
            this.del.setValue(str, obj);
        }

        public boolean supportsDefaultValue() {
            return this.del.supportsDefaultValue();
        }

        public boolean canRead() {
            return this.del.canRead();
        }

        public PropertyEditor getPropertyEditor() {
            return this.del.getPropertyEditor();
        }

        public boolean isHidden() {
            return this.del.isHidden();
        }

        public Object getValue() throws IllegalAccessException, InvocationTargetException {
            return this.del.getValue();
        }

        public void setExpert(boolean param) {
            this.del.setExpert(param);
        }

        public void setValue(Object val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            this.del.setValue(val);
            this.t.resolvePropertyChange();
        }

        public void setShortDescription(String str) {
            this.del.setShortDescription(str);
        }

        public boolean isExpert() {
            return this.del.isExpert();
        }

        public boolean canWrite() {
            return this.del.canWrite();
        }

        public Class getValueType() {
            return this.del.getValueType();
        }

        public String getDisplayName() {
            return this.del.getDisplayName();
        }

        public Enumeration<String> attributeNames() {
            return this.del.attributeNames();
        }

        public String getShortDescription() {
            return this.del.getShortDescription();
        }

        public String getName() {
            return this.del.getName();
        }

        public void setHidden(boolean param) {
            this.del.setHidden(param);
        }

        public void setDisplayName(String str) {
            this.del.setDisplayName(str);
        }

        public boolean isPreferred() {
            return this.del.isPreferred();
        }

        public Object getValue(String str) {
            return this.del.getValue(str);
        }

        public void setPreferred(boolean param) {
            this.del.setPreferred(param);
        }
    }

}

