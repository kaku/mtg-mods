/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.ModuleManager
 *  org.netbeans.core.startup.Main
 *  org.netbeans.core.startup.MainLookup
 *  org.openide.modules.ModuleInfo
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.settings.convertors;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Logger;
import org.netbeans.ModuleManager;
import org.netbeans.core.startup.Main;
import org.netbeans.core.startup.MainLookup;
import org.netbeans.modules.settings.convertors.SerialDataConvertor;
import org.netbeans.modules.settings.convertors.XMLSettingsSupport;
import org.openide.modules.ModuleInfo;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.WeakListeners;

final class ModuleInfoManager {
    private static final ModuleInfoManager mim = new ModuleInfoManager();
    private HashMap<String, ModuleInfo> modules = null;
    private Lookup.Result<ModuleInfo> modulesResult = null;
    private HashMap<ModuleInfo, PCL> mapOfListeners;

    private ModuleInfoManager() {
    }

    public static final ModuleInfoManager getDefault() {
        return mim;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public ModuleInfo getModule(String codeBaseName) {
        Collection l = null;
        if (this.modules == null) {
            l = this.getModulesResult().allInstances();
        }
        ModuleInfoManager moduleInfoManager = this;
        synchronized (moduleInfoManager) {
            if (this.modules == null) {
                this.fillModules(l);
            }
            return this.modules.get(codeBaseName);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Lookup.Result<ModuleInfo> getModulesResult() {
        ModuleInfoManager moduleInfoManager = this;
        synchronized (moduleInfoManager) {
            if (this.modulesResult == null) {
                Lookup lookup = ModuleInfoManager.getModuleLookup();
                this.modulesResult = lookup.lookup(new Lookup.Template(ModuleInfo.class));
                this.modulesResult.addLookupListener(new LookupListener(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    public void resultChanged(LookupEvent ev) {
                        List reloaded;
                        Collection l = ModuleInfoManager.this.getModulesResult().allInstances();
                        XMLSettingsSupport.err.fine("Modules changed: " + l);
                         var4_3 = this;
                        synchronized (var4_3) {
                            ModuleInfoManager.this.fillModules(l);
                            reloaded = ModuleInfoManager.this.replaceReloadedModules();
                            XMLSettingsSupport.err.fine("Reloaded modules: " + reloaded);
                        }
                        ModuleInfoManager.this.notifyReloads(reloaded);
                    }
                });
            }
            return this.modulesResult;
        }
    }

    private static Lookup getModuleLookup() {
        Lookup l = Lookup.getDefault();
        if (l instanceof MainLookup) {
            l = Main.getModuleSystem().getManager().getModuleLookup();
        }
        return l;
    }

    private void notifyReloads(List l) {
        for (PCL lsnr : l) {
            lsnr.notifyReload();
        }
    }

    private void fillModules(Collection<? extends ModuleInfo> l) {
        HashMap<String, ModuleInfo> m = new HashMap<String, ModuleInfo>((l.size() << 2) / 3 + 1);
        for (ModuleInfo mi : l) {
            m.put(mi.getCodeNameBase(), mi);
        }
        this.modules = m;
    }

    private List<PCL> replaceReloadedModules() {
        if (this.mapOfListeners == null) {
            return Collections.emptyList();
        }
        Iterator<ModuleInfo> it = new ArrayList<ModuleInfo>(this.mapOfListeners.keySet()).iterator();
        ArrayList<PCL> reloaded = new ArrayList<PCL>();
        while (it.hasNext()) {
            ModuleInfo miNew;
            ModuleInfo mi = it.next();
            if (mi == (miNew = this.modules.get(mi.getCodeNameBase())) || miNew == null) continue;
            PCL lsnr = this.mapOfListeners.remove((Object)mi);
            lsnr.setModuleInfo(miNew);
            reloaded.add(lsnr);
            this.mapOfListeners.put(miNew, lsnr);
        }
        return reloaded;
    }

    public synchronized void registerPropertyChangeListener(SerialDataConvertor sdc, ModuleInfo mi) {
        PCL lsnr;
        if (this.mapOfListeners == null) {
            this.mapOfListeners = new HashMap(this.modules.size());
        }
        if ((lsnr = this.mapOfListeners.get((Object)mi)) == null) {
            lsnr = new PCL(mi);
            this.mapOfListeners.put(mi, lsnr);
        }
        PropertyChangeListener pcl = WeakListeners.propertyChange((PropertyChangeListener)sdc, (Object)lsnr);
        lsnr.addPropertyChangeListener(sdc, pcl);
    }

    public synchronized void unregisterPropertyChangeListener(SerialDataConvertor sdc, ModuleInfo mi) {
        if (this.mapOfListeners == null) {
            return;
        }
        PCL lsnr = this.mapOfListeners.get((Object)mi);
        if (lsnr != null) {
            lsnr.removePropertyChangeListener(sdc);
        }
    }

    public synchronized boolean isReloaded(ModuleInfo mi) {
        if (this.mapOfListeners == null) {
            return false;
        }
        PCL lsnr = this.mapOfListeners.get((Object)mi);
        return lsnr != null && lsnr.isReloaded();
    }

    public synchronized boolean isReloaded(String codeBaseName) {
        if (this.mapOfListeners == null) {
            return false;
        }
        return this.isReloaded(this.getModule(codeBaseName));
    }

    private static final class PCL
    implements PropertyChangeListener {
        private boolean aModuleHasBeenChanged = false;
        private boolean wasModuleEnabled;
        private ModuleInfo mi;
        private PropertyChangeSupport changeSupport;
        private Map<SerialDataConvertor, PropertyChangeListener> origs;

        public PCL(ModuleInfo mi) {
            this.mi = mi;
            this.wasModuleEnabled = mi.isEnabled();
            mi.addPropertyChangeListener((PropertyChangeListener)this);
        }

        void setModuleInfo(ModuleInfo mi) {
            this.mi.removePropertyChangeListener((PropertyChangeListener)this);
            this.aModuleHasBeenChanged = true;
            this.mi = mi;
            mi.addPropertyChangeListener((PropertyChangeListener)this);
        }

        void notifyReload() {
            this.firePropertyChange();
        }

        boolean isReloaded() {
            return this.aModuleHasBeenChanged;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("enabled".equals(evt.getPropertyName())) {
                boolean change;
                if (!Boolean.TRUE.equals(evt.getNewValue())) {
                    this.aModuleHasBeenChanged = true;
                    change = this.wasModuleEnabled;
                } else {
                    change = !this.wasModuleEnabled;
                }
                this.wasModuleEnabled = this.mi.isEnabled();
                if (change) {
                    this.firePropertyChange();
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void addPropertyChangeListener(SerialDataConvertor sdc, PropertyChangeListener listener) {
            PCL pCL = this;
            synchronized (pCL) {
                PropertyChangeListener old;
                if (this.changeSupport == null) {
                    this.changeSupport = new PropertyChangeSupport(this);
                    this.origs = new WeakHashMap<SerialDataConvertor, PropertyChangeListener>();
                }
                if ((old = this.origs.get(sdc)) != null) {
                    return;
                }
                this.origs.put(sdc, listener);
            }
            this.changeSupport.addPropertyChangeListener(listener);
        }

        public void removePropertyChangeListener(PropertyChangeListener listener) {
            if (this.changeSupport != null) {
                this.changeSupport.removePropertyChangeListener(listener);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void removePropertyChangeListener(SerialDataConvertor sdc) {
            PCL pCL = this;
            synchronized (pCL) {
                if (this.origs == null) {
                    return;
                }
                PropertyChangeListener pcl = this.origs.remove(sdc);
                if (pcl != null) {
                    this.removePropertyChangeListener(pcl);
                }
            }
        }

        private void firePropertyChange() {
            if (this.changeSupport != null) {
                this.changeSupport.firePropertyChange("enabled", null, null);
            }
        }
    }

}

