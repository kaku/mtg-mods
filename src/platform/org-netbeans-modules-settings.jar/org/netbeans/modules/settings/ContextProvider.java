/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.settings;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.Date;
import java.util.Enumeration;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

public final class ContextProvider {
    private ContextProvider() {
    }

    public static Writer createWriterContextProvider(Writer w, FileObject src) {
        return new WriterProvider(w, src);
    }

    public static Reader createReaderContextProvider(Reader r, FileObject src) {
        return new ReaderProvider(r, src);
    }

    private static final class FileObjectContext
    extends FileObject {
        private static final String UNSUPPORTED = "The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.";
        private final FileObject fo;

        public FileObjectContext(FileObject fo) {
            this.fo = fo;
        }

        public void addFileChangeListener(FileChangeListener fcl) {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }

        public FileObject createData(String name, String ext) throws IOException {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }

        public FileObject createFolder(String name) throws IOException {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }

        public void delete(FileLock lock) throws IOException {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }

        public Object getAttribute(String attrName) {
            return this.fo.getAttribute(attrName);
        }

        public Enumeration<String> getAttributes() {
            return this.fo.getAttributes();
        }

        public FileObject[] getChildren() {
            return new FileObject[0];
        }

        public String getExt() {
            return this.fo.getExt();
        }

        public FileObject getFileObject(String name, String ext) {
            return null;
        }

        public FileSystem getFileSystem() throws FileStateInvalidException {
            return this.fo.getFileSystem();
        }

        public InputStream getInputStream() throws FileNotFoundException {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }

        public String getName() {
            return this.fo.getName();
        }

        public OutputStream getOutputStream(FileLock lock) throws IOException {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }

        public FileObject getParent() {
            return this.fo.getParent();
        }

        public long getSize() {
            return this.fo.getSize();
        }

        public boolean isData() {
            return true;
        }

        public boolean isFolder() {
            return false;
        }

        public boolean isReadOnly() {
            return this.fo.isReadOnly();
        }

        public boolean isRoot() {
            return false;
        }

        public boolean isValid() {
            return this.fo.isValid();
        }

        public Date lastModified() {
            return this.fo.lastModified();
        }

        public FileLock lock() throws IOException {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }

        public void removeFileChangeListener(FileChangeListener fcl) {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }

        public void rename(FileLock lock, String name, String ext) throws IOException {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }

        public void setAttribute(String attrName, Object value) throws IOException {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }

        public void setImportant(boolean b) {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }
    }

    private static final class ReaderProvider
    extends Reader
    implements Lookup.Provider {
        private final Reader orig;
        private final FileObject src;
        private Lookup lookup;

        public ReaderProvider(Reader r, FileObject src) {
            this.orig = r;
            this.src = src;
        }

        @Override
        public void close() throws IOException {
            this.orig.close();
        }

        @Override
        public int read(char[] cbuf, int off, int len) throws IOException {
            return this.orig.read(cbuf, off, len);
        }

        public Lookup getLookup() {
            if (this.lookup == null) {
                this.lookup = Lookups.singleton((Object)((Object)new FileObjectContext(this.src)));
            }
            return this.lookup;
        }
    }

    private static final class WriterProvider
    extends Writer
    implements Lookup.Provider {
        private final Writer orig;
        private final FileObject src;
        private Lookup lookup;

        public WriterProvider(Writer w, FileObject src) {
            this.orig = w;
            this.src = src;
        }

        @Override
        public void close() throws IOException {
            this.orig.close();
        }

        @Override
        public void flush() throws IOException {
            this.orig.flush();
        }

        @Override
        public void write(char[] cbuf, int off, int len) throws IOException {
            this.orig.write(cbuf, off, len);
        }

        public Lookup getLookup() {
            if (this.lookup == null) {
                this.lookup = Lookups.singleton((Object)((Object)new FileObjectContext(this.src)));
            }
            return this.lookup;
        }
    }

}

