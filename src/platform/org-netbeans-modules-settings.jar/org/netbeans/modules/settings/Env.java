/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.Environment
 *  org.openide.loaders.Environment$Provider
 *  org.openide.loaders.InstanceDataObject
 *  org.openide.loaders.XMLDataObject
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.settings;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;
import org.netbeans.modules.settings.InstanceProvider;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.Environment;
import org.openide.loaders.InstanceDataObject;
import org.openide.loaders.XMLDataObject;
import org.openide.util.Lookup;

public final class Env
implements Environment.Provider {
    public static final String EA_CONVERTOR = "settings.convertor";
    public static final String EA_PROVIDER_PATH = "settings.providerPath";
    public static final String EA_PUBLICID = "hint.originalPublicID";
    public static final String EA_INSTANCE_CLASS_NAME = "settings.instanceClass";
    public static final String EA_INSTANCE_OF = "settings.instanceOf";
    public static final String EA_INSTANCE_CREATE = "settings.instanceCreate";
    public static final String EA_SUBCLASSES = "settings.subclasses";
    private final FileObject providerFO;
    private static String xmlLookupsPrefix = "xml/lookups";
    private static String xmlEntitiesPrefix = "xml/entities";

    public static Environment.Provider create(FileObject fo) {
        return new Env(fo);
    }

    private Env(FileObject fo) {
        this.providerFO = fo;
    }

    public Lookup getEnvironment(DataObject dobj) {
        boolean recognize = false;
        if (dobj instanceof InstanceDataObject) {
            recognize = true;
        } else if (dobj instanceof XMLDataObject) {
            recognize = Boolean.TRUE.equals(dobj.getPrimaryFile().getParent().getAttribute("recognizeXML"));
        }
        if (!recognize) {
            return Lookup.EMPTY;
        }
        InstanceProvider icp = new InstanceProvider(dobj, this.providerFO);
        return icp.getLookup();
    }

    public static Set<String> parseAttribute(Object attr) {
        if (attr != null && attr instanceof String) {
            StringTokenizer s = new StringTokenizer((String)attr, ",");
            HashSet<String> set = new HashSet<String>(10);
            while (s.hasMoreTokens()) {
                set.add(s.nextToken().trim());
            }
            return set;
        }
        return Collections.emptySet();
    }

    public static FileObject findProvider(Class clazz) throws IOException {
        String prefix = "xml/memory/";
        FileObject memContext = FileUtil.getConfigFile((String)prefix);
        if (memContext == null) {
            throw new FileNotFoundException("SFS/xml/memory/");
        }
        for (Class c = clazz; c != null; c = c.getSuperclass()) {
            String providerPath;
            boolean subclasses;
            String name = c.getName().replace('.', '/');
            String convertorPath = new StringBuffer(200).append(prefix).append(name).toString();
            FileObject fo = FileUtil.getConfigFile((String)convertorPath);
            if (fo == null || (providerPath = (String)fo.getAttribute("settings.providerPath")) == null) continue;
            if (c.equals(clazz)) {
                return FileUtil.getConfigFile((String)providerPath);
            }
            Object inheritAttribute = fo.getAttribute("settings.subclasses");
            if (!(inheritAttribute instanceof Boolean) || !(subclasses = ((Boolean)inheritAttribute).booleanValue())) continue;
            return FileUtil.getConfigFile((String)providerPath);
        }
        return null;
    }

    public static FileObject findEntityRegistration(FileObject provider) {
        String filename = provider.getPath();
        int i = filename.lastIndexOf(46);
        if (i != -1 && i > filename.lastIndexOf(47)) {
            filename = filename.substring(0, i);
        }
        String resource = xmlEntitiesPrefix + filename.substring(xmlLookupsPrefix.length(), filename.length());
        return FileUtil.getConfigFile((String)resource);
    }
}

