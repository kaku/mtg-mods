/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.SaveCookie
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.loaders.DataObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.settings;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.ref.SoftReference;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.settings.ContextProvider;
import org.netbeans.modules.settings.Env;
import org.netbeans.modules.settings.InstanceProvider;
import org.netbeans.modules.settings.ScheduledRequest;
import org.netbeans.spi.settings.Convertor;
import org.netbeans.spi.settings.Saver;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;

final class SaveSupport {
    public static final String PROP_SAVE = "savecookie";
    public static final String PROP_FILE_CHANGED = "fileChanged";
    static final String EA_NAME = "name";
    private PropertyChangeSupport changeSupport;
    private Convertor convertor;
    private final SaveCookieImpl instToSave;
    private boolean isChanged;
    private final FileObject file;
    private final SoftReference<Object> instance;
    private final InstanceProvider ip;
    private Boolean knownToBeTemplate;

    public SaveSupport(InstanceProvider ip, Object inst) {
        this.instToSave = new SaveCookieImpl();
        this.isChanged = false;
        this.knownToBeTemplate = null;
        this.ip = ip;
        this.instance = new SoftReference<Object>(inst);
        this.file = ip.getFile();
    }

    private Convertor getConvertor() {
        return this.convertor;
    }

    private Convertor initConvertor() {
        Object inst = this.instance.get();
        if (inst == null) {
            throw new IllegalStateException("setting object cannot be null: " + this.ip);
        }
        try {
            FileObject newProviderFO = Env.findProvider(inst.getClass());
            if (newProviderFO != null) {
                if (this.getPublicID(newProviderFO).equals(this.getPublicID(this.ip.getProvider()))) {
                    this.convertor = this.ip.getConvertor();
                    return this.convertor;
                }
                Object attrb = newProviderFO.getAttribute("settings.convertor");
                if (attrb == null || !(attrb instanceof Convertor)) {
                    throw new IOException("cannot create convertor: " + attrb + ", provider: " + (Object)newProviderFO);
                }
                this.convertor = (Convertor)attrb;
                return this.convertor;
            }
            this.convertor = this.ip.getConvertor();
        }
        catch (IOException ex) {
            Logger.getLogger(SaveSupport.class.getName()).log(Level.WARNING, null, ex);
        }
        return this.convertor;
    }

    private String getPublicID(FileObject fo) throws IOException {
        Object publicId;
        FileObject foEntity = Env.findEntityRegistration(fo);
        if (foEntity == null) {
            foEntity = fo;
        }
        if ((publicId = foEntity.getAttribute("hint.originalPublicID")) == null || !(publicId instanceof String)) {
            throw new IOException("missing or invalid attribute: hint.originalPublicID, provider: " + (Object)foEntity);
        }
        return (String)publicId;
    }

    public final SaveCookie getSaveCookie() {
        return this.instToSave;
    }

    public final boolean isChanged() {
        return this.isChanged;
    }

    public synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
        if (this.changeSupport == null) {
            this.changeSupport = new PropertyChangeSupport(this);
            Object inst = this.instance.get();
            if (inst == null) {
                return;
            }
            Convertor conv = this.initConvertor();
            if (conv != null) {
                conv.registerSaver(inst, this.instToSave);
            }
        }
        this.changeSupport.addPropertyChangeListener(listener);
    }

    public synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
        if (this.changeSupport != null) {
            this.changeSupport.removePropertyChangeListener(listener);
            Object inst = this.instance.get();
            if (inst == null) {
                return;
            }
            Convertor conv = this.getConvertor();
            if (conv != null) {
                conv.unregisterSaver(inst, this.instToSave);
            }
        }
    }

    private void firePropertyChange(String name) {
        if (this.changeSupport != null) {
            this.changeSupport.firePropertyChange(name, null, null);
        }
    }

    public void writeDown() throws IOException {
        this.instToSave.writeDown();
    }

    private class SaveCookieImpl
    implements FileSystem.AtomicAction,
    SaveCookie,
    Saver {
        private ByteArrayOutputStream buf;

        private SaveCookieImpl() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void run() throws IOException {
            if (!SaveSupport.this.ip.getDataObject().isValid()) {
                Logger.getAnonymousLogger().fine("invalid data object cannot be used for storing " + (Object)SaveSupport.this.ip.getDataObject());
                return;
            }
            FileLock lock = null;
            Object object = SaveSupport.access$200((SaveSupport)SaveSupport.this).READWRITE_LOCK;
            synchronized (object) {
                lock = SaveSupport.this.ip.getScheduledRequest().getFileLock();
                if (lock == null) {
                    return;
                }
                OutputStream los = SaveSupport.this.file.getOutputStream(lock);
                BufferedOutputStream os = new BufferedOutputStream(los, 1024);
                try {
                    this.buf.writeTo(os);
                }
                finally {
                    os.close();
                }
            }
        }

        public void save() throws IOException {
            if (!SaveSupport.this.isChanged) {
                return;
            }
            SaveSupport.this.ip.getScheduledRequest().runAndWait();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void writeDown() throws IOException {
            Object inst = SaveSupport.this.instance.get();
            if (inst == null) {
                return;
            }
            Convertor conv = SaveSupport.this.getConvertor();
            if (conv == null) {
                return;
            }
            ByteArrayOutputStream b = new ByteArrayOutputStream(1024);
            Writer w = ContextProvider.createWriterContextProvider(new OutputStreamWriter((OutputStream)b, "UTF-8"), SaveSupport.this.file);
            SaveSupport.this.isChanged = false;
            try {
                conv.write(w, inst);
            }
            finally {
                w.close();
            }
            this.buf = b;
            SaveSupport.this.file.getFileSystem().runAtomicAction((FileSystem.AtomicAction)this);
            this.buf = null;
            this.synchronizeName(inst);
            if (!SaveSupport.this.isChanged) {
                SaveSupport.this.firePropertyChange("savecookie");
            }
        }

        @Override
        public void markDirty() {
            if (SaveSupport.this.isChanged || !SaveSupport.this.ip.getDataObject().isValid()) {
                return;
            }
            if (SaveSupport.this.knownToBeTemplate == null) {
                SaveSupport.this.knownToBeTemplate = SaveSupport.this.ip.getDataObject().isTemplate() ? Boolean.TRUE : Boolean.FALSE;
            }
            if (SaveSupport.this.knownToBeTemplate.booleanValue()) {
                return;
            }
            SaveSupport.this.isChanged = true;
            SaveSupport.this.firePropertyChange("savecookie");
        }

        @Override
        public void requestSave() throws IOException {
            if (SaveSupport.this.isChanged || !SaveSupport.this.ip.getDataObject().isValid()) {
                return;
            }
            if (SaveSupport.this.knownToBeTemplate == null) {
                SaveSupport.this.knownToBeTemplate = SaveSupport.this.ip.getDataObject().isTemplate() ? Boolean.TRUE : Boolean.FALSE;
            }
            if (SaveSupport.this.knownToBeTemplate.booleanValue()) {
                return;
            }
            SaveSupport.this.isChanged = true;
            SaveSupport.this.firePropertyChange("savecookie");
            SaveSupport.this.ip.getScheduledRequest().schedule(SaveSupport.this.instance.get());
        }

        private void synchronizeName(Object inst) {
            Method getter;
            try {
                try {
                    getter = inst.getClass().getMethod("getDisplayName", new Class[0]);
                }
                catch (NoSuchMethodException me) {
                    getter = inst.getClass().getMethod("getName", new Class[0]);
                }
            }
            catch (Exception ex) {
                return;
            }
            if (!getter.isAccessible()) {
                return;
            }
            try {
                String name = (String)getter.invoke(inst, new Object[0]);
                String oldName = SaveSupport.this.ip.getDataObject().getName();
                if (!name.equals(oldName)) {
                    SaveSupport.this.file.setAttribute("name", (Object)name);
                } else if (SaveSupport.this.file.getAttribute("name") == null) {
                    SaveSupport.this.file.setAttribute("name", (Object)name);
                }
            }
            catch (Exception ex) {
                Exceptions.attachLocalizedMessage((Throwable)ex, (String)SaveSupport.this.file.toString());
                Logger.getLogger(SaveSupport.class.getName()).log(Level.WARNING, null, ex);
            }
        }
    }

}

