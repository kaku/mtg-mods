/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.jna.FromNativeContext
 *  com.sun.jna.FunctionMapper
 *  com.sun.jna.IntegerType
 *  com.sun.jna.Native
 *  com.sun.jna.Pointer
 *  com.sun.jna.PointerType
 *  com.sun.jna.Structure
 *  com.sun.jna.TypeMapper
 *  com.sun.jna.ptr.ByReference
 *  com.sun.jna.ptr.IntByReference
 *  com.sun.jna.ptr.PointerByReference
 *  com.sun.jna.win32.StdCallLibrary
 *  com.sun.jna.win32.StdCallLibrary$StdCallCallback
 *  com.sun.jna.win32.W32APIFunctionMapper
 *  com.sun.jna.win32.W32APITypeMapper
 *  org.netbeans.modules.masterfs.providers.Notifier
 */
package org.netbeans.modules.masterfs.watcher.windows;

import com.sun.jna.FromNativeContext;
import com.sun.jna.FunctionMapper;
import com.sun.jna.IntegerType;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.PointerType;
import com.sun.jna.Structure;
import com.sun.jna.TypeMapper;
import com.sun.jna.ptr.ByReference;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIFunctionMapper;
import com.sun.jna.win32.W32APITypeMapper;
import java.io.File;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.masterfs.providers.Notifier;

public final class WindowsNotifier
extends Notifier<Void> {
    static final Logger LOG = Logger.getLogger(WindowsNotifier.class.getName());
    public static HANDLE INVALID_HANDLE_VALUE = new HANDLE(Pointer.createConstant((long)(Pointer.SIZE == 8 ? -1 : 0xFFFFFFFFL)));
    static final Kernel32 KERNEL32 = (Kernel32)Native.loadLibrary((String)"kernel32", Kernel32.class, (Map)new HashMap(){});
    public static final int INFINITE = -1;
    public static final int FILE_NOTIFY_CHANGE_NAME = 3;
    public static final int FILE_NOTIFY_CHANGE_ATTRIBUTES = 4;
    public static final int FILE_NOTIFY_CHANGE_SIZE = 8;
    public static final int FILE_NOTIFY_CHANGE_LAST_WRITE = 16;
    public static final int FILE_NOTIFY_CHANGE_CREATION = 64;
    public static final int FILE_NOTIFY_CHANGE_SECURITY = 256;
    private static final int NOTIFY_MASK = 351;
    public static final int FILE_LIST_DIRECTORY = 1;
    public static final int OPEN_EXISTING = 3;
    public static final int FILE_SHARE_READ = 1;
    public static final int FILE_SHARE_WRITE = 2;
    public static final int FILE_SHARE_DELETE = 4;
    public static final int FILE_FLAG_OVERLAPPED = 1073741824;
    public static final int FILE_FLAG_BACKUP_SEMANTICS = 33554432;
    private static int watcherThreadID;
    private Thread watcher;
    private HANDLE port;
    private final Map<String, FileInfo> rootMap = new HashMap<String, FileInfo>();
    private final Map<HANDLE, FileInfo> handleMap = new HashMap<HANDLE, FileInfo>();
    private final BlockingQueue<String> events = new LinkedBlockingQueue<String>();
    private static final int BUFFER_SIZE = 4096;

    public void removeWatch(Void key) throws IOException {
    }

    public String nextEvent() throws IOException, InterruptedException {
        return this.events.take();
    }

    public Void addWatch(String path) throws IOException {
        if (path.isEmpty()) {
            return null;
        }
        String root = null;
        if (path.length() >= 3 && path.charAt(1) == ':') {
            root = path.substring(0, 3).replace('/', '\\');
            if (root.charAt(2) != '\\') {
                throw new IOException("wrong path: " + path);
            }
            if (path.length() == 3) {
                LOG.log(Level.INFO, "Adding listener for drive {0}", path);
            }
        } else {
            String normedPath = path.replace('/', '\\');
            if (normedPath.startsWith("\\\\")) {
                int thirdBackslash = normedPath.indexOf(92, 3);
                if (thirdBackslash != -1) {
                    int endOfRoot = normedPath.indexOf(92, thirdBackslash + 1);
                    if (endOfRoot == -1) {
                        endOfRoot = normedPath.length();
                    }
                    root = normedPath.substring(0, endOfRoot);
                } else {
                    throw new IOException("wrong path: " + path);
                }
            }
        }
        if (this.rootMap.containsKey(root)) {
            return null;
        }
        path = root;
        int mask = 7;
        int flags = 1107296256;
        HANDLE handle = KERNEL32.CreateFile(path, 1, mask, null, 3, flags, null);
        if (INVALID_HANDLE_VALUE.equals((Object)handle)) {
            throw new IOException("Unable to open " + path + ": " + KERNEL32.GetLastError());
        }
        FileInfo finfo = new FileInfo(path, handle);
        this.rootMap.put(path, finfo);
        this.handleMap.put(handle, finfo);
        this.port = KERNEL32.CreateIoCompletionPort(handle, this.port, handle.getPointer(), 0);
        if (INVALID_HANDLE_VALUE.equals((Object)this.port)) {
            throw new IOException("Unable to create/use I/O Completion port for " + path + ": " + KERNEL32.GetLastError());
        }
        if (!KERNEL32.ReadDirectoryChangesW(handle, finfo.info, finfo.info.size(), true, 351, finfo.infoLength, finfo.overlapped, null)) {
            int err = KERNEL32.GetLastError();
            throw new IOException("ReadDirectoryChangesW failed on " + finfo.path + ", handle " + (Object)((Object)handle) + ": " + err);
        }
        if (this.watcher == null) {
            Thread t = new Thread("W32 File Monitor"){

                @Override
                public void run() {
                    while (WindowsNotifier.this.watcher != null) {
                        FileInfo finfo = WindowsNotifier.this.waitForChange();
                        if (finfo == null) continue;
                        try {
                            WindowsNotifier.this.handleChanges(finfo);
                        }
                        catch (IOException e) {
                            WindowsNotifier.LOG.log(Level.INFO, "handleChanges", e);
                        }
                    }
                }
            };
            t.setDaemon(true);
            t.start();
            this.watcher = t;
        }
        return null;
    }

    protected void start() throws IOException {
    }

    public void stop() throws IOException {
        try {
            Thread w = this.watcher;
            if (w == null) {
                return;
            }
            this.watcher = null;
            w.interrupt();
            w.join(2000);
        }
        catch (InterruptedException ex) {
            throw (IOException)new InterruptedIOException().initCause(ex);
        }
    }

    private void notify(File file) {
        this.events.add(file.getPath());
    }

    private void handleChanges(FileInfo finfo) throws IOException {
        FILE_NOTIFY_INFORMATION fni = finfo.info;
        fni.read();
        do {
            File file = new File(finfo.path, fni.getFilename());
            this.notify(file);
        } while ((fni = fni.next()) != null);
        if (!KERNEL32.ReadDirectoryChangesW(finfo.handle, finfo.info, finfo.info.size(), true, 351, finfo.infoLength, finfo.overlapped, null)) {
            int err = KERNEL32.GetLastError();
            throw new IOException("ReadDirectoryChangesW failed on " + finfo.path + ": " + err);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private FileInfo waitForChange() {
        IntByReference rcount = new IntByReference();
        HANDLEByReference rkey = new HANDLEByReference();
        PointerByReference roverlap = new PointerByReference();
        KERNEL32.GetQueuedCompletionStatus(this.port, rcount, rkey, roverlap, -1);
        WindowsNotifier windowsNotifier = this;
        synchronized (windowsNotifier) {
            return this.handleMap.get((Object)rkey.getValue());
        }
    }

    private class FileInfo {
        public final String path;
        public final HANDLE handle;
        public final FILE_NOTIFY_INFORMATION info;
        public final IntByReference infoLength;
        public final OVERLAPPED overlapped;

        public FileInfo(String path, HANDLE h) {
            this.info = new FILE_NOTIFY_INFORMATION(4096);
            this.infoLength = new IntByReference();
            this.overlapped = new OVERLAPPED();
            this.path = path;
            this.handle = h;
        }
    }

    static interface Kernel32
    extends StdCallLibrary {
        public HANDLE CreateFile(String var1, int var2, int var3, SECURITY_ATTRIBUTES var4, int var5, int var6, HANDLE var7);

        public HANDLE CreateIoCompletionPort(HANDLE var1, HANDLE var2, Pointer var3, int var4);

        public int GetLastError();

        public boolean GetQueuedCompletionStatus(HANDLE var1, IntByReference var2, ByReference var3, PointerByReference var4, int var5);

        public boolean PostQueuedCompletionStatus(HANDLE var1, int var2, Pointer var3, OVERLAPPED var4);

        public boolean CloseHandle(HANDLE var1);

        public boolean ReadDirectoryChangesW(HANDLE var1, FILE_NOTIFY_INFORMATION var2, int var3, boolean var4, int var5, IntByReference var6, OVERLAPPED var7, OVERLAPPED_COMPLETION_ROUTINE var8);

        public static interface OVERLAPPED_COMPLETION_ROUTINE
        extends StdCallLibrary.StdCallCallback {
            public void callback(int var1, int var2, OVERLAPPED var3);
        }

    }

    public static class SECURITY_ATTRIBUTES
    extends Structure {
        public final int nLength;
        public Pointer lpSecurityDescriptor;
        public boolean bInheritHandle;

        public SECURITY_ATTRIBUTES() {
            this.nLength = this.size();
        }

        protected List getFieldOrder() {
            return Arrays.asList("nLength", "lpSecurityDescriptor", "bInheritHandle");
        }
    }

    public static class FILE_NOTIFY_INFORMATION
    extends Structure {
        public int NextEntryOffset;
        public int Action;
        public int FileNameLength;
        public char[] FileName = new char[1];

        private FILE_NOTIFY_INFORMATION() {
        }

        public FILE_NOTIFY_INFORMATION(int size) {
            if (size < this.size()) {
                throw new IllegalArgumentException("Size must greater than " + this.size() + ", requested " + size);
            }
            this.allocateMemory(size);
        }

        protected List getFieldOrder() {
            return Arrays.asList("NextEntryOffset", "Action", "FileNameLength", "FileName");
        }

        public String getFilename() {
            return new String(this.FileName, 0, this.FileNameLength / 2);
        }

        public void read() {
            this.FileName = new char[0];
            super.read();
            this.FileName = this.getPointer().getCharArray(12, this.FileNameLength / 2);
        }

        public FILE_NOTIFY_INFORMATION next() {
            if (this.NextEntryOffset == 0) {
                return null;
            }
            FILE_NOTIFY_INFORMATION next = new FILE_NOTIFY_INFORMATION();
            next.useMemory(this.getPointer(), this.NextEntryOffset);
            next.read();
            return next;
        }
    }

    public static class HANDLEByReference
    extends ByReference {
        public HANDLEByReference() {
            this(null);
        }

        public HANDLEByReference(HANDLE h) {
            super(Pointer.SIZE);
            this.setValue(h);
        }

        public void setValue(HANDLE h) {
            this.getPointer().setPointer(0, h != null ? h.getPointer() : null);
        }

        public HANDLE getValue() {
            Pointer p = this.getPointer().getPointer(0);
            if (p == null) {
                return null;
            }
            if (WindowsNotifier.INVALID_HANDLE_VALUE.getPointer().equals((Object)p)) {
                return WindowsNotifier.INVALID_HANDLE_VALUE;
            }
            HANDLE h = new HANDLE();
            h.setPointer(p);
            return h;
        }
    }

    public static class OVERLAPPED
    extends Structure {
        public ULONG_PTR Internal;
        public ULONG_PTR InternalHigh;
        public int Offset;
        public int OffsetHigh;
        public HANDLE hEvent;

        protected List getFieldOrder() {
            return Arrays.asList("Internal", "InternalHigh", "Offset", "OffsetHigh", "hEvent");
        }
    }

    public static class ULONG_PTR
    extends IntegerType {
        public ULONG_PTR() {
            this(0);
        }

        public ULONG_PTR(long value) {
            super(Pointer.SIZE, value);
        }
    }

    public static final class HANDLE
    extends PointerType {
        private boolean immutable;

        public HANDLE() {
        }

        public HANDLE(Pointer p) {
            this.setPointer(p);
            this.immutable = true;
        }

        public Object fromNative(Object nativeValue, FromNativeContext context) {
            Object o = super.fromNative(nativeValue, context);
            if (WindowsNotifier.INVALID_HANDLE_VALUE.equals(o)) {
                return WindowsNotifier.INVALID_HANDLE_VALUE;
            }
            return o;
        }

        public void setPointer(Pointer p) {
            if (this.immutable) {
                throw new UnsupportedOperationException("immutable reference");
            }
            super.setPointer(p);
        }
    }

}

