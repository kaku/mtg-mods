/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Actions
 *  org.openide.awt.Actions$MenuItem
 *  org.openide.awt.Actions$ToolbarButton
 *  org.openide.awt.JMenuPlus
 *  org.openide.explorer.view.MenuView
 *  org.openide.explorer.view.MenuView$Menu
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.FilterNode$Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.nodes.NodeAcceptor
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeMemberEvent
 *  org.openide.nodes.NodeOp
 *  org.openide.nodes.NodeReorderEvent
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.HelpCtx$Provider
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.NodeAction
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 *  org.openide.windows.WindowManager
 */
package org.openide.actions;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import org.netbeans.modules.openide.loaders.DataNodeUtils;
import org.openide.awt.Actions;
import org.openide.awt.JMenuPlus;
import org.openide.explorer.view.MenuView;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFilter;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataShadow;
import org.openide.loaders.TemplateWizard;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.nodes.NodeAcceptor;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeOp;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;
import org.openide.util.actions.NodeAction;
import org.openide.util.actions.Presenter;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class NewTemplateAction
extends NodeAction {
    private static DataObject selectedTemplate;
    private static DataFolder targetFolder;
    private static int MAX_RECENT_ITEMS;
    private boolean active = false;
    private DataFolder privilegedListFolder;
    private DataFolder recentListFolder;
    private boolean recentChanged = true;
    private List<DataObject> recentList = new ArrayList<DataObject>(0);
    private static final Node[] EMPTY_NODE_ARRAY;

    static TemplateWizard getWizard(Node n) {
        Cookie c;
        TemplateWizard t;
        Node[] arr;
        if (n == null && (arr = WindowManager.getDefault().getRegistry().getActivatedNodes()).length == 1) {
            n = arr[0];
        }
        targetFolder = null;
        for (Node folder = n; targetFolder == null && folder != null; folder = folder.getParentNode()) {
            targetFolder = (DataFolder)folder.getCookie(DataFolder.class);
        }
        Cookie cookie = c = n == null ? null : (Cookie)n.getCookie(Cookie.class);
        if (c != null && (t = c.getTemplateWizard()) != null) {
            return t;
        }
        return new DefaultTemplateWizard();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void performAction(Node[] activatedNodes) {
        if (this.active) {
            return;
        }
        this.active = true;
        Node n = activatedNodes.length == 1 ? activatedNodes[0] : null;
        TemplateWizard wizard = NewTemplateAction.getWizard(n);
        if (wizard instanceof DefaultTemplateWizard) {
            if (targetFolder != null && targetFolder.isValid()) {
                wizard.setTargetFolder(targetFolder);
            }
            if (selectedTemplate != null && selectedTemplate.isValid()) {
                wizard.setTemplate(selectedTemplate);
            }
        }
        boolean instantiated = false;
        try {
            wizard.setTargetName(null);
            instantiated = wizard.instantiate() != null;
        }
        catch (IOException e) {
            Exceptions.attachLocalizedMessage((Throwable)e, (String)NbBundle.getMessage(DataObject.class, (String)"EXC_TemplateFailed"));
            Exceptions.printStackTrace((Throwable)e);
        }
        finally {
            if (wizard instanceof DefaultTemplateWizard) {
                try {
                    if (instantiated && (NewTemplateAction.selectedTemplate = wizard.getTemplate()) != null) {
                        this.recentChanged = this.addRecent(selectedTemplate);
                    }
                    targetFolder = wizard.getTargetFolder();
                }
                catch (IOException ignore) {
                    selectedTemplate = null;
                    targetFolder = null;
                }
            }
            this.active = false;
        }
    }

    protected boolean asynchronous() {
        return true;
    }

    protected boolean enable(Node[] activatedNodes) {
        if (activatedNodes == null || activatedNodes.length != 1) {
            return false;
        }
        Cookie c = (Cookie)activatedNodes[0].getCookie(Cookie.class);
        if (c != null) {
            return c.getTemplateWizard() != null;
        }
        DataFolder cookie = (DataFolder)activatedNodes[0].getCookie(DataFolder.class);
        if (cookie != null && cookie.getPrimaryFile().canWrite()) {
            return true;
        }
        return false;
    }

    public String getName() {
        return NbBundle.getMessage(DataObject.class, (String)"NewTemplate");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(NewTemplateAction.class);
    }

    public JMenuItem getMenuPresenter() {
        return new Actions.MenuItem((SystemAction)this, true){

            public void setEnabled(boolean e) {
                super.setEnabled(true);
            }
        };
    }

    public Component getToolbarPresenter() {
        return new Actions.ToolbarButton((SystemAction)this){

            public void setEnabled(boolean e) {
                super.setEnabled(true);
            }
        };
    }

    public JMenuItem getPopupPresenter() {
        return this.getPopupPresenter(null, (Action)((Object)this));
    }

    private JMenuItem getPopupPresenter(Lookup actionContext, Action action) {
        Node n;
        TemplateWizard tw;
        Node[] nodes = new Node[]{};
        if (actionContext != null) {
            nodes = NewTemplateAction.getNodesFromLookup(actionContext);
        }
        if ((tw = NewTemplateAction.getWizard(n = nodes.length == 1 ? nodes[0] : null)) instanceof DefaultTemplateWizard) {
            return new MenuWithRecent(n, this.isEnabled());
        }
        MenuView.Menu menu = new MenuView.Menu(null, new TemplateActionListener(actionContext), false){

            public JPopupMenu getPopupMenu() {
                if (this.node == null) {
                    this.node = NewTemplateAction.getTemplateRoot(n);
                }
                return super.getPopupMenu();
            }
        };
        Actions.connect((JMenuItem)menu, (Action)action, (boolean)true);
        return menu;
    }

    private List<DataObject> getPrivilegedList() {
        FileObject fo;
        if (this.privilegedListFolder == null && (fo = FileUtil.getConfigFile((String)"Templates/Privileged")) != null) {
            this.privilegedListFolder = DataFolder.findFolder(fo);
        }
        if (this.privilegedListFolder != null) {
            DataObject[] data = this.privilegedListFolder.getChildren();
            ArrayList<DataObject> l2 = new ArrayList<DataObject>(data.length);
            for (int i = 0; i < data.length; ++i) {
                DataObject dobj = data[i];
                if (dobj instanceof DataShadow) {
                    dobj = ((DataShadow)dobj).getOriginal();
                }
                if (!this.isValidTemplate(dobj)) continue;
                l2.add(dobj);
            }
            return l2;
        }
        return new ArrayList<DataObject>(0);
    }

    private void doShowWizard(DataObject template, Node node) {
        targetFolder = null;
        TemplateWizard wizard = NewTemplateAction.getWizard(node);
        try {
            wizard.setTargetName(null);
            Set<DataObject> created = wizard.instantiate(template, targetFolder);
            if (created != null && wizard instanceof DefaultTemplateWizard && (NewTemplateAction.selectedTemplate = wizard.getTemplate()) != null) {
                this.recentChanged = this.addRecent(selectedTemplate);
            }
        }
        catch (IOException e) {
            Exceptions.attachLocalizedMessage((Throwable)e, (String)NbBundle.getMessage(DataObject.class, (String)"EXC_TemplateFailed"));
            Exceptions.printStackTrace((Throwable)e);
        }
    }

    private DataFolder getRecentFolder() {
        FileObject fo;
        if (this.recentListFolder == null && (fo = FileUtil.getConfigFile((String)"Templates/Recent")) != null) {
            this.recentListFolder = DataFolder.findFolder(fo);
        }
        return this.recentListFolder;
    }

    private List<DataObject> getRecentList() {
        if (!this.recentChanged) {
            return this.recentList;
        }
        if (this.getRecentFolder() != null) {
            DataObject[] data = this.getRecentFolder().getChildren();
            ArrayList<DataObject> l2 = new ArrayList<DataObject>(data.length);
            for (int i = 0; i < data.length; ++i) {
                DataObject dobj = data[i];
                if (dobj instanceof DataShadow) {
                    dobj = ((DataShadow)dobj).getOriginal();
                }
                if (this.isValidTemplate(dobj)) {
                    l2.add(dobj);
                    continue;
                }
                this.removeRecent(data[i]);
            }
            this.recentList = l2;
        } else {
            this.recentList = new ArrayList<DataObject>(0);
        }
        this.recentChanged = false;
        return this.recentList;
    }

    private boolean isValidTemplate(DataObject template) {
        return template != null && template.isTemplate() && template.isValid();
    }

    private boolean addRecent(DataObject template) {
        DataFolder folder = this.getRecentFolder();
        if (folder == null) {
            return false;
        }
        if (this.getPrivilegedList().contains(template)) {
            return false;
        }
        if (this.isRecent(template)) {
            return false;
        }
        DataObject[] templates = folder.getChildren();
        DataObject[] newOrder = new DataObject[templates.length + 1];
        for (int i = 1; i < newOrder.length; ++i) {
            newOrder[i] = templates[i - 1];
        }
        try {
            newOrder[0] = template.createShadow(folder);
            folder.setOrder(newOrder);
        }
        catch (IOException ioe) {
            Logger.getLogger(NewTemplateAction.class.getName()).log(Level.WARNING, null, ioe);
            return false;
        }
        templates = folder.getChildren();
        for (int size = templates.length; size > MAX_RECENT_ITEMS; --size) {
            this.removeRecent(templates[size - 1]);
        }
        return true;
    }

    private boolean removeRecent(DataObject template) {
        DataFolder folder = this.getRecentFolder();
        if (folder == null) {
            return false;
        }
        try {
            template.delete();
            return true;
        }
        catch (IOException ioe) {
            Logger.getLogger(NewTemplateAction.class.getName()).log(Level.WARNING, null, ioe);
            return false;
        }
    }

    private boolean isRecent(DataObject template) {
        return this.getRecentList().contains(template);
    }

    public static Node getTemplateRoot() {
        RootChildren ch = new RootChildren(null);
        DataFolder dataFolder = ch.getRootFolder();
        dataFolder.getClass();
        return new DataFolder.FolderNode(dataFolder, (Children)ch);
    }

    private static Node getTemplateRoot(Node n) {
        RootChildren ch = new RootChildren(n);
        DataFolder dataFolder = ch.getRootFolder();
        dataFolder.getClass();
        DataFolder.FolderNode help = new DataFolder.FolderNode(dataFolder, (Children)ch);
        return help;
    }

    private static boolean acceptObj(DataObject obj) {
        if (obj.isTemplate()) {
            return true;
        }
        if (obj instanceof DataFolder) {
            Object o = obj.getPrimaryFile().getAttribute("simple");
            return o == null || Boolean.TRUE.equals(o);
        }
        return false;
    }

    private void updateAction() {
    }

    private static final synchronized Node[] getNodesFromLookup(Lookup lookup) {
        if (lookup != null) {
            return lookup.lookupAll(Node.class).toArray((T[])EMPTY_NODE_ARRAY);
        }
        return EMPTY_NODE_ARRAY;
    }

    public Action createContextAwareInstance(Lookup actionContext) {
        return new DelegateAction(this, actionContext);
    }

    static {
        MAX_RECENT_ITEMS = 5;
        EMPTY_NODE_ARRAY = new Node[0];
    }

    private static final class DelegateAction
    implements Action,
    Presenter.Popup,
    LookupListener {
        private NewTemplateAction delegate;
        private Lookup actionContext;
        private Lookup.Result nodesResult;
        private PropertyChangeSupport support;

        public DelegateAction(NewTemplateAction action, Lookup actionContext) {
            this.support = new PropertyChangeSupport(this);
            this.delegate = action;
            this.actionContext = actionContext;
            this.nodesResult = actionContext.lookupResult(Node.class);
            this.nodesResult.addLookupListener((LookupListener)this);
            this.resultChanged(null);
        }

        public String toString() {
            return super.toString() + "[delegate=" + (Object)((Object)this.delegate) + "]";
        }

        @Override
        public void putValue(String key, Object value) {
        }

        @Override
        public boolean isEnabled() {
            return this.delegate.enable(NewTemplateAction.getNodesFromLookup(this.actionContext));
        }

        @Override
        public Object getValue(String key) {
            return this.delegate.getValue(key);
        }

        @Override
        public void setEnabled(boolean b) {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener listener) {
            this.support.addPropertyChangeListener(listener);
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener listener) {
            this.support.removePropertyChangeListener(listener);
        }

        public JMenuItem getPopupPresenter() {
            return this.delegate.getPopupPresenter(this.actionContext, this);
        }

        public void resultChanged(LookupEvent ev) {
            this.getPopupPresenter();
        }
    }

    private class NodeLookupListener
    implements LookupListener {
        private NodeLookupListener() {
        }

        public void resultChanged(LookupEvent ev) {
            NewTemplateAction.this.updateAction();
        }
    }

    private static class DefaultTemplateWizard
    extends TemplateWizard {
        DefaultTemplateWizard() {
        }
    }

    private static class DataShadowFilterNode
    extends FilterNode
    implements NodeListener {
        private String name;

        public DataShadowFilterNode(Node or, Children children, String name) {
            super(or, children);
            this.name = name;
            this.disableDelegation(4);
            or.addNodeListener(NodeOp.weakNodeListener((NodeListener)this, (Object)or));
        }

        public String getDisplayName() {
            return this.name;
        }

        private void refresh() {
            TemplateChildren ch;
            Node p;
            Node n = this.getOriginal();
            String nodeName = n.getDisplayName();
            DataObject obj = null;
            DataShadow shadow = (DataShadow)n.getCookie(DataShadow.class);
            if (shadow != null) {
                DataNode dn = new DataNode(shadow, FilterNode.Children.LEAF);
                nodeName = dn.getDisplayName();
                obj = shadow.getOriginal();
                n = obj.getNodeDelegate();
            }
            if (obj == null) {
                obj = (DataObject)n.getCookie(DataObject.class);
            }
            if (obj != null) {
                if (obj.isTemplate()) {
                    this.name = nodeName;
                    this.setChildren(FilterNode.Children.LEAF);
                    return;
                }
                if (NewTemplateAction.acceptObj(obj)) {
                    this.name = nodeName;
                    this.setChildren((Children)new TemplateChildren(n));
                    return;
                }
            }
            if ((p = this.getParentNode()) != null && (ch = (TemplateChildren)p.getChildren()) != null) {
                ch.refreshKeyImpl(n);
            }
        }

        public void childrenAdded(NodeMemberEvent ev) {
            this.refresh();
        }

        public void childrenRemoved(NodeMemberEvent ev) {
            this.refresh();
        }

        public void childrenReordered(NodeReorderEvent ev) {
        }

        public void nodeDestroyed(NodeEvent ev) {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            this.refresh();
        }
    }

    private static class TemplateChildren
    extends FilterNode.Children {
        public TemplateChildren(Node or) {
            super(or);
        }

        protected Node[] createNodes(Node n) {
            if (EventQueue.isDispatchThread()) {
                return new Node[]{new DataShadowFilterNode(n, LEAF, n.getDisplayName())};
            }
            String nodeName = n.getDisplayName();
            DataObject obj = null;
            DataShadow shadow = (DataShadow)n.getCookie(DataShadow.class);
            if (shadow != null) {
                DataNode dn = new DataNode(shadow, Children.LEAF);
                nodeName = dn.getDisplayName();
                obj = shadow.getOriginal();
                n = obj.getNodeDelegate();
            }
            if (obj == null) {
                obj = (DataObject)n.getCookie(DataObject.class);
            }
            if (obj != null) {
                if (obj.isTemplate()) {
                    // empty if block
                }
                if (NewTemplateAction.acceptObj(obj)) {
                    return new Node[]{new DataShadowFilterNode(n, (Children)new TemplateChildren(n), nodeName)};
                }
            }
            return new Node[]{new DataShadowFilterNode(n, LEAF, nodeName)};
        }

        final void refreshKeyImpl(Node n) {
            this.refreshKey((Object)n);
        }
    }

    private static class RootChildren
    extends Children.Keys<Node>
    implements NodeListener {
        private TemplateWizard wizard;
        private DataFolder rootFolder;
        private WeakReference<Node> current;
        private NodeListener listener;

        public RootChildren(Node n) {
            this.listener = NodeOp.weakNodeListener((NodeListener)this, (Object)null);
            TopComponent.Registry reg = WindowManager.getDefault().getRegistry();
            reg.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)((Object)this), (Object)reg));
            this.updateWizard(NewTemplateAction.getWizard(n));
        }

        public DataFolder getRootFolder() {
            if (this.rootFolder == null) {
                this.doSetKeys();
            }
            return this.rootFolder;
        }

        protected Node[] createNodes(Node n) {
            String nodeName = n.getDisplayName();
            DataObject obj = null;
            DataShadow shadow = (DataShadow)n.getCookie(DataShadow.class);
            if (shadow != null) {
                DataNode dn = new DataNode(shadow, Children.LEAF);
                nodeName = dn.getDisplayName();
                obj = shadow.getOriginal();
                n = obj.getNodeDelegate();
            }
            if (obj == null) {
                obj = (DataObject)n.getCookie(DataObject.class);
            }
            if (obj != null) {
                if (obj.isTemplate()) {
                    return new Node[]{new DataShadowFilterNode(n, LEAF, nodeName)};
                }
                if (NewTemplateAction.acceptObj(obj)) {
                    return new Node[]{new DataShadowFilterNode(n, (Children)new TemplateChildren(n), nodeName)};
                }
            }
            return null;
        }

        private void updateNode(Node n) {
            Node prev;
            if (this.current != null && this.current.get() == n) {
                return;
            }
            Node node = prev = this.current != null ? this.current.get() : null;
            if (prev != null) {
                prev.removeNodeListener(this.listener);
            }
            n.addNodeListener(this.listener);
            this.current = new WeakReference<Node>(n);
        }

        private void updateWizard(TemplateWizard w) {
            if (this.wizard == w) {
                return;
            }
            if (this.wizard != null) {
                Node n = this.wizard.getTemplatesFolder().getNodeDelegate();
                n.removeNodeListener(this.listener);
            }
            Node newNode = w.getTemplatesFolder().getNodeDelegate();
            newNode.addNodeListener(this.listener);
            this.wizard = w;
            this.updateKeys();
        }

        private void updateKeys() {
            DataNodeUtils.reqProcessor().post(new Runnable(){

                @Override
                public void run() {
                    RootChildren.this.doSetKeys();
                }
            });
        }

        private void doSetKeys() {
            this.rootFolder = this.wizard.getTemplatesFolder();
            this.setKeys((Object[])this.rootFolder.getNodeDelegate().getChildren().getNodes(true));
        }

        public void childrenReordered(NodeReorderEvent ev) {
            this.updateKeys();
        }

        public void childrenRemoved(NodeMemberEvent ev) {
            this.updateKeys();
        }

        public void childrenAdded(NodeMemberEvent ev) {
            this.updateKeys();
        }

        public void nodeDestroyed(NodeEvent ev) {
        }

        public void propertyChange(PropertyChangeEvent ev) {
            Node[] arr;
            String pn = ev.getPropertyName();
            if (this.current != null && ev.getSource() == this.current.get()) {
                if ("cookie".equals(pn)) {
                    final Node node = this.current.get();
                    Mutex.EVENT.readAccess(new Runnable(){

                        @Override
                        public void run() {
                            RootChildren.this.updateWizard(NewTemplateAction.getWizard(node));
                        }
                    });
                }
            } else if ("activatedNodes".equals(pn) && (arr = WindowManager.getDefault().getRegistry().getActivatedNodes()).length == 1) {
                this.updateNode(arr[0]);
            }
        }

    }

    private static class TemplateActionListener
    implements NodeAcceptor,
    DataFilter {
        static final long serialVersionUID = 1214995994333505784L;
        Lookup actionContext;

        TemplateActionListener(Lookup context) {
            this.actionContext = context;
        }

        public boolean acceptNodes(Node[] nodes) {
            Node[] nodesInContext = null;
            if (this.actionContext != null) {
                nodesInContext = NewTemplateAction.getNodesFromLookup(this.actionContext);
            }
            if (nodesInContext == null || nodesInContext.length != 1) {
                Logger.getAnonymousLogger().warning("Wrong count of nodes in context lookup.");
                return false;
            }
            if (nodes == null || nodes.length != 1) {
                Logger.getAnonymousLogger().warning("Wrong count of selected nodes in popup menu.");
                return false;
            }
            Node n = nodes[0];
            DataObject obj = (DataObject)n.getCookie(DataObject.class);
            if (obj == null || !obj.isTemplate()) {
                Logger.getAnonymousLogger().warning("Selected node in popup menu is not acceptable.");
                return false;
            }
            TemplateWizard wizard = NewTemplateAction.getWizard(nodesInContext[0]);
            try {
                wizard.setTargetName(null);
                wizard.instantiate(obj, targetFolder);
            }
            catch (IOException e) {
                Exceptions.attachLocalizedMessage((Throwable)e, (String)NbBundle.getMessage(DataObject.class, (String)"EXC_TemplateFailed"));
                Exceptions.printStackTrace((Throwable)e);
            }
            return true;
        }

        @Override
        public boolean acceptDataObject(DataObject obj) {
            return NewTemplateAction.acceptObj(obj);
        }
    }

    public static interface Cookie
    extends Node.Cookie {
        public TemplateWizard getTemplateWizard();
    }

    private class MenuWithRecent
    extends JMenuPlus {
        private boolean initialized;
        private Node node;
        private boolean canWrite;

        public MenuWithRecent(Node n, boolean writable) {
            this.initialized = false;
            Actions.setMenuText((AbstractButton)((Object)this), (String)NewTemplateAction.this.getName(), (boolean)false);
            this.node = n;
            this.canWrite = writable;
        }

        public JPopupMenu getPopupMenu() {
            JPopupMenu popup = super.getPopupMenu();
            if (!this.initialized) {
                popup.add(new Item(null));
                List privileged = NewTemplateAction.this.getPrivilegedList();
                if (privileged.size() > 0) {
                    popup.add(new JSeparator());
                }
                Iterator it = privileged.iterator();
                while (it.hasNext()) {
                    DataObject dobj = (DataObject)it.next();
                    if (dobj instanceof DataShadow) {
                        dobj = ((DataShadow)dobj).getOriginal();
                    }
                    popup.add(new Item(dobj));
                }
                boolean regenerate = false;
                boolean addSeparator = !NewTemplateAction.this.getRecentList().isEmpty();
                for (DataObject dobj : NewTemplateAction.this.getRecentList()) {
                    if (NewTemplateAction.this.isValidTemplate(dobj)) {
                        if (addSeparator) {
                            popup.add(new JSeparator());
                        }
                        addSeparator = false;
                        popup.add(new Item(dobj));
                        continue;
                    }
                    regenerate = true;
                }
                NewTemplateAction.this.recentChanged = NewTemplateAction.this.recentChanged || regenerate;
                this.initialized = true;
            }
            return popup;
        }

        private class Item
        extends JMenuItem
        implements HelpCtx.Provider,
        ActionListener {
            DataObject template;

            public Item(DataObject template) {
                this.template = template;
                this.setText(template == null ? NbBundle.getMessage(DataObject.class, (String)"NewTemplateAction") : template.getNodeDelegate().getDisplayName());
                if (template == null) {
                    this.setIcon(NewTemplateAction.this.getIcon());
                } else {
                    this.setIcon(new ImageIcon(template.getNodeDelegate().getIcon(1)));
                }
                this.addActionListener(this);
                this.setEnabled(MenuWithRecent.this.canWrite);
            }

            public HelpCtx getHelpCtx() {
                if (this.template != null) {
                    return this.template.getHelpCtx();
                }
                return NewTemplateAction.this.getHelpCtx();
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                NewTemplateAction.this.doShowWizard(this.template, MenuWithRecent.this.node);
            }
        }

    }

}

