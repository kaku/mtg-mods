/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CookieAction
 */
package org.openide.actions;

import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CookieAction;

public final class FileSystemRefreshAction
extends CookieAction {
    protected Class<?>[] cookieClasses() {
        return new Class[]{DataFolder.class};
    }

    protected void performAction(Node[] nodes) {
        for (Node n : nodes) {
            DataFolder df = (DataFolder)n.getCookie(DataFolder.class);
            if (df == null) continue;
            FileObject fo = df.getPrimaryFile();
            fo.refresh();
        }
    }

    protected boolean asynchronous() {
        return true;
    }

    protected int mode() {
        return 4;
    }

    public String getName() {
        return NbBundle.getBundle(DataObject.class).getString("LAB_Refresh");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(FileSystemRefreshAction.class);
    }
}

