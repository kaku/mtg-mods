/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.LifecycleManager
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.CallableSystemAction
 */
package org.openide.actions;

import java.util.Set;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.LifecycleManager;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.util.actions.CallableSystemAction;

public final class SaveAllAction
extends CallableSystemAction {
    static final long serialVersionUID = 333;
    private static final Object RUNNING = new Object();
    private ChangeListener chl;

    public SaveAllAction() {
        this.chl = new ModifiedListL();
        DataObject.getRegistry().addChangeListener(WeakListeners.change((ChangeListener)this.chl, (Object)DataObject.getRegistry()));
    }

    protected void initialize() {
        super.initialize();
        this.putProperty((Object)"enabled", (Object)Boolean.FALSE);
        this.putValue("ShortDescription", (Object)NbBundle.getMessage(DataObject.class, (String)"HINT_SaveAll"));
        this.chl = new ModifiedListL();
        DataObject.getRegistry().addChangeListener(WeakListeners.change((ChangeListener)this.chl, (Object)DataObject.getRegistry()));
    }

    public String getName() {
        return NbBundle.getMessage(DataObject.class, (String)"SaveAll");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(SaveAllAction.class);
    }

    protected String iconResource() {
        return "org/openide/loaders/saveAll.gif";
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void performAction() {
        Object object = RUNNING;
        synchronized (object) {
            while (this.getProperty(RUNNING) != null) {
                try {
                    RUNNING.wait();
                }
                catch (InterruptedException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            this.putProperty(RUNNING, RUNNING);
        }
        try {
            LifecycleManager.getDefault().saveAll();
        }
        finally {
            object = RUNNING;
            synchronized (object) {
                this.putProperty(RUNNING, (Object)null);
                RUNNING.notifyAll();
            }
        }
    }

    protected boolean asynchronous() {
        return true;
    }

    final class ModifiedListL
    implements ChangeListener {
        ModifiedListL() {
        }

        @Override
        public void stateChanged(ChangeEvent evt) {
            Mutex.EVENT.writeAccess(new Runnable(){

                @Override
                public void run() {
                    SaveAllAction.this.setEnabled(DataObject.getRegistry().getModifiedSet().size() > 0);
                }
            });
        }

    }

}

