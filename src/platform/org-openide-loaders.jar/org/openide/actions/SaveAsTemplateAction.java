/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.SaveCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.nodes.NodeAcceptor
 *  org.openide.nodes.NodeOperation
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.UserCancelException
 *  org.openide.util.actions.NodeAction
 */
package org.openide.actions;

import java.awt.Component;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.openide.actions.NewTemplateAction;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.nodes.NodeAcceptor;
import org.openide.nodes.NodeOperation;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.UserCancelException;
import org.openide.util.actions.NodeAction;

public final class SaveAsTemplateAction
extends NodeAction {
    public HelpCtx getHelpCtx() {
        return new HelpCtx(SaveAsTemplateAction.class);
    }

    public String getName() {
        return NbBundle.getMessage(DataObject.class, (String)"SaveAsTemplate");
    }

    @Deprecated
    public String iconResource() {
        return super.iconResource();
    }

    protected boolean surviveFocusChange() {
        return false;
    }

    protected boolean enable(Node[] activatedNodes) {
        if (activatedNodes == null || activatedNodes.length == 0) {
            return false;
        }
        for (int i = 0; i < activatedNodes.length; ++i) {
            DataObject curCookie = (DataObject)activatedNodes[i].getCookie(DataObject.class);
            if (curCookie != null && curCookie.isCopyAllowed()) continue;
            return false;
        }
        return true;
    }

    protected void performAction(Node[] activatedNodes) {
        Node[] selected;
        FolderNodeAcceptor acceptor = FolderNodeAcceptor.getInstance();
        String title = NbBundle.getMessage(DataObject.class, (String)"Title_SaveAsTemplate");
        String rootTitle = NbBundle.getMessage(DataObject.class, (String)"CTL_SaveAsTemplate");
        Node templatesNode = NewTemplateAction.getTemplateRoot();
        templatesNode.setDisplayName(NbBundle.getMessage(DataObject.class, (String)"CTL_SaveAsTemplate_TemplatesRoot"));
        try {
            selected = NodeOperation.getDefault().select(title, rootTitle, templatesNode, (NodeAcceptor)acceptor, null);
        }
        catch (UserCancelException ex) {
            return;
        }
        DataFolder targetFolder = (DataFolder)selected[0].getCookie(DataFolder.class);
        for (int i = 0; i < activatedNodes.length; ++i) {
            this.createNewTemplate((DataObject)activatedNodes[i].getCookie(DataObject.class), targetFolder);
        }
    }

    protected boolean asynchronous() {
        return false;
    }

    private void createNewTemplate(DataObject source, DataFolder targetFolder) {
        try {
            SaveCookie cookie = source.getCookie(SaveCookie.class);
            if (cookie != null) {
                cookie.save();
            }
            DataObject newTemplate = source.copy(targetFolder);
            DataObject templateSample = null;
            for (DataObject d : targetFolder.getChildren()) {
                if (!d.isTemplate()) continue;
                templateSample = d;
                break;
            }
            newTemplate.setTemplate(true);
            if (templateSample == null) {
                newTemplate.getPrimaryFile().setAttribute("javax.script.ScriptEngine", (Object)"freemarker");
            } else {
                SaveAsTemplateAction.setTemplateAttributes(newTemplate.getPrimaryFile(), SaveAsTemplateAction.getAttributes(templateSample.getPrimaryFile()));
            }
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    private static void setTemplateAttributes(FileObject fo, Map<String, Object> attributes) throws IOException {
        for (Map.Entry<String, Object> entry : attributes.entrySet()) {
            if ("SystemFileSystem.localizingBundle".equals(entry.getKey())) continue;
            fo.setAttribute(entry.getKey(), entry.getValue());
        }
    }

    private static Map<String, Object> getAttributes(FileObject fo) {
        HashMap<String, Object> attributes = new HashMap<String, Object>();
        Enumeration attributeNames = fo.getAttributes();
        while (attributeNames.hasMoreElements()) {
            Object attrValue;
            String attrName = (String)attributeNames.nextElement();
            if (attrName == null || (attrValue = fo.getAttribute(attrName)) == null) continue;
            attributes.put(attrName, attrValue);
        }
        return attributes;
    }

    static final class FolderNodeAcceptor
    implements NodeAcceptor {
        private static FolderNodeAcceptor instance;
        private DataFolder rootFolder;

        private FolderNodeAcceptor(DataFolder root) {
            this.rootFolder = root;
        }

        public boolean acceptNodes(Node[] nodes) {
            boolean res = false;
            if (nodes == null || nodes.length != 1) {
                res = false;
            } else {
                Node n = nodes[0];
                DataFolder df = (DataFolder)n.getCookie(DataFolder.class);
                if (df != null) {
                    res = !this.rootFolder.equals(df);
                }
            }
            return res;
        }

        static FolderNodeAcceptor getInstance() {
            DataFolder rootFolder = (DataFolder)NewTemplateAction.getTemplateRoot().getCookie(DataFolder.class);
            if (instance == null) {
                instance = new FolderNodeAcceptor(rootFolder);
            }
            return instance;
        }
    }

}

