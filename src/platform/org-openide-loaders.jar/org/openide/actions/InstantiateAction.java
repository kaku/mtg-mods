/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.UserCancelException
 *  org.openide.util.actions.NodeAction
 */
package org.openide.actions;

import java.io.IOException;
import java.util.Set;
import org.openide.actions.NewTemplateAction;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.UserCancelException;
import org.openide.util.actions.NodeAction;

@Deprecated
public class InstantiateAction
extends NodeAction {
    static final long serialVersionUID = 1482795804240508824L;

    protected boolean enable(Node[] activatedNodes) {
        if (activatedNodes.length != 1) {
            return false;
        }
        DataObject obj = (DataObject)activatedNodes[0].getCookie(DataObject.class);
        return obj != null && obj.isTemplate();
    }

    protected void performAction(Node[] activatedNodes) {
        DataObject obj = (DataObject)activatedNodes[0].getCookie(DataObject.class);
        if (obj != null && obj.isTemplate()) {
            try {
                InstantiateAction.instantiateTemplate(obj);
            }
            catch (UserCancelException ex) {
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
    }

    public String getName() {
        return NbBundle.getMessage(DataObject.class, (String)"Instantiate");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(InstantiateAction.class);
    }

    public static Set<DataObject> instantiateTemplate(DataObject obj) throws IOException {
        return NewTemplateAction.getWizard(null).instantiate(obj);
    }
}

