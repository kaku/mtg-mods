/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.actions.SaveAction
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package org.openide.actions;

import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.EventListener;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;
import javax.swing.event.SwingPropertyChangeSupport;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.actions.SaveAction;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.SaveAsCapable;
import org.openide.util.ContextAwareAction;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

final class SaveAsAction
extends AbstractAction
implements ContextAwareAction {
    private Lookup context;
    private Lookup.Result<SaveAsCapable> lkpInfo;
    private boolean isGlobal = false;
    private boolean isDirty = true;
    private PropertyChangeListener registryListener;
    private LookupListener lookupListener;

    private SaveAsAction() {
        this(Utilities.actionsGlobalContext(), true);
    }

    private SaveAsAction(Lookup context, boolean isGlobal) {
        super(NbBundle.getMessage(DataObject.class, (String)"CTL_SaveAsAction"));
        this.context = context;
        this.isGlobal = isGlobal;
        this.putValue("noIconInMenu", Boolean.TRUE);
        this.setEnabled(false);
    }

    public static ContextAwareAction create() {
        return new SaveAsAction();
    }

    @Override
    public boolean isEnabled() {
        if (this.isDirty || null == this.changeSupport || !this.changeSupport.hasListeners("enabled")) {
            this.refreshEnabled();
        }
        return super.isEnabled();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.refreshListeners();
        Collection inst = this.lkpInfo.allInstances();
        if (inst.size() > 0) {
            SaveAsCapable saveAs = (SaveAsCapable)inst.iterator().next();
            File newFile = this.getNewFileName();
            if (null != newFile) {
                FileObject newFolder = null;
                try {
                    File targetFolder = newFile.getParentFile();
                    if (null == targetFolder) {
                        throw new IOException(newFile.getAbsolutePath());
                    }
                    newFolder = FileUtil.createFolder((File)targetFolder);
                }
                catch (IOException ioE) {
                    NotifyDescriptor error = new NotifyDescriptor((Object)NbBundle.getMessage(DataObject.class, (String)"MSG_CannotCreateTargetFolder"), NbBundle.getMessage(DataObject.class, (String)"LBL_SaveAsTitle"), -1, 0, new Object[]{NotifyDescriptor.OK_OPTION}, NotifyDescriptor.OK_OPTION);
                    DialogDisplayer.getDefault().notify(error);
                    return;
                }
                try {
                    saveAs.saveAs(newFolder, newFile.getName());
                }
                catch (IOException ioE) {
                    Exceptions.attachLocalizedMessage((Throwable)ioE, (String)NbBundle.getMessage(DataObject.class, (String)"MSG_SaveAsFailed", (Object)newFile.getName(), (Object)ioE.getLocalizedMessage()));
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ioE);
                }
            }
        }
    }

    private File getNewFileName() {
        File initialFolder;
        File newFile = null;
        File currentFile = null;
        FileObject currentFileObject = this.getCurrentFileObject();
        if (null != currentFileObject) {
            currentFile = newFile = FileUtil.toFile((FileObject)currentFileObject);
            if (null == newFile) {
                newFile = new File(currentFileObject.getNameExt());
            }
        }
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle(NbBundle.getMessage(DataObject.class, (String)"LBL_SaveAsTitle"));
        chooser.setMultiSelectionEnabled(false);
        if (null != newFile) {
            chooser.setSelectedFile(newFile);
            FileUtil.preventFileChooserSymlinkTraversal((JFileChooser)chooser, (File)newFile.getParentFile());
        }
        if (null != (initialFolder = this.getInitialFolderFrom(newFile))) {
            chooser.setCurrentDirectory(initialFolder);
        }
        File origFile = newFile;
        do {
            NotifyDescriptor nd;
            if (0 != chooser.showSaveDialog(WindowManager.getDefault().getMainWindow())) {
                return null;
            }
            newFile = chooser.getSelectedFile();
            if (null == newFile) break;
            if (newFile.equals(origFile)) {
                nd = new NotifyDescriptor((Object)NbBundle.getMessage(DataObject.class, (String)"MSG_SaveAs_SameFileSelected"), NbBundle.getMessage(DataObject.class, (String)"MSG_SaveAs_SameFileSelected_Title"), -1, 1, new Object[]{NotifyDescriptor.OK_OPTION}, NotifyDescriptor.OK_OPTION);
                DialogDisplayer.getDefault().notify(nd);
                continue;
            }
            if (!newFile.exists()) break;
            nd = new NotifyDescriptor((Object)NbBundle.getMessage(DataObject.class, (String)"MSG_SaveAs_OverwriteQuestion", (Object)newFile.getName()), NbBundle.getMessage(DataObject.class, (String)"MSG_SaveAs_OverwriteQuestion_Title"), 0, 3, new Object[]{NotifyDescriptor.NO_OPTION, NotifyDescriptor.YES_OPTION}, NotifyDescriptor.NO_OPTION);
            if (NotifyDescriptor.YES_OPTION == DialogDisplayer.getDefault().notify(nd)) break;
        } while (true);
        if (this.isFromUserDir(currentFile)) {
            File lastUsedDir = chooser.getCurrentDirectory();
            NbPreferences.forModule(SaveAction.class).put("lastUsedDir", lastUsedDir.getAbsolutePath());
        }
        return newFile;
    }

    private FileObject getCurrentFileObject() {
        DataObject dob;
        TopComponent tc = TopComponent.getRegistry().getActivated();
        if (null != tc && null != (dob = (DataObject)tc.getLookup().lookup(DataObject.class))) {
            return dob.getPrimaryFile();
        }
        return null;
    }

    private File getInitialFolderFrom(File newFile) {
        File res = new File(System.getProperty("user.home"));
        if (null != newFile) {
            File parent = newFile.getParentFile();
            if (this.isFromUserDir(parent)) {
                String strLastUsedDir = NbPreferences.forModule(SaveAction.class).get("lastUsedDir", res.getAbsolutePath());
                res = new File(strLastUsedDir);
                if (!res.exists() || !res.isDirectory()) {
                    res = new File(System.getProperty("user.home"));
                }
            } else {
                res = parent;
            }
        }
        return res;
    }

    private boolean isFromUserDir(File file) {
        if (null == file) {
            return false;
        }
        File nbUserDir = new File(System.getProperty("netbeans.user"));
        return file.getAbsolutePath().startsWith(nbUserDir.getAbsolutePath());
    }

    public Action createContextAwareInstance(Lookup actionContext) {
        return new SaveAsAction(actionContext, false);
    }

    @Override
    public synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
        super.addPropertyChangeListener(listener);
        this.refreshListeners();
    }

    @Override
    public synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
        super.removePropertyChangeListener(listener);
        Mutex.EVENT.readAccess(new Runnable(){

            @Override
            public void run() {
                SaveAsAction.this.refreshListeners();
            }
        });
    }

    private PropertyChangeListener createRegistryListener() {
        return WeakListeners.propertyChange((PropertyChangeListener)new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                SaveAsAction.this.isDirty = true;
            }
        }, (Object)TopComponent.getRegistry());
    }

    private LookupListener createLookupListener() {
        return (LookupListener)WeakListeners.create(LookupListener.class, (EventListener)new LookupListener(){

            public void resultChanged(LookupEvent ev) {
                SaveAsAction.this.isDirty = true;
            }
        }, this.lkpInfo);
    }

    private void refreshEnabled() {
        TopComponent tc;
        if (this.lkpInfo == null) {
            Lookup.Template tpl = new Lookup.Template(SaveAsCapable.class);
            this.lkpInfo = this.context.lookup(tpl);
        }
        boolean isEditorWindowActivated = null != (tc = TopComponent.getRegistry().getActivated()) && WindowManager.getDefault().isEditorTopComponent(tc);
        this.setEnabled(null != this.lkpInfo && this.lkpInfo.allItems().size() != 0 && isEditorWindowActivated);
        this.isDirty = false;
    }

    private void refreshListeners() {
        assert (SwingUtilities.isEventDispatchThread());
        if (this.lkpInfo == null) {
            Lookup.Template tpl = new Lookup.Template(SaveAsCapable.class);
            this.lkpInfo = this.context.lookup(tpl);
        }
        if (null == this.changeSupport || !this.changeSupport.hasListeners("enabled")) {
            if (this.isGlobal && null != this.registryListener) {
                TopComponent.getRegistry().removePropertyChangeListener(this.registryListener);
                this.registryListener = null;
            }
            if (null != this.lookupListener) {
                this.lkpInfo.removeLookupListener(this.lookupListener);
                this.lookupListener = null;
            }
        } else {
            if (null == this.registryListener) {
                this.registryListener = this.createRegistryListener();
                TopComponent.getRegistry().addPropertyChangeListener(this.registryListener);
            }
            if (null == this.lookupListener) {
                this.lookupListener = this.createLookupListener();
                this.lkpInfo.addLookupListener(this.lookupListener);
            }
            this.refreshEnabled();
        }
    }

    boolean _isEnabled() {
        return super.isEnabled();
    }

}

