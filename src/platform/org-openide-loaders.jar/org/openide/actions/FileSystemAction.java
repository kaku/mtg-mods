/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.JInlineMenu
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.Enumerations
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 *  org.openide.windows.WindowManager
 */
package org.openide.actions;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenuItem;
import org.openide.actions.FileSystemRefreshAction;
import org.openide.awt.JInlineMenu;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataShadow;
import org.openide.nodes.Node;
import org.openide.util.ContextAwareAction;
import org.openide.util.Enumerations;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.WeakListeners;
import org.openide.util.actions.Presenter;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class FileSystemAction
extends SystemAction
implements ContextAwareAction,
Presenter.Menu,
Presenter.Popup {
    static JMenuItem[] NONE = new JMenuItem[0];

    private static Node[] nodes(Lookup lookup) {
        Collection c = lookup != null ? lookup.lookupAll(Node.class) : Collections.emptyList();
        return c.toArray((T[])new Node[c.size()]);
    }

    static JMenuItem[] createMenu(boolean popUp, Lookup lookup) {
        Node[] n = FileSystemAction.nodes(lookup);
        if (n == null) {
            n = WindowManager.getDefault().getRegistry().getActivatedNodes();
        }
        HashMap<FileSystem, LinkedHashSet<FileObject>> fsSet = new HashMap<FileSystem, LinkedHashSet<FileObject>>();
        LinkedList<DataObject> l = new LinkedList<DataObject>();
        if (n == null || n.length == 0) {
            l.addAll(lookup.lookupAll(DataObject.class));
        } else {
            for (Node node : n) {
                DataObject obj = (DataObject)node.getCookie(DataObject.class);
                if (obj == null) continue;
                l.add(obj);
            }
        }
        if (!l.isEmpty()) {
            Iterator i$ = l.iterator();
            while (i$.hasNext()) {
                DataObject obj = (DataObject)i$.next();
                while (obj instanceof DataShadow) {
                    obj = ((DataShadow)obj).getOriginal();
                }
                if (obj == null) continue;
                try {
                    FileSystem fs = obj.getPrimaryFile().getFileSystem();
                    LinkedHashSet<FileObject> foSet = (LinkedHashSet<FileObject>)fsSet.get((Object)fs);
                    if (foSet == null) {
                        foSet = new LinkedHashSet<FileObject>();
                        fsSet.put(fs, foSet);
                    }
                    foSet.addAll(obj.files());
                }
                catch (FileStateInvalidException ex) {}
            }
            if (fsSet.size() == 0 || fsSet.size() > 1) {
                return FileSystemAction.createMenu(Enumerations.empty(), popUp, lookup);
            }
            LinkedList<SystemAction> result = new LinkedList<SystemAction>();
            LinkedHashSet<FileObject> backSet = new LinkedHashSet<FileObject>();
            for (Map.Entry entry : fsSet.entrySet()) {
                FileSystem fs = (FileSystem)entry.getKey();
                Set foSet = (Set)entry.getValue();
                LinkedList backupList = new LinkedList(foSet);
                Iterator it = backupList.iterator();
                while (it.hasNext()) {
                    FileObject fo = (FileObject)it.next();
                    try {
                        if (fo.getFileSystem() == fs) continue;
                        it.remove();
                    }
                    catch (FileStateInvalidException ex) {
                        it.remove();
                    }
                }
                backSet.addAll(backupList);
                result.addAll(fs.findExtrasFor(backSet).lookupAll(Action.class));
            }
            if (FileSystemAction.isManualRefresh()) {
                result.add(FileSystemRefreshAction.get(FileSystemRefreshAction.class));
            }
            return FileSystemAction.createMenu(Collections.enumeration(result), popUp, (Lookup)FileSystemAction.createProxyLookup(lookup, backSet));
        }
        return NONE;
    }

    private static boolean isManualRefresh() {
        return NbPreferences.root().node("org/openide/actions/FileSystemRefreshAction").getBoolean("manual", false);
    }

    private static ProxyLookup createProxyLookup(Lookup lookup, Set<FileObject> backSet) {
        return new ProxyLookup(new Lookup[]{lookup, Lookups.fixed((Object[])((Object[])backSet.toArray((T[])new FileObject[backSet.size()])))});
    }

    static JMenuItem[] createMenu(Enumeration<? extends Action> en, boolean popUp, Lookup lookup) {
        en = Enumerations.removeDuplicates(en);
        ArrayList<JMenuItem> items = new ArrayList<JMenuItem>();
        while (en.hasMoreElements()) {
            Action a = (Action)en.nextElement();
            if (lookup != null && a instanceof ContextAwareAction) {
                a = ((ContextAwareAction)a).createContextAwareInstance(lookup);
            }
            boolean enabled = false;
            try {
                enabled = a.isEnabled();
            }
            catch (RuntimeException e) {
                Exceptions.attachMessage((Throwable)e, (String)("Guilty action: " + a.getClass().getName()));
                Exceptions.printStackTrace((Throwable)e);
            }
            if (!enabled) continue;
            JMenuItem item = null;
            if (popUp) {
                if (a instanceof Presenter.Popup) {
                    item = ((Presenter.Popup)a).getPopupPresenter();
                }
            } else if (a instanceof Presenter.Menu) {
                item = ((Presenter.Menu)a).getMenuPresenter();
            }
            if (item == null) continue;
            items.add(item);
        }
        JMenuItem[] array = new JMenuItem[items.size()];
        items.toArray(array);
        return array;
    }

    public JMenuItem getMenuPresenter() {
        return new Menu(false, null);
    }

    public JMenuItem getPopupPresenter() {
        return new Menu(true, null);
    }

    public String getName() {
        return NbBundle.getMessage(DataObject.class, (String)"ACT_FileSystemAction");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(FileSystemAction.class);
    }

    public void actionPerformed(ActionEvent e) {
        assert (false);
    }

    public Action createContextAwareInstance(Lookup actionContext) {
        return new DelegateAction(actionContext);
    }

    private static final class DelegateAction
    extends AbstractAction
    implements Presenter.Menu,
    Presenter.Popup {
        private Lookup lookup;

        public DelegateAction(Lookup lookup) {
            this.lookup = lookup;
        }

        public JMenuItem getMenuPresenter() {
            return new Menu(false, this.lookup);
        }

        public JMenuItem getPopupPresenter() {
            return new Menu(true, this.lookup);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            assert (false);
        }
    }

    private static class Menu
    extends JInlineMenu
    implements PropertyChangeListener {
        private boolean popup;
        private JMenuItem[] last = FileSystemAction.NONE;
        private Lookup lookup;
        static final long serialVersionUID = 2650151487189209766L;
        boolean needsChange = false;

        Menu(boolean popup, Lookup lookup) {
            this.popup = popup;
            this.lookup = lookup;
            this.changeMenuItems(FileSystemAction.createMenu(popup, lookup));
            if (lookup == null) {
                TopComponent.Registry r = WindowManager.getDefault().getRegistry();
                r.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)r));
            }
        }

        synchronized void changeMenuItems(JMenuItem[] items) {
            this.removeListeners(this.last);
            this.addListeners(items);
            this.last = items;
            this.setMenuItems(items);
        }

        private void addListeners(JMenuItem[] items) {
            int len = items.length;
            for (int i = 0; i < len; ++i) {
                items[i].addPropertyChangeListener(this);
            }
        }

        private void removeListeners(JMenuItem[] items) {
            int len = items.length;
            for (int i = 0; i < len; ++i) {
                items[i].removePropertyChangeListener(this);
            }
        }

        public void addNotify() {
            if (this.needsChange) {
                this.changeMenuItems(FileSystemAction.createMenu(this.popup, this.lookup));
                this.needsChange = false;
            }
            super.addNotify();
        }

        public void removeNotify() {
            this.removeListeners(this.last);
            this.last = FileSystemAction.NONE;
        }

        @Override
        public void propertyChange(PropertyChangeEvent ev) {
            String name = ev.getPropertyName();
            if (name == null || name.equals("enabled") || name.equals("activatedNodes")) {
                this.needsChange = true;
            }
        }
    }

}

