/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.api.queries.FileEncodingQuery
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.filesystems.FileUtil
 *  org.openide.nodes.CookieSet
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.nodes.NodeAdapter
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeOp
 *  org.openide.text.CloneableEditor
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.CloneableEditorSupport$Env
 *  org.openide.text.CloneableEditorSupport$Pane
 *  org.openide.text.Line
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.Parameters
 *  org.openide.util.UserQuestionException
 *  org.openide.util.WeakListeners
 *  org.openide.util.WeakSet
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 *  org.openide.windows.CloneableOpenSupport
 *  org.openide.windows.CloneableOpenSupport$Env
 *  org.openide.xml.XMLUtil
 */
package org.openide.text;

import java.awt.EventQueue;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedOutputStream;
import java.io.CharConversionException;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectInputValidation;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.StyledDocument;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.api.queries.FileEncodingQuery;
import org.netbeans.modules.openide.loaders.DataObjectAccessor;
import org.netbeans.modules.openide.loaders.SimpleES;
import org.netbeans.modules.openide.loaders.UIException;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.OpenSupport;
import org.openide.loaders.SaveAsCapable;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;
import org.openide.nodes.NodeAdapter;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeOp;
import org.openide.text.CloneableEditor;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.Line;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.Parameters;
import org.openide.util.UserQuestionException;
import org.openide.util.WeakListeners;
import org.openide.util.WeakSet;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.windows.CloneableOpenSupport;
import org.openide.xml.XMLUtil;

public class DataEditorSupport
extends CloneableEditorSupport {
    static final Logger ERR = Logger.getLogger("org.openide.text.DataEditorSupport");
    private final DataObject obj;
    private NodeListener nodeL;
    static boolean TABNAMES_HTML = Boolean.parseBoolean(System.getProperty("nb.tabnames.html", "true"));
    private static Set<FileObject> warnedEncodingFiles = new WeakSet();
    private static Map<DataObject, Charset> charsets = Collections.synchronizedMap(new HashMap());
    private static final Map<DataObject, Integer> cacheCounter = Collections.synchronizedMap(new HashMap());

    public DataEditorSupport(DataObject obj, CloneableEditorSupport.Env env) {
        this(obj, (Lookup)new DOEnvLookup(obj), env);
    }

    public DataEditorSupport(DataObject obj, @NullAllowed Lookup lkp, CloneableEditorSupport.Env env) {
        super(env, lkp == null ? obj.getLookup() : lkp);
        this.obj = obj;
    }

    final CloneableEditorSupport.Env desEnv() {
        return (CloneableEditorSupport.Env)this.env;
    }

    public static CloneableEditorSupport create(DataObject obj, MultiDataObject.Entry entry, CookieSet set) {
        return new SimpleES(obj, entry, set);
    }

    public static CloneableEditorSupport create(DataObject obj, MultiDataObject.Entry entry, CookieSet set, @NullAllowed Callable<CloneableEditorSupport.Pane> paneFactory) {
        return new SimpleES(obj, entry, set, paneFactory);
    }

    public final DataObject getDataObject() {
        return this.obj;
    }

    protected String messageOpening() {
        return NbBundle.getMessage(DataObject.class, (String)"CTL_ObjectOpen", (Object)this.obj.getPrimaryFile().getNameExt(), (Object)FileUtil.getFileDisplayName((FileObject)this.obj.getPrimaryFile()));
    }

    protected String messageOpened() {
        return null;
    }

    protected String messageSave() {
        return NbBundle.getMessage(DataObject.class, (String)"MSG_SaveFile", (Object)this.obj.getPrimaryFile().getNameExt());
    }

    public static String annotateName(String label, boolean html, boolean modified, boolean readOnly) {
        Parameters.notNull((CharSequence)"original", (Object)label);
        if (html && TABNAMES_HTML) {
            if (label.startsWith("<html>")) {
                label = label.substring(6);
            }
            if (modified) {
                label = "<b>" + label + "</b>";
            }
            if (readOnly) {
                label = "<i>" + label + "</i>";
            }
            return "<html>" + label;
        }
        if (html && !label.startsWith("<html>")) {
            label = "<html>" + label;
        }
        int version = modified ? (readOnly ? 2 : 1) : (readOnly ? 0 : 3);
        try {
            return NbBundle.getMessage(DataObject.class, (String)"LAB_EditorName", (Object)version, (Object)label);
        }
        catch (IllegalArgumentException iae) {
            String pattern = NbBundle.getMessage(DataObject.class, (String)"LAB_EditorName");
            ERR.log(Level.WARNING, "#166035: formatting failed. pattern=" + pattern + ", version=" + version + ", name=" + label, iae);
            return label;
        }
    }

    protected String messageName() {
        if (!this.obj.isValid()) {
            return "";
        }
        return DataEditorSupport.annotateName(this.obj.getNodeDelegate().getDisplayName(), false, this.isModified(), !this.obj.getPrimaryFile().canWrite());
    }

    protected String messageHtmlName() {
        if (!this.obj.isValid()) {
            return null;
        }
        String name = this.obj.getNodeDelegate().getHtmlDisplayName();
        if (name == null) {
            try {
                name = XMLUtil.toElementContent((String)this.obj.getNodeDelegate().getDisplayName());
            }
            catch (CharConversionException ex) {
                return null;
            }
        }
        return DataEditorSupport.annotateName(name, true, this.isModified(), !this.obj.getPrimaryFile().canWrite());
    }

    protected String documentID() {
        if (!this.obj.isValid()) {
            return "";
        }
        return this.obj.getPrimaryFile().getNameExt();
    }

    public static String toolTip(FileObject file, boolean modified, boolean readOnly) {
        String tip = FileUtil.getFileDisplayName((FileObject)file);
        if (TABNAMES_HTML) {
            if (modified) {
                tip = tip + NbBundle.getMessage(DataObject.class, (String)"TIP_editor_modified");
            }
            if (readOnly) {
                tip = tip + NbBundle.getMessage(DataObject.class, (String)"TIP_editor_ro");
            }
        }
        return tip;
    }

    protected String messageToolTip() {
        return DataEditorSupport.toolTip(this.obj.getPrimaryFile(), this.isModified(), !this.obj.getPrimaryFile().canWrite());
    }

    protected String messageLine(Line line) {
        return NbBundle.getMessage(DataObject.class, (String)"FMT_LineDisplayName2", (Object)this.obj.getPrimaryFile().getNameExt(), (Object)FileUtil.getFileDisplayName((FileObject)this.obj.getPrimaryFile()), (Object)new Integer(line.getLineNumber() + 1));
    }

    protected void initializeCloneableEditor(CloneableEditor editor) {
        if (this.obj.isValid()) {
            Node ourNode = this.obj.getNodeDelegate();
            editor.setActivatedNodes(new Node[]{ourNode});
            editor.setIcon(ourNode.getIcon(1));
            DataNodeListener nl = new DataNodeListener(editor);
            ourNode.addNodeListener(NodeOp.weakNodeListener((NodeListener)nl, (Object)ourNode));
            this.nodeL = nl;
        }
    }

    protected void notifyClosed() {
        this.nodeL = null;
        super.notifyClosed();
    }

    protected StyledDocument createStyledDocument(EditorKit kit) {
        StyledDocument doc = super.createStyledDocument(kit);
        doc.putProperty("title", FileUtil.getFileDisplayName((FileObject)this.obj.getPrimaryFile()));
        doc.putProperty("stream", this.obj);
        Logger.getLogger("TIMER").log(Level.FINE, "Document", new Object[]{this.obj.getPrimaryFile(), doc});
        return doc;
    }

    protected boolean canClose() {
        if (this.desEnv().isModified() && this.isEnvReadOnly()) {
            Object result = DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Confirmation((Object)NbBundle.getMessage(DataObject.class, (String)"MSG_FileReadOnlyClosing", (Object[])new Object[]{((Env)this.env).getFileImpl().getNameExt()}), 2, 2));
            return result == NotifyDescriptor.OK_OPTION;
        }
        return super.canClose();
    }

    protected void loadFromStreamToKit(StyledDocument doc, InputStream stream, EditorKit kit) throws IOException, BadLocationException {
        InputStreamReader r;
        Charset c = charsets.get(this.getDataObject());
        if (c == null) {
            c = FileEncodingQuery.getEncoding((FileObject)this.getDataObject().getPrimaryFile());
        }
        FileObject fo = this.getDataObject().getPrimaryFile();
        doc.putProperty("default-line-separator", fo.getAttribute("default-line-separator"));
        if (warnedEncodingFiles.contains((Object)fo)) {
            r = new InputStreamReader(stream, c);
        } else {
            CharsetDecoder decoder = c.newDecoder();
            decoder.reset();
            r = new InputStreamReader(stream, decoder);
        }
        try {
            kit.read(r, (Document)doc, 0);
        }
        catch (CharacterCodingException e) {
            ERR.log(Level.FINE, "Encoding problem using " + c, e);
            doc.remove(0, doc.getLength());
            DataEditorSupport.createAndThrowIncorrectCharsetUQE(fo, c);
        }
        catch (IllegalStateException e) {
            ERR.log(Level.FINE, "Encoding problem using " + c, e);
            doc.remove(0, doc.getLength());
            DataEditorSupport.createAndThrowIncorrectCharsetUQE(fo, c);
        }
    }

    private static boolean createAndThrowIncorrectCharsetUQE(final FileObject fo, Charset charset) throws UserQuestionException {
        ERR.log(Level.INFO, "Encoding problem using {0} for {1}", new Object[]{charset, fo});
        throw new UserQuestionException(NbBundle.getMessage(DataObject.class, (String)"MSG_EncodingProblem", (Object)charset, (Object)fo.getPath())){

            public void confirmed() throws IOException {
                warnedEncodingFiles.add(fo);
            }
        };
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static int incrementCacheCounter(DataObject tmpObj) {
        Map<DataObject, Integer> map = cacheCounter;
        synchronized (map) {
            Integer count = cacheCounter.get(tmpObj);
            if (count == null) {
                count = 0;
            }
            Integer n = count;
            Integer n2 = count = Integer.valueOf(count + 1);
            cacheCounter.put(tmpObj, count);
            return count;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static synchronized int decrementCacheCounter(DataObject tmpObj) {
        Map<DataObject, Integer> map = cacheCounter;
        synchronized (map) {
            Integer count = cacheCounter.get(tmpObj);
            assert (count != null);
            Integer n = count;
            Integer n2 = count = Integer.valueOf(count - 1);
            if (count == 0) {
                cacheCounter.remove(tmpObj);
            } else {
                cacheCounter.put(tmpObj, count);
            }
            return count;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void saveFromKitToStream(StyledDocument doc, EditorKit kit, OutputStream stream) throws IOException, BadLocationException {
        if (doc == null) {
            throw new NullPointerException("Document is null");
        }
        if (kit == null) {
            throw new NullPointerException("Kit is null");
        }
        Charset c = charsets.get(this.getDataObject());
        if (c == null) {
            c = FileEncodingQuery.getEncoding((FileObject)this.getDataObject().getPrimaryFile());
        }
        FilterOutputStream fos = new FilterOutputStream(stream){

            @Override
            public void close() throws IOException {
                this.flush();
            }
        };
        OutputStreamWriter w = new OutputStreamWriter((OutputStream)fos, c);
        try {
            kit.write(w, (Document)doc, 0, doc.getLength());
        }
        finally {
            w.close();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public StyledDocument openDocument() throws IOException {
        DataObject tmpObj = this.getDataObject();
        Charset c = charsets.get(tmpObj);
        if (c == null) {
            c = FileEncodingQuery.getEncoding((FileObject)tmpObj.getPrimaryFile());
        }
        try {
            charsets.put(tmpObj, c);
            DataEditorSupport.incrementCacheCounter(tmpObj);
            StyledDocument styledDocument = super.openDocument();
            return styledDocument;
        }
        finally {
            if (DataEditorSupport.decrementCacheCounter(tmpObj) == 0) {
                charsets.remove(tmpObj);
            }
            ERR.finest("openDocument - charset removed");
        }
    }

    public void saveDocument() throws IOException {
        SaveImpl aa = new SaveImpl(this);
        FileUtil.runAtomicAction((FileSystem.AtomicAction)aa);
    }

    final void superSaveDoc() throws IOException {
        super.saveDocument();
    }

    private boolean isEnvReadOnly() {
        CloneableEditorSupport.Env myEnv = this.desEnv();
        return myEnv instanceof Env && !((Env)myEnv).getFileImpl().canWrite();
    }

    final DataObject getDataObjectHack2() {
        return this.obj;
    }

    final void callUpdateTitles() {
        this.updateTitles();
    }

    public static DataObject findDataObject(Line l) {
        if (l == null) {
            throw new NullPointerException();
        }
        return (DataObject)l.getLookup().lookup(DataObject.class);
    }

    public void saveAs(FileObject folder, String fileName) throws IOException {
        if (this.env instanceof Env) {
            OpenCookie c;
            String newExtension = FileUtil.getExtension((String)fileName);
            DataObject newDob = null;
            DataObject currentDob = this.getDataObject();
            if (!currentDob.isModified() || null == this.getDocument()) {
                DataFolder df = DataFolder.findFolder(folder);
                FileObject newFile = folder.getFileObject(fileName);
                if (null != newFile) {
                    newFile.delete();
                }
                newDob = DataObjectAccessor.DEFAULT.copyRename(currentDob, df, this.getFileNameNoExtension(fileName), newExtension);
            } else {
                FileObject newFile = FileUtil.createData((FileObject)folder, (String)fileName);
                this.saveDocumentAs(newFile.getOutputStream());
                currentDob.setModified(false);
                newDob = DataObject.find(newFile);
            }
            if (null != newDob && null != (c = newDob.getCookie(OpenCookie.class))) {
                this.close(false);
                c.open();
            }
        }
    }

    private String getFileNameNoExtension(String fileName) {
        int index = fileName.lastIndexOf(".");
        if (index == -1) {
            return fileName;
        }
        return fileName.substring(0, index);
    }

    private void saveDocumentAs(OutputStream output) throws IOException {
        class SaveAsWriter
        implements Runnable {
            private IOException ex;
            final /* synthetic */ OutputStream val$output;
            final /* synthetic */ StyledDocument val$myDoc;

            SaveAsWriter() {
                this.val$output = var2_2;
                this.val$myDoc = var3_3;
            }

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                try {
                    BufferedOutputStream os = null;
                    try {
                        os = new BufferedOutputStream(this.val$output);
                        this$0.saveFromKitToStream(this.val$myDoc, os);
                        os.close();
                        os = null;
                    }
                    catch (BadLocationException blex) {
                        DataEditorSupport.ERR.log(Level.INFO, null, blex);
                    }
                    finally {
                        if (os != null) {
                            os.close();
                        }
                    }
                }
                catch (IOException e) {
                    this.ex = e;
                }
            }

            public void after() throws IOException {
                if (this.ex != null) {
                    throw this.ex;
                }
            }
        }
        StyledDocument myDoc = this.getDocument();
        SaveAsWriter saveAsWriter = new SaveAsWriter(this, output, myDoc);
        myDoc.render(saveAsWriter);
        saveAsWriter.after();
    }

    private void saveFromKitToStream(StyledDocument myDoc, OutputStream os) throws IOException, BadLocationException {
        EditorKit kit = this.createEditorKit();
        this.saveFromKitToStream(myDoc, kit, os);
    }

    private static class SaveImpl
    implements FileSystem.AtomicAction {
        private static final SaveImpl DEFAULT = new SaveImpl(null);
        private final DataEditorSupport des;

        public SaveImpl(DataEditorSupport des) {
            this.des = des;
        }

        public void run() throws IOException {
            if (this.des.desEnv().isModified() && this.des.isEnvReadOnly()) {
                IOException e = new IOException("File is read-only: " + (Object)((Env)this.des.env).getFileImpl());
                UIException.annotateUser(e, null, NbBundle.getMessage(DataObject.class, (String)"MSG_FileReadOnlySaving", (Object[])new Object[]{((Env)this.des.env).getFileImpl().getNameExt()}), null, null);
                throw e;
            }
            DataObject tmpObj = this.des.getDataObject();
            Charset c = (Charset)charsets.get(tmpObj);
            if (c == null) {
                c = FileEncodingQuery.getEncoding((FileObject)tmpObj.getPrimaryFile());
            }
            try {
                charsets.put(tmpObj, c);
                DataEditorSupport.incrementCacheCounter(tmpObj);
                DataEditorSupport.ERR.finest("SaveImpl - charset put");
                try {
                    this.des.superSaveDoc();
                }
                catch (UserQuestionException ex) {
                    NotifyDescriptor.Confirmation nd = new NotifyDescriptor.Confirmation((Object)ex.getLocalizedMessage(), 0);
                    Object res = DialogDisplayer.getDefault().notify((NotifyDescriptor)nd);
                    if (NotifyDescriptor.OK_OPTION.equals(res)) {
                        ex.confirmed();
                    }
                }
            }
            catch (IOException e) {
                UIException.annotateUser(e, null, NbBundle.getMessage(DataObject.class, (String)"MSG_SaveAsFailed", (Object[])new Object[]{((Env)this.des.env).getFileImpl().getNameExt(), e.getLocalizedMessage()}), null, new Date());
                throw e;
            }
            finally {
                if (DataEditorSupport.decrementCacheCounter(tmpObj) == 0) {
                    charsets.remove(tmpObj);
                }
            }
        }

        public int hashCode() {
            return this.getClass().hashCode();
        }

        public boolean equals(Object obj) {
            return obj != null && this.getClass() == obj.getClass();
        }
    }

    private static class DOEnvLookup
    extends AbstractLookup
    implements PropertyChangeListener {
        static final long serialVersionUID = 333;
        private DataObject dobj;
        private InstanceContent ic;

        public DOEnvLookup(DataObject dobj) {
            this(dobj, new InstanceContent());
        }

        private DOEnvLookup(DataObject dobj, InstanceContent ic) {
            super((AbstractLookup.Content)ic);
            this.ic = ic;
            this.dobj = dobj;
            dobj.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)dobj));
            this.updateLookup();
        }

        private void updateLookup() {
            this.ic.set(Arrays.asList(new Object[]{this.dobj, this.dobj.getPrimaryFile()}), null);
        }

        @Override
        public void propertyChange(PropertyChangeEvent ev) {
            String propName = ev.getPropertyName();
            if (propName == null || propName.equals("primaryFile")) {
                this.updateLookup();
            }
        }
    }

    private final class DataNodeListener
    extends NodeAdapter {
        CloneableEditor editor;

        DataNodeListener(CloneableEditor editor) {
            this.editor = editor;
        }

        public void propertyChange(final PropertyChangeEvent ev) {
            Mutex.EVENT.writeAccess(new Runnable(){

                @Override
                public void run() {
                    if ("displayName".equals(ev.getPropertyName())) {
                        DataEditorSupport.this.callUpdateTitles();
                    }
                    if ("icon".equals(ev.getPropertyName()) && DataEditorSupport.this.obj.isValid()) {
                        DataNodeListener.this.editor.setIcon(DataEditorSupport.this.obj.getNodeDelegate().getIcon(1));
                    }
                }
            });
        }

    }

    private static final class EnvListener
    extends FileChangeAdapter {
        private Reference<Env> env;

        public EnvListener(FileObject listen, Env env) {
            this.env = new WeakReference<Env>(env);
            this.addFileChangeListener(listen);
        }

        private void addFileChangeListener(FileObject fo) {
            try {
                fo.getFileSystem().addFileChangeListener((FileChangeListener)this);
            }
            catch (FileStateInvalidException ex) {
                DataEditorSupport.ERR.log(Level.INFO, "cannot add listener to " + (Object)fo, (Throwable)ex);
            }
        }

        private void removeFileChangeListener(FileObject fo) {
            try {
                fo.getFileSystem().removeFileChangeListener((FileChangeListener)this);
            }
            catch (FileStateInvalidException ex) {
                DataEditorSupport.ERR.log(Level.INFO, "cannot remove listener from " + (Object)fo, (Throwable)ex);
            }
        }

        public void fileDeleted(FileEvent fe) {
            Env myEnv = this.env.get();
            FileObject fo = fe.getFile();
            if (myEnv != null) {
                myEnv.updateDocumentProperty();
            }
            if (myEnv == null || myEnv.getFileImpl() == null) {
                this.removeFileChangeListener(fo);
                return;
            }
            if (myEnv.getFileImpl() == fo) {
                this.removeFileChangeListener(fo);
                myEnv.fileRemoved(true);
                this.addFileChangeListener(fo);
            }
        }

        public void fileChanged(FileEvent fe) {
            if (fe.firedFrom((FileSystem.AtomicAction)DEFAULT)) {
                return;
            }
            Env myEnv = this.env.get();
            if (myEnv == null || myEnv.getFileImpl() == null) {
                this.removeFileChangeListener(fe.getFile());
                return;
            }
            if (myEnv.getFileImpl() != fe.getFile()) {
                return;
            }
            if (fe.getFile().isVirtual()) {
                fe.getFile().removeFileChangeListener((FileChangeListener)this);
                myEnv.fileRemoved(true);
                fe.getFile().addFileChangeListener((FileChangeListener)this);
            } else {
                myEnv.fileChanged(fe);
            }
        }

        public void fileRenamed(FileRenameEvent fe) {
            Env myEnv = this.env.get();
            if (myEnv != null) {
                myEnv.updateDocumentProperty();
                myEnv.fileRenamed();
            }
        }

        public void fileAttributeChanged(FileAttributeEvent fae) {
            Env myEnv = this.env.get();
            if (myEnv == null || myEnv.getFileImpl() != fae.getFile()) {
                return;
            }
            if ("DataEditorSupport.read-only.refresh".equals(fae.getName())) {
                myEnv.readOnlyRefresh();
            }
        }
    }

    public static abstract class Env
    extends OpenSupport.Env
    implements CloneableEditorSupport.Env {
        private static final long serialVersionUID = -2945098431098324441L;
        private transient FileObject fileObject;
        transient FileLock fileLock;
        private static transient Set<FileObject> warnedFiles = new HashSet<FileObject>();
        private transient FileSystem.AtomicAction action = null;
        private transient Boolean canWrite;
        private final int BIG_FILE_THRESHOLD_MB = Integer.getInteger("org.openide.text.big.file.size", 1);

        public Env(DataObject obj) {
            super(obj);
        }

        private FileObject getFileImpl() {
            this.changeFile();
            return this.fileObject;
        }

        protected abstract FileObject getFile();

        protected abstract FileLock takeLock() throws IOException;

        protected final void changeFile() {
            boolean lockAgain;
            FileObject newFile = this.getFile();
            if (newFile.equals((Object)this.fileObject)) {
                return;
            }
            if (this.fileLock != null) {
                if (this.fileLock.isValid()) {
                    DataEditorSupport.ERR.fine("changeFile releaseLock: " + (Object)this.fileLock + " for " + (Object)this.fileObject);
                    this.fileLock.releaseLock();
                    lockAgain = true;
                } else {
                    this.fileLock = null;
                    lockAgain = false;
                }
            } else {
                lockAgain = false;
            }
            boolean wasNull = this.fileObject == null;
            this.fileObject = newFile;
            DataEditorSupport.ERR.fine("changeFile: " + (Object)newFile + " for " + (Object)this.fileObject);
            new EnvListener(this.fileObject, this);
            if (lockAgain) {
                try {
                    this.fileLock = this.takeLock();
                    DataEditorSupport.ERR.fine("changeFile takeLock: " + (Object)this.fileLock + " for " + (Object)this.fileObject);
                }
                catch (IOException e) {
                    Logger.getLogger(DataEditorSupport.class.getName()).log(Level.WARNING, null, e);
                }
            }
            if (!wasNull) {
                this.firePropertyChange("expectedTime", null, this.getTime());
            }
        }

        public InputStream inputStream() throws IOException {
            FileObject fo = this.getFileImpl();
            if (!warnedFiles.contains((Object)fo) && fo.getSize() > (long)(this.BIG_FILE_THRESHOLD_MB * 1024 * 1024)) {
                throw new ME(fo.getSize());
            }
            this.initCanWrite(false);
            InputStream is = this.getFileImpl().getInputStream();
            return is;
        }

        public OutputStream outputStream() throws IOException {
            DataEditorSupport.ERR.fine("outputStream: " + (Object)this.fileLock + " for " + (Object)this.fileObject);
            if (this.fileLock == null || !this.fileLock.isValid()) {
                this.fileLock = this.takeLock();
            }
            DataEditorSupport.ERR.fine("outputStream after takeLock: " + (Object)this.fileLock + " for " + (Object)this.fileObject);
            try {
                return this.getFileImpl().getOutputStream(this.fileLock);
            }
            catch (IOException fse) {
                if (this.fileLock == null || !this.fileLock.isValid()) {
                    this.fileLock = this.takeLock();
                }
                DataEditorSupport.ERR.fine("ugly workaround for #40552: " + (Object)this.fileLock + " for " + (Object)this.fileObject);
                return this.getFileImpl().getOutputStream(this.fileLock);
            }
        }

        public Date getTime() {
            this.action = new FileSystem.AtomicAction(){

                public void run() throws IOException {
                    Env.this.getFileImpl().refresh(false);
                }
            };
            try {
                this.getFileImpl().getFileSystem().runAtomicAction(this.action);
            }
            catch (IOException ex) {
                // empty catch block
            }
            return this.getFileImpl().lastModified();
        }

        public String getMimeType() {
            return this.getFileImpl().getMIMEType();
        }

        @Override
        public void markModified() throws IOException {
            if (EventQueue.isDispatchThread()) {
                class Mark
                implements Runnable {
                    final AtomicBoolean cancel;
                    IOException error;

                    Mark(FileObject fo) {
                        this.cancel = new AtomicBoolean();
                        this.error = new IOException("Operation cancelled");
                        Exceptions.attachLocalizedMessage((Throwable)this.error, (String)NbBundle.getMessage(DataObject.class, (String)"MSG_MarkModifiedCancel", (Object)fo.getPath()));
                    }

                    @Override
                    public void run() {
                        try {
                            Env.this.markModifiedImpl(this.cancel);
                            this.error = null;
                        }
                        catch (IOException ex) {
                            this.error = ex;
                        }
                    }
                }
                Mark m = new Mark(this.fileObject);
                ProgressUtils.runOffEventDispatchThread((Runnable)m, (String)NbBundle.getMessage(DataObject.class, (String)"MSG_MarkModified", (Object)this.fileObject.getPath()), (AtomicBoolean)m.cancel, (boolean)false, (int)1000, (int)3000);
                IOException err = m.error;
                if (err != null) {
                    throw err;
                }
            } else {
                this.markModifiedImpl(null);
            }
        }

        private void markModifiedImpl(AtomicBoolean cancel) throws IOException {
            if (this.fileLock == null || !this.fileLock.isValid()) {
                this.fileLock = this.takeLock();
            }
            DataEditorSupport.ERR.fine("markModified: " + (Object)this.fileLock + " for " + (Object)this.fileObject);
            if (!this.getFileImpl().canWrite()) {
                if (this.fileLock != null && this.fileLock.isValid()) {
                    this.fileLock.releaseLock();
                }
                throw new IOException("File " + this.getFileImpl().getNameExt() + " is read-only!");
            }
            if (cancel == null || !cancel.get()) {
                this.getDataObject().setModified(true);
            }
        }

        @Override
        public void unmarkModified() {
            DataEditorSupport.ERR.fine("unmarkModified: " + (Object)this.fileLock + " for " + (Object)this.fileObject);
            if (this.fileLock != null && this.fileLock.isValid()) {
                this.fileLock.releaseLock();
                DataEditorSupport.ERR.fine("releaseLock: " + (Object)this.fileLock + " for " + (Object)this.fileObject);
            }
            this.getDataObject().setModified(false);
        }

        final void fileChanged(FileEvent fe) {
            DataEditorSupport.ERR.fine("fileChanged: " + fe.isExpected() + " for " + (Object)this.fileObject);
            if (this.action != null && fe.firedFrom(this.action)) {
                return;
            }
            if (fe.isExpected()) {
                this.firePropertyChange("time", null, null);
            } else {
                this.firePropertyChange("time", null, new Date(fe.getTime()));
            }
        }

        private boolean initCanWrite(boolean refresh) {
            if (this.canWrite == null || !refresh) {
                this.canWrite = this.getFileImpl().canWrite();
                return false;
            }
            boolean oldCanWrite = this.canWrite;
            this.canWrite = this.getFileImpl().canWrite();
            return oldCanWrite != this.canWrite;
        }

        private void readOnlyRefresh() {
            if (this.initCanWrite(true)) {
                if (!this.canWrite.booleanValue() && this.isModified()) {
                    DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)NbBundle.getMessage(DataObject.class, (String)"MSG_FileReadOnlyChanging", (Object[])new Object[]{this.getFileImpl().getNameExt()}), 2));
                }
                this.firePropertyChange("DataEditorSupport.read-only.changing", this.canWrite == false, this.canWrite);
            }
        }

        final void fileRemoved(boolean canBeVetoed) {
        }

        final void updateDocumentProperty() {
            StyledDocument doc;
            EditorCookie ec = this.getDataObject().getCookie(EditorCookie.class);
            if (ec != null && (doc = ec.getDocument()) != null) {
                doc.putProperty("title", FileUtil.getFileDisplayName((FileObject)this.getDataObject().getPrimaryFile()));
            }
        }

        final void fileRenamed() {
            this.firePropertyChange("expectedTime", null, this.getTime());
        }

        private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
            ois.defaultReadObject();
            ois.registerValidation(new ObjectInputValidation(){

                @Override
                public void validateObject() throws InvalidObjectException {
                    warnedFiles.add(Env.this.getFileImpl());
                }
            }, 0);
        }

        class ME
        extends UserQuestionException {
            static final long serialVersionUID = 1;
            private long size;

            public ME(long size) {
                super("The file is too big. " + size + " bytes.");
                this.size = size;
            }

            public String getLocalizedMessage() {
                Object[] arr = new Object[]{Env.this.getFileImpl().getPath(), Env.this.getFileImpl().getNameExt(), new Long(this.size), new Long(this.size / 1024 + 1), new Long(this.size / 0x100000), new Long(this.size / 0x40000000)};
                return NbBundle.getMessage(DataObject.class, (String)"MSG_ObjectIsTooBig", (Object[])arr);
            }

            public void confirmed() {
                warnedFiles.add(Env.this.getFileImpl());
            }
        }

        private class SaveAsCapableImpl
        implements SaveAsCapable {
            private SaveAsCapableImpl() {
            }

            @Override
            public void saveAs(FileObject folder, String fileName) throws IOException {
                CloneableOpenSupport cos = Env.this.findCloneableOpenSupport();
                if (cos instanceof DataEditorSupport) {
                    ((DataEditorSupport)cos).saveAs(folder, fileName);
                }
            }
        }

    }

}

