/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.UndoRedo
 *  org.openide.awt.UndoRedo$Manager
 *  org.openide.cookies.CloseCookie
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.EditorCookie$Observable
 *  org.openide.cookies.OpenCookie
 *  org.openide.cookies.PrintCookie
 *  org.openide.cookies.SaveCookie
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.CookieSet
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.nodes.NodeAdapter
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeOp
 *  org.openide.text.CloneableEditor
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.CloneableEditorSupport$Env
 *  org.openide.text.CloneableEditorSupport$Pane
 *  org.openide.text.Line
 *  org.openide.text.Line$Set
 *  org.openide.text.PositionRef
 *  org.openide.util.Exceptions
 *  org.openide.util.Mutex
 *  org.openide.util.Task
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.CloneableOpenSupport
 *  org.openide.windows.CloneableOpenSupport$Env
 *  org.openide.windows.CloneableTopComponent
 *  org.openide.windows.CloneableTopComponent$Ref
 */
package org.openide.text;

import java.awt.Container;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.OutputStream;
import java.lang.reflect.Method;
import javax.swing.JEditorPane;
import javax.swing.event.ChangeListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.EditorKit;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.openide.awt.UndoRedo;
import org.openide.cookies.CloseCookie;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.OpenCookie;
import org.openide.cookies.PrintCookie;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.OpenSupport;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;
import org.openide.nodes.NodeAdapter;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeOp;
import org.openide.text.CloneableEditor;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.DataEditorSupport;
import org.openide.text.Line;
import org.openide.text.PositionRef;
import org.openide.util.Exceptions;
import org.openide.util.Mutex;
import org.openide.util.Task;
import org.openide.util.actions.SystemAction;
import org.openide.windows.CloneableOpenSupport;
import org.openide.windows.CloneableTopComponent;

@Deprecated
public class EditorSupport
extends OpenSupport
implements EditorCookie.Observable,
OpenCookie,
CloseCookie,
PrintCookie {
    @Deprecated
    public static final String EDITOR_MODE = "editor";
    @Deprecated
    protected String modifiedAppendix = " *";
    private boolean listenToModifs = true;
    private Del del;

    public EditorSupport(MultiDataObject.Entry entry) {
        super(entry, new DelEnv(entry.getDataObject()));
        this.del = new Del(entry.getDataObject(), (DelEnv)this.env, this.allEditors);
    }

    @Override
    protected String messageOpening() {
        return this.del.superMessageOpening();
    }

    @Override
    protected String messageOpened() {
        return this.del.superMessageOpened();
    }

    protected String messageSave() {
        return this.del.superMessageSave();
    }

    protected String messageName() {
        return this.del.superMessageName();
    }

    protected String messageToolTip() {
        return this.del.superMessageToolTip();
    }

    protected void updateTitles() {
        this.del.superUpdateTitles();
    }

    protected CloneableTopComponent createCloneableTopComponent() {
        this.prepareDocument();
        MultiDataObject obj = this.findDataObject();
        Editor editor = new Editor(obj);
        return editor;
    }

    protected UndoRedo.Manager createUndoRedoManager() {
        return this.del.superUndoRedoManager();
    }

    public void open() {
        this.del.open();
    }

    public boolean close() {
        return this.del.close();
    }

    protected boolean close(boolean ask) {
        return this.del.superClose(ask);
    }

    public synchronized Task prepareDocument() {
        return this.del.prepareDocument();
    }

    public StyledDocument openDocument() throws IOException {
        return this.del.openDocument();
    }

    public StyledDocument getDocument() {
        return this.del.getDocument();
    }

    public boolean isDocumentLoaded() {
        return this.del.isDocumentLoaded();
    }

    public void saveDocument() throws IOException {
        this.del.superSaveDocument();
    }

    protected void saveFromKitToStream(StyledDocument doc, EditorKit kit, OutputStream stream) throws IOException, BadLocationException {
        this.del.superSaveFromKitToStream(doc, kit, stream);
    }

    public boolean isModified() {
        return this.del.isModified();
    }

    protected MultiDataObject findDataObject() {
        return this.entry.getDataObject();
    }

    public final PositionRef createPositionRef(int offset, Position.Bias bias) {
        return this.del.createPositionRef(offset, bias);
    }

    public Line.Set getLineSet() {
        return this.del.getLineSet();
    }

    public void setMIMEType(String s) {
        this.del.setMIMEType(s);
    }

    @Deprecated
    public void setActions(SystemAction[] actions) {
    }

    protected EditorKit createEditorKit() {
        return this.del.superCreateEditorKit();
    }

    public void setModificationListening(boolean listenToModifs) {
        this.listenToModifs = listenToModifs;
    }

    public void addChangeListener(ChangeListener l) {
        this.del.addChangeListener(l);
    }

    public void removeChangeListener(ChangeListener l) {
        this.del.removeChangeListener(l);
    }

    public final void addPropertyChangeListener(PropertyChangeListener l) {
        this.del.addPropertyChangeListener(l);
    }

    public final void removePropertyChangeListener(PropertyChangeListener l) {
        this.del.removePropertyChangeListener(l);
    }

    public void print() {
        this.del.print();
    }

    protected void loadFromStreamToKit(StyledDocument doc, InputStream stream, EditorKit kit) throws IOException, BadLocationException {
        this.del.superLoadFromStreamToKit(doc, stream, kit);
    }

    protected void reloadDocument() {
        this.reloadDocumentTask().waitFinished();
    }

    protected Task reloadDocumentTask() {
        return this.del.superReloadDocument();
    }

    protected Editor openAt(PositionRef pos) {
        CloneableEditorSupport.Pane p = this.del.openAt(pos, -1);
        if (p instanceof Editor) {
            return (Editor)p;
        }
        Container c = p.getEditorPane();
        while (!(c instanceof Editor)) {
            c = c.getParent();
        }
        return (Editor)((Object)c);
    }

    protected boolean canClose() {
        return this.del.superCanClose();
    }

    public JEditorPane[] getOpenedPanes() {
        return this.del.getOpenedPanes();
    }

    protected void notifyUnmodified() {
        this.modifySaveCookie(false);
        this.del.superNotifyUnmodified();
    }

    protected boolean notifyModified() {
        if (this.del.superNotifyModified()) {
            this.modifySaveCookie(true);
            return true;
        }
        return false;
    }

    protected void notifyClosed() {
        this.del.superNotifyClosed();
    }

    static EditorSupport extract(CloneableEditorSupport ces) {
        Del del = (Del)ces;
        return del.es();
    }

    final void modifySaveCookie(boolean add) {
        if (this.listenToModifs) {
            if (add) {
                ((EntryEnv)this.env).addSaveCookie();
            } else {
                ((EntryEnv)this.env).removeSaveCookie();
            }
        }
    }

    private static final class DelEnv
    extends EntryEnv {
        static final long serialVersionUID = 174320972368471234L;

        public DelEnv(MultiDataObject obj) {
            super(obj);
        }

        @Override
        public CloneableOpenSupport findCloneableOpenSupport() {
            CloneableOpenSupport o = super.findCloneableOpenSupport();
            if (o instanceof EditorSupport) {
                EditorSupport es = (EditorSupport)o;
                return es.del;
            }
            return o;
        }
    }

    private static class EntryEnv
    extends DataEditorSupport.Env
    implements SaveCookie {
        static final long serialVersionUID = 354528097109874355L;
        private static Method getCookieSetMethod = null;

        public EntryEnv(MultiDataObject obj) {
            super(obj);
        }

        @Override
        protected FileObject getFile() {
            return this.getDataObject().getPrimaryFile();
        }

        @Override
        protected FileLock takeLock() throws IOException {
            return ((MultiDataObject)this.getDataObject()).getPrimaryEntry().takeLock();
        }

        @Override
        public void propertyChange(PropertyChangeEvent ev) {
            EditorSupport es;
            if ("primaryFile".equals(ev.getPropertyName())) {
                this.changeFile();
            }
            if ("name".equals(ev.getPropertyName()) && (es = this.getDataObject().getCookie(EditorSupport.class)) != null) {
                es.updateTitles();
            }
            super.propertyChange(ev);
        }

        public void save() throws IOException {
            EditorSupport es = this.getDataObject().getCookie(EditorSupport.class);
            if (es == null) {
                throw new IOException("no EditorSupport found on this data object");
            }
            es.saveDocument();
        }

        final void addSaveCookie() {
            DataObject dataObj = this.getDataObject();
            if (dataObj instanceof MultiDataObject && dataObj.getCookie(SaveCookie.class) == null) {
                EntryEnv.getCookieSet((MultiDataObject)dataObj).add((Node.Cookie)this);
            }
        }

        final void removeSaveCookie() {
            DataObject dataObj = this.getDataObject();
            if (dataObj instanceof MultiDataObject && dataObj.getCookie(SaveCookie.class) == this) {
                EntryEnv.getCookieSet((MultiDataObject)dataObj).remove((Node.Cookie)this);
            }
        }

        private static final CookieSet getCookieSet(MultiDataObject obj) {
            try {
                if (getCookieSetMethod == null) {
                    getCookieSetMethod = MultiDataObject.class.getDeclaredMethod("getCookieSet", new Class[0]);
                    getCookieSetMethod.setAccessible(true);
                }
                return (CookieSet)getCookieSetMethod.invoke(obj, new Object[0]);
            }
            catch (Exception e) {
                Exceptions.printStackTrace((Throwable)e);
                return new CookieSet();
            }
        }

        @Override
        public CloneableOpenSupport findCloneableOpenSupport() {
            CloneableOpenSupport s = super.findCloneableOpenSupport();
            if (s != null) {
                return s;
            }
            EditorSupport es = this.getDataObject().getCookie(EditorSupport.class);
            if (es != null) {
                return es.del;
            }
            return null;
        }
    }

    private final class Del
    extends DataEditorSupport
    implements EditorCookie.Observable {
        private NodeListener nodeL;

        public Del(DataObject obj, CloneableEditorSupport.Env env, CloneableTopComponent.Ref ref) {
            super(obj, env);
            this.allEditors = ref;
        }

        public final EditorSupport es() {
            return EditorSupport.this;
        }

        protected void notifyUnmodified() {
            EditorSupport.this.notifyUnmodified();
        }

        protected boolean notifyModified() {
            return EditorSupport.this.notifyModified();
        }

        @Override
        protected void notifyClosed() {
            EditorSupport.this.notifyClosed();
        }

        final void superNotifyUnmodified() {
            super.notifyUnmodified();
        }

        final boolean superNotifyModified() {
            return super.notifyModified();
        }

        final void superNotifyClosed() {
            this.nodeL = null;
            super.notifyClosed();
        }

        protected CloneableEditor createCloneableEditor() {
            throw new IllegalStateException("Do not call!");
        }

        protected CloneableEditorSupport.Pane createPane() {
            CloneableTopComponent ctc = this.createCloneableTopComponent();
            if (ctc instanceof Editor) {
                return (CloneableEditor)ctc;
            }
            CloneableEditorSupport.Pane pan = (CloneableEditorSupport.Pane)ctc.getClientProperty((Object)"CloneableEditorSupport.Pane");
            if (pan != null) {
                return pan;
            }
            if (ctc instanceof CloneableEditorSupport.Pane) {
                return (CloneableEditorSupport.Pane)ctc;
            }
            return new Editor(this.getDataObject());
        }

        @Override
        protected String messageToolTip() {
            return EditorSupport.this.messageToolTip();
        }

        @Override
        protected String messageName() {
            return EditorSupport.this.messageName();
        }

        @Override
        protected String messageOpening() {
            return EditorSupport.this.messageOpening();
        }

        @Override
        protected String messageOpened() {
            return EditorSupport.this.messageOpened();
        }

        @Override
        protected String messageSave() {
            return EditorSupport.this.messageSave();
        }

        protected void updateTitles() {
            EditorSupport.this.updateTitles();
        }

        final String superMessageToolTip() {
            return super.messageToolTip();
        }

        final String superMessageName() {
            return super.messageName();
        }

        final String superMessageOpening() {
            return super.messageOpening();
        }

        final String superMessageOpened() {
            return super.messageOpened();
        }

        final String superMessageSave() {
            return super.messageSave();
        }

        final void superUpdateTitles() {
            super.updateTitles();
        }

        protected boolean close(boolean ask) {
            return EditorSupport.this.close(ask);
        }

        protected boolean superClose(boolean ask) {
            return super.close(ask);
        }

        protected CloneableTopComponent createCloneableTopComponent() {
            CloneableTopComponent ctc = EditorSupport.this.createCloneableTopComponent();
            if (ctc instanceof CloneableEditor) {
                this.initializeCloneableEditor((CloneableEditor)ctc);
            }
            return ctc;
        }

        @Override
        protected void initializeCloneableEditor(CloneableEditor editor) {
            DataObject obj = this.getDataObject();
            if (obj.isValid()) {
                Node ourNode = obj.getNodeDelegate();
                editor.setActivatedNodes(new Node[]{ourNode});
                editor.setIcon(ourNode.getIcon(1));
                DataNodeListener nl = new DataNodeListener(editor);
                ourNode.addNodeListener(NodeOp.weakNodeListener((NodeListener)nl, (Object)ourNode));
                this.nodeL = nl;
            }
        }

        final void superLoadFromStreamToKit(StyledDocument doc, InputStream stream, EditorKit kit) throws IOException, BadLocationException {
            super.loadFromStreamToKit(doc, stream, kit);
        }

        @Override
        protected void loadFromStreamToKit(StyledDocument doc, InputStream stream, EditorKit kit) throws IOException, BadLocationException {
            EditorSupport.this.loadFromStreamToKit(doc, stream, kit);
        }

        protected void superSaveFromKitToStream(StyledDocument doc, EditorKit kit, OutputStream stream) throws IOException, BadLocationException {
            super.saveFromKitToStream(doc, kit, stream);
        }

        @Override
        protected void saveFromKitToStream(StyledDocument doc, EditorKit kit, OutputStream stream) throws IOException, BadLocationException {
            EditorSupport.this.saveFromKitToStream(doc, kit, stream);
        }

        protected Task reloadDocument() {
            return EditorSupport.this.reloadDocumentTask();
        }

        final Task superReloadDocument() {
            return super.reloadDocument();
        }

        @Override
        public void saveDocument() throws IOException {
            EditorSupport.this.saveDocument();
        }

        final void superSaveDocument() throws IOException {
            super.saveDocument();
        }

        final UndoRedo.Manager superUndoRedoManager() {
            return super.createUndoRedoManager();
        }

        protected UndoRedo.Manager createUndoRedoManager() {
            return EditorSupport.this.createUndoRedoManager();
        }

        EditorKit superCreateEditorKit() {
            return super.createEditorKit();
        }

        protected EditorKit createEditorKit() {
            return EditorSupport.this.createEditorKit();
        }

        final boolean superCanClose() {
            return super.canClose();
        }

        @Override
        protected boolean canClose() {
            return EditorSupport.this.canClose();
        }

        private final class DataNodeListener
        extends NodeAdapter {
            private final CloneableEditor editor;

            DataNodeListener(CloneableEditor editor) {
                this.editor = editor;
            }

            public void propertyChange(PropertyChangeEvent ev) {
                DataObject obj;
                if ("displayName".equals(ev.getPropertyName())) {
                    Del.this.updateTitles();
                }
                if ("icon".equals(ev.getPropertyName()) && (obj = Del.this.getDataObject()).isValid()) {
                    Mutex.EVENT.writeAccess(new Runnable(){

                        @Override
                        public void run() {
                            DataNodeListener.this.editor.setIcon(obj.getNodeDelegate().getIcon(1));
                        }
                    });
                }
            }

        }

    }

    public static class Editor
    extends CloneableEditor {
        protected DataObject obj;
        static final long serialVersionUID = -185739563792410059L;

        public Editor() {
        }

        public Editor(DataObject obj) {
            this(obj, obj.getCookie(EditorSupport.class));
        }

        public Editor(DataObject obj, EditorSupport support) {
            super((CloneableEditorSupport)support.del);
            this.obj = obj;
        }

        public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
            super.readExternal(in);
            CloneableEditorSupport ces = this.cloneableEditorSupport();
            if (ces instanceof Del) {
                this.obj = ((Del)ces).getDataObjectHack2();
            }
        }
    }

}

