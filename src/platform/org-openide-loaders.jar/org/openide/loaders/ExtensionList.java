/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Enumerations
 *  org.openide.util.Utilities
 */
package org.openide.loaders;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.openide.filesystems.FileObject;
import org.openide.util.Enumerations;
import org.openide.util.Utilities;

public class ExtensionList
implements Cloneable,
Serializable {
    private static final boolean CASE_INSENSITIVE = Utilities.isWindows() || Utilities.getOperatingSystem() == 2048 || Utilities.getOperatingSystem() == 16384;
    private Set<String> list;
    private Set<String> mimeTypes;
    static final long serialVersionUID = 8868581349510386291L;

    public synchronized Object clone() {
        try {
            ExtensionList l = (ExtensionList)super.clone();
            if (this.list != null) {
                l.list = new HashSet<String>();
                l.list.addAll(this.list);
            }
            if (this.mimeTypes != null) {
                l.mimeTypes = new HashSet<String>();
                l.mimeTypes.addAll(this.mimeTypes);
            }
            return l;
        }
        catch (CloneNotSupportedException ex) {
            throw new InternalError();
        }
    }

    public synchronized void addExtension(String ext) {
        if (this.list == null) {
            this.list = new HashSet<String>();
        }
        this.list.add(ext);
    }

    public void removeExtension(String ext) {
        if (this.list != null) {
            this.list.remove(ext);
        }
    }

    public synchronized void addMimeType(String mime) {
        if (this.mimeTypes == null) {
            this.mimeTypes = new HashSet<String>();
        }
        this.mimeTypes.add(mime);
    }

    public void removeMimeType(String mime) {
        if (this.mimeTypes != null) {
            this.mimeTypes.remove(mime);
        }
    }

    public boolean isRegistered(String s) {
        if (this.list == null) {
            return false;
        }
        try {
            String ext = s.substring(s.lastIndexOf(46) + 1);
            return this.list.contains(ext);
        }
        catch (StringIndexOutOfBoundsException ex) {
            return false;
        }
    }

    public boolean isRegistered(FileObject fo) {
        String mime;
        if (this.list != null && this.list.contains(fo.getExt())) {
            return true;
        }
        if (this.mimeTypes != null && this.mimeTypes.contains(mime = fo.getMIMEType(this.mimeTypes.toArray(new String[0])))) {
            return true;
        }
        return false;
    }

    public Enumeration<String> extensions() {
        return ExtensionList.en(this.list);
    }

    public Enumeration<String> mimeTypes() {
        return ExtensionList.en(this.mimeTypes);
    }

    public String toString() {
        return "ExtensionList[" + this.list + this.mimeTypes + "]";
    }

    public boolean equals(Object o) {
        if (!(o instanceof ExtensionList)) {
            return false;
        }
        ExtensionList e = (ExtensionList)o;
        return ExtensionList.equalSets(this.list, e.list, CASE_INSENSITIVE) && ExtensionList.equalSets(this.mimeTypes, e.mimeTypes, false);
    }

    public int hashCode() {
        int x = 0;
        if (this.list != null) {
            x = ExtensionList.normalizeSet(this.list, CASE_INSENSITIVE).hashCode();
        }
        if (this.mimeTypes != null) {
            x += ExtensionList.normalizeSet(this.mimeTypes, false).hashCode();
        }
        return x;
    }

    private static boolean equalSets(Set<String> s1, Set<String> s2, boolean flattenCase) {
        if (s1 == null && s2 == null) {
            return true;
        }
        Set<String> s1a = ExtensionList.normalizeSet(s1, flattenCase);
        Set<String> s2a = ExtensionList.normalizeSet(s2, flattenCase);
        return s1a.equals(s2a);
    }

    private static Set<String> normalizeSet(Set<String> s, boolean flattenCase) {
        if (s == null || s.isEmpty()) {
            return Collections.emptySet();
        }
        if (flattenCase) {
            HashSet<String> s2 = new HashSet<String>(s.size() * 4 / 3 + 1);
            for (String item : s) {
                s2.add(item.toLowerCase(Locale.US));
            }
            return s2;
        }
        return s;
    }

    private static Enumeration<String> en(Collection<String> c) {
        if (c == null) {
            return Enumerations.empty();
        }
        return Collections.enumeration(ExtensionList.createExtensionSet(c));
    }

    private static SortedSet<String> createExtensionSet(Collection<String> clone) {
        TreeSet t = CASE_INSENSITIVE ? new TreeSet(String.CASE_INSENSITIVE_ORDER) : new TreeSet<String>();
        t.addAll(clone);
        return t;
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        ObjectInputStream.GetField gf = ois.readFields();
        Set<String> whichList = gf.get("list", null);
        if (whichList instanceof Map) {
            whichList = ((Map)((Object)whichList)).keySet();
        }
        if (whichList != null) {
            this.list = ExtensionList.createExtensionSet(whichList);
        }
        this.mimeTypes = (Set)gf.get("mimeTypes", null);
    }
}

