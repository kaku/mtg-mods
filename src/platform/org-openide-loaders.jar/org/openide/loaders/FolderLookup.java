/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.InstanceCookie$Of
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Template
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.AbstractLookup$Pair
 *  org.openide.util.lookup.ProxyLookup
 */
package org.openide.loaders;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataLoader;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.loaders.DataObjectPool;
import org.openide.loaders.FolderInstance;
import org.openide.loaders.FolderList;
import org.openide.loaders.InstanceDataObject;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.ProxyLookup;

@Deprecated
public class FolderLookup
extends FolderInstance {
    private static final Object LOCK = new Object();
    private ProxyLkp lookup;
    private String rootName;
    private final boolean isRoot;
    private static final Set<String> notified = Collections.synchronizedSet(new HashSet());

    public FolderLookup(DataObject.Container df) {
        this(df, "FL[");
    }

    public FolderLookup(DataObject.Container df, String prefix) {
        this(df, prefix, true);
    }

    private FolderLookup(DataObject.Container df, String prefix, boolean isRoot) {
        super(df);
        this.rootName = prefix;
        this.isRoot = isRoot;
    }

    @Override
    public final Class<?> instanceClass() {
        return ProxyLkp.class;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final Lookup getLookup() {
        boolean inited = false;
        Object object = LOCK;
        synchronized (object) {
            if (this.lookup == null) {
                this.lookup = new ProxyLkp(this);
                inited = true;
            }
        }
        if (inited) {
            this.checkRecreate();
        }
        return this.lookup;
    }

    @Override
    protected final Object createInstance(InstanceCookie[] cookies) throws IOException, ClassNotFoundException {
        FolderLookupData flData = new FolderLookupData();
        if (this.isRoot) {
            flData.lookups.add(null);
        }
        for (int i = 0; i < cookies.length; ++i) {
            try {
                Object obj = cookies[i].instanceCreate();
                if (obj instanceof FolderLookupData) {
                    flData.items.addAll(((FolderLookupData)obj).items);
                    flData.lookups.addAll(((FolderLookupData)obj).lookups);
                    continue;
                }
                if (obj instanceof Lookup) {
                    flData.lookups.add((Lookup)obj);
                    continue;
                }
                flData.items.add((ICItem)((Object)obj));
                continue;
            }
            catch (IOException ex) {
                FolderLookup.exception(ex);
                continue;
            }
            catch (ClassNotFoundException ex) {
                FolderLookup.exception(ex);
            }
        }
        if (!this.isRoot) {
            return flData;
        }
        this.getLookup();
        this.lookup.update(flData.items, flData.lookups);
        return this.lookup;
    }

    @Override
    protected Object instanceForCookie(DataObject dobj, InstanceCookie cookie) throws IOException, ClassNotFoundException {
        boolean isLookup = cookie instanceof InstanceCookie.Of ? ((InstanceCookie.Of)cookie).instanceOf(Lookup.class) : Lookup.class.isAssignableFrom(cookie.instanceClass());
        if (isLookup) {
            return cookie.instanceCreate();
        }
        return new ICItem(dobj, this.rootName, cookie);
    }

    @Override
    protected InstanceCookie acceptFolder(DataFolder df) {
        return new FolderLookup(df, FolderLookup.objectName(this.rootName, df), false);
    }

    @Override
    protected InstanceCookie acceptContainer(DataObject.Container df) {
        return new FolderLookup(df, this.rootName == null ? "<container>" : this.rootName + "<container>", false);
    }

    @Override
    protected final Task postCreationTask(Runnable run) {
        run.run();
        return null;
    }

    private static String objectName(String name, DataObject obj) {
        if (name == null) {
            return obj.getName();
        }
        return name + '/' + obj.getName();
    }

    private static void exception(Exception e) {
        Logger.getLogger(FolderLookup.class.getName()).log(Level.INFO, null, e);
    }

    private static void exception(Exception e, FileObject fo) {
        Logger.getLogger(FolderLookup.class.getName()).log(notified.add(fo.getPath()) ? Level.INFO : Level.FINE, "Bad file: " + (Object)fo, e);
    }

    private static class FolderLookupData {
        private Collection<ICItem> items = new ArrayList<ICItem>(30);
        private List<Lookup> lookups = new ArrayList<Lookup>(5);
    }

    private static final class ICItem
    extends AbstractLookup.Pair {
        static final long serialVersionUID = 10;
        static final ThreadLocal<ICItem> DANGEROUS = new ThreadLocal();
        private static final Logger ERR = Logger.getLogger(ICItem.class.getName());
        private FileObject fo;
        private transient InstanceCookie ic;
        private transient DataObject obj;
        private transient Reference<Object> ref;
        private String rootName;

        public ICItem(DataObject obj, String rootName, InstanceCookie ic) {
            this.ic = ic;
            this.obj = obj;
            this.rootName = rootName;
            this.fo = obj.getPrimaryFile();
            if (ERR.isLoggable(Level.FINE)) {
                ERR.fine("New ICItem: " + obj);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void init() {
            if (this.ic != null) {
                return;
            }
            ICItem prev = DANGEROUS.get();
            try {
                DANGEROUS.set(this);
                if (this.obj == null) {
                    try {
                        this.obj = DataObject.find(this.fo);
                    }
                    catch (DataObjectNotFoundException donfe) {
                        this.ic = new BrokenInstance("No DataObject for " + this.fo.getPath(), donfe);
                        DANGEROUS.set(prev);
                        return;
                    }
                }
                this.ic = this.obj.getCookie(InstanceCookie.class);
                if (this.ic == null) {
                    this.ic = new BrokenInstance("No cookie for " + this.fo.getPath(), null);
                }
            }
            finally {
                DANGEROUS.set(prev);
            }
        }

        protected boolean instanceOf(Class clazz) {
            this.init();
            if (ERR.isLoggable(Level.FINE)) {
                ERR.fine("instanceOf: " + clazz.getName() + " obj: " + this.obj);
            }
            if (this.ic instanceof InstanceCookie.Of) {
                InstanceCookie.Of of = (InstanceCookie.Of)this.ic;
                boolean res = of.instanceOf(clazz);
                if (ERR.isLoggable(Level.FINE)) {
                    ERR.fine("  of: " + res);
                }
                return res;
            }
            try {
                boolean res = clazz.isAssignableFrom(this.ic.instanceClass());
                if (ERR.isLoggable(Level.FINE)) {
                    ERR.fine("  plain: " + res);
                }
                return res;
            }
            catch (ClassNotFoundException ex) {
                FolderLookup.exception(ex, this.fo);
            }
            catch (IOException ex) {
                FolderLookup.exception(ex, this.fo);
            }
            return false;
        }

        public Object getInstance() {
            this.init();
            try {
                Object obj = this.ic.instanceCreate();
                if (ERR.isLoggable(Level.FINE)) {
                    ERR.fine("  getInstance: " + obj + " for " + this.obj);
                }
                this.ref = new WeakReference<Object>(obj);
                return obj;
            }
            catch (ClassNotFoundException ex) {
                FolderLookup.exception(ex, this.fo);
            }
            catch (IOException ex) {
                FolderLookup.exception(ex, this.fo);
            }
            return null;
        }

        public int hashCode() {
            this.init();
            return System.identityHashCode((Object)this.ic);
        }

        public boolean equals(Object obj) {
            if (obj instanceof ICItem) {
                ICItem i = (ICItem)((Object)obj);
                i.init();
                this.init();
                return this.ic == i.ic;
            }
            return false;
        }

        public String getId() {
            this.init();
            if (this.obj == null) {
                return "<broken: " + this.fo.getPath() + ">";
            }
            return FolderLookup.objectName(this.rootName, this.obj);
        }

        public String getDisplayName() {
            this.init();
            if (this.obj == null) {
                return "<broken: " + this.fo.getPath() + ">";
            }
            return this.obj.getNodeDelegate().getDisplayName();
        }

        protected boolean creatorOf(Object obj) {
            Reference<Object> w = this.ref;
            if (w != null && w.get() == obj) {
                return true;
            }
            if (this.obj instanceof InstanceDataObject) {
                return ((InstanceDataObject)this.obj).creatorOf(obj);
            }
            return false;
        }

        public Class getType() {
            this.init();
            try {
                return this.ic.instanceClass();
            }
            catch (IOException ex) {
            }
            catch (ClassNotFoundException ex) {
                // empty catch block
            }
            return Object.class;
        }

        private static final class BrokenInstance
        implements InstanceCookie.Of {
            private final String message;
            private final Exception ex;

            public BrokenInstance(String message, Exception ex) {
                this.message = message;
                this.ex = ex;
            }

            public String instanceName() {
                return "java.lang.Object";
            }

            private ClassNotFoundException die() {
                if (this.ex != null) {
                    return new ClassNotFoundException(this.message, this.ex);
                }
                return new ClassNotFoundException(this.message);
            }

            public Class instanceClass() throws IOException, ClassNotFoundException {
                throw this.die();
            }

            public Object instanceCreate() throws IOException, ClassNotFoundException {
                throw this.die();
            }

            public boolean instanceOf(Class type) {
                return false;
            }
        }

    }

    static final class ProxyLkp
    extends ProxyLookup
    implements Serializable {
        static final Dispatch DISPATCH = new Dispatch();
        private static final long serialVersionUID = 1;
        private transient FolderLookup fl;
        private transient AbstractLookup.Content content;
        private transient boolean readFromStream;

        public ProxyLkp(FolderLookup folder) {
            this(folder, new AbstractLookup.Content((Executor)DISPATCH));
        }

        private ProxyLkp(FolderLookup folder, AbstractLookup.Content content) {
            super(new Lookup[]{new AbstractLookup(content)});
            this.fl = folder;
            this.content = content;
        }

        public String toString() {
            return "FolderLookup.lookup[\"" + this.fl.rootName + "\"]";
        }

        private void writeObject(ObjectOutputStream oos) throws IOException {
            Lookup[] ls = this.getLookups();
            for (int i = 0; i < ls.length; ++i) {
                oos.writeObject((Object)ls[i]);
            }
            oos.writeObject(null);
            oos.writeObject(this.fl.folder);
            oos.writeObject(this.fl.rootName);
            oos.writeObject((Object)this.content);
        }

        private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
            Lookup l;
            ArrayList<Lookup> ls = new ArrayList<Lookup>();
            while ((l = (Lookup)ois.readObject()) != null) {
                ls.add(l);
            }
            Lookup[] arr = ls.toArray((T[])new Lookup[ls.size()]);
            DataFolder df = (DataFolder)ois.readObject();
            String root = (String)ois.readObject();
            this.fl = new FolderLookup(df, root, true);
            this.fl.lookup = this;
            this.content = (AbstractLookup.Content)ois.readObject();
            this.setLookups((Executor)DISPATCH, arr);
            this.readFromStream = true;
            RequestProcessor.getDefault().post((Runnable)((Object)this.fl), 0, 1);
        }

        public void update(Collection<ICItem> items, List<Lookup> lookups) {
            this.readFromStream = false;
            Lookup pairs = this.getLookups()[0];
            this.content.setPairs(items);
            if (this.fl.err().isLoggable(Level.FINE)) {
                this.fl.err().fine("Changed pairs: " + items);
            }
            lookups.set(0, pairs);
            Lookup[] arr = lookups.toArray((T[])new Lookup[lookups.size()]);
            this.setLookups((Executor)DISPATCH, arr);
            if (this.fl.err().isLoggable(Level.FINE)) {
                this.fl.err().fine("Changed lookups: " + lookups);
            }
        }

        protected void beforeLookup(Lookup.Template template) {
            if (this.readFromStream) {
                return;
            }
            if (template.getType().equals(DataLoader.class)) {
                return;
            }
            if (!FolderList.isFolderRecognizerThread() && ICItem.DANGEROUS.get() == null) {
                if (!DataObjectPool.isConstructorAllowed()) {
                    this.fl.waitFinished();
                } else {
                    try {
                        while (!this.fl.waitFinished(10000)) {
                            long blocked = DataObjectPool.getPOOL().timeInWaitNotified();
                            if (blocked <= 10000) continue;
                            Exception td = new Exception();
                            this.fl.err().log(Level.INFO, "Preventing deadlock #65543: Do not call FolderLookup from inside DataObject operations!", td);
                            for (Map.Entry<Thread, StackTraceElement[]> entry : Thread.getAllStackTraces().entrySet()) {
                                td.setStackTrace(entry.getValue());
                                this.fl.err().log(Level.INFO, "Thread " + entry.getKey(), td);
                            }
                            return;
                        }
                    }
                    catch (InterruptedException ex) {
                        this.fl.err().log(Level.INFO, null, ex);
                    }
                }
            } else if (this.fl.folder != null && this.fl.folder.getName().equals("Factories") && template.getType().isAssignableFrom(DataObject.Factory.class)) {
                this.fl.waitFinished();
            }
        }

        public void waitFinished() {
            this.fl.waitFinished();
        }
    }

    static final class Dispatch
    implements Executor,
    Runnable {
        private static final RequestProcessor DISPATCH = new RequestProcessor("Lookup Dispatch Thread");
        private static final Logger LOG = Logger.getLogger(Dispatch.class.getName());
        private final Queue<Runnable> queue = new ConcurrentLinkedQueue<Runnable>();
        private final RequestProcessor.Task task;

        Dispatch() {
            this.task = DISPATCH.create((Runnable)this, true);
        }

        @Override
        public void execute(Runnable command) {
            LOG.log(Level.FINER, "Enqueued: {0}", command);
            this.queue.add(command);
            this.task.schedule(0);
        }

        public void waitFinished() {
            this.task.waitFinished();
        }

        @Override
        public void run() {
            LOG.finer("Processing dispatched commands");
            do {
                Runnable r;
                if ((r = this.queue.poll()) == null) {
                    LOG.log(Level.FINER, "Processing done. Queue: {0}", this.queue.isEmpty());
                    return;
                }
                LOG.log(Level.FINER, "Processing {0}", r);
                r.run();
            } while (true);
        }
    }

}

