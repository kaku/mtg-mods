/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.CookieSet
 *  org.openide.util.RequestProcessor
 */
package org.openide.loaders;

import java.io.IOException;
import org.netbeans.modules.openide.loaders.DataObjectAccessor;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.FolderInstance;
import org.openide.loaders.FolderList;
import org.openide.loaders.MultiDataObject;
import org.openide.nodes.CookieSet;
import org.openide.util.RequestProcessor;

final class DataObjectAccessorImpl
extends DataObjectAccessor {
    DataObjectAccessorImpl() {
    }

    @Override
    public DataObject copyRename(DataObject dob, DataFolder f, String name, String ext) throws IOException {
        return dob.copyRename(f, name, ext);
    }

    @Override
    public CookieSet getCookieSet(MultiDataObject dob) {
        return dob.getCookieSet();
    }

    @Override
    public boolean isInstancesThread() {
        return FolderInstance.PROCESSOR.isRequestProcessorThread() || FolderList.isFolderRecognizerThread();
    }

    @Override
    public void precreateInstances(FolderInstance fi) {
        fi.precreateInstances();
    }
}

