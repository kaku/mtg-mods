/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.InstanceCookie$Of
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$Status
 *  org.openide.nodes.BeanChildren
 *  org.openide.nodes.BeanChildren$Factory
 *  org.openide.nodes.BeanNode
 *  org.openide.nodes.BeanNode$Descriptor
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.nodes.Node$IndexedProperty
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.PropertySupport
 *  org.openide.nodes.PropertySupport$ReadOnly
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.util.SharedClassObject
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 */
package org.openide.loaders;

import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ImageObserver;
import java.awt.image.WritableRaster;
import java.beans.BeanDescriptor;
import java.beans.BeanInfo;
import java.beans.EventSetDescriptor;
import java.beans.IntrospectionException;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.beans.beancontext.BeanContext;
import java.beans.beancontext.BeanContextChild;
import java.beans.beancontext.BeanContextMembershipEvent;
import java.beans.beancontext.BeanContextMembershipListener;
import java.beans.beancontext.BeanContextProxy;
import java.beans.beancontext.BeanContextSupport;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.loaders.InstanceDataObject;
import org.openide.nodes.BeanChildren;
import org.openide.nodes.BeanNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.SharedClassObject;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;

final class InstanceNode
extends DataNode
implements Runnable {
    private static final String INSTANCE_ICON_BASE = "org/openide/loaders/instanceObject.gif";
    private static final String XML_EXT = "settings";
    private PropL propertyChangeListener = null;
    private PropertyChangeListener dobjListener;
    private boolean isSheetCreated = false;
    private boolean noBeanInfo = false;
    private boolean brokenIcon;

    public InstanceNode(InstanceDataObject obj) {
        this(obj, Boolean.FALSE.equals(obj.getPrimaryFile().getAttribute("beaninfo")));
    }

    private InstanceNode(InstanceDataObject obj, boolean noBeanInfo) {
        super(obj, InstanceNode.getChildren(obj, noBeanInfo));
        this.initIconBase();
        this.noBeanInfo = noBeanInfo;
        if (!noBeanInfo && !this.getDataObject().getPrimaryFile().hasExt("settings")) {
            SwingUtilities.invokeLater(this);
        }
        this.dobjListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equals("cookie")) {
                    if (InstanceNode.this.propertyChangeListener != null) {
                        InstanceNode.this.propertyChangeListener.destroy();
                        InstanceNode.this.propertyChangeListener = null;
                    }
                    if (InstanceNode.this.noBeanInfo || InstanceNode.this.ic() == null) {
                        InstanceNode.this.initIconBase();
                    } else {
                        InstanceNode.this.fireIconChange();
                    }
                    InstanceNode.this.fireNameChange(null, null);
                    InstanceNode.this.fireDisplayNameChange(null, null);
                    InstanceNode.this.fireShortDescriptionChange(null, null);
                    if (InstanceNode.this.isSheetCreated) {
                        InstanceNode.this.setSheet(InstanceNode.this.createSheet());
                    }
                }
            }
        };
        obj.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.dobjListener, (Object)obj));
    }

    private void initIconBase() {
        InstanceCookie.Of ic = this.ic();
        String iconBase = "org/openide/loaders/instanceObject.gif";
        if (ic == null) {
            iconBase = "org/openide/loaders/instanceBroken.gif";
        }
        this.setIconBaseWithExtension(iconBase);
    }

    private static Children getChildren(DataObject dobj, boolean noBeanInfo) {
        if (noBeanInfo) {
            return Children.LEAF;
        }
        InstanceCookie inst = dobj.getCookie(InstanceCookie.class);
        if (inst == null) {
            return Children.LEAF;
        }
        try {
            Class clazz = inst.instanceClass();
            if (BeanContext.class.isAssignableFrom(clazz) || BeanContextProxy.class.isAssignableFrom(clazz)) {
                return new InstanceChildren((InstanceDataObject)dobj);
            }
            return Children.LEAF;
        }
        catch (Exception ex) {
            return Children.LEAF;
        }
    }

    private InstanceDataObject i() {
        return (InstanceDataObject)this.getDataObject();
    }

    private InstanceCookie.Of ic() {
        return this.getDataObject().getCookie(InstanceCookie.Of.class);
    }

    @Override
    public Image getIcon(int type) {
        if (this.noBeanInfo) {
            return super.getIcon(type);
        }
        Image img = null;
        try {
            DataObject dobj = this.getDataObject();
            img = dobj.getPrimaryFile().getFileSystem().getStatus().annotateIcon(img, type, dobj.files());
        }
        catch (FileStateInvalidException e) {
            // empty catch block
        }
        if (img == null) {
            img = this.initIcon(type);
        }
        if (img == null) {
            img = super.getIcon(type);
        }
        return img;
    }

    @Override
    public Image getOpenedIcon(int type) {
        return this.getIcon(type);
    }

    private void initPList() {
        try {
            InstanceCookie.Of ic = this.ic();
            if (ic == null) {
                return;
            }
            BeanInfo info = Utilities.getBeanInfo((Class)ic.instanceClass());
            EventSetDescriptor[] descs = info.getEventSetDescriptors();
            Method setter = null;
            for (int i = 0; descs != null && i < descs.length; ++i) {
                setter = descs[i].getAddListenerMethod();
                if (setter == null || !setter.getName().equals("addPropertyChangeListener")) continue;
                Object bean = ic.instanceCreate();
                this.propertyChangeListener = new PropL();
                setter.invoke(bean, WeakListeners.propertyChange((PropertyChangeListener)this.propertyChangeListener, (Object)bean));
            }
        }
        catch (Exception ex) {
        }
        catch (LinkageError ex) {
            // empty catch block
        }
    }

    private Image initIcon(int type) {
        if (this.brokenIcon) {
            return null;
        }
        Image beanInfoIcon = null;
        InstanceCookie.Of ic = null;
        try {
            BeanInfo bi;
            ic = this.ic();
            if (ic == null) {
                return null;
            }
            Class clazz = ic.instanceClass();
            String className = clazz.getName();
            if (className.equals("javax.swing.JSeparator") || className.equals("javax.swing.JToolBar$Separator")) {
                Class clazzTmp = Class.forName("javax.swing.JSeparator");
                bi = Utilities.getBeanInfo(clazzTmp);
            } else {
                bi = Utilities.getBeanInfo((Class)clazz);
            }
            if (bi != null && (beanInfoIcon = bi.getIcon(type)) != null) {
                beanInfoIcon = InstanceNode.toBufferedImage(beanInfoIcon, true);
            }
            if (beanInfoIcon == null && Action.class.isAssignableFrom(clazz)) {
                Object base;
                Action action = (Action)ic.instanceCreate();
                Icon icon = (Icon)action.getValue("SmallIcon");
                beanInfoIcon = icon != null ? ImageUtilities.icon2Image((Icon)icon) : ((base = action.getValue("iconBase")) instanceof String ? ImageUtilities.loadImage((String)((String)base), (boolean)true) : ImageUtilities.loadImage((String)"org/openide/loaders/empty.gif", (boolean)true));
            }
        }
        catch (Exception e) {
            Logger.getLogger(InstanceNode.class.getName()).log(Level.WARNING, null, e);
            Logger.getLogger(InstanceNode.class.getName()).log(Level.INFO, "ic = {0}", (Object)ic);
            this.brokenIcon = true;
        }
        catch (LinkageError e) {
            Logger.getLogger(InstanceNode.class.getName()).log(Level.WARNING, null, e);
            Logger.getLogger(InstanceNode.class.getName()).log(Level.INFO, "ic = {0}", (Object)ic);
            this.brokenIcon = true;
        }
        return beanInfoIcon;
    }

    @Override
    public void run() {
        try {
            InstanceCookie.Of ic = this.ic();
            if (ic == null) {
                return;
            }
            Class clazz = ic.instanceClass();
            String className = clazz.getName();
            if (className.equals("javax.swing.JSeparator") || className.equals("javax.swing.JToolBar$Separator")) {
                this.setDisplayName(NbBundle.getMessage(InstanceDataObject.class, (String)"LBL_separator_instance"));
                return;
            }
            if (Action.class.isAssignableFrom(clazz)) {
                String name;
                Action action = (Action)ic.instanceCreate();
                String string = name = action != null ? (String)action.getValue("Name") : null;
                if (name == null) {
                    DataObject.LOG.warning("Please attach following information to the issue <http://www.netbeans.org/issues/show_bug.cgi?id=31227>: action " + className + " does not implement SystemAction.getName() or Action.getValue(NAME) properly. It returns null!");
                    this.setDisplayName(className);
                    return;
                }
                int amper = name.indexOf(38);
                if (amper != -1) {
                    name = name.substring(0, amper) + name.substring(amper + 1);
                }
                if (name.endsWith("...")) {
                    name = name.substring(0, name.length() - 3);
                }
                name = name.trim();
                this.setDisplayName(name);
                return;
            }
        }
        catch (Exception e) {
            Logger.getLogger(InstanceNode.class.getName()).log(Level.WARNING, null, e);
            this.setDisplayName(this.getDataObject().getName());
            return;
        }
    }

    private String getNameForBean() {
        try {
            Method nameGetter;
            InstanceCookie.Of ic = this.ic();
            if (ic == null) {
                return NbBundle.getMessage(InstanceNode.class, (String)"LBL_BrokenSettings");
            }
            Class clazz = ic.instanceClass();
            try {
                nameGetter = clazz.getMethod("getName", null);
                if (nameGetter.getReturnType() != String.class) {
                    throw new NoSuchMethodException();
                }
            }
            catch (NoSuchMethodException e) {
                try {
                    nameGetter = clazz.getMethod("getDisplayName", null);
                    if (nameGetter.getReturnType() != String.class) {
                        throw new NoSuchMethodException();
                    }
                }
                catch (NoSuchMethodException ee) {
                    return null;
                }
            }
            Object bean = ic.instanceCreate();
            return (String)nameGetter.invoke(bean, new Object[0]);
        }
        catch (Exception ex) {
            return null;
        }
    }

    private Method getDeclaredSetter() {
        Method nameSetter = null;
        try {
            InstanceCookie.Of ic = this.ic();
            if (ic == null) {
                return null;
            }
            Class clazz = ic.instanceClass();
            try {
                nameSetter = clazz.getMethod("setName", String.class);
            }
            catch (NoSuchMethodException e) {
                nameSetter = clazz.getMethod("setDisplayName", String.class);
            }
        }
        catch (Exception ex) {
            // empty catch block
        }
        return nameSetter;
    }

    @Override
    public void setName(String name) {
        if (!this.getDataObject().getPrimaryFile().hasExt("settings")) {
            super.setName(name);
            return;
        }
        String old = this.getNameImpl();
        if (old != null && old.equals(name)) {
            return;
        }
        InstanceCookie.Of ic = this.ic();
        if (ic == null) {
            super.setName(name);
            return;
        }
        Method nameSetter = this.getDeclaredSetter();
        if (nameSetter != null) {
            try {
                Object bean = ic.instanceCreate();
                nameSetter.invoke(bean, name);
                this.i().scheduleSave();
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        super.setName(name);
    }

    @Override
    public String getDisplayName() {
        String name = (String)this.getDataObject().getPrimaryFile().getAttribute("name");
        if (name == null) {
            try {
                String def = "\b";
                FileSystem.Status fsStatus = this.getDataObject().getPrimaryFile().getFileSystem().getStatus();
                name = fsStatus.annotateName(def, this.getDataObject().files());
                if (name.indexOf(def) < 0) {
                    return name;
                }
                name = this.getNameForBean();
                name = name != null ? fsStatus.annotateName(name, this.getDataObject().files()) : super.getDisplayName();
            }
            catch (FileStateInvalidException e) {
                // empty catch block
            }
        }
        return name;
    }

    private String getNameImpl() {
        String name = this.getNameForBean();
        if (name == null) {
            name = this.getName();
        }
        return name;
    }

    @Override
    protected Sheet createSheet() {
        Sheet orig;
        if (this.getDataObject().getPrimaryFile().hasExt("ser") || this.getDataObject().getPrimaryFile().hasExt("settings")) {
            orig = new Sheet();
            this.changeSheet(orig);
        } else {
            orig = super.createSheet();
            Sheet.Set props = orig.get("properties");
            InstanceCookie.Of ic = this.ic();
            if (ic == null) {
                props.put((Node.Property)new PropertySupport.ReadOnly<String>("className", String.class, NbBundle.getMessage(InstanceDataObject.class, (String)"PROP_instance_class"), NbBundle.getMessage(InstanceDataObject.class, (String)"HINT_instance_class"), (InstanceCookie)ic){
                    final /* synthetic */ InstanceCookie val$ic;

                    public String getValue() {
                        return this.val$ic.instanceName();
                    }
                });
            }
        }
        this.isSheetCreated = true;
        return orig;
    }

    private void changeSheet(Sheet orig) {
        Sheet.Set props = orig.get("properties");
        try {
            InstanceCookie.Of ic = this.ic();
            if (ic == null) {
                return;
            }
            BeanInfo beanInfo = Utilities.getBeanInfo((Class)ic.instanceClass());
            BeanNode.Descriptor descr = BeanNode.computeProperties((Object)ic.instanceCreate(), (BeanInfo)beanInfo);
            this.initPList();
            props = Sheet.createPropertiesSet();
            if (descr.property != null) {
                InstanceNode.convertProps(props, descr.property, this.i());
            }
            orig.put(props);
            if (descr.expert != null && descr.expert.length != 0) {
                Sheet.Set p = Sheet.createExpertSet();
                InstanceNode.convertProps(p, descr.expert, this.i());
                orig.put(p);
            }
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(InstanceNode.class.getName()).log(Level.WARNING, null, ex);
        }
        catch (IOException ex) {
            Logger.getLogger(InstanceNode.class.getName()).log(Level.WARNING, null, ex);
        }
        catch (IntrospectionException ex) {
            Logger.getLogger(InstanceNode.class.getName()).log(Level.WARNING, null, ex);
        }
        catch (LinkageError ex) {
            Logger.getLogger(InstanceNode.class.getName()).log(Level.WARNING, null, ex);
        }
    }

    private static final void convertProps(Sheet.Set set, Node.Property[] arr, InstanceDataObject ido) {
        for (int i = 0; i < arr.length; ++i) {
            if (arr[i] instanceof Node.IndexedProperty) {
                set.put(new I((Node.IndexedProperty)arr[i], ido));
                continue;
            }
            set.put(new P(arr[i], ido));
        }
    }

    private static final Image toBufferedImage(Image img, boolean load) {
        if (load) {
            new ImageIcon(img);
        }
        BufferedImage rep = InstanceNode.createBufferedImage();
        Graphics2D g = rep.createGraphics();
        g.drawImage(img, 0, 0, null);
        g.dispose();
        img.flush();
        return rep;
    }

    private static final BufferedImage createBufferedImage() {
        ColorModel model = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getColorModel(2);
        BufferedImage buffImage = new BufferedImage(model, model.createCompatibleWritableRaster(16, 16), model.isAlphaPremultiplied(), null);
        return buffImage;
    }

    @Override
    public boolean canRename() {
        if (!this.getDataObject().getPrimaryFile().hasExt("settings")) {
            return super.canRename();
        }
        return this.getDeclaredSetter() != null;
    }

    @Override
    public boolean canDestroy() {
        if (!this.getDataObject().getPrimaryFile().hasExt("settings")) {
            return super.canDestroy();
        }
        try {
            InstanceCookie.Of ic = this.ic();
            if (ic == null) {
                return true;
            }
            Class clazz = ic.instanceClass();
            return !SharedClassObject.class.isAssignableFrom(clazz);
        }
        catch (Exception ex) {
            return false;
        }
    }

    @Override
    public boolean canCut() {
        if (!this.getDataObject().getPrimaryFile().hasExt("settings")) {
            return super.canCut();
        }
        try {
            InstanceCookie.Of ic = this.ic();
            if (ic == null) {
                return false;
            }
            Class clazz = ic.instanceClass();
            return !SharedClassObject.class.isAssignableFrom(clazz);
        }
        catch (Exception ex) {
            return false;
        }
    }

    @Override
    public boolean canCopy() {
        if (!this.getDataObject().getPrimaryFile().hasExt("settings")) {
            return super.canCopy();
        }
        try {
            InstanceCookie.Of ic = this.ic();
            if (ic == null) {
                return false;
            }
            Class clazz = ic.instanceClass();
            return !SharedClassObject.class.isAssignableFrom(clazz);
        }
        catch (Exception ex) {
            return false;
        }
    }

    public String getShortDescription() {
        if (this.noBeanInfo) {
            return super.getShortDescription();
        }
        try {
            InstanceCookie.Of ic = this.ic();
            if (ic == null) {
                return this.getDataObject().getPrimaryFile().toString();
            }
            Class clazz = ic.instanceClass();
            BeanDescriptor bd = Utilities.getBeanInfo((Class)clazz).getBeanDescriptor();
            String desc = bd.getShortDescription();
            return desc.equals(bd.getName()) ? this.getDisplayName() : desc;
        }
        catch (Exception ex) {
            return super.getShortDescription();
        }
        catch (LinkageError ex) {
            return super.getShortDescription();
        }
    }

    @Override
    public Action getPreferredAction() {
        return null;
    }

    private final class PropL
    implements PropertyChangeListener {
        private boolean doNotListen;

        PropL() {
            this.doNotListen = false;
        }

        @Override
        public void propertyChange(PropertyChangeEvent e) {
            if (this.doNotListen) {
                return;
            }
            InstanceNode.this.firePropertyChange(e.getPropertyName(), e.getOldValue(), e.getNewValue());
        }

        public void destroy() {
            this.doNotListen = true;
        }
    }

    private static class BeanContextNode<T>
    extends BeanNode<T> {
        public BeanContextNode(T bean, InstanceDataObject task) throws IntrospectionException {
            super(bean, BeanContextNode.getChildren(bean, task));
            this.changeSheet(this.getSheet(), task);
        }

        private void changeSheet(Sheet orig, InstanceDataObject task) {
            Sheet.Set props = orig.get("properties");
            if (props != null) {
                InstanceNode.convertProps(props, props.getProperties(), task);
            }
            if ((props = orig.get("expert")) != null) {
                InstanceNode.convertProps(props, props.getProperties(), task);
            }
        }

        private static Children getChildren(Object bean, InstanceDataObject task) {
            BeanContextChild bch;
            if (bean instanceof BeanContext) {
                return new BeanChildren((BeanContext)bean, (BeanChildren.Factory)new BeanFactoryImpl(task));
            }
            if (bean instanceof BeanContextProxy && (bch = ((BeanContextProxy)bean).getBeanContextProxy()) instanceof BeanContext) {
                return new BeanChildren((BeanContext)bch, (BeanChildren.Factory)new BeanFactoryImpl(task));
            }
            return Children.LEAF;
        }

        public boolean canDestroy() {
            return false;
        }
    }

    private static class BeanFactoryImpl
    implements BeanChildren.Factory {
        InstanceDataObject task;

        public BeanFactoryImpl(InstanceDataObject task) {
            this.task = task;
        }

        public Node createNode(Object bean) throws IntrospectionException {
            return new BeanContextNode<Object>(bean, this.task);
        }
    }

    private static final class InstanceChildren
    extends Children.Keys
    implements PropertyChangeListener {
        WeakReference<PropertyChangeListener> dobjListener;
        InstanceDataObject dobj;
        Object bean;
        ContextL contextL = null;

        public InstanceChildren(InstanceDataObject dobj) {
            this.dobj = dobj;
        }

        protected void addNotify() {
            super.addNotify();
            PropertyChangeListener p = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this.dobj);
            this.dobjListener = new WeakReference<PropertyChangeListener>(p);
            this.dobj.addPropertyChangeListener(p);
            this.contextL = new ContextL(this);
            this.propertyChange(null);
        }

        protected void removeNotify() {
            if (this.contextL != null && this.bean != null) {
                ((BeanContext)this.bean).removeBeanContextMembershipListener(this.contextL);
            }
            this.contextL = null;
            PropertyChangeListener p = this.dobjListener.get();
            if (p != null) {
                this.dobj.removePropertyChangeListener(p);
                this.dobjListener.clear();
            }
            this.setKeys(Collections.emptySet());
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (evt != null && !evt.getPropertyName().equals("cookie")) {
                return;
            }
            if (this.contextL != null && this.bean != null) {
                ((BeanContext)this.bean).removeBeanContextMembershipListener(this.contextL);
            }
            try {
                InstanceCookie ic = this.dobj.getCookie(InstanceCookie.class);
                if (ic == null) {
                    this.bean = null;
                    return;
                }
                Class clazz = ic.instanceClass();
                this.bean = BeanContext.class.isAssignableFrom(clazz) ? ic.instanceCreate() : (BeanContextProxy.class.isAssignableFrom(clazz) ? ((BeanContextProxy)this.dobj.instanceCreate()).getBeanContextProxy() : null);
            }
            catch (Exception ex) {
                this.bean = null;
                Exceptions.printStackTrace((Throwable)ex);
            }
            if (this.bean != null) {
                ((BeanContext)this.bean).addBeanContextMembershipListener(this.contextL);
            }
            this.updateKeys();
        }

        private void updateKeys() {
            if (this.bean == null) {
                this.setKeys(Collections.emptySet());
            } else {
                this.setKeys(((BeanContext)this.bean).toArray());
            }
        }

        protected Node[] createNodes(Object key) {
            Object ctx = this.bean;
            if (this.bean == null) {
                return new Node[0];
            }
            try {
                BeanContextSupport bcs;
                if (key instanceof BeanContextSupport && ((BeanContext)ctx).contains((bcs = (BeanContextSupport)key).getBeanContextPeer())) {
                    return new Node[0];
                }
                return new Node[]{new BeanContextNode<Object>(key, this.dobj)};
            }
            catch (IntrospectionException ex) {
                return new Node[0];
            }
        }

        private static final class ContextL
        implements BeanContextMembershipListener {
            private WeakReference<InstanceChildren> ref;

            ContextL(InstanceChildren bc) {
                this.ref = new WeakReference<InstanceChildren>(bc);
            }

            @Override
            public void childrenAdded(BeanContextMembershipEvent bcme) {
                InstanceChildren bc = this.ref.get();
                if (bc != null) {
                    bc.updateKeys();
                }
            }

            @Override
            public void childrenRemoved(BeanContextMembershipEvent bcme) {
                InstanceChildren bc = this.ref.get();
                if (bc != null) {
                    bc.updateKeys();
                }
            }
        }

    }

    private static final class I<T>
    extends Node.IndexedProperty<T, InstanceDataObject> {
        private Node.IndexedProperty<T, InstanceDataObject> del;
        private InstanceDataObject t;

        public I(Node.IndexedProperty<T, InstanceDataObject> del, InstanceDataObject t) {
            super(del.getValueType(), del.getElementType());
            this.del = del;
            this.t = t;
        }

        public void setName(String str) {
            this.del.setName(str);
        }

        public void restoreDefaultValue() throws IllegalAccessException, InvocationTargetException {
            this.del.restoreDefaultValue();
        }

        public void setValue(String str, Object obj) {
            this.del.setValue(str, obj);
        }

        public boolean supportsDefaultValue() {
            return this.del.supportsDefaultValue();
        }

        public boolean canRead() {
            return this.del.canRead();
        }

        public PropertyEditor getPropertyEditor() {
            return this.del.getPropertyEditor();
        }

        public boolean isHidden() {
            return this.del.isHidden();
        }

        public T getValue() throws IllegalAccessException, InvocationTargetException {
            return (T)this.del.getValue();
        }

        public void setExpert(boolean param) {
            this.del.setExpert(param);
        }

        public void setValue(T val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            this.del.setValue(val);
            this.t.scheduleSave();
        }

        public void setShortDescription(String str) {
            this.del.setShortDescription(str);
        }

        public boolean isExpert() {
            return this.del.isExpert();
        }

        public boolean canWrite() {
            return this.del.canWrite();
        }

        public Class<T> getValueType() {
            return this.del.getValueType();
        }

        public String getDisplayName() {
            return this.del.getDisplayName();
        }

        public Enumeration<String> attributeNames() {
            return this.del.attributeNames();
        }

        public String getShortDescription() {
            return this.del.getShortDescription();
        }

        public String getName() {
            return this.del.getName();
        }

        public void setHidden(boolean param) {
            this.del.setHidden(param);
        }

        public void setDisplayName(String str) {
            this.del.setDisplayName(str);
        }

        public boolean isPreferred() {
            return this.del.isPreferred();
        }

        public Object getValue(String str) {
            return this.del.getValue(str);
        }

        public void setPreferred(boolean param) {
            this.del.setPreferred(param);
        }

        public boolean canIndexedRead() {
            return this.del.canIndexedRead();
        }

        public Class<InstanceDataObject> getElementType() {
            return this.del.getElementType();
        }

        public InstanceDataObject getIndexedValue(int index) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            return (InstanceDataObject)this.del.getIndexedValue(index);
        }

        public boolean canIndexedWrite() {
            return this.del.canIndexedWrite();
        }

        public void setIndexedValue(int indx, InstanceDataObject val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            this.del.setIndexedValue(indx, (Object)val);
            this.t.scheduleSave();
        }

        public PropertyEditor getIndexedPropertyEditor() {
            return this.del.getIndexedPropertyEditor();
        }
    }

    private static final class P<T>
    extends Node.Property<T> {
        private Node.Property<T> del;
        private InstanceDataObject t;

        public P(Node.Property<T> del, InstanceDataObject t) {
            super(del.getValueType());
            this.del = del;
            this.t = t;
        }

        public void setName(String str) {
            this.del.setName(str);
        }

        public void restoreDefaultValue() throws IllegalAccessException, InvocationTargetException {
            this.del.restoreDefaultValue();
        }

        public void setValue(String str, Object obj) {
            this.del.setValue(str, obj);
        }

        public boolean supportsDefaultValue() {
            return this.del.supportsDefaultValue();
        }

        public boolean canRead() {
            return this.del.canRead();
        }

        public PropertyEditor getPropertyEditor() {
            return this.del.getPropertyEditor();
        }

        public boolean isHidden() {
            return this.del.isHidden();
        }

        public T getValue() throws IllegalAccessException, InvocationTargetException {
            return (T)this.del.getValue();
        }

        public void setExpert(boolean param) {
            this.del.setExpert(param);
        }

        public void setValue(T val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            this.del.setValue(val);
            this.t.scheduleSave();
        }

        public void setShortDescription(String str) {
            this.del.setShortDescription(str);
        }

        public boolean isExpert() {
            return this.del.isExpert();
        }

        public boolean canWrite() {
            return this.del.canWrite();
        }

        public Class<T> getValueType() {
            return this.del.getValueType();
        }

        public String getDisplayName() {
            return this.del.getDisplayName();
        }

        public Enumeration<String> attributeNames() {
            return this.del.attributeNames();
        }

        public String getShortDescription() {
            return this.del.getShortDescription();
        }

        public String getName() {
            return this.del.getName();
        }

        public void setHidden(boolean param) {
            this.del.setHidden(param);
        }

        public void setDisplayName(String str) {
            this.del.setDisplayName(str);
        }

        public boolean isPreferred() {
            return this.del.isPreferred();
        }

        public Object getValue(String str) {
            return this.del.getValue(str);
        }

        public void setPreferred(boolean param) {
            this.del.setPreferred(param);
        }
    }

}

