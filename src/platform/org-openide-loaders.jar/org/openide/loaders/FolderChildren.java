/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileUtil
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.FilterNode$Children
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Template
 *  org.openide.util.Mutex
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 *  org.openide.util.WeakListeners
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package org.openide.loaders;

import java.awt.EventQueue;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.openide.loaders.DataNodeUtils;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.ChangeableDataFilter;
import org.openide.loaders.DataFilter;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.loaders.DataObjectPool;
import org.openide.loaders.FolderChildrenPair;
import org.openide.loaders.FolderList;
import org.openide.loaders.FolderOrder;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.WeakListeners;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

final class FolderChildren
extends Children.Keys<FolderChildrenPair>
implements PropertyChangeListener,
ChangeListener,
FileChangeListener {
    private FolderList folder;
    private final DataFilter filter;
    private PropertyChangeListener listener;
    private FileChangeListener fcListener;
    private ChangeListener changeListener;
    private final Logger err;
    private volatile Collection<FolderChildrenPair> pairs;
    private volatile Task refTask = Task.EMPTY;
    private static final boolean DELAYED_CREATION_ENABLED = !"false".equals(System.getProperty("org.openide.loaders.FolderChildren.delayedCreation"));

    public FolderChildren(DataFolder f) {
        this(f, DataFilter.ALL);
    }

    public FolderChildren(DataFolder f, DataFilter filter) {
        super(true);
        String log = f.getPrimaryFile().isRoot() ? "org.openide.loaders.FolderChildren" : "org.openide.loaders.FolderChildren." + f.getPrimaryFile().getPath().replace('/', '.');
        this.err = Logger.getLogger(log);
        this.folder = FolderList.find(f.getPrimaryFile(), true);
        this.filter = filter;
        this.listener = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this.folder);
        this.fcListener = FileUtil.weakFileChangeListener((FileChangeListener)this, (Object)this.folder.getPrimaryFile());
    }

    DataFilter getFilter() {
        return this.filter;
    }

    void applyKeys(Collection<FolderChildrenPair> pairs) {
        this.setKeys(pairs);
        this.pairs = pairs;
    }

    static void waitRefresh() {
        DataNodeUtils.reqProcessor().post((Runnable)Task.EMPTY, 0, 1).waitFinished();
    }

    @Override
    public void propertyChange(PropertyChangeEvent ev) {
        this.err.log(Level.FINE, "Got a change {0}", ev.getPropertyName());
        this.refreshChildren(RefreshMode.SHALLOW);
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        boolean doRefresh;
        Object source = e.getSource();
        FileObject fo = null;
        if (source instanceof DataObject) {
            DataObject dobj = (DataObject)source;
            fo = dobj.getPrimaryFile();
        } else if (source instanceof FileObject) {
            fo = (FileObject)source;
        }
        if (fo != null) {
            FileObject folderFO = this.folder.getPrimaryFile();
            doRefresh = !fo.isFolder() ? fo.getParent() == folderFO : fo == folderFO;
        } else {
            doRefresh = true;
        }
        if (doRefresh) {
            this.refreshChildren(RefreshMode.DEEP);
        }
    }

    private void refreshChildren(RefreshMode operation) {
        class R
        implements Runnable {
            List<FolderChildrenPair> positioned;
            RefreshMode op;

            R() {
                this.positioned = null;
            }

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                block8 : {
                    if (this.op == RefreshMode.DEEP) {
                        this.positioned = this.getPositionedFolderChildrenPairs();
                        this.op = RefreshMode.DEEP_LATER;
                        Children.MUTEX.postWriteRequest((Runnable)this);
                        return;
                    }
                    FolderChildren.this.err.log(Level.FINE, "refreshChildren {0}", (Object)this.op);
                    try {
                        if (this.op == RefreshMode.CLEAR) {
                            FolderChildren.this.applyKeys(Collections.<FolderChildrenPair>emptyList());
                            break block8;
                        }
                        if (this.op == RefreshMode.DEEP_LATER) {
                            assert (this.positioned != null);
                            FolderChildren.this.applyKeys(Collections.<FolderChildrenPair>emptyList());
                            FolderChildren.this.applyKeys(this.positioned);
                            break block8;
                        }
                        if (this.op == RefreshMode.SHALLOW) {
                            FolderChildren.this.applyKeys(this.getPositionedFolderChildrenPairs());
                            break block8;
                        }
                        throw new IllegalStateException("Unknown op: " + (Object)((Object)this.op));
                    }
                    finally {
                        FolderChildren.this.err.log(Level.FINE, "refreshChildren {0}, done", (Object)this.op);
                    }
                }
            }

            private List<FolderChildrenPair> getPositionedFolderChildrenPairs() {
                FileObject[] arr = FolderChildren.this.folder.getPrimaryFile().getChildren();
                FolderOrder order = FolderOrder.findFor(FolderChildren.this.folder.getPrimaryFile());
                Arrays.sort(arr, order);
                ArrayList<FolderChildrenPair> list = new ArrayList<FolderChildrenPair>(arr.length);
                for (FileObject fo : FileUtil.getOrder(Arrays.asList(arr), (boolean)false)) {
                    DataFilter.FileBased f;
                    if (FolderChildren.this.filter instanceof DataFilter.FileBased && !(f = (DataFilter.FileBased)FolderChildren.this.filter).acceptFileObject(fo)) continue;
                    list.add(new FolderChildrenPair(fo));
                }
                return list;
            }
        }
        R run = new R();
        if (operation == RefreshMode.SHALLOW_IMMEDIATE) {
            this.refTask.waitFinished();
            run.op = RefreshMode.SHALLOW;
            run.run();
        } else {
            run.op = operation;
            this.refTask = DataNodeUtils.reqProcessor().post((Runnable)run);
        }
    }

    protected Node[] createNodes(FolderChildrenPair pair) {
        Node[] arrnode;
        boolean delayCreation = DELAYED_CREATION_ENABLED && EventQueue.isDispatchThread() && !pair.primaryFile.isFolder();
        DelayedNode ret = delayCreation ? new DelayedNode(pair) : this.createNode(pair);
        if (ret == null) {
            arrnode = null;
        } else {
            Node[] arrnode2 = new Node[1];
            arrnode = arrnode2;
            arrnode2[0] = ret;
        }
        return arrnode;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final Node createNode(FolderChildrenPair pair) {
        long time;
        Node ret;
        block7 : {
            time = System.currentTimeMillis();
            ret = null;
            try {
                FileObject pf = pair.primaryFile;
                DataObject obj = DataObject.find(pf);
                if (!obj.isValid() || !pf.equals((Object)obj.getPrimaryFile()) || this.filter != null && !this.filter.acceptDataObject(obj)) break block7;
                ret = obj.getClonedNodeDelegate(this.filter);
                if (obj.isValid()) break block7;
                ret = null;
            }
            catch (DataObjectNotFoundException e) {
                try {
                    Logger.getLogger(FolderChildren.class.getName()).log(Level.FINE, null, e);
                }
                catch (Throwable var8_9) {
                    long took = System.currentTimeMillis() - time;
                    if (this.err.isLoggable(Level.FINE)) {
                        this.err.log(Level.FINE, "createNodes: {0} took: {1} ms", new Object[]{pair, took});
                        this.err.log(Level.FINE, "  returning: {0}", (Object)ret);
                    }
                    throw var8_9;
                }
                long took = System.currentTimeMillis() - time;
                if (this.err.isLoggable(Level.FINE)) {
                    this.err.log(Level.FINE, "createNodes: {0} took: {1} ms", new Object[]{pair, took});
                    this.err.log(Level.FINE, "  returning: {0}", (Object)ret);
                }
            }
        }
        long took = System.currentTimeMillis() - time;
        if (this.err.isLoggable(Level.FINE)) {
            this.err.log(Level.FINE, "createNodes: {0} took: {1} ms", new Object[]{pair, took});
            this.err.log(Level.FINE, "  returning: {0}", (Object)ret);
        }
        return ret;
    }

    public Node[] getNodes(boolean optimalResult) {
        Node[] arr;
        Level previous = null;
        int limit = 1000;
        int round = 0;
        do {
            if (optimalResult) {
                this.waitOptimalResult();
            }
            arr = this.getNodes();
            boolean stop = true;
            for (Node n : arr) {
                if (!(n instanceof DelayedNode)) continue;
                DelayedNode dn = (DelayedNode)n;
                if (!FolderChildren.checkChildrenMutex() || !dn.waitFinished()) continue;
                this.err.log(Level.FINE, "Waiting for delayed node {0}", dn);
                stop = false;
                if (round <= 600) continue;
                this.err.log(Level.WARNING, "Scheduling additional refresh for {0}", dn);
                dn.scheduleRefresh("fallback");
            }
            if (stop) break;
            if (round == 500) {
                this.err.warning("getNodes takes ages, turning on logging");
                previous = this.err.getLevel();
                this.err.setLevel(Level.FINE);
            }
            if (round == 1000) {
                this.err.warning(FolderChildren.threadDump());
                this.err.setLevel(previous);
                boolean thrw = false;
                if (!$assertionsDisabled) {
                    thrw = true;
                    if (!true) {
                        throw new AssertionError();
                    }
                }
                if (!thrw) break;
                throw new IllegalStateException("Too many repetitions in getNodes(true). Giving up.");
            }
            ++round;
        } while (true);
        if (previous != null) {
            this.err.setLevel(previous);
        }
        return arr;
    }

    private static void appendThread(StringBuffer sb, String indent, Thread t, Map<Thread, StackTraceElement[]> data) {
        sb.append(indent).append("Thread ").append(t.getName()).append('\n');
        StackTraceElement[] stack = data.get(t);
        if (stack != null) {
            for (StackTraceElement e : stack) {
                sb.append("\tat ").append(e.getClassName()).append('.').append(e.getMethodName()).append('(').append(e.getFileName()).append(':').append(e.getLineNumber()).append(")\n");
            }
        }
    }

    private static void appendGroup(StringBuffer sb, String indent, ThreadGroup tg, Map<Thread, StackTraceElement[]> data) {
        sb.append(indent).append("Group ").append(tg.getName()).append('\n');
        indent = indent.concat("  ");
        int groups = tg.activeGroupCount();
        ThreadGroup[] chg = new ThreadGroup[groups];
        tg.enumerate(chg, false);
        for (ThreadGroup inner : chg) {
            if (inner == null) continue;
            FolderChildren.appendGroup(sb, indent, inner, data);
        }
        int threads = tg.activeCount();
        Thread[] cht = new Thread[threads];
        tg.enumerate(cht, false);
        for (Thread t : cht) {
            if (t == null) continue;
            FolderChildren.appendThread(sb, indent, t, data);
        }
    }

    private static String threadDump() {
        Map<Thread, StackTraceElement[]> all = Thread.getAllStackTraces();
        ThreadGroup root = Thread.currentThread().getThreadGroup();
        while (root.getParent() != null) {
            root = root.getParent();
        }
        StringBuffer sb = new StringBuffer();
        FolderChildren.appendGroup(sb, "", root, all);
        return sb.toString();
    }

    public Node findChild(String name) {
        if (FolderChildren.checkChildrenMutex()) {
            this.waitOptimalResult();
        }
        int i = 0;
        Collection<FolderChildrenPair> tmp = this.pairs;
        if (tmp != null) {
            for (FolderChildrenPair p : tmp) {
                FileObject pf = p.primaryFile;
                if (pf.getNameExt().startsWith(name)) {
                    try {
                        Node original = DataObject.find(pf).getNodeDelegate();
                        if (!original.getName().equals(name)) continue;
                        Node candidate = this.getNodeAt(i);
                        if (candidate != null && candidate.getName().equals(name)) {
                            return candidate;
                        }
                    }
                    catch (DataObjectNotFoundException ex) {
                        this.err.log(Level.INFO, "Can't find object for " + (Object)pf, ex);
                    }
                }
                ++i;
            }
        }
        return Children.Keys.super.findChild(name);
    }

    private void waitOptimalResult() {
        if (FolderChildren.checkChildrenMutex()) {
            this.err.fine("waitOptimalResult");
            if (!this.isInitialized()) {
                this.refreshChildren(RefreshMode.SHALLOW);
            }
            this.folder.waitProcessingFinished();
            this.refTask.waitFinished();
            this.err.fine("waitOptimalResult: waitProcessingFinished");
        } else {
            Logger.getLogger(FolderChildren.class.getName()).log(Level.WARNING, null, new IllegalStateException("getNodes(true) called while holding the Children.MUTEX"));
        }
    }

    public int getNodesCount(boolean optimalResult) {
        if (optimalResult) {
            this.waitOptimalResult();
        }
        return this.getNodesCount();
    }

    static boolean checkChildrenMutex() {
        return !Children.MUTEX.isReadAccess() && !Children.MUTEX.isWriteAccess();
    }

    protected void addNotify() {
        this.err.fine("addNotify begin");
        this.folder.addPropertyChangeListener(this.listener);
        this.folder.getPrimaryFile().addFileChangeListener(this.fcListener);
        if (this.filter instanceof ChangeableDataFilter) {
            ChangeableDataFilter chF = (ChangeableDataFilter)this.filter;
            this.changeListener = WeakListeners.change((ChangeListener)this, (Object)chF);
            chF.addChangeListener(this.changeListener);
        }
        this.refreshChildren(RefreshMode.SHALLOW);
        this.err.fine("addNotify end");
    }

    protected void removeNotify() {
        this.err.fine("removeNotify begin");
        this.folder.getPrimaryFile().removeFileChangeListener(this.fcListener);
        this.folder.removePropertyChangeListener(this.listener);
        if (this.filter instanceof ChangeableDataFilter) {
            ((ChangeableDataFilter)this.filter).removeChangeListener(this.changeListener);
            this.changeListener = null;
        }
        List<FolderChildrenPair> emptyList = Collections.emptyList();
        this.applyKeys(emptyList);
        this.err.fine("removeNotify end");
    }

    public String toString() {
        return this.folder != null ? this.folder.getPrimaryFile().toString() : Object.super.toString();
    }

    public void fileAttributeChanged(FileAttributeEvent fe) {
        if ("NetBeansAttrAssignedLoader".equals(fe.getName())) {
            DataObjectPool.checkAttributeChanged(fe);
            this.refreshKey((Object)new FolderChildrenPair(fe.getFile()));
            this.refreshChildren(RefreshMode.SHALLOW_IMMEDIATE);
        }
    }

    public void fileChanged(FileEvent fe) {
    }

    public void fileDataCreated(FileEvent fe) {
        this.refreshChildren(RefreshMode.SHALLOW);
    }

    public void fileDeleted(FileEvent fe) {
        this.refreshChildren(RefreshMode.SHALLOW);
    }

    public void fileFolderCreated(FileEvent fe) {
        this.refreshChildren(RefreshMode.SHALLOW);
    }

    public void fileRenamed(FileRenameEvent fe) {
        this.refreshChildren(RefreshMode.SHALLOW);
    }

    private final class DelayedLkp
    extends AbstractLookup {
        DelayedNode node;
        final InstanceContent ic;

        public DelayedLkp(InstanceContent content) {
            super((AbstractLookup.Content)content);
            this.ic = content;
        }

        protected void beforeLookup(Lookup.Template<?> template) {
            DataObject obj;
            Class type = template.getType();
            if (DataObject.class.isAssignableFrom(type) && (obj = this.convert(this.node)) != null) {
                this.ic.add((Object)obj);
            }
        }

        public DataObject convert(DelayedNode obj) {
            FolderChildrenPair pair = obj.pair;
            if (EventQueue.isDispatchThread()) {
                FolderChildren.this.err.log(Level.WARNING, "Attempt to obtain DataObject for {0} from EDT", (Object)pair.primaryFile);
                boolean assertsOn = false;
                if (!$assertionsDisabled) {
                    assertsOn = true;
                    if (!true) {
                        throw new AssertionError();
                    }
                }
                if (assertsOn) {
                    FolderChildren.this.err.log(Level.INFO, "Ineffective since #199391 was implemented", new Exception("Find for " + (Object)pair.primaryFile));
                }
            }
            try {
                return DataObject.find(pair.primaryFile);
            }
            catch (DataObjectNotFoundException ex) {
                FolderChildren.this.err.log(Level.INFO, "Cannot convert " + (Object)pair.primaryFile, ex);
                return null;
            }
        }
    }

    private final class DelayedNode
    extends FilterNode
    implements Runnable {
        final FolderChildrenPair pair;
        private RequestProcessor.Task task;

        public DelayedNode(FolderChildrenPair pair) {
            this(pair, folderChildren.new DelayedLkp(new InstanceContent()));
        }

        private DelayedNode(FolderChildrenPair pair, DelayedLkp lkp) {
            this(pair, new AbstractNode(FilterNode.Children.LEAF, (Lookup)lkp));
            lkp.ic.add((Object)pair.primaryFile);
            lkp.node = this;
        }

        private DelayedNode(FolderChildrenPair pair, AbstractNode an) {
            super((Node)an);
            this.pair = pair;
            an.setName(pair.primaryFile.getNameExt());
            an.setIconBaseWithExtension("org/openide/loaders/unknown.gif");
            this.scheduleRefresh("constructor");
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            Node n = FolderChildren.this.createNode(this.pair);
            if (n != null) {
                this.changeOriginal(n, !n.isLeaf());
            } else {
                FolderChildren.this.refreshKey(this.pair);
            }
            DelayedNode delayedNode = this;
            synchronized (delayedNode) {
                this.task = null;
            }
            FolderChildren.this.err.log(Level.FINE, "delayed node refreshed {0} original: {1}", new Object[]{this, n});
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public final boolean waitFinished() {
            RequestProcessor.Task t;
            DelayedNode delayedNode = this;
            synchronized (delayedNode) {
                t = this.task;
                if (t == null) {
                    return false;
                }
            }
            FolderChildren.this.err.log(Level.FINE, "original before wait: {0}", (Object)this.getOriginal());
            t.waitFinished();
            FolderChildren.this.err.log(Level.FINE, "original after wait: {0}", (Object)this.getOriginal());
            FolderChildren.this.err.log(Level.FINE, "task after waitFinished {0}", (Object)this.task);
            return true;
        }

        final synchronized void scheduleRefresh(String by) {
            this.task = DataNodeUtils.reqProcessor().post((Runnable)this);
            FolderChildren.this.err.log(Level.FINE, "Task initialized by {0} to {1} for {2}", new Object[]{by, this.task, this});
        }
    }

    private static enum RefreshMode {
        SHALLOW,
        SHALLOW_IMMEDIATE,
        DEEP,
        DEEP_LATER,
        CLEAR;
        

        private RefreshMode() {
        }
    }

}

