/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.Modules
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.Node
 *  org.openide.util.Enumerations
 *  org.openide.util.Enumerations$Processor
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.Lookups
 */
package org.openide.loaders;

import java.awt.Image;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.OptionalDataException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.EventListener;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.loaders.BrokenDataShadow;
import org.openide.loaders.DataFilter;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataLoader;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.DataObjectPool;
import org.openide.loaders.DataShadow;
import org.openide.loaders.DefaultDataObject;
import org.openide.loaders.ExtensionList;
import org.openide.loaders.FileEntry;
import org.openide.loaders.InstanceDataObject;
import org.openide.loaders.MimeFactory;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.loaders.OperationEvent;
import org.openide.loaders.OperationListener;
import org.openide.loaders.ShadowChangeAdapter;
import org.openide.loaders.UniFileLoader;
import org.openide.loaders.XMLDataObject;
import org.openide.modules.ModuleInfo;
import org.openide.modules.Modules;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.Enumerations;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

public abstract class DataLoaderPool
implements Serializable {
    static final long serialVersionUID = -360141823874889956L;
    private static MultiFileLoader[] systemLoaders;
    private static MultiFileLoader[] defaultLoaders;
    private static DataLoaderPool DEFAULT;
    private transient DataLoader[] loaderArray;
    private transient List<DataLoader> allLoaders;
    private transient List<DataLoader> prefLoaders;
    private transient int cntchanges;
    private transient EventListenerList listeners;
    private transient DataLoader preferredLoader;
    static final EmptyDataLoaderRecognized emptyDataLoaderRecognized;

    public static synchronized DataLoaderPool getDefault() {
        if (DEFAULT == null && (DataLoaderPool.DEFAULT = (DataLoaderPool)Lookup.getDefault().lookup(DataLoaderPool.class)) == null) {
            DEFAULT = new DefaultPool();
        }
        return DEFAULT;
    }

    protected DataLoaderPool() {
    }

    protected DataLoaderPool(DataLoader loader) {
        this.preferredLoader = loader;
    }

    protected abstract Enumeration<? extends DataLoader> loaders();

    public final synchronized void addChangeListener(ChangeListener chl) {
        if (this.listeners == null) {
            this.listeners = new EventListenerList();
        }
        this.listeners.add(ChangeListener.class, chl);
    }

    public final synchronized void removeChangeListener(ChangeListener chl) {
        if (this.listeners != null) {
            this.listeners.remove(ChangeListener.class, chl);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final void fireChangeEvent(ChangeEvent che) {
        Object[] list;
        DataLoaderPool dataLoaderPool = this;
        synchronized (dataLoaderPool) {
            ++this.cntchanges;
            this.loaderArray = null;
            this.allLoaders = null;
            this.prefLoaders = null;
            if (this.listeners == null) {
                return;
            }
            list = this.listeners.getListenerList();
        }
        for (int i = list.length - 2; i >= 0; i -= 2) {
            if (list[i] != ChangeListener.class) continue;
            ChangeListener l = (ChangeListener)list[i + 1];
            l.stateChanged(che);
        }
    }

    public static OperationListener createWeakOperationListener(OperationListener l, Object s) {
        return (OperationListener)WeakListeners.create(OperationListener.class, (EventListener)l, (Object)s);
    }

    public final synchronized void addOperationListener(OperationListener l) {
        if (this.listeners == null) {
            this.listeners = new EventListenerList();
        }
        this.listeners.add(OperationListener.class, l);
    }

    public final synchronized void removeOperationListener(OperationListener l) {
        if (this.listeners != null) {
            this.listeners.remove(OperationListener.class, l);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void fireOperationEvent(OperationEvent ev, int type) {
        Object[] list;
        DataLoaderPool dataLoaderPool = this;
        synchronized (dataLoaderPool) {
            if (this.listeners == null) {
                return;
            }
            list = this.listeners.getListenerList();
        }
        block12 : for (int i = list.length - 2; i >= 0; i -= 2) {
            if (list[i] != OperationListener.class) continue;
            OperationListener l = (OperationListener)list[i + 1];
            switch (type) {
                case 1: {
                    l.operationCopy((OperationEvent.Copy)ev);
                    continue block12;
                }
                case 2: {
                    l.operationMove((OperationEvent.Move)ev);
                    continue block12;
                }
                case 3: {
                    l.operationDelete(ev);
                    continue block12;
                }
                case 4: {
                    l.operationRename((OperationEvent.Rename)ev);
                    continue block12;
                }
                case 5: {
                    l.operationCreateShadow((OperationEvent.Copy)ev);
                    continue block12;
                }
                case 6: {
                    l.operationCreateFromTemplate((OperationEvent.Copy)ev);
                    continue block12;
                }
                case 7: {
                    l.operationPostCreate(ev);
                }
            }
        }
    }

    public final Enumeration<DataLoader> allLoaders() {
        return this.computeLoaders(true);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final Enumeration<DataLoader> computeLoaders(boolean computeAll) {
        List<DataLoader> pref;
        List<DataLoader> all;
        int oldcnt;
        DataLoaderPool dataLoaderPool = this;
        synchronized (dataLoaderPool) {
            all = this.allLoaders;
            pref = this.prefLoaders;
            oldcnt = this.cntchanges;
        }
        if (all == null) {
            all = new ArrayList<DataLoader>();
            if (this.preferredLoader != null) {
                all.add(this.preferredLoader);
            }
            all.addAll(Arrays.asList(DataLoaderPool.getSystemLoaders()));
            Enumeration<? extends DataLoader> en = this.loaders();
            while (en.hasMoreElements()) {
                all.add(en.nextElement());
            }
            pref = new ArrayList<DataLoader>(all);
            all.addAll(Arrays.asList(DataLoaderPool.getDefaultLoaders()));
            DataLoaderPool dataLoaderPool2 = this;
            synchronized (dataLoaderPool2) {
                if (oldcnt == this.cntchanges) {
                    this.allLoaders = all;
                    this.prefLoaders = pref;
                }
            }
        }
        return Collections.enumeration(computeAll ? all : pref);
    }

    final Enumeration<DataObject.Factory> allLoaders(FileObject fo) {
        class MimeEnum
        implements Enumeration<DataObject.Factory> {
            final String mime;
            Enumeration<? extends DataObject.Factory> delegate;

            public MimeEnum(String mime) {
                this.mime = mime;
            }

            private Enumeration<? extends DataObject.Factory> delegate() {
                if (this.delegate == null) {
                    String path = "Loaders/" + this.mime + "/Factories";
                    this.delegate = Collections.enumeration(Lookups.forPath((String)path).lookupAll(DataObject.Factory.class));
                }
                return this.delegate;
            }

            @Override
            public boolean hasMoreElements() {
                return this.delegate().hasMoreElements();
            }

            @Override
            public DataObject.Factory nextElement() {
                return this.delegate().nextElement();
            }
        }
        String mime = fo.getMIMEType();
        Enumeration mimeLoaders = new MimeEnum(mime);
        mimeLoaders = Enumerations.concat((Enumeration)mimeLoaders, (Enumeration)new MimeEnum("content/unknown"));
        Enumeration first = this.computeLoaders(false);
        try {
            if (fo.getFileSystem().isDefault()) {
                first = Enumerations.concat(first, (Enumeration)Enumerations.singleton((Object)DataLoaderPool.getFolderLoader()));
            }
        }
        catch (FileStateInvalidException ex) {
            // empty catch block
        }
        return Enumerations.concat(first, (Enumeration)Enumerations.concat((Enumeration)mimeLoaders, (Enumeration)Enumerations.array((Object[])DataLoaderPool.getDefaultLoaders())));
    }

    public DataLoader[] toArray() {
        DataLoader[] localArray = this.loaderArray;
        if (localArray != null) {
            return localArray;
        }
        ArrayList<DataLoader> loaders = new ArrayList<DataLoader>();
        Enumeration<? extends DataLoader> en = this.loaders();
        while (en.hasMoreElements()) {
            loaders.add(en.nextElement());
        }
        localArray = new DataLoader[loaders.size()];
        localArray = loaders.toArray(localArray);
        this.loaderArray = localArray;
        return localArray;
    }

    public final DataLoader firstProducerOf(Class<? extends DataObject> clazz) {
        Enumeration<DataLoader> en = this.allLoaders();
        while (en.hasMoreElements()) {
            DataLoader dl = en.nextElement();
            if (!dl.getRepresentationClass().isAssignableFrom(clazz)) continue;
            return dl;
        }
        return null;
    }

    public final Enumeration<DataLoader> producersOf(Class<? extends DataObject> clazz) {
        class ProducerOf
        implements Enumerations.Processor<DataLoader, DataLoader> {
            final /* synthetic */ Class val$clazz;

            ProducerOf() {
                this.val$clazz = var2_2;
            }

            public DataLoader process(DataLoader dl, Collection ignore) {
                return this.val$clazz.isAssignableFrom(dl.getRepresentationClass()) ? dl : null;
            }
        }
        return Enumerations.filter(this.allLoaders(), (Enumerations.Processor)new ProducerOf(this, clazz));
    }

    public DataObject findDataObject(FileObject fo) throws IOException {
        return this.findDataObject(fo, emptyDataLoaderRecognized);
    }

    public DataObject findDataObject(FileObject fo, DataLoader.RecognizedFiles r) throws IOException {
        DataObject obj;
        DataLoader pref = DataLoaderPool.getPreferredLoader(fo);
        if (pref != null && (obj = pref.findDataObject(fo, r)) != null) {
            return obj;
        }
        Set recognized = r == emptyDataLoaderRecognized ? emptyDataLoaderRecognized : new HashSet<E>();
        Enumeration<DataObject.Factory> en = this.allLoaders(fo);
        while (en.hasMoreElements()) {
            DataObject.Factory l = en.nextElement();
            DataObject obj2 = l instanceof DataLoader ? l.findDataObject(fo, recognized) : DataObjectPool.handleFindDataObject(l, fo, recognized);
            if (!recognized.isEmpty()) {
                for (FileObject f : recognized) {
                    r.markRecognized(f);
                }
                recognized.clear();
            }
            if (obj2 == null) continue;
            return obj2;
        }
        return null;
    }

    public static void setPreferredLoader(FileObject fo, DataLoader loader) throws IOException {
        DataLoader prev = DataLoaderPool.getPreferredLoader(fo);
        if (prev == loader) {
            return;
        }
        if (loader == null) {
            fo.setAttribute("NetBeansAttrAssignedLoader", (Object)null);
        } else {
            Class<?> c = loader.getClass();
            fo.setAttribute("NetBeansAttrAssignedLoader", (Object)c.getName());
            ModuleInfo module = Modules.getDefault().ownerOf(c);
            if (module != null) {
                fo.setAttribute("NetBeansAttrAssignedLoaderModule", (Object)module.getCodeNameBase());
            }
        }
        HashSet<FileObject> one = new HashSet<FileObject>();
        one.add(fo);
        if (!DataObjectPool.getPOOL().revalidate(one).isEmpty()) {
            DataObject.LOG.log(Level.FINE, "It was not possible to invalidate data object: {0}", (Object)fo);
        }
    }

    public static DataLoader getPreferredLoader(FileObject fo) {
        String assignedLoaderName = (String)fo.getAttribute("NetBeansAttrAssignedLoader");
        if (assignedLoaderName != null) {
            String modulename = (String)fo.getAttribute("NetBeansAttrAssignedLoaderModule");
            if (modulename != null) {
                Iterator<E> modules = Lookup.getDefault().lookupAll(ModuleInfo.class).iterator();
                boolean ok = false;
                while (modules.hasNext()) {
                    ModuleInfo module = (ModuleInfo)modules.next();
                    if (!module.getCodeNameBase().equals(modulename)) continue;
                    if (module.isEnabled()) {
                        ok = true;
                        break;
                    }
                    return null;
                }
                if (!ok) {
                    return null;
                }
            }
            try {
                ClassLoader load = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
                if (load == null) {
                    load = DataLoaderPool.class.getClassLoader();
                }
                return DataLoader.getLoader(Class.forName(assignedLoaderName, true, load).asSubclass(DataLoader.class));
            }
            catch (Exception ex) {
                Logger.getLogger(DataLoaderPool.class.getName()).log(Level.WARNING, null, ex);
            }
        }
        return null;
    }

    public static <T extends DataObject> DataObject.Factory factory(Class<T> dataObjectClass, String mimeType, Image image) {
        return new MimeFactory<T>(dataObjectClass, mimeType, image, null);
    }

    static <T extends DataObject> DataObject.Factory factory(FileObject fo) throws ClassNotFoundException {
        return MimeFactory.layer(fo);
    }

    private static synchronized MultiFileLoader[] getSystemLoaders() {
        if (systemLoaders == null) {
            systemLoaders = new MultiFileLoader[]{DataLoader.getLoader(ShadowLoader.class), DataLoader.getLoader(InstanceLoaderSystem.class)};
        }
        return systemLoaders;
    }

    private static synchronized MultiFileLoader[] getDefaultLoaders() {
        if (defaultLoaders == null) {
            defaultLoaders = new MultiFileLoader[]{DataLoader.getLoader(FolderLoader.class), DataLoader.getLoader(XMLDataObject.Loader.class), DataLoader.getLoader(InstanceLoader.class), DataLoader.getLoader(DefaultLoader.class)};
        }
        return defaultLoaders;
    }

    static MultiFileLoader getDefaultFileLoader() {
        return DataLoaderPool.getDefaultLoaders()[3];
    }

    static MultiFileLoader getFolderLoader() {
        return DataLoaderPool.getDefaultLoaders()[0];
    }

    static MultiFileLoader getShadowLoader() {
        return DataLoaderPool.getSystemLoaders()[0];
    }

    static MultiFileLoader getInstanceLoader() {
        return DataLoaderPool.getDefaultLoaders()[2];
    }

    static {
        emptyDataLoaderRecognized = new EmptyDataLoaderRecognized();
    }

    static final class FolderLoader
    extends UniFileLoader {
        static final long serialVersionUID = -8325525104047820255L;

        public FolderLoader() {
            super("org.openide.loaders.DataFolder");
        }

        @Override
        protected String actionsContext() {
            return "Loaders/folder/any/Actions";
        }

        @Override
        protected String defaultDisplayName() {
            return NbBundle.getMessage(DataLoaderPool.class, (String)"LBL_folder_loader_display_name");
        }

        @Override
        protected FileObject findPrimaryFile(FileObject fo) {
            if (fo.isFolder()) {
                return fo;
            }
            return null;
        }

        @Override
        protected MultiDataObject.Entry createPrimaryEntry(MultiDataObject obj, FileObject primaryFile) {
            return new FileEntry.Folder(obj, primaryFile);
        }

        @Override
        protected MultiDataObject createMultiObject(FileObject primaryFile) throws DataObjectExistsException, IOException {
            return new DataFolder(primaryFile, this);
        }

        MultiDataObject createMultiObject(FileObject primaryFile, DataFolder original) throws DataObjectExistsException, IOException {
            class NodeSharingDataFolder
            extends DataFolder {
                final /* synthetic */ DataFolder val$original;

                public NodeSharingDataFolder() throws DataObjectExistsException, IllegalArgumentException {
                    this.val$original = fileObject;
                    super((FileObject)fo, this$0);
                }

                @Override
                protected Node createNodeDelegate() {
                    return new FilterNode(this.val$original.getNodeDelegate());
                }

                @Override
                Node getClonedNodeDelegate(DataFilter filter) {
                    return new FilterNode(this.val$original.getClonedNodeDelegate(filter));
                }
            }
            return new NodeSharingDataFolder(this, primaryFile, original);
        }

        @Override
        public void readExternal(ObjectInput oi) throws IOException, ClassNotFoundException {
            try {
                super.readExternal(oi);
            }
            catch (OptionalDataException ode) {
                // empty catch block
            }
        }

    }

    private static final class ShadowLoader
    extends UniFileLoader {
        static final long serialVersionUID = -11013405787959120L;
        private static ShadowChangeAdapter changeAdapter = new ShadowChangeAdapter();

        public ShadowLoader() {
            super("org.openide.loaders.DataShadow");
        }

        @Override
        protected String defaultDisplayName() {
            return NbBundle.getMessage(DataLoaderPool.class, (String)"LBL_shadow_loader_display_name");
        }

        @Override
        protected FileObject findPrimaryFile(FileObject fo) {
            if (fo.isData() && fo.hasExt("shadow")) {
                return fo;
            }
            return null;
        }

        @Override
        protected MultiDataObject.Entry createPrimaryEntry(MultiDataObject obj, FileObject primaryFile) {
            return new FileEntry(obj, primaryFile);
        }

        @Override
        protected MultiDataObject createMultiObject(FileObject primaryFile) throws DataObjectExistsException, IOException {
            try {
                DataObject d = DataShadow.deserialize(primaryFile);
                if (d != null) {
                    return new DataShadow(primaryFile, d, this);
                }
            }
            catch (IOException ex) {
                // empty catch block
            }
            return new BrokenDataShadow(primaryFile, this);
        }

        @Override
        public void writeExternal(ObjectOutput oo) throws IOException {
        }

        @Override
        public void readExternal(ObjectInput oi) throws IOException, ClassNotFoundException {
        }
    }

    private static final class DefaultLoader
    extends MultiFileLoader {
        static final long serialVersionUID = -6761887227412396555L;

        public DefaultLoader() {
            super("org.openide.loaders.DefaultDataObject");
        }

        @Override
        protected String actionsContext() {
            return "Loaders/content/unknown/Actions";
        }

        @Override
        protected String defaultDisplayName() {
            return NbBundle.getMessage(DataLoaderPool.class, (String)"LBL_default_loader_display_name");
        }

        @Override
        protected FileObject findPrimaryFile(FileObject fo) {
            if (fo.isFolder()) {
                return null;
            }
            return fo;
        }

        @Override
        protected MultiDataObject createMultiObject(FileObject primaryFile) throws DataObjectExistsException, IOException {
            return new DefaultDataObject(primaryFile, this);
        }

        @Override
        protected MultiDataObject.Entry createPrimaryEntry(MultiDataObject obj, FileObject primaryFile) {
            return new FileEntry(obj, primaryFile);
        }

        @Override
        protected MultiDataObject.Entry createSecondaryEntry(MultiDataObject obj, FileObject secondaryFile) {
            throw new UnsupportedOperationException();
        }

        @Override
        void checkFiles(MultiDataObject obj) {
        }
    }

    private static class InstanceLoader
    extends UniFileLoader {
        private static final long serialVersionUID = -3462727693843631328L;

        public InstanceLoader() {
            super("org.openide.loaders.InstanceDataObject");
        }

        protected void initialize() {
            super.initialize();
            this.setExtensions(null);
        }

        @Override
        protected String actionsContext() {
            return "Loaders/application/x-nbsettings/Actions";
        }

        @Override
        protected String defaultDisplayName() {
            return NbBundle.getMessage(DataLoaderPool.class, (String)"LBL_instance_loader_display_name");
        }

        @Override
        protected MultiDataObject createMultiObject(FileObject primaryFile) throws DataObjectExistsException, IOException {
            InstanceDataObject obj = new InstanceDataObject(primaryFile, this);
            return obj;
        }

        @Override
        public void writeExternal(ObjectOutput oo) throws IOException {
            oo.writeObject(this);
            super.writeExternal(oo);
        }

        @Override
        public void readExternal(ObjectInput oi) throws IOException, ClassNotFoundException {
            Object o = oi.readObject();
            if (o instanceof SystemAction[]) {
                this.setActions((SystemAction[])o);
                this.setExtensions(this.getExtensions());
            } else if (o instanceof ExtensionList) {
                ExtensionList list = (ExtensionList)o;
                this.setExtensions(list);
            } else {
                super.readExternal(oi);
                this.setExtensions(this.getExtensions());
            }
        }

        @Override
        public void setExtensions(ExtensionList ext) {
            super.setExtensions(this.initExtensions(ext));
        }

        private ExtensionList initExtensions(ExtensionList ext) {
            String[] rqext = this.getRequiredExt();
            if (ext == null) {
                ext = new ExtensionList();
            }
            for (int i = 0; i < rqext.length; ++i) {
                ext.addExtension(rqext[i]);
            }
            return ext;
        }

        @Override
        protected FileObject findPrimaryFile(FileObject fo) {
            FileObject r = super.findPrimaryFile(fo);
            if (r != null && r.getPath().equals("loaders.ser")) {
                try {
                    if (r.getFileSystem().isDefault()) {
                        return null;
                    }
                }
                catch (FileStateInvalidException e) {
                    Logger.getLogger(DataLoaderPool.class.getName()).log(Level.WARNING, null, (Throwable)e);
                }
            }
            return r;
        }

        protected String[] getRequiredExt() {
            return new String[]{"instance", "ser", "settings"};
        }
    }

    private static class InstanceLoaderSystem
    extends InstanceLoader {
        private static final long serialVersionUID = -935749906623354837L;

        @Override
        protected FileObject findPrimaryFile(FileObject fo) {
            FileSystem fs = null;
            try {
                fs = fo.getFileSystem();
            }
            catch (FileStateInvalidException e) {
                return null;
            }
            if (!fs.isDefault()) {
                return null;
            }
            return super.findPrimaryFile(fo);
        }

        @Override
        protected String[] getRequiredExt() {
            return new String[]{"instance", "settings"};
        }
    }

    private static final class DefaultPool
    extends DataLoaderPool
    implements LookupListener {
        private final Lookup.Result<DataLoader> result = Lookup.getDefault().lookupResult(DataLoader.class);

        public DefaultPool() {
            this.result.addLookupListener((LookupListener)this);
        }

        @Override
        protected Enumeration<? extends DataLoader> loaders() {
            return Collections.enumeration(this.result.allInstances());
        }

        public void resultChanged(LookupEvent e) {
            this.fireChangeEvent(new ChangeEvent(this));
        }
    }

    private static class EmptyDataLoaderRecognized
    implements DataLoader.RecognizedFiles,
    Set<FileObject> {
        private EmptyDataLoaderRecognized() {
        }

        @Override
        public void markRecognized(FileObject fo) {
        }

        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return true;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @Override
        public Iterator<FileObject> iterator() {
            return Collections.emptySet().iterator();
        }

        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return a;
        }

        @Override
        public boolean add(FileObject e) {
            return false;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean addAll(Collection<? extends FileObject> c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            return false;
        }

        @Override
        public void clear() {
        }
    }

}

