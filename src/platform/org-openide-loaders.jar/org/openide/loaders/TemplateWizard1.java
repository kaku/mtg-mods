/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.Mnemonics
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.view.BeanTreeView
 *  org.openide.explorer.view.NodeTreeModel
 *  org.openide.explorer.view.Visualizer
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.nodes.Children
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.FilterNode$Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.AsyncGUIJob
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 */
package org.openide.loaders;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.FocusEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.net.URL;
import java.util.LinkedList;
import java.util.ResourceBundle;
import javax.accessibility.AccessibleContext;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.text.EditorKit;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import org.openide.WizardDescriptor;
import org.openide.awt.HtmlBrowser;
import org.openide.awt.Mnemonics;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.BeanTreeView;
import org.openide.explorer.view.NodeTreeModel;
import org.openide.explorer.view.Visualizer;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFilter;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataShadow;
import org.openide.loaders.TemplateWizard;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.AsyncGUIJob;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;

final class TemplateWizard1
extends JPanel
implements DataFilter,
ExplorerManager.Provider,
PropertyChangeListener,
AsyncGUIJob {
    private static final String PROP_CONTENT_SELECTED_INDEX = "WizardPanel_contentSelectedIndex";
    private static final String PROP_CONTENT_DATA = "WizardPanel_contentData";
    private ChangeListener listener;
    private DataObject template;
    private DataFolder templatesRoot;
    private ExplorerManager manager;
    private InitData initData;
    private JLabel browserLabel;
    private JPanel browserPanel;
    private JLabel noBrowser;
    private JLabel templatesLabel;
    private TemplatesTreeView treeView;
    private HtmlBrowser browser;

    public TemplateWizard1() {
        this.initComponents();
        this.treeView = new TemplatesTreeView();
        this.treeView.setDefaultActionAllowed(false);
        this.treeView.setPopupAllowed(false);
        this.treeView.setSelectionMode(1);
        GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.gridx = 0;
        gridBagConstraints1.gridy = 1;
        gridBagConstraints1.fill = 1;
        gridBagConstraints1.insets = new Insets(0, 0, 11, 0);
        gridBagConstraints1.weightx = 1.0;
        gridBagConstraints1.weighty = 1.0;
        this.add((Component)((Object)this.treeView), gridBagConstraints1);
        ResourceBundle bundle = NbBundle.getBundle(TemplateWizard1.class);
        this.setName(bundle.getString("LAB_TemplateChooserPanelName"));
        this.putClientProperty("WizardPanel_contentSelectedIndex", 0);
        this.putClientProperty("WizardPanel_contentData", new String[]{this.getName(), "..."});
        this.putClientProperty("LAB_SelectTemplateBorder", bundle.getString("LAB_SelectTemplateBorder"));
        this.putClientProperty("LAB_TemplateDescriptionBorder", bundle.getString("LAB_TemplateDescriptionBorder"));
        this.putClientProperty("ACSD_TemplatesTree", bundle.getString("ACSD_TemplatesTree"));
        this.putClientProperty("ACSD_TemplateWizard1", bundle.getString("ACSD_TemplateWizard1"));
        this.updateRootNode(null);
        this.templatesLabel.setLabelFor((Component)((Object)this.treeView));
        this.noBrowser.setText(bundle.getString("MSG_InitDescription"));
        CardLayout card = (CardLayout)this.browserPanel.getLayout();
        card.show(this.browserPanel, "noBrowser");
        Utilities.attachInitJob((Component)this, (AsyncGUIJob)this);
    }

    @Override
    public void addNotify() {
        Mnemonics.setLocalizedText((JLabel)this.templatesLabel, (String)((String)this.getClientProperty("LAB_SelectTemplateBorder")));
        Mnemonics.setLocalizedText((JLabel)this.browserLabel, (String)((String)this.getClientProperty("LAB_TemplateDescriptionBorder")));
        this.treeView.getAccessibleContext().setAccessibleDescription((String)this.getClientProperty("ACSD_TemplatesTree"));
        this.getAccessibleContext().setAccessibleDescription((String)this.getClientProperty("ACSD_TemplateWizard1"));
        super.addNotify();
    }

    public ExplorerManager getExplorerManager() {
        if (this.manager == null) {
            this.manager = new ExplorerManager();
            this.manager.addPropertyChangeListener((PropertyChangeListener)this);
        }
        return this.manager;
    }

    @Override
    public boolean requestDefaultFocus() {
        return this.treeView.requestDefaultFocus();
    }

    @Override
    public Dimension getPreferredSize() {
        return TemplateWizard.PREF_DIM;
    }

    private void updateRootNode(DataFolder root) {
        FileObject fo;
        if (root == null && (fo = FileUtil.getConfigFile((String)"Templates")) != null && fo.isFolder()) {
            root = DataFolder.findFolder(fo);
        }
        if (root == null || root.equals(this.templatesRoot)) {
            return;
        }
        this.templatesRoot = root;
        DataShadowFilterChildren ch = new DataShadowFilterChildren(root.getNodeDelegate());
        this.getExplorerManager().setRootContext((Node)new DataShadowFilterNode(root.getNodeDelegate(), (Children)ch, root.getNodeDelegate().getDisplayName()));
    }

    private void updateDescription(DataObject obj) {
        URL url = null;
        if (obj != null) {
            url = TemplateWizard.getDescription(obj);
        }
        CardLayout card = (CardLayout)this.browserPanel.getLayout();
        if (url != null && this.getExplorerManager().getSelectedNodes().length != 0) {
            if (this.browser != null) {
                this.browser.setURL(url);
                if (!this.browser.isVisible()) {
                    card.show(this.browserPanel, "browser");
                }
            }
        } else {
            card.show(this.browserPanel, "noBrowser");
        }
    }

    private void initComponents() {
        this.browserPanel = new JPanel();
        this.noBrowser = new JLabel();
        this.templatesLabel = new JLabel();
        this.browserLabel = new JLabel();
        this.setLayout(new GridBagLayout());
        this.setPreferredSize(new Dimension(0, 0));
        this.browserPanel.setLayout(new CardLayout());
        this.noBrowser.setBackground(UIManager.getDefaults().getColor("EditorPane.background"));
        this.noBrowser.setHorizontalAlignment(0);
        this.noBrowser.setMinimumSize(new Dimension(0, 25));
        this.noBrowser.setOpaque(true);
        this.browserPanel.add((Component)this.noBrowser, "noBrowser");
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weighty = 0.5;
        this.add((Component)this.browserPanel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 0, 5, 0);
        this.add((Component)this.templatesLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 0, 5, 0);
        this.add((Component)this.browserLabel, gridBagConstraints);
    }

    private void nameFocusGained(FocusEvent evt) {
    }

    private void templatesTreeValueChanged(TreeSelectionEvent evt) {
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName() == "selectedNodes" && this.listener != null) {
            this.listener.stateChanged(new ChangeEvent(this));
            this.updateDescription(this.template);
        }
    }

    private void packagesListValueChanged(ListSelectionEvent evt) {
    }

    @Override
    public boolean acceptDataObject(DataObject obj) {
        if (obj instanceof DataFolder) {
            Object o = obj.getPrimaryFile().getAttribute("simple");
            return o == null || Boolean.TRUE.equals(o);
        }
        return obj.isTemplate();
    }

    public void construct() {
        this.initData = new InitData();
        this.initData.browser = new HtmlBrowser(false, false);
        this.initData.browser.setName("browser");
        this.initData.noDescMsg = NbBundle.getBundle(TemplateWizard1.class).getString("MSG_NoDescription");
        this.initData.noDescBorder = new EtchedBorder();
        Component comp = this.initData.browser.getBrowserComponent();
        if (!(comp instanceof JEditorPane)) {
            return;
        }
        EditorKit kit = ((JEditorPane)comp).getEditorKitForContentType("text/html");
        if (!(kit instanceof HTMLEditorKit)) {
            return;
        }
        HTMLEditorKit htmlkit = (HTMLEditorKit)kit;
        if (htmlkit.getStyleSheet().getStyleSheets() != null) {
            return;
        }
        StyleSheet css = new StyleSheet();
        Font f = new JTextArea().getFont();
        css.addRule(new StringBuffer("body { font-size: ").append(f.getSize()).append("; font-family: ").append(f.getName()).append("; }").toString());
        css.addStyleSheet(htmlkit.getStyleSheet());
        htmlkit.setStyleSheet(css);
    }

    public void finished() {
        this.browser = this.initData.browser;
        this.browserLabel.setLabelFor((Component)this.browser);
        this.browser.getAccessibleContext().setAccessibleName(this.browserLabel.getText());
        this.browserPanel.add((Component)this.browser, "browser");
        this.updateDescription(this.template);
        this.noBrowser.setText(this.initData.noDescMsg);
        this.noBrowser.setBorder(this.initData.noDescBorder);
        this.initData = null;
    }

    void implReadSettings(Object settings) {
        TemplateWizard wizard = (TemplateWizard)((Object)settings);
        wizard.setTitle(NbBundle.getBundle(TemplateWizard.class).getString("CTL_TemplateTitle"));
        this.updateRootNode(wizard.getTemplatesFolder());
        this.template = wizard.getTemplate();
        if (this.template != null && !this.template.isValid()) {
            this.template = null;
        }
        DataFolder stop = wizard.getTemplatesFolder();
        final LinkedList<String> names = new LinkedList<String>();
        for (DataObject obj = this.template; obj != null && obj != stop; obj = obj.getFolder()) {
            String key = obj.getNodeDelegate().getName();
            names.addFirst(key);
        }
        RequestProcessor.getDefault().post(new Runnable(){
            private Node selection;

            @Override
            public void run() {
                if (this.selection == null) {
                    Node node = TemplateWizard1.this.getExplorerManager().getRootContext();
                    for (String name : names) {
                        if ((node = node.getChildren().findChild(name)) != null) continue;
                        node = TemplateWizard1.this.getExplorerManager().getRootContext();
                        break;
                    }
                    this.selection = node;
                    SwingUtilities.invokeLater(this);
                } else {
                    try {
                        TemplateWizard1.this.getExplorerManager().setSelectedNodes(new Node[]{this.selection});
                    }
                    catch (PropertyVetoException evt) {
                        // empty catch block
                    }
                }
            }
        }, 300, 1);
    }

    void implStoreSettings(Object settings) {
        if (this.template != null) {
            TemplateWizard wizard = (TemplateWizard)((Object)settings);
            if (wizard.getTemplate() != this.template) {
                Component c = wizard.targetChooser().getComponent();
                if (c instanceof JComponent) {
                    ((JComponent)c).putClientProperty("WizardPanel_contentData", new String[]{c.getName()});
                    ((JComponent)c).putClientProperty("WizardPanel_contentSelectedIndex", new Integer(0));
                }
            } else {
                Component c = wizard.targetChooser().getComponent();
                if (c instanceof JComponent && ((JComponent)c).getClientProperty("WizardPanel_contentData") == null) {
                    ((JComponent)c).putClientProperty("WizardPanel_contentData", new String[]{c.getName()});
                    ((JComponent)c).putClientProperty("WizardPanel_contentSelectedIndex", new Integer(0));
                }
            }
            wizard.setTemplateImpl(this.template, false);
        }
    }

    boolean implIsValid() {
        boolean enable = false;
        Node[] n = this.getExplorerManager().getSelectedNodes();
        if (n.length == 1) {
            this.template = (DataObject)n[0].getCookie(DataObject.class);
            enable = this.template != null && this.template.isTemplate();
        }
        return enable;
    }

    void addChangeListener(ChangeListener l) {
        if (this.listener != null) {
            throw new IllegalStateException();
        }
        this.listener = l;
    }

    void removeChangeListener(ChangeListener l) {
        this.listener = null;
    }

    private static final class TemplatesTreeView
    extends BeanTreeView {
        TemplatesTreeView() {
            this.tree.setEditable(false);
            this.tree.setLargeModel(false);
            this.setBorder((Border)UIManager.get("Nb.ScrollPane.border"));
        }

        protected NodeTreeModel createModel() {
            return new TemplatesModel();
        }
    }

    private static final class TemplatesModel
    extends NodeTreeModel {
        TemplatesModel() {
        }

        public int getChildCount(Object o) {
            Node n = Visualizer.findNode((Object)o);
            DataObject obj = (DataObject)n.getCookie(DataObject.class);
            return obj == null || obj.isTemplate() ? 0 : super.getChildCount(o);
        }

        public boolean isLeaf(Object o) {
            Node n = Visualizer.findNode((Object)o);
            DataObject obj = (DataObject)n.getCookie(DataObject.class);
            return obj == null || obj.isTemplate();
        }
    }

    private static class DataShadowFilterNode
    extends FilterNode {
        private String name;

        public DataShadowFilterNode(Node or, Children children, String name) {
            super(or, children);
            this.name = name;
            this.disableDelegation(4);
        }

        public String getDisplayName() {
            return this.name;
        }

        public boolean canRename() {
            return false;
        }
    }

    private class DataShadowFilterChildren
    extends FilterNode.Children {
        public DataShadowFilterChildren(Node or) {
            super(or);
        }

        protected Node[] createNodes(Node key) {
            Node n = key;
            String nodeName = n.getDisplayName();
            DataObject obj = null;
            DataShadow shadow = (DataShadow)n.getCookie(DataShadow.class);
            if (shadow != null) {
                DataNode dn = new DataNode(shadow, Children.LEAF);
                nodeName = dn.getDisplayName();
                obj = shadow.getOriginal();
                n = obj.getNodeDelegate();
            }
            if (obj == null) {
                obj = (DataObject)n.getCookie(DataObject.class);
            }
            if (obj != null) {
                if (obj.isTemplate()) {
                    return new Node[]{new DataShadowFilterNode(n, Children.LEAF, nodeName)};
                }
                if (TemplateWizard1.this.acceptDataObject(obj)) {
                    return new Node[]{new DataShadowFilterNode(n, (Children)new DataShadowFilterChildren(n), nodeName)};
                }
            }
            return new Node[0];
        }
    }

    private static final class InitData {
        HtmlBrowser browser;
        String noDescMsg;
        Border noDescBorder;

        private InitData() {
        }
    }

}

