/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.openide.loaders;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.Collection;
import java.util.Map;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.CreateFromTemplateHandler;
import org.openide.loaders.DataObject;
import org.openide.loaders.MultiDataObject;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class FileEntry
extends MultiDataObject.Entry {
    static final long serialVersionUID = 5972727204237511983L;

    public FileEntry(MultiDataObject obj, FileObject fo) {
        MultiDataObject multiDataObject = obj;
        multiDataObject.getClass();
        super(multiDataObject, fo);
    }

    @Override
    public FileObject copy(FileObject f, String suffix) throws IOException {
        FileObject fo = this.getFile();
        String newName = fo.getName() + suffix;
        return fo.copy(f, newName, fo.getExt());
    }

    @Override
    public FileObject copyRename(FileObject f, String name, String ext) throws IOException {
        FileObject fo = this.getFile();
        return fo.copy(f, name, ext);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public FileObject rename(String name) throws IOException {
        boolean locked = this.isLocked();
        FileLock lock = this.takeLock();
        try {
            this.getFile().rename(lock, name, this.getFile().getExt());
        }
        finally {
            if (!locked) {
                lock.releaseLock();
            }
        }
        return this.getFile();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public FileObject move(FileObject f, String suffix) throws IOException {
        boolean locked = this.isLocked();
        FileObject fo = this.getFile();
        FileLock lock = this.takeLock();
        try {
            String newName = fo.getName() + suffix;
            FileObject dest = fo.move(lock, f, newName, fo.getExt());
            if (dest == null) {
                throw new IOException((Object)fo + "move(" + (Object)lock + ", " + (Object)f + ", " + newName + ", " + fo.getExt() + " yields null!");
            }
            FileObject fileObject = dest;
            return fileObject;
        }
        finally {
            if (!locked) {
                lock.releaseLock();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void delete() throws IOException {
        boolean locked = this.isLocked();
        FileLock lock = this.takeLock();
        try {
            this.getFile().delete(lock);
        }
        finally {
            if (!locked) {
                lock.releaseLock();
            }
        }
    }

    @Override
    public FileObject createFromTemplate(FileObject f, String name) throws IOException {
        if (name == null) {
            name = FileUtil.findFreeFileName((FileObject)f, (String)this.getFile().getName(), (String)this.getFile().getExt());
        }
        FileObject fo = null;
        for (CreateFromTemplateHandler h : Lookup.getDefault().lookupAll(CreateFromTemplateHandler.class)) {
            if (!h.accept(this.getFile())) continue;
            fo = h.createFromTemplate(this.getFile(), f, name, DataObject.CreateAction.enhanceParameters(DataObject.CreateAction.findParameters(name), name, this.getFile().getExt()));
            assert (fo != null);
            break;
        }
        if (fo == null) {
            fo = this.getFile().copy(f, name, this.getFile().getExt());
        }
        DataObject.setTemplate(fo, false);
        return fo;
    }

    public static final class Folder
    extends MultiDataObject.Entry {
        public Folder(MultiDataObject obj, FileObject fo) {
            MultiDataObject multiDataObject = obj;
            multiDataObject.getClass();
            super(multiDataObject, fo);
        }

        @Override
        public FileObject copy(FileObject f, String suffix) throws IOException {
            String add = suffix + (this.getFile().getExt().length() > 0 ? new StringBuilder().append(".").append(this.getFile().getExt()).toString() : "");
            FileObject fo = FileUtil.createFolder((FileObject)f, (String)(this.getFile().getName() + add));
            FileUtil.copyAttributes((FileObject)this.getFile(), (FileObject)fo);
            return fo;
        }

        @Override
        public FileObject move(FileObject f, String suffix) throws IOException {
            return this.copy(f, suffix);
        }

        @Override
        public FileObject createFromTemplate(FileObject f, String name) throws IOException {
            if (name == null) {
                name = FileUtil.findFreeFileName((FileObject)f, (String)this.getFile().getName(), (String)this.getFile().getExt());
            }
            FileObject fo = FileUtil.createFolder((FileObject)f, (String)name);
            FileUtil.copyAttributes((FileObject)this.getFile(), (FileObject)fo);
            DataObject.setTemplate(fo, false);
            return fo;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public FileObject rename(String name) throws IOException {
            boolean locked = this.isLocked();
            FileLock lock = this.takeLock();
            try {
                this.getFile().rename(lock, name, null);
            }
            finally {
                if (!locked) {
                    lock.releaseLock();
                }
            }
            return this.getFile();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void delete() throws IOException {
            boolean locked = this.isLocked();
            FileLock lock = this.takeLock();
            try {
                this.getFile().delete(lock);
            }
            finally {
                if (!locked) {
                    lock.releaseLock();
                }
            }
        }
    }

    public static final class Numb
    extends MultiDataObject.Entry {
        static final long serialVersionUID = -6572157492885890612L;

        public Numb(MultiDataObject obj, FileObject fo) {
            MultiDataObject multiDataObject = obj;
            multiDataObject.getClass();
            super(multiDataObject, fo);
        }

        @Override
        public boolean isImportant() {
            return false;
        }

        @Override
        public FileObject copy(FileObject f, String suffix) {
            return null;
        }

        @Override
        public FileObject rename(String name) throws IOException {
            this.stdBehaving();
            return null;
        }

        @Override
        public FileObject move(FileObject f, String suffix) throws IOException {
            this.stdBehaving();
            return null;
        }

        @Override
        public void delete() throws IOException {
            this.stdBehaving();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void stdBehaving() throws IOException {
            if (this.getFile() == null) {
                return;
            }
            if (this.isLocked()) {
                throw new IOException(NbBundle.getBundle(FileEntry.class).getString("EXC_SharedAccess"));
            }
            FileLock lock = this.takeLock();
            try {
                this.getFile().delete(lock);
            }
            finally {
                if (lock != null) {
                    lock.releaseLock();
                }
            }
        }

        @Override
        public FileObject createFromTemplate(FileObject f, String name) {
            return null;
        }
    }

    public static abstract class Format
    extends FileEntry {
        static final long serialVersionUID = 8896750589709521197L;

        public Format(MultiDataObject obj, FileObject fo) {
            super(obj, fo);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public FileObject createFromTemplate(FileObject f, String name) throws IOException {
            FileObject fo;
            String ext = this.getFile().getExt();
            if (name == null) {
                name = FileUtil.findFreeFileName((FileObject)f, (String)this.getFile().getName(), (String)ext);
            }
            fo = null;
            for (CreateFromTemplateHandler h : Lookup.getDefault().lookupAll(CreateFromTemplateHandler.class)) {
                if (!h.accept(this.getFile())) continue;
                fo = h.createFromTemplate(this.getFile(), f, name, DataObject.CreateAction.enhanceParameters(DataObject.CreateAction.findParameters(name), name, this.getFile().getExt()));
                assert (fo != null);
                break;
            }
            if (fo != null) {
                DataObject.setTemplate(fo, false);
                return fo;
            }
            fo = f.createData(name, ext);
            java.text.Format frm = this.createFormat(f, name, ext);
            BufferedReader r = new BufferedReader(new InputStreamReader(this.getFile().getInputStream()));
            try {
                FileLock lock = fo.lock();
                try {
                    BufferedWriter w = new BufferedWriter(new OutputStreamWriter(fo.getOutputStream(lock)));
                    try {
                        String current;
                        while ((current = r.readLine()) != null) {
                            w.write(frm.format(current));
                            w.newLine();
                        }
                    }
                    finally {
                        w.close();
                    }
                }
                finally {
                    lock.releaseLock();
                }
            }
            finally {
                r.close();
            }
            FileUtil.copyAttributes((FileObject)this.getFile(), (FileObject)fo);
            DataObject.setTemplate(fo, false);
            return fo;
        }

        protected abstract java.text.Format createFormat(FileObject var1, String var2, String var3);
    }

}

