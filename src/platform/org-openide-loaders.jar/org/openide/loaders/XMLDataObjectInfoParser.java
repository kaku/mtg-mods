/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.xml.XMLUtil
 */
package org.openide.loaders;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.Properties;
import java.util.jar.Attributes;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.loaders.Environment;
import org.openide.loaders.XMLDataObject;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.xml.XMLUtil;
import org.xml.sax.ContentHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.DefaultHandler;

final class XMLDataObjectInfoParser
extends DefaultHandler
implements FileChangeListener,
LexicalHandler,
LookupListener {
    private static final StopSaxException STOP = new StopSaxException();
    private static XMLReader sharedParserImpl = null;
    private static final String NULL = "";
    private Reference<XMLDataObject> xml;
    private String parsedId;
    private Lookup lookup;
    private Lookup.Result result;
    private ThreadLocal<Class<?>> QUERY = new ThreadLocal();

    XMLDataObjectInfoParser(XMLDataObject xml) {
        this.xml = new WeakReference<XMLDataObject>(xml);
    }

    public String getPublicId() {
        String nu;
        String id = this.waitFinished();
        return id == (nu = "") ? null : id;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Object lookupCookie(Class<?> clazz) {
        if (this.QUERY.get() == clazz) {
            if (XMLDataObject.ERR.isLoggable(Level.FINE)) {
                XMLDataObject.ERR.fine("Cyclic deps on queried class: " + clazz + " for " + this.getXml());
            }
            return null;
        }
        Class previous = this.QUERY.get();
        try {
            Lookup.Result r;
            Lookup l;
            block19 : {
                this.QUERY.set(clazz);
                if (XMLDataObject.ERR.isLoggable(Level.FINE)) {
                    XMLDataObject.ERR.fine("Will do query for class: " + clazz + " for " + this.getXml());
                }
                do {
                    String id = this.waitFinished();
                    XMLDataObjectInfoParser xMLDataObjectInfoParser = this;
                    synchronized (xMLDataObjectInfoParser) {
                        l = this.lookup != null ? this.lookup : null;
                    }
                    if (XMLDataObject.ERR.isLoggable(Level.FINE)) {
                        XMLDataObject.ERR.fine("Lookup is " + (Object)l + " for id: " + id);
                    }
                    if (l == null) {
                        l = this.updateLookup(this.getXml(), null, id);
                        if (XMLDataObject.ERR.isLoggable(Level.FINE)) {
                            XMLDataObject.ERR.fine("Updating lookup: " + (Object)l);
                        }
                    }
                    if (XMLDataObject.ERR.isLoggable(Level.FINE)) {
                        XMLDataObject.ERR.fine("Wait lookup is over: " + (Object)l + this.getXml());
                    }
                    if (l != null) break block19;
                } while (this.parsedId != null);
                l = Lookup.EMPTY;
            }
            if ((r = this.result) != null) {
                if (XMLDataObject.ERR.isLoggable(Level.FINE)) {
                    XMLDataObject.ERR.fine("Querying the result: " + (Object)r);
                }
            } else if (XMLDataObject.ERR.isLoggable(Level.FINE)) {
                XMLDataObject.ERR.fine("No result for lookup: " + (Object)this.lookup);
            }
            Object ret = l.lookup(clazz);
            if (XMLDataObject.ERR.isLoggable(Level.FINE)) {
                XMLDataObject.ERR.fine("Returning value: " + ret + " for " + this.getXml());
            }
            Object object = ret;
            return object;
        }
        finally {
            this.QUERY.set(previous);
        }
    }

    public String waitFinished() {
        return this.waitFinished(null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive exception aggregation
     */
    private String waitFinished(String ignorePreviousId) {
        String previousID;
        if (sharedParserImpl == null) {
            XMLDataObject.ERR.fine("No sharedParserImpl, exiting");
            return "";
        }
        XMLReader parser = sharedParserImpl;
        XMLDataObject realXML = this.getXml();
        if (realXML == null) {
            return "";
        }
        FileObject myFileObject = realXML.getPrimaryFile();
        String newID = null;
        if (XMLDataObject.ERR.isLoggable(Level.FINE)) {
            XMLDataObject.ERR.fine("Going to read parsedId for " + realXML);
        }
        XMLDataObjectInfoParser xMLDataObjectInfoParser = this;
        synchronized (xMLDataObjectInfoParser) {
            previousID = this.parsedId;
        }
        if (previousID != null) {
            if (XMLDataObject.ERR.isLoggable(Level.FINE)) {
                XMLDataObject.ERR.fine("Has already been parsed: " + this.parsedId + " for " + realXML);
            }
            return previousID;
        }
        URL url = null;
        InputStream in = null;
        try {
            url = myFileObject.getURL();
        }
        catch (IOException ex2) {
            this.warning(ex2, "I/O exception while retrieving xml FileObject URL.");
            return "";
        }
        XMLDataObjectInfoParser ex2 = this;
        synchronized (ex2) {
            try {
                if (!myFileObject.isValid()) {
                    if (XMLDataObject.ERR.isLoggable(Level.FINE)) {
                        XMLDataObject.ERR.fine("Invalid file object: " + (Object)myFileObject);
                    }
                    String string = "";
                    return string;
                }
                this.parsedId = "";
                if (XMLDataObject.ERR.isLoggable(Level.FINE)) {
                    XMLDataObject.ERR.fine("parsedId set to NULL for " + realXML);
                }
                try {
                    in = myFileObject.getInputStream();
                }
                catch (IOException ex) {
                    this.warning(ex, "I/O exception while opening " + (Object)myFileObject);
                    String ex = "";
                    try {
                        if (in != null) {
                            in.close();
                        }
                    }
                    catch (IOException ex) {
                        XMLDataObject.ERR.log(Level.WARNING, null, ex);
                    }
                    return ex;
                }
                try {
                    XMLReader ex = sharedParserImpl;
                    synchronized (ex) {
                        this.configureParser(parser, false, this);
                        parser.setContentHandler(this);
                        parser.setErrorHandler(this);
                        InputSource input = new InputSource(url.toExternalForm());
                        input.setByteStream(in);
                        parser.parse(input);
                    }
                    if (XMLDataObject.ERR.isLoggable(Level.FINE)) {
                        XMLDataObject.ERR.fine("Parse finished for " + realXML);
                    }
                }
                catch (StopSaxException stopped) {
                    newID = this.parsedId;
                    XMLDataObject.ERR.fine("Parsing successfully stopped: " + this.parsedId + " for " + realXML);
                }
                catch (SAXException checkStop) {
                    if (STOP.getMessage().equals(checkStop.getMessage())) {
                        newID = this.parsedId;
                        XMLDataObject.ERR.fine("Parsing stopped with STOP message: " + this.parsedId + " for " + realXML);
                    } else {
                        String msg = "Thread:" + Thread.currentThread().getName();
                        XMLDataObject.ERR.warning("DocListener should not throw SAXException but STOP one.\n" + msg);
                        XMLDataObject.ERR.log(Level.WARNING, null, checkStop);
                        Exception ex = checkStop.getException();
                        if (ex != null) {
                            XMLDataObject.ERR.log(Level.WARNING, null, ex);
                        }
                    }
                }
                catch (FileNotFoundException ex) {
                    XMLDataObject.ERR.log(Level.INFO, null, ex);
                }
                catch (IOException ex) {
                    XMLDataObject.ERR.log(Level.INFO, null, ex);
                }
                finally {
                    if (Boolean.getBoolean("netbeans.profile.memory")) {
                        parser.setContentHandler(XMLDataObject.NullHandler.INSTANCE);
                        parser.setErrorHandler(XMLDataObject.NullHandler.INSTANCE);
                        try {
                            parser.setProperty("http://xml.org/sax/properties/lexical-handler", XMLDataObject.NullHandler.INSTANCE);
                        }
                        catch (SAXException ignoreIt) {}
                        try {
                            parser.parse((InputSource)null);
                        }
                        catch (Exception ignoreIt) {}
                    }
                    parser = null;
                }
            }
            finally {
                try {
                    if (in != null) {
                        in.close();
                    }
                }
                catch (IOException ex) {
                    XMLDataObject.ERR.log(Level.WARNING, null, ex);
                }
            }
        }
        if (ignorePreviousId != null && ignorePreviousId.equals(newID)) {
            if (XMLDataObject.ERR.isLoggable(Level.FINE)) {
                XMLDataObject.ERR.fine("No update to ID: " + ignorePreviousId + " for " + realXML);
            }
            return newID;
        }
        if (XMLDataObject.ERR.isLoggable(Level.FINE)) {
            XMLDataObject.ERR.fine("New id: " + newID + " for " + realXML);
        }
        if (newID != null) {
            this.updateLookup(realXML, previousID, newID);
        }
        return newID;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Lookup updateLookup(XMLDataObject realXML, String previousID, String id) {
        Lookup newLookup;
        if (realXML == null) {
            return this.lookup;
        }
        XMLDataObjectInfoParser xMLDataObjectInfoParser = this;
        synchronized (xMLDataObjectInfoParser) {
            if (previousID != null && previousID.equals(id) && this.lookup != null) {
                XMLDataObject.ERR.fine("No need to update lookup: " + id + " for " + realXML);
                return this.lookup;
            }
        }
        XMLDataObject.Info info = XMLDataObject.getRegisteredInfo(id);
        if (info != null) {
            newLookup = XMLDataObject.createInfoLookup(realXML, info);
            if (XMLDataObject.ERR.isLoggable(Level.FINE)) {
                XMLDataObject.ERR.fine("Lookup from info: " + (Object)newLookup + " for " + realXML);
            }
        } else {
            newLookup = Environment.findForOne(realXML);
            if (XMLDataObject.ERR.isLoggable(Level.FINE)) {
                XMLDataObject.ERR.fine("Lookup from env: " + (Object)newLookup + " for " + realXML);
            }
            if (newLookup == null) {
                newLookup = Lookup.EMPTY;
            }
        }
        XMLDataObjectInfoParser xMLDataObjectInfoParser2 = this;
        synchronized (xMLDataObjectInfoParser2) {
            Lookup.Result prevRes = this.result;
            this.lookup = newLookup;
            if (XMLDataObject.ERR.isLoggable(Level.FINE)) {
                XMLDataObject.ERR.fine("Shared lookup updated: " + (Object)this.lookup + " for " + realXML);
            }
            this.result = this.lookup.lookupResult(Node.Cookie.class);
            this.result.addLookupListener((LookupListener)this);
            if (prevRes != null) {
                prevRes.removeLookupListener((LookupListener)this);
                if (XMLDataObject.ERR.isLoggable(Level.FINE)) {
                    XMLDataObject.ERR.fine("Firing property change for " + realXML);
                }
                realXML.firePropertyChange("cookie", null, null);
                if (XMLDataObject.ERR.isLoggable(Level.FINE)) {
                    XMLDataObject.ERR.fine("Firing done for " + realXML);
                }
            }
            return newLookup;
        }
    }

    private void configureParser(XMLReader parser, boolean validation, LexicalHandler lex) {
        try {
            parser.setFeature("http://xml.org/sax/features/validation", validation);
        }
        catch (SAXException sex) {
            XMLDataObject.ERR.fine("Warning: XML parser does not support validation feature.");
        }
        try {
            parser.setProperty("http://xml.org/sax/properties/lexical-handler", lex);
        }
        catch (SAXException sex) {
            XMLDataObject.ERR.fine("Warning: XML parser does not support lexical-handler feature.");
        }
    }

    public void warning(Throwable ex) {
        this.warning(ex, null);
    }

    public void warning(Throwable ex, String annotation) {
        XMLDataObject.ERR.log(Level.INFO, annotation, ex);
    }

    @Override
    public void startDTD(String root, String pID, String sID) throws SAXException {
        this.parsedId = pID == null ? "" : pID;
        XMLDataObject.ERR.fine("Parsed to " + this.parsedId);
        this.stop();
    }

    @Override
    public void endDTD() throws SAXException {
        this.stop();
    }

    @Override
    public void startEntity(String name) throws SAXException {
        XMLDataObject.ERR.log(Level.FINEST, "startEntity {0}", name);
    }

    @Override
    public void endEntity(String name) throws SAXException {
        XMLDataObject.ERR.log(Level.FINEST, "endEntity {0}", name);
    }

    @Override
    public void startCDATA() throws SAXException {
        XMLDataObject.ERR.log(Level.FINEST, "startCDATA");
    }

    @Override
    public void endCDATA() throws SAXException {
        XMLDataObject.ERR.log(Level.FINEST, "endCDATA");
    }

    @Override
    public void comment(char[] ch, int start, int length) throws SAXException {
        XMLDataObject.ERR.log(Level.FINEST, "comment len: {0}", length);
    }

    @Override
    public void error(SAXParseException p1) throws SAXException {
        this.stop();
    }

    @Override
    public void fatalError(SAXParseException p1) throws SAXException {
        this.stop();
    }

    @Override
    public void endDocument() throws SAXException {
        this.stop();
    }

    public void startElement(String uri, String lName, String qName, Attributes atts) throws SAXException {
        this.stop();
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        XMLDataObject.ERR.log(Level.FINEST, "characters len: {0}", length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        XMLDataObject.ERR.log(Level.FINEST, "endElement: {0}", qName);
    }

    @Override
    public void endPrefixMapping(String prefix) throws SAXException {
        XMLDataObject.ERR.log(Level.FINEST, "endPrefix: {0}", prefix);
    }

    @Override
    public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
        XMLDataObject.ERR.log(Level.FINEST, "ignorableWhitespace: {0}", length);
    }

    @Override
    public void notationDecl(String name, String publicId, String systemId) throws SAXException {
        XMLDataObject.ERR.log(Level.FINEST, "notationDecl: {0}", name);
    }

    @Override
    public void processingInstruction(String target, String data) throws SAXException {
        XMLDataObject.ERR.log(Level.FINEST, "processingInstruction: {0}", target);
    }

    @Override
    public InputSource resolveEntity(String publicId, String systemId) throws IOException, SAXException {
        XMLDataObject.ERR.log(Level.FINEST, "resolveEntity: {0}", publicId);
        return super.resolveEntity(publicId, systemId);
    }

    @Override
    public void skippedEntity(String name) throws SAXException {
        XMLDataObject.ERR.log(Level.FINEST, "skippedEntity: {0}", name);
    }

    @Override
    public void startDocument() throws SAXException {
        XMLDataObject.ERR.log(Level.FINEST, "startDocument");
    }

    @Override
    public void startElement(String uri, String localName, String qName, org.xml.sax.Attributes attributes) throws SAXException {
        XMLDataObject.ERR.log(Level.FINEST, "startElement: {0}", qName);
        this.stop();
    }

    @Override
    public void startPrefixMapping(String prefix, String uri) throws SAXException {
        XMLDataObject.ERR.log(Level.FINEST, "startPrefixMapping: {0}", prefix);
    }

    @Override
    public void unparsedEntityDecl(String name, String publicId, String systemId, String notationName) throws SAXException {
        XMLDataObject.ERR.log(Level.FINEST, "unparsedEntityDecl: {0}", name);
    }

    private void stop() throws SAXException {
        XMLDataObject.ERR.log(Level.FINEST, "stop");
        throw STOP;
    }

    public void fileFolderCreated(FileEvent fe) {
    }

    public void fileDataCreated(FileEvent fe) {
    }

    private void fileCreated(FileObject fo) {
    }

    public void fileChanged(FileEvent fe) {
        XMLDataObject realXML = this.getXml();
        if (realXML == null) {
            return;
        }
        if (realXML.getPrimaryFile().equals((Object)fe.getFile())) {
            realXML.clearDocument();
            String prevId = this.parsedId;
            this.parsedId = null;
            XMLDataObject.ERR.fine("cleared parsedId");
            this.waitFinished(prevId);
        }
    }

    public void fileDeleted(FileEvent fe) {
    }

    public void fileRenamed(FileRenameEvent fe) {
    }

    public void fileAttributeChanged(FileAttributeEvent fe) {
    }

    public void resultChanged(LookupEvent lookupEvent) {
        XMLDataObject realXML = this.getXml();
        if (realXML == null) {
            return;
        }
        realXML.firePropertyChange("cookie", null, null);
        Node n = realXML.getNodeDelegateOrNull();
        if (n instanceof XMLDataObject.XMLNode) {
            ((XMLDataObject.XMLNode)n).update();
        }
    }

    private XMLDataObject getXml() {
        return this.xml.get();
    }

    static {
        try {
            sharedParserImpl = XMLUtil.createXMLReader();
            sharedParserImpl.setEntityResolver(new EmptyEntityResolver());
        }
        catch (SAXException ex) {
            Exceptions.attachLocalizedMessage((Throwable)ex, (String)"System does not contain JAXP 1.1 compliant parser!");
            Logger.getLogger(XMLDataObject.class.getName()).log(Level.WARNING, null, ex);
        }
        try {
            Properties props = System.getProperties();
            String SAX2_KEY = "org.xml.sax.driver";
            if (props.getProperty("org.xml.sax.driver") == null) {
                props.put("org.xml.sax.driver", sharedParserImpl.getClass().getName());
            }
        }
        catch (RuntimeException ex) {
            // empty catch block
        }
    }

    private static class EmptyEntityResolver
    implements EntityResolver {
        EmptyEntityResolver() {
        }

        @Override
        public InputSource resolveEntity(String publicId, String systemID) {
            InputSource ret = new InputSource(new StringReader(""));
            ret.setSystemId("StringReader");
            return ret;
        }
    }

    private static class StopSaxException
    extends SAXException {
        public StopSaxException() {
            super("STOP");
        }
    }

}

