/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.CookieSet
 *  org.openide.nodes.CookieSet$Factory
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.CloneableEditorSupport$Pane
 *  org.openide.util.Lookup
 */
package org.openide.loaders;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.openide.loaders.SimpleES;
import org.openide.loaders.MultiDataObject;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.DataEditorSupport;
import org.openide.util.Lookup;

final class MultiDOEditor
implements Callable<CloneableEditorSupport.Pane>,
CookieSet.Factory {
    private CloneableEditorSupport support;
    private static final Method factory;
    private final MultiDataObject outer;
    private final String mimeType;
    private final boolean useMultiview;

    MultiDOEditor(MultiDataObject outer, String mimeType, boolean useMultiview) {
        this.outer = outer;
        this.mimeType = mimeType;
        this.useMultiview = useMultiview;
    }

    static boolean isMultiViewAvailable() {
        return factory != null;
    }

    static CloneableEditorSupport.Pane createMultiViewPane(String mimeType, MultiDataObject outer) {
        try {
            return (CloneableEditorSupport.Pane)factory.invoke(null, mimeType, outer);
        }
        catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public CloneableEditorSupport.Pane call() throws Exception {
        if (factory != null) {
            return MultiDOEditor.createMultiViewPane(this.mimeType, this.outer);
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public <T extends Node.Cookie> T createCookie(Class<T> klass) {
        if (klass.isAssignableFrom(SimpleES.class)) {
            MultiDOEditor multiDOEditor = this;
            synchronized (multiDOEditor) {
                if (this.support == null) {
                    this.support = DataEditorSupport.create(this.outer, this.outer.getPrimaryEntry(), this.outer.getCookieSet(), this.useMultiview ? this : null);
                }
            }
            return (T)((Node.Cookie)klass.cast((Object)this.support));
        }
        return null;
    }

    public static void registerEditor(MultiDataObject multi, String mime, boolean useMultiview) {
        MultiDOEditor ed = new MultiDOEditor(multi, mime, useMultiview);
        multi.getCookieSet().add(SimpleES.class, (CookieSet.Factory)ed);
    }

    static {
        ClassLoader l = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        if (l == null) {
            l = Thread.currentThread().getContextClassLoader();
        }
        if (l == null) {
            l = MultiDOEditor.class.getClassLoader();
        }
        Method m = null;
        try {
            Class multiviews = Class.forName("org.netbeans.core.api.multiview.MultiViews", true, l);
            m = multiviews.getMethod("createCloneableMultiView", String.class, Serializable.class);
        }
        catch (NoSuchMethodException ex) {
            MultiDataObject.LOG.log(Level.WARNING, "Cannot find a method", ex);
        }
        catch (ClassNotFoundException ex) {
            MultiDataObject.LOG.info("Not using multiviews for MultiDataObject.registerEditor()");
            MultiDataObject.LOG.log(Level.FINE, "Cannot find a class", ex);
        }
        factory = m;
    }
}

