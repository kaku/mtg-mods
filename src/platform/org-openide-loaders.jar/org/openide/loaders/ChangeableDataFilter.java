/*
 * Decompiled with CFR 0_118.
 */
package org.openide.loaders;

import javax.swing.event.ChangeListener;
import org.openide.loaders.DataFilter;

public interface ChangeableDataFilter
extends DataFilter {
    public void addChangeListener(ChangeListener var1);

    public void removeChangeListener(ChangeListener var1);
}

