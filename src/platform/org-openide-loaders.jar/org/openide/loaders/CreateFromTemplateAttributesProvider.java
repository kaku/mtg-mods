/*
 * Decompiled with CFR 0_118.
 */
package org.openide.loaders;

import java.util.Map;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;

public interface CreateFromTemplateAttributesProvider {
    public Map<String, ?> attributesFor(DataObject var1, DataFolder var2, String var3);
}

