/*
 * Decompiled with CFR 0_118.
 */
package org.openide.loaders;

import java.beans.PropertyEditorSupport;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;

class SortModeEditor
extends PropertyEditorSupport {
    private static final DataFolder.SortMode[] values = new DataFolder.SortMode[]{DataFolder.SortMode.NONE, DataFolder.SortMode.NAMES, DataFolder.SortMode.CLASS, DataFolder.SortMode.FOLDER_NAMES, DataFolder.SortMode.LAST_MODIFIED, DataFolder.SortMode.SIZE, DataFolder.SortMode.EXTENSIONS};
    private static final String[] modes = new String[]{DataObject.getString("VALUE_sort_none"), DataObject.getString("VALUE_sort_names"), DataObject.getString("VALUE_sort_class"), DataObject.getString("VALUE_sort_folder_names"), DataObject.getString("VALUE_sort_last_modified"), DataObject.getString("VALUE_sort_size"), DataObject.getString("VALUE_sort_extensions")};

    SortModeEditor() {
    }

    @Override
    public String[] getTags() {
        return modes;
    }

    @Override
    public String getAsText() {
        Object obj = this.getValue();
        for (int i = 0; i < values.length; ++i) {
            if (obj != values[i]) continue;
            return modes[i];
        }
        return null;
    }

    @Override
    public void setAsText(String str) {
        for (int i = 0; i < modes.length; ++i) {
            if (!str.equals(modes[i])) continue;
            this.setValue(values[i]);
            return;
        }
    }
}

