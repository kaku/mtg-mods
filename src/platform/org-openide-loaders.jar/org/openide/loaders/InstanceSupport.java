/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.InstanceCookie$Of
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.SharedClassObject
 *  org.openide.util.io.NbObjectInputStream
 */
package org.openide.loaders;

import java.applet.Applet;
import java.awt.Component;
import java.awt.MenuComponent;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.MultiDataObject;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.SharedClassObject;
import org.openide.util.io.NbObjectInputStream;

public class InstanceSupport
implements InstanceCookie.Of {
    private MultiDataObject.Entry entry;
    private Throwable clazzException;
    private Class clazz;
    private Boolean applet;
    private Boolean bean;
    private static Class writeRepl;

    public InstanceSupport(MultiDataObject.Entry entry) {
        this.entry = entry;
    }

    MultiDataObject.Entry entry() {
        return this.entry;
    }

    public String instanceName() {
        String p = this.instanceOrigin().getPath();
        int x = p.lastIndexOf(46);
        if (x != -1 && x > p.lastIndexOf(47)) {
            p = p.substring(0, x);
        }
        return p.replace('/', '.');
    }

    public Class<?> instanceClass() throws IOException, ClassNotFoundException {
        return this.instanceClass(null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    final Class<?> instanceClass(ClassLoader cl) throws IOException, ClassNotFoundException {
        if (this.clazzException != null) {
            if (this.clazzException instanceof IOException) {
                throw (IOException)this.clazzException;
            }
            if (this.clazzException instanceof ClassNotFoundException) {
                throw (ClassNotFoundException)this.clazzException;
            }
            throw (ThreadDeath)this.clazzException;
        }
        if (this.clazz != null) {
            return this.clazz;
        }
        try {
            if (this.isSerialized()) {
                InputStream is = this.instanceOrigin().getInputStream();
                try {
                    Class class_ = this.clazz = this.readClass(is);
                    return class_;
                }
                finally {
                    is.close();
                }
            }
            this.clazz = this.findClass(this.instanceName(), cl);
            if (this.clazz == null) {
                throw new ClassNotFoundException(this.instanceName());
            }
            return this.clazz;
        }
        catch (IOException ex) {
            Exceptions.attachMessage((Throwable)ex, (String)("From file: " + (Object)this.entry.getFile()));
            this.clazzException = ex;
            throw ex;
        }
        catch (ClassNotFoundException ex) {
            Exceptions.attachMessage((Throwable)ex, (String)("From file: " + (Object)this.entry.getFile()));
            this.clazzException = ex;
            throw ex;
        }
        catch (RuntimeException re) {
            this.clazzException = new ClassNotFoundException("From file: " + (Object)this.entry.getFile() + " due to: " + re.toString(), re);
            throw (ClassNotFoundException)this.clazzException;
        }
        catch (LinkageError le) {
            this.clazzException = new ClassNotFoundException("From file: " + (Object)this.entry.getFile() + " due to: " + le.toString(), le);
            throw (ClassNotFoundException)this.clazzException;
        }
    }

    public boolean instanceOf(Class<?> type) {
        try {
            return type.isAssignableFrom(this.instanceClass());
        }
        catch (IOException ex) {
            return false;
        }
        catch (ClassNotFoundException ex) {
            return false;
        }
    }

    public FileObject instanceOrigin() {
        return this.entry.getFile();
    }

    public Object instanceCreate() throws IOException, ClassNotFoundException {
        try {
            if (this.isSerialized()) {
                BufferedInputStream bis = new BufferedInputStream(this.instanceOrigin().getInputStream(), 1024);
                NbObjectInputStream nbis = new NbObjectInputStream((InputStream)bis);
                Object o = nbis.readObject();
                nbis.close();
                return o;
            }
            Class c = this.instanceClass();
            if (SharedClassObject.class.isAssignableFrom(c)) {
                return SharedClassObject.findObject(c.asSubclass(SharedClassObject.class), (boolean)true);
            }
            Constructor init = c.getDeclaredConstructor(new Class[0]);
            init.setAccessible(true);
            return init.newInstance(null);
        }
        catch (IOException ex) {
            Exceptions.attachLocalizedMessage((Throwable)ex, (String)this.instanceName());
            throw ex;
        }
        catch (ClassNotFoundException ex) {
            throw ex;
        }
        catch (Exception e) {
            throw new ClassNotFoundException("Cannot instantiate " + this.instanceName() + " for " + (Object)this.entry.getFile(), e);
        }
        catch (LinkageError e) {
            throw new ClassNotFoundException("Cannot instantiate " + this.instanceName() + " for " + (Object)this.entry.getFile(), e);
        }
    }

    @Deprecated
    public boolean isApplet() {
        if (this.applet != null) {
            return this.applet;
        }
        boolean b = this.instanceOf(Applet.class);
        this.applet = b ? Boolean.TRUE : Boolean.FALSE;
        return b;
    }

    @Deprecated
    public boolean isExecutable() {
        try {
            Method main = this.instanceClass().getDeclaredMethod("main", String[].class);
            int m = main.getModifiers();
            return Modifier.isPublic(m) && Modifier.isStatic(m) && Void.TYPE.equals(main.getReturnType());
        }
        catch (Exception ex) {
            return false;
        }
        catch (LinkageError re) {
            return false;
        }
    }

    @Deprecated
    public boolean isJavaBean() {
        if (this.bean != null) {
            return this.bean;
        }
        if (this.isSerialized()) {
            this.bean = Boolean.TRUE;
            return true;
        }
        try {
            Constructor c;
            Class clazz = this.instanceClass();
            int modif = clazz.getModifiers();
            if (!Modifier.isPublic(modif) || Modifier.isAbstract(modif)) {
                this.bean = Boolean.FALSE;
                return false;
            }
            try {
                c = clazz.getConstructor(new Class[0]);
            }
            catch (NoSuchMethodException e) {
                this.bean = Boolean.FALSE;
                return false;
            }
            if (c == null || !Modifier.isPublic(c.getModifiers())) {
                this.bean = Boolean.FALSE;
                return false;
            }
            for (Class outer = clazz.getDeclaringClass(); outer != null; outer = outer.getDeclaringClass()) {
                if (!Modifier.isStatic(modif)) {
                    return false;
                }
                modif = outer.getModifiers();
                if (Modifier.isPublic(modif)) continue;
                return false;
            }
        }
        catch (Exception ex) {
            this.bean = Boolean.FALSE;
            return true;
        }
        catch (LinkageError e) {
            this.bean = Boolean.FALSE;
            return false;
        }
        this.bean = Boolean.TRUE;
        return true;
    }

    @Deprecated
    public boolean isInterface() {
        try {
            return this.instanceClass().isInterface();
        }
        catch (IOException ex) {
            return false;
        }
        catch (ClassNotFoundException cnfe) {
            return false;
        }
    }

    public String toString() {
        return this.instanceName();
    }

    @Deprecated
    public static HelpCtx findHelp(InstanceCookie instance) {
        try {
            Object o;
            HelpCtx h;
            Class clazz = instance.instanceClass();
            if (Component.class.isAssignableFrom(clazz) || MenuComponent.class.isAssignableFrom(clazz)) {
                String name = clazz.getName();
                String[] pkgs = new String[]{"java.awt.", "javax.swing.", "javax.swing.border."};
                for (int i = 0; i < pkgs.length; ++i) {
                    if (!name.startsWith(pkgs[i]) || name.substring(pkgs[i].length()).indexOf(46) != -1) continue;
                    return new HelpCtx(name);
                }
            }
            if ((o = instance.instanceCreate()) != null && o != instance && (h = HelpCtx.findHelp((Object)o)) != HelpCtx.DEFAULT_HELP) {
                return h;
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        return null;
    }

    private boolean isSerialized() {
        return this.instanceOrigin().getExt().equals("ser");
    }

    private Class readClass(InputStream is) throws IOException, ClassNotFoundException {
        class OIS
        extends ObjectInputStream {
            public OIS(InputStream iss) throws IOException {
                super(iss);
            }

            public Class resolveClass(ObjectStreamClass osc) throws IOException, ClassNotFoundException {
                Class c = InstanceSupport.this.findClass(osc.getName(), null);
                if (c == writeRepl) {
                    return c;
                }
                throw new ClassEx(c);
            }
        }
        OIS ois = new OIS(new BufferedInputStream(is));
        try {
            ois.readObject();
            throw new ClassNotFoundException();
        }
        catch (ClassEx ex) {
            return ex.clazz;
        }
    }

    private Class findClass(String name, ClassLoader customLoader) throws ClassNotFoundException {
        ClassLoader loader = null;
        try {
            Class c;
            try {
                if (customLoader != null) {
                    c = customLoader.loadClass(name);
                } else {
                    loader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
                    if (loader == null) {
                        loader = this.getClass().getClassLoader();
                    }
                    c = loader.loadClass(name);
                }
            }
            catch (ClassNotFoundException ex) {
                c = this.createClassLoader().loadClass(name);
            }
            return c;
        }
        catch (ClassNotFoundException ex) {
            Exceptions.attachMessage((Throwable)ex, (String)("ClassLoader: " + loader));
            throw ex;
        }
        catch (RuntimeException ex) {
            throw ex;
        }
        catch (LinkageError le) {
            throw new ClassNotFoundException(le.toString(), le);
        }
    }

    protected ClassLoader createClassLoader() {
        ClassLoader l = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        return l;
    }

    static {
        try {
            writeRepl = Class.forName("org.openide.util.SharedClassObject$WriteReplace");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static class ClassEx
    extends IOException {
        public Class clazz;
        static final long serialVersionUID = 4810039297880922426L;

        public ClassEx(Class c) {
            this.clazz = c;
        }
    }

    public static class Instance
    implements InstanceCookie.Of {
        private Object obj;

        public Instance(Object obj) {
            this.obj = obj;
        }

        public String instanceName() {
            return this.obj.getClass().getName();
        }

        public Class<?> instanceClass() {
            return this.obj.getClass();
        }

        public Object instanceCreate() {
            return this.obj;
        }

        public boolean instanceOf(Class<?> type) {
            return type.isAssignableFrom(this.instanceClass());
        }
    }

}

