/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.ConnectionCookie
 *  org.openide.cookies.ConnectionCookie$Event
 *  org.openide.cookies.ConnectionCookie$Listener
 *  org.openide.cookies.ConnectionCookie$Type
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.nodes.Node$Handle
 */
package org.openide.loaders;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.cookies.ConnectionCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.MultiDataObject;
import org.openide.nodes.Node;

@Deprecated
public class ConnectionSupport
implements ConnectionCookie {
    private static final String EA_LISTENERS = "EA-OpenIDE-Connection";
    private MultiDataObject.Entry entry;
    private ConnectionCookie.Type[] types;
    private Set<ConnectionCookie.Type> typesSet;
    private LinkedList<Pair> listeners;

    public ConnectionSupport(MultiDataObject.Entry entry, ConnectionCookie.Type[] types) {
        this.entry = entry;
        this.types = types;
        this.listeners = new LinkedList();
    }

    public synchronized void register(ConnectionCookie.Type type, Node listener) throws IOException {
        this.testSupported(type);
        boolean persistent = type.isPersistent();
        LinkedList<Pair> list = persistent ? (LinkedList<Pair>)this.entry.getFile().getAttribute("EA-OpenIDE-Connection") : this.listeners;
        if (list == null) {
            list = new LinkedList<Pair>();
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            Node n;
            Pair pair = (Pair)it.next();
            if (!type.equals((Object)pair.getType())) continue;
            try {
                n = pair.getNode();
            }
            catch (IOException e) {
                Logger.getLogger(ConnectionSupport.class.getName()).log(Level.WARNING, null, e);
                it.remove();
                continue;
            }
            if (!n.equals((Object)listener)) continue;
            it.remove();
        }
        list.add(persistent ? new Pair(type, listener.getHandle()) : new Pair(type, listener));
        if (persistent) {
            this.entry.getFile().setAttribute("EA-OpenIDE-Connection", list);
        }
    }

    public synchronized void unregister(ConnectionCookie.Type type, Node listener) throws IOException {
        this.testSupported(type);
        boolean persistent = type.isPersistent();
        LinkedList list = persistent ? (LinkedList)this.entry.getFile().getAttribute("EA-OpenIDE-Connection") : this.listeners;
        if (list == null) {
            return;
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            Node n;
            Pair pair = (Pair)it.next();
            if (!type.equals((Object)pair.getType())) continue;
            try {
                n = pair.getNode();
            }
            catch (IOException e) {
                it.remove();
                continue;
            }
            if (!n.equals((Object)listener)) continue;
            it.remove();
        }
        if (persistent) {
            this.entry.getFile().setAttribute("EA-OpenIDE-Connection", (Object)list);
        }
    }

    public Set<ConnectionCookie.Type> getTypes() {
        if (this.typesSet == null) {
            this.typesSet = Collections.unmodifiableSet(new HashSet<ConnectionCookie.Type>(Arrays.asList(this.types)));
        }
        return this.typesSet;
    }

    public List<ConnectionCookie.Type> getRegisteredTypes() {
        LinkedList<ConnectionCookie.Type> typesList = new LinkedList<ConnectionCookie.Type>();
        LinkedList list = this.listeners;
        for (int i = 0; i <= 1; ++i) {
            if (i == 1) {
                list = (LinkedList)this.entry.getFile().getAttribute("EA-OpenIDE-Connection");
            }
            if (list == null) continue;
            for (Pair p : list) {
                typesList.add(p.getType());
            }
        }
        return typesList;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void fireEvent(ConnectionCookie.Event ev) {
        LinkedList list;
        boolean persistent;
        ConnectionSupport connectionSupport = this;
        synchronized (connectionSupport) {
            ConnectionCookie.Type type = ev.getType();
            persistent = type.isPersistent();
            list = persistent ? (LinkedList)this.entry.getFile().getAttribute("EA-OpenIDE-Connection") : this.listeners;
            if (list == null) {
                return;
            }
            list = (LinkedList)list.clone();
        }
        int size = list.size();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            Pair pair = (Pair)it.next();
            if (!pair.getType().overlaps(ev.getType())) continue;
            try {
                ConnectionCookie.Listener l = (ConnectionCookie.Listener)pair.getNode().getCookie(ConnectionCookie.Listener.class);
                if (l == null) continue;
                try {
                    l.notify(ev);
                    continue;
                }
                catch (IllegalArgumentException e) {
                    it.remove();
                    continue;
                }
                catch (ClassCastException e) {
                    it.remove();
                }
            }
            catch (IOException e) {
                it.remove();
            }
        }
        if (persistent && list.size() != size) {
            try {
                this.entry.getFile().setAttribute("EA-OpenIDE-Connection", (Object)list);
            }
            catch (IOException e) {
                // empty catch block
            }
        }
    }

    public synchronized Set listenersFor(ConnectionCookie.Type type) {
        LinkedList list = type.isPersistent() ? (LinkedList)this.entry.getFile().getAttribute("EA-OpenIDE-Connection") : this.listeners;
        if (list == null) {
            return Collections.emptySet();
        }
        Iterator it = list.iterator();
        HashSet<Node> set = new HashSet<Node>(7);
        while (it.hasNext()) {
            Pair pair = (Pair)it.next();
            if (!type.overlaps(pair.getType())) continue;
            try {
                set.add(pair.getNode());
            }
            catch (IOException e) {}
        }
        return set;
    }

    private void testSupported(ConnectionCookie.Type t) throws InvalidObjectException {
        for (int i = 0; i < this.types.length; ++i) {
            if (!t.overlaps(this.types[i])) continue;
            return;
        }
        throw new InvalidObjectException(t.toString());
    }

    private static final class Pair
    implements Serializable {
        private ConnectionCookie.Type type;
        private Object value;
        static final long serialVersionUID = 387180886175136728L;

        public Pair(ConnectionCookie.Type t, Node n) {
            this.type = t;
            this.value = n;
        }

        public Pair(ConnectionCookie.Type t, Node.Handle h) throws IOException {
            if (h == null) {
                throw new IOException();
            }
            this.type = t;
            this.value = h;
        }

        public ConnectionCookie.Type getType() {
            return this.type;
        }

        public Node getNode() throws IOException {
            return this.value instanceof Node ? (Node)this.value : ((Node.Handle)this.value).getNode();
        }
    }

}

