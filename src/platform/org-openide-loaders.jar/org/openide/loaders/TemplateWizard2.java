/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.explorer.propertysheet.DefaultPropertyModel
 *  org.openide.explorer.propertysheet.PropertyModel
 *  org.openide.explorer.propertysheet.PropertyPanel
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.openide.loaders;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ResourceBundle;
import javax.accessibility.AccessibleContext;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.openide.awt.Mnemonics;
import org.openide.explorer.propertysheet.DefaultPropertyModel;
import org.openide.explorer.propertysheet.PropertyModel;
import org.openide.explorer.propertysheet.PropertyPanel;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.TemplateWizard;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

final class TemplateWizard2
extends JPanel
implements DocumentListener {
    private ChangeListener listener;
    private static final String PROP_LOCATION_FOLDER = "locationFolder";
    private File locationFolder;
    private DefaultPropertyModel locationFolderModel;
    private String extension;
    private PropertyPanel dataFolderPanel;
    private JLabel jLabel1;
    private JPanel namePanel;
    private JTextField newObjectName;

    public TemplateWizard2() {
        this.initLocationFolder();
        this.initComponents();
        this.setName(DataObject.getString("LAB_TargetLocationPanelName"));
        ResourceBundle bundle = NbBundle.getBundle(TemplateWizard2.class);
        this.newObjectName.getDocument().addDocumentListener(this);
        this.newObjectName.unregisterKeyboardAction(KeyStroke.getKeyStroke(10, 0));
        this.newObjectName.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_NewObjectName"));
    }

    private void initLocationFolder() {
        PropertyDescriptor pd = null;
        try {
            Method getterMethod = this.getClass().getDeclaredMethod("getLocationFolder", new Class[0]);
            getterMethod.setAccessible(true);
            Method setterMethod = this.getClass().getDeclaredMethod("setLocationFolder", File.class);
            setterMethod.setAccessible(true);
            pd = new PropertyDescriptor("locationFolder", getterMethod, setterMethod);
            pd.setValue("directories", true);
            pd.setValue("files", false);
        }
        catch (IntrospectionException ie) {
            Exceptions.printStackTrace((Throwable)ie);
        }
        catch (NoSuchMethodException nsme) {
            Exceptions.printStackTrace((Throwable)nsme);
        }
        this.locationFolderModel = new DefaultPropertyModel((Object)this, pd);
    }

    private static String defaultNewObjectName() {
        return DataObject.getString("FMT_DefaultNewObjectName");
    }

    private void initComponents() {
        this.namePanel = new JPanel();
        this.jLabel1 = new JLabel();
        this.newObjectName = new JTextField();
        this.dataFolderPanel = new PropertyPanel((PropertyModel)this.locationFolderModel, 2);
        FormListener formListener = new FormListener();
        this.setLayout(new BorderLayout());
        this.namePanel.setLayout(new BorderLayout(12, 0));
        this.jLabel1.setLabelFor(this.newObjectName);
        ResourceBundle bundle = ResourceBundle.getBundle("org/openide/loaders/Bundle");
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)bundle.getString("CTL_NewObjectName"));
        this.namePanel.add((Component)this.jLabel1, "West");
        this.newObjectName.addFocusListener(formListener);
        this.namePanel.add((Component)this.newObjectName, "Center");
        this.add((Component)this.namePanel, "North");
        this.add((Component)this.dataFolderPanel, "Center");
    }

    private void newObjectNameFocusGained(FocusEvent evt) {
        this.newObjectName.selectAll();
    }

    void addChangeListener(ChangeListener l) {
        if (this.listener != null) {
            throw new IllegalStateException();
        }
        this.listener = l;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.newObjectName.requestFocus();
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getBundle(TemplateWizard2.class).getString("ACSD_TemplateWizard2"));
    }

    void implReadSettings(Object settings) {
        TemplateWizard wizard = (TemplateWizard)((Object)settings);
        DataObject template = wizard.getTemplate();
        if (template != null) {
            this.extension = template.getPrimaryFile().getExt();
        }
        this.setNewObjectName(wizard.getTargetName());
        try {
            this.setLocationDataFolder(wizard.getTargetFolder());
        }
        catch (IOException ioe) {
            this.setLocationFolder(null);
        }
    }

    public void removeChangeListener(ChangeListener l) {
        this.listener = null;
    }

    void implStoreSettings(Object settings) {
        TemplateWizard wizard = (TemplateWizard)((Object)settings);
        wizard.setTargetFolder(this.getLocationDataFolder());
        String name = this.newObjectName.getText();
        if (name.equals(TemplateWizard2.defaultNewObjectName())) {
            name = null;
        }
        wizard.setTargetName(name);
    }

    String implIsValid() {
        DataFolder lF = this.getLocationDataFolder();
        if (lF == null) {
            return NbBundle.getMessage(TemplateWizard2.class, (String)"MSG_fs_or_folder_does_not_exist");
        }
        if (!lF.getPrimaryFile().canWrite()) {
            return NbBundle.getMessage(TemplateWizard2.class, (String)"MSG_fs_is_readonly");
        }
        if (this.locationFolder.exists()) {
            return NbBundle.getMessage(TemplateWizard2.class, (String)"MSG_file_already_exist", (Object)this.locationFolder.getAbsolutePath());
        }
        if ((Utilities.isWindows() || Utilities.getOperatingSystem() == 2048) && TemplateWizard.checkCaseInsensitiveName(lF.getPrimaryFile(), this.newObjectName.getText(), this.extension)) {
            return NbBundle.getMessage(TemplateWizard2.class, (String)"MSG_file_already_exist", (Object)this.newObjectName.getText());
        }
        return null;
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        if (e.getDocument() == this.newObjectName.getDocument()) {
            SwingUtilities.invokeLater(new Updater());
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        this.changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        if (e.getDocument() == this.newObjectName.getDocument()) {
            SwingUtilities.invokeLater(new Updater());
        }
    }

    @Override
    public void requestFocus() {
        this.newObjectName.requestFocus();
        this.newObjectName.selectAll();
    }

    private void setNewObjectName(String name) {
        String n = name;
        if (name == null || name.length() == 0) {
            n = TemplateWizard2.defaultNewObjectName();
        }
        this.newObjectName.getDocument().removeDocumentListener(this);
        this.newObjectName.setText(n);
        this.newObjectName.getDocument().addDocumentListener(this);
        if (name == null || name.length() == 0) {
            this.newObjectName.selectAll();
        }
    }

    private void fireStateChanged() {
        if (this.listener != null) {
            this.listener.stateChanged(new ChangeEvent(this));
        }
    }

    public void setLocationFolder(File fd) {
        if (this.locationFolder == fd) {
            return;
        }
        if (this.locationFolder != null && this.locationFolder.equals(fd)) {
            return;
        }
        File oldLocation = this.locationFolder;
        this.locationFolder = fd;
        this.firePropertyChange("locationFolder", oldLocation, this.locationFolder);
        this.fireStateChanged();
    }

    private void setLocationDataFolder(DataFolder fd) {
        this.setLocationFolder(fd != null ? FileUtil.toFile((FileObject)fd.getPrimaryFile()) : null);
    }

    public File getLocationFolder() {
        return this.locationFolder;
    }

    private DataFolder getLocationDataFolder() {
        FileObject f;
        if (this.locationFolder != null && (f = FileUtil.toFileObject((File)FileUtil.normalizeFile((File)this.locationFolder))) != null && f.isFolder()) {
            return DataFolder.findFolder(f);
        }
        return null;
    }

    private class Updater
    implements Runnable {
        Updater() {
        }

        @Override
        public void run() {
            if (TemplateWizard2.this.newObjectName.getText().equals("")) {
                TemplateWizard2.this.setNewObjectName("");
            }
            TemplateWizard2.this.fireStateChanged();
        }
    }

    private class FormListener
    implements FocusListener {
        FormListener() {
        }

        @Override
        public void focusGained(FocusEvent evt) {
            if (evt.getSource() == TemplateWizard2.this.newObjectName) {
                TemplateWizard2.this.newObjectNameFocusGained(evt);
            }
        }

        @Override
        public void focusLost(FocusEvent evt) {
        }
    }

}

