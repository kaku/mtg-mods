/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$FinishablePanel
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.openide.loaders;

import java.awt.Component;
import java.io.IOException;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.NewObjectPanel;
import org.openide.loaders.TemplateWizard;
import org.openide.loaders.TemplateWizard2;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

final class NewObjectWizardPanel
implements WizardDescriptor.FinishablePanel<WizardDescriptor> {
    private NewObjectPanel newObjectPanelUI;
    private ChangeListener listener;
    DataFolder targetFolder;
    private String extension;
    private TemplateWizard wizard;

    NewObjectWizardPanel() {
    }

    private NewObjectPanel getPanelUI() {
        if (this.newObjectPanelUI == null) {
            this.newObjectPanelUI = new NewObjectPanel();
            this.newObjectPanelUI.addChangeListener(this.listener);
        }
        return this.newObjectPanelUI;
    }

    public void addChangeListener(ChangeListener l) {
        if (this.listener != null) {
            throw new IllegalStateException();
        }
        if (this.newObjectPanelUI != null) {
            this.newObjectPanelUI.addChangeListener(l);
        }
        this.listener = l;
    }

    public void removeChangeListener(ChangeListener l) {
        this.listener = null;
        if (this.newObjectPanelUI != null) {
            this.newObjectPanelUI.removeChangeListener(l);
        }
    }

    public Component getComponent() {
        return this.getPanelUI();
    }

    public HelpCtx getHelp() {
        return new HelpCtx(NewObjectPanel.class);
    }

    public boolean isValid() {
        String errorMsg = null;
        boolean isOK = true;
        if (!this.targetFolder.getPrimaryFile().canWrite()) {
            errorMsg = NbBundle.getMessage(TemplateWizard2.class, (String)"MSG_fs_is_readonly");
            isOK = false;
        }
        if (isOK) {
            Object obj = this.targetFolder.getPrimaryFile().getAttribute("isRemoteAndSlow");
            boolean makeFileExistsChecks = true;
            if (obj instanceof Boolean) {
                boolean bl = makeFileExistsChecks = (Boolean)obj == false;
            }
            if (makeFileExistsChecks) {
                FileObject f = this.targetFolder.getPrimaryFile().getFileObject(this.getPanelUI().getNewObjectName(), this.extension);
                if (f != null) {
                    errorMsg = NbBundle.getMessage(TemplateWizard2.class, (String)"MSG_file_already_exist", (Object)f.getNameExt());
                    isOK = false;
                }
                if ((Utilities.isWindows() || Utilities.getOperatingSystem() == 2048) && TemplateWizard.checkCaseInsensitiveName(this.targetFolder.getPrimaryFile(), this.getPanelUI().getNewObjectName(), this.extension)) {
                    errorMsg = NbBundle.getMessage(TemplateWizard2.class, (String)"MSG_file_already_exist", (Object)this.getPanelUI().getNewObjectName());
                    isOK = false;
                }
            }
        }
        this.wizard.putProperty("WizardPanel_errorMessage", (Object)errorMsg);
        return isOK;
    }

    public void readSettings(WizardDescriptor settings) {
        this.wizard = (TemplateWizard)settings;
        DataObject template = this.wizard.getTemplate();
        if (template != null) {
            this.extension = template.getPrimaryFile().getExt();
        }
        try {
            this.targetFolder = this.wizard.getTargetFolder();
        }
        catch (IOException x) {
            Exceptions.printStackTrace((Throwable)x);
        }
    }

    public void storeSettings(WizardDescriptor settings) {
        String name = this.getPanelUI().getNewObjectName();
        if (name.equals(NewObjectPanel.defaultNewObjectName())) {
            name = null;
        }
        if (this.wizard != null) {
            this.wizard.setTargetName(name);
            this.wizard = null;
        }
    }

    public boolean isFinishPanel() {
        return true;
    }
}

