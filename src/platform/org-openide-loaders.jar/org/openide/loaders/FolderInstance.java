/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.InstanceCookie$Of
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 *  org.openide.util.TaskListener
 *  org.openide.util.WeakListeners
 */
package org.openide.loaders;

import java.awt.EventQueue;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.openide.loaders.AWTTask;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.FolderList;
import org.openide.loaders.FolderListListener;
import org.openide.nodes.Node;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.TaskListener;
import org.openide.util.WeakListeners;

public abstract class FolderInstance
extends Task
implements InstanceCookie {
    static final RequestProcessor PROCESSOR = new RequestProcessor("Folder Instance Processor");
    private static final ThreadLocal<Object> CURRENT = new ThreadLocal<T>();
    private static final ThreadLocal<Object> LAST_CURRENT = new ThreadLocal<T>();
    protected DataFolder folder;
    private DataObject.Container container;
    private HashMap<FileObject, HoldInstance> map = new HashMap(17);
    private Task[] waitFor;
    private Object object = CURRENT;
    private Listener listener;
    private Logger err;
    private Task recognizingTask;
    private Task creationTask;
    private volatile int creationSequence;
    private boolean precreateInstances;

    public FolderInstance(DataFolder df) {
        this((DataObject.Container)df);
    }

    public FolderInstance(DataObject.Container container) {
        this(container, null);
    }

    private FolderInstance(DataObject.Container container, String logName) {
        if (container instanceof DataFolder) {
            this.folder = (DataFolder)container;
            if (logName == null) {
                logName = this.folder.getPrimaryFile().getPath().replace('/', '.');
            }
            container = FolderList.find(this.folder.getPrimaryFile(), true);
        }
        this.listener = new Listener();
        logName = logName == null ? "org.openide.loaders.FolderInstance" : "org.openide.loaders.FolderInstance." + logName;
        this.err = Logger.getLogger(logName);
        this.container = container;
        container.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.listener, (Object)container));
        if (this.err.isLoggable(Level.FINE)) {
            this.err.fine("new " + (Object)((Object)this));
        }
    }

    void precreateInstances() {
        this.precreateInstances = true;
    }

    public String instanceName() {
        try {
            return this.instanceClass().getName();
        }
        catch (IOException ex) {
            return "java.lang.Object";
        }
        catch (ClassNotFoundException ex) {
            return "java.lang.Object";
        }
    }

    public Class<?> instanceClass() throws IOException, ClassNotFoundException {
        Object tmp = this.object;
        if (tmp != null) {
            if (tmp instanceof IOException) {
                throw (IOException)tmp;
            }
            if (tmp instanceof ClassNotFoundException) {
                throw (ClassNotFoundException)tmp;
            }
            return tmp.getClass();
        }
        return Object.class;
    }

    public Object instanceCreate() throws IOException, ClassNotFoundException {
        Object tmp = CURRENT.get();
        if (tmp == null || LAST_CURRENT.get() != this) {
            this.err.fine("do into waitFinished");
            this.waitFinished();
            tmp = this.object;
        }
        if (this.err.isLoggable(Level.FINE)) {
            this.err.fine("instanceCreate: " + tmp);
        }
        if (tmp instanceof IOException) {
            throw (IOException)tmp;
        }
        if (tmp instanceof ClassNotFoundException) {
            throw (ClassNotFoundException)tmp;
        }
        if (tmp == CURRENT) {
            throw new IOException("Cyclic reference. Somebody is trying to get value from FolderInstance (" + this.getClass().getName() + ") from the same thread that is processing the instance");
        }
        return tmp;
    }

    public final void instanceFinished() {
        this.waitFinished();
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public void waitFinished() {
        boolean isLog = this.err.isLoggable(Level.FINE);
        do {
            this.err.fine("waitProcessingFinished on container");
            FolderInstance.waitProcessingFinished(this.container);
            Task originalRecognizing = this.checkRecognizingStarted();
            if (isLog) {
                this.err.fine("checkRecognizingStarted: " + (Object)originalRecognizing);
            }
            originalRecognizing.waitFinished();
            Task t = this.creationTask;
            if (isLog) {
                this.err.fine("creationTask: " + (Object)t);
            }
            if (t != null) {
                if (EventQueue.isDispatchThread()) {
                    if (!AWTTask.waitFor(t)) {
                        continue;
                    }
                } else {
                    t.waitFinished();
                }
            }
            Object[] toWait = this.waitFor;
            if (isLog) {
                this.err.fine("toWait: " + Arrays.toString(toWait));
            }
            if (toWait != null) {
                for (int i = 0; i < toWait.length; ++i) {
                    if (isLog) {
                        this.err.fine("  wait[" + i + "]: " + toWait[i]);
                    }
                    toWait[i].waitFinished();
                }
            }
            if (originalRecognizing == this.checkRecognizingStarted()) break;
        } while (true);
        if (!isLog) return;
        this.err.fine("breaking the wait loop");
    }

    public void run() {
        this.recreate();
        this.instanceFinished();
    }

    protected InstanceCookie acceptDataObject(DataObject dob) {
        DataObject.Container c;
        DataFolder fld;
        int acceptType = -1;
        Object cookie = dob.getCookie(InstanceCookie.class);
        try {
            cookie = cookie == null ? null : this.acceptCookie((InstanceCookie)cookie);
            acceptType = 1;
        }
        catch (IOException ex) {
            this.err.log(Level.WARNING, null, ex);
            this.revertProblematicFile(dob);
            cookie = null;
        }
        catch (ClassNotFoundException ex) {
            this.err.log(Level.INFO, null, ex);
            this.revertProblematicFile(dob);
            cookie = null;
        }
        if (cookie == null && (fld = dob.getCookie(DataFolder.class)) != null) {
            HoldInstance previous = this.map.get((Object)fld.getPrimaryFile());
            if (previous != null && previous.cookie != null) {
                cookie = previous;
                acceptType = 2;
            } else {
                cookie = this.acceptFolder(fld);
                acceptType = 3;
            }
        }
        if (cookie == null && (c = dob.getCookie(DataObject.Container.class)) != null) {
            cookie = this.acceptContainer(c);
            acceptType = 4;
        }
        if (this.err.isLoggable(Level.FINE)) {
            this.err.fine("acceptDataObject: " + dob + " cookie: " + cookie + " acceptType: " + acceptType);
        }
        return cookie;
    }

    private void revertProblematicFile(DataObject dob) {
        try {
            dob.getPrimaryFile().revert();
        }
        catch (IOException x) {
            this.err.log(Level.INFO, null, x);
        }
    }

    protected InstanceCookie acceptCookie(InstanceCookie cookie) throws IOException, ClassNotFoundException {
        return cookie;
    }

    protected InstanceCookie acceptFolder(DataFolder df) {
        return this.acceptContainer(df);
    }

    protected InstanceCookie acceptContainer(DataObject.Container container) {
        return null;
    }

    protected abstract Object createInstance(InstanceCookie[] var1) throws IOException, ClassNotFoundException;

    protected Object instanceForCookie(DataObject obj, InstanceCookie cookie) throws IOException, ClassNotFoundException {
        return cookie.instanceCreate();
    }

    public synchronized void recreate() {
        this.err.fine("recreate");
        this.recognizingTask = FolderInstance.computeChildrenList(this.container, this.listener);
        if (this.err.isLoggable(Level.FINE)) {
            this.err.fine("  recognizing task is now " + (Object)this.recognizingTask);
        }
        this.notifyRunning();
    }

    final void checkRecreate() {
        if (this.isFinished()) {
            this.recreate();
        }
    }

    private final synchronized Task checkRecognizingStarted() {
        if (this.recognizingTask == null) {
            this.recreate();
        }
        return this.recognizingTask;
    }

    private static void waitProcessingFinished(DataObject.Container c) {
        if (c instanceof FolderList) {
            ((FolderList)c).waitProcessingFinished();
        }
    }

    private static Task computeChildrenList(final DataObject.Container container, final FolderListListener listener) {
        if (container instanceof FolderList) {
            FolderList list = (FolderList)container;
            return list.computeChildrenList(listener);
        }
        return PROCESSOR.post(new Runnable(){

            @Override
            public void run() {
                DataObject[] arr = container.getChildren();
                ArrayList<DataObject> list = new ArrayList<DataObject>(arr.length);
                for (int i = 0; i < arr.length; ++i) {
                    listener.process(arr[i], list);
                }
                listener.finished(list);
            }
        });
    }

    final void processObjects(Collection<DataObject> arr) {
        class R
        extends Task {
            HoldInstance[] all;
            Object[] instances;
            int sequence;
            Task postCreationTask;
            RequestProcessor.Task instancesTask;
            final /* synthetic */ Collection val$arr;

            R() {
                this.val$arr = var2_2;
            }

            public void init() {
                this.all = this$0.defaultProcessObjects(this.val$arr);
            }

            public void instances() {
                this.instances = new Object[this.all.length];
                for (int indx = 0; indx < this.all.length; ++indx) {
                    try {
                        this.instances[indx] = this.all[indx].instanceCreate();
                        if (this.instances[indx] != null) {
                            continue;
                        }
                    }
                    catch (IOException ex) {
                        this$0.err().log(Level.INFO, "Cannot create " + this.all[indx], ex);
                    }
                    catch (ClassNotFoundException ex) {
                        this$0.err().log(Level.INFO, "Cannot create " + this.all[indx], ex);
                    }
                    this.all[indx] = new HoldInstance(null, this.all[indx].cookie);
                }
            }

            public void run() {
                if (this.sequence != this$0.creationSequence) {
                    return;
                }
                if (this.instancesTask != null && FolderInstance.PROCESSOR.isRequestProcessorThread()) {
                    this.init();
                    this.instances();
                    this.postCreationTask = this$0.postCreationTask((Runnable)((Object)this));
                    return;
                }
                if (this.all == null) {
                    this.init();
                }
                this$0.defaultProcessObjectsFinal(this.all);
            }

            public void waitFinished() {
                if (this.instancesTask != null) {
                    this.instancesTask.waitFinished();
                }
                if (this.postCreationTask != null) {
                    this.postCreationTask.waitFinished();
                }
            }

            public boolean waitFinished(long milliseconds) throws InterruptedException {
                long waitBy = System.currentTimeMillis() - milliseconds;
                if (this.instancesTask != null && !this.instancesTask.waitFinished(milliseconds)) {
                    return false;
                }
                if (this.postCreationTask != null) {
                    long wait = waitBy - System.currentTimeMillis();
                    if (wait < 1) {
                        wait = 1;
                    }
                    return this.postCreationTask.waitFinished(wait);
                }
                return true;
            }
        }
        R process = new R(this, arr);
        process.sequence = ++this.creationSequence;
        if (this.precreateInstances) {
            process.instancesTask = PROCESSOR.create((Runnable)((Object)process));
            this.creationTask = process;
            process.instancesTask.schedule(0);
        } else {
            this.creationTask = this.postCreationTask((Runnable)((Object)process));
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private final HoldInstance[] defaultProcessObjects(Collection<DataObject> arr) {
        HashSet<FileObject> toRemove;
        this.err.fine("defaultProcessObjects");
        if (this.err.isLoggable(Level.FINEST)) {
            this.err.finest("  objects to process:" + Arrays.toString(arr.toArray()));
        }
        ArrayList<HoldInstance> cookies = new ArrayList<HoldInstance>();
        ThreadLocal<Object> threadLocal = CURRENT;
        synchronized (threadLocal) {
            toRemove = new HashSet<FileObject>(this.map.keySet());
        }
        for (DataObject obj : arr) {
            Object fo;
            if (!obj.isValid()) continue;
            InstanceCookie cookie = this.acceptDataObject(obj);
            if (cookie != null) {
                fo = obj.getPrimaryFile();
                boolean attachListener = true;
                HoldInstance prevCookie = null;
                if (toRemove.remove(fo) && (prevCookie = this.map.get(fo)) != null && (prevCookie.cookie == null || !prevCookie.cookie.equals((Object)cookie))) {
                    prevCookie = null;
                    attachListener = false;
                }
                if (prevCookie == null) {
                    HoldInstance hold = cookie instanceof HoldInstance ? (HoldInstance)cookie : new HoldInstance(obj, cookie);
                    ThreadLocal<Object> threadLocal2 = CURRENT;
                    synchronized (threadLocal2) {
                        this.map.put((FileObject)fo, hold);
                    }
                    if (attachListener) {
                        obj.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.listener, (Object)obj));
                    }
                    cookies.add(hold);
                    continue;
                }
                cookies.add(prevCookie);
                continue;
            }
            fo = CURRENT;
            synchronized (fo) {
                FileObject fo2 = obj.getPrimaryFile();
                toRemove.remove((Object)fo2);
                HoldInstance hold = this.map.get((Object)fo2);
                if (hold != null && hold.cookie == null) {
                    continue;
                }
                hold = new HoldInstance(obj, null);
                this.map.put(fo2, hold);
            }
            obj.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.listener, (Object)obj));
        }
        ThreadLocal<Object> i$ = CURRENT;
        synchronized (i$) {
            this.map.keySet().removeAll(toRemove);
        }
        HoldInstance[] all = new HoldInstance[cookies.size()];
        cookies.toArray(all);
        this.updateWaitFor(all);
        return all;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Loose catch block
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Lifted jumps to return sites
     */
    final void defaultProcessObjectsFinal(HoldInstance[] all) {
        Object result;
        block23 : {
            result = null;
            result = this.createInstance((InstanceCookie[])all);
            if (!this.err.isLoggable(Level.FINE)) break block23;
            this.err.fine("notifying finished");
            for (int log = 0; log < all.length; ++log) {
                this.err.log(Level.FINE, "  #{0}: {1}", new Object[]{log, all[log]});
            }
        }
        this.object = result;
        Object prevResult2 = CURRENT.get();
        CURRENT.set(result);
        Object prevLast = LAST_CURRENT.get();
        LAST_CURRENT.set((Object)((Object)this));
        try {
            this.notifyFinished();
            return;
        }
        finally {
            CURRENT.set(prevResult2);
            LAST_CURRENT.set(prevLast);
        }
        catch (IOException ex) {
            block24 : {
                result = ex;
                if (!this.err.isLoggable(Level.FINE)) break block24;
                this.err.fine("notifying finished");
                for (int log = 0; log < all.length; ++log) {
                    this.err.log(Level.FINE, "  #{0}: {1}", new Object[]{log, all[log]});
                }
            }
            this.object = result;
            Object prevResult = CURRENT.get();
            CURRENT.set(result);
            Object prevLast2 = LAST_CURRENT.get();
            LAST_CURRENT.set((Object)((Object)this));
            try {
                this.notifyFinished();
                return;
            }
            finally {
                CURRENT.set(prevResult);
                LAST_CURRENT.set(prevLast2);
            }
            catch (ClassNotFoundException ex2) {
                block25 : {
                    result = ex2;
                    if (!this.err.isLoggable(Level.FINE)) break block25;
                    this.err.fine("notifying finished");
                    for (int log = 0; log < all.length; ++log) {
                        this.err.log(Level.FINE, "  #{0}: {1}", new Object[]{log, all[log]});
                    }
                }
                this.object = result;
                prevResult = CURRENT.get();
                CURRENT.set(result);
                Object prevLast3 = LAST_CURRENT.get();
                LAST_CURRENT.set((Object)((Object)this));
                try {
                    this.notifyFinished();
                    return;
                }
                finally {
                    CURRENT.set(prevResult);
                    LAST_CURRENT.set(prevLast3);
                }
                catch (Throwable throwable) {
                    if (this.err.isLoggable(Level.FINE)) {
                        this.err.fine("notifying finished");
                        for (int log = 0; log < all.length; ++log) {
                            this.err.log(Level.FINE, "  #{0}: {1}", new Object[]{log, all[log]});
                        }
                    }
                    this.object = result;
                    Object prevResult3 = CURRENT.get();
                    CURRENT.set(result);
                    Object prevLast4 = LAST_CURRENT.get();
                    LAST_CURRENT.set((Object)((Object)this));
                    try {
                        this.notifyFinished();
                        throw throwable;
                    }
                    finally {
                        CURRENT.set(prevResult3);
                        LAST_CURRENT.set(prevLast4);
                    }
                }
            }
        }
    }

    private void updateWaitFor(HoldInstance[] arr) {
        ArrayList<Task> out = new ArrayList<Task>(arr.length);
        for (int i = 0; i < arr.length; ++i) {
            Task t = arr[i].getTask();
            if (t == null) continue;
            out.add(t);
        }
        this.waitFor = out.toArray((T[])new Task[out.size()]);
    }

    protected Task postCreationTask(Runnable run) {
        return PROCESSOR.post(run);
    }

    final Logger err() {
        return this.err;
    }

    public String toString() {
        return this.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode((Object)((Object)this))) + "(" + this.container + ")";
    }

    private class HoldInstance
    implements InstanceCookie.Of,
    TaskListener {
        private final DataObject source;
        protected final InstanceCookie cookie;

        public HoldInstance(DataObject source, InstanceCookie cookie) {
            this.cookie = cookie;
            this.source = source;
            if (cookie instanceof Task) {
                Task t = (Task)cookie;
                t.addTaskListener((TaskListener)WeakListeners.create(TaskListener.class, (EventListener)((Object)this), (Object)t));
            }
        }

        public String instanceName() {
            return this.cookie.instanceName();
        }

        public boolean instanceOf(Class<?> type) {
            if (this.cookie instanceof InstanceCookie.Of) {
                InstanceCookie.Of of = (InstanceCookie.Of)this.cookie;
                return of.instanceOf(type);
            }
            try {
                Class clazz = this.cookie.instanceClass();
                return type.isAssignableFrom(clazz);
            }
            catch (IOException ex) {
                return false;
            }
            catch (ClassNotFoundException ex) {
                return false;
            }
        }

        public Class instanceClass() throws IOException, ClassNotFoundException {
            return this.cookie.instanceClass();
        }

        public Object instanceCreate() throws IOException, ClassNotFoundException {
            if (this.source == null) {
                return null;
            }
            return FolderInstance.this.instanceForCookie(this.source, this.cookie);
        }

        public void taskFinished(Task task) {
            FolderInstance.this.checkRecreate();
        }

        public Task getTask() {
            if (this.cookie instanceof Task) {
                return (Task)this.cookie;
            }
            return null;
        }

        public String toString() {
            return super.toString() + "[" + (this.source != null ? this.source.getPrimaryFile().getPath() : "null") + "]";
        }
    }

    private class Listener
    implements PropertyChangeListener,
    FolderListListener {
        Listener() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent ev) {
            Object s = ev.getSource();
            if (s == FolderInstance.this.container) {
                if ("children".equals(ev.getPropertyName())) {
                    FolderInstance.this.err.fine("PROP_CHILDREN");
                    FolderInstance.this.recreate();
                }
                return;
            }
            if ("name".equals(ev.getPropertyName()) && s instanceof DataObject) {
                FolderInstance.this.err.fine("PROP_NAME");
                FolderInstance.this.recreate();
            }
            if ("cookie".equals(ev.getPropertyName()) && s instanceof DataObject) {
                HoldInstance hi;
                DataObject source = (DataObject)s;
                FolderInstance.this.err.fine("PROP_COOKIE: " + source);
                InstanceCookie ic = FolderInstance.this.acceptDataObject(source);
                FileObject fo = source.getPrimaryFile();
                ThreadLocal threadLocal = CURRENT;
                synchronized (threadLocal) {
                    hi = (HoldInstance)FolderInstance.this.map.get((Object)fo);
                }
                if (hi != null) {
                    FolderInstance.this.err.fine("previous instance: " + hi + " new instance " + (Object)ic);
                    if (ic == null || ic != hi && !ic.equals((Object)hi.cookie)) {
                        hi = new HoldInstance(source, ic);
                        threadLocal = CURRENT;
                        synchronized (threadLocal) {
                            FolderInstance.this.map.put(fo, hi);
                        }
                        FolderInstance.this.recreate();
                    }
                }
            }
        }

        @Override
        public void finished(List<DataObject> arr) {
            FolderInstance.this.processObjects(arr);
        }

        @Override
        public void process(DataObject obj, List<DataObject> arr) {
            arr.add(obj);
        }
    }

}

