/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.openide.loaders;

import java.io.IOException;
import java.util.Map;
import org.openide.filesystems.FileObject;

public abstract class CreateFromTemplateHandler {
    public static final String FREE_FILE_EXTENSION = "freeFileExtension";

    protected abstract boolean accept(FileObject var1);

    protected abstract FileObject createFromTemplate(FileObject var1, FileObject var2, String var3, Map<String, Object> var4) throws IOException;
}

