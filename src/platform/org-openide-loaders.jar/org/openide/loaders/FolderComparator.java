/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 */
package org.openide.loaders;

import java.util.Date;
import java.util.Enumeration;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataLoader;
import org.openide.loaders.DataLoaderPool;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;

class FolderComparator
extends DataFolder.SortMode {
    public static final int NONE = 0;
    public static final int NAMES = 1;
    public static final int CLASS = 2;
    public static final int FOLDER_NAMES = 3;
    public static final int LAST_MODIFIED = 4;
    public static final int SIZE = 5;
    public static final int EXTENSIONS = 6;
    private int mode;

    public FolderComparator() {
        this(3);
    }

    public FolderComparator(int mode) {
        this.mode = mode;
    }

    @Override
    public int compare(DataObject o1, DataObject o2) {
        return this.doCompare(o1, o2);
    }

    int doCompare(Object obj1, Object obj2) {
        switch (this.mode) {
            case 0: {
                return 0;
            }
            case 1: {
                return this.compareNames(obj1, obj2);
            }
            case 2: {
                return this.compareClass(obj1, obj2);
            }
            case 3: {
                return this.compareFoldersFirst(obj1, obj2);
            }
            case 4: {
                return FolderComparator.compareLastModified(obj1, obj2);
            }
            case 5: {
                return FolderComparator.compareSize(obj1, obj2);
            }
            case 6: {
                return this.compareExtensions(obj1, obj2);
            }
        }
        assert (false);
        return 0;
    }

    static FileObject findFileObject(Object o) {
        if (o instanceof FileObject) {
            return (FileObject)o;
        }
        if (o instanceof DataObject) {
            return ((DataObject)o).getPrimaryFile();
        }
        Node n = (Node)o;
        DataObject obj = (DataObject)n.getCookie(DataObject.class);
        return obj.getPrimaryFile();
    }

    private static DataObject findDataObject(Object o) {
        if (o instanceof DataObject) {
            return (DataObject)o;
        }
        if (o instanceof FileObject) {
            try {
                return DataObject.find((FileObject)o);
            }
            catch (DataObjectNotFoundException ex) {
                return null;
            }
        }
        Node n = (Node)o;
        DataObject obj = (DataObject)n.getCookie(DataObject.class);
        return obj;
    }

    private int compareNames(Object o1, Object o2) {
        return FolderComparator.findFileObject(o1).getNameExt().compareTo(FolderComparator.findFileObject(o2).getNameExt());
    }

    private int compareFoldersFirst(Object o1, Object o2) {
        boolean f2;
        boolean f1 = FolderComparator.findFileObject(o1).isFolder();
        if (f1 != (f2 = FolderComparator.findFileObject(o2).isFolder())) {
            return f1 ? -1 : 1;
        }
        return this.compareNames(o1, o2);
    }

    private int compareExtensions(Object o1, Object o2) {
        String ext2;
        boolean folder2;
        FileObject obj1 = FolderComparator.findFileObject(o1);
        FileObject obj2 = FolderComparator.findFileObject(o2);
        boolean folder1 = obj1.isFolder();
        if (folder1 != (folder2 = obj2.isFolder())) {
            return folder1 ? -1 : 1;
        }
        if (folder1) {
            return obj1.getNameExt().compareTo(obj2.getNameExt());
        }
        String ext1 = obj1.getExt();
        if (ext1.equals(ext2 = obj2.getExt())) {
            return obj1.getName().compareTo(obj2.getName());
        }
        return ext1.compareTo(ext2);
    }

    private int compareClass(Object o1, Object o2) {
        Class c2;
        DataObject obj1 = FolderComparator.findDataObject(o1);
        DataObject obj2 = FolderComparator.findDataObject(o2);
        Class c1 = obj1.getClass();
        if (c1 == (c2 = obj2.getClass())) {
            return this.compareNames(obj1, obj2);
        }
        DataLoaderPool dlp = DataLoaderPool.getDefault();
        Enumeration<DataLoader> loaders = dlp.allLoaders();
        while (loaders.hasMoreElements()) {
            Class<? extends DataObject> clazz = loaders.nextElement().getRepresentationClass();
            if (clazz == DataObject.class) continue;
            boolean r1 = clazz.isAssignableFrom(c1);
            boolean r2 = clazz.isAssignableFrom(c2);
            if (r1 && r2) {
                return this.compareNames(obj1, obj2);
            }
            if (r1) {
                return -1;
            }
            if (!r2) continue;
            return 1;
        }
        return this.compareNames(obj1, obj2);
    }

    private static int compareLastModified(Object o1, Object o2) {
        boolean f2;
        Date d2;
        boolean f1 = FolderComparator.findFileObject(o1).isFolder();
        if (f1 != (f2 = FolderComparator.findFileObject(o2).isFolder())) {
            return f1 ? -1 : 1;
        }
        FileObject fo1 = FolderComparator.findFileObject(o1);
        FileObject fo2 = FolderComparator.findFileObject(o2);
        Date d1 = fo1.lastModified();
        if (d1.after(d2 = fo2.lastModified())) {
            return -1;
        }
        if (d2.after(d1)) {
            return 1;
        }
        return fo1.getNameExt().compareTo(fo2.getNameExt());
    }

    private static int compareSize(Object o1, Object o2) {
        long s2;
        boolean f2;
        boolean f1 = FolderComparator.findFileObject(o1).isFolder();
        if (f1 != (f2 = FolderComparator.findFileObject(o2).isFolder())) {
            return f1 ? -1 : 1;
        }
        FileObject fo1 = FolderComparator.findFileObject(o1);
        FileObject fo2 = FolderComparator.findFileObject(o2);
        long s1 = fo1.getSize();
        if (s1 > (s2 = fo2.getSize())) {
            return -1;
        }
        if (s2 > s1) {
            return 1;
        }
        return fo1.getNameExt().compareTo(fo2.getNameExt());
    }
}

