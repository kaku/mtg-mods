/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.openide.loaders;

import java.io.IOException;
import org.openide.filesystems.FileObject;

public class DataObjectNotFoundException
extends IOException {
    static final long serialVersionUID = 1646623156535839081L;
    private FileObject obj;

    public DataObjectNotFoundException(FileObject obj) {
        super(obj.toString());
        this.obj = obj;
    }

    public FileObject getFileObject() {
        return this.obj;
    }
}

