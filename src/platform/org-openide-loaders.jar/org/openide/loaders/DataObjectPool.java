/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.util.RequestProcessor
 *  org.openide.util.Utilities
 *  org.openide.util.WeakSet
 */
package org.openide.loaders;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.EventObject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataLoader;
import org.openide.loaders.DataLoaderPool;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.FolderList;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.loaders.OperationEvent;
import org.openide.loaders.ShadowChangeAdapter;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.util.WeakSet;

final class DataObjectPool
implements ChangeListener {
    private static final ThreadLocal<Collection<Item>> FIND = new ThreadLocal();
    private static final Validator VALIDATOR = new Validator();
    private static final Collection<Item> TOKEN = Collections.unmodifiableList(new ArrayList());
    private DoubleHashMap map;
    private Map<FileObject, List<Item>> children;
    private final Set<FileSystem> knownFileSystems;
    private static final Logger err = Logger.getLogger("org.openide.loaders.DataObject.find");
    private static DataObjectPool POOL;
    private static final Object lockPOOL;
    private volatile long inWaitNotified;
    private Thread atomic;
    private RequestProcessor privileged;
    private FileObject blocked;
    private Set<Item> toNotify;
    private final Map<FileObject, Integer> registrationCounts;
    private static final DataLoaderPool lp;
    private static final Logger LISTENER;

    static final void fastCache(boolean fast) {
        DataObjectPool.POOL.children = fast ? null : new HashMap<FileObject, List<Item>>();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static DataObjectPool getPOOL() {
        Object object = lockPOOL;
        synchronized (object) {
            if (POOL != null) {
                return POOL;
            }
            POOL = new DataObjectPool();
        }
        lp.addChangeListener(POOL);
        return POOL;
    }

    private static Collection<Item> enterAllowConstructor() {
        Collection<Item> prev = FIND.get();
        FIND.set(TOKEN);
        return prev;
    }

    private static void exitAllowConstructor(Collection<Item> previous) {
        Collection<Item> l = FIND.get();
        FIND.set(previous);
        if (l != TOKEN) {
            DataObjectPool.getPOOL().notifyCreationAll(l);
        }
    }

    static final boolean isConstructorAllowed() {
        return FIND.get() != null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static DataObject handleFindDataObject(DataLoader loader, FileObject fo, DataLoader.RecognizedFiles rec) throws IOException {
        DataObject ret;
        Collection<Item> prev = DataObjectPool.enterAllowConstructor();
        try {
            DataObjectPool.getPOOL().enterRecognition(fo);
            ret = loader.handleFindDataObject(fo, rec);
        }
        finally {
            DataObjectPool.exitAllowConstructor(prev);
        }
        return ret;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static DataObject handleFindDataObject(DataObject.Factory factory, FileObject fo, Set<? super FileObject> rec) throws IOException {
        DataObject ret;
        Collection<Item> prev = DataObjectPool.enterAllowConstructor();
        try {
            DataObjectPool.getPOOL().enterRecognition(fo);
            ret = factory.findDataObject(fo, rec);
        }
        finally {
            DataObjectPool.exitAllowConstructor(prev);
        }
        return ret;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static MultiDataObject createMultiObject(MultiFileLoader loader, FileObject fo) throws IOException {
        MultiDataObject ret;
        Collection<Item> prev = DataObjectPool.enterAllowConstructor();
        try {
            ret = loader.createMultiObject(fo);
        }
        finally {
            DataObjectPool.exitAllowConstructor(prev);
        }
        return ret;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static MultiDataObject createMultiObject(DataLoaderPool.FolderLoader loader, FileObject fo, DataFolder original) throws IOException {
        MultiDataObject ret;
        Collection<Item> prev = DataObjectPool.enterAllowConstructor();
        try {
            ret = loader.createMultiObject(fo, original);
        }
        finally {
            DataObjectPool.exitAllowConstructor(prev);
        }
        return ret;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void runAtomicActionSimple(FileObject fo, FileSystem.AtomicAction action) throws IOException {
        Collection<Item> prev = DataObjectPool.enterAllowConstructor();
        try {
            fo.getFileSystem().runAtomicAction(action);
        }
        finally {
            DataObjectPool.exitAllowConstructor(prev);
        }
    }

    public void runAtomicAction(FileObject target, FileSystem.AtomicAction action) throws IOException {
        class WrapAtomicAction
        implements FileSystem.AtomicAction {
            final /* synthetic */ FileObject val$target;
            final /* synthetic */ FileSystem.AtomicAction val$action;

            WrapAtomicAction() {
                this.val$target = var2_2;
                this.val$action = var3_3;
            }

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            public void run() throws IOException {
                FileObject prevBlocked;
                Thread prev;
                DataObjectPool dataObjectPool = this$0;
                synchronized (dataObjectPool) {
                    this$0.enterRecognition(null);
                    prev = this$0.atomic;
                    prevBlocked = this$0.blocked;
                    this$0.atomic = Thread.currentThread();
                    this$0.blocked = this.val$target;
                }
                Collection findPrev = DataObjectPool.enterAllowConstructor();
                try {
                    this.val$action.run();
                }
                finally {
                    DataObjectPool dataObjectPool2 = this$0;
                    synchronized (dataObjectPool2) {
                        this$0.atomic = prev;
                        this$0.blocked = prevBlocked;
                        this$0.notifyAll();
                    }
                    DataObjectPool.exitAllowConstructor(findPrev);
                }
            }

            public boolean equals(Object obj) {
                if (obj == null) {
                    return false;
                }
                return this.val$action.equals(obj) || obj.equals((Object)this.val$action);
            }

            public int hashCode() {
                return this.val$action.hashCode();
            }
        }
        target.getFileSystem().runAtomicAction((FileSystem.AtomicAction)new WrapAtomicAction(this, target, action));
    }

    public synchronized void enterPrivilegedProcessor(RequestProcessor delegate) {
        if (this.atomic == Thread.currentThread()) {
            if (this.privileged != null) {
                throw new IllegalStateException("Previous privileged is not null: " + (Object)this.privileged + " now: " + (Object)delegate);
            }
            this.privileged = delegate;
        }
        this.notifyAll();
    }

    public synchronized void exitPrivilegedProcessor(RequestProcessor delegate) {
        if (this.atomic == Thread.currentThread()) {
            if (this.privileged != delegate) {
                throw new IllegalStateException("Trying to unregister wrong privileged. Prev: " + (Object)this.privileged + " now: " + (Object)delegate);
            }
            this.privileged = null;
        }
        this.notifyAll();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private synchronized void enterRecognition(FileObject fo) {
        while (!(this.atomic == null || this.atomic == Thread.currentThread() || this.privileged != null && this.privileged.isRequestProcessorThread() || fo != null && this.blocked != null && !this.blocked.equals((Object)fo.getParent()))) {
            if (err.isLoggable(Level.FINE)) {
                err.fine("Enter recognition block: " + Thread.currentThread());
                err.fine("            waiting for: " + (Object)fo);
                err.fine("        blocking thread: " + this.atomic);
                err.fine("             blocked on: " + (Object)this.blocked);
            }
            try {
                if (FolderList.isFolderRecognizerThread()) {
                    this.inWaitNotified = System.currentTimeMillis();
                }
                this.wait();
                continue;
            }
            catch (InterruptedException ex) {}
            continue;
            finally {
                if (!FolderList.isFolderRecognizerThread()) continue;
                this.inWaitNotified = -1;
                continue;
            }
        }
    }

    private DataObjectPool() {
        this.map = new DoubleHashMap();
        this.children = new HashMap<FileObject, List<Item>>();
        this.knownFileSystems = new WeakSet();
        this.inWaitNotified = -1;
        this.toNotify = new HashSet<Item>();
        this.registrationCounts = new WeakHashMap<FileObject, Integer>();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public DataObject find(FileObject fo) {
        DataObjectPool dataObjectPool = this;
        synchronized (dataObjectPool) {
            Collection<Item> l;
            Item doh = (Item)this.map.get((Object)fo);
            if (doh == null || !fo.isValid()) {
                return null;
            }
            if (this.toNotify.contains(doh) && ((l = FIND.get()) == null || !l.contains(doh))) {
                return null;
            }
            return doh.getDataObjectOrNull();
        }
    }

    void countRegistration(FileObject fo) {
        Integer i = this.registrationCounts.get((Object)fo);
        Integer i2 = i == null ? Integer.valueOf(0) : Integer.valueOf(i + 1);
        this.registrationCounts.put(fo, i2);
    }

    int registrationCount(FileObject fo) {
        Integer i = this.registrationCounts.get((Object)fo);
        if (i == null) {
            return 0;
        }
        return i;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void refreshAllFolders() {
        HashSet files;
        DataObjectPool dataObjectPool = this;
        synchronized (dataObjectPool) {
            files = new HashSet(this.map.keySet());
        }
        for (FileObject fo : files) {
            DataObject obj;
            if (!fo.isFolder() || !((obj = this.find(fo)) instanceof DataFolder)) continue;
            DataFolder df = (DataFolder)obj;
            FileObject file = df.getPrimaryFile();
            DataObjectPool dataObjectPool2 = this;
            synchronized (dataObjectPool2) {
                if (this.toNotify.isEmpty() || !this.toNotify.contains(this.map.get((Object)file))) {
                    FolderList.changedDataSystem(file);
                }
                continue;
            }
        }
    }

    public Set<DataObject> revalidate(Set<FileObject> s) {
        return VALIDATOR.revalidate(s);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Set<DataObject> revalidate() {
        HashSet<Item> set;
        DataObjectPool dataObjectPool = this;
        synchronized (dataObjectPool) {
            set = new HashSet<Item>(this.map.values());
        }
        return this.revalidate(DataObjectPool.createSetOfAllFiles(set));
    }

    public void notifyCreation(DataObject obj) {
        this.notifyCreation(obj.item());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void notifyCreation(Item item) {
        DataObjectPool dataObjectPool = this;
        synchronized (dataObjectPool) {
            if (err.isLoggable(Level.FINE)) {
                err.fine("Notify created: " + item + " by " + Thread.currentThread());
            }
            if (this.toNotify.isEmpty()) {
                if (err.isLoggable(Level.FINE)) {
                    err.fine("  but toNotify is empty");
                }
                return;
            }
            if (!this.toNotify.remove(item)) {
                if (err.isLoggable(Level.FINE)) {
                    err.fine("  the item is not there: " + this.toNotify);
                }
                return;
            }
            this.notifyAll();
        }
        DataObject obj = item.getDataObjectOrNull();
        if (obj != null) {
            lp.fireOperationEvent(new OperationEvent(obj), 7);
        }
    }

    private void notifyCreationAll(Collection<Item> l) {
        if (l.isEmpty()) {
            return;
        }
        for (Item i : l) {
            this.notifyCreation(i);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Converted monitor instructions to comments
     * Lifted jumps to return sites
     */
    public void waitNotified(DataObject obj) {
        do {
            block17 : {
                var2_2 = this;
                // MONITORENTER : var2_2
                try {
                    this.enterRecognition(obj.getPrimaryFile().getParent());
                    if (this.toNotify.isEmpty()) {
                        return;
                    }
                    l = DataObjectPool.FIND.get();
                    item = obj.item();
                    if (l != null && l.contains(item)) {
                        return;
                    }
                    if (!this.toNotify.contains(item)) {
                        return;
                    }
                    if (DataObjectPool.err.isLoggable(Level.FINE)) {
                        DataObjectPool.err.fine("waitTillNotified: " + Thread.currentThread());
                        DataObjectPool.err.fine("      waitingFor: " + obj.getPrimaryFile().getPath());
                    }
                    if (FolderList.isFolderRecognizerThread()) {
                        this.inWaitNotified = System.currentTimeMillis();
                    }
                    this.wait();
                    break block17;
                }
                catch (InterruptedException ex) {}
                ** GOTO lbl27
                finally {
                    if (FolderList.isFolderRecognizerThread()) {
                        this.inWaitNotified = -1;
                    }
                }
            }
            // MONITOREXIT : var2_2
        } while (true);
    }

    final long timeInWaitNotified() {
        long l = this.inWaitNotified;
        if (l == -1) {
            return -1;
        }
        l = System.currentTimeMillis() - l;
        if (l < 0) {
            l = 0;
        }
        return l;
    }

    private void notifyAdd(Item item) {
        this.toNotify.add(item);
        Collection<Item> l = FIND.get();
        if (l == TOKEN) {
            l = new ArrayList<Item>();
            FIND.set(l);
        }
        l.add(item);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static Collection<Item> getTargets(FileEvent fe, boolean checkSiblings) {
        FileObject fo = fe.getFile();
        FileObject[] siblings = null;
        FileObject parent = null;
        do {
            block11 : {
                DataObjectPool dataObjectPool = DataObjectPool.getPOOL();
                synchronized (dataObjectPool) {
                    Item itm = (Item)DataObjectPool.POOL.map.get((Object)fo);
                    if (itm != null) {
                        return Collections.singleton(itm);
                    }
                    List<Item> arr = DataObjectPool.POOL.children.get((Object)fo.getParent());
                    if (arr != null) {
                        return new ArrayList<Item>(arr);
                    }
                    if (!checkSiblings) {
                        return Collections.emptySet();
                    }
                    LinkedList<Item> toNotify = new LinkedList<Item>();
                    if (parent == null) {
                        parent = fo.getParent();
                    }
                    if (parent != null) {
                        if (siblings == null) {
                            break block11;
                        }
                        for (int i = 0; i < siblings.length; ++i) {
                            DataObject obj;
                            itm = (Item)DataObjectPool.POOL.map.get((Object)siblings[i]);
                            if (itm == null || (obj = itm.getDataObjectOrNull()) == null) continue;
                            toNotify.add(itm);
                        }
                    }
                    return toNotify;
                }
            }
            siblings = parent.getChildren();
        } while (true);
    }

    public static void checkAttributeChanged(FileAttributeEvent fe) {
        if (LISTENER.isLoggable(Level.FINE)) {
            LISTENER.fine("fileAttributeChanged: " + (Object)fe);
        }
        for (Item item : DataObjectPool.getTargets((FileEvent)fe, false)) {
            DataObject dobj = item.getDataObjectOrNull();
            if (LISTENER.isLoggable(Level.FINE)) {
                LISTENER.fine("  to: " + dobj);
            }
            if (dobj == null) continue;
            dobj.notifyAttributeChanged(fe);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Item register(FileObject fo, DataLoader loader) throws DataObjectExistsException {
        DataObject obj;
        Item doh;
        if (FIND.get() == null) {
            throw new IllegalStateException("DataObject constructor can be called only thru DataObject.find - use that method");
        }
        try {
            FileSystem fs = fo.getFileSystem();
            Set<FileSystem> set = this.knownFileSystems;
            synchronized (set) {
                if (!this.knownFileSystems.contains((Object)fs)) {
                    fs.addFileChangeListener((FileChangeListener)new FSListener());
                    this.knownFileSystems.add(fs);
                }
            }
        }
        catch (FileStateInvalidException e) {
            // empty catch block
        }
        FileObject parent = fo.getParent();
        DataObjectPool dataObjectPool = this;
        synchronized (dataObjectPool) {
            doh = (Item)this.map.get((Object)fo);
            if (doh == null) {
                doh = new Item(fo);
                this.map.putWithParent(fo, parent, doh);
                this.countRegistration(fo);
                this.notifyAdd(doh);
                VALIDATOR.notifyRegistered(fo);
                return doh;
            }
            obj = doh.getDataObjectOrNull();
            if (obj == null) {
                doh = new Item(fo);
                this.map.putWithParent(fo, parent, doh);
                this.countRegistration(fo);
                this.notifyAdd(doh);
                return doh;
            }
            if (!VALIDATOR.reregister(obj, loader)) {
                throw new DataObjectExistsException(obj);
            }
        }
        try {
            obj.setValid(false);
            dataObjectPool = this;
            synchronized (dataObjectPool) {
                Item doh2 = (Item)this.map.get((Object)fo);
                if (doh2 == null) {
                    doh = new Item(fo);
                    this.map.putWithParent(fo, parent, doh);
                    this.countRegistration(fo);
                    this.notifyAdd(doh);
                    return doh;
                }
            }
        }
        catch (PropertyVetoException ex) {
            VALIDATOR.refusingObjects.add(obj);
        }
        throw new DataObjectExistsException(obj);
    }

    private synchronized void deregister(Item item, FileObject fo, FileObject parent, boolean refresh) {
        Item item2;
        DataFolder df;
        Item previous = this.map.remove((Object)fo);
        if (previous != null && previous != item) {
            this.map.putWithParent(fo, parent, previous);
            if (this.toNotify.remove(item)) {
                this.notifyAll();
            }
            return;
        }
        if (refresh && (fo = fo.getParent()) != null && (item2 = (Item)this.map.get((Object)fo)) != null && (df = (DataFolder)item2.getDataObjectOrNull()) != null) {
            VALIDATOR.refreshFolderOf(df);
        }
    }

    private synchronized Item changePrimaryFile(Item item, FileObject newFile, FileObject newParent) {
        if (item.primaryFile == newFile) {
            return item;
        }
        Item prev = this.map.remove((Object)item.primaryFile);
        if (prev == null && item.getDataObjectOrNull() == null) {
            return item;
        }
        assert (prev == item);
        Item ni = new Item(item, newFile);
        this.map.putWithParent(newFile, newParent, ni);
        this.countRegistration(newFile);
        return ni;
    }

    @Override
    public void stateChanged(ChangeEvent ev) {
        this.revalidate();
    }

    private static Set<FileObject> createSetOfAllFiles(Collection<Item> c) {
        HashSet<FileObject> set = new HashSet<FileObject>(c.size() * 7);
        for (Item item : c) {
            DataObject obj = item.getDataObjectOrNull();
            if (obj == null) continue;
            DataObjectPool.getPOOL().waitNotified(obj);
            set.addAll(obj.files());
        }
        return set;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    Iterator<Item> getActiveDataObjects() {
        DataObjectPool dataObjectPool = this;
        synchronized (dataObjectPool) {
            return new ArrayList(this.map.values()).iterator();
        }
    }

    static {
        lockPOOL = new Object();
        lp = DataLoaderPool.getDefault();
        LISTENER = Logger.getLogger("org.openide.loaders.DataObjectPool.Listener");
    }

    private final class DoubleHashMap
    extends HashMap<FileObject, Item> {
        public DoubleHashMap() {
            super(512);
        }

        @Override
        public Item put(FileObject obj, Item item) {
            return this.putWithParent(obj, obj.getParent(), item);
        }

        final Item putWithParent(FileObject obj, FileObject parent, Item item) {
            Item prev = HashMap.super.put(obj, item);
            if (DataObjectPool.this.children == null) {
                return prev;
            }
            if (parent == null) {
                return prev;
            }
            ArrayList<Item> arr = (ArrayList<Item>)DataObjectPool.this.children.get((Object)parent);
            if (arr == null) {
                arr = new ArrayList<Item>();
            }
            arr.add(item);
            return prev;
        }

        @Override
        public Item remove(Object obj) {
            Item prev = (Item)HashMap.super.remove(obj);
            if (!(obj instanceof FileObject)) {
                return prev;
            }
            if (DataObjectPool.this.children == null) {
                return prev;
            }
            FileObject parent = ((FileObject)obj).getParent();
            if (parent == null) {
                return prev;
            }
            List arr = (List)DataObjectPool.this.children.get((Object)parent);
            if (arr != null) {
                arr.remove(obj);
                if (arr.isEmpty()) {
                    DataObjectPool.this.children.remove((Object)parent);
                }
            }
            return prev;
        }
    }

    private static final class Validator
    implements DataLoader.RecognizedFiles {
        private static final Logger err = Logger.getLogger("org.openide.loaders.DataObject.Validator");
        private Set<FileObject> files;
        private Thread current;
        private int waiters;
        private int reenterCount;
        private Set<FileObject> recognizedFiles;
        private Set<DataObject> refusingObjects;
        private Set<FileObject> createdFiles;

        Validator() {
        }

        private synchronized Set<FileObject> enter(Set<FileObject> set) {
            boolean log = err.isLoggable(Level.FINE);
            if (log) {
                err.fine("enter: " + set + " on thread: " + Thread.currentThread());
            }
            if (this.current == Thread.currentThread()) {
                ++this.reenterCount;
                if (log) {
                    err.fine("current thread, rentered: " + this.reenterCount);
                }
            } else {
                ++this.waiters;
                if (log) {
                    err.fine("Waiting as waiter: " + this.waiters);
                }
                while (this.current != null) {
                    try {
                        this.wait();
                    }
                    catch (InterruptedException ex) {}
                }
                this.current = Thread.currentThread();
                --this.waiters;
                if (log) {
                    err.fine("Wait finished, waiters: " + this.waiters + " new current: " + this.current);
                }
            }
            if (this.files == null) {
                if (log) {
                    err.fine("New files: " + set);
                }
                this.files = set;
            } else {
                this.files.addAll(set);
                if (log) {
                    err.fine("Added files: " + set);
                    err.fine("So they are: " + this.files);
                }
            }
            return this.files;
        }

        private synchronized void exit() {
            boolean log = err.isLoggable(Level.FINE);
            if (this.reenterCount == 0) {
                this.current = null;
                if (this.waiters == 0) {
                    this.files = null;
                }
                this.notify();
                if (log) {
                    err.fine("Exit and notify from " + Thread.currentThread());
                }
            } else {
                --this.reenterCount;
                if (log) {
                    err.fine("Exit reentrant: " + this.reenterCount);
                }
            }
        }

        private synchronized boolean goOn() {
            return this.waiters == 0;
        }

        public void refreshFolderOf(DataFolder df) {
            if (this.createdFiles == null) {
                FolderList.changedDataSystem(df.getPrimaryFile());
            }
        }

        @Override
        public void markRecognized(FileObject fo) {
            this.recognizedFiles.add(fo);
        }

        public void notifyRegistered(FileObject fo) {
            if (this.createdFiles != null) {
                this.createdFiles.add(fo);
            }
        }

        public boolean reregister(DataObject obj, DataLoader loader) {
            if (this.recognizedFiles == null) {
                return false;
            }
            if (obj.getLoader() == loader) {
                return false;
            }
            if (this.createdFiles.contains((Object)obj.getPrimaryFile())) {
                return false;
            }
            if (this.refusingObjects.contains(obj)) {
                return false;
            }
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Set<DataObject> revalidate(Set<FileObject> s) {
            if (s.size() == 1 && this.current == Thread.currentThread() && this.files != null && this.files.contains((Object)s.iterator().next())) {
                return new HashSet<DataObject>();
            }
            LinkedList<DataObject> createObjects = new LinkedList<DataObject>();
            boolean log = err.isLoggable(Level.FINE);
            try {
                s = this.enter(s);
                this.recognizedFiles = new HashSet<FileObject>();
                this.refusingObjects = new HashSet<DataObject>();
                this.createdFiles = new HashSet<FileObject>();
                DataLoaderPool pool = lp;
                Iterator<FileObject> it = s.iterator();
                while (it.hasNext() && this.goOn()) {
                    try {
                        FileObject fo = it.next();
                        if (log) {
                            err.fine("Iterate: " + (Object)fo);
                        }
                        if (this.recognizedFiles.contains((Object)fo)) continue;
                        boolean invalidate = false;
                        DataObject orig = DataObjectPool.getPOOL().find(fo);
                        if (log) {
                            err.fine("Original: " + orig);
                        }
                        if (orig == null) continue;
                        DataObject obj = pool.findDataObject(fo, this);
                        createObjects.add(obj);
                        invalidate = obj != orig;
                        if (!invalidate) continue;
                        if (log) {
                            err.fine("Invalidate: " + obj);
                        }
                        it.remove();
                        try {
                            orig.setValid(false);
                            continue;
                        }
                        catch (PropertyVetoException ex) {
                            this.refusingObjects.add(orig);
                            if (!log) continue;
                            err.fine("  Refusing: " + orig);
                        }
                    }
                    catch (DataObjectExistsException ex) {
                    }
                    catch (IOException ioe) {
                        Logger.getLogger(DataObjectPool.class.getName()).log(Level.WARNING, null, ioe);
                    }
                    catch (ConcurrentModificationException cme) {
                        it = s.iterator();
                        if (!log) continue;
                        err.log(Level.FINE, null, cme);
                        err.fine("New iterator over: " + s);
                    }
                }
                Set<DataObject> cme = this.refusingObjects;
                return cme;
            }
            finally {
                this.recognizedFiles = null;
                this.refusingObjects = null;
                this.createdFiles = null;
                this.exit();
                if (log) {
                    err.fine("will do refreshAllFolders: " + s.size());
                }
                DataObjectPool.getPOOL().refreshAllFolders();
                if (log) {
                    err.fine("refreshAllFolders done");
                }
            }
        }
    }

    static final class ItemReference
    extends WeakReference<DataObject>
    implements Runnable {
        private Item item;

        ItemReference(DataObject dobject, Item item) {
            super(dobject, Utilities.activeReferenceQueue());
            this.item = item;
        }

        @Override
        public void run() {
            this.item.deregister(false);
            this.item = null;
        }
    }

    static final class Item {
        private static final Reference<DataObject> REFERENCE_NOT_SET = new WeakReference<Object>(null);
        private Reference<DataObject> obj;
        final FileObject primaryFile;

        public Item(FileObject fo) {
            assert (Thread.holdsLock(DataObjectPool.getPOOL()));
            this.primaryFile = fo;
            this.obj = REFERENCE_NOT_SET;
        }

        private Item(Item clone, FileObject newFo) {
            assert (Thread.holdsLock(DataObjectPool.getPOOL()));
            this.primaryFile = newFo;
            this.obj = clone.obj;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void setDataObject(DataObject dobj) {
            DataObjectPool dataObjectPool = DataObjectPool.getPOOL();
            synchronized (dataObjectPool) {
                this.obj = new ItemReference(dobj, this);
                if (dobj != null && !dobj.getPrimaryFile().isValid()) {
                    DataObjectPool.getPOOL().countRegistration(dobj.getPrimaryFile());
                    this.deregister(false);
                }
                DataObjectPool.getPOOL().notifyAll();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        DataObject getDataObjectOrNull() {
            DataObjectPool dataObjectPool = DataObjectPool.getPOOL();
            synchronized (dataObjectPool) {
                while (this.obj == REFERENCE_NOT_SET) {
                    try {
                        DataObjectPool.getPOOL().wait();
                    }
                    catch (InterruptedException exc) {}
                }
                return this.obj.get();
            }
        }

        public DataObject getDataObject() {
            DataObject o = this.getDataObjectOrNull();
            if (o == null) {
                throw new IllegalStateException();
            }
            return o;
        }

        public void deregister(boolean refresh) {
            DataObjectPool.getPOOL().deregister(this, this.primaryFile, this.primaryFile.getParent(), refresh);
        }

        public Item changePrimaryFile(FileObject newFile) {
            return DataObjectPool.getPOOL().changePrimaryFile(this, newFile, newFile.getParent());
        }

        public boolean isValid() {
            if (DataObjectPool.getPOOL().map.get((Object)this.primaryFile) == this) {
                return this.primaryFile.isValid();
            }
            return false;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public String toString() {
            DataObjectPool dataObjectPool = DataObjectPool.getPOOL();
            synchronized (dataObjectPool) {
                DataObject o = this.obj.get();
                if (o == null) {
                    return "nothing[" + (Object)this.primaryFile + "]";
                }
                return o.toString();
            }
        }
    }

    private final class FSListener
    extends FileChangeAdapter {
        FSListener() {
        }

        public void fileChanged(FileEvent fe) {
            if (LISTENER.isLoggable(Level.FINE)) {
                LISTENER.fine("fileChanged: " + (Object)fe);
            }
            for (Item item : DataObjectPool.getTargets(fe, false)) {
                DataObject dobj = item.getDataObjectOrNull();
                if (LISTENER.isLoggable(Level.FINE)) {
                    LISTENER.fine("  to: " + dobj);
                }
                if (dobj == null) continue;
                dobj.notifyFileChanged(fe);
            }
        }

        public void fileRenamed(FileRenameEvent fe) {
            if (LISTENER.isLoggable(Level.FINE)) {
                LISTENER.fine("fileRenamed: " + (Object)fe);
            }
            for (Item item : DataObjectPool.getTargets((FileEvent)fe, false)) {
                DataObject dobj = item.getDataObjectOrNull();
                if (LISTENER.isLoggable(Level.FINE)) {
                    LISTENER.fine("  to: " + dobj);
                }
                if (dobj == null) continue;
                dobj.notifyFileRenamed(fe);
            }
        }

        public void fileDeleted(FileEvent fe) {
            if (LISTENER.isLoggable(Level.FINE)) {
                LISTENER.fine("fileDeleted: " + (Object)fe);
            }
            for (Item item : DataObjectPool.getTargets(fe, true)) {
                DataObject dobj = item.getDataObjectOrNull();
                if (LISTENER.isLoggable(Level.FINE)) {
                    LISTENER.fine("  to: " + dobj);
                }
                if (dobj == null) continue;
                dobj.notifyFileDeleted(fe);
            }
        }

        public void fileDataCreated(FileEvent fe) {
            if (LISTENER.isLoggable(Level.FINE)) {
                LISTENER.fine("fileDataCreated: " + (Object)fe);
            }
            for (Item item : DataObjectPool.getTargets(fe, true)) {
                DataObject dobj = item.getDataObjectOrNull();
                if (LISTENER.isLoggable(Level.FINE)) {
                    LISTENER.fine("  to: " + dobj);
                }
                if (dobj == null) continue;
                dobj.notifyFileDataCreated(fe);
            }
            ShadowChangeAdapter.checkBrokenDataShadows((EventObject)fe);
        }

        public void fileAttributeChanged(FileAttributeEvent fe) {
            DataObjectPool.checkAttributeChanged(fe);
        }

        public void fileFolderCreated(FileEvent fe) {
            if (LISTENER.isLoggable(Level.FINE)) {
                LISTENER.fine("fileFolderCreated: " + (Object)fe);
            }
            ShadowChangeAdapter.checkBrokenDataShadows((EventObject)fe);
        }
    }

}

