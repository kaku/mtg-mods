/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.openide.loaders;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ResourceBundle;
import javax.accessibility.AccessibleContext;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.text.Document;
import org.openide.awt.Mnemonics;
import org.openide.loaders.TemplateWizard;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

final class NewObjectPanel
extends JPanel
implements DocumentListener {
    private ChangeListener listener;
    private JLabel jLabel1;
    private JPanel namePanel;
    private JTextField newObjectName;

    public NewObjectPanel() {
        this.initComponents();
        this.setName(NewObjectPanel.getString("LAB_NewObjectPanelName"));
        this.setBorder(new EmptyBorder(new Insets(8, 8, 8, 8)));
        this.newObjectName.getDocument().addDocumentListener(this);
        this.newObjectName.unregisterKeyboardAction(KeyStroke.getKeyStroke(10, 0));
        ResourceBundle bundle = NbBundle.getBundle(NewObjectPanel.class);
        this.setNewObjectName("");
        this.putClientProperty("WizardPanel_contentData", new String[]{this.getName()});
        this.putClientProperty("WizardPanel_contentSelectedIndex", new Integer(0));
        this.newObjectName.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_NewObjectName"));
        this.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_NewObjectPanel"));
    }

    @Override
    public Dimension getPreferredSize() {
        return TemplateWizard.PREF_DIM;
    }

    private void initComponents() {
        this.namePanel = new JPanel();
        this.jLabel1 = new JLabel();
        this.newObjectName = new JTextField();
        FormListener formListener = new FormListener();
        this.setPreferredSize(new Dimension(560, 520));
        this.setLayout(new BorderLayout(0, 8));
        this.namePanel.setPreferredSize(new Dimension(0, 0));
        this.namePanel.setLayout(new GridBagLayout());
        this.jLabel1.setLabelFor(this.newObjectName);
        ResourceBundle bundle = ResourceBundle.getBundle("org/openide/loaders/Bundle");
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)bundle.getString("CTL_NewObjectName"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 0, 0, 12);
        this.namePanel.add((Component)this.jLabel1, gridBagConstraints);
        this.newObjectName.addFocusListener(formListener);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.namePanel.add((Component)this.newObjectName, gridBagConstraints);
        this.add((Component)this.namePanel, "Center");
    }

    private void packageNameFocusGained(FocusEvent evt) {
    }

    private void packageModelChanged(ListDataEvent evt) {
    }

    private void newObjectNameFocusGained(FocusEvent evt) {
        if (Utilities.getOperatingSystem() == 8 || Utilities.getOperatingSystem() == 256) {
            return;
        }
        this.newObjectName.selectAll();
    }

    private void templatesTreeValueChanged(TreeSelectionEvent evt) {
    }

    @Override
    public void changedUpdate(DocumentEvent p1) {
        if (p1.getDocument() == this.newObjectName.getDocument()) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    if (NewObjectPanel.this.newObjectName.getText().equals("")) {
                        NewObjectPanel.this.setNewObjectName("");
                    }
                    NewObjectPanel.this.fireStateChanged();
                }
            });
        }
    }

    @Override
    public void removeUpdate(DocumentEvent p1) {
        this.changedUpdate(p1);
    }

    @Override
    public void insertUpdate(DocumentEvent p1) {
        this.changedUpdate(p1);
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.newObjectName.requestFocus();
    }

    void addChangeListener(ChangeListener l) {
        if (this.listener != null) {
            throw new IllegalStateException();
        }
        this.listener = l;
    }

    void removeChangeListener(ChangeListener l) {
        this.listener = null;
    }

    private void fireStateChanged() {
        if (this.listener != null) {
            this.listener.stateChanged(new ChangeEvent(this));
        }
    }

    private void setNewObjectName(String name) {
        String n = name;
        if (name == null || name.length() == 0) {
            n = NewObjectPanel.defaultNewObjectName();
        }
        this.newObjectName.getDocument().removeDocumentListener(this);
        this.newObjectName.setText(n);
        this.newObjectName.getDocument().addDocumentListener(this);
        if (name == null || name.length() == 0) {
            this.newObjectName.selectAll();
        }
    }

    public String getNewObjectName() {
        return this.newObjectName.getText();
    }

    static String defaultNewObjectName() {
        return NewObjectPanel.getString("FMT_DefaultNewObjectName");
    }

    private static String getString(String key) {
        return NbBundle.getBundle(NewObjectPanel.class).getString(key);
    }

    private class FormListener
    implements FocusListener {
        FormListener() {
        }

        @Override
        public void focusGained(FocusEvent evt) {
            if (evt.getSource() == NewObjectPanel.this.newObjectName) {
                NewObjectPanel.this.newObjectNameFocusGained(evt);
            }
        }

        @Override
        public void focusLost(FocusEvent evt) {
        }
    }

}

