/*
 * Decompiled with CFR 0_118.
 */
package org.openide.loaders;

import java.util.EventListener;
import org.openide.loaders.OperationEvent;

public interface OperationListener
extends EventListener {
    public void operationPostCreate(OperationEvent var1);

    public void operationCopy(OperationEvent.Copy var1);

    public void operationMove(OperationEvent.Move var1);

    public void operationDelete(OperationEvent var1);

    public void operationRename(OperationEvent.Rename var1);

    public void operationCreateShadow(OperationEvent.Copy var1);

    public void operationCreateFromTemplate(OperationEvent.Copy var1);
}

