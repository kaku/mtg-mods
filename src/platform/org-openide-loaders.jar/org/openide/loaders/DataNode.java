/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileStatusEvent
 *  org.openide.filesystems.FileStatusListener
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$HtmlStatus
 *  org.openide.filesystems.FileSystem$Status
 *  org.openide.filesystems.FileUtil
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.CookieSet
 *  org.openide.nodes.CookieSet$Before
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.nodes.Node$Handle
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.PropertySupport
 *  org.openide.nodes.PropertySupport$ReadOnly
 *  org.openide.nodes.PropertySupport$ReadWrite
 *  org.openide.nodes.PropertySupport$Reflection
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.datatransfer.ExTransferable
 *  org.openide.util.datatransfer.ExTransferable$Single
 */
package org.openide.loaders;

import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.Action;
import org.netbeans.modules.openide.loaders.DataNodeUtils;
import org.netbeans.modules.openide.loaders.UIException;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileStatusEvent;
import org.openide.filesystems.FileStatusListener;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataLoader;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectPool;
import org.openide.loaders.DefaultDataObject;
import org.openide.loaders.FolderList;
import org.openide.loaders.LoaderTransfer;
import org.openide.loaders.MimeFactory;
import org.openide.loaders.MultiDataObject;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.ExTransferable;

public class DataNode
extends AbstractNode {
    static final long serialVersionUID = -7882925922830244768L;
    private DataObject obj;
    private PropL propL;
    private static boolean showFileExtensions = true;
    private static final String PROP_EXTENSION = "extension";
    private static Class defaultLookup;
    private static RequestProcessor.Task refreshNamesIconsTask;
    private static Set<DataNode> refreshNameNodes;
    private static Set<DataNode> refreshIconNodes;
    private static boolean refreshNamesIconsRunning;
    private static final Object refreshNameIconLock;

    public DataNode(DataObject obj, Children ch) {
        this(obj, ch, null);
    }

    public DataNode(DataObject obj, Children ch, Lookup lookup) {
        super(ch, lookup);
        this.obj = obj;
        this.propL = new PropL();
        if (lookup == null) {
            this.setCookieSet(CookieSet.createGeneric((CookieSet.Before)this.propL));
        }
        obj.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.propL, (Object)obj));
        super.setName(obj.getName());
        this.updateDisplayName();
    }

    private void updateDisplayName() {
        FileObject prim = this.obj.getPrimaryFile();
        String newDisplayName = prim.isRoot() ? FileUtil.getFileDisplayName((FileObject)prim) : (showFileExtensions || this.obj instanceof DataFolder || this.obj instanceof DefaultDataObject ? prim.getNameExt() : prim.getName());
        if (this.displayFormat != null) {
            this.setDisplayName(this.displayFormat.format(new Object[]{newDisplayName}));
        } else {
            this.setDisplayName(newDisplayName);
        }
    }

    public DataObject getDataObject() {
        return this.obj;
    }

    public void setName(String name, boolean rename) {
        try {
            if (rename) {
                this.obj.rename(name);
            }
            super.setName(name);
            this.updateDisplayName();
        }
        catch (IOException ex) {
            String msg = Exceptions.findLocalizedMessage((Throwable)ex);
            if (msg == null) {
                msg = NbBundle.getMessage(DataNode.class, (String)"MSG_renameError", (Object)this.getName(), (Object)name);
            }
            IllegalArgumentException e = new IllegalArgumentException(ex);
            Exceptions.attachLocalizedMessage((Throwable)e, (String)msg);
            throw e;
        }
    }

    public void setName(String name) {
        this.setName(name, true);
    }

    public String getDisplayName() {
        String s = super.getDisplayName();
        try {
            s = this.obj.getPrimaryFile().getFileSystem().getStatus().annotateName(s, (Set)new LazyFilesSet());
        }
        catch (FileStateInvalidException e) {
            // empty catch block
        }
        return s;
    }

    public String getHtmlDisplayName() {
        try {
            FileSystem.Status stat = this.obj.getPrimaryFile().getFileSystem().getStatus();
            if (stat instanceof FileSystem.HtmlStatus) {
                FileSystem.HtmlStatus hstat = (FileSystem.HtmlStatus)stat;
                String result = hstat.annotateNameHtml(super.getDisplayName(), (Set)new LazyFilesSet());
                if (!super.getDisplayName().equals(result)) {
                    return result;
                }
            }
        }
        catch (FileStateInvalidException e) {
            // empty catch block
        }
        return super.getHtmlDisplayName();
    }

    private Image getImageFromFactory(int type) {
        MimeFactory fact = (MimeFactory)this.getLookup().lookup(MimeFactory.class);
        return fact != null ? fact.getImage(type) : null;
    }

    public Image getIcon(int type) {
        Image img = this.getImageFromFactory(type);
        if (img == null) {
            img = super.getIcon(type);
        }
        try {
            img = this.obj.getPrimaryFile().getFileSystem().getStatus().annotateIcon(img, type, (Set)new LazyFilesSet());
        }
        catch (FileStateInvalidException e) {
            // empty catch block
        }
        return img;
    }

    public Image getOpenedIcon(int type) {
        Image img = this.getImageFromFactory(type);
        if (img == null) {
            img = super.getOpenedIcon(type);
        }
        try {
            img = this.obj.getPrimaryFile().getFileSystem().getStatus().annotateIcon(img, type, (Set)new LazyFilesSet());
        }
        catch (FileStateInvalidException e) {
            // empty catch block
        }
        return img;
    }

    public HelpCtx getHelpCtx() {
        return this.obj.getHelpCtx();
    }

    public boolean canRename() {
        return this.obj.isRenameAllowed();
    }

    public boolean canDestroy() {
        return this.obj.isDeleteAllowed();
    }

    public void destroy() throws IOException {
        if (this.obj.isDeleteAllowed()) {
            this.obj.delete();
        }
        super.destroy();
    }

    public boolean canCopy() {
        return this.obj.isCopyAllowed();
    }

    public boolean canCut() {
        return this.obj.isMoveAllowed();
    }

    @Deprecated
    protected SystemAction[] createActions() {
        return null;
    }

    public Action[] getActions(boolean context) {
        if (this.systemActions == null) {
            this.systemActions = this.createActions();
        }
        if (this.systemActions != null) {
            return this.systemActions;
        }
        MimeFactory mime = (MimeFactory)this.getLookup().lookup(MimeFactory.class);
        if (mime != null) {
            return mime.getActions();
        }
        return this.obj.getLoader().getSwingActions();
    }

    @Deprecated
    public SystemAction[] getActions() {
        if (this.systemActions == null) {
            this.systemActions = this.createActions();
        }
        if (this.systemActions != null) {
            return this.systemActions;
        }
        return this.obj.getLoader().getActions();
    }

    public Action getPreferredAction() {
        if (this.obj.isTemplate()) {
            return null;
        }
        Action action = super.getPreferredAction();
        if (action != null) {
            return action;
        }
        Action[] arr = this.getActions(false);
        if (arr != null && arr.length > 0) {
            return arr[0];
        }
        return null;
    }

    public <T extends Node.Cookie> T getCookie(Class<T> cl) {
        if (this.ownLookup()) {
            return (T)super.getCookie(cl);
        }
        T c = this.obj.getCookie(cl);
        if (c != null) {
            return c;
        }
        return (T)super.getCookie(cl);
    }

    protected Sheet createSheet() {
        Sheet s = Sheet.createDefault();
        Sheet.Set ss = s.get("properties");
        Node.Property p = DataNode.createNameProperty(this.obj);
        ss.put(p);
        FileObject fo = this.getDataObject().getPrimaryFile();
        if (DataNode.couldBeTemplate(fo) && fo.canWrite()) {
            try {
                p = new PropertySupport.Reflection((Object)this.obj, Boolean.TYPE, "isTemplate", "setTemplate");
                p.setName("template");
                p.setDisplayName(DataObject.getString("PROP_template"));
                p.setShortDescription(DataObject.getString("HINT_template"));
                ss.put(p);
            }
            catch (Exception ex) {
                throw new InternalError();
            }
        }
        if (fo.isData()) {
            if (!this.obj.getPrimaryFile().getNameExt().equals(this.obj.getName())) {
                ss.put((Node.Property)new ExtensionProperty());
            }
            ss.put((Node.Property)new SizeProperty());
            ss.put((Node.Property)new LastModifiedProperty());
        }
        ss.put((Node.Property)new AllFilesProperty());
        return s;
    }

    public Object getValue(String attributeName) {
        if ("slowRename".equals(attributeName)) {
            return Boolean.TRUE;
        }
        return super.getValue(attributeName);
    }

    private static boolean couldBeTemplate(FileObject fo) {
        FileSystem fs;
        try {
            fs = fo.getFileSystem();
        }
        catch (FileStateInvalidException e) {
            return false;
        }
        return fs.isDefault() && fo.getPath().startsWith("Templates/");
    }

    public Transferable clipboardCopy() throws IOException {
        ExTransferable t = ExTransferable.create((Transferable)super.clipboardCopy());
        t.put(LoaderTransfer.transferable(this.getDataObject(), 1));
        this.addExternalFileTransferable(t, this.getDataObject());
        return t;
    }

    public Transferable clipboardCut() throws IOException {
        ExTransferable t = ExTransferable.create((Transferable)super.clipboardCut());
        t.put(LoaderTransfer.transferable(this.getDataObject(), 4));
        this.addExternalFileTransferable(t, this.getDataObject());
        return t;
    }

    private void addExternalFileTransferable(ExTransferable t, DataObject d) {
        FileObject fo = d.getPrimaryFile();
        File file = FileUtil.toFile((FileObject)fo);
        if (null != file) {
            final ArrayList<File> list = new ArrayList<File>(1);
            list.add(file);
            t.put(new ExTransferable.Single(DataFlavor.javaFileListFlavor){

                public Object getData() {
                    return list;
                }
            });
            final String uriList = Utilities.toURI((File)file).toString() + "\r\n";
            t.put(new ExTransferable.Single(this.createUriListFlavor()){

                public Object getData() {
                    return uriList;
                }
            });
        }
    }

    private DataFlavor createUriListFlavor() {
        try {
            return new DataFlavor("text/uri-list;class=java.lang.String");
        }
        catch (ClassNotFoundException ex) {
            throw new AssertionError(ex);
        }
    }

    static Node.Property createNameProperty(final DataObject obj) {
        PropertySupport.ReadWrite<String> p = new PropertySupport.ReadWrite<String>("name", String.class, DataObject.getString("PROP_name"), DataObject.getString("HINT_name")){

            public String getValue() {
                return obj.getName();
            }

            public void setValue(String val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
                if (!this.canWrite()) {
                    throw new IllegalAccessException();
                }
                if (val == null) {
                    throw new IllegalArgumentException();
                }
                try {
                    obj.rename(val);
                }
                catch (IOException ex) {
                    String msg = null;
                    msg = ex.getLocalizedMessage() == null || ex.getLocalizedMessage().equals(ex.getMessage()) ? NbBundle.getMessage(DataNode.class, (String)"MSG_renameError", (Object)obj.getName(), (Object)val) : ex.getLocalizedMessage();
                    UIException.annotateUser(ex, null, msg, null, null);
                    throw new InvocationTargetException(ex);
                }
            }

            public boolean canWrite() {
                return obj.isRenameAllowed();
            }

            public Object getValue(String key) {
                if ("suppressCustomEditor".equals(key)) {
                    return Boolean.TRUE;
                }
                return PropertySupport.ReadWrite.super.getValue(key);
            }
        };
        return p;
    }

    private void updateFilesInCookieSet(Set<FileObject> obj) {
        if (this.ownLookup()) {
            return;
        }
        this.getCookieSet().assign(FileObject.class, (Object[])obj.toArray((T[])new FileObject[0]));
    }

    void fireChange(final PropertyChangeEvent ev) {
        Mutex.EVENT.writeAccess(new Runnable(){

            @Override
            public void run() {
                if ("children".equals(ev.getPropertyName())) {
                    return;
                }
                if ("primaryFile".equals(ev.getPropertyName())) {
                    DataNode.this.propL.updateStatusListener();
                    DataNode.this.setName(DataNode.this.obj.getName(), false);
                    DataNode.this.updateFilesInCookieSet(DataNode.this.obj.files());
                    return;
                }
                if ("files".equals(ev.getPropertyName())) {
                    DataNode.this.updateFilesInCookieSet(DataNode.this.obj.files());
                }
                if ("name".equals(ev.getPropertyName())) {
                    DataNode.this.setName(DataNode.this.obj.getName());
                    DataNode.this.updateDisplayName();
                }
                if ("cookie".equals(ev.getPropertyName())) {
                    DataNode.this.fireCookieChange();
                }
                if ("valid".equals(ev.getPropertyName())) {
                    Object newVal = ev.getNewValue();
                    if (newVal instanceof Boolean && !((Boolean)newVal).booleanValue()) {
                        DataNode.this.fireNodeDestroyed();
                    }
                    return;
                }
                List<String> transmitProperties = Arrays.asList("name", "files", "template");
                if (transmitProperties.contains(ev.getPropertyName())) {
                    DataNode.this.firePropertyChange(ev.getPropertyName(), ev.getOldValue(), ev.getNewValue());
                }
            }
        });
    }

    public Node.Handle getHandle() {
        return new ObjectHandle(this.obj, this.obj.isValid() ? this != this.obj.getNodeDelegate() : true);
    }

    final void fireChangeAccess(boolean icon, boolean name) {
        if (name) {
            this.fireDisplayNameChange(null, null);
        }
        if (icon) {
            this.fireIconChange();
        }
    }

    public static boolean getShowFileExtensions() {
        return showFileExtensions;
    }

    public static void setShowFileExtensions(boolean s) {
        boolean refresh = showFileExtensions != s;
        showFileExtensions = s;
        if (refresh) {
            DataNodeUtils.reqProcessor().post(new Runnable(){

                @Override
                public void run() {
                    Iterator<DataObjectPool.Item> it = DataObjectPool.getPOOL().getActiveDataObjects();
                    while (it.hasNext()) {
                        DataObject obj = it.next().getDataObjectOrNull();
                        if (obj == null || !(obj.getNodeDelegate() instanceof DataNode)) continue;
                        ((DataNode)obj.getNodeDelegate()).updateDisplayName();
                    }
                }
            }, 300, 1);
        }
    }

    private boolean ownLookup() {
        if (defaultLookup == null) {
            try {
                defaultLookup = Class.forName("org.openide.nodes.NodeLookup", false, Node.class.getClassLoader());
            }
            catch (ClassNotFoundException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                return false;
            }
        }
        return !defaultLookup.isInstance((Object)this.getLookup());
    }

    static {
        refreshNamesIconsTask = null;
        refreshNameNodes = null;
        refreshIconNodes = null;
        refreshNamesIconsRunning = false;
        refreshNameIconLock = "DataNode.refreshNameIconLock";
    }

    private class LazyFilesSet
    implements Set<FileObject> {
        private Set<FileObject> obj_files;

        private LazyFilesSet() {
        }

        private synchronized void lazyInitialization() {
            this.obj_files = DataNode.this.obj.files();
        }

        @Override
        public boolean add(FileObject o) {
            this.lazyInitialization();
            return this.obj_files.add(o);
        }

        @Override
        public boolean addAll(Collection<? extends FileObject> c) {
            this.lazyInitialization();
            return this.obj_files.addAll(c);
        }

        @Override
        public void clear() {
            this.lazyInitialization();
            this.obj_files.clear();
        }

        @Override
        public boolean contains(Object o) {
            this.lazyInitialization();
            return this.obj_files.contains(o);
        }

        @Override
        public boolean containsAll(Collection c) {
            this.lazyInitialization();
            return this.obj_files.containsAll(c);
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public Iterator<FileObject> iterator() {
            return new FilesIterator();
        }

        @Override
        public boolean remove(Object o) {
            this.lazyInitialization();
            return this.obj_files.remove(o);
        }

        @Override
        public boolean removeAll(Collection c) {
            this.lazyInitialization();
            return this.obj_files.removeAll(c);
        }

        @Override
        public boolean retainAll(Collection c) {
            this.lazyInitialization();
            return this.obj_files.retainAll(c);
        }

        @Override
        public int size() {
            this.lazyInitialization();
            return this.obj_files.size();
        }

        @Override
        public Object[] toArray() {
            this.lazyInitialization();
            return this.obj_files.toArray();
        }

        @Override
        public <FileObject> FileObject[] toArray(FileObject[] a) {
            this.lazyInitialization();
            return this.obj_files.toArray(a);
        }

        @Override
        public boolean equals(Object obj) {
            this.lazyInitialization();
            return obj instanceof Set && this.obj_files.equals(obj);
        }

        public String toString() {
            this.lazyInitialization();
            return this.obj_files.toString();
        }

        @Override
        public int hashCode() {
            this.lazyInitialization();
            return this.obj_files.hashCode();
        }

        private final class FilesIterator
        implements Iterator<FileObject> {
            private boolean first;
            private Iterator<FileObject> itDelegate;

            FilesIterator() {
                this.first = true;
                this.itDelegate = null;
            }

            @Override
            public boolean hasNext() {
                return this.first ? true : this.getIteratorDelegate().hasNext();
            }

            @Override
            public FileObject next() {
                if (this.first) {
                    this.first = false;
                    return DataNode.this.obj.getPrimaryFile();
                }
                return this.getIteratorDelegate().next();
            }

            @Override
            public void remove() {
                this.getIteratorDelegate().remove();
            }

            private Iterator<FileObject> getIteratorDelegate() {
                if (this.itDelegate == null) {
                    LazyFilesSet.this.lazyInitialization();
                    this.itDelegate = LazyFilesSet.this.obj_files.iterator();
                    this.itDelegate.next();
                }
                return this.itDelegate;
            }
        }

    }

    private static class ObjectHandle
    implements Node.Handle {
        private FileObject obj;
        private boolean clone;
        static final long serialVersionUID = 6616060729084681518L;

        public ObjectHandle(DataObject obj, boolean clone) {
            this.obj = obj.getPrimaryFile();
            this.clone = clone;
        }

        public Node getNode() throws IOException {
            if (this.obj == null) {
                throw new IOException("File could not be restored");
            }
            Node n = DataObject.find(this.obj).getNodeDelegate();
            return this.clone ? n.cloneNode() : n;
        }
    }

    private static class NamesUpdater
    implements Runnable {
        private NamesUpdater() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            DataNode[] _refreshNameNodes;
            int i;
            DataNode[] _refreshIconNodes;
            Object object = refreshNameIconLock;
            synchronized (object) {
                if (refreshNameNodes != null) {
                    _refreshNameNodes = refreshNameNodes.toArray((T[])new DataNode[refreshNameNodes.size()]);
                    refreshNameNodes.clear();
                } else {
                    _refreshNameNodes = new DataNode[]{};
                }
                if (refreshIconNodes != null) {
                    _refreshIconNodes = refreshIconNodes.toArray((T[])new DataNode[refreshIconNodes.size()]);
                    refreshIconNodes.clear();
                } else {
                    _refreshIconNodes = new DataNode[]{};
                }
                refreshNamesIconsRunning = false;
            }
            for (i = 0; i < _refreshNameNodes.length; ++i) {
                _refreshNameNodes[i].fireChangeAccess(false, true);
            }
            for (i = 0; i < _refreshIconNodes.length; ++i) {
                _refreshIconNodes[i].fireChangeAccess(true, false);
            }
        }
    }

    private class PropL
    implements PropertyChangeListener,
    FileStatusListener,
    CookieSet.Before {
        private FileStatusListener weakL;
        private FileSystem previous;

        public PropL() {
            this.updateStatusListener();
        }

        @Override
        public void propertyChange(PropertyChangeEvent ev) {
            DataNode.this.fireChange(ev);
        }

        private void updateStatusListener() {
            if (this.previous != null) {
                this.previous.removeFileStatusListener(this.weakL);
            }
            try {
                this.previous = DataNode.this.obj.getPrimaryFile().getFileSystem();
                if (this.weakL == null) {
                    this.weakL = FileUtil.weakFileStatusListener((FileStatusListener)this, (Object)null);
                }
                this.previous.addFileStatusListener(this.weakL);
            }
            catch (FileStateInvalidException ex) {
                this.previous = null;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void annotationChanged(FileStatusEvent ev) {
            boolean thisChanged;
            if (DataNode.this.getDataObject() instanceof MultiDataObject) {
                MultiDataObject multi = (MultiDataObject)DataNode.this.getDataObject();
                thisChanged = ev.hasChanged(multi.getPrimaryFile());
                if (!thisChanged) {
                    for (FileObject fo : multi.getSecondary().keySet()) {
                        if (!ev.hasChanged(fo)) continue;
                        thisChanged = true;
                        break;
                    }
                }
            } else {
                thisChanged = false;
                for (FileObject fo : DataNode.this.obj.files()) {
                    if (!ev.hasChanged(fo)) continue;
                    thisChanged = true;
                    break;
                }
            }
            if (thisChanged) {
                Object it = refreshNameIconLock;
                synchronized (it) {
                    boolean post = false;
                    if (ev.isNameChange()) {
                        if (refreshNameNodes == null) {
                            refreshNameNodes = new HashSet();
                        }
                        post |= refreshNameNodes.add(DataNode.this);
                    }
                    if (ev.isIconChange()) {
                        if (refreshIconNodes == null) {
                            refreshIconNodes = new HashSet();
                        }
                        post |= refreshIconNodes.add(DataNode.this);
                    }
                    if (post && !refreshNamesIconsRunning) {
                        refreshNamesIconsRunning = true;
                        if (refreshNamesIconsTask == null) {
                            refreshNamesIconsTask = DataNodeUtils.reqProcessor().post((Runnable)new NamesUpdater());
                        } else {
                            refreshNamesIconsTask.schedule(0);
                        }
                    }
                }
            }
        }

        public void beforeLookup(Class<?> clazz) {
            if (clazz.isAssignableFrom(FileObject.class)) {
                DataNode.this.updateFilesInCookieSet(DataNode.this.obj.files());
            }
        }
    }

    private final class ExtensionProperty
    extends PropertySupport.ReadWrite<String> {
        public ExtensionProperty() {
            super("extension", String.class, DataObject.getString("PROP_extension"), DataObject.getString("HINT_extension"));
        }

        public boolean canWrite() {
            return DataNode.this.obj.isRenameAllowed();
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            return DataNode.this.obj.getPrimaryFile().getExt();
        }

        public void setValue(final String newExt) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            if (!this.getValue().equals(newExt)) {
                if (DataNode.this.obj.isModified()) {
                    String message = DataObject.getString("ERROR_extension");
                    DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)message));
                    return;
                }
                DataNodeUtils.reqProcessor().post(new Runnable(){

                    @Override
                    public void run() {
                        ExtensionProperty.this.setNewExt(newExt);
                    }
                });
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void setNewExt(String newExt) {
            try {
                FileObject prim = DataNode.this.obj.getPrimaryFile();
                FileLock lock = prim.lock();
                try {
                    prim.rename(lock, prim.getName(), newExt);
                }
                finally {
                    lock.releaseLock();
                }
                DataNode.this.obj.dispose();
                if (DataNode.this.obj instanceof MultiDataObject) {
                    FolderList folderList = FolderList.find(prim.getParent(), true);
                    folderList.getChildren();
                    folderList.refresh();
                }
            }
            catch (IOException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
            }
        }

    }

    private final class LastModifiedProperty
    extends PropertySupport.ReadOnly<Date> {
        public LastModifiedProperty() {
            super("lastModified", Date.class, DataObject.getString("PROP_lastModified"), DataObject.getString("HINT_lastModified"));
        }

        public Date getValue() {
            return DataNode.this.getDataObject().getPrimaryFile().lastModified();
        }
    }

    private final class SizeProperty
    extends PropertySupport.ReadOnly<Long> {
        public SizeProperty() {
            super("size", Long.TYPE, DataObject.getString("PROP_size"), DataObject.getString("HINT_size"));
        }

        public Long getValue() {
            return new Long(DataNode.this.getDataObject().getPrimaryFile().getSize());
        }
    }

    private final class AllFilesProperty
    extends PropertySupport.ReadOnly<String[]> {
        public AllFilesProperty() {
            super("files", String[].class, DataObject.getString("PROP_files"), DataObject.getString("HINT_files"));
        }

        public String[] getValue() {
            Set<FileObject> files = DataNode.this.obj.files();
            FileObject primary = DataNode.this.obj.getPrimaryFile();
            Object[] res = new String[files.size()];
            assert (files.contains((Object)primary));
            int i = 1;
            Iterator<FileObject> it = files.iterator();
            while (it.hasNext()) {
                FileObject next;
                res[(next = it.next()) == primary ? 0 : i++] = this.name(next);
            }
            Arrays.sort(res, 1, res.length);
            return res;
        }

        private String name(FileObject fo) {
            return FileUtil.getFileDisplayName((FileObject)fo);
        }
    }

}

