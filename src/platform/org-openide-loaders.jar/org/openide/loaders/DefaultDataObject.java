/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.cookies.CloseCookie
 *  org.openide.cookies.EditCookie
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.EditorCookie$Observable
 *  org.openide.cookies.OpenCookie
 *  org.openide.cookies.PrintCookie
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.Children
 *  org.openide.nodes.CookieSet
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.openide.loaders;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.cookies.CloseCookie;
import org.openide.cookies.EditCookie;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.OpenCookie;
import org.openide.cookies.PrintCookie;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.DataObjectPool;
import org.openide.loaders.DefaultES;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.Children;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

final class DefaultDataObject
extends MultiDataObject
implements OpenCookie {
    static final long serialVersionUID = -4936309935667095746L;
    private DefaultES support;
    private boolean cookieSetFixed = false;

    DefaultDataObject(FileObject fo, MultiFileLoader loader) throws DataObjectExistsException {
        super(fo, loader);
    }

    @Override
    protected int associateLookup() {
        return 1;
    }

    @Override
    protected Node createNodeDelegate() {
        DataNode dn = new DataNode(this, Children.LEAF);
        dn.setShortDescription(NbBundle.getMessage(DefaultDataObject.class, (String)"HINT_DefaultDataObject"));
        return dn;
    }

    @Override
    public String getName() {
        return this.getPrimaryFile().getNameExt();
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected FileObject handleRename(String name) throws IOException {
        FileLock lock = this.getPrimaryEntry().takeLock();
        int pos = name.lastIndexOf(46);
        try {
            if (pos < 0) {
                this.getPrimaryFile().rename(lock, name, null);
            } else if (pos == 0) {
                this.getPrimaryFile().rename(lock, name, null);
            } else if (!name.equals(this.getPrimaryFile().getNameExt())) {
                this.getPrimaryFile().rename(lock, name.substring(0, pos), name.substring(pos + 1, name.length()));
                DataObjectPool.getPOOL().revalidate(new HashSet<FileObject>(Collections.singleton(this.getPrimaryFile())));
            }
        }
        finally {
            lock.releaseLock();
        }
        return this.getPrimaryFile();
    }

    @Override
    protected DataObject handleCreateFromTemplate(DataFolder df, String name) throws IOException {
        if (name != null && name.endsWith("." + this.getPrimaryFile().getExt())) {
            name = name.substring(0, name.lastIndexOf("." + this.getPrimaryFile().getExt()));
        }
        return super.handleCreateFromTemplate(df, name);
    }

    @Override
    protected DataObject handleCopyRename(DataFolder df, String name, String ext) throws IOException {
        FileObject fo = this.getPrimaryEntry().copyRename(df.getPrimaryFile(), name, ext);
        return DataObject.find(fo);
    }

    public void open() {
        EditorCookie ic = this.getCookie(EditorCookie.class);
        if (ic != null) {
            ic.open();
        } else {
            ArrayList<Object> options = new ArrayList<Object>();
            options.add(NotifyDescriptor.OK_OPTION);
            options.add(NotifyDescriptor.CANCEL_OPTION);
            NotifyDescriptor nd = new NotifyDescriptor((Object)NbBundle.getMessage(DefaultDataObject.class, (String)"MSG_BinaryFileQuestion"), NbBundle.getMessage(DefaultDataObject.class, (String)"MSG_BinaryFileWarning"), -1, 3, options.toArray(), (Object)null);
            Object ret = DialogDisplayer.getDefault().notify(nd);
            if (ret != NotifyDescriptor.OK_OPTION) {
                return;
            }
            EditorCookie c = this.getCookie(EditorCookie.class, true);
            c.open();
        }
    }

    @Override
    public Lookup getLookup() {
        return this.getCookieSet().getLookup();
    }

    @Override
    public <T extends Node.Cookie> T getCookie(Class<T> c) {
        return this.getCookie(c, false);
    }

    @Override
    final void checkCookieSet(Class<?> c) {
        if (Node.Cookie.class.isAssignableFrom(c) && !this.cookieSetFixed) {
            Class<Node.Cookie> cookie = c.asSubclass(Node.Cookie.class);
            this.fixCookieSet(cookie, false);
        }
    }

    final <T extends Node.Cookie> T getCookie(Class<T> c, boolean force) {
        if (c == OpenCookie.class) {
            return (T)((Node.Cookie)c.cast(this));
        }
        T cook = super.getCookie(c);
        if (cook != null) {
            return cook;
        }
        this.fixCookieSet(c, force);
        return (T)this.getCookieSet().getCookie(c);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fixCookieSet(Class<?> c, boolean force) {
        if (this.cookieSetFixed && !force || this.support != null) {
            return;
        }
        if (c.isAssignableFrom(EditCookie.class) || c.isAssignableFrom(EditorCookie.Observable.class) || c.isAssignableFrom(PrintCookie.class) || c.isAssignableFrom(CloseCookie.class) || c == DefaultES.class) {
            try {
                if (!force) {
                    byte[] arr = new byte[2048];
                    InputStream is = this.getPrimaryFile().getInputStream();
                    try {
                        int len = is.read(arr);
                        for (int i = 0; i < len; ++i) {
                            if (arr[i] < 0 || arr[i] > 31 || arr[i] == 10 || arr[i] == 13 || arr[i] == 9 || arr[i] == 12) continue;
                            this.cookieSetFixed = true;
                            return;
                        }
                    }
                    finally {
                        is.close();
                    }
                }
                this.support = new DefaultES(this, this.getPrimaryEntry(), this.getCookieSet());
                this.getCookieSet().assign(DefaultES.class, (Object[])new DefaultES[]{this.support});
                this.cookieSetFixed = true;
            }
            catch (IOException ex) {
                LOG.log(Level.INFO, "Cannot read " + this.getPrimaryEntry(), ex);
            }
        }
    }
}

