/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.openide.loaders;

import java.util.EventObject;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataLoaderPool;
import org.openide.loaders.DataObject;

public class OperationEvent
extends EventObject {
    static final int COPY = 1;
    static final int MOVE = 2;
    static final int DELETE = 3;
    static final int RENAME = 4;
    static final int SHADOW = 5;
    static final int TEMPL = 6;
    static final int CREATE = 7;
    private DataObject obj;
    private static final DataLoaderPool pl = DataLoaderPool.getDefault();
    static final long serialVersionUID = -3884037468317843808L;

    OperationEvent(DataObject obj) {
        super(pl);
        this.obj = obj;
    }

    public DataObject getObject() {
        return this.obj;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(super.toString());
        sb.append(":");
        sb.append(" for ");
        sb.append(this.obj);
        this.writeDebug(sb);
        return sb.toString();
    }

    void writeDebug(StringBuffer sb) {
    }

    public static final class Copy
    extends OperationEvent {
        private DataObject orig;
        static final long serialVersionUID = -2768331988864546290L;

        Copy(DataObject obj, DataObject orig) {
            super(obj);
            this.orig = orig;
        }

        public DataObject getOriginalDataObject() {
            return this.orig;
        }

        @Override
        final void writeDebug(StringBuffer sb) {
            sb.append(" originalobj: ");
            sb.append(this.orig);
        }
    }

    public static final class Move
    extends OperationEvent {
        private FileObject file;
        static final long serialVersionUID = -7753279728025703632L;

        Move(DataObject obj, FileObject file) {
            super(obj);
            this.file = file;
        }

        public FileObject getOriginalPrimaryFile() {
            return this.file;
        }

        @Override
        final void writeDebug(StringBuffer sb) {
            sb.append(" originalfile: ");
            sb.append((Object)this.file);
        }
    }

    public static final class Rename
    extends OperationEvent {
        private String name;
        static final long serialVersionUID = -1584168503454848519L;

        Rename(DataObject obj, String name) {
            super(obj);
            this.name = name;
        }

        public String getOriginalName() {
            return this.name;
        }

        @Override
        final void writeDebug(StringBuffer sb) {
            sb.append(" originalname: ");
            sb.append(this.name);
        }
    }

}

