/*
 * Decompiled with CFR 0_118.
 */
package org.openide.loaders;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

final class XMLEntityResolverChain
implements EntityResolver {
    private final List<EntityResolver> resolverChain = new ArrayList<EntityResolver>(3);

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean addEntityResolver(EntityResolver resolver) {
        if (resolver == null) {
            throw new IllegalArgumentException();
        }
        List<EntityResolver> list = this.resolverChain;
        synchronized (list) {
            if (this.resolverChain.contains(resolver)) {
                return false;
            }
            return this.resolverChain.add(resolver);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public EntityResolver removeEntityResolver(EntityResolver resolver) {
        if (resolver == null) {
            throw new IllegalArgumentException();
        }
        List<EntityResolver> list = this.resolverChain;
        synchronized (list) {
            int index = this.resolverChain.indexOf(resolver);
            if (index < 0) {
                return null;
            }
            return this.resolverChain.remove(index);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public InputSource resolveEntity(String publicID, String systemID) throws SAXException, IOException {
        SAXException lsex = null;
        IOException lioex = null;
        List<EntityResolver> list = this.resolverChain;
        synchronized (list) {
            for (EntityResolver resolver : this.resolverChain) {
                try {
                    InputSource test = resolver.resolveEntity(publicID, systemID);
                    if (test == null) continue;
                    return test;
                }
                catch (SAXException sex) {
                    lsex = sex;
                    continue;
                }
                catch (IOException ioex) {
                    lioex = ioex;
                    continue;
                }
            }
            if (lioex != null) {
                throw lioex;
            }
            if (lsex != null) {
                throw lsex;
            }
            return null;
        }
    }
}

