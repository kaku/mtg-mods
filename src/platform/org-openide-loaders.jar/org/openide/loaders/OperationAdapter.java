/*
 * Decompiled with CFR 0_118.
 */
package org.openide.loaders;

import org.openide.loaders.OperationEvent;
import org.openide.loaders.OperationListener;

public class OperationAdapter
implements OperationListener {
    @Override
    public void operationPostCreate(OperationEvent ev) {
    }

    @Override
    public void operationCopy(OperationEvent.Copy ev) {
    }

    @Override
    public void operationMove(OperationEvent.Move ev) {
    }

    @Override
    public void operationDelete(OperationEvent ev) {
    }

    @Override
    public void operationRename(OperationEvent.Rename ev) {
    }

    @Override
    public void operationCreateShadow(OperationEvent.Copy ev) {
    }

    @Override
    public void operationCreateFromTemplate(OperationEvent.Copy ev) {
    }
}

