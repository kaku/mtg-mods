/*
 * Decompiled with CFR 0_118.
 */
package org.openide.loaders;

import java.io.Serializable;
import org.openide.loaders.DataFilter;
import org.openide.loaders.DataObject;

class DataFilterAll
implements DataFilter,
Serializable {
    static final long serialVersionUID = -760448687111430451L;

    DataFilterAll() {
    }

    @Override
    public boolean acceptDataObject(DataObject obj) {
        return true;
    }

    public Object writeReplace() {
        return new Replace();
    }

    static class Replace
    implements Serializable {
        static final long serialVersionUID = 3204495526835476127L;

        Replace() {
        }

        public Object readResolve() {
            return DataFilter.ALL;
        }
    }

}

