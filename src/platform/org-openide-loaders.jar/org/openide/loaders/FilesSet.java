/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 */
package org.openide.loaders;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.loaders.MultiDataObject;

final class FilesSet
implements Set<FileObject> {
    private final MultiDataObject mymdo;
    private boolean lazyWorkDone;
    private FileObject primaryFile;
    private Map<FileObject, MultiDataObject.Entry> secondary;
    private TreeSet<FileObject> delegate;

    public FilesSet(MultiDataObject mdo) {
        this.mymdo = mdo;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void doLazyWork() {
        FilesSet filesSet = this;
        synchronized (filesSet) {
            if (!this.lazyWorkDone) {
                this.lazyWorkDone = true;
                Object object = this.mymdo.synchObjectSecondary();
                synchronized (object) {
                    Set<MultiDataObject.Entry> res = this.mymdo.secondaryEntries(false);
                    assert (res == null);
                    this.primaryFile = this.mymdo.getPrimaryFile();
                    this.secondary = this.mymdo.getSecondary();
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Set<FileObject> getDelegate() {
        this.doLazyWork();
        Map<FileObject, MultiDataObject.Entry> map = this.secondary;
        synchronized (map) {
            if (this.delegate == null) {
                this.delegate = new TreeSet(new FilesComparator());
                this.delegate.add(this.primaryFile);
                this.delegate.addAll(this.secondary.keySet());
            }
        }
        return this.delegate;
    }

    @Override
    public boolean add(FileObject obj) {
        return this.getDelegate().add(obj);
    }

    @Override
    public boolean addAll(Collection<? extends FileObject> collection) {
        return this.getDelegate().addAll(collection);
    }

    @Override
    public void clear() {
        this.getDelegate().clear();
    }

    @Override
    public boolean contains(Object obj) {
        return this.getDelegate().contains(obj);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return this.getDelegate().containsAll(collection);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean isEmpty() {
        this.doLazyWork();
        Map<FileObject, MultiDataObject.Entry> map = this.secondary;
        synchronized (map) {
            return this.delegate == null ? false : this.delegate.isEmpty();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Iterator<FileObject> iterator() {
        this.doLazyWork();
        Map<FileObject, MultiDataObject.Entry> map = this.secondary;
        synchronized (map) {
            return this.delegate == null ? new FilesIterator() : this.delegate.iterator();
        }
    }

    @Override
    public boolean remove(Object obj) {
        return this.getDelegate().remove(obj);
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return this.getDelegate().removeAll(collection);
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return this.getDelegate().retainAll(collection);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int size() {
        this.doLazyWork();
        Map<FileObject, MultiDataObject.Entry> map = this.secondary;
        synchronized (map) {
            return this.delegate == null ? this.secondary.size() + 1 : this.delegate.size();
        }
    }

    @Override
    public Object[] toArray() {
        return this.getDelegate().toArray();
    }

    @Override
    public <T> T[] toArray(T[] obj) {
        return this.getDelegate().toArray(obj);
    }

    @Override
    public boolean equals(Object obj) {
        return this.getDelegate().equals(obj);
    }

    public String toString() {
        return this.getDelegate().toString();
    }

    @Override
    public int hashCode() {
        return this.getDelegate().hashCode();
    }

    private final class FilesComparator
    implements Comparator<FileObject> {
        FilesComparator() {
        }

        @Override
        public int compare(FileObject f1, FileObject f2) {
            if (f1 == f2) {
                return 0;
            }
            if (f1 == FilesSet.this.primaryFile) {
                return -1;
            }
            if (f2 == FilesSet.this.primaryFile) {
                return 1;
            }
            int res = f1.getNameExt().compareTo(f2.getNameExt());
            if (res == 0) {
                try {
                    if (f1.getFileSystem() == f2.getFileSystem()) {
                        return 0;
                    }
                    return f1.getFileSystem().getSystemName().compareTo(f2.getFileSystem().getSystemName());
                }
                catch (FileStateInvalidException fsie) {
                    return 0;
                }
            }
            return res;
        }
    }

    private final class FilesIterator
    implements Iterator<FileObject> {
        private boolean first;
        private Iterator<FileObject> itDelegate;

        FilesIterator() {
            this.first = true;
            this.itDelegate = null;
        }

        @Override
        public boolean hasNext() {
            return this.first ? true : this.getIteratorDelegate().hasNext();
        }

        @Override
        public FileObject next() {
            if (this.first) {
                this.first = false;
                return FilesSet.this.primaryFile;
            }
            return this.getIteratorDelegate().next();
        }

        @Override
        public void remove() {
            this.getIteratorDelegate().remove();
        }

        private Iterator<FileObject> getIteratorDelegate() {
            if (this.itDelegate == null) {
                this.itDelegate = FilesSet.this.getDelegate().iterator();
                this.itDelegate.next();
            }
            return this.itDelegate;
        }
    }

}

