/*
 * Decompiled with CFR 0_118.
 */
package org.openide.loaders;

import java.util.EventObject;
import org.openide.loaders.BrokenDataShadow;
import org.openide.loaders.DataLoaderPool;
import org.openide.loaders.DataShadow;
import org.openide.loaders.OperationEvent;
import org.openide.loaders.OperationListener;

class ShadowChangeAdapter
implements OperationListener {
    ShadowChangeAdapter() {
        DataLoaderPool.getDefault().addOperationListener(this);
    }

    static void checkBrokenDataShadows(EventObject ev) {
        BrokenDataShadow.checkValidity(ev);
    }

    static void checkDataShadows(EventObject ev) {
        DataShadow.checkValidity(ev);
    }

    @Override
    public void operationPostCreate(OperationEvent ev) {
        ShadowChangeAdapter.checkBrokenDataShadows(ev);
    }

    @Override
    public void operationCopy(OperationEvent.Copy ev) {
    }

    @Override
    public void operationMove(OperationEvent.Move ev) {
        ShadowChangeAdapter.checkDataShadows(ev);
        ShadowChangeAdapter.checkBrokenDataShadows(ev);
    }

    @Override
    public void operationDelete(OperationEvent ev) {
        ShadowChangeAdapter.checkDataShadows(ev);
    }

    @Override
    public void operationRename(OperationEvent.Rename ev) {
        ShadowChangeAdapter.checkDataShadows(ev);
        ShadowChangeAdapter.checkBrokenDataShadows(ev);
    }

    @Override
    public void operationCreateShadow(OperationEvent.Copy ev) {
    }

    @Override
    public void operationCreateFromTemplate(OperationEvent.Copy ev) {
        ShadowChangeAdapter.checkBrokenDataShadows(ev);
    }
}

