/*
 * Decompiled with CFR 0_118.
 */
package org.openide.loaders;

import java.io.IOException;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectPool;

public class DataObjectExistsException
extends IOException {
    static final long serialVersionUID = 4719319528535266801L;
    private DataObject obj;

    public DataObjectExistsException(DataObject obj) {
        this.obj = obj;
    }

    public DataObject getDataObject() {
        DataObjectPool.getPOOL().waitNotified(this.obj);
        return this.obj;
    }

    @Override
    public Throwable fillInStackTrace() {
        return this;
    }
}

