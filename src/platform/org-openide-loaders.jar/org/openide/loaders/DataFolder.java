/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$InputLine
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.SaveCookie
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$Status
 *  org.openide.filesystems.FileUtil
 *  org.openide.nodes.Children
 *  org.openide.nodes.CookieSet
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.Index
 *  org.openide.nodes.Index$Support
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.nodes.Node$Handle
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeMemberEvent
 *  org.openide.nodes.NodeOp
 *  org.openide.nodes.NodeReorderEvent
 *  org.openide.nodes.NodeTransfer
 *  org.openide.nodes.PropertySupport
 *  org.openide.nodes.PropertySupport$ReadWrite
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Enumerations
 *  org.openide.util.Enumerations$Processor
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.NbCollections
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.datatransfer.ExClipboard
 *  org.openide.util.datatransfer.ExTransferable
 *  org.openide.util.datatransfer.ExTransferable$Multi
 *  org.openide.util.datatransfer.NewType
 *  org.openide.util.datatransfer.PasteType
 */
package org.openide.loaders;

import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.cookies.InstanceCookie;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFilter;
import org.openide.loaders.DataLoader;
import org.openide.loaders.DataLoaderPool;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.loaders.DataObjectPool;
import org.openide.loaders.DataShadow;
import org.openide.loaders.DataTransferSupport;
import org.openide.loaders.FolderChildren;
import org.openide.loaders.FolderComparator;
import org.openide.loaders.FolderList;
import org.openide.loaders.FolderListListener;
import org.openide.loaders.FolderOrder;
import org.openide.loaders.FolderRenameHandler;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.loaders.OperationEvent;
import org.openide.loaders.SortModeEditor;
import org.openide.nodes.Children;
import org.openide.nodes.CookieSet;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Index;
import org.openide.nodes.Node;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeOp;
import org.openide.nodes.NodeReorderEvent;
import org.openide.nodes.NodeTransfer;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Enumerations;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.NbCollections;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.datatransfer.ExClipboard;
import org.openide.util.datatransfer.ExTransferable;
import org.openide.util.datatransfer.NewType;
import org.openide.util.datatransfer.PasteType;

public class DataFolder
extends MultiDataObject
implements DataObject.Container {
    static final long serialVersionUID = -8244904281845488751L;
    public static final String PROP_CHILDREN = "children";
    public static final String PROP_SORT_MODE = "sortMode";
    static final String EA_SORT_MODE = "OpenIDE-Folder-SortMode";
    static final String EA_ORDER = "OpenIDE-Folder-Order";
    public static final String PROP_ORDER = "order";
    public static final String SET_SORTING = "sorting";
    private static final String FOLDER_ICON_BASE = "org/openide/loaders/defaultFolder.gif";
    private static final String ROOT_SHADOW_NAME = "Root";
    private static DataFlavor uriListDataFlavor;
    private FolderList list;
    private PropertyChangeListener pcl;
    private DataTransferSupport dataTransferSupport;
    private static final ThreadLocal<boolean[]> KEEP_ALIVE;
    private static Image[] IMGS;

    @Deprecated
    public DataFolder(FileObject fo) throws DataObjectExistsException, IllegalArgumentException {
        this(fo, DataLoaderPool.getFolderLoader());
    }

    protected DataFolder(FileObject fo, MultiFileLoader loader) throws DataObjectExistsException, IllegalArgumentException {
        this(fo, loader, true);
    }

    @Deprecated
    protected DataFolder(FileObject fo, DataLoader loader) throws DataObjectExistsException, IllegalArgumentException {
        super(fo, loader);
        this.dataTransferSupport = new Paste();
        this.init(fo, true);
    }

    private DataFolder(FileObject fo, MultiFileLoader loader, boolean attach) throws DataObjectExistsException, IllegalArgumentException {
        super(fo, loader);
        this.dataTransferSupport = new Paste();
        this.init(fo, attach);
    }

    private void init(FileObject fo, boolean attach) throws IllegalArgumentException {
        if (!fo.isFolder()) {
            throw new IllegalArgumentException("Not folder: " + (Object)fo);
        }
        this.list = this.reassignList(fo, attach);
    }

    private FolderList reassignList(FileObject fo, boolean attach) {
        FolderList folderList = FolderList.find(fo, true);
        if (attach) {
            this.pcl = new ListPCL();
            folderList.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.pcl, (Object)folderList));
        }
        return folderList;
    }

    public static DataFolder create(DataFolder folder, String name) throws IOException {
        StringTokenizer tok = new StringTokenizer(name, "/");
        while (tok.hasMoreTokens()) {
            String piece = tok.nextToken();
            if (DataFolder.confirmName(piece)) continue;
            throw new IOException(NbBundle.getMessage(DataFolder.class, (String)"EXC_WrongName", (Object)piece));
        }
        return DataFolder.findFolder(FileUtil.createFolder((FileObject)folder.getPrimaryFile(), (String)name));
    }

    public final synchronized void setSortMode(SortMode mode) throws IOException {
        SortMode old = this.getOrder().getSortMode();
        this.getOrder().setSortMode(mode);
        this.firePropertyChange("sortMode", old, this.getOrder().getSortMode());
    }

    public final SortMode getSortMode() {
        return this.getOrder().getSortMode();
    }

    public final synchronized void setOrder(DataObject[] arr) throws IOException {
        this.getOrder().setOrder(arr);
        this.firePropertyChange("order", null, null);
    }

    private FolderOrder getOrder() {
        return FolderOrder.findFor(this.getPrimaryFile());
    }

    @Override
    public Lookup getLookup() {
        if (DataFolder.class == this.getClass()) {
            return this.getCookieSet().getLookup();
        }
        return super.getLookup();
    }

    @Override
    public String getName() {
        return this.getPrimaryFile().getNameExt();
    }

    @Override
    public DataObject[] getChildren() {
        return this.list.getChildren();
    }

    final List<DataObject> getChildrenList() {
        return this.list.getChildrenList();
    }

    final RequestProcessor.Task computeChildrenList(FolderListListener l) {
        return this.list.computeChildrenList(l);
    }

    public Enumeration<DataObject> children() {
        return Collections.enumeration(this.getChildrenList());
    }

    public Enumeration<DataObject> children(boolean rec) {
        class Processor
        implements Enumerations.Processor<DataObject, DataObject> {
            final /* synthetic */ boolean val$rec;

            Processor() {
                this.val$rec = n;
            }

            public DataObject process(DataObject dataObj, Collection<DataObject> toAdd) {
                if (this.val$rec && dataObj instanceof DataFolder) {
                    toAdd.addAll(Arrays.asList(((DataFolder)dataObj).getChildren()));
                }
                return dataObj;
            }
        }
        if (!rec) {
            return this.children();
        }
        Enumeration en = Enumerations.queue((Enumeration)Enumerations.array((Object[])this.getChildren()), (Enumerations.Processor)new Processor(this, rec));
        return en;
    }

    @Override
    protected synchronized Node createNodeDelegate() {
        return new FolderNode(this);
    }

    @Override
    Node getClonedNodeDelegate(DataFilter filter) {
        Node n = this.getNodeDelegate();
        Children c = n.getChildren();
        if (c.getClass() == FolderChildren.class) {
            DataFilter f = ((FolderChildren)c).getFilter();
            if (f == DataFilter.ALL) {
                return new ClonedFilter(n, filter);
            }
            if (filter != DataFilter.ALL && filter != f) {
                return new ClonedFilter(n, DataFolder.filterCompose(f, filter));
            }
            return n.cloneNode();
        }
        return n.cloneNode();
    }

    private static DataFilter filterCompose(final DataFilter f1, final DataFilter f2) {
        if (f1.equals(f2)) {
            return f1;
        }
        return new DataFilter(){

            @Override
            public boolean acceptDataObject(DataObject obj) {
                return f1.acceptDataObject(obj) && f2.acceptDataObject(obj);
            }
        };
    }

    static Children createNodeChildren(DataFolder df, DataFilter filter) {
        return new FolderChildren(df, filter);
    }

    public Children createNodeChildren(DataFilter filter) {
        return DataFolder.createNodeChildren(this, filter);
    }

    @Override
    public boolean isDeleteAllowed() {
        return this.isRenameAllowed();
    }

    @Override
    public boolean isCopyAllowed() {
        return true;
    }

    @Override
    public boolean isMoveAllowed() {
        return this.isRenameAllowed();
    }

    @Override
    public boolean isRenameAllowed() {
        FileObject fo = this.getPrimaryFile();
        return !fo.isRoot() && fo.canWrite();
    }

    @Override
    public HelpCtx getHelpCtx() {
        return null;
    }

    public static DataFolder findFolder(FileObject fo) {
        DataObject d;
        try {
            d = DataObject.find(fo);
        }
        catch (DataObjectNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
        if (!(d instanceof DataFolder)) {
            throw new IllegalArgumentException("Not a DataFolder: " + (Object)fo + " (was a " + d.getClass().getName() + ") (file is folder? " + fo.isFolder() + ")");
        }
        return (DataFolder)d;
    }

    public static DataObject.Container findContainer(FileObject fo) {
        if (fo.isFolder()) {
            return FolderList.find(fo, true);
        }
        throw new IllegalArgumentException("Not a folder: " + (Object)fo);
    }

    @Override
    protected DataObject handleCopy(DataFolder f) throws IOException {
        DataFolder newFolderDF;
        DataFolder.testNesting(this, f);
        Enumeration<DataObject> en = this.children();
        DataObject newFolderDO = super.handleCopy(f);
        if (newFolderDO instanceof DataFolder) {
            newFolderDF = (DataFolder)newFolderDO;
        } else {
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)NbBundle.getMessage(DataObject.class, (String)"MSG_FMT_FileExists", (Object)this.getName(), (Object)f.getName()), 2));
            return newFolderDO;
        }
        while (en.hasMoreElements()) {
            try {
                DataObject obj = en.nextElement();
                if (obj.isCopyAllowed()) {
                    obj.copy(newFolderDF);
                    continue;
                }
                DataObject.LOG.warning(NbBundle.getMessage(DataFolder.class, (String)"FMT_CannotCopyDo", (Object)obj.getName()));
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return newFolderDF;
    }

    static void testNesting(DataFolder folder, DataFolder targetFolder) throws IOException {
        if (targetFolder.equals(folder)) {
            IOException ioe = new IOException("Error Copying File or Folder");
            Exceptions.attachLocalizedMessage((Throwable)ioe, (String)NbBundle.getMessage(DataFolder.class, (String)"EXC_CannotCopyTheSame", (Object)folder.getName()));
            throw ioe;
        }
        for (DataFolder testFolder = targetFolder.getFolder(); testFolder != null; testFolder = testFolder.getFolder()) {
            if (!testFolder.equals(folder)) continue;
            IOException ioe = new IOException("Error copying file or folder: " + (Object)folder.getPrimaryFile() + " cannot be copied to its subfolder " + (Object)targetFolder.getPrimaryFile());
            Exceptions.attachLocalizedMessage((Throwable)ioe, (String)NbBundle.getMessage(DataFolder.class, (String)"EXC_CannotCopySubfolder", (Object)folder.getName()));
            throw ioe;
        }
    }

    @Override
    protected void handleDelete() throws IOException {
        Enumeration<DataObject> en = this.children();
        FileLock lightWeightLock = null;
        try {
            lightWeightLock = DataFolder.createLightWeightLock(this);
            while (en.hasMoreElements()) {
                DataObject obj = en.nextElement();
                if (!obj.isValid()) continue;
                obj.delete();
            }
        }
        catch (IOException iex) {
            FileObject fo = this.getPrimaryFile();
            String message = NbBundle.getMessage(DataFolder.class, (String)"EXC_CannotDelete2", (Object)FileUtil.getFileDisplayName((FileObject)fo));
            Exceptions.attachLocalizedMessage((Throwable)iex, (String)message);
            throw iex;
        }
        finally {
            if (lightWeightLock != null) {
                lightWeightLock.releaseLock();
            }
        }
        if (!DataObject.isCurrentActionTerminated()) {
            super.handleDelete();
        }
    }

    private static FileLock createLightWeightLock(DataFolder df) {
        FileObject fo = df.getPrimaryFile();
        assert (fo != null);
        Object o = fo.getAttribute("LIGHTWEIGHT_LOCK_SET");
        assert (o == null || o instanceof FileLock);
        return (FileLock)o;
    }

    @Override
    protected FileObject handleRename(String name) throws IOException {
        if (!DataFolder.confirmName(name)) {
            IOException e = new IOException("bad name: " + name);
            Exceptions.attachLocalizedMessage((Throwable)e, (String)NbBundle.getMessage(DataFolder.class, (String)"EXC_WrongName", (Object)name));
            throw e;
        }
        return super.handleRename(name);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected FileObject handleMove(DataFolder df) throws IOException {
        FileObject originalFolder = this.getPrimaryFile();
        FileLock lock = originalFolder.lock();
        List<MultiDataObject.Pair> backup = this.saveEntries();
        boolean clearKeepAlive = false;
        try {
            FileObject newFile = super.handleMove(df);
            DataFolder newFolder = null;
            boolean dispose = false;
            boolean[] keepAlive = KEEP_ALIVE.get();
            if (keepAlive == null) {
                keepAlive = new boolean[]{false};
                KEEP_ALIVE.set(keepAlive);
            }
            int COUNT_DOWN_INIT = 20;
            for (int countDown = 20; countDown >= 0; --countDown) {
                try {
                    MultiFileLoader loader = this.getMultiFileLoader();
                    assert (loader instanceof DataLoaderPool.FolderLoader);
                    DataLoaderPool.FolderLoader folderLoader = (DataLoaderPool.FolderLoader)loader;
                    newFolder = (DataFolder)DataObjectPool.createMultiObject(folderLoader, newFile, this);
                    dispose = false;
                    break;
                }
                catch (DataObjectExistsException e) {
                    newFolder = (DataFolder)e.getDataObject();
                    newFolder.dispose();
                    dispose = true;
                    continue;
                }
            }
            Enumeration<DataObject> en = this.children();
            while (en.hasMoreElements()) {
                try {
                    DataObject obj = en.nextElement();
                    if (obj.isMoveAllowed()) {
                        obj.move(newFolder);
                        continue;
                    }
                    keepAlive[0] = true;
                    DataObject.LOG.warning(NbBundle.getMessage(DataFolder.class, (String)"FMT_CannotMoveDo", (Object)obj.getName()));
                }
                catch (IOException ex) {
                    keepAlive[0] = true;
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            if (keepAlive[0]) {
                this.restoreEntries(backup);
                this.list.refresh();
                assert (newFolder.getClass().getName().indexOf("NodeSharingDataFolder") >= 0);
                newFolder.dispose();
                FileObject ex = originalFolder;
                return ex;
            }
            try {
                if (!DataObject.isCurrentActionTerminated()) {
                    originalFolder.delete(lock);
                }
            }
            catch (IOException e) {
                Throwable t = Exceptions.attachLocalizedMessage((Throwable)e, (String)DataObject.getString("EXC_folder_delete_failed"));
                Exceptions.printStackTrace((Throwable)t);
            }
            if (dispose) {
                try {
                    this.setValid(false);
                    newFile = originalFolder;
                }
                catch (PropertyVetoException e) {
                    this.restoreEntries(backup);
                    newFile = this.getPrimaryEntry().getFile();
                }
            } else {
                this.changeItemByFolder(this.item().changePrimaryFile(newFile));
                newFolder.dispose();
                this.list = this.reassignList(newFile, true);
            }
            FileObject e = newFile;
            return e;
        }
        finally {
            if (clearKeepAlive) {
                KEEP_ALIVE.remove();
            }
            lock.releaseLock();
        }
    }

    @Override
    protected DataObject handleCreateFromTemplate(DataFolder f, String name) throws IOException {
        DataFolder newFolder = (DataFolder)super.handleCreateFromTemplate(f, name);
        Enumeration<DataObject> en = this.children();
        while (en.hasMoreElements()) {
            try {
                DataObject obj = en.nextElement();
                obj.createFromTemplate(newFolder);
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return newFolder;
    }

    @Override
    protected DataShadow handleCreateShadow(DataFolder f) throws IOException {
        DataFolder.testNesting(this, f);
        String name = this.getPrimaryFile().isRoot() ? FileUtil.findFreeFileName((FileObject)f.getPrimaryFile(), (String)"Root", (String)"shadow") : null;
        return DataShadow.create(f, name, this);
    }

    @Override
    boolean isMergingFolders(FileObject who, FileObject targetFolder) {
        return !targetFolder.equals((Object)who.getParent());
    }

    private static boolean confirmName(String folderName) {
        return folderName.indexOf(47) == -1 && folderName.indexOf(92) == -1;
    }

    private static Image icon2image(String key) {
        Object obj = UIManager.get(key);
        if (obj instanceof Image) {
            return (Image)obj;
        }
        if (obj instanceof Icon) {
            Icon icon = (Icon)obj;
            return ImageUtilities.icon2Image((Icon)icon);
        }
        return null;
    }

    static Image findIcon(int index, String k1, String k2) {
        if (IMGS[index] != null) {
            return IMGS[index];
        }
        Image i1 = DataFolder.icon2image(k1);
        if (i1 == null) {
            i1 = DataFolder.icon2image(k2);
        }
        DataFolder.IMGS[index] = i1;
        return i1;
    }

    static {
        KEEP_ALIVE = new ThreadLocal();
        IMGS = new Image[2];
    }

    private final class ListPCL
    implements PropertyChangeListener {
        ListPCL() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent ev) {
            if (this == DataFolder.this.pcl && !"refresh".equals(ev.getPropertyName())) {
                DataFolder.this.firePropertyChange("children", null, null);
            }
        }
    }

    private class Paste
    extends DataTransferSupport {
        Paste() {
        }

        @Override
        protected DataTransferSupport.PasteTypeExt[] definePasteTypes(int op) {
            switch (op) {
                case 4: {
                    return new DataTransferSupport.PasteTypeExt[]{new DataTransferSupport.PasteTypeExt(){

                        public String getName() {
                            return DataObject.getString("PT_move");
                        }

                        public HelpCtx getHelpCtx() {
                            return new HelpCtx(Paste.class.getName() + ".move");
                        }

                        @Override
                        protected boolean handleCanPaste(DataObject obj) {
                            return obj.isMoveAllowed() && !this.isParent(DataFolder.this.getPrimaryFile(), obj.getPrimaryFile());
                        }

                        @Override
                        protected void handlePaste(DataObject obj) throws IOException {
                            obj.move(DataFolder.this);
                        }

                        @Override
                        protected boolean cleanClipboard() {
                            return true;
                        }

                        private boolean isParent(FileObject fo, FileObject parent) {
                            File parentFile = FileUtil.toFile((FileObject)parent);
                            File foFile = FileUtil.toFile((FileObject)fo);
                            if (foFile != null && parentFile != null) {
                                return Paste.this.isParentFile(foFile, parentFile);
                            }
                            try {
                                if (fo.getFileSystem() != parent.getFileSystem()) {
                                    return false;
                                }
                            }
                            catch (IOException ex) {
                                // empty catch block
                            }
                            while (fo != null) {
                                if (fo.equals((Object)parent)) {
                                    return true;
                                }
                                fo = fo.getParent();
                            }
                            return false;
                        }
                    }};
                }
                case 1: {
                    return new DataTransferSupport.PasteTypeExt[]{new DataTransferSupport.PasteTypeExt(){

                        public String getName() {
                            return DataObject.getString("PT_copy");
                        }

                        public HelpCtx getHelpCtx() {
                            return new HelpCtx(Paste.class.getName() + ".copy");
                        }

                        @Override
                        protected boolean handleCanPaste(DataObject obj) {
                            return obj.isCopyAllowed();
                        }

                        @Override
                        protected void handlePaste(DataObject obj) throws IOException {
                            this.saveIfModified(obj);
                            obj.copy(DataFolder.this);
                        }

                        private void saveIfModified(DataObject obj) throws IOException {
                            SaveCookie sc;
                            if (obj.isModified() && (sc = obj.getCookie(SaveCookie.class)) != null) {
                                sc.save();
                            }
                        }
                    }, new DataTransferSupport.PasteTypeExt(){

                        public String getName() {
                            return DataObject.getString("PT_instantiate");
                        }

                        public HelpCtx getHelpCtx() {
                            return new HelpCtx(Paste.class.getName() + ".instantiate");
                        }

                        @Override
                        protected boolean handleCanPaste(DataObject obj) {
                            return obj.isTemplate();
                        }

                        @Override
                        protected void handlePaste(DataObject obj) throws IOException {
                            obj.createFromTemplate(DataFolder.this);
                        }
                    }, new DataTransferSupport.PasteTypeExt(){

                        public String getName() {
                            return DataObject.getString("PT_shadow");
                        }

                        public HelpCtx getHelpCtx() {
                            return new HelpCtx(Paste.class.getName() + ".shadow");
                        }

                        @Override
                        protected boolean handleCanPaste(DataObject obj) {
                            try {
                                if (!DataFolder.this.getPrimaryFile().getFileSystem().isDefault()) {
                                    return false;
                                }
                            }
                            catch (FileStateInvalidException ex) {
                                return false;
                            }
                            return obj.isShadowAllowed();
                        }

                        @Override
                        protected void handlePaste(DataObject obj) throws IOException {
                            obj.createShadow(DataFolder.this);
                        }
                    }};
                }
            }
            return new DataTransferSupport.PasteTypeExt[0];
        }

        private boolean isParentFile(File foFile, File parentFile) {
            boolean retVal = false;
            while (foFile != null) {
                if (foFile.equals(parentFile)) {
                    retVal = true;
                    break;
                }
                foFile = foFile.getParentFile();
            }
            return retVal;
        }

        @Override
        protected int[] defineOperations() {
            return new int[]{4, 1};
        }

        @Override
        protected void handleCreatePasteTypes(Transferable t, List<PasteType> s) {
            Node node = NodeTransfer.node((Transferable)t, (int)1);
            if (node != null) {
                try {
                    InstanceCookie cookie = (InstanceCookie)node.getCookie(InstanceCookie.class);
                    if (cookie != null && Serializable.class.isAssignableFrom(cookie.instanceClass())) {
                        s.add(new DataTransferSupport.SerializePaste(DataFolder.this, cookie));
                        s.add(new DataTransferSupport.InstantiatePaste(DataFolder.this, cookie));
                    }
                }
                catch (IOException e) {
                }
                catch (ClassNotFoundException e) {
                    // empty catch block
                }
            }
        }

    }

    private final class NewFolder
    extends NewType {
        NewFolder() {
        }

        public String getName() {
            return DataObject.getString("CTL_NewFolder");
        }

        public HelpCtx getHelpCtx() {
            return HelpCtx.DEFAULT_HELP;
        }

        public void create() throws IOException {
            NotifyDescriptor.InputLine input = new NotifyDescriptor.InputLine(DataObject.getString("CTL_NewFolderName"), DataObject.getString("CTL_NewFolderTitle"));
            input.setInputText(DataObject.getString("CTL_NewFolderValue"));
            if (DialogDisplayer.getDefault().notify((NotifyDescriptor)input) == NotifyDescriptor.OK_OPTION) {
                String folderName = input.getInputText();
                if ("".equals(folderName)) {
                    return;
                }
                FileObject folder = DataFolder.this.getPrimaryFile();
                int dotPos = -1;
                while ((dotPos = folderName.indexOf(".")) != -1) {
                    String subFolder = folderName.substring(0, dotPos);
                    folderName = folderName.substring(dotPos + 1);
                    FileObject existingFile = folder.getFileObject(subFolder);
                    if (existingFile != null) {
                        if (!existingFile.isFolder()) {
                            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)NbBundle.getMessage(DataObject.class, (String)"MSG_FMT_FileExists", (Object)subFolder, (Object)folder.getName()), 2));
                            return;
                        }
                        folder = existingFile;
                        continue;
                    }
                    if (!DataFolder.confirmName(subFolder)) {
                        throw new IOException(NbBundle.getMessage(DataObject.class, (String)"EXC_WrongName", (Object)subFolder));
                    }
                    folder = folder.createFolder(subFolder);
                }
                if (!"".equals(folderName)) {
                    FileObject existingFile = folder.getFileObject(folderName);
                    if (existingFile != null) {
                        if (existingFile.isFolder()) {
                            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)NbBundle.getMessage(DataObject.class, (String)"MSG_FMT_FolderExists", (Object)folderName, (Object)folder.getName()), 1));
                        } else {
                            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)NbBundle.getMessage(DataObject.class, (String)"MSG_FMT_FileExists", (Object)folderName, (Object)folder.getName()), 2));
                        }
                        return;
                    }
                    if (!DataFolder.confirmName(folderName)) {
                        throw new IOException(NbBundle.getMessage(DataObject.class, (String)"EXC_WrongName", (Object)folderName));
                    }
                    DataObject created = DataObject.find(folder.createFolder(folderName));
                    if (created != null) {
                        DataLoaderPool.getDefault().fireOperationEvent(new OperationEvent.Copy(created, DataFolder.this), 6);
                    }
                }
            }
        }
    }

    public class FolderNode
    extends DataNode {
        final /* synthetic */ DataFolder this$0;

        public FolderNode(DataFolder dataFolder, Children ch) {
            this.this$0 = dataFolder;
            super(dataFolder, ch);
            this.setIconBaseWithExtension("org/openide/loaders/defaultFolder.gif");
        }

        protected FolderNode(DataFolder dataFolder) {
            this.this$0 = dataFolder;
            super(dataFolder, DataFolder.createNodeChildren(dataFolder, DataFilter.ALL));
            this.setIconBaseWithExtension("org/openide/loaders/defaultFolder.gif");
        }

        @Override
        public Image getIcon(int type) {
            Image img = null;
            if (type == 1) {
                img = DataFolder.findIcon(0, "Nb.Explorer.Folder.icon", "Tree.closedIcon");
            }
            if (img == null) {
                img = super.getIcon(type);
            } else {
                try {
                    DataObject obj = this.getDataObject();
                    img = obj.getPrimaryFile().getFileSystem().getStatus().annotateIcon(img, type, obj.files());
                }
                catch (FileStateInvalidException e) {
                    // empty catch block
                }
            }
            return img;
        }

        @Override
        public Image getOpenedIcon(int type) {
            Image img = null;
            if (type == 1) {
                img = DataFolder.findIcon(1, "Nb.Explorer.Folder.openedIcon", "Tree.openIcon");
            }
            if (img == null) {
                img = super.getOpenedIcon(type);
            } else {
                try {
                    DataObject obj = this.getDataObject();
                    img = obj.getPrimaryFile().getFileSystem().getStatus().annotateIcon(img, type, obj.files());
                }
                catch (FileStateInvalidException e) {
                    // empty catch block
                }
            }
            return img;
        }

        @Override
        public <T extends Node.Cookie> T getCookie(Class<T> clazz) {
            if (clazz == org.openide.nodes.Index.class || clazz == Index.class) {
                try {
                    if (this.this$0.getPrimaryFile().getFileSystem().isDefault() || Boolean.TRUE.equals(this.this$0.getPrimaryFile().getAttribute("DataFolder.Index.reorderable"))) {
                        return (T)((Node.Cookie)clazz.cast((Object)new Index(this.this$0, (Node)this)));
                    }
                }
                catch (FileStateInvalidException ex) {
                    Logger.getLogger(DataFolder.class.getName()).log(Level.WARNING, null, (Throwable)ex);
                }
            }
            return super.getCookie(clazz);
        }

        @Override
        protected Sheet createSheet() {
            Sheet s = super.createSheet();
            Sheet.Set ss = new Sheet.Set();
            ss.setName("sorting");
            ss.setDisplayName(DataObject.getString("PROP_sorting"));
            ss.setShortDescription(DataObject.getString("HINT_sorting"));
            PropertySupport.ReadWrite<SortMode> p = new PropertySupport.ReadWrite<SortMode>("sortMode", SortMode.class, DataObject.getString("PROP_sort"), DataObject.getString("HINT_sort")){

                public SortMode getValue() {
                    return FolderNode.this.this$0.getSortMode();
                }

                public void setValue(SortMode o) throws InvocationTargetException {
                    try {
                        FolderNode.this.this$0.setSortMode(o);
                    }
                    catch (IOException ex) {
                        throw new InvocationTargetException(ex);
                    }
                }

                public PropertyEditor getPropertyEditor() {
                    return new SortModeEditor();
                }
            };
            ss.put((Node.Property)p);
            s.put(ss);
            return s;
        }

        @Override
        public Action getPreferredAction() {
            return null;
        }

        public NewType[] getNewTypes() {
            return new NewType[0];
        }

        private synchronized FolderRenameHandler getRenameHandler() {
            Collection handlers = Lookup.getDefault().lookupAll(FolderRenameHandler.class);
            if (handlers.size() == 0) {
                return null;
            }
            if (handlers.size() > 1) {
                DataObject.LOG.warning("Multiple instances of FolderRenameHandler found in Lookup; only using first one: " + handlers);
            }
            return (FolderRenameHandler)handlers.iterator().next();
        }

        @Override
        public void setName(String name) {
            FolderRenameHandler handler = this.getRenameHandler();
            if (handler == null) {
                super.setName(name);
            } else {
                handler.handleRename(this.this$0, name);
            }
        }

        protected void createPasteTypes(Transferable t, List<PasteType> s) {
            List<File> files;
            super.createPasteTypes(t, s);
            if (this.this$0.getPrimaryFile().canWrite()) {
                this.this$0.dataTransferSupport.createPasteTypes(t, s);
            }
            if (null != (files = this.getDraggedFilesList(t)) && !files.isEmpty() && s.isEmpty()) {
                ArrayList<Transferable> transferables = new ArrayList<Transferable>(files.size());
                for (File f : files) {
                    Transferable nodeTransferable;
                    if (f.getName().length() == 0 || null == (nodeTransferable = this.createNodeTransferable(f))) continue;
                    transferables.add(nodeTransferable);
                }
                if (transferables.size() == 0) {
                    return;
                }
                ExTransferable.Multi multi = new ExTransferable.Multi(transferables.toArray(new Transferable[transferables.size()]));
                super.createPasteTypes((Transferable)multi, s);
                if (this.this$0.getPrimaryFile().canWrite()) {
                    this.this$0.dataTransferSupport.createPasteTypes((Transferable)multi, s);
                }
            }
        }

        Transferable createNodeTransferable(File f) {
            Transferable result = null;
            FileObject fo = FileUtil.toFileObject((File)FileUtil.normalizeFile((File)f));
            if (null != fo) {
                try {
                    Node delegate;
                    DataObject dob = DataObject.find(fo);
                    if (null != dob && !(delegate = dob.getNodeDelegate()).equals((Object)this)) {
                        result = dob.getNodeDelegate().clipboardCopy();
                        ExClipboard exClipboard = (ExClipboard)Lookup.getDefault().lookup(ExClipboard.class);
                        if (exClipboard != null) {
                            result = exClipboard.convert(result);
                        }
                    }
                }
                catch (IOException ioE) {
                    Logger.getLogger(DataFolder.class.getName()).log(Level.INFO, null, ioE);
                }
            }
            return result;
        }

        private List<File> getDraggedFilesList(Transferable t) {
            try {
                if (t.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
                    List fileList = (List)t.getTransferData(DataFlavor.javaFileListFlavor);
                    if (null != fileList) {
                        List checkedList = NbCollections.checkedListByCopy((List)fileList, File.class, (boolean)true);
                        return this.filterRelativePaths(checkedList);
                    }
                } else if (t.isDataFlavorSupported(this.getUriListDataFlavor())) {
                    String uriList = (String)t.getTransferData(this.getUriListDataFlavor());
                    return this.textURIListToFileList(uriList);
                }
            }
            catch (UnsupportedFlavorException ex) {
                Logger.getLogger(DataFolder.class.getName()).log(Level.WARNING, null, ex);
            }
            catch (IOException ex) {
                Logger.getLogger(DataFlavor.class.getName()).log(Level.FINE, null, ex);
            }
            return null;
        }

        private List<File> filterRelativePaths(List<File> list) {
            ArrayList<File> absOnly = new ArrayList<File>();
            for (File f : list) {
                if (!f.isAbsolute()) continue;
                absOnly.add(f);
            }
            return absOnly;
        }

        private DataFlavor getUriListDataFlavor() {
            if (null == uriListDataFlavor) {
                try {
                    uriListDataFlavor = new DataFlavor("text/uri-list;class=java.lang.String");
                }
                catch (ClassNotFoundException cnfE) {
                    throw new AssertionError(cnfE);
                }
            }
            return uriListDataFlavor;
        }

        private List<File> textURIListToFileList(String data) {
            ArrayList<File> list = new ArrayList<File>(1);
            StringTokenizer st = new StringTokenizer(data, "\r\n");
            while (st.hasMoreTokens()) {
                String s = st.nextToken();
                if (s.startsWith("#")) continue;
                try {
                    URI uri = new URI(s);
                    File file = Utilities.toFile((URI)uri);
                    list.add(file);
                }
                catch (URISyntaxException e) {
                }
                catch (IllegalArgumentException e) {}
            }
            return list;
        }

    }

    public static abstract class SortMode
    implements Comparator<DataObject> {
        public static final SortMode NONE = new FolderComparator(0);
        public static final SortMode NAMES = new FolderComparator(1);
        public static final SortMode CLASS = new FolderComparator(2);
        public static final SortMode FOLDER_NAMES = new FolderComparator(3);
        public static final SortMode LAST_MODIFIED = new FolderComparator(4);
        public static final SortMode SIZE = new FolderComparator(5);
        public static final SortMode EXTENSIONS = new FolderComparator(6);

        void write(FileObject f) throws IOException {
            String x = this == FOLDER_NAMES ? "F" : (this == NAMES ? "N" : (this == CLASS ? "C" : (this == LAST_MODIFIED ? "M" : (this == SIZE ? "S" : "O"))));
            f.setAttribute("OpenIDE-Folder-SortMode", (Object)x);
        }

        static SortMode read(FileObject f) {
            String x = (String)f.getAttribute("OpenIDE-Folder-SortMode");
            if (x == null || x.length() != 1) {
                return FOLDER_NAMES;
            }
            char c = x.charAt(0);
            switch (c) {
                case 'N': {
                    return NAMES;
                }
                case 'C': {
                    return CLASS;
                }
                case 'O': {
                    return NONE;
                }
                case 'M': {
                    return LAST_MODIFIED;
                }
                case 'S': {
                    return SIZE;
                }
            }
            return FOLDER_NAMES;
        }
    }

    public static class Index
    extends Index.Support {
        private DataFolder df;
        private Node node;
        private Listener listener;

        @Deprecated
        public Index(DataFolder df) {
            this(df, df.getNodeDelegate());
        }

        public Index(DataFolder df, Node node) {
            this.df = df;
            this.node = node;
            this.listener = new Listener();
            node.addNodeListener(NodeOp.weakNodeListener((NodeListener)this.listener, (Object)node));
        }

        public int getNodesCount() {
            return this.getNodes().length;
        }

        public Node[] getNodes() {
            return this.node.getChildren().getNodes(FolderChildren.checkChildrenMutex());
        }

        public void reorder(int[] perm) {
            DataObject[] curObjs = this.df.getChildren();
            DataObject[] newObjs = new DataObject[curObjs.length];
            Node[] nodes = this.getNodes();
            if (nodes.length != perm.length) {
                throw new IllegalArgumentException("permutation of incorrect length: " + perm.length + " rather than " + nodes.length);
            }
            HashMap<String, DataObject> names = new HashMap<String, DataObject>(2 * curObjs.length);
            for (int i = 0; i < curObjs.length; ++i) {
                Node del = curObjs[i].getNodeDelegate();
                if (del.getCookie(DataObject.class) != null) continue;
                names.put(del.getName(), curObjs[i]);
            }
            DataObject[] dperm = new DataObject[perm.length];
            for (int i2 = 0; i2 < perm.length; ++i2) {
                DataObject d = (DataObject)nodes[i2].getCookie(DataObject.class);
                if (d == null) {
                    d = (DataObject)names.get(nodes[i2].getName());
                }
                if (d == null) {
                    throw new IllegalArgumentException("cannot reorder node with no DataObject: " + (Object)nodes[i2]);
                }
                if (d.getFolder() != this.df) {
                    throw new IllegalArgumentException("wrong folder for: " + (Object)d.getPrimaryFile() + " rather than " + (Object)this.df.getPrimaryFile());
                }
                dperm[perm[i2]] = d;
            }
            HashSet<DataObject> dpermSet = new HashSet<DataObject>(Arrays.asList(dperm));
            if (dpermSet.size() != dperm.length) {
                throw new IllegalArgumentException("duplicate DataObject's among reordered childen");
            }
            int dindex = 0;
            for (int i3 = 0; i3 < curObjs.length; ++i3) {
                newObjs[i3] = dpermSet.remove(curObjs[i3]) ? dperm[dindex++] : curObjs[i3];
            }
            try {
                this.df.setOrder(newObjs);
            }
            catch (IOException ex) {
                Exceptions.attachLocalizedMessage((Throwable)ex, (String)DataObject.getString("EXC_ReorderFailed"));
                Exceptions.printStackTrace((Throwable)ex);
            }
        }

        public void reorder() {
            Index.Support.showIndexedCustomizer((org.openide.nodes.Index)this);
        }

        void fireChangeEventAccess() {
            this.fireChangeEvent(new ChangeEvent((Object)this));
        }

        private final class Listener
        implements NodeListener {
            Listener() {
            }

            public void propertyChange(PropertyChangeEvent ev) {
            }

            public void nodeDestroyed(NodeEvent ev) {
            }

            public void childrenReordered(NodeReorderEvent ev) {
                Index.this.fireChangeEventAccess();
            }

            public void childrenRemoved(NodeMemberEvent ev) {
                Index.this.fireChangeEventAccess();
            }

            public void childrenAdded(NodeMemberEvent ev) {
                Index.this.fireChangeEventAccess();
            }
        }

    }

    private static final class ClonedFilterHandle
    implements Node.Handle {
        private static final long serialVersionUID = 24234097765186L;
        private DataObject folder;
        private DataFilter filter;

        public ClonedFilterHandle(DataFolder folder, DataFilter filter) {
            this.folder = folder;
            this.filter = filter;
        }

        public Node getNode() throws IOException {
            if (this.folder instanceof DataFolder) {
                DataFolder dataFolder = (DataFolder)this.folder;
                dataFolder.getClass();
                return dataFolder.new ClonedFilter(this.filter);
            }
            throw new InvalidObjectException(this.folder == null ? "" : this.folder.toString());
        }
    }

    private final class ClonedFilter
    extends FilterNode {
        private DataFilter filter;
        private int hashCode;

        public ClonedFilter(Node n, DataFilter filter) {
            super(n, DataFolder.this.createNodeChildren(filter));
            this.hashCode = -1;
            this.filter = filter;
        }

        public ClonedFilter(DataFilter filter) {
            this(dataFolder.getNodeDelegate(), filter);
        }

        public Node cloneNode() {
            if (DataFolder.this.isValid()) {
                return new ClonedFilter(this.filter);
            }
            return super.cloneNode();
        }

        public Node.Handle getHandle() {
            return new ClonedFilterHandle(DataFolder.this, this.filter);
        }

        public boolean equals(Object o) {
            if (o == null) {
                return false;
            }
            if (o == this) {
                return true;
            }
            if (o instanceof FolderNode) {
                FolderNode fn = (FolderNode)((Object)o);
                if (fn.getCookie(DataFolder.class) != DataFolder.this) {
                    return false;
                }
                Children ch = fn.getChildren();
                if (ch instanceof FolderChildren) {
                    ((FolderChildren)ch).getFilter().equals(this.filter);
                }
                return false;
            }
            if (o instanceof ClonedFilter) {
                ClonedFilter cf = (ClonedFilter)((Object)o);
                return cf.getCookie(DataFolder.class) == DataFolder.this && cf.filter.equals(this.filter);
            }
            return false;
        }

        public int hashCode() {
            if (this.hashCode == -1) {
                this.hashCode = DataFolder.this.isValid() ? DataFolder.this.getNodeDelegate().hashCode() : super.hashCode();
                if (this.hashCode == -1) {
                    this.hashCode = -2;
                }
            }
            return this.hashCode;
        }
    }

}

