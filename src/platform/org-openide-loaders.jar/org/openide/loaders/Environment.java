/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 */
package org.openide.loaders;

import java.util.Collection;
import javax.naming.Context;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.loaders.DefaultSettingsContext;
import org.openide.util.Lookup;

public final class Environment {
    private static Lookup.Result<Provider> result;

    private Environment() {
    }

    public static Lookup find(DataObject obj) {
        while (obj != null) {
            Lookup l = Environment.findForOne(obj);
            if (l != null) {
                return l;
            }
            FileObject fo = obj.getPrimaryFile().getParent();
            if (fo == null) break;
            try {
                obj = DataObject.find(fo);
                continue;
            }
            catch (DataObjectNotFoundException ex) {
                break;
            }
        }
        return Lookup.EMPTY;
    }

    @Deprecated
    public static Context findSettingsContext(DataObject obj) {
        for (Provider ep : Environment.getProviders().allInstances()) {
            Context ctx;
            Lookup lookup = ep.getEnvironment(obj);
            if (lookup == null || (ctx = (Context)lookup.lookup(Context.class)) == null) continue;
            return ctx;
        }
        return new DefaultSettingsContext(obj);
    }

    static Lookup findForOne(DataObject obj) {
        for (Provider ep : Environment.getProviders().allInstances()) {
            Lookup lookup = ep.getEnvironment(obj);
            if (lookup == null) continue;
            return lookup;
        }
        return null;
    }

    static Lookup.Result<Provider> getProviders() {
        if (result == null) {
            result = Lookup.getDefault().lookupResult(Provider.class);
        }
        return result;
    }

    public static interface Provider {
        public Lookup getEnvironment(DataObject var1);
    }

}

