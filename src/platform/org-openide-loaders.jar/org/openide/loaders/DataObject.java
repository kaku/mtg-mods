/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.actions.Savable
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.netbeans.spi.actions.AbstractSavable
 *  org.openide.cookies.SaveCookie
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Cancellable
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.HelpCtx$Provider
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakSet
 */
package org.openide.loaders;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.text.DateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.event.ChangeListener;
import org.netbeans.api.actions.Savable;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.modules.openide.loaders.DataObjectAccessor;
import org.netbeans.modules.openide.loaders.DataObjectEncodingQueryImplementation;
import org.netbeans.modules.openide.loaders.Unmodify;
import org.netbeans.spi.actions.AbstractSavable;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.loaders.Bundle;
import org.openide.loaders.CreateFromTemplateAttributesProvider;
import org.openide.loaders.DataFilter;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataLoader;
import org.openide.loaders.DataLoaderPool;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObjectAccessorImpl;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.loaders.DataObjectPool;
import org.openide.loaders.DataShadow;
import org.openide.loaders.FolderList;
import org.openide.loaders.OperationEvent;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Cancellable;
import org.openide.util.ChangeSupport;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.WeakSet;

public abstract class DataObject
implements Node.Cookie,
Serializable,
HelpCtx.Provider,
Lookup.Provider {
    private static final long serialVersionUID = 3328227388376142699L;
    public static final String PROP_TEMPLATE = "template";
    public static final String PROP_NAME = "name";
    public static final String PROP_HELP = "helpCtx";
    public static final String PROP_MODIFIED = "modified";
    public static final String PROP_COOKIE = "cookie";
    public static final String PROP_VALID = "valid";
    public static final String PROP_PRIMARY_FILE = "primaryFile";
    public static final String PROP_FILES = "files";
    private static ThreadLocal<ProgressInfo> PROGRESS_INFO_TL = new ThreadLocal<T>();
    static final String EA_ASSIGNED_LOADER = "NetBeansAttrAssignedLoader";
    static final String EA_ASSIGNED_LOADER_MODULE = "NetBeansAttrAssignedLoaderModule";
    private static final Logger OBJ_LOG = Logger.getLogger(DataObject.class.getName());
    private static final ModifiedRegistry modified = new ModifiedRegistry();
    private static final Set<DataObject> syncModified = Collections.synchronizedSet(modified);
    private boolean modif = false;
    private transient Node nodeDelegate;
    private transient DataObjectPool.Item item;
    private DataLoader loader;
    private PropertyChangeSupport changeSupport;
    private VetoableChangeSupport vetoableChangeSupport;
    private static final Object listenersMethodLock = new Object();
    private final Object nodeCreationLock = new Object();
    private static Object synchObject = new Object();
    static final Logger LOG = Logger.getLogger("org.openide.loaders");
    private static Registry REGISTRY_INSTANCE;
    private static final Set<Class<?>> warnedClasses;

    public DataObject(FileObject pf, DataLoader loader) throws DataObjectExistsException {
        this(pf, DataObjectPool.getPOOL().register(pf, loader), loader);
    }

    private DataObject(FileObject pf, DataObjectPool.Item item, DataLoader loader) {
        OBJ_LOG.log(Level.FINE, "created {0}", (Object)pf);
        this.item = item;
        this.loader = loader;
        item.setDataObject(this);
    }

    protected void dispose() {
        DataObjectPool.Item i = this.item();
        if (i != null) {
            DataObjectPool.getPOOL().countRegistration(i.primaryFile);
            i.deregister(true);
            i.setDataObject(null);
            this.firePropertyChange("valid", Boolean.TRUE, Boolean.FALSE);
        }
    }

    final DataObjectPool.Item item() {
        return this.item;
    }

    private void changeItem(DataObjectPool.Item item) {
        this.item = item;
    }

    final void changeItemByFolder(DataObjectPool.Item item) {
        assert (this instanceof DataFolder);
        this.changeItem(item);
    }

    public void setValid(boolean valid) throws PropertyVetoException {
        if (!valid && this.isValid()) {
            this.markInvalid0();
        }
    }

    final void markInvalid0() throws PropertyVetoException {
        this.fireVetoableChange("valid", Boolean.TRUE, Boolean.FALSE);
        this.dispose();
        this.setModified(false);
    }

    public final boolean isValid() {
        return this.item().isValid();
    }

    public final DataLoader getLoader() {
        return this.loader;
    }

    protected final void markFiles() throws IOException {
        for (FileObject fo : this.files()) {
            this.loader.markFile(fo);
        }
    }

    public Set<FileObject> files() {
        return Collections.singleton(this.getPrimaryFile());
    }

    public final Node getNodeDelegate() {
        if (!this.isValid()) {
            String debugMessage = "this=" + this + " id=" + System.identityHashCode(this) + " primaryFileId=" + System.identityHashCode((Object)this.getPrimaryFile()) + " valid=" + this.getPrimaryFile().isValid() + "\n";
            DataObject dob = DataObjectPool.getPOOL().find(this.getPrimaryFile());
            debugMessage = debugMessage + "pool=" + dob;
            if (dob != null) {
                debugMessage = debugMessage + " id=" + System.identityHashCode(dob);
                if (dob.getPrimaryFile() != null) {
                    debugMessage = debugMessage + " primaryFileId=" + System.identityHashCode((Object)dob.getPrimaryFile()) + " valid=" + dob.getPrimaryFile().isValid();
                }
            }
            IllegalStateException e = new IllegalStateException("The data object " + (Object)this.getPrimaryFile() + " is invalid; you may not call getNodeDelegate on it any more; see #17020 and please fix your code.\n" + debugMessage);
            Logger.getLogger(DataObject.class.getName()).log(Level.INFO, null, e);
        }
        return this.getNodeDelegateImpl();
    }

    private final Node getNodeDelegateImpl() {
        if (this.nodeDelegate == null) {
            Children.MUTEX.readAccess(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    Object object = DataObject.this.nodeCreationLock;
                    synchronized (object) {
                        if (DataObject.this.nodeDelegate == null) {
                            DataObject.this.nodeDelegate = DataObject.this.createNodeDelegate();
                        }
                    }
                }
            });
            if (this.nodeDelegate == null) {
                throw new IllegalStateException("DataObject " + this + " has null node delegate");
            }
        }
        return this.nodeDelegate;
    }

    Node getClonedNodeDelegate(DataFilter filter) {
        return this.getNodeDelegate().cloneNode();
    }

    final Node getNodeDelegateOrNull() {
        return this.nodeDelegate;
    }

    final void setNodeDelegate(Node n) {
        this.nodeDelegate = n;
    }

    protected Node createNodeDelegate() {
        return new DataNode(this, Children.LEAF);
    }

    protected FileLock takePrimaryFileLock() throws IOException {
        return this.getPrimaryFile().lock();
    }

    static boolean setTemplate(FileObject fo, boolean newTempl) throws IOException {
        boolean oldTempl = false;
        Object o = fo.getAttribute("template");
        if (o instanceof Boolean && ((Boolean)o).booleanValue()) {
            oldTempl = true;
        }
        if (oldTempl == newTempl) {
            return false;
        }
        fo.setAttribute("template", (Object)(newTempl ? Boolean.TRUE : null));
        return true;
    }

    public final void setTemplate(boolean newTempl) throws IOException {
        if (!DataObject.setTemplate(this.getPrimaryFile(), newTempl)) {
            return;
        }
        this.firePropertyChange("template", !newTempl ? Boolean.TRUE : Boolean.FALSE, newTempl ? Boolean.TRUE : Boolean.FALSE);
    }

    public final boolean isTemplate() {
        Object o = this.getPrimaryFile().getAttribute("template");
        boolean ret = false;
        if (o instanceof Boolean) {
            ret = (Boolean)o;
        }
        return ret;
    }

    public abstract boolean isDeleteAllowed();

    public abstract boolean isCopyAllowed();

    public abstract boolean isMoveAllowed();

    public boolean isShadowAllowed() {
        return true;
    }

    public abstract boolean isRenameAllowed();

    public boolean isModified() {
        return this.modif;
    }

    public void setModified(boolean modif) {
        boolean log = OBJ_LOG.isLoggable(Level.FINE);
        if (log) {
            String msg = "setModified(): modif=" + modif + ", original-modif=" + this.modif;
            if (OBJ_LOG.isLoggable(Level.FINEST)) {
                OBJ_LOG.log(Level.FINEST, msg, new Exception());
            } else {
                OBJ_LOG.log(Level.FINE, msg);
            }
        }
        if (this.modif != modif) {
            this.modif = modif;
            Savable present = (Savable)this.getLookup().lookup(AbstractSavable.class);
            if (log) {
                OBJ_LOG.log(Level.FINE, "setModified(): present={0}", new Object[]{present});
            }
            if (modif) {
                syncModified.add(this);
                if (present == null) {
                    new DOSavable(this).add();
                }
            } else {
                Unmodify un;
                syncModified.remove(this);
                if (present == null) {
                    new DOSavable(this).remove();
                }
                if ((un = (Unmodify)this.getLookup().lookup(Unmodify.class)) != null) {
                    un.unmodify();
                }
            }
            this.firePropertyChange("modified", !modif ? Boolean.TRUE : Boolean.FALSE, modif ? Boolean.TRUE : Boolean.FALSE);
        }
    }

    public abstract HelpCtx getHelpCtx();

    public final FileObject getPrimaryFile() {
        return this.item().primaryFile;
    }

    public static DataObject find(FileObject fo) throws DataObjectNotFoundException {
        if (fo == null) {
            throw new IllegalArgumentException("Called DataObject.find on null");
        }
        try {
            if (!fo.isValid()) {
                FileStateInvalidException ex = new FileStateInvalidException(fo.toString());
                Exceptions.attachLocalizedMessage((Throwable)ex, (String)Bundle.EXC_FIND_4_INVALID(fo.getPath()));
                throw ex;
            }
            DataObject obj = DataObjectPool.getPOOL().find(fo);
            if (obj != null) {
                return obj;
            }
            DataLoaderPool p = DataLoaderPool.getDefault();
            assert (p != null);
            obj = p.findDataObject(fo);
            if (obj != null) {
                return obj;
            }
            throw new DataObjectNotFoundException(fo);
        }
        catch (DataObjectExistsException ex) {
            return ex.getDataObject();
        }
        catch (IOException ex) {
            throw (DataObjectNotFoundException)new DataObjectNotFoundException(fo).initCause(ex);
        }
    }

    public static Registry getRegistry() {
        return REGISTRY_INSTANCE;
    }

    public String getName() {
        return this.getPrimaryFile().getName();
    }

    public String toString() {
        return super.toString() + '[' + (Object)this.getPrimaryFile() + ']';
    }

    public final DataFolder getFolder() {
        FileObject fo = this.getPrimaryFile().getParent();
        return fo == null ? null : DataFolder.findFolder(fo);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final DataObject copy(final DataFolder f) throws IOException {
        ProgressInfo pi = DataObject.getProgressInfo();
        if (pi == null) {
            pi = DataObject.initProgressInfo(Bundle.LBL_Copying(this.getName()), this);
        } else if (pi.isTerminated()) {
            return null;
        }
        try {
            pi.updateProgress(this);
            final DataObject[] result = new DataObject[1];
            this.invokeAtomicAction(f.getPrimaryFile(), new FileSystem.AtomicAction(){

                public void run() throws IOException {
                    result[0] = DataObject.this.handleCopy(f);
                }
            }, null);
            DataObject.fireOperationEvent(new OperationEvent.Copy(result[0], this), 1);
            DataObject dataObject = result[0];
            return dataObject;
        }
        finally {
            DataObject.finishProgressInfoIfDone(pi, this);
        }
    }

    protected abstract DataObject handleCopy(DataFolder var1) throws IOException;

    final DataObject copyRename(final DataFolder f, final String name, final String ext) throws IOException {
        final DataObject[] result = new DataObject[1];
        this.invokeAtomicAction(f.getPrimaryFile(), new FileSystem.AtomicAction(){

            public void run() throws IOException {
                result[0] = DataObject.this.handleCopyRename(f, name, ext);
            }
        }, null);
        DataObject.fireOperationEvent(new OperationEvent(result[0]), 7);
        return result[0];
    }

    protected DataObject handleCopyRename(DataFolder f, String name, String ext) throws IOException {
        throw new IOException("Unsupported operation");
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final void delete() throws IOException {
        ProgressInfo pi = DataObject.getProgressInfo();
        if (pi == null) {
            pi = DataObject.initProgressInfo(Bundle.LBL_Deleting(this.getName()), this);
        } else if (pi.isTerminated()) {
            return;
        }
        try {
            pi.updateProgress(this);
            this.invokeAtomicAction(this.getPrimaryFile(), new FileSystem.AtomicAction(){

                public void run() throws IOException {
                    DataObject.this.handleDelete();
                    if (DataObject.isCurrentActionTerminated() && DataObject.this.isValid()) {
                        return;
                    }
                    DataObjectPool.getPOOL().countRegistration(DataObject.this.item().primaryFile);
                    DataObject.this.item().deregister(false);
                    DataObject.this.item().setDataObject(null);
                }
            }, this.synchObject());
            if (pi.isTerminated() && this.isValid()) {
                return;
            }
            this.firePropertyChange("valid", Boolean.TRUE, Boolean.FALSE);
            DataObject.fireOperationEvent(new OperationEvent(this), 3);
        }
        finally {
            DataObject.finishProgressInfoIfDone(pi, this);
        }
    }

    protected abstract void handleDelete() throws IOException;

    public final void rename(String name) throws IOException {
        class Op
        implements FileSystem.AtomicAction {
            FileObject oldPf;
            FileObject newPf;
            String oldName;
            String newName;

            Op() {
            }

            public void run() throws IOException {
                this.oldName = DataObject.this.getName();
                if (this.oldName.equals(this.newName)) {
                    return;
                }
                this.oldPf = DataObject.this.getPrimaryFile();
                this.newPf = DataObject.this.handleRename(this.newName);
                if (this.oldPf != this.newPf) {
                    DataObject.this.changeItem(DataObject.this.item().changePrimaryFile(this.newPf));
                }
                this.newName = DataObject.this.getName();
            }
        }
        if (name == null || name.trim().length() == 0) {
            IllegalArgumentException iae = new IllegalArgumentException(this.getName());
            String msg = NbBundle.getMessage(DataObject.class, (String)"MSG_NotValidName", (Object)this.getName());
            Exceptions.attachLocalizedMessage((Throwable)iae, (String)msg);
            throw iae;
        }
        Op op = new Op();
        op.newName = name;
        FileObject target = this.getPrimaryFile().getParent();
        if (target == null) {
            target = this.getPrimaryFile();
        }
        this.invokeAtomicAction(target, op, this.synchObject());
        if (op.oldName.equals(op.newName)) {
            return;
        }
        if (op.oldPf != op.newPf) {
            this.firePropertyChange("primaryFile", (Object)op.oldPf, (Object)op.newPf);
        }
        this.firePropertyChange("name", op.oldName, op.newName);
        this.firePropertyChange("files", null, null);
        DataObject.fireOperationEvent(new OperationEvent.Rename(this, op.oldName), 4);
    }

    protected abstract FileObject handleRename(String var1) throws IOException;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final void move(DataFolder df) throws IOException {
        class Op
        implements FileSystem.AtomicAction {
            FileObject old;
            final /* synthetic */ DataFolder val$df;

            Op() {
                this.val$df = var2_2;
            }

            public void run() throws IOException {
                if (this$0.getFolder() == null) {
                    return;
                }
                if (this.val$df.equals(this$0.getFolder())) {
                    return;
                }
                this.old = this$0.getPrimaryFile();
                FileObject mf = this$0.handleMove(this.val$df);
                this$0.changeItem(this$0.item().changePrimaryFile(mf));
            }
        }
        Op op = new Op(this, df);
        ProgressInfo pi = DataObject.getProgressInfo();
        if (pi == null) {
            pi = DataObject.initProgressInfo(Bundle.LBL_Moving(this.getName()), this);
        } else if (pi.isTerminated()) {
            return;
        }
        try {
            pi.updateProgress(this);
            this.invokeAtomicAction(df.getPrimaryFile(), op, this.synchObject());
            this.firePropertyChange("primaryFile", (Object)op.old, (Object)this.getPrimaryFile());
            DataObject.fireOperationEvent(new OperationEvent.Move(this, op.old), 2);
        }
        finally {
            DataObject.finishProgressInfoIfDone(pi, this);
        }
    }

    protected abstract FileObject handleMove(DataFolder var1) throws IOException;

    protected DataShadow handleCreateShadow(DataFolder f) throws IOException {
        return DataShadow.create(f, this);
    }

    public final DataShadow createShadow(final DataFolder f) throws IOException {
        final DataShadow[] result = new DataShadow[1];
        this.invokeAtomicAction(f.getPrimaryFile(), new FileSystem.AtomicAction(){

            public void run() throws IOException {
                result[0] = DataObject.this.handleCreateShadow(f);
            }
        }, null);
        DataObject.fireOperationEvent(new OperationEvent.Copy(result[0], this), 5);
        return result[0];
    }

    public final DataObject createFromTemplate(DataFolder f) throws IOException {
        return this.createFromTemplate(f, null);
    }

    public final DataObject createFromTemplate(DataFolder f, String name) throws IOException {
        return this.createFromTemplate(f, name, Collections.<K, V>emptyMap());
    }

    public final DataObject createFromTemplate(DataFolder f, String name, Map<String, ? extends Object> parameters) throws IOException {
        CreateAction create = new CreateAction(this, f, name, parameters);
        this.invokeAtomicAction(f.getPrimaryFile(), create, null);
        DataObject.fireOperationEvent(new OperationEvent.Copy(create.result, this), 6);
        return create.result;
    }

    protected abstract DataObject handleCreateFromTemplate(DataFolder var1, String var2) throws IOException;

    private static void fireOperationEvent(OperationEvent ev, int type) {
        DataLoaderPool.getDefault().fireOperationEvent(ev, type);
    }

    Object synchObject() {
        return synchObject;
    }

    private void invokeAtomicAction(FileObject target, FileSystem.AtomicAction action, Object lockTheSession) throws IOException {
        class WrapRun
        implements FileSystem.AtomicAction {
            final /* synthetic */ Object val$lockTheSession;
            final /* synthetic */ FileSystem.AtomicAction val$action;

            WrapRun() {
                this.val$lockTheSession = var2_2;
                this.val$action = var3_3;
            }

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            public void run() throws IOException {
                Object object = this.val$lockTheSession;
                synchronized (object) {
                    this.val$action.run();
                }
            }
        }
        FileSystem.AtomicAction toRun = lockTheSession != null ? new WrapRun(this, lockTheSession, action) : action;
        if (Boolean.getBoolean("netbeans.dataobject.insecure.operation")) {
            DataObjectPool.getPOOL().runAtomicActionSimple(target, toRun);
            return;
        }
        if (this instanceof DataFolder) {
            DataObjectPool.getPOOL().runAtomicActionSimple(target, toRun);
        } else {
            DataObjectPool.getPOOL().runAtomicAction(target, toRun);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addPropertyChangeListener(PropertyChangeListener l) {
        Object object = listenersMethodLock;
        synchronized (object) {
            if (this.changeSupport == null) {
                this.changeSupport = new PropertyChangeSupport(this);
            }
        }
        this.changeSupport.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        if (this.changeSupport != null) {
            this.changeSupport.removePropertyChangeListener(l);
        }
    }

    protected final void firePropertyChange(String name, Object oldValue, Object newValue) {
        if (this.changeSupport != null) {
            this.changeSupport.firePropertyChange(name, oldValue, newValue);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addVetoableChangeListener(VetoableChangeListener l) {
        Object object = listenersMethodLock;
        synchronized (object) {
            if (this.vetoableChangeSupport == null) {
                this.vetoableChangeSupport = new VetoableChangeSupport(this);
            }
        }
        this.vetoableChangeSupport.addVetoableChangeListener(l);
    }

    public void removeVetoableChangeListener(VetoableChangeListener l) {
        if (this.vetoableChangeSupport != null) {
            this.vetoableChangeSupport.removeVetoableChangeListener(l);
        }
    }

    protected final void fireVetoableChange(String name, Object oldValue, Object newValue) throws PropertyVetoException {
        if (this.vetoableChangeSupport != null) {
            this.vetoableChangeSupport.fireVetoableChange(name, oldValue, newValue);
        }
    }

    public <T extends Node.Cookie> T getCookie(Class<T> c) {
        if (c.isInstance(this)) {
            return (T)((Node.Cookie)c.cast(this));
        }
        return null;
    }

    public Lookup getLookup() {
        Class<?> c = this.getClass();
        if (warnedClasses.add(c)) {
            LOG.warning("Should override getLookup() in " + c + ", e.g.: [MultiDataObject.this.]getCookieSet().getLookup()");
        }
        if (this.isValid()) {
            return this.getNodeDelegateImpl().getLookup();
        }
        return this.createNodeDelegate().getLookup();
    }

    protected <T extends Node.Cookie> T getCookie(DataShadow shadow, Class<T> clazz) {
        return this.getCookie(clazz);
    }

    public Object writeReplace() {
        return new Replace(this);
    }

    static String getString(String name) {
        return NbBundle.getMessage(DataObject.class, (String)name);
    }

    void recognizedByFolder() {
    }

    void notifyFileRenamed(FileRenameEvent fe) {
        if (fe.getFile().equals((Object)this.getPrimaryFile())) {
            this.firePropertyChange("name", fe.getName(), this.getName());
        }
    }

    void notifyFileDeleted(FileEvent fe) {
    }

    void notifyFileChanged(FileEvent fe) {
    }

    void notifyFileDataCreated(FileEvent fe) {
    }

    void notifyAttributeChanged(FileAttributeEvent fae) {
        String attrFromFO;
        if (!"NetBeansAttrAssignedLoader".equals(fae.getName())) {
            return;
        }
        FileObject f = fae.getFile();
        if (!(f == null || (attrFromFO = (String)f.getAttribute("NetBeansAttrAssignedLoader")) != null && attrFromFO.equals(this.getLoader().getClass().getName()))) {
            HashSet<FileObject> single = new HashSet<FileObject>();
            single.add(f);
            if (!DataObjectPool.getPOOL().revalidate(single).isEmpty()) {
                LOG.info("It was not possible to invalidate data object: " + this);
            } else {
                FolderList.changedDataSystem(f.getParent());
            }
        }
    }

    static ProgressInfo getProgressInfo() {
        return PROGRESS_INFO_TL.get();
    }

    static ProgressInfo initProgressInfo(String name, DataObject root) {
        assert (PROGRESS_INFO_TL.get() == null);
        ProgressInfo pi = new ProgressInfo(name, root);
        PROGRESS_INFO_TL.set(pi);
        OBJ_LOG.log(Level.FINEST, "ProgressInfo init: {0}", name);
        return pi;
    }

    static void finishProgressInfoIfDone(ProgressInfo pi, DataObject dob) {
        assert (PROGRESS_INFO_TL.get() == null || PROGRESS_INFO_TL.get() == pi);
        if (pi.finishIfDone(dob)) {
            PROGRESS_INFO_TL.remove();
        }
    }

    static boolean isCurrentActionTerminated() {
        ProgressInfo pi = DataObject.getProgressInfo();
        return pi != null && pi.isTerminated();
    }

    static {
        DataObjectAccessor.DEFAULT = new DataObjectAccessorImpl();
        REGISTRY_INSTANCE = new Registry();
        warnedClasses = Collections.synchronizedSet(new WeakSet());
    }

    static class ProgressInfo {
        private final int NAME_LEN_LIMIT = 128;
        private final ProgressHandle progressHandle;
        private final AtomicBoolean terminated = new AtomicBoolean();
        private final DataObject root;

        public ProgressInfo(String name, DataObject root) {
            Cancellable can = root instanceof DataFolder ? new Cancellable(){

                public boolean cancel() {
                    ProgressInfo.this.terminated.set(true);
                    return true;
                }
            } : null;
            ProgressHandle ph = ProgressHandleFactory.createHandle((String)name, (Cancellable)can);
            ph.setInitialDelay(500);
            ph.start();
            this.progressHandle = ph;
            this.root = root;
        }

        public void updateProgress(DataObject dob) {
            OBJ_LOG.log(Level.FINEST, "Update ProgressInfo: {0}", dob);
            String displayName = dob.getPrimaryFile() == null ? dob.getName() : dob.getPrimaryFile().getPath();
            if (displayName != null && displayName.length() > 128) {
                displayName = "..." + displayName.substring(displayName.length() - 128 + 3, displayName.length());
            }
            this.progressHandle.progress(displayName);
        }

        public void terminate() {
            this.terminated.set(true);
        }

        public boolean isTerminated() {
            return this.terminated.get();
        }

        public boolean finishIfDone(DataObject currentFile) {
            if (currentFile == this.root) {
                this.progressHandle.finish();
                return true;
            }
            return false;
        }

    }

    static final class CreateAction
    implements FileSystem.AtomicAction {
        public DataObject result;
        private String name;
        private DataFolder f;
        private DataObject orig;
        private Map<String, ? extends Object> param;
        private static ThreadLocal<CreateAction> CURRENT = new ThreadLocal();

        public CreateAction(DataObject orig, DataFolder f, String name, Map<String, ? extends Object> param) {
            this.orig = orig;
            this.f = f;
            this.name = name;
            this.param = param;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void run() throws IOException {
            DataFolder prevFold = DataObjectEncodingQueryImplementation.enterIgnoreTargetFolder(this.f);
            CreateAction prev = CURRENT.get();
            try {
                CURRENT.set(this);
                this.result = this.orig.handleCreateFromTemplate(this.f, this.name);
            }
            finally {
                DataObjectEncodingQueryImplementation.exitIgnoreTargetFolder(prevFold);
                CURRENT.set(prev);
            }
        }

        public static Map<String, Object> findParameters(String name) {
            CreateAction c = CURRENT.get();
            if (c == null) {
                return Collections.emptyMap();
            }
            HashMap<String, Object> all = new HashMap<String, Object>();
            for (CreateFromTemplateAttributesProvider provider : Lookup.getDefault().lookupAll(CreateFromTemplateAttributesProvider.class)) {
                Map map = provider.attributesFor(c.orig, c.f, c.name);
                if (map == null) continue;
                for (Map.Entry e : map.entrySet()) {
                    all.put(e.getKey(), e.getValue());
                }
            }
            if (c.param != null) {
                for (Map.Entry e : c.param.entrySet()) {
                    all.put((String)e.getKey(), e.getValue());
                }
            }
            if (!all.containsKey("name") && name != null) {
                if (Boolean.TRUE.equals(all.get("freeFileExtension"))) {
                    name = name.replaceFirst("[.].*", "");
                }
                all.put("name", name);
            }
            if (!all.containsKey("user")) {
                all.put("user", System.getProperty("user.name"));
            }
            Date d = new Date();
            if (!all.containsKey("date")) {
                all.put("date", DateFormat.getDateInstance().format(d));
            }
            if (!all.containsKey("time")) {
                all.put("time", DateFormat.getTimeInstance().format(d));
            }
            if (!all.containsKey("dateTime")) {
                all.put("dateTime", d);
            }
            return Collections.unmodifiableMap(all);
        }

        public static Map<String, Object> enhanceParameters(Map<String, Object> old, String name, String ext) {
            HashMap<String, Object> all = new HashMap<String, Object>(old);
            if (!all.containsKey("nameAndExt") && name != null) {
                if (!(ext == null || ext.length() <= 0 || Boolean.TRUE.equals(old.get("freeFileExtension")) && name.indexOf(46) != -1)) {
                    all.put("nameAndExt", name + '.' + ext);
                } else {
                    all.put("nameAndExt", name);
                }
            }
            return Collections.unmodifiableMap(all);
        }
    }

    private static final class DOSavable
    extends AbstractSavable
    implements Icon {
        final DataObject obj;

        public DOSavable(DataObject obj) {
            this.obj = obj;
        }

        public String findDisplayName() {
            return this.obj.getNodeDelegate().getDisplayName();
        }

        protected void handleSave() throws IOException {
            SaveCookie sc = this.obj.getCookie(SaveCookie.class);
            if (sc != null) {
                sc.save();
            }
        }

        public boolean equals(Object other) {
            if (other instanceof DOSavable) {
                DOSavable dos = (DOSavable)other;
                return this.obj.equals(dos.obj);
            }
            return false;
        }

        public int hashCode() {
            return this.obj.hashCode();
        }

        final void remove() {
            this.unregister();
        }

        final void add() {
            this.register();
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            this.icon().paintIcon(c, g, x, y);
        }

        @Override
        public int getIconWidth() {
            return this.icon().getIconWidth();
        }

        @Override
        public int getIconHeight() {
            return this.icon().getIconHeight();
        }

        private Icon icon() {
            return ImageUtilities.image2Icon((Image)this.obj.getNodeDelegate().getIcon(1));
        }
    }

    private static final class ModifiedRegistry
    extends HashSet<DataObject> {
        static final long serialVersionUID = -2861723614638919680L;
        private static final Logger REGLOG = Logger.getLogger("org.openide.loaders.DataObject.Registry");
        private final ChangeSupport cs;

        ModifiedRegistry() {
            this.cs = new ChangeSupport((Object)this);
        }

        public final void addChangeListener(ChangeListener chl) {
            this.cs.addChangeListener(chl);
        }

        public final void removeChangeListener(ChangeListener chl) {
            this.cs.removeChangeListener(chl);
        }

        @Override
        public boolean add(DataObject o) {
            boolean result = HashSet.super.add(o);
            REGLOG.log(Level.FINER, "Data Object {0} modified, change {1}", new Object[]{o, result});
            if (result) {
                this.cs.fireChange();
            }
            return result;
        }

        @Override
        public boolean remove(Object o) {
            boolean result = HashSet.super.remove(o);
            REGLOG.log(Level.FINER, "Data Object {0} unmodified, change {1}", new Object[]{o, result});
            if (result) {
                this.cs.fireChange();
            }
            return result;
        }
    }

    public static final class Registry {
        private Registry() {
        }

        public void addChangeListener(ChangeListener chl) {
            modified.addChangeListener(chl);
        }

        public void removeChangeListener(ChangeListener chl) {
            modified.removeChangeListener(chl);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Set<DataObject> getModifiedSet() {
            Set set = syncModified;
            synchronized (set) {
                HashSet<DataObject> set2 = new HashSet<DataObject>(syncModified);
                return set2;
            }
        }

        public DataObject[] getModified() {
            return this.getModifiedSet().toArray(new DataObject[0]);
        }
    }

    @Retention(value=RetentionPolicy.SOURCE)
    @Target(value={ElementType.TYPE})
    public static @interface Registrations {
        public Registration[] value();
    }

    @Retention(value=RetentionPolicy.SOURCE)
    @Target(value={ElementType.TYPE})
    public static @interface Registration {
        public String mimeType();

        public String displayName() default "";

        public String iconBase() default "";

        public int position() default Integer.MAX_VALUE;
    }

    public static interface Container
    extends Node.Cookie {
        public static final String PROP_CHILDREN = "children";

        public DataObject[] getChildren();

        public void addPropertyChangeListener(PropertyChangeListener var1);

        public void removePropertyChangeListener(PropertyChangeListener var1);
    }

    public static interface Factory {
        public DataObject findDataObject(FileObject var1, Set<? super FileObject> var2) throws IOException;
    }

    private static final class Replace
    implements Serializable {
        private FileObject fo;
        private transient DataObject obj;
        private static final long serialVersionUID = -627843044348243058L;

        public Replace(DataObject obj) {
            this.obj = obj;
            this.fo = obj.getPrimaryFile();
        }

        public Object readResolve() {
            return this.obj;
        }

        private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
            ois.defaultReadObject();
            if (this.fo == null) {
                throw new FileNotFoundException();
            }
            this.obj = DataObject.find(this.fo);
        }
    }

}

