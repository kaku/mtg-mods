/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Enumerations
 */
package org.openide.loaders;

import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.WeakHashMap;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.FolderComparator;
import org.openide.loaders.FolderList;
import org.openide.util.Enumerations;

final class FolderOrder
implements Comparator<Object> {
    private static final WeakHashMap<FileObject, Reference<FolderOrder>> map = new WeakHashMap(101);
    private static final Map<FileObject, Object> knownOrders = Collections.synchronizedMap(new WeakHashMap(50));
    private Map<String, Integer> order;
    private FileObject folder;
    private DataFolder.SortMode sortMode;
    private Object previous;

    private FolderOrder(FileObject folder) {
        this.folder = folder;
    }

    public void setSortMode(DataFolder.SortMode mode) throws IOException {
        this.sortMode = mode;
        mode.write(this.folder);
    }

    public DataFolder.SortMode getSortMode() {
        if (this.sortMode == null) {
            this.sortMode = DataFolder.SortMode.read(this.folder);
        }
        return this.sortMode;
    }

    public synchronized void setOrder(DataObject[] arr) throws IOException {
        if (arr != null) {
            this.order = new HashMap<String, Integer>(arr.length * 4 / 3 + 1);
            Enumeration en = Enumerations.removeDuplicates((Enumeration)Enumerations.array((Object[])arr));
            int i = 0;
            while (en.hasMoreElements()) {
                DataObject obj = (DataObject)en.nextElement();
                FileObject fo = obj.getPrimaryFile();
                if (!this.folder.equals((Object)fo.getParent())) continue;
                this.order.put(fo.getNameExt(), i++);
            }
        } else {
            this.order = null;
        }
        this.write();
    }

    @Override
    public int compare(Object obj1, Object obj2) {
        Integer i2;
        Integer i1 = this.order == null ? null : this.order.get(FolderComparator.findFileObject(obj1).getNameExt());
        Integer n = i2 = this.order == null ? null : this.order.get(FolderComparator.findFileObject(obj2).getNameExt());
        if (i1 == null) {
            if (i2 != null) {
                return 1;
            }
            return ((FolderComparator)this.getSortMode()).doCompare(obj1, obj2);
        }
        if (i2 == null) {
            return -1;
        }
        if (i1.intValue() == i2.intValue()) {
            return 0;
        }
        if (i1 < i2) {
            return -1;
        }
        return 1;
    }

    public void write() throws IOException {
        if (this.folder.getAttribute("OpenIDE-Folder-Order") != null) {
            this.folder.setAttribute("OpenIDE-Folder-Order", (Object)null);
        }
        if (this.order != null) {
            FileObject[] children = new FileObject[this.order.size()];
            for (Map.Entry<String, Integer> entry : this.order.entrySet()) {
                children[entry.getValue().intValue()] = this.folder.getFileObject(entry.getKey());
            }
            FileUtil.setOrder(Arrays.asList(children));
        }
    }

    private void read() {
        Object o = this.folder.getAttribute("OpenIDE-Folder-Order");
        if (this.previous == null && o == null || this.previous != null && this.previous.equals(o)) {
            return;
        }
        if (o instanceof Object[] && this.previous instanceof Object[] && FolderOrder.compare((Object[])o, (Object[])this.previous)) {
            return;
        }
        this.doRead(o);
        this.previous = o;
        if (this.previous != null) {
            knownOrders.put(this.folder, this.previous);
        }
        FolderList.changedFolderOrder(this.folder);
    }

    private static boolean compare(Object[] a, Object[] b) {
        Object[] arr;
        if (a == b) {
            return true;
        }
        int len = Math.min(a.length, b.length);
        for (int i = 0; i < len; ++i) {
            if (a[i] == b[i]) continue;
            if (a[i] == null) {
                return false;
            }
            if (a[i].equals(b[i])) continue;
            if (a[i] instanceof Object[] && b[i] instanceof Object[]) {
                if (FolderOrder.compare((Object[])a[i], (Object[])b[i])) continue;
                return false;
            }
            return false;
        }
        Object[] arrobject = arr = a.length > b.length ? a : b;
        if (FolderOrder.checkNonNull(arr, len)) {
            return false;
        }
        return true;
    }

    private static boolean checkNonNull(Object[] a, int from) {
        for (int i = from; i < a.length; ++i) {
            if (a[i] == null) continue;
            return true;
        }
        return false;
    }

    private void doRead(Object o) {
        if (o == null) {
            this.order = null;
            return;
        }
        if (o instanceof String[][]) {
            String[][] namesExts = (String[][])o;
            if (namesExts.length != 2) {
                this.order = null;
                return;
            }
            String[] names = namesExts[0];
            String[] exts = namesExts[1];
            if (names == null || exts == null || names.length != exts.length) {
                this.order = null;
                return;
            }
            HashMap<String, Integer> set = new HashMap<String, Integer>(names.length);
            for (int i = 0; i < names.length; ++i) {
                set.put(names[i], i);
            }
            this.order = set;
            return;
        }
        if (o instanceof String) {
            String sepnames = (String)o;
            HashMap<String, Integer> set = new HashMap<String, Integer>();
            StringTokenizer tok = new StringTokenizer(sepnames, "/");
            int i = 0;
            while (tok.hasMoreTokens()) {
                String file = tok.nextToken();
                set.put(file, i);
                ++i;
            }
            this.order = set;
            return;
        }
        this.order = null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static FolderOrder findFor(FileObject folder) {
        FolderOrder order = null;
        Object object = map;
        synchronized (object) {
            Reference<FolderOrder> ref = map.get((Object)folder);
            FolderOrder folderOrder = order = ref == null ? null : ref.get();
            if (order == null) {
                order = new FolderOrder(folder);
                order.previous = knownOrders.get((Object)folder);
                order.doRead(order.previous);
                map.put(folder, new SoftReference<FolderOrder>(order));
            }
        }
        object = order;
        synchronized (object) {
            order.read();
            return order;
        }
    }
}

