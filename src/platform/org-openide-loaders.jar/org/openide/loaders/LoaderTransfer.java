/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.datatransfer.ExTransferable
 *  org.openide.util.datatransfer.ExTransferable$Single
 *  org.openide.util.datatransfer.MultiTransferObject
 */
package org.openide.loaders;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.loaders.DataObject;
import org.openide.util.datatransfer.ExTransferable;
import org.openide.util.datatransfer.MultiTransferObject;

public abstract class LoaderTransfer {
    public static final int DND_NONE = 0;
    public static final int DND_COPY = 1;
    public static final int DND_MOVE = 2;
    public static final int DND_COPY_OR_MOVE = 3;
    public static final int DND_LINK = 1073741824;
    public static final int DND_REFERENCE = 1073741824;
    public static final int CLIPBOARD_COPY = 1;
    public static final int CLIPBOARD_CUT = 4;
    public static final int COPY = 1;
    public static final int MOVE = 6;
    private static MessageFormat dndMimeType = new MessageFormat("application/x-java-openide-dataobjectdnd;class=org.openide.loaders.DataObject;mask={0}");

    private LoaderTransfer() {
    }

    public static ExTransferable.Single transferable(final DataObject d, int actions) {
        return new ExTransferable.Single(LoaderTransfer.createDndFlavor(actions)){

            public Object getData() {
                return d;
            }
        };
    }

    public static DataObject getDataObject(Transferable t, int action) {
        DataFlavor[] flavors = t.getTransferDataFlavors();
        if (flavors == null) {
            return null;
        }
        int len = flavors.length;
        String subtype = "x-java-openide-dataobjectdnd";
        String primary = "application";
        String mask = "mask";
        for (int i = 0; i < len; ++i) {
            DataFlavor df = flavors[i];
            if (!df.getSubType().equals(subtype) || !df.getPrimaryType().equals(primary)) continue;
            try {
                int m = Integer.valueOf(df.getParameter(mask));
                if ((m & action) == 0) continue;
                DataObject o = (DataObject)t.getTransferData(df);
                if (o.isValid()) {
                    return o;
                }
                return null;
            }
            catch (NumberFormatException nfe) {
                LoaderTransfer.maybeReportException(nfe);
                continue;
            }
            catch (ClassCastException cce) {
                LoaderTransfer.maybeReportException(cce);
                continue;
            }
            catch (IOException ioe) {
                DataObject.LOG.fine("Object in clipboard refers to a non existing file. " + ioe.toString());
                continue;
            }
            catch (UnsupportedFlavorException ufe) {
                LoaderTransfer.maybeReportException(ufe);
            }
        }
        return null;
    }

    public static DataObject[] getDataObjects(Transferable t, int action) {
        try {
            if (t.isDataFlavorSupported(ExTransferable.multiFlavor)) {
                MultiTransferObject mto = (MultiTransferObject)t.getTransferData(ExTransferable.multiFlavor);
                int count = mto.getCount();
                ArrayList<DataObject> datas = new ArrayList<DataObject>(1);
                boolean ok = true;
                for (int i = 0; i < count; ++i) {
                    DataObject d = LoaderTransfer.getDataObject(mto.getTransferableAt(i), action);
                    if (d == null) {
                        ok = false;
                        break;
                    }
                    if (datas.contains(d)) continue;
                    datas.add(d);
                }
                if (ok && !datas.isEmpty()) {
                    return datas.toArray(new DataObject[0]);
                }
            } else {
                DataObject d = LoaderTransfer.getDataObject(t, action);
                if (d != null) {
                    return new DataObject[]{d};
                }
            }
        }
        catch (ClassCastException cce) {
            LoaderTransfer.maybeReportException(cce);
        }
        catch (IOException ioe) {
            LoaderTransfer.maybeReportException(ioe);
        }
        catch (UnsupportedFlavorException ufe) {
            LoaderTransfer.maybeReportException(ufe);
        }
        return null;
    }

    private static DataFlavor createDndFlavor(int actions) {
        try {
            return new DataFlavor(dndMimeType.format(new Object[]{new Integer(actions)}), null, DataObject.class.getClassLoader());
        }
        catch (ClassNotFoundException ex) {
            throw new AssertionError(ex);
        }
    }

    private static void maybeReportException(Exception e) {
        Logger.getLogger(LoaderTransfer.class.getName()).log(Level.WARNING, null, e);
    }

}

