/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$AsynchronousInstantiatingIterator
 *  org.openide.WizardDescriptor$BackgroundInstantiatingIterator
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.WizardDescriptor$ProgressInstantiatingIterator
 */
package org.openide.loaders;

import java.io.IOException;
import java.util.Set;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.WizardDescriptor;
import org.openide.loaders.DataObject;
import org.openide.loaders.TemplateWizard;
import org.openide.loaders.TemplateWizardIterImpl;

class TemplateWizardIteratorWrapper
implements WizardDescriptor.AsynchronousInstantiatingIterator<WizardDescriptor>,
ChangeListener {
    protected TemplateWizardIterImpl iterImpl;

    TemplateWizardIteratorWrapper(TemplateWizardIterImpl iterImpl) {
        this.iterImpl = iterImpl;
    }

    public TemplateWizardIterImpl getOriginalIterImpl() {
        return this.iterImpl;
    }

    public void first() {
        this.iterImpl.first();
    }

    public void setIterator(TemplateWizard.Iterator it, boolean notify) {
        this.iterImpl.setIterator(it, notify);
    }

    public TemplateWizard.Iterator getIterator() {
        return this.iterImpl.getIterator();
    }

    public WizardDescriptor.Panel<WizardDescriptor> current() {
        return this.iterImpl.current();
    }

    public String name() {
        return this.iterImpl.name();
    }

    public boolean hasNext() {
        return this.iterImpl.hasNext();
    }

    public boolean hasPrevious() {
        return this.iterImpl.hasPrevious();
    }

    public void nextPanel() {
        this.iterImpl.nextPanel();
    }

    public void previousPanel() {
        this.iterImpl.previousPanel();
    }

    @Override
    public void stateChanged(ChangeEvent p1) {
        this.iterImpl.stateChanged(p1);
    }

    public void addChangeListener(ChangeListener listener) {
        this.iterImpl.addChangeListener(listener);
    }

    public void removeChangeListener(ChangeListener listener) {
        this.iterImpl.removeChangeListener(listener);
    }

    public void initialize(WizardDescriptor wiz) {
        this.iterImpl.initialize(wiz);
    }

    public void uninitialize(WizardDescriptor wiz) {
        this.iterImpl.uninitialize();
    }

    public Set<DataObject> instantiate() throws IOException {
        return this.iterImpl.instantiate();
    }

    static class ProgressInstantiatingIterator
    extends TemplateWizardIteratorWrapper
    implements WizardDescriptor.ProgressInstantiatingIterator<WizardDescriptor> {
        ProgressInstantiatingIterator(TemplateWizardIterImpl it) {
            super(it);
        }

        public Set<DataObject> instantiate(ProgressHandle handle) throws IOException {
            return this.iterImpl.instantiate(handle);
        }
    }

    static class BackgroundInstantiatingIterator
    extends TemplateWizardIteratorWrapper
    implements WizardDescriptor.BackgroundInstantiatingIterator<WizardDescriptor> {
        BackgroundInstantiatingIterator(TemplateWizardIterImpl it) {
            super(it);
        }
    }

    static class AsynchronousInstantiatingIterator
    extends TemplateWizardIteratorWrapper
    implements WizardDescriptor.AsynchronousInstantiatingIterator<WizardDescriptor> {
        AsynchronousInstantiatingIterator(TemplateWizardIterImpl it) {
            super(it);
        }
    }

}

