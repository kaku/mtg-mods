/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.openide.loaders;

import java.io.Serializable;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFilterAll;
import org.openide.loaders.DataObject;

public interface DataFilter
extends Serializable {
    public static final DataFilter ALL = new DataFilterAll();
    @Deprecated
    public static final long serialVersionUID = 0;

    public boolean acceptDataObject(DataObject var1);

    public static interface FileBased
    extends DataFilter {
        public boolean acceptFileObject(FileObject var1);
    }

}

