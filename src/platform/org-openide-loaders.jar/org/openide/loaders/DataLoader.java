/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.nodes.NodeOp
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.SharedClassObject
 *  org.openide.util.Utilities
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.io.SafeException
 */
package org.openide.loaders;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataLdrActions;
import org.openide.loaders.DataLoaderPool;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectPool;
import org.openide.nodes.NodeOp;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.SharedClassObject;
import org.openide.util.Utilities;
import org.openide.util.actions.SystemAction;
import org.openide.util.io.SafeException;

public abstract class DataLoader
extends SharedClassObject
implements DataObject.Factory {
    static final Logger ERR = Logger.getLogger("org.openide.loaders.DataLoader");
    private static final long serialVersionUID = 1986614061378346169L;
    public static final String PROP_DISPLAY_NAME = "displayName";
    public static final String PROP_ACTIONS = "actions";
    private static final String PROP_DEF_ACTIONS = "defaultActions";
    private static final Object ACTION_MANAGER = new Object();
    private static final Object PROP_REPRESENTATION_CLASS = new Object();
    private static final Object PROP_REPRESENTATION_CLASS_NAME = new Object();
    private static final int LOADER_VERSION = 1;

    @Deprecated
    protected DataLoader(Class<? extends DataObject> representationClass) {
        this.putProperty(PROP_REPRESENTATION_CLASS, representationClass);
        this.putProperty(PROP_REPRESENTATION_CLASS_NAME, (Object)representationClass.getName());
        if (representationClass.getClassLoader() == this.getClass().getClassLoader()) {
            ERR.warning("Use of super(" + representationClass.getName() + ".class) in " + this.getClass().getName() + "() should be replaced with super(\"" + representationClass.getName() + "\") to reduce unnecessary class loading");
        }
    }

    protected DataLoader(String representationClassName) {
        this.putProperty(PROP_REPRESENTATION_CLASS_NAME, (Object)representationClassName);
        assert (this.getRepresentationClass() != null);
    }

    public final Class<? extends DataObject> getRepresentationClass() {
        Class<DataObject> cls;
        Class _cls = (Class)this.getProperty(PROP_REPRESENTATION_CLASS);
        if (_cls != null) {
            return _cls.asSubclass(DataObject.class);
        }
        String clsName = (String)this.getProperty(PROP_REPRESENTATION_CLASS_NAME);
        try {
            cls = Class.forName(clsName, false, this.getClass().getClassLoader()).asSubclass(DataObject.class);
        }
        catch (ClassNotFoundException cnfe) {
            throw (IllegalStateException)new IllegalStateException("Failed to load " + clsName + " from " + this.getClass().getClassLoader()).initCause(cnfe);
        }
        this.putProperty(PROP_REPRESENTATION_CLASS, cls);
        return cls;
    }

    public final String getRepresentationClassName() {
        return (String)this.getProperty(PROP_REPRESENTATION_CLASS_NAME);
    }

    public final SystemAction[] getActions() {
        Action[] arr = this.getSwingActions();
        ArrayList<SystemAction> list = new ArrayList<SystemAction>();
        for (int i = 0; i < arr.length; ++i) {
            if (!(arr[i] instanceof SystemAction) && arr[i] != null) continue;
            list.add((SystemAction)arr[i]);
        }
        return list.toArray((T[])new SystemAction[list.size()]);
    }

    final Action[] getSwingActions() {
        DataLdrActions mgr = this.findManager();
        if (mgr != null) {
            Action[] actions;
            try {
                actions = (Action[])mgr.instanceCreate();
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                actions = null;
            }
            catch (ClassNotFoundException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                actions = null;
            }
            if (actions == null) {
                return new Action[0];
            }
            return actions;
        }
        SystemAction[] actions = (SystemAction[])this.getProperty((Object)"actions");
        if (actions == null && (actions = (SystemAction[])this.getProperty((Object)"defaultActions")) == null) {
            actions = this.defaultActions();
            this.putProperty("defaultActions", (Object)actions, false);
        }
        return actions;
    }

    protected String actionsContext() {
        return null;
    }

    @Deprecated
    protected SystemAction[] defaultActions() {
        SystemAction[] actions = NodeOp.getDefaultActions();
        return actions;
    }

    private final DataLdrActions findManager() {
        Object manager = this.getProperty(ACTION_MANAGER);
        if (manager instanceof Class) {
            return null;
        }
        DataLdrActions mgr = (DataLdrActions)((Object)manager);
        boolean newlyCreated = false;
        if (mgr == null) {
            SystemAction[] arr;
            String context = this.actionsContext();
            if (context == null) {
                this.putProperty(ACTION_MANAGER, this.getClass());
                return null;
            }
            FileObject fo = FileUtil.getConfigFile((String)context);
            if (fo == null) {
                fo = FileUtil.getConfigRoot();
                try {
                    fo = FileUtil.createFolder((FileObject)fo, (String)context);
                }
                catch (IOException ex) {
                    ERR.log(Level.WARNING, null, ex);
                }
                newlyCreated = true;
            }
            mgr = new DataLdrActions(DataFolder.findFolder(fo), this);
            if (newlyCreated && (arr = this.defaultActions()) != null) {
                mgr.setActions(arr);
            }
            this.putProperty(ACTION_MANAGER, (Object)mgr);
        }
        return mgr;
    }

    final void waitForActions() {
        DataLdrActions mgr = this.findManager();
        if (mgr != null) {
            mgr.waitFinished();
        }
    }

    public final void setActions(SystemAction[] actions) {
        DataLdrActions mgr = this.findManager();
        if (mgr != null) {
            mgr.setActions(actions);
        } else {
            this.putProperty("actions", (Object)actions, true);
        }
    }

    final void setSwingActions(List arr) {
        this.firePropertyChange("actions", (Object)null, (Object)null);
    }

    public final String getDisplayName() {
        String dn = (String)this.getProperty((Object)"displayName");
        if (dn != null) {
            return dn;
        }
        dn = this.defaultDisplayName();
        if (dn != null) {
            return dn;
        }
        return this.getRepresentationClassName();
    }

    protected final void setDisplayName(String displayName) {
        this.putProperty("displayName", (Object)displayName, true);
    }

    protected String defaultDisplayName() {
        return NbBundle.getBundle(DataLoader.class).getString("LBL_loader_display_name");
    }

    @Override
    public final DataObject findDataObject(FileObject fo, Set<? super FileObject> recognized) throws IOException {
        class Rec
        implements RecognizedFiles {
            final /* synthetic */ Set val$recognized;

            Rec() {
                this.val$recognized = var2_2;
            }

            @Override
            public void markRecognized(FileObject fo) {
                this.val$recognized.add(fo);
            }
        }
        RecognizedFiles rec = recognized == DataLoaderPool.emptyDataLoaderRecognized ? DataLoaderPool.emptyDataLoaderRecognized : new Rec(this, recognized);
        return this.findDataObject(fo, rec);
    }

    public final DataObject findDataObject(FileObject fo, RecognizedFiles recognized) throws IOException {
        try {
            return DataObjectPool.handleFindDataObject(this, fo, recognized);
        }
        catch (IOException ioe) {
            throw ioe;
        }
        catch (ThreadDeath td) {
            throw td;
        }
        catch (RuntimeException e) {
            if (e.getClass().getName().startsWith("org.openide.util.lookup")) {
                throw e;
            }
            IOException ioe = new IOException(e.toString());
            Logger.getLogger(DataLoader.class.getName()).log(Level.WARNING, null, e);
            ioe.initCause(e);
            throw ioe;
        }
    }

    protected abstract DataObject handleFindDataObject(FileObject var1, RecognizedFiles var2) throws IOException;

    public final void markFile(FileObject fo) throws IOException {
        DataLoaderPool.setPreferredLoader(fo, this);
    }

    public void writeExternal(ObjectOutput oo) throws IOException {
        oo.writeObject(new Integer(1));
        SystemAction[] arr = (SystemAction[])this.getProperty((Object)"actions");
        if (arr == null) {
            oo.writeObject(null);
        } else {
            LinkedList<String> names = new LinkedList<String>();
            for (int i = 0; i < arr.length; ++i) {
                if (arr[i] == null) {
                    names.add(null);
                    continue;
                }
                names.add(arr[i].getClass().getName());
            }
            oo.writeObject(names.toArray());
        }
        String dn = (String)this.getProperty((Object)"displayName");
        if (dn == null) {
            dn = "";
        }
        oo.writeUTF(dn);
    }

    public void readExternal(ObjectInput oi) throws IOException, ClassNotFoundException {
        String displayName;
        ClassNotFoundException main = null;
        int version = 0;
        Object first = oi.readObject();
        if (first instanceof Integer) {
            version = (Integer)first;
            first = oi.readObject();
        }
        Object[] arr = (Object[])first;
        boolean isdefault = true;
        SystemAction[] defactions = this.getActions();
        if (version > 0 || version == 0 && arr.length != defactions.length) {
            isdefault = false;
        }
        if (arr != null) {
            ArrayList<SystemAction> ll = new ArrayList<SystemAction>(arr.length);
            for (int i = 0; i < arr.length; ++i) {
                if (arr[i] == null) {
                    ll.add(null);
                    if (version != 0 || !isdefault || defactions[i] == null) continue;
                    isdefault = false;
                    continue;
                }
                try {
                    ClassLoader loader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
                    if (loader == null) {
                        loader = this.getClass().getClassLoader();
                    }
                    Class<SystemAction> c = Class.forName(Utilities.translate((String)((String)arr[i])), false, loader).asSubclass(SystemAction.class);
                    SystemAction ac = SystemAction.get(c);
                    ll.add(ac);
                    if (version != 0 || !isdefault || defactions[i].equals((Object)ac)) continue;
                    isdefault = false;
                    continue;
                }
                catch (ClassNotFoundException ex) {
                    if (main == null) {
                        main = ex;
                        continue;
                    }
                    Throwable t = main;
                    while (t.getCause() != null) {
                        t = t.getCause();
                    }
                    t.initCause(ex);
                }
            }
            if (main == null && !isdefault) {
                this.setActions(ll.toArray((T[])new SystemAction[ll.size()]));
            }
        }
        if ((displayName = oi.readUTF()).equals("") || version == 0 && displayName.equals(this.defaultDisplayName())) {
            displayName = null;
        }
        this.setDisplayName(displayName);
        if (main != null) {
            SafeException se = new SafeException((Exception)main);
            String message = NbBundle.getMessage(DataLoader.class, (String)"EXC_missing_actions_in_loader", (Object)this.getDisplayName());
            Exceptions.attachLocalizedMessage((Throwable)se, (String)message);
            throw se;
        }
    }

    protected boolean clearSharedData() {
        return false;
    }

    public static <T extends DataLoader> T getLoader(Class<T> loaderClass) {
        return (T)((DataLoader)DataLoader.findObject(loaderClass, (boolean)true));
    }

    public static interface RecognizedFiles {
        public void markRecognized(FileObject var1);
    }

}

