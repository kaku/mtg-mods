/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.actions.CopyAction
 *  org.openide.actions.CutAction
 *  org.openide.actions.DeleteAction
 *  org.openide.actions.PasteAction
 *  org.openide.actions.PropertiesAction
 *  org.openide.actions.ToolsAction
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.URLMapper
 *  org.openide.nodes.Children
 *  org.openide.nodes.CookieSet
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.nodes.PropertySupport
 *  org.openide.nodes.PropertySupport$Name
 *  org.openide.nodes.PropertySupport$ReadWrite
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 */
package org.openide.loaders;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventObject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import org.openide.actions.CopyAction;
import org.openide.actions.CutAction;
import org.openide.actions.DeleteAction;
import org.openide.actions.PasteAction;
import org.openide.actions.PropertiesAction;
import org.openide.actions.ToolsAction;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.URLMapper;
import org.openide.loaders.DataLoaderPool;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.DataShadow;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.loaders.OperationEvent;
import org.openide.nodes.Children;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;

final class BrokenDataShadow
extends MultiDataObject {
    private URL url;
    private static Map<String, Set<Reference<BrokenDataShadow>>> allDataShadows;
    private static final long serialVersionUID = -3046981691235483810L;

    public BrokenDataShadow(FileObject fo, MultiFileLoader loader) throws DataObjectExistsException {
        super(fo, loader);
        try {
            this.url = DataShadow.readURL(fo);
        }
        catch (IOException ex) {
            try {
                this.url = new URL("file", null, "/UNKNOWN");
            }
            catch (MalformedURLException ex2) {
                Logger.getLogger(BrokenDataShadow.class.getName()).log(Level.WARNING, null, ex2);
            }
        }
        BrokenDataShadow.enqueueBrokenDataShadow(this);
    }

    @Override
    public Lookup getLookup() {
        return this.getCookieSet().getLookup();
    }

    static synchronized Map<String, Set<Reference<BrokenDataShadow>>> getDataShadowsSet() {
        if (allDataShadows == null) {
            allDataShadows = new HashMap<String, Set<Reference<BrokenDataShadow>>>();
        }
        return allDataShadows;
    }

    private static synchronized void enqueueBrokenDataShadow(BrokenDataShadow ds) {
        Map<String, Set<Reference<BrokenDataShadow>>> m = BrokenDataShadow.getDataShadowsSet();
        String prim = ds.getUrl().toExternalForm();
        DataShadow.DSWeakReference<BrokenDataShadow> ref = new DataShadow.DSWeakReference<BrokenDataShadow>(ds);
        Set<Reference<BrokenDataShadow>> s = m.get(prim);
        if (s == null) {
            s = Collections.singleton(ref);
            BrokenDataShadow.getDataShadowsSet().put(prim, s);
        } else {
            if (!(s instanceof HashSet)) {
                s = new HashSet<Reference<BrokenDataShadow>>(s);
                BrokenDataShadow.getDataShadowsSet().put(prim, s);
            }
            s.add(ref);
        }
    }

    private static synchronized List<BrokenDataShadow> getAllDataShadows() {
        if (allDataShadows == null || allDataShadows.isEmpty()) {
            return null;
        }
        ArrayList<BrokenDataShadow> ret = new ArrayList<BrokenDataShadow>(allDataShadows.size());
        for (Set<Reference<BrokenDataShadow>> ref : allDataShadows.values()) {
            for (Reference<BrokenDataShadow> r : ref) {
                BrokenDataShadow shadow = r.get();
                if (shadow == null) continue;
                ret.add(shadow);
            }
        }
        return ret;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void checkValidity(EventObject ev) {
        FileObject file;
        DataObject src = null;
        if (ev instanceof OperationEvent) {
            src = ((OperationEvent)ev).getObject();
        }
        if (src != null) {
            file = src.getPrimaryFile();
        } else if (ev instanceof FileEvent) {
            file = ((FileEvent)ev).getFile();
        } else {
            return;
        }
        try {
            if (!file.getFileSystem().isDefault()) {
                return;
            }
        }
        catch (FileStateInvalidException e2) {
            DataObject.LOG.log(Level.WARNING, e2.toString(), (Throwable)e2);
            return;
        }
        Class<BrokenDataShadow> e2 = BrokenDataShadow.class;
        synchronized (BrokenDataShadow.class) {
            String key;
            if (allDataShadows == null || allDataShadows.isEmpty()) {
                // ** MonitorExit[e] (shouldn't be in output)
                return;
            }
            // ** MonitorExit[e] (shouldn't be in output)
            try {
                key = file.getURL().toExternalForm();
            }
            catch (FileStateInvalidException ex) {
                return;
            }
            Set<Reference<BrokenDataShadow>> shadows = null;
            Class<BrokenDataShadow> class_ = BrokenDataShadow.class;
            synchronized (BrokenDataShadow.class) {
                if (allDataShadows == null || allDataShadows.isEmpty()) {
                    // ** MonitorExit[var5_8] (shouldn't be in output)
                    return;
                }
                if (src != null && (shadows = allDataShadows.get(key)) == null) {
                    // ** MonitorExit[var5_8] (shouldn't be in output)
                    return;
                }
                // ** MonitorExit[var5_8] (shouldn't be in output)
                List<BrokenDataShadow> all = BrokenDataShadow.getAllDataShadows();
                if (all == null) {
                    return;
                }
                int size = all.size();
                for (int i = 0; i < size; ++i) {
                    BrokenDataShadow obj = all.get(i);
                    obj.refresh();
                }
                return;
            }
        }
    }

    private BrokenDataShadow(FileObject fo) throws DataObjectExistsException {
        this(fo, DataLoaderPool.getShadowLoader());
    }

    @Override
    public boolean isDeleteAllowed() {
        return this.getPrimaryFile().canWrite();
    }

    public void refresh() {
        try {
            if (URLMapper.findFileObject((URL)this.getUrl()) != null) {
                this.setValid(false);
            }
        }
        catch (PropertyVetoException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isCopyAllowed() {
        return true;
    }

    @Override
    public boolean isMoveAllowed() {
        return this.getPrimaryFile().canWrite();
    }

    @Override
    public boolean isRenameAllowed() {
        return this.getPrimaryFile().canWrite();
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    protected Node createNodeDelegate() {
        return new BrokenShadowNode(this);
    }

    URL getUrl() {
        return this.url;
    }

    private static final class BrokenShadowNode
    extends DataNode {
        private static MessageFormat format;
        private Sheet sheet;
        private static final String ICON_NAME = "org/openide/loaders/brokenShadow.gif";

        public BrokenShadowNode(BrokenDataShadow par) {
            super(par, Children.LEAF);
            this.setIconBaseWithExtension("org/openide/loaders/brokenShadow.gif");
        }

        @Override
        public String getDisplayName() {
            if (format == null) {
                format = new MessageFormat(DataObject.getString("FMT_brokenShadowName"));
            }
            return format.format(this.createArguments());
        }

        @Override
        public Action[] getActions(boolean context) {
            return new Action[]{SystemAction.get(CutAction.class), SystemAction.get(CopyAction.class), SystemAction.get(PasteAction.class), null, SystemAction.get(DeleteAction.class), null, SystemAction.get(ToolsAction.class), SystemAction.get(PropertiesAction.class)};
        }

        public Node.PropertySet[] getPropertySets() {
            if (this.sheet == null) {
                this.sheet = this.cloneSheet();
            }
            return this.sheet.toArray();
        }

        private Sheet cloneSheet() {
            Node.PropertySet[] sets = super.getPropertySets();
            Sheet s = new Sheet();
            for (int i = 0; i < sets.length; ++i) {
                Sheet.Set ss = new Sheet.Set();
                ss.put(sets[i].getProperties());
                ss.setName(sets[i].getName());
                ss.setDisplayName(sets[i].getDisplayName());
                ss.setShortDescription(sets[i].getShortDescription());
                this.modifySheetSet(ss);
                s.put(ss);
            }
            return s;
        }

        private void modifySheetSet(Sheet.Set ss) {
            Object p = ss.remove("name");
            if (p != null) {
                p = new PropertySupport.Name((Node)this);
                ss.put((Node.Property)p);
                p = new Name();
                ss.put((Node.Property)p);
            }
        }

        private Object[] createArguments() {
            return new Object[]{this.getDataObject().getName()};
        }

        private final class Name
        extends PropertySupport.ReadWrite<String> {
            public Name() {
                super("BrokenLink", String.class, DataObject.getString("PROP_brokenShadowOriginalName"), DataObject.getString("HINT_brokenShadowOriginalName"));
            }

            public String getValue() {
                BrokenDataShadow bds = (BrokenDataShadow)BrokenShadowNode.this.getDataObject();
                return bds.getUrl().toExternalForm();
            }

            public void setValue(String newLink) {
                BrokenDataShadow bds = (BrokenDataShadow)BrokenShadowNode.this.getDataObject();
                try {
                    URL u = new URL(newLink);
                    DataShadow.writeOriginal(bds.getPrimaryFile(), u);
                    bds.url = u;
                }
                catch (IOException ex) {
                    throw (IllegalArgumentException)new IllegalArgumentException(ex.toString()).initCause(ex);
                }
                bds.refresh();
            }
        }

    }

}

