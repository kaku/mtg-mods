/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.openide.loaders;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataLoader;
import org.openide.loaders.DataLoaderPool;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.loaders.DataObjectPool;
import org.openide.loaders.FolderListListener;
import org.openide.loaders.FolderOrder;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;

final class FolderList
implements FileChangeListener,
DataObject.Container {
    static final long serialVersionUID = -592616022226761148L;
    private static final int LATER_PRIORITY = 5;
    private static final RequestProcessor PROCESSOR = new RequestProcessor("Folder recognizer");
    private static final Map<FileObject, Reference<FolderList>> map = new WeakHashMap<FileObject, Reference<FolderList>>(101);
    private static int REFRESH_TIME = -1;
    private FileObject folder;
    private volatile transient RequestProcessor.Task refreshTask;
    private volatile transient ComparatorTask comparatorTask;
    private transient Map<FileObject, Reference<DataObject>> primaryFiles = null;
    private transient List<FileObject> order;
    private static final Logger err = Logger.getLogger("org.openide.loaders.FolderList");
    private transient PropertyChangeSupport pcs;
    private transient boolean folderCreated = false;
    private transient FileChangeListener weakFCL;

    private FolderList(FileObject folder, boolean attach) {
        this.weakFCL = FileUtil.weakFileChangeListener((FileChangeListener)this, (Object)null);
        this.folder = folder;
        if (attach) {
            folder.addFileChangeListener(this.weakFCL);
        }
    }

    public final FileObject getPrimaryFile() {
        return this.folder;
    }

    public String toString() {
        return "FolderList{" + (Object)this.folder + "}";
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static FolderList find(FileObject folder, boolean create) {
        FolderList list = null;
        Class<FolderList> class_ = FolderList.class;
        synchronized (FolderList.class) {
            Reference<FolderList> ref = map.get((Object)folder);
            FolderList folderList = list = ref == null ? null : ref.get();
            if (list == null && create) {
                list = new FolderList(folder, true);
                map.put(folder, new SoftReference<FolderList>(list));
            }
            // ** MonitorExit[var3_3] (shouldn't be in output)
            return list;
        }
    }

    public boolean isCreated() {
        return this.folderCreated;
    }

    public static boolean isFolderRecognizerThread() {
        return PROCESSOR.isRequestProcessorThread();
    }

    public static void changedFolderOrder(FileObject folder) {
        FolderList list = FolderList.find(folder, false);
        if (list != null) {
            list.changeComparator();
        }
    }

    public static void changedDataSystem(FileObject folder) {
        FolderList list = FolderList.find(folder, false);
        if (err.isLoggable(Level.FINE)) {
            err.fine("changedDataSystem: " + (Object)folder + " on " + Thread.currentThread());
        }
        if (list != null) {
            list.refresh();
        }
    }

    @Override
    public DataObject[] getChildren() {
        List<DataObject> res = this.getChildrenList();
        if (res == null) {
            return new DataObject[0];
        }
        DataObject[] arr = new DataObject[res.size()];
        res.toArray(arr);
        return arr;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public List<DataObject> getChildrenList() {
        try {
            DataObjectPool.getPOOL().enterPrivilegedProcessor(PROCESSOR);
            ListTask lt = this.getChildrenList(null);
            lt.task.waitFinished();
        }
        finally {
            DataObjectPool.getPOOL().exitPrivilegedProcessor(PROCESSOR);
        }
        assert (lt.result != null);
        return lt.result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void waitProcessingFinished() {
        ComparatorTask t;
        FolderList folderList = this;
        synchronized (folderList) {
            t = this.comparatorTask;
            err.log(Level.FINE, "Waiting for comparator {0}", t);
        }
        if (t != null) {
            t.waitFinished();
        }
        folderList = this;
        synchronized (folderList) {
            t = this.refreshTask;
            err.log(Level.FINE, "Waiting for refresh {0}", t);
        }
        if (t != null) {
            t.waitFinished();
        }
    }

    public RequestProcessor.Task computeChildrenList(FolderListListener filter) {
        return this.getChildrenList((FolderListListener)filter).task;
    }

    private ListTask getChildrenList(FolderListListener filter) {
        ListTask lt = new ListTask(filter);
        int priority = Thread.currentThread().getPriority();
        lt.task = PROCESSOR.post((Runnable)lt, 0, priority);
        return lt;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private synchronized void changeComparator() {
        Object lock;
        boolean log = err.isLoggable(Level.FINE);
        if (log) {
            err.log(Level.FINE, "changeComparator on {0}", (Object)this.folder);
        }
        Object object = lock = new Object();
        synchronized (object) {
            this.comparatorTask = new ComparatorTask(this.comparatorTask, log, lock).post();
        }
    }

    final void assertNullComparator() {
        assert (this.comparatorTask == null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void refresh() {
        if (this.pcs != null) {
            this.pcs.firePropertyChange("refresh", null, null);
        }
        final long now = System.currentTimeMillis();
        final boolean LOG = err.isLoggable(Level.FINE);
        if (LOG) {
            err.fine("refresh on " + (Object)this.folder + " @" + now);
        }
        FolderList folderList = this;
        synchronized (folderList) {
            if (this.refreshTask == null) {
                this.refreshTask = PROCESSOR.post(new Runnable(){

                    @Override
                    public void run() {
                        ComparatorTask t = FolderList.this.comparatorTask;
                        if (t != null) {
                            t.waitFinished();
                        }
                        if (LOG) {
                            err.fine("-- refresh on " + (Object)FolderList.this.folder + ": now=" + now);
                        }
                        if (FolderList.this.primaryFiles != null) {
                            FolderList.this.createBoth(null, true);
                        }
                    }
                }, FolderList.getRefreshTime(), 5);
            } else {
                this.refreshTask.schedule(FolderList.getRefreshTime());
            }
        }
    }

    private static int getRefreshTime() {
        if (REFRESH_TIME >= 0) {
            return REFRESH_TIME;
        }
        String sysProp = System.getProperty("org.openide.loaders.FolderList.refresh.interval");
        if (sysProp != null) {
            try {
                REFRESH_TIME = Integer.parseInt(sysProp);
            }
            catch (NumberFormatException nfe) {
                Logger.getLogger(FolderList.class.getName()).log(Level.WARNING, null, nfe);
            }
        }
        if (REFRESH_TIME < 0) {
            REFRESH_TIME = 10;
        }
        err.fine("getRefreshTime: " + REFRESH_TIME);
        return REFRESH_TIME;
    }

    public void fileChanged(FileEvent fe) {
        FileObject fo;
        boolean LOG = err.isLoggable(Level.FINE);
        if (LOG) {
            err.fine("fileChanged: " + (Object)fe);
        }
        if ((fo = fe.getFile()).isData() && fo.isValid()) {
            DataFolder.SortMode sortMode;
            if (this.primaryFiles != null) {
                try {
                    DataObject obj = DataObject.find(fo);
                    if (!this.primaryFiles.containsKey((Object)obj.getPrimaryFile())) {
                        this.refresh();
                    }
                }
                catch (DataObjectNotFoundException ex) {
                    Logger.getLogger(FolderList.class.getName()).log(Level.WARNING, null, ex);
                }
            }
            if ((sortMode = this.getComparator().getSortMode()) == DataFolder.SortMode.LAST_MODIFIED || sortMode == DataFolder.SortMode.SIZE) {
                this.changeComparator();
            }
        }
    }

    public void fileDeleted(FileEvent fe) {
        boolean LOG = err.isLoggable(Level.FINE);
        if (LOG) {
            err.fine("fileDeleted: " + (Object)fe);
        }
        if (this.primaryFiles == null || this.primaryFiles.containsKey((Object)fe.getFile())) {
            this.refresh();
        }
    }

    public void fileDataCreated(FileEvent fe) {
        boolean LOG = err.isLoggable(Level.FINE);
        if (LOG) {
            err.fine("fileDataCreated: " + (Object)fe);
        }
        this.refresh();
    }

    public void fileFolderCreated(FileEvent fe) {
        boolean LOG = err.isLoggable(Level.FINE);
        if (LOG) {
            err.fine("fileFolderCreated: " + (Object)fe);
        }
        this.refresh();
    }

    public void fileRenamed(FileRenameEvent fe) {
        boolean LOG = err.isLoggable(Level.FINE);
        if (LOG) {
            err.fine("fileRenamed: " + (Object)fe);
        }
        this.refresh();
        this.changeComparator();
    }

    public void fileAttributeChanged(FileAttributeEvent fe) {
        boolean LOG = err.isLoggable(Level.FINE);
        if (LOG) {
            err.fine("fileAttributeChanged: " + (Object)fe);
        }
        if (fe.getFile() == this.folder) {
            if (fe.getName() == null) {
                this.changeComparator();
                return;
            }
            if ("OpenIDE-Folder-Order".equals(fe.getName()) || "OpenIDE-Folder-SortMode".equals(fe.getName())) {
                this.changeComparator();
            }
        }
        if (FileUtil.affectsOrder((FileAttributeEvent)fe)) {
            this.changeComparator();
        }
    }

    private FolderOrder getComparator() {
        return FolderOrder.findFor(this.folder);
    }

    private List<DataObject> getObjects(FolderListListener f) {
        List<DataObject> res;
        boolean LOG = err.isLoggable(Level.FINE);
        if (LOG) {
            err.fine("getObjects on " + (Object)this.folder);
        }
        if (this.primaryFiles == null) {
            res = this.createBoth(f, false);
        } else if (this.order != null) {
            res = this.createObjects(this.order, this.primaryFiles, f);
        } else {
            res = this.createObjects(this.primaryFiles.keySet(), this.primaryFiles, f);
            res = this.carefullySort(res, this.getComparator());
            this.order = FolderList.createOrder(res);
        }
        return res;
    }

    private List<DataObject> carefullySort(List<DataObject> l, FolderOrder c) {
        boolean LOG = err.isLoggable(Level.FINE);
        if (LOG) {
            err.fine("carefullySort on " + (Object)this.folder);
        }
        Collections.sort(l, c);
        LinkedHashMap<FileObject, DataObject> files = new LinkedHashMap<FileObject, DataObject>(l.size());
        for (DataObject d : l) {
            FileObject f = d.getPrimaryFile();
            if (!this.folder.equals((Object)f.getParent())) continue;
            f.removeFileChangeListener(this.weakFCL);
            f.addFileChangeListener(this.weakFCL);
            files.put(f, d);
        }
        if (LOG) {
            err.fine("carefullySort before getOrder");
        }
        List sorted = FileUtil.getOrder(files.keySet(), (boolean)true);
        ArrayList<DataObject> dobs = new ArrayList<DataObject>(sorted.size());
        for (FileObject f : sorted) {
            dobs.add((DataObject)files.get((Object)f));
        }
        return dobs;
    }

    private static List<FileObject> createOrder(List<DataObject> list) {
        int size = list.size();
        ArrayList<FileObject> res = new ArrayList<FileObject>(size);
        for (int i = 0; i < size; ++i) {
            res.add(list.get(i).getPrimaryFile());
        }
        return res;
    }

    private List<DataObject> createObjects(Collection<FileObject> order, Map<FileObject, Reference<DataObject>> map, FolderListListener f) {
        boolean LOG = err.isLoggable(Level.FINE);
        if (LOG) {
            err.fine("createObjects on " + (Object)this.folder);
        }
        int size = order.size();
        ArrayList<DataObject> res = new ArrayList<DataObject>(size);
        for (FileObject fo : order) {
            DataObject obj;
            if (LOG) {
                err.fine("  iterating" + (Object)fo);
            }
            if (!fo.isValid()) {
                if (!LOG) continue;
                err.fine("    not valid, continue");
                continue;
            }
            Reference<DataObject> ref = map.get((Object)fo);
            DataObject dataObject = obj = ref != null ? ref.get() : null;
            if (obj == null) {
                if (LOG) {
                    err.fine("    reference is " + ref + " obj is " + obj);
                }
                try {
                    obj = DataObject.find(fo);
                    ref = new SoftReference<DataObject>(obj);
                    map.put(fo, ref);
                }
                catch (DataObjectNotFoundException ex) {
                    err.log(Level.INFO, null, ex);
                }
            }
            if (obj == null) continue;
            if (LOG) {
                err.fine("    deliver: ref is " + ref + " obj is " + obj);
            }
            if (f == null) {
                res.add(obj);
                continue;
            }
            f.process(obj, res);
        }
        if (f != null) {
            if (LOG) {
                err.fine("  finished: " + res);
            }
            f.finished(res);
        }
        if (LOG) {
            err.fine("createObjects ends on " + (Object)this.folder);
        }
        return res;
    }

    private List<DataObject> createBoth(FolderListListener filter, boolean notify) {
        boolean LOG = err.isLoggable(Level.FINE);
        if (LOG) {
            err.fine("createBoth on " + (Object)this.folder);
        }
        HashMap<FileObject, Reference<DataObject>> file = new HashMap<FileObject, Reference<DataObject>>();
        List all = new ArrayList<DataObject>();
        List res = new ArrayList();
        HashMap remove = this.primaryFiles == null ? new HashMap() : new HashMap<FileObject, Reference<DataObject>>(this.primaryFiles);
        ArrayList<DataObject> add = new ArrayList<DataObject>();
        DataLoaderPool pool = DataLoaderPool.getDefault();
        final HashSet marked = new HashSet();
        DataLoader.RecognizedFiles recog = new DataLoader.RecognizedFiles(){

            @Override
            public void markRecognized(FileObject fo) {
                if (fo != null) {
                    marked.add(fo);
                }
            }
        };
        Enumeration en = this.folder.getChildren(false);
        while (en.hasMoreElements()) {
            DataObject obj;
            FileObject fo = (FileObject)en.nextElement();
            if (marked.contains((Object)fo)) continue;
            try {
                obj = pool.findDataObject(fo, recog);
            }
            catch (DataObjectExistsException ex) {
                obj = ex.getDataObject();
            }
            catch (IOException ex) {
                obj = null;
                Exceptions.printStackTrace((Throwable)ex);
            }
            catch (Throwable td) {
                obj = null;
                err.log(Level.WARNING, "Error recognizing " + (Object)fo, td);
            }
            if (obj == null) continue;
            obj.recognizedByFolder();
            FileObject primary = obj.getPrimaryFile();
            boolean doNotRemovePrimaryFile = false;
            if (!file.containsKey((Object)primary)) {
                boolean goIn;
                boolean bl = goIn = this.primaryFiles == null;
                if (!goIn) {
                    Reference<DataObject> r = this.primaryFiles.get((Object)primary);
                    boolean bl2 = goIn = r == null;
                    if (!goIn) {
                        DataObject obj2 = r.get();
                        boolean bl3 = goIn = obj2 == null || obj2 != obj;
                        if (goIn) {
                            doNotRemovePrimaryFile = true;
                        }
                    }
                }
                if (goIn) {
                    add.add(obj);
                }
                all.add(obj);
                if (filter == null) {
                    res.add((DataObject)obj);
                } else {
                    filter.process(obj, res);
                }
            }
            if (!doNotRemovePrimaryFile) {
                remove.remove((Object)primary);
            }
            file.put(primary, new SoftReference<DataObject>(obj));
        }
        this.primaryFiles = file;
        all = this.carefullySort(all, this.getComparator());
        this.order = FolderList.createOrder(all);
        res = all.size() == res.size() ? all : this.carefullySort(res, this.getComparator());
        if (notify) {
            this.fireChildrenChange(add, remove.keySet());
        }
        if (LOG) {
            err.fine("Notifying filter: " + filter);
        }
        if (filter != null) {
            filter.finished(res);
        }
        return res;
    }

    private void fireChildrenChange(Collection<?> add, Collection<?> removed) {
        if (!(this.pcs == null || add.isEmpty() && removed.isEmpty())) {
            this.pcs.firePropertyChange("children", null, null);
        }
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener l) {
        if (this.pcs != null) {
            this.pcs.removePropertyChangeListener(l);
        }
    }

    @Override
    public synchronized void addPropertyChangeListener(PropertyChangeListener l) {
        if (this.pcs == null) {
            this.pcs = new PropertyChangeSupport(this);
        }
        this.pcs.addPropertyChangeListener(l);
    }

    static {
        FolderListListener.class.hashCode();
    }

    private class ComparatorTask
    implements Runnable {
        private final ComparatorTask previous;
        private final boolean log;
        private final Object lock;
        private RequestProcessor.Task rpTask;

        ComparatorTask(ComparatorTask previous, boolean log, Object lock) {
            this.previous = previous;
            this.log = log;
            this.lock = lock;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            List v;
            List r = null;
            Object object = this.lock;
            synchronized (object) {
                if (this.log) {
                    err.log(Level.FINE, "changeComparator on {0}: previous", (Object)FolderList.this.folder);
                }
                if (this.previous != null) {
                    this.previous.cancelOrWaitFinished();
                }
                if (this != FolderList.this.comparatorTask) {
                    err.log(Level.FINE, "changeComparator on {0}: skipped", (Object)FolderList.this.folder);
                    return;
                }
                if (this.log) {
                    err.log(Level.FINE, "changeComparator on {0}: get old", (Object)FolderList.this.folder);
                }
                if (!(v = FolderList.this.getObjects(null)).isEmpty()) {
                    FolderList.this.order = null;
                    if (this.log) {
                        err.fine("changeComparator: get new");
                    }
                    r = FolderList.this.getObjects(null);
                }
            }
            object = FolderList.this;
            synchronized (object) {
                if (FolderList.this.comparatorTask != this) {
                    err.fine("changeComparator: task changed meanwhile");
                    return;
                }
                FolderList.this.comparatorTask = null;
                err.fine("changeComparator: task set to null");
            }
            if (r != null) {
                if (this.log) {
                    err.fine("changeComparator: fire change");
                }
                FolderList.this.fireChildrenChange(r, v);
            }
        }

        private void cancelOrWaitFinished() {
            if (this.previous != null) {
                this.previous.cancelOrWaitFinished();
            }
            if (!this.rpTask.cancel()) {
                this.rpTask.waitFinished();
            }
        }

        void waitFinished() {
            this.rpTask.waitFinished();
        }

        ComparatorTask post() {
            this.rpTask = PROCESSOR.post((Runnable)this, 0, 1);
            return this;
        }
    }

    private final class ListTask
    implements Runnable {
        private FolderListListener filter;
        public List<DataObject> result;
        public RequestProcessor.Task task;

        public ListTask(FolderListListener filter) {
            this.filter = filter;
        }

        @Override
        public void run() {
            try {
                this.computeResult();
            }
            catch (Error t) {
                err.log(Level.WARNING, "cannot compute data objects for " + (Object)FolderList.this.folder, t);
                throw t;
            }
            catch (RuntimeException ex) {
                err.log(Level.WARNING, "cannot compute data objects for " + (Object)FolderList.this.folder, ex);
                throw ex;
            }
        }

        private void computeResult() {
            boolean LOG = err.isLoggable(Level.FINE);
            if (LOG) {
                err.fine("ListTask.run 1 on " + (Object)FolderList.this.folder);
            }
            if (FolderList.this.comparatorTask != null) {
                FolderList.this.comparatorTask.waitFinished();
            }
            if (FolderList.this.refreshTask != null) {
                FolderList.this.refreshTask.waitFinished();
            }
            err.fine("ListTask.run 2");
            this.result = FolderList.this.getObjects(this.filter);
            assert (this.result != null);
            err.log(Level.FINE, "ListTask.run 3: {0}", this.result);
            FolderList.this.folderCreated = true;
        }

        public String toString() {
            return "ListTask@" + Integer.toHexString(System.identityHashCode(this)) + "[" + (Object)FolderList.this.folder + "]";
        }
    }

}

