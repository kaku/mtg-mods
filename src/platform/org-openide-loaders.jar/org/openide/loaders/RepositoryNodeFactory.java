/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 */
package org.openide.loaders;

import org.openide.loaders.DataFilter;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

@Deprecated
public abstract class RepositoryNodeFactory {
    public static RepositoryNodeFactory getDefault() {
        RepositoryNodeFactory rnf = (RepositoryNodeFactory)Lookup.getDefault().lookup(RepositoryNodeFactory.class);
        if (rnf == null) {
            rnf = new Trivial();
        }
        return rnf;
    }

    protected RepositoryNodeFactory() {
    }

    public abstract Node repository(DataFilter var1);

    private static final class Trivial
    extends RepositoryNodeFactory {
        private Trivial() {
        }

        @Override
        public Node repository(DataFilter f) {
            return new AbstractNode(Children.LEAF);
        }
    }

}

