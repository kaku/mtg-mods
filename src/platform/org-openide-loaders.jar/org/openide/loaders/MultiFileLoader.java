/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.openide.loaders;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataLoader;
import org.openide.loaders.DataLoaderPool;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.loaders.DataObjectPool;
import org.openide.loaders.MultiDataObject;

public abstract class MultiFileLoader
extends DataLoader {
    private static final long serialVersionUID = 1521919955690157343L;

    @Deprecated
    protected MultiFileLoader(Class<? extends DataObject> representationClass) {
        super(representationClass);
    }

    protected MultiFileLoader(String representationClassName) {
        super(representationClassName);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Converted monitor instructions to comments
     * Lifted jumps to return sites
     */
    @Override
    protected final DataObject handleFindDataObject(FileObject fo, DataLoader.RecognizedFiles recognized) throws IOException {
        block36 : {
            if (!fo.isValid()) {
                return null;
            }
            if (!MultiFileLoader.$assertionsDisabled && Thread.holdsLock(DataObjectPool.getPOOL())) {
                throw new AssertionError();
            }
            primary = this.findPrimaryFileImpl(fo);
            if (primary == null) {
                return null;
            }
            willLog = MultiFileLoader.ERR.isLoggable(Level.FINE);
            if (willLog) {
                MultiFileLoader.ERR.log(Level.FINE, "{0} is accepting: {1}", new Object[]{this.getClass().getName(), fo});
            }
            if (primary == fo) ** GOTO lbl29
            if (willLog) {
                MultiFileLoader.ERR.log(Level.FINE, "checking correctness: primary is different than provided file: {0} fo: {1}", new Object[]{primary, fo});
            }
            en = DataLoaderPool.getDefault().allLoaders(primary);
            do lbl-1000: // 3 sources:
            {
                if ((l = en.nextElement()) == this) {
                    MultiFileLoader.ERR.fine("ok, consistent");
                    break block36;
                }
                if (!(l instanceof MultiFileLoader) || (ml = (MultiFileLoader)l).findPrimaryFile(primary) != primary) ** GOTO lbl-1000
                if (willLog) {
                    MultiFileLoader.ERR.log(Level.FINE, "loader seems to also take care of the file: {0}", ml);
                }
                try {
                    snd = ml.findDataObject(primary, recognized);
                    continue;
                }
                catch (DataObjectExistsException ex) {
                    snd = ex.getDataObject();
                }
            } while (snd == null);
            return null;
        }
        try {
            obj = this.createMultiObject(primary);
            ml = DataObjectPool.getPOOL();
            // MONITORENTER : ml
            originalItem = obj.item();
            // MONITOREXIT : ml
            if (willLog) {
                MultiFileLoader.ERR.log(Level.FINE, "{0} created object for: {1} obj: {2}", new Object[]{this.getClass().getName(), fo, obj});
            }
            if (obj == null) {
                throw new IOException("Loader: " + this + " returned null from createMultiObject(" + (Object)primary + ")");
            }
        }
        catch (DataObjectExistsException ex) {
            if (willLog) {
                MultiFileLoader.ERR.log(Level.FINE, "{0} get existing data object for: {1}", new Object[]{this.getClass().getName(), fo});
            }
            dataObject = ex.getDataObject();
            ex = DataObjectPool.getPOOL();
            // MONITORENTER : ex
            originalItem = dataObject.item();
            // MONITOREXIT : ex
            if (willLog) {
                MultiFileLoader.ERR.log(Level.FINE, "{0} object already exists for: {1} obj: {2}", new Object[]{this.getClass().getName(), fo, dataObject});
            }
            if (dataObject.getLoader() != this) {
                if (willLog) {
                    MultiFileLoader.ERR.log(Level.FINE, "{0} loader is wrong: {1}", new Object[]{this.getClass().getName(), dataObject.getLoader().getClass().getName()});
                }
                if (dataObject.getLoader() instanceof MultiFileLoader) {
                    mfl = (MultiFileLoader)dataObject.getLoader();
                    loaderPrimary = mfl.findPrimaryFileImpl(fo);
                    MultiFileLoader.ERR.log(Level.FINE, "Its primary file is {0}", (Object)loaderPrimary);
                    if (loaderPrimary != null && dataObject.getPrimaryFile() != loaderPrimary) {
                        MultiFileLoader.ERR.log(Level.FINE, "Which is different than primary of found: {0}", dataObject);
                        before = DataLoaderPool.getDefault().allLoaders(fo);
                        while (before.hasMoreElements()) {
                            o = before.nextElement();
                            if (o == mfl) {
                                MultiFileLoader.ERR.log(Level.FINE, "Returning null");
                                return null;
                            }
                            if (o != this) continue;
                            MultiFileLoader.ERR.log(Level.FINE, "The loader{0} is after {1}. So do break.", new Object[]{mfl, this});
                            break;
                        }
                    }
                }
                dataObject = this.checkCollision(dataObject, fo);
            }
            if (!(dataObject instanceof MultiDataObject)) {
                if (willLog == false) throw ex;
                MultiFileLoader.ERR.log(Level.FINE, "{0} object is not MultiDataObject: {1}", new Object[]{this.getClass().getName(), dataObject});
                throw ex;
            }
            obj = (MultiDataObject)dataObject;
        }
        catch (IOException ex) {
            MultiFileLoader.ERR.log(Level.FINE, null, ex);
            throw ex;
        }
        if (obj.getLoader() != this) {
            if (willLog == false) return obj;
            MultiFileLoader.ERR.log(Level.FINE, "{0} wrong loader: {1}", new Object[]{this.getClass().getName(), obj.getLoader().getClass().getName()});
            return obj;
        }
        if (willLog) {
            MultiFileLoader.ERR.log(Level.FINE, "{0} marking secondary entries", this.getClass().getName());
        }
        obj.markSecondaryEntriesRecognized(recognized);
        if (willLog) {
            MultiFileLoader.ERR.log(Level.FINE, "{0} register entry: {1}", new Object[]{this.getClass().getName(), fo});
        }
        tryAgain = false;
        e = null;
        mfl = DataObjectPool.getPOOL();
        // MONITORENTER : mfl
        if (originalItem != obj.item() || !originalItem.isValid() || !fo.isValid()) {
            tryAgain = true;
        } else {
            e = obj.registerEntry(fo);
        }
        // MONITOREXIT : mfl
        if (tryAgain) {
            return this.handleFindDataObject(fo, recognized);
        }
        if (willLog == false) return obj;
        MultiFileLoader.ERR.log(Level.FINE, "{0} success: {1}", new Object[]{this.getClass().getName(), e});
        return obj;
    }

    protected abstract FileObject findPrimaryFile(FileObject var1);

    protected abstract MultiDataObject createMultiObject(FileObject var1) throws DataObjectExistsException, IOException;

    protected abstract MultiDataObject.Entry createPrimaryEntry(MultiDataObject var1, FileObject var2);

    protected abstract MultiDataObject.Entry createSecondaryEntry(MultiDataObject var1, FileObject var2);

    DataObject checkCollision(DataObject obj, FileObject file) {
        FileObject primary = obj.getPrimaryFile();
        DataObjectPool.getPOOL().revalidate(new HashSet<FileObject>(Collections.singleton(primary)));
        DataObject result = DataObjectPool.getPOOL().find(primary);
        return result;
    }

    void checkConsistency(MultiDataObject obj) {
        FileObject primary = obj.getPrimaryFile();
        if (primary.equals((Object)this.findPrimaryFileImpl(primary))) {
            return;
        }
        try {
            obj.setValid(false);
        }
        catch (PropertyVetoException ex) {
            // empty catch block
        }
    }

    void checkFiles(MultiDataObject obj) {
        FileObject primary = obj.getPrimaryFile();
        assert (primary != null);
        FileObject parent = primary.getParent();
        assert (parent != null);
        FileObject[] arr = parent.getChildren();
        for (int i = 0; i < arr.length; ++i) {
            FileObject pf;
            if (obj.getSecondary().get((Object)arr[i]) != null || (pf = this.findPrimaryFileImpl(arr[i])) != primary) continue;
            try {
                DataObject.find(arr[i]);
                continue;
            }
            catch (DataObjectNotFoundException ex) {
                // empty catch block
            }
        }
    }

    MultiDataObject.Entry createSecondaryEntryImpl(MultiDataObject obj, FileObject secondaryFile) {
        return this.createSecondaryEntry(obj, secondaryFile);
    }

    FileObject findPrimaryFileImpl(FileObject fo) {
        return this.findPrimaryFile(fo);
    }
}

