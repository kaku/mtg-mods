/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$AsynchronousInstantiatingIterator
 *  org.openide.WizardDescriptor$BackgroundInstantiatingIterator
 *  org.openide.WizardDescriptor$InstantiatingIterator
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.WizardDescriptor$ProgressInstantiatingIterator
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 *  org.openide.util.NbBundle
 *  org.openide.util.Union2
 *  org.openide.windows.WindowManager
 */
package org.openide.loaders;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeListener;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.NewObjectWizardPanel;
import org.openide.loaders.TemplateWizardIterImpl;
import org.openide.loaders.TemplateWizardIteratorWrapper;
import org.openide.loaders.TemplateWizardPanel1;
import org.openide.loaders.TemplateWizardPanel2;
import org.openide.nodes.Node;
import org.openide.util.ContextAwareAction;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.Union2;
import org.openide.windows.WindowManager;

public class TemplateWizard
extends WizardDescriptor {
    private static final String EA_DESCRIPTION = "templateWizardURL";
    private static final String EA_ITERATOR = "templateWizardIterator";
    @Deprecated
    private static final String EA_DESC_RESOURCE = "templateWizardDescResource";
    private static final String CUSTOM_DESCRIPTION = "instantiatingWizardURL";
    private static final String CUSTOM_ITERATOR = "instantiatingIterator";
    static Dimension PREF_DIM = new Dimension(560, 350);
    private static final Logger LOG = Logger.getLogger(TemplateWizard.class.getName());
    private WizardDescriptor.Panel<WizardDescriptor> templateChooser;
    private WizardDescriptor.Panel<WizardDescriptor> targetChooser;
    private boolean showTargetChooser = true;
    private Iterator targetIterator;
    private TemplateWizardIteratorWrapper iterator;
    private DataObject template;
    private DataFolder templatesFolder;
    private String targetName = null;
    private DataFolder targetDataFolder;
    private boolean titleFormatSet = false;
    private PropertyChangeListener pcl;
    private Component lastComp;
    private BlockingQueue<Union2<Set<DataObject>, IOException>> newObjects = null;
    private ProgressHandle progressHandle;
    private boolean isInstantiating = false;

    public TemplateWizard() {
        this(new TemplateWizardIteratorWrapper(new TemplateWizardIterImpl()));
    }

    private TemplateWizard(TemplateWizardIteratorWrapper it) {
        super((WizardDescriptor.Iterator)it);
        this.iterator = it;
        this.iterator.initialize(this);
        this.putProperty("WizardPanel_autoWizardStyle", (Object)Boolean.TRUE);
        this.putProperty("WizardPanel_contentDisplayed", (Object)Boolean.TRUE);
        this.putProperty("WizardPanel_contentNumbered", (Object)Boolean.TRUE);
        this.setTitle(NbBundle.getMessage(TemplateWizard.class, (String)"CTL_TemplateTitle"));
        this.setTitleFormat(new MessageFormat("{0}"));
    }

    protected TemplateWizard(Iterator it) {
        this();
        this.iterator.setIterator(it, false);
    }

    protected void initialize() {
        if (this.iterator != null) {
            this.iterator.initialize(this);
            this.newObjects = new ArrayBlockingQueue<Union2<Set<DataObject>, IOException>>(1);
        }
        super.initialize();
    }

    final void setTemplateImpl(DataObject obj, boolean notify) {
        DataObject old = this.template;
        if (this.template != obj) {
            this.template = obj;
        }
        String title = this.getTitleFormat().format(new Object[]{obj.getNodeDelegate().getDisplayName()});
        this.putProperty("NewFileWizard_Title", (Object)title);
        if (old != this.template) {
            Iterator it;
            if (obj == null || (it = TemplateWizard.getIterator(obj)) == null) {
                it = this.defaultIterator();
            }
            if (it instanceof InstantiatingIteratorBridge) {
                WizardDescriptor.InstantiatingIterator newIt = ((InstantiatingIteratorBridge)it).getOriginalIterator();
                if (newIt instanceof WizardDescriptor.ProgressInstantiatingIterator) {
                    TemplateWizardIteratorWrapper.ProgressInstantiatingIterator newIterImplWrapper = new TemplateWizardIteratorWrapper.ProgressInstantiatingIterator(this.iterator.getOriginalIterImpl());
                    this.iterator = newIterImplWrapper;
                    this.setPanelsAndSettings((WizardDescriptor.Iterator)newIterImplWrapper, (Object)this);
                } else if (newIt instanceof WizardDescriptor.BackgroundInstantiatingIterator) {
                    TemplateWizardIteratorWrapper.BackgroundInstantiatingIterator newIterImplWrapper = new TemplateWizardIteratorWrapper.BackgroundInstantiatingIterator(this.iterator.getOriginalIterImpl());
                    this.iterator = newIterImplWrapper;
                    this.setPanelsAndSettings((WizardDescriptor.Iterator)newIterImplWrapper, (Object)this);
                } else if (newIt instanceof WizardDescriptor.AsynchronousInstantiatingIterator) {
                    TemplateWizardIteratorWrapper.AsynchronousInstantiatingIterator newIterImplWrapper = new TemplateWizardIteratorWrapper.AsynchronousInstantiatingIterator(this.iterator.getOriginalIterImpl());
                    this.iterator = newIterImplWrapper;
                    this.setPanelsAndSettings((WizardDescriptor.Iterator)newIterImplWrapper, (Object)this);
                }
            }
            this.iterator.setIterator(it, notify);
        }
        this.putProperty("NewFileWizard_Title", (Object)title);
    }

    public DataObject getTemplate() {
        return this.template;
    }

    public void setTemplate(final DataObject obj) {
        if (obj != null) {
            Mutex.EVENT.readAccess((Mutex.Action)new Mutex.Action<Void>(){

                public Void run() {
                    TemplateWizard.this.setTemplateImpl(obj, true);
                    return null;
                }
            });
        }
    }

    public void setTemplatesFolder(DataFolder folder) {
        this.templatesFolder = folder;
    }

    public DataFolder getTemplatesFolder() {
        FileObject fo;
        DataFolder df = this.templatesFolder;
        if (df == null && (fo = FileUtil.getConfigFile((String)"Templates")) != null && fo.isFolder()) {
            return DataFolder.findFolder(fo);
        }
        return df;
    }

    public DataFolder getTargetFolder() throws IOException {
        LOG.log(Level.FINE, "targetFolder={0} for {1}", new Object[]{this.targetDataFolder, this});
        if (this.targetDataFolder == null) {
            throw new IOException(NbBundle.getMessage(TemplateWizard.class, (String)"ERR_NoFilesystem"));
        }
        return this.targetDataFolder;
    }

    private void reload(DataObject obj) {
        Iterator it;
        if (obj == null || (it = TemplateWizard.getIterator(obj)) == null) {
            it = this.defaultIterator();
        }
        this.iterator.setIterator(it, true);
    }

    public void setTargetFolder(DataFolder f) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "set targetFolder=" + f + " for " + (Object)((Object)this), new Throwable());
        }
        this.targetDataFolder = f;
    }

    public String getTargetName() {
        return this.targetName;
    }

    public void setTargetName(String name) {
        this.targetName = name;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public WizardDescriptor.Panel<WizardDescriptor> templateChooser() {
        TemplateWizard templateWizard = this;
        synchronized (templateWizard) {
            if (this.templateChooser == null) {
                this.templateChooser = this.createTemplateChooser();
            }
        }
        return this.templateChooser;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public WizardDescriptor.Panel<WizardDescriptor> targetChooser() {
        TemplateWizard templateWizard = this;
        synchronized (templateWizard) {
            if (this.targetChooser == null) {
                this.targetChooser = this.createTargetChooser();
            }
        }
        return this.targetChooser;
    }

    final synchronized Iterator defaultIterator() {
        if (this.targetIterator == null) {
            this.targetIterator = this.createDefaultIterator();
        }
        return this.targetIterator;
    }

    protected WizardDescriptor.Panel<WizardDescriptor> createTemplateChooser() {
        return new TemplateWizardPanel1();
    }

    protected WizardDescriptor.Panel<WizardDescriptor> createTargetChooser() {
        if (this.showTargetChooser) {
            return new TemplateWizardPanel2();
        }
        return new NewObjectWizardPanel();
    }

    protected Iterator createDefaultIterator() {
        return new DefaultIterator();
    }

    public Set<DataObject> instantiate() throws IOException {
        this.showTargetChooser = true;
        return this.instantiateImpl(null, null);
    }

    public Set<DataObject> instantiate(DataObject template) throws IOException {
        this.showTargetChooser = true;
        return this.instantiateImpl(template, null);
    }

    public Set<DataObject> instantiate(DataObject template, DataFolder targetFolder) throws IOException {
        this.showTargetChooser = false;
        return this.instantiateImpl(template, targetFolder);
    }

    private ProgressHandle getProgressHandle() {
        return this.progressHandle;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    Set<DataObject> instantiateNewObjects(ProgressHandle handle) throws IOException {
        Union2 val;
        this.progressHandle = handle;
        Object option = this.getValue();
        if (option == FINISH_OPTION || option == YES_OPTION || option == OK_OPTION) {
            this.showWaitCursor();
            try {
                val = Union2.createFirst(this.handleInstantiate());
            }
            catch (IOException x) {
                val = Union2.createSecond((Object)x);
            }
            finally {
                this.showNormalCursor();
            }
        }
        val = Union2.createFirst((Object)null);
        if (this.lastComp != null) {
            this.lastComp.removePropertyChangeListener(this.propL());
            this.lastComp = null;
        }
        this.newObjects.clear();
        this.newObjects.add((Union2)val);
        if (val.hasFirst()) {
            return (Set)val.first();
        }
        throw (IOException)val.second();
    }

    private Set<DataObject> instantiateImpl(DataObject template, DataFolder targetFolder) throws IOException {
        Union2<Set<DataObject>, IOException> val;
        this.showTargetChooser |= targetFolder == null;
        this.targetChooser = null;
        if (targetFolder != null) {
            this.setTargetFolder(targetFolder);
        }
        if (template != null) {
            this.template = null;
            this.setTemplate(template);
            if (this.iterator != null) {
                this.iterator.initialize(this);
            }
        } else if (this.iterator != null) {
            this.iterator.initialize(this);
            this.iterator.first();
        }
        Mutex.EVENT.readAccess((Mutex.Action)new Mutex.Action<Void>(){

            public Void run() {
                TemplateWizard.this.updateState();
                return null;
            }
        });
        this.setValue((Object)null);
        Object result = DialogDisplayer.getDefault().notify((NotifyDescriptor)this);
        if (result == CLOSED_OPTION || result == CANCEL_OPTION) {
            return null;
        }
        try {
            val = this.newObjects.take();
        }
        catch (InterruptedException x) {
            throw new IOException(x);
        }
        if (val.hasFirst()) {
            return (Set)val.first();
        }
        throw (IOException)val.second();
    }

    private void showWaitCursor() {
        Mutex.EVENT.writeAccess(new Runnable(){

            @Override
            public void run() {
                try {
                    Frame f = WindowManager.getDefault().getMainWindow();
                    if (f instanceof JFrame) {
                        Component c = ((JFrame)f).getGlassPane();
                        c.setVisible(true);
                        c.setCursor(Cursor.getPredefinedCursor(3));
                    }
                }
                catch (NullPointerException npe) {
                    LOG.log(Level.WARNING, null, npe);
                }
            }
        });
    }

    private void showNormalCursor() {
        Mutex.EVENT.writeAccess(new Runnable(){

            @Override
            public void run() {
                try {
                    Frame f = WindowManager.getDefault().getMainWindow();
                    if (f instanceof JFrame) {
                        Component c = ((JFrame)f).getGlassPane();
                        c.setCursor(null);
                        c.setVisible(false);
                    }
                }
                catch (NullPointerException npe) {
                    LOG.log(Level.WARNING, null, npe);
                }
            }
        });
    }

    public void setTitleFormat(MessageFormat format) {
        this.titleFormatSet = true;
        super.setTitleFormat(format);
    }

    public MessageFormat getTitleFormat() {
        if (!this.titleFormatSet) {
            this.setTitleFormat(new MessageFormat(NbBundle.getMessage(TemplateWizard.class, (String)"CTL_TemplateTitle")));
        }
        return super.getTitleFormat();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected Set<DataObject> handleInstantiate() throws IOException {
        try {
            this.isInstantiating = true;
            Set<DataObject> set = this.iterator.getIterator().instantiate(this);
            return set;
        }
        finally {
            this.isInstantiating = false;
        }
    }

    public static void setDescription(DataObject obj, URL url) throws IOException {
        obj.getPrimaryFile().setAttribute("templateWizardURL", (Object)url);
        obj.getPrimaryFile().setAttribute("instantiatingWizardURL", (Object)url);
    }

    public static URL getDescription(DataObject obj) {
        URL desc = (URL)obj.getPrimaryFile().getAttribute("instantiatingWizardURL");
        if (desc != null) {
            return desc;
        }
        desc = (URL)obj.getPrimaryFile().getAttribute("templateWizardURL");
        if (desc != null) {
            return desc;
        }
        String rsrc = (String)obj.getPrimaryFile().getAttribute("templateWizardDescResource");
        if (rsrc != null) {
            try {
                URL better = new URL("nbresloc:/" + rsrc);
                try {
                    TemplateWizard.setDescription(obj, better);
                }
                catch (IOException ioe) {
                    // empty catch block
                }
                return better;
            }
            catch (MalformedURLException mfue) {
                Exceptions.printStackTrace((Throwable)mfue);
            }
        }
        return null;
    }

    @Deprecated
    public static void setDescriptionAsResource(DataObject obj, String rsrc) throws IOException {
        if (rsrc != null && rsrc.startsWith("/")) {
            LOG.log(Level.WARNING, "auto-stripping leading slash from resource path in TemplateWizard.setDescriptionAsResource: {0}", rsrc);
            rsrc = rsrc.substring(1);
        }
        obj.getPrimaryFile().setAttribute("templateWizardDescResource", (Object)rsrc);
    }

    @Deprecated
    public static String getDescriptionAsResource(DataObject obj) {
        return (String)obj.getPrimaryFile().getAttribute("templateWizardDescResource");
    }

    @Deprecated
    public static void setIterator(DataObject obj, Iterator iter) throws IOException {
        obj.getPrimaryFile().setAttribute("instantiatingIterator", (Object)iter);
        obj.getPrimaryFile().setAttribute("templateWizardIterator", (Object)iter);
    }

    public static Iterator getIterator(DataObject obj) {
        Object unknownIterator = obj.getPrimaryFile().getAttribute("instantiatingIterator");
        if (unknownIterator == null) {
            unknownIterator = obj.getPrimaryFile().getAttribute("templateWizardIterator");
        }
        Iterator it = null;
        if (unknownIterator instanceof Iterator) {
            it = (Iterator)unknownIterator;
        }
        if (unknownIterator instanceof WizardDescriptor.InstantiatingIterator) {
            it = new InstantiatingIteratorBridge((WizardDescriptor.InstantiatingIterator)unknownIterator);
        }
        if (it != null) {
            return it;
        }
        return obj.getCookie(Iterator.class);
    }

    static boolean checkCaseInsensitiveName(FileObject folder, String name, String extension) {
        Enumeration children = folder.getChildren(false);
        while (children.hasMoreElements()) {
            FileObject fo = (FileObject)children.nextElement();
            if (!extension.equalsIgnoreCase(fo.getExt()) || !name.equalsIgnoreCase(fo.getName())) continue;
            return true;
        }
        return false;
    }

    protected void updateState() {
        assert (EventQueue.isDispatchThread());
        if (this.isInstantiating) {
            return;
        }
        super.updateState();
        if (this.lastComp != null) {
            this.lastComp.removePropertyChangeListener(this.propL());
        }
        this.lastComp = this.iterator.current().getComponent();
        this.lastComp.addPropertyChangeListener(this.propL());
        this.putProperty("WizardPanel_contentSelectedIndex", (Object)new Integer(this.getContentSelectedIndex()));
        if (this.getContentData() != null) {
            this.putProperty("WizardPanel_contentData", (Object)this.getContentData());
        }
    }

    private String[] getContentData() {
        Object property;
        Component first = this.templateChooser().getComponent();
        if (this.iterator.current() == this.templateChooser()) {
            return (String[])((JComponent)first).getClientProperty("WizardPanel_contentData");
        }
        String[] cd = null;
        Component c = this.iterator.current().getComponent();
        if (c instanceof JComponent && (property = ((JComponent)c).getClientProperty("WizardPanel_contentData")) instanceof String[]) {
            String[] cont = (String[])property;
            Object value = ((JComponent)first).getClientProperty("WizardPanel_contentData");
            if (value instanceof String[]) {
                cd = new String[cont.length + 1];
                cd[0] = ((String[])value)[0];
                System.arraycopy(cont, 0, cd, 1, cont.length);
            } else {
                cd = new String[cont.length];
                System.arraycopy(cont, 0, cd, 0, cont.length);
            }
        }
        return cd;
    }

    private int getContentSelectedIndex() {
        Object property;
        if (this.iterator.current() == this.templateChooser()) {
            return 0;
        }
        Component c = this.iterator.current().getComponent();
        if (c instanceof JComponent && (property = ((JComponent)c).getClientProperty("WizardPanel_contentSelectedIndex")) instanceof Integer) {
            return (Integer)property + 1;
        }
        return 1;
    }

    private PropertyChangeListener propL() {
        if (this.pcl == null) {
            this.pcl = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent ev) {
                    if ("WizardPanel_contentSelectedIndex".equals(ev.getPropertyName())) {
                        TemplateWizard.this.putProperty("WizardPanel_contentSelectedIndex", (Object)new Integer(TemplateWizard.this.getContentSelectedIndex()));
                    } else if ("WizardPanel_contentData".equals(ev.getPropertyName()) && TemplateWizard.this.getContentData() != null) {
                        TemplateWizard.this.putProperty("WizardPanel_contentData", (Object)TemplateWizard.this.getContentData());
                    }
                }
            };
        }
        return this.pcl;
    }

    final TemplateWizardIterImpl getIterImpl() {
        return this.iterator.getOriginalIterImpl();
    }

    private static class InstantiatingIteratorBridge
    implements Iterator {
        private WizardDescriptor.InstantiatingIterator<WizardDescriptor> instantiatingIterator;

        public InstantiatingIteratorBridge(WizardDescriptor.InstantiatingIterator<WizardDescriptor> it) {
            this.instantiatingIterator = it;
        }

        private WizardDescriptor.InstantiatingIterator getOriginalIterator() {
            return this.instantiatingIterator;
        }

        public void addChangeListener(ChangeListener l) {
            this.instantiatingIterator.addChangeListener(l);
        }

        public WizardDescriptor.Panel<WizardDescriptor> current() {
            return this.instantiatingIterator.current();
        }

        public boolean hasNext() {
            return this.instantiatingIterator.hasNext();
        }

        public boolean hasPrevious() {
            return this.instantiatingIterator.hasPrevious();
        }

        public String name() {
            return this.instantiatingIterator.name();
        }

        public void nextPanel() {
            this.instantiatingIterator.nextPanel();
        }

        public void previousPanel() {
            this.instantiatingIterator.previousPanel();
        }

        public void removeChangeListener(ChangeListener l) {
            this.instantiatingIterator.removeChangeListener(l);
        }

        @Override
        public void initialize(TemplateWizard wiz) {
            this.instantiatingIterator.initialize((WizardDescriptor)wiz);
        }

        @Override
        public Set<DataObject> instantiate(TemplateWizard wiz) throws IOException {
            Set workSet;
            if (this.instantiatingIterator instanceof WizardDescriptor.ProgressInstantiatingIterator) {
                assert (wiz.getProgressHandle() != null);
                workSet = ((WizardDescriptor.ProgressInstantiatingIterator)this.instantiatingIterator).instantiate(wiz.getProgressHandle());
            } else {
                workSet = this.instantiatingIterator.instantiate();
            }
            if (workSet == null) {
                LOG.log(Level.WARNING, "Wizard iterator of type {0} illegally returned null from the instantiate method", this.instantiatingIterator.getClass().getName());
                return Collections.emptySet();
            }
            LinkedHashSet<DataObject> resultSet = new LinkedHashSet<DataObject>(workSet.size());
            for (Object obj : workSet) {
                DataObject dobj;
                assert (obj != null);
                if (obj instanceof DataObject) {
                    resultSet.add((DataObject)obj);
                    continue;
                }
                if (obj instanceof FileObject) {
                    dobj = DataObject.find((FileObject)obj);
                    resultSet.add(dobj);
                    continue;
                }
                if (!(obj instanceof Node) || (dobj = (DataObject)((Node)obj).getCookie(DataObject.class)) == null) continue;
                resultSet.add(dobj);
            }
            return resultSet;
        }

        @Override
        public void uninitialize(TemplateWizard wiz) {
            this.instantiatingIterator.uninitialize((WizardDescriptor)wiz);
        }
    }

    private final class DefaultIterator
    implements Iterator {
        DefaultIterator() {
        }

        public String name() {
            return "";
        }

        @Override
        public Set<DataObject> instantiate(TemplateWizard wiz) throws IOException {
            Action a;
            String n = wiz.getTargetName();
            DataFolder folder = wiz.getTargetFolder();
            DataObject template = wiz.getTemplate();
            HashMap wizardProps = new HashMap();
            for (Map.Entry entry : wiz.getProperties().entrySet()) {
                wizardProps.put("wizard." + (String)entry.getKey(), entry.getValue());
            }
            DataObject obj = template.createFromTemplate(folder, n, wizardProps);
            final Node node = obj.getNodeDelegate();
            Action _a = node.getPreferredAction();
            if (_a instanceof ContextAwareAction) {
                _a = ((ContextAwareAction)_a).createContextAwareInstance(node.getLookup());
            }
            if ((a = _a) != null) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        a.actionPerformed(new ActionEvent((Object)node, 1001, ""));
                    }
                });
            }
            return Collections.singleton(obj);
        }

        @Override
        public void initialize(TemplateWizard wiz) {
        }

        @Override
        public void uninitialize(TemplateWizard wiz) {
        }

        public WizardDescriptor.Panel<WizardDescriptor> current() {
            return TemplateWizard.this.targetChooser();
        }

        public boolean hasNext() {
            return false;
        }

        public boolean hasPrevious() {
            return false;
        }

        public void nextPanel() {
            throw new NoSuchElementException();
        }

        public void previousPanel() {
            throw new NoSuchElementException();
        }

        public void addChangeListener(ChangeListener l) {
        }

        public void removeChangeListener(ChangeListener l) {
        }

    }

    public static interface Iterator
    extends WizardDescriptor.Iterator<WizardDescriptor>,
    Serializable,
    Node.Cookie {
        public Set<DataObject> instantiate(TemplateWizard var1) throws IOException;

        public void initialize(TemplateWizard var1);

        public void uninitialize(TemplateWizard var1);
    }

}

