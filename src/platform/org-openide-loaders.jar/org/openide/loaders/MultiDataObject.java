/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.nodes.Children
 *  org.openide.nodes.CookieSet
 *  org.openide.nodes.CookieSet$Before
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.UserCancelException
 */
package org.openide.loaders;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.Bundle;
import org.openide.loaders.CreateFromTemplateHandler;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataLoader;
import org.openide.loaders.DataLoaderPool;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.loaders.DataObjectPool;
import org.openide.loaders.FileEntry;
import org.openide.loaders.FilesSet;
import org.openide.loaders.FolderList;
import org.openide.loaders.MultiDOEditor;
import org.openide.loaders.MultiFileLoader;
import org.openide.loaders.UniFileLoader;
import org.openide.nodes.Children;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;
import org.openide.util.UserCancelException;

public class MultiDataObject
extends DataObject {
    static final long serialVersionUID = -7750146802134210308L;
    private static final Object cookieSetLock = new Object();
    private static final Object secondaryCreationLock = new Object();
    private static final RequestProcessor firingProcessor = new RequestProcessor("MDO PropertyChange processor");
    private Map<String, PropertyChangeEvent> later;
    private static final RequestProcessor delayProcessor = new RequestProcessor("MDO Firing delayer");
    private RequestProcessor.Task delayedPropFilesTask;
    private static final Object delayedPropFilesLock = new Object();
    static final Logger ERR = Logger.getLogger(MultiDataObject.class.getName());
    private Entry primary;
    private HashMap<FileObject, Entry> secondary;
    private CookieSet cookieSet;
    boolean checked = false;
    private static final String[] TEMPLATE_ATTRIBUTES = new String[]{"iconBase", "SystemFileSystem.icon", "SystemFileSystem.icon32", "instantiatingIterator", "instantiatingWizardURL"};
    private ChangeAndBefore chLis;
    private static EmptyRecognizer RECOGNIZER = new EmptyRecognizer();

    public MultiDataObject(FileObject fo, MultiFileLoader loader) throws DataObjectExistsException {
        super(fo, loader);
        this.primary = this.createPrimaryEntry(this, this.getPrimaryFile());
    }

    @Deprecated
    MultiDataObject(FileObject fo, DataLoader loader) throws DataObjectExistsException {
        super(fo, loader);
        this.primary = this.createPrimaryEntry(this, this.getPrimaryFile());
    }

    public final MultiFileLoader getMultiFileLoader() {
        DataLoader loader = this.getLoader();
        if (!(loader instanceof MultiFileLoader)) {
            return null;
        }
        return (MultiFileLoader)loader;
    }

    @Override
    public Set<FileObject> files() {
        return new FilesSet(this);
    }

    @Override
    public boolean isDeleteAllowed() {
        return this.getPrimaryFile().canWrite() && !this.existReadOnlySecondary();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean existReadOnlySecondary() {
        Object object = this.synchObjectSecondary();
        synchronized (object) {
            for (FileObject f : this.getSecondary().keySet()) {
                if (f.canWrite()) continue;
                return true;
            }
        }
        return false;
    }

    private Map<FileObject, Entry> checkSecondary() {
        if (!this.checked) {
            this.checkFiles(this);
            this.checked = true;
        }
        return this.getSecondary();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    Map<FileObject, Entry> getSecondary() {
        Object object = secondaryCreationLock;
        synchronized (object) {
            if (this.secondary == null) {
                this.secondary = new HashMap(4);
            }
            if (ERR.isLoggable(Level.FINE)) {
                ERR.fine("getSecondary for " + this + " is " + this.secondary);
            }
            return this.secondary;
        }
    }

    @Override
    public boolean isCopyAllowed() {
        return true;
    }

    @Override
    public boolean isMoveAllowed() {
        return this.getPrimaryFile().canWrite() && !this.existReadOnlySecondary();
    }

    @Override
    public boolean isRenameAllowed() {
        return this.getPrimaryFile().canWrite() && !this.existReadOnlySecondary();
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    Object synchObjectSecondary() {
        Map<FileObject, Entry> lock = this.checkSecondary();
        if (lock == null) {
            throw new IllegalStateException("checkSecondary was null from " + this);
        }
        return this.checkSecondary();
    }

    @Override
    protected Node createNodeDelegate() {
        if (this.associateLookup() >= 1) {
            return new DataNode(this, Children.LEAF, this.getLookup());
        }
        DataNode dataNode = (DataNode)super.createNodeDelegate();
        return dataNode;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final void addSecondaryEntry(Entry fe) {
        Map<FileObject, Entry> map = this.getSecondary();
        synchronized (map) {
            this.getSecondary().put(fe.getFile(), fe);
            if (ERR.isLoggable(Level.FINE)) {
                ERR.fine("addSecondaryEntry: " + fe + " for " + this);
            }
        }
        FolderList l = this.getFolderList();
        if (l == null) {
            this.firePropertyChangeLater("files", null, null);
        } else if (l.isCreated()) {
            this.firePropertyChangeLater("files", null, null);
        } else {
            this.firePropFilesAfterFinishing();
        }
    }

    private FolderList getFolderList() {
        FileObject parent = this.primary.file.getParent();
        if (parent != null) {
            return FolderList.find(parent, false);
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final void removeSecondaryEntry(Entry fe) {
        Map<FileObject, Entry> map = this.getSecondary();
        synchronized (map) {
            this.getSecondary().remove((Object)fe.getFile());
            if (ERR.isLoggable(Level.FINE)) {
                ERR.fine("removeSecondaryEntry: " + fe + " for " + this);
            }
        }
        this.firePropertyChangeLater("files", null, null);
        this.updateFilesInCookieSet();
        if (fe.isImportant()) {
            this.checkConsistency(this);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void markSecondaryEntriesRecognized(DataLoader.RecognizedFiles recognized) {
        if (recognized == DataLoaderPool.emptyDataLoaderRecognized) {
            return;
        }
        Map<FileObject, Entry> map = this.getSecondary();
        synchronized (map) {
            for (FileObject fo : this.getSecondary().keySet()) {
                recognized.markRecognized(fo);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final Entry registerEntry(FileObject fo) {
        Map<FileObject, Entry> map = this.getSecondary();
        synchronized (map) {
            if (fo == null) {
                return this.primary;
            }
            if (fo.equals((Object)this.getPrimaryFile())) {
                return this.primary;
            }
            Entry e = this.getSecondary().get((Object)fo);
            if (e != null) {
                return e;
            }
            e = this.createSecondaryEntry(this, fo);
            this.addSecondaryEntry(e);
            return e;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void removeFile(FileObject fo) {
        Map<FileObject, Entry> map = this.getSecondary();
        synchronized (map) {
            Entry e = this.getSecondary().get((Object)fo);
            if (e != null) {
                this.removeSecondaryEntry(e);
            }
        }
    }

    public final Entry getPrimaryEntry() {
        return this.primary;
    }

    public final Set<Entry> secondaryEntries() {
        return this.secondaryEntries(true);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final Set<Entry> secondaryEntries(boolean allocate) {
        Object object = this.synchObjectSecondary();
        synchronized (object) {
            this.removeAllInvalid();
            return allocate ? new HashSet<Entry>(this.getSecondary().values()) : null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final Entry findSecondaryEntry(FileObject fo) {
        Entry e;
        Object object = this.synchObjectSecondary();
        synchronized (object) {
            this.removeAllInvalid();
            e = this.getSecondary().get((Object)fo);
        }
        return e;
    }

    private void removeAllInvalid() {
        ERR.log(Level.FINE, "removeAllInvalid, started {0}", this);
        Iterator<Map.Entry<FileObject, Entry>> it = this.checkSecondary().entrySet().iterator();
        boolean fire = false;
        while (it.hasNext()) {
            Map.Entry<FileObject, Entry> e = it.next();
            FileObject fo = e.getKey();
            if (fo != null && fo.isValid()) continue;
            it.remove();
            if (ERR.isLoggable(Level.FINE)) {
                ERR.log(Level.FINE, "removeAllInvalid, removed: {0} for {1}", new Object[]{fo, this});
            }
            fire = true;
        }
        ERR.log(Level.FINE, "removeAllInvalid, finished {0}", this);
        if (fire) {
            this.firePropertyChangeLater("files", null, null);
        }
    }

    @Override
    protected FileLock takePrimaryFileLock() throws IOException {
        return this.getPrimaryEntry().takeLock();
    }

    private String existInFolder(FileObject fo, FileObject folder) {
        if (fo.isFolder() && this.isMergingFolders(fo, folder)) {
            return "";
        }
        String orig = fo.getName();
        String name = FileUtil.findFreeFileName((FileObject)folder, (String)orig, (String)fo.getExt());
        if (name.length() <= orig.length()) {
            return "";
        }
        return name.substring(orig.length());
    }

    boolean isMergingFolders(FileObject who, FileObject targetFolder) {
        return false;
    }

    @Override
    protected DataObject handleCopy(DataFolder df) throws IOException {
        FileObject fo;
        String suffix = this.existInFolder(this.getPrimaryEntry().getFile(), df.getPrimaryFile());
        if (suffix == null) {
            throw new UserCancelException();
        }
        boolean template = this.isTemplate();
        for (Entry e : this.secondaryEntries()) {
            fo = e.copy(df.getPrimaryFile(), suffix);
            if (!template) continue;
            FileUtil.copyAttributes((FileObject)e.getFile(), (FileObject)fo);
            MultiDataObject.copyTemplateAttributes(e.getFile(), fo);
        }
        fo = this.getPrimaryEntry().copy(df.getPrimaryFile(), suffix);
        if (fo == null || !fo.isValid()) {
            IOException ex = new IOException("copied file is not valid " + (Object)fo);
            Exceptions.attachLocalizedMessage((Throwable)ex, (String)Bundle.EXC_NO_LONGER_VALID((Object)fo));
            throw ex;
        }
        if (template) {
            FileObject source = this.getPrimaryEntry().getFile();
            MultiDataObject.copyUniqueAttribute(source, fo, "displayName");
            FileUtil.copyAttributes((FileObject)source, (FileObject)fo);
            MultiDataObject.copyTemplateAttributes(source, fo);
        }
        boolean fullRescan = this.getMultiFileLoader() == null || this.getMultiFileLoader().findPrimaryFile(fo) != fo || this.getMultiFileLoader() == DataLoaderPool.getDefaultFileLoader();
        try {
            return fullRescan ? DataObject.find(fo) : this.createMultiObject(fo);
        }
        catch (DataObjectExistsException ex) {
            return ex.getDataObject();
        }
        catch (DataObjectNotFoundException ex) {
            if (!fo.isValid()) {
                Exceptions.attachLocalizedMessage((Throwable)ex, (String)Bundle.EXC_NO_LONGER_VALID((Object)fo));
            }
            throw ex;
        }
    }

    private static void copyAttributes(FileObject source, FileObject dest, String[] attrNames) throws IOException {
        for (String attr : attrNames) {
            Object value = source.getAttribute(attr);
            if (value == null) continue;
            dest.setAttribute(attr, value);
        }
    }

    private static void copyTemplateAttributes(FileObject source, FileObject dest) throws IOException {
        MultiDataObject.copyAttributes(source, dest, TEMPLATE_ATTRIBUTES);
        Enumeration attrs = source.getAttributes();
        while (attrs.hasMoreElements()) {
            Object value;
            String attr = (String)attrs.nextElement();
            if (!attr.startsWith("template") || (value = source.getAttribute(attr)) == null) continue;
            dest.setAttribute(attr, value);
        }
    }

    private static void copyUniqueAttribute(FileObject source, FileObject dest, String attrName) throws IOException {
        Object value = source.getAttribute(attrName);
        if (value == null) {
            return;
        }
        FileObject parent = dest.getParent();
        if (parent == null || !(value instanceof String)) {
            dest.setAttribute(attrName, value);
            return;
        }
        String valueBase = (String)value;
        FileObject[] ch = parent.getChildren();
        int i = 0;
        do {
            boolean isValueInChildren = false;
            for (int j = 0; j < ch.length; ++j) {
                Object v = ch[j].getAttribute(attrName);
                if (!value.equals(v)) continue;
                isValueInChildren = true;
                break;
            }
            if (!isValueInChildren) break;
            value = valueBase + " " + ++i;
        } while (true);
        dest.setAttribute(attrName, value);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void handleDelete() throws IOException {
        Object e;
        Iterator<Map.Entry<FileObject, Entry>> it;
        ArrayList toRemove = new ArrayList();
        Object object = this.synchObjectSecondary();
        synchronized (object) {
            this.removeAllInvalid();
            it = new ArrayList<Map.Entry<FileObject, Entry>>(this.getSecondary().entrySet()).iterator();
        }
        while (it.hasNext()) {
            e = it.next();
            ((Entry)e.getValue()).delete();
            toRemove.add(e.getKey());
        }
        e = this.synchObjectSecondary();
        synchronized (e) {
            for (FileObject f : toRemove) {
                this.getSecondary().remove((Object)f);
                if (!ERR.isLoggable(Level.FINE)) continue;
                ERR.fine("  handleDelete, removed entry: " + (Object)f);
            }
        }
        this.getPrimaryEntry().delete();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected FileObject handleRename(String name) throws IOException {
        Iterator<Map.Entry<FileObject, Entry>> it;
        Map.Entry<FileObject, Entry> e;
        Map<String, Object> templateAttrs = this.getTemplateAttrs();
        this.getPrimaryEntry().changeFile(this.getPrimaryEntry().rename(name));
        this.setTemplateAttrs(templateAttrs);
        HashMap<FileObject, Entry> add = null;
        ArrayList toRemove = new ArrayList();
        Object object = this.synchObjectSecondary();
        synchronized (object) {
            this.removeAllInvalid();
            it = new ArrayList<Map.Entry<FileObject, Entry>>(this.getSecondary().entrySet()).iterator();
        }
        while (it.hasNext()) {
            e = it.next();
            FileObject fo = e.getValue().rename(name);
            if (fo == null) {
                toRemove.add(e.getKey());
                continue;
            }
            if (fo.equals(e.getKey())) continue;
            if (add == null) {
                add = new HashMap<FileObject, Entry>();
            }
            Entry entry = (Entry)e.getValue();
            entry.changeFile(fo);
            add.put(entry.getFile(), entry);
            toRemove.add(e.getKey());
        }
        if (add != null || !toRemove.isEmpty()) {
            e = this.synchObjectSecondary();
            synchronized (e) {
                if (!toRemove.isEmpty()) {
                    for (FileObject f : toRemove) {
                        this.getSecondary().remove((Object)f);
                        if (!ERR.isLoggable(Level.FINE)) continue;
                        ERR.fine("handleRename, removed: " + (Object)f + " for " + this);
                    }
                }
                if (add != null) {
                    this.getSecondary().putAll(add);
                    if (ERR.isLoggable(Level.FINE)) {
                        ERR.fine("handleRename, putAll: " + add + " for " + this);
                    }
                }
            }
            this.firePropertyChangeLater("files", null, null);
        }
        return this.getPrimaryEntry().getFile();
    }

    private Map<String, Object> getTemplateAttrs() {
        if (this.isTemplate()) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            FileObject fo = this.getPrimaryFile();
            Enumeration attributes = fo.getAttributes();
            while (attributes.hasMoreElements()) {
                Object val;
                String key = (String)attributes.nextElement();
                if (!key.startsWith("template") || (val = fo.getAttribute(key)) == null) continue;
                map.put(key, val);
            }
            for (String key : TEMPLATE_ATTRIBUTES) {
                Object val = fo.getAttribute(key);
                if (val == null) continue;
                map.put(key, val);
            }
            return map;
        }
        return null;
    }

    private void setTemplateAttrs(Map<String, Object> attrs) throws IOException {
        if (attrs != null && this.isTemplate()) {
            FileObject fo = this.getPrimaryFile();
            for (Map.Entry<String, Object> entry : attrs.entrySet()) {
                if (entry.getValue() == null || fo.getAttribute(entry.getKey()) != null) continue;
                fo.setAttribute(entry.getKey(), entry.getValue());
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected FileObject handleMove(DataFolder df) throws IOException {
        String suffix = this.existInFolder(this.getPrimaryEntry().getFile(), df.getPrimaryFile());
        if (suffix == null) {
            throw new UserCancelException();
        }
        List<Pair> backup = this.saveEntries();
        try {
            int count;
            FileObject newPF;
            Iterator<Map.Entry<FileObject, Entry>> it;
            Object e;
            HashMap<FileObject, Entry> add = null;
            ArrayList toRemove = new ArrayList();
            Object object = this.synchObjectSecondary();
            synchronized (object) {
                this.removeAllInvalid();
                ArrayList<Map.Entry<FileObject, Entry>> list = new ArrayList<Map.Entry<FileObject, Entry>>(this.getSecondary().entrySet());
                count = list.size();
                it = list.iterator();
            }
            if (ERR.isLoggable(Level.FINE)) {
                ERR.fine("move " + this + " to " + df + " number of secondary entries: " + count);
                ERR.fine("moving primary entry: " + this.getPrimaryEntry());
            }
            if ((newPF = this.getPrimaryEntry().move(df.getPrimaryFile(), suffix)) == null) {
                throw new NullPointerException("Invalid move on " + this.getPrimaryEntry() + " of " + this + " returned null");
            }
            this.getPrimaryEntry().changeFile(newPF);
            if (ERR.isLoggable(Level.FINE)) {
                ERR.fine("               moved: " + (Object)this.getPrimaryEntry().getFile());
            }
            while (it.hasNext()) {
                e = it.next();
                if (ERR.isLoggable(Level.FINE)) {
                    ERR.fine("moving entry :" + e);
                }
                FileObject fo = ((Entry)e.getValue()).move(df.getPrimaryFile(), suffix);
                if (ERR.isLoggable(Level.FINE)) {
                    ERR.fine("  moved to   :" + (Object)fo);
                }
                if (fo == null) {
                    toRemove.add(e.getKey());
                    continue;
                }
                if (fo.equals(e.getKey())) continue;
                if (add == null) {
                    add = new HashMap<FileObject, Entry>();
                }
                Entry entry = (Entry)e.getValue();
                entry.changeFile(fo);
                add.put(entry.getFile(), entry);
                toRemove.add(e.getKey());
            }
            if (add != null || !toRemove.isEmpty()) {
                e = this.synchObjectSecondary();
                synchronized (e) {
                    if (!toRemove.isEmpty()) {
                        Object[] objects = toRemove.toArray();
                        for (int i = 0; i < objects.length; ++i) {
                            this.getSecondary().remove(objects[i]);
                            if (!ERR.isLoggable(Level.FINE)) continue;
                            ERR.fine("handleMove, remove: " + objects[i] + " for " + this);
                        }
                    }
                    if (add != null) {
                        this.getSecondary().putAll(add);
                        if (ERR.isLoggable(Level.FINE)) {
                            ERR.fine("handleMove, putAll: " + add + " for " + this);
                        }
                    }
                }
                this.firePropertyChangeLater("files", null, null);
            }
            if (ERR.isLoggable(Level.FINE)) {
                ERR.fine("successfully moved " + this);
            }
            return this.getPrimaryEntry().getFile();
        }
        catch (IOException e) {
            if (ERR.isLoggable(Level.FINE)) {
                ERR.fine("exception is here, restoring entries " + this);
                ERR.log(Level.FINE, null, e);
            }
            this.restoreEntries(backup);
            if (ERR.isLoggable(Level.FINE)) {
                ERR.fine("entries restored " + this);
            }
            throw e;
        }
    }

    @Override
    protected DataObject handleCreateFromTemplate(DataFolder df, String name) throws IOException {
        if (name == null) {
            name = FileUtil.findFreeFileName((FileObject)df.getPrimaryFile(), (String)this.getPrimaryFile().getName(), (String)this.getPrimaryFile().getExt());
        }
        FileObject pf = null;
        Map<String, Object> params = null;
        for (CreateFromTemplateHandler h : Lookup.getDefault().lookupAll(CreateFromTemplateHandler.class)) {
            FileObject current;
            if (!h.accept(current = this.getPrimaryEntry().getFile())) continue;
            if (params == null) {
                params = DataObject.CreateAction.findParameters(name);
            }
            pf = h.createFromTemplate(current, df.getPrimaryFile(), name, DataObject.CreateAction.enhanceParameters(params, name, current.getExt()));
            assert (pf != null);
            break;
        }
        if (params == null) {
            pf = this.getPrimaryEntry().createFromTemplate(df.getPrimaryFile(), name);
        }
        block3 : for (Entry entry : this.secondaryEntries()) {
            for (CreateFromTemplateHandler h2 : Lookup.getDefault().lookupAll(CreateFromTemplateHandler.class)) {
                FileObject current;
                if (!h2.accept(current = entry.getFile())) continue;
                if (params == null) {
                    params = DataObject.CreateAction.findParameters(name);
                }
                FileObject fo = h2.createFromTemplate(current, df.getPrimaryFile(), name, DataObject.CreateAction.enhanceParameters(params, name, current.getExt()));
                assert (fo != null);
                continue block3;
            }
            entry.createFromTemplate(df.getPrimaryFile(), name);
        }
        try {
            if (this.getMultiFileLoader() == DataLoaderPool.getDefaultFileLoader()) {
                return DataObject.find(pf);
            }
            return this.createMultiObject(pf);
        }
        catch (DataObjectExistsException ex) {
            return ex.getDataObject();
        }
    }

    @Override
    protected DataObject handleCopyRename(DataFolder df, String name, String ext) throws IOException {
        if (this.getLoader() instanceof UniFileLoader || this.getLoader() == DataLoaderPool.getDefaultFileLoader()) {
            FileObject fo = this.getPrimaryEntry().copyRename(df.getPrimaryFile(), name, ext);
            return DataObject.find(fo);
        }
        throw new IOException("SaveAs operation not supported for this file type.");
    }

    @Deprecated
    protected final void setCookieSet(CookieSet s) {
        this.setCookieSet(s, true);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void setCookieSet(CookieSet s, boolean fireChange) {
        Object object = cookieSetLock;
        synchronized (object) {
            ChangeAndBefore ch = this.getChangeListener();
            if (this.cookieSet != null) {
                this.cookieSet.removeChangeListener((ChangeListener)ch);
            }
            s.addChangeListener((ChangeListener)ch);
            this.cookieSet = s;
        }
        if (fireChange) {
            this.fireCookieChange();
        }
    }

    protected final CookieSet getCookieSet() {
        return this.getCookieSet(true);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final CookieSet getCookieSet(boolean create) {
        Object object = cookieSetLock;
        synchronized (object) {
            if (this.cookieSet != null) {
                return this.cookieSet;
            }
            if (!create) {
                return null;
            }
            CookieSet g = CookieSet.createGeneric((CookieSet.Before)this.getChangeListener());
            g.assign(DataObject.class, (Object[])new DataObject[]{this});
            this.setCookieSet(g, false);
            return this.cookieSet;
        }
    }

    @Override
    public <T extends Node.Cookie> T getCookie(Class<T> type) {
        Node.Cookie cookie;
        CookieSet c = this.cookieSet;
        if (c != null && (cookie = c.getCookie(type)) != null) {
            return (T)cookie;
        }
        return super.getCookie(type);
    }

    @Override
    public Lookup getLookup() {
        int version = this.associateLookup();
        assert (version <= 1);
        if (version >= 1) {
            return this.getCookieSet().getLookup();
        }
        return super.getLookup();
    }

    protected int associateLookup() {
        return 0;
    }

    protected final void registerEditor(String mimeType, boolean useMultiview) {
        MultiDOEditor.registerEditor(this, mimeType, useMultiview);
    }

    final void fireCookieChange() {
        this.firePropertyChange("cookie", null, null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void firePropertyChangeLater(String name, Object oldV, Object newV) {
        RequestProcessor requestProcessor = firingProcessor;
        synchronized (requestProcessor) {
            if (this.later == null) {
                this.later = new LinkedHashMap<String, PropertyChangeEvent>();
            }
            this.later.put(name, new PropertyChangeEvent(this, name, oldV, newV));
        }
        firingProcessor.post(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                Map fire;
                RequestProcessor requestProcessor = firingProcessor;
                synchronized (requestProcessor) {
                    fire = MultiDataObject.this.later;
                    MultiDataObject.this.later = null;
                }
                if (fire == null) {
                    return;
                }
                for (PropertyChangeEvent ev : fire.values()) {
                    String name = ev.getPropertyName();
                    MultiDataObject.this.firePropertyChange(name, ev.getOldValue(), ev.getNewValue());
                    if (!"files".equals(name) && !"primaryFile".equals(name)) continue;
                    MultiDataObject.this.updateFilesInCookieSet();
                }
            }
        }, 100, 1);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void firePropFilesAfterFinishing() {
        Object object = delayedPropFilesLock;
        synchronized (object) {
            if (this.delayedPropFilesTask == null) {
                this.delayedPropFilesTask = delayProcessor.post(new Runnable(){

                    @Override
                    public void run() {
                        FolderList l = MultiDataObject.this.getFolderList();
                        if (l != null) {
                            l.waitProcessingFinished();
                        }
                        MultiDataObject.this.firePropertyChangeLater("files", null, null);
                    }
                });
            } else {
                this.delayedPropFilesTask.schedule(0);
            }
        }
    }

    @Override
    final void recognizedByFolder() {
        this.checked = true;
    }

    final ChangeAndBefore getChangeListener() {
        if (this.chLis == null) {
            this.chLis = new ChangeAndBefore();
        }
        return this.chLis;
    }

    private final Entry createPrimaryEntry(MultiDataObject obj, FileObject fo) {
        MultiFileLoader loader = this.getMultiFileLoader();
        if (loader != null) {
            return loader.createPrimaryEntry(obj, fo);
        }
        Entry e = fo.isFolder() ? new FileEntry.Folder(obj, fo) : new FileEntry(obj, fo);
        return e;
    }

    private final Entry createSecondaryEntry(MultiDataObject obj, FileObject fo) {
        MultiFileLoader loader = this.getMultiFileLoader();
        if (loader != null) {
            return loader.createSecondaryEntryImpl(obj, fo);
        }
        Entry e = fo.isFolder() ? new FileEntry.Folder(obj, fo) : new FileEntry(obj, fo);
        return e;
    }

    private final MultiDataObject createMultiObject(FileObject fo) throws DataObjectExistsException, IOException {
        MultiFileLoader loader = this.getMultiFileLoader();
        MultiDataObject obj = loader != null ? DataObjectPool.createMultiObject(loader, fo) : (MultiDataObject)this.getLoader().findDataObject(fo, RECOGNIZER);
        return obj;
    }

    private final void checkConsistency(MultiDataObject obj) {
        MultiFileLoader loader = this.getMultiFileLoader();
        if (loader != null) {
            loader.checkConsistency(obj);
        }
    }

    private final void checkFiles(MultiDataObject obj) {
        MultiFileLoader loader = this.getMultiFileLoader();
        if (loader != null) {
            loader.checkFiles(obj);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final List<Pair> saveEntries() {
        Object object = this.synchObjectSecondary();
        synchronized (object) {
            LinkedList<Pair> ll = new LinkedList<Pair>();
            ll.add(new Pair(this.getPrimaryEntry()));
            for (Entry en : this.secondaryEntries()) {
                ll.add(new Pair(en));
            }
            return ll;
        }
    }

    final void restoreEntries(List<Pair> backup) {
        for (Pair p : backup) {
            if (p.entry.getFile().equals((Object)p.file)) continue;
            if (p.file.isValid()) {
                p.entry.changeFile(p.file);
                continue;
            }
            try {
                if (p.entry.getFile().isData()) {
                    p.entry.changeFile(p.entry.getFile().copy(p.file.getParent(), p.file.getName(), p.file.getExt()));
                    continue;
                }
                FileObject fo = p.file.getParent().createFolder(p.file.getName());
                FileUtil.copyAttributes((FileObject)p.entry.getFile(), (FileObject)fo);
                p.entry.changeFile(fo);
            }
            catch (IOException e) {}
        }
    }

    @Override
    void notifyFileDeleted(FileEvent fe) {
        this.removeFile(fe.getFile());
        if (fe.getFile().equals((Object)this.getPrimaryFile())) {
            try {
                this.markInvalid0();
            }
            catch (PropertyVetoException ex) {
                Logger.getLogger(MultiDataObject.class.getName()).log(Level.FINE, null, ex);
            }
        }
    }

    @Override
    void notifyFileDataCreated(FileEvent fe) {
        this.checked = false;
    }

    final void updateFilesInCookieSet() {
        CookieSet set = this.getCookieSet(false);
        if (set != null) {
            set.assign(FileObject.class, (Object[])this.files().toArray((T[])new FileObject[0]));
        }
    }

    final void updateNodeInCookieSet() {
        CookieSet set;
        if (this.isValid() && this.associateLookup() >= 1 && (set = this.getCookieSet(false)) != null) {
            set.assign(Node.class, (Object[])new Node[]{this.getNodeDelegate()});
        }
    }

    void checkCookieSet(Class<?> c) {
    }

    private static final class EntryReplace
    implements Serializable {
        static final long serialVersionUID = -1498798537289529182L;
        private FileObject file;
        private transient Entry entry;

        public EntryReplace(FileObject fo) {
            this.file = fo;
        }

        private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
            ois.defaultReadObject();
            try {
                DataObject obj = DataObject.find(this.file);
                if (obj instanceof MultiDataObject) {
                    MultiDataObject m = (MultiDataObject)obj;
                    if (this.file.equals((Object)m.getPrimaryFile())) {
                        this.entry = m.getPrimaryEntry();
                    } else {
                        Entry e = m.findSecondaryEntry(this.file);
                        if (e == null) {
                            throw new InvalidObjectException(obj.toString());
                        }
                        this.entry = e;
                    }
                }
            }
            catch (DataObjectNotFoundException ex) {
                throw new InvalidObjectException(ex.getMessage());
            }
        }

        public Object readResolve() {
            return this.entry;
        }
    }

    private final class ChangeAndBefore
    implements ChangeListener,
    CookieSet.Before {
        private ChangeAndBefore() {
        }

        @Override
        public void stateChanged(ChangeEvent ev) {
            MultiDataObject.this.fireCookieChange();
        }

        public void beforeLookup(Class<?> clazz) {
            if (clazz.isAssignableFrom(FileObject.class)) {
                MultiDataObject.this.updateFilesInCookieSet();
            }
            if (clazz.isAssignableFrom(Node.class)) {
                MultiDataObject.this.updateNodeInCookieSet();
            }
            MultiDataObject.this.checkCookieSet(clazz);
        }
    }

    public abstract class Entry
    implements Serializable {
        static final long serialVersionUID = 6024795908818133571L;
        private FileObject file;
        private transient WeakReference<FileLock> lock;

        protected Entry(FileObject file) {
            if (file == null) {
                throw new NullPointerException();
            }
            this.file = file;
            if (!this.isImportant()) {
                file.setImportant(false);
            }
        }

        final void changeFile(FileObject newFile) {
            FileLock l;
            if (newFile == null) {
                throw new NullPointerException("NPE for " + (Object)this.file);
            }
            if (newFile.equals((Object)this.file)) {
                return;
            }
            if (MultiDataObject.ERR.isLoggable(Level.FINE)) {
                MultiDataObject.ERR.fine("changeFile: " + (Object)newFile + " for " + this + " of " + this.getDataObject());
            }
            newFile.setImportant(this.isImportant());
            this.file = newFile;
            FileLock fileLock = l = this.lock == null ? null : this.lock.get();
            if (l != null && l.isValid()) {
                if (MultiDataObject.ERR.isLoggable(Level.FINE)) {
                    MultiDataObject.ERR.fine("releasing old lock: " + this + " was: " + (Object)l);
                }
                l.releaseLock();
            }
            this.lock = null;
        }

        public final FileObject getFile() {
            return this.file;
        }

        public final MultiDataObject getDataObject() {
            return MultiDataObject.this;
        }

        public boolean isImportant() {
            return true;
        }

        public abstract FileObject copy(FileObject var1, String var2) throws IOException;

        public abstract FileObject rename(String var1) throws IOException;

        public abstract FileObject move(FileObject var1, String var2) throws IOException;

        public abstract void delete() throws IOException;

        public abstract FileObject createFromTemplate(FileObject var1, String var2) throws IOException;

        public FileObject copyRename(FileObject f, String name, String ext) throws IOException {
            throw new IOException("Unsupported operation");
        }

        public FileLock takeLock() throws IOException {
            FileLock l;
            FileLock fileLock = l = this.lock == null ? null : this.lock.get();
            if (l == null || !l.isValid()) {
                l = this.getFile().lock();
                this.lock = new WeakReference<FileLock>(l);
            }
            if (MultiDataObject.ERR.isLoggable(Level.FINE)) {
                MultiDataObject.ERR.fine("takeLock: " + this + " is: " + (Object)l);
            }
            return l;
        }

        public boolean isLocked() {
            FileLock l = this.lock == null ? null : this.lock.get();
            return l != null && l.isValid();
        }

        public boolean equals(Object o) {
            if (!(o instanceof Entry)) {
                return false;
            }
            return this.getFile().equals((Object)((Entry)o).getFile());
        }

        public int hashCode() {
            return this.getFile().hashCode();
        }

        protected Object writeReplace() {
            return new EntryReplace(this.getFile());
        }
    }

    static final class Pair {
        Entry entry;
        FileObject file;

        Pair(Entry e) {
            this.entry = e;
            this.file = e.getFile();
        }
    }

    private static class EmptyRecognizer
    implements DataLoader.RecognizedFiles {
        EmptyRecognizer() {
        }

        @Override
        public void markRecognized(FileObject fo) {
        }
    }

}

