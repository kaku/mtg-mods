/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.InstanceCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 *  org.openide.util.actions.SystemAction
 */
package org.openide.loaders;

import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JSeparator;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataLoader;
import org.openide.loaders.DataObject;
import org.openide.loaders.FolderInstance;
import org.openide.loaders.InstanceDataObject;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.actions.SystemAction;

final class DataLdrActions
extends FolderInstance {
    private Reference<DataLoader> ref;
    private Task creation;
    private static RequestProcessor RP = new RequestProcessor("Loader Actions");

    public DataLdrActions(DataFolder f, DataLoader l) {
        super(f);
        this.ref = new WeakReference<DataLoader>(l);
    }

    public synchronized void setActions(SystemAction[] arr) {
        class DoTheWork
        implements Runnable,
        FileSystem.AtomicAction {
            private int state;
            final /* synthetic */ SystemAction[] val$arr;

            DoTheWork() {
                this.val$arr = var2_2;
            }

            private void work() throws IOException {
                DataObject[] now = this$0.folder.getChildren();
                HashMap<Object, DataObject> nowToObj = new HashMap<Object, DataObject>();
                LinkedList<DataObject> sepObjs = new LinkedList<DataObject>();
                for (int i = 0; i < now.length; ++i) {
                    InstanceCookie ic = now[i].getCookie(InstanceCookie.class);
                    if (ic == null) continue;
                    try {
                        Object instance = ic.instanceCreate();
                        if (instance instanceof Action) {
                            nowToObj.put(instance, now[i]);
                            continue;
                        }
                        if (!(instance instanceof JSeparator)) continue;
                        sepObjs.add(now[i]);
                        continue;
                    }
                    catch (ClassNotFoundException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
                ArrayList<DataObject> order = new ArrayList<DataObject>();
                for (int i2 = 0; i2 < this.val$arr.length; ++i2) {
                    DataObject obj2 = (DataObject)nowToObj.remove((Object)this.val$arr[i2]);
                    if (obj2 == null) {
                        obj2 = this.val$arr[i2] != null ? InstanceDataObject.create(this$0.folder, null, this.val$arr[i2].getClass()) : (!sepObjs.isEmpty() ? (DataObject)sepObjs.removeFirst() : InstanceDataObject.create(this$0.folder, "Separator" + order.size(), JSeparator.class));
                    }
                    order.add(obj2);
                }
                for (DataObject obj2 : nowToObj.values()) {
                    obj2.delete();
                }
                for (DataObject obj2 : sepObjs) {
                    obj2.delete();
                }
                this$0.folder.setOrder(order.toArray(new DataObject[0]));
            }

            @Override
            public void run() {
                try {
                    switch (this.state) {
                        case 0: {
                            this.state = 1;
                            this$0.folder.getPrimaryFile().getFileSystem().runAtomicAction((FileSystem.AtomicAction)this);
                            break;
                        }
                        case 1: {
                            this.work();
                        }
                    }
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }
        DoTheWork dtw = new DoTheWork(this, arr);
        this.creation = RP.post((Runnable)dtw);
    }

    @Override
    protected Object createInstance(InstanceCookie[] cookies) throws IOException, ClassNotFoundException {
        ArrayList<Action> list = new ArrayList<Action>();
        for (int i = 0; i < cookies.length; ++i) {
            block5 : {
                try {
                    Class clazz = cookies[i].instanceClass();
                    if (JSeparator.class.isAssignableFrom(clazz)) {
                        list.add(null);
                    }
                    break block5;
                }
                catch (ClassNotFoundException cnf) {
                    this.err().log(Level.INFO, "Cannot resolve registration of {0}", (Object)cookies[i]);
                    this.err().log(Level.CONFIG, cnf.getMessage(), cnf);
                }
                continue;
            }
            Object action = cookies[i].instanceCreate();
            if (!(action instanceof Action)) continue;
            list.add((Action)action);
        }
        DataLoader l = this.ref.get();
        if (l != null) {
            l.setSwingActions(list);
        }
        return list.toArray(new Action[0]);
    }

    @Override
    protected InstanceCookie acceptFolder(DataFolder df) {
        return null;
    }

    @Override
    protected Task postCreationTask(Runnable run) {
        return RP.post(run);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void waitFinished() {
        Task t;
        DataLdrActions dataLdrActions = this;
        synchronized (dataLdrActions) {
            t = this.creation;
        }
        if (t != null) {
            t.waitFinished();
        }
        super.waitFinished();
    }

}

