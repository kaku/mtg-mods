/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.explorer.ExplorerManager
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 */
package org.openide.loaders;

import java.awt.Component;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;
import org.openide.explorer.ExplorerManager;
import org.openide.loaders.TemplateWizard1;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;

final class TemplateWizardPanel1
implements WizardDescriptor.Panel<WizardDescriptor> {
    private TemplateWizard1 templateWizard1UI;
    private ChangeListener listener;

    TemplateWizardPanel1() {
    }

    private TemplateWizard1 getPanelUI() {
        if (this.templateWizard1UI == null) {
            this.templateWizard1UI = new TemplateWizard1();
            this.templateWizard1UI.addChangeListener(this.listener);
        }
        return this.templateWizard1UI;
    }

    public void addChangeListener(ChangeListener l) {
        if (this.listener != null) {
            throw new IllegalStateException();
        }
        if (this.templateWizard1UI != null) {
            this.templateWizard1UI.addChangeListener(l);
        }
        this.listener = l;
    }

    public void removeChangeListener(ChangeListener l) {
        this.listener = null;
        if (this.templateWizard1UI != null) {
            this.templateWizard1UI.removeChangeListener(l);
        }
    }

    public Component getComponent() {
        return this.getPanelUI();
    }

    public HelpCtx getHelp() {
        if (this.templateWizard1UI != null && this.templateWizard1UI.getExplorerManager().getRootContext() != Node.EMPTY) {
            return new HelpCtx(TemplateWizard1.class.getName() + "." + this.templateWizard1UI.getExplorerManager().getRootContext().getName());
        }
        return new HelpCtx(TemplateWizard1.class);
    }

    public boolean isValid() {
        if (this.templateWizard1UI == null) {
            return false;
        }
        return this.getPanelUI().implIsValid();
    }

    public void readSettings(WizardDescriptor settings) {
        this.getPanelUI().implReadSettings((Object)settings);
    }

    public void storeSettings(WizardDescriptor settings) {
        this.getPanelUI().implStoreSettings((Object)settings);
    }
}

