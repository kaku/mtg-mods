/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.openide.loaders;

import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObjectPool;

final class FolderChildrenPair {
    public final FileObject primaryFile;
    public final int seq;

    FolderChildrenPair(FileObject primaryFile) {
        this.primaryFile = primaryFile;
        this.seq = DataObjectPool.getPOOL().registrationCount(primaryFile);
    }

    public int hashCode() {
        return this.primaryFile.hashCode() ^ this.seq;
    }

    public boolean equals(Object o) {
        if (o instanceof FolderChildrenPair) {
            FolderChildrenPair p = (FolderChildrenPair)o;
            if (!this.primaryFile.equals((Object)p.primaryFile)) {
                return false;
            }
            if (this.seq == -1 || p.seq == -1) {
                return true;
            }
            return this.seq == p.seq;
        }
        return false;
    }

    public String toString() {
        return "FolderChildren.Pair[" + (Object)this.primaryFile + "," + this.seq + "]";
    }
}

