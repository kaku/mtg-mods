/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.openide.loaders;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import javax.naming.Binding;
import javax.naming.CompositeName;
import javax.naming.Context;
import javax.naming.InvalidNameException;
import javax.naming.Name;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NameNotFoundException;
import javax.naming.NameParser;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.OperationNotSupportedException;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;

final class DefaultSettingsContext
implements Context,
NameParser {
    private final DataObject dobj;
    private final Hashtable env;

    public DefaultSettingsContext(DataObject dobj) {
        this.dobj = dobj;
        this.env = new Hashtable();
    }

    @Override
    public Object addToEnvironment(String propName, Object propVal) throws NamingException {
        return null;
    }

    @Override
    public void bind(Name name, Object obj) throws NamingException {
        String attrName = this.getRelativeName(name);
        FileObject fo = this.dobj.getPrimaryFile();
        Object attrVal = fo.getAttribute(attrName);
        if (attrVal != null) {
            throw new NameAlreadyBoundException(attrName + " = " + attrVal);
        }
        try {
            fo.setAttribute(attrName, obj);
        }
        catch (IOException ex) {
            NamingException ne = new NamingException(attrName + " = " + obj);
            ne.setRootCause(ex);
        }
    }

    @Override
    public void bind(String name, Object obj) throws NamingException {
        this.bind(this.parse(name), obj);
    }

    @Override
    public void close() throws NamingException {
    }

    @Override
    public Name composeName(Name name, Name prefix) throws NamingException {
        throw new OperationNotSupportedException();
    }

    @Override
    public String composeName(String name, String prefix) throws NamingException {
        throw new OperationNotSupportedException();
    }

    @Override
    public Context createSubcontext(Name name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    @Override
    public Context createSubcontext(String name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    @Override
    public void destroySubcontext(Name name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    @Override
    public void destroySubcontext(String name) throws NamingException {
        throw new OperationNotSupportedException();
    }

    public Hashtable getEnvironment() throws NamingException {
        return this.env;
    }

    @Override
    public String getNameInNamespace() throws NamingException {
        return ".";
    }

    @Override
    public NameParser getNameParser(Name name) throws NamingException {
        return this;
    }

    @Override
    public NameParser getNameParser(String name) throws NamingException {
        return this;
    }

    public NamingEnumeration list(String name) throws NamingException {
        return this.list(this.parse(name));
    }

    public NamingEnumeration list(Name name) throws NamingException {
        if (name == null) {
            throw new InvalidNameException("name cannot be null");
        }
        int size = name.size();
        if (size == 0) {
            throw new InvalidNameException("name cannot be empty");
        }
        if (size > 1 || !".".equals(name.get(0))) {
            throw new InvalidNameException("subcontexts unsupported: " + name);
        }
        return new BindingEnumeration(this.dobj.getPrimaryFile());
    }

    @Override
    public NamingEnumeration<Binding> listBindings(Name name) throws NamingException {
        return this.list(name);
    }

    @Override
    public NamingEnumeration<Binding> listBindings(String name) throws NamingException {
        return this.list(name);
    }

    @Override
    public Object lookup(String name) throws NamingException {
        return this.lookup(this.parse(name));
    }

    @Override
    public Object lookup(Name name) throws NamingException {
        String attrName = this.getRelativeName(name);
        return this.dobj.getPrimaryFile().getAttribute(attrName);
    }

    @Override
    public Object lookupLink(String name) throws NamingException {
        return this.lookupLink(this.parse(name));
    }

    @Override
    public Object lookupLink(Name name) throws NamingException {
        return this.lookup(name);
    }

    @Override
    public void rebind(Name name, Object obj) throws NamingException {
        String attrName = this.getRelativeName(name);
        FileObject fo = this.dobj.getPrimaryFile();
        try {
            fo.setAttribute(attrName, obj);
        }
        catch (IOException ex) {
            NamingException ne = new NamingException(name + " = " + obj);
            ne.setRootCause(ex);
        }
    }

    @Override
    public void rebind(String name, Object obj) throws NamingException {
        this.rebind(this.parse(name), obj);
    }

    @Override
    public Object removeFromEnvironment(String propName) throws NamingException {
        return null;
    }

    @Override
    public void rename(Name oldName, Name newName) throws NamingException {
        String oldAttrName = this.getRelativeName(oldName);
        String newAttrName = this.getRelativeName(newName);
        FileObject fo = this.dobj.getPrimaryFile();
        Object attrVal = fo.getAttribute(newAttrName);
        if (attrVal != null) {
            throw new NameAlreadyBoundException(newAttrName + " = " + attrVal);
        }
        try {
            attrVal = fo.getAttribute(oldAttrName);
            fo.setAttribute(newAttrName, attrVal);
            fo.setAttribute(oldAttrName, (Object)null);
        }
        catch (IOException ex) {
            NamingException ne = new NamingException(oldName + "->" + newName);
            ne.setRootCause(ex);
        }
    }

    @Override
    public void rename(String oldName, String newName) throws NamingException {
        this.rename(this.parse(oldName), this.parse(newName));
    }

    @Override
    public void unbind(String name) throws NamingException {
        this.unbind(this.parse(name));
    }

    @Override
    public void unbind(Name name) throws NamingException {
        String attrName = this.getRelativeName(name);
        FileObject fo = this.dobj.getPrimaryFile();
        if (fo.getAttribute(attrName) == null) {
            NameNotFoundException ne = new NameNotFoundException();
            ne.setResolvedName(name);
            throw ne;
        }
        try {
            fo.setAttribute(attrName, (Object)null);
        }
        catch (IOException ex) {
            NamingException ne = new NamingException();
            ne.setResolvedName(name);
            ne.setRootCause(ex);
        }
    }

    @Override
    public Name parse(String name) throws NamingException {
        if (name == null) {
            throw new InvalidNameException("name cannot be null");
        }
        return new CompositeName(name);
    }

    private String getRelativeName(Name name) throws NamingException {
        if (name == null) {
            throw new InvalidNameException("name cannot be null");
        }
        if (name.isEmpty()) {
            throw new InvalidNameException("name cannot be empty");
        }
        String rel = null;
        Enumeration<String> en = name.getAll();
        while (en.hasMoreElements()) {
            if (rel == null) {
                String item = en.nextElement();
                if (".".equals(item)) continue;
                if ("..".equals(item)) {
                    throw new InvalidNameException("subcontexts unsupported: " + name);
                }
                rel = item;
                continue;
            }
            throw new InvalidNameException("subcontexts unsupported: " + name);
        }
        return rel;
    }

    public String toString() {
        String retValue = super.toString();
        return retValue + '[' + this.dobj + ']';
    }

    private static final class BindingEnumeration
    implements NamingEnumeration<Binding> {
        private final Enumeration<String> en;
        private final FileObject fo;

        public BindingEnumeration(FileObject fo) {
            this.fo = fo;
            this.en = fo.getAttributes();
        }

        @Override
        public void close() throws NamingException {
        }

        @Override
        public boolean hasMore() throws NamingException {
            return this.hasMoreElements();
        }

        @Override
        public boolean hasMoreElements() {
            return this.en.hasMoreElements();
        }

        @Override
        public Binding next() throws NamingException {
            return this.nextElement();
        }

        @Override
        public Binding nextElement() {
            String name = this.en.nextElement();
            Object val = this.fo.getAttribute(name);
            return new Binding(name, val);
        }
    }

}

