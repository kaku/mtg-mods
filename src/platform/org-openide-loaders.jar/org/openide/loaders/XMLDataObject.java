/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.CloseCookie
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.EditorCookie$Observable
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.InstanceCookie$Of
 *  org.openide.cookies.OpenCookie
 *  org.openide.cookies.PrintCookie
 *  org.openide.cookies.SaveCookie
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.nodes.Children
 *  org.openide.nodes.CookieSet
 *  org.openide.nodes.CookieSet$Factory
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.FilterNode$Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.CloneableEditorSupport$Env
 *  org.openide.text.CloneableEditorSupport$Pane
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Pair
 *  org.openide.windows.CloneableOpenSupport
 *  org.openide.xml.EntityCatalog
 *  org.openide.xml.XMLUtil
 */
package org.openide.loaders;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import org.netbeans.modules.openide.loaders.RuntimeCatalog;
import org.openide.cookies.CloseCookie;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.InstanceCookie;
import org.openide.cookies.OpenCookie;
import org.openide.cookies.PrintCookie;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.FileEntry;
import org.openide.loaders.MultiDOEditor;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.loaders.SaveAsCapable;
import org.openide.loaders.XMLDataObjectInfoParser;
import org.openide.loaders.XMLEntityResolverChain;
import org.openide.nodes.Children;
import org.openide.nodes.CookieSet;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.DataEditorSupport;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.AbstractLookup;
import org.openide.windows.CloneableOpenSupport;
import org.openide.xml.EntityCatalog;
import org.openide.xml.XMLUtil;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Parser;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderAdapter;

public class XMLDataObject
extends MultiDataObject {
    static final long serialVersionUID = 8757854986453256578L;
    @Deprecated
    public static final String XMLINFO_DTD_PUBLIC_ID_FORTE = "-//Forte for Java//DTD xmlinfo//EN";
    @Deprecated
    public static final String XMLINFO_DTD_PUBLIC_ID = "-//NetBeans IDE//DTD xmlinfo//EN";
    public static final String MIME = "text/xml";
    public static final int STATUS_NOT = 0;
    public static final int STATUS_OK = 1;
    public static final int STATUS_WARNING = 2;
    public static final int STATUS_ERROR = 3;
    public static final String PROP_DOCUMENT = "document";
    @Deprecated
    public static final String PROP_INFO = "info";
    private static ErrorPrinter errorHandler = new ErrorPrinter();
    @Deprecated
    private static XMLEntityResolverChain chainingEntityResolver;
    private static HashMap<String, Info> infos;
    private static Object emgrLock;
    private DelDoc doc;
    private int status = 0;
    @Deprecated
    private EditorCookie editor = null;
    private XMLDataObjectInfoParser infoParser;
    static final Logger ERR;
    static final Constructor<?> cnstr;

    public XMLDataObject(FileObject fo, MultiFileLoader loader) throws DataObjectExistsException {
        this(fo, loader, true);
    }

    protected XMLDataObject(FileObject fo, MultiFileLoader loader, boolean registerEditor) throws DataObjectExistsException {
        super(fo, loader);
        if (registerEditor) {
            this.registerEditor();
        }
    }

    private void registerEditor() {
        CookieSet.Factory factory = new CookieSet.Factory(){

            public <T extends Node.Cookie> T createCookie(Class<T> klass) {
                if (klass.isAssignableFrom(EditorCookie.class) || klass.isAssignableFrom(OpenCookie.class) || klass.isAssignableFrom(CloseCookie.class) || klass.isAssignableFrom(PrintCookie.class)) {
                    if (XMLDataObject.this.editor == null) {
                        XMLDataObject.this.editor = XMLDataObject.this.createEditorCookie();
                    }
                    if (XMLDataObject.this.editor == null) {
                        return null;
                    }
                    return (T)(klass.isAssignableFrom(XMLDataObject.this.editor.getClass()) ? (Node.Cookie)klass.cast((Object)XMLDataObject.this.editor) : null);
                }
                return null;
            }
        };
        CookieSet cookies = this.getCookieSet();
        cookies.add(EditorCookie.class, factory);
        cookies.add(OpenCookie.class, factory);
        cookies.add(CloseCookie.class, factory);
        cookies.add(PrintCookie.class, factory);
        cookies.assign(SaveAsCapable.class, (Object[])new SaveAsCapable[]{new SaveAsCapable(){

            @Override
            public void saveAs(FileObject folder, String fileName) throws IOException {
                EditorCookie ec = (EditorCookie)XMLDataObject.this.getCookieSet().getCookie(EditorCookie.class);
                if (ec instanceof DataEditorSupport) {
                    ((DataEditorSupport)ec).saveAs(folder, fileName);
                } else {
                    Logger.getLogger(XMLDataObject.class.getName()).log(Level.FINE, "'Save As' requires DataEditorSupport");
                }
            }
        }});
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private final XMLDataObjectInfoParser getIP() {
        Object object = emgrLock;
        synchronized (object) {
            if (this.infoParser == null) {
                this.infoParser = new XMLDataObjectInfoParser(this);
            }
        }
        return this.infoParser;
    }

    @Override
    protected Node createNodeDelegate() {
        return new XMLNode(this);
    }

    @Deprecated
    protected void updateIconBase(String res) {
    }

    @Override
    protected void handleDelete() throws IOException {
        this.getIP().waitFinished();
        super.handleDelete();
    }

    @Override
    public HelpCtx getHelpCtx() {
        try {
            if (this.getPrimaryFile().getFileSystem().isDefault() && this.getCookie(InstanceCookie.class) != null) {
                return HelpCtx.DEFAULT_HELP;
            }
        }
        catch (FileStateInvalidException fsie) {
            // empty catch block
        }
        return new HelpCtx(XMLDataObject.class);
    }

    @Override
    public <T extends Node.Cookie> T getCookie(Class<T> cls) {
        this.getIP().waitFinished();
        Object cake = this.getIP().lookupCookie(cls);
        if (ERR.isLoggable(Level.FINE)) {
            ERR.fine("Query for " + cls + " for " + this);
            ERR.fine("Gives a cake " + cake + " for " + this);
        }
        if (cake instanceof InstanceCookie) {
            cake = this.ofCookie((InstanceCookie)cake, cls);
        }
        if (ERR.isLoggable(Level.FINE)) {
            ERR.fine("After ofCookie: " + cake + " for " + this);
        }
        if (cake == null) {
            cake = super.getCookie(cls);
        }
        if (ERR.isLoggable(Level.FINE)) {
            ERR.fine("getCookie returns " + cake + " for " + this);
        }
        if (cake instanceof Node.Cookie) {
            assert (cake == null || cls.isInstance(cake));
            return (T)((Node.Cookie)cls.cast(cake));
        }
        return null;
    }

    @Override
    public Lookup getLookup() {
        if (this.getClass() == XMLDataObject.class) {
            Node n = this.getNodeDelegateOrNull();
            if (n == null) {
                n = this.createNodeDelegate();
                this.setNodeDelegate(n);
            }
            return n.getLookup();
        }
        return super.getLookup();
    }

    private InstanceCookie ofCookie(InstanceCookie ic, Class<?> cls) {
        if (ic instanceof InstanceCookie.Of) {
            return ic;
        }
        if (!cls.isAssignableFrom(ICDel.class)) {
            return ic;
        }
        ICDel d = new ICDel(this, ic);
        return d;
    }

    private void notifyEx(Exception e) {
        Exceptions.attachLocalizedMessage((Throwable)e, (String)"Cannot resolve following class in xmlinfo.");
        Exceptions.printStackTrace((Throwable)e);
    }

    @Deprecated
    protected EditorCookie createEditorCookie() {
        return new XMLEditorSupport(this);
    }

    private final void addSaveCookie(SaveCookie save) {
        this.getCookieSet().add((Node.Cookie)save);
    }

    private final void removeSaveCookie(SaveCookie save) {
        this.getCookieSet().remove((Node.Cookie)save);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final Document getDocument() throws IOException, SAXException {
        if (ERR.isLoggable(Level.FINE)) {
            ERR.fine("getDocument for " + this);
        }
        XMLDataObject xMLDataObject = this;
        synchronized (xMLDataObject) {
            DelDoc d = this.doc;
            if (d == null) {
                this.doc = d = new DelDoc(this);
            }
            return d.getProxyDocument();
        }
    }

    final void clearDocument() {
        if (ERR.isLoggable(Level.FINE)) {
            ERR.fine("clearDocument for " + this);
        }
        this.doc = null;
        this.firePropertyChange("document", null, null);
    }

    @Override
    void notifyFileChanged(FileEvent fe) {
        super.notifyFileChanged(fe);
        this.getIP().fileChanged(fe);
    }

    public final int getStatus() {
        return this.status;
    }

    @Deprecated
    public final Info getInfo() {
        return null;
    }

    @Deprecated
    public final synchronized void setInfo(Info ii) throws IOException {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final Document parsePrimaryFile() throws IOException, SAXException {
        if (ERR.isLoggable(Level.FINE)) {
            ERR.fine("parsePrimaryFile for " + this);
        }
        String loc = this.getPrimaryFile().getURL().toExternalForm();
        try {
            return XMLUtil.parse((InputSource)new InputSource(loc), (boolean)false, (boolean)true, (ErrorHandler)errorHandler, (EntityResolver)XMLDataObject.getSystemResolver());
        }
        catch (IOException e) {
            InputStream is = this.getPrimaryFile().getInputStream();
            try {
                Document document = XMLUtil.parse((InputSource)new InputSource(is), (boolean)false, (boolean)true, (ErrorHandler)errorHandler, (EntityResolver)XMLDataObject.getSystemResolver());
                return document;
            }
            finally {
                is.close();
            }
        }
    }

    @Deprecated
    public static Document parse(URL url) throws IOException, SAXException {
        return XMLDataObject.parse(url, errorHandler, false);
    }

    @Deprecated
    public static Document parse(URL url, boolean validate) throws IOException, SAXException {
        return XMLDataObject.parse(url, errorHandler, validate);
    }

    @Deprecated
    public static Document parse(URL url, ErrorHandler eh) throws IOException, SAXException {
        return XMLDataObject.parse(url, eh, false);
    }

    @Deprecated
    public static Document parse(URL url, ErrorHandler eh, boolean validate) throws IOException, SAXException {
        return XMLUtil.parse((InputSource)new InputSource(url.toExternalForm()), (boolean)validate, (boolean)false, (ErrorHandler)eh, (EntityResolver)XMLDataObject.getChainingEntityResolver());
    }

    @Deprecated
    public static Parser createParser() {
        return XMLDataObject.createParser(false);
    }

    @Deprecated
    public static Parser createParser(boolean validate) {
        try {
            XMLReaderAdapter parser = new XMLReaderAdapter(XMLUtil.createXMLReader((boolean)validate));
            parser.setEntityResolver(XMLDataObject.getChainingEntityResolver());
            return parser;
        }
        catch (SAXException ex) {
            Exceptions.attachLocalizedMessage((Throwable)ex, (String)"Can not create a SAX parser!\nCheck javax.xml.parsers.SAXParserFactory property features and the parser library presence on classpath.");
            Exceptions.printStackTrace((Throwable)ex);
            return null;
        }
    }

    @Deprecated
    public static Document createDocument() {
        try {
            DocumentBuilderFactory factory;
            DocumentBuilder builder;
            try {
                factory = DocumentBuilderFactory.newInstance();
                factory.setValidating(false);
                factory.setNamespaceAware(false);
            }
            catch (FactoryConfigurationError err) {
                Exceptions.attachLocalizedMessage((Throwable)err, (String)"Can not create a factory!\nCheck javax.xml.parsers.DocumentBuilderFactory  property and the factory library presence on classpath.");
                Exceptions.printStackTrace((Throwable)err);
                return null;
            }
            try {
                builder = factory.newDocumentBuilder();
            }
            catch (ParserConfigurationException ex) {
                SAXException sex = new SAXException("Configuration exception.");
                sex.initCause(ex);
                Exceptions.attachLocalizedMessage((Throwable)sex, (String)"Can not create a DOM builder!\nCheck javax.xml.parsers.DocumentBuilderFactory property and the builder library presence on classpath.");
                throw sex;
            }
            return builder.newDocument();
        }
        catch (SAXException ex) {
            return null;
        }
    }

    @Deprecated
    public static void write(Document doc, Writer writer) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        XMLUtil.write((Document)doc, (OutputStream)baos, (String)"UTF-8");
        writer.write(baos.toString("UTF-8"));
    }

    @Deprecated
    public static void write(Document doc, OutputStream out, String enc) throws IOException {
        XMLUtil.write((Document)doc, (OutputStream)out, (String)enc);
    }

    @Deprecated
    public static InputSource createInputSource(URL url) throws IOException {
        return new InputSource(url.toExternalForm());
    }

    @Deprecated
    public static void registerCatalogEntry(String publicId, String uri) {
        ((RuntimeCatalog)((Object)Lookup.getDefault().lookup(RuntimeCatalog.class))).registerCatalogEntry(publicId, uri);
    }

    @Deprecated
    public static void registerCatalogEntry(String publicId, String resourceName, ClassLoader loader) {
        ((RuntimeCatalog)((Object)Lookup.getDefault().lookup(RuntimeCatalog.class))).registerCatalogEntry(publicId, resourceName, loader);
    }

    @Deprecated
    public static boolean addEntityResolver(EntityResolver resolver) {
        return XMLDataObject.getChainingEntityResolver().addEntityResolver(resolver);
    }

    @Deprecated
    public static EntityResolver removeEntityResolver(EntityResolver resolver) {
        return XMLDataObject.getChainingEntityResolver().removeEntityResolver(resolver);
    }

    @Deprecated
    private static synchronized XMLEntityResolverChain getChainingEntityResolver() {
        if (chainingEntityResolver == null) {
            chainingEntityResolver = new XMLEntityResolverChain();
            chainingEntityResolver.addEntityResolver(XMLDataObject.getSystemResolver());
        }
        return chainingEntityResolver;
    }

    private static EntityResolver getSystemResolver() {
        return EntityCatalog.getDefault();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Deprecated
    public static void registerInfo(String publicId, Info info) {
        HashMap<String, Info> hashMap = infos;
        synchronized (hashMap) {
            if (info == null) {
                infos.remove(publicId);
            } else {
                infos.put(publicId, info);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Deprecated
    public static Info getRegisteredInfo(String publicId) {
        HashMap<String, Info> hashMap = infos;
        synchronized (hashMap) {
            Info ret = infos.get(publicId);
            return ret == null ? null : (Info)ret.clone();
        }
    }

    static Lookup createInfoLookup(XMLDataObject obj, Info info) {
        return new InfoLkp(obj, info);
    }

    private Node findNode() {
        Node n = (Node)this.getIP().lookupCookie(Node.class);
        if (n == null) {
            return new PlainDataNode();
        }
        return n;
    }

    static {
        infos = new HashMap();
        emgrLock = new Object();
        ERR = Logger.getLogger(XMLDataObject.class.getName());
        try {
            Class proxy = Proxy.getProxyClass(XMLDataObject.class.getClassLoader(), Document.class, DocumentType.class);
            cnstr = proxy.getConstructor(InvocationHandler.class);
            new DelDoc(null);
        }
        catch (NoSuchMethodException ex) {
            throw new IllegalStateException(ex);
        }
    }

    private static final class DelDoc
    implements InvocationHandler {
        private final XMLDataObject obj;
        private Reference<Document> xmlDocument;
        private final Document proxyDocument;

        DelDoc(XMLDataObject obj) {
            this.obj = obj;
            try {
                this.proxyDocument = (Document)XMLDataObject.cnstr.newInstance(this);
            }
            catch (Exception ex) {
                throw new IllegalStateException(ex);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private final Document getDocumentImpl(boolean force) {
            DelDoc delDoc = this;
            synchronized (delDoc) {
                Document doc;
                Document document = doc = this.xmlDocument == null ? null : this.xmlDocument.get();
                if (doc != null) {
                    return doc;
                }
                if (!force) {
                    return null;
                }
                this.obj.status = 1;
                try {
                    Document d = this.obj.parsePrimaryFile();
                    this.xmlDocument = new SoftReference<Document>(d);
                    return d;
                }
                catch (SAXException e) {
                    XMLDataObject.ERR.log(Level.WARNING, null, e);
                }
                catch (IOException e) {
                    XMLDataObject.ERR.log(Level.WARNING, null, e);
                }
                this.obj.status = 3;
                Document d = XMLUtil.createDocument((String)"brokenDocument", (String)null, (String)null, (String)null);
                this.xmlDocument = new SoftReference<Document>(d);
                this.obj.firePropertyChange("document", null, null);
                return d;
            }
        }

        public Document getProxyDocument() {
            return this.proxyDocument;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (method.getName().equals("getDoctype") && args == null) {
                return (DocumentType)((Object)this.proxyDocument);
            }
            if (method.getName().equals("getPublicId") && args == null) {
                Document d = this.getDocumentImpl(false);
                if (d != null) {
                    DocumentType doctype = d.getDoctype();
                    return doctype == null ? null : doctype.getPublicId();
                }
                return this.obj.getIP().getPublicId();
            }
            return method.invoke(this.getDocumentImpl(true), args);
        }
    }

    private static class ICDel
    implements InstanceCookie.Of {
        private XMLDataObject obj;
        private InstanceCookie ic;

        public ICDel(XMLDataObject obj, InstanceCookie ic) {
            this.obj = obj;
            this.ic = ic;
        }

        public String instanceName() {
            return this.ic.instanceName();
        }

        public Class<?> instanceClass() throws IOException, ClassNotFoundException {
            return this.ic.instanceClass();
        }

        public Object instanceCreate() throws IOException, ClassNotFoundException {
            return this.ic.instanceCreate();
        }

        public boolean instanceOf(Class<?> cls2) {
            if (this.ic instanceof InstanceCookie.Of) {
                return ((InstanceCookie.Of)this.ic).instanceOf(cls2);
            }
            try {
                return cls2.isAssignableFrom(this.instanceClass());
            }
            catch (IOException ioe) {
                return false;
            }
            catch (ClassNotFoundException cnfe) {
                return false;
            }
        }

        public int hashCode() {
            return 2 * this.obj.hashCode() + this.ic.hashCode();
        }

        public boolean equals(Object obj) {
            if (obj instanceof ICDel) {
                ICDel d = (ICDel)obj;
                return d.obj == obj && d.ic == this.ic;
            }
            return false;
        }
    }

    final class XMLNode
    extends FilterNode {
        public XMLNode(XMLDataObject obj) {
            this(obj.findNode());
        }

        private XMLNode(Node del) {
            super(del, (Children)new FilterNode.Children(del));
        }

        final void update() {
            this.changeOriginal(XMLDataObject.this.findNode(), true);
        }
    }

    private final class PlainDataNode
    extends DataNode {
        public PlainDataNode() {
            super(XMLDataObject.this, Children.LEAF);
            this.setIconBaseWithExtension("org/openide/loaders/xmlObject.gif");
        }
    }

    private static final class InfoLkp
    extends AbstractLookup {
        public final Info info;

        public InfoLkp(XMLDataObject obj, Info info) {
            this.info = info;
            Iterator it = info.processorClasses();
            ArrayList<InfoPair> arr = new ArrayList<InfoPair>(info.processors.size());
            while (it.hasNext()) {
                Class c = it.next();
                arr.add(new InfoPair(obj, c));
            }
            this.setPairs(arr);
        }

        private static final class InfoPair
        extends AbstractLookup.Pair {
            private Class<?> clazz;
            private Object obj;

            protected InfoPair(XMLDataObject obj, Class<?> c) {
                this.obj = obj;
                this.clazz = c;
            }

            protected boolean instanceOf(Class c) {
                Class cc = c;
                Class temp = this.clazz;
                if (temp == null) {
                    return cc.isInstance(this.obj);
                }
                return cc.isAssignableFrom(temp);
            }

            protected boolean creatorOf(Object obj) {
                return this.obj == obj;
            }

            public synchronized Object getInstance() {
                if (this.clazz == null) {
                    return this.obj;
                }
                XMLDataObject xmlDataObject = (XMLDataObject)this.obj;
                this.obj = null;
                Class next = this.clazz;
                this.clazz = null;
                try {
                    if (Processor.class.isAssignableFrom(next)) {
                        this.obj = next.newInstance();
                        Processor proc = (Processor)this.obj;
                        proc.attachTo(xmlDataObject);
                        return this.obj;
                    }
                    Constructor<?>[] arr = next.getConstructors();
                    for (int i = 0; i < arr.length; ++i) {
                        Class<?>[] params = arr[i].getParameterTypes();
                        if (params.length != 1 || params[0] != DataObject.class && params[0] != XMLDataObject.class) continue;
                        this.obj = arr[i].newInstance(xmlDataObject);
                        return this.obj;
                    }
                    throw new InternalError("XMLDataObject processor class " + next + " invalid");
                }
                catch (InvocationTargetException e) {
                    xmlDataObject.notifyEx(e);
                }
                catch (InstantiationException e) {
                    xmlDataObject.notifyEx(e);
                }
                catch (IllegalAccessException e) {
                    xmlDataObject.notifyEx(e);
                }
                return this.obj;
            }

            public Class getType() {
                Class temp = this.clazz;
                return temp != null ? temp : this.obj.getClass();
            }

            public String getId() {
                return "Info[" + this.getType().getName();
            }

            public String getDisplayName() {
                return this.getType().getName();
            }
        }

    }

    @Deprecated
    public static final class Info
    implements Cloneable {
        List<Class<?>> processors = new ArrayList();
        String iconBase = null;

        public Object clone() {
            Info ii = new Info();
            for (Class proc : this.processors) {
                ii.processors.add(proc);
            }
            ii.iconBase = this.iconBase;
            return ii;
        }

        public synchronized void addProcessorClass(Class<?> proc) {
            if (!Processor.class.isAssignableFrom(proc)) {
                Constructor<?>[] arr = proc.getConstructors();
                for (int i = 0; i < arr.length; ++i) {
                    Class<?>[] params = arr[i].getParameterTypes();
                    if (params.length != 1 || params[0] != DataObject.class && params[0] != XMLDataObject.class) continue;
                    arr = null;
                    break;
                }
                if (arr != null) {
                    throw new IllegalArgumentException();
                }
            }
            this.processors.add(proc);
        }

        public boolean removeProcessorClass(Class<?> proc) {
            return this.processors.remove(proc);
        }

        public Iterator<Class<?>> processorClasses() {
            return this.processors.iterator();
        }

        public void setIconBase(String base) {
            this.iconBase = base;
        }

        public String getIconBase() {
            return this.iconBase;
        }

        public void write(Writer writer) throws IOException {
            throw new IOException("Not supported anymore");
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (!(obj instanceof Info)) {
                return false;
            }
            Info i = (Info)obj;
            return (this.iconBase != null && this.iconBase.equals(i.iconBase) || i.iconBase == this.iconBase) && this.processors.equals(i.processors);
        }
    }

    @Deprecated
    public static interface Processor
    extends Node.Cookie {
        public void attachTo(XMLDataObject var1);
    }

    static final class Loader
    extends MultiFileLoader {
        static final long serialVersionUID = 3917883920409453930L;

        public Loader() {
            super("org.openide.loaders.XMLDataObject");
        }

        @Override
        protected String actionsContext() {
            return "Loaders/text/xml/Actions";
        }

        @Override
        protected String defaultDisplayName() {
            return NbBundle.getMessage(XMLDataObject.class, (String)"PROP_XmlLoader_Name");
        }

        @Override
        protected FileObject findPrimaryFile(FileObject fo) {
            String mime = fo.getMIMEType();
            if (mime.endsWith("/xml") || mime.endsWith("+xml")) {
                return fo;
            }
            return null;
        }

        @Override
        protected MultiDataObject createMultiObject(FileObject primaryFile) throws DataObjectExistsException {
            return new XMLDataObject(primaryFile, this);
        }

        @Override
        protected MultiDataObject.Entry createPrimaryEntry(MultiDataObject obj, FileObject primaryFile) {
            return new FileEntry(obj, primaryFile);
        }

        @Override
        protected MultiDataObject.Entry createSecondaryEntry(MultiDataObject obj, FileObject secondaryFile) {
            return new FileEntry(obj, secondaryFile);
        }
    }

    static class NullHandler
    extends DefaultHandler
    implements LexicalHandler {
        static final NullHandler INSTANCE = new NullHandler();

        NullHandler() {
        }

        @Override
        public void startDTD(String root, String pID, String sID) throws SAXException {
        }

        @Override
        public void endDTD() throws SAXException {
        }

        @Override
        public void startEntity(String name) throws SAXException {
        }

        @Override
        public void endEntity(String name) throws SAXException {
        }

        @Override
        public void startCDATA() throws SAXException {
        }

        @Override
        public void endCDATA() throws SAXException {
        }

        @Override
        public void comment(char[] ch, int start, int length) throws SAXException {
        }
    }

    static class ErrorPrinter
    implements ErrorHandler {
        ErrorPrinter() {
        }

        private void message(String level, SAXParseException e) {
            if (!DataObject.LOG.isLoggable(Level.FINE)) {
                return;
            }
            Object[] arrobject = new Object[5];
            arrobject[0] = level;
            arrobject[1] = e.getMessage();
            arrobject[2] = e.getSystemId() == null ? "" : e.getSystemId();
            arrobject[3] = "" + e.getLineNumber();
            arrobject[4] = "" + e.getColumnNumber();
            String msg = NbBundle.getMessage(XMLDataObject.class, (String)"PROP_XmlMessage", (Object[])arrobject);
            DataObject.LOG.fine(msg);
        }

        @Override
        public void error(SAXParseException e) {
            this.message(NbBundle.getMessage(XMLDataObject.class, (String)"PROP_XmlError"), e);
        }

        @Override
        public void warning(SAXParseException e) {
            this.message(NbBundle.getMessage(XMLDataObject.class, (String)"PROP_XmlWarning"), e);
        }

        @Override
        public void fatalError(SAXParseException e) {
            this.message(NbBundle.getMessage(XMLDataObject.class, (String)"PROP_XmlFatalError"), e);
        }
    }

    private static class XMLEditorSupport
    extends DataEditorSupport
    implements OpenCookie,
    EditorCookie.Observable,
    PrintCookie,
    CloseCookie {
        public XMLEditorSupport(XMLDataObject obj) {
            super(obj, new XMLEditorEnv(obj));
            if (obj.getPrimaryFile().getMIMEType().indexOf("xml") == -1) {
                this.setMIMEType("text/xml");
            }
        }

        protected boolean notifyModified() {
            if (!super.notifyModified()) {
                return false;
            }
            if (this.getDataObject().getCookie(SaveCookie.class) == null) {
                ((XMLDataObject)this.getDataObject()).addSaveCookie(new Save());
                this.getDataObject().setModified(true);
            }
            return true;
        }

        protected void notifyUnmodified() {
            super.notifyUnmodified();
            SaveCookie save = this.getDataObject().getCookie(SaveCookie.class);
            if (save != null) {
                ((XMLDataObject)this.getDataObject()).removeSaveCookie(save);
                this.getDataObject().setModified(false);
            }
        }

        protected CloneableEditorSupport.Pane createPane() {
            if (MultiDOEditor.isMultiViewAvailable()) {
                MultiDataObject mdo = (MultiDataObject)this.getDataObject();
                return MultiDOEditor.createMultiViewPane("text/plain", mdo);
            }
            return super.createPane();
        }

        private static class XMLEditorEnv
        extends DataEditorSupport.Env {
            private static final long serialVersionUID = 6593415381104273008L;

            public XMLEditorEnv(DataObject obj) {
                super(obj);
            }

            @Override
            protected FileObject getFile() {
                return this.getDataObject().getPrimaryFile();
            }

            @Override
            protected FileLock takeLock() throws IOException {
                return ((XMLDataObject)this.getDataObject()).getPrimaryEntry().takeLock();
            }

            @Override
            public CloneableOpenSupport findCloneableOpenSupport() {
                return (CloneableOpenSupport)this.getDataObject().getCookie(EditorCookie.class);
            }
        }

        class Save
        implements SaveCookie {
            Save() {
            }

            public void save() throws IOException {
                XMLEditorSupport.this.saveDocument();
                XMLEditorSupport.this.getDataObject().setModified(false);
            }
        }

    }

}

