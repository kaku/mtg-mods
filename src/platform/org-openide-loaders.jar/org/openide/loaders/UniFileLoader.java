/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.io.SafeException
 */
package org.openide.loaders;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.ExtensionList;
import org.openide.loaders.FileEntry;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.util.io.SafeException;

public abstract class UniFileLoader
extends MultiFileLoader {
    private static final long serialVersionUID = -6190649471408985837L;
    public static final String PROP_EXTENSIONS = "extensions";

    @Deprecated
    protected UniFileLoader(Class<? extends DataObject> representationClass) {
        super(representationClass);
    }

    protected UniFileLoader(String representationClassName) {
        super(representationClassName);
    }

    @Override
    protected FileObject findPrimaryFile(FileObject fo) {
        if (fo.isFolder()) {
            return null;
        }
        return this.getExtensions().isRegistered(fo) ? fo : null;
    }

    @Override
    protected abstract MultiDataObject createMultiObject(FileObject var1) throws DataObjectExistsException, IOException;

    @Override
    protected MultiDataObject.Entry createPrimaryEntry(MultiDataObject obj, FileObject primaryFile) {
        return new FileEntry(obj, primaryFile);
    }

    @Override
    protected MultiDataObject.Entry createSecondaryEntry(MultiDataObject obj, FileObject secondaryFile) {
        StringBuilder buf = new StringBuilder("Error in data system. Please reopen the bug #17014 with the following message: ");
        buf.append("\n  DataLoader:");
        buf.append(this.getClass().getName());
        buf.append("\n  DataObject:");
        buf.append(obj);
        buf.append("\n  PrimaryEntry:");
        buf.append(obj.getPrimaryEntry());
        buf.append("\n  PrimaryFile:");
        buf.append((Object)obj.getPrimaryFile());
        buf.append("\n  SecondaryFile:");
        buf.append((Object)secondaryFile);
        buf.append("\n");
        throw new UnsupportedOperationException(buf.toString());
    }

    @Override
    final DataObject checkCollision(DataObject obj, FileObject file) {
        return null;
    }

    @Override
    final void checkConsistency(MultiDataObject obj) {
    }

    @Override
    final void checkFiles(MultiDataObject obj) {
    }

    public void setExtensions(ExtensionList ext) {
        this.putProperty("extensions", (Object)ext, true);
    }

    public ExtensionList getExtensions() {
        ExtensionList l = (ExtensionList)this.getProperty((Object)"extensions");
        if (l == null) {
            l = new ExtensionList();
            this.putProperty("extensions", (Object)l, false);
        }
        return l;
    }

    @Override
    public void writeExternal(ObjectOutput oo) throws IOException {
        super.writeExternal(oo);
        oo.writeObject(this.getProperty((Object)"extensions"));
    }

    @Override
    public void readExternal(ObjectInput oi) throws IOException, ClassNotFoundException {
        SafeException se;
        try {
            super.readExternal(oi);
            se = null;
        }
        catch (SafeException se2) {
            se = se2;
        }
        this.setExtensions((ExtensionList)oi.readObject());
        if (se != null) {
            throw se;
        }
    }
}

