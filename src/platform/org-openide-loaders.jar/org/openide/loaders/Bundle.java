/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.openide.loaders;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_SettingsFile() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_SettingsFile");
    }

    static String EXC_FIND_4_INVALID(Object the_path) {
        return NbBundle.getMessage(Bundle.class, (String)"EXC_FIND_4_INVALID", (Object)the_path);
    }

    static String EXC_NO_LONGER_VALID(Object arg0) {
        return NbBundle.getMessage(Bundle.class, (String)"EXC_NO_LONGER_VALID", (Object)arg0);
    }

    static String LBL_Copying(Object File_name) {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_Copying", (Object)File_name);
    }

    static String LBL_Deleting(Object Deleted_file_or_folder) {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_Deleting", (Object)Deleted_file_or_folder);
    }

    static String LBL_Moving(Object File_name) {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_Moving", (Object)File_name);
    }

    private void Bundle() {
    }
}

