/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.EditCookie
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.util.WeakSet
 *  org.openide.windows.CloneableOpenSupport
 *  org.openide.windows.CloneableOpenSupport$Env
 *  org.openide.windows.CloneableTopComponent
 *  org.openide.windows.CloneableTopComponent$Ref
 */
package org.openide.loaders;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import org.openide.cookies.EditCookie;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.loaders.DataObject;
import org.openide.loaders.MultiDataObject;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.util.WeakSet;
import org.openide.windows.CloneableOpenSupport;
import org.openide.windows.CloneableTopComponent;

public abstract class OpenSupport
extends CloneableOpenSupport {
    protected MultiDataObject.Entry entry;

    public OpenSupport(MultiDataObject.Entry entry) {
        this(entry, new Env(entry.getDataObject()));
    }

    protected OpenSupport(MultiDataObject.Entry entry, Env env) {
        super((CloneableOpenSupport.Env)env);
        this.entry = entry;
    }

    protected String messageOpening() {
        MultiDataObject obj = this.entry.getDataObject();
        return NbBundle.getMessage(OpenSupport.class, (String)"CTL_ObjectOpen", (Object)obj.getName(), (Object)obj.getPrimaryFile().toString());
    }

    protected String messageOpened() {
        return null;
    }

    final CloneableTopComponent.Ref allEditors() {
        return this.allEditors;
    }

    private static final class Listener
    extends CloneableTopComponent.Ref {
        static final long serialVersionUID = -1934890789745432531L;
        private MultiDataObject.Entry entry;

        Listener() {
        }

        public Object readResolve() {
            MultiDataObject obj = this.entry.getDataObject();
            OpenSupport os = null;
            OpenCookie oc = obj.getCookie(OpenCookie.class);
            if (oc != null && oc instanceof OpenSupport) {
                os = (OpenSupport)oc;
            } else {
                EditCookie edc = obj.getCookie(EditCookie.class);
                if (edc != null && edc instanceof OpenSupport) {
                    os = (OpenSupport)edc;
                } else {
                    EditorCookie ec = obj.getCookie(EditorCookie.class);
                    if (ec != null && ec instanceof OpenSupport) {
                        os = (OpenSupport)ec;
                    }
                }
            }
            if (os == null) {
                return this;
            }
            return os.allEditors();
        }
    }

    private static final class FileSystemNameListener
    implements PropertyChangeListener,
    VetoableChangeListener {
        private final Set<Env> environments = new WeakSet(30);

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void add(Env env) {
            Set<Env> set = this.environments;
            synchronized (set) {
                this.environments.add(env);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("systemName".equals(evt.getPropertyName())) {
                HashSet<Env> envs;
                Set<Env> set = this.environments;
                synchronized (set) {
                    envs = new HashSet<Env>(this.environments);
                }
                for (Env env : envs) {
                    env.firePropertyChange("valid", Boolean.TRUE, Boolean.FALSE);
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
            if ("systemName".equals(evt.getPropertyName())) {
                HashSet<Env> envs;
                Set<Env> set = this.environments;
                synchronized (set) {
                    envs = new HashSet<Env>(this.environments);
                }
                for (Env env : envs) {
                    env.fireVetoableChange("valid", Boolean.TRUE, Boolean.FALSE);
                }
            }
        }
    }

    public static class Env
    implements CloneableOpenSupport.Env,
    Serializable,
    PropertyChangeListener,
    VetoableChangeListener {
        static final long serialVersionUID = -1934890789745432531L;
        private DataObject obj;
        private transient PropertyChangeSupport propSupp;
        private transient VetoableChangeSupport vetoSupp;
        private static final Map<FileSystem, Reference<FileSystemNameListener>> fsListenerMap = new WeakHashMap<FileSystem, Reference<FileSystemNameListener>>(30);
        private static final Object LOCK_SUPPORT = new Object();

        public Env(DataObject obj) {
            this.obj = obj;
            this.init();
        }

        private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
            ois.defaultReadObject();
            this.init();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void init() {
            FileSystemNameListener fsListener;
            FileSystem fs;
            this.obj.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this.obj));
            try {
                fs = this.obj.getPrimaryFile().getFileSystem();
            }
            catch (FileStateInvalidException fsie) {
                throw (IllegalStateException)new IllegalStateException("FileSystem is invalid for " + (Object)this.obj.getPrimaryFile() + "!").initCause((Throwable)fsie);
            }
            boolean initListening = false;
            Map<FileSystem, Reference<FileSystemNameListener>> map = fsListenerMap;
            synchronized (map) {
                Reference<FileSystemNameListener> fsListenerRef = fsListenerMap.get((Object)fs);
                FileSystemNameListener fileSystemNameListener = fsListener = fsListenerRef == null ? null : fsListenerRef.get();
                if (fsListener == null) {
                    fsListener = new FileSystemNameListener();
                    fsListenerMap.put(fs, new WeakReference<FileSystemNameListener>(fsListener));
                    initListening = true;
                }
            }
            if (initListening) {
                fs.addPropertyChangeListener((PropertyChangeListener)fsListener);
                fs.addVetoableChangeListener((VetoableChangeListener)fsListener);
            }
            fsListener.add(this);
        }

        protected final DataObject getDataObject() {
            return this.obj;
        }

        public String toString() {
            return this.obj.getPrimaryFile().getNameExt();
        }

        public void addPropertyChangeListener(PropertyChangeListener l) {
            this.prop().addPropertyChangeListener(l);
        }

        public void removePropertyChangeListener(PropertyChangeListener l) {
            this.prop().removePropertyChangeListener(l);
        }

        public void addVetoableChangeListener(VetoableChangeListener l) {
            this.veto().addVetoableChangeListener(l);
        }

        public void removeVetoableChangeListener(VetoableChangeListener l) {
            this.veto().removeVetoableChangeListener(l);
        }

        public boolean isValid() {
            return this.getDataObject().isValid();
        }

        public boolean isModified() {
            return this.getDataObject().isModified();
        }

        public void markModified() throws IOException {
            this.getDataObject().setModified(true);
        }

        public void unmarkModified() {
            this.getDataObject().setModified(false);
        }

        public CloneableOpenSupport findCloneableOpenSupport() {
            OpenCookie oc = this.getDataObject().getCookie(OpenCookie.class);
            if (oc != null && oc instanceof CloneableOpenSupport) {
                return (CloneableOpenSupport)oc;
            }
            EditCookie edc = this.getDataObject().getCookie(EditCookie.class);
            if (edc != null && edc instanceof CloneableOpenSupport) {
                return (CloneableOpenSupport)edc;
            }
            EditorCookie ec = this.getDataObject().getCookie(EditorCookie.class);
            if (ec != null && ec instanceof CloneableOpenSupport) {
                return (CloneableOpenSupport)ec;
            }
            return null;
        }

        @Override
        public void propertyChange(PropertyChangeEvent ev) {
            if ("modified".equals(ev.getPropertyName())) {
                if (this.getDataObject().isModified()) {
                    this.getDataObject().addVetoableChangeListener(this);
                } else {
                    this.getDataObject().removeVetoableChangeListener(this);
                }
            }
            this.firePropertyChange(ev.getPropertyName(), ev.getOldValue(), ev.getNewValue());
        }

        @Override
        public void vetoableChange(PropertyChangeEvent ev) throws PropertyVetoException {
            this.fireVetoableChange(ev.getPropertyName(), ev.getOldValue(), ev.getNewValue());
        }

        protected void firePropertyChange(String name, Object oldValue, Object newValue) {
            this.prop().firePropertyChange(name, oldValue, newValue);
        }

        protected void fireVetoableChange(String name, Object oldValue, Object newValue) throws PropertyVetoException {
            this.veto().fireVetoableChange(name, oldValue, newValue);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private PropertyChangeSupport prop() {
            Object object = LOCK_SUPPORT;
            synchronized (object) {
                if (this.propSupp == null) {
                    this.propSupp = new PropertyChangeSupport(this);
                }
                return this.propSupp;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private VetoableChangeSupport veto() {
            Object object = LOCK_SUPPORT;
            synchronized (object) {
                if (this.vetoSupp == null) {
                    this.vetoSupp = new VetoableChangeSupport(this);
                }
                return this.vetoSupp;
            }
        }
    }

}

