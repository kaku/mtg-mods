/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$Status
 *  org.openide.filesystems.FileUtil
 *  org.openide.nodes.CookieSet
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 */
package org.openide.loaders;

import java.awt.Image;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.Set;
import javax.swing.Action;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataLdrActions;
import org.openide.loaders.DataLoader;
import org.openide.loaders.DataLoaderPool;
import org.openide.loaders.DataObject;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.CookieSet;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;

class MimeFactory<T extends DataObject>
implements DataObject.Factory {
    final Class<? extends T> clazz;
    final Constructor<? extends T> factory;
    final String mimeType;
    Image img;
    final FileObject fo;

    public MimeFactory(Class<? extends T> clazz, String mimeType, Image img, FileObject fo) {
        this.clazz = clazz;
        this.mimeType = mimeType;
        this.img = img;
        try {
            this.factory = clazz.getConstructor(FileObject.class, MultiFileLoader.class);
            this.factory.setAccessible(true);
        }
        catch (NoSuchMethodException ex) {
            throw (IllegalStateException)new IllegalStateException(ex.getMessage()).initCause(ex);
        }
        this.fo = fo;
    }

    public static MimeFactory<DataObject> layer(FileObject fo) throws ClassNotFoundException {
        String className = (String)fo.getAttribute("dataObjectClass");
        if (className == null) {
            throw new IllegalStateException("No attribute dataObjectClass for " + (Object)fo);
        }
        String mimeType = (String)fo.getAttribute("mimeType");
        ClassLoader l = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        if (l == null) {
            l = Thread.currentThread().getContextClassLoader();
        }
        if (l == null) {
            l = MimeFactory.class.getClassLoader();
        }
        Class<DataObject> clazz = l.loadClass(className).asSubclass(DataObject.class);
        return new MimeFactory<DataObject>(clazz, mimeType, null, fo);
    }

    @Override
    public DataObject findDataObject(FileObject fo, Set<? super FileObject> recognized) throws IOException {
        DataObject obj = null;
        Throwable e = null;
        try {
            obj = (DataObject)this.factory.newInstance(new Object[]{fo, DataLoaderPool.getDefaultFileLoader()});
        }
        catch (InstantiationException ex) {
            e = ex;
        }
        catch (IllegalAccessException ex) {
            e = ex;
        }
        catch (IllegalArgumentException ex) {
            e = ex;
        }
        catch (InvocationTargetException ex) {
            if (ex.getTargetException() instanceof IOException) {
                throw (IOException)ex.getTargetException();
            }
            e = ex;
        }
        if (obj == null) {
            throw (IOException)new IOException(e.getMessage()).initCause(e);
        }
        if (obj instanceof MultiDataObject) {
            MultiDataObject mdo = (MultiDataObject)obj;
            mdo.getCookieSet().assign(DataObject.Factory.class, (Object[])new DataObject.Factory[]{this});
        }
        return obj;
    }

    final Image getImage(int type) {
        if (this.img == null && this.fo != null) {
            this.img = ImageUtilities.loadImage((String)"org/openide/loaders/empty.gif", (boolean)true);
            try {
                this.img = this.fo.getFileSystem().getStatus().annotateIcon(this.img, type, Collections.singleton(this.fo));
            }
            catch (FileStateInvalidException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return this.img;
    }

    final Action[] getActions() {
        FileObject actions = FileUtil.getConfigFile((String)("Loaders/" + this.mimeType + "/Actions"));
        if (actions != null) {
            DataFolder folder = DataFolder.findFolder(actions);
            try {
                return (Action[])new DataLdrActions(folder, null).instanceCreate();
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            catch (ClassNotFoundException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return DataLoaderPool.getDefaultFileLoader().getSwingActions();
    }
}

