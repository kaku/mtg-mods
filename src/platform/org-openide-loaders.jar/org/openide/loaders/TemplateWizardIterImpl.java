/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.Mutex
 */
package org.openide.loaders;

import java.io.IOException;
import java.util.EventListener;
import java.util.Set;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.WizardDescriptor;
import org.openide.loaders.DataObject;
import org.openide.loaders.TemplateWizard;
import org.openide.util.Mutex;

class TemplateWizardIterImpl
implements WizardDescriptor.Iterator,
ChangeListener,
Runnable {
    private TemplateWizard.Iterator iterator;
    private ChangeListener iteratorListener;
    private boolean showingPanel = false;
    private boolean newIteratorInstalled = false;
    private EventListenerList listenerList = null;
    private TemplateWizard wizardInstance;
    private boolean iteratorInitialized = false;

    TemplateWizardIterImpl() {
    }

    private WizardDescriptor.Panel<WizardDescriptor> firstPanel() {
        return this.wizardInstance.templateChooser();
    }

    public void first() {
        this.showingPanel = true;
        this.fireStateChanged();
    }

    public void setIterator(TemplateWizard.Iterator it, boolean notify) {
        if (this.iterator != null && this.iteratorInitialized) {
            this.iterator.removeChangeListener(this.iteratorListener);
            this.iterator.uninitialize(this.wizardInstance);
        }
        it.initialize(this.wizardInstance);
        this.iteratorListener = new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                TemplateWizardIterImpl.this.stateChanged(e);
            }
        };
        it.addChangeListener(this.iteratorListener);
        this.iteratorInitialized = true;
        this.iterator = it;
        if (notify) {
            if (this.showingPanel) {
                this.newIteratorInstalled = true;
            }
            this.showingPanel = false;
            this.fireStateChanged();
        }
    }

    public TemplateWizard.Iterator getIterator() {
        if (this.iterator == null) {
            this.setIterator(this.wizardInstance.defaultIterator(), false);
        }
        if (!this.iteratorInitialized && this.iterator != null) {
            this.iterator.initialize(this.wizardInstance);
            this.iteratorInitialized = true;
        }
        return this.iterator;
    }

    public WizardDescriptor.Panel<WizardDescriptor> current() {
        return this.showingPanel ? this.firstPanel() : this.getIterator().current();
    }

    public String name() {
        return this.showingPanel ? "" : this.getIterator().name();
    }

    public boolean hasNext() {
        return this.showingPanel || this.getIterator().hasNext();
    }

    public boolean hasPrevious() {
        return !this.showingPanel;
    }

    public void nextPanel() {
        if (this.showingPanel || this.newIteratorInstalled) {
            this.showingPanel = false;
            this.newIteratorInstalled = false;
        } else {
            this.getIterator().nextPanel();
        }
    }

    public void previousPanel() {
        if (this.getIterator().hasPrevious()) {
            this.getIterator().previousPanel();
        } else {
            this.showingPanel = true;
            this.newIteratorInstalled = false;
        }
    }

    @Override
    public void stateChanged(ChangeEvent p1) {
        this.fireStateChanged();
    }

    public synchronized void addChangeListener(ChangeListener listener) {
        if (this.listenerList == null) {
            this.listenerList = new EventListenerList();
        }
        this.listenerList.add(ChangeListener.class, listener);
    }

    public synchronized void removeChangeListener(ChangeListener listener) {
        if (this.listenerList != null) {
            this.listenerList.remove(ChangeListener.class, listener);
        }
    }

    public void initialize(WizardDescriptor wiz) {
        if (!(wiz instanceof TemplateWizard)) {
            throw new IllegalArgumentException("WizardDescriptor must be instance of TemplateWizard, but is " + (Object)wiz);
        }
        this.wizardInstance = (TemplateWizard)wiz;
        if (this.wizardInstance.getTemplate() == null) {
            this.showingPanel = true;
        } else {
            this.newIteratorInstalled = false;
            this.showingPanel = false;
        }
        TemplateWizard.Iterator it = this.iterator;
        if (it != null && !this.iteratorInitialized) {
            it.initialize(this.wizardInstance);
            this.iteratorInitialized = true;
        }
    }

    public void uninitialize() {
        if (this.iterator != null && this.wizardInstance != null) {
            this.iterator.uninitialize(this.wizardInstance);
            this.iteratorInitialized = false;
        }
        this.showingPanel = true;
    }

    public Set<DataObject> instantiate() throws IOException {
        assert (this.wizardInstance != null);
        return this.wizardInstance.instantiateNewObjects(null);
    }

    public Set<DataObject> instantiate(ProgressHandle handle) throws IOException {
        assert (this.wizardInstance != null);
        return this.wizardInstance.instantiateNewObjects(handle);
    }

    private void fireStateChanged() {
        Mutex.EVENT.writeAccess((Runnable)this);
    }

    @Override
    public void run() {
        if (this.listenerList == null) {
            return;
        }
        ChangeEvent e = null;
        Object[] listeners = this.listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] != ChangeListener.class) continue;
            if (e == null) {
                e = new ChangeEvent(this);
            }
            ((ChangeListener)listeners[i + 1]).stateChanged(e);
        }
    }

}

