/*
 * Decompiled with CFR 0_118.
 */
package org.openide.loaders;

import org.openide.loaders.DataFolder;

public interface FolderRenameHandler {
    public void handleRename(DataFolder var1, String var2) throws IllegalArgumentException;
}

