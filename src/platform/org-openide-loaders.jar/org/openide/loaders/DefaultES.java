/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.CloseCookie
 *  org.openide.cookies.EditCookie
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.EditorCookie$Observable
 *  org.openide.cookies.OpenCookie
 *  org.openide.cookies.PrintCookie
 *  org.openide.cookies.SaveCookie
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.CookieSet
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.CloneableEditorSupport$Env
 *  org.openide.text.CloneableEditorSupport$Pane
 *  org.openide.util.Lookup
 *  org.openide.windows.CloneableOpenSupport
 */
package org.openide.loaders;

import java.io.IOException;
import org.netbeans.modules.openide.loaders.Unmodify;
import org.openide.cookies.CloseCookie;
import org.openide.cookies.EditCookie;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.OpenCookie;
import org.openide.cookies.PrintCookie;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DefaultDataObject;
import org.openide.loaders.MultiDOEditor;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.SaveAsCapable;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.DataEditorSupport;
import org.openide.util.Lookup;
import org.openide.windows.CloneableOpenSupport;

final class DefaultES
extends DataEditorSupport
implements OpenCookie,
EditCookie,
EditorCookie.Observable,
PrintCookie,
CloseCookie,
SaveAsCapable {
    private final SaveCookie saveCookie;
    private CookieSet set;

    DefaultES(MultiDataObject obj, MultiDataObject.Entry entry, CookieSet set) {
        super(obj, null, new Environment(obj, entry));
        this.saveCookie = new SaveCookieImpl();
        this.set = set;
        this.setMIMEType("text/plain");
    }

    protected boolean notifyModified() {
        if (!super.notifyModified()) {
            return false;
        }
        this.addSaveCookie();
        return true;
    }

    protected void notifyUnmodified() {
        super.notifyUnmodified();
        this.removeSaveCookie(true);
    }

    protected boolean asynchronousOpen() {
        return true;
    }

    protected CloneableEditorSupport.Pane createPane() {
        if (MultiDOEditor.isMultiViewAvailable()) {
            MultiDataObject mdo = (MultiDataObject)this.getDataObject();
            return MultiDOEditor.createMultiViewPane("text/plain", mdo);
        }
        return super.createPane();
    }

    private void addSaveCookie() {
        DataObject obj = this.getDataObject();
        if (obj.getCookie(SaveCookie.class) == null) {
            this.set.add((Node.Cookie)this.saveCookie);
            obj.setModified(true);
        }
    }

    private void removeSaveCookie(boolean setModified) {
        DataObject obj = this.getDataObject();
        SaveCookie cookie = obj.getCookie(SaveCookie.class);
        if (cookie != null && cookie.equals((Object)this.saveCookie)) {
            this.set.remove((Node.Cookie)this.saveCookie);
            if (setModified) {
                obj.setModified(false);
            }
        }
    }

    private class SaveCookieImpl
    implements SaveCookie,
    Unmodify {
        public void save() throws IOException {
            DefaultES.this.saveDocument();
        }

        @Override
        public void unmodify() {
            DefaultES.this.removeSaveCookie(false);
        }
    }

    private static class Environment
    extends DataEditorSupport.Env {
        private static final long serialVersionUID = 5451434321155443431L;
        private MultiDataObject.Entry entry;

        public Environment(DataObject obj, MultiDataObject.Entry entry) {
            super(obj);
            this.entry = entry;
        }

        @Override
        protected FileObject getFile() {
            return this.entry.getFile();
        }

        @Override
        protected FileLock takeLock() throws IOException {
            return this.entry.takeLock();
        }

        @Override
        public CloneableOpenSupport findCloneableOpenSupport() {
            DataObject obj = this.getDataObject();
            DefaultES ret = obj instanceof DefaultDataObject ? ((DefaultDataObject)obj).getCookie(DefaultES.class, true) : this.getDataObject().getCookie(DefaultES.class);
            super.findCloneableOpenSupport();
            return ret;
        }
    }

}

