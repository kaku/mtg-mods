/*
 * Decompiled with CFR 0_118.
 */
package org.openide.loaders;

import java.util.List;
import org.openide.loaders.DataObject;

interface FolderListListener {
    public void process(DataObject var1, List<DataObject> var2);

    public void finished(List<DataObject> var1);
}

