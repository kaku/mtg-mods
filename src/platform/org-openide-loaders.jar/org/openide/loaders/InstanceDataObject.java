/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ServiceType
 *  org.openide.ServiceType$Registry
 *  org.openide.actions.DeleteAction
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.InstanceCookie$Of
 *  org.openide.cookies.SaveCookie
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.ModuleInfo
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.CookieSet
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.FilterNode$Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.nodes.Node$Handle
 *  org.openide.util.Enumerations
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.util.WeakSet
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 */
package org.openide.loaders;

import java.beans.PropertyVetoException;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.ServiceType;
import org.openide.actions.DeleteAction;
import org.openide.cookies.InstanceCookie;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataLoaderPool;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.loaders.DataObjectPool;
import org.openide.loaders.DefaultES;
import org.openide.loaders.Environment;
import org.openide.loaders.InstanceNode;
import org.openide.loaders.InstanceSupport;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.modules.ModuleInfo;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.CookieSet;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.Enumerations;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.util.WeakSet;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

public class InstanceDataObject
extends MultiDataObject
implements InstanceCookie.Of {
    static final long serialVersionUID = -6134784731744777123L;
    private static final String EA_INSTANCE_CLASS = "instanceClass";
    private static final String EA_INSTANCE_CREATE = "instanceCreate";
    private static final String EA_INSTANCE_OF = "instanceOf";
    static final String EA_NAME = "name";
    private static final int SAVE_DELAY = 2000;
    private static final char OPEN = '[';
    private static final char CLOSE = ']';
    public static final String INSTANCE = "instance";
    static final String SER_EXT = "ser";
    static final String XML_EXT = "settings";
    private static final String ICON_NAME = "icon";
    private Ser ser;
    private boolean savingCanceled = false;
    private String nameCache;
    private static final RequestProcessor PROCESSOR = new RequestProcessor("Instance processor");
    private static final Logger err = Logger.getLogger("org.openide.loaders.InstanceDataObject");
    private static final Object IDO_LOCK = new Object();
    private UpdatableNode un;
    private Lookup lkp;
    private static Object INIT_LOOKUP = new Object();
    private Lookup.Result cookieResult = null;
    private Lookup.Result nodeResult = null;
    private Lookup cookiesLkp = null;
    private LookupListener cookiesLsnr = null;
    private LookupListener nodeLsnr = null;
    private static final Set<FileObject> warnedAboutBrackets = new WeakSet();
    private static final int MAX_FILENAME_LENGTH = 50;
    private FileLock fileLock;
    private static final String EA_PROVIDER_PATH = "settings.providerPath";
    private static final String EA_SUBCLASSES = "settings.subclasses";
    private static final List<String> createdIDOs = Collections.synchronizedList(new ArrayList(1));

    public InstanceDataObject(FileObject pf, MultiFileLoader loader) throws DataObjectExistsException {
        super(pf, loader);
        if (pf.hasExt("ser")) {
            this.ser = new Ser(this);
            this.getCookieSet().add((Node.Cookie)this.ser);
        } else if (!pf.hasExt("settings")) {
            this.ser = new Ser(this);
        }
        try {
            if (!pf.getFileSystem().isDefault()) {
                this.getCookieSet().add((Node.Cookie)new DefaultES(this, this.getPrimaryEntry(), this.getCookieSet()));
            }
        }
        catch (FileStateInvalidException ex) {
            // empty catch block
        }
    }

    private Object getLock() {
        return IDO_LOCK;
    }

    private static FileObject findFO(DataFolder folder, String name, String className) {
        FileObject fo = folder.getPrimaryFile();
        String classNameEnc = className.replace('.', '-');
        Enumeration en = fo.getChildren(false);
        while (en.hasMoreElements()) {
            FileObject newFile = (FileObject)en.nextElement();
            if (!newFile.hasExt("instance")) continue;
            if (name != null) {
                if (!name.equals(InstanceDataObject.getName(newFile))) {
                    continue;
                }
            } else if (!classNameEnc.equals(InstanceDataObject.getName(newFile))) continue;
            if (!className.equals(Ser.getClassName(newFile))) continue;
            return newFile;
        }
        return null;
    }

    private static String getName(FileObject fo) {
        String superName = (String)fo.getAttribute("name");
        if (superName != null) {
            return superName;
        }
        superName = fo.getName();
        int bracket = superName.indexOf(91);
        if (bracket == -1) {
            return InstanceDataObject.unescape(superName);
        }
        InstanceDataObject.warnAboutBrackets(fo);
        return InstanceDataObject.unescape(superName.substring(0, bracket));
    }

    public static InstanceDataObject find(DataFolder folder, String name, String className) {
        FileObject newFile = InstanceDataObject.findFO(folder, name, className);
        if (newFile != null) {
            try {
                return (InstanceDataObject)DataObject.find(newFile);
            }
            catch (DataObjectNotFoundException e) {
                // empty catch block
            }
        }
        return null;
    }

    public static InstanceDataObject find(DataFolder folder, String name, Class<?> clazz) {
        return InstanceDataObject.find(folder, name, clazz.getName());
    }

    public static InstanceDataObject create(DataFolder folder, final String name, final String className) throws IOException {
        final FileObject fo = folder.getPrimaryFile();
        if (name != null && name.length() == 0) {
            throw new IOException("name cannot be empty");
        }
        FileObject newFile = InstanceDataObject.findFO(folder, name, className);
        if (newFile == null) {
            final FileObject[] fos = new FileObject[1];
            DataObjectPool.getPOOL().runAtomicAction(fo, new FileSystem.AtomicAction(){

                public void run() throws IOException {
                    String fileName = name == null ? FileUtil.findFreeFileName((FileObject)fo, (String)className.replace('.', '-'), (String)"instance") : InstanceDataObject.escape(name);
                    fos[0] = fo.createData(fileName, "instance");
                    fos[0].setAttribute("instanceClass", (Object)className);
                }
            });
            newFile = fos[0];
        }
        return (InstanceDataObject)DataObject.find(newFile);
    }

    public static InstanceDataObject create(DataFolder folder, String name, Class<?> clazz) throws IOException {
        return InstanceDataObject.create(folder, name, clazz.getName());
    }

    public static InstanceDataObject create(DataFolder folder, String name, Object instance, ModuleInfo info) throws IOException {
        return InstanceDataObject.create(folder, name, instance, info, false);
    }

    public static InstanceDataObject create(DataFolder folder, String name, Object instance, ModuleInfo info, boolean create) throws IOException {
        if (name != null && name.length() == 0) {
            throw new IOException("name cannot be empty");
        }
        return Creator.createInstanceDataObject(folder, name, instance, info, create);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static InstanceDataObject storeSettings(DataFolder df, String name, Object obj, ModuleInfo mi) throws IOException {
        InstanceDataObject ido;
        FileObject fo = df.getPrimaryFile();
        FileObject newFile = fo.getFileObject(name, "settings");
        String fullname = fo.getPath() + '/' + name + '.' + "settings";
        boolean attachWithSave = false;
        try {
            if (newFile == null) {
                System.setProperty("InstanceDataObject.current.file", fo.getPath() + "/" + name + "." + "settings");
                ByteArrayOutputStream buf = InstanceDataObject.storeThroughConvertor(obj, new FileObjectContext(fo, name));
                System.setProperty("InstanceDataObject.current.file", "");
                createdIDOs.add(fullname);
                newFile = fo.createData(name, "settings");
                FileLock flock = null;
                try {
                    flock = newFile.lock();
                    OutputStream os = newFile.getOutputStream(flock);
                    os.write(buf.toByteArray());
                    os.close();
                }
                finally {
                    if (flock != null) {
                        flock.releaseLock();
                    }
                }
            }
            attachWithSave = true;
            DataObject created = null;
            for (int i = 0; i < 2 && !((created = DataObject.find(newFile)) instanceof InstanceDataObject); ++i) {
                FileObject pf = created.getPrimaryFile();
                if (!pf.getFileSystem().isDefault()) {
                    DataLoaderPool.setPreferredLoader(pf, DataLoaderPool.getInstanceLoader());
                }
                try {
                    created.setValid(false);
                    continue;
                }
                catch (PropertyVetoException ex) {
                    LOG.log(Level.INFO, "Cannot invalidate " + created, ex);
                }
            }
            if (!(created instanceof InstanceDataObject)) {
                throw new IOException("Cannot create InstanceDataObject for " + created);
            }
            ido = (InstanceDataObject)created;
            ido.attachToConvertor(obj, attachWithSave);
        }
        finally {
            createdIDOs.remove(fullname);
        }
        return ido;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static boolean remove(DataFolder folder, String name, String className) {
        FileLock lock = null;
        try {
            FileObject fileToRemove = InstanceDataObject.findFO(folder, name, className);
            if (fileToRemove == null) {
                boolean bl = false;
                return bl;
            }
            lock = fileToRemove.lock();
            fileToRemove.delete(lock);
        }
        catch (IOException exc) {
            boolean bl = false;
            return bl;
        }
        finally {
            if (lock != null) {
                lock.releaseLock();
            }
        }
        return true;
    }

    public static boolean remove(DataFolder folder, String name, Class<?> clazz) {
        return InstanceDataObject.remove(folder, name, clazz.getName());
    }

    @Override
    public HelpCtx getHelpCtx() {
        HelpCtx test = InstanceSupport.findHelp((InstanceCookie)this);
        if (test != null) {
            return test;
        }
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    protected Node createNodeDelegate() {
        if (this.getPrimaryFile().hasExt("settings")) {
            this.un = new UpdatableNode(this.createNodeDelegateImpl());
            return this.un;
        }
        return this.createNodeDelegateImpl();
    }

    private final String warningMessage() {
        Object by = this.getPrimaryFile().getAttribute("layers");
        if (by instanceof Object[]) {
            by = Arrays.toString((Object[])by);
        }
        return "Cannot get class for " + (Object)this.getPrimaryFile() + " defined by " + by;
    }

    private Node createNodeDelegateImpl() {
        Node n;
        try {
            if (!this.getPrimaryFile().getFileSystem().isDefault()) {
                return new DataNode(this, Children.LEAF);
            }
        }
        catch (FileStateInvalidException ex) {
            err.log(Level.WARNING, null, (Throwable)ex);
            return new DataNode(this, Children.LEAF);
        }
        if (this.getPrimaryFile().hasExt("settings")) {
            if (null == this.getCookieFromEP(InstanceCookie.class)) {
                return new CookieAdjustingFilter((Node)new UnrecognizedSettingNode());
            }
            n = this.getCookieFromEP(Node.class);
            if (n != null) {
                return new CookieAdjustingFilter(n);
            }
        }
        try {
            Node.Handle h;
            if (this.instanceOf(Node.class)) {
                n = (Node)this.instanceCreate();
                if (n != null) {
                    return new CookieAdjustingFilter(n);
                }
            } else if (this.instanceOf(Node.Handle.class) && (h = (Node.Handle)this.instanceCreate()) != null) {
                return new CookieAdjustingFilter(h.getNode());
            }
        }
        catch (IOException ex) {
            err.log(Level.WARNING, null, ex);
        }
        catch (ClassNotFoundException ex) {
            err.log(Level.WARNING, null, ex);
        }
        return new InstanceNode(this);
    }

    private <T> T getCookieFromEP(Class<T> clazz) {
        return (T)this.getCookiesLookup().lookup(clazz);
    }

    @Override
    void notifyFileChanged(FileEvent fe) {
        super.notifyFileChanged(fe);
        if (this.getPrimaryFile().hasExt("settings")) {
            if (!Creator.isFiredFromMe(fe) && !Creator2.isFiredFromMe(fe)) {
                this.getCookiesLookup(true);
            }
        } else if (this.ser instanceof Ser) {
            this.ser = new Ser(this);
            this.getCookieSet().assign(InstanceCookie.class, (Object[])new InstanceCookie[]{this.ser});
        }
    }

    @Override
    public <T extends Node.Cookie> T getCookie(Class<T> clazz) {
        Node.Cookie supe = null;
        if (this.getPrimaryFile().hasExt("settings")) {
            String filename = this.getPrimaryFile().getPath();
            if (createdIDOs.contains(filename)) {
                return null;
            }
            T res = this.getCookieFromEP(clazz);
            Node.Cookie cookie = supe = res instanceof Node.Cookie ? (Node.Cookie)clazz.cast(res) : null;
            if (InstanceCookie.class.isAssignableFrom(clazz)) {
                return (T)supe;
            }
        }
        if (supe == null) {
            supe = (Node.Cookie)super.getCookie(clazz);
        }
        return (T)supe;
    }

    @Override
    void checkCookieSet(Class<?> clazz) {
        if (this.getPrimaryFile().hasExt("settings")) {
            String filename = this.getPrimaryFile().getPath();
            if (createdIDOs.contains(filename)) {
                return;
            }
            Object res = this.getCookieFromEP(clazz);
            if (res != null) {
                // empty if block
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Lookup getLookup() {
        Object object = INIT_LOOKUP;
        synchronized (object) {
            if (this.lkp != null) {
                return this.lkp;
            }
            this.lkp = this.getPrimaryFile().hasExt("settings") ? new ProxyLookup(new Lookup[]{this.getCookieSet().getLookup(), this.getCookiesLookup()}) : this.getCookieSet().getLookup();
            return this.lkp;
        }
    }

    private Lookup getCookiesLookup() {
        return this.getCookiesLookup(false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Lookup getCookiesLookup(boolean reinit) {
        Object object = this.getLock();
        synchronized (object) {
            if (!reinit && this.cookiesLkp != null) {
                return this.cookiesLkp;
            }
        }
        Lookup envLkp = Environment.findForOne(this);
        Object object2 = this.getLock();
        synchronized (object2) {
            if (reinit || this.cookiesLkp == null || envLkp == null || !envLkp.getClass().equals(this.cookiesLkp.getClass())) {
                this.cookiesLkp = envLkp == null ? Lookup.EMPTY : envLkp;
                this.initCookieResult();
                this.initNodeResult();
            }
        }
        if (this.nodeResult != null) {
            this.nodeResult.allItems();
        }
        if (this.cookieResult != null) {
            this.cookieResult.allItems();
        }
        return this.cookiesLkp;
    }

    private void initNodeResult() {
        if (this.nodeResult != null && this.nodeLsnr != null) {
            this.nodeResult.removeLookupListener(this.nodeLsnr);
        }
        if (this.cookiesLkp != null && !this.cookiesLkp.equals((Object)Lookup.EMPTY)) {
            this.nodeResult = this.cookiesLkp.lookupResult(InstanceCookie.class);
            this.nodeLsnr = new LookupListener(){

                public void resultChanged(LookupEvent lookupEvent) {
                    if (InstanceDataObject.this.un != null) {
                        InstanceDataObject.this.un.update();
                    }
                }
            };
            this.nodeResult.addLookupListener(this.nodeLsnr);
        }
    }

    private void initCookieResult() {
        if (this.cookieResult != null && this.cookiesLsnr != null) {
            this.cookieResult.removeLookupListener(this.cookiesLsnr);
        }
        if (this.cookiesLkp != null && !this.cookiesLkp.equals((Object)Lookup.EMPTY)) {
            this.cookieResult = this.cookiesLkp.lookupResult(Node.Cookie.class);
            this.cookiesLsnr = new LookupListener(){

                public void resultChanged(LookupEvent lookupEvent) {
                    InstanceDataObject.this.firePropertyChange("cookie", null, null);
                }
            };
            this.cookieResult.addLookupListener(this.cookiesLsnr);
        }
    }

    private InstanceCookie.Of delegateIC() {
        Ser ic = null;
        ic = this.getPrimaryFile().hasExt("settings") ? this.getCookieFromEP(InstanceCookie.Of.class) : this.ser;
        return ic;
    }

    public String instanceName() {
        InstanceCookie.Of delegateIC = this.delegateIC();
        if (delegateIC == null) {
            return this.getName();
        }
        return delegateIC.instanceName();
    }

    public Class<?> instanceClass() throws IOException, ClassNotFoundException {
        InstanceCookie.Of delegateIC = this.delegateIC();
        if (delegateIC == null) {
            return this.getClass();
        }
        try {
            return delegateIC.instanceClass();
        }
        catch (ClassNotFoundException ex) {
            Exceptions.attachMessage((Throwable)ex, (String)this.warningMessage());
            throw ex;
        }
        catch (IOException ex) {
            Exceptions.attachMessage((Throwable)ex, (String)this.warningMessage());
            throw ex;
        }
    }

    public boolean instanceOf(Class<?> type) {
        InstanceCookie.Of delegateIC = this.delegateIC();
        if (delegateIC == null) {
            return type.isAssignableFrom(this.getClass());
        }
        return delegateIC.instanceOf(type);
    }

    public Object instanceCreate() throws IOException, ClassNotFoundException {
        InstanceCookie.Of delegateIC = this.delegateIC();
        if (delegateIC == null) {
            return this;
        }
        return delegateIC.instanceCreate();
    }

    final boolean creatorOf(Object inst) {
        InstanceCookie.Of delegateIC = this.delegateIC();
        if (delegateIC instanceof Ser) {
            return ((Ser)delegateIC).creatorOf(inst);
        }
        return false;
    }

    @Override
    public String getName() {
        if (this.nameCache != null) {
            return this.nameCache;
        }
        String superName = (String)this.getPrimaryFile().getAttribute("name");
        if (superName == null) {
            superName = super.getName();
            int bracket = superName.indexOf(91);
            if (bracket == -1) {
                superName = InstanceDataObject.unescape(superName);
            } else {
                InstanceDataObject.warnAboutBrackets(this.getPrimaryFile());
                superName = InstanceDataObject.unescape(superName.substring(0, bracket));
            }
        }
        this.nameCache = superName;
        return superName;
    }

    private static void warnAboutBrackets(FileObject fo) {
        if (warnedAboutBrackets.add(fo)) {
            err.warning("Use of [] in " + (Object)fo + " is deprecated.");
            err.warning("(Please use the string-valued file attribute instanceClass instead.)");
        }
    }

    @Override
    protected FileObject handleRename(String name) throws IOException {
        FileObject fo = this.getPrimaryFile();
        fo.setAttribute("name", (Object)name);
        return fo;
    }

    static String escape(String text) {
        boolean spacenasty = text.startsWith(" ") || text.endsWith(" ") || text.indexOf("  ") != -1;
        int len = text.length();
        StringBuffer escaped = new StringBuffer(len);
        for (int i = 0; i < len; ++i) {
            char c = text.charAt(i);
            if (c == '/' || c == ':' || c == '\\' || c == '[' || c == ']' || c == '<' || c == '>' || c == '?' || c == '*' || c == '|' || c == ' ' && spacenasty || c == '.' || c == '\"' || c < ' ' || c > '~' || c == '#') {
                escaped.append('#');
                String hex = Integer.toString(c, 16).toUpperCase(Locale.ENGLISH);
                if (hex.length() < 4) {
                    escaped.append('0');
                }
                if (hex.length() < 3) {
                    escaped.append('0');
                }
                if (hex.length() < 2) {
                    escaped.append('0');
                }
                escaped.append(hex);
                continue;
            }
            escaped.append(c);
        }
        return escaped.toString();
    }

    static String unescape(String text) {
        int len = text.length();
        StringBuffer unesc = new StringBuffer(len);
        for (int i = 0; i < len; ++i) {
            char c = text.charAt(i);
            if (c == '#') {
                if (i + 4 >= len) {
                    err.warning("trailing garbage in instance name: " + text);
                    break;
                }
                try {
                    char[] hex = new char[4];
                    text.getChars(i + 1, i + 5, hex, 0);
                    unesc.append((char)Integer.parseInt(new String(hex), 16));
                }
                catch (NumberFormatException nfe) {
                    err.log(Level.WARNING, null, nfe);
                }
                i += 4;
                continue;
            }
            unesc.append(c);
        }
        return unesc.toString();
    }

    static String escapeAndCut(String name) {
        int maxLen = 50;
        String ename = InstanceDataObject.escape(name);
        if (ename.length() <= maxLen) {
            return ename;
        }
        String hash = Integer.toHexString(ename.hashCode()).toUpperCase(Locale.ENGLISH);
        String start = ename.substring(0, maxLen = maxLen > hash.length() ? (maxLen - hash.length()) / 2 : 1);
        if (start.lastIndexOf(35) >= start.length() - 5) {
            start = start.substring(0, start.lastIndexOf(35));
        }
        String end = ename.substring(ename.length() - maxLen);
        return start + hash + end;
    }

    final void scheduleSave() {
        if (this.isSavingCanceled() || !this.getPrimaryFile().hasExt("ser")) {
            return;
        }
        this.doFileLock();
        this.ser.getSaveTask().schedule(2000);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private FileLock doFileLock() {
        Object object = this.getLock();
        synchronized (object) {
            if (this.fileLock != null) {
                return this.fileLock;
            }
            try {
                this.fileLock = this.getPrimaryFile().lock();
            }
            catch (IOException ex) {
                err.log(Level.WARNING, this.getPrimaryFile().toString());
                err.log(Level.WARNING, null, ex);
            }
            return this.fileLock;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void relaseFileLock() {
        Object object = this.getLock();
        synchronized (object) {
            if (this.fileLock == null) {
                return;
            }
            this.fileLock.releaseLock();
            this.fileLock = null;
        }
    }

    @Override
    protected DataObject handleCreateFromTemplate(DataFolder df, String name) throws IOException {
        try {
            if (this.getPrimaryFile().hasExt("settings")) {
                InstanceCookie ic = this.getCookie(InstanceCookie.class);
                Object obj = ic.instanceCreate();
                InstanceDataObject d = this.createSettingsFile(df, name, obj);
                this.attachToConvertor(null);
                return d;
            }
            if (!this.getPrimaryFile().hasExt("instance") && Serializable.class.isAssignableFrom(this.instanceClass())) {
                InstanceCookie ic = this.getCookie(InstanceCookie.class);
                Object obj = ic.instanceCreate();
                return DataObject.find(this.createSerFile(df, name, obj));
            }
        }
        catch (ClassNotFoundException ex) {
            err.log(Level.WARNING, null, ex);
        }
        return super.handleCreateFromTemplate(df, name);
    }

    @Override
    protected DataObject handleCopy(DataFolder df) throws IOException {
        if (this.getPrimaryFile().getFileSystem().isDefault()) {
            try {
                InstanceCookie ic;
                if (this.getPrimaryFile().hasExt("settings")) {
                    InstanceCookie ic2 = this.getCookie(InstanceCookie.class);
                    if (ic2 != null) {
                        Object obj = ic2.instanceCreate();
                        InstanceDataObject ido = this.createSettingsFile(df, this.getNodeDelegate().getDisplayName(), obj);
                        ido.attachToConvertor(null);
                        return ido;
                    }
                } else if (!this.getPrimaryFile().hasExt("instance") && Serializable.class.isAssignableFrom(this.instanceClass()) && (ic = this.getCookie(InstanceCookie.class)) != null) {
                    Object obj = ic.instanceCreate();
                    return DataObject.find(this.createSerFile(df, this.getNodeDelegate().getDisplayName(), obj));
                }
            }
            catch (ClassNotFoundException ex) {
                err.log(Level.WARNING, null, ex);
            }
        }
        return super.handleCopy(df);
    }

    private boolean isSavingCanceled() {
        return this.savingCanceled;
    }

    @Override
    protected void dispose() {
        if (this.getPrimaryFile().hasExt("ser")) {
            RequestProcessor.Task task;
            this.savingCanceled = true;
            if (this.ser != null && ((task = this.ser.getSaveTask()).getDelay() > 0 || this.ser.isSaving() && !task.isFinished())) {
                task.waitFinished();
            }
            this.relaseFileLock();
        } else if (this.getPrimaryFile().hasExt("settings")) {
            SaveCookie s = this.getCookie(SaveCookie.class);
            try {
                if (s != null) {
                    s.save();
                }
            }
            catch (IOException ex) {
                // empty catch block
            }
        }
        super.dispose();
    }

    @Override
    protected void handleDelete() throws IOException {
        this.savingCanceled = true;
        if (this.getPrimaryFile().hasExt("settings")) {
            this.handleDeleteSettings();
            return;
        }
        if (this.ser != null) {
            RequestProcessor.Task task = this.ser.getSaveTask();
            task.cancel();
            if (this.ser.isSaving() && !task.isFinished()) {
                task.waitFinished();
            }
        }
        this.relaseFileLock();
        super.handleDelete();
    }

    private void handleDeleteSettings() throws IOException {
        SaveCookie s = this.getCookie(SaveCookie.class);
        try {
            if (s != null) {
                s.save();
            }
        }
        catch (IOException ex) {
            // empty catch block
        }
        super.handleDelete();
    }

    private InstanceDataObject createSettingsFile(DataFolder df, String name, Object obj) throws IOException {
        String filename;
        boolean isServiceType = false;
        if (obj instanceof ServiceType) {
            isServiceType = true;
            ServiceType sr = (ServiceType)obj;
            String stName = name = name == null ? sr.getName() : name;
            ServiceType.Registry r = (ServiceType.Registry)Lookup.getDefault().lookup(ServiceType.Registry.class);
            int i = 1;
            while (r.find(stName) != null) {
                stName = new StringBuffer(name.length() + 2).append(name).append('_').append(i).toString();
                ++i;
            }
            if (!stName.equals(sr.getName())) {
                sr = sr.createClone();
                obj = sr;
                sr.setName(stName);
            }
            filename = InstanceDataObject.escapeAndCut(stName);
        } else {
            filename = name == null ? this.getPrimaryFile().getName() : InstanceDataObject.escapeAndCut(name);
        }
        filename = FileUtil.findFreeFileName((FileObject)df.getPrimaryFile(), (String)filename, (String)this.getPrimaryFile().getExt());
        Creator2 c2 = new Creator2(df, filename, obj);
        FileUtil.runAtomicAction((FileSystem.AtomicAction)c2);
        if (name != null && !isServiceType) {
            c2.newFile.getPrimaryFile().setAttribute("name", (Object)name);
        }
        return c2.newFile;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private FileObject createSerFile(DataFolder df, String name, Object obj) throws IOException {
        FileLock lock = null;
        OutputStream ostream = null;
        FileObject newFile = null;
        try {
            FileObject fo = df.getPrimaryFile();
            if (name == null) {
                name = FileUtil.findFreeFileName((FileObject)df.getPrimaryFile(), (String)this.getPrimaryFile().getName(), (String)this.getPrimaryFile().getExt());
            }
            if ((newFile = fo.getFileObject(name, "ser")) == null) {
                newFile = fo.createData(name, "ser");
            }
            lock = newFile.lock();
            ostream = newFile.getOutputStream(lock);
            ObjectOutputStream p = new ObjectOutputStream(ostream);
            p.writeObject(obj);
            p.flush();
        }
        finally {
            if (ostream != null) {
                ostream.close();
            }
            if (lock != null) {
                lock.releaseLock();
            }
        }
        return newFile;
    }

    final void setCustomClassLoader(ClassLoader cl) {
        if (this.ser != null) {
            this.ser.setCustomClassLoader(cl);
        }
    }

    private static ByteArrayOutputStream storeThroughConvertor(Object inst, FileObjectContext ctx) throws IOException {
        FileObject fo = InstanceDataObject.resolveConvertor(inst);
        Object convertor = fo.getAttribute("settings.convertor");
        if (convertor == null) {
            throw new IOException("missing attribute settings.convertor");
        }
        ByteArrayOutputStream b = new ByteArrayOutputStream(1024);
        OutputStreamWriter w = new OutputStreamWriter((OutputStream)b, "UTF-8");
        InstanceDataObject.convertorWriteMethod(convertor, new WriterProvider(w, ctx), inst);
        w.close();
        return b;
    }

    private static void convertorWriteMethod(Object convertor, Writer w, Object inst) throws IOException {
        Throwable e2;
        block5 : {
            Throwable e2 = null;
            try {
                Method method = convertor.getClass().getMethod("write", Writer.class, Object.class);
                method.setAccessible(true);
                method.invoke(convertor, w, inst);
            }
            catch (NoSuchMethodException ex) {
                e2 = ex;
            }
            catch (IllegalAccessException ex) {
                e2 = ex;
            }
            catch (InvocationTargetException ex) {
                e2 = ex.getTargetException();
                if (!(e2 instanceof IOException)) break block5;
                throw (IOException)e2;
            }
        }
        if (e2 != null) {
            throw (IOException)new IOException("Problem with Convertor.write method. " + e2).initCause(e2);
        }
    }

    private static FileObject resolveConvertor(Object obj) throws IOException {
        Class clazz;
        String prefix = "xml/memory";
        FileObject memContext = FileUtil.getConfigFile((String)prefix);
        if (memContext == null) {
            throw new FileNotFoundException("SFS:xml/memory while converting a " + obj.getClass().getName());
        }
        Class c = clazz = obj.getClass();
        while (c != null) {
            String className = c.getName();
            String convertorPath = new StringBuffer(200).append(prefix).append('/').append(className.replace('.', '/')).toString();
            FileObject fo = FileUtil.getConfigFile((String)convertorPath);
            if (fo != null) {
                boolean subclasses;
                String providerPath = (String)fo.getAttribute("settings.providerPath");
                if (providerPath == null) {
                    c = c.getSuperclass();
                    continue;
                }
                if (c.equals(clazz) || Object.class.equals(c)) {
                    FileObject ret = FileUtil.getConfigFile((String)providerPath);
                    if (ret == null) {
                        throw new FileNotFoundException("Invalid settings.providerPath=" + providerPath + " under SFS/xml/memory/ for " + clazz);
                    }
                    return ret;
                }
                Object inheritAttribute = fo.getAttribute("settings.subclasses");
                if (inheritAttribute instanceof Boolean && (subclasses = ((Boolean)inheritAttribute).booleanValue())) {
                    FileObject ret = FileUtil.getConfigFile((String)providerPath);
                    if (ret == null) {
                        throw new FileNotFoundException("Invalid settings.providerPath under SFS/xml/memory/ for " + clazz);
                    }
                    return ret;
                }
            }
            c = c.getSuperclass();
        }
        throw new FileNotFoundException("None convertor was found under SFS/xml/memory/ for " + clazz);
    }

    private void attachToConvertor(Object obj) throws IOException {
        this.attachToConvertor(obj, false);
    }

    private void attachToConvertor(Object obj, boolean save) throws IOException {
        Object ic = this.getCookiesLookup().lookup(InstanceCookie.class);
        if (ic == null) {
            throw new IllegalStateException("Trying to store object " + obj + " which most probably belongs to already disabled module!");
        }
        InstanceDataObject.convertorSetInstanceMethod(ic, obj, save);
    }

    private static void convertorSetInstanceMethod(Object convertor, Object inst, boolean save) throws IOException {
        ReflectiveOperationException e2;
        block5 : {
            ReflectiveOperationException e2 = null;
            try {
                Method method = convertor.getClass().getMethod("setInstance", Object.class, Boolean.TYPE);
                method.setAccessible(true);
                Object[] arrobject = new Object[2];
                arrobject[0] = inst;
                arrobject[1] = save ? Boolean.TRUE : Boolean.FALSE;
                method.invoke(convertor, arrobject);
            }
            catch (NoSuchMethodException ex) {
                e2 = ex;
            }
            catch (IllegalAccessException ex) {
                e2 = ex;
            }
            catch (InvocationTargetException ex) {
                e2 = ex;
                if (!(ex.getTargetException() instanceof IOException)) break block5;
                throw (IOException)ex.getTargetException();
            }
        }
        if (e2 != null) {
            Exceptions.attachLocalizedMessage((Throwable)e2, (String)("Problem with InstanceCookie.setInstance method: " + convertor.getClass()));
            err.log(Level.WARNING, null, e2);
        }
    }

    @Override
    void notifyAttributeChanged(FileAttributeEvent fae) {
        this.nameCache = null;
        super.notifyAttributeChanged(fae);
    }

    private static final class FileObjectContext
    extends FileObject {
        private static final String UNSUPPORTED = "The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.";
        private final FileObject fo;
        private final FileObject parent;
        private final String name;

        public FileObjectContext(FileObject parent, String name) {
            this.parent = parent;
            this.name = name;
            this.fo = parent.getFileObject(name, "settings");
        }

        public void addFileChangeListener(FileChangeListener fcl) {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }

        public FileObject createData(String name, String ext) throws IOException {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }

        public FileObject createFolder(String name) throws IOException {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }

        public void delete(FileLock lock) throws IOException {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }

        public Object getAttribute(String attrName) {
            return this.fo == null ? null : this.fo.getAttribute(attrName);
        }

        public Enumeration<String> getAttributes() {
            return this.fo == null ? Enumerations.empty() : this.fo.getAttributes();
        }

        public FileObject[] getChildren() {
            return new FileObject[0];
        }

        public String getExt() {
            return "settings";
        }

        public FileObject getFileObject(String name, String ext) {
            return null;
        }

        public FileSystem getFileSystem() throws FileStateInvalidException {
            return this.parent.getFileSystem();
        }

        public InputStream getInputStream() throws FileNotFoundException {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }

        public String getName() {
            return this.name;
        }

        public OutputStream getOutputStream(FileLock lock) throws IOException {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }

        public FileObject getParent() {
            return this.parent;
        }

        public long getSize() {
            return this.fo == null ? 0 : this.fo.getSize();
        }

        public boolean isData() {
            return true;
        }

        public boolean isFolder() {
            return false;
        }

        public boolean isReadOnly() {
            return this.parent.isReadOnly();
        }

        public boolean isRoot() {
            return false;
        }

        public boolean isValid() {
            return this.fo == null ? false : this.fo.isValid();
        }

        public Date lastModified() {
            return this.fo == null ? this.parent.lastModified() : this.fo.lastModified();
        }

        public FileLock lock() throws IOException {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }

        public void removeFileChangeListener(FileChangeListener fcl) {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }

        public void rename(FileLock lock, String name, String ext) throws IOException {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }

        public void setAttribute(String attrName, Object value) throws IOException {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }

        public void setImportant(boolean b) {
            throw new UnsupportedOperationException("The Restricted FileObject implementation allowing to get just read-only informations about name and location. It should prevent any manipulation with file or its content.");
        }
    }

    private static final class WriterProvider
    extends Writer
    implements Lookup.Provider {
        private final Writer orig;
        private final FileObjectContext ctx;
        private Lookup lookup;

        public WriterProvider(Writer w, FileObjectContext ctx) {
            this.orig = w;
            this.ctx = ctx;
        }

        @Override
        public void close() throws IOException {
            this.orig.close();
        }

        @Override
        public void flush() throws IOException {
            this.orig.flush();
        }

        @Override
        public void write(char[] cbuf, int off, int len) throws IOException {
            this.orig.write(cbuf, off, len);
        }

        public Lookup getLookup() {
            if (this.lookup == null) {
                this.lookup = Lookups.singleton((Object)((Object)this.ctx));
            }
            return this.lookup;
        }
    }

    private static class Creator
    implements FileSystem.AtomicAction {
        private ModuleInfo mi = null;
        private DataFolder folder = null;
        private Object instance = null;
        private String name = null;
        private InstanceDataObject result = null;
        private boolean create;
        private static final Creator me = new Creator();

        private Creator() {
        }

        public void run() throws IOException {
            FileObject fo = this.folder.getPrimaryFile();
            String filename = this.name;
            if (filename == null) {
                filename = this.instance.getClass().getName().replace('.', '-');
                filename = FileUtil.findFreeFileName((FileObject)fo, (String)filename, (String)"settings");
            } else {
                String escapedFileName = InstanceDataObject.escape(filename);
                FileObject newFile = fo.getFileObject(escapedFileName, "settings");
                filename = newFile == null ? InstanceDataObject.escapeAndCut(filename) : escapedFileName;
                if (this.create) {
                    filename = FileUtil.findFreeFileName((FileObject)fo, (String)filename, (String)"settings");
                }
            }
            this.result = InstanceDataObject.storeSettings(this.folder, filename, this.instance, this.mi);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public static InstanceDataObject createInstanceDataObject(DataFolder folder, String name, Object instance, ModuleInfo mi, boolean create) throws IOException {
            Creator creator = me;
            synchronized (creator) {
                Creator.me.mi = mi;
                Creator.me.folder = folder;
                Creator.me.instance = instance;
                Creator.me.name = name;
                Creator.me.create = create;
                DataObjectPool.getPOOL().runAtomicActionSimple(folder.getPrimaryFile(), me);
                Creator.me.mi = null;
                Creator.me.folder = null;
                Creator.me.instance = null;
                Creator.me.name = null;
                InstanceDataObject result = Creator.me.result;
                Creator.me.result = null;
                return result;
            }
        }

        public static boolean isFiredFromMe(FileEvent fe) {
            return fe.firedFrom((FileSystem.AtomicAction)me);
        }
    }

    private static final class Ser
    extends InstanceSupport
    implements Runnable {
        private Reference<Object> bean = new SoftReference<Object>(null);
        private long saveTime;
        private ClassLoader customClassLoader = null;
        private InstanceDataObject dobj;
        private RequestProcessor.Task task;
        private boolean saving = false;

        public Ser(InstanceDataObject dobj) {
            super(dobj.getPrimaryEntry());
            this.dobj = dobj;
        }

        private void setSaveTime(long t) {
            this.saveTime = t;
            if (err.isLoggable(Level.FINER)) {
                err.log(Level.FINER, "saveTime for {0} set: {1}", new Object[]{this.dobj.getPrimaryFile().getPath(), this.saveTime});
            }
        }

        @Override
        public String instanceName() {
            Object o;
            FileObject fo = this.entry().getFile();
            if (fo.lastModified().getTime() <= this.saveTime && (o = this.bean.get()) != null) {
                return o.getClass().getName();
            }
            if (!fo.hasExt("instance")) {
                return super.instanceName();
            }
            return Ser.getClassName(fo);
        }

        private static String getClassName(FileObject fo) {
            int last;
            Object attr = fo.getAttribute("instanceClass");
            if (attr instanceof Class) {
                return ((Class)attr).getName();
            }
            if (attr instanceof String) {
                return Utilities.translate((String)((String)attr));
            }
            if (attr != null) {
                err.warning("instanceClass was a " + attr.getClass().getName());
            }
            if ((attr = fo.getAttribute("class:instanceCreate")) instanceof Class) {
                return ((Class)attr).getName();
            }
            attr = fo.getAttribute("instanceCreate");
            if (attr != null) {
                err.warning("Instance file " + (Object)fo + " uses " + "instanceCreate" + " attribute, but doesn't define " + "instanceOf" + " attribute. " + "Please add " + "instanceOf" + " attr to avoid multiple instances creation," + "see details at http://www.netbeans.org/issues/show_bug.cgi?id=131951");
                return attr.getClass().getName();
            }
            String name = fo.getName();
            int first = name.indexOf(91) + 1;
            if (first != 0) {
                InstanceDataObject.warnAboutBrackets(fo);
            }
            if ((last = name.indexOf(93)) < 0) {
                last = name.length();
            }
            if (first < last) {
                name = name.substring(first, last);
            }
            name = name.replace('-', '.');
            name = Utilities.translate((String)name);
            return name;
        }

        public Class instanceClass() throws IOException, ClassNotFoundException {
            return super.instanceClass(this.customClassLoader);
        }

        public boolean instanceOf(Class type) {
            Object o;
            FileObject fo = this.entry().getFile();
            if (fo.lastModified().getTime() <= this.saveTime && (o = this.bean.get()) != null) {
                return type.isInstance(o);
            }
            Boolean res = Ser.inListOfClasses(type, this.entry().getFile());
            if (res == null) {
                return super.instanceOf(type);
            }
            return res;
        }

        @Override
        public synchronized Object instanceCreate() throws IOException, ClassNotFoundException {
            long fileTime;
            Object o;
            FileObject fo = this.entry().getFile();
            boolean log = err.isLoggable(Level.FINER);
            if (log) {
                err.log(Level.FINER, "instanceCreate for {0}", fo.getPath());
            }
            if ((fileTime = fo.lastModified().getTime()) <= this.saveTime) {
                o = this.bean.get();
                if (log) {
                    err.log(Level.FINER, "  times are OK: {0} <= {1}", new Object[]{fileTime, this.saveTime});
                    err.log(Level.FINER, "  using cached instance {0}", o);
                }
            } else {
                o = null;
                err.log(Level.FINER, "  using freshed instance");
            }
            if (o != null) {
                return o;
            }
            long nowTime = System.currentTimeMillis();
            this.setSaveTime(fileTime < nowTime ? nowTime : fileTime);
            boolean useFallback = true;
            if (fo.hasExt("instance")) {
                o = fo.getAttribute("instanceCreate");
                if (o == null && fo.getAttribute("class:instanceCreate") instanceof Class) {
                    useFallback = false;
                }
                err.log(Level.FINER, "  instanceCreate result: {0}", o);
            }
            if (o == null && useFallback) {
                o = super.instanceCreate();
            }
            this.bean = new SoftReference<Object>(o);
            if (log) {
                err.log(Level.FINER, "  result for " + fo.getPath() + " will be: " + o);
            }
            return o;
        }

        final boolean creatorOf(Object inst) {
            Reference<Object> r = this.bean;
            return r != null && r.get() == inst;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            try {
                this.saving = true;
                this.runImpl();
            }
            finally {
                this.dobj.relaseFileLock();
                this.saving = false;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void runImpl() {
            Object bean = this.bean.get();
            if (bean == null) {
                return;
            }
            try {
                FileLock lock = this.dobj.doFileLock();
                if (lock == null) {
                    return;
                }
                ObjectOutputStream oos = new ObjectOutputStream(this.entry().getFile().getOutputStream(lock));
                try {
                    oos.writeObject(bean);
                    this.setSaveTime(this.entry().getFile().lastModified().getTime());
                }
                finally {
                    oos.close();
                }
            }
            catch (IOException ex) {
                err.log(Level.WARNING, NbBundle.getMessage(InstanceDataObject.class, (String)"EXC_CannotSaveBean", (Object)this.instanceName(), (Object)this.entry().getFile().getPath()), ex);
            }
        }

        private static Boolean inListOfClasses(Class type, FileObject fo) {
            Object obj = fo.getAttribute("instanceOf");
            if (obj instanceof String) {
                String typeName = type.getName();
                StringTokenizer tok = new StringTokenizer((String)obj, "\n\t ,;:");
                while (tok.hasMoreTokens()) {
                    String t = tok.nextToken().trim();
                    if (!typeName.equals(t)) continue;
                    return Boolean.TRUE;
                }
                return Boolean.FALSE;
            }
            if (obj != null) {
                err.warning("instanceOf was a " + obj.getClass().getName());
            }
            return null;
        }

        final void setCustomClassLoader(ClassLoader cl) {
            this.customClassLoader = cl;
        }

        public RequestProcessor.Task getSaveTask() {
            if (this.task == null) {
                this.task = PROCESSOR.create((Runnable)this);
            }
            return this.task;
        }

        public boolean isSaving() {
            return this.saving;
        }
    }

    private static class Creator2
    implements FileSystem.AtomicAction {
        private InstanceDataObject newFile;
        private static final Creator2 me = new Creator2(null, null, null);
        private final DataFolder df;
        private final String filename;
        private final Object obj;

        public Creator2(DataFolder df, String filename, Object obj) {
            this.df = df;
            this.filename = filename;
            this.obj = obj;
        }

        static boolean isFiredFromMe(FileEvent fe) {
            return fe.firedFrom((FileSystem.AtomicAction)me);
        }

        public void run() throws IOException {
            this.newFile = InstanceDataObject.storeSettings(this.df, this.filename, this.obj, null);
        }

        public boolean equals(Object obj) {
            return this.getClass().equals(obj.getClass());
        }

        public int hashCode() {
            return this.getClass().hashCode();
        }
    }

    private final class CookieAdjustingFilter
    extends FilterNode {
        public CookieAdjustingFilter(Node n) {
            super(n, null, (Lookup)new ProxyLookup(new Lookup[]{n.getLookup(), Lookups.singleton((Object)InstanceDataObject.this)}));
        }

        public Node.Handle getHandle() {
            return this.getOriginal().getHandle();
        }

        public boolean equals(Object o) {
            return this == o || this.getOriginal().equals(o) || o != null && o.equals((Object)this.getOriginal());
        }

        public int hashCode() {
            return this.getOriginal().hashCode();
        }
    }

    private final class UnrecognizedSettingNode
    extends AbstractNode {
        public UnrecognizedSettingNode() {
            super(Children.LEAF);
            this.setName(NbBundle.getMessage(InstanceDataObject.class, (String)"LBL_BrokenSettings"));
            this.setIconBaseWithExtension("org/openide/loaders/instanceBroken.gif");
            this.setShortDescription(InstanceDataObject.this.getPrimaryFile().toString());
        }

        public boolean canDestroy() {
            return true;
        }

        public boolean canCut() {
            return false;
        }

        public boolean canCopy() {
            return false;
        }

        public boolean canRename() {
            return false;
        }

        public void destroy() throws IOException {
            InstanceDataObject.this.delete();
        }

        protected SystemAction[] createActions() {
            return new SystemAction[]{SystemAction.get(DeleteAction.class)};
        }
    }

    private final class UpdatableNode
    extends FilterNode {
        public UpdatableNode(Node n) {
            super(n);
        }

        public void update() {
            FilterNode.Children.MUTEX.postWriteRequest(new Runnable(){

                @Override
                public void run() {
                    UpdatableNode.this.changeOriginal(InstanceDataObject.this.createNodeDelegateImpl(), true);
                }
            });
        }

    }

}

