/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.openide.loaders;

import java.io.IOException;
import org.openide.filesystems.FileObject;

public interface SaveAsCapable {
    public void saveAs(FileObject var1, String var2) throws IOException;
}

