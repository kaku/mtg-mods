/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$InputLine
 *  org.openide.awt.Actions
 *  org.openide.cookies.InstanceCookie
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.datatransfer.ExTransferable
 *  org.openide.util.datatransfer.PasteType
 */
package org.openide.loaders;

import java.awt.datatransfer.Transferable;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Actions;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectPool;
import org.openide.loaders.InstanceDataObject;
import org.openide.loaders.LoaderTransfer;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.RequestProcessor;
import org.openide.util.datatransfer.ExTransferable;
import org.openide.util.datatransfer.PasteType;

abstract class DataTransferSupport {
    private static final Logger err = Logger.getLogger("org.openide.loaders.DataTransferSupport");

    DataTransferSupport() {
    }

    protected abstract PasteTypeExt[] definePasteTypes(int var1);

    protected abstract int[] defineOperations();

    protected void handleCreatePasteTypes(Transferable t, List<PasteType> s) {
    }

    public final void createPasteTypes(Transferable t, List<PasteType> s) {
        int[] ops = this.defineOperations();
        for (int i = 0; i < ops.length; ++i) {
            DataObject[] objs = LoaderTransfer.getDataObjects(t, ops[i]);
            if (objs == null || objs.length == 0) continue;
            PasteTypeExt[] pts = this.definePasteTypes(ops[i]);
            for (int j = 0; j < pts.length; ++j) {
                pts[j].setDataObjects(objs);
                if (!pts[j].canPaste()) continue;
                s.add(pts[j]);
            }
        }
        this.handleCreatePasteTypes(t, s);
    }

    static class InstantiatePaste
    extends PasteType {
        private InstanceCookie cookie;
        private DataFolder target;

        public InstantiatePaste(DataFolder target, InstanceCookie cookie) {
            this.cookie = cookie;
            this.target = target;
        }

        public String getName() {
            return DataObject.getString("PT_instance");
        }

        public HelpCtx getHelpCtx() {
            return new HelpCtx(InstantiatePaste.class);
        }

        public final Transferable paste() throws IOException {
            try {
                Class clazz = this.cookie.instanceClass();
                InstanceDataObject.create(this.getTargetFolder(), null, clazz);
            }
            catch (ClassNotFoundException ex) {
                throw new IOException(ex.getMessage());
            }
            return null;
        }

        protected DataFolder getTargetFolder() throws IOException {
            return this.target;
        }
    }

    static class SerializePaste
    extends PasteType {
        private InstanceCookie cookie;
        private DataFolder target;

        public SerializePaste(DataFolder target, InstanceCookie cookie) {
            this.cookie = cookie;
            this.target = target;
        }

        public String getName() {
            return DataObject.getString("PT_serialize");
        }

        public HelpCtx getHelpCtx() {
            return new HelpCtx(SerializePaste.class);
        }

        public final Transferable paste() throws IOException {
            final DataFolder trg = this.getTargetFolder();
            String name = this.cookie.instanceName();
            int i = name.lastIndexOf(46) + 1;
            if (i != 0 && i != name.length()) {
                name = name.substring(i);
            }
            name = FileUtil.findFreeFileName((FileObject)trg.getPrimaryFile(), (String)name, (String)"ser");
            final NotifyDescriptor.InputLine nd = new NotifyDescriptor.InputLine(DataObject.getString("SerializeBean_Text"), DataObject.getString("SerializeBean_Title"));
            nd.setInputText(name);
            if (NotifyDescriptor.OK_OPTION == DialogDisplayer.getDefault().notify((NotifyDescriptor)nd)) {
                DataObjectPool.getPOOL().runAtomicAction(trg.getPrimaryFile(), new FileSystem.AtomicAction(){

                    public void run() throws IOException {
                        FileObject fo = trg.getPrimaryFile().createData(nd.getInputText(), "ser");
                        FileLock lock = fo.lock();
                        ObjectOutputStream oos = null;
                        try {
                            oos = new ObjectOutputStream(new BufferedOutputStream(fo.getOutputStream(lock)));
                            oos.writeObject(SerializePaste.this.cookie.instanceCreate());
                        }
                        catch (ClassNotFoundException e) {
                            throw new IOException(e.getMessage());
                        }
                        finally {
                            if (oos != null) {
                                oos.close();
                            }
                            lock.releaseLock();
                        }
                    }
                });
            }
            return null;
        }

        protected DataFolder getTargetFolder() throws IOException {
            return this.target;
        }

    }

    static abstract class PasteTypeExt
    extends PasteType {
        private static final RequestProcessor RP = new RequestProcessor("Paste Support");
        private DataObject[] objs;

        protected abstract boolean handleCanPaste(DataObject var1);

        protected abstract void handlePaste(DataObject var1) throws IOException;

        protected boolean cleanClipboard() {
            return false;
        }

        public final boolean canPaste() {
            for (int i = 0; i < this.objs.length; ++i) {
                if (this.handleCanPaste(this.objs[i])) continue;
                return false;
            }
            return true;
        }

        public final Transferable paste() throws IOException {
            if (SwingUtilities.isEventDispatchThread()) {
                RP.post(new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        String n = Actions.cutAmpersand((String)PasteTypeExt.this.getName());
                        ProgressHandle h = ProgressHandleFactory.createHandle((String)n);
                        h.start();
                        h.switchToIndeterminate();
                        try {
                            PasteTypeExt.this.doPaste();
                        }
                        catch (IOException ioe) {
                            Exceptions.printStackTrace((Throwable)ioe);
                        }
                        finally {
                            h.finish();
                        }
                    }
                });
            } else {
                this.doPaste();
            }
            return this.cleanClipboard() ? ExTransferable.EMPTY : null;
        }

        private void doPaste() throws IOException {
            if (err.isLoggable(Level.FINE)) {
                err.log(Level.FINE, null, new Throwable("Issue #58666: Called " + (Object)((Object)this) + " doPaste() on objects " + Arrays.asList(this.objs)));
            }
            for (int i = 0; i < this.objs.length; ++i) {
                this.handlePaste(this.objs[i]);
            }
        }

        public final void setDataObjects(DataObject[] objs) {
            this.objs = objs;
        }

    }

}

