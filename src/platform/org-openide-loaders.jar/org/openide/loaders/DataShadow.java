/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.filesystems.FileSystem$HtmlStatus
 *  org.openide.filesystems.FileSystem$Status
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.FilterNode$NodeAdapter
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.PropertySupport
 *  org.openide.nodes.PropertySupport$Name
 *  org.openide.nodes.PropertySupport$ReadWrite
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$ExceptionAction
 *  org.openide.util.MutexException
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 *  org.openide.util.Utilities
 *  org.openide.util.datatransfer.ExTransferable
 *  org.openide.util.datatransfer.ExTransferable$Single
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 *  org.openide.xml.XMLUtil
 */
package org.openide.loaders;

import java.awt.Image;
import java.awt.datatransfer.Transferable;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Reader;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventObject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.loaders.BrokenDataShadow;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataLoader;
import org.openide.loaders.DataLoaderPool;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.loaders.DataObjectPool;
import org.openide.loaders.FolderList;
import org.openide.loaders.LoaderTransfer;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.loaders.OperationEvent;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.nodes.NodeListener;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.MutexException;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.Utilities;
import org.openide.util.datatransfer.ExTransferable;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import org.openide.xml.XMLUtil;

public class DataShadow
extends MultiDataObject
implements DataObject.Container {
    static final long serialVersionUID = 6305590675982925167L;
    private static final String SFS_NAME = "SystemFileSystem";
    private DataObject original;
    private OrigL origL = null;
    private LinkedList<ShadowNode> nodes = new LinkedList();
    private DSLookup lookup;
    static final String SHADOW_EXTENSION = "shadow";
    private static Map<FileObject, Set<Reference<DataShadow>>> allDataShadows;
    private static Mutex MUTEX;
    private static RequestProcessor RP;
    private static Reference<Task> lastTask;

    private static synchronized Map<FileObject, Set<Reference<DataShadow>>> getDataShadowsSet() {
        if (allDataShadows == null) {
            allDataShadows = new HashMap<FileObject, Set<Reference<DataShadow>>>();
        }
        return allDataShadows;
    }

    private static synchronized void enqueueDataShadow(DataShadow ds) {
        Map<FileObject, Set<Reference<DataShadow>>> m = DataShadow.getDataShadowsSet();
        FileObject prim = ds.original.getPrimaryFile();
        DSWeakReference<DataShadow> ref = new DSWeakReference<DataShadow>(ds);
        Set<Reference<DataShadow>> s = m.get((Object)prim);
        if (s == null) {
            s = Collections.singleton(ref);
            DataShadow.getDataShadowsSet().put(prim, s);
        } else {
            if (!(s instanceof HashSet)) {
                s = new HashSet<Reference<DataShadow>>(s);
                DataShadow.getDataShadowsSet().put(prim, s);
            }
            s.add(ref);
        }
    }

    private static synchronized List<DataShadow> getAllDataShadows() {
        if (allDataShadows == null || allDataShadows.isEmpty()) {
            return null;
        }
        ArrayList<DataShadow> ret = new ArrayList<DataShadow>(allDataShadows.size());
        for (Set<Reference<DataShadow>> ref : allDataShadows.values()) {
            for (Reference<DataShadow> r : ref) {
                DataShadow shadow = r.get();
                if (shadow == null) continue;
                ret.add(shadow);
            }
        }
        return ret;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void checkValidity(EventObject ev) {
        DataObject src = null;
        if (ev instanceof OperationEvent) {
            src = ((OperationEvent)ev).getObject();
        }
        Set<Reference<DataShadow>> shadows = null;
        Class<DataShadow> class_ = DataShadow.class;
        synchronized (DataShadow.class) {
            if (allDataShadows == null || allDataShadows.isEmpty()) {
                // ** MonitorExit[var3_3] (shouldn't be in output)
                return;
            }
            if (src != null) {
                shadows = allDataShadows.get((Object)src.getPrimaryFile());
                if (shadows == null) {
                    // ** MonitorExit[var3_3] (shouldn't be in output)
                    return;
                }
                shadows = new HashSet<Reference<DataShadow>>(shadows);
            }
            // ** MonitorExit[var3_3] (shouldn't be in output)
            DataObject changed = null;
            if (ev instanceof OperationEvent.Rename || ev instanceof OperationEvent.Move) {
                changed = ((OperationEvent)ev).getObject();
            }
            if (shadows != null) {
                for (Reference<DataShadow> r : shadows) {
                    DataShadow shadow = r.get();
                    if (shadow == null) continue;
                    shadow.refresh(shadow.original == changed);
                }
                return;
            }
            List<DataShadow> all = DataShadow.getAllDataShadows();
            if (all == null) {
                return;
            }
            int size = all.size();
            for (int i = 0; i < size; ++i) {
                DataShadow obj = all.get(i);
                obj.refresh(obj.original == changed);
            }
            return;
        }
    }

    protected DataShadow(FileObject fo, DataObject original, MultiFileLoader loader) throws DataObjectExistsException {
        super(fo, loader);
        this.init(original);
    }

    @Deprecated
    protected DataShadow(FileObject fo, DataObject original, DataLoader loader) throws DataObjectExistsException {
        super(fo, loader);
        this.init(original);
    }

    private void init(DataObject original) {
        if (original == null) {
            throw new IllegalArgumentException();
        }
        this.setOriginal(original);
        DataShadow.enqueueDataShadow(this);
    }

    private DataShadow(FileObject fo, DataObject original) throws DataObjectExistsException {
        this(fo, original, DataLoaderPool.getShadowLoader());
    }

    public static DataShadow create(DataFolder folder, DataObject original) throws IOException {
        return DataShadow.create(folder, null, original, "shadow");
    }

    public static DataShadow create(DataFolder folder, String name, DataObject original) throws IOException {
        return DataShadow.create(folder, name, original, "shadow");
    }

    public static DataShadow create(DataFolder folder, String name, DataObject original, String ext) throws IOException {
        FileObject fo = folder.getPrimaryFile();
        CreateShadow cs = new CreateShadow(name, ext, fo, original);
        DataObjectPool.getPOOL().runAtomicAction(fo, cs);
        return cs.getShadow();
    }

    private static FileObject writeOriginal(final String name, final String ext, final FileObject trg, final DataObject obj) throws IOException {
        try {
            return (FileObject)MUTEX.writeAccess((Mutex.ExceptionAction)new Mutex.ExceptionAction<FileObject>(){

                public FileObject run() throws IOException {
                    FileObject fo;
                    if (trg.isData()) {
                        fo = trg;
                    } else {
                        String n;
                        if (name == null) {
                            String baseName = obj.getName().replace(':', '_').replace('/', '_');
                            n = FileUtil.findFreeFileName((FileObject)trg, (String)baseName, (String)ext);
                        } else {
                            n = name;
                        }
                        fo = trg.createData(n, ext);
                    }
                    DataShadow.writeShadowFile(fo, obj.getPrimaryFile().getURL());
                    return fo;
                }
            });
        }
        catch (MutexException e) {
            throw (IOException)e.getException();
        }
    }

    static void writeOriginal(final FileObject shadow, final URL url) throws IOException {
        try {
            MUTEX.writeAccess((Mutex.ExceptionAction)new Mutex.ExceptionAction<Void>(){

                public Void run() throws IOException {
                    DataShadow.writeShadowFile(shadow, url);
                    return null;
                }
            });
        }
        catch (MutexException e) {
            throw (IOException)e.getException();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void writeShadowFile(FileObject fo, URL url) throws IOException {
        FileLock lock = fo.lock();
        OutputStreamWriter os = new OutputStreamWriter(fo.getOutputStream(lock), "UTF-8");
        try {
            os.write(url.toExternalForm());
        }
        finally {
            os.close();
            lock.releaseLock();
        }
    }

    public static FileObject findOriginal(FileObject fileObject) throws IOException {
        URI u;
        FileObject target;
        String[] fileAndFileSystem = DataShadow.readOriginalFileAndFileSystem(fileObject);
        String path = fileAndFileSystem[0];
        assert (path != null);
        try {
            u = new URI(path);
        }
        catch (URISyntaxException e) {
            u = null;
        }
        String fsname = fileAndFileSystem[1];
        if (u != null && u.isAbsolute()) {
            target = URLMapper.findFileObject((URL)u.toURL());
            if (target == null) {
                FileObject cfgRoot = FileUtil.getConfigRoot();
                URI cfgURI = cfgRoot.toURI();
                String prefix = cfgURI.toString();
                String urlText = u.toString();
                if (urlText.startsWith(prefix)) {
                    String rootPathFragment = cfgURI.getRawPath();
                    String cfgPart = u.getRawPath().substring(rootPathFragment.length());
                    String translated = Utilities.translate((String)cfgPart);
                    if (!translated.equals(cfgPart)) {
                        try {
                            URI temp = new URI(prefix + translated);
                            target = URLMapper.findFileObject((URL)temp.toURL());
                        }
                        catch (URISyntaxException ex) {
                            LOG.log(Level.WARNING, "Could not form URI from {0}: {1}", new Object[]{translated, ex});
                        }
                    }
                }
            }
        } else {
            FileSystem fs;
            String translated;
            boolean sfs;
            if ("SystemFileSystem".equals(fsname)) {
                fs = FileUtil.getConfigRoot().getFileSystem();
                sfs = true;
            } else {
                fs = fileObject.getFileSystem();
                sfs = false;
            }
            target = fs.findResource(path);
            if (target == null && fsname == null) {
                sfs = true;
                target = FileUtil.getConfigFile((String)path);
            }
            if (sfs && target == null && !path.equals(translated = Utilities.translate((String)path))) {
                target = FileUtil.getConfigFile((String)translated);
            }
        }
        return target;
    }

    protected static DataObject deserialize(FileObject fileObject) throws IOException {
        FileObject target = DataShadow.findOriginal(fileObject);
        if (target != null) {
            return DataObject.find(target);
        }
        String[] fileAndFileSystem = DataShadow.readOriginalFileAndFileSystem(fileObject);
        String path = fileAndFileSystem[0];
        throw new FileNotFoundException(path + ':' + fileAndFileSystem[1]);
    }

    static URL readURL(FileObject fileObject) throws IOException {
        URI u;
        String[] fileAndFileSystem = DataShadow.readOriginalFileAndFileSystem(fileObject);
        String path = fileAndFileSystem[0];
        assert (path != null);
        try {
            u = new URI(path);
        }
        catch (URISyntaxException e) {
            u = null;
        }
        if (u != null && u.isAbsolute()) {
            return u.toURL();
        }
        String fsname = fileAndFileSystem[1];
        FileSystem fs = "SystemFileSystem".equals(fsname) || fsname == null && fileObject.getFileSystem().findResource(path) == null && FileUtil.getConfigFile((String)path) != null ? FileUtil.getConfigRoot().getFileSystem() : fileObject.getFileSystem();
        return new URL(fs.getRoot().getURL(), path);
    }

    private static String[] readOriginalFileAndFileSystem(final FileObject f) throws IOException {
        if (f.getSize() == 0) {
            Object fileName = f.getAttribute("originalFile");
            if (fileName instanceof String) {
                return new String[]{(String)fileName, (String)f.getAttribute("originalFileSystem")};
            }
            if (fileName instanceof URL) {
                return new String[]{((URL)fileName).toExternalForm(), null};
            }
            throw new FileNotFoundException(f.getPath());
        }
        try {
            return (String[])MUTEX.readAccess((Mutex.ExceptionAction)new Mutex.ExceptionAction<String[]>(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                public String[] run() throws IOException {
                    BufferedReader ois = new BufferedReader(new InputStreamReader(f.getInputStream(), "UTF-8"));
                    try {
                        String s = ois.readLine();
                        String fs = ois.readLine();
                        String[] arrstring = new String[]{s, fs};
                        return arrstring;
                    }
                    finally {
                        ois.close();
                    }
                }
            });
        }
        catch (MutexException e) {
            throw (IOException)e.getException();
        }
    }

    private FileObject checkOriginal(DataObject orig) throws IOException {
        if (orig == null) {
            return null;
        }
        return DataShadow.deserialize(this.getPrimaryFile()).getPrimaryFile();
    }

    public DataObject getOriginal() {
        DataShadow.waitUpdatesProcessed();
        return this.original;
    }

    @Override
    public DataObject[] getChildren() {
        return new DataObject[]{this.original};
    }

    @Override
    protected Node createNodeDelegate() {
        return new ShadowNode(this);
    }

    @Override
    public boolean isDeleteAllowed() {
        return this.getPrimaryFile().canWrite();
    }

    @Override
    public boolean isCopyAllowed() {
        return true;
    }

    @Override
    public boolean isMoveAllowed() {
        return this.getPrimaryFile().canWrite();
    }

    @Override
    public boolean isRenameAllowed() {
        return this.getPrimaryFile().canWrite();
    }

    @Override
    public HelpCtx getHelpCtx() {
        return this.original.getHelpCtx();
    }

    @Override
    protected DataShadow handleCreateShadow(DataFolder f) throws IOException {
        if (this.original instanceof DataFolder) {
            DataFolder.testNesting((DataFolder)this.original, f);
        }
        return this.original.handleCreateShadow(f);
    }

    @Override
    public <T extends Node.Cookie> T getCookie(Class<T> c) {
        if (c.isInstance(this)) {
            return (T)((Node.Cookie)c.cast(this));
        }
        return this.original.getCookie(this, c);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Lookup getLookup() {
        if (this.getClass() == DataShadow.class) {
            Class<DataShadow> class_ = DataShadow.class;
            synchronized (DataShadow.class) {
                if (this.lookup == null) {
                    this.lookup = new DSLookup(this, this.original);
                }
                // ** MonitorExit[var1_1] (shouldn't be in output)
                return this.lookup;
            }
        }
        return super.getLookup();
    }

    public void refresh() {
        this.refresh(false);
    }

    private void refresh(boolean moved) {
        try {
            FileObject obj;
            if (moved) {
                this.tryUpdate();
            }
            if ((obj = this.checkOriginal(this.original)) != null) {
                if (obj != this.original.getPrimaryFile()) {
                    this.setOriginal(DataObject.find(obj));
                }
                return;
            }
        }
        catch (IOException e) {
            // empty catch block
        }
        try {
            this.setValid(false);
        }
        catch (PropertyVetoException e) {
            // empty catch block
        }
    }

    @Override
    void notifyAttributeChanged(FileAttributeEvent fae) {
        super.notifyAttributeChanged(fae);
        this.refresh(false);
    }

    @Override
    void notifyFileChanged(FileEvent fe) {
        super.notifyFileChanged(fe);
        if (!CreateShadow.isOurs(fe)) {
            this.refresh(false);
        }
    }

    private void tryUpdate() throws IOException {
        URL url = DataShadow.readURL(this.getPrimaryFile());
        if (url.equals(this.original.getPrimaryFile().getURL())) {
            return;
        }
        DataShadow.writeOriginal(null, null, this.getPrimaryFile(), this.original);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void setOriginal(DataObject o) {
        if (this.origL == null) {
            this.origL = new OrigL(this);
        }
        if (this.original != null) {
            this.original.removePropertyChangeListener(this.origL);
        }
        DataObject oldOriginal = this.original;
        o.addPropertyChangeListener(this.origL);
        this.original = o;
        if (this.lookup != null) {
            this.lookup.refresh(this, this.original);
        }
        ShadowNode[] n = null;
        LinkedList<ShadowNode> linkedList = this.nodes;
        synchronized (linkedList) {
            n = this.nodes.toArray((T[])new ShadowNode[this.nodes.size()]);
        }
        try {
            for (int i = 0; i < n.length; ++i) {
                n[i].originalChanged();
            }
        }
        catch (IllegalStateException e) {
            System.out.println("Please reopen the bug #18998 if you see this message.");
            System.out.println("Old:" + oldOriginal + (oldOriginal == null ? "" : new StringBuilder().append(" / ").append(oldOriginal.isValid()).append(" / ").append(System.identityHashCode(oldOriginal)).toString()));
            System.out.println("New:" + this.original + (this.original == null ? "" : new StringBuilder().append(" / ").append(this.original.isValid()).append(" / ").append(System.identityHashCode(this.original)).toString()));
            throw e;
        }
    }

    private static void updateShadowOriginal(DataShadow shadow) {
        class Updator
        implements Runnable {
            DataShadow sh;
            FileObject primary;

            Updator() {
            }

            @Override
            public void run() {
                DataObject newOrig;
                try {
                    newOrig = DataObject.find(this.primary);
                }
                catch (DataObjectNotFoundException e) {
                    newOrig = null;
                }
                if (newOrig != null) {
                    this.sh.setOriginal(newOrig);
                } else {
                    DataShadow.checkValidity(new OperationEvent(this.sh.original));
                }
                this.primary = null;
                this.sh = null;
            }
        }
        Updator u = new Updator();
        u.sh = shadow;
        u.primary = u.sh.original.getPrimaryFile();
        ERR.fine("updateShadowOriginal: " + u.sh + " primary " + (Object)u.primary);
        lastTask = new WeakReference<RequestProcessor.Task>(RP.post((Runnable)u, 100, 2));
    }

    static final void waitUpdatesProcessed() {
        Task t;
        if (!RP.isRequestProcessorThread() && (t = lastTask.get()) != null) {
            t.waitFinished();
        }
    }

    @Override
    protected DataObject handleCopy(DataFolder f) throws IOException {
        if (this.original instanceof DataFolder) {
            DataFolder.testNesting((DataFolder)this.original, f);
        }
        return super.handleCopy(f);
    }

    @Override
    protected FileObject handleMove(DataFolder f) throws IOException {
        if (this.original instanceof DataFolder) {
            DataFolder.testNesting((DataFolder)this.original, f);
        }
        return super.handleMove(f);
    }

    static {
        MUTEX = new Mutex();
        RP = new RequestProcessor("DataShadow validity check");
        lastTask = new WeakReference<Object>(null);
    }

    private static class CreateShadow
    implements FileSystem.AtomicAction {
        private final String name;
        private final String ext;
        private final FileObject fo;
        private final DataObject original;
        private DataShadow res;

        CreateShadow(String name, String ext, FileObject fo, DataObject original) {
            this.name = name;
            this.ext = ext;
            this.fo = fo;
            this.original = original;
        }

        static boolean isOurs(FileEvent ev) {
            return ev.firedFrom((FileSystem.AtomicAction)new CreateShadow(null, null, null, null));
        }

        DataShadow getShadow() {
            return this.res;
        }

        public void run() throws IOException {
            FileObject file = DataShadow.writeOriginal(this.name, this.ext, this.fo, this.original);
            final DataObject obj = DataObject.find(file);
            if (!(obj instanceof DataShadow)) {
                throw new DataObjectNotFoundException(obj.getPrimaryFile()){

                    @Override
                    public String getMessage() {
                        return super.getMessage() + ": " + obj.getClass().getName();
                    }
                };
            }
            this.res = (DataShadow)obj;
            FolderList.changedDataSystem(this.fo);
        }

        public boolean equals(Object obj) {
            return obj != null && this.getClass().equals(obj.getClass());
        }

        public int hashCode() {
            return this.getClass().hashCode();
        }

    }

    private static class DSLookup
    extends ProxyLookup {
        public DSLookup(DataShadow ds, DataObject original) {
            super(DSLookup.compute(ds, original));
        }

        public void refresh(DataShadow ds, DataObject original) {
            this.setLookups(DSLookup.compute(ds, original));
        }

        private static Lookup[] compute(DataShadow ds, DataObject original) {
            return new Lookup[]{Lookups.singleton((Object)ds), original.getLookup()};
        }
    }

    static final class DSWeakReference<T>
    extends WeakReference<T>
    implements Runnable {
        private int hash;
        private FileObject original;

        DSWeakReference(T o) {
            super(o, Utilities.activeReferenceQueue());
            this.hash = o.hashCode();
            if (o instanceof DataShadow) {
                DataShadow s = (DataShadow)o;
                this.original = s.original.getPrimaryFile();
            }
        }

        public int hashCode() {
            return this.hash;
        }

        public boolean equals(Object o) {
            Object mine = this.get();
            if (mine == null) {
                return false;
            }
            if (o instanceof DSWeakReference) {
                DSWeakReference him = (DSWeakReference)o;
                return mine.equals(him.get());
            }
            return false;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            if (this.original != null) {
                Map map = DataShadow.getDataShadowsSet();
                synchronized (map) {
                    DataShadow.getDataShadowsSet().remove((Object)this.original);
                }
            }
            Map<String, Set<Reference<BrokenDataShadow>>> map = BrokenDataShadow.getDataShadowsSet();
            synchronized (map) {
                BrokenDataShadow.getDataShadowsSet().remove(this);
            }
        }
    }

    protected static class ShadowNode
    extends FilterNode {
        private static MessageFormat format;
        private static MessageFormat descriptionFormat;
        private static final String ATTR_USEOWNNAME = "UseOwnName";
        private DataShadow obj;
        private Sheet sheet;

        public ShadowNode(DataShadow shadow) {
            this(shadow, shadow.original.getNodeDelegate());
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private ShadowNode(DataShadow shadow, Node node) {
            super(node);
            this.obj = shadow;
            LinkedList linkedList = this.obj.nodes;
            synchronized (linkedList) {
                this.obj.nodes.add(this);
            }
        }

        public Node cloneNode() {
            ShadowNode sn = new ShadowNode(this.obj);
            return sn;
        }

        public void setName(String name) {
            try {
                if (!name.equals(this.obj.getName())) {
                    this.obj.rename(name);
                    if (this.obj.original.getPrimaryFile().isRoot()) {
                        this.obj.getPrimaryFile().setAttribute("UseOwnName", (Object)Boolean.TRUE);
                    }
                    this.fireDisplayNameChange(null, null);
                    this.fireNameChange(null, null);
                }
            }
            catch (IOException ex) {
                throw new IllegalArgumentException(ex.getMessage());
            }
        }

        public String getName() {
            return this.obj.getName();
        }

        public String getDisplayName() {
            if (format == null) {
                format = new MessageFormat(NbBundle.getBundle(DataShadow.class).getString("FMT_shadowName"));
            }
            String n = format.format(this.createArguments());
            try {
                return this.obj.getPrimaryFile().getFileSystem().getStatus().annotateName(n, this.obj.files());
            }
            catch (FileStateInvalidException fsie) {
                return n;
            }
        }

        public String getHtmlDisplayName() {
            if (format == null) {
                format = new MessageFormat(NbBundle.getBundle(DataShadow.class).getString("FMT_shadowName"));
            }
            try {
                String n = XMLUtil.toElementContent((String)format.format(this.createArguments()));
                FileSystem.Status s = this.obj.getPrimaryFile().getFileSystem().getStatus();
                if (s instanceof FileSystem.HtmlStatus) {
                    return ((FileSystem.HtmlStatus)s).annotateNameHtml(n, this.obj.files());
                }
            }
            catch (IOException e) {
                // empty catch block
            }
            return null;
        }

        private Object[] createArguments() {
            String shadowName = this.obj.getName();
            String origDisp = this.obj.original.isValid() ? this.obj.original.getNodeDelegate().getDisplayName() : "";
            Boolean useOwnName = (Boolean)this.obj.getPrimaryFile().getAttribute("UseOwnName");
            if (this.obj.original.getPrimaryFile().isRoot() && (useOwnName == null || !useOwnName.booleanValue())) {
                try {
                    shadowName = this.obj.original.getPrimaryFile().getFileSystem().getDisplayName();
                }
                catch (FileStateInvalidException e) {
                    // empty catch block
                }
            }
            return new Object[]{shadowName, super.getDisplayName(), ShadowNode.systemNameOrFileName(this.obj.getPrimaryFile()), ShadowNode.systemNameOrFileName(this.obj.original.getPrimaryFile()), origDisp};
        }

        private static String systemNameOrFileName(FileObject fo) {
            return FileUtil.getFileDisplayName((FileObject)fo);
        }

        public String getShortDescription() {
            if (descriptionFormat == null) {
                descriptionFormat = new MessageFormat(NbBundle.getBundle(DataShadow.class).getString("FMT_shadowHint"));
            }
            return descriptionFormat.format(this.createArguments());
        }

        public Image getIcon(int type) {
            Image i = this.rootIcon(type);
            if (i != null) {
                return i;
            }
            return super.getIcon(type);
        }

        public Image getOpenedIcon(int type) {
            Image i = this.rootIcon(type);
            if (i != null) {
                return i;
            }
            return super.getOpenedIcon(type);
        }

        private Image rootIcon(int type) {
            FileObject orig = this.obj.original.getPrimaryFile();
            if (orig.isRoot()) {
                try {
                    FileSystem fs = orig.getFileSystem();
                    try {
                        Image i = Introspector.getBeanInfo(fs.getClass()).getIcon(type);
                        if (i != null) {
                            return fs.getStatus().annotateIcon(i, type, this.obj.original.files());
                        }
                    }
                    catch (IntrospectionException ie) {
                        Logger.getLogger(DataShadow.class.getName()).log(Level.WARNING, null, ie);
                    }
                }
                catch (FileStateInvalidException fsie) {
                    // empty catch block
                }
            }
            return null;
        }

        public boolean canDestroy() {
            return this.obj.isDeleteAllowed();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void destroy() throws IOException {
            LinkedList linkedList = this.obj.nodes;
            synchronized (linkedList) {
                this.obj.nodes.remove((Object)this);
            }
            this.obj.delete();
        }

        public final boolean canRename() {
            return this.obj.isRenameAllowed();
        }

        public final boolean canCopy() {
            return this.obj.isCopyAllowed();
        }

        public final boolean canCut() {
            return this.obj.isMoveAllowed();
        }

        public <T extends Node.Cookie> T getCookie(Class<T> cl) {
            T c = this.obj.getCookie(cl);
            if (c != null) {
                return c;
            }
            return (T)super.getCookie(cl);
        }

        public Node.PropertySet[] getPropertySets() {
            Sheet s = this.sheet;
            if (s == null) {
                s = this.sheet = this.cloneSheet();
            }
            return s.toArray();
        }

        public Transferable clipboardCopy() throws IOException {
            ExTransferable t = ExTransferable.create((Transferable)super.clipboardCopy());
            t.put(LoaderTransfer.transferable(this.obj, 1));
            return t;
        }

        public Transferable clipboardCut() throws IOException {
            ExTransferable t = ExTransferable.create((Transferable)super.clipboardCut());
            t.put(LoaderTransfer.transferable(this.obj, 4));
            return t;
        }

        public Transferable drag() throws IOException {
            return this.clipboardCopy();
        }

        protected NodeListener createNodeListener() {
            return new PropL(this);
        }

        public boolean equals(Object o) {
            if (o instanceof ShadowNode) {
                ShadowNode sn = (ShadowNode)((Object)o);
                return sn.obj == this.obj;
            }
            return false;
        }

        public int hashCode() {
            return this.obj.hashCode();
        }

        private Sheet cloneSheet() {
            Node.PropertySet[] sets = this.getOriginal().getPropertySets();
            Sheet s = new Sheet();
            for (int i = 0; i < sets.length; ++i) {
                Sheet.Set ss = new Sheet.Set();
                ss.put(sets[i].getProperties());
                ss.setName(sets[i].getName());
                ss.setDisplayName(sets[i].getDisplayName());
                ss.setShortDescription(sets[i].getShortDescription());
                this.modifySheetSet(ss);
                s.put(ss);
            }
            return s;
        }

        private void modifySheetSet(Sheet.Set ss) {
            Object p = ss.remove("name");
            if (p != null) {
                p = new PropertySupport.Name((Node)this);
                ss.put((Node.Property)p);
                p = new Name();
                ss.put((Node.Property)p);
            }
        }

        private void originalChanged() {
            DataObject ori = this.obj.original;
            if (ori.isValid()) {
                this.changeOriginal(ori.getNodeDelegate(), true);
            } else {
                DataShadow.updateShadowOriginal(this.obj);
            }
        }

        private static class PropL
        extends FilterNode.NodeAdapter {
            public PropL(ShadowNode sn) {
                super((FilterNode)sn);
            }

            protected void propertyChange(FilterNode fn, PropertyChangeEvent ev) {
                if ("propertySets".equals(ev.getPropertyName())) {
                    ShadowNode sn = (ShadowNode)fn;
                    sn.sheet = null;
                }
                super.propertyChange(fn, ev);
            }
        }

        private final class Name
        extends PropertySupport.ReadWrite<String> {
            public Name() {
                super("OriginalName", String.class, DataObject.getString("PROP_ShadowOriginalName"), DataObject.getString("HINT_ShadowOriginalName"));
            }

            public String getValue() {
                return ShadowNode.this.obj.original.getName();
            }

            public void setValue(String val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
                if (!this.canWrite()) {
                    throw new IllegalAccessException();
                }
                try {
                    DataObject orig = ShadowNode.this.obj.original;
                    orig.rename(val);
                    DataShadow.writeOriginal(null, null, ShadowNode.this.obj.getPrimaryFile(), orig);
                }
                catch (IOException ex) {
                    throw new InvocationTargetException(ex);
                }
            }

            public boolean canWrite() {
                return ShadowNode.this.obj.original.isRenameAllowed();
            }
        }

    }

    private static class OrigL
    implements PropertyChangeListener {
        Reference<DataShadow> shadow = null;

        public OrigL(DataShadow shadow) {
            this.shadow = new WeakReference<DataShadow>(shadow);
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            DataShadow shadow = this.shadow.get();
            if (shadow != null && "valid".equals(evt.getPropertyName())) {
                DataShadow.updateShadowOriginal(shadow);
            }
        }
    }

}

