/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$FinishablePanel
 *  org.openide.util.HelpCtx
 */
package org.openide.loaders;

import java.awt.Component;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;
import org.openide.loaders.TemplateWizard2;
import org.openide.util.HelpCtx;

final class TemplateWizardPanel2
implements WizardDescriptor.FinishablePanel<WizardDescriptor> {
    private TemplateWizard2 templateWizard2UI;
    private ChangeListener listener;
    private WizardDescriptor settings;

    TemplateWizardPanel2() {
    }

    private TemplateWizard2 getPanelUI() {
        if (this.templateWizard2UI == null) {
            this.templateWizard2UI = new TemplateWizard2();
            this.templateWizard2UI.addChangeListener(this.listener);
        }
        return this.templateWizard2UI;
    }

    public void addChangeListener(ChangeListener l) {
        if (this.listener != null) {
            throw new IllegalStateException();
        }
        if (this.templateWizard2UI != null) {
            this.templateWizard2UI.addChangeListener(l);
        }
        this.listener = l;
    }

    public void removeChangeListener(ChangeListener l) {
        this.listener = null;
        if (this.templateWizard2UI != null) {
            this.templateWizard2UI.removeChangeListener(l);
        }
    }

    public Component getComponent() {
        return this.getPanelUI();
    }

    public HelpCtx getHelp() {
        return new HelpCtx(TemplateWizard2.class);
    }

    public boolean isValid() {
        if (this.templateWizard2UI == null) {
            return false;
        }
        String err = this.getPanelUI().implIsValid();
        if (this.getPanelUI().isShowing()) {
            this.settings.putProperty("WizardPanel_errorMessage", (Object)err);
        }
        return err == null;
    }

    public void readSettings(WizardDescriptor settings) {
        this.settings = settings;
        this.getPanelUI().implReadSettings((Object)settings);
    }

    public void storeSettings(WizardDescriptor settings) {
        this.getPanelUI().implStoreSettings((Object)settings);
        this.settings = null;
    }

    public boolean isFinishPanel() {
        return true;
    }
}

