/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.AcceleratorBinding
 *  org.openide.awt.Actions
 *  org.openide.awt.ToolbarWithOverflow
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.InstanceCookie$Of
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.nodes.Node
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Task
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Toolbar
 */
package org.openide.awt;

import java.awt.Component;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.EventListener;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ToolBarUI;
import javax.swing.plaf.metal.MetalLookAndFeel;
import org.netbeans.modules.openide.loaders.AWTTask;
import org.netbeans.modules.openide.loaders.DataObjectAccessor;
import org.openide.awt.AcceleratorBinding;
import org.openide.awt.Actions;
import org.openide.awt.ToolbarPool;
import org.openide.awt.ToolbarWithOverflow;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.FolderInstance;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.Task;
import org.openide.util.actions.Presenter;

public class Toolbar
extends ToolbarWithOverflow {
    @Deprecated
    public static final int BASIC_HEIGHT = 34;
    static final Logger LOG = Logger.getLogger(Toolbar.class.getName());
    private String displayName;
    private DataFolder backingFolder;
    private Folder processor;
    private static final boolean isMetalLaF = MetalLookAndFeel.class.isAssignableFrom(UIManager.getLookAndFeel().getClass());
    static final long serialVersionUID = 5011742660516204764L;
    private JButton label;
    private static final Insets emptyInsets;
    private static Action emptyAction;

    public Toolbar() {
        this("");
    }

    public Toolbar(String name) {
        this(name, name, false);
    }

    public Toolbar(String name, String displayName) {
        this(name, displayName, false);
    }

    public Toolbar(String name, boolean f) {
        this(name, name, f);
    }

    Toolbar(DataFolder folder) {
        this.backingFolder = folder;
        this.initAll(folder.getName(), false);
        this.putClientProperty((Object)"folder", (Object)folder);
    }

    public boolean isOpaque() {
        if (null != UIManager.get("NbMainWindow.showCustomBackground")) {
            return !UIManager.getBoolean("NbMainWindow.showCustomBackground");
        }
        return super.isOpaque();
    }

    DataFolder getFolder() {
        return this.backingFolder;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final Folder waitFinished() {
        if (this.backingFolder == null) {
            return null;
        }
        Toolbar toolbar = this;
        synchronized (toolbar) {
            if (this.processor == null && this.isVisible()) {
                this.processor = new Folder();
            }
            return this.processor;
        }
    }

    public void addNotify() {
        super.addNotify();
        this.waitFinished();
    }

    public Component[] getComponents() {
        this.waitFinished();
        return super.getComponents();
    }

    public void setVisible(boolean b) {
        super.setVisible(b);
        this.waitFinished();
    }

    protected void addImpl(Component c, Object constraints, int idx) {
        if (c instanceof AbstractButton) {
            c.setFocusable(false);
            ((JComponent)c).setOpaque(false);
            if (isMetalLaF) {
                ((AbstractButton)c).setBorderPainted(false);
                ((AbstractButton)c).setOpaque(false);
            }
            if (!isMetalLaF) {
                ((AbstractButton)c).setMargin(emptyInsets);
            }
            if (null != this.label && c != this.label) {
                this.remove((Component)this.label);
                this.label = null;
            }
        } else if (c instanceof JToolBar.Separator) {
            JToolBar.Separator separator = (JToolBar.Separator)c;
            if (this.getOrientation() == 1) {
                separator.setOrientation(0);
            } else {
                separator.setOrientation(1);
            }
        }
        super.addImpl(c, constraints, idx);
    }

    public Toolbar(String name, String displayName, boolean f) {
        this.setDisplayName(displayName);
        this.initAll(name, f);
    }

    @Deprecated
    public static int getBasicHeight() {
        return ToolbarPool.getDefault().getPreferredIconSize();
    }

    private void initAll(String name, boolean f) {
        this.setName(name);
        this.setFloatable(f);
        this.getAccessibleContext().setAccessibleName(this.displayName == null ? this.getName() : this.displayName);
        this.getAccessibleContext().setAccessibleDescription(this.getName());
    }

    public String getUIClassID() {
        if (UIManager.get("Nb.Toolbar.ui") != null) {
            return "Nb.Toolbar.ui";
        }
        return super.getUIClassID();
    }

    @Deprecated
    public static int rowCount(int height) {
        return 1;
    }

    @Deprecated
    public void setDnDListener(DnDListener l) {
    }

    @Deprecated
    protected void fireDragToolbar(int dx, int dy, int type) {
    }

    @Deprecated
    protected void fireDropToolbar(int dx, int dy, int type) {
    }

    public String getDisplayName() {
        if (this.displayName == null) {
            if (this.backingFolder.isValid()) {
                try {
                    return this.backingFolder.getNodeDelegate().getDisplayName();
                }
                catch (IllegalStateException ex) {
                    // empty catch block
                }
            }
            return this.backingFolder.getName();
        }
        return this.displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    private static void acceleratorBindingsWarmUp() {
        if (null == emptyAction) {
            emptyAction = new Action(){

                @Override
                public Object getValue(String key) {
                    return null;
                }

                @Override
                public void putValue(String key, Object value) {
                }

                @Override
                public void setEnabled(boolean b) {
                }

                @Override
                public boolean isEnabled() {
                    return true;
                }

                @Override
                public void addPropertyChangeListener(PropertyChangeListener listener) {
                }

                @Override
                public void removePropertyChangeListener(PropertyChangeListener listener) {
                }

                @Override
                public void actionPerformed(ActionEvent e) {
                }
            };
            AcceleratorBinding.setAccelerator((Action)emptyAction, (FileObject)FileUtil.getConfigRoot());
        }
    }

    public void setUI(ToolBarUI ui) {
        super.setUI(ui);
        if (null != this.backingFolder && null != this.processor) {
            this.processor.recreate();
        }
    }

    static {
        try {
            Class.forName(AcceleratorBinding.class.getName());
        }
        catch (ClassNotFoundException x) {
            throw new ExceptionInInitializerError(x);
        }
        emptyInsets = new Insets(1, 1, 1, 1);
    }

    private static class DefaultIconButton
    extends JButton {
        private Icon unknownIcon;

        private DefaultIconButton() {
        }

        @Override
        public Icon getIcon() {
            Icon retValue = super.getIcon();
            if (null == retValue && (null == this.getText() || this.getText().length() == 0)) {
                if (this.unknownIcon == null) {
                    this.unknownIcon = ImageUtilities.loadImageIcon((String)"org/openide/loaders/unknown.gif", (boolean)false);
                }
                retValue = this.unknownIcon;
            }
            return retValue;
        }
    }

    @Deprecated
    public static class DnDEvent
    extends EventObject {
        public static final int DND_ONE = 1;
        public static final int DND_END = 2;
        public static final int DND_LINE = 3;
        private String name;
        private int dx;
        private int dy;
        private int type;
        static final long serialVersionUID = 4389530973297716699L;

        public DnDEvent(Toolbar toolbar, String name, int dx, int dy, int type) {
            super((Object)toolbar);
            this.name = name;
            this.dx = dx;
            this.dy = dy;
            this.type = type;
        }

        public String getName() {
            return this.name;
        }

        public int getDX() {
            return this.dx;
        }

        public int getDY() {
            return this.dy;
        }

        public int getType() {
            return this.type;
        }
    }

    @Deprecated
    public static interface DnDListener
    extends EventListener {
        public void dragToolbar(DnDEvent var1);

        public void dropToolbar(DnDEvent var1);
    }

    final class Folder
    extends FolderInstance {
        private Map<Object, DataObject> cookiesToObjects;

        public Folder() {
            super(Toolbar.this.backingFolder);
            this.cookiesToObjects = new HashMap<Object, DataObject>();
            DataObjectAccessor.DEFAULT.precreateInstances(this);
            this.recreate();
            Toolbar.acceleratorBindingsWarmUp();
        }

        @Override
        public String instanceName() {
            return Toolbar.this.getClass().getName();
        }

        public Class instanceClass() throws IOException, ClassNotFoundException {
            return Toolbar.this.getClass();
        }

        @Override
        protected Object instanceForCookie(DataObject obj, InstanceCookie cookie) throws IOException, ClassNotFoundException {
            Object result = super.instanceForCookie(obj, cookie);
            this.cookiesToObjects.put(result, obj);
            return result;
        }

        @Override
        protected InstanceCookie acceptCookie(InstanceCookie cookie) throws IOException, ClassNotFoundException {
            boolean action;
            boolean is;
            if (cookie instanceof InstanceCookie.Of) {
                InstanceCookie.Of of = (InstanceCookie.Of)cookie;
                action = of.instanceOf(Action.class);
                is = of.instanceOf(Component.class) || of.instanceOf(Presenter.Toolbar.class) || action;
            } else {
                Class c = cookie.instanceClass();
                action = Action.class.isAssignableFrom(c);
                boolean bl = is = Component.class.isAssignableFrom(c) || Presenter.Toolbar.class.isAssignableFrom(c) || action;
            }
            if (action) {
                cookie.instanceCreate();
            }
            return is ? cookie : null;
        }

        @Override
        protected InstanceCookie acceptFolder(DataFolder df) {
            return null;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        protected Object createInstance(InstanceCookie[] cookies) throws IOException, ClassNotFoundException {
            Toolbar.this.removeAll();
            for (int i = 0; i < cookies.length; ++i) {
                try {
                    Object obj = cookies[i].instanceCreate();
                    DataObject file = this.cookiesToObjects.get(obj);
                    if (obj instanceof Presenter.Toolbar) {
                        if (obj instanceof Action && file != null) {
                            AcceleratorBinding.setAccelerator((Action)((Action)obj), (FileObject)file.getPrimaryFile());
                        }
                        obj = ((Presenter.Toolbar)obj).getToolbarPresenter();
                    }
                    if (obj instanceof Component) {
                        if (obj instanceof JComponent && "Fixed".equals(((JComponent)obj).getClientProperty("Toolbar"))) {
                            Toolbar.this.removeAll();
                            Toolbar.this.setBorder(null);
                        }
                        if (obj instanceof JComponent) {
                            if (ToolbarPool.getDefault().getPreferredIconSize() == 24) {
                                ((JComponent)obj).putClientProperty("PreferredIconSize", new Integer(24));
                            }
                            ((JComponent)obj).putClientProperty("file", file);
                        }
                        Toolbar.this.add((Component)obj);
                    }
                    if (!(obj instanceof Action)) continue;
                    Action a = (Action)obj;
                    DefaultIconButton b = new DefaultIconButton();
                    if (ToolbarPool.getDefault().getPreferredIconSize() == 24) {
                        b.putClientProperty("PreferredIconSize", new Integer(24));
                    }
                    if (null == a.getValue("SmallIcon") && (null == a.getValue("Name") || a.getValue("Name").toString().length() == 0)) {
                        a.putValue("SmallIcon", ImageUtilities.loadImageIcon((String)"org/openide/loaders/unknown.gif", (boolean)false));
                    }
                    Actions.connect((AbstractButton)b, (Action)a);
                    b.putClientProperty("file", file);
                    Toolbar.this.add((Component)b);
                    if (file == null) continue;
                    AcceleratorBinding.setAccelerator((Action)a, (FileObject)file.getPrimaryFile());
                }
                catch (IOException ex) {
                    Toolbar.LOG.log(Level.WARNING, null, ex);
                }
                catch (ClassNotFoundException ex) {
                    Toolbar.LOG.log(Level.WARNING, null, ex);
                }
                finally {
                    this.cookiesToObjects.clear();
                }
            }
            if (cookies.length == 0) {
                Toolbar.this.label = new JButton("<" + Actions.cutAmpersand((String)Toolbar.this.getDisplayName()) + ">");
                Toolbar.this.add((Component)Toolbar.this.label);
            }
            Toolbar.this.invalidate();
            return Toolbar.this;
        }

        @Override
        protected Task postCreationTask(Runnable run) {
            return new AWTTask(run, this);
        }
    }

}

