/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.AcceleratorBinding
 *  org.openide.awt.Actions
 *  org.openide.awt.Mnemonics
 *  org.openide.cookies.InstanceCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeMemberEvent
 *  org.openide.nodes.NodeOp
 *  org.openide.nodes.NodeReorderEvent
 *  org.openide.util.Exceptions
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.Task
 *  org.openide.util.Utilities
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Toolbar
 */
package org.openide.awt;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.openide.loaders.AWTTask;
import org.netbeans.modules.openide.loaders.DataObjectAccessor;
import org.openide.awt.AcceleratorBinding;
import org.openide.awt.Actions;
import org.openide.awt.DynaMenuModel;
import org.openide.awt.Mnemonics;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.FolderInstance;
import org.openide.nodes.Node;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeOp;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.Exceptions;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.Task;
import org.openide.util.Utilities;
import org.openide.util.actions.Presenter;

public class MenuBar
extends JMenuBar
implements Externalizable {
    private MenuBarFolder menuBarFolder;
    static final long serialVersionUID = -4721949937356581268L;
    private static final Logger LOG;

    public MenuBar() {
    }

    public MenuBar(DataFolder folder) {
        this();
        DataFolder theFolder = folder;
        if (theFolder == null) {
            FileObject root = FileUtil.getConfigRoot();
            FileObject fo = null;
            try {
                fo = FileUtil.createFolder((FileObject)root, (String)"Menu");
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            if (fo == null) {
                throw new IllegalStateException("No Menu/");
            }
            theFolder = DataFolder.findFolder(fo);
        }
        this.startLoading(theFolder);
        if (!EventQueue.isDispatchThread()) {
            EventQueue.invokeLater(new Runnable(){

                @Override
                public void run() {
                    MenuBar.this.updateUI();
                }
            });
        }
        if (folder != null) {
            this.getAccessibleContext().setAccessibleDescription(folder.getName());
        }
    }

    @Override
    public boolean isOpaque() {
        if (null != UIManager.get("NbMainWindow.showCustomBackground")) {
            return !UIManager.getBoolean("NbMainWindow.showCustomBackground");
        }
        return super.isOpaque();
    }

    @Override
    public void updateUI() {
        if (EventQueue.isDispatchThread()) {
            super.updateUI();
            boolean GTK = "GTK".equals(UIManager.getLookAndFeel().getID());
            if (!GTK) {
                this.setBorder(BorderFactory.createEmptyBorder());
            }
        }
    }

    @Override
    public int getMenuCount() {
        if (this.menuBarFolder != null && !Thread.holdsLock(this.getTreeLock())) {
            this.menuBarFolder.waitFinished();
        }
        return super.getMenuCount();
    }

    @Override
    public void addImpl(Component c, Object constraint, int idx) {
        if (Utilities.isMac() && Boolean.getBoolean("apple.laf.useScreenMenuBar") && !(c instanceof JMenu)) {
            return;
        }
        super.addImpl(c, constraint, idx);
    }

    public void waitFinished() {
        this.menuBarFolder.instanceFinished();
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(this.menuBarFolder.getFolder());
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        this.startLoading((DataFolder)in.readObject());
    }

    void startLoading(DataFolder folder) {
        this.menuBarFolder = new MenuBarFolder(folder);
    }

    static void allInstances(InstanceCookie[] arr, List<Object> list) {
        Throwable ex = null;
        for (int i = 0; i < arr.length; ++i) {
            Throwable newEx = null;
            try {
                Object o = arr[i].instanceCreate();
                if (o == LazyMenu.SEPARATOR) {
                    o = new JSeparator();
                }
                list.add(o);
            }
            catch (ClassNotFoundException e) {
                newEx = e;
            }
            catch (IOException e) {
                newEx = e;
            }
            if (newEx == null) continue;
            Throwable t = newEx;
            do {
                if (t.getCause() == null) {
                    if (t instanceof ClassNotFoundException) {
                        newEx = new ClassNotFoundException(t.getMessage(), ex);
                        newEx.setStackTrace(t.getStackTrace());
                        break;
                    }
                    t.initCause(ex);
                    break;
                }
                t = t.getCause();
            } while (true);
            ex = newEx;
        }
        if (ex != null) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    static {
        try {
            Class.forName(AcceleratorBinding.class.getName());
        }
        catch (ClassNotFoundException x) {
            throw new ExceptionInInitializerError(x);
        }
        LOG = Logger.getLogger(MenuBar.class.getName());
    }

    private static final class LazySeparator
    extends JSeparator
    implements Runnable {
        @Override
        public void updateUI() {
            if (EventQueue.isDispatchThread()) {
                super.updateUI();
            } else {
                Mutex.EVENT.readAccess((Runnable)this);
            }
        }

        @Override
        public void run() {
            this.updateUI();
        }
    }

    private static class LazyMenu
    extends JMenu
    implements NodeListener,
    Runnable,
    ChangeListener {
        static final JSeparator SEPARATOR = new LazySeparator();
        final DataFolder master;
        final boolean icon;
        final MenuFolder slave;
        final DynaMenuModel dynaModel;
        private boolean selected = false;

        public LazyMenu(DataFolder df, boolean icon) {
            this.master = df;
            this.icon = icon;
            this.dynaModel = new DynaMenuModel();
            this.slave = new MenuFolder();
            this.setName(df.getName());
            FileObject pf = df.getPrimaryFile();
            Object prefix = pf.getAttribute("property-prefix");
            if (prefix instanceof String) {
                Enumeration en = pf.getAttributes();
                while (en.hasMoreElements()) {
                    String attrName = (String)en.nextElement();
                    if (!attrName.startsWith((String)prefix)) continue;
                    this.putClientProperty(attrName.substring(((String)prefix).length()), pf.getAttribute(attrName));
                }
            }
            Node n = this.master.getNodeDelegate();
            n.addNodeListener(NodeOp.weakNodeListener((NodeListener)this, (Object)n));
            Mutex.EVENT.readAccess((Runnable)this);
            this.getModel().addChangeListener(this);
        }

        @Override
        public void updateUI() {
            if (EventQueue.isDispatchThread()) {
                super.updateUI();
            } else {
                Mutex.EVENT.readAccess((Runnable)this);
            }
        }

        @Override
        public int getItemCount() {
            this.conditionalInitialize();
            return super.getItemCount();
        }

        @Override
        public int getMenuComponentCount() {
            this.conditionalInitialize();
            return super.getMenuComponentCount();
        }

        @Override
        public Component[] getMenuComponents() {
            this.conditionalInitialize();
            return super.getMenuComponents();
        }

        private void conditionalInitialize() {
            if (Thread.holdsLock(this.getTreeLock())) {
                return;
            }
            if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
                for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
                    if (!"com.apple.laf.AquaRootPaneUI".equals(ste.getClassName())) continue;
                    if ("windowDeactivated".equals(ste.getMethodName())) {
                        return;
                    }
                    if (!"windowActivated".equals(ste.getMethodName())) continue;
                    return;
                }
            }
            this.doInitialize();
        }

        @Override
        protected boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition, boolean pressed) {
            if (Utilities.isMac()) {
                boolean isAlt;
                int mods = e.getModifiers();
                boolean isCtrl = (mods & 2) != 0;
                boolean bl = isAlt = (mods & 8) != 0;
                if (isAlt && e instanceof MarkedKeyEvent) {
                    mods &= -3;
                    mods &= -129;
                    mods |= 8;
                    MarkedKeyEvent newEvent = new MarkedKeyEvent((Component)e.getSource(), e.getID(), e.getWhen(), mods |= 512, e.getKeyCode(), e.getKeyChar(), e.getKeyLocation());
                    KeyStroke newStroke = null;
                    if (null != ks) {
                        newStroke = e.getID() == 400 ? KeyStroke.getKeyStroke((int)ks.getKeyChar(), mods) : KeyStroke.getKeyStroke(ks.getKeyCode(), mods, !ks.isOnKeyRelease());
                    }
                    boolean result = super.processKeyBinding(newStroke, newEvent, condition, pressed);
                    if (newEvent.isConsumed()) {
                        e.consume();
                    }
                    return result;
                }
                if (!isAlt) {
                    return super.processKeyBinding(ks, e, condition, pressed);
                }
                return false;
            }
            return super.processKeyBinding(ks, e, condition, pressed);
        }

        private void updateProps() {
            assert (EventQueue.isDispatchThread());
            this.getModel().removeChangeListener(this);
            if (this.master.isValid()) {
                Node n = this.master.getNodeDelegate();
                Mnemonics.setLocalizedText((AbstractButton)this, (String)n.getDisplayName());
                if (this.icon) {
                    this.setIcon(new ImageIcon(n.getIcon(1)));
                }
            } else {
                this.setText(this.master.getName());
                this.setIcon(null);
            }
            this.getModel().addChangeListener(this);
        }

        @Override
        public void run() {
            if (this.master == null) {
                return;
            }
            this.updateUI();
            this.updateProps();
        }

        public void propertyChange(PropertyChangeEvent ev) {
            if ("displayName".equals(ev.getPropertyName()) || "name".equals(ev.getPropertyName()) || "icon".equals(ev.getPropertyName())) {
                Mutex.EVENT.readAccess((Runnable)this);
            }
        }

        public void childrenAdded(NodeMemberEvent ev) {
        }

        public void childrenRemoved(NodeMemberEvent ev) {
        }

        public void childrenReordered(NodeReorderEvent ev) {
        }

        public void nodeDestroyed(NodeEvent ev) {
        }

        @Override
        public void stateChanged(ChangeEvent event) {
            if (Utilities.isMac()) {
                if (this.selected) {
                    this.selected = false;
                } else {
                    this.selected = true;
                    this.doInitialize();
                    this.dynaModel.checkSubmenu(this);
                }
            }
        }

        @Override
        public void setPopupMenuVisible(boolean b) {
            boolean isVisible;
            if (!Utilities.isMac() && b != (isVisible = this.isPopupMenuVisible()) && b) {
                this.doInitialize();
                this.dynaModel.checkSubmenu(this);
            }
            super.setPopupMenuVisible(b);
        }

        private void doInitialize() {
            if (this.slave != null) {
                this.slave.waitFinishedSuper();
            }
        }

        static {
            new DynaMenuModel();
        }

        private class MenuFolder
        extends FolderInstance {
            private Map<Object, FileObject> cookiesToFiles;

            public MenuFolder() {
                super(LazyMenu.this.master);
                this.cookiesToFiles = new HashMap<Object, FileObject>();
                DataObjectAccessor.DEFAULT.precreateInstances(this);
            }

            @Override
            public String instanceName() {
                return LazyMenu.class.getName();
            }

            public Class instanceClass() {
                return JMenu.class;
            }

            @Override
            public Object instanceCreate() throws IOException, ClassNotFoundException {
                return LazyMenu.this;
            }

            @Override
            public void waitFinished() {
            }

            void waitFinishedSuper() {
                super.waitFinished();
            }

            @Override
            protected Object instanceForCookie(DataObject obj, InstanceCookie cookie) throws IOException, ClassNotFoundException {
                JSeparator result = cookie.instanceClass().equals(JSeparator.class) ? LazyMenu.SEPARATOR : super.instanceForCookie(obj, cookie);
                this.cookiesToFiles.put(result, obj.getPrimaryFile());
                return result;
            }

            @Override
            protected InstanceCookie acceptCookie(InstanceCookie cookie) throws IOException, ClassNotFoundException {
                Class c = cookie.instanceClass();
                boolean action = Action.class.isAssignableFrom(c);
                if (action) {
                    cookie.instanceCreate();
                }
                boolean is = Presenter.Menu.class.isAssignableFrom(c) || JMenuItem.class.isAssignableFrom(c) || JSeparator.class.isAssignableFrom(c) || action;
                return is ? cookie : null;
            }

            @Override
            protected InstanceCookie acceptFolder(DataFolder df) {
                boolean hasIcon = df.getPrimaryFile().getAttribute("SystemFileSystem.icon") != null;
                return new LazyMenu((DataFolder)df, (boolean)hasIcon).slave;
            }

            @Override
            protected Object createInstance(InstanceCookie[] cookies) throws IOException, ClassNotFoundException {
                LazyMenu m = LazyMenu.this;
                assert (EventQueue.isDispatchThread());
                LinkedList<Object> cInstances = new LinkedList<Object>();
                MenuBar.allInstances(cookies, cInstances);
                if (cInstances.isEmpty()) {
                    JMenuItem item = new JMenuItem(NbBundle.getMessage(DataObject.class, (String)"CTL_EmptyMenu"));
                    item.setEnabled(false);
                    m.add(item);
                }
                m.dynaModel.loadSubmenu(cInstances, m, true, this.cookiesToFiles);
                return m;
            }

            @Override
            protected Task postCreationTask(Runnable run) {
                return new AWTTask(run, this);
            }
        }

    }

    private static final class MarkedKeyEvent
    extends KeyEvent {
        MarkedKeyEvent(Component c, int id, long when, int mods, int code, char kchar, int loc) {
            super(c, id, when, mods, code, kchar, loc);
        }
    }

    private final class MenuBarFolder
    extends FolderInstance {
        private ArrayList<Component> managed;
        private Map<Object, DataObject> cookiesToObjects;

        public MenuBarFolder(DataFolder folder) {
            super(folder);
            this.cookiesToObjects = new HashMap<Object, DataObject>();
            DataObjectAccessor.DEFAULT.precreateInstances(this);
            new DynaMenuModel();
            this.recreate();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void cleanUp() {
            Object object = MenuBar.this.getTreeLock();
            synchronized (object) {
                Iterator<Component> it = this.getManaged().iterator();
                while (it.hasNext()) {
                    MenuBar.this.remove(it.next());
                }
                this.getManaged().clear();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void addComponent(Component c) {
            Object object = MenuBar.this.getTreeLock();
            synchronized (object) {
                MenuBar.this.add(c, this.getManaged().size());
                this.getManaged().add(c);
            }
        }

        @Override
        public String instanceName() {
            return MenuBar.class.getName();
        }

        public Class instanceClass() {
            return MenuBar.class;
        }

        @Override
        protected Object instanceForCookie(DataObject obj, InstanceCookie cookie) throws IOException, ClassNotFoundException {
            Object result = super.instanceForCookie(obj, cookie);
            this.cookiesToObjects.put(result, obj);
            return result;
        }

        @Override
        protected InstanceCookie acceptCookie(InstanceCookie cookie) throws IOException, ClassNotFoundException {
            Class cls = cookie.instanceClass();
            boolean is = Component.class.isAssignableFrom(cls) || Presenter.Toolbar.class.isAssignableFrom(cls) || Action.class.isAssignableFrom(cls);
            return is ? cookie : null;
        }

        @Override
        protected InstanceCookie acceptFolder(DataFolder df) {
            return new LazyMenu((DataFolder)df, (boolean)false).slave;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        protected Object createInstance(InstanceCookie[] cookies) throws IOException, ClassNotFoundException {
            MenuBar mb;
            LinkedList<Object> ll = new LinkedList<Object>();
            MenuBar.allInstances(cookies, ll);
            mb = MenuBar.this;
            if (ll.equals(Arrays.asList(mb.getComponents()))) {
                return mb;
            }
            this.cleanUp();
            try {
                for (Object o : ll) {
                    Component component = this.convertToComponent(o);
                    if (component == null) continue;
                    this.addComponent(component);
                }
            }
            finally {
                this.cookiesToObjects.clear();
            }
            mb.validate();
            mb.repaint();
            return mb;
        }

        private Component convertToComponent(Object obj) {
            Component retVal = null;
            if (obj instanceof Component) {
                retVal = (Component)obj;
            } else if (obj instanceof Presenter.Toolbar) {
                DataObject file = this.cookiesToObjects.get(obj);
                if (obj instanceof Action && file != null) {
                    AcceleratorBinding.setAccelerator((Action)((Action)obj), (FileObject)file.getPrimaryFile());
                }
                retVal = ((Presenter.Toolbar)obj).getToolbarPresenter();
            } else if (obj instanceof Action) {
                Action a = (Action)obj;
                JButton button = new JButton();
                Actions.connect((AbstractButton)button, (Action)a);
                retVal = button;
            }
            if (retVal instanceof JButton) {
                ((JButton)retVal).setBorderPainted(false);
                ((JButton)retVal).setMargin(new Insets(0, 2, 0, 2));
            }
            return retVal;
        }

        DataFolder getFolder() {
            return this.folder;
        }

        @Override
        protected Task postCreationTask(Runnable run) {
            return new AWTTask(run, this);
        }

        private ArrayList<Component> getManaged() {
            assert (Thread.holdsLock(MenuBar.this.getTreeLock()));
            if (this.managed == null) {
                this.managed = new ArrayList();
            }
            return this.managed;
        }
    }

}

