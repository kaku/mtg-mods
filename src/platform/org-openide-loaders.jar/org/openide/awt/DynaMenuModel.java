/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.AcceleratorBinding
 *  org.openide.awt.Actions
 *  org.openide.awt.Actions$MenuItem
 *  org.openide.awt.DynamicMenuContent
 *  org.openide.filesystems.FileObject
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Utilities
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 */
package org.openide.awt;

import java.awt.Component;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.UIManager;
import org.openide.awt.AcceleratorBinding;
import org.openide.awt.Actions;
import org.openide.awt.DynamicMenuContent;
import org.openide.filesystems.FileObject;
import org.openide.util.ImageUtilities;
import org.openide.util.Utilities;
import org.openide.util.actions.Presenter;

class DynaMenuModel {
    private static final Icon BLANK_ICON = new ImageIcon(ImageUtilities.loadImage((String)"org/openide/loaders/empty.gif"));
    private List<JComponent> menuItems;
    private HashMap<DynamicMenuContent, JComponent[]> actionToMenuMap = new HashMap();
    private boolean isWithIcons = false;

    public void loadSubmenu(List<Object> cInstances, JMenu m, boolean remove, Map<Object, FileObject> cookiesToFiles) {
        boolean addSeparator = false;
        Object curIcon = null;
        Iterator<Object> it = cInstances.iterator();
        this.menuItems = new ArrayList<JComponent>(cInstances.size());
        this.actionToMenuMap.clear();
        while (it.hasNext()) {
            FileObject file;
            Object obj = it.next();
            if (obj instanceof Action && (file = cookiesToFiles.get(obj)) != null) {
                AcceleratorBinding.setAccelerator((Action)((Action)obj), (FileObject)file);
            }
            if (obj instanceof Presenter.Menu) {
                obj = ((Presenter.Menu)obj).getMenuPresenter();
            }
            if (obj instanceof DynamicMenuContent) {
                if (addSeparator) {
                    this.menuItems.add(null);
                    addSeparator = false;
                }
                DynamicMenuContent mn = (DynamicMenuContent)obj;
                JComponent[] itms = this.convertArray(mn.getMenuPresenters());
                this.actionToMenuMap.put(mn, itms);
                for (JComponent comp : Arrays.asList(itms)) {
                    this.menuItems.add(comp);
                    this.isWithIcons = this.checkIcon(comp, this.isWithIcons);
                }
                continue;
            }
            if (obj instanceof JMenuItem) {
                if (addSeparator) {
                    this.menuItems.add(null);
                    addSeparator = false;
                }
                this.isWithIcons = this.checkIcon(obj, this.isWithIcons);
                this.menuItems.add((JMenuItem)obj);
                continue;
            }
            if (obj instanceof JSeparator) {
                addSeparator = this.menuItems.size() > 0;
                continue;
            }
            if (!(obj instanceof Action)) continue;
            if (addSeparator) {
                this.menuItems.add(null);
                addSeparator = false;
            }
            Action a = (Action)obj;
            Actions.MenuItem item = new Actions.MenuItem(a, true);
            this.isWithIcons = this.checkIcon((Object)item, this.isWithIcons);
            this.actionToMenuMap.put((DynamicMenuContent)item, new JComponent[]{item});
            this.menuItems.add((JComponent)item);
        }
        if (this.isWithIcons) {
            this.menuItems = this.alignVertically(this.menuItems);
        }
        if (remove) {
            m.removeAll();
        }
        JComponent curItem = null;
        boolean wasSeparator = false;
        Iterator<JComponent> iter = this.menuItems.iterator();
        while (iter.hasNext()) {
            curItem = iter.next();
            if (curItem == null) {
                curItem = DynaMenuModel.createSeparator();
            }
            m.add(curItem);
            boolean isSeparator = curItem instanceof JSeparator;
            if (isSeparator && wasSeparator) {
                curItem.setVisible(false);
            }
            if (curItem instanceof InvisibleMenuItem) continue;
            wasSeparator = isSeparator;
        }
    }

    private boolean checkIcon(Object obj, boolean isWithIconsAlready) {
        if (isWithIconsAlready) {
            return isWithIconsAlready;
        }
        if (obj instanceof JMenuItem && ((JMenuItem)obj).getIcon() != null && !BLANK_ICON.equals(((JMenuItem)obj).getIcon())) {
            return true;
        }
        return false;
    }

    public void checkSubmenu(JMenu menu) {
        boolean oldisWithIcons = this.isWithIcons;
        boolean changed = false;
        for (Map.Entry<DynamicMenuContent, JComponent[]> entry : this.actionToMenuMap.entrySet()) {
            int menuIndex;
            int i;
            DynamicMenuContent pres = entry.getKey();
            JComponent[] old = entry.getValue();
            boolean oldIndex = false;
            Component[] menuones = menu.getPopupMenu().getComponents();
            int n = menuIndex = old.length > 0 ? this.findFirstItemIndex(old[0], menuones) : -1;
            JComponent[] newones = this.convertArray(pres.synchMenuPresenters(this.unconvertArray(old)));
            if (this.compareEqualArrays(old, newones)) continue;
            if (menuIndex < 0) {
                menuIndex = 0;
            } else {
                for (i = 0; i < old.length; ++i) {
                    if (old[i] == null) continue;
                    menu.getPopupMenu().remove(old[i]);
                    this.menuItems.remove(old[i]);
                }
            }
            for (i = 0; i < newones.length; ++i) {
                JComponent one = newones[i];
                menu.getPopupMenu().add((Component)one, i + menuIndex);
                changed = true;
                this.menuItems.add(one);
                boolean thisOneHasIcon = this.checkIcon(one, false);
                if (!thisOneHasIcon && this.isWithIcons) {
                    this.alignVertically(Collections.singletonList(one));
                }
                if (!thisOneHasIcon || this.isWithIcons) continue;
                this.isWithIcons = true;
            }
            entry.setValue(newones);
        }
        boolean hasAnyIcons = false;
        Component[] menuones = menu.getPopupMenu().getComponents();
        for (int i = 0; !(i >= menuones.length || menuones[i] != null && (hasAnyIcons = this.checkIcon(menuones[i], hasAnyIcons))); ++i) {
        }
        DynaMenuModel.checkSeparators(menuones, menu.getPopupMenu());
        if (!hasAnyIcons && this.isWithIcons) {
            this.isWithIcons = false;
        }
        if (oldisWithIcons != this.isWithIcons) {
            this.menuItems = this.alignVertically(this.menuItems);
        }
        if (changed && Utilities.isWindows()) {
            menu.getPopupMenu().revalidate();
        }
    }

    static void checkSeparators(Component[] menuones, JPopupMenu parent) {
        boolean wasSeparator = false;
        for (int i = 0; i < menuones.length; ++i) {
            boolean isVisible;
            Component curItem = menuones[i];
            if (curItem == null) continue;
            boolean isSeparator = curItem instanceof JSeparator;
            if (isSeparator && (isVisible = curItem.isVisible()) != !wasSeparator) {
                parent.remove(i);
                JSeparator newOne = DynaMenuModel.createSeparator();
                newOne.setVisible(!wasSeparator);
                parent.add((Component)newOne, i);
            }
            if (curItem instanceof InvisibleMenuItem) continue;
            wasSeparator = isSeparator;
        }
    }

    private JComponent[] convertArray(JComponent[] arr) {
        if (arr == null || arr.length == 0) {
            return new JComponent[]{new InvisibleMenuItem()};
        }
        JComponent[] toRet = new JComponent[arr.length];
        for (int i = 0; i < arr.length; ++i) {
            toRet[i] = arr[i] == null ? DynaMenuModel.createSeparator() : arr[i];
        }
        return toRet;
    }

    private JComponent[] unconvertArray(JComponent[] arr) {
        if (arr.length == 1 && arr[0] instanceof InvisibleMenuItem) {
            return new JComponent[0];
        }
        return arr;
    }

    private int findFirstItemIndex(JComponent first, Component[] menuItems) {
        for (int i = 0; i < menuItems.length; ++i) {
            if (first != menuItems[i]) continue;
            return i;
        }
        return -1;
    }

    private boolean compareEqualArrays(JComponent[] one, JComponent[] two) {
        if (one.length != two.length) {
            return false;
        }
        for (int i = 0; i < one.length; ++i) {
            if (one[i] == two[i]) continue;
            return false;
        }
        return true;
    }

    private List<JComponent> alignVertically(List<JComponent> menuItems) {
        if (!UIManager.getBoolean("Nb.MenuBar.VerticalAlign")) {
            return menuItems;
        }
        ArrayList<JComponent> result = new ArrayList<JComponent>(menuItems.size());
        JMenuItem curItem = null;
        for (JComponent obj : menuItems) {
            if (obj instanceof JMenuItem) {
                curItem = (JMenuItem)obj;
                if (this.isWithIcons && curItem != null && curItem.getIcon() == null) {
                    curItem.setIcon(BLANK_ICON);
                } else if (!this.isWithIcons && curItem != null) {
                    curItem.setIcon(null);
                }
            }
            result.add(obj);
        }
        return result;
    }

    private static JSeparator createSeparator() {
        JMenu menu = new JMenu();
        menu.addSeparator();
        return (JSeparator)menu.getPopupMenu().getComponent(0);
    }

    static final class InvisibleMenuItem
    extends JMenuItem {
        InvisibleMenuItem() {
        }

        @Override
        public boolean isVisible() {
            return false;
        }
    }

}

