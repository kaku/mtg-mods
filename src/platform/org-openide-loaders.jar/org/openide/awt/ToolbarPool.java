/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.MouseUtils
 *  org.openide.awt.MouseUtils$PopupMouseAdapter
 *  org.openide.cookies.InstanceCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Task
 *  org.openide.util.TaskListener
 */
package org.openide.awt;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleRole;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import org.netbeans.modules.openide.loaders.AWTTask;
import org.netbeans.modules.openide.loaders.DataObjectAccessor;
import org.openide.awt.MouseUtils;
import org.openide.awt.Toolbar;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.FolderInstance;
import org.openide.util.Task;
import org.openide.util.TaskListener;

public final class ToolbarPool
extends JComponent
implements Accessible {
    private static ToolbarPool defaultPool;
    private Folder instance;
    private DataFolder folder;
    private Map<String, Toolbar> toolbars;
    private ArrayList<String> toolbarNames;
    private Map<String, Configuration> toolbarConfigs;
    private String name = "";
    private Component center;
    private PopupListener listener;
    private AccessibleContext toolbarAccessibleContext;
    public static final String DEFAULT_CONFIGURATION = "Standard";
    private static final ThreadLocal<Boolean> DONT_WAIT;
    private TPTaskListener taskListener;
    private int preferredIconSize = 24;
    static final long serialVersionUID = 3420915387298484008L;

    public static synchronized ToolbarPool getDefault() {
        if (defaultPool == null) {
            FileObject root = FileUtil.getConfigRoot();
            FileObject fo = null;
            try {
                fo = FileUtil.createFolder((FileObject)root, (String)"Toolbars");
            }
            catch (IOException ex) {
                Logger.getLogger(ToolbarPool.class.getName()).log(Level.CONFIG, "Cannot create Toolbars folder.", ex);
            }
            if (fo == null) {
                throw new IllegalStateException("No Toolbars/");
            }
            DataFolder folder = DataFolder.findFolder(fo);
            defaultPool = new ToolbarPool(folder);
            ToolbarPool.defaultPool.instance.recreate();
        }
        return defaultPool;
    }

    public ToolbarPool(DataFolder df) {
        this.folder = df;
        this.setLayout(new BorderLayout());
        this.listener = new PopupListener();
        this.toolbars = new TreeMap<String, Toolbar>();
        this.toolbarNames = new ArrayList(20);
        this.toolbarConfigs = new TreeMap<String, Configuration>();
        this.instance = new Folder(df);
        this.getAccessibleContext().setAccessibleName(this.instance.instanceName());
        this.getAccessibleContext().setAccessibleDescription(this.instance.instanceName());
    }

    public int getPreferredIconSize() {
        return this.preferredIconSize;
    }

    public void setPreferredIconSize(int preferredIconSize) throws IllegalArgumentException {
        if (preferredIconSize != 16 && preferredIconSize != 24) {
            throw new IllegalArgumentException("Unsupported argument value:" + preferredIconSize);
        }
        this.preferredIconSize = preferredIconSize;
    }

    public final void waitFinished() {
        this.instance.waitFinished();
    }

    public final boolean isFinished() {
        return this.instance.isFinished();
    }

    void update(Map<String, Toolbar> toolbars, Map<String, Configuration> conf, ArrayList<String> toolbarNames) {
        this.toolbars = toolbars;
        this.toolbarNames = new ArrayList<String>(toolbarNames);
        this.toolbarConfigs = conf;
        if (!"".equals(this.name)) {
            this.setConfiguration(this.name);
        }
    }

    private synchronized void updateDefault() {
        Toolbar[] bars = this.getToolbarsNow();
        this.name = "";
        if (bars.length == 1) {
            this.revalidate((Component)((Object)bars[0]));
        } else {
            JPanel tp = new JPanel(new FlowLayout(0, 0, 0));
            for (int i = 0; i < bars.length; ++i) {
                tp.add((Component)((Object)bars[i]));
            }
            this.revalidate(tp);
        }
    }

    private synchronized void activate(Configuration c) {
        Component comp = c.activate();
        this.name = c.getName();
        this.revalidate(comp);
    }

    @Deprecated
    public void setToolbarsListener(Toolbar.DnDListener l) {
        for (Toolbar t : this.toolbars.values()) {
            t.setDnDListener(l);
        }
    }

    private void revalidate(Component c) {
        if (c != this.center) {
            if (this.center != null) {
                this.remove(this.center);
                this.center.removeMouseListener((MouseListener)((Object)this.listener));
            }
            this.center = c;
            this.add(this.center, "Center");
            this.center.addMouseListener((MouseListener)((Object)this.listener));
            this.invalidate();
            this.revalidate();
            this.repaint();
        }
    }

    public final Toolbar findToolbar(String name) {
        return this.toolbars.get(name);
    }

    public final String getConfiguration() {
        return this.name;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final void setConfiguration(String n) {
        Boolean prev = DONT_WAIT.get();
        try {
            DONT_WAIT.set(true);
            this.setConfigurationNow(n);
        }
        finally {
            DONT_WAIT.set(prev);
        }
    }

    private void setConfigurationNow(String n) {
        String old = this.name;
        if (!this.instance.isFinished()) {
            if (this.taskListener == null) {
                this.taskListener = new TPTaskListener();
                this.instance.addTaskListener((TaskListener)this.taskListener);
            }
            this.taskListener.setConfiguration(n);
            return;
        }
        if (this.taskListener != null) {
            this.instance.removeTaskListener((TaskListener)this.taskListener);
            this.taskListener = null;
        }
        Configuration config = null;
        if (n != null) {
            config = this.toolbarConfigs.get(n);
        }
        if (config != null) {
            this.activate(config);
        } else if (this.toolbarConfigs.isEmpty()) {
            this.updateDefault();
        } else {
            config = this.toolbarConfigs.get("Standard");
            if (config == null) {
                config = this.toolbarConfigs.values().iterator().next();
            }
            this.activate(config);
        }
        this.firePropertyChange("configuration", old, this.name);
    }

    public final DataFolder getFolder() {
        return this.folder;
    }

    public final Toolbar[] getToolbars() {
        if (!Boolean.TRUE.equals(DONT_WAIT.get())) {
            this.waitFinished();
        }
        return this.getToolbarsNow();
    }

    final synchronized Toolbar[] getToolbarsNow() {
        Toolbar[] arr = new Toolbar[this.toolbarNames.size()];
        int index = 0;
        for (String tn : this.toolbarNames) {
            arr[index++] = this.findToolbar(tn);
        }
        return arr;
    }

    public final String[] getConfigurations() {
        if (!Boolean.TRUE.equals(DONT_WAIT.get())) {
            this.waitFinished();
        }
        return this.getConfigurationsNow();
    }

    final synchronized String[] getConfigurationsNow() {
        ArrayList<String> list = new ArrayList<String>(this.toolbarConfigs.keySet());
        Collections.sort(list);
        String[] arr = new String[list.size()];
        return list.toArray(arr);
    }

    @Override
    public AccessibleContext getAccessibleContext() {
        if (this.toolbarAccessibleContext == null) {
            this.toolbarAccessibleContext = new JComponent.AccessibleJComponent(){

                @Override
                public AccessibleRole getAccessibleRole() {
                    return AccessibleRole.TOOL_BAR;
                }
            };
        }
        return this.toolbarAccessibleContext;
    }

    static {
        DONT_WAIT = new ThreadLocal();
    }

    private static final class ComponentConfiguration
    extends JPopupMenu
    implements Configuration,
    ActionListener {
        private Component comp;
        static final long serialVersionUID = -409474484612485719L;

        ComponentConfiguration() {
        }

        public ComponentConfiguration(Component comp) {
            this.comp = comp;
        }

        @Override
        public Component activate() {
            return this.comp;
        }

        @Override
        public String getName() {
            if (null == this.comp) {
                return super.getName();
            }
            return this.comp.getName();
        }

        @Override
        public JPopupMenu getContextMenu() {
            this.removeAll();
            ButtonGroup bg = new ButtonGroup();
            String current = ToolbarPool.getDefault().getConfiguration();
            String[] arr$ = ToolbarPool.getDefault().getConfigurationsNow();
            int len$ = arr$.length;
            for (int i$ = 0; i$ < len$; ++i$) {
                String name;
                JRadioButtonMenuItem mi = new JRadioButtonMenuItem(name, (name = arr$[i$]).compareTo(current) == 0);
                mi.addActionListener(this);
                bg.add(mi);
                this.add(mi);
            }
            return this;
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            ToolbarPool.getDefault().setConfiguration(evt.getActionCommand());
        }
    }

    public static interface Configuration {
        public Component activate();

        public String getName();

        public JPopupMenu getContextMenu();
    }

    private class PopupListener
    extends MouseUtils.PopupMouseAdapter {
        PopupListener() {
        }

        protected void showPopup(MouseEvent e) {
            Configuration conf = (Configuration)ToolbarPool.this.toolbarConfigs.get(ToolbarPool.this.name);
            if (conf != null) {
                JPopupMenu pop = conf.getContextMenu();
                pop.show(e.getComponent(), e.getX(), e.getY());
            }
        }
    }

    private class Folder
    extends FolderInstance {
        public Folder(DataFolder f) {
            super(f);
            DataObjectAccessor.DEFAULT.precreateInstances(this);
        }

        @Override
        public String instanceName() {
            return this.instanceClass().getName();
        }

        public Class instanceClass() {
            return ToolbarPool.class;
        }

        @Override
        protected InstanceCookie acceptCookie(InstanceCookie cookie) throws IOException, ClassNotFoundException {
            Class cls = cookie.instanceClass();
            if (Configuration.class.isAssignableFrom(cls)) {
                return cookie;
            }
            if (Component.class.isAssignableFrom(cls)) {
                return cookie;
            }
            return null;
        }

        @Override
        protected InstanceCookie acceptFolder(DataFolder df) {
            Toolbar res = new Toolbar(df);
            FileObject fo = df.getPrimaryFile();
            Object disable = fo.getAttribute("nb.toolbar.overflow.disable");
            if (Boolean.TRUE.equals(disable)) {
                res.putClientProperty((Object)"nb.toolbar.overflow.disable", (Object)Boolean.TRUE);
            }
            return res.waitFinished();
        }

        @Override
        protected Object createInstance(InstanceCookie[] cookies) throws IOException, ClassNotFoundException {
            assert (EventQueue.isDispatchThread());
            int length = cookies.length;
            TreeMap<String, Toolbar> toolbars = new TreeMap<String, Toolbar>();
            ArrayList<String> toolbarNames = new ArrayList<String>();
            TreeMap<String, Configuration> conf = new TreeMap<String, Configuration>();
            for (int i = 0; i < length; ++i) {
                try {
                    String name;
                    Object obj = cookies[i].instanceCreate();
                    if (obj instanceof Toolbar) {
                        Toolbar toolbar = (Toolbar)((Object)obj);
                        toolbar.removeMouseListener((MouseListener)((Object)ToolbarPool.this.listener));
                        toolbar.addMouseListener((MouseListener)((Object)ToolbarPool.this.listener));
                        toolbars.put(toolbar.getName(), toolbar);
                        toolbarNames.add(toolbar.getName());
                        continue;
                    }
                    if (obj instanceof Configuration) {
                        Configuration config = (Configuration)obj;
                        name = config.getName();
                        if (name == null) {
                            name = cookies[i].instanceName();
                        }
                        conf.put(name, config);
                        continue;
                    }
                    if (!(obj instanceof Component)) continue;
                    Component comp = (Component)obj;
                    name = comp.getName();
                    if (name == null) {
                        name = cookies[i].instanceName();
                    }
                    conf.put(name, new ComponentConfiguration(comp));
                    continue;
                }
                catch (IOException ex) {
                    Logger.getLogger(ToolbarPool.class.getName()).log(Level.INFO, "Error while creating toolbars.", ex);
                    continue;
                }
                catch (ClassNotFoundException ex) {
                    Logger.getLogger(ToolbarPool.class.getName()).log(Level.INFO, "Error while creating toolbars.", ex);
                }
            }
            ToolbarPool.this.update(toolbars, conf, toolbarNames);
            return ToolbarPool.this;
        }

        @Override
        protected Task postCreationTask(Runnable run) {
            return new AWTTask(run, this);
        }
    }

    private class TPTaskListener
    implements TaskListener {
        private String conf;

        TPTaskListener() {
        }

        public void taskFinished(Task task) {
            ToolbarPool.this.setConfiguration(this.conf);
            this.conf = null;
        }

        void setConfiguration(String conf) {
            if (this.conf == null) {
                this.conf = conf;
            }
        }
    }

}

