/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.templates;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value={ElementType.TYPE, ElementType.METHOD, ElementType.PACKAGE})
@Retention(value=RetentionPolicy.SOURCE)
public @interface TemplateRegistration {
    public String folder();

    public int position() default Integer.MAX_VALUE;

    public String id() default "";

    public String[] content() default {};

    public String displayName() default "";

    public String iconBase() default "";

    public String description() default "";

    public String scriptEngine() default "";

    public String[] category() default {};

    public boolean requireProject() default 1;

    public String targetName() default "";
}

