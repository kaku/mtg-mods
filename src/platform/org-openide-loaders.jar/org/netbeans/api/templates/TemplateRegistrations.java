/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.templates;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.netbeans.api.templates.TemplateRegistration;

@Target(value={ElementType.TYPE, ElementType.METHOD, ElementType.PACKAGE})
@Retention(value=RetentionPolicy.SOURCE)
public @interface TemplateRegistrations {
    public TemplateRegistration[] value();
}

