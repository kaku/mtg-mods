/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.queries.FileEncodingQuery
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.text.IndentEngine
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.templates;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Map;
import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;
import org.netbeans.api.queries.FileEncodingQuery;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.CreateFromTemplateHandler;
import org.openide.text.IndentEngine;
import org.openide.util.Lookup;

public class ScriptingCreateFromTemplateHandler
extends CreateFromTemplateHandler {
    public static final String SCRIPT_ENGINE_ATTR = "javax.script.ScriptEngine";
    private static ScriptEngineManager manager;
    private static final String ENCODING_PROPERTY_NAME = "encoding";

    @Override
    protected boolean accept(FileObject orig) {
        return ScriptingCreateFromTemplateHandler.engine(orig) != null;
    }

    @Override
    protected FileObject createFromTemplate(FileObject template, FileObject f, String name, Map<String, Object> values) throws IOException {
        boolean noExt;
        String extWithDot;
        boolean bl = noExt = Boolean.TRUE.equals(values.get("freeFileExtension")) && name.indexOf(46) != -1;
        if (noExt) {
            extWithDot = null;
        } else {
            extWithDot = "" + '.' + template.getExt();
            if (name.endsWith(extWithDot)) {
                name = name.substring(0, name.length() - extWithDot.length());
            }
        }
        String nameUniq = FileUtil.findFreeFileName((FileObject)f, (String)name, (String)(noExt ? null : template.getExt()));
        FileObject output = FileUtil.createData((FileObject)f, (String)(noExt ? nameUniq : nameUniq + extWithDot));
        Charset targetEnc = FileEncodingQuery.getEncoding((FileObject)output);
        Charset sourceEnc = FileEncodingQuery.getEncoding((FileObject)template);
        ScriptEngine eng = ScriptingCreateFromTemplateHandler.engine(template);
        Bindings bind = eng.getContext().getBindings(100);
        bind.putAll(values);
        if (!values.containsKey("encoding")) {
            bind.put("encoding", (Object)targetEnc.name());
        }
        Writer w = null;
        Reader is = null;
        try {
            w = new OutputStreamWriter(output.getOutputStream(), targetEnc);
            IndentEngine format = IndentEngine.find((String)template.getMIMEType());
            if (format != null) {
                PlainDocument doc = new PlainDocument();
                doc.putProperty("stream", (Object)template);
                w = format.createWriter((Document)doc, 0, w);
            }
            eng.getContext().setWriter(new PrintWriter(w));
            eng.getContext().setAttribute(FileObject.class.getName(), (Object)template, 100);
            eng.getContext().setAttribute("javax.script.filename", template.getNameExt(), 100);
            is = new InputStreamReader(template.getInputStream(), sourceEnc);
            eng.eval(is);
        }
        catch (ScriptException ex) {
            IOException io = new IOException(ex.getMessage(), ex);
            throw io;
        }
        finally {
            if (w != null) {
                w.close();
            }
            if (is != null) {
                is.close();
            }
        }
        return output;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static ScriptEngine engine(FileObject fo) {
        Object obj = fo.getAttribute("javax.script.ScriptEngine");
        if (obj instanceof ScriptEngine) {
            return (ScriptEngine)obj;
        }
        if (obj instanceof String) {
            Class<ScriptingCreateFromTemplateHandler> class_ = ScriptingCreateFromTemplateHandler.class;
            synchronized (ScriptingCreateFromTemplateHandler.class) {
                if (manager == null) {
                    ClassLoader loader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
                    manager = new ScriptEngineManager(loader != null ? loader : Thread.currentThread().getContextClassLoader());
                }
                // ** MonitorExit[var2_2] (shouldn't be in output)
                return manager.getEngineByName((String)obj);
            }
        }
        return null;
    }
}

