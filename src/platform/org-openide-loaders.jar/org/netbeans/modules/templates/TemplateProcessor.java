/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$InstantiatingIterator
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 */
package org.netbeans.modules.templates;

import java.lang.annotation.Annotation;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.tools.FileObject;
import org.netbeans.api.templates.TemplateRegistration;
import org.netbeans.api.templates.TemplateRegistrations;
import org.openide.WizardDescriptor;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;

@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
public class TemplateProcessor
extends LayerGeneratingProcessor {
    public Set<String> getSupportedAnnotationTypes() {
        return new HashSet<String>(Arrays.asList(TemplateRegistration.class.getCanonicalName(), TemplateRegistrations.class.getCanonicalName()));
    }

    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        if (roundEnv.processingOver()) {
            return false;
        }
        for (Element e2 : roundEnv.getElementsAnnotatedWith(TemplateRegistration.class)) {
            TemplateRegistration r = e2.getAnnotation(TemplateRegistration.class);
            if (r == null) continue;
            this.process(e2, r);
        }
        for (Element e : roundEnv.getElementsAnnotatedWith(TemplateRegistrations.class)) {
            TemplateRegistrations rr = e.getAnnotation(TemplateRegistrations.class);
            if (rr == null) continue;
            for (TemplateRegistration t : rr.value()) {
                this.process(e, t);
            }
        }
        return true;
    }

    private void process(Element e, TemplateRegistration t) throws LayerGenerationException {
        String basename;
        LayerBuilder builder = this.layer(new Element[]{e});
        if (!t.id().isEmpty()) {
            if (t.content().length > 0) {
                throw new LayerGenerationException("Cannot specify both id and content", e, this.processingEnv, (Annotation)t);
            }
            basename = t.id();
        } else if (t.content().length > 0) {
            basename = TemplateProcessor.basename(t.content()[0]);
        } else if (e.getKind() == ElementKind.CLASS) {
            basename = ((TypeElement)e).getQualifiedName().toString().replace('.', '-');
        } else if (e.getKind() == ElementKind.METHOD) {
            basename = ((TypeElement)e.getEnclosingElement()).getQualifiedName().toString().replace('.', '-') + '-' + e.getSimpleName();
        } else {
            throw new LayerGenerationException("cannot use @Template on a package without specifying content", e, this.processingEnv, (Annotation)t);
        }
        String folder = "Templates/" + t.folder() + '/';
        LayerBuilder.File f = builder.file(folder + basename);
        f.boolvalue("template", true);
        f.position(t.position());
        if (!t.displayName().isEmpty()) {
            f.bundlevalue("displayName", t.displayName());
        }
        if (!t.iconBase().isEmpty()) {
            builder.validateResource(t.iconBase(), e, (Annotation)t, "iconBase", true);
            f.stringvalue("iconBase", t.iconBase());
        } else if (t.content().length == 0) {
            throw new LayerGenerationException("Must specify iconBase if content is not specified", e, this.processingEnv, (Annotation)t);
        }
        if (!t.description().isEmpty()) {
            f.urlvalue("instantiatingWizardURL", this.contentURI(e, t.description(), builder, t, "description"));
        }
        if (e.getKind() != ElementKind.PACKAGE) {
            f.instanceAttribute("instantiatingIterator", WizardDescriptor.InstantiatingIterator.class);
        }
        if (t.content().length > 0) {
            f.url(this.contentURI(e, t.content()[0], builder, t, "content").toString());
            for (int i = 1; i < t.content().length; ++i) {
                builder.file(folder + TemplateProcessor.basename(t.content()[i])).url(this.contentURI(e, t.content()[i], builder, t, "content").toString()).position(0).write();
            }
        }
        if (!t.scriptEngine().isEmpty()) {
            f.stringvalue("javax.script.ScriptEngine", t.scriptEngine());
        }
        if (t.category().length > 0) {
            StringBuilder sb = new StringBuilder();
            for (String c : t.category()) {
                if (sb.length() > 0) {
                    sb.append(',');
                }
                sb.append(c);
            }
            f.stringvalue("templateCategory", sb.toString());
        }
        f.boolvalue("requireProject", t.requireProject());
        if (!t.targetName().trim().isEmpty()) {
            f.bundlevalue("targetName", t.targetName());
        }
        f.write();
    }

    private static String basename(String relativeResource) {
        return relativeResource.replaceFirst(".+/", "").replaceFirst("[.]template$", "");
    }

    private URI contentURI(Element e, String relativePath, LayerBuilder builder, TemplateRegistration t, String annotationMethod) throws LayerGenerationException {
        String path = LayerBuilder.absolutizeResource((Element)e, (String)relativePath);
        builder.validateResource(path, e, (Annotation)t, annotationMethod, false);
        try {
            return new URI("nbresloc", "/" + path, null).normalize();
        }
        catch (URISyntaxException x) {
            throw new LayerGenerationException("could not translate " + path, e, this.processingEnv, (Annotation)t);
        }
    }
}

