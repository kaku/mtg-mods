/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.InstanceCookie
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileUtil
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Template
 *  org.openide.util.WeakListeners
 *  org.openide.util.lookup.ProxyLookup
 *  org.openide.xml.EntityCatalog
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.modules.openide.loaders;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.loaders.Environment;
import org.openide.loaders.InstanceDataObject;
import org.openide.loaders.XMLDataObject;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.WeakListeners;
import org.openide.util.lookup.ProxyLookup;
import org.openide.xml.EntityCatalog;
import org.openide.xml.XMLUtil;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.xml.sax.ContentHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.DefaultHandler;

public final class FileEntityResolver
extends EntityCatalog
implements Environment.Provider {
    private static final String ENTITY_PREFIX = "/xml/entities";
    private static final String LOOKUP_PREFIX = "/xml/lookups";
    static final Logger ERR = Logger.getLogger(FileEntityResolver.class.getName());
    private static Method method;
    private static final StopSaxException STOP;

    public InputSource resolveEntity(String publicID, String systemID) throws IOException, SAXException {
        if (publicID == null) {
            return null;
        }
        String id = FileEntityResolver.convertPublicId(publicID);
        StringBuffer sb = new StringBuffer(200);
        sb.append("/xml/entities");
        sb.append(id);
        FileObject fo = FileUtil.getConfigFile((String)sb.toString());
        if (fo != null) {
            InputSource in = new InputSource(fo.getInputStream());
            try {
                Object myPublicID = fo.getAttribute("hint.originalPublicID");
                if (myPublicID instanceof String) {
                    in.setPublicId((String)myPublicID);
                }
                URL url = fo.getURL();
                in.setSystemId(url.toString());
            }
            catch (IOException ex) {
                // empty catch block
            }
            return in;
        }
        return null;
    }

    @Override
    public Lookup getEnvironment(DataObject obj) {
        if (obj instanceof XMLDataObject) {
            XMLDataObject xml = (XMLDataObject)obj;
            String id = null;
            try {
                DocumentType domDTD = xml.getDocument().getDoctype();
                if (domDTD != null) {
                    id = domDTD.getPublicId();
                }
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                return null;
            }
            catch (SAXException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                return null;
            }
            if (id == null) {
                return null;
            }
            id = FileEntityResolver.convertPublicId(id);
            return new Lkp(id, xml);
        }
        if (obj instanceof InstanceDataObject) {
            return this.getEnvForIDO((InstanceDataObject)obj);
        }
        return null;
    }

    private Lookup getEnvForIDO(InstanceDataObject ido) {
        DTDParser parser = new DTDParser(ido.getPrimaryFile());
        parser.parse();
        String id = parser.getPublicId();
        if (id == null) {
            return null;
        }
        id = FileEntityResolver.convertPublicId(id);
        return new Lkp(id, ido);
    }

    private static Lookup findLookup(DataObject obj, DataObject source) {
        if (source == null) {
            return null;
        }
        try {
            InstanceCookie cookie = source.getCookie(InstanceCookie.class);
            if (cookie != null) {
                Object inst = cookie.instanceCreate();
                if (inst instanceof Environment.Provider) {
                    return ((Environment.Provider)inst).getEnvironment(obj);
                }
                if (!(obj instanceof XMLDataObject)) {
                    return null;
                }
                if (inst instanceof XMLDataObject.Processor) {
                    XMLDataObject.Info info = new XMLDataObject.Info();
                    info.addProcessorClass(inst.getClass());
                    inst = info;
                }
                if (inst instanceof XMLDataObject.Info) {
                    return FileEntityResolver.createInfoLookup((XMLDataObject)obj, (XMLDataObject.Info)inst);
                }
            }
        }
        catch (IOException ex) {
            ERR.log(Level.INFO, "no environment for " + obj, ex);
        }
        catch (ClassNotFoundException ex) {
            ERR.log(Level.INFO, "no environment for " + obj, ex);
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static Lookup createInfoLookup(XMLDataObject obj, XMLDataObject.Info info) {
        Class<FileEntityResolver> class_ = FileEntityResolver.class;
        synchronized (FileEntityResolver.class) {
            if (method == null) {
                try {
                    Method m = XMLDataObject.class.getDeclaredMethod("createInfoLookup", XMLDataObject.class, XMLDataObject.Info.class);
                    m.setAccessible(true);
                    method = m;
                }
                catch (Exception ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                    // ** MonitorExit[var2_2] (shouldn't be in output)
                    return null;
                }
            }
            // ** MonitorExit[var2_2] (shouldn't be in output)
            try {
                return (Lookup)method.invoke(null, obj, info);
            }
            catch (Exception ex) {
                Exceptions.printStackTrace((Throwable)ex);
                return null;
            }
        }
    }

    /*
     * Unable to fully structure code
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private static String convertPublicId(String publicID) {
        arr = publicID.toCharArray();
        numberofslashes = 0;
        state = 0;
        write = 0;
        i = 0;
        while (i < arr.length) {
            ch = arr[i];
            switch (state) {
                case 0: {
                    if (ch == 43 || ch == 45 || ch == 73 || ch == 83 || ch == 79) break;
                    state = 1;
                }
                case 1: {
                    if (ch == 47) {
                        state = 2;
                        if (++numberofslashes == 3) return new String(arr, 0, write);
                        arr[write++] = 47;
                        break;
                    }
                    ** GOTO lbl-1000
                }
                case 2: {
                    if (ch == 47) break;
                    state = 1;
                }
                default: lbl-1000: // 2 sources:
                {
                    arr[write++] = ch >= 65 && ch <= 90 || ch >= 97 && ch <= 122 || ch >= 48 && ch <= 57 ? ch : 95;
                }
            }
            ++i;
        }
        return new String(arr, 0, write);
    }

    private static FileObject findObject(String id, FileObject[] last) {
        StringBuffer sb = new StringBuffer(200);
        sb.append("/xml/lookups");
        sb.append(id);
        int len = sb.length();
        sb.append(".instance");
        String toSearch1 = sb.toString();
        int indx = FileEntityResolver.searchFolder(FileUtil.getConfigRoot(), toSearch1, last);
        if (indx == -1) {
            return null;
        }
        FileObject fo = last[0].getFileObject(toSearch1.substring(indx));
        if (fo == null) {
            sb.setLength(len);
            sb.append(".xml");
            fo = last[0].getFileObject(sb.toString().substring(indx));
        }
        return fo;
    }

    private static int searchFolder(FileObject fo, String resourceName, FileObject[] last) {
        int pos = 0;
        do {
            int next;
            if ((next = resourceName.indexOf(47, pos)) == -1) {
                last[0] = fo;
                return pos;
            }
            if (next == pos) {
                ++pos;
                continue;
            }
            FileObject nf = fo.getFileObject(resourceName.substring(pos, next));
            if (nf == null) {
                last[0] = fo;
                return -1;
            }
            pos = next + 1;
            fo = nf;
        } while (true);
    }

    static {
        STOP = new StopSaxException();
    }

    private static final class Lkp
    extends ProxyLookup
    implements PropertyChangeListener,
    FileChangeListener {
        private String id;
        private Reference<DataObject> xml;
        private volatile FileObject folder;
        private volatile DataObject obj;

        public Lkp(String id, DataObject xml) {
            super(new Lookup[0]);
            this.id = id;
            this.xml = new WeakReference<DataObject>(xml);
        }

        protected void beforeLookup(Lookup.Template t) {
            if (FileEntityResolver.ERR.isLoggable(Level.FINE)) {
                FileEntityResolver.ERR.fine("beforeLookup: " + t.getType() + " for " + this.getXml());
            }
            if (this.folder == null && this.obj == null) {
                this.update();
            }
        }

        private void update() {
            if (FileEntityResolver.ERR.isLoggable(Level.FINE)) {
                FileEntityResolver.ERR.fine("update: " + this.id + " for " + this.getXml());
            }
            FileObject[] last = new FileObject[1];
            FileObject fo = FileEntityResolver.findObject(this.id, last);
            if (FileEntityResolver.ERR.isLoggable(Level.FINE)) {
                FileEntityResolver.ERR.fine("fo: " + (Object)fo + " for " + this.getXml());
            }
            DataObject o = null;
            if (fo != null) {
                try {
                    o = DataObject.find(fo);
                    if (FileEntityResolver.ERR.isLoggable(Level.FINE)) {
                        FileEntityResolver.ERR.fine("object found: " + o + " for " + this.getXml());
                    }
                }
                catch (DataObjectNotFoundException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            if (o == this.obj) {
                if (FileEntityResolver.ERR.isLoggable(Level.FINE)) {
                    FileEntityResolver.ERR.fine("same data object for " + this.getXml());
                }
                Lookup l = FileEntityResolver.findLookup(this.getXml(), o);
                if (o != null && l != null) {
                    if (FileEntityResolver.ERR.isLoggable(Level.FINE)) {
                        FileEntityResolver.ERR.fine("updating lookups for " + this.getXml());
                    }
                    this.setLookups(new Lookup[]{l});
                    if (FileEntityResolver.ERR.isLoggable(Level.FINE)) {
                        FileEntityResolver.ERR.fine("updating lookups done for " + this.getXml());
                    }
                    return;
                }
            } else {
                Lookup l = FileEntityResolver.findLookup(this.getXml(), o);
                if (o != null && l != null) {
                    if (FileEntityResolver.ERR.isLoggable(Level.FINE)) {
                        FileEntityResolver.ERR.fine("change the lookup");
                    }
                    o.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)o));
                    this.obj = o;
                    this.setLookups(new Lookup[]{l});
                    if (FileEntityResolver.ERR.isLoggable(Level.FINE)) {
                        FileEntityResolver.ERR.fine("change in lookup done for " + this.getXml());
                    }
                    if (FileEntityResolver.ERR.isLoggable(Level.FINE)) {
                        FileEntityResolver.ERR.fine("data object updated to " + this.obj + " for " + this.getXml());
                    }
                    return;
                }
                this.obj = o;
                if (FileEntityResolver.ERR.isLoggable(Level.FINE)) {
                    FileEntityResolver.ERR.fine("data object updated to " + this.obj + " for " + this.getXml());
                }
            }
            if (FileEntityResolver.ERR.isLoggable(Level.FINE)) {
                FileEntityResolver.ERR.fine("delegating to nobody for " + this.obj + " for " + this.getXml());
            }
            this.setLookups(new Lookup[0]);
            if (this.folder != last[0]) {
                this.folder = last[0];
                last[0].addFileChangeListener(FileUtil.weakFileChangeListener((FileChangeListener)this, (Object)last[0]));
            }
        }

        public void fileDeleted(FileEvent fe) {
            this.update();
        }

        public void fileFolderCreated(FileEvent fe) {
            this.update();
        }

        public void fileDataCreated(FileEvent fe) {
            this.update();
        }

        public void fileAttributeChanged(FileAttributeEvent fe) {
        }

        @Override
        public void propertyChange(PropertyChangeEvent ev) {
            String name = ev.getPropertyName();
            if ("cookie".equals(name) || "name".equals(name) || "valid".equals(name) || "primaryFile".equals(name)) {
                this.update();
            }
        }

        public void fileRenamed(FileRenameEvent fe) {
            this.update();
        }

        public void fileChanged(FileEvent fe) {
        }

        private DataObject getXml() {
            return this.xml.get();
        }
    }

    private static class DTDParser
    extends DefaultHandler
    implements LexicalHandler {
        private String publicId = null;
        private FileObject src;

        public DTDParser(FileObject src) {
            this.src = src;
        }

        public String getPublicId() {
            return this.publicId;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void parse() {
            InputStream in = null;
            try {
                XMLReader reader = XMLUtil.createXMLReader((boolean)false, (boolean)false);
                reader.setContentHandler(this);
                reader.setErrorHandler(this);
                reader.setEntityResolver(this);
                in = new BufferedInputStream(this.src.getInputStream());
                InputSource is = new InputSource(in);
                try {
                    reader.setFeature("http://xml.org/sax/features/validation", false);
                }
                catch (SAXException sex) {
                    FileEntityResolver.ERR.warning("XML parser does not support validation feature.");
                }
                try {
                    reader.setProperty("http://xml.org/sax/properties/lexical-handler", this);
                }
                catch (SAXException sex) {
                    FileEntityResolver.ERR.warning("XML parser does not support lexical-handler feature.");
                }
                reader.parse(is);
            }
            catch (StopSaxException ex) {
                FileEntityResolver.ERR.log(Level.FINE, null, ex);
            }
            catch (Exception ex) {
                if ("org.openide.util.lookup.AbstractLookup$ISE".equals(ex.getClass().getName())) {
                    throw (IllegalStateException)ex;
                }
                try {
                    if (this.src.getFileSystem().isDefault() && !this.src.getPath().startsWith("Windows2Local")) {
                        FileEntityResolver.ERR.log(Level.WARNING, null, new IOException("Parsing " + (Object)this.src + ": " + ex.getMessage()).initCause(ex));
                    }
                }
                catch (FileStateInvalidException fie) {
                    // empty catch block
                }
            }
            finally {
                try {
                    if (in != null) {
                        in.close();
                    }
                }
                catch (IOException exc) {
                    FileEntityResolver.ERR.log(Level.WARNING, "Closing stream for " + (Object)this.src, exc);
                }
            }
        }

        @Override
        public InputSource resolveEntity(String publicId, String systemID) {
            InputSource ret = new InputSource(new StringReader(""));
            ret.setSystemId("StringReader");
            return ret;
        }

        @Override
        public void endDTD() throws SAXException {
            throw STOP;
        }

        @Override
        public void startDTD(String name, String publicId, String systemId) throws SAXException {
            this.publicId = publicId;
        }

        @Override
        public void startEntity(String str) throws SAXException {
        }

        @Override
        public void endEntity(String str) throws SAXException {
        }

        @Override
        public void comment(char[] values, int param, int param2) throws SAXException {
        }

        @Override
        public void startCDATA() throws SAXException {
        }

        @Override
        public void endCDATA() throws SAXException {
        }
    }

    private static class StopSaxException
    extends SAXException {
        public StopSaxException() {
            super("STOP");
        }
    }

}

