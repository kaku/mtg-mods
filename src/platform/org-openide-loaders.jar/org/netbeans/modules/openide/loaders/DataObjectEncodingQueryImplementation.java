/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.spi.queries.FileEncodingQueryImplementation
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.openide.loaders;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.spi.queries.FileEncodingQueryImplementation;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Lookup;

public class DataObjectEncodingQueryImplementation
extends FileEncodingQueryImplementation {
    private static ThreadLocal<DataFolder> TARGET = new ThreadLocal();
    private static final Logger LOG = Logger.getLogger(DataObjectEncodingQueryImplementation.class.getName());
    private static final Map<String, Boolean> MIME_TYPE_CHECK_MAP = new HashMap<String, Boolean>();

    public static DataFolder enterIgnoreTargetFolder(DataFolder df) {
        DataFolder prev = TARGET.get();
        TARGET.set(df);
        return prev;
    }

    public static void exitIgnoreTargetFolder(DataFolder prev) {
        TARGET.set(prev);
    }

    public Charset getEncoding(FileObject file) {
        Charset charset;
        assert (file != null);
        DataFolder df = TARGET.get();
        String mimeType = file.getMIMEType();
        FileEncodingQueryImplementation impl = (FileEncodingQueryImplementation)MimeLookup.getLookup((String)mimeType).lookup(FileEncodingQueryImplementation.class);
        if (impl != null && (charset = impl.getEncoding(file)) != null) {
            return charset;
        }
        if (df != null && df.getPrimaryFile().equals((Object)file.getParent())) {
            return null;
        }
        Boolean useDataObjectLookup = MIME_TYPE_CHECK_MAP.get(mimeType);
        if (useDataObjectLookup == null || useDataObjectLookup.booleanValue() || "content/unknown".equals(mimeType)) {
            DataObject dobj;
            try {
                dobj = DataObject.find(file);
            }
            catch (DataObjectNotFoundException ex) {
                LOG.warning("Invalid DataObject: " + FileUtil.getFileDisplayName((FileObject)file));
                return null;
            }
            impl = (FileEncodingQueryImplementation)dobj.getLookup().lookup(FileEncodingQueryImplementation.class);
            if (impl != null) {
                MIME_TYPE_CHECK_MAP.put(mimeType, Boolean.TRUE);
                return impl.getEncoding(file);
            }
            MIME_TYPE_CHECK_MAP.put(mimeType, Boolean.FALSE);
        }
        return null;
    }
}

