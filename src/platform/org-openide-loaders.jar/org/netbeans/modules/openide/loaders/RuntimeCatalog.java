/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.xml.EntityCatalog
 */
package org.netbeans.modules.openide.loaders;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import org.openide.xml.EntityCatalog;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

@Deprecated
public final class RuntimeCatalog
extends EntityCatalog {
    private Map<String, String> id2uri;
    private Map<String, String> id2resource;
    private Map<String, ClassLoader> id2loader;

    public InputSource resolveEntity(String name, String systemId) throws IOException, SAXException {
        String mappedURI = this.name2uri(name);
        InputStream stream = this.mapResource(name);
        if (mappedURI != null) {
            InputSource retval = new InputSource(mappedURI);
            retval.setPublicId(name);
            return retval;
        }
        if (stream != null) {
            InputSource retval = new InputSource(stream);
            retval.setPublicId(name);
            return retval;
        }
        return null;
    }

    public void registerCatalogEntry(String publicId, String uri) {
        if (this.id2uri == null) {
            this.id2uri = new HashMap<String, String>();
        }
        this.id2uri.put(publicId, uri);
    }

    public void registerCatalogEntry(String publicId, String resourceName, ClassLoader loader) {
        if (this.id2resource == null) {
            this.id2resource = new HashMap<String, String>();
        }
        this.id2resource.put(publicId, resourceName);
        if (loader != null) {
            if (this.id2loader == null) {
                this.id2loader = new HashMap<String, ClassLoader>();
            }
            this.id2loader.put(publicId, loader);
        }
    }

    private String name2uri(String publicId) {
        if (publicId == null || this.id2uri == null) {
            return null;
        }
        return this.id2uri.get(publicId);
    }

    private InputStream mapResource(String publicId) {
        if (publicId == null || this.id2resource == null) {
            return null;
        }
        String resourceName = this.id2resource.get(publicId);
        ClassLoader loader = null;
        if (resourceName == null) {
            return null;
        }
        if (this.id2loader != null) {
            loader = this.id2loader.get(publicId);
        }
        if (loader == null) {
            return ClassLoader.getSystemResourceAsStream(resourceName);
        }
        return loader.getResourceAsStream(resourceName);
    }
}

