/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.actions.Savable
 */
package org.netbeans.modules.openide.loaders;

import org.netbeans.api.actions.Savable;

public interface Unmodify
extends Savable {
    public void unmodify();
}

