/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.InstanceCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.WeakListeners
 *  org.openide.xml.EntityCatalog
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.modules.openide.loaders;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.XMLDataObject;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;
import org.openide.xml.EntityCatalog;
import org.openide.xml.XMLUtil;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

@Deprecated
public final class EntityCatalogImpl
extends EntityCatalog {
    private Map<String, String> id2uri;
    private static final RequestProcessor catalogRP = new RequestProcessor("EntityCatalog/parser");

    private EntityCatalogImpl(Map<String, String> map) {
        this.id2uri = map;
    }

    public InputSource resolveEntity(String publicID, String systemID) {
        if (publicID == null) {
            return null;
        }
        String res = this.id2uri.get(publicID);
        InputSource ret = null;
        if (res != null) {
            ret = new InputSource(res);
        }
        return ret;
    }

    public static class RegistrationProcessor
    extends DefaultHandler
    implements XMLDataObject.Processor,
    InstanceCookie,
    Runnable,
    PropertyChangeListener {
        private XMLDataObject peer;
        private Map<String, String> map;
        private RequestProcessor.Task parsingTask;
        private EntityCatalogImpl instance;

        public RegistrationProcessor() {
            this.parsingTask = catalogRP.create((Runnable)this);
            this.instance = null;
        }

        @Override
        public void attachTo(XMLDataObject xmlDO) {
            if (xmlDO == this.peer) {
                return;
            }
            this.peer = xmlDO;
            this.peer.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this.peer));
            this.parsingTask.schedule(0);
        }

        @Override
        public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
            if ("public".equals(qName)) {
                String key = atts.getValue("publicId");
                String val = atts.getValue("uri");
                if (key != null && val != null) {
                    this.map.put(key, val);
                } else {
                    throw new SAXException("invalid <public> element: missing publicId or uri");
                }
            }
        }

        @Override
        public InputSource resolveEntity(String pid, String sid) {
            if ("-//NetBeans//Entity Mapping Registration 1.0//EN".equals(pid)) {
                return new InputSource(EntityCatalogImpl.class.getResource("EntityCatalog.dtd").toExternalForm());
            }
            return null;
        }

        @Override
        public void run() {
            this.map = new Hashtable<String, String>();
            try {
                String loc = this.peer.getPrimaryFile().getURL().toExternalForm();
                InputSource src = new InputSource(loc);
                XMLReader reader = XMLUtil.createXMLReader((boolean)false);
                reader.setErrorHandler(this);
                reader.setContentHandler(this);
                reader.setEntityResolver(this);
                reader.parse(src);
            }
            catch (SAXException ex) {
                Logger.getLogger(EntityCatalogImpl.class.getName()).log(Level.WARNING, null, ex);
            }
            catch (IOException ex) {
                Logger.getLogger(EntityCatalogImpl.class.getName()).log(Level.WARNING, null, ex);
            }
        }

        public Class instanceClass() throws IOException, ClassNotFoundException {
            return EntityCatalog.class;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Object instanceCreate() throws IOException, ClassNotFoundException {
            RegistrationProcessor registrationProcessor = this;
            synchronized (registrationProcessor) {
                if (this.instance == null) {
                    this.parsingTask.waitFinished();
                    this.instance = new EntityCatalogImpl(this.map);
                }
            }
            return this.instance;
        }

        public String instanceName() {
            return "org.openide.xml.EntityCatalog";
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent e) {
            RegistrationProcessor registrationProcessor = this;
            synchronized (registrationProcessor) {
                if (this.instance == null) {
                    return;
                }
            }
            if ("document".equals(e.getPropertyName())) {
                this.run();
                this.instance.id2uri = this.map;
            }
        }
    }

}

