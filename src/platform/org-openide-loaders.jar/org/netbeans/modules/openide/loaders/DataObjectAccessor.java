/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.CookieSet
 */
package org.netbeans.modules.openide.loaders;

import java.io.IOException;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.FolderInstance;
import org.openide.loaders.MultiDataObject;
import org.openide.nodes.CookieSet;

public abstract class DataObjectAccessor {
    public static DataObjectAccessor DEFAULT;

    public abstract DataObject copyRename(DataObject var1, DataFolder var2, String var3, String var4) throws IOException;

    public abstract CookieSet getCookieSet(MultiDataObject var1);

    public abstract boolean isInstancesThread();

    public abstract void precreateInstances(FolderInstance var1);

    static {
        block2 : {
            Class<DataObject> c = DataObject.class;
            try {
                Class.forName(c.getName(), true, c.getClassLoader());
            }
            catch (ClassNotFoundException ex) {
                if ($assertionsDisabled) break block2;
                throw new AssertionError(ex);
            }
        }
    }
}

