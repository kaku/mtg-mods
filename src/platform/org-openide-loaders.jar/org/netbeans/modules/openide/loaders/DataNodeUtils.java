/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.RequestProcessor
 */
package org.netbeans.modules.openide.loaders;

import org.openide.util.RequestProcessor;

public final class DataNodeUtils {
    private static final RequestProcessor RP = new RequestProcessor("Data System Nodes");

    private DataNodeUtils() {
    }

    public static RequestProcessor reqProcessor() {
        return RP;
    }
}

