/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Mutex
 *  org.openide.util.Task
 */
package org.netbeans.modules.openide.loaders;

import java.awt.EventQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.loaders.FolderInstance;
import org.openide.util.Mutex;
import org.openide.util.Task;

public final class AWTTask
extends Task {
    static final LinkedBlockingQueue<AWTTask> PENDING = new LinkedBlockingQueue();
    private static final EDT WAKE_UP = new EDT();
    private static final Runnable PROCESSOR = new Processor();
    private final Object id;
    private boolean executed;
    private final Logger LOG = Logger.getLogger("org.openide.awt.Toolbar");

    public AWTTask(Runnable r, FolderInstance id) {
        super(r);
        this.id = id;
        PENDING.add(this);
        Mutex.EVENT.readAccess(PROCESSOR);
    }

    /*
     * Exception decompiling
     */
    public void run() {
        // This method has failed to decompile.  When submitting a bug report, please provide this stack trace, and (if you hold appropriate legal rights) the relevant class file.
        // org.benf.cfr.reader.util.ConfusedCFRException: Tried to end blocks [1[TRYBLOCK]], but top level block is 3[CATCHBLOCK]
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.processEndingBlocks(Op04StructuredStatement.java:397)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.buildNestedBlocks(Op04StructuredStatement.java:449)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op03SimpleStatement.createInitialStructuredBlock(Op03SimpleStatement.java:2877)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisInner(CodeAnalyser.java:825)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisOrWrapFail(CodeAnalyser.java:217)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysis(CodeAnalyser.java:162)
        // org.benf.cfr.reader.entities.attributes.AttributeCode.analyse(AttributeCode.java:95)
        // org.benf.cfr.reader.entities.Method.analyse(Method.java:355)
        // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:768)
        // org.benf.cfr.reader.entities.ClassFile.analyseTop(ClassFile.java:700)
        // org.benf.cfr.reader.Main.doJar(Main.java:134)
        // org.benf.cfr.reader.Main.main(Main.java:189)
        throw new IllegalStateException("Decompilation failed");
    }

    public void waitFinished() {
        if (EventQueue.isDispatchThread()) {
            if (PENDING.remove((Object)this)) {
                this.run();
            }
        } else {
            WAKE_UP.wakeUp();
            super.waitFinished();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean waitFinished(long milliseconds) throws InterruptedException {
        if (EventQueue.isDispatchThread()) {
            PENDING.remove((Object)this);
            this.run();
            return true;
        }
        WAKE_UP.wakeUp();
        AWTTask aWTTask = this;
        synchronized (aWTTask) {
            if (this.isFinished()) {
                return true;
            }
            this.wait(milliseconds);
            return this.isFinished();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static boolean waitFor(Task t) {
        assert (EventQueue.isDispatchThread());
        if (!PENDING.isEmpty()) {
            PROCESSOR.run();
            return false;
        }
        Thread previous = null;
        try {
            previous = WAKE_UP.enter();
            if (!t.waitFinished(10000)) {
                AWTTask.flush();
                boolean bl = false;
                return bl;
            }
        }
        catch (InterruptedException ex) {
            AWTTask.flush();
            boolean bl = false;
            return bl;
        }
        finally {
            WAKE_UP.exit(previous);
        }
        return true;
    }

    static void flush() {
        PROCESSOR.run();
    }

    private static final class EDT {
        private Thread awt;

        private EDT() {
        }

        public synchronized Thread enter() {
            assert (EventQueue.isDispatchThread());
            Thread p = this.awt;
            this.awt = Thread.currentThread();
            return p;
        }

        public synchronized void exit(Thread previous) {
            assert (EventQueue.isDispatchThread());
            assert (this.awt == Thread.currentThread());
            this.awt = previous;
            Thread.interrupted();
        }

        public synchronized void wakeUp() {
            if (this.awt != null) {
                this.awt.interrupt();
            }
        }
    }

    private static final class Processor
    implements Runnable {
        private static final Logger LOG = Logger.getLogger(Processor.class.getName());

        private Processor() {
        }

        @Override
        public void run() {
            assert (EventQueue.isDispatchThread());
            do {
                AWTTask t;
                if ((t = AWTTask.PENDING.poll()) == null) {
                    LOG.log(Level.FINER, " processing finished");
                    return;
                }
                LOG.log(Level.FINER, " processing {0}", (Object)t);
                t.run();
            } while (true);
        }
    }

}

