/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.CloseCookie
 *  org.openide.cookies.EditCookie
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.EditorCookie$Observable
 *  org.openide.cookies.LineCookie
 *  org.openide.cookies.OpenCookie
 *  org.openide.cookies.PrintCookie
 *  org.openide.cookies.SaveCookie
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.CookieSet
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.CloneableEditorSupport$Env
 *  org.openide.text.CloneableEditorSupport$Pane
 *  org.openide.util.Lookup
 *  org.openide.windows.CloneableOpenSupport
 */
package org.netbeans.modules.openide.loaders;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;
import org.netbeans.modules.openide.loaders.Unmodify;
import org.openide.cookies.CloseCookie;
import org.openide.cookies.EditCookie;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.LineCookie;
import org.openide.cookies.OpenCookie;
import org.openide.cookies.PrintCookie;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.SaveAsCapable;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.DataEditorSupport;
import org.openide.util.Lookup;
import org.openide.windows.CloneableOpenSupport;

public final class SimpleES
extends DataEditorSupport
implements OpenCookie,
EditCookie,
EditorCookie.Observable,
PrintCookie,
CloseCookie,
SaveAsCapable,
LineCookie {
    private final SaveCookie saveCookie;
    private final CookieSet set;
    private final Callable<CloneableEditorSupport.Pane> factory;

    public SimpleES(DataObject obj, MultiDataObject.Entry entry, CookieSet set) {
        this(obj, entry, set, null);
    }

    public SimpleES(DataObject obj, MultiDataObject.Entry entry, CookieSet set, Callable<CloneableEditorSupport.Pane> paneFactory) {
        super(obj, obj.getLookup(), new Environment(obj, entry));
        this.saveCookie = new SaveCookieImpl();
        this.set = set;
        this.factory = paneFactory;
    }

    protected boolean asynchronousOpen() {
        return true;
    }

    protected CloneableEditorSupport.Pane createPane() {
        if (this.factory != null) {
            try {
                return this.factory.call();
            }
            catch (Exception ex) {
                throw new IllegalStateException("Cannot create factory for " + this.getDataObject(), ex);
            }
        }
        return super.createPane();
    }

    protected boolean notifyModified() {
        if (!super.notifyModified()) {
            return false;
        }
        this.addSaveCookie();
        return true;
    }

    protected void notifyUnmodified() {
        super.notifyUnmodified();
        this.removeSaveCookie(true);
    }

    private void addSaveCookie() {
        DataObject obj = this.getDataObject();
        if (obj.getCookie(SaveCookie.class) == null) {
            this.set.add((Node.Cookie)this.saveCookie);
            obj.setModified(true);
        }
    }

    private void removeSaveCookie(boolean setModified) {
        DataObject obj = this.getDataObject();
        SaveCookie cookie = obj.getCookie(SaveCookie.class);
        if (cookie != null && cookie.equals((Object)this.saveCookie)) {
            this.set.remove((Node.Cookie)this.saveCookie);
            if (setModified) {
                obj.setModified(false);
            }
        }
    }

    private class SaveCookieImpl
    implements SaveCookie,
    Unmodify {
        public void save() throws IOException {
            SimpleES.this.saveDocument();
        }

        public String toString() {
            return SimpleES.this.getDataObject().getPrimaryFile().getNameExt();
        }

        @Override
        public void unmodify() {
            SimpleES.this.removeSaveCookie(false);
        }
    }

    private static class Environment
    extends DataEditorSupport.Env {
        private static final long serialVersionUID = 5451434321155443431L;
        private MultiDataObject.Entry entry;

        public Environment(DataObject obj, MultiDataObject.Entry entry) {
            super(obj);
            this.entry = entry;
        }

        @Override
        protected FileObject getFile() {
            return this.entry.getFile();
        }

        @Override
        protected FileLock takeLock() throws IOException {
            return this.entry.takeLock();
        }

        @Override
        public CloneableOpenSupport findCloneableOpenSupport() {
            return (CloneableOpenSupport)this.getDataObject().getCookie(SimpleES.class);
        }

        @Override
        public String getMimeType() {
            ScriptEngineManager m;
            String name;
            Iterator<String> i$;
            ScriptEngine eng;
            Object n = this.entry.getFile().getAttribute("javax.script.ScriptEngine");
            if (n instanceof String && (eng = (m = new ScriptEngineManager()).getEngineByName(name = (String)n)) != null && (i$ = eng.getFactory().getMimeTypes().iterator()).hasNext()) {
                String mime = i$.next();
                return mime;
            }
            return super.getMimeType();
        }
    }

}

