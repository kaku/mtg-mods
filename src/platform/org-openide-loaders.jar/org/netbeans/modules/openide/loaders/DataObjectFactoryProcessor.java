/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 */
package org.netbeans.modules.openide.loaders;

import java.lang.annotation.Annotation;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;
import org.openide.loaders.DataObject;
import org.openide.loaders.MultiFileLoader;

@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
@SupportedAnnotationTypes(value={"org.openide.loaders.DataObject.Registration", "org.openide.loaders.DataObject.Registrations"})
public class DataObjectFactoryProcessor
extends LayerGeneratingProcessor {
    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        if (roundEnv.processingOver()) {
            return false;
        }
        for (Element e2 : roundEnv.getElementsAnnotatedWith(DataObject.Registration.class)) {
            DataObject.Registration dfr = e2.getAnnotation(DataObject.Registration.class);
            if (dfr == null) continue;
            this.process(e2, dfr);
        }
        for (Element e : roundEnv.getElementsAnnotatedWith(DataObject.Registrations.class)) {
            DataObject.Registrations dfrr = e.getAnnotation(DataObject.Registrations.class);
            if (dfrr == null) continue;
            for (DataObject.Registration t : dfrr.value()) {
                this.process(e, t);
            }
        }
        return true;
    }

    private void process(Element e, DataObject.Registration dfr) throws LayerGenerationException {
        LinkedList<ExecutableElement> ee;
        TypeMirror dataObjectType = this.type(DataObject.class);
        TypeMirror fileObjectType = this.type(FileObject.class);
        TypeMirror multiFileLoaderType = this.type(MultiFileLoader.class);
        TypeMirror dataObjectFactoryType = this.type(DataObject.Factory.class);
        LayerBuilder builder = this.layer(new Element[]{e});
        String className = this.processingEnv.getElementUtils().getBinaryName((TypeElement)e).toString();
        String factoryId = className.replace(".", "-");
        boolean useFactory = true;
        if (this.isAssignable(e.asType(), dataObjectType)) {
            ee = new LinkedList<ExecutableElement>();
            for (ExecutableElement element : ElementFilter.constructorsIn(e.getEnclosedElements())) {
                if (element.getKind() != ElementKind.CONSTRUCTOR || !element.getModifiers().contains((Object)Modifier.PUBLIC) || element.getParameters().size() != 2 || !this.isAssignable(element.getParameters().get(0).asType(), fileObjectType) || !this.isAssignable(element.getParameters().get(1).asType(), multiFileLoaderType)) continue;
                ee.add(element);
            }
            if (ee.isEmpty()) {
                throw new LayerGenerationException("DataObject subclass with @DataObject.Registration needs a public constructor with FileObject and MultiFileLoader parameters", e, this.processingEnv, (Annotation)dfr);
            }
            useFactory = true;
        } else if (this.isAssignable(e.asType(), dataObjectFactoryType)) {
            ee = new LinkedList();
            for (ExecutableElement element : ElementFilter.constructorsIn(e.getEnclosedElements())) {
                if (element.getKind() != ElementKind.CONSTRUCTOR || !element.getModifiers().contains((Object)Modifier.PUBLIC) || !element.getParameters().isEmpty()) continue;
                ee.add(element);
            }
            if (ee.isEmpty()) {
                throw new LayerGenerationException("DataObject.Factory subclass with @DataObject.Registration needs a public default constructor", e, this.processingEnv, (Annotation)dfr);
            }
            useFactory = false;
            factoryId = className.replace(".class", "").replace(".", "-");
        } else {
            throw new LayerGenerationException("Usage @DataObject.Registration only on DataObject.Factory subclass or DataObject subclass", e, this.processingEnv, (Annotation)dfr);
        }
        if (dfr.mimeType() == null) {
            throw new LayerGenerationException("@DataObject.Factory.Registration mimeType() cannot be null", e, this.processingEnv, (Annotation)dfr, "mimeTypes");
        }
        String aMimeType = dfr.mimeType();
        LayerBuilder.File f = builder.file("Loaders/" + aMimeType + "/Factories/" + factoryId + ".instance");
        if (dfr.iconBase().length() > 0) {
            builder.validateResource(dfr.iconBase(), e.getEnclosingElement(), (Annotation)dfr, "iconBase", true);
            f.stringvalue("iconBase", dfr.iconBase());
        }
        f.position(dfr.position());
        if (!dfr.displayName().isEmpty()) {
            f.bundlevalue("displayName", dfr.displayName(), (Annotation)dfr, "displayName");
        }
        if (useFactory) {
            f.methodvalue("instanceCreate", "org.openide.loaders.DataLoaderPool", "factory");
            f.stringvalue("dataObjectClass", className);
            f.stringvalue("mimeType", aMimeType);
        }
        f.write();
    }

    private TypeMirror type(Class<?> type) {
        TypeElement e = this.processingEnv.getElementUtils().getTypeElement(type.getCanonicalName());
        return e == null ? null : e.asType();
    }

    private boolean isAssignable(TypeMirror first, TypeMirror snd) {
        if (snd == null) {
            return false;
        }
        return this.processingEnv.getTypeUtils().isAssignable(first, snd);
    }
}

