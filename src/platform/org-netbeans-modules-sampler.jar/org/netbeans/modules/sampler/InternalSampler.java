/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.actions.Openable
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.modules.Places
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.sampler;

import java.awt.EventQueue;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.lang.management.ThreadMXBean;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.actions.Openable;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.modules.sampler.Bundle;
import org.netbeans.modules.sampler.Sampler;
import org.netbeans.modules.sampler.SamplesOutputStream;
import org.netbeans.modules.sampler.SelfSampleVFS;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.modules.Places;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

final class InternalSampler
extends Sampler {
    private static final String SAMPLER_NAME = "selfsampler";
    private static final String FILE_NAME = "selfsampler.npss";
    private static final String UNKNOW_MIME_TYPE = "content/unknown";
    private static final String X_DEBUG_ARG = "-Xdebug";
    private static final String JDWP_DEBUG_ARG = "-agentlib:jdwp";
    private static final String JDWP_DEBUG_ARG_PREFIX = "-agentlib:jdwp=";
    private static final Logger LOGGER = Logger.getLogger(InternalSampler.class.getName());
    private static Boolean debugMode;
    private static String lastReason;
    private ProgressHandle progress;

    static InternalSampler createInternalSampler(String key) {
        if (SamplesOutputStream.isSupported() && InternalSampler.isRunMode()) {
            return new InternalSampler(key);
        }
        return null;
    }

    private static synchronized boolean isDebugged() {
        if (debugMode == null) {
            debugMode = Boolean.FALSE;
            RuntimeMXBean runtime = ManagementFactory.getRuntimeMXBean();
            List<String> args = runtime.getInputArguments();
            if (args.contains("-Xdebug")) {
                debugMode = Boolean.TRUE;
            } else if (args.contains("-agentlib:jdwp")) {
                debugMode = Boolean.TRUE;
            } else {
                for (String arg : args) {
                    if (!arg.startsWith("-agentlib:jdwp=")) continue;
                    debugMode = Boolean.TRUE;
                    break;
                }
            }
        }
        return debugMode;
    }

    private static boolean isRunMode() {
        boolean runMode = true;
        String reason = null;
        if (InternalSampler.isDebugged()) {
            reason = "running in debug mode";
            runMode = false;
        }
        if (runMode) {
            try {
                Class.forName("org.netbeans.lib.profiler.server.ProfilerServer", false, ClassLoader.getSystemClassLoader());
                reason = "running under profiler";
                runMode = false;
            }
            catch (ClassNotFoundException ex) {
                // empty catch block
            }
        }
        if (!runMode && !reason.equals(lastReason)) {
            LOGGER.log(Level.INFO, "Slowness detector disabled - {0}", reason);
        }
        lastReason = reason;
        return runMode;
    }

    InternalSampler(String thread) {
        super(thread);
    }

    @Override
    protected void printStackTrace(Throwable ex) {
        Exceptions.printStackTrace((Throwable)ex);
    }

    @Override
    protected void saveSnapshot(byte[] arr) throws IOException {
        File outFile = File.createTempFile("selfsampler", ".npss");
        File userDir = Places.getUserDirectory();
        File gestures = null;
        outFile = FileUtil.normalizeFile((File)outFile);
        this.writeToFile(outFile, arr);
        if (userDir != null) {
            gestures = new File(new File(new File(userDir, "var"), "log"), "uigestures");
        }
        SelfSampleVFS fs = gestures != null && gestures.exists() ? new SelfSampleVFS(new String[]{"selfsampler.npss", "selfsampler.log"}, new File[]{outFile, gestures}) : new SelfSampleVFS(new String[]{"selfsampler.npss"}, new File[]{outFile});
        FileObject fo = fs.findResource("selfsampler.npss");
        if ("content/unknown".equals(fo.getMIMEType())) {
            String msg = Bundle.SelfSamplerAction_SavedFile(outFile.getAbsolutePath());
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)msg));
        } else {
            DataObject dobj = DataObject.find((FileObject)fo);
            ((Openable)dobj.getLookup().lookup(Openable.class)).open();
        }
    }

    private void writeToFile(File file, byte[] arr) {
        try {
            FileOutputStream fstream = new FileOutputStream(file);
            fstream.write(arr);
            fstream.close();
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    @Override
    ThreadMXBean getThreadMXBean() {
        return ManagementFactory.getThreadMXBean();
    }

    @Override
    void openProgress(int steps) {
        if (EventQueue.isDispatchThread()) {
            return;
        }
        this.progress = ProgressHandleFactory.createHandle((String)Bundle.Save_Progress());
        this.progress.start(steps);
    }

    @Override
    void closeProgress() {
        if (EventQueue.isDispatchThread()) {
            return;
        }
        this.progress.finish();
        this.progress = null;
    }

    @Override
    void progress(int i) {
        if (EventQueue.isDispatchThread()) {
            return;
        }
        if (this.progress != null) {
            this.progress.progress(i);
        }
    }
}

