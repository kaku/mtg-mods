/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.sampler;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.sampler.InternalSampler;
import org.netbeans.modules.sampler.SamplesOutputStream;

public abstract class Sampler {
    private static final int SAMPLER_RATE = 10;
    private static final double MAX_AVERAGE = 30.0;
    private static final double MAX_STDDEVIATION = 40.0;
    private static final int MAX_SAMPLING_TIME = 300;
    private static final int MIN_SAMPLES = 50;
    private static final int MAX_SAMPLES = 30000;
    private final String name;
    private Timer timer;
    private ByteArrayOutputStream out;
    private SamplesOutputStream samplesStream;
    private long startTime;
    private long nanoTimeCorrection;
    private long samples;
    private long laststamp;
    private double max;
    private double min = 9.223372036854776E18;
    private double sum;
    private double devSquaresSum;
    private volatile boolean stopped;
    private volatile boolean running;

    @CheckForNull
    public static Sampler createSampler(@NonNull String name) {
        return InternalSampler.createInternalSampler(name);
    }

    @CheckForNull
    public static Sampler createManualSampler(@NonNull String name) {
        if (SamplesOutputStream.isSupported()) {
            return new InternalSampler(name);
        }
        return null;
    }

    Sampler(String n) {
        this.name = n;
    }

    abstract ThreadMXBean getThreadMXBean();

    abstract void saveSnapshot(byte[] var1) throws IOException;

    abstract void printStackTrace(Throwable var1);

    abstract void openProgress(int var1);

    abstract void closeProgress();

    abstract void progress(int var1);

    private void updateStats(long timestamp) {
        if (this.laststamp != 0) {
            double diff = (double)(timestamp - this.laststamp) / 1000000.0;
            ++this.samples;
            this.sum += diff;
            this.devSquaresSum += (diff - 10.0) * (diff - 10.0);
            if (diff > this.max) {
                this.max = diff;
            } else if (diff < this.min) {
                this.min = diff;
            }
        }
        this.laststamp = timestamp;
    }

    public final synchronized void start() {
        if (this.running) {
            throw new IllegalStateException("sampling is already running");
        }
        if (this.stopped) {
            throw new IllegalStateException("it is not possible to restart sampling");
        }
        this.running = true;
        final ThreadMXBean threadBean = this.getThreadMXBean();
        this.out = new ByteArrayOutputStream(65536);
        try {
            this.samplesStream = new SamplesOutputStream(this.out, this, 30000);
        }
        catch (IOException ex) {
            this.printStackTrace(ex);
            return;
        }
        this.startTime = System.currentTimeMillis();
        this.nanoTimeCorrection = this.startTime * 1000000 - System.nanoTime();
        this.timer = new Timer("sampler-" + this.name);
        this.timer.schedule(new TimerTask(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                Sampler sampler = Sampler.this;
                synchronized (sampler) {
                    if (Sampler.this.stopped) {
                        return;
                    }
                    try {
                        ThreadInfo[] infos = threadBean.dumpAllThreads(false, false);
                        long timestamp = System.nanoTime() + Sampler.this.nanoTimeCorrection;
                        Sampler.this.samplesStream.writeSample(infos, timestamp, Thread.currentThread().getId());
                        Sampler.this.updateStats(timestamp);
                    }
                    catch (Throwable ex) {
                        Sampler.this.printStackTrace(ex);
                    }
                }
            }
        }, 10, 10);
    }

    public final void cancel() {
        this.stopSampling(true, null);
    }

    public final void stopAndWriteTo(@NonNull DataOutputStream dos) {
        this.stopSampling(false, dos);
    }

    public final void stop() {
        this.stopSampling(false, null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private synchronized void stopSampling(boolean cancel, DataOutputStream dos) {
        try {
            boolean writeCommand;
            if (!this.running) {
                throw new IllegalStateException("sampling was not started");
            }
            if (this.stopped) {
                throw new IllegalStateException("sampling is not running");
            }
            this.stopped = true;
            this.timer.cancel();
            if (cancel || this.samples < 1) {
                return;
            }
            if (SwingUtilities.isEventDispatchThread()) {
                throw new IllegalStateException("sampling cannot be stopped from EDT");
            }
            double average = this.sum / (double)this.samples;
            double std_deviation = Math.sqrt(this.devSquaresSum / (double)this.samples);
            boolean bl = writeCommand = dos != null;
            if (writeCommand) {
                Object[] params = new Object[]{this.startTime, "Samples", this.samples, "Average", average, "Minimum", this.min, "Maximum", this.max, "Std. deviation", std_deviation};
                Logger.getLogger("org.netbeans.ui.performance").log(Level.CONFIG, "Snapshot statistics", params);
                if (average > 30.0 || std_deviation > 40.0 || this.samples < 50) {
                    return;
                }
            }
            this.samplesStream.close();
            this.samplesStream = null;
            if (writeCommand) {
                dos.write(this.out.toByteArray());
                dos.close();
                return;
            }
            this.saveSnapshot(this.out.toByteArray());
        }
        catch (IOException ex) {
            this.printStackTrace(ex);
        }
        finally {
            this.out = null;
            this.samplesStream = null;
        }
    }

}

