/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.sampler;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String Save_Progress() {
        return NbBundle.getMessage(Bundle.class, (String)"Save_Progress");
    }

    static String SelfSamplerAction_SavedFile(Object arg0) {
        return NbBundle.getMessage(Bundle.class, (String)"SelfSamplerAction_SavedFile", (Object)arg0);
    }

    private void Bundle() {
    }
}

