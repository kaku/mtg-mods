/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.AbstractFileSystem
 *  org.openide.filesystems.AbstractFileSystem$Attr
 *  org.openide.filesystems.AbstractFileSystem$Info
 *  org.openide.filesystems.AbstractFileSystem$List
 *  org.openide.util.Enumerations
 */
package org.netbeans.modules.sampler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Enumeration;
import org.openide.filesystems.AbstractFileSystem;
import org.openide.util.Enumerations;

class SelfSampleVFS
extends AbstractFileSystem
implements AbstractFileSystem.List,
AbstractFileSystem.Info,
AbstractFileSystem.Attr {
    private final String[] names;
    private final File[] contents;

    SelfSampleVFS(String[] names, File[] contents) {
        this.names = names;
        this.contents = contents;
        this.list = this;
        this.info = this;
        this.attr = this;
    }

    public String getDisplayName() {
        return "";
    }

    public boolean isReadOnly() {
        return true;
    }

    public String[] children(String f) {
        return f.equals("") ? this.names : null;
    }

    private File findFile(String name) {
        for (int i = 0; i < this.names.length; ++i) {
            if (!name.equals(this.names[i])) continue;
            return this.contents[i];
        }
        return null;
    }

    public Date lastModified(String name) {
        File f = this.findFile(name);
        return f == null ? null : new Date(f.lastModified());
    }

    public boolean folder(String name) {
        return name.equals("");
    }

    public boolean readOnly(String name) {
        return true;
    }

    public String mimeType(String name) {
        return null;
    }

    public long size(String name) {
        File f = this.findFile(name);
        return f == null ? -1 : f.length();
    }

    public InputStream inputStream(String name) throws FileNotFoundException {
        File f = this.findFile(name);
        if (f == null) {
            throw new FileNotFoundException();
        }
        return new FileInputStream(f);
    }

    public OutputStream outputStream(String name) throws IOException {
        throw new IOException();
    }

    public void lock(String name) throws IOException {
        throw new IOException();
    }

    public void unlock(String name) {
        throw new UnsupportedOperationException(name);
    }

    public void markUnimportant(String name) {
        throw new UnsupportedOperationException(name);
    }

    public Object readAttribute(String name, String attrName) {
        if ("java.io.File".equals(attrName)) {
            return this.findFile(name);
        }
        return null;
    }

    public void writeAttribute(String name, String attrName, Object value) throws IOException {
        throw new IOException();
    }

    public Enumeration<String> attributes(String name) {
        return Enumerations.empty();
    }

    public void renameAttributes(String oldName, String newName) {
        throw new UnsupportedOperationException(oldName);
    }

    public void deleteAttributes(String name) {
        throw new UnsupportedOperationException(name);
    }
}

