/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.sampler;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.management.ThreadInfo;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.zip.GZIPOutputStream;
import javax.management.openmbean.CompositeData;
import org.netbeans.modules.sampler.Sampler;

class SamplesOutputStream {
    private static final String[][] methods = new String[][]{{"sun.management.ThreadInfoCompositeData", "toCompositeData"}, {"com.ibm.lang.management.ManagementUtils", "toThreadInfoCompositeData"}};
    static final String ID = "NPSS";
    public static final String FILE_EXT = ".npss";
    static final int RESET_THRESHOLD = 5000;
    static final int STEPS = 1000;
    static byte version = 2;
    private static Method toCompositeDataMethod;
    OutputStream outStream;
    Map<Long, ThreadInfo> lastThreadInfos;
    Map<StackTraceElement, WeakReference<StackTraceElement>> steCache;
    List<Sample> samples;
    Sampler progress;
    int maxSamples;
    int offset;

    public static boolean isSupported() {
        return toCompositeDataMethod != null;
    }

    SamplesOutputStream(OutputStream os, Sampler progress, int max) throws IOException {
        this.maxSamples = max;
        this.progress = progress;
        this.outStream = os;
        this.writeHeader(os);
        this.lastThreadInfos = new HashMap<Long, ThreadInfo>();
        this.steCache = new WeakHashMap<StackTraceElement, WeakReference<StackTraceElement>>(8192);
        this.samples = new ArrayList<Sample>(1024);
    }

    void writeSample(ThreadInfo[] infos, long time, long selfThreadId) throws IOException {
        ArrayList<Long> sameT = new ArrayList<Long>();
        ArrayList<ThreadInfo> newT = new ArrayList<ThreadInfo>();
        ArrayList<Long> tids = new ArrayList<Long>();
        for (ThreadInfo tinfo : infos) {
            Object[] stack;
            long id;
            Object[] lastStack;
            if (tinfo == null || (id = tinfo.getThreadId()) == selfThreadId) continue;
            Long tid = tinfo.getThreadId();
            ThreadInfo lastThread = this.lastThreadInfos.get(tid);
            tids.add(tid);
            if (lastThread != null && lastThread.getThreadState().equals((Object)tinfo.getThreadState()) && Arrays.deepEquals(lastStack = lastThread.getStackTrace(), stack = tinfo.getStackTrace())) {
                sameT.add(tid);
                continue;
            }
            this.internStackTrace(tinfo);
            newT.add(tinfo);
            this.lastThreadInfos.put(tid, tinfo);
        }
        this.addSample(new Sample(time, sameT, newT));
        HashSet<Long> ids = new HashSet<Long>(this.lastThreadInfos.keySet());
        ids.removeAll(tids);
        this.lastThreadInfos.keySet().removeAll(ids);
    }

    private void addSample(Sample sample) {
        if (this.samples.size() == this.maxSamples) {
            Sample removedSample = this.samples.set(this.offset, sample);
            this.offset = (this.offset + 1) % this.maxSamples;
            Sample lastSample = this.samples.get(this.offset);
            this.updateLastSample(removedSample, lastSample);
        } else {
            this.samples.add(sample);
        }
    }

    Sample getSample(int index) {
        int arrayIndex = index;
        if (this.samples.size() == this.maxSamples) {
            arrayIndex = (this.offset + index) % this.maxSamples;
        }
        return this.samples.get(arrayIndex);
    }

    void removeSample(int index) {
        int arrayIndex = index;
        if (this.samples.size() == this.maxSamples) {
            arrayIndex = (this.offset + index) % this.maxSamples;
        }
        this.samples.set(arrayIndex, null);
    }

    private void updateLastSample(Sample removedSample, Sample lastSample) {
        List removedNewThreads = removedSample.getNewThreads();
        List sameThreads = lastSample.getSameThread();
        List newThreads = lastSample.getNewThreads();
        for (ThreadInfo ti : removedNewThreads) {
            Long tid = ti.getThreadId();
            if (!sameThreads.contains(tid)) continue;
            newThreads.add(ti);
            sameThreads.remove(tid);
        }
    }

    private static CompositeData toCompositeData(ThreadInfo tinfo) {
        try {
            return (CompositeData)toCompositeDataMethod.invoke(null, tinfo);
        }
        catch (IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
        catch (IllegalArgumentException ex) {
            throw new RuntimeException(ex);
        }
        catch (InvocationTargetException ex) {
            throw new RuntimeException(ex);
        }
    }

    void close() throws IOException {
        this.steCache = null;
        GZIPOutputStream stream = new GZIPOutputStream(this.outStream, 65536);
        ObjectOutputStream out = new ObjectOutputStream(stream);
        int size = this.samples.size();
        out.writeInt(size);
        out.writeLong(this.getSample(size - 1).getTime());
        this.openProgress();
        for (int i = 0; i < size; ++i) {
            Sample s = this.getSample(i);
            this.removeSample(i);
            if (i > 0 && i % 5000 == 0) {
                out.reset();
            }
            s.writeToStream(out);
            if ((i + 40) % 50 != 0) continue;
            this.step(1000 * i / size);
        }
        this.step(1000);
        out.close();
        this.closeProgress();
    }

    private void writeHeader(OutputStream os) throws IOException {
        os.write("NPSS".getBytes());
        os.write(version);
    }

    private void internStackTrace(ThreadInfo tinfo) {
        if (this.steCache == null) {
            return;
        }
        StackTraceElement[] stack = tinfo.getStackTrace();
        for (int i = 0; i < stack.length; ++i) {
            StackTraceElement ste = stack[i];
            WeakReference<StackTraceElement> oldStackRef = this.steCache.get(ste);
            if (oldStackRef != null) {
                stack[i] = oldStackRef.get();
                assert (stack[i] != null);
                continue;
            }
            this.steCache.put(ste, new WeakReference<StackTraceElement>(ste));
        }
    }

    private void openProgress() {
        if (this.progress != null) {
            this.progress.openProgress(1000);
        }
    }

    private void closeProgress() {
        if (this.progress != null) {
            this.progress.closeProgress();
        }
    }

    private void step(int i) {
        if (this.progress != null) {
            this.progress.progress(i);
        }
    }

    static {
        for (String[] method : methods) {
            String className = method[0];
            String methodName = method[1];
            try {
                Class clazz = Class.forName(className);
                toCompositeDataMethod = clazz.getMethod(methodName, ThreadInfo.class);
                if (toCompositeDataMethod == null) continue;
                break;
            }
            catch (ClassNotFoundException ex) {
                continue;
            }
            catch (NoSuchMethodException ex) {
                continue;
            }
            catch (SecurityException ex) {
                // empty catch block
            }
        }
    }

    private static class Sample {
        private final long time;
        private final List<Long> sameThread;
        private final List<ThreadInfo> newThreads;

        Sample(long t, List<Long> sameT, List<ThreadInfo> newT) {
            this.time = t;
            this.sameThread = sameT;
            this.newThreads = newT;
        }

        private long getTime() {
            return this.time;
        }

        private List<Long> getSameThread() {
            return this.sameThread;
        }

        private List<ThreadInfo> getNewThreads() {
            return this.newThreads;
        }

        private void writeToStream(ObjectOutputStream out) throws IOException {
            out.writeLong(this.time);
            out.writeInt(this.sameThread.size());
            for (Long tid : this.sameThread) {
                out.writeLong(tid);
            }
            out.writeInt(this.newThreads.size());
            for (ThreadInfo tic : this.newThreads) {
                out.writeObject(SamplesOutputStream.toCompositeData(tic));
            }
        }
    }

}

