/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.etable;

import javax.swing.DefaultListSelectionModel;

class ETableSelectionModel
extends DefaultListSelectionModel {
    private ThreadLocal<Boolean> insertingLines = new ThreadLocal();

    ETableSelectionModel() {
    }

    @Override
    public void insertIndexInterval(int index, int length, boolean before) {
        this.insertingLines.set(Boolean.TRUE);
        super.insertIndexInterval(index, length, before);
    }

    @Override
    public int getSelectionMode() {
        if (this.insertingLines.get() == Boolean.TRUE) {
            this.insertingLines.remove();
            return 0;
        }
        return super.getSelectionMode();
    }
}

