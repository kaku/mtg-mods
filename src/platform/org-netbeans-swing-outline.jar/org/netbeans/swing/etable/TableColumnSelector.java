/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.etable;

public abstract class TableColumnSelector {
    public abstract String[] selectVisibleColumns(TreeNode var1, String[] var2);

    public abstract String[] selectVisibleColumns(String[] var1, String[] var2);

    public static interface TreeNode {
        public String getText();

        public boolean isLeaf();

        public TreeNode[] getChildren();
    }

}

