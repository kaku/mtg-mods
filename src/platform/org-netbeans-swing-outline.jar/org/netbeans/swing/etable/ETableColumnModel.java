/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.etable;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.Vector;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.netbeans.swing.etable.ETable;
import org.netbeans.swing.etable.ETableColumn;
import org.netbeans.swing.etable.TableColumnSelector;

public class ETableColumnModel
extends DefaultTableColumnModel {
    private static final String NUMBER_OF_COLUMNS = "ColumnsNumber";
    private static final String NUMBER_OF_HIDDEN_COLUMNS = "HiddenColumnsNumber";
    private static final String PROP_HIDDEN_PREFIX = "Hidden";
    private static final String PROP_HIDDEN_POSITION_PREFIX = "HiddenPosition";
    protected transient List<TableColumn> sortedColumns = new ArrayList<TableColumn>();
    protected List<TableColumn> hiddenColumns = new ArrayList<TableColumn>();
    List<Integer> hiddenColumnsPosition = new ArrayList<Integer>();
    private TableColumnSelector.TreeNode columnHierarchyRoot;

    public void readSettings(Properties p, String propertyPrefix, ETable table) {
        int i;
        this.tableColumns = new Vector();
        this.sortedColumns = new ArrayList<TableColumn>();
        String s = p.getProperty(propertyPrefix + "ColumnsNumber");
        int numColumns = Integer.parseInt(s);
        for (int i2 = 0; i2 < numColumns; ++i2) {
            ETableColumn setc;
            int j;
            ETableColumn etc = (ETableColumn)table.createColumn(i2);
            etc.readSettings(p, i2, propertyPrefix);
            this.addColumn(etc);
            if (etc.getComparator() == null) continue;
            for (j = 0; j < this.sortedColumns.size() && (setc = (ETableColumn)this.sortedColumns.get(j)).getSortRank() <= etc.getSortRank(); ++j) {
            }
            this.sortedColumns.add(j, etc);
        }
        this.hiddenColumns = new ArrayList<TableColumn>();
        String sh = p.getProperty(propertyPrefix + "HiddenColumnsNumber");
        int numHiddenColumns = Integer.parseInt(sh);
        for (i = 0; i < numHiddenColumns; ++i) {
            ETableColumn etc = (ETableColumn)table.createColumn(0);
            etc.readSettings(p, i, propertyPrefix + "Hidden");
            this.hiddenColumns.add(etc);
        }
        this.hiddenColumnsPosition = new ArrayList<Integer>();
        for (i = 0; i < numHiddenColumns; ++i) {
            String myPrefix = propertyPrefix + "HiddenPosition" + "ETableColumn-" + Integer.toString(i);
            String posStr = p.getProperty(myPrefix);
            int pos = Integer.parseInt(posStr);
            this.hiddenColumnsPosition.add(pos);
        }
    }

    public void writeSettings(Properties p, String propertyPrefix) {
        int i = 0;
        int numColumns = this.tableColumns.size();
        p.setProperty(propertyPrefix + "ColumnsNumber", Integer.toString(numColumns));
        for (Object obj : this.tableColumns) {
            if (!(obj instanceof ETableColumn)) continue;
            ETableColumn etc = (ETableColumn)obj;
            etc.writeSettings(p, i++, propertyPrefix);
        }
        i = 0;
        int numHiddenColumns = this.hiddenColumns.size();
        p.setProperty(propertyPrefix + "HiddenColumnsNumber", Integer.toString(numHiddenColumns));
        for (TableColumn obj2 : this.hiddenColumns) {
            if (!(obj2 instanceof ETableColumn)) continue;
            ETableColumn etc = (ETableColumn)obj2;
            etc.writeSettings(p, i++, propertyPrefix + "Hidden");
        }
        for (i = 0; i < numHiddenColumns; ++i) {
            int pos = this.hiddenColumnsPosition.get(i);
            String myPrefix = propertyPrefix + "HiddenPosition" + "ETableColumn-" + Integer.toString(i);
            p.setProperty(myPrefix, Integer.toString(pos));
        }
    }

    public Comparator<ETable.RowMapping> getComparator() {
        if (this.sortedColumns.isEmpty()) {
            return new ETable.OriginalRowComparator();
        }
        return new CompoundComparator();
    }

    public void setColumnSorted(ETableColumn etc, boolean ascending, int newRank) {
        if (!etc.isSortingAllowed()) {
            return;
        }
        boolean wasSorted = this.sortedColumns.contains(etc);
        if (wasSorted) {
            etc.setAscending(ascending);
            etc.setSortRank(newRank);
            this.sortedColumns.remove(etc);
        } else {
            etc.setSorted(newRank, ascending);
        }
        if (newRank > 0) {
            this.sortedColumns.add(newRank - 1, etc);
        }
    }

    void toggleSortedColumn(ETableColumn etc, boolean cleanAll) {
        if (!etc.isSortingAllowed()) {
            return;
        }
        boolean wasSorted = this.sortedColumns.contains(etc);
        if (cleanAll) {
            this.clearSortedColumns(etc);
        }
        if (wasSorted) {
            if (etc.isAscending()) {
                etc.setAscending(false);
            } else {
                this.sortedColumns.remove(etc);
                etc.setSorted(0, false);
            }
            this.updateRanks();
        } else {
            etc.setSorted(this.sortedColumns.size() + 1, true);
            this.sortedColumns.add(etc);
        }
    }

    @Override
    public void removeColumn(TableColumn column) {
        this.removeColumn(column, true);
    }

    private void removeColumn(TableColumn column, boolean doShift) {
        if (this.sortedColumns.remove(column)) {
            int i = 1;
            for (TableColumn sc : this.sortedColumns) {
                if (sc instanceof ETableColumn) {
                    ETableColumn etc = (ETableColumn)sc;
                    etc.setSorted(i, etc.isAscending());
                }
                ++i;
            }
        }
        if (this.removeHiddenColumn(column, doShift) < 0) {
            int columnIndex = this.tableColumns.indexOf(column);
            super.removeColumn(column);
            if (doShift) {
                int i;
                int n = this.hiddenColumnsPosition.size();
                for (i = 0; i < n; ++i) {
                    if (this.hiddenColumnsPosition.get(i) > columnIndex) continue;
                    ++columnIndex;
                }
                for (i = 0; i < n; ++i) {
                    int index = this.hiddenColumnsPosition.get(i);
                    if (index <= columnIndex) continue;
                    this.hiddenColumnsPosition.set(i, --index);
                }
            }
        }
    }

    private int removeHiddenColumn(TableColumn column, boolean doShift) {
        int hiddenIndex = -1;
        for (int i = 0; i < this.hiddenColumns.size(); ++i) {
            if (!column.equals(this.hiddenColumns.get(i))) continue;
            hiddenIndex = i;
            break;
        }
        if (hiddenIndex >= 0) {
            this.hiddenColumns.remove(hiddenIndex);
            int hi = this.hiddenColumnsPosition.remove(hiddenIndex);
            if (doShift) {
                int n = this.hiddenColumnsPosition.size();
                for (int i2 = 0; i2 < n; ++i2) {
                    int index = this.hiddenColumnsPosition.get(i2);
                    if (index <= hi) continue;
                    this.hiddenColumnsPosition.set(i2, --index);
                }
            }
            return hi;
        }
        return -1;
    }

    public void setColumnHidden(TableColumn column, boolean hidden) {
        int index;
        if (hidden) {
            int index2;
            if (!this.hiddenColumns.contains(column) && (index2 = this.tableColumns.indexOf(column)) >= 0) {
                this.removeColumn(column, false);
                this.hiddenColumns.add(column);
                for (Integer pos : this.hiddenColumnsPosition) {
                    if (pos > index2) continue;
                    ++index2;
                }
                while (this.hiddenColumnsPosition.contains(index2)) {
                    ++index2;
                }
                this.hiddenColumnsPosition.add(index2);
            }
        } else if (!this.tableColumns.contains(column) && (index = this.removeHiddenColumn(column, false)) >= 0) {
            int i = index;
            for (Integer pos : this.hiddenColumnsPosition) {
                if (pos >= index) continue;
                --i;
            }
            index = Math.min(i, this.tableColumns.size());
            this.addColumn(column, index);
        }
    }

    private void addColumn(TableColumn aColumn, int index) {
        if (aColumn == null) {
            throw new IllegalArgumentException("Object is null");
        }
        this.tableColumns.insertElementAt(aColumn, index);
        aColumn.addPropertyChangeListener(this);
        this.totalColumnWidth = -1;
        this.fireColumnAdded(new TableColumnModelEvent(this, 0, index));
    }

    @Override
    public void moveColumn(int ci1, int ci2) {
        int i;
        int index;
        super.moveColumn(ci1, ci2);
        int n = this.hiddenColumns.size();
        for (i = 0; i < n; ++i) {
            index = this.hiddenColumnsPosition.get(i);
            if (ci1 >= index) {
                ++ci1;
            }
            if (ci2 < index) continue;
            ++ci2;
        }
        if (ci1 < ci2) {
            for (i = 0; i < n; ++i) {
                index = this.hiddenColumnsPosition.get(i);
                if (ci1 >= index || index > ci2) continue;
                this.hiddenColumnsPosition.set(i, --index);
            }
        }
        if (ci2 < ci1) {
            for (i = 0; i < n; ++i) {
                index = this.hiddenColumnsPosition.get(i);
                if (ci2 > index || index >= ci1) continue;
                this.hiddenColumnsPosition.set(i, ++index);
            }
        }
    }

    List<TableColumn> getAllColumns() {
        ArrayList<TableColumn> columns = Collections.list(this.getColumns());
        int n = this.hiddenColumns.size();
        for (int i = 0; i < n; ++i) {
            int index = this.hiddenColumnsPosition.get(i);
            index = Math.min(index, columns.size());
            columns.add(index, this.hiddenColumns.get(i));
        }
        return columns;
    }

    public boolean isColumnHidden(TableColumn tc) {
        return this.hiddenColumns.contains(tc);
    }

    public void clearSortedColumns() {
        for (TableColumn o : this.sortedColumns) {
            if (!(o instanceof ETableColumn)) continue;
            ETableColumn etc = (ETableColumn)o;
            etc.setSorted(0, false);
        }
        this.sortedColumns = new ArrayList<TableColumn>();
    }

    void clearSortedColumns(TableColumn notThisOne) {
        boolean wasSorted = this.sortedColumns.contains(notThisOne);
        for (TableColumn o : this.sortedColumns) {
            if (!(o instanceof ETableColumn) || o == notThisOne) continue;
            ETableColumn etc = (ETableColumn)o;
            etc.setSorted(0, false);
        }
        this.sortedColumns = new ArrayList<TableColumn>();
        if (wasSorted) {
            this.sortedColumns.add(notThisOne);
        }
    }

    private void updateRanks() {
        int i = 1;
        for (TableColumn o : this.sortedColumns) {
            ETableColumn etc;
            if (o instanceof ETableColumn && (etc = (ETableColumn)o).isSorted()) {
                etc.setSortRank(i);
            }
            ++i;
        }
    }

    List<TableColumn> getSortedColumns() {
        return this.sortedColumns;
    }

    public void clean() {
        ArrayList<TableColumn> allColumns = new ArrayList<TableColumn>(this.tableColumns.size() + this.hiddenColumns.size());
        allColumns.addAll(this.tableColumns);
        allColumns.addAll(this.hiddenColumns);
        for (TableColumn tc : allColumns) {
            this.removeColumn(tc);
        }
    }

    public TableColumnSelector.TreeNode getColumnHierarchyRoot() {
        return this.columnHierarchyRoot;
    }

    public void setColumnHierarchyRoot(TableColumnSelector.TreeNode columnHierarchyRoot) {
        this.columnHierarchyRoot = columnHierarchyRoot;
    }

    private class CompoundComparator
    implements Comparator<ETable.RowMapping> {
        private Comparator<ETable.RowMapping> original;

        public CompoundComparator() {
            this.original = new ETable.OriginalRowComparator();
        }

        @Override
        public int compare(ETable.RowMapping o1, ETable.RowMapping o2) {
            for (TableColumn o : ETableColumnModel.this.sortedColumns) {
                ETableColumn etc;
                int res;
                Comparator<ETable.RowMapping> c;
                if (!(o instanceof ETableColumn) || (c = (etc = (ETableColumn)o).getComparator()) == null || (res = c.compare(o1, o2)) == 0) continue;
                return res;
            }
            return this.original.compare(o1, o2);
        }
    }

}

