/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.etable;

import java.awt.datatransfer.Transferable;
import javax.swing.JComponent;
import javax.swing.TransferHandler;
import org.netbeans.swing.etable.ETable;
import org.netbeans.swing.etable.ETableTransferable;

public class ETableTransferHandler
extends TransferHandler {
    @Override
    protected Transferable createTransferable(JComponent c) {
        if (c instanceof ETable) {
            int[] rows;
            int counter;
            int[] cols;
            ETable table = (ETable)c;
            if (!table.getRowSelectionAllowed() && !table.getColumnSelectionAllowed()) {
                return null;
            }
            if (!table.getRowSelectionAllowed()) {
                int rowCount = table.getRowCount();
                rows = new int[rowCount];
                counter = 0;
                while (counter < rowCount) {
                    rows[counter] = counter++;
                }
            } else {
                rows = table.getSelectedRows();
            }
            if (!table.getColumnSelectionAllowed()) {
                int colCount = table.getColumnCount();
                cols = new int[colCount];
                counter = 0;
                while (counter < colCount) {
                    cols[counter] = counter++;
                }
            } else {
                cols = table.getSelectedColumns();
            }
            if (rows == null || cols == null || rows.length == 0 || cols.length == 0) {
                return null;
            }
            StringBuffer plainBuf = new StringBuffer();
            String itemDelim = table.getTransferDelimiter(false);
            String lineDelim = table.getTransferDelimiter(true);
            for (int row = 0; row < rows.length; ++row) {
                for (int col = 0; col < cols.length; ++col) {
                    Object obj = table.getValueAt(rows[row], cols[col]);
                    String val = table.convertValueToString(obj);
                    plainBuf.append(val + itemDelim);
                }
                plainBuf.delete(plainBuf.length() - itemDelim.length(), plainBuf.length() - 1);
                plainBuf.append(lineDelim);
            }
            plainBuf.deleteCharAt(plainBuf.length() - 1);
            return new ETableTransferable(plainBuf.toString());
        }
        return null;
    }

    @Override
    public int getSourceActions(JComponent c) {
        return 1;
    }
}

