/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.etable;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.plaf.UIResource;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.netbeans.swing.etable.ETable;
import org.netbeans.swing.etable.ETableColumn;
import org.netbeans.swing.etable.ETableColumnModel;

class ETableHeader
extends JTableHeader {
    public ETableHeader() {
    }

    public ETableHeader(TableColumnModel cm) {
        super(cm);
    }

    @Override
    public void setDefaultRenderer(TableCellRenderer defaultRenderer) {
        super.setDefaultRenderer(new ETableHeaderRenderer(defaultRenderer));
    }

    private static Icon mergeIcons(Icon icon1, Icon icon2, int x, int y, Component c) {
        int w = 0;
        int h = 0;
        if (icon1 != null) {
            w = icon1.getIconWidth();
            h = icon1.getIconHeight();
        }
        if (icon2 != null) {
            w = icon2.getIconWidth() + x > w ? icon2.getIconWidth() + x : w;
            int n = h = icon2.getIconHeight() + y > h ? icon2.getIconHeight() + y : h;
        }
        if (w < 1) {
            w = 16;
        }
        if (h < 1) {
            h = 16;
        }
        ColorModel model = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getColorModel(2);
        BufferedImage buffImage = new BufferedImage(model, model.createCompatibleWritableRaster(w, h), model.isAlphaPremultiplied(), null);
        Graphics2D g = buffImage.createGraphics();
        if (icon1 != null) {
            icon1.paintIcon(c, g, 0, 0);
        }
        if (icon2 != null) {
            icon2.paintIcon(c, g, x, y);
        }
        g.dispose();
        return new ImageIcon(buffImage);
    }

    private static class SortUpIcon
    implements Icon {
        @Override
        public int getIconWidth() {
            return 8;
        }

        @Override
        public int getIconHeight() {
            return 8;
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            g.setColor(Color.BLACK);
            g.drawLine(x, y + 6, x + 8, y + 6);
            g.drawLine(x, y + 6, x + 4, y + 2);
            g.drawLine(x + 8, y + 6, x + 4, y + 2);
        }
    }

    private static class SortDownIcon
    implements Icon {
        @Override
        public int getIconWidth() {
            return 8;
        }

        @Override
        public int getIconHeight() {
            return 8;
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            g.setColor(Color.BLACK);
            g.drawLine(x, y + 2, x + 8, y + 2);
            g.drawLine(x, y + 2, x + 4, y + 6);
            g.drawLine(x + 8, y + 2, x + 4, y + 6);
        }
    }

    private class ETableHeaderRenderer
    extends DefaultTableCellRenderer
    implements TableCellRenderer,
    UIResource {
        private TableCellRenderer headerRendererUI;
        private Map<ETableColumn, TableCellRenderer> defaultColumnHeaderRenderers;

        private ETableHeaderRenderer(TableCellRenderer headerRenderer) {
            this.defaultColumnHeaderRenderers = new HashMap<ETableColumn, TableCellRenderer>();
            this.headerRendererUI = headerRenderer;
            this.setName("TableHeader.renderer");
        }

        @Override
        public void updateUI() {
            TableCellRenderer defaultTableHeaderRenderer = this.headerRendererUI;
            if (defaultTableHeaderRenderer instanceof JComponent) {
                ((JComponent)((Object)defaultTableHeaderRenderer)).updateUI();
            } else {
                super.updateUI();
            }
        }

        private TableCellRenderer getColumnHeaderRenderer(TableColumn tc) {
            if (tc instanceof ETableColumn) {
                TableCellRenderer columnHeaderRenderer;
                ETableColumn eColumn = (ETableColumn)tc;
                if (!this.defaultColumnHeaderRenderers.containsKey(eColumn)) {
                    TableCellRenderer tcr = eColumn.createDefaultHeaderRenderer();
                    if (tcr instanceof ETableColumn.ETableColumnHeaderRendererDelegate) {
                        this.defaultColumnHeaderRenderers.put(eColumn, null);
                        columnHeaderRenderer = null;
                    } else {
                        eColumn.setTableHeaderRendererDelegate(this.headerRendererUI);
                        columnHeaderRenderer = tcr;
                    }
                } else {
                    columnHeaderRenderer = this.defaultColumnHeaderRenderers.get(eColumn);
                }
                if (columnHeaderRenderer != null) {
                    return columnHeaderRenderer;
                }
                return this.headerRendererUI;
            }
            return this.headerRendererUI;
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            TableColumn tcm = ETableHeader.this.getColumnModel().getColumn(column);
            TableCellRenderer headerRenderer = this.getColumnHeaderRenderer(tcm);
            Component resUI = headerRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            if (resUI instanceof JLabel) {
                JLabel label = (JLabel)resUI;
                String valueString = "";
                if (value != null) {
                    valueString = value.toString();
                }
                if (table instanceof ETable) {
                    ETable et = (ETable)table;
                    valueString = et.getColumnDisplayName(valueString);
                }
                Icon sortIcon = null;
                List<TableColumn> sortedColumns = ((ETableColumnModel)table.getColumnModel()).getSortedColumns();
                int sortRank = 0;
                boolean ascending = false;
                Icon customIcon = null;
                if (tcm instanceof ETableColumn) {
                    ETableColumn eColumn = (ETableColumn)tcm;
                    sortRank = eColumn.getSortRank();
                    ascending = eColumn.isAscending();
                    customIcon = eColumn.getCustomIcon();
                }
                if (sortRank != 0) {
                    if (sortedColumns.size() > 1) {
                        valueString = valueString == null || valueString.isEmpty() ? Integer.toString(sortRank) : "" + sortRank + " " + valueString;
                    }
                    label.setFont(new Font(label.getFont().getName(), 1, label.getFont().getSize()));
                    if (ascending) {
                        sortIcon = UIManager.getIcon("ETableHeader.ascendingIcon");
                        if (sortIcon == null) {
                            sortIcon = new SortUpIcon();
                        }
                    } else {
                        sortIcon = UIManager.getIcon("ETableHeader.descendingIcon");
                        if (sortIcon == null) {
                            sortIcon = new SortDownIcon();
                        }
                    }
                }
                label.setText(valueString);
                if (sortIcon == null) {
                    label.setIcon(customIcon);
                } else if (customIcon == null) {
                    label.setIcon(sortIcon);
                } else {
                    label.setIcon(ETableHeader.mergeIcons(customIcon, sortIcon, 16, 0, label));
                }
            }
            return resUI;
        }
    }

}

