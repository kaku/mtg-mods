/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.etable;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.io.StringReader;

public class ETableTransferable
implements Transferable {
    protected String plainData;
    private static DataFlavor[] stringFlavors;
    private static DataFlavor[] plainFlavors;

    public ETableTransferable(String plainData) {
        this.plainData = plainData;
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        int nPlain = this.isPlainSupported() ? plainFlavors.length : 0;
        int nString = this.isPlainSupported() ? stringFlavors.length : 0;
        int nFlavors = nPlain + nString;
        DataFlavor[] flavors = new DataFlavor[nFlavors];
        int nDone = 0;
        if (nPlain > 0) {
            System.arraycopy(plainFlavors, 0, flavors, nDone, nPlain);
            nDone += nPlain;
        }
        if (nString > 0) {
            System.arraycopy(stringFlavors, 0, flavors, nDone, nString);
            nDone += nString;
        }
        return flavors;
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        DataFlavor[] flavors = this.getTransferDataFlavors();
        for (int i = 0; i < flavors.length; ++i) {
            if (!flavors[i].equals(flavor)) continue;
            return true;
        }
        return false;
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        if (this.isPlainFlavor(flavor)) {
            String data = this.getPlainData();
            String string = data = data == null ? "" : data;
            if (String.class.equals(flavor.getRepresentationClass())) {
                return data;
            }
            if (Reader.class.equals(flavor.getRepresentationClass())) {
                return new StringReader(data);
            }
            if (InputStream.class.equals(flavor.getRepresentationClass())) {
                return new ByteArrayInputStream(data.getBytes());
            }
        } else if (this.isStringFlavor(flavor)) {
            String data = this.getPlainData();
            data = data == null ? "" : data;
            return data;
        }
        throw new UnsupportedFlavorException(flavor);
    }

    protected boolean isPlainFlavor(DataFlavor flavor) {
        DataFlavor[] flavors = plainFlavors;
        for (int i = 0; i < flavors.length; ++i) {
            if (!flavors[i].equals(flavor)) continue;
            return true;
        }
        return false;
    }

    protected boolean isPlainSupported() {
        return this.plainData != null;
    }

    protected String getPlainData() {
        return this.plainData;
    }

    protected boolean isStringFlavor(DataFlavor flavor) {
        DataFlavor[] flavors = stringFlavors;
        for (int i = 0; i < flavors.length; ++i) {
            if (!flavors[i].equals(flavor)) continue;
            return true;
        }
        return false;
    }

    static {
        try {
            plainFlavors = new DataFlavor[3];
            ETableTransferable.plainFlavors[0] = new DataFlavor("text/plain;class=java.lang.String");
            ETableTransferable.plainFlavors[1] = new DataFlavor("text/plain;class=java.io.Reader");
            ETableTransferable.plainFlavors[2] = new DataFlavor("text/plain;charset=unicode;class=java.io.InputStream");
            stringFlavors = new DataFlavor[2];
            ETableTransferable.stringFlavors[0] = new DataFlavor("application/x-java-jvm-local-objectref;class=java.lang.String");
            ETableTransferable.stringFlavors[1] = DataFlavor.stringFlavor;
        }
        catch (ClassNotFoundException cle) {
            System.err.println("error initializing SheetTasbleTransferable");
        }
    }
}

