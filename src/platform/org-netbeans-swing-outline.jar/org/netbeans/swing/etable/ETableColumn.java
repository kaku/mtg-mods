/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.etable;

import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Properties;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.netbeans.swing.etable.ETable;

public class ETableColumn
extends TableColumn
implements Comparable<ETableColumn> {
    static final String PROP_PREFIX = "ETableColumn-";
    private static final String PROP_WIDTH = "Width";
    private static final String PROP_PREFERRED_WIDTH = "PreferredWidth";
    private static final String PROP_SORT_RANK = "SortRank";
    private static final String PROP_COMPARATOR = "Comparator";
    private static final String PROP_HEADER_VALUE = "HeaderValue";
    private static final String PROP_MODEL_INDEX = "ModelIndex";
    private static final String PROP_ASCENDING = "Ascending";
    private int sortRank = 0;
    private Comparator<ETable.RowMapping> comparator;
    private boolean ascending = true;
    private ETable table;
    private Icon customIcon;
    private TableCellRenderer myHeaderRenderer;
    Comparator nestedComparator;

    public ETableColumn(ETable table) {
        this.table = table;
    }

    public ETableColumn(int modelIndex, ETable table) {
        super(modelIndex);
        this.table = table;
    }

    public ETableColumn(int modelIndex, int width, ETable table) {
        super(modelIndex, width);
        this.table = table;
    }

    public ETableColumn(int modelIndex, int width, TableCellRenderer cellRenderer, TableCellEditor cellEditor, ETable table) {
        super(modelIndex, width, cellRenderer, cellEditor);
        this.table = table;
    }

    @Deprecated
    public void setSorted(int rank, boolean ascending) {
        if (!(this.isSortingAllowed() || rank == 0 && this.comparator == null)) {
            throw new IllegalStateException("Cannot sort an unsortable column.");
        }
        this.ascending = ascending;
        this.sortRank = rank;
        this.comparator = rank != 0 ? this.getRowComparator(this.getModelIndex(), ascending) : null;
    }

    public boolean isSorted() {
        return this.comparator != null;
    }

    public void setSortRank(int newRank) {
        if (!this.isSortingAllowed() && newRank != 0) {
            throw new IllegalStateException("Cannot sort an unsortable column.");
        }
        this.sortRank = newRank;
    }

    public int getSortRank() {
        return this.sortRank;
    }

    Comparator<ETable.RowMapping> getComparator() {
        return this.comparator;
    }

    public boolean isAscending() {
        return this.ascending;
    }

    public void setAscending(boolean ascending) {
        if (!this.isSortingAllowed()) {
            throw new IllegalStateException("Cannot sort an unsortable column.");
        }
        if (!this.isSorted()) {
            return;
        }
        if (this.ascending == ascending) {
            return;
        }
        Comparator<ETable.RowMapping> c = this.getRowComparator(this.getModelIndex(), ascending);
        if (c == null) {
            throw new IllegalStateException("getRowComparator returned null for " + this);
        }
        this.ascending = ascending;
        this.comparator = c;
    }

    @Override
    public void setHeaderRenderer(TableCellRenderer tcr) {
        super.setHeaderRenderer(tcr);
    }

    public boolean isHidingAllowed() {
        return true;
    }

    public boolean isSortingAllowed() {
        return true;
    }

    public void setCustomIcon(Icon i) {
        this.customIcon = i;
    }

    Icon getCustomIcon() {
        return this.customIcon;
    }

    void updatePreferredWidth(JTable table, boolean resize) {
        TableModel dataModel = table.getModel();
        int rows = dataModel.getRowCount();
        if (rows == 0) {
            return;
        }
        int sum = 0;
        int max = 15;
        for (int i = 0; i < rows; ++i) {
            Object data = dataModel.getValueAt(i, this.modelIndex);
            int estimate = this.estimatedWidth(data, table);
            sum += estimate;
            if (estimate <= max) continue;
            max = estimate;
        }
        this.setPreferredWidth(max += 5);
        if (resize) {
            this.resize(max, table);
        }
    }

    private void resize(int newWidth, JTable table) {
        Container container;
        int oldWidth = this.getWidth();
        JTableHeader header = table.getTableHeader();
        if (header == null) {
            return;
        }
        header.setResizingColumn(this);
        final int oldMin = this.getMinWidth();
        final int oldMax = this.getMaxWidth();
        this.setMinWidth(newWidth);
        this.setMaxWidth(newWidth);
        this.setWidth(newWidth);
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                ETableColumn.this.setMinWidth(oldMin);
                ETableColumn.this.setMaxWidth(oldMax);
            }
        });
        if (header.getParent() == null || (container = header.getParent().getParent()) == null || !(container instanceof JScrollPane)) {
            header.setResizingColumn(null);
            return;
        }
        if (!container.getComponentOrientation().isLeftToRight() && !header.getComponentOrientation().isLeftToRight() && table != null) {
            JViewport viewport = ((JScrollPane)container).getViewport();
            int viewportWidth = viewport.getWidth();
            int diff = newWidth - oldWidth;
            int newHeaderWidth = table.getWidth() + diff;
            Dimension tableSize = table.getSize();
            tableSize.width += diff;
            table.setSize(tableSize);
            if (newHeaderWidth >= viewportWidth && table.getAutoResizeMode() == 0) {
                Point p = viewport.getViewPosition();
                p.x = Math.max(0, Math.min(newHeaderWidth - viewportWidth, p.x + diff));
                viewport.setViewPosition(p);
            }
        }
        header.setResizingColumn(null);
    }

    private int estimatedWidth(Object dataObject, JTable table) {
        Class c;
        TableCellRenderer cr = this.getCellRenderer();
        if (cr == null) {
            c = table.getModel().getColumnClass(this.modelIndex);
            cr = table.getDefaultRenderer(c);
        }
        c = cr.getTableCellRendererComponent(table, dataObject, false, false, 0, table.getColumnModel().getColumnIndex(this.getIdentifier()));
        return c.getPreferredSize().width;
    }

    public void readSettings(Properties p, int index, String propertyPrefix) {
        String s1;
        String s2;
        String s3;
        String myPrefix = propertyPrefix + "ETableColumn-" + Integer.toString(index) + "-";
        String s0 = p.getProperty(myPrefix + "ModelIndex");
        if (s0 != null) {
            this.modelIndex = Integer.parseInt(s0);
        }
        if ((s1 = p.getProperty(myPrefix + "Width")) != null) {
            this.width = Integer.parseInt(s1);
        }
        if ((s2 = p.getProperty(myPrefix + "PreferredWidth")) != null) {
            this.setPreferredWidth(Integer.parseInt(s2));
        }
        this.ascending = true;
        String s4 = p.getProperty(myPrefix + "Ascending");
        if ("false".equals(s4)) {
            this.ascending = false;
        }
        if ((s3 = p.getProperty(myPrefix + "SortRank")) != null) {
            this.sortRank = Integer.parseInt(s3);
            if (this.sortRank > 0) {
                this.comparator = this.getRowComparator(this.modelIndex, this.ascending);
            }
        }
        this.headerValue = p.getProperty(myPrefix + "HeaderValue");
    }

    public void writeSettings(Properties p, int index, String propertyPrefix) {
        String myPrefix = propertyPrefix + "ETableColumn-" + Integer.toString(index) + "-";
        p.setProperty(myPrefix + "ModelIndex", Integer.toString(this.modelIndex));
        p.setProperty(myPrefix + "Width", Integer.toString(this.width));
        p.setProperty(myPrefix + "PreferredWidth", Integer.toString(this.getPreferredWidth()));
        p.setProperty(myPrefix + "SortRank", Integer.toString(this.sortRank));
        p.setProperty(myPrefix + "Ascending", this.ascending ? "true" : "false");
        if (this.headerValue != null) {
            p.setProperty(myPrefix + "HeaderValue", this.headerValue.toString());
        }
    }

    @Override
    public int compareTo(ETableColumn obj) {
        if (this.modelIndex < obj.modelIndex) {
            return -1;
        }
        if (this.modelIndex > obj.modelIndex) {
            return 1;
        }
        return 0;
    }

    protected Comparator<ETable.RowMapping> getRowComparator(int column, boolean ascending) {
        if (ascending) {
            return new RowComparator(column);
        }
        return new FlippingComparator(new RowComparator(column));
    }

    public void setNestedComparator(Comparator c) {
        this.nestedComparator = c;
    }

    public Comparator getNestedComparator() {
        return this.nestedComparator;
    }

    @Override
    protected TableCellRenderer createDefaultHeaderRenderer() {
        return new ETableColumnHeaderRendererDelegate();
    }

    void setTableHeaderRendererDelegate(TableCellRenderer delegate) {
        this.myHeaderRenderer = delegate;
    }

    private static Icon mergeIcons(Icon icon1, Icon icon2, int x, int y, Component c) {
        int w = 0;
        int h = 0;
        if (icon1 != null) {
            w = icon1.getIconWidth();
            h = icon1.getIconHeight();
        }
        if (icon2 != null) {
            w = icon2.getIconWidth() + x > w ? icon2.getIconWidth() + x : w;
            int n = h = icon2.getIconHeight() + y > h ? icon2.getIconHeight() + y : h;
        }
        if (w < 1) {
            w = 16;
        }
        if (h < 1) {
            h = 16;
        }
        ColorModel model = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getColorModel(2);
        BufferedImage buffImage = new BufferedImage(model, model.createCompatibleWritableRaster(w, h), model.isAlphaPremultiplied(), null);
        Graphics2D g = buffImage.createGraphics();
        if (icon1 != null) {
            icon1.paintIcon(c, g, 0, 0);
        }
        if (icon2 != null) {
            icon2.paintIcon(c, g, x, y);
        }
        g.dispose();
        return new ImageIcon(buffImage);
    }

    public class RowComparator
    implements Comparator<ETable.RowMapping> {
        protected int column;

        public RowComparator(int column) {
            this.column = column;
        }

        @Override
        public int compare(ETable.RowMapping rm1, ETable.RowMapping rm2) {
            Object obj1 = rm1.getTransformedValue(this.column);
            Object obj2 = rm2.getTransformedValue(this.column);
            if (obj1 == null && obj2 == null) {
                return 0;
            }
            if (obj1 == null) {
                return -1;
            }
            if (obj2 == null) {
                return 1;
            }
            if (ETableColumn.this.getNestedComparator() != null) {
                return ETableColumn.this.getNestedComparator().compare(obj1, obj2);
            }
            Class cl1 = obj1.getClass();
            Class cl2 = obj2.getClass();
            if (obj1 instanceof Comparable && cl1.isAssignableFrom(cl2)) {
                Comparable c1 = (Comparable)obj1;
                return c1.compareTo(obj2);
            }
            if (obj2 instanceof Comparable && cl2.isAssignableFrom(cl1)) {
                Comparable c2 = (Comparable)obj2;
                return - c2.compareTo(obj1);
            }
            return obj1.toString().compareTo(obj2.toString());
        }
    }

    static class FlippingComparator
    implements Comparator<ETable.RowMapping> {
        private Comparator<ETable.RowMapping> origComparator;

        public FlippingComparator(Comparator<ETable.RowMapping> orig) {
            this.origComparator = orig;
        }

        @Override
        public int compare(ETable.RowMapping o1, ETable.RowMapping o2) {
            return - this.origComparator.compare(o1, o2);
        }

        public Comparator<ETable.RowMapping> getOriginalComparator() {
            return this.origComparator;
        }
    }

    class ETableColumnHeaderRendererDelegate
    implements TableCellRenderer {
        ETableColumnHeaderRendererDelegate() {
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            return ETableColumn.this.myHeaderRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
    }

}

