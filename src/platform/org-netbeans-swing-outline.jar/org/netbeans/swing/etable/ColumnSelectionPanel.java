/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.etable;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.netbeans.swing.etable.ETable;
import org.netbeans.swing.etable.ETableColumn;
import org.netbeans.swing.etable.ETableColumnModel;
import org.netbeans.swing.etable.TableColumnSelector;

class ColumnSelectionPanel
extends JPanel {
    private static final String COLUMNS_SELECTOR_HINT = "ColumnsSelectorHint";
    private Map<ETableColumn, JCheckBox> checkBoxes = new HashMap<ETableColumn, JCheckBox>();
    private ETableColumnModel columnModel;

    public ColumnSelectionPanel(ETable table) {
        ETableColumnModel etcm;
        TableColumnModel colModel = table.getColumnModel();
        if (!(colModel instanceof ETableColumnModel)) {
            return;
        }
        this.columnModel = etcm = (ETableColumnModel)colModel;
        List<TableColumn> columns = etcm.getAllColumns();
        Collections.sort(columns, ETableColumnComparator.DEFAULT);
        int width = 1;
        JPanel p = this.layoutPanel(columns, width, table);
        Dimension prefSize = p.getPreferredSize();
        final Rectangle screenBounds = ColumnSelectionPanel.getUsableScreenBounds(ColumnSelectionPanel.getCurrentGraphicsConfiguration());
        JComponent toAdd = null;
        if (prefSize.width > screenBounds.width - 100 || prefSize.height > screenBounds.height - 100) {
            JScrollPane currentScrollPane = new JScrollPane(){

                @Override
                public Dimension getPreferredSize() {
                    Dimension sz = new Dimension(super.getPreferredSize());
                    if (sz.width > screenBounds.width - 100) {
                        sz.width = screenBounds.width * 3 / 4;
                    }
                    if (sz.height > screenBounds.height - 100) {
                        sz.height = screenBounds.height * 3 / 4;
                    }
                    return sz;
                }
            };
            currentScrollPane.setViewportView(p);
            toAdd = currentScrollPane;
        } else {
            toAdd = p;
        }
        this.add(toAdd);
    }

    private JPanel layoutPanel(List<TableColumn> columns, int width, ETable table) {
        JPanel toAdd = new JPanel(new GridBagLayout());
        HashMap<String, Serializable> displayNameToCheckBox = new HashMap<String, Serializable>();
        ArrayList<String> displayNames = new ArrayList<String>();
        for (int col = 0; col < columns.size(); ++col) {
            ETableColumn etc = (ETableColumn)columns.get(col);
            JCheckBox checkBox = new JCheckBox();
            Object transformed = table.transformValue(etc);
            String dName = transformed == etc || transformed == null ? table.getColumnDisplayName(etc.getHeaderValue().toString()) : transformed.toString();
            checkBox.setText(dName);
            JCheckBox transfCheckBox = (JCheckBox)table.transformValue(checkBox);
            if (transfCheckBox != null) {
                checkBox = transfCheckBox;
            }
            this.checkBoxes.put(etc, checkBox);
            checkBox.setSelected(!this.columnModel.isColumnHidden(etc));
            checkBox.setEnabled(etc.isHidingAllowed());
            if (!displayNames.contains(dName)) {
                displayNameToCheckBox.put(dName, checkBox);
            } else {
                ArrayList<JCheckBox> al = null;
                Object theFirstOne = displayNameToCheckBox.get(dName);
                if (theFirstOne instanceof JCheckBox) {
                    JCheckBox firstCheckBox = (JCheckBox)theFirstOne;
                    al = new ArrayList<JCheckBox>();
                    al.add(firstCheckBox);
                } else if (theFirstOne instanceof ArrayList) {
                    al = (ArrayList<JCheckBox>)theFirstOne;
                } else {
                    throw new IllegalStateException("Wrong object theFirstOne is " + theFirstOne);
                }
                al.add(checkBox);
                displayNameToCheckBox.put(dName, al);
            }
            displayNames.add(dName);
        }
        String first = (String)displayNames.remove(0);
        Collections.sort(displayNames, Collator.getInstance());
        displayNames.add(0, first);
        int i = 0;
        int j = 0;
        int index = 0;
        int rows = columns.size() / width;
        Object hint = table.transformValue("ColumnsSelectorHint");
        if (hint == "ColumnsSelectorHint") {
            hint = ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("ColumnsSelectorHint");
        }
        if (hint != null) {
            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = 0;
            gridBagConstraints.insets = new Insets(5, 12, 12, 12);
            gridBagConstraints.anchor = 18;
            toAdd.add((Component)new JLabel(hint.toString()), gridBagConstraints);
        }
        Iterator it = displayNames.iterator();
        while (it.hasNext()) {
            if (i >= rows) {
                i = 0;
                ++j;
            }
            String displayName = (String)it.next();
            Object obj = displayNameToCheckBox.get(displayName);
            JCheckBox checkBox = null;
            if (obj instanceof JCheckBox) {
                checkBox = (JCheckBox)obj;
            } else if (obj instanceof ArrayList) {
                ArrayList al = (ArrayList)obj;
                if (index >= al.size()) {
                    index = 0;
                }
                checkBox = (JCheckBox)al.get(index++);
            } else {
                throw new IllegalStateException("Wrong object obj is " + obj);
            }
            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = j;
            gridBagConstraints.gridy = i + (hint == null ? i : i + 1);
            gridBagConstraints.insets = new Insets(0, 12, 0, 12);
            gridBagConstraints.anchor = 18;
            gridBagConstraints.weightx = 1.0;
            toAdd.add((Component)checkBox, gridBagConstraints);
            ++i;
        }
        return toAdd;
    }

    public void changeColumnVisibility() {
        if (this.columnModel == null) {
            return;
        }
        Iterator<ETableColumn> it = this.checkBoxes.keySet().iterator();
        while (it.hasNext()) {
            ETableColumn etc;
            JCheckBox checkBox = this.checkBoxes.get(etc = it.next());
            this.columnModel.setColumnHidden(etc, !checkBox.isSelected());
        }
    }

    static void showColumnSelectionPopupOrDialog(Component c, ETable table) {
        if (table.isPopupUsedFromTheCorner()) {
            ColumnSelectionPanel.showColumnSelectionPopup(c, table);
        } else {
            ColumnSelectionPanel.showColumnSelectionDialog(table);
        }
    }

    static void showColumnSelectionPopup(Component c, ETable table) {
        ColumnSelectionPanel.showColumnSelectionPopup(c, 8, 8, table);
    }

    static void showColumnSelectionPopup(Component c, int posx, int posy, final ETable table) {
        if (!table.isColumnHidingAllowed()) {
            return;
        }
        JPopupMenu popup = new JPopupMenu();
        TableColumnModel columnModel = table.getColumnModel();
        if (!(columnModel instanceof ETableColumnModel)) {
            return;
        }
        TableColumnSelector tcs = table.getColumnSelector();
        if (tcs != null && !table.isPopupUsedFromTheCorner()) {
            JMenuItem selector = new JMenuItem(table.getSelectVisibleColumnsLabel());
            selector.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    ColumnSelectionPanel.showColumnSelectionDialog(table);
                }
            });
            popup.add(selector);
        } else {
            final ETableColumnModel etcm = (ETableColumnModel)columnModel;
            List<TableColumn> columns = etcm.getAllColumns();
            Collections.sort(columns, ETableColumnComparator.DEFAULT);
            HashMap<String, Serializable> displayNameToCheckBox = new HashMap<String, Serializable>();
            ArrayList<String> displayNames = new ArrayList<String>();
            for (final ETableColumn etc : columns) {
                JCheckBoxMenuItem checkBox = new JCheckBoxMenuItem();
                Object transformed = table.transformValue(etc);
                String dName = transformed == etc || transformed == null ? table.getColumnDisplayName(etc.getHeaderValue().toString()) : transformed.toString();
                checkBox.setText(dName);
                checkBox = (JCheckBoxMenuItem)table.transformValue(checkBox);
                checkBox.setSelected(!etcm.isColumnHidden(etc));
                checkBox.setEnabled(etc.isHidingAllowed());
                final JCheckBoxMenuItem finalChB = checkBox;
                checkBox.addActionListener(new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        etcm.setColumnHidden(etc, !finalChB.isSelected());
                        table.updateColumnSelectionMouseListener();
                    }
                });
                if (!displayNames.contains(dName)) {
                    displayNameToCheckBox.put(dName, checkBox);
                } else {
                    ArrayList<JCheckBoxMenuItem> al = null;
                    Object theFirstOne = displayNameToCheckBox.get(dName);
                    if (theFirstOne instanceof JCheckBoxMenuItem) {
                        JCheckBoxMenuItem firstCheckBox = (JCheckBoxMenuItem)theFirstOne;
                        al = new ArrayList<JCheckBoxMenuItem>();
                        al.add(firstCheckBox);
                    } else if (theFirstOne instanceof ArrayList) {
                        al = (ArrayList<JCheckBoxMenuItem>)theFirstOne;
                    } else {
                        throw new IllegalStateException("Wrong object theFirstOne is " + theFirstOne);
                    }
                    al.add(checkBox);
                    displayNameToCheckBox.put(dName, al);
                }
                displayNames.add(dName);
            }
            Collections.sort(displayNames, Collator.getInstance());
            int index = 0;
            for (String displayName : displayNames) {
                Object obj = displayNameToCheckBox.get(displayName);
                JCheckBoxMenuItem checkBox = null;
                if (obj instanceof JCheckBoxMenuItem) {
                    checkBox = (JCheckBoxMenuItem)obj;
                } else if (obj instanceof ArrayList) {
                    ArrayList al = (ArrayList)obj;
                    if (index >= al.size()) {
                        index = 0;
                    }
                    checkBox = (JCheckBoxMenuItem)al.get(index++);
                } else {
                    throw new IllegalStateException("Wrong object obj is " + obj);
                }
                popup.add(checkBox);
            }
        }
        popup.show(c, posx, posy);
    }

    static void showColumnSelectionDialog(ETable table) {
        if (!table.isColumnHidingAllowed()) {
            return;
        }
        TableColumnSelector tcs = table.getColumnSelector();
        if (tcs != null) {
            ETableColumnModel etcm = (ETableColumnModel)table.getColumnModel();
            TableColumnSelector.TreeNode root = etcm.getColumnHierarchyRoot();
            if (root != null) {
                String[] origVisible = ColumnSelectionPanel.getAvailableColumnNames(table, true);
                String[] visibleColumns = tcs.selectVisibleColumns(root, origVisible);
                ColumnSelectionPanel.makeVisibleColumns(table, visibleColumns);
            } else {
                String[] availableColumns = ColumnSelectionPanel.getAvailableColumnNames(table, false);
                String[] origVisible = ColumnSelectionPanel.getAvailableColumnNames(table, true);
                String[] visibleColumns = tcs.selectVisibleColumns(availableColumns, origVisible);
                ColumnSelectionPanel.makeVisibleColumns(table, visibleColumns);
            }
            return;
        }
        ColumnSelectionPanel panel = new ColumnSelectionPanel(table);
        int res = JOptionPane.showConfirmDialog(table, panel, table.getSelectVisibleColumnsLabel(), 2);
        if (res == 0) {
            panel.changeColumnVisibility();
            table.updateColumnSelectionMouseListener();
        }
    }

    private static void makeVisibleColumns(ETable table, String[] visibleColumns) {
        ETableColumn etc;
        HashSet<String> visible = new HashSet<String>(Arrays.asList(visibleColumns));
        TableColumnModel columnModel = table.getColumnModel();
        if (!(columnModel instanceof ETableColumnModel)) {
            return;
        }
        ETableColumnModel etcm = (ETableColumnModel)columnModel;
        List<TableColumn> columns = etcm.getAllColumns();
        Collections.sort(columns, ETableColumnComparator.DEFAULT);
        HashMap<String, ETableColumn> nameToColumn = new HashMap<String, ETableColumn>();
        Iterator<TableColumn> it = columns.iterator();
        while (it.hasNext()) {
            String dName = table.getColumnDisplayName((etc = (ETableColumn)it.next()).getHeaderValue().toString());
            etcm.setColumnHidden(etc, !visible.contains(dName));
            nameToColumn.put(dName, etc);
        }
        for (int i = 0; i < visibleColumns.length; ++i) {
            etc = (ETableColumn)nameToColumn.get(visibleColumns[i]);
            if (etc == null) {
                throw new IllegalStateException("Cannot find column with name " + visibleColumns[i]);
            }
            int currentIndex = etcm.getColumnIndex(etc.getIdentifier());
            etcm.moveColumn(currentIndex, i);
        }
    }

    private static String[] getAvailableColumnNames(ETable table, boolean visibleOnly) {
        TableColumnModel columnModel = table.getColumnModel();
        if (!(columnModel instanceof ETableColumnModel)) {
            return new String[0];
        }
        ETableColumnModel etcm = (ETableColumnModel)columnModel;
        List<TableColumn> columns = visibleOnly ? Collections.list(etcm.getColumns()) : etcm.getAllColumns();
        Collections.sort(columns, ETableColumnComparator.DEFAULT);
        ArrayList<String> displayNames = new ArrayList<String>();
        for (ETableColumn etc : columns) {
            String dName = table.getColumnDisplayName(etc.getHeaderValue().toString());
            displayNames.add(dName);
        }
        Collections.sort(displayNames, Collator.getInstance());
        return displayNames.toArray(new String[displayNames.size()]);
    }

    private static GraphicsConfiguration getCurrentGraphicsConfiguration() {
        Window w;
        Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (focusOwner != null && (w = SwingUtilities.getWindowAncestor(focusOwner)) != null) {
            return w.getGraphicsConfiguration();
        }
        return GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
    }

    private static Rectangle getUsableScreenBounds(GraphicsConfiguration gconf) {
        if (gconf == null) {
            gconf = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
        }
        return new Rectangle(gconf.getBounds());
    }

    private static class ETableColumnComparator
    implements Comparator<TableColumn> {
        public static final ETableColumnComparator DEFAULT = new ETableColumnComparator();

        private ETableColumnComparator() {
        }

        @Override
        public int compare(TableColumn o1, TableColumn o2) {
            if (o1 instanceof ETableColumn && o2 instanceof ETableColumn) {
                ((ETableColumn)o1).compareTo((ETableColumn)o2);
            }
            return 0;
        }
    }

}

