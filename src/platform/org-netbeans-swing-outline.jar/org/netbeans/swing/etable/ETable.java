/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 */
package org.netbeans.swing.etable;

import java.awt.AWTEvent;
import java.awt.AWTKeyStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.ContainerOrderFocusTraversalPolicy;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FocusTraversalPolicy;
import java.awt.Font;
import java.awt.Image;
import java.awt.KeyboardFocusManager;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.EventListener;
import java.util.EventObject;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.Vector;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TableModelEvent;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.text.Document;
import org.netbeans.swing.etable.ColumnSelectionPanel;
import org.netbeans.swing.etable.ETableColumn;
import org.netbeans.swing.etable.ETableColumnModel;
import org.netbeans.swing.etable.ETableHeader;
import org.netbeans.swing.etable.ETableSelectionModel;
import org.netbeans.swing.etable.ETableTransferHandler;
import org.netbeans.swing.etable.QuickFilter;
import org.netbeans.swing.etable.TableColumnSelector;
import org.netbeans.swing.outline.DefaultOutlineModel;
import org.openide.util.ImageUtilities;

public class ETable
extends JTable {
    public static final String PROP_QUICK_FILTER = "quickFilter";
    private static final String ACTION_FOCUS_NEXT = "focusNext";
    private static final int FULLY_EDITABLE = 1;
    private static final int FULLY_NONEDITABLE = 2;
    private static final int DEFAULT = 3;
    private static final String SEARCH_COLUMN = "SearchColumn";
    private static final String DEFAULT_COLUMNS_ICON = "columns.gif";
    private int editing = 3;
    private boolean sortable = true;
    protected transient int[] sortingPermutation;
    protected transient int[] inverseSortingPermutation;
    private transient int filteredRowCount;
    private Object quickFilterObject;
    private int quickFilterColumn = -1;
    private String maxPrefix;
    int SEARCH_FIELD_PREFERRED_SIZE = 160;
    int SEARCH_FIELD_SPACE = 3;
    private final JTextField searchTextField;
    private final int heightOfTextField;
    private JPanel searchPanel;
    private JComboBox searchCombo;
    private ETableColumn searchColumn;
    private String selectVisibleColumnsLabel;
    private boolean inEditRequest;
    private boolean inRemoveRequest;
    private static String COMPUTING_TOOLTIP = "ComputingTooltip";
    private String[] quickFilterFormatStrings;
    private MouseListener headerMouseListener;
    private MouseListener columnSelectionMouseListener;
    private TableColumnSelector columnSelector;
    private static TableColumnSelector defaultColumnSelector;
    private final Object columnSelectionOnMouseClickLock;
    private ColumnSelection[] columnSelectionOnMouseClick;
    private boolean columnHidingAllowed;
    private SelectedRows selectedRowsWhenTableChanged;
    private int[][] sortingPermutationsWhenTableChanged;
    private int selectedColumnWhenTableChanged;

    public ETable() {
        this.searchTextField = new SearchTextField();
        this.heightOfTextField = this.searchTextField.getPreferredSize().height;
        this.searchPanel = null;
        this.searchCombo = null;
        this.searchColumn = null;
        this.selectVisibleColumnsLabel = ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("LBL_SelectVisibleColumns");
        this.inEditRequest = false;
        this.inRemoveRequest = false;
        this.quickFilterFormatStrings = new String[]{"{0} == {1}", "{0} <> {1}", "{0} > {1}", "{0} < {1}", "{0} >= {1}", "{0} <= {1}", ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("LBL_NoFilter")};
        this.headerMouseListener = new HeaderMouseListener();
        this.columnSelectionMouseListener = new ColumnSelectionMouseListener();
        this.columnSelectionOnMouseClickLock = new Object();
        this.columnSelectionOnMouseClick = new ColumnSelection[]{ColumnSelection.NO_SELECTION, ColumnSelection.DIALOG, ColumnSelection.NO_SELECTION, ColumnSelection.POPUP};
        this.columnHidingAllowed = true;
        this.selectedRowsWhenTableChanged = null;
        this.sortingPermutationsWhenTableChanged = null;
        this.selectedColumnWhenTableChanged = -1;
        this.updateMouseListener();
    }

    public ETable(TableModel dm) {
        super(dm);
        this.searchTextField = new SearchTextField();
        this.heightOfTextField = this.searchTextField.getPreferredSize().height;
        this.searchPanel = null;
        this.searchCombo = null;
        this.searchColumn = null;
        this.selectVisibleColumnsLabel = ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("LBL_SelectVisibleColumns");
        this.inEditRequest = false;
        this.inRemoveRequest = false;
        this.quickFilterFormatStrings = new String[]{"{0} == {1}", "{0} <> {1}", "{0} > {1}", "{0} < {1}", "{0} >= {1}", "{0} <= {1}", ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("LBL_NoFilter")};
        this.headerMouseListener = new HeaderMouseListener();
        this.columnSelectionMouseListener = new ColumnSelectionMouseListener();
        this.columnSelectionOnMouseClickLock = new Object();
        this.columnSelectionOnMouseClick = new ColumnSelection[]{ColumnSelection.NO_SELECTION, ColumnSelection.DIALOG, ColumnSelection.NO_SELECTION, ColumnSelection.POPUP};
        this.columnHidingAllowed = true;
        this.selectedRowsWhenTableChanged = null;
        this.sortingPermutationsWhenTableChanged = null;
        this.selectedColumnWhenTableChanged = -1;
        this.updateMouseListener();
    }

    public ETable(TableModel dm, TableColumnModel cm) {
        super(dm, cm);
        this.searchTextField = new SearchTextField();
        this.heightOfTextField = this.searchTextField.getPreferredSize().height;
        this.searchPanel = null;
        this.searchCombo = null;
        this.searchColumn = null;
        this.selectVisibleColumnsLabel = ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("LBL_SelectVisibleColumns");
        this.inEditRequest = false;
        this.inRemoveRequest = false;
        this.quickFilterFormatStrings = new String[]{"{0} == {1}", "{0} <> {1}", "{0} > {1}", "{0} < {1}", "{0} >= {1}", "{0} <= {1}", ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("LBL_NoFilter")};
        this.headerMouseListener = new HeaderMouseListener();
        this.columnSelectionMouseListener = new ColumnSelectionMouseListener();
        this.columnSelectionOnMouseClickLock = new Object();
        this.columnSelectionOnMouseClick = new ColumnSelection[]{ColumnSelection.NO_SELECTION, ColumnSelection.DIALOG, ColumnSelection.NO_SELECTION, ColumnSelection.POPUP};
        this.columnHidingAllowed = true;
        this.selectedRowsWhenTableChanged = null;
        this.sortingPermutationsWhenTableChanged = null;
        this.selectedColumnWhenTableChanged = -1;
        this.updateMouseListener();
    }

    public ETable(TableModel dm, TableColumnModel cm, ListSelectionModel sm) {
        super(dm, cm, sm);
        this.searchTextField = new SearchTextField();
        this.heightOfTextField = this.searchTextField.getPreferredSize().height;
        this.searchPanel = null;
        this.searchCombo = null;
        this.searchColumn = null;
        this.selectVisibleColumnsLabel = ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("LBL_SelectVisibleColumns");
        this.inEditRequest = false;
        this.inRemoveRequest = false;
        this.quickFilterFormatStrings = new String[]{"{0} == {1}", "{0} <> {1}", "{0} > {1}", "{0} < {1}", "{0} >= {1}", "{0} <= {1}", ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("LBL_NoFilter")};
        this.headerMouseListener = new HeaderMouseListener();
        this.columnSelectionMouseListener = new ColumnSelectionMouseListener();
        this.columnSelectionOnMouseClickLock = new Object();
        this.columnSelectionOnMouseClick = new ColumnSelection[]{ColumnSelection.NO_SELECTION, ColumnSelection.DIALOG, ColumnSelection.NO_SELECTION, ColumnSelection.POPUP};
        this.columnHidingAllowed = true;
        this.selectedRowsWhenTableChanged = null;
        this.sortingPermutationsWhenTableChanged = null;
        this.selectedColumnWhenTableChanged = -1;
        this.updateMouseListener();
    }

    public ETable(int numRows, int numColumns) {
        super(numRows, numColumns);
        this.searchTextField = new SearchTextField();
        this.heightOfTextField = this.searchTextField.getPreferredSize().height;
        this.searchPanel = null;
        this.searchCombo = null;
        this.searchColumn = null;
        this.selectVisibleColumnsLabel = ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("LBL_SelectVisibleColumns");
        this.inEditRequest = false;
        this.inRemoveRequest = false;
        this.quickFilterFormatStrings = new String[]{"{0} == {1}", "{0} <> {1}", "{0} > {1}", "{0} < {1}", "{0} >= {1}", "{0} <= {1}", ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("LBL_NoFilter")};
        this.headerMouseListener = new HeaderMouseListener();
        this.columnSelectionMouseListener = new ColumnSelectionMouseListener();
        this.columnSelectionOnMouseClickLock = new Object();
        this.columnSelectionOnMouseClick = new ColumnSelection[]{ColumnSelection.NO_SELECTION, ColumnSelection.DIALOG, ColumnSelection.NO_SELECTION, ColumnSelection.POPUP};
        this.columnHidingAllowed = true;
        this.selectedRowsWhenTableChanged = null;
        this.sortingPermutationsWhenTableChanged = null;
        this.selectedColumnWhenTableChanged = -1;
        this.updateMouseListener();
    }

    public ETable(Vector rowData, Vector columnNames) {
        super(rowData, columnNames);
        this.searchTextField = new SearchTextField();
        this.heightOfTextField = this.searchTextField.getPreferredSize().height;
        this.searchPanel = null;
        this.searchCombo = null;
        this.searchColumn = null;
        this.selectVisibleColumnsLabel = ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("LBL_SelectVisibleColumns");
        this.inEditRequest = false;
        this.inRemoveRequest = false;
        this.quickFilterFormatStrings = new String[]{"{0} == {1}", "{0} <> {1}", "{0} > {1}", "{0} < {1}", "{0} >= {1}", "{0} <= {1}", ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("LBL_NoFilter")};
        this.headerMouseListener = new HeaderMouseListener();
        this.columnSelectionMouseListener = new ColumnSelectionMouseListener();
        this.columnSelectionOnMouseClickLock = new Object();
        this.columnSelectionOnMouseClick = new ColumnSelection[]{ColumnSelection.NO_SELECTION, ColumnSelection.DIALOG, ColumnSelection.NO_SELECTION, ColumnSelection.POPUP};
        this.columnHidingAllowed = true;
        this.selectedRowsWhenTableChanged = null;
        this.sortingPermutationsWhenTableChanged = null;
        this.selectedColumnWhenTableChanged = -1;
        this.updateMouseListener();
    }

    public ETable(Object[][] rowData, Object[] columnNames) {
        super(rowData, columnNames);
        this.searchTextField = new SearchTextField();
        this.heightOfTextField = this.searchTextField.getPreferredSize().height;
        this.searchPanel = null;
        this.searchCombo = null;
        this.searchColumn = null;
        this.selectVisibleColumnsLabel = ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("LBL_SelectVisibleColumns");
        this.inEditRequest = false;
        this.inRemoveRequest = false;
        this.quickFilterFormatStrings = new String[]{"{0} == {1}", "{0} <> {1}", "{0} > {1}", "{0} < {1}", "{0} >= {1}", "{0} <= {1}", ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("LBL_NoFilter")};
        this.headerMouseListener = new HeaderMouseListener();
        this.columnSelectionMouseListener = new ColumnSelectionMouseListener();
        this.columnSelectionOnMouseClickLock = new Object();
        this.columnSelectionOnMouseClick = new ColumnSelection[]{ColumnSelection.NO_SELECTION, ColumnSelection.DIALOG, ColumnSelection.NO_SELECTION, ColumnSelection.POPUP};
        this.columnHidingAllowed = true;
        this.selectedRowsWhenTableChanged = null;
        this.sortingPermutationsWhenTableChanged = null;
        this.selectedColumnWhenTableChanged = -1;
        this.updateMouseListener();
    }

    @Override
    protected ListSelectionModel createDefaultSelectionModel() {
        return new ETableSelectionModel();
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        if (this.editing == 1) {
            return true;
        }
        if (this.editing == 2) {
            return false;
        }
        return super.isCellEditable(row, column);
    }

    public void setFullyEditable(boolean fullyEditable) {
        if (fullyEditable) {
            this.editing = 1;
            if (!this.getShowHorizontalLines()) {
                this.setShowHorizontalLines(true);
            }
            Color colorBorderAllEditable = UIManager.getColor("Table.borderAllEditable");
            Border border = null;
            border = colorBorderAllEditable != null ? BorderFactory.createLineBorder(colorBorderAllEditable) : BorderFactory.createLineBorder(Color.GRAY);
            Border filler = BorderFactory.createLineBorder(this.getBackground());
            CompoundBorder compound = new CompoundBorder(border, filler);
            this.setBorder(new CompoundBorder(compound, border));
        } else {
            this.editing = 3;
            this.setBorder(null);
        }
        Color c = UIManager.getColor("Table.defaultGrid");
        if (c != null) {
            this.setGridColor(c);
        }
        if (this.isFullyNonEditable()) {
            this.setupSearch();
        }
    }

    public void setFullyNonEditable(boolean fullyNonEditable) {
        if (fullyNonEditable) {
            Color lineBorderColor;
            this.editing = 2;
            if (this.getShowHorizontalLines()) {
                this.setShowHorizontalLines(false);
            }
            if ((lineBorderColor = UIManager.getColor("Table.border")) == null) {
                lineBorderColor = Color.GRAY;
            }
            this.setBorder(BorderFactory.createLineBorder(lineBorderColor));
            Color c = UIManager.getColor("Table.noneditableGrid");
            if (c != null) {
                this.setGridColor(c);
            }
        } else {
            Color defaultGridColor;
            this.editing = 3;
            this.setBorder(null);
            if (!this.getShowHorizontalLines()) {
                this.setShowHorizontalLines(true);
            }
            if ((defaultGridColor = UIManager.getColor("Table.defaultGrid")) != null) {
                this.setGridColor(defaultGridColor);
            }
        }
        if (this.isFullyNonEditable()) {
            this.setupSearch();
        }
    }

    public boolean isFullyEditable() {
        return this.editing == 1;
    }

    public boolean isFullyNonEditable() {
        return this.editing == 2;
    }

    public void setCellBackground(Component renderer, boolean isSelected, int row, int column) {
        Color c = null;
        c = row % 2 == 0 ? (isSelected ? UIManager.getColor("Table.selectionBackground2") : UIManager.getColor("Table.background2")) : (isSelected ? UIManager.getColor("Table.selectionBackground1") : UIManager.getColor("Table.background1"));
        if (c != null) {
            renderer.setBackground(c);
        }
    }

    @Override
    public void createDefaultColumnsFromModel() {
        TableModel model = this.getModel();
        if (model != null) {
            this.firePropertyChange("createdDefaultColumnsFromModel", null, null);
            TableColumnModel colModel = this.getColumnModel();
            int modelColumnCount = model.getColumnCount();
            List<TableColumn> oldHiddenColumns = null;
            int[] hiddenColumnIndexes = null;
            int[] sortedColumnIndexes = null;
            if (colModel instanceof ETableColumnModel) {
                ETableColumnModel etcm = (ETableColumnModel)colModel;
                oldHiddenColumns = etcm.hiddenColumns;
                hiddenColumnIndexes = new int[oldHiddenColumns.size()];
                for (int hci = 0; hci < hiddenColumnIndexes.length; ++hci) {
                    Object name = oldHiddenColumns.get(hci).getHeaderValue();
                    int index = -1;
                    if (name != null) {
                        for (int i = 0; i < modelColumnCount; ++i) {
                            if (!name.equals(model.getColumnName(i))) continue;
                            index = i;
                            break;
                        }
                    }
                    hiddenColumnIndexes[hci] = index;
                }
                List<TableColumn> sortedColumns = this.sortable ? etcm.getSortedColumns() : Collections.EMPTY_LIST;
                int ns = sortedColumns.size();
                if (ns > 0) {
                    sortedColumnIndexes = new int[ns];
                    for (int sci = 0; sci < ns; ++sci) {
                        Object name = sortedColumns.get(sci).getHeaderValue();
                        int index = -1;
                        if (name != null) {
                            for (int i = 0; i < modelColumnCount; ++i) {
                                if (!name.equals(model.getColumnName(i))) continue;
                                index = i;
                                break;
                            }
                        }
                        sortedColumnIndexes[sci] = index;
                    }
                }
            }
            TableColumn[] newColumns = new TableColumn[modelColumnCount];
            int oi = 0;
            Sorting[] columnSorting = new Sorting[modelColumnCount];
            for (int i = 0; i < modelColumnCount; ++i) {
                class Sorting {
                    boolean ascending;
                    int sortRank;

                    Sorting(boolean ascending, int sortRank) {
                        this.ascending = ascending;
                        this.sortRank = sortRank;
                    }
                }
                newColumns[i] = this.createColumn(i);
                boolean isHidden = false;
                for (int hci = 0; hci < hiddenColumnIndexes.length; ++hci) {
                    if (hiddenColumnIndexes[hci] != i) continue;
                    isHidden = true;
                }
                if (oi >= colModel.getColumnCount()) continue;
                TableColumn tc = colModel.getColumn(oi++);
                if (!isHidden) {
                    newColumns[i].setPreferredWidth(tc.getPreferredWidth());
                    newColumns[i].setWidth(tc.getWidth());
                }
                if (!this.sortable || !(tc instanceof ETableColumn) || !(newColumns[i] instanceof ETableColumn)) continue;
                ETableColumn etc = (ETableColumn)tc;
                ETableColumn enc = (ETableColumn)newColumns[i];
                enc.nestedComparator = etc.nestedComparator;
                if (!enc.isSortingAllowed()) continue;
                columnSorting[i] = new Sorting(etc.isAscending(), etc.getSortRank());
            }
            int cc = colModel.getColumnCount();
            while (cc > 0) {
                colModel.removeColumn(colModel.getColumn(--cc));
            }
            if (colModel instanceof ETableColumnModel) {
                ETableColumnModel etcm = (ETableColumnModel)colModel;
                etcm.hiddenColumns = new ArrayList<TableColumn>();
                etcm.hiddenColumnsPosition = new ArrayList<Integer>();
                etcm.clearSortedColumns();
            }
            for (int i2 = 0; i2 < newColumns.length; ++i2) {
                this.addColumn(newColumns[i2]);
            }
            if (colModel instanceof ETableColumnModel) {
                int index;
                ETableColumnModel etcm = (ETableColumnModel)colModel;
                if (hiddenColumnIndexes != null) {
                    for (int hci = 0; hci < hiddenColumnIndexes.length; ++hci) {
                        index = hiddenColumnIndexes[hci];
                        if (index < 0) continue;
                        etcm.setColumnHidden(newColumns[index], true);
                    }
                }
                if (sortedColumnIndexes != null) {
                    for (int sci = 0; sci < sortedColumnIndexes.length; ++sci) {
                        index = sortedColumnIndexes[sci];
                        if (index < 0) continue;
                        Sorting sorting = columnSorting[index];
                        if (!(newColumns[index] instanceof ETableColumn) || sorting == null) continue;
                        ETableColumn etc = (ETableColumn)newColumns[index];
                        etcm.setColumnSorted(etc, sorting.ascending, sorting.sortRank);
                    }
                }
            }
            this.firePropertyChange("createdDefaultColumnsFromModel", null, newColumns);
        }
    }

    public String getTransferDelimiter(boolean line) {
        if (line) {
            return "\n";
        }
        return "\t";
    }

    public String convertValueToString(Object value) {
        if ((value = this.transformValue(value)) == null) {
            return "";
        }
        return value.toString();
    }

    protected TableColumn createColumn(int modelIndex) {
        return new ETableColumn(modelIndex, this);
    }

    @Override
    protected TableColumnModel createDefaultColumnModel() {
        return new ETableColumnModel();
    }

    @Override
    public Object getValueAt(int row, int column) {
        int modelRow = row;
        return super.getValueAt(modelRow, column);
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {
        super.setValueAt(aValue, row, column);
    }

    @Override
    public int getRowCount() {
        if (this.quickFilterColumn != -1 && this.quickFilterObject != null) {
            if (this.filteredRowCount == -1) {
                this.computeFilteredRowCount();
            }
            return this.filteredRowCount;
        }
        return super.getRowCount();
    }

    public void setQuickFilter(int column, Object filterObject) {
        this.quickFilterColumn = column;
        this.quickFilterObject = filterObject;
        this.resetPermutation();
        this.filteredRowCount = -1;
        super.tableChanged(new TableModelEvent(this.getModel()));
        this.firePropertyChange("quickFilter", null, null);
    }

    public Object getQuickFilterObject() {
        return this.quickFilterObject;
    }

    public int getQuickFilterColumn() {
        return this.quickFilterColumn;
    }

    public void unsetQuickFilter() {
        this.quickFilterObject = null;
        this.quickFilterColumn = -1;
        this.filteredRowCount = -1;
        this.resetPermutation();
        super.tableChanged(new TableModelEvent(this.getModel()));
        this.firePropertyChange("quickFilter", null, null);
    }

    @Override
    public void setModel(TableModel dataModel) {
        super.setModel(dataModel);
        this.filteredRowCount = -1;
        this.resetPermutation();
        this.quickFilterColumn = -1;
        this.quickFilterObject = null;
        this.updateMouseListener();
        if (this.defaultRenderersByColumnClass != null) {
            this.updatePreferredWidths();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public String getToolTipText(MouseEvent event) {
        try {
            this.putClientProperty(COMPUTING_TOOLTIP, Boolean.TRUE);
            String string = super.getToolTipText(event);
            return string;
        }
        finally {
            this.putClientProperty(COMPUTING_TOOLTIP, Boolean.FALSE);
        }
    }

    public boolean isColumnHidingAllowed() {
        return this.columnHidingAllowed;
    }

    public void setColumnHidingAllowed(boolean allowColumnHiding) {
        if (allowColumnHiding != this.columnHidingAllowed) {
            this.columnHidingAllowed = allowColumnHiding;
            this.configureEnclosingScrollPane();
        }
    }

    @Override
    protected void initializeLocalVars() {
        super.initializeLocalVars();
        this.updatePreferredWidths();
        this.setSurrendersFocusOnKeystroke(true);
        this.setFocusCycleRoot(true);
        this.setFocusTraversalPolicy(new STPolicy());
        this.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        this.putClientProperty("JTable.autoStartsEdit", Boolean.FALSE);
        Set emptySet = Collections.emptySet();
        this.setFocusTraversalKeys(0, emptySet);
        this.setFocusTraversalKeys(1, emptySet);
        this.setFocusCycleRoot(false);
        this.unregisterKeyboardAction(KeyStroke.getKeyStroke(10, 0));
        this.unregisterKeyboardAction(KeyStroke.getKeyStroke(10, 1));
        InputMap imp = this.getInputMap(0);
        ActionMap am = this.getActionMap();
        imp.put(KeyStroke.getKeyStroke(9, 3, false), "focusNext");
        imp.put(KeyStroke.getKeyStroke(9, 2, false), "focusNext");
        CTRLTabAction ctrlTab = new CTRLTabAction();
        am.put("focusNext", ctrlTab);
        imp.put(KeyStroke.getKeyStroke(32, 0, false), "beginEdit");
        this.getActionMap().put("beginEdit", new EditAction());
        imp.put(KeyStroke.getKeyStroke(27, 0, false), "cancelEdit");
        this.getActionMap().put("cancelEdit", new CancelEditAction());
        imp.put(KeyStroke.getKeyStroke(10, 0, false), "enter");
        this.getActionMap().put("enter", new EnterAction());
        imp.put(KeyStroke.getKeyStroke(9, 0), "next");
        imp.put(KeyStroke.getKeyStroke(9, 64), "previous");
        am.put("next", new NavigationAction(true));
        am.put("previous", new NavigationAction(false));
        this.setTransferHandler(new ETableTransferHandler());
    }

    @Override
    protected boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition, boolean pressed) {
        if (pressed) {
            if (e.getKeyChar() == '+' && (e.getModifiers() & 2) == 2) {
                this.updatePreferredWidths();
                e.consume();
                return true;
            }
            if (e.getKeyChar() == '-' && (e.getModifiers() & 2) == 2) {
                this.unsetQuickFilter();
                e.consume();
                return true;
            }
            if (e.getKeyChar() == '*' && (e.getModifiers() & 2) == 2) {
                ColumnSelectionPanel.showColumnSelectionDialog(this);
                e.consume();
                return true;
            }
        }
        boolean retValue = super.processKeyBinding(ks, e, condition, pressed);
        return retValue;
    }

    public void setColumnSorted(int columnIndex, boolean ascending, int rank) {
        int ii = this.convertColumnIndexToView(columnIndex);
        if (ii < 0) {
            return;
        }
        this.sortable = true;
        TableColumnModel tcm = this.getColumnModel();
        if (tcm instanceof ETableColumnModel) {
            ETableColumnModel etcm = (ETableColumnModel)tcm;
            TableColumn tc = tcm.getColumn(ii);
            if (tc instanceof ETableColumn) {
                int wasSelectedColumn;
                SelectedRows selectedRows;
                ETableColumn etc = (ETableColumn)tc;
                if (!etc.isSortingAllowed()) {
                    return;
                }
                if (this.getUpdateSelectionOnSort()) {
                    selectedRows = this.getSelectedRowsInModel();
                    wasSelectedColumn = this.getSelectedColumn();
                } else {
                    selectedRows = null;
                    wasSelectedColumn = -1;
                }
                etcm.setColumnSorted(etc, ascending, rank);
                this.resetPermutation();
                this.tableChanged(new TableModelEvent(this.getModel(), -1, this.getRowCount()));
                if (selectedRows != null) {
                    this.changeSelectionInModel(selectedRows, wasSelectedColumn);
                }
            }
        }
    }

    @Override
    protected void configureEnclosingScrollPane() {
        Container p;
        Container gp;
        super.configureEnclosingScrollPane();
        if (this.isFullyNonEditable()) {
            this.setupSearch();
        }
        if ((p = this.getParent()) instanceof JViewport && (gp = p.getParent()) instanceof JScrollPane) {
            JScrollPane scrollPane = (JScrollPane)gp;
            JViewport viewport = scrollPane.getViewport();
            if (viewport == null || viewport.getView() != this) {
                return;
            }
            if (this.isColumnHidingAllowed()) {
                Icon ii = UIManager.getIcon("Table.columnSelection");
                if (ii == null) {
                    ii = new ImageIcon(ImageUtilities.loadImage((String)"org/netbeans/swing/etable/columns.gif", (boolean)true));
                }
                final JButton b = new JButton(ii);
                if (this.tableHeader != null) {
                    b.setBackground(this.tableHeader.getBackground());
                } else {
                    b.setBackground(new JTableHeader().getBackground());
                }
                b.setToolTipText(this.selectVisibleColumnsLabel);
                b.getAccessibleContext().setAccessibleName(this.selectVisibleColumnsLabel);
                b.getAccessibleContext().setAccessibleDescription(this.selectVisibleColumnsLabel);
                b.addActionListener(new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        ColumnSelectionPanel.showColumnSelectionPopupOrDialog(b, ETable.this);
                    }
                });
                b.addMouseListener(new MouseAdapter(){

                    @Override
                    public void mouseClicked(MouseEvent me) {
                        ColumnSelection cs = ETable.this.getColumnSelectionOn(me.getButton());
                        switch (cs) {
                            case POPUP: {
                                ColumnSelectionPanel.showColumnSelectionPopup(b, ETable.this);
                                break;
                            }
                            case DIALOG: {
                                ColumnSelectionPanel.showColumnSelectionDialog(ETable.this);
                            }
                        }
                    }
                });
                b.setFocusable(false);
                scrollPane.setCorner("UPPER_RIGHT_CORNER", b);
            } else {
                scrollPane.setCorner("UPPER_RIGHT_CORNER", null);
            }
        }
        this.updateColumnSelectionMouseListener();
    }

    private SelectedRows getSelectedRowsInModel() {
        SelectedRows sr = new SelectedRows();
        int[] rows = this.getSelectedRows();
        sr.rowsInView = rows;
        int[] modelRows = new int[rows.length];
        for (int i = 0; i < rows.length; ++i) {
            modelRows[i] = this.convertRowIndexToModel(rows[i]);
        }
        sr.rowsInModel = modelRows;
        ListSelectionModel rsm = this.getSelectionModel();
        sr.anchorInView = rsm.getAnchorSelectionIndex();
        sr.leadInView = rsm.getLeadSelectionIndex();
        sr.anchorInModel = this.convertRowIndexToModel(sr.anchorInView);
        sr.leadInModel = this.convertRowIndexToModel(sr.leadInView);
        return sr;
    }

    private void changeSelectionInModel(SelectedRows selectedRows, int selectedColumn) {
        boolean wasAutoScroll = this.getAutoscrolls();
        this.setAutoscrolls(false);
        DefaultListSelectionModel rsm = (DefaultListSelectionModel)this.getSelectionModel();
        rsm.setValueIsAdjusting(true);
        ListSelectionModel csm = this.getColumnModel().getSelectionModel();
        csm.setValueIsAdjusting(true);
        int leadRow = this.convertRowIndexToView(selectedRows.leadInModel);
        int anchorRow = this.convertRowIndexToView(selectedRows.anchorInModel);
        Arrays.sort(selectedRows.rowsInView);
        int[] newRowsInView = new int[selectedRows.rowsInModel.length];
        int n = this.getModel().getRowCount();
        for (int i = 0; i < selectedRows.rowsInModel.length; ++i) {
            int viewIndex;
            newRowsInView[i] = i < selectedRows.rowsInView.length && (selectedRows.rowsInView[i] < 0 || selectedRows.rowsInView[i] >= n) ? -1 : (viewIndex = this.convertRowIndexToView(selectedRows.rowsInModel[i]));
        }
        Arrays.sort(newRowsInView);
        int[] rowsInView = selectedRows.rowsInView;
        int i2 = 0;
        int j = 0;
        while (i2 < rowsInView.length || j < newRowsInView.length) {
            int toSelect;
            int selected = i2 < rowsInView.length ? rowsInView[i2] : Integer.MAX_VALUE;
            int n2 = toSelect = j < newRowsInView.length ? newRowsInView[j] : Integer.MAX_VALUE;
            if (selected == toSelect) {
                ++i2;
                ++j;
                continue;
            }
            if (selected < toSelect) {
                if (selected > -1) {
                    int selected2 = selected;
                    while (i2 + 1 < rowsInView.length && rowsInView[i2 + 1] == selected2 + 1 && selected2 + 1 < toSelect) {
                        ++selected2;
                        ++i2;
                    }
                    rsm.removeSelectionInterval(selected, selected2);
                }
                ++i2;
                continue;
            }
            if (toSelect > -1) {
                int toSelect2 = toSelect;
                while (j + 1 < newRowsInView.length && newRowsInView[j + 1] == toSelect2 + 1 && toSelect2 + 1 < selected) {
                    ++toSelect2;
                    ++j;
                }
                rsm.addSelectionInterval(toSelect, toSelect2);
            }
            ++j;
        }
        if (anchorRow != selectedRows.anchorInView) {
            rsm.setAnchorSelectionIndex(anchorRow);
        }
        if (leadRow != selectedRows.leadInView) {
            if (leadRow == -1) {
                for (int k = 0; k < newRowsInView.length; ++k) {
                    if (newRowsInView[k] != -1) continue;
                    while (k + 1 < newRowsInView.length && newRowsInView[k + 1] == -1) {
                        ++k;
                    }
                    if (k + 1 < newRowsInView.length) {
                        leadRow = newRowsInView[k + 1];
                        break;
                    }
                    if (k <= 0) continue;
                    leadRow = newRowsInView[0];
                    break;
                }
            }
            rsm.moveLeadSelectionIndex(leadRow);
        }
        rsm.setValueIsAdjusting(false);
        csm.setValueIsAdjusting(false);
        if (wasAutoScroll) {
            this.setAutoscrolls(true);
        }
    }

    private void updateSelectedLines(int[] currentLineSelections, int currentLeadRow, int currentAnchorRow, int[] newLineSelections, int newLeadRow, int newAnchorRow) {
        boolean wasAutoScroll = this.getAutoscrolls();
        this.setAutoscrolls(false);
        DefaultListSelectionModel rsm = (DefaultListSelectionModel)this.getSelectionModel();
        rsm.setValueIsAdjusting(true);
        ListSelectionModel csm = this.getColumnModel().getSelectionModel();
        csm.setValueIsAdjusting(true);
        int i = 0;
        int j = 0;
        while (i < currentLineSelections.length || j < newLineSelections.length) {
            int toSelect;
            int selected = i < currentLineSelections.length ? currentLineSelections[i] : Integer.MAX_VALUE;
            int n = toSelect = j < newLineSelections.length ? newLineSelections[j] : Integer.MAX_VALUE;
            if (selected == toSelect) {
                ++i;
                ++j;
                continue;
            }
            if (selected < toSelect) {
                if (selected > -1) {
                    int selected2 = selected;
                    while (i + 1 < currentLineSelections.length && currentLineSelections[i + 1] == selected2 + 1 && selected2 + 1 < toSelect) {
                        ++selected2;
                        ++i;
                    }
                    rsm.removeSelectionInterval(selected, selected2);
                }
                ++i;
                continue;
            }
            if (toSelect > -1) {
                int toSelect2 = toSelect;
                while (j + 1 < newLineSelections.length && newLineSelections[j + 1] == toSelect2 + 1 && toSelect2 + 1 < selected) {
                    ++toSelect2;
                    ++j;
                }
                rsm.addSelectionInterval(toSelect, toSelect2);
            }
            ++j;
        }
        if (newAnchorRow != currentAnchorRow) {
            rsm.setAnchorSelectionIndex(newAnchorRow);
        }
        if (newLeadRow != currentLeadRow) {
            if (newLeadRow == -1) {
                for (int k = 0; k < newLineSelections.length; ++k) {
                    if (newLineSelections[k] != -1) continue;
                    while (k + 1 < newLineSelections.length && newLineSelections[k + 1] == -1) {
                        ++k;
                    }
                    if (k + 1 < newLineSelections.length) {
                        newLeadRow = newLineSelections[k + 1];
                        break;
                    }
                    if (k <= 0) continue;
                    newLeadRow = newLineSelections[0];
                    break;
                }
            }
            rsm.moveLeadSelectionIndex(newLeadRow);
        }
        rsm.setValueIsAdjusting(false);
        csm.setValueIsAdjusting(false);
        if (wasAutoScroll) {
            this.setAutoscrolls(true);
        }
    }

    void updateColumnSelectionMouseListener() {
        Container gp;
        Container p = this.getParent();
        if (p instanceof JViewport && (gp = p.getParent()) instanceof JScrollPane) {
            JScrollPane scrollPane = (JScrollPane)gp;
            JViewport viewport = scrollPane.getViewport();
            if (viewport == null || viewport.getView() != this) {
                return;
            }
            scrollPane.removeMouseListener(this.columnSelectionMouseListener);
            if (this.getColumnModel().getColumnCount() == 0) {
                scrollPane.addMouseListener(this.columnSelectionMouseListener);
            }
        }
        if (this.searchCombo != null) {
            this.searchCombo.setModel(this.getSearchComboModel());
        }
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        boolean needsTotalRefresh = true;
        if (e == null || e.getFirstRow() == -1) {
            this.resetPermutation();
            this.filteredRowCount = -1;
            super.tableChanged(e);
            return;
        }
        boolean areMoreEventsPending = false;
        Object source = e.getSource();
        if (source instanceof DefaultOutlineModel) {
            areMoreEventsPending = ((DefaultOutlineModel)source).areMoreEventsPending();
        }
        if (e.getType() == 1 || e.getType() == -1) {
            int count;
            if (this.selectedRowsWhenTableChanged == null) {
                this.selectedRowsWhenTableChanged = this.getSelectedRowsInModel();
                this.sortingPermutationsWhenTableChanged = new int[2][];
                this.sortingPermutationsWhenTableChanged[0] = this.sortingPermutation;
                this.sortingPermutationsWhenTableChanged[1] = this.inverseSortingPermutation;
                this.selectedColumnWhenTableChanged = this.getSelectedColumn();
                this.resetPermutation();
                this.filteredRowCount = -1;
            }
            int[] invSortPermutation = this.sortingPermutationsWhenTableChanged[1];
            TableModelEvent se = e;
            if (invSortPermutation != null) {
                int lr;
                int fr = e.getFirstRow();
                if (fr >= 0 && fr < invSortPermutation.length) {
                    fr = invSortPermutation[fr];
                }
                if ((lr = e.getLastRow()) >= 0 && lr < invSortPermutation.length) {
                    lr = invSortPermutation[lr];
                }
                se = new TableModelEvent((TableModel)e.getSource(), fr, lr, e.getColumn(), e.getType());
            }
            super.tableChanged(se);
            int first = e.getFirstRow();
            int last = e.getLastRow();
            if (first > last) {
                int r = last;
                last = first;
                first = r;
            }
            if ((count = last - first + 1) > 0) {
                int[] wasSelectedRows = this.selectedRowsWhenTableChanged.rowsInModel;
                if (e.getType() == 1) {
                    for (int i = 0; i < wasSelectedRows.length; ++i) {
                        if (wasSelectedRows[i] < first) continue;
                        int[] arrn = wasSelectedRows;
                        int n = i;
                        arrn[n] = arrn[n] + count;
                    }
                } else {
                    for (int i = 0; i < wasSelectedRows.length; ++i) {
                        if (wasSelectedRows[i] < first) continue;
                        if (wasSelectedRows[i] <= last) {
                            wasSelectedRows[i] = -1;
                            continue;
                        }
                        int[] arrn = wasSelectedRows;
                        int n = i;
                        arrn[n] = arrn[n] - count;
                    }
                }
            }
            if (!areMoreEventsPending) {
                if (this.selectedRowsWhenTableChanged.rowsInModel.length > 0) {
                    this.selectedRowsWhenTableChanged.rowsInView = this.getSelectedRows();
                    this.changeSelectionInModel(this.selectedRowsWhenTableChanged, this.selectedColumnWhenTableChanged);
                }
                this.selectedRowsWhenTableChanged = null;
                this.sortingPermutationsWhenTableChanged = null;
                this.selectedColumnWhenTableChanged = -1;
            }
            return;
        }
        int modelColumn = e.getColumn();
        int start = e.getFirstRow();
        int end = e.getLastRow();
        if (start != -1 && (start = this.convertRowIndexToView(start)) == -1) {
            start = 0;
        }
        if (end != -1 && (end = this.convertRowIndexToView(end)) == -1) {
            end = Integer.MAX_VALUE;
        }
        if (start == end) {
            TableModelEvent tme = new TableModelEvent((TableModel)e.getSource(), start, end, modelColumn);
            super.tableChanged(tme);
            return;
        }
        if (modelColumn != -1) {
            Enumeration<TableColumn> enumeration = this.getColumnModel().getColumns();
            boolean index = false;
            while (enumeration.hasMoreElements()) {
                ETableColumn etc;
                TableColumn aColumn = enumeration.nextElement();
                if (aColumn.getModelIndex() != modelColumn || (etc = (ETableColumn)aColumn).isSorted() || this.quickFilterColumn == modelColumn) continue;
                needsTotalRefresh = false;
            }
        }
        if (needsTotalRefresh) {
            SelectedRows selectedRows = this.getSelectedRowsInModel();
            int wasSelectedColumn = this.getSelectedColumn();
            this.resetPermutation();
            this.filteredRowCount = -1;
            super.tableChanged(new TableModelEvent(this.getModel()));
            this.changeSelectionInModel(selectedRows, wasSelectedColumn);
        } else {
            TableModelEvent tme = new TableModelEvent((TableModel)e.getSource(), 0, this.getModel().getRowCount(), modelColumn);
            super.tableChanged(tme);
        }
    }

    private void resetPermutation() {
        assert (SwingUtilities.isEventDispatchThread());
        this.sortingPermutation = null;
        this.inverseSortingPermutation = null;
    }

    private TableColumn getResizingColumn(Point p) {
        int columnIndex;
        JTableHeader header = this.getTableHeader();
        if (header == null) {
            return null;
        }
        int column = header.columnAtPoint(p);
        if (column == -1) {
            return null;
        }
        Rectangle r = header.getHeaderRect(column);
        r.grow(-3, 0);
        if (r.contains(p)) {
            return null;
        }
        int midPoint = r.x + r.width / 2;
        if (header.getComponentOrientation().isLeftToRight()) {
            columnIndex = p.x < midPoint ? column - 1 : column;
        } else {
            int n = columnIndex = p.x < midPoint ? column : column - 1;
        }
        if (columnIndex == -1) {
            return null;
        }
        return header.getColumnModel().getColumn(columnIndex);
    }

    private void updateMouseListener() {
        JTableHeader jth = this.getTableHeader();
        if (jth != null) {
            jth.removeMouseListener(this.headerMouseListener);
            jth.addMouseListener(this.headerMouseListener);
        }
    }

    @Override
    protected JTableHeader createDefaultTableHeader() {
        return new ETableHeader(this.columnModel);
    }

    private void computeFilteredRowCount() {
        if (this.quickFilterColumn == -1 || this.quickFilterObject == null) {
            this.filteredRowCount = -1;
            return;
        }
        if (this.sortingPermutation != null) {
            this.filteredRowCount = this.sortingPermutation.length;
            return;
        }
        this.sortAndFilter();
        if (this.sortingPermutation != null) {
            this.filteredRowCount = this.sortingPermutation.length;
        }
    }

    @Override
    public int convertRowIndexToModel(int row) {
        if (!(this.getColumnModel() instanceof ETableColumnModel)) {
            return super.convertRowIndexToModel(row);
        }
        if (this.sortingPermutation == null) {
            this.sortAndFilter();
        }
        if (this.sortingPermutation != null) {
            if (row >= 0 && row < this.sortingPermutation.length) {
                return this.sortingPermutation[row];
            }
            return -1;
        }
        return row;
    }

    @Override
    public int convertRowIndexToView(int row) {
        if (!(this.getColumnModel() instanceof ETableColumnModel)) {
            return super.convertRowIndexToView(row);
        }
        if (this.inverseSortingPermutation == null) {
            this.sortAndFilter();
        }
        if (this.inverseSortingPermutation != null) {
            if (row >= 0 && row < this.inverseSortingPermutation.length) {
                return this.inverseSortingPermutation[row];
            }
            return -1;
        }
        return row;
    }

    public void setSelectVisibleColumnsLabel(String localizedLabel) {
        this.selectVisibleColumnsLabel = localizedLabel;
    }

    String getSelectVisibleColumnsLabel() {
        return this.selectVisibleColumnsLabel;
    }

    public void setQuickFilterFormatStrings(String[] newFormats) {
        if (newFormats == null || newFormats.length != this.quickFilterFormatStrings.length) {
            return;
        }
        this.quickFilterFormatStrings = (String[])newFormats.clone();
    }

    public String[] getQuickFilterFormatStrings() {
        return (String[])this.quickFilterFormatStrings.clone();
    }

    public String getColumnDisplayName(String columnName) {
        return columnName;
    }

    public Object transformValue(Object value) {
        return value;
    }

    public JMenuItem getQuickFilterPopup(int column, Object value, String label) {
        JMenu menu = new JMenu(label);
        String columnDisplayName = this.getColumnDisplayName(this.getColumnName(column));
        JMenuItem equalsItem = this.getQuickFilterEqualsItem(column, value, columnDisplayName, this.quickFilterFormatStrings[0], true);
        menu.add(equalsItem);
        JMenuItem notequalsItem = this.getQuickFilterEqualsItem(column, value, columnDisplayName, this.quickFilterFormatStrings[1], false);
        menu.add(notequalsItem);
        JMenuItem greaterItem = this.getQuickFilterCompareItem(column, value, columnDisplayName, this.quickFilterFormatStrings[2], true, false);
        menu.add(greaterItem);
        JMenuItem lessItem = this.getQuickFilterCompareItem(column, value, columnDisplayName, this.quickFilterFormatStrings[3], false, false);
        menu.add(lessItem);
        JMenuItem greaterEqualsItem = this.getQuickFilterCompareItem(column, value, columnDisplayName, this.quickFilterFormatStrings[4], true, true);
        menu.add(greaterEqualsItem);
        JMenuItem lessEqualsItem = this.getQuickFilterCompareItem(column, value, columnDisplayName, this.quickFilterFormatStrings[5], false, true);
        menu.add(lessEqualsItem);
        JMenuItem noFilterItem = this.getQuickFilterNoFilterItem(this.quickFilterFormatStrings[6]);
        menu.add(noFilterItem);
        return menu;
    }

    public JMenuItem getQuickFilterEqualsItem(int column, Object value, String columnName, String text, boolean equals) {
        String s = MessageFormat.format(text, columnName, value);
        JMenuItem res = new JMenuItem(s);
        int modelColumn = this.convertColumnIndexToModel(column);
        res.addActionListener(new EqualsQuickFilter(modelColumn, value, equals));
        return res;
    }

    public JMenuItem getQuickFilterNoFilterItem(String label) {
        JMenuItem res = new JMenuItem(label);
        res.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ETable.this.unsetQuickFilter();
            }
        });
        return res;
    }

    public JMenuItem getQuickFilterCompareItem(int column, Object value, String columnName, String text, boolean greater, boolean equalsCounts) {
        String s = MessageFormat.format(text, columnName, value);
        JMenuItem res = new JMenuItem(s);
        int modelColumn = this.convertColumnIndexToModel(column);
        res.addActionListener(new CompareQuickFilter(modelColumn, value, greater, equalsCounts));
        return res;
    }

    protected void sortAndFilter() {
        Comparator<RowMapping> c;
        ETableColumnModel etcm;
        TableColumnModel tcm = this.getColumnModel();
        if (tcm instanceof ETableColumnModel && (c = (etcm = (ETableColumnModel)tcm).getComparator()) != null) {
            TableModel model = this.getModel();
            int noRows = model.getRowCount();
            ArrayList<RowMapping> rows = new ArrayList<RowMapping>();
            for (int i = 0; i < noRows; ++i) {
                if (!this.acceptByQuickFilter(model, i)) continue;
                rows.add(new RowMapping(i, model, this));
            }
            Collections.sort(rows, c);
            int[] res = new int[rows.size()];
            int[] invRes = new int[noRows];
            int i2 = 0;
            while (i2 < res.length) {
                int rmi;
                RowMapping rm = (RowMapping)rows.get(i2);
                res[i2] = rmi = rm.getModelRowIndex();
                invRes[rmi] = i2++;
            }
            int[] oldRes = this.sortingPermutation;
            int[] oldInvRes = this.inverseSortingPermutation;
            this.sortingPermutation = res;
            this.inverseSortingPermutation = invRes;
        }
    }

    protected boolean acceptByQuickFilter(TableModel model, int row) {
        if (this.quickFilterColumn == -1 || this.quickFilterObject == null) {
            return true;
        }
        Object value = model.getValueAt(row, this.quickFilterColumn);
        value = this.transformValue(value);
        if (this.quickFilterObject instanceof QuickFilter) {
            QuickFilter filter = (QuickFilter)this.quickFilterObject;
            return filter.accept(value);
        }
        if (value == null) {
            return false;
        }
        return value.equals(this.quickFilterObject);
    }

    void updatePreferredWidths() {
        Enumeration<TableColumn> en = this.getColumnModel().getColumns();
        while (en.hasMoreElements()) {
            TableColumn obj = en.nextElement();
            if (!(obj instanceof ETableColumn)) continue;
            ETableColumn etc = (ETableColumn)obj;
            etc.updatePreferredWidth(this, false);
        }
    }

    public void readSettings(Properties p, String propertyPrefix) {
        ETableColumnModel etcm = (ETableColumnModel)this.createDefaultColumnModel();
        etcm.readSettings(p, propertyPrefix, this);
        this.setColumnModel(etcm);
        String scs = p.getProperty(propertyPrefix + "SearchColumn");
        if (scs != null) {
            try {
                int index = Integer.parseInt(scs);
                for (int i = 0; i < etcm.getColumnCount(); ++i) {
                    TableColumn tc = etcm.getColumn(i);
                    if (tc.getModelIndex() != index) continue;
                    this.searchColumn = (ETableColumn)tc;
                    break;
                }
            }
            catch (NumberFormatException nfe) {
                nfe.printStackTrace();
            }
        }
        this.filteredRowCount = -1;
        this.resetPermutation();
        super.tableChanged(new TableModelEvent(this.getModel()));
    }

    public void writeSettings(Properties p, String propertyPrefix) {
        TableColumnModel tcm = this.getColumnModel();
        if (tcm instanceof ETableColumnModel) {
            ETableColumnModel etcm = (ETableColumnModel)tcm;
            etcm.writeSettings(p, propertyPrefix);
        }
        if (this.searchColumn != null) {
            p.setProperty(propertyPrefix + "SearchColumn", Integer.toString(this.searchColumn.getModelIndex()));
        }
    }

    private List<Integer> doSearch(String prefix) {
        ArrayList<Integer> results = new ArrayList<Integer>();
        int startIndex = 0;
        int size = this.getRowCount();
        if (size == 0 || this.getColumnCount() == 0) {
            return results;
        }
        int column = 0;
        if (this.searchColumn != null) {
            column = this.convertColumnIndexToView(this.searchColumn.getModelIndex());
        }
        if (column < 0) {
            return results;
        }
        while (startIndex < size) {
            Object val = this.getValueAt(startIndex, column);
            String s = null;
            if (val != null) {
                s = this.convertValueToString(val);
            }
            if (s != null && s.toUpperCase().indexOf(prefix.toUpperCase()) != -1) {
                results.add(new Integer(startIndex));
                if (this.maxPrefix == null) {
                    this.maxPrefix = s;
                }
                this.maxPrefix = ETable.findMaxPrefix(this.maxPrefix, s);
            }
            ++startIndex;
        }
        return results;
    }

    private static String findMaxPrefix(String str1, String str2) {
        int i = 0;
        while (str1.regionMatches(true, 0, str2, 0, i)) {
            ++i;
        }
        if (--i >= 0) {
            return str1.substring(0, i);
        }
        return null;
    }

    private void setupSearch() {
        KeyListener[] keyListeners = (KeyListener[])this.getListeners(KeyListener.class);
        for (int i = 0; i < keyListeners.length; ++i) {
            this.removeKeyListener(keyListeners[i]);
        }
        this.addKeyListener(new KeyAdapter(){
            private boolean armed;

            @Override
            public void keyPressed(KeyEvent e) {
                int modifiers = e.getModifiers();
                int keyCode = e.getKeyCode();
                if (modifiers > 0 && modifiers != 1 || e.isActionKey()) {
                    return;
                }
                char c = e.getKeyChar();
                if (!Character.isISOControl(c) && keyCode != 16 && keyCode != 27) {
                    this.armed = true;
                    e.consume();
                }
            }

            @Override
            public void keyTyped(KeyEvent e) {
                if (this.armed) {
                    KeyStroke stroke = KeyStroke.getKeyStrokeForEvent(e);
                    ETable.this.searchTextField.setText(String.valueOf(stroke.getKeyChar()));
                    ETable.this.displaySearchField();
                    e.consume();
                    this.armed = false;
                }
            }
        });
        SearchFieldListener searchFieldListener = new SearchFieldListener();
        this.searchTextField.addKeyListener(searchFieldListener);
        this.searchTextField.addFocusListener(searchFieldListener);
        this.searchTextField.getDocument().addDocumentListener(searchFieldListener);
    }

    private void prepareSearchPanel() {
        if (this.searchPanel == null) {
            this.searchPanel = new JPanel();
            String s = UIManager.getString("LBL_QUICKSEARCH");
            if (s == null) {
                s = "Quick search in";
            }
            JLabel lbl = new JLabel(s);
            this.searchPanel.setLayout(new BoxLayout(this.searchPanel, 0));
            this.searchPanel.add(lbl);
            this.searchCombo = new JComboBox(this.getSearchComboModel());
            if (this.searchColumn != null) {
                Object value = this.searchColumn.getHeaderValue();
                String valueString = "";
                if (value != null) {
                    valueString = value.toString();
                }
                valueString = this.getColumnDisplayName(valueString);
                this.searchCombo.setSelectedItem(valueString);
            }
            SearchComboListener scl = new SearchComboListener();
            this.searchCombo.addItemListener(scl);
            this.searchCombo.addFocusListener(scl);
            this.searchCombo.addKeyListener(scl);
            this.searchPanel.add(this.searchCombo);
            this.searchPanel.add(this.searchTextField);
            lbl.setLabelFor(this.searchTextField);
            this.searchPanel.setBorder(BorderFactory.createRaisedBevelBorder());
            lbl.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
        }
    }

    private ComboBoxModel getSearchComboModel() {
        DefaultComboBoxModel<String> result = new DefaultComboBoxModel<String>();
        Enumeration<TableColumn> en = this.getColumnModel().getColumns();
        while (en.hasMoreElements()) {
            TableColumn column = en.nextElement();
            if (!(column instanceof ETableColumn)) continue;
            ETableColumn etc = (ETableColumn)column;
            Object value = etc.getHeaderValue();
            String valueString = "";
            if (value != null) {
                valueString = value.toString();
            }
            valueString = this.getColumnDisplayName(valueString);
            result.addElement(valueString);
        }
        return result;
    }

    public void displaySearchField() {
        if (!this.searchTextField.isDisplayable()) {
            this.searchTextField.setFont(this.getFont());
            this.prepareSearchPanel();
            this.add(this.searchPanel);
        }
        this.doLayout();
        this.invalidate();
        this.validate();
        this.repaint();
        this.searchTextField.requestFocus();
    }

    @Override
    public void doLayout() {
        super.doLayout();
        Rectangle visibleRect = this.getVisibleRect();
        if (this.searchPanel != null && this.searchPanel.isDisplayable()) {
            int width = Math.min(visibleRect.width - this.SEARCH_FIELD_SPACE * 2, this.searchPanel.getPreferredSize().width - this.searchTextField.getPreferredSize().width + this.SEARCH_FIELD_PREFERRED_SIZE - this.SEARCH_FIELD_SPACE);
            this.searchPanel.setBounds(Math.max(this.SEARCH_FIELD_SPACE, visibleRect.x + visibleRect.width - width), visibleRect.y + this.SEARCH_FIELD_SPACE, Math.min(visibleRect.width, width) - this.SEARCH_FIELD_SPACE, this.heightOfTextField);
        }
    }

    private void removeSearchField() {
        if (this.searchPanel.isDisplayable()) {
            this.remove(this.searchPanel);
            Rectangle r = this.searchPanel.getBounds();
            this.repaint(r);
        }
    }

    private void showColumnSelection(MouseEvent me) {
        ColumnSelection cs = this.getColumnSelectionOn(me.getButton());
        switch (cs) {
            case POPUP: {
                ColumnSelectionPanel.showColumnSelectionPopup(me.getComponent(), me.getX(), me.getY(), this);
                break;
            }
            case DIALOG: {
                ColumnSelectionPanel.showColumnSelectionDialog(this);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean editCellAt(int row, int column, EventObject e) {
        this.inEditRequest = true;
        if (this.editingRow == row && this.editingColumn == column && this.isEditing()) {
            this.inEditRequest = false;
            return false;
        }
        if (this.isEditing()) {
            this.removeEditor();
            this.changeSelection(row, column, false, false);
        }
        try {
            boolean ret = super.editCellAt(row, column, e);
            if (ret) {
                this.editorComp.requestFocus();
            }
            boolean bl = ret;
            return bl;
        }
        finally {
            this.inEditRequest = false;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeEditor() {
        this.inRemoveRequest = true;
        try {
            Object object = this.getTreeLock();
            synchronized (object) {
                super.removeEditor();
            }
        }
        finally {
            this.inRemoveRequest = false;
        }
    }

    private boolean isKnownComponent(Component c) {
        if (c == null) {
            return false;
        }
        if (this.isAncestorOf(c)) {
            return true;
        }
        if (c == this.editorComp) {
            return true;
        }
        if (this.editorComp != null && this.editorComp instanceof Container && ((Container)this.editorComp).isAncestorOf(c)) {
            return true;
        }
        return false;
    }

    public TableColumnSelector getColumnSelector() {
        return this.columnSelector != null ? this.columnSelector : defaultColumnSelector;
    }

    public void setColumnSelector(TableColumnSelector columnSelector) {
        this.columnSelector = columnSelector;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isPopupUsedFromTheCorner() {
        Object object = this.columnSelectionOnMouseClickLock;
        synchronized (object) {
            ColumnSelection cs = this.columnSelectionOnMouseClick[1];
            return cs == ColumnSelection.POPUP;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setPopupUsedFromTheCorner(boolean popupUsedFromTheCorner) {
        Object object = this.columnSelectionOnMouseClickLock;
        synchronized (object) {
            this.columnSelectionOnMouseClick[1] = popupUsedFromTheCorner ? ColumnSelection.POPUP : ColumnSelection.DIALOG;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public ColumnSelection getColumnSelectionOn(int mouseButton) {
        if (mouseButton < 0) {
            throw new IllegalArgumentException("Button = " + mouseButton);
        }
        Object object = this.columnSelectionOnMouseClickLock;
        synchronized (object) {
            if (mouseButton >= this.columnSelectionOnMouseClick.length) {
                return null;
            }
            return this.columnSelectionOnMouseClick[mouseButton];
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setColumnSelectionOn(int mouseButton, ColumnSelection selection) {
        if (mouseButton < 0) {
            throw new IllegalArgumentException("Button = " + mouseButton);
        }
        Object object = this.columnSelectionOnMouseClickLock;
        synchronized (object) {
            if (mouseButton >= this.columnSelectionOnMouseClick.length) {
                ColumnSelection[] csp = new ColumnSelection[mouseButton + 1];
                System.arraycopy(this.columnSelectionOnMouseClick, 0, csp, 0, this.columnSelectionOnMouseClick.length);
                this.columnSelectionOnMouseClick = csp;
            }
            this.columnSelectionOnMouseClick[mouseButton] = selection;
        }
    }

    public final void showColumnSelectionDialog() {
        ColumnSelectionPanel.showColumnSelectionDialog(this);
    }

    public static TableColumnSelector getDefaultColumnSelector() {
        return defaultColumnSelector;
    }

    public static void setDefaultColumnSelector(TableColumnSelector aDefaultColumnSelector) {
        defaultColumnSelector = aDefaultColumnSelector;
    }

    @Override
    public void setAutoCreateRowSorter(boolean autoCreateRowSorter) {
        if (this.getColumnModel() instanceof ETableColumnModel && autoCreateRowSorter) {
            throw new UnsupportedOperationException("ETable with ETableColumnModel has it's own sorting mechanism. JTable's RowSorter can not be used.");
        }
        super.setAutoCreateRowSorter(autoCreateRowSorter);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    @Override
    public void setRowSorter(RowSorter<? extends TableModel> sorter) {
        if (this.getColumnModel() instanceof ETableColumnModel) {
            if (sorter != null) throw new UnsupportedOperationException("ETable with ETableColumnModel has it's own sorting mechanism. Use ETableColumnModel to define sorting, or set a different TableColumnModel.");
            this.sortable = false;
            ((ETableColumnModel)this.getColumnModel()).clearSortedColumns();
            return;
        } else {
            super.setRowSorter(sorter);
        }
    }

    @Override
    public RowSorter<? extends TableModel> getRowSorter() {
        if (this.getColumnModel() instanceof ETableColumnModel) {
            return null;
        }
        return super.getRowSorter();
    }

    private static final class SelectedRows {
        int anchorInView;
        int anchorInModel;
        int leadInView;
        int leadInModel;
        int[] rowsInView;
        int[] rowsInModel;

        private SelectedRows() {
        }

        public String toString() {
            return "SelectedRows[anchorInView=" + this.anchorInView + ", anchorInModel=" + this.anchorInModel + ", leadInView=" + this.leadInView + ", leadInModel=" + this.leadInModel + ", rowsInView=" + Arrays.toString(this.rowsInView) + ", rowsInModel=" + Arrays.toString(this.rowsInModel) + "]";
        }
    }

    private class CTRLTabAction
    extends AbstractAction {
        private CTRLTabAction() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            ETable.this.setFocusCycleRoot(false);
            try {
                Container con = ETable.this.getFocusCycleRootAncestor();
                if (con != null) {
                    Component to;
                    AWTEvent eo = EventQueue.getCurrentEvent();
                    boolean backward = false;
                    if (eo instanceof KeyEvent) {
                        backward = (((KeyEvent)eo).getModifiers() & 1) != 0 && (((KeyEvent)eo).getModifiersEx() & 64) != 0;
                    }
                    Container c = ETable.this;
                    Container parentWithFTP = null;
                    do {
                        FocusTraversalPolicy ftp = con.getFocusTraversalPolicy();
                        Component component = to = backward ? ftp.getComponentBefore(con, c) : ftp.getComponentAfter(con, c);
                        if (to == ETable.this) {
                            Component component2 = to = backward ? ftp.getFirstComponent(con) : ftp.getLastComponent(con);
                        }
                        if (to != ETable.this) continue;
                        parentWithFTP = con.getParent();
                        if (parentWithFTP != null) {
                            parentWithFTP = parentWithFTP.getFocusCycleRootAncestor();
                        }
                        if (parentWithFTP == null) continue;
                        c = con;
                        con = parentWithFTP;
                    } while (to == ETable.this && parentWithFTP != null);
                    if (to != null) {
                        to.requestFocus();
                    }
                }
            }
            finally {
                ETable.this.setFocusCycleRoot(true);
            }
        }
    }

    private class EnterAction
    extends AbstractAction {
        private EnterAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            JButton b;
            JRootPane jrp = ETable.this.getRootPane();
            if (jrp != null && (b = ETable.this.getRootPane().getDefaultButton()) != null && b.isEnabled()) {
                b.doClick();
            }
        }

        @Override
        public boolean isEnabled() {
            return !ETable.this.isEditing() && !ETable.this.inRemoveRequest;
        }
    }

    private class CancelEditAction
    extends AbstractAction {
        private CancelEditAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Action a;
            if (ETable.this.isEditing() || ETable.this.editorComp != null) {
                ETable.this.removeEditor();
                return;
            }
            Component c = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            InputMap imp = ETable.this.getRootPane().getInputMap(1);
            ActionMap am = ETable.this.getRootPane().getActionMap();
            KeyStroke escape = KeyStroke.getKeyStroke(27, 0, false);
            Object key = imp.get(escape);
            if (key == null) {
                key = "Cancel";
            }
            if (key != null && (a = am.get(key)) != null) {
                String commandKey = (String)a.getValue("ActionCommandKey");
                if (commandKey == null) {
                    commandKey = key.toString();
                }
                a.actionPerformed(new ActionEvent(this, 1001, commandKey));
            }
        }

        @Override
        public boolean isEnabled() {
            return true;
        }
    }

    private class EditAction
    extends AbstractAction {
        private EditAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int row = ETable.this.getSelectedRow();
            int col = ETable.this.getSelectedColumn();
            int[] selectedRows = ETable.this.getSelectedRows();
            boolean edited = ETable.this.editCellAt(row, col, e);
            if (!edited) {
                int r;
                int[] arr$ = selectedRows;
                int len$ = arr$.length;
                for (int i$ = 0; !(i$ >= len$ || (r = arr$[i$]) != row && (edited = ETable.this.editCellAt(r, col, e))); ++i$) {
                }
            }
        }

        @Override
        public boolean isEnabled() {
            return ETable.this.getSelectedRow() != -1 && ETable.this.getSelectedColumn() != -1 && !ETable.this.isEditing();
        }
    }

    private final class NavigationAction
    extends AbstractAction {
        private boolean direction;

        public NavigationAction(boolean direction) {
            this.direction = direction;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int targetColumn;
            int targetRow;
            if (ETable.this.isEditing()) {
                ETable.this.removeEditor();
            }
            if (this.direction) {
                if (ETable.this.getSelectedColumn() == ETable.this.getColumnCount() - 1) {
                    targetColumn = 0;
                    targetRow = ETable.this.getSelectedRow() + 1;
                } else {
                    targetColumn = ETable.this.getSelectedColumn() + 1;
                    targetRow = ETable.this.getSelectedRow();
                }
            } else if (ETable.this.getSelectedColumn() == 0) {
                targetColumn = ETable.this.getColumnCount() - 1;
                targetRow = ETable.this.getSelectedRow() - 1;
            } else {
                targetRow = ETable.this.getSelectedRow();
                targetColumn = ETable.this.getSelectedColumn() - 1;
            }
            if (targetRow >= ETable.this.getRowCount() || targetRow < 0) {
                Container grandcestor;
                Component sibling;
                JRootPane rp;
                JButton jb;
                Container ancestor = ETable.this.getFocusCycleRootAncestor();
                Component component = sibling = this.direction ? ancestor.getFocusTraversalPolicy().getComponentAfter(ancestor, ETable.this) : ancestor.getFocusTraversalPolicy().getComponentBefore(ancestor, ETable.this);
                if (sibling == ETable.this && (grandcestor = ancestor.getFocusCycleRootAncestor()) != null) {
                    sibling = this.direction ? grandcestor.getFocusTraversalPolicy().getComponentAfter(grandcestor, ancestor) : grandcestor.getFocusTraversalPolicy().getComponentBefore(grandcestor, ancestor);
                    ancestor = grandcestor;
                }
                if (sibling == ETable.this && ancestor.getFocusTraversalPolicy().getFirstComponent(ancestor) != null) {
                    sibling = ancestor.getFocusTraversalPolicy().getFirstComponent(ancestor);
                }
                if (sibling == ETable.this && (jb = (rp = ETable.this.getRootPane()).getDefaultButton()) != null) {
                    sibling = jb;
                }
                if (sibling != null) {
                    if (sibling == ETable.this) {
                        ETable.this.changeSelection(this.direction ? 0 : ETable.this.getRowCount() - 1, this.direction ? 0 : ETable.this.getColumnCount() - 1, false, false);
                    } else {
                        sibling.requestFocus();
                    }
                    return;
                }
            }
            ETable.this.changeSelection(targetRow, targetColumn, false, false);
        }
    }

    private class STPolicy
    extends ContainerOrderFocusTraversalPolicy {
        private STPolicy() {
        }

        @Override
        public Component getComponentAfter(Container focusCycleRoot, Component aComponent) {
            if (ETable.this.inRemoveRequest) {
                return ETable.this;
            }
            Component result = super.getComponentAfter(focusCycleRoot, aComponent);
            return result;
        }

        @Override
        public Component getComponentBefore(Container focusCycleRoot, Component aComponent) {
            if (ETable.this.inRemoveRequest) {
                return ETable.this;
            }
            return super.getComponentBefore(focusCycleRoot, aComponent);
        }

        @Override
        public Component getFirstComponent(Container focusCycleRoot) {
            if (!ETable.this.inRemoveRequest && ETable.this.isEditing()) {
                return ETable.this.editorComp;
            }
            return ETable.this;
        }

        @Override
        public Component getDefaultComponent(Container focusCycleRoot) {
            if (ETable.this.inRemoveRequest && ETable.this.isEditing() && ETable.this.editorComp.isShowing()) {
                return ETable.this.editorComp;
            }
            return ETable.this;
        }

        @Override
        protected boolean accept(Component aComponent) {
            if (ETable.this.isEditing() && ETable.this.inEditRequest) {
                return ETable.this.isKnownComponent(aComponent);
            }
            return super.accept(aComponent) && aComponent.isShowing();
        }
    }

    private class HeaderMouseListener
    extends MouseAdapter {
        private HeaderMouseListener() {
        }

        @Override
        public void mouseClicked(MouseEvent me) {
            if (me.getButton() == 3) {
                ETable.this.showColumnSelection(me);
                return;
            }
            TableColumn resColumn = ETable.this.getResizingColumn(me.getPoint());
            if (ETable.this.sortable && resColumn == null && me.getClickCount() == 1) {
                int column = ETable.this.columnAtPoint(me.getPoint());
                if (column < 0) {
                    return;
                }
                TableColumnModel tcm = ETable.this.getColumnModel();
                if (tcm instanceof ETableColumnModel) {
                    ETableColumnModel etcm = (ETableColumnModel)tcm;
                    TableColumn tc = tcm.getColumn(column);
                    if (tc instanceof ETableColumn) {
                        int wasSelectedColumn;
                        SelectedRows selectedRows;
                        ETableColumn etc = (ETableColumn)tc;
                        if (!etc.isSortingAllowed()) {
                            return;
                        }
                        if (ETable.this.getUpdateSelectionOnSort()) {
                            selectedRows = ETable.this.getSelectedRowsInModel();
                            wasSelectedColumn = ETable.this.getSelectedColumn();
                        } else {
                            selectedRows = null;
                            wasSelectedColumn = -1;
                        }
                        boolean clear = (me.getModifiers() & 1) != 1;
                        etcm.toggleSortedColumn(etc, clear);
                        ETable.this.resetPermutation();
                        ETable.this.tableChanged(new TableModelEvent(ETable.this.getModel(), 0, ETable.this.getRowCount()));
                        if (selectedRows != null) {
                            ETable.this.changeSelectionInModel(selectedRows, wasSelectedColumn);
                        }
                        ETable.this.getTableHeader().resizeAndRepaint();
                    }
                }
            }
            if (resColumn != null && me.getClickCount() == 2 && resColumn instanceof ETableColumn) {
                ETableColumn etc = (ETableColumn)resColumn;
                etc.updatePreferredWidth(ETable.this, true);
            }
        }
    }

    private class ColumnSelectionMouseListener
    extends MouseAdapter {
        private ColumnSelectionMouseListener() {
        }

        @Override
        public void mouseClicked(MouseEvent me) {
            if (me.getButton() != 1) {
                ETable.this.showColumnSelection(me);
            }
        }
    }

    static class OriginalRowComparator
    implements Comparator<RowMapping> {
        @Override
        public int compare(RowMapping rm1, RowMapping rm2) {
            int i2;
            int i1 = rm1.getModelRowIndex();
            return i1 < (i2 = rm2.getModelRowIndex()) ? -1 : (i1 == i2 ? 0 : 1);
        }
    }

    public static final class RowMapping {
        private final int originalIndex;
        private final TableModel model;
        private final ETable table;
        private Object transformed = TRANSFORMED_NONE;
        private int transformedColumn;
        private Object[] allTransformed = null;
        private static final Object TRANSFORMED_NONE = new Object();

        public RowMapping(int index, TableModel model) {
            this.originalIndex = index;
            this.model = model;
            this.table = null;
        }

        public RowMapping(int index, TableModel model, ETable table) {
            this.originalIndex = index;
            this.model = model;
            this.table = table;
        }

        public int getModelRowIndex() {
            return this.originalIndex;
        }

        public Object getModelObject(int column) {
            return this.model.getValueAt(this.originalIndex, column);
        }

        public Object getTransformedValue(int column) {
            Object value;
            if (column >= this.model.getColumnCount()) {
                return null;
            }
            if (this.table == null) {
                throw new IllegalStateException("The table was not set.");
            }
            if (TRANSFORMED_NONE == this.transformed) {
                value = this.transformed = this.table.transformValue(this.getModelObject(column));
                this.transformedColumn = column;
            } else if (this.allTransformed != null) {
                if (this.allTransformed.length <= column) {
                    Object[] newTransformed = new Object[column + 1];
                    System.arraycopy(this.allTransformed, 0, newTransformed, 0, this.allTransformed.length);
                    for (int i = this.allTransformed.length; i < newTransformed.length; ++i) {
                        newTransformed[i] = TRANSFORMED_NONE;
                    }
                    this.allTransformed = newTransformed;
                }
                value = TRANSFORMED_NONE == this.allTransformed[column] ? (this.allTransformed[column] = this.table.transformValue(this.getModelObject(column))) : this.allTransformed[column];
            } else if (this.transformedColumn != column) {
                int n = Math.max(this.transformedColumn, column) + 1;
                this.allTransformed = new Object[n];
                for (int i = 0; i < n; ++i) {
                    this.allTransformed[i] = TRANSFORMED_NONE;
                }
                this.allTransformed[this.transformedColumn] = this.transformed;
                value = this.allTransformed[column] = this.table.transformValue(this.getModelObject(column));
            } else {
                value = this.transformed;
            }
            return value;
        }
    }

    private class SearchComboListener
    extends KeyAdapter
    implements FocusListener,
    ItemListener {
        SearchComboListener() {
        }

        @Override
        public void itemStateChanged(ItemEvent itemEvent) {
            Object selItem = ETable.this.searchCombo.getSelectedItem();
            Enumeration<TableColumn> en = ETable.this.getColumnModel().getColumns();
            while (en.hasMoreElements()) {
                TableColumn column = en.nextElement();
                if (!(column instanceof ETableColumn)) continue;
                ETableColumn etc = (ETableColumn)column;
                Object value = etc.getHeaderValue();
                String valueString = "";
                if (value != null) {
                    valueString = value.toString();
                }
                if (!(valueString = ETable.this.getColumnDisplayName(valueString)).equals(selItem)) continue;
                ETable.this.searchColumn = etc;
            }
            String text = ETable.this.searchTextField.getText();
            ETable.this.searchTextField.setText("");
            ETable.this.searchTextField.setText(text);
            ETable.this.searchTextField.requestFocus();
        }

        @Override
        public void keyPressed(KeyEvent e) {
            int keyCode = e.getKeyCode();
            if (keyCode == 27) {
                ETable.this.removeSearchField();
                ETable.this.requestFocus();
            }
        }

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {
            Component c = e.getOppositeComponent();
            if (c != ETable.this.searchTextField) {
                ETable.this.removeSearchField();
            }
        }
    }

    private class SearchFieldListener
    extends KeyAdapter
    implements DocumentListener,
    FocusListener {
        private List results;
        private int currentSelectionIndex;

        SearchFieldListener() {
            this.results = new ArrayList();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            this.searchForRow();
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            this.searchForRow();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            this.searchForRow();
        }

        @Override
        public void keyPressed(KeyEvent e) {
            int keyCode = e.getKeyCode();
            if (keyCode == 27) {
                ETable.this.removeSearchField();
                ETable.this.requestFocus();
            } else if (keyCode == 38) {
                --this.currentSelectionIndex;
                this.displaySearchResult();
                e.consume();
            } else if (keyCode == 40) {
                ++this.currentSelectionIndex;
                this.displaySearchResult();
                e.consume();
            } else if (keyCode == 9) {
                if (ETable.this.maxPrefix != null) {
                    ETable.this.searchTextField.setText(ETable.this.maxPrefix);
                }
                e.consume();
            } else if (keyCode == 10) {
                ETable.this.removeSearchField();
                e.consume();
                ETable.this.requestFocus();
            }
        }

        private void searchForRow() {
            this.currentSelectionIndex = 0;
            this.results.clear();
            ETable.this.maxPrefix = null;
            String text = ETable.this.searchTextField.getText().toUpperCase();
            if (text.length() > 0) {
                this.results = ETable.this.doSearch(text);
                int[] rows = ETable.this.getSelectedRows();
                int selectedRowIndex = rows == null || rows.length == 0 ? 0 : rows[0];
                int r = 0;
                Iterator it = this.results.iterator();
                while (it.hasNext()) {
                    int curResult = (Integer)it.next();
                    if (selectedRowIndex <= curResult) {
                        this.currentSelectionIndex = r;
                        break;
                    }
                    ++r;
                }
                this.displaySearchResult();
            }
        }

        private void displaySearchResult() {
            int sz = this.results.size();
            if (sz > 0) {
                if (this.currentSelectionIndex < 0) {
                    this.currentSelectionIndex = 0;
                }
                if (this.currentSelectionIndex >= sz) {
                    this.currentSelectionIndex = sz - 1;
                }
                int selRow = (Integer)this.results.get(this.currentSelectionIndex);
                ETable.this.setRowSelectionInterval(selRow, selRow);
                Rectangle rect = ETable.this.getCellRect(selRow, 0, true);
                ETable.this.scrollRectToVisible(rect);
                ETable.this.displaySearchField();
            } else {
                ETable.this.clearSelection();
            }
        }

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {
            Component c = e.getOppositeComponent();
            if (c != ETable.this.searchCombo) {
                ETable.this.removeSearchField();
            }
        }
    }

    private class SearchTextField
    extends JTextField {
        private SearchTextField() {
        }

        @Override
        public boolean isManagingFocus() {
            return true;
        }

        @Override
        public void processKeyEvent(KeyEvent ke) {
            if (ke.getKeyCode() == 27) {
                ETable.this.removeSearchField();
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        ETable.this.requestFocus();
                    }
                });
            } else {
                super.processKeyEvent(ke);
            }
        }

    }

    private class CompareQuickFilter
    implements ActionListener,
    QuickFilter {
        private int column;
        private Object value;
        private boolean greater;
        private boolean equalsCounts;

        public CompareQuickFilter(int column, Object value, boolean greater, boolean equalsCounts) {
            this.column = column;
            this.value = value;
            this.greater = greater;
            this.equalsCounts = equalsCounts;
        }

        @Override
        public boolean accept(Object aValue) {
            if (this.equalsCounts) {
                if (this.greater) {
                    return this.doCompare(this.value, aValue) <= 0;
                }
                return this.doCompare(this.value, aValue) >= 0;
            }
            if (this.greater) {
                return this.doCompare(this.value, aValue) < 0;
            }
            return this.doCompare(this.value, aValue) > 0;
        }

        private int doCompare(Object obj1, Object obj2) {
            if (obj1 == null && obj2 == null) {
                return 0;
            }
            if (obj1 == null) {
                return -1;
            }
            if (obj2 == null) {
                return 1;
            }
            if (obj1 instanceof Comparable && obj1.getClass().isAssignableFrom(obj2.getClass())) {
                Comparable c1 = (Comparable)obj1;
                return c1.compareTo(obj2);
            }
            return obj1.toString().compareTo(obj2.toString());
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            ETable.this.setQuickFilter(this.column, this);
        }
    }

    private class EqualsQuickFilter
    implements ActionListener,
    QuickFilter {
        private int column;
        private Object value;
        private boolean equals;

        public EqualsQuickFilter(int column, Object value, boolean equals) {
            this.column = column;
            this.value = value;
            this.equals = equals;
        }

        @Override
        public boolean accept(Object aValue) {
            if (this.value == null && aValue == null) {
                return this.equals;
            }
            if (this.value == null || aValue == null) {
                return !this.equals;
            }
            if (this.equals) {
                return this.value.equals(aValue);
            }
            return !this.value.equals(aValue);
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            ETable.this.setQuickFilter(this.column, this);
        }
    }

    public static enum ColumnSelection {
        NO_SELECTION,
        POPUP,
        DIALOG;
        

        private ColumnSelection() {
        }
    }

}

