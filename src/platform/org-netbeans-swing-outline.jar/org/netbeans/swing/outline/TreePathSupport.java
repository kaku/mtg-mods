/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.outline;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.AbstractLayoutCache;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreePath;
import org.netbeans.swing.outline.ExtTreeWillExpandListener;
import org.netbeans.swing.outline.OutlineModel;

public final class TreePathSupport {
    private List<TreeExpansionListener> eListeners = new ArrayList<TreeExpansionListener>();
    private List<TreeWillExpandListener> weListeners = new ArrayList<TreeWillExpandListener>();
    private AbstractLayoutCache layout;

    public TreePathSupport(OutlineModel mdl, AbstractLayoutCache layout) {
        this.layout = layout;
    }

    public void clear() {
    }

    public void expandPath(TreePath path) {
        assert (SwingUtilities.isEventDispatchThread());
        if (this.layout.isExpanded(path)) {
            return;
        }
        TreeExpansionEvent e = new TreeExpansionEvent(this, path);
        try {
            this.fireTreeWillExpand(e, true);
            this.layout.setExpandedState(path, true);
            this.fireTreeExpansion(e, true);
        }
        catch (ExpandVetoException eve) {
            this.fireTreeExpansionVetoed(e, eve);
        }
    }

    public void collapsePath(TreePath path) {
        assert (SwingUtilities.isEventDispatchThread());
        if (!this.layout.isExpanded(path)) {
            return;
        }
        TreeExpansionEvent e = new TreeExpansionEvent(this, path);
        try {
            this.fireTreeWillExpand(e, false);
            this.layout.setExpandedState(path, false);
            this.fireTreeExpansion(e, false);
        }
        catch (ExpandVetoException eve) {
            this.fireTreeExpansionVetoed(e, eve);
        }
    }

    public void removePath(TreePath path) {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fireTreeExpansion(TreeExpansionEvent e, boolean expanded) {
        int size = this.eListeners.size();
        TreeExpansionListener[] listeners = new TreeExpansionListener[size];
        TreePathSupport treePathSupport = this;
        synchronized (treePathSupport) {
            listeners = this.eListeners.toArray(listeners);
        }
        for (int i = 0; i < listeners.length; ++i) {
            if (expanded) {
                listeners[i].treeExpanded(e);
                continue;
            }
            listeners[i].treeCollapsed(e);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fireTreeWillExpand(TreeExpansionEvent e, boolean expanded) throws ExpandVetoException {
        int size = this.weListeners.size();
        TreeWillExpandListener[] listeners = new TreeWillExpandListener[size];
        TreePathSupport treePathSupport = this;
        synchronized (treePathSupport) {
            listeners = this.weListeners.toArray(listeners);
        }
        for (int i = 0; i < listeners.length; ++i) {
            if (expanded) {
                listeners[i].treeWillExpand(e);
                continue;
            }
            listeners[i].treeWillCollapse(e);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fireTreeExpansionVetoed(TreeExpansionEvent e, ExpandVetoException ex) {
        int size = this.weListeners.size();
        TreeWillExpandListener[] listeners = new TreeWillExpandListener[size];
        TreePathSupport treePathSupport = this;
        synchronized (treePathSupport) {
            listeners = this.weListeners.toArray(listeners);
        }
        for (int i = 0; i < listeners.length; ++i) {
            if (!(listeners[i] instanceof ExtTreeWillExpandListener)) continue;
            ((ExtTreeWillExpandListener)listeners[i]).treeExpansionVetoed(e, ex);
        }
    }

    public boolean hasBeenExpanded(TreePath path) {
        assert (SwingUtilities.isEventDispatchThread());
        return path != null && this.layout.isExpanded(path);
    }

    public boolean isExpanded(TreePath path) {
        assert (SwingUtilities.isEventDispatchThread());
        if (path == null) {
            return false;
        }
        if (!this.layout.isRootVisible() && path.getParentPath() == null) {
            return true;
        }
        boolean nodeExpanded = this.layout.isExpanded(path);
        if (!nodeExpanded) {
            return false;
        }
        TreePath parentPath = path.getParentPath();
        if (parentPath != null) {
            return this.isExpanded(parentPath);
        }
        return true;
    }

    public boolean isVisible(TreePath path) {
        if (path != null) {
            TreePath parentPath = path.getParentPath();
            if (parentPath != null) {
                return this.isExpanded(parentPath);
            }
            return true;
        }
        return false;
    }

    public TreePath[] getExpandedDescendants(TreePath parent) {
        assert (SwingUtilities.isEventDispatchThread());
        TreePath[] result = new TreePath[]{};
        if (this.isExpanded(parent)) {
            ArrayList<TreePath> results = null;
            Enumeration<TreePath> tpe = this.layout.getVisiblePathsFrom(parent);
            if (tpe != null) {
                while (tpe.hasMoreElements()) {
                    TreePath path = tpe.nextElement();
                    if (path == parent || !this.layout.isExpanded(path) || !parent.isDescendant(path)) continue;
                    if (results == null) {
                        results = new ArrayList<TreePath>();
                    }
                    results.add(path);
                }
                if (results != null) {
                    result = results.toArray(result);
                }
            }
        }
        return result;
    }

    public synchronized void addTreeExpansionListener(TreeExpansionListener l) {
        this.eListeners.add(l);
    }

    public synchronized void removeTreeExpansionListener(TreeExpansionListener l) {
        this.eListeners.remove(l);
    }

    public synchronized void addTreeWillExpandListener(TreeWillExpandListener l) {
        this.weListeners.add(l);
    }

    public synchronized void removeTreeWillExpandListener(TreeWillExpandListener l) {
        this.weListeners.remove(l);
    }
}

