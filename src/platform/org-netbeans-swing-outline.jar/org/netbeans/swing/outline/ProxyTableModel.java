/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.outline;

import java.util.ArrayList;
import java.util.List;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import org.netbeans.swing.outline.OutlineModel;
import org.netbeans.swing.outline.RowModel;

final class ProxyTableModel
implements TableModel {
    private List<TableModelListener> listeners = new ArrayList<TableModelListener>();
    private RowModel rowmodel;
    private OutlineModel outlineModel;

    public ProxyTableModel(RowModel rowmodel) {
        this.rowmodel = rowmodel;
    }

    void setOutlineModel(OutlineModel mdl) {
        this.outlineModel = mdl;
    }

    OutlineModel getOutlineModel() {
        return this.outlineModel;
    }

    public Class getColumnClass(int columnIndex) {
        return this.rowmodel.getColumnClass(columnIndex);
    }

    @Override
    public int getColumnCount() {
        return this.rowmodel.getColumnCount();
    }

    @Override
    public String getColumnName(int columnIndex) {
        return this.rowmodel.getColumnName(columnIndex);
    }

    @Override
    public int getRowCount() {
        return -1;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object node = this.getNodeForRow(rowIndex);
        if (node == null) {
            assert (false);
            return null;
        }
        return this.rowmodel.getValueFor(node, columnIndex);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        Object node = this.getNodeForRow(rowIndex);
        return this.rowmodel.isCellEditable(node, columnIndex);
    }

    @Override
    public synchronized void removeTableModelListener(TableModelListener l) {
        this.listeners.remove(l);
    }

    @Override
    public synchronized void addTableModelListener(TableModelListener l) {
        this.listeners.add(l);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fire(TableModelEvent e) {
        TableModelListener[] l;
        ProxyTableModel proxyTableModel = this;
        synchronized (proxyTableModel) {
            l = new TableModelListener[this.listeners.size()];
            l = this.listeners.toArray(l);
        }
        for (int i = 0; i < l.length; ++i) {
            l[i].tableChanged(e);
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Object node = this.getNodeForRow(rowIndex);
        this.rowmodel.setValueFor(node, columnIndex, aValue);
        TableModelEvent e = new TableModelEvent(this, rowIndex, rowIndex, columnIndex);
        this.fire(e);
    }

    private Object getNodeForRow(int row) {
        return this.getOutlineModel().getValueAt(row, 0);
    }
}

