/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.outline;

import javax.swing.table.TableModel;
import javax.swing.tree.AbstractLayoutCache;
import javax.swing.tree.TreeModel;
import org.netbeans.swing.outline.TreePathSupport;

public interface OutlineModel
extends TableModel,
TreeModel {
    public TreePathSupport getTreePathSupport();

    public AbstractLayoutCache getLayout();

    public boolean isLargeModel();
}

