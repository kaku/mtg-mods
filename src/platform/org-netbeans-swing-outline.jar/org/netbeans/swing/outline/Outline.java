/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.outline;

import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JToolTip;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.Timer;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TreeModelEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.tree.AbstractLayoutCache;
import javax.swing.tree.TreePath;
import org.netbeans.swing.etable.ETable;
import org.netbeans.swing.etable.ETableColumn;
import org.netbeans.swing.etable.ETableColumnModel;
import org.netbeans.swing.outline.CheckRenderDataProvider;
import org.netbeans.swing.outline.DefaultOutlineCellRenderer;
import org.netbeans.swing.outline.OutlineModel;
import org.netbeans.swing.outline.RenderDataProvider;
import org.netbeans.swing.outline.TreePathSupport;

public class Outline
extends ETable {
    private static final int MAX_TOOLTIP_LENGTH = 1000;
    private boolean initialized = false;
    private Boolean cachedRootVisible = null;
    private RenderDataProvider renderDataProvider = null;
    private ComponentListener componentListener = null;
    private boolean selectionDisabled = false;
    private boolean rowHeightIsSet = false;
    private int selectedRow = -1;
    private int[] lastEditPosition;
    private JToolTip toolTip = null;
    private transient Map<TreePath, ETable.RowMapping> tempSortMap = null;
    private final transient Object tempSortMapLock = new Object();
    private KeyStroke lastProcessedKeyStroke;

    public Outline() {
        this.init();
    }

    public Outline(OutlineModel mdl) {
        super(mdl);
        this.init();
    }

    private void init() {
        this.initialized = true;
        this.setDefaultRenderer(Object.class, new DefaultOutlineCellRenderer());
        ActionMap am = this.getActionMap();
        Action a = am.get("selectNextColumn");
        am.put("selectNextColumn", new ExpandAction(true, a));
        a = am.get("selectPreviousColumn");
        am.put("selectPreviousColumn", new ExpandAction(false, a));
        this.getSelectionModel().addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (Outline.this.getSelectedRowCount() == 1) {
                    Outline.this.selectedRow = Outline.this.getSelectedRow();
                } else {
                    Outline.this.selectedRow = -1;
                }
            }
        });
    }

    @Override
    public TableCellRenderer getCellRenderer(int row, int column) {
        TableColumn tableColumn;
        TableCellRenderer renderer;
        int c = this.convertColumnIndexToModel(column);
        TableCellRenderer result = c == 0 ? ((renderer = (tableColumn = this.getColumnModel().getColumn(column)).getCellRenderer()) == null ? this.getDefaultRenderer(Object.class) : renderer) : super.getCellRenderer(row, column);
        return result;
    }

    public RenderDataProvider getRenderDataProvider() {
        return this.renderDataProvider;
    }

    public void setRenderDataProvider(RenderDataProvider provider) {
        if (provider != this.renderDataProvider) {
            RenderDataProvider old = this.renderDataProvider;
            this.renderDataProvider = provider;
            this.firePropertyChange("renderDataProvider", old, provider);
        }
    }

    TreePathSupport getTreePathSupport() {
        OutlineModel mdl = this.getOutlineModel();
        if (mdl != null) {
            return mdl.getTreePathSupport();
        }
        return null;
    }

    public final AbstractLayoutCache getLayoutCache() {
        OutlineModel mdl = this.getOutlineModel();
        if (mdl != null) {
            return mdl.getLayout();
        }
        return null;
    }

    boolean isTreeColumnIndex(int column) {
        int c = this.convertColumnIndexToModel(column);
        return c == 0;
    }

    public boolean isVisible(TreePath path) {
        if (this.getTreePathSupport() != null) {
            return this.getTreePathSupport().isVisible(path);
        }
        return false;
    }

    @Override
    public void setRowHeight(int val) {
        this.rowHeightIsSet = true;
        super.setRowHeight(val);
        if (this.getLayoutCache() != null) {
            this.getLayoutCache().setRowHeight(val);
        }
    }

    public void setRootVisible(boolean val) {
        if (this.getOutlineModel() == null) {
            Boolean bl = this.cachedRootVisible = val ? Boolean.TRUE : Boolean.FALSE;
        }
        if (val != this.isRootVisible()) {
            TreePath rootPath;
            this.getLayoutCache().setRootVisible(val);
            if (this.getLayoutCache().getRowCount() > 0 && null != (rootPath = this.getLayoutCache().getPathForRow(0))) {
                this.getLayoutCache().treeStructureChanged(new TreeModelEvent((Object)this, rootPath));
            }
            this.sortAndFilter();
            this.firePropertyChange("rootVisible", !val, val);
        }
    }

    public boolean isRootVisible() {
        if (this.getLayoutCache() == null) {
            return this.cachedRootVisible != null ? this.cachedRootVisible : true;
        }
        return this.getLayoutCache().isRootVisible();
    }

    @Override
    public void setRowHeight(int row, int rowHeight) {
        Logger.getLogger(Outline.class.getName()).warning("Not supported yet.");
    }

    @Override
    protected TableColumn createColumn(int modelIndex) {
        return new OutlineColumn(modelIndex);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public String getToolTipText(MouseEvent event) {
        try {
            Object renderer;
            this.putClientProperty("ComputingTooltip", Boolean.TRUE);
            this.toolTip = null;
            String tipText = null;
            Point p = event.getPoint();
            int hitColumnIndex = this.columnAtPoint(p);
            int hitRowIndex = this.rowAtPoint(p);
            if (hitColumnIndex != -1 && hitRowIndex != -1) {
                RenderDataProvider rendata;
                Component component;
                Object value;
                String toolT;
                if (this.convertColumnIndexToModel(hitColumnIndex) == 0 && (rendata = this.getRenderDataProvider()) != null && (value = this.getValueAt(hitRowIndex, hitColumnIndex)) != null && (toolT = rendata.getTooltipText(value)) != null && (toolT = toolT.trim()).length() > 0) {
                    tipText = toolT;
                }
                if ((component = this.prepareRenderer((TableCellRenderer)(renderer = this.getCellRenderer(hitRowIndex, hitColumnIndex)), hitRowIndex, hitColumnIndex)) instanceof JComponent) {
                    Rectangle cellRect = this.getCellRect(hitRowIndex, hitColumnIndex, false);
                    p.translate(- cellRect.x, - cellRect.y);
                    MouseEvent newEvent = new MouseEvent(component, event.getID(), event.getWhen(), event.getModifiers(), p.x, p.y, event.getXOnScreen(), event.getYOnScreen(), event.getClickCount(), event.isPopupTrigger(), 0);
                    if (tipText == null) {
                        tipText = ((JComponent)component).getToolTipText(newEvent);
                    }
                    this.toolTip = ((JComponent)component).createToolTip();
                }
            }
            if (tipText == null) {
                tipText = this.getToolTipText();
            }
            if (tipText != null && (tipText = tipText.trim()).length() > 1000) {
                tipText = tipText.substring(0, 1000) + "...";
            }
            renderer = tipText;
            return renderer;
        }
        finally {
            this.putClientProperty("ComputingTooltip", Boolean.FALSE);
        }
    }

    @Override
    public JToolTip createToolTip() {
        JToolTip t = this.toolTip;
        this.toolTip = null;
        if (t != null) {
            t.addMouseMotionListener(new MouseMotionAdapter(){
                boolean initialized;

                @Override
                public void mouseMoved(MouseEvent e) {
                    if (!this.initialized) {
                        this.initialized = true;
                    } else {
                        ToolTipManager.sharedInstance().mousePressed(e);
                    }
                }
            });
            return t;
        }
        return super.createToolTip();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void sortAndFilter() {
        Comparator<ETable.RowMapping> c;
        ETableColumnModel etcm;
        TableColumnModel tcm = this.getColumnModel();
        if (tcm instanceof ETableColumnModel && (c = (etcm = (ETableColumnModel)tcm).getComparator()) != null) {
            int i;
            TableModel model = this.getModel();
            int noRows = model.getRowCount();
            ArrayList<ETable.RowMapping> rows = new ArrayList<ETable.RowMapping>();
            Object object = this.tempSortMapLock;
            synchronized (object) {
                if (this.tempSortMap != null) {
                    return;
                }
                HashMap<TreePath, ETable.RowMapping> tsm = new HashMap<TreePath, ETable.RowMapping>();
                for (i = 0; i < noRows; ++i) {
                    if (!this.acceptByQuickFilter(model, i)) continue;
                    TreePath tp = this.getLayoutCache().getPathForRow(i);
                    ETable.RowMapping rm = new ETable.RowMapping(i, model, this);
                    tsm.put(tp, rm);
                    rows.add(rm);
                }
                this.tempSortMap = tsm;
                Collections.sort(rows, c);
                this.tempSortMap = null;
            }
            int[] res = new int[rows.size()];
            int[] invRes = new int[noRows];
            i = 0;
            while (i < res.length) {
                int rmi;
                ETable.RowMapping rm = (ETable.RowMapping)rows.get(i);
                res[i] = rmi = rm.getModelRowIndex();
                invRes[rmi] = i++;
            }
            int[] oldRes = this.sortingPermutation;
            int[] oldInvRes = this.inverseSortingPermutation;
            this.sortingPermutation = res;
            this.inverseSortingPermutation = invRes;
        }
    }

    @Override
    public void setModel(TableModel mdl) {
        if (this.initialized && !(mdl instanceof OutlineModel)) {
            throw new IllegalArgumentException("Table model for an Outline must be an instance of OutlineModel");
        }
        if (mdl instanceof OutlineModel) {
            AbstractLayoutCache layout = ((OutlineModel)mdl).getLayout();
            if (this.cachedRootVisible != null) {
                layout.setRootVisible(this.cachedRootVisible);
            }
            layout.setRowHeight(this.getRowHeight());
            if (((OutlineModel)mdl).isLargeModel()) {
                this.addComponentListener(this.getComponentListener());
                layout.setNodeDimensions(new ND());
            } else if (this.componentListener != null) {
                this.removeComponentListener(this.componentListener);
                this.componentListener = null;
            }
        }
        super.setModel(mdl);
    }

    public OutlineModel getOutlineModel() {
        TableModel mdl = this.getModel();
        if (mdl instanceof OutlineModel) {
            return (OutlineModel)this.getModel();
        }
        return null;
    }

    public void expandPath(TreePath path) {
        this.getTreePathSupport().expandPath(path);
    }

    public boolean isExpanded(TreePath path) {
        return this.getTreePathSupport().isExpanded(path);
    }

    public void collapsePath(TreePath path) {
        this.getTreePathSupport().collapsePath(path);
    }

    public Rectangle getPathBounds(TreePath path) {
        Insets i = this.getInsets();
        Rectangle bounds = this.getLayoutCache().getBounds(path, null);
        if (bounds != null && i != null) {
            bounds.x += i.left;
            bounds.y += i.top;
        }
        return bounds;
    }

    public TreePath getClosestPathForLocation(int x, int y) {
        Insets i = this.getInsets();
        TreePath tp = i != null ? this.getLayoutCache().getPathClosestTo(x - i.left, y - i.top) : this.getLayoutCache().getPathClosestTo(x, y);
        int row = this.getLayoutCache().getRowForPath(tp);
        row = this.convertRowIndexToModel(row);
        tp = this.getLayoutCache().getPathForRow(row);
        return tp;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition, boolean pressed) {
        this.lastProcessedKeyStroke = ks;
        try {
            boolean bl = super.processKeyBinding(ks, e, condition, pressed);
            return bl;
        }
        finally {
            this.lastProcessedKeyStroke = null;
        }
    }

    @Override
    public boolean editCellAt(int row, int column, EventObject e) {
        boolean isTreeColumn = this.isTreeColumnIndex(column);
        if (isTreeColumn && e instanceof MouseEvent) {
            MouseEvent me = (MouseEvent)e;
            TreePath path = this.getLayoutCache().getPathForRow(this.convertRowIndexToModel(row));
            if (!this.getOutlineModel().isLeaf(path.getLastPathComponent())) {
                int handleWidth = DefaultOutlineCellRenderer.getExpansionHandleWidth();
                Insets ins = this.getInsets();
                int nd = path.getPathCount() - (this.isRootVisible() ? 1 : 2);
                if (nd < 0) {
                    nd = 0;
                }
                int handleStart = ins.left + nd * DefaultOutlineCellRenderer.getNestingWidth();
                int handleEnd = ins.left + handleStart + handleWidth;
                int columnStart = this.getCellRect((int)row, (int)column, (boolean)false).x;
                TableColumn tableColumn = this.getColumnModel().getColumn(column);
                TableCellEditor columnCellEditor = tableColumn.getCellEditor();
                if (me.getX() > ins.left && me.getX() >= (handleStart += columnStart) && me.getX() <= (handleEnd += columnStart) || me.getClickCount() > 1 && columnCellEditor == null) {
                    boolean expanded = this.getLayoutCache().isExpanded(path);
                    if (!expanded) {
                        this.getTreePathSupport().expandPath(path);
                        Object ourObject = path.getLastPathComponent();
                        int cCount = this.getOutlineModel().getChildCount(ourObject);
                        if (cCount > 0) {
                            int lastRow = row;
                            for (int i = 0; i < cCount; ++i) {
                                Object child = this.getOutlineModel().getChild(ourObject, i);
                                TreePath childPath = path.pathByAddingChild(child);
                                int childRow = this.getLayoutCache().getRowForPath(childPath);
                                if ((childRow = this.convertRowIndexToView(childRow)) <= lastRow) continue;
                                lastRow = childRow;
                            }
                            int firstRow = row;
                            Rectangle rectLast = this.getCellRect(lastRow, 0, true);
                            Rectangle rectFirst = this.getCellRect(firstRow, 0, true);
                            Rectangle rectFull = new Rectangle(rectFirst.x, rectFirst.y, rectLast.x + rectLast.width - rectFirst.x, rectLast.y + rectLast.height - rectFirst.y);
                            this.scrollRectToVisible(rectFull);
                        }
                    } else {
                        this.getTreePathSupport().collapsePath(path);
                    }
                    this.selectionDisabled = true;
                    return false;
                }
            }
            if (this.checkAt(row, column, me)) {
                return false;
            }
        } else if (isTreeColumn && e instanceof ActionEvent && ((ActionEvent)e).getModifiers() == 0 && this.lastProcessedKeyStroke != null && this.lastProcessedKeyStroke.getKeyCode() == 32 && this.checkAt(row, column, null)) {
            return false;
        }
        boolean res = false;
        if (!isTreeColumn || e instanceof MouseEvent && this.isEditEvent(row, column, (MouseEvent)e)) {
            res = super.editCellAt(row, column, e);
        }
        if (res && isTreeColumn && null != this.getEditorComponent()) {
            this.configureTreeCellEditor(this.getEditorComponent(), row, column);
        }
        if (e == null && !res && isTreeColumn) {
            this.checkAt(row, column, null);
        }
        return res;
    }

    private boolean isEditEvent(int row, int column, MouseEvent me) {
        boolean noModifiers;
        if (me.getClickCount() > 1) {
            return true;
        }
        boolean bl = noModifiers = me.getModifiersEx() == 1024;
        if (this.lastEditPosition != null && this.selectedRow == row && noModifiers && this.lastEditPosition[0] == row && this.lastEditPosition[1] == column) {
            int handleWidth = DefaultOutlineCellRenderer.getExpansionHandleWidth();
            Insets ins = this.getInsets();
            TreePath path = this.getLayoutCache().getPathForRow(this.convertRowIndexToModel(row));
            int nd = path.getPathCount() - (this.isRootVisible() ? 1 : 2);
            if (nd < 0) {
                nd = 0;
            }
            int handleStart = ins.left + nd * DefaultOutlineCellRenderer.getNestingWidth();
            int handleEnd = ins.left + handleStart + handleWidth;
            int columnStart = this.getCellRect((int)row, (int)column, (boolean)false).x;
            handleStart += columnStart;
            if (me.getX() >= (handleEnd += columnStart)) {
                this.lastEditPosition = null;
                return true;
            }
        }
        this.lastEditPosition = new int[]{row, column};
        return false;
    }

    protected final boolean checkAt(int row, int column, MouseEvent me) {
        RenderDataProvider render = this.getRenderDataProvider();
        TableCellRenderer tcr = this.getDefaultRenderer(Object.class);
        if (render instanceof CheckRenderDataProvider && tcr instanceof DefaultOutlineCellRenderer) {
            CheckRenderDataProvider crender = (CheckRenderDataProvider)render;
            DefaultOutlineCellRenderer ocr = (DefaultOutlineCellRenderer)tcr;
            Object value = this.getValueAt(row, column);
            if (crender.isCheckable(value) && crender.isCheckEnabled(value)) {
                boolean chBoxPosition;
                if (me == null) {
                    chBoxPosition = true;
                } else {
                    int handleWidth = DefaultOutlineCellRenderer.getExpansionHandleWidth();
                    int chWidth = ocr.getTheCheckBoxWidth();
                    Insets ins = this.getInsets();
                    TreePath path = this.getLayoutCache().getPathForRow(this.convertRowIndexToModel(row));
                    int nd = path.getPathCount() - (this.isRootVisible() ? 1 : 2);
                    if (nd < 0) {
                        nd = 0;
                    }
                    int chStart = ins.left + nd * DefaultOutlineCellRenderer.getNestingWidth() + handleWidth;
                    int chEnd = chStart + chWidth;
                    boolean bl = chBoxPosition = me.getX() > ins.left && me.getX() >= chStart && me.getX() <= chEnd;
                }
                if (chBoxPosition) {
                    Boolean selected = crender.isSelected(value);
                    if (selected == null || Boolean.TRUE.equals(selected)) {
                        crender.setSelected(value, Boolean.FALSE);
                    } else {
                        crender.setSelected(value, Boolean.TRUE);
                    }
                    Rectangle r = this.getCellRect(row, column, true);
                    this.repaint(r.x, r.y, r.width, r.height);
                    return true;
                }
            }
        }
        return false;
    }

    protected void configureTreeCellEditor(Component editor, int row, int column) {
        if (!(editor instanceof JComponent)) {
            return;
        }
        TreeCellEditorBorder b = new TreeCellEditorBorder();
        TreePath path = this.getLayoutCache().getPathForRow(this.convertRowIndexToModel(row));
        Object o = this.getValueAt(row, column);
        RenderDataProvider rdp = this.getRenderDataProvider();
        b.icon = rdp.getIcon(o);
        b.nestingDepth = Math.max(0, path.getPathCount() - (this.isRootVisible() ? 1 : 2));
        b.isLeaf = this.getOutlineModel().isLeaf(o);
        b.isExpanded = this.getLayoutCache().isExpanded(path);
        ((JComponent)editor).setBorder(b);
    }

    @Override
    public void addNotify() {
        super.addNotify();
        if (!this.rowHeightIsSet) {
            this.calcRowHeight();
        }
    }

    private void calcRowHeight() {
        Integer i = (Integer)UIManager.get("netbeans.outline.rowHeight");
        int rHeight = 20;
        if (i != null) {
            rHeight = i;
        } else {
            Font f = this.getFont();
            FontMetrics fm = this.getFontMetrics(f);
            int h = Math.max(fm.getHeight() + fm.getMaxDescent(), DefaultOutlineCellRenderer.getExpansionHandleHeight());
            rHeight = Math.max(rHeight, h) + 2;
        }
        this.setRowHeight(rHeight);
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        super.tableChanged(e);
    }

    @Override
    public void changeSelection(int rowIndex, int columnIndex, boolean toggle, boolean extend) {
        if (this.selectionDisabled) {
            return;
        }
        super.changeSelection(rowIndex, columnIndex, toggle, extend);
    }

    @Override
    protected void processMouseEvent(MouseEvent e) {
        switch (e.getID()) {
            case 501: {
                this.selectionDisabled = false;
                break;
            }
            case 502: {
                this.selectionDisabled = false;
                break;
            }
            case 500: {
                this.selectionDisabled = false;
                break;
            }
            case 504: {
                this.selectionDisabled = false;
                break;
            }
            case 505: {
                this.selectionDisabled = false;
                break;
            }
            case 503: {
                break;
            }
            case 506: {
                if (!this.selectionDisabled) break;
                return;
            }
        }
        super.processMouseEvent(e);
    }

    private ComponentListener getComponentListener() {
        if (this.componentListener == null) {
            this.componentListener = new SizeManager();
        }
        return this.componentListener;
    }

    private JScrollPane getScrollPane() {
        JScrollPane result = null;
        if (this.getParent() instanceof JViewport && ((JViewport)this.getParent()).getParent() instanceof JScrollPane) {
            result = (JScrollPane)((JViewport)this.getParent()).getParent();
        }
        return result;
    }

    private void change() {
        this.revalidate();
        this.repaint();
    }

    private class ExpandAction
    extends AbstractAction {
        private boolean expand;
        private Action origAction;

        public ExpandAction(boolean expand, Action orig) {
            this.expand = expand;
            this.origAction = orig;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (Outline.this.getSelectedRowCount() == 1 && Outline.this.isTreeColumnIndex(Outline.this.getSelectedColumn())) {
                TreePath parentPath;
                TreePath selPath = Outline.this.getLayoutCache().getPathForRow(Outline.this.convertRowIndexToModel(Outline.this.getSelectedRow()));
                if (null != selPath && !Outline.this.getOutlineModel().isLeaf(selPath.getLastPathComponent())) {
                    TreePath parentPath2;
                    boolean expanded = Outline.this.getLayoutCache().isExpanded(selPath);
                    if (expanded && !this.expand) {
                        Outline.this.collapsePath(selPath);
                        return;
                    }
                    if (!expanded && this.expand) {
                        Outline.this.expandPath(selPath);
                        return;
                    }
                    if (expanded && this.expand && Outline.this.getOutlineModel().getChildCount(selPath.getLastPathComponent()) > 0) {
                        int row = Outline.this.getSelectedRow() + 1;
                        if (row < Outline.this.getRowCount()) {
                            this.selectCell(row, Outline.this.getSelectedColumn());
                            return;
                        }
                    } else if (!expanded && !this.expand && (parentPath2 = selPath.getParentPath()) != null) {
                        int row = Outline.this.convertRowIndexToView(Outline.this.getLayoutCache().getRowForPath(parentPath2));
                        this.selectCell(row, Outline.this.getSelectedColumn());
                        return;
                    }
                } else if (null != selPath && Outline.this.getOutlineModel().isLeaf(selPath.getLastPathComponent()) && !this.expand && (parentPath = selPath.getParentPath()) != null) {
                    int row = Outline.this.convertRowIndexToView(Outline.this.getLayoutCache().getRowForPath(parentPath));
                    this.selectCell(row, Outline.this.getSelectedColumn());
                    return;
                }
            }
            if (null != this.origAction) {
                this.origAction.actionPerformed(e);
            }
        }

        private void selectCell(int row, int col) {
            Outline.this.changeSelection(row, col, false, false);
            Outline.this.scrollRectToVisible(Outline.this.getCellRect(row, col, false));
        }
    }

    private static class TreeCellEditorBorder
    implements Border {
        private Insets insets = new Insets(0, 0, 0, 0);
        private boolean isLeaf;
        private boolean isExpanded;
        private Icon icon;
        private int nestingDepth;
        private final int ICON_TEXT_GAP = new JLabel().getIconTextGap();

        private TreeCellEditorBorder() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            this.insets.left = this.nestingDepth * DefaultOutlineCellRenderer.getNestingWidth() + DefaultOutlineCellRenderer.getExpansionHandleWidth() + 1;
            this.insets.left += this.icon != null ? this.icon.getIconWidth() + this.ICON_TEXT_GAP : 0;
            this.insets.top = 1;
            this.insets.right = 1;
            this.insets.bottom = 1;
            return this.insets;
        }

        @Override
        public boolean isBorderOpaque() {
            return false;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            int iconY;
            int iconX = this.nestingDepth * DefaultOutlineCellRenderer.getNestingWidth();
            if (!this.isLeaf) {
                Icon expIcon = this.isExpanded ? DefaultOutlineCellRenderer.getExpandedIcon() : DefaultOutlineCellRenderer.getCollapsedIcon();
                iconY = expIcon.getIconHeight() < height ? height / 2 - expIcon.getIconHeight() / 2 : 0;
                expIcon.paintIcon(c, g, iconX, iconY);
            }
            if (null != this.icon) {
                iconY = this.icon.getIconHeight() < height ? height / 2 - this.icon.getIconHeight() / 2 : 0;
                this.icon.paintIcon(c, g, iconX += DefaultOutlineCellRenderer.getExpansionHandleWidth() + 1, iconY);
            }
        }
    }

    private class SizeManager
    extends ComponentAdapter
    implements ActionListener {
        protected Timer timer;
        protected JScrollBar scrollBar;

        private SizeManager() {
            this.timer = null;
            this.scrollBar = null;
        }

        @Override
        public void componentMoved(ComponentEvent e) {
            if (this.timer == null) {
                JScrollPane scrollPane = Outline.this.getScrollPane();
                if (scrollPane == null) {
                    Outline.this.change();
                } else {
                    this.scrollBar = scrollPane.getVerticalScrollBar();
                    if (this.scrollBar == null || !this.scrollBar.getValueIsAdjusting()) {
                        this.scrollBar = scrollPane.getHorizontalScrollBar();
                        if (this.scrollBar != null && this.scrollBar.getValueIsAdjusting()) {
                            this.startTimer();
                        } else {
                            Outline.this.change();
                        }
                    } else {
                        this.startTimer();
                    }
                }
            }
        }

        protected void startTimer() {
            if (this.timer == null) {
                this.timer = new Timer(200, this);
                this.timer.setRepeats(true);
            }
            this.timer.start();
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            if (this.scrollBar == null || !this.scrollBar.getValueIsAdjusting()) {
                if (this.timer != null) {
                    this.timer.stop();
                }
                Outline.this.change();
                this.timer = null;
                this.scrollBar = null;
            }
        }

        @Override
        public void componentHidden(ComponentEvent e) {
        }

        @Override
        public void componentResized(ComponentEvent e) {
        }

        @Override
        public void componentShown(ComponentEvent e) {
        }
    }

    private class ND
    extends AbstractLayoutCache.NodeDimensions {
        private ND() {
        }

        @Override
        public Rectangle getNodeDimensions(Object value, int row, int depth, boolean expanded, Rectangle bounds) {
            int wid = Outline.this.getColumnModel().getColumn(0).getPreferredWidth();
            bounds.setBounds(0, row * Outline.this.getRowHeight(), wid, Outline.this.getRowHeight());
            return bounds;
        }
    }

    protected class OutlineColumn
    extends ETableColumn {
        public OutlineColumn(int modelIndex) {
            super(modelIndex, Outline.this);
        }

        @Override
        protected Comparator<ETable.RowMapping> getRowComparator(int column, boolean ascending) {
            return new OutlineRowComparator(column, ascending);
        }

        @Override
        public boolean isHidingAllowed() {
            return this.getModelIndex() != 0;
        }

        @Override
        public boolean isSortingAllowed() {
            return true;
        }

        private class OutlineRowComparator
        extends ETableColumn.RowComparator {
            private boolean ascending;

            public OutlineRowComparator(int column, boolean ascending) {
                super(column);
                this.ascending = true;
                this.ascending = ascending;
            }

            @Override
            public int compare(ETable.RowMapping rm1, ETable.RowMapping rm2) {
                int index2;
                int index1 = rm1.getModelRowIndex();
                if (index1 == (index2 = rm2.getModelRowIndex())) {
                    return 0;
                }
                TreePath tp1 = Outline.this.getLayoutCache().getPathForRow(index1);
                TreePath tp2 = Outline.this.getLayoutCache().getPathForRow(index2);
                if (tp1 == null) {
                    if (tp2 == null) {
                        return 0;
                    }
                    return -1;
                }
                if (tp2 == null) {
                    return 1;
                }
                if (tp1.isDescendant(tp2)) {
                    return -1;
                }
                if (tp2.isDescendant(tp1)) {
                    return 1;
                }
                boolean tp1Changed = false;
                boolean tp2Changed = false;
                TreePath parent1 = tp1.getParentPath();
                TreePath parent2 = tp2.getParentPath();
                if (parent1 != null && parent2 != null && parent1.equals(parent2) && Outline.this.getOutlineModel().isLeaf(tp1.getLastPathComponent()) && Outline.this.getOutlineModel().isLeaf(tp2.getLastPathComponent())) {
                    return this.ascending ? super.compare(rm1, rm2) : - super.compare(rm1, rm2);
                }
                while (tp1.getPathCount() < tp2.getPathCount()) {
                    tp2 = tp2.getParentPath();
                    tp2Changed = true;
                }
                while (tp1.getPathCount() > tp2.getPathCount()) {
                    tp1 = tp1.getParentPath();
                    tp1Changed = true;
                }
                parent1 = tp1.getParentPath();
                parent2 = tp2.getParentPath();
                while (parent1 != null && parent2 != null && !parent1.equals(parent2)) {
                    tp1 = parent1;
                    tp2 = parent2;
                    parent1 = tp1.getParentPath();
                    parent2 = tp2.getParentPath();
                    tp1Changed = true;
                    tp2Changed = true;
                }
                if (tp1Changed || tp2Changed) {
                    return this.compare((ETable.RowMapping)Outline.this.tempSortMap.get(tp1), (ETable.RowMapping)Outline.this.tempSortMap.get(tp2));
                }
                return this.ascending ? super.compare(rm1, rm2) : - super.compare(rm1, rm2);
            }
        }

    }

}

