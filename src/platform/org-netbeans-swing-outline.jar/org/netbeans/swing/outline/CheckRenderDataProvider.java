/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.outline;

import org.netbeans.swing.outline.RenderDataProvider;

public interface CheckRenderDataProvider
extends RenderDataProvider {
    public boolean isCheckable(Object var1);

    public boolean isCheckEnabled(Object var1);

    public Boolean isSelected(Object var1);

    public void setSelected(Object var1, Boolean var2);
}

