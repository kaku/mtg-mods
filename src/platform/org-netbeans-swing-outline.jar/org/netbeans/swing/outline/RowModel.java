/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.outline;

public interface RowModel {
    public int getColumnCount();

    public Object getValueFor(Object var1, int var2);

    public Class getColumnClass(int var1);

    public boolean isCellEditable(Object var1, int var2);

    public void setValueFor(Object var1, int var2, Object var3);

    public String getColumnName(int var1);
}

