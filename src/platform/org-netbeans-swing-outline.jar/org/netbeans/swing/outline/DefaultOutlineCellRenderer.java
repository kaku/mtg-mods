/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.outline;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.CellRendererPane;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.LabelUI;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.AbstractLayoutCache;
import javax.swing.tree.TreePath;
import org.netbeans.swing.outline.CheckRenderDataProvider;
import org.netbeans.swing.outline.Outline;
import org.netbeans.swing.outline.OutlineModel;
import org.netbeans.swing.outline.RenderDataProvider;

public class DefaultOutlineCellRenderer
extends DefaultTableCellRenderer {
    private static int expansionHandleWidth = 0;
    private static int expansionHandleHeight = 0;
    private boolean expanded = false;
    private boolean leaf = true;
    private boolean showHandle = true;
    private int nestingDepth = 0;
    private final JCheckBox theCheckBox = new JCheckBox();
    private final CellRendererPane fakeCellRendererPane;
    private JCheckBox checkBox;
    private Reference<RenderDataProvider> lastRendererRef = new WeakReference<Object>(null);
    private Reference<Object> lastRenderedValueRef = new WeakReference<Object>(null);
    private static final Border expansionBorder = new ExpansionHandleBorder();
    private static final Class htmlRendererClass = DefaultOutlineCellRenderer.useSwingHtmlRendering() ? null : HtmlRenderer.getDelegate();
    private final HtmlRenderer.Renderer htmlRenderer = htmlRendererClass != null ? HtmlRenderer.access$100(htmlRendererClass) : null;
    private final boolean swingRendering = this.htmlRenderer == null;

    private static boolean useSwingHtmlRendering() {
        try {
            return Boolean.getBoolean("nb.useSwingHtmlRendering");
        }
        catch (SecurityException se) {
            return false;
        }
    }

    public DefaultOutlineCellRenderer() {
        this.theCheckBox.setSize(this.theCheckBox.getPreferredSize());
        this.theCheckBox.setBorderPainted(false);
        this.theCheckBox.setOpaque(false);
        this.fakeCellRendererPane = new CellRendererPane();
        this.fakeCellRendererPane.add(this.theCheckBox);
    }

    @Override
    public final void setBorder(Border b) {
        if (!this.swingRendering) {
            super.setBorder(b);
            return;
        }
        if (b == expansionBorder) {
            super.setBorder(b);
        } else {
            super.setBorder(BorderFactory.createCompoundBorder(b, expansionBorder));
        }
    }

    @Override
    protected void setValue(Object value) {
        if (this.swingRendering) {
            super.setValue(value);
        }
    }

    private static Icon getDefaultOpenIcon() {
        return UIManager.getIcon("Tree.openIcon");
    }

    private static Icon getDefaultClosedIcon() {
        return UIManager.getIcon("Tree.closedIcon");
    }

    private static Icon getDefaultLeafIcon() {
        return UIManager.getIcon("Tree.leafIcon");
    }

    static Icon getExpandedIcon() {
        return UIManager.getIcon("Tree.expandedIcon");
    }

    static Icon getCollapsedIcon() {
        return UIManager.getIcon("Tree.collapsedIcon");
    }

    static int getNestingWidth() {
        return DefaultOutlineCellRenderer.getExpansionHandleWidth();
    }

    static int getExpansionHandleWidth() {
        if (expansionHandleWidth == 0) {
            expansionHandleWidth = DefaultOutlineCellRenderer.getExpandedIcon().getIconWidth();
        }
        return expansionHandleWidth;
    }

    static int getExpansionHandleHeight() {
        if (expansionHandleHeight == 0) {
            expansionHandleHeight = DefaultOutlineCellRenderer.getExpandedIcon().getIconHeight();
        }
        return expansionHandleHeight;
    }

    private void setNestingDepth(int i) {
        this.nestingDepth = i;
    }

    private void setExpanded(boolean val) {
        this.expanded = val;
    }

    private void setLeaf(boolean val) {
        this.leaf = val;
    }

    private void setShowHandle(boolean val) {
        this.showHandle = val;
    }

    private void setCheckBox(JCheckBox checkBox) {
        this.checkBox = checkBox;
    }

    private boolean isLeaf() {
        return this.leaf;
    }

    private boolean isExpanded() {
        return this.expanded;
    }

    private boolean isShowHandle() {
        return this.showHandle;
    }

    private int getNestingDepth() {
        return this.nestingDepth;
    }

    private JCheckBox getCheckBox() {
        return this.checkBox;
    }

    int getTheCheckBoxWidth() {
        return this.theCheckBox.getSize().width;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Outline tbl;
        this.setForeground(null);
        this.setBackground(null);
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        JLabel label = null;
        UIDefaults laf = UIManager.getLookAndFeelDefaults();
        if (!this.swingRendering) {
            this.htmlRenderer.setColors(this.getForeground(), this.getBackground());
            label = (JLabel)this.htmlRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            LabelUI labelUI = label.getUI();
            if (labelUI != null && labelUI.getClass().getName().contains("org.openide.awt.HtmlLabelUI")) {
                label.putClientProperty("htmlLabelIgnoreEnsureContrastingColor", Boolean.TRUE);
                label.putClientProperty("htmlLabelCustomSelectionForegroundColor", Boolean.TRUE);
            }
        }
        if ((tbl = (Outline)table).isTreeColumnIndex(column)) {
            int nd;
            AbstractLayoutCache layout = tbl.getLayoutCache();
            row = tbl.convertRowIndexToModel(row);
            boolean isleaf = tbl.getOutlineModel().isLeaf(value);
            this.setLeaf(isleaf);
            this.setShowHandle(true);
            TreePath path = layout.getPathForRow(row);
            boolean isExpanded = layout.isExpanded(path);
            this.setExpanded(isExpanded);
            int n = path == null ? 0 : (nd = path.getPathCount() - (tbl.isRootVisible() ? 1 : 2));
            if (nd < 0) {
                nd = 0;
            }
            this.setNestingDepth(nd);
            RenderDataProvider rendata = tbl.getRenderDataProvider();
            Icon icon = null;
            if (rendata != null && value != null) {
                CheckRenderDataProvider crendata;
                Color displayNameFg = null;
                String displayName = rendata.getDisplayName(value);
                if (displayName != null) {
                    if (rendata.isHtmlDisplayName(value) && !displayName.startsWith("<html") && !displayName.startsWith("<HTML")) {
                        if (this.swingRendering) {
                            this.setText("<html>" + displayName.replaceAll(" ", "&nbsp;") + "</html>");
                        } else {
                            label.setText("<html>" + displayName.replaceAll(" ", "&nbsp;") + "</html>");
                        }
                    } else if (this.swingRendering) {
                        this.setText(displayName);
                    } else {
                        label.setText(displayName);
                    }
                    displayNameFg = DefaultOutlineCellRenderer.getHtmlColor(displayName);
                }
                this.lastRendererRef = new WeakReference<RenderDataProvider>(rendata);
                this.lastRenderedValueRef = new WeakReference<Object>(value);
                Color bg = rendata.getBackground(value);
                Color fg = rendata.getForeground(value);
                if (bg != null && !isSelected) {
                    if (this.swingRendering) {
                        this.setBackground(bg);
                    } else {
                        label.setBackground(bg);
                    }
                } else if (!this.swingRendering) {
                    label.setBackground(this.getBackground());
                }
                icon = rendata.getIcon(value);
                if (fg != null && !isSelected) {
                    if (this.swingRendering) {
                        this.setForeground(fg);
                    } else {
                        label.setForeground(fg);
                    }
                } else if (!this.swingRendering) {
                    if (displayNameFg != null) {
                        label.setForeground(displayNameFg);
                    } else if (icon == null) {
                        label.setForeground(laf.getColor("outline-htmllabel-fg"));
                    } else {
                        label.setForeground(laf.getColor("outline-htmllabel-main-fg"));
                    }
                }
                JCheckBox cb = null;
                if (rendata instanceof CheckRenderDataProvider && (crendata = (CheckRenderDataProvider)rendata).isCheckable(value)) {
                    cb = this.theCheckBox;
                    Boolean chSelected = crendata.isSelected(value);
                    cb.setEnabled(true);
                    cb.setSelected(!Boolean.FALSE.equals(chSelected));
                    cb.getModel().setArmed(chSelected == null);
                    cb.getModel().setPressed(chSelected == null);
                    cb.setEnabled(crendata.isCheckEnabled(value));
                    cb.setBackground(this.getBackground());
                }
                this.setCheckBox(cb);
            } else {
                this.setCheckBox(null);
            }
            if (icon == null) {
                icon = !isleaf ? (isExpanded ? DefaultOutlineCellRenderer.getDefaultOpenIcon() : DefaultOutlineCellRenderer.getDefaultClosedIcon()) : DefaultOutlineCellRenderer.getDefaultLeafIcon();
            }
            if (this.swingRendering) {
                this.setIcon(icon);
            } else {
                label.setIcon(icon);
            }
        } else {
            this.setCheckBox(null);
            if (this.swingRendering) {
                this.setIcon(null);
            } else {
                label.setIcon(null);
            }
            this.setShowHandle(false);
            this.lastRendererRef = new WeakReference<Object>(null);
            this.lastRenderedValueRef = new WeakReference<Object>(null);
        }
        if (this.swingRendering) {
            return this;
        }
        Border b = this.getBorder();
        if (b == null) {
            label.setBorder(expansionBorder);
        } else {
            label.setBorder(BorderFactory.createCompoundBorder(b, expansionBorder));
        }
        label.setOpaque(true);
        label.putClientProperty(DefaultOutlineCellRenderer.class, this);
        return label;
    }

    private static Color getHtmlColor(String text) {
        String textTemp = text.replaceAll("\\s+", " ").toLowerCase();
        int index = textTemp.indexOf("font color");
        if ((index = textTemp.indexOf("#", index)) != -1) {
            try {
                String colorString = textTemp.substring(index, index + 7);
                return Color.decode(colorString);
            }
            catch (IndexOutOfBoundsException ex1) {
            }
            catch (NumberFormatException ex2) {
                // empty catch block
            }
        }
        return null;
    }

    @Override
    public String getToolTipText() {
        String toolT;
        RenderDataProvider rendata = this.lastRendererRef.get();
        Object value = this.lastRenderedValueRef.get();
        if (rendata != null && value != null && (toolT = rendata.getTooltipText(value)) != null && (toolT = toolT.trim()).length() > 0) {
            return toolT;
        }
        return super.getToolTipText();
    }

    private static final class HtmlRenderer {
        private static final String HTML_RENDERER_CLASS = "org.openide.awt.HtmlRenderer";

        private HtmlRenderer() {
        }

        static Class getDelegate() {
            Class delegate;
            try {
                delegate = ClassLoader.getSystemClassLoader().loadClass("org.openide.awt.HtmlRenderer");
            }
            catch (ClassNotFoundException ex) {
                try {
                    delegate = Thread.currentThread().getContextClassLoader().loadClass("org.openide.awt.HtmlRenderer");
                }
                catch (ClassNotFoundException ex2) {
                    try {
                        Class lookupClass = ClassLoader.getSystemClassLoader().loadClass("org.openide.util.Lookup");
                        try {
                            Object defaultLookup = lookupClass.getMethod("getDefault", new Class[0]).invoke(null, new Object[0]);
                            ClassLoader systemClassLoader = (ClassLoader)lookupClass.getMethod("lookup", Class.class).invoke(defaultLookup, ClassLoader.class);
                            if (systemClassLoader == null) {
                                return null;
                            }
                            delegate = systemClassLoader.loadClass("org.openide.awt.HtmlRenderer");
                        }
                        catch (NoSuchMethodException mex) {
                            Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, ex);
                            return null;
                        }
                        catch (SecurityException mex) {
                            Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, ex);
                            return null;
                        }
                        catch (IllegalAccessException mex) {
                            Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, ex);
                            return null;
                        }
                        catch (IllegalArgumentException mex) {
                            Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, ex);
                            return null;
                        }
                        catch (InvocationTargetException mex) {
                            Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, ex);
                            return null;
                        }
                    }
                    catch (ClassNotFoundException ex3) {
                        return null;
                    }
                    catch (SecurityException se) {
                        return null;
                    }
                }
                catch (SecurityException se) {
                    return null;
                }
            }
            catch (SecurityException se) {
                return null;
            }
            return delegate;
        }

        private static Renderer createRenderer(Class htmlRendererClass) {
            try {
                Method createRenderer = htmlRendererClass.getMethod("createRenderer", new Class[0]);
                return new Renderer(createRenderer.invoke(null, new Object[0]));
            }
            catch (NoSuchMethodException ex) {
                Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
            catch (SecurityException ex) {
                Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
            catch (IllegalAccessException ex) {
                Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
            catch (IllegalArgumentException ex) {
                Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
            catch (InvocationTargetException ex) {
                Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }

        static /* synthetic */ Renderer access$100(Class x0) {
            return HtmlRenderer.createRenderer(x0);
        }

        private static class Renderer {
            private Object renderer;
            private Method getTableCellRendererComponent;

            private Renderer(Object renderer) throws NoSuchMethodException {
                this.renderer = renderer;
                this.getTableCellRendererComponent = TableCellRenderer.class.getMethod("getTableCellRendererComponent", JTable.class, Object.class, Boolean.TYPE, Boolean.TYPE, Integer.TYPE, Integer.TYPE);
            }

            public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean leadSelection, int row, int column) {
                try {
                    return (Component)this.getTableCellRendererComponent.invoke(this.renderer, table, value, selected, leadSelection, row, column);
                }
                catch (IllegalAccessException ex) {
                    Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, ex);
                    throw new IllegalStateException(ex);
                }
                catch (IllegalArgumentException ex) {
                    Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, ex);
                    throw new IllegalStateException(ex);
                }
                catch (InvocationTargetException ex) {
                    Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, ex);
                    throw new IllegalStateException(ex);
                }
            }

            private void setColors(Color foreground, Color background) {
                Component c = (Component)this.renderer;
                c.setForeground(foreground);
                c.setBackground(background);
            }
        }

    }

    private static class ExpansionHandleBorder
    implements Border {
        private static final boolean isGtk = "GTK".equals(UIManager.getLookAndFeel().getID());
        private Insets insets = new Insets(0, 0, 0, 0);
        private static JLabel lExpandedIcon = null;
        private static JLabel lCollapsedIcon = null;

        private ExpansionHandleBorder() {
            if (isGtk) {
                lExpandedIcon = new JLabel(DefaultOutlineCellRenderer.getExpandedIcon(), 11);
                lCollapsedIcon = new JLabel(DefaultOutlineCellRenderer.getCollapsedIcon(), 11);
            }
        }

        @Override
        public Insets getBorderInsets(Component c) {
            DefaultOutlineCellRenderer ren = (DefaultOutlineCellRenderer)((JComponent)c).getClientProperty(DefaultOutlineCellRenderer.class);
            if (ren == null) {
                ren = (DefaultOutlineCellRenderer)c;
            }
            if (ren.isShowHandle()) {
                this.insets.left = DefaultOutlineCellRenderer.getExpansionHandleWidth() + ren.getNestingDepth() * DefaultOutlineCellRenderer.getNestingWidth();
                this.insets.top = 1;
                this.insets.right = 1;
                this.insets.bottom = 1;
            } else {
                this.insets.left = 1;
                this.insets.top = 1;
                this.insets.right = 1;
                this.insets.bottom = 1;
            }
            if (ren.getCheckBox() != null) {
                this.insets.left += DefaultOutlineCellRenderer.access$500((DefaultOutlineCellRenderer)ren).getSize().width;
            }
            return this.insets;
        }

        @Override
        public boolean isBorderOpaque() {
            return false;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            JCheckBox chBox;
            DefaultOutlineCellRenderer ren = (DefaultOutlineCellRenderer)((JComponent)c).getClientProperty(DefaultOutlineCellRenderer.class);
            if (ren == null) {
                ren = (DefaultOutlineCellRenderer)c;
            }
            if (ren.isShowHandle() && !ren.isLeaf()) {
                Icon icon = ren.isExpanded() ? DefaultOutlineCellRenderer.getExpandedIcon() : DefaultOutlineCellRenderer.getCollapsedIcon();
                int iconX = ren.getNestingDepth() * DefaultOutlineCellRenderer.getNestingWidth();
                int iconY = icon.getIconHeight() < height ? height / 2 - icon.getIconHeight() / 2 : 0;
                if (isGtk) {
                    JLabel lbl = ren.isExpanded() ? lExpandedIcon : lCollapsedIcon;
                    lbl.setSize(Math.max(DefaultOutlineCellRenderer.getExpansionHandleWidth(), iconX + DefaultOutlineCellRenderer.getExpansionHandleWidth()), height);
                    lbl.paint(g);
                } else {
                    icon.paintIcon(c, g, iconX, iconY);
                }
            }
            if ((chBox = ren.getCheckBox()) != null) {
                int chBoxX = DefaultOutlineCellRenderer.getExpansionHandleWidth() + ren.getNestingDepth() * DefaultOutlineCellRenderer.getNestingWidth();
                Dimension chDim = chBox.getSize();
                Graphics gch = g.create(chBoxX, 0, chDim.width, chDim.height);
                chBox.paint(gch);
            }
        }
    }

}

