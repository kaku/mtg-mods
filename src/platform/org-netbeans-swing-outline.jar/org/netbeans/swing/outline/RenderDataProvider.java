/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.outline;

import java.awt.Color;
import javax.swing.Icon;

public interface RenderDataProvider {
    public String getDisplayName(Object var1);

    public boolean isHtmlDisplayName(Object var1);

    public Color getBackground(Object var1);

    public Color getForeground(Object var1);

    public String getTooltipText(Object var1);

    public Icon getIcon(Object var1);
}

