/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.outline;

import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.ExpandVetoException;

public interface ExtTreeWillExpandListener
extends TreeWillExpandListener {
    public void treeExpansionVetoed(TreeExpansionEvent var1, ExpandVetoException var2);
}

