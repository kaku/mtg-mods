/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.outline;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.SwingUtilities;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.table.TableModel;
import javax.swing.tree.AbstractLayoutCache;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import org.netbeans.swing.outline.DefaultOutlineModel;
import org.netbeans.swing.outline.ExtTreeWillExpandListener;
import org.netbeans.swing.outline.TreePathSupport;

final class EventBroadcaster
implements TableModelListener,
TreeModelListener,
ExtTreeWillExpandListener,
TreeExpansionListener {
    static boolean log = false;
    private int logcount = 0;
    private DefaultOutlineModel model;
    private TreeExpansionEvent inProgressEvent = null;
    private TableModelEvent pendingExpansionEvent = null;
    private boolean inMultiEvent = false;
    private static final int NODES_CHANGED = 0;
    private static final int NODES_INSERTED = 1;
    private static final int NODES_REMOVED = 2;
    private static final int STRUCTURE_CHANGED = 3;
    private static final String[] types = new String[]{"nodesChanged", "nodesInserted", "nodesRemoved", "structureChanged"};
    private List<TableModelListener> tableListeners = new ArrayList<TableModelListener>();
    private List<TreeModelListener> treeListeners = new ArrayList<TreeModelListener>();

    public EventBroadcaster(DefaultOutlineModel model) {
        this.setModel(model);
    }

    private void log(String method, Object o) {
        if (log) {
            if (o instanceof TableModelEvent) {
                o = EventBroadcaster.tableModelEventToString((TableModelEvent)o);
            }
            System.err.println("EB-" + this.logcount++ + " " + method + ":" + (o instanceof String ? (String)o : o.toString()));
        }
    }

    public boolean areMoreEventsPending() {
        return this.inMultiEvent;
    }

    private DefaultOutlineModel getModel() {
        return this.model;
    }

    private void setModel(DefaultOutlineModel model) {
        this.model = model;
    }

    private AbstractLayoutCache getLayout() {
        return this.getModel().getLayout();
    }

    private TreePathSupport getTreePathSupport() {
        return this.getModel().getTreePathSupport();
    }

    private TreeModel getTreeModel() {
        return this.getModel().getTreeModel();
    }

    private TableModel getTableModel() {
        return this.getModel().getTableModel();
    }

    public synchronized void addTableModelListener(TableModelListener l) {
        this.tableListeners.add(l);
    }

    public synchronized void addTreeModelListener(TreeModelListener l) {
        this.treeListeners.add(l);
    }

    public synchronized void removeTableModelListener(TableModelListener l) {
        this.tableListeners.remove(l);
    }

    public synchronized void removeTreeModelListener(TreeModelListener l) {
        this.treeListeners.remove(l);
    }

    private void fireTableChange(TableModelEvent e, TableModelListener[] listeners) {
        if (e == null) {
            return;
        }
        assert (e.getSource() == this.getModel());
        this.log("fireTableChange", e);
        for (int i = 0; i < listeners.length; ++i) {
            listeners[i].tableChanged(e);
        }
    }

    void fireTableChange(TableModelEvent e) {
        if (e == null) {
            return;
        }
        this.inMultiEvent = false;
        this.fireTableChange(e, this.getTableModelListeners());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fireTableChange(TableModelEvent[] e) {
        if (e == null || e.length == 0) {
            return;
        }
        TableModelListener[] listeners = this.getTableModelListeners();
        this.inMultiEvent = e.length > 1;
        try {
            for (int i = 0; i < e.length; ++i) {
                if (i == e.length - 1) {
                    this.inMultiEvent = false;
                }
                this.fireTableChange(e[i], listeners);
            }
        }
        finally {
            this.inMultiEvent = false;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private TableModelListener[] getTableModelListeners() {
        TableModelListener[] listeners;
        EventBroadcaster eventBroadcaster = this;
        synchronized (eventBroadcaster) {
            listeners = new TableModelListener[this.tableListeners.size()];
            listeners = this.tableListeners.toArray(listeners);
        }
        return listeners;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private synchronized void fireTreeChange(TreeModelEvent e, int type) {
        TreeModelListener[] listeners;
        if (e == null) {
            return;
        }
        assert (e.getSource() == this.getModel());
        EventBroadcaster eventBroadcaster = this;
        synchronized (eventBroadcaster) {
            listeners = new TreeModelListener[this.treeListeners.size()];
            listeners = this.treeListeners.toArray(listeners);
        }
        this.log("fireTreeChange-" + types[type], e);
        block9 : for (int i = 0; i < listeners.length; ++i) {
            switch (type) {
                case 0: {
                    listeners[i].treeNodesChanged(e);
                    continue block9;
                }
                case 1: {
                    listeners[i].treeNodesInserted(e);
                    continue block9;
                }
                case 2: {
                    listeners[i].treeNodesRemoved(e);
                    continue block9;
                }
                case 3: {
                    listeners[i].treeStructureChanged(e);
                    continue block9;
                }
                default: {
                    assert (false);
                    continue block9;
                }
            }
        }
    }

    @Override
    public void tableChanged(final TableModelEvent e) {
        assert (e.getType() == 0);
        if (SwingUtilities.isEventDispatchThread()) {
            this.fireTableChange(this.translateEvent(e));
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    EventBroadcaster.this.tableChanged(e);
                }
            });
        }
    }

    @Override
    public void treeNodesChanged(TreeModelEvent e) {
        assert (SwingUtilities.isEventDispatchThread());
        this.fireTreeChange(this.translateEvent(e), 0);
        TableModelEvent[] events = this.translateEvent(e, 0);
        this.getLayout().treeNodesChanged(e);
        this.fireTableChange(events);
    }

    @Override
    public void treeNodesInserted(TreeModelEvent e) {
        assert (SwingUtilities.isEventDispatchThread());
        this.fireTreeChange(this.translateEvent(e), 1);
        TableModelEvent[] events = this.translateEvent(e, 1);
        this.getLayout().treeNodesInserted(e);
        this.fireTableChange(events);
    }

    @Override
    public void treeNodesRemoved(TreeModelEvent e) {
        assert (SwingUtilities.isEventDispatchThread());
        this.fireTreeChange(this.translateEvent(e), 2);
        TableModelEvent[] events = this.translateEvent(e, 2);
        this.getLayout().treeNodesRemoved(e);
        this.fireTableChange(events);
    }

    @Override
    public void treeStructureChanged(TreeModelEvent e) {
        assert (SwingUtilities.isEventDispatchThread());
        this.getLayout().treeStructureChanged(e);
        this.fireTreeChange(this.translateEvent(e), 3);
        if (!this.getLayout().isExpanded(e.getTreePath())) {
            this.treeNodesChanged(e);
            return;
        }
        this.getTreePathSupport().clear();
        this.fireTableChange(new TableModelEvent(this.getModel()));
    }

    @Override
    public void treeWillCollapse(TreeExpansionEvent event) throws ExpandVetoException {
        assert (SwingUtilities.isEventDispatchThread());
        this.log("treeWillCollapse", event);
        this.pendingExpansionEvent = this.translateEvent(event, false);
        this.log("treeWillCollapse generated ", this.pendingExpansionEvent);
        this.inProgressEvent = event;
    }

    @Override
    public void treeWillExpand(TreeExpansionEvent event) throws ExpandVetoException {
        assert (SwingUtilities.isEventDispatchThread());
        this.log("treeWillExpand", event);
        this.pendingExpansionEvent = this.translateEvent(event, true);
        this.log("treeWillExpand generated", this.pendingExpansionEvent);
        this.inProgressEvent = event;
    }

    @Override
    public void treeCollapsed(TreeExpansionEvent event) {
        int row;
        TreePath path;
        assert (SwingUtilities.isEventDispatchThread());
        this.log("treeCollapsed", event);
        if (event != null && (path = event.getPath()) != null && this.getTreePathSupport().isVisible(path)) {
            this.getLayout().setExpandedState(path, false);
        }
        this.log("about to fire", this.pendingExpansionEvent);
        if (event != null) {
            TreePath path2 = event.getPath();
            row = this.getLayout().getRowForPath(path2);
        } else {
            row = -1;
        }
        TableModelEvent evt = row == -1 ? new TableModelEvent(this.getModel()) : new TableModelEvent(this.getModel(), row, row, 0, 0);
        this.fireTableChange(new TableModelEvent[]{evt, this.pendingExpansionEvent});
        this.pendingExpansionEvent = null;
        this.inProgressEvent = null;
    }

    @Override
    public void treeExpanded(TreeExpansionEvent event) {
        int row;
        assert (SwingUtilities.isEventDispatchThread());
        this.log("treeExpanded", event);
        if (event != null) {
            this.updateExpandedDescendants(event.getPath());
        }
        this.log("about to fire", this.pendingExpansionEvent);
        if (event != null) {
            TreePath path = event.getPath();
            row = this.getLayout().getRowForPath(path);
        } else {
            row = -1;
        }
        TableModelEvent evt = row == -1 ? new TableModelEvent(this.getModel()) : new TableModelEvent(this.getModel(), row, row, 0, 0);
        this.fireTableChange(new TableModelEvent[]{evt, this.pendingExpansionEvent});
        this.pendingExpansionEvent = null;
        this.inProgressEvent = null;
    }

    @Override
    public void treeExpansionVetoed(TreeExpansionEvent event, ExpandVetoException exception) {
        assert (SwingUtilities.isEventDispatchThread());
        this.log("treeExpansionVetoed", exception);
        if (event == this.inProgressEvent) {
            this.pendingExpansionEvent = null;
            this.inProgressEvent = null;
        }
    }

    private void updateExpandedDescendants(TreePath path) {
        this.getLayout().setExpandedState(path, true);
        TreePath[] descendants = this.getTreePathSupport().getExpandedDescendants(path);
        if (descendants.length > 0) {
            for (int i = 0; i < descendants.length; ++i) {
                this.getLayout().setExpandedState(descendants[i], true);
            }
        }
    }

    private TableModelEvent translateEvent(TableModelEvent e) {
        TableModelEvent nue = new TableModelEvent(this.getModel(), e.getFirstRow(), e.getLastRow(), e.getColumn() + 1, e.getType());
        return nue;
    }

    private TreeModelEvent translateEvent(TreeModelEvent e) {
        TreeModelEvent nue = new TreeModelEvent((Object)this.getModel(), e.getPath(), e.getChildIndices(), e.getChildren());
        return nue;
    }

    private TableModelEvent[] translateEvent(TreeModelEvent e, int type) {
        int i;
        boolean inClosedNode;
        int[][] blocks;
        TreePath path = e.getTreePath();
        boolean bl = inClosedNode = !this.getLayout().isExpanded(path);
        if (inClosedNode) {
            int row = this.getLayout().getRowForPath(path);
            if (row != -1) {
                switch (type) {
                    case 0: 
                    case 1: 
                    case 2: {
                        return new TableModelEvent[]{new TableModelEvent(this.getModel(), row, row, 0, 0)};
                    }
                }
                assert (false);
            }
            return new TableModelEvent[0];
        }
        int[] rowIndices = this.computeRowIndices(e);
        boolean discontiguous = EventBroadcaster.isDiscontiguous(rowIndices);
        if (discontiguous) {
            blocks = EventBroadcaster.getContiguousIndexBlocks(rowIndices, type == 2);
            this.log("discontiguous " + types[type] + " event", "" + blocks.length + " blocks");
        } else {
            blocks = new int[][]{rowIndices};
        }
        TableModelEvent[] result = new TableModelEvent[blocks.length];
        block8 : for (i = 0; i < blocks.length; ++i) {
            int[] currBlock = blocks[i];
            switch (type) {
                case 0: {
                    result[i] = this.createTableChangeEvent(e, currBlock);
                    continue block8;
                }
                case 1: {
                    result[i] = this.createTableInsertionEvent(e, currBlock);
                    continue block8;
                }
                case 2: {
                    result[i] = this.createTableDeletionEvent(e, currBlock);
                    continue block8;
                }
                default: {
                    assert (false);
                    continue block8;
                }
            }
        }
        this.log("translateEvent", e);
        this.log("generated table events", new Integer(result.length));
        if (log) {
            for (i = 0; i < result.length; ++i) {
                this.log("  Event " + i, result[i]);
            }
        }
        return result;
    }

    private TableModelEvent translateEvent(TreeExpansionEvent e, boolean expand) {
        TreePath path = e.getPath();
        int firstRow = this.getLayout().getRowForPath(path) + 1;
        if (firstRow == -1) {
            return null;
        }
        TreePath[] paths = this.getTreePathSupport().getExpandedDescendants(path);
        int count = this.getTreeModel().getChildCount(path.getLastPathComponent());
        for (int i = 0; i < paths.length; ++i) {
            count += this.getTreeModel().getChildCount(paths[i].getLastPathComponent());
        }
        int lastRow = firstRow + count - 1;
        TableModelEvent result = new TableModelEvent(this.getModel(), firstRow, lastRow, -1, expand ? 1 : -1);
        return result;
    }

    private TableModelEvent createTableChangeEvent(TreeModelEvent e, int[] indices) {
        TreePath path = e.getTreePath();
        int row = this.getLayout().getRowForPath(path);
        int first = null == indices ? row : indices[0];
        int last = null == indices ? row : indices[indices.length - 1];
        TableModelEvent result = new TableModelEvent(this.getModel(), first, last, 0, 0);
        return result;
    }

    private TableModelEvent createTableInsertionEvent(TreeModelEvent e, int[] indices) {
        TableModelEvent result;
        this.log("createTableInsertionEvent", e);
        TreePath path = e.getTreePath();
        int row = this.getLayout().getRowForPath(path);
        boolean realInsert = this.getLayout().isExpanded(path);
        if (realInsert) {
            if (indices.length == 1) {
                int affectedRow = indices[0];
                result = new TableModelEvent(this.getModel(), affectedRow, affectedRow, -1, 1);
            } else {
                int lowest = indices[0];
                int highest = indices[indices.length - 1];
                result = new TableModelEvent(this.getModel(), lowest, highest, -1, 1);
            }
        } else {
            result = new TableModelEvent(this.getModel(), row, row, -1);
        }
        return result;
    }

    private TableModelEvent createTableDeletionEvent(TreeModelEvent e, int[] indices) {
        this.log("createTableDeletionEvent " + Arrays.asList(EventBroadcaster.toArrayOfInteger(indices)), e);
        TreePath path = e.getTreePath();
        int row = this.getLayout().getRowForPath(path);
        if (row == -1) {
            // empty if block
        }
        int firstRow = indices[0];
        int lastRow = indices[indices.length - 1];
        this.log("TableModelEvent: fromRow: ", new Integer(firstRow));
        this.log(" toRow: ", new Integer(lastRow));
        TableModelEvent result = new TableModelEvent(this.getModel(), firstRow, lastRow, -1, -1);
        return result;
    }

    static boolean isDiscontiguous(int[] indices) {
        if (indices == null || indices.length <= 1) {
            return false;
        }
        Arrays.sort(indices);
        int lastVal = indices[0];
        for (int i = 1; i < indices.length; ++i) {
            if (indices[i] != lastVal + 1) {
                return true;
            }
            ++lastVal;
        }
        return false;
    }

    static int[][] getContiguousIndexBlocks(int[] indices, boolean reverseOrder) {
        if (indices.length == 0) {
            return new int[][]{new int[0]};
        }
        if (indices.length == 1) {
            return new int[][]{indices};
        }
        if (reverseOrder) {
            EventBroadcaster.inverseSort(indices);
        } else {
            Arrays.sort(indices);
        }
        ArrayList<int[]> blocks = new ArrayList<int[]>();
        int startIndex = 0;
        for (int i = 1; i < indices.length; ++i) {
            boolean newBlock;
            int lastVal = indices[i - 1];
            boolean bl = reverseOrder ? indices[i] != lastVal - 1 : (newBlock = indices[i] != lastVal + 1);
            if (!newBlock) continue;
            int[] block = new int[i - startIndex];
            System.arraycopy(indices, startIndex, block, 0, block.length);
            blocks.add(block);
            startIndex = i;
        }
        int[] block = new int[indices.length - startIndex];
        System.arraycopy(indices, startIndex, block, 0, block.length);
        blocks.add(block);
        return (int[][])blocks.toArray((T[])new int[0][]);
    }

    private static Integer[] toArrayOfInteger(int[] ints) {
        Integer[] result = new Integer[ints.length];
        for (int i = 0; i < ints.length; ++i) {
            result[i] = new Integer(ints[i]);
        }
        return result;
    }

    private static void inverseSort(int[] array) {
        int i = 0;
        while (i < array.length) {
            int[] arrn = array;
            int n = i++;
            arrn[n] = arrn[n] * -1;
        }
        Arrays.sort(array);
        i = 0;
        while (i < array.length) {
            int[] arrn = array;
            int n = i++;
            arrn[n] = arrn[n] * -1;
        }
    }

    private static String tableModelEventToString(TableModelEvent e) {
        StringBuilder sb = new StringBuilder();
        sb.append("TableModelEvent ");
        switch (e.getType()) {
            case 1: {
                sb.append("insert ");
                break;
            }
            case -1: {
                sb.append("delete ");
                break;
            }
            case 0: {
                sb.append("update ");
                break;
            }
            default: {
                sb.append("Unknown type ").append(e.getType());
            }
        }
        sb.append("from ");
        switch (e.getFirstRow()) {
            case -1: {
                sb.append("header row ");
                break;
            }
            default: {
                sb.append(e.getFirstRow());
                sb.append(' ');
            }
        }
        sb.append("to ");
        sb.append(e.getLastRow());
        sb.append(" column ");
        switch (e.getColumn()) {
            case -1: {
                sb.append("ALL_COLUMNS");
                break;
            }
            default: {
                sb.append(e.getColumn());
            }
        }
        return sb.toString();
    }

    private int[] computeRowIndices(TreeModelEvent e) {
        int[] rowIndices;
        int parentRow = this.getLayout().getRowForPath(e.getTreePath());
        if (e.getChildren() != null) {
            rowIndices = new int[e.getChildren().length];
            for (int i = 0; i < e.getChildren().length; ++i) {
                TreePath childPath = e.getTreePath().pathByAddingChild(e.getChildren()[i]);
                int index = this.getLayout().getRowForPath(childPath);
                rowIndices[i] = index < 0 ? parentRow + e.getChildIndices()[i] + 1 : index;
            }
        } else {
            rowIndices = null;
        }
        return rowIndices;
    }

}

