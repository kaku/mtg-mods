/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.outline;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.table.TableModel;
import javax.swing.tree.AbstractLayoutCache;
import javax.swing.tree.FixedHeightLayoutCache;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.VariableHeightLayoutCache;
import org.netbeans.swing.outline.EventBroadcaster;
import org.netbeans.swing.outline.OutlineModel;
import org.netbeans.swing.outline.ProxyTableModel;
import org.netbeans.swing.outline.RowModel;
import org.netbeans.swing.outline.TreePathSupport;

public class DefaultOutlineModel
implements OutlineModel {
    private TreeModel treeModel;
    private TableModel tableModel;
    private AbstractLayoutCache layout;
    private TreePathSupport treePathSupport;
    private EventBroadcaster broadcaster;
    private String nodesColumnLabel = "Nodes";
    private static final int NODES_CHANGED = 0;
    private static final int NODES_INSERTED = 1;
    private static final int NODES_REMOVED = 2;
    private static final int STRUCTURE_CHANGED = 3;

    public static OutlineModel createOutlineModel(TreeModel treeModel, RowModel rowModel) {
        return DefaultOutlineModel.createOutlineModel(treeModel, rowModel, false, null);
    }

    public static OutlineModel createOutlineModel(TreeModel treeModel, RowModel rowModel, boolean isLargeModel) {
        return DefaultOutlineModel.createOutlineModel(treeModel, rowModel, isLargeModel, null);
    }

    public static OutlineModel createOutlineModel(TreeModel treeModel, RowModel rowModel, boolean isLargeModel, String nodesColumnLabel) {
        return new DefaultOutlineModel(treeModel, rowModel, isLargeModel, nodesColumnLabel);
    }

    protected DefaultOutlineModel(TreeModel treeModel, RowModel rowModel, boolean largeModel, String nodesColumnLabel) {
        this(treeModel, new ProxyTableModel(rowModel), largeModel, nodesColumnLabel);
    }

    protected DefaultOutlineModel(TreeModel treeModel, TableModel tableModel, boolean largeModel, String nodesColumnLabel) {
        this.treeModel = treeModel;
        this.tableModel = tableModel;
        if (nodesColumnLabel != null) {
            this.nodesColumnLabel = nodesColumnLabel;
        }
        this.layout = largeModel ? new FixedHeightLayoutCache() : new VariableHeightLayoutCache();
        this.broadcaster = new EventBroadcaster(this);
        this.layout.setRootVisible(true);
        this.layout.setModel(this);
        this.treePathSupport = new TreePathSupport(this, this.layout);
        this.treePathSupport.addTreeExpansionListener(this.broadcaster);
        this.treePathSupport.addTreeWillExpandListener(this.broadcaster);
        treeModel.addTreeModelListener(this.broadcaster);
        tableModel.addTableModelListener(this.broadcaster);
        if (tableModel instanceof ProxyTableModel) {
            ((ProxyTableModel)tableModel).setOutlineModel(this);
        }
    }

    @Override
    public final TreePathSupport getTreePathSupport() {
        return this.treePathSupport;
    }

    @Override
    public final AbstractLayoutCache getLayout() {
        return this.layout;
    }

    public boolean areMoreEventsPending() {
        return this.broadcaster.areMoreEventsPending();
    }

    TreeModel getTreeModel() {
        return this.treeModel;
    }

    TableModel getTableModel() {
        return this.tableModel;
    }

    @Override
    public final Object getChild(Object parent, int index) {
        return this.treeModel.getChild(parent, index);
    }

    @Override
    public final int getChildCount(Object parent) {
        return this.treeModel.getChildCount(parent);
    }

    public final Class getColumnClass(int columnIndex) {
        if (columnIndex == 0) {
            return Object.class;
        }
        return this.tableModel.getColumnClass(columnIndex - 1);
    }

    @Override
    public final int getColumnCount() {
        return this.tableModel.getColumnCount() + 1;
    }

    @Override
    public String getColumnName(int columnIndex) {
        if (columnIndex == 0) {
            return this.nodesColumnLabel;
        }
        return this.tableModel.getColumnName(columnIndex - 1);
    }

    public void setNodesColumnLabel(String label) {
        this.nodesColumnLabel = label;
        this.broadcaster.fireTableChange(new TableModelEvent(this, -1, -1, 0, -1));
    }

    @Override
    public final int getIndexOfChild(Object parent, Object child) {
        return this.treeModel.getIndexOfChild(parent, child);
    }

    @Override
    public final Object getRoot() {
        return this.treeModel.getRoot();
    }

    @Override
    public final int getRowCount() {
        return this.layout.getRowCount();
    }

    @Override
    public final Object getValueAt(int rowIndex, int columnIndex) {
        TreePath path;
        Object result = columnIndex == 0 ? ((path = this.getLayout().getPathForRow(rowIndex)) != null ? path.getLastPathComponent() : null) : this.tableModel.getValueAt(rowIndex, columnIndex - 1);
        return result;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return false;
        }
        return this.tableModel.isCellEditable(rowIndex, columnIndex - 1);
    }

    @Override
    public final boolean isLeaf(Object node) {
        return null != node && this.treeModel.isLeaf(node);
    }

    @Override
    public final synchronized void addTableModelListener(TableModelListener l) {
        this.broadcaster.addTableModelListener(l);
    }

    @Override
    public final synchronized void addTreeModelListener(TreeModelListener l) {
        this.broadcaster.addTreeModelListener(l);
    }

    @Override
    public final synchronized void removeTableModelListener(TableModelListener l) {
        this.broadcaster.removeTableModelListener(l);
    }

    @Override
    public final synchronized void removeTreeModelListener(TreeModelListener l) {
        this.broadcaster.removeTreeModelListener(l);
    }

    @Override
    public final void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (columnIndex != 0) {
            this.tableModel.setValueAt(aValue, rowIndex, columnIndex - 1);
        } else {
            this.setTreeValueAt(aValue, rowIndex);
        }
    }

    protected void setTreeValueAt(Object aValue, int rowIndex) {
    }

    @Override
    public final void valueForPathChanged(TreePath path, Object newValue) {
        this.treeModel.valueForPathChanged(path, newValue);
    }

    @Override
    public boolean isLargeModel() {
        return this.layout instanceof FixedHeightLayoutCache;
    }
}

