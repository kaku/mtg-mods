/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.Mutex
 */
package org.openide.util.actions;

import java.beans.PropertyChangeEvent;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.Action;
import org.openide.nodes.Node;
import org.openide.nodes.NodeAdapter;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeOp;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.Mutex;
import org.openide.util.actions.NodeAction;

public abstract class CookieAction
extends NodeAction {
    private static final String PROP_COOKIES = "cookies";
    public static final int MODE_ONE = 1;
    public static final int MODE_SOME = 2;
    public static final int MODE_ALL = 4;
    public static final int MODE_EXACTLY_ONE = 8;
    public static final int MODE_ANY = 7;
    private static final long serialVersionUID = 6031319415908298424L;
    private CookiesChangeListener listener;

    public CookieAction() {
        this.listener = new CookiesChangeListener(this);
    }

    protected abstract int mode();

    protected abstract Class<?>[] cookieClasses();

    private Class<?>[] getCookies() {
        Class[] ret = (Class[])this.getProperty((Object)"cookies");
        if (ret != null) {
            return ret;
        }
        ret = this.cookieClasses();
        this.putProperty((Object)"cookies", (Object)ret);
        return ret;
    }

    @Override
    protected boolean enable(Node[] activatedNodes) {
        if (activatedNodes.length == 0) {
            return false;
        }
        this.listener.setNodes(activatedNodes);
        return this.doEnable(activatedNodes);
    }

    @Override
    public Action createContextAwareInstance(Lookup actionContext) {
        return new CookieDelegateAction(this, actionContext);
    }

    boolean doEnable(Node[] activatedNodes) {
        int supported = this.resolveSupported(activatedNodes);
        if (supported == 0) {
            return false;
        }
        int mode = this.mode();
        return (mode & 1) != 0 || (mode & 4) != 0 && supported == activatedNodes.length || (mode & 8) != 0 && activatedNodes.length == 1 || (mode & 2) != 0 && supported < activatedNodes.length;
    }

    private int resolveSupported(Node[] activatedNodes) {
        int ret = 0;
        Class<?>[] cookies = this.getCookies();
        block0 : for (Node n : activatedNodes) {
            for (Class<?> cookie : cookies) {
                Lookup.Template templ = new Lookup.Template(cookie);
                if (n.getLookup().lookupItem(templ) == null) continue;
                ++ret;
                continue block0;
            }
        }
        return ret;
    }

    static final class CookieDelegateAction
    extends NodeAction.DelegateAction
    implements NodeListener,
    Runnable {
        private NodeListener listener;
        private List<Reference<Node>> nodes;

        public CookieDelegateAction(CookieAction a, Lookup actionContext) {
            super(a, actionContext);
            this.listener = NodeOp.weakNodeListener(this, null);
            this.setNodes(this.nodes());
        }

        @Override
        public void resultChanged(LookupEvent ev) {
            this.setNodes(this.nodes());
            this.superResultChanged(ev);
        }

        private void superResultChanged(LookupEvent ev) {
            super.resultChanged(ev);
        }

        @Override
        public void childrenAdded(NodeMemberEvent ev) {
        }

        @Override
        public void childrenRemoved(NodeMemberEvent ev) {
        }

        @Override
        public void childrenReordered(NodeReorderEvent ev) {
        }

        @Override
        public void nodeDestroyed(NodeEvent ev) {
        }

        @Override
        public void propertyChange(PropertyChangeEvent ev) {
            if (!"cookie".equals(ev.getPropertyName())) {
                return;
            }
            Mutex.EVENT.readAccess((Runnable)this);
        }

        @Override
        public void run() {
            this.superResultChanged(null);
        }

        private void setNodes(Node[] newNodes) {
            this.detachListeners(this.nodes);
            if (newNodes != null) {
                this.nodes = new ArrayList<Reference<Node>>(newNodes.length);
                for (int i = 0; i < newNodes.length; ++i) {
                    this.nodes.add(new WeakReference<Node>(newNodes[i]));
                }
            }
            this.attachListeners(this.nodes);
        }

        private void detachListeners(List<Reference<Node>> nodes) {
            if (nodes != null) {
                Iterator<Reference<Node>> it = nodes.iterator();
                while (it.hasNext()) {
                    Node node = it.next().get();
                    if (node == null) continue;
                    node.removeNodeListener(this.listener);
                }
            }
        }

        private void attachListeners(List<Reference<Node>> nodes) {
            if (nodes != null) {
                Iterator<Reference<Node>> it = nodes.iterator();
                while (it.hasNext()) {
                    Node node = it.next().get();
                    if (node == null) continue;
                    node.addNodeListener(this.listener);
                }
            }
        }

        protected void finalize() {
            this.detachListeners(this.nodes);
        }
    }

    private static final class CookiesChangeListener
    extends NodeAdapter {
        private NodeListener listener;
        private volatile List<Reference<Node>> nodes;
        private Reference<CookieAction> action;

        public CookiesChangeListener(CookieAction a) {
            this.listener = NodeOp.weakNodeListener(this, null);
            this.action = new WeakReference<CookieAction>(a);
        }

        void setNodes(Node[] newNodes) {
            List<Reference<Node>> nodes2 = this.nodes;
            if (nodes2 != null) {
                this.detachListeners(nodes2);
            }
            this.nodes = null;
            if (newNodes != null) {
                ArrayList<Reference<Node>> tmp = new ArrayList<Reference<Node>>(newNodes.length);
                for (int i = 0; i < newNodes.length; ++i) {
                    tmp.add(new WeakReference<Node>(newNodes[i]));
                }
                this.attachListeners(tmp);
                this.nodes = tmp;
            }
        }

        void detachListeners(List<Reference<Node>> nodes) {
            if (nodes == null) {
                return;
            }
            Iterator<Reference<Node>> it = nodes.iterator();
            while (it.hasNext()) {
                Node node = it.next().get();
                if (node == null) continue;
                node.removeNodeListener(this.listener);
            }
        }

        void attachListeners(List<Reference<Node>> nodes) {
            Iterator<Reference<Node>> it = nodes.iterator();
            while (it.hasNext()) {
                Node node = it.next().get();
                if (node == null) continue;
                node.addNodeListener(this.listener);
            }
        }

        @Override
        public void propertyChange(PropertyChangeEvent ev) {
            if (!"cookie".equals(ev.getPropertyName())) {
                return;
            }
            final CookieAction a = this.action.get();
            if (a == null) {
                return;
            }
            List<Reference<Node>> _nodes = this.nodes;
            if (_nodes != null) {
                ArrayList<Node> nonNullNodes = new ArrayList<Node>(_nodes.size());
                Iterator<Reference<Node>> it = _nodes.iterator();
                while (it.hasNext()) {
                    Node node = it.next().get();
                    if (node != null) {
                        nonNullNodes.add(node);
                        continue;
                    }
                    return;
                }
                final Node[] nodes2 = new Node[nonNullNodes.size()];
                nonNullNodes.toArray(nodes2);
                Mutex.EVENT.writeAccess(new Runnable(){

                    @Override
                    public void run() {
                        a.setEnabled(a.enable(nodes2));
                    }
                });
            }
        }

        protected void finalize() {
            this.detachListeners(this.nodes);
        }

    }

}

