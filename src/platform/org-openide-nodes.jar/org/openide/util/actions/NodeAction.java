/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Actions
 *  org.openide.awt.Actions$MenuItem
 *  org.openide.awt.Actions$ToolbarButton
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Mutex
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.WeakSet
 *  org.openide.util.actions.ActionInvoker
 *  org.openide.util.actions.CallableSystemAction
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.util.actions.Presenter$Toolbar
 */
package org.openide.util.actions;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EventListener;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JMenuItem;
import org.openide.awt.Actions;
import org.openide.nodes.Node;
import org.openide.util.ContextAwareAction;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Mutex;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.WeakSet;
import org.openide.util.actions.ActionInvoker;
import org.openide.util.actions.CallableSystemAction;
import org.openide.util.actions.Presenter;

public abstract class NodeAction
extends CallableSystemAction
implements ContextAwareAction {
    private static final long serialVersionUID = -5672895970450115226L;
    private static final String PROP_HAS_LISTENERS = "hasListeners";
    private static final String PROP_LAST_NODES = "lastNodes";
    private static final String PROP_LAST_ENABLED = "lastEnabled";
    private static NodesL l;
    private static final Set<NodeAction> listeningActions;

    protected void initialize() {
        super.initialize();
        this.putProperty((Object)"hasListeners", (Object)Boolean.FALSE);
        this.putProperty((Object)"enabled", (Object)null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void addNotify() {
        super.addNotify();
        this.putProperty((Object)"hasListeners", (Object)Boolean.TRUE);
        Set<NodeAction> set = listeningActions;
        synchronized (set) {
            if (l == null) {
                l = new NodesL();
            }
            if (listeningActions.isEmpty()) {
                l.setActive(true);
            }
            listeningActions.add(this);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void removeNotify() {
        Set<NodeAction> set = listeningActions;
        synchronized (set) {
            listeningActions.remove((Object)this);
            if (listeningActions.isEmpty()) {
                l.setActive(false);
            }
        }
        this.putProperty((Object)"hasListeners", (Object)Boolean.FALSE);
        this.putProperty((Object)"enabled", (Object)null);
        super.removeNotify();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isEnabled() {
        Object[] ns = null;
        Boolean b = null;
        Object object = this.getLock();
        synchronized (object) {
            b = (Boolean)this.getProperty((Object)"enabled");
            if (l == null) {
                l = new NodesL();
                l.setActive(true);
            }
            NodesL listener = l;
            if (b == null) {
                ns = listener.getActivatedNodes(this.surviveFocusChange());
                Reference r = (Reference)this.getProperty((Object)"lastNodes");
                if (r != null && Arrays.equals((Node[])r.get(), ns)) {
                    b = (Boolean)this.getProperty((Object)"lastEnabled");
                    if (((Boolean)this.getProperty((Object)"hasListeners")).booleanValue()) {
                        this.putProperty((Object)"enabled", (Object)b);
                    }
                }
            }
        }
        if (b == null) {
            b = ns != null && this.enable((Node[])ns) ? Boolean.TRUE : Boolean.FALSE;
            object = this.getLock();
            synchronized (object) {
                this.putProperty((Object)"lastNodes", new WeakReference<Object[]>(ns));
                this.putProperty((Object)"lastEnabled", (Object)b);
                if (((Boolean)this.getProperty((Object)"hasListeners")).booleanValue()) {
                    this.putProperty((Object)"enabled", (Object)b);
                }
            }
        }
        return b;
    }

    public void setEnabled(boolean e) {
        this.putProperty((Object)"lastEnabled", (Object)null);
        this.putProperty((Object)"lastNodes", (Object)null);
        Boolean propHasListener = (Boolean)this.getProperty((Object)"hasListeners");
        if (propHasListener != null && propHasListener.booleanValue()) {
            super.setEnabled(e);
        } else {
            this.putProperty("enabled", (Object)null, true);
        }
    }

    @Deprecated
    public void actionPerformed(ActionEvent ev) {
        Object s;
        Object object = s = ev == null ? null : ev.getSource();
        if (s instanceof Node) {
            ActionInvoker.invokeAction((Action)((Object)this), (ActionEvent)ev, (boolean)this.amIasynchronous(), (Runnable)new Runnable(){

                @Override
                public void run() {
                    NodeAction.this.performAction(new Node[]{(Node)s});
                }
            });
        } else if (s instanceof Node[]) {
            ActionInvoker.invokeAction((Action)((Object)this), (ActionEvent)ev, (boolean)this.amIasynchronous(), (Runnable)new Runnable(){

                @Override
                public void run() {
                    NodeAction.this.performAction((Node[])s);
                }
            });
        } else {
            super.actionPerformed(ev);
        }
    }

    @Deprecated
    public void performAction() {
        this.performAction(this.getActivatedNodes());
    }

    public final Node[] getActivatedNodes() {
        NodesL listener = l;
        return listener == null ? new Node[]{} : listener.getActivatedNodes(true);
    }

    protected boolean surviveFocusChange() {
        return true;
    }

    protected abstract void performAction(Node[] var1);

    protected abstract boolean enable(Node[] var1);

    public Action createContextAwareInstance(Lookup actionContext) {
        return new DelegateAction(this, actionContext);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void maybeFireEnabledChange() {
        boolean fire = false;
        Object object = this.getLock();
        synchronized (object) {
            if (this.getProperty((Object)"enabled") != null) {
                this.putProperty((Object)"enabled", (Object)null);
                fire = true;
            }
        }
        if (fire) {
            try {
                this.firePropertyChange("enabled", (Object)null, (Object)null);
            }
            catch (NullPointerException e) {
                Exceptions.attachMessage((Throwable)e, (String)("You cannot add " + this.getClass().getName() + " directly to a JMenu etc.; use org.openide.awt.Actions.connect instead"));
                Logger.getLogger(NodeAction.class.getName()).log(Level.WARNING, null, e);
            }
        }
    }

    final boolean amIasynchronous() {
        return this.asynchronous();
    }

    static {
        listeningActions = new WeakSet(100);
    }

    static class DelegateAction
    implements Action,
    LookupListener,
    Presenter.Menu,
    Presenter.Popup,
    Presenter.Toolbar {
        private static final Node[] EMPTY_NODE_ARRAY = new Node[0];
        private NodeAction delegate;
        private Lookup.Result<Node> result;
        private boolean enabled = true;
        private PropertyChangeSupport support;

        public DelegateAction(NodeAction a, Lookup actionContext) {
            this.support = new PropertyChangeSupport(this);
            this.delegate = a;
            this.result = actionContext.lookupResult(Node.class);
            this.result.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this, this.result));
            this.resultChanged(null);
        }

        public String toString() {
            return super.toString() + "[delegate=" + (Object)((Object)this.delegate) + "]";
        }

        public final synchronized Node[] nodes() {
            if (this.result != null) {
                return this.result.allInstances().toArray(EMPTY_NODE_ARRAY);
            }
            return EMPTY_NODE_ARRAY;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            ActionInvoker.invokeAction((Action)((Object)this.delegate), (ActionEvent)e, (boolean)this.delegate.amIasynchronous(), (Runnable)new Runnable(){

                @Override
                public void run() {
                    DelegateAction.this.delegate.performAction(DelegateAction.this.nodes());
                }
            });
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener listener) {
            this.support.addPropertyChangeListener(listener);
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener listener) {
            this.support.removePropertyChangeListener(listener);
        }

        @Override
        public void putValue(String key, Object o) {
        }

        @Override
        public Object getValue(String key) {
            return this.delegate.getValue(key);
        }

        @Override
        public boolean isEnabled() {
            return this.enabled;
        }

        @Override
        public void setEnabled(boolean b) {
        }

        public void resultChanged(LookupEvent ev) {
            final boolean old = this.enabled;
            this.enabled = this.delegate.enable(this.nodes());
            Mutex.EVENT.readAccess(new Runnable(){

                @Override
                public void run() {
                    DelegateAction.this.support.firePropertyChange("enabled", old, DelegateAction.this.enabled);
                }
            });
        }

        public JMenuItem getMenuPresenter() {
            if (this.isMethodOverridden(this.delegate, "getMenuPresenter")) {
                return this.delegate.getMenuPresenter();
            }
            return new Actions.MenuItem((Action)this, true);
        }

        public JMenuItem getPopupPresenter() {
            if (this.isMethodOverridden(this.delegate, "getPopupPresenter")) {
                return this.delegate.getPopupPresenter();
            }
            return new Actions.MenuItem((Action)this, false);
        }

        public Component getToolbarPresenter() {
            if (this.isMethodOverridden(this.delegate, "getToolbarPresenter")) {
                return this.delegate.getToolbarPresenter();
            }
            return new Actions.ToolbarButton((Action)this);
        }

        private boolean isMethodOverridden(NodeAction d, String name) {
            try {
                Method m = d.getClass().getMethod(name, new Class[0]);
                return m.getDeclaringClass() != CallableSystemAction.class;
            }
            catch (NoSuchMethodException ex) {
                ex.printStackTrace();
                throw new IllegalStateException("Error searching for method " + name + " in " + (Object)((Object)d));
            }
        }

    }

    private static final class NodesL
    implements LookupListener {
        private volatile Lookup.Result<Node> result;
        private boolean chgSFC = false;
        private boolean chgNSFC = false;
        private Reference<Node>[] activatedNodes;

        public Node[] getActivatedNodes(boolean survive) {
            Lookup.Result<Node> r;
            block3 : {
                if (survive && this.activatedNodes != null) {
                    Node[] arr = new Node[this.activatedNodes.length];
                    for (int i = 0; i < arr.length; ++i) {
                        arr[i] = this.activatedNodes[i].get();
                        if (arr[i] != null) {
                            continue;
                        }
                        break block3;
                    }
                    return arr;
                }
            }
            return (r = this.result) == null ? new Node[]{} : r.allInstances().toArray(new Node[0]);
        }

        synchronized void setActive(boolean active) {
            Lookup context = Utilities.actionsGlobalContext();
            if (active) {
                if (this.result == null) {
                    this.result = context.lookupResult(Node.class);
                    this.result.addLookupListener((LookupListener)this);
                }
            } else {
                this.forget(true);
                this.forget(false);
            }
        }

        public void resultChanged(LookupEvent ev) {
            Lookup.Item item;
            Lookup.Result<Node> r = this.result;
            if (r == null) {
                return;
            }
            this.chgSFC = true;
            this.chgNSFC = true;
            Collection items = this.result.allItems();
            boolean updateActivatedNodes = true;
            if (items.size() == 1 && "none".equals((item = (Lookup.Item)items.iterator().next()).getId()) && item.getInstance() == null) {
                updateActivatedNodes = false;
            }
            if (updateActivatedNodes) {
                Iterator it = this.result.allInstances().iterator();
                ArrayList list = new ArrayList();
                while (it.hasNext()) {
                    list.add(new WeakReference(it.next()));
                }
                this.activatedNodes = list.toArray(new Reference[list.size()]);
            }
            this.update();
        }

        public void update() {
            if (this.chgSFC) {
                this.forget(true);
                this.chgSFC = false;
            }
            if (this.chgNSFC) {
                this.forget(false);
                this.chgNSFC = false;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void forget(boolean sfc) {
            ArrayList as;
            Set set = listeningActions;
            synchronized (set) {
                as = new ArrayList(listeningActions.size());
                Iterator it = listeningActions.iterator();
                while (it.hasNext()) {
                    as.add(it.next());
                }
            }
            for (final NodeAction a : as) {
                if (a.surviveFocusChange() != sfc) continue;
                Mutex.EVENT.readAccess(new Runnable(){

                    @Override
                    public void run() {
                        a.maybeFireEnabledChange();
                    }
                });
            }
        }

    }

}

