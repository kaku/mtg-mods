/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.actions.Savable
 */
package org.openide.cookies;

import org.netbeans.api.actions.Savable;
import org.openide.nodes.Node;

public interface SaveCookie
extends Node.Cookie,
Savable {
}

