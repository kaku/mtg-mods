/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.actions.Viewable
 */
package org.openide.cookies;

import org.netbeans.api.actions.Viewable;
import org.openide.nodes.Node;

public interface ViewCookie
extends Viewable,
Node.Cookie {
}

