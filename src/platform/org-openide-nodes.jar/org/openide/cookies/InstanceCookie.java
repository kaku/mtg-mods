/*
 * Decompiled with CFR 0_118.
 */
package org.openide.cookies;

import java.io.IOException;
import org.openide.nodes.Node;

public interface InstanceCookie
extends Node.Cookie {
    public String instanceName();

    public Class<?> instanceClass() throws IOException, ClassNotFoundException;

    public Object instanceCreate() throws IOException, ClassNotFoundException;

    public static interface Of
    extends InstanceCookie {
        public boolean instanceOf(Class<?> var1);
    }

}

