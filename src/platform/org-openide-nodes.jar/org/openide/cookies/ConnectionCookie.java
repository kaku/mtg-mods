/*
 * Decompiled with CFR 0_118.
 */
package org.openide.cookies;

import java.io.IOException;
import java.io.Serializable;
import java.util.EventListener;
import java.util.EventObject;
import java.util.Set;
import org.openide.nodes.Node;

@Deprecated
public interface ConnectionCookie
extends Node.Cookie {
    public void register(Type var1, Node var2) throws IOException;

    public void unregister(Type var1, Node var2) throws IOException;

    public Set<? extends Type> getTypes();

    public static class Event
    extends EventObject {
        static final long serialVersionUID = 7177610435688865839L;
        private Type type;

        public Event(Node n, Type t) {
            super(n);
            this.type = t;
        }

        public Node getNode() {
            return (Node)this.getSource();
        }

        public Type getType() {
            return this.type;
        }
    }

    public static interface Type
    extends Serializable {
        public Class<?> getEventClass();

        public boolean isPersistent();

        public boolean overlaps(Type var1);
    }

    public static interface Listener
    extends Node.Cookie,
    EventListener {
        public void notify(Event var1) throws IllegalArgumentException, ClassCastException;
    }

}

