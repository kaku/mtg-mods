/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.actions.Printable
 */
package org.openide.cookies;

import org.netbeans.api.actions.Printable;
import org.openide.nodes.Node;

public interface PrintCookie
extends Printable,
Node.Cookie {
}

