/*
 * Decompiled with CFR 0_118.
 */
package org.openide.cookies;

import org.openide.nodes.Node;

@Deprecated
public interface FilterCookie
extends Node.Cookie {
    public Class getFilterClass();

    public Object getFilter();

    public void setFilter(Object var1);
}

