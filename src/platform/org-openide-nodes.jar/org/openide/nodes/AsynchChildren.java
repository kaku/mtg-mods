/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.openide.nodes;

import java.awt.EventQueue;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.RequestProcessor;

final class AsynchChildren<T>
extends Children.Keys<Object>
implements ChildFactory.Observer,
Runnable {
    private final ChildFactory<T> factory;
    private final RequestProcessor.Task task;
    private static final RequestProcessor PROC = new RequestProcessor("Asynch children creator ", 4, true);
    private static final Logger logger = Logger.getLogger(AsynchChildren.class.getName());
    volatile boolean initialized = false;
    volatile boolean cancelled = false;
    volatile boolean notified;
    private final Object notifyLock = new Object();

    AsynchChildren(ChildFactory<T> factory) {
        this.factory = factory;
        this.task = PROC.create((Runnable)this, true);
    }

    @Override
    protected void addNotify() {
        logger.log(Level.FINER, "addNotify on {0}", new Object[]{this});
        if (!this.initialized && this.task.isFinished() || this.cancelled) {
            this.cancelled = false;
            Node n = this.factory.getWaitNode();
            if (n != null) {
                this.setKeys(new Object[]{n});
            }
            this.task.schedule(0);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void removeNotify() {
        logger.log(Level.FINER, "removeNotify on {0}", new Object[]{this});
        try {
            this.cancelled = true;
            this.task.cancel();
            this.initialized = false;
            this.setKeys(Collections.emptyList());
        }
        finally {
            Object object = this.notifyLock;
            synchronized (object) {
                if (this.notified) {
                    this.factory.removeNotify();
                }
            }
        }
    }

    @Override
    public void refresh(boolean immediate) {
        logger.log(Level.FINE, "Refresh on {0} immediate {1}", new Object[]{this, immediate &= !EventQueue.isDispatchThread()});
        if (logger.isLoggable(Level.FINEST)) {
            logger.log(Level.FINEST, "Refresh: ", new Exception());
        }
        if (immediate) {
            boolean done;
            LinkedList keys = new LinkedList();
            while (!(done = this.factory.createKeys(keys))) {
            }
            this.setKeys(keys);
        } else {
            this.task.schedule(0);
        }
    }

    @Override
    public Node[] getNodes(boolean optimalResult) {
        Node[] result = Children.Keys.super.getNodes();
        if (optimalResult) {
            this.task.waitFinished();
            result = Children.Keys.super.getNodes();
        }
        return result;
    }

    @Override
    public Node findChild(String name) {
        Node[] result = this.getNodes(true);
        return Children.Keys.super.findChild(name);
    }

    @Override
    protected Node[] createNodes(Object key) {
        if (ChildFactory.isWaitNode(key)) {
            return new Node[]{(Node)key};
        }
        return this.factory.createNodesForKey(key);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void run() {
        boolean done;
        boolean fail = this.cancelled || Thread.interrupted();
        logger.log(Level.FINE, "Running background children creation on {0} fail = {1}", new Object[]{this, fail});
        if (fail) {
            this.setKeys(Collections.emptyList());
            return;
        }
        final int minimalCount = this.getNodesCount();
        LinkedList keys = new LinkedList<T>(){

            @Override
            public boolean add(T e) {
                if (AsynchChildren.this.cancelled || Thread.interrupted()) {
                    throw new Stop();
                }
                LinkedList.super.add(e);
                LinkedList<Node> newKeys = new LinkedList<Node>(this);
                Node n = AsynchChildren.this.factory.getWaitNode();
                if (n != null) {
                    newKeys.add(n);
                }
                newKeys.removeAll(Collections.singleton(null));
                if (newKeys.size() > minimalCount) {
                    AsynchChildren.this.setKeys(newKeys);
                }
                return true;
            }
        };
        do {
            Node n;
            Object object = this.notifyLock;
            synchronized (object) {
                if (!this.notified) {
                    this.notified = true;
                    this.factory.addNotify();
                }
            }
            if (this.cancelled || Thread.interrupted()) {
                this.setKeys(Collections.emptyList());
                return;
            }
            try {
                done = this.factory.createKeys(keys);
            }
            catch (Stop stop) {
                done = true;
            }
            if (this.cancelled || Thread.interrupted()) {
                this.setKeys(Collections.emptyList());
                return;
            }
            LinkedList<Node> newKeys = new LinkedList<Node>(keys);
            if (!done && (n = this.factory.getWaitNode()) != null) {
                newKeys.add(n);
            }
            this.setKeys(newKeys);
        } while (!done && !Thread.interrupted() && !this.cancelled);
        this.initialized = done;
    }

    public String toString() {
        return Object.super.toString() + "[" + this.factory + "]";
    }

    private static final class Stop
    extends RuntimeException {
        private Stop() {
        }
    }

}

