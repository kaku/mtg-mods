/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.Lookup$Result
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Pair
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 */
package org.openide.nodes;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.event.ChangeListener;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.CookieSetLkp;
import org.openide.nodes.Node;
import org.openide.util.ChangeSupport;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public final class CookieSet
implements Lookup.Provider {
    private static ThreadLocal<Object> QUERY_MODE = new ThreadLocal();
    private HashMap<Class, R> map = new HashMap(31);
    private final ChangeSupport cs;
    private final CookieSetLkp ic;
    private Lookup lookup;

    public CookieSet() {
        this(null, null);
    }

    private CookieSet(CookieSetLkp ic, Lookup lookup) {
        this.cs = new ChangeSupport((Object)this);
        this.ic = ic;
        this.lookup = lookup;
    }

    public static CookieSet createGeneric(Before before) {
        CookieSetLkp al = new CookieSetLkp(before);
        return new CookieSet(al, (Lookup)al);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Lookup getLookup() {
        ThreadLocal<Object> threadLocal = QUERY_MODE;
        synchronized (threadLocal) {
            if (this.lookup == null) {
                AbstractNode an = new AbstractNode(this);
                this.lookup = an.getLookup();
            }
        }
        return this.lookup;
    }

    public void add(Node.Cookie cookie) {
        this.addImpl(cookie);
        this.fireChangeEvent();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void addImpl(Object cookie) {
        CookieSet cookieSet = this;
        synchronized (cookieSet) {
            this.registerCookie(cookie.getClass(), cookie);
        }
        if (this.ic != null) {
            this.ic.add(cookie);
        }
    }

    public void remove(Node.Cookie cookie) {
        this.removeImpl(cookie);
        this.fireChangeEvent();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void removeImpl(Object cookie) {
        CookieSet cookieSet = this;
        synchronized (cookieSet) {
            this.unregisterCookie(cookie.getClass(), cookie);
        }
        if (this.ic != null) {
            this.ic.remove(cookie);
        }
    }

    public <T extends Node.Cookie> T getCookie(Class<T> clazz) {
        if (this.ic != null) {
            this.ic.beforeLookupImpl(clazz);
        }
        return this.lookupCookie(clazz);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private <T extends Node.Cookie> T lookupCookie(Class<T> clazz) {
        Node.Cookie ret = null;
        Object queryMode = QUERY_MODE.get();
        CookieSet cookieSet = this;
        synchronized (cookieSet) {
            R r = this.findR(clazz);
            if (r == null) {
                if (queryMode == null || this.ic == null) {
                    return null;
                }
            } else {
                ret = r.cookie();
                if (queryMode instanceof Set) {
                    Set keys = (Set)queryMode;
                    keys.addAll(this.map.keySet());
                }
            }
        }
        if (ret instanceof CookieEntry) {
            if (clazz == queryMode) {
                QUERY_MODE.set(ret);
                ret = null;
            } else {
                ret = ((CookieEntry)ret).getCookie(true);
            }
        } else if (!(ret != null || this.ic == null || Node.Cookie.class.isAssignableFrom(clazz) && clazz != Node.Cookie.class)) {
            CookieSet.enhancedQueryMode(this.lookup, clazz);
            ret = null;
        }
        return (T)((Node.Cookie)clazz.cast(ret));
    }

    static void enhancedQueryMode(Lookup lookup, Class<?> clazz) {
        Object type = QUERY_MODE.get();
        if (type != clazz) {
            return;
        }
        Collection items = lookup.lookupResult(clazz).allItems();
        if (items.size() == 0) {
            return;
        }
        AbstractLookup.Pair[] arr = new AbstractLookup.Pair[items.size()];
        Iterator it = items.iterator();
        for (int i = 0; i < arr.length; ++i) {
            arr[i] = new PairWrap((Lookup.Item)it.next());
        }
        QUERY_MODE.set(arr);
    }

    public void addChangeListener(ChangeListener l) {
        this.cs.addChangeListener(l);
    }

    public void removeChangeListener(ChangeListener l) {
        this.cs.removeChangeListener(l);
    }

    static Object entryQueryMode(Class c) {
        Object prev = QUERY_MODE.get();
        QUERY_MODE.set(c);
        return prev;
    }

    static Object entryAllClassesMode() {
        Object prev = QUERY_MODE.get();
        QUERY_MODE.set(new HashSet());
        return prev;
    }

    static Collection<AbstractLookup.Pair> exitQueryMode(Object prev) {
        Object cookie = QUERY_MODE.get();
        QUERY_MODE.set(prev);
        if (cookie instanceof CookieEntry) {
            return Collections.singleton(new CookieEntryPair((CookieEntry)cookie));
        }
        if (cookie instanceof AbstractLookup.Pair[]) {
            return Arrays.asList((AbstractLookup.Pair[])cookie);
        }
        return null;
    }

    static Set exitAllClassesMode(Object prev) {
        Object cookie = QUERY_MODE.get();
        QUERY_MODE.set(prev);
        if (cookie instanceof HashSet) {
            return (Set)cookie;
        }
        return null;
    }

    final void fireChangeEvent() {
        this.cs.fireChange();
    }

    private void registerCookie(Class<?> c, Object cookie) {
        if (c == null || !Node.Cookie.class.isAssignableFrom(c)) {
            return;
        }
        Class<Node.Cookie> nc = c.asSubclass(Node.Cookie.class);
        R r = this.findR(nc);
        if (r == null) {
            r = new R();
            this.map.put(c, r);
        }
        r.add((Node.Cookie)cookie);
        this.registerCookie(c.getSuperclass(), cookie);
        Class<?>[] inter = c.getInterfaces();
        for (int i = 0; i < inter.length; ++i) {
            this.registerCookie(inter[i], cookie);
        }
    }

    private void unregisterCookie(Class<?> c, Object cookie) {
        if (c == null || !Node.Cookie.class.isAssignableFrom(c)) {
            return;
        }
        Class<Node.Cookie> nc = c.asSubclass(Node.Cookie.class);
        R r = this.findR(nc);
        if (r != null) {
            r.remove((Node.Cookie)cookie);
        }
        this.unregisterCookie(c.getSuperclass(), cookie);
        Class<?>[] inter = c.getInterfaces();
        for (int i = 0; i < inter.length; ++i) {
            this.unregisterCookie(inter[i], cookie);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void add(Class<? extends Node.Cookie> cookieClass, Factory factory) {
        if (factory == null) {
            throw new IllegalArgumentException();
        }
        CookieEntry ce = new CookieEntry(factory, cookieClass);
        CookieSet cookieSet = this;
        synchronized (cookieSet) {
            this.registerCookie(cookieClass, ce);
        }
        if (this.ic != null) {
            this.ic.add(ce, C.INSTANCE);
        }
        this.fireChangeEvent();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void add(Class<? extends Node.Cookie>[] cookieClass, Factory factory) {
        if (factory == null) {
            throw new IllegalArgumentException();
        }
        CookieEntry[] arr = new CookieEntry[cookieClass.length];
        CookieSet cookieSet = this;
        synchronized (cookieSet) {
            for (int i = 0; i < cookieClass.length; ++i) {
                arr[i] = new CookieEntry(factory, cookieClass[i]);
                this.registerCookie(cookieClass[i], arr[i]);
            }
        }
        if (this.ic != null) {
            int i = 0;
            for (Class<? extends Node.Cookie> c : cookieClass) {
                this.ic.add(arr[i++], C.INSTANCE);
            }
        }
        this.fireChangeEvent();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void remove(Class<? extends Node.Cookie> cookieClass, Factory factory) {
        if (factory == null) {
            throw new IllegalArgumentException();
        }
        CookieEntry ce = null;
        CookieSet cookieSet = this;
        synchronized (cookieSet) {
            Node.Cookie c;
            R r = this.findR(cookieClass);
            if (r != null && (c = r.cookie()) instanceof CookieEntry) {
                ce = (CookieEntry)c;
                if (ce.factory == factory) {
                    this.unregisterCookie(cookieClass, c);
                }
            }
        }
        if (this.ic != null && ce != null) {
            this.ic.remove(ce, C.INSTANCE);
        }
        this.fireChangeEvent();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void remove(Class<? extends Node.Cookie>[] cookieClass, Factory factory) {
        if (factory == null) {
            throw new IllegalArgumentException();
        }
        CookieEntry[] arr = new CookieEntry[cookieClass.length];
        CookieSet cookieSet = this;
        synchronized (cookieSet) {
            for (int i = 0; i < cookieClass.length; ++i) {
                CookieEntry ce;
                Node.Cookie c;
                R r = this.findR(cookieClass[i]);
                if (r == null || !((c = r.cookie()) instanceof CookieEntry)) continue;
                arr[i] = ce = (CookieEntry)c;
                if (ce.factory != factory) continue;
                this.unregisterCookie(cookieClass[i], c);
            }
        }
        if (this.ic != null) {
            for (CookieEntry ce : arr) {
                if (ce == null) continue;
                this.ic.remove(ce, C.INSTANCE);
            }
        }
        this.fireChangeEvent();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public /* varargs */ <T> void assign(Class<? extends T> clazz, T ... instances) {
        if (Node.Cookie.class.isAssignableFrom(clazz)) {
            Node.Cookie cookie;
            Class<Node.Cookie> cookieClazz = clazz.asSubclass(Node.Cookie.class);
            while ((cookie = this.lookupCookie(cookieClazz)) != null) {
                CookieSet cookieSet = this;
                synchronized (cookieSet) {
                    R r = this.findR(cookie.getClass());
                    if (r != null && r.cookies != null) {
                        List<Node.Cookie> arr = r.cookies;
                        r.base = null;
                        r.cookies = null;
                        for (Node.Cookie c : arr) {
                            this.unregisterCookie(cookie.getClass(), c);
                        }
                    }
                }
                this.unregisterCookie(cookie.getClass(), cookie);
                if (this.ic == null) continue;
                this.ic.remove(cookie);
            }
            for (T t : instances) {
                this.addImpl(t);
            }
            this.fireChangeEvent();
        } else if (this.ic != null) {
            CookieSet cookieClazz = this;
            synchronized (cookieClazz) {
                for (T t : instances) {
                    this.registerCookie(t.getClass(), t);
                }
            }
            this.ic.replaceInstances(clazz, instances, this);
        }
    }

    private R findR(Class<? extends Node.Cookie> c) {
        return this.map.get(c);
    }

    private static Class<? extends Node.Cookie> baseForCookie(Node.Cookie c) {
        if (c instanceof CookieEntry) {
            return ((CookieEntry)c).klass;
        }
        return c.getClass();
    }

    static class C
    implements InstanceContent.Convertor<CookieEntry, Node.Cookie> {
        static final C INSTANCE = new C();

        C() {
        }

        public Node.Cookie convert(CookieEntry obj) {
            return obj.getCookie(true);
        }

        public Class<? extends Node.Cookie> type(CookieEntry obj) {
            return obj.klass;
        }

        public String id(CookieEntry obj) {
            return obj.klass.getName();
        }

        public String displayName(CookieEntry obj) {
            return obj.klass.getName();
        }
    }

    private static final class CookieEntryPair
    extends AbstractLookup.Pair {
        private CookieEntry entry;

        public CookieEntryPair(CookieEntry e) {
            this.entry = e;
        }

        protected boolean creatorOf(Object obj) {
            return obj == this.entry.getCookie(false);
        }

        public String getDisplayName() {
            return this.getId();
        }

        public String getId() {
            return this.entry.klass.getName();
        }

        public Object getInstance() {
            return this.entry.getCookie(true);
        }

        public Class getType() {
            return this.entry.klass;
        }

        protected boolean instanceOf(Class c) {
            Class k = c;
            return k.isAssignableFrom(this.entry.klass);
        }

        public int hashCode() {
            return this.entry.hashCode() + 5;
        }

        public boolean equals(Object obj) {
            if (obj instanceof CookieEntryPair) {
                return ((CookieEntryPair)obj).entry == this.entry;
            }
            return false;
        }
    }

    static final class PairWrap
    extends AbstractLookup.Pair {
        private Lookup.Item<?> item;
        private boolean created;

        public PairWrap(Lookup.Item<?> item) {
            this.item = item;
        }

        protected boolean instanceOf(Class c) {
            Class k = c;
            return k.isAssignableFrom(this.getType());
        }

        protected boolean creatorOf(Object obj) {
            return this.created && this.getInstance() == obj;
        }

        public Object getInstance() {
            this.created = true;
            return this.item.getInstance();
        }

        public Class<? extends Object> getType() {
            return this.item.getType();
        }

        public String getId() {
            return this.item.getId();
        }

        public String getDisplayName() {
            return this.item.getDisplayName();
        }

        public int hashCode() {
            return 777 + this.item.hashCode();
        }

        public boolean equals(Object object) {
            if (object instanceof PairWrap) {
                PairWrap p = (PairWrap)((Object)object);
                return this.item.equals((Object)p.item);
            }
            return false;
        }
    }

    private static final class R {
        public List<Node.Cookie> cookies;
        public Class base;

        R() {
        }

        public void add(Node.Cookie cookie) {
            if (this.cookies == null) {
                this.cookies = new ArrayList<Node.Cookie>(1);
                this.cookies.add(cookie);
                this.base = CookieSet.baseForCookie(cookie);
                return;
            }
            Class newBase = CookieSet.baseForCookie(cookie);
            if (this.base == null || newBase.isAssignableFrom(this.base)) {
                this.cookies.set(0, cookie);
                this.base = newBase;
            } else {
                this.cookies.add(cookie);
            }
        }

        public boolean remove(Node.Cookie cookie) {
            if (this.cookies == null) {
                return true;
            }
            if (this.cookies.remove(cookie) && this.cookies.size() == 0) {
                this.base = null;
                this.cookies = null;
                return true;
            }
            this.base = CookieSet.baseForCookie(this.cookies.get(0));
            return false;
        }

        public Node.Cookie cookie() {
            return this.cookies == null || this.cookies.isEmpty() ? null : this.cookies.get(0);
        }
    }

    private static class CookieEntry
    implements Node.Cookie {
        final Factory factory;
        private final Class<? extends Node.Cookie> klass;
        private Reference<Node.Cookie> cookie;

        public CookieEntry(Factory factory, Class<? extends Node.Cookie> klass) {
            this.factory = factory;
            this.klass = klass;
        }

        public synchronized Node.Cookie getCookie(boolean create) {
            Node.Cookie ret;
            if (create) {
                if (this.cookie == null || (ret = this.cookie.get()) == null) {
                    ret = this.factory.createCookie(this.klass);
                    if (ret == null) {
                        return null;
                    }
                    this.cookie = new WeakReference<Node.Cookie>(ret);
                }
            } else {
                ret = this.cookie == null ? null : this.cookie.get();
            }
            return ret;
        }
    }

    public static interface Before {
        public void beforeLookup(Class<?> var1);
    }

    public static interface Factory {
        public <T extends Node.Cookie> T createCookie(Class<T> var1);
    }

}

