/*
 * Decompiled with CFR 0_118.
 */
package org.openide.nodes;

import org.openide.nodes.Node;

public interface NodeAcceptor {
    public boolean acceptNodes(Node[] var1);
}

