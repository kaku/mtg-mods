/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.openide.nodes;

import java.beans.Beans;
import java.beans.Introspector;
import java.beans.PropertyEditor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public abstract class PropertySupport<T>
extends Node.Property<T> {
    private boolean canR;
    private boolean canW;

    public PropertySupport(String name, Class<T> type, String displayName, String shortDescription, boolean canR, boolean canW) {
        super(type);
        this.setName(name);
        this.setDisplayName(displayName);
        this.setShortDescription(shortDescription);
        this.canR = canR;
        this.canW = canW;
    }

    @Override
    public boolean canRead() {
        return this.canR;
    }

    @Override
    public boolean canWrite() {
        return this.canW;
    }

    static <T> T cast(Class<T> c, Object o) {
        if (c.isPrimitive()) {
            return (T)o;
        }
        return c.cast(o);
    }

    public static final class Name
    extends PropertySupport<String> {
        private final Node node;

        public Name(Node node) {
            this(node, NbBundle.getBundle(PropertySupport.class).getString("CTL_StandardName"), NbBundle.getBundle(PropertySupport.class).getString("CTL_StandardHint"));
        }

        public Name(Node node, String propName, String hint) {
            super("name", String.class, propName, hint, true, node.canRename());
            this.node = node;
        }

        @Override
        public String getValue() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            return this.node.getName();
        }

        @Override
        public void setValue(String val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            String oldName = this.node.getName();
            this.node.setName(val);
            this.node.firePropertyChange("name", oldName, val);
        }
    }

    public static abstract class WriteOnly<T>
    extends PropertySupport<T> {
        public WriteOnly(String name, Class<T> type, String displayName, String shortDescription) {
            super(name, type, displayName, shortDescription, false, true);
        }

        @Override
        public T getValue() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            throw new IllegalAccessException("Cannod read from WriteOnly property");
        }
    }

    public static abstract class ReadOnly<T>
    extends PropertySupport<T> {
        public ReadOnly(String name, Class<T> type, String displayName, String shortDescription) {
            super(name, type, displayName, shortDescription, true, false);
        }

        @Override
        public void setValue(T val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            throw new IllegalAccessException("Cannot write to ReadOnly property");
        }
    }

    public static abstract class ReadWrite<T>
    extends PropertySupport<T> {
        public ReadWrite(String name, Class<T> type, String displayName, String shortDescription) {
            super(name, type, displayName, shortDescription, true, true);
        }
    }

    public static class Reflection<T>
    extends Node.Property<T> {
        protected Object instance;
        private Method setter;
        private Method getter;
        private Class<? extends PropertyEditor> propertyEditorClass;

        public Reflection(Object instance, Class<T> valueType, Method getter, Method setter) {
            super(valueType);
            if (getter != null && !Modifier.isPublic(getter.getModifiers())) {
                throw new IllegalArgumentException("Cannot use a non-public getter " + getter);
            }
            if (setter != null && !Modifier.isPublic(setter.getModifiers())) {
                throw new IllegalArgumentException("Cannot use a non-public setter " + setter);
            }
            if (getter != null) {
                this.setName(Introspector.decapitalize(getter.getName().replaceFirst("^(get|is|has)", "")));
            } else if (setter != null) {
                this.setName(Introspector.decapitalize(setter.getName().replaceFirst("^set", "")));
            }
            this.instance = instance;
            this.setter = setter;
            this.getter = getter;
        }

        public Reflection(Object instance, Class<T> valueType, String getter, String setter) throws NoSuchMethodException {
            this(instance, valueType, getter == null ? null : Reflection.findAccessibleClass(instance.getClass()).getMethod(getter, new Class[0]), setter == null ? null : Reflection.findAccessibleClass(instance.getClass()).getMethod(setter, valueType));
        }

        public Reflection(Object instance, Class<T> valueType, String property) throws NoSuchMethodException {
            this(instance, valueType, Reflection.findGetter(instance, valueType, property), Reflection.findAccessibleClass(instance.getClass()).getMethod(Reflection.firstLetterToUpperCase(property, "set"), valueType));
        }

        private static <C> Class<? super C> findAccessibleClass(Class<C> clazz) {
            if (Modifier.isPublic(clazz.getModifiers())) {
                return clazz;
            }
            Class<C> sup = clazz.getSuperclass();
            if (sup == null) {
                return Object.class;
            }
            return Reflection.findAccessibleClass(sup);
        }

        private static String firstLetterToUpperCase(String s, String pref) {
            switch (s.length()) {
                case 0: {
                    return pref;
                }
                case 1: {
                    return pref + Character.toUpperCase(s.charAt(0));
                }
            }
            return pref + Character.toUpperCase(s.charAt(0)) + s.substring(1);
        }

        private static Method findGetter(Object instance, Class valueType, String property) throws NoSuchMethodException {
            try {
                return Reflection.findAccessibleClass(instance.getClass()).getMethod(Reflection.firstLetterToUpperCase(property, "get"), new Class[0]);
            }
            catch (NoSuchMethodException e) {
                if (valueType != Boolean.TYPE) {
                    throw e;
                }
                NoSuchMethodException nsme = e;
                return Reflection.findAccessibleClass(instance.getClass()).getMethod(Reflection.firstLetterToUpperCase(property, "is"), new Class[0]);
            }
        }

        @Override
        public boolean canRead() {
            return this.getter != null;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public T getValue() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            if (this.getter == null) {
                throw new IllegalAccessException();
            }
            Object valideInstance = Beans.getInstanceOf(this.instance, this.getter.getDeclaringClass());
            try {
                return PropertySupport.cast(this.getValueType(), this.getter.invoke(valideInstance, new Object[0]));
            }
            catch (IllegalAccessException ex) {
                Object t;
                try {
                    this.getter.setAccessible(true);
                    t = PropertySupport.cast(this.getValueType(), this.getter.invoke(valideInstance, new Object[0]));
                }
                catch (Throwable var4_6) {
                    try {
                        this.getter.setAccessible(false);
                        throw var4_6;
                    }
                    catch (IllegalArgumentException iae) {
                        StringBuffer sb = new StringBuffer("Attempted to invoke method ");
                        sb.append(this.getter.getName());
                        sb.append(" from class ");
                        sb.append(this.getter.getDeclaringClass().getName());
                        sb.append(" on an instance of ");
                        sb.append(valideInstance.getClass().getName());
                        sb.append(" Problem:");
                        sb.append(iae.getMessage());
                        throw (IllegalArgumentException)new IllegalArgumentException(sb.toString()).initCause(iae);
                    }
                }
                this.getter.setAccessible(false);
                return t;
            }
        }

        @Override
        public boolean canWrite() {
            return this.setter != null;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void setValue(T val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            if (this.setter == null) {
                throw new IllegalAccessException();
            }
            Object valideInstance = Beans.getInstanceOf(this.instance, this.setter.getDeclaringClass());
            try {
                this.setter.invoke(valideInstance, val);
            }
            catch (IllegalAccessException ex) {
                try {
                    this.setter.setAccessible(true);
                    this.setter.invoke(valideInstance, val);
                }
                finally {
                    this.setter.setAccessible(false);
                }
            }
        }

        @Override
        public PropertyEditor getPropertyEditor() {
            if (this.propertyEditorClass != null) {
                try {
                    return this.propertyEditorClass.newInstance();
                }
                catch (InstantiationException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
                catch (IllegalAccessException iex) {
                    Exceptions.printStackTrace((Throwable)iex);
                }
            }
            return Node.Property.super.getPropertyEditor();
        }

        public void setPropertyEditorClass(Class<? extends PropertyEditor> clazz) {
            this.propertyEditorClass = clazz;
        }
    }

}

