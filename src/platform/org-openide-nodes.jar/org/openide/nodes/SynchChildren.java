/*
 * Decompiled with CFR 0_118.
 */
package org.openide.nodes;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

final class SynchChildren<T>
extends Children.Keys<T>
implements ChildFactory.Observer {
    private final ChildFactory<T> factory;
    volatile boolean active = false;

    SynchChildren(ChildFactory<T> factory) {
        this.factory = factory;
    }

    @Override
    protected void addNotify() {
        this.active = true;
        this.factory.addNotify();
        this.refresh(true);
    }

    @Override
    protected void removeNotify() {
        this.active = false;
        this.setKeys(Collections.emptyList());
        this.factory.removeNotify();
    }

    @Override
    protected Node[] createNodes(T key) {
        return this.factory.createNodesForKey(key);
    }

    @Override
    public void refresh(boolean immediate) {
        if (this.active) {
            LinkedList toPopulate = new LinkedList();
            while (!this.factory.createKeys(toPopulate)) {
            }
            this.setKeys(toPopulate);
        }
    }
}

