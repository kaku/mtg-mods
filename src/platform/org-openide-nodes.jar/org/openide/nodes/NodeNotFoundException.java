/*
 * Decompiled with CFR 0_118.
 */
package org.openide.nodes;

import java.io.IOException;
import org.openide.nodes.Node;

public final class NodeNotFoundException
extends IOException {
    static final long serialVersionUID = 1493446763320691906L;
    private final Node node;
    private final String name;
    private final int depth;

    NodeNotFoundException(Node node, String name, int depth) {
        this.node = node;
        this.name = name;
        this.depth = depth;
    }

    public Node getClosestNode() {
        return this.node;
    }

    public String getMissingChildName() {
        return this.name;
    }

    public int getClosestNodeDepth() {
        return this.depth;
    }

    @Override
    public String getMessage() {
        return "Could not find child '" + this.name + "' of " + this.node + " at depth " + this.depth;
    }
}

