/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Template
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Pair
 */
package org.openide.nodes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import org.openide.nodes.CookieSet;
import org.openide.nodes.CookieSetLkp;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;

final class NodeLookup
extends AbstractLookup {
    static final ThreadLocal<Node> NO_COOKIE_CHANGE = new ThreadLocal();
    private Collection<Class> queriedCookieClasses = new ArrayList<Class>();
    private Node node;

    public NodeLookup(Node n) {
        this.node = n;
        this.addPair(new CookieSetLkp.SimpleItem<Node>(n));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void addCookie(Node node, Class<?> c, Collection<AbstractLookup.Pair> collection, Map<AbstractLookup.Pair, Class> fromPairToClass) {
        Collection<AbstractLookup.Pair> pairs;
        Object res;
        Object prev = CookieSet.entryQueryMode(c);
        try {
            Class fake = c;
            res = node.getCookie(fake);
        }
        finally {
            pairs = CookieSet.exitQueryMode(prev);
        }
        if (pairs == null) {
            if (res == null) {
                return;
            }
            AbstractLookup.Pair orig = new AbstractLookup.Pair(res);
            AbstractLookup.Pair p = res instanceof Node ? orig : new CookieSet.PairWrap(orig);
            pairs = Collections.singleton(p);
        }
        collection.addAll(pairs);
        for (AbstractLookup.Pair p : pairs) {
            Class oldClazz = fromPairToClass.get((Object)p);
            if (oldClazz != null && !c.isAssignableFrom(oldClazz)) continue;
            fromPairToClass.put(p, c);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final void beforeLookup(Lookup.Template template) {
        Class type = template.getType();
        Set<Node> nds = Node.blockEvents();
        try {
            this.blockingBeforeLookup(type);
        }
        finally {
            Node.unblockEvents(nds);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void blockingBeforeLookup(Class<?> type) {
        if (type == Object.class) {
            Set all;
            Object prev = null;
            try {
                prev = CookieSet.entryAllClassesMode();
                Node.Cookie ignoreResult = this.node.getCookie(Node.Cookie.class);
            }
            finally {
                all = CookieSet.exitAllClassesMode(prev);
            }
            for (Class c : all) {
                this.updateLookupAsCookiesAreChanged(c);
            }
            if (!this.queriedCookieClasses.contains(Node.Cookie.class)) {
                this.updateLookupAsCookiesAreChanged(Node.Cookie.class);
            }
        }
        if (!this.queriedCookieClasses.contains(type)) {
            this.updateLookupAsCookiesAreChanged(type);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void updateLookupAsCookiesAreChanged(Class toAdd) {
        class Cmp
        implements Comparator<AbstractLookup.Pair> {
            final /* synthetic */ Map val$m;

            Cmp() {
                this.val$m = var2_2;
            }

            @Override
            public int compare(AbstractLookup.Pair p1, AbstractLookup.Pair p2) {
                Class c1 = (Class)this.val$m.get((Object)p1);
                Class c2 = (Class)this.val$m.get((Object)p2);
                assert (c1 != null);
                assert (c2 != null);
                if (c1 == c2) {
                    return 0;
                }
                if (c1.isAssignableFrom(c2)) {
                    return -1;
                }
                if (c2.isAssignableFrom(c1)) {
                    return 1;
                }
                if (c1.isAssignableFrom(p2.getType())) {
                    return -1;
                }
                if (c2.isAssignableFrom(p1.getType())) {
                    return 1;
                }
                return 0;
            }
        }
        Iterator<Class> it;
        LinkedHashSet<AbstractLookup.Pair> instances;
        LinkedHashMap<AbstractLookup.Pair, Class> fromPairToQueryClass;
        NodeLookup nodeLookup = this;
        synchronized (nodeLookup) {
            if (toAdd != null) {
                if (this.queriedCookieClasses.contains(toAdd)) {
                    return;
                }
                this.queriedCookieClasses.add(toAdd);
            }
            instances = new LinkedHashSet<AbstractLookup.Pair>(this.queriedCookieClasses.size());
            fromPairToQueryClass = new LinkedHashMap<AbstractLookup.Pair, Class>();
            it = new ArrayList<Class>(this.queriedCookieClasses).iterator();
            CookieSetLkp.SimpleItem<Node> nodePair = new CookieSetLkp.SimpleItem<Node>(this.node);
            instances.add(nodePair);
            fromPairToQueryClass.put(nodePair, Node.class);
        }
        while (it.hasNext()) {
            Class c = it.next();
            NodeLookup.addCookie(this.node, c, instances, fromPairToQueryClass);
        }
        LinkedHashMap<AbstractLookup.Pair, Class> m = fromPairToQueryClass;
        ArrayList list = new ArrayList(instances);
        Collections.sort(list, new Cmp(this, m));
        if (toAdd == null) {
            this.setPairs(list);
        } else {
            Node prev = NO_COOKIE_CHANGE.get();
            try {
                NO_COOKIE_CHANGE.set(this.node);
                this.setPairs(list);
            }
            finally {
                NO_COOKIE_CHANGE.set(prev);
            }
        }
    }

}

