/*
 * Decompiled with CFR 0_118.
 */
package org.openide.nodes;

import java.util.Collection;
import java.util.List;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

abstract class EntrySupport {
    public final Children children;

    protected EntrySupport(Children children) {
        this.children = children;
    }

    public abstract int getNodesCount(boolean var1);

    public abstract Node[] getNodes(boolean var1);

    public abstract Node getNodeAt(int var1);

    public abstract Node[] testNodes();

    public abstract boolean isInitialized();

    abstract void notifySetEntries();

    final void setEntries(Collection<? extends Children.Entry> entries) {
        this.setEntries(entries, false);
    }

    abstract void setEntries(Collection<? extends Children.Entry> var1, boolean var2);

    protected abstract List<Children.Entry> getEntries();

    abstract List<Node> snapshot();

    abstract void refreshEntry(Children.Entry var1);
}

