/*
 * Decompiled with CFR 0_118.
 */
package org.openide.nodes;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeEvent;

public class NodeMemberEvent
extends NodeEvent {
    static final long serialVersionUID = -3973509253579305102L;
    private boolean add;
    private Node[] delta;
    private int[] indices;
    private final List<Node> currSnapshot;
    private final List<Node> prevSnapshot;
    Children.Entry sourceEntry;

    NodeMemberEvent(Node n, boolean add, Node[] delta, Node[] from) {
        super(n);
        this.add = add;
        this.delta = delta;
        this.prevSnapshot = from != null ? Arrays.asList(from) : null;
        this.currSnapshot = n.getChildren().snapshot();
    }

    public final List<Node> getSnapshot() {
        return this.currSnapshot;
    }

    public final boolean isAddEvent() {
        return this.add;
    }

    NodeMemberEvent(Node n, boolean add, int[] indices, List<Node> current, List<Node> previous) {
        super(n);
        this.add = add;
        this.indices = indices;
        Arrays.sort(this.indices);
        this.currSnapshot = current;
        this.prevSnapshot = previous;
    }

    List<Node> getPrevSnapshot() {
        return this.prevSnapshot == null ? this.currSnapshot : this.prevSnapshot;
    }

    public final Node[] getDelta() {
        if (this.delta == null) {
            assert (this.indices != null);
            List<Node> l = this.getPrevSnapshot();
            Node[] arr = new Node[this.indices.length];
            for (int i = 0; i < arr.length; ++i) {
                arr[i] = l.get(this.indices[i]);
            }
            this.delta = arr;
        }
        return this.delta;
    }

    public synchronized int[] getDeltaIndices() {
        int i;
        if (this.indices != null) {
            return this.indices;
        }
        List<Node> nodes = this.getPrevSnapshot();
        List<Node> list = Arrays.asList(this.delta);
        HashSet<Node> set = new HashSet<Node>(list);
        this.indices = new int[this.delta.length];
        int j = 0;
        for (i = 0; i < nodes.size() && j < this.indices.length; ++i) {
            if (!set.contains(nodes.get(i))) continue;
            this.indices[j++] = i;
        }
        if (j != this.delta.length) {
            StringBuilder m = new StringBuilder(1000);
            m.append("Some of a set of deleted nodes are not present in the original ones.\n");
            m.append("See #15478; you may need to check that your Children.Keys keys are safely comparable.");
            m.append("\ni: ").append(i);
            m.append("\nj: ").append(j);
            m.append("\nThis: ").append(this);
            m.append("\nCurrent state:\n");
            m.append(nodes);
            m.append("\nDelta:\n");
            m.append(list);
            throw new IllegalStateException(m.toString());
        }
        return this.indices;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getName());
        sb.append("[node=");
        sb.append(this.getSource());
        sb.append(", add=");
        sb.append(this.isAddEvent());
        Node[] deltaNodes = this.delta;
        int[] deltaIndices = this.getDeltaIndices();
        for (int i = 0; i < deltaIndices.length; ++i) {
            sb.append("\n  ");
            sb.append(i);
            sb.append(" at ");
            sb.append(deltaIndices[i]);
            if (deltaNodes == null) continue;
            sb.append(" = ");
            sb.append(deltaNodes[i]);
        }
        sb.append("\n]");
        sb.append("\ncurr. snapshot: " + this.currSnapshot.getClass().getName());
        sb.append("\n" + this.currSnapshot);
        sb.append("\nprev. snapshot: " + this.getPrevSnapshot().getClass().getName());
        sb.append("\n" + this.getPrevSnapshot());
        return sb.toString();
    }
}

