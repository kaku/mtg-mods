/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Privileged
 *  org.openide.util.Utilities
 */
package org.openide.nodes;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.nodes.Children;
import org.openide.nodes.ChildrenArray;
import org.openide.nodes.EntrySupport;
import org.openide.nodes.Node;
import org.openide.nodes.NodeOp;
import org.openide.util.Mutex;
import org.openide.util.Utilities;

class EntrySupportDefault
extends EntrySupport {
    private List<Children.Entry> entries = Collections.emptyList();
    private static final Reference<ChildrenArray> EMPTY = new WeakReference<Object>(null);
    private Reference<ChildrenArray> array = EMPTY;
    private Map<Children.Entry, Info> map;
    private static final Object LOCK = new Object();
    private static final Logger LOGGER = Logger.getLogger(EntrySupportDefault.class.getName());
    private Thread initThread;
    private boolean inited = false;
    private boolean mustNotifySetEnties = false;

    public EntrySupportDefault(Children ch) {
        super(ch);
    }

    public String toString() {
        return Object.super.toString() + " array: " + this.array.get();
    }

    @Override
    public boolean isInitialized() {
        ChildrenArray arr = this.array.get();
        return this.inited && arr != null && arr.isInitialized();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    List<Node> snapshot() {
        Node[] nodes = this.getNodes();
        try {
            Children.PR.enterReadAccess();
            DefaultSnapshot defaultSnapshot = this.createSnapshot();
            return defaultSnapshot;
        }
        finally {
            Children.PR.exitReadAccess();
        }
    }

    DefaultSnapshot createSnapshot() {
        return new DefaultSnapshot(this.getNodes(), this.array.get());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final Node[] getNodes() {
        Node[] nodes;
        boolean LOG_ENABLED = LOGGER.isLoggable(Level.FINER);
        if (LOG_ENABLED) {
            LOGGER.finer("getNodes() " + this);
        }
        boolean[] results = new boolean[2];
        do {
            ChildrenArray tmpArray = this.getArray(results);
            try {
                Children.PR.enterReadAccess();
                if (this != this.children.getEntrySupport()) {
                    Node[] arrnode = new Node[]{};
                    return arrnode;
                }
                results[1] = this.isInitialized();
                nodes = tmpArray.nodes();
            }
            finally {
                Children.PR.exitReadAccess();
            }
            if (LOG_ENABLED) {
                LOGGER.finer("  length     : " + (nodes == null ? "nodes is null" : Integer.valueOf(nodes.length)));
                LOGGER.finer("  init now   : " + this.isInitialized());
            }
            if (!results[1]) continue;
            return nodes;
        } while (!results[0]);
        this.notifySetEntries();
        return nodes == null ? new Node[]{} : nodes;
    }

    @Override
    public Node[] getNodes(boolean optimalResult) {
        boolean LOG_ENABLED = LOGGER.isLoggable(Level.FINER);
        ChildrenArray hold = null;
        Node find = null;
        if (optimalResult) {
            if (LOG_ENABLED) {
                LOGGER.finer("computing optimal result");
            }
            hold = this.getArray(null);
            if (LOG_ENABLED) {
                LOGGER.finer("optimal result is here: " + hold);
            }
            find = this.children.findChild(null);
            if (LOG_ENABLED) {
                LOGGER.finer("Find child got: " + find);
            }
            Children.LOG.log(Level.FINEST, "after findChild: {0}", optimalResult);
        }
        return this.getNodes();
    }

    @Override
    public final int getNodesCount(boolean optimalResult) {
        return this.getNodes(optimalResult).length;
    }

    @Override
    public Node getNodeAt(int index) {
        Node[] nodes = this.getNodes();
        return index < nodes.length ? nodes[index] : null;
    }

    final Node[] justComputeNodes() {
        if (this.map == null) {
            this.map = Collections.synchronizedMap(new HashMap(17));
            LOGGER.finer("Map initialized");
        }
        LinkedList<Node> l = new LinkedList<Node>();
        for (Children.Entry entry : this.entries) {
            Info info = this.findInfo(entry);
            l.addAll(info.nodes(false));
        }
        Node[] arr = l.toArray(new Node[l.size()]);
        for (int i = 0; i < arr.length; ++i) {
            Node n = arr[i];
            if (n == null) {
                LOGGER.warning("null node among children!");
                for (int j = 0; j < arr.length; ++j) {
                    LOGGER.log(Level.WARNING, "  {0} = {1}", new Object[]{j, arr[j]});
                }
                for (Children.Entry entry2 : this.entries) {
                    Info info = this.findInfo(entry2);
                    LOGGER.log(Level.WARNING, "  entry: {0} info {1} nodes: {2}", new Object[]{entry2, info, info.nodes(false)});
                }
                throw new NullPointerException("arr[" + i + "] is null");
            }
            n.assignTo(this.children, i);
            n.fireParentNodeChange(null, this.children.parent);
        }
        return arr;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Info findInfo(Children.Entry entry) {
        Map<Children.Entry, Info> map = this.map;
        synchronized (map) {
            Info info = this.map.get(entry);
            if (info == null) {
                info = new Info(entry);
                this.map.put(entry, info);
                if (LOGGER.isLoggable(Level.FINER)) {
                    LOGGER.finer("Put: " + entry + " info: " + info);
                }
            }
            return info;
        }
    }

    @Override
    void notifySetEntries() {
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.finer(this + " mustNotifySetEntries()");
        }
        this.mustNotifySetEnties = true;
    }

    private void checkConsistency() {
        assert (this.map.size() == this.entries.size());
    }

    @Override
    protected void setEntries(Collection<? extends Children.Entry> entries, boolean noCheck) {
        List<Info> toAdd;
        Node[] current;
        assert (noCheck || Children.MUTEX.isWriteAccess());
        boolean LOG_ENABLED = LOGGER.isLoggable(Level.FINER);
        ChildrenArray holder = this.array.get();
        if (LOG_ENABLED) {
            LOGGER.finer("setEntries for " + this + " on " + Thread.currentThread());
            LOGGER.finer("       values: " + entries);
            LOGGER.finer("       holder: " + holder);
            LOGGER.finer("       mustNotifySetEntries: " + this.mustNotifySetEnties);
        }
        Node[] arrnode = current = holder == null ? null : holder.nodes();
        if (this.mustNotifySetEnties) {
            if (holder == null) {
                holder = this.getArray(null);
            }
            if (current == null) {
                holder.entrySupport = this;
                current = holder.nodes();
            }
            this.mustNotifySetEnties = false;
        } else if (holder == null || current == null) {
            this.entries = new ArrayList<Children.Entry>(entries);
            if (this.map != null) {
                this.map.keySet().retainAll(new HashSet<Children.Entry>(this.entries));
            }
            return;
        }
        this.checkConsistency();
        LinkedHashSet<Children.Entry> toRemove = new LinkedHashSet<Children.Entry>(this.entries);
        HashSet<? extends Children.Entry> entriesSet = new HashSet<Children.Entry>(entries);
        toRemove.removeAll(entriesSet);
        if (!toRemove.isEmpty()) {
            this.updateRemove(current, toRemove);
            current = holder.nodes();
        }
        if (!(toAdd = this.updateOrder(current, entries)).isEmpty()) {
            this.updateAdd(toAdd, new ArrayList<Children.Entry>(entries));
        }
    }

    private void checkInfo(Info info, Children.Entry entry, Collection<? extends Children.Entry> entries, Map<Children.Entry, Info> map) {
        if (info == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Error in ").append(this.getClass().getName()).append(" with entry ").append(entry).append(" from among entries:");
            for (Children.Entry e : entries) {
                sb.append("\n  ").append(e).append(" contained: ").append(map.containsKey(e));
            }
            sb.append("\nprobably caused by faulty key implementation. The key hashCode() and equals() methods must behave as for an IMMUTABLE object and the hashCode() must return the same value for equals() keys.");
            sb.append("\nmapping:");
            for (Map.Entry ei : map.entrySet()) {
                sb.append("\n  ").append(ei.getKey()).append(" => ").append(ei.getValue());
            }
            throw new IllegalStateException(sb.toString());
        }
    }

    private void updateRemove(Node[] current, Set<Children.Entry> toRemove) {
        assert (Children.MUTEX.isWriteAccess());
        LinkedList<Node> nodes = new LinkedList<Node>();
        ChildrenArray cha = this.array.get();
        for (Children.Entry en : toRemove) {
            Info info = this.map.remove(en);
            this.checkInfo(info, en, new ArrayList(), this.map);
            nodes.addAll(info.nodes(true));
            cha.remove(info);
        }
        this.entries.removeAll(toRemove);
        this.checkConsistency();
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.finer("Current : " + this.entries);
            LOGGER.finer("Removing: " + toRemove);
        }
        if (!nodes.isEmpty()) {
            this.clearNodes();
            this.notifyRemove(nodes, current);
        }
    }

    private List<Info> updateOrder(Node[] current, Collection<? extends Children.Entry> newEntries) {
        assert (Children.MUTEX.isWriteAccess());
        LinkedList<Info> toAdd = new LinkedList<Info>();
        HashMap<Info, Integer> offsets = new HashMap<Info, Integer>();
        int previousPos = 0;
        for (Children.Entry entry : this.entries) {
            Info info = this.map.get(entry);
            this.checkInfo(info, entry, this.entries, this.map);
            offsets.put(info, previousPos);
            previousPos += info.length();
        }
        int[] perm = new int[current.length];
        int currentPos = 0;
        int permSize = 0;
        LinkedList<Children.Entry> reorderedEntries = null;
        for (Children.Entry entry2 : newEntries) {
            Info info = this.map.get(entry2);
            if (info == null) {
                info = new Info(entry2);
                toAdd.add(info);
            } else {
                int len = info.length();
                if (reorderedEntries == null) {
                    reorderedEntries = new LinkedList<Children.Entry>();
                }
                reorderedEntries.add(entry2);
                Integer previousInt = (Integer)offsets.get(info);
                int previousPos2 = previousInt;
                if (currentPos != previousPos2) {
                    for (int i = 0; i < len; ++i) {
                        perm[previousPos2 + i] = 1 + currentPos + i;
                    }
                    permSize += len;
                }
            }
            currentPos += info.length();
        }
        if (permSize > 0) {
            for (int i = 0; i < perm.length; ++i) {
                if (perm[i] == 0) {
                    perm[i] = i;
                    continue;
                }
                int[] arrn = perm;
                int n = i;
                arrn[n] = arrn[n] - 1;
            }
            if (LOGGER.isLoggable(Level.FINER)) {
                LOGGER.finer("Entries before reordering: " + this.entries);
                LOGGER.finer("Entries after reordering: " + reorderedEntries);
            }
            this.entries = reorderedEntries;
            this.checkConsistency();
            this.clearNodes();
            Node p = this.children.parent;
            if (p != null) {
                p.fireReorderChange(perm);
            }
        }
        return toAdd;
    }

    private void updateAdd(Collection<Info> infos, List<Children.Entry> entries) {
        assert (Children.MUTEX.isWriteAccess());
        LinkedList<Node> nodes = new LinkedList<Node>();
        for (Info info : infos) {
            nodes.addAll(info.nodes(false));
            this.map.put(info.entry, info);
        }
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.finer("Entries before updateAdd(): " + this.entries);
            LOGGER.finer("Entries after updateAdd(): " + entries);
        }
        this.entries = entries;
        this.checkConsistency();
        if (!nodes.isEmpty()) {
            this.clearNodes();
            this.notifyAdd(nodes);
        }
    }

    @Override
    final void refreshEntry(Children.Entry entry) {
        Collection<Node> newNodes;
        ChildrenArray holder = this.array.get();
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.finer("refreshEntry: " + entry + " holder=" + holder);
        }
        if (holder == null) {
            return;
        }
        Node[] current = holder.nodes();
        if (current == null) {
            return;
        }
        this.checkConsistency();
        Info info = this.map.get(entry);
        if (info == null) {
            return;
        }
        Collection<Node> oldNodes = info.nodes(false);
        if (oldNodes.equals(newNodes = info.entry.nodes(null))) {
            return;
        }
        HashSet<Node> toRemove = new HashSet<Node>(oldNodes);
        toRemove.removeAll(new HashSet<Node>(newNodes));
        if (!toRemove.isEmpty()) {
            oldNodes.removeAll(toRemove);
            this.clearNodes();
            this.notifyRemove(toRemove, current);
            current = holder.nodes();
        }
        List<Node> toAdd = this.refreshOrder(entry, oldNodes, newNodes);
        info.useNodes(newNodes);
        if (!toAdd.isEmpty()) {
            this.clearNodes();
            this.notifyAdd(toAdd);
        }
    }

    private List<Node> refreshOrder(Children.Entry entry, Collection<Node> oldNodes, Collection<Node> newNodes) {
        LinkedList<Node> toAdd = new LinkedList<Node>();
        HashSet<Node> oldNodesSet = new HashSet<Node>(oldNodes);
        HashSet<Node> toProcess = new HashSet<Node>(oldNodesSet);
        Node[] permArray = new Node[oldNodes.size()];
        Iterator<Node> it2 = newNodes.iterator();
        int pos = 0;
        while (it2.hasNext()) {
            Node n = it2.next();
            if (oldNodesSet.remove(n)) {
                permArray[pos++] = n;
                continue;
            }
            if (!toProcess.contains(n)) {
                toAdd.add(n);
                continue;
            }
            it2.remove();
        }
        int[] perm = NodeOp.computePermutation(oldNodes.toArray(new Node[oldNodes.size()]), permArray);
        if (perm != null) {
            this.clearNodes();
            this.findInfo(entry).useNodes(Arrays.asList(permArray));
            Node p = this.children.parent;
            if (p != null) {
                p.fireReorderChange(perm);
            }
        }
        return toAdd;
    }

    Node[] notifyRemove(Collection<Node> nodes, Node[] current) {
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.finer("notifyRemove: " + nodes);
            LOGGER.finer("Current     : " + Arrays.asList(current));
        }
        Node[] arr = nodes.toArray(new Node[nodes.size()]);
        if (this.children.parent != null) {
            if (this.children.getEntrySupport() == this) {
                this.children.parent.fireSubNodesChange(false, arr, current);
            }
            for (Node n : nodes) {
                n.deassignFrom(this.children);
                n.fireParentNodeChange(this.children.parent, null);
            }
        }
        this.children.destroyNodes(arr);
        return arr;
    }

    void notifyAdd(Collection<Node> nodes) {
        Node n2;
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.finer("notifyAdd: " + nodes);
        }
        for (Node n2 : nodes) {
            n2.assignTo(this.children, -1);
            n2.fireParentNodeChange(null, this.children.parent);
        }
        Node[] arr = nodes.toArray(new Node[nodes.size()]);
        n2 = this.children.parent;
        if (n2 != null && this.children.getEntrySupport() == this) {
            n2.fireSubNodesChange(true, arr, null);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Node[] testNodes() {
        ChildrenArray arr = this.array.get();
        if (arr == null) {
            return null;
        }
        try {
            Children.PR.enterReadAccess();
            Node[] arrnode = arr.nodes();
            return arrnode;
        }
        finally {
            Children.PR.exitReadAccess();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private ChildrenArray getArray(boolean[] cannotWorkBetter) {
        ChildrenArray arr;
        boolean LOG_ENABLED = LOGGER.isLoggable(Level.FINER);
        boolean doInitialize = false;
        Object object = LOCK;
        synchronized (object) {
            arr = this.array.get();
            if (arr == null) {
                arr = new ChildrenArray();
                this.registerChildrenArray(arr, true);
                doInitialize = true;
                this.initThread = Thread.currentThread();
            }
        }
        if (doInitialize) {
            class SetAndNotify
            implements Runnable {
                public ChildrenArray toSet;
                public Children whatSet;
                final /* synthetic */ boolean val$LOG_ENABLED;

                SetAndNotify() {
                    this.val$LOG_ENABLED = n;
                }

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    Object object = LOCK;
                    synchronized (object) {
                        this$0.initThread = null;
                        LOCK.notifyAll();
                    }
                    if (this.val$LOG_ENABLED) {
                        LOGGER.finer("notifyAll done");
                    }
                }
            }
            block24 : {
                if (LOG_ENABLED) {
                    LOGGER.finer("Initialize " + this + " on " + Thread.currentThread());
                }
                try {
                    this.children.callAddNotify();
                    if (!LOG_ENABLED) break block24;
                    LOGGER.finer("addNotify successfully called for " + this + " on " + Thread.currentThread());
                }
                catch (Throwable var7_10) {
                    boolean notifyLater = Children.MUTEX.isReadAccess();
                    if (LOG_ENABLED) {
                        LOGGER.finer("notifyAll for " + this + " on " + Thread.currentThread() + "  notifyLater: " + notifyLater);
                    }
                    arr.entrySupport = this;
                    this.inited = true;
                    SetAndNotify setAndNotify = new SetAndNotify(this, LOG_ENABLED);
                    setAndNotify.toSet = arr;
                    setAndNotify.whatSet = this.children;
                    if (notifyLater) {
                        Children.MUTEX.postWriteRequest((Runnable)setAndNotify);
                        throw var7_10;
                    } else {
                        setAndNotify.run();
                    }
                    throw var7_10;
                }
            }
            boolean notifyLater = Children.MUTEX.isReadAccess();
            if (LOG_ENABLED) {
                LOGGER.finer("notifyAll for " + this + " on " + Thread.currentThread() + "  notifyLater: " + notifyLater);
            }
            arr.entrySupport = this;
            this.inited = true;
            SetAndNotify setAndNotify = new SetAndNotify(this, LOG_ENABLED);
            setAndNotify.toSet = arr;
            setAndNotify.whatSet = this.children;
            if (notifyLater) {
                Children.MUTEX.postWriteRequest((Runnable)setAndNotify);
                return arr;
            } else {
                setAndNotify.run();
            }
            return arr;
        }
        if (this.initThread == null) return arr;
        if (Children.MUTEX.isReadAccess() || Children.MUTEX.isWriteAccess() || this.initThread == Thread.currentThread()) {
            if (LOG_ENABLED) {
                LOGGER.log(Level.FINER, "cannot initialize better " + this + " on " + Thread.currentThread() + " read access: " + Children.MUTEX.isReadAccess() + " write access: " + Children.MUTEX.isWriteAccess() + " initThread: " + this.initThread);
            }
            if (cannotWorkBetter != null) {
                cannotWorkBetter[0] = true;
            }
            arr.entrySupport = this;
            return arr;
        }
        Object notifyLater = LOCK;
        synchronized (notifyLater) {
            while (this.initThread != null) {
                if (LOG_ENABLED) {
                    LOGGER.finer("waiting for children for " + this + " on " + Thread.currentThread());
                }
                try {
                    LOCK.wait();
                }
                catch (InterruptedException ex) {}
            }
        }
        if (!LOG_ENABLED) return arr;
        LOGGER.finer(" children are here for " + this + " on " + Thread.currentThread() + " children " + this.children);
        return arr;
    }

    private void clearNodes() {
        ChildrenArray arr;
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.finer("  clearNodes()");
        }
        if ((arr = this.array.get()) != null) {
            arr.clear();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void registerChildrenArray(ChildrenArray chArr, boolean weak) {
        boolean LOG_ENABLED = LOGGER.isLoggable(Level.FINER);
        if (LOG_ENABLED) {
            LOGGER.finer("registerChildrenArray: " + chArr + " weak: " + weak);
        }
        Object object = LOCK;
        synchronized (object) {
            if (this.array != null && this.array.get() == chArr && ((ChArrRef)this.array).isWeak() == weak) {
                return;
            }
            this.array = new ChArrRef(chArr, weak);
        }
        if (LOG_ENABLED) {
            LOGGER.finer("pointed by: " + chArr + " to: " + this.array);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void finalizedChildrenArray(Reference caller) {
        assert (caller.get() == null);
        try {
            Children.PR.enterWriteAccess();
            if (LOGGER.isLoggable(Level.FINER)) {
                LOGGER.fine("previous array: " + this.array + " caller: " + caller);
            }
            Object object = LOCK;
            synchronized (object) {
                if (this.array == caller && this.children.getEntrySupport() == this) {
                    this.mustNotifySetEnties = false;
                    this.array = EMPTY;
                    this.inited = false;
                    this.children.callRemoveNotify();
                    assert (this.array == EMPTY);
                }
            }
        }
        finally {
            Children.PR.exitWriteAccess();
        }
    }

    @Override
    protected List<Children.Entry> getEntries() {
        return new ArrayList<Children.Entry>(this.entries);
    }

    private class ChArrRef
    extends WeakReference<ChildrenArray>
    implements Runnable {
        private final ChildrenArray chArr;

        public ChArrRef(ChildrenArray referent, boolean weak) {
            super(referent, Utilities.activeReferenceQueue());
            this.chArr = weak ? null : referent;
        }

        @Override
        public ChildrenArray get() {
            return this.chArr != null ? this.chArr : (ChildrenArray)WeakReference.super.get();
        }

        boolean isWeak() {
            return this.chArr == null;
        }

        @Override
        public void run() {
            EntrySupportDefault.this.finalizedChildrenArray(this);
        }
    }

    static class DefaultSnapshot
    extends AbstractList<Node> {
        private Node[] nodes;
        Object holder;

        public DefaultSnapshot(Node[] nodes, ChildrenArray cha) {
            this.nodes = nodes;
            this.holder = cha;
        }

        @Override
        public Node get(int index) {
            return this.nodes != null && index < this.nodes.length ? this.nodes[index] : null;
        }

        @Override
        public int size() {
            return this.nodes != null ? this.nodes.length : 0;
        }
    }

    final class Info {
        int length;
        final Children.Entry entry;

        public Info(Children.Entry entry) {
            this.entry = entry;
        }

        public Collection<Node> nodes(boolean hasToExist) {
            assert (!hasToExist || EntrySupportDefault.this.array.get() != null);
            ChildrenArray arr = EntrySupportDefault.this.getArray(null);
            return arr.nodesFor(this, hasToExist);
        }

        public void useNodes(Collection<Node> nodes) {
            ChildrenArray arr = EntrySupportDefault.this.getArray(null);
            arr.useNodes(this, nodes);
            for (Node n : nodes) {
                n.assignTo(EntrySupportDefault.this.children, -1);
                n.fireParentNodeChange(null, EntrySupportDefault.this.children.parent);
            }
        }

        public int length() {
            return this.length;
        }

        public String toString() {
            return "Children.Info[" + this.entry + ",length=" + this.length + "]";
        }
    }

}

