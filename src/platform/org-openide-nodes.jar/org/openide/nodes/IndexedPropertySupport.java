/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package org.openide.nodes;

import java.beans.Beans;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.util.Utilities;

public class IndexedPropertySupport<T, E>
extends Node.IndexedProperty<T, E> {
    protected Object instance;
    private Method setter;
    private Method getter;
    private Method indexedSetter;
    private Method indexedGetter;

    public IndexedPropertySupport(Object instance, Class<T> valueType, Class<E> elementType, Method getter, Method setter, Method indexedGetter, Method indexedSetter) {
        super(valueType, elementType);
        this.instance = instance;
        this.setter = setter;
        this.getter = getter;
        this.indexedSetter = indexedSetter;
        this.indexedGetter = indexedGetter;
    }

    @Override
    public final void setDisplayName(String s) {
        Node.IndexedProperty.super.setDisplayName(s);
    }

    @Override
    public final void setName(String s) {
        Node.IndexedProperty.super.setName(s);
    }

    @Override
    public final void setShortDescription(String s) {
        Node.IndexedProperty.super.setShortDescription(s);
    }

    @Override
    public boolean canRead() {
        return this.getter != null;
    }

    @Override
    public T getValue() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        if (!this.canRead()) {
            throw new IllegalAccessException();
        }
        Object validInstance = Beans.getInstanceOf(this.instance, this.getter.getDeclaringClass());
        return PropertySupport.cast(this.getValueType(), this.getter.invoke(validInstance, new Object[0]));
    }

    @Override
    public boolean canWrite() {
        return this.setter != null;
    }

    @Override
    public void setValue(T val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        if (!this.canWrite()) {
            throw new IllegalAccessException();
        }
        Object validInstance = Beans.getInstanceOf(this.instance, this.setter.getDeclaringClass());
        Object value = val;
        if (val != null && this.setter.getParameterTypes()[0].getComponentType().isPrimitive() && !val.getClass().getComponentType().isPrimitive()) {
            value = Utilities.toPrimitiveArray((Object[])((Object[])val));
        }
        this.setter.invoke(validInstance, value);
    }

    @Override
    public boolean canIndexedRead() {
        return this.indexedGetter != null;
    }

    @Override
    public E getIndexedValue(int index) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        if (!this.canIndexedRead()) {
            throw new IllegalAccessException();
        }
        Object validInstance = Beans.getInstanceOf(this.instance, this.indexedGetter.getDeclaringClass());
        return PropertySupport.cast(this.getElementType(), this.indexedGetter.invoke(validInstance, index));
    }

    @Override
    public boolean canIndexedWrite() {
        return this.indexedSetter != null;
    }

    @Override
    public void setIndexedValue(int index, E val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        if (!this.canIndexedWrite()) {
            throw new IllegalAccessException();
        }
        Object validInstance = Beans.getInstanceOf(this.instance, this.indexedSetter.getDeclaringClass());
        this.indexedSetter.invoke(validInstance, new Integer(index), val);
    }
}

