/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package org.openide.nodes;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.nodes.Children;
import org.openide.nodes.EntrySupportLazy;
import org.openide.nodes.Node;
import org.openide.nodes.NodeOp;
import org.openide.util.Utilities;

final class EntrySupportLazyState {
    static final EntrySupportLazyState UNINITIALIZED = new EntrySupportLazyState();
    private final boolean inited;
    private final Thread initThread;
    private final boolean initInProgress;
    private final boolean mustNotifySetEntries;
    private final List<Children.Entry> entries;
    private final List<Children.Entry> visibleEntries;
    private final Map<Children.Entry, EntryInfo> entryToInfo;

    private EntrySupportLazyState() {
        this(false, null, false, false, Collections.emptyList(), Collections.emptyList(), Collections.emptyMap());
    }

    private EntrySupportLazyState(boolean inited, Thread initThread, boolean initInProgress, boolean mustNotifySetEntries, List<Children.Entry> entries, List<Children.Entry> visibleEntries, Map<Children.Entry, EntryInfo> entryToInfo) {
        this.inited = inited;
        this.initThread = initThread;
        this.initInProgress = initInProgress;
        this.mustNotifySetEntries = mustNotifySetEntries;
        this.entries = entries;
        this.visibleEntries = visibleEntries;
        this.entryToInfo = entryToInfo;
    }

    final boolean isInited() {
        return this.inited;
    }

    final boolean isInitInProgress() {
        return this.initInProgress;
    }

    final Thread initThread() {
        return this.initThread;
    }

    final boolean isMustNotify() {
        return this.mustNotifySetEntries;
    }

    final List<Children.Entry> getEntries() {
        return Collections.unmodifiableList(this.entries);
    }

    final List<Children.Entry> getVisibleEntries() {
        return Collections.unmodifiableList(this.visibleEntries);
    }

    final Map<Children.Entry, EntryInfo> getEntryToInfo() {
        return Collections.unmodifiableMap(this.entryToInfo);
    }

    private EntrySupportLazyState cloneState() {
        try {
            return (EntrySupportLazyState)this.clone();
        }
        catch (CloneNotSupportedException ex) {
            throw new IllegalStateException(ex);
        }
    }

    final EntrySupportLazyState changeInited(boolean newInited) {
        return new EntrySupportLazyState(newInited, this.initThread, this.initInProgress, this.mustNotifySetEntries, this.entries, this.visibleEntries, this.entryToInfo);
    }

    final EntrySupportLazyState changeThread(Thread t) {
        return new EntrySupportLazyState(this.inited, t, this.initInProgress, this.mustNotifySetEntries, this.entries, this.visibleEntries, this.entryToInfo);
    }

    final EntrySupportLazyState changeProgress(boolean b) {
        return new EntrySupportLazyState(this.inited, this.initThread, b, this.mustNotifySetEntries, this.entries, this.visibleEntries, this.entryToInfo);
    }

    final EntrySupportLazyState changeMustNotify(boolean b) {
        return new EntrySupportLazyState(this.inited, this.initThread, this.initInProgress, b, this.entries, this.visibleEntries, this.entryToInfo);
    }

    final EntrySupportLazyState changeEntries(List<Children.Entry> entries, List<Children.Entry> visibleEntries, Map<Children.Entry, EntryInfo> entryToInfo) {
        if (entries == null) {
            entries = this.entries;
        }
        if (visibleEntries == null) {
            visibleEntries = this.visibleEntries;
        }
        if (entryToInfo == null) {
            entryToInfo = this.entryToInfo;
        }
        EntrySupportLazyState state = new EntrySupportLazyState(this.inited, this.initThread, this.initInProgress, this.mustNotifySetEntries, entries, visibleEntries, entryToInfo);
        int entriesSize = 0;
        int entryToInfoSize = 0;
        assert ((entriesSize = state.getEntries().size()) >= 0);
        assert ((entryToInfoSize = state.getEntryToInfo().size()) >= 0);
        assert (state.getEntries().size() == state.getEntryToInfo().size());
        return state;
    }

    public String toString() {
        int entriesSize = this.getEntries().size();
        int entryToInfoSize = this.getEntryToInfo().size();
        return "Inited: " + this.inited + "\nThread: " + this.initThread + "\nInProgress: " + this.initInProgress + "\nMustNotify: " + this.mustNotifySetEntries + "\nEntries: " + this.getEntries().size() + "; vis. entries: " + EntrySupportLazy.notNull(this.getVisibleEntries()).size() + "; Infos: " + this.getEntryToInfo().size() + "; entriesSize: " + entriesSize + "; entryToInfoSize: " + entryToInfoSize + EntrySupportLazy.dumpEntriesInfos(this.getEntries(), this.getEntryToInfo());
    }

    private static final class NodeRef
    extends WeakReference<Node>
    implements Runnable {
        private final EntryInfo info;

        public NodeRef(Node node, EntryInfo info) {
            super(node, Utilities.activeReferenceQueue());
            info.lazy().registerNode(1, info);
            this.info = info;
        }

        @Override
        public void run() {
            this.info.lazy().registerNode(-1, this.info);
        }
    }

    static final class EntryInfo {
        private final EntrySupportLazy lazy;
        private final Children.Entry entry;
        private final int index;
        private NodeRef refNode;
        Thread creatingNodeThread;

        public EntryInfo(EntrySupportLazy lazy, Children.Entry entry) {
            this(lazy, entry, -1, (NodeRef)null);
        }

        private EntryInfo(EntrySupportLazy lazy, Children.Entry entry, int index, NodeRef refNode) {
            this.lazy = lazy;
            this.entry = entry;
            this.index = index;
            this.refNode = refNode;
        }

        private EntryInfo(EntrySupportLazy lazy, Children.Entry entry, int index, Node refNode) {
            this.lazy = lazy;
            this.entry = entry;
            this.index = index;
            this.refNode = new NodeRef(refNode, this);
        }

        final EntryInfo changeNode(Node node) {
            if (node != null) {
                return new EntryInfo(this.lazy, this.entry, this.index, node);
            }
            return new EntryInfo(this.lazy, this.entry, this.index, this.refNode);
        }

        final EntryInfo changeIndex(int index) {
            return new EntryInfo(this.lazy, this.entry, index, this.refNode);
        }

        final EntrySupportLazy lazy() {
            return this.lazy;
        }

        final Children.Entry entry() {
            return this.entry;
        }

        private Object lock() {
            return this.lazy.LOCK;
        }

        public final Node getNode() {
            return this.getNode(false, null);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public final Node getNode(boolean refresh, Object source) {
            Node node;
            do {
                Object ex;
                boolean creating = false;
                Object object = this.lock();
                synchronized (object) {
                    if (refresh) {
                        this.refNode = null;
                    }
                    if (this.refNode != null && (node = (Node)this.refNode.get()) != null) {
                        return node;
                    }
                    if (this.creatingNodeThread != null) {
                        if (this.creatingNodeThread == Thread.currentThread()) {
                            return new EntrySupportLazy.DummyNode();
                        }
                        try {
                            this.lock().wait();
                        }
                        catch (InterruptedException ex) {}
                    } else {
                        this.creatingNodeThread = Thread.currentThread();
                        creating = true;
                    }
                }
                Collection nodes = Collections.emptyList();
                try {
                    if (creating) {
                        try {
                            nodes = this.entry.nodes(source);
                        }
                        catch (RuntimeException ex) {
                            NodeOp.warning(ex);
                        }
                    }
                    ex = this.lock();
                }
                catch (Throwable var9_9) {
                    Object object2 = this.lock();
                    synchronized (object2) {
                        if (!creating) {
                            if (this.refNode != null && (node = (Node)this.refNode.get()) != null) {
                                return node;
                            }
                            // MONITOREXIT [5, 21, 8, 13, 15] lbl67 : MonitorExitStatement: MONITOREXIT : var10_10
                            continue;
                        }
                        if (nodes.isEmpty()) {
                            node = new EntrySupportLazy.DummyNode();
                        } else {
                            if (nodes.size() > 1) {
                                EntrySupportLazy.LOGGER.log(Level.FINE, "Number of nodes for Entry: {0} is {1} instead of 1", new Object[]{this.entry, nodes.size()});
                            }
                            node = (Node)nodes.iterator().next();
                        }
                        this.refNode = new NodeRef(node, this);
                        if (creating) {
                            this.creatingNodeThread = null;
                            this.lock().notifyAll();
                        }
                    }
                    throw var9_9;
                }
                synchronized (ex) {
                    if (!creating) {
                        if (this.refNode != null && (node = (Node)this.refNode.get()) != null) {
                            return node;
                        }
                        continue;
                    }
                    if (nodes.isEmpty()) {
                        node = new EntrySupportLazy.DummyNode();
                    } else {
                        if (nodes.size() > 1) {
                            EntrySupportLazy.LOGGER.log(Level.FINE, "Number of nodes for Entry: {0} is {1} instead of 1", new Object[]{this.entry, nodes.size()});
                        }
                        node = (Node)nodes.iterator().next();
                    }
                    this.refNode = new NodeRef(node, this);
                    if (creating) {
                        this.creatingNodeThread = null;
                        this.lock().notifyAll();
                    }
                    break;
                }
                break;
            } while (true);
            Children ch = this.lazy().children;
            node.assignTo(ch, -1);
            node.fireParentNodeChange(null, ch.parent);
            return node;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        Node currentNode() {
            Object object = this.lock();
            synchronized (object) {
                return this.refNode == null ? null : (Node)this.refNode.get();
            }
        }

        final boolean isHidden() {
            return this.index == -2;
        }

        final int getIndex() {
            assert (this.index >= 0);
            return this.index;
        }

        public String toString() {
            return "EntryInfo for entry: " + this.entry + ", node: " + (this.refNode == null ? null : (Node)this.refNode.get());
        }
    }

}

