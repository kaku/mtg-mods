/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.datatransfer.ExTransferable
 *  org.openide.util.datatransfer.ExTransferable$Single
 *  org.openide.util.datatransfer.MultiTransferObject
 *  org.openide.util.datatransfer.PasteType
 */
package org.openide.nodes;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.nodes.Node;
import org.openide.util.datatransfer.ExTransferable;
import org.openide.util.datatransfer.MultiTransferObject;
import org.openide.util.datatransfer.PasteType;

public abstract class NodeTransfer {
    public static final int DND_NONE = 0;
    public static final int DND_COPY = 1;
    public static final int DND_MOVE = 2;
    public static final int DND_COPY_OR_MOVE = 3;
    public static final int DND_LINK = 1073741824;
    public static final int DND_REFERENCE = 1073741824;
    public static final int CLIPBOARD_COPY = 1;
    public static final int CLIPBOARD_CUT = 4;
    public static final int COPY = 1;
    public static final int MOVE = 6;
    private static final DataFlavor nodePasteFlavor;
    private static MessageFormat dndMimeType;

    private NodeTransfer() {
    }

    private static DataFlavor createDndFlavor(int actions) {
        Exception ex2;
        Exception ex2;
        try {
            return new DataFlavor(dndMimeType.format(new Object[]{new Integer(actions)}), null, Node.class.getClassLoader());
        }
        catch (IllegalArgumentException iae) {
            ex2 = iae;
        }
        catch (ClassNotFoundException cnfE) {
            ex2 = cnfE;
        }
        throw new IllegalStateException("Cannot createDndFlavor(" + actions + ")", ex2);
    }

    public static ExTransferable.Single transferable(final Node n, int actions) {
        return new ExTransferable.Single(NodeTransfer.createDndFlavor(actions)){

            public Object getData() {
                return n;
            }
        };
    }

    public static Node node(Transferable t, int action) {
        DataFlavor[] flavors = t.getTransferDataFlavors();
        if (flavors == null) {
            return null;
        }
        int len = flavors.length;
        String subtype = "x-java-openide-nodednd";
        String primary = "application";
        String mask = "mask";
        for (int i = 0; i < len; ++i) {
            DataFlavor df = flavors[i];
            if (!df.getSubType().equals(subtype) || !df.getPrimaryType().equals(primary)) continue;
            try {
                int m = Integer.valueOf(df.getParameter(mask));
                if ((m & action) == 0) continue;
                return (Node)t.getTransferData(df);
            }
            catch (NumberFormatException nfe) {
                NodeTransfer.maybeReportException(nfe);
                continue;
            }
            catch (ClassCastException cce) {
                NodeTransfer.maybeReportException(cce);
                continue;
            }
            catch (IOException ioe) {
                NodeTransfer.maybeReportException(ioe);
                continue;
            }
            catch (UnsupportedFlavorException ufe) {
                NodeTransfer.maybeReportException(ufe);
            }
        }
        return null;
    }

    public static Node[] nodes(Transferable t, int action) {
        try {
            if (t.isDataFlavorSupported(ExTransferable.multiFlavor)) {
                MultiTransferObject mto = (MultiTransferObject)t.getTransferData(ExTransferable.multiFlavor);
                int count = mto.getCount();
                Node[] ns = new Node[count];
                boolean ok = true;
                for (int i = 0; i < count; ++i) {
                    Node n = NodeTransfer.node(mto.getTransferableAt(i), action);
                    if (n == null) {
                        ok = false;
                        break;
                    }
                    ns[i] = n;
                }
                if (ok && count > 0) {
                    return ns;
                }
            } else {
                Node n = NodeTransfer.node(t, action);
                if (n != null) {
                    return new Node[]{n};
                }
            }
        }
        catch (ClassCastException cce) {
            NodeTransfer.maybeReportException(cce);
        }
        catch (IOException ioe) {
            NodeTransfer.maybeReportException(ioe);
        }
        catch (UnsupportedFlavorException ufe) {
            NodeTransfer.maybeReportException(ufe);
        }
        return null;
    }

    public static <T extends Node.Cookie> T cookie(Transferable t, int action, Class<T> cookie) {
        Node n = NodeTransfer.node(t, action);
        return n == null ? null : (T)n.getCookie(cookie);
    }

    public static ExTransferable.Single createPaste(final Paste paste) {
        return new ExTransferable.Single(nodePasteFlavor){

            public Object getData() {
                return paste;
            }
        };
    }

    public static Paste findPaste(Transferable t) {
        try {
            if (t.isDataFlavorSupported(nodePasteFlavor)) {
                return (Paste)t.getTransferData(nodePasteFlavor);
            }
        }
        catch (ClassCastException cce) {
            NodeTransfer.maybeReportException(cce);
        }
        catch (IOException ioe) {
            NodeTransfer.maybeReportException(ioe);
        }
        catch (UnsupportedFlavorException ufe) {
            NodeTransfer.maybeReportException(ufe);
        }
        return null;
    }

    private static void maybeReportException(Exception e) {
        Logger.getLogger(NodeTransfer.class.getName()).log(Level.WARNING, "Node transfer error: {0}", e.getMessage());
        Logger.getLogger(NodeTransfer.class.getName()).log(Level.INFO, null, e);
    }

    static {
        try {
            nodePasteFlavor = new DataFlavor("application/x-java-openide-nodepaste;class=org.openide.nodes.Node", Node.getString("LBL_nodePasteFlavor"), Node.class.getClassLoader());
        }
        catch (ClassNotFoundException e) {
            throw new AssertionError(e);
        }
        dndMimeType = new MessageFormat("application/x-java-openide-nodednd;class=org.openide.nodes.Node;mask={0}");
    }

    public static interface Paste {
        public PasteType[] types(Node var1);
    }

}

