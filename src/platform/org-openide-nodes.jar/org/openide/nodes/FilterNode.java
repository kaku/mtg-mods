/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Privileged
 *  org.openide.util.Parameters
 *  org.openide.util.WeakSet
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.datatransfer.NewType
 *  org.openide.util.datatransfer.PasteType
 */
package org.openide.nodes;

import java.awt.Component;
import java.awt.Image;
import java.awt.datatransfer.Transferable;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.event.EventListenerList;
import org.openide.nodes.Children;
import org.openide.nodes.EntrySupport;
import org.openide.nodes.EntrySupportDefault;
import org.openide.nodes.EntrySupportLazy;
import org.openide.nodes.EntrySupportLazyState;
import org.openide.nodes.Node;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Mutex;
import org.openide.util.Parameters;
import org.openide.util.WeakSet;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.NewType;
import org.openide.util.datatransfer.PasteType;

public class FilterNode
extends Node {
    protected static final int DELEGATE_SET_NAME = 1;
    protected static final int DELEGATE_GET_NAME = 2;
    protected static final int DELEGATE_SET_DISPLAY_NAME = 4;
    protected static final int DELEGATE_GET_DISPLAY_NAME = 8;
    protected static final int DELEGATE_SET_SHORT_DESCRIPTION = 16;
    protected static final int DELEGATE_GET_SHORT_DESCRIPTION = 32;
    protected static final int DELEGATE_DESTROY = 64;
    protected static final int DELEGATE_GET_ACTIONS = 128;
    protected static final int DELEGATE_GET_CONTEXT_ACTIONS = 256;
    protected static final int DELEGATE_SET_VALUE = 512;
    protected static final int DELEGATE_GET_VALUE = 1024;
    private static final int DELEGATE_ALL = 2047;
    private static final Map<Class<?>, Boolean> overridesGetDisplayNameCache = new WeakHashMap(27);
    private static final Map<Class<?>, Boolean> replaceProvidedLookupCache = new WeakHashMap(27);
    private static volatile int hashCodeDepth;
    private Node original;
    private PropertyChangeListener propL;
    private NodeListener nodeL;
    private int delegateMask;
    private boolean pchlAttached = false;
    private boolean childrenProvided;
    private boolean lookupProvided;
    private final Object LISTENER_LOCK = new Object();
    static final Logger LOGGER;

    public FilterNode(Node original) {
        this(original, null);
    }

    public FilterNode(Node original, org.openide.nodes.Children children) {
        this(original, children, new FilterLookup());
    }

    public FilterNode(Node original, org.openide.nodes.Children children, Lookup lookup) {
        super(children == null ? (original.isLeaf() ? org.openide.nodes.Children.LEAF : new Children(original)) : children, lookup);
        Parameters.notNull((CharSequence)"original", (Object)original);
        this.childrenProvided = children != null;
        this.lookupProvided = lookup != null && !(lookup instanceof FilterLookup);
        this.original = original;
        this.init();
        Lookup lkp = this.internalLookup(false);
        if (lkp instanceof FilterLookup) {
            ((FilterLookup)lkp).ownNode(this);
        } else if (lkp == null) {
            this.getNodeListener();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    final Lookup replaceProvidedLookup(Lookup lookup) {
        Map map = replaceProvidedLookupCache;
        synchronized (map) {
            Boolean b = replaceProvidedLookupCache.get(this.getClass());
            if (b == null) {
                b = !this.overridesAMethod("getCookie", Class.class);
                replaceProvidedLookupCache.put(this.getClass(), b);
            }
            return b != false ? lookup : null;
        }
    }

    private /* varargs */ boolean overridesAMethod(String name, Class ... arguments) {
        if (this.getClass() == FilterNode.class) {
            return false;
        }
        try {
            Method m = this.getClass().getMethod(name, arguments);
            if (m.getDeclaringClass() != FilterNode.class) {
                return true;
            }
        }
        catch (NoSuchMethodException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        return false;
    }

    private void init() {
        this.delegateMask = 2047;
    }

    @Override
    void notifyPropertyChangeListenerAdded(PropertyChangeListener l) {
        if (!this.pchlAttached) {
            this.original.addPropertyChangeListener(this.getPropertyChangeListener());
            this.pchlAttached = true;
        }
    }

    @Override
    void notifyPropertyChangeListenerRemoved(PropertyChangeListener l) {
        if (this.getPropertyChangeListenersCount() == 0) {
            this.original.removePropertyChangeListener(this.getPropertyChangeListener());
            this.pchlAttached = false;
        }
    }

    protected final void enableDelegation(int mask) {
        if ((mask & -2048) != 0) {
            throw new IllegalArgumentException("Bad delegation mask: " + mask);
        }
        this.delegateMask |= mask;
    }

    protected final void disableDelegation(int mask) {
        if ((mask & -2048) != 0) {
            throw new IllegalArgumentException("Bad delegation mask: " + mask);
        }
        this.delegateMask &= ~ mask;
    }

    private final boolean delegating(int what) {
        return (this.delegateMask & what) != 0;
    }

    @Override
    public Node cloneNode() {
        if (this.isDefault()) {
            return new FilterNode(this.original);
        }
        return new FilterNode(this);
    }

    private boolean checkIfIamAccessibleFromOriginal(Node original) {
        if (this == original) {
            throw new IllegalArgumentException("Node cannot be its own original (even thru indirect chain)");
        }
        if (original instanceof FilterNode) {
            FilterNode f = (FilterNode)original;
            this.checkIfIamAccessibleFromOriginal(f.original);
        }
        return true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final void changeOriginal(Node original, boolean changeChildren) {
        Lookup lkp;
        Parameters.notNull((CharSequence)"original", (Object)original);
        if (changeChildren && !(this.getChildren() instanceof Children) && this.getChildren() != Children.LEAF) {
            throw new IllegalStateException("Can't change implicitly defined Children on FilterNode");
        }
        assert (this.checkIfIamAccessibleFromOriginal(original));
        try {
            Children.PR.enterWriteAccess();
            this.original.removeNodeListener(this.getNodeListener());
            if (this.pchlAttached) {
                this.original.removePropertyChangeListener(this.getPropertyChangeListener());
            }
            this.original = original;
            this.original.addNodeListener(this.getNodeListener());
            if (this.pchlAttached) {
                this.original.addPropertyChangeListener(this.getPropertyChangeListener());
            }
            if (changeChildren) {
                if (original.isLeaf() && this.getChildren() != Children.LEAF) {
                    this.setChildren(Children.LEAF);
                } else if (!original.isLeaf() && this.getChildren() == Children.LEAF) {
                    this.setChildren(new Children(original));
                } else if (!original.isLeaf() && this.getChildren() != Children.LEAF) {
                    ((Children)this.getChildren()).changeOriginal(original);
                }
            }
        }
        finally {
            Children.PR.exitWriteAccess();
        }
        if ((lkp = this.internalLookup(false)) instanceof FilterLookup) {
            ((FilterLookup)lkp).checkNode();
        }
        this.fireCookieChange();
        this.fireNameChange(null, null);
        this.fireDisplayNameChange(null, null);
        this.fireShortDescriptionChange(null, null);
        this.fireIconChange();
        this.fireOpenedIconChange();
        this.firePropertySetsChange(null, null);
    }

    @Override
    public void setValue(String attributeName, Object value) {
        if (this.delegating(512)) {
            this.original.setValue(attributeName, value);
        } else {
            super.setValue(attributeName, value);
        }
    }

    @Override
    public Object getValue(String attributeName) {
        if (this.delegating(1024)) {
            return this.original.getValue(attributeName);
        }
        return super.getValue(attributeName);
    }

    @Override
    public void setName(String s) {
        if (this.delegating(1)) {
            this.original.setName(s);
        } else {
            super.setName(s);
        }
    }

    @Override
    public String getName() {
        if (this.delegating(2)) {
            return this.original.getName();
        }
        return super.getName();
    }

    @Override
    public void setDisplayName(String s) {
        if (this.delegating(4)) {
            this.original.setDisplayName(s);
        } else {
            super.setDisplayName(s);
        }
    }

    @Override
    public String getDisplayName() {
        if (this.delegating(8)) {
            return this.original.getDisplayName();
        }
        return super.getDisplayName();
    }

    @Override
    public void setShortDescription(String s) {
        if (this.delegating(16)) {
            this.original.setShortDescription(s);
        } else {
            super.setShortDescription(s);
        }
    }

    @Override
    public String getShortDescription() {
        if (this.delegating(32)) {
            return this.original.getShortDescription();
        }
        return super.getShortDescription();
    }

    @Override
    public Image getIcon(int type) {
        Image icon = this.original.getIcon(type);
        if (icon != null) {
            return icon;
        }
        LOGGER.log(Level.WARNING, "Cannot return null from {0}.getIcon", this.original.getClass().getName());
        return Node.EMPTY.getIcon(type);
    }

    @Override
    public Image getOpenedIcon(int type) {
        return this.original.getOpenedIcon(type);
    }

    @Override
    public HelpCtx getHelpCtx() {
        return this.original.getHelpCtx();
    }

    @Override
    public boolean canRename() {
        return this.original.canRename();
    }

    @Override
    public boolean canDestroy() {
        return this.original.canDestroy();
    }

    @Override
    public void destroy() throws IOException {
        if (this.delegating(64)) {
            this.original.destroy();
        } else {
            super.destroy();
        }
    }

    private final void originalDestroyed() {
        try {
            super.destroy();
        }
        catch (IOException ex) {
            Logger.getLogger(FilterNode.class.getName()).log(Level.WARNING, null, ex);
        }
    }

    @Override
    public Node.PropertySet[] getPropertySets() {
        return this.original.getPropertySets();
    }

    @Override
    public Transferable clipboardCopy() throws IOException {
        return this.original.clipboardCopy();
    }

    @Override
    public Transferable clipboardCut() throws IOException {
        return this.original.clipboardCut();
    }

    @Override
    public boolean canCopy() {
        return this.original.canCopy();
    }

    @Override
    public boolean canCut() {
        return this.original.canCut();
    }

    @Override
    public Transferable drag() throws IOException {
        return this.original.drag();
    }

    @Override
    public PasteType getDropType(Transferable t, int action, int index) {
        return this.original.getDropType(t, action, index);
    }

    @Override
    public PasteType[] getPasteTypes(Transferable t) {
        return this.original.getPasteTypes(t);
    }

    @Override
    public NewType[] getNewTypes() {
        return this.original.getNewTypes();
    }

    @Deprecated
    @Override
    public SystemAction[] getActions() {
        if (this.delegating(128)) {
            return this.original.getActions();
        }
        return super.getActions();
    }

    @Deprecated
    @Override
    public SystemAction[] getContextActions() {
        if (this.delegating(256)) {
            return this.original.getContextActions();
        }
        return super.getContextActions();
    }

    @Deprecated
    @Override
    public SystemAction getDefaultAction() {
        return this.original.getDefaultAction();
    }

    @Override
    public Action[] getActions(boolean context) {
        if (context ? !this.delegating(128) || this.overridesAMethod("getContextActions", new Class[0]) : !this.delegating(256) || this.overridesAMethod("getActions", new Class[0])) {
            return super.getActions(context);
        }
        Action[] retValue = this.original.getActions(context);
        return retValue;
    }

    @Override
    public Action getPreferredAction() {
        Action retValue = this.overridesAMethod("getDefaultAction", new Class[0]) ? super.getPreferredAction() : this.original.getPreferredAction();
        return retValue;
    }

    @Override
    public String getHtmlDisplayName() {
        if (this.overridesGetDisplayName()) {
            return null;
        }
        return this.delegating(8) ? this.original.getHtmlDisplayName() : super.getHtmlDisplayName();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean overridesGetDisplayName() {
        Map map = overridesGetDisplayNameCache;
        synchronized (map) {
            Boolean b = overridesGetDisplayNameCache.get(this.getClass());
            if (b == null) {
                b = this.overridesAMethod("getDisplayName", new Class[0]);
                overridesGetDisplayNameCache.put(this.getClass(), b);
            }
            return b;
        }
    }

    @Override
    public boolean hasCustomizer() {
        return this.original.hasCustomizer();
    }

    @Override
    public Component getCustomizer() {
        return this.original.getCustomizer();
    }

    @Override
    public <T extends Node.Cookie> T getCookie(Class<T> type) {
        Lookup l = this.internalLookup(true);
        if (l != null) {
            Object res = l.lookup(type);
            return (T)(type.isInstance(res) && res instanceof Node.Cookie ? (Node.Cookie)type.cast(res) : null);
        }
        return this.original.getCookie(type);
    }

    @Override
    public Node.Handle getHandle() {
        if (!this.isDefault()) {
            return null;
        }
        Node.Handle original = this.original.getHandle();
        if (original == null) {
            return null;
        }
        return new FilterHandle(original);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Node)) {
            return false;
        }
        if (this == o) {
            return true;
        }
        Node left = FilterNode.getRepresentation(this);
        Node right = FilterNode.getRepresentation((Node)o);
        if (left instanceof FilterNode || right instanceof FilterNode) {
            return left == right;
        }
        return left.equals(right);
    }

    private static Node getRepresentation(Node n) {
        while (n instanceof FilterNode) {
            FilterNode fn = (FilterNode)n;
            if (!fn.isDefault()) {
                return n;
            }
            n = fn.original;
        }
        return n;
    }

    @Override
    public int hashCode() {
        try {
            int result;
            assert (FilterNode.hashCodeLogging(true));
            int n = result = this.isDefault() ? this.original.hashCode() : System.identityHashCode(this);
            assert (FilterNode.hashCodeLogging(false));
            return result;
        }
        catch (StackError err) {
            err.add(this);
            throw err;
        }
    }

    private static boolean hashCodeLogging(boolean enter) {
        if (hashCodeDepth > 1000) {
            hashCodeDepth = 0;
            throw new StackError();
        }
        hashCodeDepth = enter ? ++hashCodeDepth : --hashCodeDepth;
        return true;
    }

    protected Node getOriginal() {
        return this.original;
    }

    protected PropertyChangeListener createPropertyChangeListener() {
        return new PropertyChangeAdapter(this);
    }

    protected NodeListener createNodeListener() {
        return new NodeAdapter(this);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    PropertyChangeListener getPropertyChangeListener() {
        Object object = this.LISTENER_LOCK;
        synchronized (object) {
            if (this.propL == null) {
                this.propL = this.createPropertyChangeListener();
            }
            return this.propL;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    NodeListener getNodeListener() {
        Object object = this.LISTENER_LOCK;
        synchronized (object) {
            if (this.nodeL == null) {
                this.nodeL = this.createNodeListener();
                this.getOriginal().addNodeListener(this.nodeL);
            }
            return this.nodeL;
        }
    }

    @Override
    final void listenerAdded() {
        this.getNodeListener();
    }

    private boolean isDefault() {
        if (this.getClass() != FilterNode.class) {
            return false;
        }
        return !this.childrenProvided && !this.lookupProvided;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    final void updateChildren() {
        if (this.isDefault()) {
            org.openide.nodes.Children newChildren;
            newChildren = null;
            try {
                Children.PR.enterReadAccess();
                if (this.original.hierarchy == Children.LEAF && this.hierarchy != Children.LEAF) {
                    newChildren = Children.LEAF;
                } else if (this.original.hierarchy != Children.LEAF && this.hierarchy == Children.LEAF) {
                    newChildren = new Children(this.original);
                }
            }
            finally {
                Children.PR.exitReadAccess();
            }
            if (newChildren != null) {
                this.setChildren(newChildren);
            }
        } else {
            super.updateChildren();
        }
    }

    static {
        LOGGER = Logger.getLogger(FilterNode.class.getName());
    }

    private static final class FilterLookup
    extends Lookup {
        private FilterNode node;
        private Lookup delegate;
        private Set<ProxyResult> results;

        FilterLookup() {
        }

        public void ownNode(FilterNode n) {
            this.node = n;
        }

        private <T> T replaceNodes(T orig, Class<T> clazz) {
            if (FilterLookup.isNodeQuery(clazz) && orig == this.node.getOriginal() && clazz.isInstance(this.node)) {
                return clazz.cast(this.node);
            }
            return orig;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Lookup checkNode() {
            Lookup l = this.node.getOriginal().getLookup();
            if (this.delegate == l) {
                return l;
            }
            Iterator<ProxyResult> toCheck = null;
            FilterLookup filterLookup = this;
            synchronized (filterLookup) {
                if (l != this.delegate) {
                    this.delegate = l;
                    if (this.results != null) {
                        toCheck = new ArrayList<ProxyResult>(this.results).iterator();
                    }
                }
            }
            if (toCheck != null) {
                while (toCheck.hasNext()) {
                    ProxyResult p = (ProxyResult)((Object)toCheck.next());
                    if (!p.updateLookup(l)) continue;
                    p.resultChanged(null);
                }
            }
            return this.delegate;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public <T> Lookup.Result<T> lookup(Lookup.Template<T> template) {
            ProxyResult<T> p = new ProxyResult<T>(template);
            FilterLookup filterLookup = this;
            synchronized (filterLookup) {
                if (this.results == null) {
                    this.results = new WeakSet();
                }
                this.results.add(p);
            }
            return p;
        }

        public <T> T lookup(Class<T> clazz) {
            Object result = this.checkNode().lookup(clazz);
            if (result == null && clazz.isInstance(this.node)) {
                result = clazz.cast(this.node);
            }
            return (T)this.replaceNodes(result, clazz);
        }

        private static boolean isNodeQuery(Class<?> c) {
            return Node.class.isAssignableFrom(c) || c.isAssignableFrom(Node.class);
        }

        public <T> Lookup.Item<T> lookupItem(Lookup.Template<T> template) {
            boolean nodeQ = FilterLookup.isNodeQuery(template.getType());
            FilterItem i = this.checkNode().lookupItem(template);
            if (nodeQ && i == null && template.getType().isInstance(this.node) && (template.getInstance() == null || template.getInstance() == this.node)) {
                i = this.checkNode().lookupItem(FilterLookup.wackohacko(template.getId(), template.getInstance()));
            }
            return nodeQ && i != null ? new FilterItem(i, template.getType()) : i;
        }

        private static <T> Lookup.Template<T> wackohacko(String id, T instance) {
            return new Lookup.Template(Node.class, id, instance);
        }

        private final class FilterItem<T>
        extends Lookup.Item<T> {
            private Lookup.Item<T> delegate;
            private Class<T> clazz;

            FilterItem(Lookup.Item<T> d, Class<T> clazz) {
                this.delegate = d;
                this.clazz = clazz;
            }

            public String getDisplayName() {
                return this.delegate.getDisplayName();
            }

            public String getId() {
                return this.delegate.getId();
            }

            public T getInstance() {
                return (T)FilterLookup.this.replaceNodes(this.delegate.getInstance(), this.clazz);
            }

            public Class<? extends T> getType() {
                return this.delegate.getType();
            }
        }

        private final class ProxyResult<T>
        extends Lookup.Result<T>
        implements LookupListener {
            private Lookup.Template<T> template;
            private Lookup.Result<T> delegate;
            private EventListenerList listeners;

            ProxyResult(Lookup.Template<T> template) {
                this.template = template;
            }

            private Lookup.Result<T> checkResult() {
                this.updateLookup(FilterLookup.this.checkNode());
                return this.delegate;
            }

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            public boolean updateLookup(Lookup l) {
                Collection oldPairs = this.delegate != null ? this.delegate.allItems() : null;
                ProxyResult proxyResult = this;
                synchronized (proxyResult) {
                    if (this.delegate != null) {
                        this.delegate.removeLookupListener((LookupListener)this);
                    }
                    this.delegate = l.lookup(this.template);
                    if (this.template.getType().isAssignableFrom(FilterLookup.this.node.getClass()) && this.delegate.allItems().isEmpty()) {
                        this.delegate = l.lookup(FilterLookup.wackohacko(this.template.getId(), this.template.getInstance()));
                    }
                    this.delegate.addLookupListener((LookupListener)this);
                }
                if (oldPairs == null) {
                    return false;
                }
                Collection newPairs = this.delegate.allItems();
                return !oldPairs.equals(newPairs);
            }

            public synchronized void addLookupListener(LookupListener l) {
                if (this.listeners == null) {
                    this.listeners = new EventListenerList();
                }
                this.listeners.add(LookupListener.class, l);
            }

            public synchronized void removeLookupListener(LookupListener l) {
                if (this.listeners != null) {
                    this.listeners.remove(LookupListener.class, l);
                }
            }

            public Collection<? extends T> allInstances() {
                Collection c = this.checkResult().allInstances();
                if (FilterLookup.isNodeQuery(this.template.getType())) {
                    ArrayList<Object> ll = new ArrayList<Object>(c.size());
                    for (Object o : c) {
                        ll.add(FilterLookup.this.replaceNodes(o, this.template.getType()));
                    }
                    if (ll.isEmpty() && this.template.getType().isInstance(FilterLookup.this.node) && (this.template.getInstance() == null || this.template.getInstance() == FilterLookup.this.node)) {
                        ll.add(this.template.getType().cast(FilterLookup.this.node));
                    }
                    return ll;
                }
                return c;
            }

            public Set<Class<? extends T>> allClasses() {
                return this.checkResult().allClasses();
            }

            public Collection<? extends Lookup.Item<T>> allItems() {
                return this.checkResult().allItems();
            }

            public void resultChanged(LookupEvent anEvent) {
                EventListenerList l = this.listeners;
                if (l == null) {
                    return;
                }
                Object[] listeners = l.getListenerList();
                if (listeners.length == 0) {
                    return;
                }
                LookupEvent ev = new LookupEvent((Lookup.Result)this);
                for (int i = listeners.length - 1; i >= 0; i -= 2) {
                    LookupListener ll = (LookupListener)listeners[i];
                    ll.resultChanged(ev);
                }
            }
        }

    }

    private static final class FilterHandle
    implements Node.Handle {
        static final long serialVersionUID = 7928908039428333839L;
        private Node.Handle original;

        public FilterHandle(Node.Handle original) {
            this.original = original;
        }

        @Override
        public Node getNode() throws IOException {
            return new FilterNode(this.original.getNode());
        }

        public String toString() {
            return "FilterHandle[" + this.original + "]";
        }
    }

    private static class ChildrenAdapter
    implements NodeListener {
        private Reference<Children> childrenRef;

        public ChildrenAdapter(Children ch) {
            this.childrenRef = new WeakReference<Children>(ch);
        }

        @Override
        public void propertyChange(PropertyChangeEvent ev) {
        }

        @Override
        public void childrenAdded(NodeMemberEvent ev) {
            Children children = this.childrenRef.get();
            if (children == null) {
                return;
            }
            children.filterChildrenAdded(ev);
        }

        @Override
        public void childrenRemoved(NodeMemberEvent ev) {
            Children children = this.childrenRef.get();
            if (children == null) {
                return;
            }
            children.filterChildrenRemoved(ev);
        }

        @Override
        public void childrenReordered(NodeReorderEvent ev) {
            Children children = this.childrenRef.get();
            if (children == null) {
                return;
            }
            children.filterChildrenReordered(ev);
        }

        @Override
        public void nodeDestroyed(NodeEvent ev) {
        }
    }

    public static class Children
    extends Children.Keys<Node>
    implements Cloneable {
        protected Node original;
        private ChildrenAdapter nodeL;

        public Children(Node or) {
            this(or, or.getChildren().isLazy());
        }

        private Children(Node or, boolean lazy) {
            super(lazy);
            this.original = or;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         * Enabled aggressive block sorting
         * Enabled unnecessary exception pruning
         * Enabled aggressive exception aggregation
         * Converted monitor instructions to comments
         * Lifted jumps to return sites
         */
        @Override
        EntrySupport entrySupport() {
            Object origSupport;
            FilterChildrenSupport support = null;
            Class<org.openide.nodes.Children> class_ = org.openide.nodes.Children.class;
            // MONITORENTER : org.openide.nodes.Children.class
            if (this.getEntrySupport() != null && !this.getEntrySupport().isInitialized()) {
                support = (FilterChildrenSupport)((Object)this.getEntrySupport());
            }
            // MONITOREXIT : class_
            if (support != null) {
                assert (!Thread.holdsLock(org.openide.nodes.Children.class));
                origSupport = this.original.getChildren().entrySupport();
                Class<org.openide.nodes.Children> class_2 = org.openide.nodes.Children.class;
                // MONITORENTER : org.openide.nodes.Children.class
                if (this.getEntrySupport() == support && support.originalSupport() != origSupport) {
                    this.setEntrySupport(null);
                }
                // MONITOREXIT : class_2
            }
            origSupport = org.openide.nodes.Children.class;
            // MONITORENTER : org.openide.nodes.Children.class
            if (this.getEntrySupport() != null) {
                // MONITOREXIT : origSupport
                return this.getEntrySupport();
            }
            // MONITOREXIT : origSupport
            org.openide.nodes.Children origChildren = this.original.getChildren();
            EntrySupport os = origChildren.entrySupport();
            boolean osIsLazy = origChildren.isLazy();
            Class<org.openide.nodes.Children> class_3 = org.openide.nodes.Children.class;
            // MONITORENTER : org.openide.nodes.Children.class
            if (this.getEntrySupport() != null) {
                // MONITOREXIT : class_3
                return this.getEntrySupport();
            }
            this.lazySupport = osIsLazy;
            EntrySupport es = this.lazySupport ? new LazySupport(this, (EntrySupportLazy)os) : new DefaultSupport(this, (EntrySupportDefault)os);
            this.setEntrySupport(es);
            this.postInitializeEntrySupport(es);
            // MONITOREXIT : class_3
            return this.getEntrySupport();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected final void changeOriginal(Node original) {
            try {
                boolean wasAttached;
                PR.enterWriteAccess();
                if (FilterNode.LOGGER.isLoggable(Level.FINER)) {
                    FilterNode.LOGGER.finer("changeOriginal() " + this);
                    FilterNode.LOGGER.finer("    old original children: " + this.original.getChildren());
                    FilterNode.LOGGER.finer("    old original lazy support: " + this.original.getChildren().lazySupport);
                    FilterNode.LOGGER.finer("    new original: " + original);
                    FilterNode.LOGGER.finer("    new original children: " + original.getChildren());
                    FilterNode.LOGGER.finer("    new original lazy support: " + original.getChildren().lazySupport);
                    FilterNode.LOGGER.finer("    Children adapter: " + this.nodeL);
                }
                boolean bl = wasAttached = this.nodeL != null;
                if (wasAttached) {
                    this.original.removeNodeListener(this.nodeL);
                    this.nodeL = null;
                }
                this.changeSupport(original);
                if (wasAttached) {
                    this.addNotifyImpl();
                }
            }
            finally {
                PR.exitWriteAccess();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         * Enabled aggressive block sorting
         * Enabled unnecessary exception pruning
         * Enabled aggressive exception aggregation
         * Converted monitor instructions to comments
         * Lifted jumps to return sites
         */
        private void changeSupport(Node newOriginal) {
            Object snapshot;
            boolean LOG_ENABLED = FilterNode.LOGGER.isLoggable(Level.FINER);
            boolean init = this.entrySupport().isInitialized();
            boolean changed = false;
            if (LOG_ENABLED) {
                FilterNode.LOGGER.finer("changeSupport() " + this);
                FilterNode.LOGGER.finer("    newOriginal: " + newOriginal);
                FilterNode.LOGGER.finer("    entrySupport().isInitialized(): " + init);
                FilterNode.LOGGER.finer("    parent: " + this.parent);
            }
            if (init && this.parent != null && (snapshot = this.entrySupport().snapshot()).size() > 0) {
                int[] idxs = Children.getSnapshotIdxs(snapshot);
                if (newOriginal != null) {
                    this.original = newOriginal;
                }
                changed = true;
                Class<org.openide.nodes.Children> class_ = org.openide.nodes.Children.class;
                // MONITORENTER : org.openide.nodes.Children.class
                this.setEntrySupport(null);
                // MONITOREXIT : class_
                if (LOG_ENABLED) {
                    FilterNode.LOGGER.finer("   firing node removal: " + snapshot);
                }
                this.parent.fireSubNodesChangeIdx(false, idxs, null, Collections.<Node>emptyList(), (List<Node>)snapshot);
            }
            if (!changed) {
                if (newOriginal != null) {
                    this.original = newOriginal;
                }
                snapshot = org.openide.nodes.Children.class;
                // MONITORENTER : org.openide.nodes.Children.class
                this.setEntrySupport(null);
                // MONITOREXIT : snapshot
            }
            if (!init) {
                if (newOriginal != null) return;
            }
            this.entrySupport().notifySetEntries();
            if (LOG_ENABLED) {
                FilterNode.LOGGER.log(Level.FINER, "    initializing new support");
            }
            this.entrySupport().getNodesCount(false);
        }

        protected void finalize() {
            if (this.nodeL != null) {
                this.original.removeNodeListener(this.nodeL);
            }
            this.nodeL = null;
        }

        @Override
        public Object clone() {
            return new Children(this.original);
        }

        @Override
        protected void addNotify() {
            this.addNotifyImpl();
        }

        private void addNotifyImpl() {
            this.nodeL = new ChildrenAdapter(this);
            this.original.addNodeListener(this.nodeL);
            this.filterSupport().update();
        }

        @Override
        protected void removeNotify() {
            this.setKeys(Collections.emptySet());
            if (this.nodeL != null) {
                this.original.removeNodeListener(this.nodeL);
                this.nodeL = null;
            }
        }

        protected Node copyNode(Node node) {
            return node.cloneNode();
        }

        @Override
        public Node findChild(String name) {
            return this.filterSupport().findChild(name);
        }

        @Override
        protected Node[] createNodes(Node key) {
            return new Node[]{this.copyNode(key)};
        }

        @Deprecated
        @Override
        public boolean add(Node[] arr) {
            return this.original.getChildren().add(arr);
        }

        private FilterChildrenSupport filterSupport() {
            return (FilterChildrenSupport)((Object)this.entrySupport());
        }

        private boolean checkSupportChanged() {
            FilterChildrenSupport support = (FilterChildrenSupport)((Object)this.entrySupport());
            EntrySupport origSupport = this.original.getChildren().entrySupport();
            if (support.originalSupport() != origSupport) {
                assert (MUTEX.isWriteAccess());
                this.changeSupport(null);
                return true;
            }
            return false;
        }

        @Deprecated
        @Override
        public boolean remove(Node[] arr) {
            return this.original.getChildren().remove(arr);
        }

        protected void filterChildrenAdded(NodeMemberEvent ev) {
            if (this.checkSupportChanged()) {
                return;
            }
            this.filterSupport().filterChildrenAdded(ev);
        }

        protected void filterChildrenRemoved(NodeMemberEvent ev) {
            if (this.checkSupportChanged()) {
                return;
            }
            this.filterSupport().filterChildrenRemoved(ev);
        }

        protected void filterChildrenReordered(NodeReorderEvent ev) {
            this.filterSupport().filterChildrenReordered(ev);
        }

        @Override
        public Node[] getNodes(boolean optimalResult) {
            return this.filterSupport().callGetNodes(optimalResult);
        }

        @Override
        public int getNodesCount(boolean optimalResult) {
            return this.filterSupport().callGetNodesCount(optimalResult);
        }

        @Override
        Children.Entry createEntryForKey(Node key) {
            return this.filterSupport().createEntryForKey(key);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        void switchSupport(boolean toLazy) {
            try {
                PR.enterWriteAccess();
                ((Children.Keys)this.original.getChildren()).switchSupport(toLazy);
                Children.Keys.super.switchSupport(toLazy);
            }
            finally {
                PR.exitWriteAccess();
            }
        }

        static interface FilterChildrenSupport {
            public Node[] callGetNodes(boolean var1);

            public int callGetNodesCount(boolean var1);

            public Node findChild(String var1);

            public void filterChildrenAdded(NodeMemberEvent var1);

            public void filterChildrenRemoved(NodeMemberEvent var1);

            public void filterChildrenReordered(NodeReorderEvent var1);

            public void update();

            public Children.Entry createEntryForKey(Node var1);

            public EntrySupport originalSupport();
        }

        private class LazySupport
        extends EntrySupportLazy
        implements FilterChildrenSupport {
            EntrySupportLazy origSupport;

            public LazySupport(org.openide.nodes.Children ch, EntrySupportLazy origSupport) {
                super(ch);
                this.origSupport = origSupport;
            }

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            protected EntrySupportLazy.LazySnapshot createSnapshot(List<Children.Entry> entries, Map<Children.Entry, EntrySupportLazyState.EntryInfo> e2i, boolean delayed) {
                Object object = this.LOCK;
                synchronized (object) {
                    return delayed ? new FilterDelayedLazySnapshot(entries, e2i) : new FilterLazySnapshot(entries, e2i);
                }
            }

            @Override
            public Node[] callGetNodes(boolean optimalResult) {
                Node[] hold = null;
                if (optimalResult) {
                    hold = Children.this.original.getChildren().getNodes(true);
                }
                hold = Children.this.getNodes();
                return hold;
            }

            @Override
            public int callGetNodesCount(boolean optimalResult) {
                return Children.this.original.getChildren().getNodesCount(optimalResult);
            }

            @Override
            public Node findChild(String name) {
                Children.this.original.getChildren().findChild(name);
                return Children.this.findChild(name);
            }

            @Override
            public void filterChildrenAdded(NodeMemberEvent ev) {
                if (ev.sourceEntry == null) {
                    this.update();
                } else {
                    this.doRefreshEntry(ev.sourceEntry);
                }
            }

            @Override
            public void filterChildrenRemoved(NodeMemberEvent ev) {
                if (ev.sourceEntry == null) {
                    this.update();
                } else {
                    this.doRefreshEntry(ev.sourceEntry);
                }
            }

            @Override
            public void filterChildrenReordered(NodeReorderEvent ev) {
                this.update();
            }

            @Override
            public void update() {
                ChildrenAdapter cha;
                boolean LOG_ENABLED = LOGGER.isLoggable(Level.FINER);
                if (LOG_ENABLED) {
                    LOGGER.finer("updateEntries() " + this);
                }
                if ((cha = Children.this.nodeL) != null) {
                    final int count = this.origSupport.getNodesCount(false);
                    Children.MUTEX.postWriteRequest(new Runnable(){

                        @Override
                        public void run() {
                            LazySupport.this.updateEntries(count);
                        }
                    });
                }
            }

            private void updateEntries(int count) {
                boolean LOG_ENABLED = LOGGER.isLoggable(Level.FINER);
                if (LOG_ENABLED) {
                    LOGGER.finer("updateEntries() " + this);
                }
                if (LOG_ENABLED) {
                    LOGGER.finer("    origSupport.getNodesCount(): " + count);
                }
                List<Children.Entry> origEntries = this.origSupport.getEntries();
                if (LOG_ENABLED) {
                    LOGGER.finer("    origSupport.getEntries() - size: " + origEntries.size() + " data: " + origEntries);
                }
                ArrayList<FilterNodeEntry> filtEntries = new ArrayList<FilterNodeEntry>(origEntries.size());
                for (Children.Entry e : origEntries) {
                    filtEntries.add(new FilterNodeEntry(e));
                }
                this.setEntries(filtEntries);
                if (!this.origSupport.isInitialized()) {
                    this.origSupport.notifySetEntries();
                    return;
                }
            }

            private void doRefreshEntry(Children.Entry entry) {
                this.refreshEntry((Children.Entry)((Object)new FilterNodeEntry(entry)));
            }

            @Override
            public Children.Entry createEntryForKey(Node key) {
                Children.Entry entry = this.origSupport.entryForNode(key);
                return new FilterNodeEntry(entry);
            }

            @Override
            public EntrySupport originalSupport() {
                return this.origSupport;
            }

            private final class FilterNodeEntry
            extends Children.Keys<Object> {
                Children.Entry origEntry;

                public FilterNodeEntry(Children.Entry origEntry) {
                    super(Children.this);
                    this.origEntry = origEntry;
                }

                public Object getKey() {
                    return this;
                }

                public int getCnt() {
                    return 0;
                }

                public Collection<Node> nodes(Object source) {
                    Node node;
                    if (source != null) {
                        EntrySupportLazy.LazySnapshot origSnapshot = (EntrySupportLazy.LazySnapshot)source;
                        node = origSnapshot.get(this.origEntry);
                    } else {
                        node = LazySupport.this.origSupport.getNode(this.origEntry);
                    }
                    this.key = node;
                    if (node == null || EntrySupportLazy.isDummyNode(node)) {
                        return Collections.emptyList();
                    }
                    Node[] nodes = Children.this.createNodes(node);
                    if (nodes == null) {
                        return Collections.emptyList();
                    }
                    return Arrays.asList(nodes);
                }

                public int hashCode() {
                    return this.origEntry.hashCode();
                }

                public boolean equals(Object o) {
                    return o instanceof FilterNodeEntry ? this.origEntry.equals(((FilterNodeEntry)o).origEntry) : false;
                }

                public String toString() {
                    return "FilterNodeEntry[" + this.origEntry + "]@" + Integer.toString(this.hashCode(), 16);
                }
            }

            final class FilterDelayedLazySnapshot
            extends FilterLazySnapshot {
                public FilterDelayedLazySnapshot(List<Children.Entry> entries, Map<Children.Entry, EntrySupportLazyState.EntryInfo> e2i) {
                    super(entries, e2i);
                }
            }

            class FilterLazySnapshot
            extends EntrySupportLazy.LazySnapshot {
                private EntrySupportLazy.LazySnapshot origSnapshot;

                public FilterLazySnapshot(List<Children.Entry> entries, Map<Children.Entry, EntrySupportLazyState.EntryInfo> e2i) {
                    super(entries, e2i);
                    this.origSnapshot = LazySupport.this.origSupport.createSnapshot();
                }

                @Override
                public Node get(Children.Entry entry) {
                    EntrySupportLazyState.EntryInfo info = (EntrySupportLazyState.EntryInfo)this.entryToInfo.get(entry);
                    Node node = info.currentNode();
                    if (node == null) {
                        node = info.getNode(false, this.origSnapshot);
                    }
                    if (EntrySupportLazy.isDummyNode(node)) {
                        LazySupport.this.hideEmpty(null, entry);
                    }
                    return node;
                }
            }

        }

        private class DefaultSupport
        extends EntrySupportDefault
        implements FilterChildrenSupport {
            EntrySupportDefault origSupport;

            public DefaultSupport(org.openide.nodes.Children ch, EntrySupportDefault origSupport) {
                super(ch);
                this.origSupport = origSupport;
            }

            @Override
            protected EntrySupportDefault.DefaultSnapshot createSnapshot() {
                Object[] newHolder;
                EntrySupportDefault.DefaultSnapshot snapshot = super.createSnapshot();
                snapshot.holder = newHolder = new Object[]{snapshot.holder, this.origSupport.createSnapshot()};
                return snapshot;
            }

            @Override
            public Node[] callGetNodes(boolean optimalResult) {
                Node[] hold = null;
                if (optimalResult) {
                    hold = Children.this.original.getChildren().getNodes(true);
                }
                hold = Children.this.getNodes();
                return hold;
            }

            @Override
            public int callGetNodesCount(boolean optimalResult) {
                int cnt = 0;
                if (optimalResult) {
                    cnt = Children.this.original.getChildren().getNodesCount(true);
                }
                int ret = Children.this.getNodesCount();
                org.openide.nodes.Children.LOG.log(Level.FINEST, "Count {1} gives {2}", new Object[]{cnt, ret});
                return ret;
            }

            @Override
            public Node findChild(String name) {
                Node dontGC = Children.this.original.getChildren().findChild(name);
                return Children.this.findChild(name);
            }

            @Override
            public void filterChildrenAdded(NodeMemberEvent ev) {
                this.updateKeys();
            }

            @Override
            public void filterChildrenRemoved(NodeMemberEvent ev) {
                this.updateKeys();
            }

            @Override
            public void filterChildrenReordered(NodeReorderEvent ev) {
                this.updateKeys();
            }

            @Override
            public void update() {
                this.updateKeys();
            }

            private void updateKeys() {
                ChildrenAdapter cha;
                boolean LOG_ENABLED = FilterNode.LOGGER.isLoggable(Level.FINER);
                if (LOG_ENABLED) {
                    FilterNode.LOGGER.finer("updateKeys() " + this);
                }
                if ((cha = Children.this.nodeL) != null) {
                    if (LOG_ENABLED) {
                        FilterNode.LOGGER.finer("    getting original nodes");
                    }
                    Object[] arr = Children.this.original.getChildren().getNodes();
                    if (LOG_ENABLED) {
                        FilterNode.LOGGER.finer("    setKeys(), keys: " + Arrays.toString(arr));
                    }
                    Children.this.setKeys(arr);
                    if (!this.origSupport.isInitialized()) {
                        this.origSupport.notifySetEntries();
                    }
                }
            }

            @Override
            public Children.Entry createEntryForKey(Node key) {
                return new Children.Keys.KE(Children.this, key);
            }

            @Override
            public EntrySupport originalSupport() {
                return this.origSupport;
            }
        }

    }

    protected static class NodeAdapter
    implements NodeListener {
        private Reference<FilterNode> fnRef;

        public NodeAdapter(FilterNode fn) {
            this.fnRef = new WeakReference<FilterNode>(fn);
        }

        @Override
        public final void propertyChange(PropertyChangeEvent ev) {
            FilterNode fn = this.fnRef.get();
            if (fn == null) {
                return;
            }
            this.propertyChange(fn, ev);
        }

        protected void propertyChange(FilterNode fn, PropertyChangeEvent ev) {
            String n = ev.getPropertyName();
            if (n.equals("parentNode")) {
                return;
            }
            if (n.equals("displayName")) {
                fn.fireOwnPropertyChange("displayName", (String)ev.getOldValue(), (String)ev.getNewValue());
                return;
            }
            if (n.equals("name")) {
                fn.fireOwnPropertyChange("name", (String)ev.getOldValue(), (String)ev.getNewValue());
                return;
            }
            if (n.equals("shortDescription")) {
                fn.fireOwnPropertyChange("shortDescription", (String)ev.getOldValue(), (String)ev.getNewValue());
                return;
            }
            if (n.equals("icon")) {
                fn.fireIconChange();
                return;
            }
            if (n.equals("openedIcon")) {
                fn.fireOpenedIconChange();
                return;
            }
            if (n.equals("propertySets")) {
                fn.firePropertySetsChange((Node.PropertySet[])ev.getOldValue(), (Node.PropertySet[])ev.getNewValue());
                return;
            }
            if (n.equals("cookie")) {
                fn.fireCookieChange();
                return;
            }
            if (n.equals("leaf")) {
                fn.updateChildren();
            }
        }

        @Override
        public void childrenAdded(NodeMemberEvent ev) {
        }

        @Override
        public void childrenRemoved(NodeMemberEvent ev) {
        }

        @Override
        public void childrenReordered(NodeReorderEvent ev) {
        }

        @Override
        public final void nodeDestroyed(NodeEvent ev) {
            FilterNode fn = this.fnRef.get();
            if (fn == null) {
                return;
            }
            fn.originalDestroyed();
        }

        static Set<NodeListener> checkDormant(NodeListener l, Set<NodeListener> in) {
            if (l instanceof NodeAdapter && ((NodeAdapter)l).fnRef.get() == null) {
                HashSet<NodeListener> ret = new HashSet<NodeListener>();
                if (in != null) {
                    ret.addAll(in);
                }
                ret.add(l);
                return ret;
            }
            return in;
        }
    }

    protected static class PropertyChangeAdapter
    implements PropertyChangeListener {
        private Reference<FilterNode> fnRef;

        public PropertyChangeAdapter(FilterNode fn) {
            this.fnRef = new WeakReference<FilterNode>(fn);
        }

        @Override
        public final void propertyChange(PropertyChangeEvent ev) {
            FilterNode fn = this.fnRef.get();
            if (fn == null) {
                return;
            }
            this.propertyChange(fn, ev);
        }

        protected void propertyChange(FilterNode fn, PropertyChangeEvent ev) {
            fn.firePropertyChange(ev.getPropertyName(), ev.getOldValue(), ev.getNewValue());
        }

        static Set<PropertyChangeListener> checkDormant(PropertyChangeListener l, Set<PropertyChangeListener> in) {
            if (l instanceof PropertyChangeAdapter && ((PropertyChangeAdapter)l).fnRef.get() == null) {
                HashSet<PropertyChangeListener> ret = new HashSet<PropertyChangeListener>();
                if (in != null) {
                    ret.addAll(in);
                }
                ret.add(l);
                return ret;
            }
            return in;
        }
    }

    private static class StackError
    extends StackOverflowError {
        private IdentityHashMap<FilterNode, FilterNode> nodes;

        private StackError() {
        }

        public void add(FilterNode n) {
            if (this.nodes == null) {
                this.nodes = new IdentityHashMap();
            }
            if (!this.nodes.containsKey(n)) {
                this.nodes.put(n, n);
            }
        }

        @Override
        public String getMessage() {
            StringBuffer sb = new StringBuffer();
            sb.append("StackOver in FilterNodes:\n");
            for (FilterNode f : this.nodes.keySet()) {
                sb.append("  class: ");
                sb.append(f.getClass().getName());
                sb.append(" id: ");
                sb.append(Integer.toString(System.identityHashCode(f), 16));
                sb.append("\n");
            }
            return sb.toString();
        }
    }

}

