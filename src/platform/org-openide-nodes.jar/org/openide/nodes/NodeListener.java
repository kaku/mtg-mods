/*
 * Decompiled with CFR 0_118.
 */
package org.openide.nodes;

import java.beans.PropertyChangeListener;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeReorderEvent;

public interface NodeListener
extends PropertyChangeListener {
    public void childrenAdded(NodeMemberEvent var1);

    public void childrenRemoved(NodeMemberEvent var1);

    public void childrenReordered(NodeReorderEvent var1);

    public void nodeDestroyed(NodeEvent var1);
}

