/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.datatransfer.NewType
 *  org.openide.util.datatransfer.PasteType
 */
package org.openide.nodes;

import java.awt.Component;
import java.awt.Image;
import java.awt.datatransfer.Transferable;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.MissingResourceException;
import java.util.WeakHashMap;
import javax.swing.Action;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.nodes.Children;
import org.openide.nodes.CookieSet;
import org.openide.nodes.DefaultHandle;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.nodes.NodeTransfer;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.NewType;
import org.openide.util.datatransfer.PasteType;

public class AbstractNode
extends Node {
    private static final String[] icons = new String[]{"", "32", "", "32", "Open", "Open32", "Open", "Open32"};
    private static final int ICON_BASE = -1;
    private static final int OPENED_ICON_BASE = 3;
    private static final PasteType[] NO_PASTE_TYPES = new PasteType[0];
    private static final NewType[] NO_NEW_TYPES = new NewType[0];
    private static final String DEFAULT_ICON_BASE = "org/openide/nodes/defaultNode";
    private static final String DEFAULT_ICON_EXTENSION = ".gif";
    private static final String DEFAULT_ICON = "org/openide/nodes/defaultNode.png";
    private static final WeakHashMap<Class, Object> overridesGetDefaultAction = new WeakHashMap(32);
    protected MessageFormat displayFormat;
    private Action preferredAction;
    private String iconBase = "org/openide/nodes/defaultNode";
    private String iconExtension = ".png";
    private Object lookup;
    private Sheet sheet;
    @Deprecated
    protected SystemAction[] systemActions;
    private SheetAndCookieListener sheetCookieL = null;

    public AbstractNode(Children children) {
        this(children, null);
    }

    public AbstractNode(Children children, Lookup lookup) {
        super(children, lookup);
        super.setName("");
    }

    AbstractNode(CookieSet set) {
        super(Children.LEAF);
        this.lookup = set;
    }

    @Override
    public Node cloneNode() {
        try {
            if (this instanceof Cloneable) {
                return (Node)this.clone();
            }
        }
        catch (CloneNotSupportedException ex) {
            // empty catch block
        }
        return new FilterNode(this);
    }

    @Override
    public void setName(String s) {
        super.setName(s);
        MessageFormat mf = this.displayFormat;
        if (mf != null) {
            this.setDisplayName(mf.format(new Object[]{s}));
        } else {
            this.fireDisplayNameChange(null, null);
        }
    }

    @Deprecated
    public void setIconBase(String base) {
        this.setIconBaseWithExtension(base, ".gif");
    }

    public final void setIconBaseWithExtension(String baseExt) {
        int lastDot = baseExt.lastIndexOf(46);
        int lastSlash = baseExt.lastIndexOf(47);
        if (lastSlash > lastDot || lastDot == -1) {
            this.setIconBaseWithExtension(baseExt, "");
        } else {
            String base = baseExt.substring(0, lastDot);
            String ext = baseExt.substring(lastDot);
            this.setIconBaseWithExtension(base, ext);
        }
    }

    private final void setIconBaseWithExtension(String base, String extension) {
        if (base.equals(this.iconBase) && extension.equals(this.iconExtension)) {
            return;
        }
        this.iconBase = base;
        this.iconExtension = extension;
        this.fireIconChange();
        this.fireOpenedIconChange();
    }

    @Override
    public Image getIcon(int type) {
        return this.findIcon(type, -1);
    }

    @Override
    public Image getOpenedIcon(int type) {
        return this.findIcon(type, 3);
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    private Image findIcon(int type, int ib) {
        String res = this.iconBase + icons[type + ib] + this.iconExtension;
        Image im = ImageUtilities.loadImage((String)res, (boolean)true);
        if (im != null) {
            return im;
        }
        res = this.iconBase + icons[1 + ib] + this.iconExtension;
        im = ImageUtilities.loadImage((String)res, (boolean)true);
        if (im != null) {
            return im;
        }
        if (ib == 3) {
            return this.findIcon(type, -1);
        }
        return this.getDefaultIcon();
    }

    Image getDefaultIcon() {
        Image i = ImageUtilities.loadImage((String)"org/openide/nodes/defaultNode.png", (boolean)true);
        if (i == null) {
            throw new MissingResourceException("No default icon", "", "org/openide/nodes/defaultNode.png");
        }
        return i;
    }

    @Override
    public boolean canRename() {
        return false;
    }

    @Override
    public boolean canDestroy() {
        return false;
    }

    protected final synchronized void setSheet(Sheet s) {
        this.setSheetImpl(s);
        this.firePropertySetsChange(null, null);
    }

    private synchronized void setSheetImpl(Sheet s) {
        if (this.sheetCookieL == null) {
            this.sheetCookieL = new SheetAndCookieListener();
        }
        if (this.sheet != null) {
            this.sheet.removePropertyChangeListener(this.sheetCookieL);
        }
        s.addPropertyChangeListener(this.sheetCookieL);
        this.sheet = s;
    }

    protected Sheet createSheet() {
        return new Sheet();
    }

    protected final synchronized Sheet getSheet() {
        if (this.sheet != null) {
            return this.sheet;
        }
        Sheet s = this.createSheet();
        if (s == null) {
            throw new IllegalStateException("createSheet returns null in " + this.getClass().getName());
        }
        this.setSheetImpl(s);
        return s;
    }

    @Override
    public Node.PropertySet[] getPropertySets() {
        Sheet s = this.getSheet();
        return s.toArray();
    }

    @Override
    boolean propertySetsAreKnown() {
        return this.sheet != null;
    }

    @Override
    public Transferable clipboardCopy() throws IOException {
        return NodeTransfer.transferable(this, 1);
    }

    @Override
    public Transferable clipboardCut() throws IOException {
        return NodeTransfer.transferable(this, 4);
    }

    @Override
    public Transferable drag() throws IOException {
        return this.clipboardCopy();
    }

    @Override
    public boolean canCopy() {
        return true;
    }

    @Override
    public boolean canCut() {
        return false;
    }

    protected void createPasteTypes(Transferable t, List<PasteType> s) {
        NodeTransfer.Paste p = NodeTransfer.findPaste(t);
        if (p != null) {
            s.addAll(Arrays.asList(p.types(this)));
        }
    }

    @Override
    public final PasteType[] getPasteTypes(Transferable t) {
        LinkedList<PasteType> s = new LinkedList<PasteType>();
        this.createPasteTypes(t, s);
        return s.toArray((T[])NO_PASTE_TYPES);
    }

    @Override
    public PasteType getDropType(Transferable t, int action, int index) {
        LinkedList<PasteType> s = new LinkedList<PasteType>();
        this.createPasteTypes(t, s);
        return s.isEmpty() ? null : s.get(0);
    }

    @Override
    public NewType[] getNewTypes() {
        return NO_NEW_TYPES;
    }

    private boolean overridesAMethod(String name, Class[] arguments) {
        try {
            Method m = this.getClass().getMethod(name, arguments);
            if (m.getDeclaringClass() != AbstractNode.class) {
                return true;
            }
        }
        catch (NoSuchMethodException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Action getPreferredAction() {
        boolean delegate = false;
        Class c = this.getClass();
        if (c != AbstractNode.class) {
            WeakHashMap<Class, Object> weakHashMap = overridesGetDefaultAction;
            synchronized (weakHashMap) {
                Boolean b;
                Object in = overridesGetDefaultAction.get(c);
                if (in == this) {
                    overridesGetDefaultAction.put(c, Boolean.FALSE);
                    return this.preferredAction;
                }
                if (in == null) {
                    Boolean bl = b = this.overridesAMethod("getDefaultAction", new Class[0]) ? Boolean.TRUE : Boolean.FALSE;
                    if (b.booleanValue()) {
                        overridesGetDefaultAction.put(c, this);
                        this.getDefaultAction();
                        if (overridesGetDefaultAction.get(c) == this) {
                            overridesGetDefaultAction.put(c, b);
                        }
                    } else {
                        overridesGetDefaultAction.put(c, b);
                    }
                } else {
                    b = (Boolean)in;
                }
                delegate = b;
            }
        }
        return delegate ? this.getDefaultAction() : this.preferredAction;
    }

    @Deprecated
    @Override
    public SystemAction getDefaultAction() {
        Action a = this.getPreferredAction();
        if (a instanceof SystemAction) {
            return (SystemAction)a;
        }
        return null;
    }

    @Deprecated
    public void setDefaultAction(SystemAction action) {
        this.preferredAction = action;
    }

    @Deprecated
    @Override
    public SystemAction[] getActions() {
        if (this.systemActions == null) {
            this.systemActions = this.createActions();
            if (this.systemActions == null) {
                this.systemActions = super.getActions();
            }
        }
        return this.systemActions;
    }

    @Deprecated
    protected SystemAction[] createActions() {
        return null;
    }

    @Override
    public boolean hasCustomizer() {
        return false;
    }

    @Override
    public Component getCustomizer() {
        return null;
    }

    @Deprecated
    protected final synchronized void setCookieSet(CookieSet s) {
        CookieSet cookieSet;
        if (this.internalLookup(false) != null) {
            throw new IllegalStateException("CookieSet cannot be used when lookup is associated with the node");
        }
        if (this.sheetCookieL == null) {
            this.sheetCookieL = new SheetAndCookieListener();
        }
        if ((cookieSet = (CookieSet)this.lookup) != null) {
            cookieSet.removeChangeListener(this.sheetCookieL);
        }
        s.addChangeListener(this.sheetCookieL);
        this.lookup = s;
        this.fireCookieChange();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final CookieSet getCookieSet() {
        if (this.internalLookup(false) != null) {
            throw new IllegalStateException("CookieSet cannot be used when lookup is associated with the node");
        }
        CookieSet s = (CookieSet)this.lookup;
        if (s != null) {
            return s;
        }
        AbstractNode abstractNode = this;
        synchronized (abstractNode) {
            if (this.lookup != null) {
                return (CookieSet)this.lookup;
            }
            this.setCookieSet(new CookieSet());
            return (CookieSet)this.lookup;
        }
    }

    @Override
    public <T extends Node.Cookie> T getCookie(Class<T> type) {
        if (this.lookup instanceof CookieSet) {
            CookieSet c = (CookieSet)this.lookup;
            return c.getCookie(type);
        }
        return super.getCookie(type);
    }

    @Override
    public Node.Handle getHandle() {
        return DefaultHandle.createHandle(this);
    }

    private final class SheetAndCookieListener
    implements PropertyChangeListener,
    ChangeListener {
        SheetAndCookieListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent ev) {
            AbstractNode.this.firePropertySetsChange(null, null);
        }

        @Override
        public void stateChanged(ChangeEvent ev) {
            AbstractNode.this.fireCookieChange();
        }
    }

}

