/*
 * Decompiled with CFR 0_118.
 */
package org.openide.nodes;

import java.util.List;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeEvent;

public final class NodeReorderEvent
extends NodeEvent {
    static final long serialVersionUID = 4479234495493767448L;
    private int[] newIndices;
    private final List<Node> currSnapshot;

    NodeReorderEvent(Node n, int[] newIndices) {
        super(n);
        this.newIndices = newIndices;
        this.currSnapshot = n.getChildren().snapshot();
    }

    public final List<Node> getSnapshot() {
        return this.currSnapshot;
    }

    public int newIndexOf(int i) {
        return this.newIndices[i];
    }

    public int[] getPermutation() {
        return this.newIndices;
    }

    public int getPermutationSize() {
        return this.newIndices.length;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.getClass().getName());
        sb.append("[node=");
        sb.append(this.getSource());
        sb.append(", permutation = (");
        int[] perm = this.getPermutation();
        int i = 0;
        while (i < perm.length) {
            sb.append(perm[i]);
            if (++i >= perm.length) continue;
            sb.append(", ");
        }
        sb.append(")]");
        return sb.toString();
    }
}

