/*
 * Decompiled with CFR 0_118.
 */
package org.openide.nodes;

import java.beans.IntrospectionException;
import java.beans.beancontext.BeanContext;
import java.beans.beancontext.BeanContextMembershipEvent;
import java.beans.beancontext.BeanContextMembershipListener;
import java.beans.beancontext.BeanContextSupport;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.nodes.BeanNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeAdapter;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;

public class BeanChildren
extends Children.Keys {
    private static final Factory DEFAULT_FACTORY = new BeanFactory();
    private static final Map<Node, Reference[]> nodes2Beans = new WeakHashMap<Node, Reference[]>();
    private BeanContext bean;
    private Factory factory;
    private ContextL contextL;

    public BeanChildren(BeanContext bean) {
        this(bean, DEFAULT_FACTORY);
    }

    public BeanChildren(BeanContext bean, Factory factory) {
        this.bean = bean;
        this.factory = factory;
    }

    final void updateKeys() {
        this.setKeys(this.bean.toArray());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected Node[] createNodes(Object subbean) {
        try {
            BeanContextSupport bcs;
            if (subbean instanceof BeanContextSupport && this.bean.contains((bcs = (BeanContextSupport)subbean).getBeanContextPeer()) && bcs != bcs.getBeanContextPeer()) {
                return new Node[0];
            }
            Node n = this.factory.createNode(subbean);
            Map<Node, Reference[]> map = nodes2Beans;
            synchronized (map) {
                nodes2Beans.put(n, new Reference[]{new WeakReference<BeanContext>(this.bean), new WeakReference<Object>(subbean)});
            }
            n.addNodeListener(this.contextL);
            return new Node[]{n};
        }
        catch (IntrospectionException ex) {
            Logger.getLogger(BeanChildren.class.getName()).log(Level.WARNING, null, ex);
            return new Node[0];
        }
    }

    @Override
    protected void addNotify() {
        this.contextL = new ContextL(this);
        this.bean.addBeanContextMembershipListener(this.contextL);
        this.updateKeys();
    }

    @Override
    protected void removeNotify() {
        if (this.contextL != null) {
            this.bean.removeBeanContextMembershipListener(this.contextL);
        }
        this.contextL = null;
        this.setKeys(Collections.emptySet());
    }

    private static final class ContextL
    extends NodeAdapter
    implements BeanContextMembershipListener {
        private WeakReference<BeanChildren> ref;

        ContextL() {
        }

        ContextL(BeanChildren bc) {
            this.ref = new WeakReference<BeanChildren>(bc);
        }

        @Override
        public void childrenAdded(BeanContextMembershipEvent bcme) {
            BeanChildren bc = this.ref.get();
            if (bc != null) {
                bc.updateKeys();
            }
        }

        @Override
        public void childrenRemoved(BeanContextMembershipEvent bcme) {
            BeanChildren bc = this.ref.get();
            if (bc != null) {
                bc.updateKeys();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void nodeDestroyed(NodeEvent ev) {
            Object subbean;
            BeanContext bean;
            Reference[] refs;
            Node n = ev.getNode();
            Map map = nodes2Beans;
            synchronized (map) {
                refs = (Reference[])nodes2Beans.get(n);
            }
            if (refs != null && (bean = (BeanContext)refs[0].get()) != null && (subbean = refs[1].get()) != null) {
                try {
                    bean.remove(subbean);
                }
                catch (RuntimeException re) {
                    Logger.getLogger(BeanChildren.class.getName()).log(Level.WARNING, null, re);
                }
            }
        }
    }

    private static class BeanFactory
    implements Factory {
        BeanFactory() {
        }

        @Override
        public Node createNode(Object bean) throws IntrospectionException {
            return new BeanNode<Object>(bean);
        }
    }

    public static interface Factory {
        public Node createNode(Object var1) throws IntrospectionException;
    }

}

