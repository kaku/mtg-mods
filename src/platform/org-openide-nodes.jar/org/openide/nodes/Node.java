/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.HelpCtx$Provider
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Privileged
 *  org.openide.util.NbBundle
 *  org.openide.util.Parameters
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.datatransfer.NewType
 *  org.openide.util.datatransfer.PasteType
 */
package org.openide.nodes;

import java.awt.Component;
import java.awt.Image;
import java.awt.datatransfer.Transferable;
import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.beans.PropertyEditorManager;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JPopupMenu;
import javax.swing.event.EventListenerList;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.ChildrenArray;
import org.openide.nodes.CookieSet;
import org.openide.nodes.FilterNode;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeLookup;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeOp;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.Parameters;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.NewType;
import org.openide.util.datatransfer.PasteType;

public abstract class Node
extends FeatureDescriptor
implements Lookup.Provider,
HelpCtx.Provider {
    public static final Node EMPTY = new AbstractNode(Children.LEAF);
    public static final String PROP_DISPLAY_NAME = "displayName";
    public static final String PROP_NAME = "name";
    public static final String PROP_SHORT_DESCRIPTION = "shortDescription";
    public static final String PROP_ICON = "icon";
    public static final String PROP_OPENED_ICON = "openedIcon";
    public static final String PROP_PARENT_NODE = "parentNode";
    public static final String PROP_PROPERTY_SETS = "propertySets";
    public static final String PROP_COOKIE = "cookie";
    public static final String PROP_LEAF = "leaf";
    private static final Logger err = Logger.getLogger("org.openide.nodes.Node");
    private static Map<EventListenerList, Reference<Lookup>> lookups = new WeakHashMap<EventListenerList, Reference<Lookup>>(37);
    private static final Set<String> warnedBadProperties = new HashSet<String>(100);
    private static final Lookup.Template<Cookie> TEMPL_COOKIE = new Lookup.Template(Cookie.class);
    private static final Object INIT_LOCK = new Object();
    private static final Object LOCK = new Object();
    private Object parent;
    Children hierarchy;
    private transient EventListenerList listeners;
    private static final ThreadLocal<Set<Node>> BLOCK_EVENTS = new ThreadLocal<T>();

    protected Node(Children h) throws IllegalStateException {
        this(h, null);
    }

    protected Node(Children h, Lookup lookup) throws IllegalStateException {
        this.hierarchy = h;
        lookup = this.replaceProvidedLookup(lookup);
        this.listeners = lookup != null ? new LookupEventList(lookup) : new EventListenerList();
        h.attachTo(this);
    }

    Lookup replaceProvidedLookup(Lookup l) {
        return l;
    }

    final Lookup internalLookup(boolean init) {
        if (this.listeners instanceof LookupEventList) {
            return ((LookupEventList)this.listeners).init(init);
        }
        return null;
    }

    protected Object clone() throws CloneNotSupportedException {
        Node n = (Node)Object.super.clone();
        Children hier2 = this.hierarchy instanceof Cloneable ? (Children)this.hierarchy.cloneHierarchy() : Children.LEAF;
        n.hierarchy = hier2;
        hier2.attachTo(n);
        n.parent = null;
        n.listeners = this.listeners instanceof LookupEventList ? new LookupEventList(this.internalLookup(false)) : new EventListenerList();
        return n;
    }

    public abstract Node cloneNode();

    private Children getParentChildren() {
        Object p = this.parent;
        return p instanceof ChildrenArray ? ((ChildrenArray)p).getChildren() : (Children)p;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void assignTo(Children parent, int index) {
        Object object = LOCK;
        synchronized (object) {
            Children ch = this.getParentChildren();
            if (ch != null && ch != parent) {
                String parentNodes = null;
                String chNodes = null;
                Throwable t = null;
                try {
                    parentNodes = Arrays.toString(parent.getNodes());
                    chNodes = Arrays.toString(ch.getNodes());
                }
                catch (StackOverflowError e) {
                    t = e;
                    StackTraceElement[] from = t.getStackTrace();
                    StackTraceElement[] arr = new StackTraceElement[java.lang.Math.min(50, from.length)];
                    System.arraycopy(from, 0, arr, 0, arr.length);
                    t.setStackTrace(arr);
                }
                catch (RuntimeException e) {
                    t = e;
                }
                IllegalStateException ex = new IllegalStateException("Cannot initialize " + index + "th child of node " + parent.getNode() + "; it already belongs to node " + ch.getNode() + " (did you forgot to use cloneNode?)\nChildren of new node: " + parentNodes + "\nChildren of old node: " + chNodes);
                if (t != null) {
                    ex.initCause(t);
                }
                throw ex;
            }
            if (!(this.parent instanceof ChildrenArray)) {
                this.parent = parent;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void reassignTo(Children currentParent, ChildrenArray itsArray) {
        Object object = LOCK;
        synchronized (object) {
            if (this.parent != currentParent && this.parent != itsArray) {
                throw new IllegalStateException("Unauthorized call to change parent: " + this.parent + " and should be: " + currentParent);
            }
            this.parent = itsArray;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void deassignFrom(Children parent) {
        Object object = LOCK;
        synchronized (object) {
            Children p = this.getParentChildren();
            if (parent != p) {
                throw new IllegalArgumentException("Deassign from wrong parent. Old: " + p + " Caller: " + parent);
            }
            this.parent = null;
        }
    }

    @Override
    public void setName(String s) {
        String name = super.getName();
        if (name == null || !name.equals(s)) {
            super.setName(s);
            this.fireNameChange(name, s);
        }
    }

    @Override
    public void setDisplayName(String s) {
        String displayName = super.getDisplayName();
        if (displayName == null || !displayName.equals(s)) {
            super.setDisplayName(s);
            this.fireDisplayNameChange(displayName, s);
        }
    }

    @Override
    public void setShortDescription(String s) {
        String descr = super.getShortDescription();
        if (descr == null || !descr.equals(s)) {
            super.setShortDescription(s);
            this.fireShortDescriptionChange(descr, s);
        }
    }

    @Deprecated
    @Override
    public void setHidden(boolean hidden) {
        super.setHidden(hidden);
    }

    public abstract Image getIcon(int var1);

    public abstract Image getOpenedIcon(int var1);

    public abstract HelpCtx getHelpCtx();

    public final Children getChildren() {
        this.updateChildren();
        return this.hierarchy;
    }

    void updateChildren() {
        Children ch = this.hierarchy;
        if (ch instanceof Children.LazyChildren) {
            ch = ((Children.LazyChildren)ch).getOriginal();
            this.setChildren(ch);
        }
    }

    protected final void setChildren(final Children ch) {
        Parameters.notNull((CharSequence)"ch", (Object)ch);
        Children.MUTEX.postWriteRequest(new Runnable(){

            @Override
            public void run() {
                boolean wasLeaf;
                List<Node> prevSnapshot = null;
                boolean wasInited = Node.this.hierarchy.isInitialized();
                boolean bl = wasLeaf = Node.this.hierarchy == Children.LEAF;
                if (wasInited && !wasLeaf) {
                    prevSnapshot = Node.this.hierarchy.snapshot();
                }
                Node.this.hierarchy.detachFrom();
                if (prevSnapshot != null && prevSnapshot.size() > 0) {
                    Node.this.hierarchy = Children.LEAF;
                    int[] idxs = Children.getSnapshotIdxs(prevSnapshot);
                    Node.this.fireSubNodesChangeIdx(false, idxs, null, Collections.<Node>emptyList(), prevSnapshot);
                }
                Node.this.hierarchy = ch;
                Node.this.hierarchy.attachTo(Node.this);
                if (wasInited && Node.this.hierarchy != Children.LEAF) {
                    Node.this.hierarchy.getNodesCount();
                    List<Node> snapshot = Node.this.hierarchy.snapshot();
                    if (snapshot.size() > 0) {
                        int[] idxs = Children.getSnapshotIdxs(snapshot);
                        Node.this.fireSubNodesChangeIdx(true, idxs, null, snapshot, null);
                    }
                }
                if (wasLeaf != (Node.this.hierarchy == Children.LEAF)) {
                    Node.this.fireOwnPropertyChange("leaf", wasLeaf, Node.this.hierarchy == Children.LEAF);
                }
            }
        });
    }

    public final boolean isLeaf() {
        this.updateChildren();
        return this.hierarchy == Children.LEAF;
    }

    public final Node getParentNode() {
        Children ch = this.getParentChildren();
        return ch == null ? null : ch.getNode();
    }

    public abstract boolean canRename();

    public abstract boolean canDestroy();

    public void destroy() throws IOException {
        Children.MUTEX.postWriteRequest(new Runnable(){

            @Override
            public void run() {
                Children p = Node.this.getParentChildren();
                if (p != null) {
                    p.remove(new Node[]{Node.this});
                }
                Node.this.fireNodeDestroyed();
            }
        });
    }

    public abstract PropertySet[] getPropertySets();

    public abstract Transferable clipboardCopy() throws IOException;

    public abstract Transferable clipboardCut() throws IOException;

    public abstract Transferable drag() throws IOException;

    public abstract boolean canCopy();

    public abstract boolean canCut();

    public abstract PasteType[] getPasteTypes(Transferable var1);

    public abstract PasteType getDropType(Transferable var1, int var2, int var3);

    public abstract NewType[] getNewTypes();

    public Action[] getActions(boolean context) {
        return context ? this.getContextActions() : this.getActions();
    }

    @Deprecated
    public SystemAction[] getActions() {
        return NodeOp.getDefaultActions();
    }

    @Deprecated
    public SystemAction[] getContextActions() {
        return this.getActions();
    }

    @Deprecated
    public SystemAction getDefaultAction() {
        return null;
    }

    public Action getPreferredAction() {
        return this.getDefaultAction();
    }

    public final JPopupMenu getContextMenu() {
        return NodeOp.findContextMenu(new Node[]{this});
    }

    public abstract boolean hasCustomizer();

    public abstract Component getCustomizer();

    public <T extends Cookie> T getCookie(Class<T> type) {
        Lookup l = this.internalLookup(true);
        if (l != null) {
            Object obj = l.lookup(type);
            if (Cookie.class.isInstance(obj)) {
                return (T)((Cookie)type.cast(obj));
            }
            CookieSet.enhancedQueryMode(l, type);
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final Lookup getLookup() {
        EventListenerList eventListenerList = this.listeners;
        synchronized (eventListenerList) {
            Lookup l = this.internalLookup(true);
            if (l != null) {
                return l;
            }
            l = this.findDelegatingLookup();
            if (l != null) {
                return l;
            }
            NodeLookup nl = new NodeLookup(this);
            this.registerDelegatingLookup(nl);
            return nl;
        }
    }

    public String getHtmlDisplayName() {
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void registerDelegatingLookup(NodeLookup l) {
        Map<EventListenerList, Reference<Lookup>> map = lookups;
        synchronized (map) {
            lookups.put(this.listeners, new WeakReference<NodeLookup>(l));
        }
    }

    final Lookup findDelegatingLookup() {
        Reference<Lookup> ref = lookups.get(this.listeners);
        return ref == null ? null : ref.get();
    }

    public abstract Handle getHandle();

    public final void addNodeListener(NodeListener l) {
        this.listeners.add(NodeListener.class, l);
        this.listenerAdded();
    }

    void listenerAdded() {
    }

    final int getNodeListenerCount() {
        return this.listeners.getListenerCount(NodeListener.class);
    }

    public final void removeNodeListener(NodeListener l) {
        this.listeners.remove(NodeListener.class, l);
    }

    public final void addPropertyChangeListener(PropertyChangeListener l) {
        int count = -1;
        if (err.isLoggable(Level.FINE)) {
            count = this.getPropertyChangeListenersCount();
        }
        this.listeners.add(PropertyChangeListener.class, l);
        if (err.isLoggable(Level.FINE)) {
            err.log(Level.FINE, "ADD - " + this.getName() + " [" + count + "]->[" + this.getPropertyChangeListenersCount() + "] " + l);
        }
        this.notifyPropertyChangeListenerAdded(l);
    }

    void notifyPropertyChangeListenerAdded(PropertyChangeListener l) {
    }

    int getPropertyChangeListenersCount() {
        return this.listeners.getListenerCount(PropertyChangeListener.class);
    }

    protected final boolean hasPropertyChangeListener() {
        return this.getPropertyChangeListenersCount() > 0;
    }

    public final void removePropertyChangeListener(PropertyChangeListener l) {
        int count = -1;
        if (err.isLoggable(Level.FINE)) {
            count = this.getPropertyChangeListenersCount();
        }
        this.listeners.remove(PropertyChangeListener.class, l);
        if (err.isLoggable(Level.FINE)) {
            err.log(Level.FINE, "RMV - " + this.getName() + " [" + count + "]->[" + this.getPropertyChangeListenersCount() + "] " + l);
        }
        this.notifyPropertyChangeListenerRemoved(l);
    }

    void notifyPropertyChangeListenerRemoved(PropertyChangeListener l) {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final void firePropertyChange(String name, Object o, Object n) {
        if (err.isLoggable(Level.WARNING) && name != null && this.propertySetsAreKnown()) {
            PropertySet[] pss = this.getPropertySets();
            boolean exists = false;
            block3 : for (int i2 = 0; i2 < pss.length; ++i2) {
                Property<?>[] ps = pss[i2].getProperties();
                for (int j = 0; j < ps.length; ++j) {
                    if (!ps[j].getName().equals(name)) continue;
                    exists = true;
                    continue block3;
                }
            }
            if (!exists) {
                Set<String> i2 = warnedBadProperties;
                synchronized (i2) {
                    String clazz = this.getClass().getName();
                    if (warnedBadProperties.add(clazz + "." + name)) {
                        StringWriter w = new StringWriter();
                        IllegalStateException ise = new IllegalStateException("Warning - the node \"" + this.getDisplayName() + "\" [" + clazz + "] is trying to fire the property " + name + " which is not included in its property sets. This is illegal. See IZ #31413 for details.");
                        ise.printStackTrace(new PrintWriter(w));
                        Logger.getLogger(Node.class.getName()).warning(w.toString());
                    }
                }
            }
        }
        if (o != null && n != null && (o == n || o.equals(n))) {
            return;
        }
        PropertyChangeEvent ev = null;
        Object[] listeners = this.listeners.getListenerList();
        Set<PropertyChangeListener> dormant = Collections.emptySet();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] != PropertyChangeListener.class) continue;
            if (ev == null) {
                ev = new PropertyChangeEvent(this, name, o, n);
            }
            PropertyChangeListener l = (PropertyChangeListener)listeners[i + 1];
            l.propertyChange(ev);
            dormant = FilterNode.PropertyChangeAdapter.checkDormant(l, dormant);
        }
        this.removeDormant(dormant, PropertyChangeListener.class);
    }

    boolean propertySetsAreKnown() {
        return false;
    }

    protected final void fireNameChange(String o, String n) {
        this.fireOwnPropertyChange("name", o, n);
    }

    protected final void fireDisplayNameChange(String o, String n) {
        this.fireOwnPropertyChange("displayName", o, n);
    }

    protected final void fireShortDescriptionChange(String o, String n) {
        this.fireOwnPropertyChange("shortDescription", o, n);
    }

    protected final void fireIconChange() {
        this.fireOwnPropertyChange("icon", null, null);
    }

    protected final void fireOpenedIconChange() {
        this.fireOwnPropertyChange("openedIcon", null, null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void fireSubNodesChange(boolean addAction, Node[] delta, Node[] from) {
        Set<NodeListener> dormant;
        dormant = Collections.emptySet();
        try {
            Children.PR.enterReadAccess();
            if (err.isLoggable(Level.FINER)) {
                err.finer("fireSubNodesChange() " + this);
                err.finer("    added: " + addAction);
                err.finer("    delta: " + Arrays.toString(delta));
                err.finer("    from: " + Arrays.toString(from));
            }
            NodeMemberEvent ev = null;
            Object[] listeners = this.listeners.getListenerList();
            for (int i = listeners.length - 2; i >= 0; i -= 2) {
                if (listeners[i] != NodeListener.class) continue;
                if (ev == null) {
                    ev = new NodeMemberEvent(this, addAction, delta, from);
                }
                NodeListener l = (NodeListener)listeners[i + 1];
                if (addAction) {
                    l.childrenAdded(ev);
                } else {
                    l.childrenRemoved(ev);
                }
                dormant = FilterNode.NodeAdapter.checkDormant(l, dormant);
            }
        }
        finally {
            Children.PR.exitReadAccess();
        }
        this.removeDormant(dormant, NodeListener.class);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void fireSubNodesChangeIdx(boolean added, int[] idxs, Children.Entry sourceEntry, List<Node> current, List<Node> previous) {
        Set<NodeListener> dormant;
        dormant = Collections.emptySet();
        try {
            Children.PR.enterReadAccess();
            if (err.isLoggable(Level.FINER)) {
                err.finer("fireSubNodesChangeIdx() " + this);
                err.finer("    added: " + added);
                err.finer("    idxs: " + Arrays.toString(idxs));
                err.finer("    sourceEntry: " + sourceEntry);
                err.finer("    current size: " + current.size() + "    current: " + current);
                err.finer(previous != null ? "    previous size: " + previous.size() + "    previous: " + previous : "    null");
            }
            NodeMemberEvent ev = null;
            Object[] tmpListeners = this.listeners.getListenerList();
            for (int i = tmpListeners.length - 2; i >= 0; i -= 2) {
                if (tmpListeners[i] != NodeListener.class) continue;
                if (ev == null) {
                    ev = new NodeMemberEvent(this, added, idxs, current, previous);
                    ev.sourceEntry = sourceEntry;
                }
                NodeListener l = (NodeListener)tmpListeners[i + 1];
                if (added) {
                    l.childrenAdded(ev);
                } else {
                    l.childrenRemoved(ev);
                }
                dormant = FilterNode.NodeAdapter.checkDormant(l, dormant);
            }
        }
        finally {
            Children.PR.exitReadAccess();
        }
        this.removeDormant(dormant, NodeListener.class);
    }

    final void fireReorderChange(int[] indices) {
        Set<NodeListener> dormant = Collections.emptySet();
        NodeReorderEvent ev = null;
        Object[] listeners = this.listeners.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] != NodeListener.class) continue;
            if (ev == null) {
                ev = new NodeReorderEvent(this, indices);
            }
            NodeListener l = (NodeListener)listeners[i + 1];
            l.childrenReordered(ev);
            dormant = FilterNode.NodeAdapter.checkDormant(l, dormant);
        }
        this.removeDormant(dormant, NodeListener.class);
    }

    protected final void fireNodeDestroyed() {
        Set<NodeListener> dormant = Collections.emptySet();
        NodeEvent ev = null;
        Object[] listeners = this.listeners.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] != NodeListener.class) continue;
            if (ev == null) {
                ev = new NodeEvent(this);
            }
            NodeListener l = (NodeListener)listeners[i + 1];
            l.nodeDestroyed(ev);
            dormant = FilterNode.NodeAdapter.checkDormant(l, dormant);
        }
        this.removeDormant(dormant, NodeListener.class);
    }

    final void fireParentNodeChange(Node o, Node n) {
        this.fireOwnPropertyChange("parentNode", o, n);
    }

    protected final void firePropertySetsChange(PropertySet[] o, PropertySet[] n) {
        this.fireOwnPropertyChange("propertySets", o, n);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final void fireCookieChange() {
        Lookup l = this.findDelegatingLookup();
        if (l instanceof NodeLookup && Node.updateNow(this)) {
            Set<Node> prev = Node.blockEvents();
            try {
                ((NodeLookup)l).updateLookupAsCookiesAreChanged(null);
            }
            finally {
                Node.unblockEvents(prev);
            }
        }
        this.fireOwnPropertyChange("cookie", null, null);
    }

    static Set<Node> blockEvents() {
        Set<Node> prev = BLOCK_EVENTS.get();
        if (prev != null) {
            return prev;
        }
        BLOCK_EVENTS.set(new HashSet<E>());
        return null;
    }

    private static boolean updateNow(Node n) {
        Set<Node> set = BLOCK_EVENTS.get();
        if (set == null) {
            return true;
        }
        set.add(n);
        return false;
    }

    static void unblockEvents(Set<Node> prev) {
        Set<Node> set = BLOCK_EVENTS.get();
        if (prev == null) {
            while (!set.isEmpty()) {
                Node[] arr;
                for (Node n : arr = set.toArray(new Node[set.size()])) {
                    NodeLookup l = (NodeLookup)n.findDelegatingLookup();
                    l.updateLookupAsCookiesAreChanged(null);
                }
                set.removeAll(Arrays.asList(arr));
            }
        }
        BLOCK_EVENTS.set(prev);
    }

    final void fireOwnPropertyChange(String name, Object o, Object n) {
        if (o != null && n != null && (o == n || o.equals(n))) {
            return;
        }
        Set<NodeListener> dormant = Collections.emptySet();
        PropertyChangeEvent ev = null;
        Object[] listeners = this.listeners.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] != NodeListener.class) continue;
            if (ev == null) {
                ev = new PropertyChangeEvent(this, name, o, n);
            }
            NodeListener l = (NodeListener)listeners[i + 1];
            l.propertyChange(ev);
            dormant = FilterNode.NodeAdapter.checkDormant(l, dormant);
        }
        this.removeDormant(dormant, NodeListener.class);
    }

    public boolean equals(Object obj) {
        if (obj instanceof FilterNode) {
            return ((FilterNode)obj).equals(this);
        }
        return this == obj;
    }

    public int hashCode() {
        return Object.super.hashCode();
    }

    static String getString(String resName) {
        return NbBundle.getBundle(Node.class).getString(resName);
    }

    @Override
    public String toString() {
        return super.toString() + "[Name=" + this.getName() + ", displayName=" + this.getDisplayName() + "]";
    }

    private <T extends PropertyChangeListener> void removeDormant(Set<T> dormant, Class<T> c) {
        for (PropertyChangeListener l : dormant) {
            this.listeners.remove(c, l);
        }
    }

    private static final class PropertyEditorRef
    extends SoftReference<PropertyEditor> {
        private final Thread createdBy = Thread.currentThread();

        public PropertyEditorRef(PropertyEditor referent) {
            super(referent);
        }

        @Override
        public PropertyEditor get() {
            if (Thread.currentThread() != this.createdBy) {
                return null;
            }
            return (PropertyEditor)SoftReference.super.get();
        }
    }

    private final class LookupEventList
    extends EventListenerList
    implements LookupListener {
        public final Lookup lookup;
        private Lookup.Result<Cookie> result;

        public LookupEventList(Lookup l) {
            this.lookup = l;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Lookup init(boolean init) {
            boolean doInit = false;
            Object object = INIT_LOCK;
            synchronized (object) {
                if (init && this.result == null) {
                    this.result = this.lookup.lookup(TEMPL_COOKIE);
                    assert (this.result != null);
                    this.result.addLookupListener((LookupListener)this);
                    doInit = true;
                }
            }
            if (doInit) {
                this.result.allItems();
            }
            return this.lookup;
        }

        public void resultChanged(LookupEvent ev) {
            FilterNode f;
            if (Node.this instanceof FilterNode && (f = (FilterNode)Node.this).getOriginal() == NodeLookup.NO_COOKIE_CHANGE.get()) {
                return;
            }
            Node.this.fireCookieChange();
        }
    }

    public static abstract class IndexedProperty<T, E>
    extends Property<T> {
        private Class<E> elementType;

        public IndexedProperty(Class<T> valueType, Class<E> elementType) {
            super(valueType);
            this.elementType = elementType;
        }

        public abstract boolean canIndexedRead();

        public Class<E> getElementType() {
            return this.elementType;
        }

        public abstract E getIndexedValue(int var1) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException;

        public abstract boolean canIndexedWrite();

        public abstract void setIndexedValue(int var1, E var2) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException;

        public PropertyEditor getIndexedPropertyEditor() {
            return PropertyEditorManager.findEditor(this.elementType);
        }

        @Override
        public boolean equals(Object property) {
            try {
                if (!Property.super.equals(property)) {
                    return false;
                }
                Class<E> propElementType = ((IndexedProperty)property).getElementType();
                Class<E> elementType = this.getElementType();
                if (propElementType == null && elementType != null || propElementType != null && elementType == null) {
                    return false;
                }
                return propElementType == null && elementType == null || propElementType.equals(elementType);
            }
            catch (ClassCastException e) {
                return false;
            }
        }

        @Override
        public int hashCode() {
            Class<E> ementType = this.getElementType();
            return Property.super.hashCode() * (this.elementType == null ? 1 : this.elementType.hashCode());
        }
    }

    public static abstract class Property<T>
    extends FeatureDescriptor {
        private static final Set<String> warnedNames = new HashSet<String>();
        private Class<T> type;
        private PropertyEditorRef edRef = null;

        public Property(Class<T> valueType) {
            this.type = valueType;
            super.setName("");
        }

        public Class<T> getValueType() {
            return this.type;
        }

        public abstract boolean canRead();

        public abstract T getValue() throws IllegalAccessException, InvocationTargetException;

        public abstract boolean canWrite();

        public abstract void setValue(T var1) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException;

        public boolean supportsDefaultValue() {
            return false;
        }

        public void restoreDefaultValue() throws IllegalAccessException, InvocationTargetException {
        }

        public boolean isDefaultValue() {
            String name = this.getClass().getName();
            if (this.supportsDefaultValue() && warnedNames.add(name)) {
                Logger.getLogger(Property.class.getName()).log(Level.WARNING, "Class " + name + " must override isDefaultValue() since it " + "overrides supportsDefaultValue() to be true");
            }
            return true;
        }

        public PropertyEditor getPropertyEditor() {
            if (this.type == null) {
                return null;
            }
            PropertyEditor result = null;
            if (this.edRef != null) {
                result = this.edRef.get();
            }
            if (result == null) {
                result = PropertyEditorManager.findEditor(this.type);
                if (result != null && (result.getClass().getName().equals("sun.beans.editors.EnumEditor") || result.getClass().getName().equals("com.sun.beans.editors.EnumEditor"))) {
                    result = null;
                }
                this.edRef = new PropertyEditorRef(result);
            }
            return result;
        }

        public boolean equals(Object property) {
            if (!(property instanceof Property)) {
                return false;
            }
            Class<T> propValueType = ((Property)property).getValueType();
            Class<T> valueType = this.getValueType();
            if (propValueType == null && valueType != null || propValueType != null && valueType == null) {
                return false;
            }
            return ((Property)property).getName().equals(this.getName()) && (propValueType == null && valueType == null || propValueType.equals(valueType));
        }

        public int hashCode() {
            Class<T> valueType;
            return this.getName().hashCode() * ((valueType = this.getValueType()) == null ? 1 : valueType.hashCode());
        }

        public String getHtmlDisplayName() {
            return null;
        }
    }

    public static abstract class PropertySet
    extends FeatureDescriptor {
        public PropertySet() {
        }

        public PropertySet(String name, String displayName, String shortDescription) {
            super.setName(name);
            super.setDisplayName(displayName);
            super.setShortDescription(shortDescription);
        }

        public abstract Property<?>[] getProperties();

        public boolean equals(Object propertySet) {
            if (!(propertySet instanceof PropertySet)) {
                return false;
            }
            String n1 = ((PropertySet)propertySet).getName();
            if (n1 == null) {
                return this.getName() == null;
            }
            return n1.equals(this.getName());
        }

        public int hashCode() {
            String n = this.getName();
            return n == null ? 0 : n.hashCode();
        }

        public String getHtmlDisplayName() {
            return null;
        }
    }

    public static interface Handle
    extends Serializable {
        @Deprecated
        public static final long serialVersionUID = -4518262478987434353L;

        public Node getNode() throws IOException;
    }

    public static interface Cookie {
    }

}

