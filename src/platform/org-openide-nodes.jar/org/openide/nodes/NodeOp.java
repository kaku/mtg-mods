/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Enumerations
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.ProxyLookup
 */
package org.openide.nodes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JPopupMenu;
import org.netbeans.modules.openide.nodes.NodesRegistrationSupport;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.LazyNode;
import org.openide.nodes.Node;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeNotFoundException;
import org.openide.util.Enumerations;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.ProxyLookup;

public final class NodeOp {
    private static final Logger LOG = Logger.getLogger(NodeOp.class.getName());
    private static SystemAction[] defaultActions;

    private NodeOp() {
    }

    @Deprecated
    public static SystemAction[] getDefaultActions() {
        if (defaultActions == null) {
            defaultActions = NodeOp.createFromNames(new String[]{"Tools", "Properties"});
        }
        return defaultActions;
    }

    @Deprecated
    public static void setDefaultActions(SystemAction[] def) {
        throw new SecurityException();
    }

    public static JPopupMenu findContextMenu(Node[] nodes) {
        Action[] arr = NodeOp.findActions(nodes);
        ArrayList<Lookup> allLookups = new ArrayList<Lookup>();
        for (Node n : nodes) {
            allLookups.add(n.getLookup());
        }
        ProxyLookup lookup = new ProxyLookup(allLookups.toArray((T[])new Lookup[allLookups.size()]));
        return Utilities.actionsToPopup((Action[])arr, (Lookup)lookup);
    }

    public static Action[] findActions(Node[] nodes) {
        Integer cntInt;
        HashSet<Action> counted;
        HashMap<Action, Integer> actions = new HashMap<Action, Integer>();
        Action[][] actionsByNode = new Action[nodes.length][];
        for (int n = 0; n < nodes.length; ++n) {
            actionsByNode[n] = nodes[n].getActions(false);
            if (actionsByNode[n] == null) {
                actionsByNode[n] = NodeOp.getDefaultActions();
            }
            counted = new HashSet();
            for (Action a : actionsByNode[n]) {
                if (a == null || counted.contains(a)) continue;
                counted.add(a);
                cntInt = (Integer)actions.get(a);
                actions.put(a, cntInt == null ? 1 : cntInt + 1);
            }
        }
        if (!actions.isEmpty()) {
            ArrayList<Action> result = new ArrayList<Action>();
            counted = new HashSet<Action>();
            for (Action action : actionsByNode[0]) {
                if (action != null) {
                    int cnt;
                    if (counted.contains(action)) continue;
                    counted.add(action);
                    cntInt = (Integer)actions.get(action);
                    int n2 = cnt = cntInt == null ? 0 : cntInt;
                    if (cnt != nodes.length) continue;
                    result.add(action);
                    continue;
                }
                result.add(null);
            }
            return result.toArray(new Action[result.size()]);
        }
        return new Action[0];
    }

    public static boolean isSon(Node parent, Node son) {
        return son.getParentNode() == parent;
    }

    public static String[] createPath(Node node, Node parent) {
        LinkedList<String> ar = new LinkedList<String>();
        while (node != null && node != parent) {
            if (node.getName() == null) {
                boolean isFilter = false;
                if (node instanceof FilterNode) {
                    isFilter = true;
                }
                throw new IllegalArgumentException("Node:" + node.getClass() + "[" + node.getDisplayName() + "]" + (isFilter ? new StringBuilder().append(" of original:").append(((FilterNode)node).getOriginal().getClass()).toString() : "") + " gets null name!");
            }
            ar.addFirst(node.getName());
            node = node.getParentNode();
        }
        String[] res = new String[ar.size()];
        ar.toArray(res);
        return res;
    }

    public static Node findChild(Node node, String name) {
        return node.getChildren().findChild(name);
    }

    public static Node findPath(Node start, Enumeration<String> names) throws NodeNotFoundException {
        int depth = 0;
        while (names.hasMoreElements()) {
            String name = names.nextElement();
            Node next = NodeOp.findChild(start, name);
            if (next == null) {
                throw new NodeNotFoundException(start, name, depth);
            }
            start = next;
            ++depth;
        }
        return start;
    }

    public static Node findPath(Node start, String[] names) throws NodeNotFoundException {
        return NodeOp.findPath(start, Enumerations.array((Object[])names));
    }

    public static Node findRoot(Node node) {
        Node parent;
        while ((parent = node.getParentNode()) != null) {
            node = parent;
        }
        return node;
    }

    public static int[] computePermutation(Node[] arr1, Node[] arr2) throws IllegalArgumentException {
        if (arr1.length != arr2.length) {
            int max = Math.max(arr1.length, arr2.length);
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < max; ++i) {
                sb.append("" + i + " ");
                if (i < arr1.length) {
                    sb.append(arr1[i].getName());
                } else {
                    sb.append("---");
                }
                sb.append(" = ");
                if (i < arr2.length) {
                    sb.append(arr2[i].getName());
                } else {
                    sb.append("---");
                }
                sb.append('\n');
            }
            throw new IllegalArgumentException(sb.toString());
        }
        HashMap<Node, Integer> map = new HashMap<Node, Integer>();
        for (int i = 0; i < arr2.length; ++i) {
            map.put(arr2[i], i);
        }
        int[] perm = new int[arr1.length];
        int diff = 0;
        for (int i2 = 0; i2 < arr1.length; ++i2) {
            Integer newPos = (Integer)map.get(arr1[i2]);
            if (newPos == null) {
                throw new IllegalArgumentException("Missing permutation index " + i2);
            }
            perm[i2] = newPos;
            if (perm[i2] == i2) continue;
            ++diff;
        }
        return diff == 0 ? null : perm;
    }

    public static Node.Handle[] toHandles(Node[] nodes) {
        LinkedList<Node.Handle> ll = new LinkedList<Node.Handle>();
        for (Node n : nodes) {
            Node.Handle h = n.getHandle();
            if (h == null) continue;
            ll.add(h);
        }
        return ll.toArray(new Node.Handle[ll.size()]);
    }

    public static Node[] fromHandles(Node.Handle[] handles) throws IOException {
        Node[] arr = new Node[handles.length];
        for (int i = 0; i < handles.length; ++i) {
            arr[i] = handles[i].getNode();
        }
        return arr;
    }

    public static NodeListener weakNodeListener(NodeListener l, Object source) {
        return (NodeListener)WeakListeners.create(NodeListener.class, (EventListener)l, (Object)source);
    }

    public static void registerPropertyEditors() {
        NodesRegistrationSupport.registerPropertyEditors();
    }

    static Node factory(Map<String, ?> map) {
        return new LazyNode(map);
    }

    static SystemAction[] createFromNames(String[] arr) {
        LinkedList<SystemAction> ll = new LinkedList<SystemAction>();
        for (String n : arr) {
            if (n == null) {
                ll.add(null);
                continue;
            }
            String name = "org.openide.actions." + n + "Action";
            try {
                ClassLoader l = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
                if (l == null) {
                    l = Thread.currentThread().getContextClassLoader();
                }
                if (l == null) {
                    l = NodeOp.class.getClassLoader();
                }
                Class<SystemAction> c = Class.forName(name, true, l).asSubclass(SystemAction.class);
                ll.add(SystemAction.get(c));
                continue;
            }
            catch (ClassNotFoundException ex) {
                NodeOp.warning(ex);
            }
        }
        return ll.toArray((T[])new SystemAction[ll.size()]);
    }

    static void exception(Throwable ex) {
        NodeOp.warning(ex);
    }

    static void warning(Throwable ex) {
        LOG.log(Level.WARNING, null, ex);
    }
}

