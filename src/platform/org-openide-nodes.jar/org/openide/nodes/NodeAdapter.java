/*
 * Decompiled with CFR 0_118.
 */
package org.openide.nodes;

import java.beans.PropertyChangeEvent;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeReorderEvent;

public class NodeAdapter
implements NodeListener {
    @Override
    public void propertyChange(PropertyChangeEvent ev) {
    }

    @Override
    public void childrenAdded(NodeMemberEvent ev) {
    }

    @Override
    public void childrenRemoved(NodeMemberEvent ev) {
    }

    @Override
    public void childrenReordered(NodeReorderEvent ev) {
    }

    @Override
    public void nodeDestroyed(NodeEvent ev) {
    }
}

