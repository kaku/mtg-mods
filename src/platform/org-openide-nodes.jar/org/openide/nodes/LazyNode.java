/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Mutex
 */
package org.openide.nodes;

import java.util.Map;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.Mutex;

final class LazyNode
extends FilterNode {
    private Map<String, ?> map;

    LazyNode(Map<String, ?> map) {
        this(new AbstractNode(new Children.Array()), map);
    }

    private LazyNode(AbstractNode an, Map<String, ?> map) {
        super(an, new SwitchChildren(an));
        ((SwitchChildren)this.getChildren()).node = this;
        this.map = map;
        an.setName((String)map.get("name"));
        an.setDisplayName((String)map.get("displayName"));
        an.setShortDescription((String)map.get("shortDescription"));
        String iconBase = (String)map.get("iconResource");
        if (iconBase != null) {
            an.setIconBaseWithExtension(iconBase);
        }
    }

    @Override
    public Action[] getActions(boolean context) {
        return this.switchToOriginal().getActions(context);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final Node switchToOriginal() {
        final Node[] n = new Node[]{null};
        LazyNode lazyNode = this;
        synchronized (lazyNode) {
            if (this.map == null) {
                return this.getOriginal();
            }
            n[0] = (Node)this.map.get("original");
            if (n[0] == null) {
                throw new IllegalArgumentException("Original Node from map " + this.map + " is null");
            }
            this.map = null;
        }
        FilterNode.Children.MUTEX.postWriteRequest(new Runnable(){

            @Override
            public void run() {
                LazyNode.this.changeOriginal(n[0], true);
            }
        });
        return n[0];
    }

    private static final class SwitchChildren
    extends FilterNode.Children {
        LazyNode node;

        public SwitchChildren(Node or) {
            super(or);
        }

        @Override
        protected void addNotify() {
            this.node.switchToOriginal();
            super.addNotify();
        }

        @Override
        public Node[] getNodes(boolean optimalResult) {
            this.node.switchToOriginal();
            return super.getNodes(optimalResult);
        }

        @Override
        public int getNodesCount(boolean optimalResult) {
            this.node.switchToOriginal();
            return super.getNodesCount(optimalResult);
        }

        @Override
        public Node findChild(String name) {
            this.node.switchToOriginal();
            return super.findChild(name);
        }
    }

}

