/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.UserCancelException
 */
package org.openide.nodes;

import java.awt.Component;
import org.openide.nodes.Node;
import org.openide.nodes.NodeAcceptor;
import org.openide.util.Lookup;
import org.openide.util.UserCancelException;

public abstract class NodeOperation {
    protected NodeOperation() {
    }

    public static NodeOperation getDefault() {
        NodeOperation no = (NodeOperation)Lookup.getDefault().lookup(NodeOperation.class);
        if (no == null) {
            throw new IllegalStateException("To use NodeOperation you should have its implementation around. For example one from openide-explorer.jar");
        }
        return no;
    }

    public abstract boolean customize(Node var1);

    public abstract void explore(Node var1);

    public abstract void showProperties(Node var1);

    public abstract void showProperties(Node[] var1);

    public /* varargs */ void showCustomEditorDialog(Node.Property<?> property, Object ... beans) {
        throw new UnsupportedOperationException();
    }

    public abstract Node[] select(String var1, String var2, Node var3, NodeAcceptor var4, Component var5) throws UserCancelException;

    public Node[] select(String title, String rootTitle, Node root, NodeAcceptor acceptor) throws UserCancelException {
        return this.select(title, rootTitle, root, acceptor, null);
    }

    public final Node select(String title, String rootTitle, Node root) throws UserCancelException {
        return this.select(title, rootTitle, root, new NodeAcceptor(){

            @Override
            public boolean acceptNodes(Node[] nodes) {
                return nodes.length == 1;
            }
        })[0];
    }

}

