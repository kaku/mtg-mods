/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.SystemAction
 */
package org.openide.nodes;

import java.awt.Component;
import java.awt.Image;
import java.beans.BeanDescriptor;
import java.beans.BeanInfo;
import java.beans.Beans;
import java.beans.Customizer;
import java.beans.EventSetDescriptor;
import java.beans.IndexedPropertyDescriptor;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyDescriptor;
import java.beans.PropertyEditor;
import java.beans.beancontext.BeanContext;
import java.beans.beancontext.BeanContextChild;
import java.beans.beancontext.BeanContextProxy;
import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.BeanChildren;
import org.openide.nodes.Children;
import org.openide.nodes.CookieSet;
import org.openide.nodes.IndexedPropertySupport;
import org.openide.nodes.Node;
import org.openide.nodes.NodeOp;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.nodes.TMUtil;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.actions.SystemAction;

public class BeanNode<T>
extends AbstractNode {
    private static final String ICON_BASE = "org/openide/nodes/beans.gif";
    private final T bean;
    private BeanInfo beanInfo;
    private Method nameGetter = null;
    private Method nameSetter = null;
    private Method removePCLMethod = null;
    private BeanNode<T> propertyChangeListener = null;
    private boolean synchronizeName;

    public BeanNode(T bean) throws IntrospectionException {
        this(bean, null, null);
    }

    protected BeanNode(T bean, Children children) throws IntrospectionException {
        this(bean, children, null);
    }

    protected BeanNode(T bean, Children children, Lookup lkp) throws IntrospectionException {
        super(children == null ? BeanNode.getChildren(bean) : children, lkp);
        if (bean == null) {
            throw new NullPointerException("cannot make a node for a null bean");
        }
        this.bean = bean;
        try {
            this.initialization(lkp != null);
        }
        catch (IntrospectionException ie) {
            throw ie;
        }
        catch (RuntimeException re) {
            throw BeanNode.mkie(re);
        }
        catch (LinkageError le) {
            throw BeanNode.mkie(le);
        }
    }

    private static Children getChildren(Object bean) {
        BeanContextChild bch;
        if (bean instanceof BeanContext) {
            return new BeanChildren((BeanContext)bean);
        }
        if (bean instanceof BeanContextProxy && (bch = ((BeanContextProxy)bean).getBeanContextProxy()) instanceof BeanContext) {
            return new BeanChildren((BeanContext)bch);
        }
        return Children.LEAF;
    }

    private static IntrospectionException mkie(Throwable t) {
        return (IntrospectionException)new IntrospectionException(t.toString()).initCause(t);
    }

    protected void setSynchronizeName(boolean watch) {
        this.synchronizeName = watch;
    }

    protected T getBean() {
        return this.bean;
    }

    @Override
    public void destroy() throws IOException {
        if (this.removePCLMethod != null) {
            try {
                Object o = Beans.getInstanceOf(this.bean, this.removePCLMethod.getDeclaringClass());
                this.removePCLMethod.invoke(o, this.propertyChangeListener);
            }
            catch (Exception e) {
                NodeOp.exception(e);
            }
        }
        super.destroy();
    }

    @Override
    public boolean canDestroy() {
        return true;
    }

    @Override
    public void setName(String s) {
        Method m;
        if (this.synchronizeName && (m = this.nameSetter) != null) {
            try {
                m.invoke(this.bean, s);
            }
            catch (Exception e) {
                NodeOp.exception(e);
            }
        }
        super.setName(s);
    }

    @Override
    public boolean canRename() {
        return !this.synchronizeName || this.nameSetter != null;
    }

    @Override
    public Image getIcon(int type) {
        Image image = this.beanInfo.getIcon(type);
        if (image != null) {
            return image;
        }
        return super.getIcon(type);
    }

    @Override
    public Image getOpenedIcon(int type) {
        return this.getIcon(type);
    }

    @Override
    public HelpCtx getHelpCtx() {
        HelpCtx h = HelpCtx.findHelp(this.bean);
        if (h != HelpCtx.DEFAULT_HELP) {
            return h;
        }
        return new HelpCtx(BeanNode.class);
    }

    protected void createProperties(T bean, BeanInfo info) {
        Descriptor d = BeanNode.computeProperties(bean, info);
        Sheet sets = this.getSheet();
        Sheet.Set pset = Sheet.createPropertiesSet();
        pset.put(d.property);
        BeanDescriptor bd = info.getBeanDescriptor();
        if (bd != null && bd.getValue("propertiesHelpID") != null) {
            pset.setValue("helpID", bd.getValue("propertiesHelpID"));
        }
        sets.put(pset);
        if (d.expert.length != 0) {
            Sheet.Set eset = Sheet.createExpertSet();
            eset.put(d.expert);
            if (bd != null && bd.getValue("expertHelpID") != null) {
                eset.setValue("helpID", bd.getValue("expertHelpID"));
            }
            sets.put(eset);
        }
    }

    @Override
    public boolean canCopy() {
        return true;
    }

    @Override
    public boolean canCut() {
        return false;
    }

    @Override
    public Action[] getActions(boolean context) {
        return NodeOp.createFromNames(new String[]{"Copy", null, "Tools", "Properties"});
    }

    @Override
    public boolean hasCustomizer() {
        return this.beanInfo.getBeanDescriptor().getCustomizerClass() != null;
    }

    @Override
    public Component getCustomizer() {
        Object o;
        Class clazz = this.beanInfo.getBeanDescriptor().getCustomizerClass();
        if (clazz == null) {
            return null;
        }
        try {
            o = clazz.newInstance();
        }
        catch (InstantiationException e) {
            NodeOp.exception(e);
            return null;
        }
        catch (IllegalAccessException e) {
            NodeOp.exception(e);
            return null;
        }
        if (!(o instanceof Customizer)) {
            return null;
        }
        Customizer cust = (Customizer)o;
        TMUtil.attachCustomizer(this, cust);
        Component comp = null;
        comp = o instanceof Component ? (Component)o : TMUtil.createDialog(o);
        if (comp == null) {
            return null;
        }
        cust.setObject(this.bean);
        if (this.removePCLMethod == null) {
            cust.addPropertyChangeListener(new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent e) {
                    BeanNode.this.firePropertyChange(e.getPropertyName(), e.getOldValue(), e.getNewValue());
                }
            });
        }
        return comp;
    }

    public static Descriptor computeProperties(Object bean, BeanInfo info) {
        ArrayList<Node.Property> property = new ArrayList<Node.Property>();
        ArrayList<Node.Property> expert = new ArrayList<Node.Property>();
        ArrayList<Node.Property> hidden = new ArrayList<Node.Property>();
        PropertyDescriptor[] propertyDescriptor = info.getPropertyDescriptors();
        int k = propertyDescriptor.length;
        for (int i = 0; i < k; ++i) {
            PropertySupport.Reflection support;
            PropertySupport.Reflection prop;
            PropertyDescriptor p;
            String aname;
            Enumeration<String> e;
            if (propertyDescriptor[i].getPropertyType() == null) continue;
            if (propertyDescriptor[i] instanceof IndexedPropertyDescriptor) {
                p = (IndexedPropertyDescriptor)propertyDescriptor[i];
                if (p.getReadMethod() != null && !p.getReadMethod().getReturnType().isArray()) continue;
                support = new IndexedPropertySupport(bean, p.getPropertyType(), p.getIndexedPropertyType(), p.getReadMethod(), p.getWriteMethod(), p.getIndexedReadMethod(), p.getIndexedWriteMethod());
                support.setName(p.getName());
                support.setDisplayName(p.getDisplayName());
                support.setShortDescription(p.getShortDescription());
                e = p.attributeNames();
                while (e.hasMoreElements()) {
                    aname = e.nextElement();
                    support.setValue(aname, p.getValue(aname));
                }
                prop = support;
            } else {
                p = propertyDescriptor[i];
                support = new PropertySupport.Reflection(bean, p.getPropertyType(), p.getReadMethod(), p.getWriteMethod());
                support.setName(p.getName());
                support.setDisplayName(p.getDisplayName());
                support.setShortDescription(p.getShortDescription());
                support.setPropertyEditorClass(p.getPropertyEditorClass());
                e = p.attributeNames();
                while (e.hasMoreElements()) {
                    aname = e.nextElement();
                    support.setValue(aname, p.getValue(aname));
                }
                prop = support;
            }
            Object help = propertyDescriptor[i].getValue("helpID");
            if (help != null && help instanceof String) {
                prop.setValue("helpID", help);
            }
            if (propertyDescriptor[i].isHidden()) {
                hidden.add(prop);
                continue;
            }
            if (propertyDescriptor[i].isExpert()) {
                expert.add(prop);
                prop.setExpert(true);
                continue;
            }
            property.add(prop);
        }
        return new Descriptor(property, expert, hidden);
    }

    private void initialization(boolean hasLookup) throws IntrospectionException {
        int i;
        Node.Cookie instanceCookie;
        this.setIconBaseWithExtension("org/openide/nodes/beans.gif");
        this.setSynchronizeName(true);
        Class clazz = this.bean.getClass();
        while (!Modifier.isPublic(clazz.getModifiers()) && !this.hasExplicitBeanInfo(clazz)) {
            if ((clazz = clazz.getSuperclass()) != null) continue;
            clazz = Object.class;
        }
        this.beanInfo = Utilities.getBeanInfo(clazz);
        this.registerName();
        this.setNameSilently(this.getNameForBean());
        BeanDescriptor descriptor = this.beanInfo.getBeanDescriptor();
        String sd = descriptor.getShortDescription();
        if (!Utilities.compareObjects((Object)sd, (Object)descriptor.getDisplayName())) {
            this.setShortDescription(sd);
        }
        EventSetDescriptor[] eventSetDescriptors = this.beanInfo.getEventSetDescriptors();
        int k = eventSetDescriptors.length;
        Method method = null;
        for (i = 0; !(i >= k || (method = eventSetDescriptors[i].getAddListenerMethod()) != null && method.getName().equals("addPropertyChangeListener") && Modifier.isPublic(method.getModifiers())); ++i) {
        }
        if (i != k) {
            try {
                Object o = Beans.getInstanceOf(this.bean, method.getDeclaringClass());
                this.propertyChangeListener = new PropL();
                method.invoke(o, WeakListeners.propertyChange(this.propertyChangeListener, (Object)o));
                this.removePCLMethod = eventSetDescriptors[i].getRemoveListenerMethod();
            }
            catch (Exception e) {
                Exceptions.attachMessage((Throwable)e, (String)("Trying to invoke " + method + " where introspected class is " + clazz.getName()));
                NodeOp.warning(e);
            }
        }
        this.createProperties(this.bean, this.beanInfo);
        Enumeration<String> e = this.beanInfo.getBeanDescriptor().attributeNames();
        while (e.hasMoreElements()) {
            String aname = e.nextElement();
            this.setValue(aname, this.beanInfo.getBeanDescriptor().getValue(aname));
        }
        if (!hasLookup && (instanceCookie = TMUtil.createInstanceCookie(this.bean)) != null) {
            this.getCookieSet().add(instanceCookie);
        }
    }

    private boolean hasExplicitBeanInfo(Class<?> clazz) {
        String className = clazz.getName();
        int indx = className.lastIndexOf(46);
        className = className.substring(indx + 1);
        String[] paths = Introspector.getBeanInfoSearchPath();
        for (int i = 0; i < paths.length; ++i) {
            String s = paths[i] + '.' + className + "BeanInfo";
            try {
                Class.forName(s);
                return true;
            }
            catch (ClassNotFoundException ex) {
                continue;
            }
        }
        return false;
    }

    private void registerName() {
        Class clazz;
        clazz = this.bean.getClass();
        while (!Modifier.isPublic(clazz.getModifiers())) {
            if ((clazz = clazz.getSuperclass()) != null) continue;
            clazz = Object.class;
        }
        try {
            try {
                this.nameGetter = clazz.getMethod("getName", new Class[0]);
                if (this.nameGetter.getReturnType() != String.class) {
                    throw new NoSuchMethodException();
                }
            }
            catch (NoSuchMethodException e) {
                try {
                    this.nameGetter = clazz.getMethod("getDisplayName", new Class[0]);
                    if (this.nameGetter.getReturnType() != String.class) {
                        throw new NoSuchMethodException();
                    }
                }
                catch (NoSuchMethodException ee) {
                    this.nameGetter = null;
                    return;
                }
            }
        }
        catch (SecurityException se) {
            NodeOp.exception(se);
            this.nameGetter = null;
            return;
        }
        try {
            String result = (String)this.nameGetter.invoke(this.bean, new Object[0]);
        }
        catch (Exception e) {
            Exceptions.attachMessage((Throwable)e, (String)("Bad method: " + clazz.getName() + "." + this.nameGetter.getName()));
            Logger.getLogger(BeanNode.class.getName()).log(Level.WARNING, null, e);
            this.nameGetter = null;
            return;
        }
        try {
            try {
                this.nameSetter = clazz.getMethod("setName", String.class);
                if (this.nameSetter.getReturnType() != Void.TYPE) {
                    throw new NoSuchMethodException();
                }
            }
            catch (NoSuchMethodException e) {
                try {
                    this.nameSetter = clazz.getMethod("setDisplayName", String.class);
                    if (this.nameSetter.getReturnType() != Void.TYPE) {
                        throw new NoSuchMethodException();
                    }
                }
                catch (NoSuchMethodException ee) {
                    this.nameSetter = null;
                }
            }
        }
        catch (SecurityException se) {
            NodeOp.exception(se);
        }
    }

    private String getNameForBean() {
        if (this.nameGetter != null) {
            try {
                String name = (String)this.nameGetter.invoke(this.bean, new Object[0]);
                return name != null ? name : "";
            }
            catch (Exception ex) {
                NodeOp.warning(ex);
            }
        }
        BeanDescriptor descriptor = this.beanInfo.getBeanDescriptor();
        return descriptor.getDisplayName();
    }

    void setNameSilently(String name) {
        super.setName(name);
    }

    @Override
    public Action getPreferredAction() {
        SystemAction[] arr = NodeOp.createFromNames(new String[]{"Properties"});
        return arr.length == 1 ? arr[0] : null;
    }

    private final class PropL
    implements PropertyChangeListener {
        PropL() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent e) {
            String newName;
            String name = e.getPropertyName();
            if (name == null) {
                BeanNode.this.firePropertyChange(null, e.getOldValue(), e.getNewValue());
            } else {
                PropertyDescriptor[] arr = BeanNode.this.beanInfo.getPropertyDescriptors();
                for (int i = 0; i < arr.length; ++i) {
                    if (arr[i].isHidden() || !name.equals(arr[i].getName())) continue;
                    BeanNode.this.firePropertyChange(e.getPropertyName(), e.getOldValue(), e.getNewValue());
                    break;
                }
            }
            if (BeanNode.this.synchronizeName && (name == null || name.equals("name") || name.equals("displayName")) && !(newName = BeanNode.this.getNameForBean()).equals(BeanNode.this.getName())) {
                BeanNode.this.setNameSilently(newName);
            }
        }
    }

    public static final class Descriptor {
        public final Node.Property[] property;
        public final Node.Property[] expert;
        public final Node.Property[] hidden;

        Descriptor(ArrayList<Node.Property> p, ArrayList<Node.Property> e, ArrayList<Node.Property> h) {
            this.property = new Node.Property[p.size()];
            p.toArray(this.property);
            this.expert = new Node.Property[e.size()];
            e.toArray(this.expert);
            this.hidden = new Node.Property[h.size()];
            h.toArray(this.hidden);
        }
    }

}

