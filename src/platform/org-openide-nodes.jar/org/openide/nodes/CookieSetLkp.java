/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Pair
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 */
package org.openide.nodes;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import org.openide.nodes.CookieSet;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

final class CookieSetLkp
extends AbstractLookup {
    private final CookieSet.Before before;
    private ThreadLocal<Object> isInReplaceInst = new ThreadLocal();

    public CookieSetLkp(CookieSet.Before b) {
        this.before = b;
    }

    public void add(Object obj) {
        this.addPair(new SimpleItem<Object>(obj));
    }

    public final <T, R> void add(T inst, InstanceContent.Convertor<T, R> conv) {
        this.addPair(new ConvertingItem<T, R>(inst, conv));
    }

    public void remove(Object obj) {
        this.removePair(new SimpleItem<Object>(obj));
    }

    public final <T, R> void remove(T inst, InstanceContent.Convertor<T, R> conv) {
        this.removePair(new ConvertingItem<T, R>(inst, conv));
    }

    void superRemovePair(AbstractLookup.Pair pair) {
        this.removePair(pair);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    <T> void replaceInstances(Class<? extends T> clazz, T[] instances, CookieSet set) {
        Object prev = this.isInReplaceInst.get();
        try {
            this.isInReplaceInst.set((Object)this);
            Iterator it = this.lookupResult(Object.class).allItems().iterator();
            ArrayList<AbstractLookup.Pair> pairs = new ArrayList<AbstractLookup.Pair>();
            boolean change = false;
            int index = 0;
            while (it.hasNext()) {
                Lookup.Item item = (Lookup.Item)it.next();
                assert (item instanceof AbstractLookup.Pair);
                if (clazz.isAssignableFrom(item.getType())) {
                    if (index < instances.length) {
                        SimpleItem simple;
                        if (item instanceof SimpleItem && (simple = (SimpleItem)item).obj == instances[index]) {
                            ++index;
                            pairs.add(simple);
                            continue;
                        }
                        change = true;
                        pairs.add(new SimpleItem<T>(instances[index++]));
                        continue;
                    }
                    change = true;
                    continue;
                }
                pairs.add((AbstractLookup.Pair)item);
            }
            while (index < instances.length) {
                change = true;
                pairs.add(new SimpleItem<T>(instances[index++]));
            }
            if (change) {
                this.setPairs(pairs);
                set.fireChangeEvent();
            }
        }
        finally {
            this.isInReplaceInst.set(prev);
        }
    }

    protected void beforeLookup(Lookup.Template<?> template) {
        this.beforeLookupImpl(template.getType());
    }

    final void beforeLookupImpl(Class<?> clazz) {
        if (this.before != null && this.isInReplaceInst.get() == null) {
            this.before.beforeLookup(clazz);
        }
    }

    static final class ConvertingItem<T, R>
    extends AbstractLookup.Pair<R> {
        private T obj;
        private WeakReference<R> ref;
        private InstanceContent.Convertor<? super T, R> conv;

        public ConvertingItem(T obj, InstanceContent.Convertor<? super T, R> conv) {
            this.obj = obj;
            this.conv = conv;
        }

        public boolean instanceOf(Class<?> c) {
            return c.isAssignableFrom(this.getType());
        }

        private R getConverted() {
            if (this.ref == null) {
                return null;
            }
            return this.ref.get();
        }

        public synchronized R getInstance() {
            Object converted = this.getConverted();
            if (converted == null) {
                converted = this.conv.convert(this.obj);
                this.ref = new WeakReference<Object>(converted);
            }
            return (R)converted;
        }

        public boolean equals(Object o) {
            if (o instanceof ConvertingItem) {
                return this.obj.equals(((ConvertingItem)o).obj);
            }
            return false;
        }

        public int hashCode() {
            return this.obj.hashCode();
        }

        public String getId() {
            return this.conv.id(this.obj);
        }

        public String getDisplayName() {
            return this.conv.displayName(this.obj);
        }

        protected boolean creatorOf(Object obj) {
            if (this.conv == null) {
                return obj == this.obj;
            }
            return obj == this.getConverted();
        }

        public Class<? extends R> getType() {
            R converted = this.getConverted();
            if (converted == null) {
                return this.conv.type(this.obj);
            }
            return converted.getClass();
        }
    }

    static final class SimpleItem<T>
    extends AbstractLookup.Pair<T> {
        private T obj;

        public SimpleItem(T obj) {
            if (obj == null) {
                throw new NullPointerException();
            }
            this.obj = obj;
        }

        public boolean instanceOf(Class<?> c) {
            return c.isInstance(this.obj);
        }

        public T getInstance() {
            return this.obj;
        }

        public boolean equals(Object o) {
            if (o instanceof SimpleItem) {
                return this.obj.equals(((SimpleItem)o).obj);
            }
            return false;
        }

        public int hashCode() {
            return this.obj.hashCode();
        }

        public String getId() {
            return "IL[" + this.obj.toString();
        }

        public String getDisplayName() {
            return this.obj.toString();
        }

        protected boolean creatorOf(Object obj) {
            return obj == this.obj;
        }

        public Class<? extends T> getType() {
            return this.obj.getClass();
        }
    }

}

