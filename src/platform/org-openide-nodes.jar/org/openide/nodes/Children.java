/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Enumerations
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Privileged
 *  org.openide.util.Parameters
 */
package org.openide.nodes;

import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.nodes.AsynchChildren;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.EntrySupport;
import org.openide.nodes.EntrySupportDefault;
import org.openide.nodes.EntrySupportLazy;
import org.openide.nodes.Node;
import org.openide.nodes.SynchChildren;
import org.openide.util.Enumerations;
import org.openide.util.Mutex;
import org.openide.util.Parameters;

public abstract class Children {
    static final Mutex.Privileged PR = new Mutex.Privileged();
    public static final Mutex MUTEX = new Mutex(PR, (Executor)new ProjectManagerDeadlockDetector());
    public static final Children LEAF = new Empty();
    static final Logger LOG = Logger.getLogger(Children.class.getName());
    private EntrySupport entrySupport;
    Node parent;
    boolean lazySupport;

    public Children() {
        this(false);
    }

    Children(boolean lazy) {
        this.lazySupport = lazy;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    EntrySupport entrySupport() {
        Class<Children> class_ = Children.class;
        synchronized (Children.class) {
            if (this.getEntrySupport() == null) {
                LOG.finer("Initializing entrySupport");
                EntrySupport es = this.lazySupport ? new EntrySupportLazy(this) : new EntrySupportDefault(this);
                this.setEntrySupport(es);
                this.postInitializeEntrySupport(es);
            }
            // ** MonitorExit[var1_1] (shouldn't be in output)
            return this.getEntrySupport();
        }
    }

    boolean isLazy() {
        return this.lazySupport;
    }

    void checkSupport() {
    }

    void postInitializeEntrySupport(EntrySupport es) {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void attachTo(Node n) throws IllegalStateException {
        if (this == LEAF) {
            return;
        }
        Children children = this;
        synchronized (children) {
            if (this.parent != null) {
                throw new IllegalStateException("An instance of Children may not be used for more than one parent node.");
            }
            this.parent = n;
        }
        Node[] nodes = this.testNodes();
        if (nodes == null) {
            return;
        }
        try {
            PR.enterReadAccess();
            nodes = this.testNodes();
            if (nodes == null) {
                return;
            }
            for (int i = 0; i < nodes.length; ++i) {
                Node node = nodes[i];
                node.assignTo(this, i);
                node.fireParentNodeChange(null, this.parent);
            }
        }
        finally {
            PR.exitReadAccess();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void detachFrom() {
        if (this == LEAF) {
            return;
        }
        Node oldParent = null;
        Children children = this;
        synchronized (children) {
            if (this.parent == null) {
                throw new IllegalStateException("Trying to detach children which do not have parent");
            }
            oldParent = this.parent;
            this.parent = null;
        }
        try {
            PR.enterReadAccess();
            Node[] nodes = this.testNodes();
            if (nodes == null) {
                return;
            }
            for (int i = 0; i < nodes.length; ++i) {
                Node node = nodes[i];
                node.deassignFrom(this);
                node.fireParentNodeChange(oldParent, null);
            }
        }
        finally {
            PR.exitReadAccess();
        }
    }

    public static <T> Children create(ChildFactory<T> factory, boolean asynchronous) {
        if (factory == null) {
            throw new NullPointerException("Null factory");
        }
        if (asynchronous) {
            AsynchChildren<T> ch = new AsynchChildren<T>(factory);
            factory.setObserver(ch);
            return ch;
        }
        SynchChildren<T> ch = new SynchChildren<T>(factory);
        factory.setObserver(ch);
        return ch;
    }

    public static Children createLazy(Callable<Children> factory) {
        return new LazyChildren(factory);
    }

    protected final Node getNode() {
        return this.parent;
    }

    final Object cloneHierarchy() throws CloneNotSupportedException {
        return this.clone();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected Object clone() throws CloneNotSupportedException {
        Class<Children> class_ = Children.class;
        synchronized (Children.class) {
            Children ch = (Children)super.clone();
            ch.parent = null;
            ch.setEntrySupport(null);
            // ** MonitorExit[var1_1] (shouldn't be in output)
            return ch;
        }
    }

    public abstract boolean add(Node[] var1);

    public abstract boolean remove(Node[] var1);

    public final Enumeration<Node> nodes() {
        return Enumerations.array((Object[])this.getNodes());
    }

    public Node findChild(String name) {
        Node[] list = this.getNodes();
        if (list.length == 0) {
            return null;
        }
        if (name == null) {
            return list[0];
        }
        for (int i = 0; i < list.length; ++i) {
            if (!name.equals(list[i].getName())) continue;
            return list[i];
        }
        return null;
    }

    protected final boolean isInitialized() {
        return this.entrySupport().isInitialized();
    }

    public final Node getNodeAt(int index) {
        this.checkSupport();
        return this.entrySupport().getNodeAt(index);
    }

    public final Node[] getNodes() {
        this.checkSupport();
        return this.entrySupport().getNodes(false);
    }

    public Node[] getNodes(boolean optimalResult) {
        this.checkSupport();
        return this.entrySupport().getNodes(optimalResult);
    }

    public final int getNodesCount() {
        this.checkSupport();
        return this.entrySupport().getNodesCount(false);
    }

    public int getNodesCount(boolean optimalResult) {
        this.checkSupport();
        return this.entrySupport().getNodesCount(optimalResult);
    }

    public final List<Node> snapshot() {
        return this.entrySupport().snapshot();
    }

    static final int[] getSnapshotIdxs(List<Node> snapshot) {
        int[] idxs = new int[snapshot.size()];
        int i = 0;
        while (i < idxs.length) {
            idxs[i] = i++;
        }
        return idxs;
    }

    protected void addNotify() {
    }

    protected void removeNotify() {
    }

    void callAddNotify() {
        this.addNotify();
    }

    final void callRemoveNotify() {
        this.removeNotify();
    }

    void destroyNodes(Node[] arr) {
    }

    private Node[] testNodes() {
        return this.getEntrySupport() == null ? null : this.entrySupport().testNodes();
    }

    final EntrySupport getEntrySupport() {
        return this.entrySupport;
    }

    final void setEntrySupport(EntrySupport entrySupport) {
        assert (Thread.holdsLock(Children.class));
        this.entrySupport = entrySupport;
    }

    private static final class ProjectManagerDeadlockDetector
    implements Executor {
        private final Mutex FALLBACK = new Mutex();
        private final AtomicReference<WeakReference<Mutex>> pmMutexRef = new AtomicReference();

        private ProjectManagerDeadlockDetector() {
        }

        @Override
        public void execute(Runnable command) {
            Mutex mutex;
            boolean ea = false;
            if (!$assertionsDisabled) {
                ea = true;
                if (!true) {
                    throw new AssertionError();
                }
            }
            if (ea && (mutex = this.getPMMutex()) != null && (mutex.isReadAccess() || mutex.isWriteAccess())) {
                throw new IllegalStateException("Should not acquire Children.MUTEX while holding ProjectManager.mutex()");
            }
            command.run();
        }

        private Mutex getPMMutex() {
            block3 : {
                WeakReference<Mutex> weakRef;
                Mutex mutex;
                WeakReference<Mutex> newWeakRef;
                do {
                    mutex = null;
                    weakRef = this.pmMutexRef.get();
                    if (weakRef != null) {
                        mutex = weakRef.get();
                    }
                    if (mutex != null) {
                        return mutex;
                    }
                    mutex = this.callPMMutexMethod();
                    if (mutex == null) break block3;
                } while (!this.pmMutexRef.compareAndSet(weakRef, newWeakRef = new WeakReference<Mutex>(mutex)));
                return mutex;
            }
            return null;
        }

        private Mutex callPMMutexMethod() {
            Class clazz = null;
            Method method = null;
            try {
                clazz = Thread.currentThread().getContextClassLoader().loadClass("org.netbeans.api.project.ProjectManager");
                method = clazz.getMethod("mutex", new Class[0]);
                return (Mutex)method.invoke(null, new Object[0]);
            }
            catch (ClassNotFoundException e) {
                return this.FALLBACK;
            }
            catch (IllegalAccessException e) {
                return this.FALLBACK;
            }
            catch (IllegalArgumentException e) {
                return this.FALLBACK;
            }
            catch (InvocationTargetException e) {
                return this.FALLBACK;
            }
            catch (NoSuchMethodException e) {
                return this.FALLBACK;
            }
            catch (ClassCastException e) {
                Class type = method.getReturnType();
                Children.LOG.log(Level.WARNING, "Reopen #175325 and save complete log: type=" + type.getName() + " type.cl=" + type.getClassLoader() + " Mutex.cl=" + Mutex.class.getClassLoader() + " clazz.cl=" + clazz.getClassLoader(), e);
                return null;
            }
        }
    }

    static class LazyChildren
    extends Children {
        private Callable<Children> factory;
        private Children original;
        private final Object originalLock = new Object();

        LazyChildren(Callable<Children> factory) {
            this.factory = factory;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        Children getOriginal() {
            Object object = this.originalLock;
            synchronized (object) {
                if (this.original == null) {
                    try {
                        this.original = this.factory.call();
                    }
                    catch (Exception ex) {
                        throw new RuntimeException(ex);
                    }
                }
                return this.original;
            }
        }

        @Override
        public boolean add(Node[] nodes) {
            return this.getOriginal().add(nodes);
        }

        @Override
        public boolean remove(Node[] nodes) {
            return this.getOriginal().remove(nodes);
        }

        @Override
        protected void addNotify() {
            this.getOriginal().addNotify();
        }

        @Override
        protected void removeNotify() {
            this.getOriginal().removeNotify();
        }

        @Override
        EntrySupport entrySupport() {
            return this.getOriginal().entrySupport();
        }

        @Override
        public Node findChild(String name) {
            return this.getOriginal().findChild(name);
        }
    }

    private static abstract class Dupl<T>
    implements Cloneable,
    Entry {
        protected Object key;

        Dupl() {
        }

        public final void updateList(Collection<? extends T> src, Collection<? super Dupl<T>> target) {
            HashMap map = new HashMap(src.size() * 2);
            for (T o : src) {
                this.updateListAndMap(o, target, map);
            }
        }

        public final void updateList(T[] arr, Collection<? super Dupl<T>> target) {
            HashMap map = new HashMap(arr.length * 2);
            for (T o : arr) {
                this.updateListAndMap(o, target, map);
            }
        }

        public final void updateListAndMap(T obj, Collection<? super Dupl<T>> list, java.util.Map<T, Object> map) {
            Parameters.notNull((CharSequence)"obj", obj);
            Dupl prev = map.put(obj, this);
            if (prev == null) {
                list.add(this.createInstance(obj, 0));
                return;
            }
            if (prev == this) {
                map.put(obj, 1);
                list.add(this.createInstance(obj, 1));
                return;
            }
            int cnt = (Integer)((Object)prev) + 1;
            map.put(obj, cnt);
            list.add(this.createInstance(obj, cnt));
        }

        public T getKey() {
            if (this.key instanceof Dupl) {
                return ((Dupl)this.key).getKey();
            }
            return (T)this.key;
        }

        public int getCnt() {
            int cnt = 0;
            Dupl d = this;
            while (d.key instanceof Dupl) {
                d = (Dupl)d.key;
                ++cnt;
            }
            return cnt;
        }

        private final Dupl<T> createInstance(Object obj, int cnt) {
            try {
                Dupl d;
                Dupl first = d = (Dupl)this.clone();
                while (cnt-- > 0) {
                    Dupl n = (Dupl)this.clone();
                    d.key = n;
                    d = n;
                }
                d.key = obj;
                return first;
            }
            catch (CloneNotSupportedException ex) {
                throw new InternalError();
            }
        }

        public int hashCode() {
            return this.getKey().hashCode();
        }

        public boolean equals(Object o) {
            if (o instanceof Dupl) {
                Dupl d = (Dupl)o;
                return this.getKey().equals(d.getKey()) && this.getCnt() == d.getCnt();
            }
            return false;
        }
    }

    public static abstract class Keys<T>
    extends Array {
        private static java.util.Map<Keys<?>, Runnable> lastRuns = new HashMap<Keys<?>, Runnable>(11);
        boolean before;

        public Keys() {
            this(false);
        }

        protected Keys(boolean lazy) {
            super(lazy);
        }

        @Override
        public Object clone() {
            Keys k = (Keys)super.clone();
            return k;
        }

        @Override
        void checkSupport() {
            if (this.lazySupport && this.nodes != null && this.nodes.size() > 0) {
                this.fallbackToDefaultSupport();
            }
        }

        void fallbackToDefaultSupport() {
            LOG.warning("Fallbacking entry support from lazy to default - Children.Array method was used");
            this.switchSupport(false);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void switchSupport(boolean toLazy) {
            if (toLazy == this.lazySupport) {
                return;
            }
            try {
                Object snapshot;
                Children.PR.enterWriteAccess();
                List<Entry> entries = this.entrySupport().getEntries();
                boolean init = this.entrySupport().isInitialized();
                if (init && this.parent != null && (snapshot = this.getEntrySupport().snapshot()).size() > 0) {
                    int[] idxs = Keys.getSnapshotIdxs(snapshot);
                    this.parent.fireSubNodesChangeIdx(false, idxs, null, Collections.<Node>emptyList(), (List<Node>)snapshot);
                }
                snapshot = Children.class;
                synchronized (Children.class) {
                    this.setEntrySupport(null);
                    // ** MonitorExit[snapshot] (shouldn't be in output)
                    this.lazySupport = toLazy;
                    if (toLazy) {
                        this.nodesEntry = null;
                    } else {
                        this.nodesEntry = this.createNodesEntry();
                        entries = new ArrayList<Entry>(entries);
                        entries.add(this.before ? 0 : entries.size(), this.nodesEntry);
                    }
                    if (init) {
                        this.entrySupport().notifySetEntries();
                    }
                    this.entrySupport().setEntries(entries);
                }
            }
            finally {
                Children.PR.exitWriteAccess();
            }
        }

        @Deprecated
        @Override
        public boolean add(Node[] arr) {
            if (this.lazySupport) {
                this.fallbackToDefaultSupport();
            }
            return super.add(arr);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Deprecated
        @Override
        public boolean remove(Node[] arr) {
            if (this.lazySupport) {
                return false;
            }
            try {
                PR.enterWriteAccess();
                if (this.nodes != null) {
                    for (int i = 0; i < arr.length; ++i) {
                        if (this.nodes.contains(arr[i])) continue;
                        arr[i] = null;
                    }
                    super.remove(arr);
                }
            }
            finally {
                PR.exitWriteAccess();
            }
            return true;
        }

        protected final void refreshKey(final T key) {
            MUTEX.postWriteRequest(new Runnable(){

                @Override
                public void run() {
                    Keys.this.entrySupport().refreshEntry(Keys.this.createEntryForKey(key));
                }
            });
        }

        Entry createEntryForKey(T key) {
            return new KE(this, key);
        }

        protected final void setKeys(Collection<? extends T> keysSet) {
            boolean asserts = false;
            if (!$assertionsDisabled) {
                asserts = true;
                if (!true) {
                    throw new AssertionError();
                }
            }
            int sz = keysSet.size();
            if (asserts && sz < 10) {
                ArrayList<T> keys = new ArrayList<T>(keysSet);
                for (int i = 0; i < sz - 1; ++i) {
                    T a = keys.get(i);
                    for (int j = i + 1; j < sz; ++j) {
                        T b = keys.get(j);
                        assert (!a.equals(b) || a.hashCode() == b.hashCode());
                    }
                }
            }
            ArrayList<Entry> l = new ArrayList<Entry>(keysSet.size() + 1);
            KE updator = new KE(this);
            if (this.lazySupport) {
                updator.updateList(keysSet, l);
            } else {
                if (this.before) {
                    l.add(this.getNodesEntry());
                }
                updator.updateList(keysSet, l);
                if (!this.before) {
                    l.add(this.getNodesEntry());
                }
            }
            this.applyKeys(l);
        }

        protected final void setKeys(T[] keys) {
            boolean asserts = false;
            if (!$assertionsDisabled) {
                asserts = true;
                if (!true) {
                    throw new AssertionError();
                }
            }
            int sz = keys.length;
            if (asserts && sz < 10) {
                for (int i = 0; i < sz - 1; ++i) {
                    T a = keys[i];
                    for (int j = i + 1; j < sz; ++j) {
                        T b = keys[j];
                        assert (!a.equals(b) || a.hashCode() == b.hashCode());
                    }
                }
            }
            ArrayList<Entry> l = new ArrayList<Entry>(keys.length + 1);
            KE updator = new KE(this);
            if (this.lazySupport) {
                updator.updateList(keys, l);
            } else {
                if (this.before) {
                    l.add(this.getNodesEntry());
                }
                updator.updateList(keys, l);
                if (!this.before) {
                    l.add(this.getNodesEntry());
                }
            }
            this.applyKeys(l);
        }

        private void applyKeys(final List<? extends Entry> l) {
            Runnable invoke = new Runnable(){

                @Override
                public void run() {
                    if (Keys.keysCheck(Keys.this, this)) {
                        Keys.this.entrySupport().setEntries(l);
                        Keys.keysExit(Keys.this, this);
                    }
                }
            };
            Keys.keysEnter(this, invoke);
            MUTEX.postWriteRequest(invoke);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected final void setBefore(boolean b) {
            try {
                PR.enterWriteAccess();
                if (this.before != b && !this.lazySupport) {
                    List<Entry> l = this.entrySupport().getEntries();
                    l.remove(this.getNodesEntry());
                    this.before = b;
                    if (b) {
                        l.add(0, this.getNodesEntry());
                    } else {
                        l.add(this.getNodesEntry());
                    }
                    this.entrySupport().setEntries(l);
                }
            }
            finally {
                PR.exitWriteAccess();
            }
        }

        protected abstract Node[] createNodes(T var1);

        @Override
        protected void destroyNodes(Node[] arr) {
            for (int i = 0; i < arr.length; ++i) {
                arr[i].fireNodeDestroyed();
            }
        }

        private static synchronized void keysEnter(Keys<?> ch, Runnable run) {
            lastRuns.put(ch, run);
        }

        private static synchronized void keysExit(Keys ch, Runnable r) {
            Runnable reg = lastRuns.remove(ch);
            if (reg != null && !reg.equals(r)) {
                lastRuns.put(ch, reg);
            }
        }

        private static synchronized boolean keysCheck(Keys ch, Runnable run) {
            return run == lastRuns.get(ch);
        }

        class KE
        extends Dupl<T> {
            final /* synthetic */ Keys this$0;

            public KE(Keys keys) {
                this.this$0 = keys;
            }

            public KE(Keys keys, T key) {
                this.this$0 = keys;
                this.key = key;
            }

            @Override
            public Collection<Node> nodes(Object source) {
                Node[] arr = this.this$0.createNodes(this.getKey());
                if (arr == null) {
                    return Collections.emptyList();
                }
                return new LinkedList<Node>(Arrays.asList(arr));
            }

            public String toString() {
                String s = this.getKey().toString();
                if (s.length() > 80) {
                    s = s.substring(s.length() - 80);
                }
                return "Children.Keys.KE[" + s + "," + this.getCnt() + "]";
            }
        }

    }

    public static class SortedMap<T>
    extends Map<T> {
        private Comparator<? super Node> comp;

        public SortedMap() {
        }

        protected SortedMap(java.util.Map<T, Node> map) {
            super(map);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void setComparator(Comparator<? super Node> c) {
            try {
                PR.enterWriteAccess();
                this.comp = c;
                this.refresh();
            }
            finally {
                PR.exitWriteAccess();
            }
        }

        public Comparator<? super Node> getComparator() {
            return this.comp;
        }

        @Override
        Collection<? extends Entry> createEntries(java.util.Map<T, Node> map) {
            TreeSet<Map.ME> l = new TreeSet<Map.ME>(new SMComparator());
            for (Map.Entry<T, Node> e : map.entrySet()) {
                l.add(new Map.ME(e.getKey(), e.getValue()));
            }
            return l;
        }

        final class SMComparator
        implements Comparator<Map.ME> {
            SMComparator() {
            }

            @Override
            public int compare(Map.ME me1, Map.ME me2) {
                Comparator c = SortedMap.this.comp;
                if (c == null) {
                    int r = ((Comparable)me1.key).compareTo(me2.key);
                    return r;
                }
                return c.compare(me1.node, me2.node);
            }
        }

    }

    public static class SortedArray
    extends Array {
        private Comparator<? super Node> comp;

        public SortedArray() {
        }

        protected SortedArray(Collection<Node> c) {
            super(c);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void setComparator(Comparator<? super Node> c) {
            try {
                PR.enterWriteAccess();
                this.comp = c;
                this.refresh();
            }
            finally {
                PR.exitWriteAccess();
            }
        }

        public Comparator<? super Node> getComparator() {
            return this.comp;
        }

        @Override
        Entry createNodesEntry() {
            return new SAE();
        }

        private final class SAE
        implements Entry {
            @Override
            public Collection<Node> nodes(Object source) {
                ArrayList<Node> al = new ArrayList<Node>(SortedArray.this.getCollection());
                Collections.sort(al, SortedArray.this.comp);
                return al;
            }
        }

    }

    public static class Map<T>
    extends Children {
        protected java.util.Map<T, Node> nodes;

        protected Map(java.util.Map<T, Node> m) {
            this.nodes = m;
        }

        public Map() {
        }

        final java.util.Map<T, Node> getMap() {
            if (this.nodes == null) {
                this.nodes = this.initMap();
            }
            return this.nodes;
        }

        @Override
        final void callAddNotify() {
            this.entrySupport().setEntries(this.createEntries(this.getMap()), true);
            super.callAddNotify();
        }

        Collection<? extends Entry> createEntries(java.util.Map<T, Node> map) {
            LinkedList<ME> l = new LinkedList<ME>();
            for (Map.Entry<T, Node> e : map.entrySet()) {
                l.add(new ME(e.getKey(), e.getValue()));
            }
            return l;
        }

        final void refreshImpl() {
            this.entrySupport().setEntries(this.createEntries(this.getMap()));
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected final void refresh() {
            try {
                PR.enterWriteAccess();
                this.refreshImpl();
            }
            finally {
                PR.exitWriteAccess();
            }
        }

        final void refreshKeyImpl(T key) {
            this.entrySupport().refreshEntry(new ME(key, null));
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected final void refreshKey(T key) {
            try {
                PR.enterWriteAccess();
                this.refreshKeyImpl(key);
            }
            finally {
                PR.exitWriteAccess();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected final void putAll(java.util.Map<? extends T, ? extends Node> map) {
            try {
                PR.enterWriteAccess();
                this.getMap().putAll(map);
                this.refreshImpl();
            }
            finally {
                PR.exitWriteAccess();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected final void put(T key, Node node) {
            block4 : {
                try {
                    PR.enterWriteAccess();
                    if (this.getMap().put(key, node) != null) {
                        this.refreshKeyImpl(key);
                        break block4;
                    }
                    this.refreshImpl();
                }
                finally {
                    PR.exitWriteAccess();
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected final void removeAll(Collection<? extends T> keys) {
            try {
                PR.enterWriteAccess();
                this.getMap().keySet().removeAll(keys);
                this.refreshImpl();
            }
            finally {
                PR.exitWriteAccess();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected void remove(T key) {
            try {
                PR.enterWriteAccess();
                if (this.nodes != null && this.nodes.remove(key) != null) {
                    this.refreshImpl();
                }
            }
            finally {
                PR.exitWriteAccess();
            }
        }

        protected java.util.Map<T, Node> initMap() {
            return new HashMap(7);
        }

        @Override
        public boolean add(Node[] arr) {
            return false;
        }

        @Override
        public boolean remove(Node[] arr) {
            return false;
        }

        static final class ME
        implements Entry {
            public Object key;
            public Node node;

            public ME(Object key, Node node) {
                this.key = key;
                this.node = node;
            }

            @Override
            public Collection<Node> nodes(Object source) {
                return Collections.singleton(this.node);
            }

            public int hashCode() {
                return this.key.hashCode();
            }

            public boolean equals(Object o) {
                if (o instanceof ME) {
                    ME me = (ME)o;
                    return this.key.equals(me.key);
                }
                return false;
            }

            public String toString() {
                return "Key (" + this.key + ")";
            }
        }

    }

    public static class Array
    extends Children
    implements Cloneable {
        Entry nodesEntry;
        protected Collection<Node> nodes;
        private static final Object COLLECTION_LOCK = new Object();

        protected Array(Collection<Node> c) {
            this();
            this.nodes = c;
        }

        public Array() {
            this(false);
        }

        Array(boolean lazy) {
            super(lazy);
            if (!lazy) {
                this.nodesEntry = this.createNodesEntry();
            }
        }

        @Override
        void postInitializeEntrySupport(EntrySupport es) {
            if (!this.lazySupport) {
                if (this.getNodesEntry() == null) {
                    this.nodesEntry = this.createNodesEntry();
                }
                es.setEntries(Collections.singleton(this.getNodesEntry()), true);
            } else if (this.getNodesEntry() != null) {
                this.nodesEntry = null;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public Object clone() {
            try {
                Array ar;
                ar = (Array)super.clone();
                try {
                    PR.enterReadAccess();
                    if (this.nodes != null) {
                        ar.nodes = ar.initCollection();
                        ar.nodes.clear();
                        for (Node n : this.nodes) {
                            ar.nodes.add(n.cloneNode());
                        }
                    }
                }
                finally {
                    PR.exitReadAccess();
                }
                return ar;
            }
            catch (CloneNotSupportedException e) {
                throw new InternalError();
            }
        }

        protected Collection<Node> initCollection() {
            return new ArrayList<Node>();
        }

        final void refreshImpl() {
            if (this.isInitialized()) {
                this.entrySupport().refreshEntry(this.getNodesEntry());
                this.entrySupport().getNodes(false);
            } else if (this.nodes != null) {
                for (Node n : this.nodes) {
                    n.assignTo(this, -1);
                }
            }
        }

        protected final void refresh() {
            this.checkSupport();
            if (this.lazySupport) {
                return;
            }
            MUTEX.postWriteRequest(new Runnable(){

                @Override
                public void run() {
                    Array.this.refreshImpl();
                }
            });
        }

        final Entry getNodesEntry() {
            return this.nodesEntry;
        }

        Entry createNodesEntry() {
            return new AE();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        final Collection<Node> getCollection() {
            Object object = COLLECTION_LOCK;
            synchronized (object) {
                if (this.nodes == null) {
                    this.nodes = this.initCollection();
                }
            }
            return this.nodes;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public boolean add(Node[] arr) {
            Object object = COLLECTION_LOCK;
            synchronized (object) {
                if (!this.getCollection().addAll(Arrays.asList(arr))) {
                    return false;
                }
            }
            this.refresh();
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public boolean remove(Node[] arr) {
            Object object = COLLECTION_LOCK;
            synchronized (object) {
                if (!this.getCollection().removeAll(Arrays.asList(arr))) {
                    return false;
                }
            }
            this.refresh();
            return true;
        }

        private final class AE
        implements Entry {
            AE() {
            }

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public Collection<Node> nodes(Object source) {
                Collection<Node> c = Array.this.getCollection();
                if (c.isEmpty()) {
                    return Collections.emptyList();
                }
                Object object = COLLECTION_LOCK;
                synchronized (object) {
                    return new ArrayList<Node>(c);
                }
            }

            public String toString() {
                return "Children.Array.AE" + Array.this.getCollection();
            }
        }

    }

    private static final class Empty
    extends Children {
        Empty() {
        }

        @Override
        public boolean add(Node[] nodes) {
            return false;
        }

        @Override
        public boolean remove(Node[] nodes) {
            return false;
        }
    }

    static interface Entry {
        public Collection<Node> nodes(Object var1);
    }

}

