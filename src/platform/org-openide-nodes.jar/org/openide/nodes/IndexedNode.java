/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.openide.nodes;

import java.awt.Component;
import java.awt.Container;
import javax.swing.JPanel;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Index;
import org.openide.nodes.IndexedCustomizer;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

public class IndexedNode
extends AbstractNode {
    private Index indexImpl;

    public IndexedNode() {
        super(new Index.ArrayChildren());
        this.indexImpl = (Index)((Object)this.getChildren());
    }

    protected IndexedNode(Children children, Index indexImpl) {
        super(children);
        this.indexImpl = indexImpl;
    }

    protected IndexedNode(Children children, Index indexImpl, Lookup lookup) {
        super(children, lookup);
        this.indexImpl = indexImpl;
    }

    @Override
    public boolean hasCustomizer() {
        return false;
    }

    @Override
    public Component getCustomizer() {
        JPanel c = new JPanel();
        IndexedCustomizer customizer = new IndexedCustomizer(c, false);
        customizer.setObject(this.indexImpl);
        return c;
    }

    @Override
    public <T extends Node.Cookie> T getCookie(Class<T> clazz) {
        if (clazz.isInstance(this.indexImpl)) {
            return (T)((Node.Cookie)clazz.cast(this.indexImpl));
        }
        Children ch = this.getChildren();
        if (clazz.isInstance(ch)) {
            return (T)((Node.Cookie)clazz.cast(ch));
        }
        return super.getCookie(clazz);
    }
}

