/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Privileged
 */
package org.openide.nodes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.TMUtil;
import org.openide.util.Mutex;

public interface Index
extends Node.Cookie {
    public int getNodesCount();

    public Node[] getNodes();

    public int indexOf(Node var1);

    public void reorder();

    public void reorder(int[] var1);

    public void move(int var1, int var2);

    public void exchange(int var1, int var2);

    public void moveUp(int var1);

    public void moveDown(int var1);

    public void addChangeListener(ChangeListener var1);

    public void removeChangeListener(ChangeListener var1);

    public static abstract class KeysChildren<T>
    extends Children.Keys<T> {
        private Index support;
        protected final List<T> list;

        public KeysChildren(List<T> ar) {
            this.list = ar;
            this.update();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Index getIndex() {
            KeysChildren keysChildren = this;
            synchronized (keysChildren) {
                if (this.support == null) {
                    this.support = this.createIndex();
                }
                return this.support;
            }
        }

        protected Index createIndex() {
            return new Support(){

                @Override
                public Node[] getNodes() {
                    List<Node> l = Arrays.asList(KeysChildren.this.getNodes());
                    if (KeysChildren.this.nodes != null) {
                        l.removeAll(KeysChildren.this.nodes);
                    }
                    return l.toArray(new Node[l.size()]);
                }

                @Override
                public int getNodesCount() {
                    return KeysChildren.this.list.size();
                }

                @Override
                public void reorder(int[] perm) {
                    KeysChildren.this.reorder(perm);
                    KeysChildren.this.update();
                    this.fireChangeEvent(new ChangeEvent(this));
                }
            };
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected void reorder(int[] perm) {
            Object object = this.lock();
            synchronized (object) {
                ArrayList<T> n = new ArrayList<T>(this.list);
                for (int i = 0; i < n.size(); ++i) {
                    this.list.set(perm[i], n.get(i));
                }
            }
        }

        protected Object lock() {
            return this.list;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public final void update() {
            ArrayList<T> keys;
            Object object = this.lock();
            synchronized (object) {
                keys = new ArrayList<T>(this.list);
            }
            Children.Keys.super.setKeys(keys);
        }

    }

    public static class ArrayChildren
    extends Children.Array
    implements Index {
        protected Index support;

        public ArrayChildren() {
            this(null);
        }

        private ArrayChildren(List<Node> ar) {
            super(ar);
            this.support = new Support(){

                @Override
                public Node[] getNodes() {
                    return ArrayChildren.this.getNodes();
                }

                @Override
                public int getNodesCount() {
                    return ArrayChildren.this.getNodesCount();
                }

                @Override
                public void reorder(int[] perm) {
                    ArrayChildren.this.reorder(perm);
                    this.fireChangeEvent(new ChangeEvent(ArrayChildren.this));
                }
            };
        }

        protected List<Node> initCollection() {
            return new ArrayList<Node>();
        }

        @Override
        public void reorder(final int[] perm) {
            MUTEX.postWriteRequest(new Runnable(){

                @Override
                public void run() {
                    Node[] n = ArrayChildren.this.nodes.toArray(new Node[ArrayChildren.this.nodes.size()]);
                    List l = (List)ArrayChildren.this.nodes;
                    for (int i = 0; i < n.length; ++i) {
                        l.set(perm[i], n[i]);
                    }
                    ArrayChildren.this.refresh();
                }
            });
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void reorder() {
            try {
                PR.enterReadAccess();
                Support.showIndexedCustomizer(this);
            }
            finally {
                PR.exitReadAccess();
            }
        }

        @Override
        public void move(int x, int y) {
            this.support.move(x, y);
        }

        @Override
        public void exchange(int x, int y) {
            this.support.exchange(x, y);
        }

        @Override
        public void moveUp(int x) {
            this.support.exchange(x, x - 1);
        }

        @Override
        public void moveDown(int x) {
            this.support.exchange(x, x + 1);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public int indexOf(Node node) {
            try {
                PR.enterReadAccess();
                int n = ((List)this.nodes).indexOf(node);
                return n;
            }
            finally {
                PR.exitReadAccess();
            }
        }

        @Override
        public void addChangeListener(ChangeListener chl) {
            this.support.addChangeListener(chl);
        }

        @Override
        public void removeChangeListener(ChangeListener chl) {
            this.support.removeChangeListener(chl);
        }

    }

    public static abstract class Support
    implements Index {
        private HashSet<ChangeListener> listeners;

        @Override
        public void move(int x, int y) {
            int[] perm = new int[this.getNodesCount()];
            if (x == y) {
                return;
            }
            for (int i = 0; i < perm.length; ++i) {
                perm[i] = i < x && i < y || i > x && i > y ? i : (i > x && i < y ? i - 1 : i + 1);
            }
            perm[x] = y;
            perm[y] = x < y ? y - 1 : y + 1;
            this.reorder(perm);
        }

        @Override
        public void exchange(int x, int y) {
            int[] perm = new int[this.getNodesCount()];
            int i = 0;
            while (i < perm.length) {
                perm[i] = i++;
            }
            perm[x] = y;
            perm[y] = x;
            this.reorder(perm);
        }

        @Override
        public void moveUp(int x) {
            this.exchange(x, x - 1);
        }

        @Override
        public void moveDown(int x) {
            this.exchange(x, x + 1);
        }

        @Override
        public void addChangeListener(ChangeListener chl) {
            if (this.listeners == null) {
                this.listeners = new HashSet();
            }
            this.listeners.add(chl);
        }

        @Override
        public void removeChangeListener(ChangeListener chl) {
            if (this.listeners == null) {
                return;
            }
            this.listeners.remove(chl);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected void fireChangeEvent(ChangeEvent che) {
            HashSet cloned;
            if (this.listeners == null) {
                return;
            }
            Support support = this;
            synchronized (support) {
                cloned = (HashSet)this.listeners.clone();
            }
            Iterator iter = cloned.iterator();
            while (iter.hasNext()) {
                ((ChangeListener)iter.next()).stateChanged(che);
            }
        }

        @Override
        public abstract Node[] getNodes();

        @Override
        public int indexOf(Node node) {
            Node[] arr = this.getNodes();
            for (int i = 0; i < arr.length; ++i) {
                if (!node.equals(arr[i])) continue;
                return i;
            }
            return -1;
        }

        @Override
        public void reorder() {
            Support.showIndexedCustomizer(this);
        }

        public static void showIndexedCustomizer(Index idx) {
            TMUtil.showIndexedCustomizer(idx);
        }

        @Override
        public abstract int getNodesCount();

        @Override
        public abstract void reorder(int[] var1);
    }

}

