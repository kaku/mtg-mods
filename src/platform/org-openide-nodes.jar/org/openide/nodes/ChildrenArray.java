/*
 * Decompiled with CFR 0_118.
 */
package org.openide.nodes;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.nodes.Children;
import org.openide.nodes.EntrySupportDefault;
import org.openide.nodes.Node;
import org.openide.nodes.NodeAdapter;
import org.openide.nodes.NodeOp;

final class ChildrenArray
extends NodeAdapter {
    public EntrySupportDefault entrySupport;
    private Node[] nodes;
    private Map<EntrySupportDefault.Info, Collection<Node>> map;
    private static final Logger LOGGER = Logger.getLogger(ChildrenArray.class.getName());

    public Children getChildren() {
        return this.entrySupport == null ? null : this.entrySupport.children;
    }

    public Node[] nodes() {
        if (this.entrySupport == null) {
            return null;
        }
        if (this.nodes == null) {
            this.nodes = this.entrySupport.justComputeNodes();
            for (int i = 0; i < this.nodes.length; ++i) {
                this.nodes[i].reassignTo(this.entrySupport.children, this);
            }
            this.entrySupport.registerChildrenArray(this, this.nodes.length > 0);
        }
        return this.nodes;
    }

    public void clear() {
        if (this.nodes != null) {
            this.nodes = null;
            this.entrySupport.registerChildrenArray(this, false);
        }
    }

    void remove(EntrySupportDefault.Info info) {
        this.map.remove(info);
    }

    public boolean isInitialized() {
        return this.nodes != null;
    }

    private String logInfo(EntrySupportDefault.Info info) {
        return info.toString() + '[' + Integer.toHexString(System.identityHashCode(info)) + ']';
    }

    public synchronized Collection<Node> nodesFor(EntrySupportDefault.Info info, boolean hasToExist) {
        boolean IS_LOG = LOGGER.isLoggable(Level.FINE);
        if (IS_LOG) {
            LOGGER.finer("nodesFor(" + this.logInfo(info) + ") on " + Thread.currentThread());
        }
        if (this.map == null) {
            assert (!hasToExist);
            this.map = new WeakHashMap<EntrySupportDefault.Info, Collection<Node>>(7);
        }
        Collection<Node> nodes = this.map.get(info);
        if (IS_LOG) {
            LOGGER.finer("  map size=" + this.map.size() + ", nodes=" + nodes);
        }
        if (nodes == null) {
            assert (!hasToExist);
            try {
                nodes = info.entry.nodes(null);
            }
            catch (RuntimeException ex) {
                NodeOp.warning(ex);
                nodes = Collections.emptyList();
            }
            if (nodes == null) {
                nodes = Collections.emptyList();
                LOGGER.warning("Null returned by " + info.entry + " (" + info.entry.getClass().getName() + ")");
            }
            info.length = nodes.size();
            this.map.put(info, nodes);
            if (IS_LOG) {
                LOGGER.finer("  created nodes=" + nodes);
            }
        }
        if (IS_LOG) {
            LOGGER.finer("  leaving nodesFor(" + this.logInfo(info) + ") on " + Thread.currentThread());
        }
        return nodes;
    }

    public synchronized void useNodes(EntrySupportDefault.Info info, Collection<Node> list) {
        if (this.map == null) {
            this.map = new WeakHashMap<EntrySupportDefault.Info, Collection<Node>>(7);
        }
        info.length = list.size();
        this.map.put(info, list);
    }

    public String toString() {
        return Object.super.toString() + "  " + this.getChildren();
    }
}

