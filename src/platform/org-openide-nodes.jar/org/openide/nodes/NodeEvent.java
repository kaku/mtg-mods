/*
 * Decompiled with CFR 0_118.
 */
package org.openide.nodes;

import java.util.EventObject;
import org.openide.nodes.Node;

public class NodeEvent
extends EventObject {
    static final long serialVersionUID = 3504069382061188226L;

    public NodeEvent(Node n) {
        super(n);
    }

    public final Node getNode() {
        return (Node)this.getSource();
    }
}

