/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Privileged
 */
package org.openide.nodes;

import java.lang.reflect.Array;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.EntrySupport;
import org.openide.nodes.EntrySupportLazyState;
import org.openide.nodes.Node;
import org.openide.util.Mutex;

class EntrySupportLazy
extends EntrySupport {
    private static final int prefetchCount = Math.max(Integer.getInteger("org.openide.explorer.VisualizerNode.prefetchCount", 50), 0);
    static final Logger LOGGER = Logger.getLogger(EntrySupportLazy.class.getName());
    protected final AtomicReference<EntrySupportLazyState> internal = new AtomicReference<EntrySupportLazyState>(EntrySupportLazyState.UNINITIALIZED);
    protected final Object LOCK = new Object();
    private int snapshotCount;

    public EntrySupportLazy(Children ch) {
        super(ch);
    }

    private void setState(EntrySupportLazyState old, EntrySupportLazyState s) {
        assert (Thread.holdsLock(this.LOCK));
        boolean success = this.internal.compareAndSet(old, s);
        assert (success);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean checkInit() {
        Object newState;
        EntrySupportLazyState state;
        boolean doInit = false;
        Object object = this.LOCK;
        synchronized (object) {
            state = this.internal.get();
            if (state.isInited()) {
                return true;
            }
            if (!state.isInitInProgress()) {
                doInit = true;
                newState = state.changeProgress(true).changeThread(Thread.currentThread());
                this.setState(state, (EntrySupportLazyState)newState);
                state = newState;
            }
        }
        boolean LOG_ENABLED = LOGGER.isLoggable(Level.FINER);
        if (doInit) {
            if (LOG_ENABLED) {
                LOGGER.finer("Initialize " + this + " on " + Thread.currentThread());
                LOGGER.finer("    callAddNotify()");
            }
            try {
                this.children.callAddNotify();
            }
            finally {
                newState = this.LOCK;
                synchronized (newState) {
                    class Notify
                    implements Runnable {
                        public Notify(EntrySupportLazyState old) {
                            EntrySupportLazyState s = EntrySupportLazy.this.internal.get();
                            EntrySupportLazy.this.setState(s, s.changeInited(true));
                        }

                        /*
                         * WARNING - Removed try catching itself - possible behaviour change.
                         */
                        @Override
                        public void run() {
                            Object object = EntrySupportLazy.this.LOCK;
                            synchronized (object) {
                                EntrySupportLazyState s = EntrySupportLazy.this.internal.get();
                                if (!s.isInited()) {
                                    throw new IllegalStateException();
                                }
                                EntrySupportLazy.this.setState(s, s.changeThread(null));
                                EntrySupportLazy.this.LOCK.notifyAll();
                            }
                        }
                    }
                    Notify notify = new Notify(state);
                    if (Children.MUTEX.isReadAccess()) {
                        Children.MUTEX.postWriteRequest((Runnable)notify);
                    } else {
                        notify.run();
                    }
                }
            }
        }
        if (Children.MUTEX.isReadAccess() || Children.MUTEX.isWriteAccess() || state.initThread() == Thread.currentThread()) {
            if (LOG_ENABLED) {
                LOGGER.log(Level.FINER, "Cannot wait for finished initialization " + this + " on " + Thread.currentThread() + " read access: " + Children.MUTEX.isReadAccess() + " write access: " + Children.MUTEX.isWriteAccess() + " initThread: " + state.initThread());
            }
            this.notifySetEntries();
            return false;
        }
        newState = this.LOCK;
        synchronized (newState) {
            EntrySupportLazyState current;
            while ((current = this.internal.get()).initThread() != null) {
                try {
                    this.LOCK.wait();
                }
                catch (InterruptedException ex) {}
            }
        }
        return true;
    }

    final int getSnapshotCount() {
        assert (Thread.holdsLock(this.LOCK));
        return this.snapshotCount;
    }

    final void incrementCount() {
        assert (Thread.holdsLock(this.LOCK));
        ++this.snapshotCount;
    }

    final void decrementCount() {
        assert (Thread.holdsLock(this.LOCK));
        ++this.snapshotCount;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    List<Node> snapshot() {
        this.checkInit();
        try {
            Children.PR.enterReadAccess();
            LazySnapshot lazySnapshot = this.createSnapshot();
            return lazySnapshot;
        }
        finally {
            Children.PR.exitReadAccess();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void registerNode(int delta, EntrySupportLazyState.EntryInfo who) {
        if (delta == -1) {
            try {
                Children.PR.enterWriteAccess();
                boolean zero = false;
                LOGGER.finer("register node");
                Object object = this.LOCK;
                synchronized (object) {
                    EntrySupportLazyState state = this.internal.get();
                    int cnt = 0;
                    boolean found = false;
                    if ((cnt += this.getSnapshotCount()) == 0) {
                        for (Children.Entry entry : EntrySupportLazy.notNull(state.getVisibleEntries())) {
                            EntrySupportLazyState.EntryInfo info = state.getEntryToInfo().get(entry);
                            if (info.currentNode() != null) {
                                ++cnt;
                                break;
                            }
                            if (info != who) continue;
                            found = true;
                        }
                    }
                    boolean bl = zero = cnt == 0 && (found || who == null);
                    if (zero) {
                        this.setState(state, state.changeInited(false).changeThread(null).changeProgress(false));
                        if (this.children.getEntrySupport() == this) {
                            if (LOGGER.isLoggable(Level.FINER)) {
                                LOGGER.finer("callRemoveNotify() " + this);
                            }
                            this.children.callRemoveNotify();
                        }
                    }
                }
            }
            finally {
                Children.PR.exitWriteAccess();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Node getNodeAt(int index) {
        if (!this.checkInit()) {
            return null;
        }
        Node node = null;
        do {
            try {
                Children.PR.enterReadAccess();
                EntrySupportLazyState state = this.internal.get();
                List<Children.Entry> e = EntrySupportLazy.notNull(state.getVisibleEntries());
                if (index >= e.size()) {
                    Node node2 = node;
                    return node2;
                }
                Children.Entry entry = e.get(index);
                EntrySupportLazyState.EntryInfo info = state.getEntryToInfo().get(entry);
                node = info.getNode();
                if (!EntrySupportLazy.isDummyNode(node)) {
                    Node node3 = node;
                    return node3;
                }
                this.hideEmpty(null, entry);
            }
            finally {
                Children.PR.exitReadAccess();
            }
        } while (!Children.MUTEX.isReadAccess());
        return node;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Node[] getNodes(boolean optimalResult) {
        Node[] tmpNodes;
        if (!this.checkInit()) {
            return new Node[0];
        }
        Node holder = null;
        if (optimalResult) {
            holder = this.children.findChild(null);
        }
        Children.LOG.log(Level.FINEST, "findChild returns: {0}", holder);
        Children.LOG.log(Level.FINEST, "after findChild: {0}", optimalResult);
        do {
            HashSet<Children.Entry> invalidEntries = null;
            tmpNodes = null;
            try {
                Children.PR.enterReadAccess();
                EntrySupportLazyState state = this.internal.get();
                List<Children.Entry> e = EntrySupportLazy.notNull(state.getVisibleEntries());
                ArrayList<Node> toReturn = new ArrayList<Node>(e.size());
                for (Children.Entry entry : e) {
                    EntrySupportLazyState.EntryInfo info = state.getEntryToInfo().get(entry);
                    assert (!info.isHidden());
                    Node node = info.getNode();
                    if (EntrySupportLazy.isDummyNode(node)) {
                        if (invalidEntries == null) {
                            invalidEntries = new HashSet<Children.Entry>();
                        }
                        invalidEntries.add(entry);
                    }
                    toReturn.add(node);
                }
                tmpNodes = toReturn.toArray(new Node[0]);
                if (invalidEntries == null) {
                    Node[] i$ = tmpNodes;
                    return i$;
                }
                this.hideEmpty(invalidEntries, null);
            }
            finally {
                Children.PR.exitReadAccess();
            }
        } while (!Children.MUTEX.isReadAccess());
        return tmpNodes;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Node[] testNodes() {
        ArrayList<Node> created;
        EntrySupportLazyState state = this.internal.get();
        if (!state.isInited()) {
            return null;
        }
        created = new ArrayList<Node>();
        try {
            Children.PR.enterReadAccess();
            for (Children.Entry entry : EntrySupportLazy.notNull(state.getVisibleEntries())) {
                EntrySupportLazyState.EntryInfo info = state.getEntryToInfo().get(entry);
                Node node = info.currentNode();
                if (node == null) continue;
                created.add(node);
            }
        }
        finally {
            Children.PR.exitReadAccess();
        }
        return created.isEmpty() ? null : created.toArray(new Node[created.size()]);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int getNodesCount(boolean optimalResult) {
        this.checkInit();
        try {
            Children.PR.enterReadAccess();
            EntrySupportLazyState state = this.internal.get();
            int n = EntrySupportLazy.notNull(state.getVisibleEntries()).size();
            return n;
        }
        finally {
            Children.PR.exitReadAccess();
        }
    }

    @Override
    public boolean isInitialized() {
        EntrySupportLazyState state = this.internal.get();
        return state.isInited();
    }

    Children.Entry entryForNode(Node key) {
        EntrySupportLazyState state = this.internal.get();
        for (Map.Entry<Children.Entry, EntrySupportLazyState.EntryInfo> entry : state.getEntryToInfo().entrySet()) {
            if (entry.getValue().currentNode() != key) continue;
            return entry.getKey();
        }
        return null;
    }

    static boolean isDummyNode(Node node) {
        return node.getClass() == DummyNode.class;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    void refreshEntry(Children.Entry entry) {
        EntrySupportLazyState[] stateHolder;
        assert (Children.MUTEX.isWriteAccess());
        boolean LOG_ENABLED = LOGGER.isLoggable(Level.FINER);
        if (LOG_ENABLED) {
            LOGGER.finer("refreshEntry() " + this);
            LOGGER.finer("    entry: " + entry);
        }
        if (!(stateHolder = new EntrySupportLazyState[]{this.internal.get()})[0].isInited()) {
            return;
        }
        EntrySupportLazyState.EntryInfo info = stateHolder[0].getEntryToInfo().get(entry);
        if (info == null) {
            if (LOG_ENABLED) {
                LOGGER.finer("    no such entry: " + entry);
            }
            return;
        }
        Node oldNode = info.currentNode();
        EntrySupportLazyState.EntryInfo newInfo = null;
        Node newNode = null;
        if (info.isHidden()) {
            newNode = info.getNode(true, null);
            newInfo = info.changeIndex(-1);
        } else {
            newInfo = info.changeNode(null);
            newNode = newInfo.getNode(true, null);
        }
        boolean newIsDummy = EntrySupportLazy.isDummyNode(newNode);
        if (newIsDummy && info.isHidden()) {
            return;
        }
        if (newNode.equals(oldNode)) {
            return;
        }
        if (!info.isHidden() || newIsDummy) {
            this.removeEntries(stateHolder, null, entry, newInfo, true, true);
        }
        if (newIsDummy) {
            return;
        }
        EntrySupportLazyState state = stateHolder[0];
        HashMap<Children.Entry, EntrySupportLazyState.EntryInfo> new2Info = new HashMap<Children.Entry, EntrySupportLazyState.EntryInfo>(state.getEntryToInfo());
        int index = 0;
        int changedIndex = -1;
        ArrayList<Children.Entry> arr = new ArrayList<Children.Entry>();
        for (Children.Entry tmpEntry : state.getEntries()) {
            EntrySupportLazyState.EntryInfo tmpInfo = null;
            if (tmpEntry.equals(entry)) {
                tmpInfo = newInfo;
                changedIndex = index;
            }
            if (tmpInfo == null) {
                tmpInfo = state.getEntryToInfo().get(tmpEntry);
            }
            if (tmpInfo.isHidden()) continue;
            new2Info.put(tmpEntry, tmpInfo.changeIndex(index++));
            arr.add(tmpEntry);
        }
        assert (changedIndex != -1);
        Object i$ = this.LOCK;
        synchronized (i$) {
            this.setState(state, state.changeEntries(null, arr, new2Info));
        }
        this.fireSubNodesChangeIdx(true, new int[]{changedIndex}, entry, this.createSnapshot(), null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    void notifySetEntries() {
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.finer("notifySetEntries() " + this);
        }
        Object object = this.LOCK;
        synchronized (object) {
            EntrySupportLazyState state = this.internal.get();
            this.setState(state, state.changeMustNotify(true));
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Converted monitor instructions to comments
     * Lifted jumps to return sites
     */
    @Override
    void setEntries(Collection<? extends Children.Entry> newEntries, boolean noCheck) {
        int addIdx;
        EntrySupportLazyState newState;
        int[] idxs;
        EntrySupportLazyState state;
        assert (Children.MUTEX.isWriteAccess());
        do {
            EntrySupportLazyState[] stateHolder;
            Set<Children.Entry> entriesToRemove;
            if ((entriesToRemove = this.setEntriesSimple(stateHolder = new EntrySupportLazyState[]{null}, newEntries)) == null) {
                return;
            }
            if (!entriesToRemove.isEmpty()) {
                this.removeEntries(stateHolder, entriesToRemove, null, null, false, false);
            }
            List<Children.Entry> toAdd = this.updateOrder(stateHolder, newEntries);
            state = stateHolder[0];
            if (toAdd.isEmpty()) return;
            ArrayList<Children.Entry> newStateEntries = new ArrayList<Children.Entry>(newEntries);
            idxs = new int[toAdd.size()];
            addIdx = 0;
            int inx = 0;
            boolean createNodes = toAdd.size() == 2 && prefetchCount > 0;
            ArrayList<Children.Entry> newStateVisibleEntries = new ArrayList<Children.Entry>();
            HashMap<Children.Entry, EntrySupportLazyState.EntryInfo> newState2Info = new HashMap<Children.Entry, EntrySupportLazyState.EntryInfo>(state.getEntryToInfo());
            for (int i2 = 0; i2 < newStateEntries.size(); ++i2) {
                Children.Entry entry = newStateEntries.get(i2);
                EntrySupportLazyState.EntryInfo info = newState2Info.get(entry);
                if (info == null) {
                    Node n;
                    info = new EntrySupportLazyState.EntryInfo(this, entry);
                    if (createNodes && EntrySupportLazy.isDummyNode(n = info.getNode())) {
                        newState2Info.put(entry, info.changeIndex(-2));
                        continue;
                    }
                    idxs[addIdx++] = inx;
                }
                if (info.isHidden()) continue;
                newState2Info.put(entry, info.changeIndex(inx++));
                newStateVisibleEntries.add(entry);
            }
            Object i2 = this.LOCK;
            // MONITORENTER : i2
            newState = state.changeEntries(newStateEntries, newStateVisibleEntries, newState2Info);
            if (this.internal.get() == state) break;
            state = this.internal.get();
            // MONITOREXIT : i2
        } while (true);
        this.setState(state, newState);
        // MONITOREXIT : i2
        if (addIdx == 0) {
            return;
        }
        if (idxs.length != addIdx) {
            int[] tmp = new int[addIdx];
            for (int i = 0; i < tmp.length; ++i) {
                tmp[i] = idxs[i];
            }
            idxs = tmp;
        }
        this.fireSubNodesChangeIdx(true, idxs, null, this.createSnapshot(), null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private List<Children.Entry> updateOrder(EntrySupportLazyState[] stateHolder, Collection<? extends Children.Entry> newEntries) {
        assert (Children.MUTEX.isWriteAccess());
        EntrySupportLazyState state = stateHolder[0];
        LinkedList<Children.Entry> toAdd = new LinkedList<Children.Entry>();
        int[] perm = new int[state.getVisibleEntries().size()];
        int currentPos = 0;
        int permSize = 0;
        LinkedList<Children.Entry> reorderedEntries = null;
        ArrayList<Children.Entry> newVisible = null;
        HashMap<Children.Entry, EntrySupportLazyState.EntryInfo> new2Infos = null;
        Map<Children.Entry, EntrySupportLazyState.EntryInfo> old2Infos = state.getEntryToInfo();
        for (Children.Entry entry : newEntries) {
            EntrySupportLazyState.EntryInfo info = old2Infos.get(entry);
            if (info == null) {
                toAdd.add(entry);
                continue;
            }
            if (reorderedEntries == null) {
                reorderedEntries = new LinkedList<Children.Entry>();
                newVisible = new ArrayList<Children.Entry>();
                new2Infos = new HashMap<Children.Entry, EntrySupportLazyState.EntryInfo>(old2Infos);
            }
            reorderedEntries.add(entry);
            if (info.isHidden()) continue;
            newVisible.add(entry);
            int oldPos = info.getIndex();
            if (currentPos != oldPos) {
                new2Infos.put(entry, info.changeIndex(currentPos));
                perm[oldPos] = 1 + currentPos;
                ++permSize;
            }
            ++currentPos;
        }
        if (permSize > 0) {
            for (int i22 = 0; i22 < perm.length; ++i22) {
                if (perm[i22] == 0) {
                    perm[i22] = i22;
                    continue;
                }
                int[] arrn = perm;
                int n = i22;
                arrn[n] = arrn[n] - 1;
            }
            Object i22 = this.LOCK;
            synchronized (i22) {
                EntrySupportLazyState newState = state.changeEntries(reorderedEntries, newVisible, new2Infos);
                this.setState(state, newState);
                stateHolder[0] = newState;
            }
            Node p = this.children.parent;
            if (p != null) {
                p.fireReorderChange(perm);
            }
        }
        return toAdd;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    Node getNode(Children.Entry entry) {
        this.checkInit();
        try {
            Children.PR.enterReadAccess();
            EntrySupportLazyState state = this.internal.get();
            EntrySupportLazyState.EntryInfo info = state.getEntryToInfo().get(entry);
            if (info == null) {
                if (LOGGER.isLoggable(Level.FINER)) {
                    LOGGER.finer("getNode() " + this);
                    LOGGER.finer("    no such entry: " + entry);
                }
                Node node = null;
                return node;
            }
            Node node = info.getNode();
            Node node2 = EntrySupportLazy.isDummyNode(node) ? null : node;
            return node2;
        }
        finally {
            Children.PR.exitReadAccess();
        }
    }

    protected void fireSubNodesChangeIdx(boolean added, int[] idxs, Children.Entry sourceEntry, List<Node> current, List<Node> previous) {
        if (this.children.parent != null && this.children.getEntrySupport() == this) {
            this.children.parent.fireSubNodesChangeIdx(added, idxs, sourceEntry, current, previous);
        }
    }

    static <T> List<T> notNull(List<T> it) {
        if (it == null) {
            return Collections.emptyList();
        }
        return it;
    }

    static String dumpEntriesInfos(List<Children.Entry> entries, Map<Children.Entry, EntrySupportLazyState.EntryInfo> entryToInfo) {
        StringBuilder sb = new StringBuilder();
        int cnt = 0;
        for (Children.Entry entry : entries) {
            sb.append("\n").append(++cnt).append(" entry ").append(entry).append(" -> ").append(entryToInfo.get(entry));
        }
        sb.append("\n\n");
        for (Map.Entry e : entryToInfo.entrySet()) {
            if (entries.contains(e.getKey())) {
                sb.append("\n").append(" contained ").append(e.getValue());
                continue;
            }
            sb.append("\n").append(" missing   ").append(e.getValue()).append(" for ").append(e.getKey());
        }
        return sb.toString();
    }

    @Override
    protected List<Children.Entry> getEntries() {
        EntrySupportLazyState state = this.internal.get();
        return state.getEntries();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    private Set<Children.Entry> setEntriesSimple(EntrySupportLazyState[] stateHolder, Collection<? extends Children.Entry> newEntries) {
        EntrySupportLazyState state;
        do {
            state = stateHolder[0] = this.internal.get();
            boolean LOG_ENABLED = LOGGER.isLoggable(Level.FINER);
            if (LOG_ENABLED) {
                LOGGER.finer("setEntries(): " + this);
                LOGGER.finer("    inited: " + state.isInited());
                LOGGER.finer("    mustNotifySetEnties: " + state.isMustNotify());
                LOGGER.finer("    newEntries size: " + newEntries.size() + " data:" + newEntries);
                LOGGER.finer("    entries size: " + state.getEntries().size() + " data:" + state.getEntries());
                LOGGER.finer("    visibleEntries size: " + EntrySupportLazy.notNull(state.getVisibleEntries()).size() + " data:" + state.getVisibleEntries());
                LOGGER.finer("    entryToInfo size: " + state.getEntryToInfo().size());
            }
            int entriesSize = 0;
            int entryToInfoSize = 0;
            assert ((entriesSize = state.getEntries().size()) >= 0);
            assert ((entryToInfoSize = state.getEntryToInfo().size()) >= 0);
            assert (state.getEntries().size() == state.getEntryToInfo().size());
            if (state.isMustNotify() || state.isInited()) break;
            ArrayList<Children.Entry> newStateEntries = new ArrayList<Children.Entry>(newEntries);
            ArrayList<Children.Entry> newStateVisibleEntries = new ArrayList<Children.Entry>(newEntries);
            HashMap<Children.Entry, EntrySupportLazyState.EntryInfo> newState2Info = new HashMap<Children.Entry, EntrySupportLazyState.EntryInfo>();
            Map<Children.Entry, EntrySupportLazyState.EntryInfo> oldState2Info = state.getEntryToInfo();
            for (Children.Entry entry : newEntries) {
                EntrySupportLazyState.EntryInfo prev = oldState2Info.get(entry);
                if (prev == null) continue;
                newState2Info.put(entry, prev);
            }
            for (int i2 = 0; i2 < newStateEntries.size(); ++i2) {
                Children.Entry entry2 = newStateEntries.get(i2);
                EntrySupportLazyState.EntryInfo info = (EntrySupportLazyState.EntryInfo)newState2Info.get(entry2);
                if (info == null) {
                    info = new EntrySupportLazyState.EntryInfo(this, entry2);
                }
                newState2Info.put(entry2, info.changeIndex(i2));
            }
            Object i2 = this.LOCK;
            synchronized (i2) {
                if (state == this.internal.get()) {
                    EntrySupportLazyState newState = state.changeEntries(newStateEntries, newStateVisibleEntries, newState2Info);
                    this.setState(state, newState);
                    stateHolder[0] = newState;
                    return null;
                }
            }
        } while (true);
        HashSet<Children.Entry> entriesToRemove = new HashSet<Children.Entry>(state.getEntries());
        entriesToRemove.removeAll(newEntries);
        return entriesToRemove;
    }

    void hideEmpty(final Set<Children.Entry> entries, final Children.Entry entry) {
        Children.MUTEX.postWriteRequest(new Runnable(){

            @Override
            public void run() {
                EntrySupportLazyState[] stateHolder = new EntrySupportLazyState[]{EntrySupportLazy.this.internal.get()};
                EntrySupportLazy.this.removeEntries(stateHolder, entries, entry, null, true, true);
            }
        });
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void removeEntries(EntrySupportLazyState[] stateHolder, Set<Children.Entry> entriesToRemove, Children.Entry entryToRemove, EntrySupportLazyState.EntryInfo newEntryInfo, boolean justHide, boolean delayed) {
        assert (Children.MUTEX.isWriteAccess());
        boolean LOG_ENABLED = LOGGER.isLoggable(Level.FINER);
        if (LOG_ENABLED) {
            LOGGER.finer("removeEntries(): " + this);
            LOGGER.finer("    entriesToRemove: " + entriesToRemove);
            LOGGER.finer("    entryToRemove: " + entryToRemove);
            LOGGER.finer("    newEntryInfo: " + newEntryInfo);
            LOGGER.finer("    justHide: " + justHide);
            LOGGER.finer("    delayed: " + delayed);
        }
        int index = 0;
        int removedIdx = 0;
        int removedNodesIdx = 0;
        int expectedSize = entriesToRemove != null ? entriesToRemove.size() : 1;
        int[] idxs = new int[expectedSize];
        EntrySupportLazyState state = stateHolder[0];
        List<Children.Entry> previousEntries = state.getVisibleEntries();
        HashMap<Children.Entry, EntrySupportLazyState.EntryInfo> previousInfos = null;
        HashMap<Children.Entry, EntrySupportLazyState.EntryInfo> new2Infos = null;
        ArrayList<Children.Entry> newEntries = justHide ? null : new ArrayList<Children.Entry>();
        Node[] removedNodes = null;
        ArrayList<Children.Entry> newStateVisibleEntries = new ArrayList<Children.Entry>();
        Map<Children.Entry, EntrySupportLazyState.EntryInfo> oldState2Info = state.getEntryToInfo();
        for (Children.Entry entry : state.getEntries()) {
            EntrySupportLazyState.EntryInfo info = oldState2Info.get(entry);
            if (info == null) continue;
            boolean remove = entriesToRemove != null ? entriesToRemove.remove(entry) : entryToRemove.equals(entry);
            if (remove) {
                if (info.isHidden()) {
                    if (justHide) continue;
                    if (new2Infos == null) {
                        new2Infos = new HashMap<Children.Entry, EntrySupportLazyState.EntryInfo>(oldState2Info);
                    }
                    new2Infos.remove(entry);
                    continue;
                }
                idxs[removedIdx++] = info.getIndex();
                if (previousInfos == null) {
                    previousInfos = new HashMap<Children.Entry, EntrySupportLazyState.EntryInfo>(oldState2Info);
                }
                Node node = info.currentNode();
                if (!info.isHidden() && node != null && !EntrySupportLazy.isDummyNode(node)) {
                    if (removedNodes == null) {
                        removedNodes = new Node[expectedSize];
                    }
                    removedNodes[removedNodesIdx++] = node;
                }
                if (new2Infos == null) {
                    new2Infos = new HashMap<Children.Entry, EntrySupportLazyState.EntryInfo>(oldState2Info);
                }
                if (justHide) {
                    EntrySupportLazyState.EntryInfo dup = newEntryInfo != null ? newEntryInfo : info.changeNode(null);
                    new2Infos.put(info.entry(), dup.changeIndex(-2));
                    continue;
                }
                new2Infos.remove(entry);
                continue;
            }
            if (new2Infos == null) {
                new2Infos = new HashMap<Children.Entry, EntrySupportLazyState.EntryInfo>(oldState2Info);
            }
            if (!info.isHidden()) {
                newStateVisibleEntries.add(info.entry());
                new2Infos.put(info.entry(), info.changeIndex(index++));
            } else {
                new2Infos.put(info.entry(), info.changeIndex(-2));
            }
            if (justHide) continue;
            newEntries.add(info.entry());
        }
        if (!justHide) {
            // empty if block
        }
        Object i$ = this.LOCK;
        synchronized (i$) {
            EntrySupportLazyState newState = state.changeEntries(newEntries, newStateVisibleEntries, new2Infos);
            this.setState(state, newState);
            stateHolder[0] = newState;
        }
        if (removedIdx == 0) {
            return;
        }
        if (removedIdx < idxs.length) {
            idxs = (int[])EntrySupportLazy.resizeArray(idxs, removedIdx);
        }
        LazySnapshot curSnapshot = this.createSnapshot(newStateVisibleEntries, new HashMap<Children.Entry, EntrySupportLazyState.EntryInfo>(new2Infos), delayed);
        LazySnapshot prevSnapshot = this.createSnapshot(previousEntries, previousInfos, false);
        this.fireSubNodesChangeIdx(false, idxs, entryToRemove, curSnapshot, prevSnapshot);
        if (removedNodesIdx > 0) {
            if (removedNodesIdx < removedNodes.length) {
                removedNodes = (Node[])EntrySupportLazy.resizeArray(removedNodes, removedNodesIdx);
            }
            if (this.children.parent != null) {
                for (Node node : removedNodes) {
                    node.deassignFrom(this.children);
                    node.fireParentNodeChange(this.children.parent, null);
                }
            }
            this.children.destroyNodes(removedNodes);
        }
    }

    private static Object resizeArray(Object oldArray, int newSize) {
        int oldSize = Array.getLength(oldArray);
        Class elementType = oldArray.getClass().getComponentType();
        Object newArray = Array.newInstance(elementType, newSize);
        int preserveLength = Math.min(oldSize, newSize);
        if (preserveLength > 0) {
            System.arraycopy(oldArray, 0, newArray, 0, preserveLength);
        }
        return newArray;
    }

    LazySnapshot createSnapshot() {
        EntrySupportLazyState state = this.internal.get();
        return this.createSnapshot(state.getVisibleEntries(), new HashMap<Children.Entry, EntrySupportLazyState.EntryInfo>(state.getEntryToInfo()), false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected LazySnapshot createSnapshot(List<Children.Entry> entries, Map<Children.Entry, EntrySupportLazyState.EntryInfo> e2i, boolean delayed) {
        Object object = this.LOCK;
        synchronized (object) {
            return delayed ? new DelayedLazySnapshot(entries, e2i) : new LazySnapshot(entries, e2i);
        }
    }

    final class DelayedLazySnapshot
    extends LazySnapshot {
        public DelayedLazySnapshot(List<Children.Entry> entries, Map<Children.Entry, EntrySupportLazyState.EntryInfo> e2i) {
            super(entries, e2i);
        }
    }

    class LazySnapshot
    extends AbstractList<Node> {
        final List<Children.Entry> entries;
        final Map<Children.Entry, EntrySupportLazyState.EntryInfo> entryToInfo;

        public LazySnapshot(List<Children.Entry> entries, Map<Children.Entry, EntrySupportLazyState.EntryInfo> e2i) {
            EntrySupportLazy.this.incrementCount();
            this.entries = entries;
            assert (entries != null);
            Map map = this.entryToInfo = e2i != null ? e2i : Collections.emptyMap();
            assert (entries.size() <= this.entryToInfo.size());
        }

        @Override
        public Node get(int index) {
            Children.Entry entry = this.entries.get(index);
            return this.get(entry);
        }

        Node get(Children.Entry entry) {
            EntrySupportLazyState.EntryInfo info = this.entryToInfo.get(entry);
            Node node = info.getNode();
            if (EntrySupportLazy.isDummyNode(node)) {
                EntrySupportLazy.this.hideEmpty(null, entry);
            }
            return node;
        }

        @Override
        public String toString() {
            return this.entries.toString();
        }

        @Override
        public int size() {
            return this.entries.size();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected void finalize() throws Throwable {
            boolean unregister = false;
            Object object = EntrySupportLazy.this.LOCK;
            synchronized (object) {
                EntrySupportLazy.this.decrementCount();
                if (EntrySupportLazy.this.getSnapshotCount() == 0) {
                    unregister = true;
                }
            }
            if (unregister) {
                EntrySupportLazy.this.registerNode(-1, null);
            }
        }
    }

    static class DummyNode
    extends AbstractNode {
        public DummyNode() {
            super(Children.LEAF);
        }
    }

}

