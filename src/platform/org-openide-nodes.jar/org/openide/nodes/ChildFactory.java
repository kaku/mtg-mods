/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.openide.nodes;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.List;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

public abstract class ChildFactory<T> {
    private Reference<Observer> observer = null;

    protected Node createNodeForKey(T key) {
        throw new AssertionError((Object)("Neither createNodeForKey() nor createNodesForKey() overridden in " + this.getClass().getName()));
    }

    protected Node[] createNodesForKey(T key) {
        Node[] arrnode;
        Node n = this.createNodeForKey(key);
        if (n == null) {
            arrnode = null;
        } else {
            Node[] arrnode2 = new Node[1];
            arrnode = arrnode2;
            arrnode2[0] = n;
        }
        return arrnode;
    }

    protected abstract boolean createKeys(List<T> var1);

    protected final void refresh(boolean immediate) {
        Observer obs;
        Observer observer = obs = this.observer == null ? null : this.observer.get();
        if (obs != null) {
            obs.refresh(immediate);
        }
    }

    Node getWaitNode() {
        Node n = this.createWaitNode();
        return n == null ? null : new WaitFilterNode(n);
    }

    protected Node createWaitNode() {
        AbstractNode n = new AbstractNode(Children.LEAF){

            @Override
            public Action[] getActions(boolean context) {
                return new Action[0];
            }
        };
        n.setIconBaseWithExtension("org/openide/nodes/wait.gif");
        n.setDisplayName(NbBundle.getMessage(ChildFactory.class, (String)"LBL_WAIT"));
        return n;
    }

    final void setObserver(Observer observer) {
        if (this.observer != null) {
            throw new IllegalStateException("Attempting to create two Children objects for a single ChildFactory " + this + ".  Use " + "FilterNode.Children over the existing Children object " + "instead");
        }
        this.observer = new WeakReference<Observer>(observer);
    }

    void removeNotify() {
    }

    void addNotify() {
    }

    static boolean isWaitNode(Object n) {
        return n instanceof WaitFilterNode;
    }

    public static abstract class Detachable<T>
    extends ChildFactory<T> {
        @Override
        protected void addNotify() {
        }

        @Override
        protected void removeNotify() {
        }
    }

    private static final class WaitFilterNode
    extends FilterNode {
        public WaitFilterNode(Node orig) {
            super(orig);
        }
    }

    static interface Observer {
        public void refresh(boolean var1);
    }

}

