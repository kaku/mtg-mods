/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 */
package org.openide.nodes;

import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.beans.Customizer;
import java.lang.reflect.Method;
import java.util.Hashtable;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.cookies.InstanceCookie;
import org.openide.nodes.Index;
import org.openide.nodes.IndexedCustomizer;
import org.openide.nodes.Node;
import org.openide.nodes.NodeOp;
import org.openide.nodes.NodeOperation;
import org.openide.util.Lookup;
import org.openide.util.Mutex;

abstract class TMUtil {
    private static final ThreadLocal<Object> TALK = new ThreadLocal();
    private static Hashtable<String, Object> algorithms = new Hashtable(10);
    private static Frame owner;

    TMUtil() {
    }

    private static Class loadClass(String className) throws Exception {
        ClassLoader loader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        if (loader == null) {
            loader = NodeOperation.class.getClassLoader();
        }
        return Class.forName(className, true, loader);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static Node.Cookie createInstanceCookie(Object bean) {
        try {
            TALK.set(bean);
            Node.Cookie cookie = TMUtil.exec("Bean") ? (Node.Cookie)TALK.get() : null;
            return cookie;
        }
        finally {
            TALK.set(null);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static Dialog createDialog(Object maybeDialogDescriptor) {
        try {
            TALK.set(maybeDialogDescriptor);
            Dialog dialog = TMUtil.exec("Dial") ? (Dialog)TALK.get() : null;
            return dialog;
        }
        finally {
            TALK.set(null);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void attachCustomizer(Node node, Customizer cust) {
        try {
            TALK.set(new Object[]{node, cust});
            TMUtil.exec("Cust");
        }
        finally {
            TALK.set(null);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static Frame mainWindow() {
        try {
            if (TMUtil.exec("Win")) {
                Frame frame = (Frame)TALK.get();
                return frame;
            }
            if (owner == null) {
                owner = (Frame)new JDialog().getOwner();
            }
            Frame frame = owner;
            return frame;
        }
        finally {
            TALK.set(null);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static ListCellRenderer findListCellRenderer() {
        try {
            if (TMUtil.exec("Rend")) {
                ListCellRenderer listCellRenderer = (ListCellRenderer)TALK.get();
                return listCellRenderer;
            }
            DefaultListCellRenderer defaultListCellRenderer = new DefaultListCellRenderer();
            return defaultListCellRenderer;
        }
        finally {
            TALK.set(null);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void showIndexedCustomizer(Index idx) {
        try {
            TALK.set(idx);
            if (!TMUtil.exec("IndexC")) {
                final IndexedCustomizer ic = new IndexedCustomizer();
                ic.setObject(idx);
                ic.setImmediateReorder(false);
                Mutex.EVENT.readAccess((Mutex.Action)new Mutex.Action<Void>(){

                    public Void run() {
                        ic.setVisible(true);
                        return null;
                    }
                });
            }
        }
        finally {
            TALK.set(null);
        }
    }

    private static boolean exec(String name) {
        Object obj = algorithms.get(name);
        if (obj == null) {
            try {
                Class c = Class.forName("org.openide.nodes.TMUtil$" + name);
                obj = c.newInstance();
            }
            catch (ClassNotFoundException ex) {
                obj = ex;
                NodeOp.exception(ex);
            }
            catch (InstantiationException ex) {
                obj = ex;
            }
            catch (IllegalAccessException ex) {
                obj = ex;
                NodeOp.exception(ex);
            }
            catch (NoClassDefFoundError ex) {
                obj = ex;
            }
            algorithms.put(name, obj);
        }
        try {
            if (obj instanceof Runnable) {
                ((Runnable)obj).run();
                return true;
            }
        }
        catch (NoClassDefFoundError ex) {
            algorithms.put(name, ex);
        }
        return false;
    }

    static final class IndexC
    implements Runnable {
        IndexC() {
        }

        @Override
        public void run() {
            Index idx = (Index)TALK.get();
            JPanel p = new JPanel();
            IndexedCustomizer ic = new IndexedCustomizer(p, false);
            ic.setObject(idx);
            ic.setImmediateReorder(false);
            DialogDescriptor dd = new DialogDescriptor((Object)p, Node.getString("LAB_order"));
            dd.setModal(true);
            dd.setOptionType(-1);
            Object result = DialogDisplayer.getDefault().notify((NotifyDescriptor)dd);
            if (result == DialogDescriptor.OK_OPTION) {
                ic.doClose();
            }
        }
    }

    static final class Rend
    implements Runnable {
        private static Class nodeRenderer;

        Rend() {
        }

        @Override
        public void run() {
            try {
                if (nodeRenderer == null) {
                    nodeRenderer = TMUtil.loadClass("org.openide.explorer.view.NodeRenderer");
                }
                TALK.set(nodeRenderer.newInstance());
            }
            catch (Exception ex) {
                throw new IllegalStateException(ex.getMessage());
            }
        }
    }

    static final class Win
    implements Runnable {
        private static Method getDefault;
        private static Method getMainWindow;

        Win() {
        }

        @Override
        public void run() {
            try {
                if (getDefault == null) {
                    Class wm = TMUtil.loadClass("org.openide.windows.WindowManager");
                    getDefault = wm.getMethod("getDefault", new Class[0]);
                    getMainWindow = wm.getMethod("getMainWindow", new Class[0]);
                }
                Object[] param = new Object[]{};
                TALK.set(getMainWindow.invoke(getDefault.invoke(null, param), param));
            }
            catch (Exception ex) {
                throw new IllegalStateException(ex.getMessage());
            }
        }
    }

    static final class Cust
    implements Runnable {
        private static Class<?> nodeCustomizer;
        private static Method attach;

        Cust() {
        }

        @Override
        public void run() {
            try {
                if (nodeCustomizer == null) {
                    nodeCustomizer = TMUtil.loadClass("org.openide.explorer.propertysheet.editors.NodeCustomizer");
                    attach = nodeCustomizer.getMethod("attach", Node.class);
                }
                Object[] arr = (Object[])TALK.get();
                Node n = (Node)arr[0];
                Object cust = arr[1];
                if (nodeCustomizer.isInstance(cust)) {
                    attach.invoke(cust, n);
                }
            }
            catch (Exception ex) {
                throw new IllegalStateException(ex.getMessage());
            }
        }
    }

    static final class Dial
    implements Runnable {
        Dial() {
        }

        @Override
        public void run() {
            Object obj = TALK.get();
            if (obj instanceof DialogDescriptor) {
                TALK.set(DialogDisplayer.getDefault().createDialog((DialogDescriptor)obj));
            } else {
                TALK.set(null);
            }
        }
    }

    static final class Bean
    implements Runnable,
    InstanceCookie {
        private Object bean;

        Bean() {
        }

        @Override
        public void run() {
            Bean n = new Bean();
            n.bean = TALK.get();
            TALK.set(n);
        }

        @Override
        public String instanceName() {
            return this.bean.getClass().getName();
        }

        public Class instanceClass() {
            return this.bean.getClass();
        }

        @Override
        public Object instanceCreate() {
            return this.bean;
        }
    }

}

