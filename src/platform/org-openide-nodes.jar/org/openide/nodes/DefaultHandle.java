/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Privileged
 */
package org.openide.nodes;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeNotFoundException;
import org.openide.util.Mutex;

public final class DefaultHandle
implements Node.Handle {
    private static final long serialVersionUID = -8739127064355983273L;
    private Node.Handle parent;
    private String path;

    DefaultHandle(Node.Handle parent, String path) {
        this.parent = parent;
        this.path = path;
    }

    @Override
    public Node getNode() throws IOException {
        Node parentNode = this.parent.getNode();
        Node child = parentNode.getChildren().findChild(this.path);
        if (child != null) {
            return child;
        }
        throw new NodeNotFoundException(parentNode, this.path, 0);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static DefaultHandle createHandle(Node node) {
        try {
            Children.PR.enterReadAccess();
            String childPath = node.getName();
            if (childPath == null) {
                DefaultHandle defaultHandle = null;
                return defaultHandle;
            }
            Node parentNode = node.getParentNode();
            if (parentNode == null) {
                DefaultHandle defaultHandle = null;
                return defaultHandle;
            }
            Node foundChild = parentNode.getChildren().findChild(childPath);
            if (foundChild != node) {
                Logger.getLogger(DefaultHandle.class.getName()).log(Level.WARNING, "parent could not find own child: node={0} parentNode={1} childPath={2} foundChild={3}", new Object[]{node, parentNode, childPath, foundChild});
                DefaultHandle defaultHandle = null;
                return defaultHandle;
            }
            Node.Handle parentHandle = parentNode.getHandle();
            if (parentHandle == null) {
                DefaultHandle defaultHandle = null;
                return defaultHandle;
            }
            DefaultHandle defaultHandle = new DefaultHandle(parentHandle, childPath);
            return defaultHandle;
        }
        finally {
            Children.PR.exitReadAccess();
        }
    }

    public String toString() {
        return "DefaultHandle[" + this.parent + "|" + this.path + "]";
    }
}

