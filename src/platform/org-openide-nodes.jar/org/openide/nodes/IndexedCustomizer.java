/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.datatransfer.ExTransferable
 *  org.openide.util.datatransfer.ExTransferable$Single
 */
package org.openide.nodes;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.Autoscroll;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragGestureRecognizer;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.dnd.InvalidDnDOperationException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.Customizer;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.openide.nodes.Index;
import org.openide.nodes.Node;
import org.openide.nodes.TMUtil;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.datatransfer.ExTransferable;

@Deprecated
public final class IndexedCustomizer
extends JDialog
implements Customizer {
    static final long serialVersionUID = -8731362267771694641L;
    private JList control;
    private JButton buttonUp;
    private JButton buttonDown;
    private JButton buttonClose;
    private Index index;
    private Node[] nodes;
    private boolean immediateReorder = true;
    private int[] permutation;
    private ChangeListener nodeChangesL;

    public IndexedCustomizer() {
        this(null, true);
    }

    IndexedCustomizer(Container p, boolean closeButton) {
        super(TMUtil.mainWindow(), true);
        if (closeButton) {
            this.setDefaultCloseOperation(2);
            this.getRootPane().registerKeyboardAction(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent evt) {
                    IndexedCustomizer.this.setVisible(false);
                    IndexedCustomizer.this.dispose();
                }
            }, KeyStroke.getKeyStroke(27, 0, true), 2);
            this.setTitle(Node.getString("LAB_order"));
        }
        if (p == null) {
            p = this.getContentPane();
        }
        p.setLayout(new GridBagLayout());
        JLabel l = new JLabel(Node.getString("LAB_listOrder"));
        l.setDisplayedMnemonic(Node.getString("LAB_listOrder_Mnemonic").charAt(0));
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.anchor = 17;
        constraints.insets = new Insets(12, 12, 2, 12);
        p.add((Component)l, constraints);
        this.control = new AutoscrollJList();
        l.setLabelFor(this.control);
        this.control.addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (IndexedCustomizer.this.control.isSelectionEmpty()) {
                    IndexedCustomizer.this.buttonUp.setEnabled(false);
                    IndexedCustomizer.this.buttonDown.setEnabled(false);
                } else {
                    int i = IndexedCustomizer.this.control.getSelectedIndex();
                    if (i > 0) {
                        IndexedCustomizer.this.buttonUp.setEnabled(true);
                    } else {
                        IndexedCustomizer.this.buttonUp.setEnabled(false);
                    }
                    if (i < IndexedCustomizer.this.nodes.length - 1) {
                        IndexedCustomizer.this.buttonDown.setEnabled(true);
                    } else {
                        IndexedCustomizer.this.buttonDown.setEnabled(false);
                    }
                }
            }
        });
        this.control.setCellRenderer(new IndexedListCellRenderer());
        this.control.setVisibleRowCount(15);
        this.control.setSelectionMode(0);
        constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.fill = 1;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        constraints.insets = new Insets(0, 12, 11, 11);
        p.add((Component)new JScrollPane(this.control), constraints);
        JPanel bb = new JPanel();
        if (closeButton) {
            this.buttonClose = new JButton(Node.getString("Button_close"));
            this.buttonClose.setMnemonic(Node.getString("Button_close_Mnemonic").charAt(0));
        }
        this.buttonUp = new JButton(Node.getString("Button_up"));
        this.buttonUp.setMnemonic(Node.getString("Button_up_Mnemonic").charAt(0));
        this.buttonDown = new JButton(Node.getString("Button_down"));
        this.buttonDown.setMnemonic(Node.getString("Button_down_Mnemonic").charAt(0));
        bb.setLayout(new GridBagLayout());
        constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.anchor = 11;
        constraints.fill = 2;
        constraints.insets = new Insets(0, 0, 5, 11);
        bb.add((Component)this.buttonUp, constraints);
        constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.anchor = 11;
        constraints.fill = 2;
        constraints.weighty = 1.0;
        constraints.insets = new Insets(0, 0, 0, 11);
        bb.add((Component)this.buttonDown, constraints);
        if (closeButton) {
            constraints = new GridBagConstraints();
            constraints.gridx = 0;
            constraints.gridy = 2;
            constraints.anchor = 15;
            constraints.fill = 2;
            constraints.insets = new Insets(0, 0, 11, 11);
            bb.add((Component)this.buttonClose, constraints);
        }
        this.buttonUp.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                int i = IndexedCustomizer.this.control.getSelectedIndex();
                IndexedCustomizer.this.moveUp(i);
                IndexedCustomizer.this.updateList();
                IndexedCustomizer.this.control.setSelectedIndex(i - 1);
                IndexedCustomizer.this.control.ensureIndexIsVisible(i - 1);
                IndexedCustomizer.this.control.repaint();
            }
        });
        this.buttonDown.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                int i = IndexedCustomizer.this.control.getSelectedIndex();
                IndexedCustomizer.this.moveDown(i);
                IndexedCustomizer.this.updateList();
                IndexedCustomizer.this.control.setSelectedIndex(i + 1);
                IndexedCustomizer.this.control.ensureIndexIsVisible(i + 1);
                IndexedCustomizer.this.control.repaint();
            }
        });
        if (closeButton) {
            this.buttonClose.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    IndexedCustomizer.this.doClose();
                    IndexedCustomizer.this.dispose();
                }
            });
        }
        this.buttonUp.setEnabled(false);
        this.buttonDown.setEnabled(false);
        constraints = new GridBagConstraints();
        constraints.gridx = 1;
        constraints.gridy = 1;
        constraints.fill = 3;
        p.add((Component)bb, constraints);
        if (closeButton) {
            this.pack();
            this.setBounds(Utilities.findCenterBounds((Dimension)this.getSize()));
            this.buttonClose.requestFocus();
            this.buttonClose.getAccessibleContext().setAccessibleDescription(Node.getString("ACSD_Button_close"));
        }
        this.buttonUp.getAccessibleContext().setAccessibleDescription(Node.getString("ACSD_Button_up"));
        this.buttonDown.getAccessibleContext().setAccessibleDescription(Node.getString("ACSD_Button_down"));
        this.control.getAccessibleContext().setAccessibleDescription(Node.getString("ACSD_ListOrder"));
        p.getAccessibleContext().setAccessibleDescription(Node.getString("ACSD_IndexedCustomizer"));
        this.getAccessibleContext().setAccessibleDescription(Node.getString("ACSD_IndexedCustomizer"));
    }

    void doClose() {
        if (!this.immediateReorder && this.index != null && this.permutation != null) {
            int[] realPerm = new int[this.permutation.length];
            int i = 0;
            while (i < realPerm.length) {
                realPerm[this.permutation[i]] = i++;
            }
            this.index.reorder(realPerm);
        }
    }

    private void updateList() {
        if (this.index == null) {
            return;
        }
        Node[] localNodes = this.index.getNodes();
        if (!this.immediateReorder) {
            this.getPermutation();
            int origLength = this.permutation.length;
            int newLength = localNodes.length;
            if (origLength < newLength) {
                this.nodes = new Node[newLength];
                int[] newPerm = new int[newLength];
                System.arraycopy(newPerm, 0, this.permutation, 0, origLength);
                for (int i = 0; i < newLength; ++i) {
                    if (i < origLength) {
                        this.nodes[i] = localNodes[this.permutation[i]];
                        continue;
                    }
                    this.nodes[i] = localNodes[i];
                    newPerm[i] = i;
                }
                this.permutation = newPerm;
            } else if (origLength > newLength) {
                this.nodes = new Node[newLength];
                this.permutation = new int[newLength];
                int i = 0;
                while (i < newLength) {
                    this.nodes[i] = localNodes[i];
                    this.permutation[i] = i++;
                }
            } else {
                this.nodes = new Node[newLength];
                for (int i = 0; i < newLength; ++i) {
                    this.nodes[i] = localNodes[this.permutation[i]];
                }
            }
        } else {
            this.nodes = (Node[])localNodes.clone();
        }
        this.control.setListData(this.nodes);
        if (this.nodes.length > 0 && this.control.getSelectedIndex() == -1) {
            this.control.setSelectedIndex(0);
        }
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(300, super.getPreferredSize().height);
    }

    public boolean isImmediateReorder() {
        return this.immediateReorder;
    }

    public void setImmediateReorder(boolean immediateReorder) {
        if (this.immediateReorder == immediateReorder) {
            return;
        }
        this.immediateReorder = immediateReorder;
        if (immediateReorder && this.permutation != null) {
            this.index.reorder(this.permutation);
            this.permutation = null;
            this.updateList();
        }
    }

    @Override
    public void setObject(Object bean) {
        if (!(bean instanceof Index)) {
            throw new IllegalArgumentException();
        }
        this.index = (Index)bean;
        this.nodeChangesL = new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent ev) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        IndexedCustomizer.this.updateList();
                    }
                });
            }

        };
        this.updateList();
        this.control.invalidate();
        this.validate();
        this.index.addChangeListener(WeakListeners.change((ChangeListener)this.nodeChangesL, (Object)this.index));
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
    }

    private void moveUp(int position) {
        if (this.index == null) {
            return;
        }
        if (this.immediateReorder) {
            this.index.moveUp(position);
        } else {
            this.getPermutation();
            int temp = this.permutation[position];
            this.permutation[position] = this.permutation[position - 1];
            this.permutation[position - 1] = temp;
        }
    }

    private void moveDown(int position) {
        if (this.index == null) {
            return;
        }
        if (this.immediateReorder) {
            this.index.moveDown(position);
        } else {
            this.getPermutation();
            int temp = this.permutation[position];
            this.permutation[position] = this.permutation[position + 1];
            this.permutation[position + 1] = temp;
        }
    }

    private int[] getPermutation() {
        if (this.permutation == null) {
            if (this.nodes == null) {
                this.nodes = (Node[])this.index.getNodes().clone();
            }
            this.permutation = new int[this.nodes.length];
            int i = 0;
            while (i < this.nodes.length) {
                this.permutation[i] = i++;
            }
        }
        return this.permutation;
    }

    void performReorder(int[] perm, int selected) {
        if (this.immediateReorder) {
            this.index.reorder(perm);
        } else {
            int[] reversed = new int[perm.length];
            int i = 0;
            while (i < reversed.length) {
                reversed[perm[i]] = i++;
            }
            int[] orig = this.getPermutation();
            this.permutation = new int[orig.length];
            for (int i2 = 0; i2 < orig.length; ++i2) {
                this.permutation[i2] = orig[reversed[i2]];
            }
        }
        this.updateList();
        this.control.setSelectedIndex(selected);
        this.control.repaint();
    }

    private static class AutoscrollJList
    extends JList
    implements Autoscroll {
        static final long serialVersionUID = 5495776972406885734L;
        Insets scrollInsets;
        Insets realInsets;
        JViewport viewport;

        AutoscrollJList() {
        }

        @Override
        public void autoscroll(Point cursorLoc) {
            JViewport viewport = this.getViewport();
            Point viewPos = viewport.getViewPosition();
            int viewHeight = viewport.getExtentSize().height;
            if (cursorLoc.y - viewPos.y <= this.realInsets.top) {
                viewport.setViewPosition(new Point(viewPos.x, Math.max(viewPos.y - this.realInsets.top, 0)));
            } else if (viewPos.y + viewHeight - cursorLoc.y <= this.realInsets.bottom) {
                viewport.setViewPosition(new Point(viewPos.x, Math.min(viewPos.y + this.realInsets.bottom, this.getHeight() - viewHeight)));
            }
        }

        @Override
        public Insets getAutoscrollInsets() {
            if (this.scrollInsets == null) {
                int height = this.getHeight();
                this.scrollInsets = new Insets(height, 0, height, 0);
                this.realInsets = new Insets(15, 0, 15, 0);
            }
            return this.scrollInsets;
        }

        JViewport getViewport() {
            if (this.viewport == null) {
                Container comp;
                for (comp = this; !(comp instanceof JViewport) && comp != null; comp = comp.getParent()) {
                }
                this.viewport = (JViewport)comp;
            }
            return this.viewport;
        }
    }

    private static final class IndexedListCellRenderer
    implements ListCellRenderer {
        static final long serialVersionUID = -5526451942677242944L;
        protected static Border hasFocusBorder = new LineBorder(UIManager.getColor("List.focusCellHighlight"));
        private ListCellRenderer delegate = TMUtil.findListCellRenderer();
        int dragIndex = -1;

        IndexedListCellRenderer() {
        }

        public void draggingEnter(int index, Point startingLoc, Point currentLoc) {
            this.dragIndex = index;
        }

        public void draggingOver(int index, Point startingLoc, Point currentLoc) {
        }

        public void draggingExit() {
            this.dragIndex = -1;
        }

        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            JComponent result = (JComponent)this.delegate.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            if (index == this.dragIndex) {
                result.setBorder(hasFocusBorder);
            }
            return result;
        }
    }

    private static final class IndexTransferable
    extends ExTransferable.Single {
        int index;

        IndexTransferable(DataFlavor flavor, int index) {
            super(flavor);
            this.index = index;
        }

        protected Object getData() throws IOException, UnsupportedFlavorException {
            return String.valueOf(this.index);
        }
    }

    private static final class IndexedDropTarget
    implements DropTargetListener {
        JList comp;
        IndexedListCellRenderer cellRenderer;
        IndexedCustomizer dialog;
        IndexedDragSource ids;
        int lastIndex = -1;

        IndexedDropTarget(IndexedCustomizer dialog, IndexedDragSource ids) {
            this.dialog = dialog;
            this.comp = dialog.control;
            this.cellRenderer = (IndexedListCellRenderer)this.comp.getCellRenderer();
            this.ids = ids;
            new DropTarget(this.comp, 2, this, true);
        }

        @Override
        public void dragEnter(DropTargetDragEvent dtde) {
            if (!this.checkConditions(dtde)) {
                dtde.rejectDrag();
            } else {
                this.lastIndex = this.comp.locationToIndex(dtde.getLocation());
                this.cellRenderer.draggingEnter(this.lastIndex, this.ids.getDragGestureEvent().getDragOrigin(), dtde.getLocation());
                this.comp.repaint(this.comp.getCellBounds(this.lastIndex, this.lastIndex));
            }
        }

        @Override
        public void dragOver(DropTargetDragEvent dtde) {
            if (!this.checkConditions(dtde)) {
                dtde.rejectDrag();
                if (this.lastIndex >= 0) {
                    this.cellRenderer.draggingExit();
                    this.comp.repaint(this.comp.getCellBounds(this.lastIndex, this.lastIndex));
                    this.lastIndex = -1;
                }
            } else {
                dtde.acceptDrag(2);
                int index = this.comp.locationToIndex(dtde.getLocation());
                if (this.lastIndex == index) {
                    this.cellRenderer.draggingOver(index, this.ids.getDragGestureEvent().getDragOrigin(), dtde.getLocation());
                } else {
                    if (this.lastIndex < 0) {
                        this.lastIndex = index;
                    }
                    this.cellRenderer.draggingExit();
                    this.cellRenderer.draggingEnter(index, this.ids.getDragGestureEvent().getDragOrigin(), dtde.getLocation());
                    this.comp.repaint(this.comp.getCellBounds(this.lastIndex, index));
                    this.lastIndex = index;
                }
            }
        }

        @Override
        public void dropActionChanged(DropTargetDragEvent dtde) {
        }

        @Override
        public void dragExit(DropTargetEvent dte) {
            if (this.lastIndex >= 0) {
                this.cellRenderer.draggingExit();
                this.comp.repaint(this.comp.getCellBounds(this.lastIndex, this.lastIndex));
            }
        }

        @Override
        public void drop(DropTargetDropEvent dtde) {
            int target;
            if (2 != dtde.getDropAction() || !dtde.isLocalTransfer()) {
                dtde.rejectDrop();
            }
            if ((target = this.comp.locationToIndex(dtde.getLocation())) < 0) {
                dtde.rejectDrop();
                return;
            }
            Transferable t = dtde.getTransferable();
            dtde.acceptDrop(2);
            try {
                int source = Integer.parseInt((String)t.getTransferData(this.ids.myFlavor));
                if (source != target) {
                    this.performReorder(source, target);
                    dtde.dropComplete(true);
                } else {
                    dtde.dropComplete(false);
                }
            }
            catch (IOException exc) {
                dtde.dropComplete(false);
            }
            catch (UnsupportedFlavorException exc) {
                dtde.dropComplete(false);
            }
            catch (NumberFormatException exc) {
                dtde.dropComplete(false);
            }
        }

        void performReorder(int source, int target) {
            int[] myPerm = new int[this.comp.getModel().getSize()];
            int i = 0;
            while (i < Math.min(source, target)) {
                myPerm[i] = i++;
            }
            i = Math.max(source, target) + 1;
            while (i < myPerm.length) {
                myPerm[i] = i++;
            }
            myPerm[source] = target;
            if (source > target) {
                for (i = target; i < source; ++i) {
                    myPerm[i] = i + 1;
                }
            } else {
                for (i = source + 1; i < target + 1; ++i) {
                    myPerm[i] = i - 1;
                }
            }
            this.dialog.performReorder(myPerm, target);
        }

        boolean checkConditions(DropTargetDragEvent dtde) {
            int index = this.comp.locationToIndex(dtde.getLocation());
            return 2 == dtde.getDropAction() && index >= 0;
        }
    }

    private static final class IndexedDragSource
    implements DragGestureListener,
    DragSourceListener {
        JList comp;
        DragGestureEvent dge;
        DataFlavor myFlavor;

        IndexedDragSource(JList comp) {
            this.comp = comp;
            DragSource ds = DragSource.getDefaultDragSource();
            ds.createDefaultDragGestureRecognizer(comp, 2, this);
        }

        @Override
        public void dragGestureRecognized(DragGestureEvent dge) {
            if ((dge.getDragAction() & 2) == 0) {
                return;
            }
            int index = this.comp.locationToIndex(dge.getDragOrigin());
            if (index < 0) {
                return;
            }
            this.myFlavor = new DataFlavor(String.class, NbBundle.getBundle(IndexedCustomizer.class).getString("IndexedFlavor"));
            try {
                dge.startDrag(DragSource.DefaultMoveDrop, (Transferable)((Object)new IndexTransferable(this.myFlavor, index)), this);
                this.dge = dge;
            }
            catch (InvalidDnDOperationException exc) {
                Logger.getLogger(IndexedCustomizer.class.getName()).log(Level.WARNING, null, exc);
            }
        }

        @Override
        public void dragEnter(DragSourceDragEvent dsde) {
        }

        @Override
        public void dragOver(DragSourceDragEvent dsde) {
        }

        @Override
        public void dropActionChanged(DragSourceDragEvent dsde) {
        }

        @Override
        public void dragExit(DragSourceEvent dse) {
        }

        @Override
        public void dragDropEnd(DragSourceDropEvent dsde) {
        }

        DragGestureEvent getDragGestureEvent() {
            return this.dge;
        }
    }

}

