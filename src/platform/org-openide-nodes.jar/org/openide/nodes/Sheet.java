/*
 * Decompiled with CFR 0_118.
 */
package org.openide.nodes;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.openide.nodes.Node;

public final class Sheet {
    public static final String PROPERTIES = "properties";
    public static final String EXPERT = "expert";
    private ArrayList<Set> sets;
    private Node.PropertySet[] array;
    private PropertyChangeSupport supp;
    private PropertyChangeListener propL;

    public Sheet() {
        this(new ArrayList<Set>(2));
    }

    Sheet(ArrayList<Set> ar) {
        this.supp = new PropertyChangeSupport(this);
        this.propL = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent ev) {
                Sheet.this.supp.firePropertyChange(null, null, null);
            }
        };
        this.sets = ar;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    public final Node.PropertySet[] toArray() {
        do {
            Sheet sheet = this;
            synchronized (sheet) {
                if (this.array != null) {
                    return this.array;
                }
            }
            Node.PropertySet[] l = new Node.PropertySet[this.sets.size()];
            this.sets.toArray(l);
            Sheet sheet2 = this;
            synchronized (sheet2) {
                if (this.array == null) {
                    this.array = l;
                }
            }
        } while (true);
    }

    public synchronized Sheet cloneSheet() {
        int len = this.sets.size();
        ArrayList<Set> l = new ArrayList<Set>(len);
        for (int i = 0; i < len; ++i) {
            l.add(this.sets.get(i).cloneSet());
        }
        return new Sheet(l);
    }

    public synchronized Set get(String name) {
        int indx = this.findIndex(name);
        return indx == -1 ? null : this.sets.get(indx);
    }

    public synchronized Set put(Set set) {
        int indx = this.findIndex(set.getName());
        Set removed = null;
        if (indx == -1) {
            this.sets.add(set);
        } else {
            removed = this.sets.set(indx, set);
            removed.removePropertyChangeListener(this.propL);
        }
        set.addPropertyChangeListener(this.propL);
        this.refresh();
        return removed;
    }

    public synchronized Set remove(String set) {
        int indx = this.findIndex(set);
        if (indx != -1) {
            Set s = this.sets.remove(indx);
            s.removePropertyChangeListener(this.propL);
            this.refresh();
            return s;
        }
        return null;
    }

    public static Sheet createDefault() {
        Sheet newSheet = new Sheet();
        newSheet.put(Sheet.createPropertiesSet());
        return newSheet;
    }

    public static Set createPropertiesSet() {
        Set ps = new Set();
        ps.setName("properties");
        ps.setDisplayName(Node.getString("Properties"));
        ps.setShortDescription(Node.getString("HINT_Properties"));
        return ps;
    }

    public static Set createExpertSet() {
        Set ps = new Set();
        ps.setExpert(true);
        ps.setName("expert");
        ps.setDisplayName(Node.getString("Expert"));
        ps.setShortDescription(Node.getString("HINT_Expert"));
        return ps;
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.supp.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.supp.removePropertyChangeListener(l);
    }

    private int findIndex(String name) {
        int s = this.sets.size();
        for (int i = 0; i < s; ++i) {
            Node.PropertySet p = this.sets.get(i);
            String pn = p.getName();
            if (pn == null || !pn.equals(name)) continue;
            return i;
        }
        return -1;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void refresh() {
        Sheet sheet = this;
        synchronized (sheet) {
            this.array = null;
        }
        this.supp.firePropertyChange(null, null, null);
    }

    public static final class Set
    extends Node.PropertySet {
        private List<Node.Property<?>> props;
        private Node.Property<?>[] array;
        private PropertyChangeSupport supp;

        public Set() {
            this(new ArrayList());
        }

        private Set(List<Node.Property<?>> al) {
            this.supp = new PropertyChangeSupport(this);
            this.props = al;
        }

        public synchronized Set cloneSet() {
            return new Set(new ArrayList(this.props));
        }

        public Node.Property<?> get(String name) {
            int indx = this.findIndex(name);
            return indx == -1 ? null : this.props.get(indx);
        }

        @Override
        public synchronized Node.Property<?>[] getProperties() {
            if (this.array == null) {
                this.array = new Node.Property[this.props.size()];
                this.props.toArray(this.array);
            }
            return this.array;
        }

        public synchronized Node.Property<?> put(Node.Property<?> p) {
            Node.Property removed;
            int indx = this.findIndex(p.getName());
            if (indx != -1) {
                removed = this.props.set(indx, p);
            } else {
                this.props.add(p);
                removed = null;
            }
            this.refresh();
            return removed;
        }

        public synchronized void put(Node.Property<?>[] ar) {
            for (int i = 0; i < ar.length; ++i) {
                Node.Property p = ar[i];
                p = ar[i];
                int indx = this.findIndex(p.getName());
                if (indx != -1) {
                    this.props.set(indx, p);
                    continue;
                }
                this.props.add(p);
            }
            this.refresh();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public synchronized Node.Property<?> remove(String name) {
            int indx = this.findIndex(name);
            if (indx != -1) {
                try {
                    Node.Property property = this.props.remove(indx);
                    return property;
                }
                finally {
                    this.refresh();
                }
            }
            return null;
        }

        public void addPropertyChangeListener(PropertyChangeListener l) {
            this.supp.addPropertyChangeListener(l);
        }

        public void removePropertyChangeListener(PropertyChangeListener l) {
            this.supp.removePropertyChangeListener(l);
        }

        private int findIndex(String name) {
            int s = this.props.size();
            for (int i = 0; i < s; ++i) {
                Node.Property p = this.props.get(i);
                if (!p.getName().equals(name)) continue;
                return i;
            }
            return -1;
        }

        private void refresh() {
            this.array = null;
            this.supp.firePropertyChange(null, null, null);
        }
    }

}

