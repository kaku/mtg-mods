/*
 * Decompiled with CFR 0_118.
 */
package org.openide.nodes;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(value=RetentionPolicy.SOURCE)
@Target(value={ElementType.PACKAGE})
public @interface PropertyEditorSearchPath {
}

