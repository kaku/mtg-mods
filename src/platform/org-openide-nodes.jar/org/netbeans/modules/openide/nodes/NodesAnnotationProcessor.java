/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 */
package org.netbeans.modules.openide.nodes;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.AnnotationValueVisitor;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.SimpleAnnotationValueVisitor6;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;
import org.netbeans.modules.openide.nodes.NodesRegistrationSupport;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;
import org.openide.nodes.BeanInfoSearchPath;
import org.openide.nodes.PropertyEditorRegistration;
import org.openide.nodes.PropertyEditorSearchPath;

@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
public class NodesAnnotationProcessor
extends LayerGeneratingProcessor {
    public Set<String> getSupportedAnnotationTypes() {
        HashSet<String> set = new HashSet<String>();
        set.add(PropertyEditorSearchPath.class.getName());
        set.add(PropertyEditorRegistration.class.getName());
        set.add(BeanInfoSearchPath.class.getName());
        return set;
    }

    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        LayerBuilder builder;
        String pkgFilename;
        String pkg;
        LayerBuilder.File file;
        Messager messager = this.processingEnv.getMessager();
        for (Element e22 : roundEnv.getElementsAnnotatedWith(PropertyEditorSearchPath.class)) {
            pkg = this.findPackage(e22);
            pkgFilename = pkg.replace(".", "-");
            builder = this.layer(new Element[]{e22});
            file = builder.file("Services/PropertyEditorManager/Package-" + pkgFilename + ".instance");
            file.methodvalue("instanceCreate", NodesRegistrationSupport.class.getName(), "createPackageRegistration");
            file.stringvalue("packagePath", pkg);
            file.stringvalue("instanceOf", NodesRegistrationSupport.PEPackageRegistration.class.getName());
            file.write();
        }
        for (Element e : roundEnv.getElementsAnnotatedWith(PropertyEditorRegistration.class)) {
            if (e.getKind() != ElementKind.CLASS) continue;
            String className = ((TypeElement)e).getQualifiedName().toString();
            Collection targetTypes = null;
            List<? extends AnnotationMirror> annotationMirrors = e.getAnnotationMirrors();
            for (AnnotationMirror am : annotationMirrors) {
                for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : am.getElementValues().entrySet()) {
                    if (!"targetType".equals(entry.getKey().getSimpleName().toString())) continue;
                    targetTypes = (Collection)entry.getValue().getValue();
                }
            }
            if (targetTypes == null) {
                messager.printMessage(Diagnostic.Kind.ERROR, "No targetType is specified", e);
                continue;
            }
            TypeElement typeElement = this.processingEnv.getElementUtils().getTypeElement("java.beans.PropertyEditor");
            if (!this.processingEnv.getTypeUtils().isSubtype(e.asType(), typeElement.asType())) {
                messager.printMessage(Diagnostic.Kind.ERROR, className + " is not subtype of PropertyEditor", e);
                continue;
            }
            LayerBuilder builder2 = this.layer(new Element[]{e});
            String clsFileName = className.replace(".", "-");
            LayerBuilder.File file2 = builder2.instanceFile("Services/PropertyEditorManager", "Class-" + clsFileName);
            file2.methodvalue("instanceCreate", NodesRegistrationSupport.class.getName(), "createClassRegistration");
            file2.stringvalue("propertyEditorClass", className);
            file2.stringvalue("instanceOf", NodesRegistrationSupport.PEClassRegistration.class.getName());
            int i = 1;
            for (AnnotationValue type : targetTypes) {
                String clsName = (String)type.accept(new SimpleAnnotationValueVisitor6<String, Object>(){

                    @Override
                    public String visitType(TypeMirror t, Object p) {
                        return t.toString();
                    }
                }, null);
                file2.stringvalue("targetType." + i, clsName);
                ++i;
            }
            file2.write();
        }
        for (Element e2 : roundEnv.getElementsAnnotatedWith(BeanInfoSearchPath.class)) {
            pkg = this.findPackage(e2);
            pkgFilename = pkg.replace(".", "-");
            builder = this.layer(new Element[]{e2});
            file = builder.file("Services/Introspector/BeanInfo-" + pkgFilename + ".instance");
            file.methodvalue("instanceCreate", NodesRegistrationSupport.class.getName(), "createBeanInfoRegistration");
            file.stringvalue("packagePath", pkg);
            file.stringvalue("instanceOf", NodesRegistrationSupport.BeanInfoRegistration.class.getName());
            file.write();
        }
        return true;
    }

    private String findPackage(Element e) {
        switch (e.getKind()) {
            case PACKAGE: {
                return ((PackageElement)e).getQualifiedName().toString();
            }
        }
        return this.findPackage(e.getEnclosingElement());
    }

}

