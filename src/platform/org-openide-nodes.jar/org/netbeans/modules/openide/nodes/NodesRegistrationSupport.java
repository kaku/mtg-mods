/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 */
package org.netbeans.modules.openide.nodes;

import java.beans.Introspector;
import java.beans.PropertyEditorManager;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;

public final class NodesRegistrationSupport {
    static final String PE_LOOKUP_PATH = "Services/PropertyEditorManager";
    static final String BEANINFO_LOOKUP_PATH = "Services/Introspector";
    static final String PACKAGE = "packagePath";
    static final String EDITOR_CLASS = "propertyEditorClass";
    private static AbstractRegistrator clsReg = null;
    private static AbstractRegistrator beanInfoReg = null;
    private static AbstractRegistrator pkgReg = null;
    private static List<String> originalPath = null;
    private static List<String> originalBeanInfoSearchPath = null;

    public static synchronized void registerPropertyEditors() {
        if (clsReg == null) {
            clsReg = new AbstractRegistrator(PEClassRegistration.class){

                @Override
                void register() {
                    ClassLoader clsLoader = NodesRegistrationSupport.findClsLoader();
                    for (PEClassRegistration clsReg : this.lookupResult.allInstances()) {
                        for (String type : clsReg.targetTypes) {
                            try {
                                Class cls = NodesRegistrationSupport.getClassFromCanonicalName(type);
                                Class editorCls = Class.forName(clsReg.editorClass, true, clsLoader);
                                PropertyEditorManager.registerEditor(cls, editorCls);
                            }
                            catch (ClassNotFoundException ex) {
                                Exceptions.printStackTrace((Throwable)ex);
                            }
                        }
                    }
                }

                @Override
                void init() {
                }
            };
        } else {
            clsReg.register();
        }
        if (pkgReg == null) {
            pkgReg = new AbstractRegistrator(PEPackageRegistration.class){

                @Override
                void register() {
                    LinkedHashSet<String> newPath = new LinkedHashSet<String>();
                    for (PEPackageRegistration pkgReg : this.lookupResult.allInstances()) {
                        newPath.add(pkgReg.pkg);
                    }
                    newPath.addAll(originalPath);
                    PropertyEditorManager.setEditorSearchPath(newPath.toArray(new String[newPath.size()]));
                }

                @Override
                void init() {
                    if (originalPath == null) {
                        originalPath = Arrays.asList(PropertyEditorManager.getEditorSearchPath());
                    }
                }
            };
        } else {
            pkgReg.register();
        }
        if (beanInfoReg == null) {
            beanInfoReg = new AbstractRegistrator(BeanInfoRegistration.class){

                @Override
                void register() {
                    LinkedHashSet<String> newPath = new LinkedHashSet<String>();
                    for (BeanInfoRegistration biReg : this.lookupResult.allInstances()) {
                        newPath.add(biReg.searchPath);
                    }
                    newPath.addAll(originalBeanInfoSearchPath);
                    Introspector.setBeanInfoSearchPath(newPath.toArray(new String[newPath.size()]));
                }

                @Override
                void init() {
                    if (originalBeanInfoSearchPath == null) {
                        originalBeanInfoSearchPath = Arrays.asList(Introspector.getBeanInfoSearchPath());
                    }
                }
            };
        } else {
            beanInfoReg.register();
        }
    }

    public static PEPackageRegistration createPackageRegistration(Map attrs) {
        String pkg = (String)attrs.get("packagePath");
        return new PEPackageRegistration(pkg);
    }

    public static PEClassRegistration createClassRegistration(Map attrs) {
        String targetType;
        String editorClass = (String)attrs.get("propertyEditorClass");
        LinkedHashSet<String> targetTypes = new LinkedHashSet<String>();
        int i = 1;
        while ((targetType = (String)attrs.get("targetType." + i)) != null) {
            targetTypes.add(targetType);
            ++i;
        }
        return new PEClassRegistration(editorClass, targetTypes);
    }

    public static BeanInfoRegistration createBeanInfoRegistration(Map attrs) {
        String pkg = (String)attrs.get("packagePath");
        return new BeanInfoRegistration(pkg);
    }

    protected static Class<?> getClassFromCanonicalName(String name) throws ClassNotFoundException {
        Class result2;
        Class result2;
        String type = name;
        int dimensions = 0;
        while (type.endsWith("[]")) {
            ++dimensions;
            type = type.substring(0, type.length() - 2);
        }
        if ("byte".equals(type)) {
            result2 = Byte.TYPE;
        } else if ("short".equals(type)) {
            result2 = Short.TYPE;
        } else if ("char".equals(type)) {
            result2 = Character.TYPE;
        } else if ("int".equals(type)) {
            result2 = Integer.TYPE;
        } else if ("long".equals(type)) {
            result2 = Long.TYPE;
        } else if ("float".equals(type)) {
            result2 = Float.TYPE;
        } else if ("double".equals(type)) {
            result2 = Double.TYPE;
        } else if ("boolean".equals(type)) {
            result2 = Boolean.TYPE;
        } else {
            ClassLoader clsLoader = NodesRegistrationSupport.findClsLoader();
            result2 = Class.forName(type, true, clsLoader);
        }
        if (dimensions > 0) {
            int[] d = new int[dimensions];
            for (int i = 0; i < d.length; ++i) {
                d[i] = 0;
            }
            result2 = Array.newInstance(result2, d).getClass();
        }
        return result2;
    }

    static ClassLoader findClsLoader() {
        ClassLoader clsLoader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        if (clsLoader == null) {
            clsLoader = Thread.currentThread().getContextClassLoader();
        }
        if (clsLoader == null) {
            clsLoader = NodesRegistrationSupport.class.getClassLoader();
        }
        return clsLoader;
    }

    private static abstract class AbstractRegistrator
    implements LookupListener {
        Lookup.Result lookupResult;
        private final Class cls;

        AbstractRegistrator(Class cls) {
            this.cls = cls;
            this.init();
            this.lookupResult = Lookup.getDefault().lookupResult(cls);
            this.register();
            this.lookupResult.addLookupListener((LookupListener)this);
        }

        abstract void register();

        abstract void init();

        public void resultChanged(LookupEvent ev) {
            this.lookupResult = Lookup.getDefault().lookupResult(this.cls);
            this.register();
        }
    }

    public static class BeanInfoRegistration {
        final String searchPath;

        BeanInfoRegistration(String searchPath) {
            this.searchPath = searchPath;
        }
    }

    public static class PEClassRegistration {
        final Set<String> targetTypes;
        final String editorClass;

        PEClassRegistration(String editorClass, Set<String> targetTypes) {
            this.editorClass = editorClass;
            this.targetTypes = targetTypes;
        }
    }

    public static class PEPackageRegistration {
        final String pkg;

        PEPackageRegistration(String pkg) {
            this.pkg = pkg;
        }
    }

}

