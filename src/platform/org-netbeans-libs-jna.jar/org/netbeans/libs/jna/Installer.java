/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.ModuleInstall
 */
package org.netbeans.libs.jna;

import org.openide.modules.ModuleInstall;

public class Installer
extends ModuleInstall {
    public void validate() {
        super.validate();
        System.setProperty("jna.boot.library.name", "jnidispatch-410");
    }
}

