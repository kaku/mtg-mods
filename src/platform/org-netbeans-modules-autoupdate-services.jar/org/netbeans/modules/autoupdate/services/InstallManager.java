/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.Module
 *  org.netbeans.Util
 *  org.netbeans.core.startup.MainLookup
 *  org.netbeans.core.startup.layers.LocalFileSystemEx
 *  org.netbeans.updater.UpdateTracking
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.InstalledFileLocator
 *  org.openide.modules.ModuleInfo
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.autoupdate.services;

import java.beans.PropertyVetoException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.Module;
import org.netbeans.Util;
import org.netbeans.api.autoupdate.OperationException;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.core.startup.MainLookup;
import org.netbeans.core.startup.layers.LocalFileSystemEx;
import org.netbeans.modules.autoupdate.services.ModuleUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.Trampoline;
import org.netbeans.modules.autoupdate.services.UpdateElementImpl;
import org.netbeans.modules.autoupdate.services.Utilities;
import org.netbeans.modules.autoupdate.updateprovider.InstallInfo;
import org.netbeans.spi.autoupdate.AutoupdateClusterCreator;
import org.netbeans.updater.UpdateTracking;
import org.openide.filesystems.FileUtil;
import org.openide.modules.InstalledFileLocator;
import org.openide.modules.ModuleInfo;
import org.openide.util.Lookup;

public class InstallManager
extends InstalledFileLocator {
    static final String NBM_LIB = "lib";
    static final String NBM_CORE = "core";
    static final String NETBEANS_DIRS = "netbeans.dirs";
    private static int countOfWarnings = 0;
    private static final int MAX_COUNT_OF_WARNINGS = 5;
    private static final Logger ERR = Logger.getLogger("org.netbeans.modules.autoupdate.services.InstallManager");
    private static final List<File> clusters = new ArrayList<File>();

    static File findTargetDirectory(UpdateElement installed, UpdateElementImpl update, Boolean globalOrLocal, boolean useUserdirAsFallback) throws OperationException {
        boolean isGlobal;
        File res;
        if (globalOrLocal == null) {
            globalOrLocal = InstallManager.isGlobalInstallation();
        }
        boolean bl = isGlobal = globalOrLocal == null ? false : globalOrLocal;
        if (Boolean.FALSE.equals(globalOrLocal)) {
            ERR.log(Level.INFO, "Forced installation in userdir only for " + update.getUpdateElement());
            return InstallManager.getUserDir();
        }
        if (installed != null) {
            res = InstallManager.getInstallDir(installed, update, isGlobal |= update.getInstallInfo().isGlobal() != null && update.getInstallInfo().isGlobal() != false, useUserdirAsFallback);
        } else {
            isGlobal |= update.isFixed();
            String targetCluster = update.getInstallInfo().getTargetCluster();
            if (targetCluster != null && targetCluster.length() > 0 || (isGlobal |= update.getInstallInfo().isGlobal() != null && update.getInstallInfo().isGlobal() != false)) {
                res = InstallManager.checkTargetCluster(update, targetCluster, isGlobal, useUserdirAsFallback);
                if (res == null && targetCluster != null && (res = InstallManager.createNonExistingCluster(targetCluster)) != null) {
                    res = InstallManager.checkTargetCluster(update, targetCluster, isGlobal, useUserdirAsFallback);
                }
                if (res == null) {
                    InstallManager.createNonExistingCluster("extra");
                    res = InstallManager.checkTargetCluster(update, "extra", isGlobal, useUserdirAsFallback);
                    File file = res = res == null ? InstallManager.getUserDir() : res;
                    if (targetCluster != null) {
                        ERR.log(Level.INFO, "Declared target cluster " + targetCluster + " in " + update.getUpdateElement() + " wasn't found or was read only. Will be used " + res);
                    } else {
                        ERR.log(Level.INFO, res + " will be used as target cluster");
                    }
                }
            } else {
                res = InstallManager.getUserDir();
            }
        }
        ERR.log(Level.FINEST, "UpdateElement " + update.getUpdateElement() + " has the target cluster " + res);
        return res;
    }

    private static File checkTargetCluster(UpdateElementImpl update, String targetCluster, boolean isGlobal, boolean useUserdirAsFallback) throws OperationException {
        if (targetCluster == null || targetCluster.length() == 0) {
            return null;
        }
        File res = null;
        for (File cluster : UpdateTracking.clusters((boolean)true)) {
            boolean wasNew;
            if (!targetCluster.equals(cluster.getName())) continue;
            boolean bl = wasNew = !cluster.exists();
            if (Utilities.canWriteInCluster(cluster)) {
                if (wasNew) {
                    cluster.mkdirs();
                    InstallManager.extendSystemFileSystem(cluster);
                }
                res = cluster;
                break;
            }
            if (!useUserdirAsFallback && isGlobal) {
                ERR.log(Level.WARNING, "There is no write permission to write in target cluster " + targetCluster + " for " + update.getUpdateElement());
                throw new OperationException(OperationException.ERROR_TYPE.WRITE_PERMISSION, update.getCodeName());
            }
            if (countOfWarnings++ < 5) {
                ERR.log(Level.WARNING, "There is no write permission to write in target cluster " + targetCluster + " for " + update.getUpdateElement());
            }
            if (countOfWarnings != 5) break;
            ERR.log(Level.WARNING, "There is no write permission to write in target cluster " + targetCluster + " for more updates or plugins.");
            break;
        }
        return res;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static File createNonExistingCluster(String targetCluster) {
        File res = null;
        for (AutoupdateClusterCreator creator : Lookup.getDefault().lookupAll(AutoupdateClusterCreator.class)) {
            File possibleCluster = Trampoline.SPI.findCluster(targetCluster, creator);
            if (possibleCluster == null) continue;
            try {
                ERR.log(Level.FINE, "Found cluster candidate " + possibleCluster + " for declared target cluster " + targetCluster);
                File[] dirs = Trampoline.SPI.registerCluster(targetCluster, possibleCluster, creator);
                res = possibleCluster;
                StringBuffer sb = new StringBuffer();
                String sep = "";
                for (File dir : dirs) {
                    sb.append(sep);
                    sb.append(dir.getPath());
                    sep = File.pathSeparator;
                }
                System.setProperty("netbeans.dirs", sb.toString());
                File f = new File(new File(InstallManager.getUserDir(), Utilities.DOWNLOAD_DIR), "netbeans.dirs");
                if (!f.exists()) {
                    f.getParentFile().mkdirs();
                    f.createNewFile();
                }
                FileOutputStream os = new FileOutputStream(f);
                try {
                    os.write(sb.toString().getBytes());
                }
                finally {
                    os.close();
                }
                ERR.log(Level.FINE, "Was written new netbeans.dirs " + sb);
                break;
            }
            catch (IOException ioe) {
                ERR.log(Level.INFO, ioe.getMessage(), ioe);
                continue;
            }
        }
        return res;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void extendSystemFileSystem(File cluster) {
        try {
            File extradir = new File(cluster, "config");
            extradir.mkdir();
            LocalFileSystemEx lfse = new LocalFileSystemEx();
            lfse.setRootDirectory(extradir);
            MainLookup.register((Object)lfse);
            Class<InstallManager> class_ = InstallManager.class;
            synchronized (InstallManager.class) {
                clusters.add(cluster);
                // ** MonitorExit[var3_5] (shouldn't be in output)
            }
        }
        catch (PropertyVetoException ioe) {
            ERR.log(Level.INFO, ioe.getMessage(), ioe);
        }
        catch (IOException ioe) {
            ERR.log(Level.INFO, ioe.getMessage(), ioe);
        }
        {
            
            return;
        }
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    private static File getInstallDir(UpdateElement installed, UpdateElementImpl update, boolean isGlobal, boolean useUserdirAsFallback) throws OperationException {
        res = null;
        i = Trampoline.API.impl(installed);
        if (!InstallManager.$assertionsDisabled && !(i instanceof ModuleUpdateElementImpl)) {
            throw new AssertionError((Object)("Impl of " + installed + " instanceof ModuleUpdateElementImpl"));
        }
        m = Utilities.toModule(((ModuleUpdateElementImpl)i).getModuleInfo());
        v0 = jarFile = m == null ? null : m.getJarFile();
        if (jarFile != null) ** GOTO lbl18
        InstallManager.ERR.log(Level.FINE, "No install dir for " + installed + " (It's ok for fixed). Is fixed? " + Trampoline.API.impl(installed).isFixed());
        targetCluster = update.getInstallInfo().getTargetCluster();
        if (targetCluster != null) {
            for (File cluster : UpdateTracking.clusters((boolean)false)) {
                if (!targetCluster.equals(cluster.getName())) continue;
                res = cluster;
                break;
            }
        }
        if (res != null) ** GOTO lbl22
        res = UpdateTracking.getPlatformDir();
        ** GOTO lbl22
lbl18: // 2 sources:
        for (File cluster : UpdateTracking.clusters((boolean)true)) {
            if (!InstallManager.isParentOf(cluster = FileUtil.normalizeFile((File)cluster), jarFile)) continue;
            res = cluster;
            break;
        }
lbl22: // 4 sources:
        if (res == null || !Utilities.canWriteInCluster(res)) {
            if (!useUserdirAsFallback && isGlobal) {
                InstallManager.ERR.log(Level.WARNING, "There is no write permission to write in target cluster " + res + " for " + update.getUpdateElement());
                throw new OperationException(OperationException.ERROR_TYPE.WRITE_PERMISSION, update.getCodeName());
            }
            if (InstallManager.countOfWarnings++ < 5) {
                InstallManager.ERR.log(Level.WARNING, "There is no write permission to write in target cluster " + res + " for " + update.getUpdateElement());
            }
            if (InstallManager.countOfWarnings == 5) {
                InstallManager.ERR.log(Level.WARNING, "There is no write permission to write in target cluster " + res + " for more updates or plugins.");
            }
            res = UpdateTracking.getUserDir();
        }
        InstallManager.ERR.log(Level.FINEST, "Install dir of " + installed + " is " + res);
        return res;
    }

    private static boolean isParentOf(File parent, File child) {
        File tmp;
        for (tmp = child.getParentFile(); tmp != null && !parent.equals(tmp); tmp = tmp.getParentFile()) {
        }
        return tmp != null;
    }

    static File getUserDir() {
        return UpdateTracking.getUserDir();
    }

    static boolean needsRestart(boolean isUpdate, UpdateElementImpl update, File dest) {
        boolean needsRestart;
        assert (update.getInstallInfo() != null);
        boolean isForcedRestart = update.getInstallInfo().needsRestart() != null && update.getInstallInfo().needsRestart() != false;
        boolean bl = needsRestart = isForcedRestart || isUpdate;
        if (!needsRestart) {
            needsRestart = InstallManager.willInstallInSystem(dest);
        }
        return needsRestart;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static boolean willInstallInSystem(File nbmFile) {
        boolean res;
        res = false;
        try {
            JarFile jf = new JarFile(nbmFile);
            try {
                for (JarEntry entry : Collections.list(jf.entries())) {
                    String entryName = entry.getName();
                    if (!entryName.startsWith("core/") && !entryName.startsWith("lib/")) continue;
                    res = true;
                    break;
                }
            }
            finally {
                jf.close();
            }
        }
        catch (IOException ioe) {
            ERR.log(Level.INFO, ioe.getMessage(), ioe);
        }
        return res;
    }

    public File locate(String relativePath, String codeNameBase, boolean localized) {
        Set<File> files = this.locateAll(relativePath, codeNameBase, localized);
        return files.isEmpty() ? null : files.iterator().next();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Set<File> locateAll(String relativePath, String codeNameBase, boolean localized) {
        Class<InstallManager> class_ = InstallManager.class;
        synchronized (InstallManager.class) {
            String name;
            String prefix;
            if (clusters.isEmpty()) {
                // ** MonitorExit[var4_4] (shouldn't be in output)
                return Collections.emptySet();
            }
            // ** MonitorExit[var4_4] (shouldn't be in output)
            if (relativePath.length() == 0) {
                throw new IllegalArgumentException("Cannot look up \"\" in InstalledFileLocator.locate");
            }
            if (relativePath.charAt(0) == '/') {
                throw new IllegalArgumentException("Paths passed to InstalledFileLocator.locate should not start with '/': " + relativePath);
            }
            int slashIdx = relativePath.lastIndexOf(47);
            if (slashIdx == relativePath.length() - 1) {
                throw new IllegalArgumentException("Paths passed to InstalledFileLocator.locate should not end in '/': " + relativePath);
            }
            if (slashIdx != -1) {
                prefix = relativePath.substring(0, slashIdx + 1);
                name = relativePath.substring(slashIdx + 1);
                assert (name.length() > 0);
            } else {
                prefix = "";
                name = relativePath;
            }
            if (localized) {
                String baseName;
                String ext;
                int i = name.lastIndexOf(46);
                if (i == -1) {
                    baseName = name;
                    ext = "";
                } else {
                    baseName = name.substring(0, i);
                    ext = name.substring(i);
                }
                String[] suffixes = Util.getLocalizingSuffixesFast();
                HashSet<File> files = new HashSet<File>();
                for (String suffixe : suffixes) {
                    String locName = baseName + suffixe + ext;
                    files.addAll(InstallManager.locateExactPath(prefix, locName));
                }
                return files;
            }
            return InstallManager.locateExactPath(prefix, name);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static Set<File> locateExactPath(String prefix, String name) {
        HashSet<File> files = new HashSet<File>();
        Class<InstallManager> class_ = InstallManager.class;
        synchronized (InstallManager.class) {
            File[] dirs;
            for (File dir : dirs = clusters.toArray(new File[clusters.size()])) {
                File f = InstallManager.makeFile(dir, prefix, name);
                if (!f.exists()) continue;
                files.add(f);
            }
            // ** MonitorExit[var3_3] (shouldn't be in output)
            return files;
        }
    }

    private static File makeFile(File dir, String prefix, String name) {
        return FileUtil.normalizeFile((File)new File(dir, prefix.replace('/', File.separatorChar) + name));
    }

    private static Boolean isGlobalInstallation() {
        String s = System.getProperty("plugin.manager.install.global");
        if (Boolean.parseBoolean(s)) {
            return Boolean.TRUE;
        }
        if (Boolean.FALSE.toString().equalsIgnoreCase(s)) {
            return Boolean.FALSE;
        }
        return null;
    }
}

