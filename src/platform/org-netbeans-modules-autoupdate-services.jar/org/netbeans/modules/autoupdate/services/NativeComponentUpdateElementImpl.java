/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.SpecificationVersion
 */
package org.netbeans.modules.autoupdate.services;

import java.util.Collections;
import java.util.List;
import org.netbeans.api.autoupdate.UpdateManager;
import org.netbeans.modules.autoupdate.services.UpdateElementImpl;
import org.netbeans.modules.autoupdate.services.UpdateUnitFactory;
import org.netbeans.modules.autoupdate.updateprovider.InstallInfo;
import org.netbeans.modules.autoupdate.updateprovider.NativeComponentItem;
import org.netbeans.modules.autoupdate.updateprovider.UpdateItemImpl;
import org.openide.modules.ModuleInfo;
import org.openide.modules.SpecificationVersion;

public class NativeComponentUpdateElementImpl
extends UpdateElementImpl {
    private String codeName;
    private String displayName;
    private SpecificationVersion specVersion;
    private String description;
    private String source;
    private String author;
    private String homepage;
    private int downloadSize;
    private String category;
    private InstallInfo installInfo;
    private NativeComponentItem nativeItem;

    public NativeComponentUpdateElementImpl(NativeComponentItem item, String providerName) {
        super(item, providerName);
        this.codeName = item.getCodeName();
        this.specVersion = item.getSpecificationVersion() == null ? null : new SpecificationVersion(item.getSpecificationVersion());
        this.source = providerName;
        this.installInfo = new InstallInfo(item);
        this.displayName = item.getDisplayName();
        this.description = item.getDescription();
        this.downloadSize = item.getDownloadSize();
        this.nativeItem = item;
    }

    @Override
    public String getCodeName() {
        return this.codeName;
    }

    @Override
    public String getDisplayName() {
        return this.displayName;
    }

    @Override
    public SpecificationVersion getSpecificationVersion() {
        return this.specVersion;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public String getNotification() {
        return null;
    }

    @Override
    public String getAuthor() {
        return this.author;
    }

    @Override
    public String getHomepage() {
        return this.homepage;
    }

    @Override
    public int getDownloadSize() {
        return this.downloadSize;
    }

    @Override
    public String getSource() {
        return this.source;
    }

    @Override
    public String getCategory() {
        if (this.category == null) {
            this.category = UpdateUnitFactory.UNSORTED_CATEGORY;
        }
        return this.category;
    }

    @Override
    public String getDate() {
        return null;
    }

    @Override
    public String getLicenseId() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getLicence() {
        return this.nativeItem.getAgreement();
    }

    @Override
    public InstallInfo getInstallInfo() {
        return this.installInfo;
    }

    @Override
    public List<ModuleInfo> getModuleInfos() {
        return Collections.emptyList();
    }

    public NativeComponentItem getNativeItem() {
        return this.nativeItem;
    }

    @Override
    public UpdateManager.TYPE getType() {
        return UpdateManager.TYPE.CUSTOM_HANDLED_COMPONENT;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean isAutoload() {
        return false;
    }

    @Override
    public boolean isEager() {
        return false;
    }

    @Override
    public boolean isFixed() {
        return false;
    }

    @Override
    public boolean isPreferredUpdate() {
        return false;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        NativeComponentUpdateElementImpl other = (NativeComponentUpdateElementImpl)obj;
        if (!(this.specVersion == other.specVersion || this.specVersion != null && this.specVersion.equals((Object)other.specVersion))) {
            return false;
        }
        if (!(this.codeName == other.codeName || this.codeName != null && this.codeName.equals(other.codeName))) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + (this.codeName != null ? this.codeName.hashCode() : 0);
        hash = 61 * hash + (this.specVersion != null ? this.specVersion.hashCode() : 0);
        return hash;
    }
}

