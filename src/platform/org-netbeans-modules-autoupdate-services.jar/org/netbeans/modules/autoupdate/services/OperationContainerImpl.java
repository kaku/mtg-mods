/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.Dependency
 *  org.openide.modules.ModuleInfo
 */
package org.netbeans.modules.autoupdate.services;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.autoupdate.InstallSupport;
import org.netbeans.api.autoupdate.OperationContainer;
import org.netbeans.api.autoupdate.OperationSupport;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.modules.autoupdate.services.FeatureUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.ModuleUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.OperationValidator;
import org.netbeans.modules.autoupdate.services.Trampoline;
import org.netbeans.modules.autoupdate.services.UpdateElementImpl;
import org.netbeans.modules.autoupdate.services.UpdateManagerImpl;
import org.netbeans.modules.autoupdate.services.UpdateUnitFactory;
import org.netbeans.modules.autoupdate.services.Utilities;
import org.openide.modules.Dependency;
import org.openide.modules.ModuleInfo;

public final class OperationContainerImpl<Support> {
    private boolean upToDate = false;
    public static final Logger LOGGER = Logger.getLogger(OperationContainerImpl.class.getName());
    private final List<OperationContainer.OperationInfo<Support>> operations = new CopyOnWriteArrayList<OperationContainer.OperationInfo<Support>>();
    private Throwable lastModified;
    private final Collection<OperationContainer.OperationInfo<Support>> affectedEagers = new HashSet<OperationContainer.OperationInfo<Support>>();
    private OperationType type;
    private OperationContainer delegate;

    private OperationContainerImpl() {
    }

    public static OperationContainerImpl<InstallSupport> createForInstall() {
        return new OperationContainerImpl<InstallSupport>(OperationType.INSTALL);
    }

    public static OperationContainerImpl<InstallSupport> createForInternalUpdate() {
        return new OperationContainerImpl<InstallSupport>(OperationType.INTERNAL_UPDATE);
    }

    public static OperationContainerImpl<InstallSupport> createForUpdate() {
        return new OperationContainerImpl<InstallSupport>(OperationType.UPDATE);
    }

    public static OperationContainerImpl<OperationSupport> createForDirectInstall() {
        OperationContainerImpl<OperationSupport> impl = new OperationContainerImpl<OperationSupport>(OperationType.INSTALL);
        impl.delegate = OperationContainer.createForUpdate();
        return impl;
    }

    public static OperationContainerImpl<OperationSupport> createForDirectUpdate() {
        OperationContainerImpl<OperationSupport> impl = new OperationContainerImpl<OperationSupport>(OperationType.UPDATE);
        impl.delegate = OperationContainer.createForUpdate();
        return impl;
    }

    public static OperationContainerImpl<OperationSupport> createForUninstall() {
        return new OperationContainerImpl<OperationSupport>(OperationType.UNINSTALL);
    }

    public static OperationContainerImpl<OperationSupport> createForDirectUninstall() {
        return new OperationContainerImpl<OperationSupport>(OperationType.DIRECT_UNINSTALL);
    }

    public static OperationContainerImpl<OperationSupport> createForEnable() {
        return new OperationContainerImpl<OperationSupport>(OperationType.ENABLE);
    }

    public static OperationContainerImpl<OperationSupport> createForDisable() {
        return new OperationContainerImpl<OperationSupport>(OperationType.DISABLE);
    }

    public static OperationContainerImpl<OperationSupport> createForDirectDisable() {
        return new OperationContainerImpl<OperationSupport>(OperationType.DIRECT_DISABLE);
    }

    public static OperationContainerImpl<OperationSupport> createForInstallNativeComponent() {
        return new OperationContainerImpl<OperationSupport>(OperationType.CUSTOM_INSTALL);
    }

    public static OperationContainerImpl<OperationSupport> createForUninstallNativeComponent() {
        return new OperationContainerImpl<OperationSupport>(OperationType.CUSTOM_UNINSTALL);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public OperationContainer.OperationInfo<Support> add(UpdateUnit updateUnit, UpdateElement updateElement) throws IllegalArgumentException {
        OperationContainer.OperationInfo retval = null;
        boolean isValid = this.isValid(updateUnit, updateElement);
        if (UpdateUnitFactory.getDefault().isScheduledForRestart(updateElement)) {
            LOGGER.log(Level.INFO, updateElement + " is scheduled for restart IDE.");
            throw new IllegalArgumentException(updateElement + " is scheduled for restart IDE.");
        }
        if (!isValid) {
            throw new IllegalArgumentException("Invalid " + updateUnit + " for operation " + (Object)((Object)this.type));
        }
        if (isValid) {
            switch (this.type) {
                case UNINSTALL: 
                case DIRECT_UNINSTALL: 
                case CUSTOM_UNINSTALL: 
                case ENABLE: 
                case DISABLE: 
                case DIRECT_DISABLE: {
                    if (updateUnit.getInstalled() == updateElement) break;
                    throw new IllegalArgumentException(updateUnit.getInstalled() + " and " + updateElement + " must be same for operation " + (Object)((Object)this.type));
                }
                case INSTALL: 
                case UPDATE: 
                case CUSTOM_INSTALL: {
                    if (updateUnit.getInstalled() != updateElement) break;
                    throw new IllegalArgumentException(updateUnit.getInstalled() + " and " + updateElement + " cannot be same for operation " + (Object)((Object)this.type));
                }
                case INTERNAL_UPDATE: {
                    break;
                }
                default: {
                    assert (false);
                    break;
                }
            }
        }
        OperationContainerImpl operationContainerImpl = this;
        synchronized (operationContainerImpl) {
            if (!this.contains(updateUnit, updateElement)) {
                retval = Trampoline.API.createOperationInfo(new OperationInfoImpl(updateUnit, updateElement));
                assert (retval != null);
                this.changeState(this.operations.add(retval));
                boolean asserts = false;
                if (!$assertionsDisabled) {
                    asserts = true;
                    if (!true) {
                        throw new AssertionError();
                    }
                }
                if (asserts) {
                    this.lastModified = new Exception("Added operation: " + retval);
                }
            }
        }
        return retval;
    }

    public boolean remove(UpdateElement updateElement) {
        OperationContainer.OperationInfo<Support> toRemove = this.find(updateElement);
        if (toRemove != null) {
            this.remove(toRemove);
        }
        return toRemove != null;
    }

    public boolean contains(UpdateElement updateElement) {
        return this.find(updateElement) != null;
    }

    private OperationContainer.OperationInfo<Support> find(UpdateElement updateElement) {
        OperationContainer.OperationInfo<Support> toRemove = null;
        for (OperationContainer.OperationInfo<Support> info : this.listAll()) {
            if (!info.getUpdateElement().equals(updateElement)) continue;
            toRemove = info;
            break;
        }
        return toRemove;
    }

    private boolean contains(UpdateUnit unit, UpdateElement element) {
        List<OperationContainer.OperationInfo<Support>> infos = this.operations;
        for (OperationContainer.OperationInfo<Support> info : infos) {
            if (!info.getUpdateElement().equals(element) && !info.getUpdateUnit().equals(unit)) continue;
            return true;
        }
        return false;
    }

    private List<OperationContainer.OperationInfo<Support>> listAll() {
        return Collections.unmodifiableList(this.operations);
    }

    public synchronized List<OperationContainer.OperationInfo<Support>> listAllWithPossibleEager() {
        if (this.upToDate) {
            return this.listAll();
        }
        this.clearCache();
        boolean checkEagers = false;
        for (OperationContainer.OperationInfo<Support> i : this.operations) {
            if (Utilities.isFirstClassModule(i.getUpdateElement())) continue;
            checkEagers = true;
            break;
        }
        if ((this.type == OperationType.INSTALL || this.type == OperationType.UPDATE || this.type == OperationType.INTERNAL_UPDATE) && checkEagers) {
            HashSet<UpdateElement> all = new HashSet<UpdateElement>(this.operations.size());
            for (OperationContainer.OperationInfo<Support> i22 : this.operations) {
                all.add(i22.getUpdateElement());
            }
            for (OperationContainer.OperationInfo<Support> i2 : this.operations) {
                all.addAll(i2.getRequiredElements());
            }
            for (UpdateElement eagerEl : UpdateManagerImpl.getInstance().getAvailableEagers()) {
                if (eagerEl.getUpdateUnit().isPending() || eagerEl.getUpdateUnit().getAvailableUpdates().isEmpty()) continue;
                UpdateElementImpl impl = Trampoline.API.impl(eagerEl);
                ArrayList<ModuleInfo> infos = new ArrayList<ModuleInfo>();
                if (impl instanceof ModuleUpdateElementImpl) {
                    ModuleUpdateElementImpl eagerImpl = (ModuleUpdateElementImpl)impl;
                    infos.add(eagerImpl.getModuleInfo());
                } else if (impl instanceof FeatureUpdateElementImpl) {
                    FeatureUpdateElementImpl eagerImpl = (FeatureUpdateElementImpl)impl;
                    infos.addAll(eagerImpl.getModuleInfos());
                } else assert (false);
                for (ModuleInfo mi : infos) {
                    HashSet<UpdateElement> reqs = new HashSet<UpdateElement>();
                    for (Dependency dep : mi.getDependencies()) {
                        Collection<UpdateElement> requestedElements = Utilities.handleDependency(eagerEl, dep, Collections.singleton(mi), new HashSet<Dependency>(), this.type == OperationType.UPDATE || this.type == OperationType.INTERNAL_UPDATE);
                        if (requestedElements == null) continue;
                        for (UpdateElement req : requestedElements) {
                            reqs.add(req);
                        }
                    }
                    if ((reqs.isEmpty() || !all.containsAll(reqs) || all.contains(eagerEl)) && (!reqs.isEmpty() || impl.getUpdateUnit().getInstalled() == null || this.type != OperationType.UPDATE || this.operations.size() <= 0)) continue;
                    OperationContainer.OperationInfo<Support> i3 = null;
                    try {
                        if (impl instanceof ModuleUpdateElementImpl) {
                            i3 = this.add(eagerEl.getUpdateUnit(), eagerEl);
                        } else if (impl instanceof FeatureUpdateElementImpl) {
                            FeatureUpdateElementImpl eagerImpl2 = (FeatureUpdateElementImpl)impl;
                            for (UpdateElementImpl contained : eagerImpl2.getContainedModuleElements()) {
                                if (!contained.isEager()) continue;
                                i3 = this.add(contained.getUpdateUnit(), contained.getUpdateElement());
                            }
                        }
                    }
                    catch (IllegalArgumentException e) {
                        boolean firstCondition = !reqs.isEmpty() && all.containsAll(reqs) && !all.contains(eagerEl);
                        boolean secondCondition = reqs.isEmpty() && impl.getUpdateUnit().getInstalled() != null && this.type == OperationType.UPDATE && this.operations.size() > 0;
                        StringBuilder sb = new StringBuilder();
                        sb.append("\nIAE while adding eager element to the ").append((Object)this.type).append(" container\n");
                        sb.append("\nEager: ").append(eagerEl);
                        sb.append("\nFirst condition : ").append(firstCondition);
                        sb.append("\nSecond condition : ").append(secondCondition);
                        sb.append("\nInstalled: ").append(impl.getUpdateUnit().getInstalled());
                        sb.append("\nPending: ").append(impl.getUpdateUnit().isPending());
                        sb.append("\nreqs: ").append(reqs).append(" (total : ").append(reqs.size()).append(")");
                        sb.append("\nall: ").append(all).append(" (total : ").append(all.size()).append(")");
                        sb.append("\noperation: ").append(this.operations).append(" (total: ").append(this.operations.size());
                        sb.append("\neager available updates: ").append(eagerEl.getUpdateUnit().getAvailableUpdates());
                        sb.append("\nUpdateElements in operations:");
                        for (OperationContainer.OperationInfo<Support> op : this.operations) {
                            sb.append("\n  ").append(op.getUpdateElement());
                        }
                        sb.append("\nUpdateElements in all:");
                        for (UpdateElement elem : all) {
                            sb.append("\n  ").append(elem);
                        }
                        sb.append("\n");
                        LOGGER.log(Level.INFO, sb.toString(), e);
                        throw e;
                    }
                    if (i3 == null) continue;
                    this.affectedEagers.add(i3);
                }
            }
        }
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "== do listAllWithPossibleEager for " + (Object)((Object)this.type) + " operation ==");
            for (OperationContainer.OperationInfo<Support> info : this.operations) {
                LOGGER.log(Level.FINE, "--> " + info.getUpdateElement());
            }
            if (this.affectedEagers != null) {
                LOGGER.log(Level.FINE, "   == includes affected eagers for " + (Object)((Object)this.type) + " operation ==");
                for (OperationContainer.OperationInfo<Support> eagerInfo : this.affectedEagers) {
                    LOGGER.log(Level.FINE, "   --> " + eagerInfo.getUpdateElement());
                }
                LOGGER.log(Level.FINE, "   == done eagers. ==");
            }
            LOGGER.log(Level.FINE, "== done. ==");
        }
        this.upToDate = true;
        return this.listAll();
    }

    public List<OperationContainer.OperationInfo<Support>> listInvalid() {
        ArrayList<OperationContainer.OperationInfo<Support>> retval = new ArrayList<OperationContainer.OperationInfo<Support>>();
        List<OperationContainer.OperationInfo<Support>> infos = this.listAll();
        for (OperationContainer.OperationInfo<Support> oii : infos) {
            if (this.isValid(oii.getUpdateUnit(), oii.getUpdateElement())) continue;
            retval.add(oii);
        }
        return retval;
    }

    public boolean isValid(UpdateUnit updateUnit, UpdateElement updateElement) {
        boolean isValid;
        if (updateElement == null) {
            throw new IllegalArgumentException("UpdateElement cannot be null for UpdateUnit " + updateUnit);
        }
        if (updateUnit == null) {
            throw new IllegalArgumentException("UpdateUnit cannot be null for UpdateElement " + updateElement);
        }
        switch (this.type) {
            case INSTALL: {
                isValid = OperationValidator.isValidOperation(this.type, updateUnit, updateElement);
                if (isValid || this.operations.size() <= 0) break;
                isValid = OperationValidator.isValidOperation(OperationType.UPDATE, updateUnit, updateElement);
                break;
            }
            case UPDATE: {
                isValid = OperationValidator.isValidOperation(this.type, updateUnit, updateElement);
                if (isValid || this.operations.size() <= 0) break;
                isValid = OperationValidator.isValidOperation(OperationType.INSTALL, updateUnit, updateElement);
                break;
            }
            case INTERNAL_UPDATE: {
                isValid = OperationValidator.isValidOperation(this.type, updateUnit, updateElement);
                if (!isValid && this.operations.size() > 0) {
                    isValid = OperationValidator.isValidOperation(OperationType.UPDATE, updateUnit, updateElement);
                }
                if (isValid || this.operations.size() <= 0) break;
                isValid = OperationValidator.isValidOperation(OperationType.INSTALL, updateUnit, updateElement);
                break;
            }
            default: {
                isValid = OperationValidator.isValidOperation(this.type, updateUnit, updateElement);
            }
        }
        return isValid;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public synchronized void remove(OperationContainer.OperationInfo op) {
        OperationContainerImpl operationContainerImpl = this;
        synchronized (operationContainerImpl) {
            this.changeState(this.operations.remove(op));
            this.changeState(this.operations.removeAll(this.affectedEagers));
            this.affectedEagers.clear();
            boolean asserts = false;
            if (!$assertionsDisabled) {
                asserts = true;
                if (!true) {
                    throw new AssertionError();
                }
            }
            if (asserts) {
                this.lastModified = new Exception("Removed " + op);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public synchronized void removeAll() {
        OperationContainerImpl operationContainerImpl = this;
        synchronized (operationContainerImpl) {
            this.changeState(true);
            this.operations.clear();
            this.affectedEagers.clear();
            boolean asserts = false;
            if (!$assertionsDisabled) {
                asserts = true;
                if (!true) {
                    throw new AssertionError();
                }
            }
            if (asserts) {
                this.lastModified = new Exception("Removed all");
            }
        }
    }

    public String toString() {
        StringWriter sb = new StringWriter();
        PrintWriter pw = new PrintWriter(sb);
        pw.print(super.toString());
        if (this.lastModified != null) {
            pw.println();
            this.lastModified.printStackTrace(pw);
        }
        pw.flush();
        return sb.toString();
    }

    private void clearCache() {
        OperationValidator.clearMaps();
    }

    private void changeState(boolean changed) {
        if (changed) {
            this.clearCache();
        }
        this.upToDate = this.upToDate && !changed;
    }

    private OperationContainerImpl(OperationType type) {
        this.type = type;
    }

    public OperationType getType() {
        return this.type;
    }

    public static enum OperationType {
        INSTALL,
        UNINSTALL,
        INTERNAL_UPDATE,
        DIRECT_UNINSTALL,
        UPDATE,
        REVERT,
        ENABLE,
        DIRECT_DISABLE,
        DISABLE,
        CUSTOM_INSTALL,
        CUSTOM_UNINSTALL;
        

        private OperationType() {
        }
    }

    public class OperationInfoImpl<Support> {
        private final UpdateElement updateElement;
        private final UpdateUnit uUnit;
        private Set<String> brokenDeps;
        private List<UpdateElement> requiredElements;

        private OperationInfoImpl(UpdateUnit uUnit, UpdateElement updateElement) {
            this.brokenDeps = null;
            this.updateElement = updateElement;
            this.uUnit = uUnit;
        }

        public UpdateElement getUpdateElement() {
            return this.updateElement;
        }

        public UpdateUnit getUpdateUnit() {
            return this.uUnit;
        }

        public List<UpdateElement> getRequiredElements() {
            if (OperationContainerImpl.this.upToDate && this.requiredElements != null) {
                return this.requiredElements;
            }
            ArrayList<ModuleInfo> moduleInfos = new ArrayList<ModuleInfo>();
            for (OperationContainer.OperationInfo oii : OperationContainerImpl.this.listAll()) {
                UpdateElementImpl impl = Trampoline.API.impl(oii.getUpdateElement());
                List<ModuleInfo> infos = impl.getModuleInfos();
                assert (infos != null);
                moduleInfos.addAll(infos);
            }
            this.brokenDeps = new HashSet<String>();
            HashSet<UpdateElement> recommeded = new HashSet<UpdateElement>();
            this.requiredElements = OperationValidator.getRequiredElements(OperationContainerImpl.this.type, this.getUpdateElement(), moduleInfos, this.brokenDeps, recommeded);
            if (!this.brokenDeps.isEmpty() && !recommeded.isEmpty()) {
                this.brokenDeps = new HashSet<String>();
                this.requiredElements = OperationValidator.getRequiredElements(OperationContainerImpl.this.type, this.getUpdateElement(), moduleInfos, this.brokenDeps, recommeded);
            }
            return this.requiredElements;
        }

        public Set<String> getBrokenDependencies() {
            if (!OperationContainerImpl.this.upToDate) {
                this.brokenDeps = null;
            }
            if (this.brokenDeps != null) {
                return this.brokenDeps;
            }
            ArrayList<ModuleInfo> moduleInfos = new ArrayList<ModuleInfo>();
            for (OperationContainer.OperationInfo oii : OperationContainerImpl.this.listAll()) {
                UpdateElementImpl impl = Trampoline.API.impl(oii.getUpdateElement());
                List<ModuleInfo> infos = impl.getModuleInfos();
                assert (infos != null);
                moduleInfos.addAll(infos);
            }
            return OperationValidator.getBrokenDependencies(OperationContainerImpl.this.type, this.getUpdateElement(), moduleInfos);
        }
    }

}

