/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package org.netbeans.modules.autoupdate.services;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import org.openide.util.NbPreferences;

public class AutoupdateSettings {
    private static final String PROP_OPEN_CONNECTION_TIMEOUT = "plugin.manager.connection.timeout";
    public static final int DEFAULT_OPEN_CONNECTION_TIMEOUT = 30000;
    private static final String PROP_LAST_CHECK = "lastCheckTime";
    private static final Logger err = Logger.getLogger(AutoupdateSettings.class.getName());

    private AutoupdateSettings() {
    }

    public static void setLastCheck(Date lastCheck) {
        err.log(Level.FINER, "Set the last check to " + lastCheck);
        if (lastCheck != null) {
            AutoupdateSettings.getPreferences().putLong("lastCheckTime", lastCheck.getTime());
        } else {
            AutoupdateSettings.getPreferences().remove("lastCheckTime");
        }
    }

    public static void setLastCheck(String updateProviderName, Date lastCheck) {
        err.log(Level.FINER, "Set the last check to " + lastCheck);
        if (lastCheck != null) {
            AutoupdateSettings.getPreferences().putLong(updateProviderName + "_" + "lastCheckTime", lastCheck.getTime());
        } else {
            AutoupdateSettings.getPreferences().remove(updateProviderName + "_" + "lastCheckTime");
        }
    }

    private static Preferences getPreferences() {
        return NbPreferences.root().node("/org/netbeans/modules/autoupdate");
    }

    public static int getOpenConnectionTimeout() {
        return AutoupdateSettings.getPreferences().getInt("plugin.manager.connection.timeout", 30000);
    }
}

