/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.Dependency
 *  org.openide.modules.ModuleInfo
 */
package org.netbeans.modules.autoupdate.services;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateManager;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.api.autoupdate.UpdateUnitProvider;
import org.netbeans.api.autoupdate.UpdateUnitProviderFactory;
import org.netbeans.modules.autoupdate.services.DependencyAggregator;
import org.netbeans.modules.autoupdate.services.KitModuleUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.Trampoline;
import org.netbeans.modules.autoupdate.services.UpdateElementImpl;
import org.netbeans.modules.autoupdate.services.UpdateUnitFactory;
import org.netbeans.modules.autoupdate.services.UpdateUnitImpl;
import org.netbeans.modules.autoupdate.services.Utilities;
import org.netbeans.spi.autoupdate.UpdateProvider;
import org.openide.modules.Dependency;
import org.openide.modules.ModuleInfo;

public class UpdateManagerImpl {
    private static final UpdateManagerImpl INSTANCE = new UpdateManagerImpl();
    private static final UpdateManager.TYPE[] DEFAULT_TYPES = new UpdateManager.TYPE[]{UpdateManager.TYPE.KIT_MODULE};
    private Reference<Cache> cacheReference = null;
    private Map<String, UpdateUnitProvider> source2UpdateUnitProvider = null;

    public static UpdateManagerImpl getInstance() {
        return INSTANCE;
    }

    private UpdateManagerImpl() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void clearCache() {
        Class<Cache> class_ = Cache.class;
        synchronized (Cache.class) {
            this.cacheReference = null;
            this.source2UpdateUnitProvider = null;
            Utilities.writeFirstClassModule(null);
            // ** MonitorExit[var1_1] (shouldn't be in output)
            return;
        }
    }

    public static /* varargs */ List<UpdateUnit> getUpdateUnits(UpdateProvider provider, UpdateManager.TYPE ... types) {
        return UpdateManagerImpl.filterUnitsByAskedTypes(UpdateUnitFactory.getDefault().getUpdateUnits(provider).values(), UpdateManagerImpl.type2checkedList(types));
    }

    public /* varargs */ List<UpdateUnit> getUpdateUnits(UpdateManager.TYPE ... types) {
        final Cache c = this.getCache();
        return new ArrayList<UpdateUnit>(UpdateManagerImpl.filterUnitsByAskedTypes(c.getUnits(), UpdateManagerImpl.type2checkedList(types))){
            Cache keepIt;
        };
    }

    public Set<UpdateElement> getAvailableEagers() {
        final Cache c = this.getCache();
        return new HashSet<UpdateElement>(c.getAvailableEagers()){
            Cache keepIt;
        };
    }

    public Set<UpdateElement> getInstalledEagers() {
        final Cache c = this.getCache();
        return new HashSet<UpdateElement>(c.getInstalledEagers()){
            Cache keepIt;
        };
    }

    public Collection<ModuleInfo> getInstalledProviders(String token) {
        UpdateUnit updateUnit;
        final Cache c = this.getCache();
        if (token.startsWith("cnb.") && (updateUnit = c.getUpdateUnit(token.substring(4))) != null && updateUnit.getInstalled() != null) {
            return Trampoline.API.impl(updateUnit.getInstalled()).getModuleInfos();
        }
        Collection<ModuleInfo> providers = c.createMapToken2InstalledProviders().get(token);
        HashSet res = providers == null || providers.isEmpty() ? new HashSet<ModuleInfo>(0){
            Cache keepIt;
        } : new HashSet<ModuleInfo>(providers){
            Cache keepIt;
        };
        return res;
    }

    public Collection<ModuleInfo> getAvailableProviders(String token) {
        UpdateUnit updateUnit;
        final Cache c = this.getCache();
        if (token.startsWith("cnb.") && (updateUnit = c.getUpdateUnit(token.substring(4))) != null && !updateUnit.getAvailableUpdates().isEmpty()) {
            return Trampoline.API.impl(updateUnit.getAvailableUpdates().get(0)).getModuleInfos();
        }
        Collection<ModuleInfo> providers = c.createMapToken2AvailableProviders().get(token);
        HashSet res = providers == null || providers.isEmpty() ? new HashSet<ModuleInfo>(0){
            Cache keepIt;
        } : new HashSet<ModuleInfo>(providers){
            Cache keepIt;
        };
        return res;
    }

    public TreeSet<UpdateElement> getInstalledKits(String cluster) {
        final Cache c = this.getCache();
        TreeSet<UpdateElement> kits = c.createMapCluster2installedKits().get(cluster);
        TreeSet res = kits == null || kits.isEmpty() ? new TreeSet<UpdateElement>(){
            Cache keepIt;
        } : new TreeSet<UpdateElement>(kits){
            Cache keepIt;
        };
        return res;
    }

    public UpdateUnit getUpdateUnit(String moduleCodeName) {
        if (moduleCodeName.indexOf(47) != -1) {
            int to = moduleCodeName.indexOf(47);
            moduleCodeName = moduleCodeName.substring(0, to);
        }
        return this.getCache().getUpdateUnit(moduleCodeName);
    }

    public List<UpdateUnit> getUpdateUnits() {
        final Cache c = this.getCache();
        return new ArrayList<UpdateUnit>(c.getUnits()){
            Cache keepIt;
        };
    }

    private static List<UpdateUnit> filterUnitsByAskedTypes(Collection<UpdateUnit> units, List<UpdateManager.TYPE> types) {
        ArrayList<UpdateUnit> askedUnits = new ArrayList<UpdateUnit>();
        ArrayList<UpdateManager.TYPE> tmpTypes = new ArrayList<UpdateManager.TYPE>(types);
        if (tmpTypes.contains((Object)UpdateManager.TYPE.MODULE) && !tmpTypes.contains((Object)UpdateManager.TYPE.KIT_MODULE)) {
            tmpTypes.add(UpdateManager.TYPE.KIT_MODULE);
        }
        for (UpdateUnit unit : units) {
            UpdateUnitImpl impl = Trampoline.API.impl(unit);
            if (!tmpTypes.contains((Object)impl.getType())) continue;
            askedUnits.add(unit);
        }
        return askedUnits;
    }

    private static /* varargs */ List<UpdateManager.TYPE> type2checkedList(UpdateManager.TYPE ... types) {
        List<UpdateManager.TYPE> l = Arrays.asList(types);
        if (types != null && types.length > 1) {
            if (l.contains((Object)UpdateManager.TYPE.MODULE) && l.contains((Object)UpdateManager.TYPE.KIT_MODULE)) {
                throw new IllegalArgumentException("Cannot mix types MODULE and KIT_MODULE into once list.");
            }
        } else if (types == null || types.length == 0) {
            l = Arrays.asList(DEFAULT_TYPES);
        }
        return l;
    }

    private Cache getCache() {
        Cache retval;
        Reference<Cache> ref = this.getCacheReference();
        Cache cache = retval = ref != null ? ref.get() : null;
        if (retval == null) {
            retval = new Cache();
            this.initCache(retval);
        }
        return retval;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    Reference<Cache> getCacheReference() {
        Class<Cache> class_ = Cache.class;
        synchronized (Cache.class) {
            // ** MonitorExit[var1_1] (shouldn't be in output)
            return this.cacheReference;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void initCache(Cache c) {
        Class<Cache> class_ = Cache.class;
        synchronized (Cache.class) {
            this.cacheReference = new WeakReference<Cache>(c);
            // ** MonitorExit[var2_2] (shouldn't be in output)
            return;
        }
    }

    public UpdateUnitProvider getUpdateUnitProvider(String source) {
        if (this.source2UpdateUnitProvider == null) {
            List<UpdateUnitProvider> providers = UpdateUnitProviderFactory.getDefault().getUpdateUnitProviders(false);
            this.source2UpdateUnitProvider = new HashMap<String, UpdateUnitProvider>(providers.size());
            for (UpdateUnitProvider updateUnitProvider : providers) {
                this.source2UpdateUnitProvider.put(updateUnitProvider.getDisplayName(), updateUnitProvider);
            }
        }
        return this.source2UpdateUnitProvider.get(source);
    }

    private class Cache {
        private Map<String, UpdateUnit> units;
        private Set<UpdateElement> availableEagers;
        private Set<UpdateElement> installedEagers;
        private Map<String, Collection<ModuleInfo>> token2installedProviders;
        private Map<String, Collection<ModuleInfo>> token2availableProviders;
        private Map<String, TreeSet<UpdateElement>> cluster2installedKits;

        Cache() {
            this.availableEagers = null;
            this.installedEagers = null;
            this.token2installedProviders = null;
            this.token2availableProviders = null;
            this.cluster2installedKits = null;
            this.units = UpdateUnitFactory.getDefault().getUpdateUnits();
        }

        public Set<UpdateElement> getAvailableEagers() {
            if (this.availableEagers == null) {
                this.createMaps();
            }
            assert (this.availableEagers != null);
            return this.availableEagers;
        }

        public Set<UpdateElement> getInstalledEagers() {
            if (this.installedEagers == null) {
                this.createMaps();
            }
            assert (this.installedEagers != null);
            return this.installedEagers;
        }

        public Map<String, Collection<ModuleInfo>> createMapToken2InstalledProviders() {
            if (this.token2installedProviders == null) {
                this.createMaps();
            }
            assert (this.token2installedProviders != null);
            return this.token2installedProviders;
        }

        public Map<String, Collection<ModuleInfo>> createMapToken2AvailableProviders() {
            if (this.token2availableProviders == null) {
                this.createMaps();
            }
            assert (this.token2availableProviders != null);
            return this.token2availableProviders;
        }

        public Map<String, TreeSet<UpdateElement>> createMapCluster2installedKits() {
            if (this.cluster2installedKits == null) {
                this.createMaps();
            }
            assert (this.cluster2installedKits != null);
            return this.cluster2installedKits;
        }

        public Collection<UpdateUnit> getUnits() {
            return this.units.values();
        }

        public UpdateUnit getUpdateUnit(String moduleCodeName) {
            return this.units.get(moduleCodeName);
        }

        private synchronized void createMaps() {
            this.availableEagers = new HashSet<UpdateElement>(this.getUnits().size());
            this.installedEagers = new HashSet<UpdateElement>(this.getUnits().size());
            this.token2installedProviders = new HashMap<String, Collection<ModuleInfo>>(11);
            this.token2availableProviders = new HashMap<String, Collection<ModuleInfo>>(11);
            this.cluster2installedKits = new HashMap<String, TreeSet<UpdateElement>>();
            DependencyAggregator.clearMaps();
            for (UpdateUnit unit : this.getUnits()) {
                UpdateElement el = unit.getInstalled();
                if (el != null) {
                    String cluster;
                    UpdateElementImpl elImpl = Trampoline.API.impl(el);
                    if (elImpl.isEager()) {
                        this.installedEagers.add(el);
                    }
                    for (ModuleInfo mi : elImpl.getModuleInfos()) {
                        for (Dependency dep : mi.getDependencies()) {
                            DependencyAggregator dec = DependencyAggregator.getAggregator(dep);
                            dec.addDependee(mi);
                        }
                        String[] provs = mi.getProvides();
                        if (provs == null || provs.length == 0) continue;
                        for (String token : provs) {
                            if (this.token2installedProviders.get(token) == null) {
                                this.token2installedProviders.put(token, new HashSet());
                            }
                            this.token2installedProviders.get(token).add(mi);
                        }
                    }
                    if (elImpl instanceof KitModuleUpdateElementImpl && (cluster = ((KitModuleUpdateElementImpl)elImpl).getInstallationCluster()) != null) {
                        if (this.cluster2installedKits.get(cluster) == null) {
                            TreeSet s = new TreeSet(new Comparator<UpdateElement>(){

                                @Override
                                public int compare(UpdateElement ue1, UpdateElement ue2) {
                                    return ue1.getCodeName().compareTo(ue2.getCodeName());
                                }
                            });
                            this.cluster2installedKits.put(cluster, s);
                        }
                        this.cluster2installedKits.get(cluster).add(el);
                    }
                }
                if (unit.getAvailableUpdates().isEmpty()) continue;
                el = unit.getAvailableUpdates().get(0);
                if (Trampoline.API.impl(el).isEager()) {
                    this.availableEagers.add(el);
                }
                for (ModuleInfo mi : Trampoline.API.impl(el).getModuleInfos()) {
                    for (Dependency dep : mi.getDependencies()) {
                        DependencyAggregator dec = DependencyAggregator.getAggregator(dep);
                        dec.addDependee(mi);
                    }
                    String[] provs = mi.getProvides();
                    if (provs == null || provs.length == 0) continue;
                    for (String token : provs) {
                        if (this.token2availableProviders.get(token) == null) {
                            this.token2availableProviders.put(token, new HashSet());
                        }
                        this.token2availableProviders.get(token).add(mi);
                    }
                }
            }
        }

    }

}

