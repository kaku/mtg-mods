/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.ModuleInfo
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 */
package org.netbeans.modules.autoupdate.services;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.swing.event.ChangeListener;
import org.openide.modules.ModuleInfo;
import org.openide.util.ChangeSupport;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;

final class ModuleCache
implements PropertyChangeListener,
LookupListener {
    private static ModuleCache INSTANCE;
    private final Lookup.Result<ModuleInfo> result;
    private final ChangeSupport support;
    private Map<String, ModuleInfo> infos;

    private ModuleCache() {
        this.support = new ChangeSupport((Object)this);
        this.result = Lookup.getDefault().lookupResult(ModuleInfo.class);
        this.result.addLookupListener((LookupListener)this);
        this.resultChanged(null);
    }

    public static synchronized ModuleCache getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ModuleCache();
        }
        return INSTANCE;
    }

    public void addChangeListener(ChangeListener l) {
        this.support.addChangeListener(l);
    }

    public void removeChangeListener(ChangeListener l) {
        this.support.removeChangeListener(l);
    }

    public ModuleInfo find(String cnb) {
        return this.infos.get(cnb);
    }

    public void resultChanged(LookupEvent ev) {
        for (ModuleInfo m : this.result.allInstances()) {
            m.removePropertyChangeListener((PropertyChangeListener)this);
            m.addPropertyChangeListener((PropertyChangeListener)this);
        }
        HashMap<String, ModuleInfo> tmp = new HashMap<String, ModuleInfo>();
        for (ModuleInfo mi : this.result.allInstances()) {
            tmp.put(mi.getCodeNameBase(), mi);
        }
        this.infos = tmp;
        if (ev != null) {
            this.fireChange();
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("enabled".equals(evt.getPropertyName())) {
            ModuleInfo mi = (ModuleInfo)evt.getSource();
        }
    }

    private void fireChange() {
        this.support.fireChange();
    }
}

