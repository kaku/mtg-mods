/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.Util
 *  org.openide.modules.Dependency
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.SpecificationVersion
 */
package org.netbeans.modules.autoupdate.services;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.Util;
import org.netbeans.modules.autoupdate.services.UpdateManagerImpl;
import org.openide.modules.Dependency;
import org.openide.modules.ModuleInfo;
import org.openide.modules.SpecificationVersion;

class DependencyChecker {
    private static final Logger err = Logger.getLogger(DependencyChecker.class.getName());

    DependencyChecker() {
    }

    public static Set<Dependency> findBrokenDependencies(Set<Dependency> deps, Collection<ModuleInfo> modules) {
        HashSet<Dependency> res = new HashSet<Dependency>();
        block7 : for (Dependency dep : deps) {
            err.log(Level.FINE, "Dependency[" + dep.getType() + "]: " + (Object)dep);
            switch (dep.getType()) {
                case 5: 
                case 6: {
                    if (!DependencyChecker.findModuleMatchesDependencyRequires(dep, modules).isEmpty()) continue block7;
                    res.add(dep);
                    break;
                }
                case 7: {
                    break;
                }
                case 1: {
                    if (DependencyChecker.matchDependencyModule(dep, modules) != null) break;
                    res.add(dep);
                    break;
                }
                case 3: {
                    if (DependencyChecker.matchDependencyJava(dep)) continue block7;
                    err.log(Level.FINE, "The Java platform version " + (Object)dep + " or higher was requested but only " + (Object)Dependency.JAVA_SPEC + " is running.");
                    res.add(dep);
                    break;
                }
                case 2: {
                    if (DependencyChecker.matchPackageDependency(dep)) continue block7;
                    err.log(Level.FINE, "The package " + (Object)dep + " was requested but it is not in current ClassPath.");
                    res.add(dep);
                    break;
                }
                default: {
                    err.log(Level.FINE, "Uncovered Dependency " + (Object)dep);
                }
            }
        }
        return res;
    }

    public static Set<Dependency> findBrokenDependenciesTransitive(ModuleInfo info, Collection<ModuleInfo> modules, Set<ModuleInfo> seen) {
        if (seen.contains((Object)info)) {
            return Collections.emptySet();
        }
        seen.add(info);
        HashSet<Dependency> res = new HashSet<Dependency>();
        block6 : for (Dependency dep : DependencyChecker.filterTypeRecommends(info.getDependencies())) {
            err.log(Level.FINE, "Dependency[" + dep.getType() + "]: " + (Object)dep);
            Collection<ModuleInfo> providers = null;
            switch (dep.getType()) {
                case 5: 
                case 6: 
                case 7: {
                    providers = DependencyChecker.findModuleMatchesDependencyRequires(dep, modules);
                    if (providers.size() > 0) {
                        for (ModuleInfo m : providers) {
                            res.addAll(DependencyChecker.findBrokenDependenciesTransitive(m, modules, seen));
                        }
                        continue block6;
                    }
                    res.add(dep);
                    break;
                }
                case 1: {
                    ModuleInfo m = DependencyChecker.matchDependencyModule(dep, modules);
                    if (m != null) {
                        res.addAll(DependencyChecker.findBrokenDependenciesTransitive(m, modules, seen));
                        break;
                    }
                    res.add(dep);
                    break;
                }
                case 3: {
                    if (DependencyChecker.matchDependencyJava(dep)) continue block6;
                    err.log(Level.FINE, "The Java platform version " + (Object)dep + " or higher was requested but only " + (Object)Dependency.JAVA_SPEC + " is running.");
                    res.add(dep);
                    break;
                }
                case 2: {
                    if (DependencyChecker.matchPackageDependency(dep)) continue block6;
                    err.log(Level.FINE, "The package " + (Object)dep + " was requested but it is not in current ClassPath.");
                    res.add(dep);
                    break;
                }
                default: {
                    err.log(Level.FINE, "Uncovered Dependency " + (Object)dep);
                }
            }
        }
        return res;
    }

    private static Set<Dependency> filterTypeRecommends(Collection<Dependency> deps) {
        HashSet<Dependency> res = new HashSet<Dependency>();
        for (Dependency dep : deps) {
            if (7 == dep.getType()) continue;
            res.add(dep);
        }
        return res;
    }

    static Collection<ModuleInfo> findModuleMatchesDependencyRequires(Dependency dep, Collection<ModuleInfo> modules) {
        UpdateManagerImpl mgr = UpdateManagerImpl.getInstance();
        HashSet<ModuleInfo> providers = new HashSet<ModuleInfo>();
        providers.addAll(mgr.getAvailableProviders(dep.getName()));
        providers.addAll(mgr.getInstalledProviders(dep.getName()));
        HashSet<ModuleInfo> res = new HashSet<ModuleInfo>(providers);
        for (ModuleInfo mi : providers) {
            for (ModuleInfo input : modules) {
                if (!mi.getCodeName().equals(input.getCodeName())) continue;
                res.add(mi);
            }
        }
        return res;
    }

    private static ModuleInfo matchDependencyModule(Dependency dep, Collection<ModuleInfo> modules) {
        for (ModuleInfo module : modules) {
            if (!DependencyChecker.checkDependencyModule(dep, module)) continue;
            return module;
        }
        return null;
    }

    public static boolean matchDependencyJava(Dependency dep) {
        if (dep.getName().equals("Java") && 1 == dep.getComparison()) {
            return Dependency.JAVA_SPEC.compareTo((Object)new SpecificationVersion(dep.getVersion())) >= 0;
        }
        return true;
    }

    public static boolean matchPackageDependency(Dependency dep) {
        return Util.checkPackageDependency((Dependency)dep, (ClassLoader)Util.class.getClassLoader());
    }

    static boolean checkDependencyModuleAllowEqual(Dependency dep, ModuleInfo module) {
        return DependencyChecker.checkDependencyModule(dep, module, true);
    }

    static boolean checkDependencyModule(Dependency dep, ModuleInfo module) {
        return DependencyChecker.checkDependencyModule(dep, module, false);
    }

    private static boolean checkDependencyModule(Dependency dep, ModuleInfo module, boolean allowEqual) {
        boolean ok = false;
        if (dep.getName().equals(module.getCodeNameBase()) || dep.getName().equals(module.getCodeName())) {
            ok = dep.getComparison() == 3 ? true : (dep.getComparison() == 1 ? (module.getSpecificationVersion() == null ? false : (new SpecificationVersion(dep.getVersion()).compareTo((Object)module.getSpecificationVersion()) > 0 ? false : (allowEqual && new SpecificationVersion(dep.getVersion()).compareTo((Object)module.getSpecificationVersion()) == 0 ? true : true))) : (module.getImplementationVersion() == null ? false : (!module.getImplementationVersion().equals(dep.getVersion()) ? false : (dep.getName().indexOf(47) == -1 || module.getCodeName().indexOf(47) != -1 ? dep.getName().equals(module.getCodeName()) : true))));
        } else {
            String cnb;
            int dash = dep.getName().indexOf(45);
            if (dash != -1) {
                int slash = dep.getName().indexOf(47);
                String cnb2 = dep.getName().substring(0, slash);
                int relMin = Integer.parseInt(dep.getName().substring(slash + 1, dash));
                int relMax = Integer.parseInt(dep.getName().substring(dash + 1));
                if (cnb2.equals(module.getCodeNameBase()) && relMin <= module.getCodeNameRelease() && relMax >= module.getCodeNameRelease()) {
                    ok = dep.getComparison() == 3 ? true : (module.getCodeNameRelease() > relMin ? true : (module.getSpecificationVersion() == null ? false : (new SpecificationVersion(dep.getVersion()).compareTo((Object)module.getSpecificationVersion()) > 0 ? false : (allowEqual && new SpecificationVersion(dep.getVersion()).compareTo((Object)module.getSpecificationVersion()) > 0 ? true : true))));
                }
            } else if (dep.getName().indexOf(47) != -1 && (cnb = dep.getName().substring(0, dep.getName().indexOf(47))).equals(module.getCodeNameBase())) {
                err.log(Level.FINE, "Unmatched major versions. Dependency " + (Object)dep + " doesn't match with module " + (Object)module);
                ok = false;
            }
        }
        return ok;
    }
}

