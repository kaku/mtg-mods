/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.autoupdate.services;

import org.netbeans.spi.autoupdate.CustomInstaller;
import org.netbeans.spi.autoupdate.CustomUninstaller;

public class UpdateItemDeploymentImpl {
    private Boolean needsRestart;
    private Boolean isGlobal;
    private String targetCluster;
    private CustomInstaller installer;
    private CustomUninstaller uninstaller;

    public UpdateItemDeploymentImpl(Boolean needsRestart, Boolean isGlobal, String targetCluster, CustomInstaller installer, CustomUninstaller uninstaller) {
        this.needsRestart = needsRestart;
        this.isGlobal = isGlobal;
        this.targetCluster = targetCluster;
        this.installer = installer;
        this.uninstaller = uninstaller;
    }

    public String getTargetCluster() {
        return this.targetCluster;
    }

    public Boolean needsRestart() {
        return this.needsRestart;
    }

    public Boolean isGlobal() {
        return this.isGlobal;
    }

    public CustomInstaller getCustomInstaller() {
        return this.installer;
    }

    public CustomUninstaller getCustomUninstaller() {
        return this.uninstaller;
    }
}

