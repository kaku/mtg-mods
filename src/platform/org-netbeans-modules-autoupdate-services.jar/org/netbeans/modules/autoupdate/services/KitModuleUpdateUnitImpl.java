/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.autoupdate.services;

import org.netbeans.api.autoupdate.UpdateManager;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.modules.autoupdate.services.ModuleUpdateUnitImpl;

public class KitModuleUpdateUnitImpl
extends ModuleUpdateUnitImpl {
    public KitModuleUpdateUnitImpl(String codename) {
        super(codename);
    }

    @Override
    public UpdateManager.TYPE getType() {
        return UpdateManager.TYPE.KIT_MODULE;
    }

    @Override
    public UpdateUnit getVisibleAncestor() {
        return this.getUpdateUnit();
    }
}

