/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.Dependency
 *  org.openide.modules.ModuleInfo
 */
package org.netbeans.modules.autoupdate.services;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.modules.autoupdate.services.UpdateManagerImpl;
import org.openide.modules.Dependency;
import org.openide.modules.ModuleInfo;

public class DependencyAggregator {
    private static Map<DependencyDecoratorKey, DependencyAggregator> key2dependency = new HashMap<DependencyDecoratorKey, DependencyAggregator>(11, 11.0f);
    private Collection<ModuleInfo> depending = new HashSet<ModuleInfo>();
    private final DependencyDecoratorKey key;
    private static final Object LOCK = new Object();

    private DependencyAggregator(DependencyDecoratorKey key) {
        this.key = key;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static DependencyAggregator getAggregator(Dependency dep) {
        DependencyDecoratorKey key = new DependencyDecoratorKey(dep.getName(), dep.getType(), dep.getComparison());
        Object object = LOCK;
        synchronized (object) {
            DependencyAggregator res = key2dependency.get(key);
            if (res == null) {
                res = new DependencyAggregator(key);
                key2dependency.put(key, res);
            }
            return res;
        }
    }

    public int getType() {
        return this.key.type;
    }

    public String getName() {
        return this.key.name;
    }

    public boolean addDependee(ModuleInfo dependee) {
        return this.depending.add(dependee);
    }

    public Collection<ModuleInfo> getDependening() {
        return this.depending;
    }

    static void clearMaps() {
        key2dependency = new HashMap<DependencyDecoratorKey, DependencyAggregator>(11, 11.0f);
    }

    public String toString() {
        return "DependencyDecorator[" + this.key.toString() + "]";
    }

    public static Collection<UpdateUnit> getRequested(Dependency dep) {
        switch (dep.getType()) {
            case 1: {
                return Collections.singleton(UpdateManagerImpl.getInstance().getUpdateUnit(dep.getName()));
            }
            case 5: 
            case 6: 
            case 7: {
                HashSet<UpdateUnit> requestedUnits = new HashSet<UpdateUnit>();
                Collection<ModuleInfo> installedProviders = UpdateManagerImpl.getInstance().getInstalledProviders(dep.getName());
                if (installedProviders.isEmpty()) {
                    Collection<ModuleInfo> availableProviders = UpdateManagerImpl.getInstance().getAvailableProviders(dep.getName());
                    if (availableProviders.isEmpty()) {
                        return null;
                    }
                    for (ModuleInfo mi : availableProviders) {
                        UpdateUnit availableUnit = UpdateManagerImpl.getInstance().getUpdateUnit(mi.getCodeNameBase());
                        if (availableUnit == null) continue;
                        requestedUnits.add(availableUnit);
                    }
                    return requestedUnits;
                }
                for (ModuleInfo mi : installedProviders) {
                    UpdateUnit installedUnit = UpdateManagerImpl.getInstance().getUpdateUnit(mi.getCodeNameBase());
                    if (installedUnit == null) continue;
                    requestedUnits.add(installedUnit);
                }
                return requestedUnits;
            }
        }
        return null;
    }

    public static class DependencyDecoratorKey {
        private final String name;
        private final int type;
        private final int hashCode;

        public DependencyDecoratorKey(String name, int dependencyType, int comparison) {
            this.name = name;
            this.type = dependencyType;
            this.hashCode = 772067 ^ this.type ^ name.hashCode();
        }

        public boolean equals(Object o) {
            if (o.getClass() != DependencyDecoratorKey.class) {
                return false;
            }
            DependencyDecoratorKey d = (DependencyDecoratorKey)o;
            return this.type == d.type && this.name.equals(d.name);
        }

        public int hashCode() {
            return this.hashCode;
        }

        public String toString() {
            StringBuilder buf = new StringBuilder(100);
            buf.append("Key[");
            if (this.type == 1) {
                buf.append("module ");
            } else if (this.type == 2) {
                buf.append("package ");
            } else if (this.type == 5) {
                buf.append("requires ");
            } else if (this.type == 6) {
                buf.append("needs ");
            } else if (this.type == 7) {
                buf.append("recommends ");
            }
            buf.append(this.name);
            buf.append(']');
            return buf.toString();
        }
    }

}

