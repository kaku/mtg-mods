/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.Dependency
 *  org.openide.modules.ModuleInfo
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.autoupdate.services;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateManager;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.api.autoupdate.UpdateUnitProvider;
import org.netbeans.modules.autoupdate.services.FeatureUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.FeatureUpdateUnitImpl;
import org.netbeans.modules.autoupdate.services.KitModuleUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.KitModuleUpdateUnitImpl;
import org.netbeans.modules.autoupdate.services.LocalizationUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.LocalizationUpdateUnitImpl;
import org.netbeans.modules.autoupdate.services.ModuleUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.ModuleUpdateUnitImpl;
import org.netbeans.modules.autoupdate.services.NativeComponentUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.NativeComponentUpdateUnitImpl;
import org.netbeans.modules.autoupdate.services.Trampoline;
import org.netbeans.modules.autoupdate.services.UpdateElementImpl;
import org.netbeans.modules.autoupdate.services.UpdateManagerImpl;
import org.netbeans.modules.autoupdate.services.UpdateUnitImpl;
import org.netbeans.modules.autoupdate.services.UpdateUnitProviderImpl;
import org.netbeans.modules.autoupdate.services.Utilities;
import org.netbeans.modules.autoupdate.updateprovider.BackupUpdateProvider;
import org.netbeans.modules.autoupdate.updateprovider.FeatureItem;
import org.netbeans.modules.autoupdate.updateprovider.InstalledModuleItem;
import org.netbeans.modules.autoupdate.updateprovider.InstalledModuleProvider;
import org.netbeans.modules.autoupdate.updateprovider.InstalledUpdateProvider;
import org.netbeans.modules.autoupdate.updateprovider.LocalizationItem;
import org.netbeans.modules.autoupdate.updateprovider.ModuleItem;
import org.netbeans.modules.autoupdate.updateprovider.NativeComponentItem;
import org.netbeans.modules.autoupdate.updateprovider.UpdateItemImpl;
import org.netbeans.spi.autoupdate.UpdateItem;
import org.netbeans.spi.autoupdate.UpdateProvider;
import org.openide.modules.Dependency;
import org.openide.modules.ModuleInfo;
import org.openide.util.NbBundle;

public class UpdateUnitFactory {
    private static final UpdateUnitFactory INSTANCE = new UpdateUnitFactory();
    private final Logger log;
    private static final DateFormat FMT = new SimpleDateFormat("mm:ss:SS");
    private static long runTime = -1;
    private Set<String> scheduledForRestartUE;
    private Set<String> scheduledForRestartUU;
    public static final String UNSORTED_CATEGORY = NbBundle.getMessage(UpdateUnitFactory.class, (String)"UpdateUnitFactory_Unsorted_Category");
    public static final String LIBRARIES_CATEGORY = NbBundle.getMessage(UpdateUnitFactory.class, (String)"UpdateUnitFactory_Libraries_Category");
    public static final String BRIDGES_CATEGORY = NbBundle.getMessage(UpdateUnitFactory.class, (String)"UpdateUnitFactory_Bridges_Category");

    private UpdateUnitFactory() {
        this.log = Logger.getLogger(this.getClass().getName());
        this.scheduledForRestartUE = null;
        this.scheduledForRestartUU = null;
    }

    public static UpdateUnitFactory getDefault() {
        return INSTANCE;
    }

    public Map<String, UpdateUnit> getUpdateUnits() {
        this.resetRunTime("Measuring of UpdateUnitFactory.getUpdateUnits()");
        List<UpdateUnitProvider> updates = UpdateUnitProviderImpl.getUpdateUnitProviders(true);
        try {
            InstalledModuleProvider.getDefault().getUpdateItems();
        }
        catch (Exception x) {
            x.printStackTrace();
        }
        this.reportRunTime("Get all installed modules.");
        Map<String, UpdateUnit> mappedImpl = this.appendUpdateItems(new HashMap<String, UpdateUnit>(), InstalledModuleProvider.getDefault());
        this.reportRunTime("Append installed units.");
        for (UpdateUnitProvider up : updates) {
            UpdateUnitProviderImpl impl = Trampoline.API.impl(up);
            mappedImpl = this.appendUpdateItems(mappedImpl, impl.getUpdateProvider());
            this.reportRunTime("AppendUpdateItems for " + impl.getUpdateProvider().getDisplayName());
        }
        return mappedImpl;
    }

    public Map<String, UpdateUnit> getUpdateUnits(UpdateProvider provider) {
        this.resetRunTime("Measuring UpdateUnitFactory.getUpdateUnits (" + provider.getDisplayName() + ")");
        Map<String, UpdateUnit> temp = this.appendUpdateItems(new HashMap<String, UpdateUnit>(), provider);
        this.reportRunTime("Get appendUpdateItems for " + provider.getDisplayName());
        HashMap<String, UpdateUnit> retval = new HashMap<String, UpdateUnit>();
        for (UpdateUnit unit : temp.values()) {
            retval.put(unit.getCodeName(), this.mergeInstalledUpdateUnit(unit));
        }
        this.reportRunTime("Get filltering by " + provider.getDisplayName());
        return temp;
    }

    Map<String, UpdateUnit> appendUpdateItems(Map<String, UpdateUnit> originalUnits, UpdateProvider provider) {
        Map<String, UpdateItem> items;
        assert (originalUnits != null);
        try {
            items = provider.getUpdateItems();
        }
        catch (IOException ioe) {
            this.log.log(Level.INFO, "Cannot read UpdateItem from UpdateProvider " + provider, ioe);
            return originalUnits;
        }
        assert (items != null);
        for (String simpleItemId : items.keySet()) {
            UpdateElement updateEl = null;
            try {
                UpdateItemImpl itemImpl = Trampoline.SPI.impl(items.get(simpleItemId));
                boolean isKitModule = false;
                if (itemImpl instanceof ModuleItem) {
                    ModuleInfo mi = ((ModuleItem)itemImpl).getModuleInfo();
                    assert (mi != null);
                    isKitModule = Utilities.isKitModule(mi);
                }
                if (itemImpl instanceof InstalledModuleItem) {
                    if (isKitModule) {
                        KitModuleUpdateElementImpl impl = new KitModuleUpdateElementImpl((InstalledModuleItem)itemImpl, null);
                        updateEl = Trampoline.API.createUpdateElement(impl);
                    } else {
                        ModuleUpdateElementImpl impl = new ModuleUpdateElementImpl((InstalledModuleItem)itemImpl, null);
                        updateEl = Trampoline.API.createUpdateElement(impl);
                    }
                } else if (itemImpl instanceof ModuleItem) {
                    if (isKitModule) {
                        KitModuleUpdateElementImpl impl = new KitModuleUpdateElementImpl((ModuleItem)itemImpl, provider.getDisplayName());
                        updateEl = Trampoline.API.createUpdateElement(impl);
                    } else {
                        ModuleUpdateElementImpl impl = new ModuleUpdateElementImpl((ModuleItem)itemImpl, provider.getDisplayName());
                        updateEl = Trampoline.API.createUpdateElement(impl);
                    }
                } else if (itemImpl instanceof LocalizationItem) {
                    updateEl = Trampoline.API.createUpdateElement(new LocalizationUpdateElementImpl((LocalizationItem)itemImpl, provider.getDisplayName()));
                } else if (itemImpl instanceof NativeComponentItem) {
                    updateEl = Trampoline.API.createUpdateElement(new NativeComponentUpdateElementImpl((NativeComponentItem)itemImpl, provider.getDisplayName()));
                } else if (itemImpl instanceof FeatureItem) {
                    FeatureUpdateElementImpl.Agent impl = new FeatureUpdateElementImpl.Agent((FeatureItem)itemImpl, provider.getDisplayName(), UpdateManager.TYPE.FEATURE);
                    updateEl = Trampoline.API.createUpdateElement(impl);
                } else assert (false);
            }
            catch (IllegalArgumentException iae) {
                this.log.log(Level.INFO, iae.getLocalizedMessage(), iae);
            }
            if (updateEl == null) continue;
            this.addElement(originalUnits, updateEl, provider);
        }
        return originalUnits;
    }

    private void addElement(Map<String, UpdateUnit> impls, UpdateElement element, UpdateProvider provider) {
        UpdateUnit unit = impls.get(element.getCodeName());
        boolean passed = false;
        UpdateElementImpl elImpl = Trampoline.API.impl(element);
        if (elImpl instanceof ModuleUpdateElementImpl && elImpl.getModuleInfos() != null && elImpl.getModuleInfos().size() == 1) {
            for (Dependency d : elImpl.getModuleInfos().get(0).getDependencies()) {
                if (5 != d.getType() || !d.getName().startsWith("org.openide.modules.os")) continue;
                for (ModuleInfo info : InstalledModuleProvider.getInstalledModules().values()) {
                    if (!Arrays.asList(info.getProvides()).contains(d.getName())) continue;
                    this.log.log(Level.FINEST, element + " which requires OS " + (Object)d + " succeed.");
                    passed = true;
                    break;
                }
                if (passed) continue;
                this.log.log(Level.FINE, element + " which requires OS " + (Object)d + " fails.");
                return;
            }
        }
        UpdateUnitImpl unitImpl = null;
        if (unit == null) {
            switch (elImpl.getType()) {
                case MODULE: {
                    unitImpl = new ModuleUpdateUnitImpl(element.getCodeName());
                    break;
                }
                case KIT_MODULE: {
                    unitImpl = new KitModuleUpdateUnitImpl(element.getCodeName());
                    break;
                }
                case STANDALONE_MODULE: 
                case FEATURE: {
                    unitImpl = new FeatureUpdateUnitImpl(element.getCodeName(), elImpl.getType());
                    break;
                }
                case CUSTOM_HANDLED_COMPONENT: {
                    unitImpl = new NativeComponentUpdateUnitImpl(element.getCodeName());
                    break;
                }
                case LOCALIZATION: {
                    unitImpl = new LocalizationUpdateUnitImpl(element.getCodeName());
                    break;
                }
                default: {
                    assert (false);
                    break;
                }
            }
            unit = Trampoline.API.createUpdateUnit(unitImpl);
            impls.put(unit.getCodeName(), unit);
        } else {
            unitImpl = Trampoline.API.impl(unit);
        }
        if (provider == InstalledUpdateProvider.getDefault()) {
            if (unitImpl.getInstalled() == null) {
                unitImpl.setInstalled(element);
            }
        } else if (provider instanceof BackupUpdateProvider) {
            unitImpl.setBackup(element);
        } else {
            unitImpl.addUpdate(element);
        }
        elImpl.setUpdateUnit(unit);
    }

    private UpdateUnit mergeInstalledUpdateUnit(UpdateUnit uu) {
        UpdateUnit fromCache = UpdateManagerImpl.getInstance().getUpdateUnit(uu.getCodeName());
        if (fromCache != null && fromCache.getInstalled() != null) {
            UpdateUnitImpl impl = Trampoline.API.impl(uu);
            impl.setInstalled(fromCache.getInstalled());
        }
        return uu;
    }

    private void resetRunTime(String msg) {
        if (this.log.isLoggable(Level.FINE)) {
            if (msg != null) {
                this.log.log(Level.FINE, "|=== " + msg + " ===|");
            }
            runTime = System.currentTimeMillis();
        }
    }

    private void reportRunTime(String msg) {
        if (this.log.isLoggable(Level.FINE)) {
            if (msg != null) {
                this.log.log(Level.FINE, msg + " === " + FMT.format(new Date(System.currentTimeMillis() - runTime)));
            }
            this.resetRunTime(null);
        }
    }

    public void scheduleForRestart(UpdateElement el) {
        if (this.scheduledForRestartUE == null) {
            this.scheduledForRestartUE = new HashSet<String>();
            this.scheduledForRestartUU = new HashSet<String>();
        }
        this.scheduledForRestartUE.add(el.getCodeName() + "_" + el.getSpecificationVersion());
        this.scheduledForRestartUU.add(el.getCodeName());
    }

    public boolean isScheduledForRestart(UpdateElement el) {
        return this.scheduledForRestartUE != null && this.scheduledForRestartUE.contains(el.getCodeName() + "_" + el.getSpecificationVersion());
    }

    public boolean isScheduledForRestart(UpdateUnit u) {
        return this.scheduledForRestartUU != null && this.scheduledForRestartUU.contains(u.getCodeName());
    }

}

