/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.Module
 *  org.netbeans.ModuleManager
 *  org.openide.modules.Dependency
 *  org.openide.modules.ModuleInfo
 */
package org.netbeans.modules.autoupdate.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.Module;
import org.netbeans.ModuleManager;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateManager;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.modules.autoupdate.services.FeatureUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.ModuleDeleterImpl;
import org.netbeans.modules.autoupdate.services.ModuleUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.NativeComponentUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.OperationContainerImpl;
import org.netbeans.modules.autoupdate.services.Trampoline;
import org.netbeans.modules.autoupdate.services.UpdateElementImpl;
import org.netbeans.modules.autoupdate.services.UpdateItemDeploymentImpl;
import org.netbeans.modules.autoupdate.services.UpdateManagerImpl;
import org.netbeans.modules.autoupdate.services.Utilities;
import org.netbeans.modules.autoupdate.updateprovider.InstallInfo;
import org.netbeans.modules.autoupdate.updateprovider.NativeComponentItem;
import org.netbeans.spi.autoupdate.CustomInstaller;
import org.netbeans.spi.autoupdate.CustomUninstaller;
import org.openide.modules.Dependency;
import org.openide.modules.ModuleInfo;

abstract class OperationValidator {
    private static final OperationValidator FOR_INSTALL = new InstallValidator();
    private static final OperationValidator FOR_INTERNAL_UPDATE = new InternalUpdateValidator();
    private static final OperationValidator FOR_UNINSTALL = new UninstallValidator();
    private static final OperationValidator FOR_UPDATE = new UpdateValidator();
    private static final OperationValidator FOR_ENABLE = new EnableValidator();
    private static final OperationValidator FOR_DISABLE = new DisableValidator();
    private static final OperationValidator FOR_CUSTOM_INSTALL = new CustomInstallValidator();
    private static final OperationValidator FOR_CUSTOM_UNINSTALL = new CustomUninstallValidator();
    private static final Logger LOGGER = Logger.getLogger(OperationValidator.class.getName());
    private static Map<Module, Set<Module>> module2depending = new HashMap<Module, Set<Module>>();
    private static Map<Module, Set<Module>> module2required = new HashMap<Module, Set<Module>>();

    private OperationValidator() {
    }

    public static boolean isValidOperation(OperationContainerImpl.OperationType type, UpdateUnit updateUnit, UpdateElement updateElement) {
        if (updateUnit.isPending()) {
            return false;
        }
        boolean isValid = false;
        switch (type) {
            case INSTALL: {
                isValid = FOR_INSTALL.isValidOperationImpl(updateUnit, updateElement);
                break;
            }
            case INTERNAL_UPDATE: {
                isValid = FOR_INTERNAL_UPDATE.isValidOperationImpl(updateUnit, updateElement);
                break;
            }
            case DIRECT_UNINSTALL: 
            case UNINSTALL: {
                isValid = FOR_UNINSTALL.isValidOperationImpl(updateUnit, updateElement);
                break;
            }
            case UPDATE: {
                isValid = FOR_UPDATE.isValidOperationImpl(updateUnit, updateElement);
                break;
            }
            case ENABLE: {
                isValid = FOR_ENABLE.isValidOperationImpl(updateUnit, updateElement);
                break;
            }
            case DIRECT_DISABLE: 
            case DISABLE: {
                isValid = FOR_DISABLE.isValidOperationImpl(updateUnit, updateElement);
                break;
            }
            case CUSTOM_INSTALL: {
                isValid = FOR_CUSTOM_INSTALL.isValidOperationImpl(updateUnit, updateElement);
                break;
            }
            case CUSTOM_UNINSTALL: {
                isValid = FOR_CUSTOM_UNINSTALL.isValidOperationImpl(updateUnit, updateElement);
                break;
            }
            default: {
                assert (false);
                break;
            }
        }
        return isValid;
    }

    public static List<UpdateElement> getRequiredElements(OperationContainerImpl.OperationType type, UpdateElement updateElement, List<ModuleInfo> moduleInfos, Collection<String> brokenDependencies, Collection<UpdateElement> recommendedElements) {
        List<UpdateElement> retval = Collections.emptyList();
        switch (type) {
            case INSTALL: {
                retval = FOR_INSTALL.getRequiredElementsImpl(updateElement, moduleInfos, brokenDependencies, recommendedElements);
                break;
            }
            case DIRECT_UNINSTALL: 
            case UNINSTALL: {
                retval = FOR_UNINSTALL.getRequiredElementsImpl(updateElement, moduleInfos, brokenDependencies, recommendedElements);
                break;
            }
            case UPDATE: {
                retval = FOR_UPDATE.getRequiredElementsImpl(updateElement, moduleInfos, brokenDependencies, recommendedElements);
                break;
            }
            case ENABLE: {
                retval = FOR_ENABLE.getRequiredElementsImpl(updateElement, moduleInfos, brokenDependencies, recommendedElements);
                break;
            }
            case DIRECT_DISABLE: 
            case DISABLE: {
                retval = FOR_DISABLE.getRequiredElementsImpl(updateElement, moduleInfos, brokenDependencies, recommendedElements);
                break;
            }
            case CUSTOM_INSTALL: {
                retval = FOR_CUSTOM_INSTALL.getRequiredElementsImpl(updateElement, moduleInfos, brokenDependencies, recommendedElements);
                break;
            }
            case CUSTOM_UNINSTALL: {
                retval = FOR_CUSTOM_UNINSTALL.getRequiredElementsImpl(updateElement, moduleInfos, brokenDependencies, recommendedElements);
                break;
            }
            case INTERNAL_UPDATE: {
                retval = FOR_INTERNAL_UPDATE.getRequiredElementsImpl(updateElement, moduleInfos, brokenDependencies, recommendedElements);
                break;
            }
            default: {
                assert (false);
                break;
            }
        }
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "== do getRequiredElements for " + (Object)((Object)type) + " of " + updateElement + " ==");
            for (UpdateElement el : retval) {
                LOGGER.log(Level.FINE, "--> " + el);
            }
            LOGGER.log(Level.FINE, "== done. ==");
        }
        return retval;
    }

    public static Set<String> getBrokenDependencies(OperationContainerImpl.OperationType type, UpdateElement updateElement, List<ModuleInfo> moduleInfos) {
        HashSet<String> broken = new HashSet();
        switch (type) {
            case ENABLE: {
                broken = Utilities.getBrokenDependenciesInInstalledModules(updateElement);
                break;
            }
            case INSTALL: 
            case INTERNAL_UPDATE: 
            case UPDATE: {
                HashSet<UpdateElement> recommeded = new HashSet<UpdateElement>();
                OperationValidator.getRequiredElements(type, updateElement, moduleInfos, broken, recommeded);
                if (recommeded.isEmpty() || broken.isEmpty()) break;
                broken = new HashSet();
                OperationValidator.getRequiredElements(type, updateElement, moduleInfos, broken, recommeded);
                break;
            }
            case DIRECT_UNINSTALL: 
            case UNINSTALL: 
            case DIRECT_DISABLE: 
            case DISABLE: 
            case CUSTOM_INSTALL: 
            case CUSTOM_UNINSTALL: {
                broken = Utilities.getBrokenDependencies(updateElement, moduleInfos);
                break;
            }
            default: {
                assert (false);
                break;
            }
        }
        return broken;
    }

    abstract boolean isValidOperationImpl(UpdateUnit var1, UpdateElement var2);

    abstract List<UpdateElement> getRequiredElementsImpl(UpdateElement var1, List<ModuleInfo> var2, Collection<String> var3, Collection<UpdateElement> var4);

    public static void clearMaps() {
        module2depending = new HashMap<Module, Set<Module>>();
        module2required = new HashMap<Module, Set<Module>>();
    }

    private static boolean containsElement(UpdateElement el, UpdateUnit unit) {
        return unit.getAvailableUpdates().contains(el);
    }

    private static Set<Module> findRequiredModulesForDeactivate(Set<Module> requestedToDeactivate, ModuleManager mm) {
        HashSet<Module> extendReqToDeactivate = new HashSet<Module>(requestedToDeactivate);
        boolean inscreasing = true;
        while (inscreasing) {
            inscreasing = false;
            HashSet<Module> tmp = new HashSet<Module>(extendReqToDeactivate);
            for (Module dep : tmp) {
                Set<Module> deps = Utilities.findDependingModules(dep, mm, module2depending);
                inscreasing |= extendReqToDeactivate.addAll(deps);
            }
        }
        HashSet<Module> moreToDeactivate = new HashSet<Module>(extendReqToDeactivate);
        inscreasing = true;
        while (inscreasing) {
            inscreasing = false;
            HashSet<Module> tmp = new HashSet<Module>(moreToDeactivate);
            for (Module req : tmp) {
                if ((Utilities.isKitModule((ModuleInfo)req) || Utilities.isEssentialModule((ModuleInfo)req)) && !extendReqToDeactivate.contains((Object)req)) continue;
                Set<Module> reqs = Utilities.findRequiredModules(req, mm, module2required);
                inscreasing |= moreToDeactivate.addAll(reqs);
            }
        }
        return OperationValidator.filterCandidatesToDeactivate(extendReqToDeactivate, moreToDeactivate, mm);
    }

    private static Set<Module> filterCandidatesToDeactivate(Collection<Module> requested, Collection<Module> candidates, ModuleManager mm) {
        HashSet<Module> result = new HashSet<Module>();
        HashSet<Module> compactSet = new HashSet<Module>(candidates);
        HashSet<Module> installedEagers = new HashSet<Module>();
        for (UpdateElement eagerEl : UpdateManagerImpl.getInstance().getInstalledEagers()) {
            UpdateElementImpl impl = Trampoline.API.impl(eagerEl);
            if (impl instanceof ModuleUpdateElementImpl) {
                ModuleInfo mi = ((ModuleUpdateElementImpl)impl).getModuleInfo();
                installedEagers.add(Utilities.toModule(mi));
                continue;
            }
            if (impl instanceof FeatureUpdateElementImpl) {
                List<ModuleInfo> infos = ((FeatureUpdateElementImpl)impl).getModuleInfos();
                for (ModuleInfo mi : infos) {
                    installedEagers.add(Utilities.toModule(mi));
                }
                continue;
            }
            assert (false);
        }
        compactSet.addAll(installedEagers);
        HashSet<Module> mustRemain = new HashSet<Module>();
        HashSet<Module> affectedEagers = new HashSet<Module>();
        for (Module depM : candidates) {
            if ((Utilities.isKitModule((ModuleInfo)depM) || Utilities.isEssentialModule((ModuleInfo)depM)) && !requested.contains((Object)depM)) {
                if (LOGGER.isLoggable(Level.FINE)) {
                    LOGGER.log(Level.FINE, "The module " + depM.getCodeNameBase() + " is KIT_MODULE and won't be deactivated now not even " + Utilities.findRequiredModules(depM, mm, module2required));
                }
                mustRemain.add(depM);
                continue;
            }
            if (mustRemain.contains((Object)depM)) {
                LOGGER.log(Level.FINE, "The module " + depM.getCodeNameBase() + " was investigated already and won't be deactivated now.");
                continue;
            }
            Set<Module> depends = Utilities.findDependingModules(depM, mm, module2depending);
            if (!compactSet.containsAll(depends)) {
                mustRemain.add(depM);
                Set<Module> otherRequiredModules = Utilities.findRequiredModules(depM, mm, module2required);
                mustRemain.addAll(otherRequiredModules);
                LOGGER.log(Level.FINE, "The module " + depM.getCodeNameBase() + " is shared and cannot be deactivated now.");
                if (!LOGGER.isLoggable(Level.FINER)) continue;
                HashSet<Module> outsideModules = new HashSet<Module>(depends);
                outsideModules.removeAll(compactSet);
                LOGGER.log(Level.FINER, "On " + depM.getCodeNameBase() + " depending modules outside of set now deactivating modules: " + outsideModules);
                LOGGER.log(Level.FINER, "With " + depM.getCodeNameBase() + " must remain also these required modules: " + otherRequiredModules);
                continue;
            }
            result.add(depM);
            HashSet<Module> reducedDepends = new HashSet<Module>(depends);
            reducedDepends.retainAll(installedEagers);
            if (reducedDepends.isEmpty()) continue;
            affectedEagers.addAll(reducedDepends);
        }
        result.removeAll(installedEagers);
        LOGGER.log(Level.FINE, "Possible affected eagers are " + affectedEagers);
        result.removeAll(OperationValidator.findDeepRequired(mustRemain, mm));
        HashSet<Module> notAffectedEagers = new HashSet<Module>();
        for (Module eager : affectedEagers) {
            Set<Module> requiredByEager = Utilities.findRequiredModules(eager, mm, module2depending);
            if (requiredByEager.removeAll(result)) continue;
            notAffectedEagers.add(eager);
        }
        affectedEagers.removeAll(notAffectedEagers);
        result.addAll(affectedEagers);
        LOGGER.log(Level.FINE, "Real affected eagers are " + affectedEagers);
        return result;
    }

    private static Set<Module> findDeepRequired(Set<Module> orig, ModuleManager mm) {
        HashSet<Module> more = new HashSet<Module>(orig);
        boolean inscreasing = true;
        while (inscreasing) {
            HashSet<Module> tmp = new HashSet<Module>(more);
            inscreasing = false;
            for (Module req : tmp) {
                Set<Module> reqs = Utilities.findRequiredModules(req, mm, module2required);
                inscreasing |= more.addAll(reqs);
            }
        }
        return more;
    }

    private static class CustomUninstallValidator
    extends OperationValidator {
        private CustomUninstallValidator() {
            super();
        }

        @Override
        boolean isValidOperationImpl(UpdateUnit unit, UpdateElement uElement) {
            boolean res = false;
            UpdateElementImpl impl = Trampoline.API.impl(uElement);
            assert (impl != null);
            if (impl != null && impl instanceof NativeComponentUpdateElementImpl) {
                NativeComponentUpdateElementImpl ni = (NativeComponentUpdateElementImpl)impl;
                res = ni.getNativeItem().getUpdateItemDeploymentImpl().getCustomUninstaller() != null;
            }
            return res;
        }

        @Override
        List<UpdateElement> getRequiredElementsImpl(UpdateElement uElement, List<ModuleInfo> moduleInfos, Collection<String> brokenDependencies, Collection<UpdateElement> recommendedElements) {
            LOGGER.log(Level.INFO, "CustomUninstallValidator doesn't care about required elements.");
            return Collections.emptyList();
        }
    }

    private static class CustomInstallValidator
    extends OperationValidator {
        private CustomInstallValidator() {
            super();
        }

        @Override
        boolean isValidOperationImpl(UpdateUnit unit, UpdateElement uElement) {
            NativeComponentUpdateElementImpl ni;
            boolean res = false;
            UpdateElementImpl impl = Trampoline.API.impl(uElement);
            assert (impl != null);
            if (impl != null && impl instanceof NativeComponentUpdateElementImpl && (ni = (NativeComponentUpdateElementImpl)impl).getInstallInfo().getCustomInstaller() != null) {
                res = OperationValidator.containsElement(uElement, unit);
            }
            return res;
        }

        @Override
        List<UpdateElement> getRequiredElementsImpl(UpdateElement uElement, List<ModuleInfo> moduleInfos, Collection<String> brokenDependencies, Collection<UpdateElement> recommendedElements) {
            LOGGER.log(Level.INFO, "CustomInstallValidator doesn't care about required elements.");
            return Collections.emptyList();
        }
    }

    private static class DisableValidator
    extends OperationValidator {
        private DisableValidator() {
            super();
        }

        @Override
        boolean isValidOperationImpl(UpdateUnit unit, UpdateElement uElement) {
            return unit.getInstalled() != null && this.isValidOperationImpl(Trampoline.API.impl(uElement));
        }

        private boolean isValidOperationImpl(UpdateElementImpl impl) {
            boolean res = false;
            switch (impl.getType()) {
                case KIT_MODULE: 
                case MODULE: {
                    Module module = Utilities.toModule(((ModuleUpdateElementImpl)impl).getModuleInfo());
                    res = Utilities.canDisable(module);
                    break;
                }
                case STANDALONE_MODULE: 
                case FEATURE: {
                    for (ModuleInfo info : ((FeatureUpdateElementImpl)impl).getModuleInfos()) {
                        Module m = Utilities.toModule(info);
                        res |= Utilities.canDisable(m);
                    }
                    break;
                }
                case CUSTOM_HANDLED_COMPONENT: {
                    res = false;
                    break;
                }
                default: {
                    assert (false);
                    break;
                }
            }
            return res;
        }

        @Override
        List<UpdateElement> getRequiredElementsImpl(UpdateElement uElement, List<ModuleInfo> moduleInfos, Collection<String> brokenDependencies, Collection<UpdateElement> recommendedElements) {
            ModuleManager mm = null;
            LinkedHashSet<Module> modules = new LinkedHashSet<Module>();
            for (ModuleInfo moduleInfo : moduleInfos) {
                Module m = Utilities.toModule(moduleInfo);
                if (Utilities.canDisable(m)) {
                    modules.add(m);
                }
                if (mm != null) continue;
                mm = m.getManager();
            }
            if (mm == null) {
                LOGGER.log(Level.WARNING, "No modules can be disabled when disabling UpdateElement " + uElement);
                return Collections.emptyList();
            }
            Set requestedToDisable = OperationValidator.findRequiredModulesForDeactivate(modules, mm);
            List toDisable = mm.simulateDisable(modules);
            boolean wasAdded = requestedToDisable.addAll(toDisable);
            if (LOGGER.isLoggable(Level.FINE) && wasAdded) {
                toDisable.removeAll(OperationValidator.filterCandidatesToDeactivate(modules, requestedToDisable, mm));
                LOGGER.log(Level.FINE, "requestedToDisable was enlarged by " + toDisable);
            }
            HashSet<UpdateElement> retval = new HashSet<UpdateElement>();
            for (Module module : requestedToDisable) {
                UpdateUnit unit;
                if (modules.contains((Object)module) || !Utilities.canDisable(module) || (unit = Utilities.toUpdateUnit(module)) == null) continue;
                retval.add(unit.getInstalled());
            }
            return new ArrayList<UpdateElement>(retval);
        }
    }

    private static class EnableValidator
    extends OperationValidator {
        private EnableValidator() {
            super();
        }

        @Override
        boolean isValidOperationImpl(UpdateUnit unit, UpdateElement uElement) {
            return unit.getInstalled() != null && this.isValidOperationImpl(Trampoline.API.impl(uElement));
        }

        private boolean isValidOperationImpl(UpdateElementImpl impl) {
            boolean res = false;
            switch (impl.getType()) {
                case KIT_MODULE: 
                case MODULE: {
                    Module module = Utilities.toModule(((ModuleUpdateElementImpl)impl).getModuleInfo());
                    res = Utilities.canEnable(module);
                    break;
                }
                case STANDALONE_MODULE: 
                case FEATURE: {
                    for (ModuleInfo info : ((FeatureUpdateElementImpl)impl).getModuleInfos()) {
                        Module m = Utilities.toModule(info);
                        res |= Utilities.canEnable(m);
                    }
                    break;
                }
                case CUSTOM_HANDLED_COMPONENT: {
                    res = false;
                    break;
                }
                default: {
                    assert (false);
                    break;
                }
            }
            return res;
        }

        private List<Module> getModulesToEnable(ModuleManager mm, Set<Module> modules) {
            List toEnable = new ArrayList<Module>();
            boolean stateChanged = true;
            while (stateChanged) {
                stateChanged = false;
                try {
                    toEnable = mm.simulateEnable(modules);
                    continue;
                }
                catch (IllegalArgumentException e) {
                    LOGGER.log(Level.INFO, "Cannot enable all modules " + modules, e);
                    LinkedHashSet<Module> tempModules = new LinkedHashSet<Module>(modules);
                    for (Module module : tempModules) {
                        if (Utilities.canEnable(module)) continue;
                        modules.remove((Object)module);
                        stateChanged = true;
                    }
                    assert (stateChanged);
                    continue;
                }
            }
            return toEnable;
        }

        @Override
        List<UpdateElement> getRequiredElementsImpl(UpdateElement uElement, List<ModuleInfo> moduleInfos, Collection<String> brokenDependencies, Collection<UpdateElement> recommendedElements) {
            UpdateElementImpl uElementImpl = Trampoline.API.impl(uElement);
            ArrayList<ModuleInfo> expandedModuleInfos = new ArrayList<ModuleInfo>(moduleInfos);
            expandedModuleInfos.addAll(uElementImpl.getModuleInfos(true));
            ModuleManager mm = null;
            LinkedHashSet<Module> modules = new LinkedHashSet<Module>();
            for (ModuleInfo moduleInfo : expandedModuleInfos) {
                Module m = Utilities.toModule(moduleInfo);
                if (Utilities.canEnable(m)) {
                    modules.add(m);
                }
                if (mm != null) continue;
                mm = m.getManager();
            }
            ArrayList<UpdateElement> retval = new ArrayList<UpdateElement>();
            if (mm != null) {
                List<Module> toEnable = this.getModulesToEnable(mm, modules);
                for (Module module : toEnable) {
                    if (modules.contains((Object)module) || !Utilities.canEnable(module) || Utilities.toUpdateUnit(module).getInstalled() == null) continue;
                    retval.add(Utilities.toUpdateUnit(module).getInstalled());
                }
            }
            return retval;
        }
    }

    private static class UpdateValidator
    extends OperationValidator {
        private UpdateValidator() {
            super();
        }

        @Override
        boolean isValidOperationImpl(UpdateUnit unit, UpdateElement uElement) {
            return unit.getInstalled() != null && OperationValidator.containsElement(uElement, unit);
        }

        @Override
        List<UpdateElement> getRequiredElementsImpl(UpdateElement uElement, List<ModuleInfo> moduleInfos, Collection<String> brokenDependencies, Collection<UpdateElement> recommendedElements) {
            HashSet<Dependency> brokenDeps = new HashSet<Dependency>();
            LinkedList<UpdateElement> res = new LinkedList<UpdateElement>(Utilities.findRequiredUpdateElements(uElement, moduleInfos, brokenDeps, true, recommendedElements));
            if (brokenDependencies != null) {
                for (Dependency dep : brokenDeps) {
                    brokenDependencies.add(dep.toString());
                }
            }
            return res;
        }
    }

    private static class UninstallValidator
    extends OperationValidator {
        private UninstallValidator() {
            super();
        }

        @Override
        boolean isValidOperationImpl(UpdateUnit unit, UpdateElement uElement) {
            return unit.getInstalled() != null && this.isValidOperationImpl(Trampoline.API.impl(uElement));
        }

        private boolean isValidOperationImpl(UpdateElementImpl impl) {
            boolean res = false;
            switch (impl.getType()) {
                case KIT_MODULE: 
                case MODULE: {
                    Module m = Utilities.toModule(((ModuleUpdateElementImpl)impl).getModuleInfo());
                    res = ModuleDeleterImpl.getInstance().canDelete((ModuleInfo)m);
                    break;
                }
                case STANDALONE_MODULE: 
                case FEATURE: {
                    for (ModuleInfo info : ((FeatureUpdateElementImpl)impl).getModuleInfos()) {
                        Module module = Utilities.toModule(info);
                        res |= ModuleDeleterImpl.getInstance().canDelete((ModuleInfo)module);
                    }
                    break;
                }
                case CUSTOM_HANDLED_COMPONENT: {
                    LOGGER.log(Level.INFO, "CUSTOM_HANDLED_COMPONENT doesn't support custom uninstaller yet.");
                    res = false;
                    break;
                }
                default: {
                    assert (false);
                    break;
                }
            }
            return res;
        }

        @Override
        List<UpdateElement> getRequiredElementsImpl(UpdateElement uElement, List<ModuleInfo> moduleInfos, Collection<String> brokenDependencies, Collection<UpdateElement> recommendedElements) {
            ModuleManager mm = null;
            LinkedHashSet<Module> modules = new LinkedHashSet<Module>();
            for (ModuleInfo moduleInfo : moduleInfos) {
                Module m = Utilities.toModule(moduleInfo);
                if (m == null) continue;
                if (!Utilities.isEssentialModule((ModuleInfo)m)) {
                    modules.add(m);
                }
                if (mm != null) continue;
                mm = m.getManager();
            }
            HashSet<UpdateElement> retval = new HashSet<UpdateElement>();
            if (mm != null) {
                Set toUninstall = OperationValidator.findRequiredModulesForDeactivate(modules, mm);
                toUninstall.removeAll(modules);
                for (Module module : toUninstall) {
                    if (Utilities.isEssentialModule((ModuleInfo)module)) {
                        LOGGER.log(Level.WARNING, "Essential module cannot be planned for uninstall but " + (Object)module);
                        continue;
                    }
                    if (!ModuleDeleterImpl.getInstance().canDelete((ModuleInfo)module)) {
                        LOGGER.log(Level.WARNING, "The module " + (Object)module + " cannot be planned for uninstall because is read-only.");
                        continue;
                    }
                    UpdateUnit unit = Utilities.toUpdateUnit(module);
                    if (unit == null) continue;
                    retval.add(unit.getInstalled());
                }
            }
            return new ArrayList<UpdateElement>(retval);
        }
    }

    private static class InstallValidator
    extends OperationValidator {
        private InstallValidator() {
            super();
        }

        @Override
        boolean isValidOperationImpl(UpdateUnit unit, UpdateElement uElement) {
            return unit.getInstalled() == null && OperationValidator.containsElement(uElement, unit);
        }

        @Override
        List<UpdateElement> getRequiredElementsImpl(UpdateElement uElement, List<ModuleInfo> moduleInfos, Collection<String> brokenDependencies, Collection<UpdateElement> recommendedElements) {
            HashSet<Dependency> brokenDeps = new HashSet<Dependency>();
            LinkedList<UpdateElement> res = new LinkedList<UpdateElement>(Utilities.findRequiredUpdateElements(uElement, moduleInfos, brokenDeps, false, recommendedElements));
            if (brokenDependencies != null) {
                for (Dependency dep : brokenDeps) {
                    brokenDependencies.add(dep.toString());
                }
            }
            return res;
        }
    }

    private static class InternalUpdateValidator
    extends UpdateValidator {
        private InternalUpdateValidator() {
        }

        @Override
        boolean isValidOperationImpl(UpdateUnit unit, UpdateElement uElement) {
            return uElement.equals(unit.getInstalled()) || OperationValidator.containsElement(uElement, unit);
        }
    }

}

