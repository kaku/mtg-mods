/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.Module
 *  org.netbeans.updater.UpdateTracking
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.SpecificationVersion
 */
package org.netbeans.modules.autoupdate.services;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.Module;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateManager;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.modules.autoupdate.services.Trampoline;
import org.netbeans.modules.autoupdate.services.UpdateElementImpl;
import org.netbeans.modules.autoupdate.services.UpdateLicenseImpl;
import org.netbeans.modules.autoupdate.services.UpdateUnitFactory;
import org.netbeans.modules.autoupdate.services.UpdateUnitImpl;
import org.netbeans.modules.autoupdate.services.Utilities;
import org.netbeans.modules.autoupdate.updateprovider.InstallInfo;
import org.netbeans.modules.autoupdate.updateprovider.InstalledModuleItem;
import org.netbeans.modules.autoupdate.updateprovider.ModuleItem;
import org.netbeans.modules.autoupdate.updateprovider.UpdateItemImpl;
import org.netbeans.updater.UpdateTracking;
import org.openide.filesystems.FileUtil;
import org.openide.modules.ModuleInfo;
import org.openide.modules.SpecificationVersion;

public class ModuleUpdateElementImpl
extends UpdateElementImpl {
    private String codeName;
    private String displayName;
    private SpecificationVersion specVersion;
    private String description;
    private String source;
    private String author;
    private String homepage;
    private int downloadSize;
    private String category;
    private String rawCategory;
    private InstallInfo installInfo;
    private static final Logger log = Logger.getLogger(ModuleUpdateElementImpl.class.getName());
    private ModuleInfo moduleInfo;
    private ModuleItem item;
    private String providerName;
    private String date;
    private boolean isEager;
    private boolean isAutoload;
    private boolean isPreferredUpdate;
    private String installationCluster;

    public ModuleUpdateElementImpl(ModuleItem item, String providerName) {
        super(item, providerName);
        this.moduleInfo = item.getModuleInfo();
        this.item = item;
        this.providerName = providerName;
        this.codeName = item.getCodeName();
        this.specVersion = item.getSpecificationVersion() == null ? null : new SpecificationVersion(item.getSpecificationVersion());
        this.installInfo = new InstallInfo(item);
        this.author = item.getAuthor();
        this.downloadSize = item.getDownloadSize();
        this.homepage = item.getHomepage();
        this.date = item.getDate();
        this.isEager = item.isEager();
        this.isAutoload = item.isAutoload();
        this.isPreferredUpdate = item.isPreferredUpdate();
    }

    @Override
    public String getCodeName() {
        return this.codeName;
    }

    @Override
    public String getDisplayName() {
        if (this.displayName == null) {
            String dn = this.moduleInfo.getDisplayName();
            assert (dn != null);
            if (dn == null) {
                log.log(Level.WARNING, "Module " + this.codeName + " doesn't provider display name. Value of \"OpenIDE-Module-Name\" cannot be null.");
            }
            this.displayName = dn == null ? this.codeName : dn;
        }
        return this.displayName;
    }

    @Override
    public SpecificationVersion getSpecificationVersion() {
        return this.specVersion;
    }

    @Override
    public String getDescription() {
        if (this.description == null) {
            this.description = (String)this.moduleInfo.getLocalizedAttribute("OpenIDE-Module-Long-Description");
        }
        return this.description;
    }

    @Override
    public String getNotification() {
        String notification = this.item.getModuleNotification();
        if (notification != null) {
            notification = notification.trim();
        }
        return notification;
    }

    @Override
    public String getAuthor() {
        return this.author;
    }

    @Override
    public String getHomepage() {
        return this.homepage;
    }

    @Override
    public int getDownloadSize() {
        return this.downloadSize;
    }

    @Override
    public String getSource() {
        if (this.source == null) {
            String string = this.source = this.item instanceof InstalledModuleItem ? ((InstalledModuleItem)this.item).getSource() : this.providerName;
            if (this.source == null) {
                this.source = Utilities.getProductVersion();
            }
        }
        return this.source;
    }

    @Override
    public String getDate() {
        return this.date;
    }

    public String getRawCategory() {
        if (this.rawCategory == null) {
            this.rawCategory = this.item.getCategory();
            if (this.rawCategory == null) {
                this.rawCategory = (String)this.moduleInfo.getLocalizedAttribute("OpenIDE-Module-Display-Category");
            }
            if (this.rawCategory == null) {
                this.rawCategory = "";
            }
        }
        return this.rawCategory;
    }

    @Override
    public String getCategory() {
        if (this.category == null) {
            this.category = this.getRawCategory();
            if (this.isAutoload() || this.isFixed()) {
                this.category = UpdateUnitFactory.LIBRARIES_CATEGORY;
            } else if (this.isEager()) {
                this.category = UpdateUnitFactory.BRIDGES_CATEGORY;
            } else if (this.category == null || this.category.length() == 0) {
                this.category = UpdateUnitFactory.UNSORTED_CATEGORY;
            }
        }
        return this.category;
    }

    @Override
    public String getLicenseId() {
        if (this.item instanceof InstalledModuleItem) {
            UpdateUnitImpl impl = Trampoline.API.impl(this.getUpdateUnit());
            UpdateElement elWithSameVersion = impl.findUpdateSameAsInstalled();
            if (elWithSameVersion != null) {
                return elWithSameVersion.getLicenseId();
            }
        } else {
            assert (this.item.getUpdateLicenseImpl() != null);
            if (this.item.getUpdateLicenseImpl() != null) {
                return this.item.getUpdateLicenseImpl().getName();
            }
        }
        return null;
    }

    @Override
    public String getLicence() {
        return this.item.getAgreement();
    }

    @Override
    public InstallInfo getInstallInfo() {
        return this.installInfo;
    }

    @Override
    public List<ModuleInfo> getModuleInfos() {
        return Collections.singletonList(this.getModuleInfo());
    }

    public ModuleInfo getModuleInfo() {
        assert (this.moduleInfo != null);
        Module info = Utilities.toModule(this.moduleInfo);
        this.moduleInfo = info != null ? info : this.item.getModuleInfo();
        return this.moduleInfo;
    }

    @Override
    public UpdateManager.TYPE getType() {
        return UpdateManager.TYPE.MODULE;
    }

    @Override
    public boolean isEnabled() {
        return this.getModuleInfo().isEnabled();
    }

    @Override
    public boolean isAutoload() {
        return this.isAutoload;
    }

    @Override
    public boolean isEager() {
        return this.isEager;
    }

    @Override
    public boolean isPreferredUpdate() {
        return this.isPreferredUpdate;
    }

    @Override
    public boolean isFixed() {
        return Utilities.toModule(this.getCodeName(), null) == null ? false : Utilities.toModule(this.getCodeName(), null).isFixed();
    }

    public String getInstallationCluster() {
        if (!(this.item instanceof InstalledModuleItem)) {
            return null;
        }
        if (this.installationCluster == null) {
            this.installationCluster = this.findInstallationCluster();
        }
        return this.installationCluster;
    }

    private String findInstallationCluster() {
        String res;
        Module m = Utilities.toModule(this.moduleInfo);
        if (m == null) {
            return null;
        }
        File jarFile = m.getJarFile();
        res = null;
        if (jarFile != null) {
            for (File cluster : UpdateTracking.clusters((boolean)true)) {
                if (!ModuleUpdateElementImpl.isParentOf(cluster = FileUtil.normalizeFile((File)cluster), jarFile)) continue;
                res = cluster.getName();
                break;
            }
        } else if (UpdateTracking.getPlatformDir() != null) {
            return UpdateTracking.getPlatformDir().getName();
        }
        return res;
    }

    private static boolean isParentOf(File parent, File child) {
        File tmp;
        for (tmp = child.getParentFile(); tmp != null && !parent.equals(tmp); tmp = tmp.getParentFile()) {
        }
        return tmp != null;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        ModuleUpdateElementImpl other = (ModuleUpdateElementImpl)obj;
        if (!(this.specVersion == other.specVersion || this.specVersion != null && this.specVersion.equals((Object)other.specVersion))) {
            return false;
        }
        if (!(this.codeName == other.codeName || this.codeName != null && this.codeName.equals(other.codeName))) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + (this.codeName != null ? this.codeName.hashCode() : 0);
        hash = 61 * hash + (this.specVersion != null ? this.specVersion.hashCode() : 0);
        return hash;
    }

    public String toString() {
        return "Impl[" + this.getUpdateElement() + "]";
    }
}

