/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.Module
 *  org.openide.modules.Dependency
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.SpecificationVersion
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.autoupdate.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.Module;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateManager;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.api.autoupdate.UpdateUnitProvider;
import org.netbeans.modules.autoupdate.services.DependencyChecker;
import org.netbeans.modules.autoupdate.services.ModuleUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.Trampoline;
import org.netbeans.modules.autoupdate.services.UpdateElementImpl;
import org.netbeans.modules.autoupdate.services.UpdateUnitFactory;
import org.netbeans.modules.autoupdate.services.Utilities;
import org.netbeans.modules.autoupdate.updateprovider.ArtificialFeaturesProvider;
import org.netbeans.modules.autoupdate.updateprovider.FeatureItem;
import org.netbeans.modules.autoupdate.updateprovider.InstallInfo;
import org.netbeans.modules.autoupdate.updateprovider.UpdateItemImpl;
import org.openide.modules.Dependency;
import org.openide.modules.ModuleInfo;
import org.openide.modules.SpecificationVersion;
import org.openide.util.NbBundle;

public class FeatureUpdateElementImpl
extends UpdateElementImpl {
    private String codeName;
    private String displayName;
    private SpecificationVersion specVersion;
    private String description;
    private String homepage;
    private String category;
    private InstallInfo installInfo;
    private static final Logger LOG = Logger.getLogger(FeatureUpdateElementImpl.class.getName());
    private Set<ModuleUpdateElementImpl> moduleElementsImpl;
    private Set<FeatureUpdateElementImpl> featureElementsImpl;
    private UpdateManager.TYPE type;

    public FeatureUpdateElementImpl(FeatureItem item, String providerName, Set<ModuleUpdateElementImpl> moduleElementsImpl, Set<FeatureUpdateElementImpl> featureElementsImpl, UpdateManager.TYPE type) {
        super(item, providerName);
        this.type = type;
        this.moduleElementsImpl = moduleElementsImpl;
        this.featureElementsImpl = featureElementsImpl;
        this.codeName = item.getCodeName();
        String itemSpec = item.getSpecificationVersion();
        if (itemSpec == null) {
            LOG.log(Level.INFO, this.codeName + " has no specificationVersion.");
        } else {
            this.specVersion = new SpecificationVersion(itemSpec);
        }
        this.installInfo = new InstallInfo(item);
        this.displayName = item.getDisplayName();
        this.description = item.getDescription();
        this.category = item.getCategory();
        if (this.category == null) {
            this.category = NbBundle.getMessage(UpdateElementImpl.class, (String)"UpdateElementImpl_Feature_CategoryName");
        }
    }

    @Override
    public String getCodeName() {
        return this.codeName;
    }

    @Override
    public String getDisplayName() {
        return this.displayName;
    }

    @Override
    public SpecificationVersion getSpecificationVersion() {
        if (this.specVersion == null) {
            this.specVersion = new SpecificationVersion(ArtificialFeaturesProvider.createVersion(this.getModuleInfos()));
        }
        return this.specVersion;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public String getNotification() {
        return null;
    }

    @Override
    public String getAuthor() {
        String res = "";
        HashSet<String> authors = new HashSet<String>();
        for (ModuleUpdateElementImpl impl : this.getContainedModuleElements()) {
            if (impl.getAuthor() == null || !authors.add(impl.getAuthor())) continue;
            res = res + (res.length() == 0 ? impl.getAuthor() : new StringBuilder().append(", ").append(impl.getAuthor()).toString());
        }
        return res;
    }

    @Override
    public String getHomepage() {
        return this.homepage;
    }

    @Override
    public int getDownloadSize() {
        int res = 0;
        for (ModuleUpdateElementImpl impl : this.getContainedModuleElements()) {
            if (impl.getUpdateUnit().getAvailableUpdates().isEmpty()) continue;
            res += impl.getUpdateUnit().getAvailableUpdates().get(0).getDownloadSize();
        }
        return res;
    }

    @Override
    public String getSource() {
        String res = "";
        HashSet<String> sources = new HashSet<String>();
        for (ModuleUpdateElementImpl impl : this.getContainedModuleElements()) {
            if (!sources.add(impl.getSource())) continue;
            res = res + (res.length() == 0 ? impl.getSource() : new StringBuilder().append(", ").append(impl.getSource()).toString());
        }
        return res;
    }

    @Override
    public String getCategory() {
        if (this.isAutoload() || this.isFixed()) {
            this.category = UpdateUnitFactory.LIBRARIES_CATEGORY;
        } else if (this.isEager()) {
            this.category = UpdateUnitFactory.BRIDGES_CATEGORY;
        } else if (this.category == null || this.category.length() == 0) {
            this.category = UpdateUnitFactory.UNSORTED_CATEGORY;
        }
        return this.category;
    }

    @Override
    public String getDate() {
        String res = null;
        Date date = null;
        for (ModuleUpdateElementImpl impl : this.getContainedModuleElements()) {
            String sd = impl.getDate();
            if (sd == null) continue;
            try {
                Date d = Utilities.parseDate(sd);
                date = date == null ? d : new Date(Math.max(date.getTime(), d.getTime()));
                continue;
            }
            catch (ParseException pe) {
                assert (false);
                continue;
            }
        }
        if (date != null) {
            res = Utilities.formatDate(date);
        }
        return res;
    }

    @Override
    public String getLicenseId() {
        String res = "";
        HashSet<String> ids = new HashSet<String>();
        for (ModuleUpdateElementImpl impl : this.getContainedModuleElements()) {
            String id;
            if (impl.getUpdateUnit().getAvailableUpdates().isEmpty() || !ids.add(id = impl.getUpdateUnit().getAvailableUpdates().get(0).getLicenseId())) continue;
            res = res + (res.length() == 0 ? id : new StringBuilder().append(",").append(id).toString());
        }
        return res;
    }

    @Override
    public String getLicence() {
        String res = "";
        HashSet<String> licenses = new HashSet<String>();
        for (ModuleUpdateElementImpl impl : this.getContainedModuleElements()) {
            String lic;
            if (impl.getUpdateUnit().getAvailableUpdates().isEmpty() || !licenses.add(lic = impl.getUpdateUnit().getAvailableUpdates().get(0).getLicence())) continue;
            res = res + (res.length() == 0 ? lic : new StringBuilder().append("<br>").append(lic).toString());
        }
        return res;
    }

    @Override
    public InstallInfo getInstallInfo() {
        return this.installInfo;
    }

    @Override
    public List<ModuleInfo> getModuleInfos() {
        return this.getModuleInfos(false);
    }

    @Override
    public List<ModuleInfo> getModuleInfos(boolean recursive) {
        ArrayList<ModuleInfo> infos = new ArrayList<ModuleInfo>();
        for (ModuleUpdateElementImpl impl : this.getContainedModuleElements()) {
            if (infos.contains((Object)impl.getModuleInfo())) continue;
            infos.add(impl.getModuleInfo());
        }
        if (recursive) {
            for (FeatureUpdateElementImpl featureImpl : this.getDependingFeatures()) {
                for (ModuleUpdateElementImpl modImpl : featureImpl.getContainedModuleElements()) {
                    if (infos.contains((Object)modImpl.getModuleInfo())) continue;
                    infos.add(modImpl.getModuleInfo());
                }
            }
        }
        return infos;
    }

    public Set<ModuleUpdateElementImpl> getContainedModuleElements() {
        assert (this.moduleElementsImpl != null);
        return this.moduleElementsImpl;
    }

    public Set<FeatureUpdateElementImpl> getDependingFeatures() {
        assert (this.featureElementsImpl != null);
        return this.featureElementsImpl;
    }

    @Override
    public UpdateManager.TYPE getType() {
        return this.type;
    }

    @Override
    public boolean isEnabled() {
        boolean res = true;
        for (ModuleUpdateElementImpl impl : this.getContainedModuleElements()) {
            res &= impl.isEnabled();
        }
        for (FeatureUpdateElementImpl featureImpl : this.getDependingFeatures()) {
            res &= featureImpl.isEnabled();
        }
        return res;
    }

    @Override
    public boolean isAutoload() {
        boolean res = true;
        for (ModuleUpdateElementImpl impl : this.getContainedModuleElements()) {
            res &= impl.isAutoload();
        }
        for (FeatureUpdateElementImpl featureImpl : this.getDependingFeatures()) {
            res &= featureImpl.isAutoload();
        }
        return res;
    }

    @Override
    public boolean isEager() {
        boolean res = true;
        for (ModuleUpdateElementImpl impl : this.getContainedModuleElements()) {
            res &= impl.isEager();
        }
        for (FeatureUpdateElementImpl featureImpl : this.getDependingFeatures()) {
            res &= featureImpl.isEager();
        }
        return res;
    }

    @Override
    public boolean isFixed() {
        boolean res = true;
        for (ModuleUpdateElementImpl impl : this.getContainedModuleElements()) {
            res &= impl.isFixed();
        }
        for (FeatureUpdateElementImpl featureImpl : this.getDependingFeatures()) {
            res &= featureImpl.isFixed();
        }
        return res;
    }

    @Override
    public boolean isPreferredUpdate() {
        boolean res = true;
        for (ModuleUpdateElementImpl impl : this.getContainedModuleElements()) {
            res &= impl.isPreferredUpdate();
        }
        for (FeatureUpdateElementImpl featureImpl : this.getDependingFeatures()) {
            res &= featureImpl.isPreferredUpdate();
        }
        return res;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        FeatureUpdateElementImpl other = (FeatureUpdateElementImpl)obj;
        if (!(this.specVersion == other.specVersion || this.specVersion != null && this.specVersion.equals((Object)other.specVersion))) {
            return false;
        }
        if (!(this.codeName == other.codeName || this.codeName != null && this.codeName.equals(other.codeName))) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + (this.codeName != null ? this.codeName.hashCode() : 0);
        hash = 61 * hash + (this.specVersion != null ? this.specVersion.hashCode() : 0);
        return hash;
    }

    public String toString() {
        return "FeatureUpdateElementImpl[" + this.codeName + "/" + (Object)this.specVersion + "]";
    }

    public static class Agent
    extends FeatureUpdateElementImpl {
        private Set<ModuleUpdateElementImpl> moduleElementsImpl;
        private Set<FeatureUpdateElementImpl> featureElementsImpl;
        private FeatureItem featureItem;

        public Agent(FeatureItem item, String providerName, UpdateManager.TYPE type) {
            super(item, providerName, null, null, type);
            this.featureItem = item;
        }

        @Override
        public Set<ModuleUpdateElementImpl> getContainedModuleElements() {
            this.initializeAgent();
            assert (this.moduleElementsImpl != null);
            return this.moduleElementsImpl;
        }

        @Override
        public Set<FeatureUpdateElementImpl> getDependingFeatures() {
            this.initializeAgent();
            assert (this.featureElementsImpl != null);
            return this.featureElementsImpl;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void initializeAgent() {
            Agent agent = this;
            synchronized (agent) {
                if (this.featureElementsImpl == null || this.moduleElementsImpl == null) {
                    HashSet<FeatureUpdateElementImpl> depFeatures = new HashSet<FeatureUpdateElementImpl>();
                    this.moduleElementsImpl = this.processContainedModules(this.featureItem.getModuleCodeNames(), null, depFeatures);
                    this.featureElementsImpl = depFeatures;
                }
            }
        }

        private Set<ModuleUpdateElementImpl> processContainedModules(Set<String> dependenciesToModulesOrFeatures, UpdateUnitProvider provider, Set<FeatureUpdateElementImpl> depFeatures) {
            HashSet<ModuleUpdateElementImpl> res = new HashSet<ModuleUpdateElementImpl>();
            assert (dependenciesToModulesOrFeatures != null);
            dependenciesToModulesOrFeatures = dependenciesToModulesOrFeatures == null ? new HashSet<String>() : new HashSet<String>(dependenciesToModulesOrFeatures);
            HashSet deps = new HashSet();
            for (String depSpec : dependenciesToModulesOrFeatures) {
                deps.addAll(Dependency.create((int)1, (String)depSpec));
            }
            List<UpdateUnit> moduleUnits = provider == null ? UpdateManager.getDefault().getUpdateUnits(UpdateManager.TYPE.MODULE) : provider.getUpdateUnits(UpdateManager.TYPE.MODULE);
            for (UpdateUnit unit : moduleUnits) {
                Iterator it = deps.iterator();
                while (it.hasNext()) {
                    Dependency dep = (Dependency)it.next();
                    assert (1 == dep.getType());
                    String name = dep.getName();
                    if (name.indexOf(47) != -1) {
                        int to = name.indexOf(47);
                        name = name.substring(0, to);
                    }
                    if (!unit.getCodeName().equals(name)) continue;
                    UpdateElement el = Agent.getMatchedUpdateElement(unit, dep);
                    if (el != null) {
                        assert (Trampoline.API.impl(el) instanceof ModuleUpdateElementImpl);
                        ModuleUpdateElementImpl impl = (ModuleUpdateElementImpl)Trampoline.API.impl(el);
                        res.add(impl);
                        dependenciesToModulesOrFeatures.remove(name);
                        it.remove();
                        continue;
                    }
                    LOG.log(Level.INFO, this.getUpdateUnit() + " requires a module " + name + " what is not present.");
                }
            }
            if (!dependenciesToModulesOrFeatures.isEmpty()) {
                List<UpdateUnit> features = provider == null ? UpdateManager.getDefault().getUpdateUnits(UpdateManager.TYPE.FEATURE) : provider.getUpdateUnits(UpdateManager.TYPE.FEATURE);
                for (UpdateUnit feat : features) {
                    Iterator it = deps.iterator();
                    while (it.hasNext()) {
                        Dependency dep = (Dependency)it.next();
                        String name = dep.getName();
                        if (!name.equals(feat.getCodeName())) continue;
                        UpdateElement el = Agent.getMatchedUpdateElement(feat, dep);
                        if (el != null) {
                            assert (Trampoline.API.impl(el) instanceof FeatureUpdateElementImpl);
                            FeatureUpdateElementImpl impl = (FeatureUpdateElementImpl)Trampoline.API.impl(el);
                            if (depFeatures.add(impl)) {
                                // empty if block
                            }
                            dependenciesToModulesOrFeatures.remove(name);
                            it.remove();
                            continue;
                        }
                        LOG.log(Level.INFO, this.getUpdateUnit() + " requires a feature " + name + " what is not present.");
                    }
                }
            }
            for (String depSpec2 : dependenciesToModulesOrFeatures) {
                for (Dependency dep : Dependency.create((int)1, (String)depSpec2)) {
                    Module m;
                    String cnb = dep.getName();
                    if (cnb.indexOf(47) != -1) {
                        int to = cnb.indexOf(47);
                        cnb = cnb.substring(0, to);
                    }
                    if ((m = Utilities.toModule(cnb, null)) == null || m.getProblems().isEmpty()) continue;
                    dependenciesToModulesOrFeatures.remove(depSpec2);
                }
            }
            if (!dependenciesToModulesOrFeatures.isEmpty()) {
                LOG.log(Level.INFO, this.featureItem + " depends on non-existing " + dependenciesToModulesOrFeatures);
            }
            return res;
        }

        private static UpdateElement getMatchedUpdateElement(UpdateUnit unit, Dependency dep) {
            if (Agent.match(unit.getInstalled(), dep)) {
                return unit.getInstalled();
            }
            if (!unit.getAvailableUpdates().isEmpty() && Agent.match(unit.getAvailableUpdates().get(0), dep)) {
                return unit.getAvailableUpdates().get(0);
            }
            return null;
        }

        private static boolean match(UpdateElement el, Dependency dep) {
            if (el == null) {
                return false;
            }
            UpdateElementImpl impl = Trampoline.API.impl(el);
            if (impl instanceof FeatureUpdateElementImpl) {
                if (dep.getVersion() == null) {
                    return true;
                }
                SpecificationVersion v = new SpecificationVersion(dep.getVersion());
                return v.compareTo((Object)impl.getSpecificationVersion()) >= 0;
            }
            return DependencyChecker.checkDependencyModuleAllowEqual(dep, Utilities.takeModuleInfo(el));
        }
    }

}

