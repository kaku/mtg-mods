/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.InvalidException
 *  org.netbeans.Module
 *  org.netbeans.ModuleManager
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.openide.LifecycleManager
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.SpecificationVersion
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$ExceptionAction
 *  org.openide.util.MutexException
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.autoupdate.services;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.InvalidException;
import org.netbeans.Module;
import org.netbeans.ModuleManager;
import org.netbeans.api.autoupdate.InstallSupport;
import org.netbeans.api.autoupdate.OperationContainer;
import org.netbeans.api.autoupdate.OperationException;
import org.netbeans.api.autoupdate.OperationSupport;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateManager;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.modules.autoupdate.services.FeatureUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.InstallSupportImpl;
import org.netbeans.modules.autoupdate.services.ModuleDeleterImpl;
import org.netbeans.modules.autoupdate.services.ModuleUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.NativeComponentUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.Trampoline;
import org.netbeans.modules.autoupdate.services.UpdateElementImpl;
import org.netbeans.modules.autoupdate.services.UpdateItemDeploymentImpl;
import org.netbeans.modules.autoupdate.services.UpdateUnitFactory;
import org.netbeans.modules.autoupdate.services.UpdateUnitImpl;
import org.netbeans.modules.autoupdate.services.Utilities;
import org.netbeans.modules.autoupdate.updateprovider.InstallInfo;
import org.netbeans.modules.autoupdate.updateprovider.NativeComponentItem;
import org.netbeans.spi.autoupdate.CustomInstaller;
import org.netbeans.spi.autoupdate.CustomUninstaller;
import org.openide.LifecycleManager;
import org.openide.modules.ModuleInfo;
import org.openide.modules.SpecificationVersion;
import org.openide.util.Mutex;
import org.openide.util.MutexException;
import org.openide.util.NbBundle;

public abstract class OperationSupportImpl {
    private static final OperationSupportImpl FOR_INSTALL = new ForInstall();
    private static final OperationSupportImpl FOR_ENABLE = new ForEnable();
    private static final OperationSupportImpl FOR_DISABLE = new ForDisable();
    private static final OperationSupportImpl FOR_DIRECT_DISABLE = new ForDirectDisable();
    private static final OperationSupportImpl FOR_UNINSTALL = new ForUninstall();
    private static final OperationSupportImpl FOR_DIRECT_UNINSTALL = new ForDirectUninstall();
    private static final OperationSupportImpl FOR_CUSTOM_INSTALL = new ForCustomInstall();
    private static final OperationSupportImpl FOR_CUSTOM_UNINSTALL = new ForCustomUninstall();
    private static final Logger LOGGER = Logger.getLogger("org.netbeans.modules.autoupdate.services.OperationSupportImpl");

    public static OperationSupportImpl forInstall() {
        return FOR_INSTALL;
    }

    public static OperationSupportImpl forUninstall() {
        return FOR_UNINSTALL;
    }

    public static OperationSupportImpl forDirectUninstall() {
        return FOR_DIRECT_UNINSTALL;
    }

    public static OperationSupportImpl forEnable() {
        return FOR_ENABLE;
    }

    public static OperationSupportImpl forDisable() {
        return FOR_DISABLE;
    }

    public static OperationSupportImpl forDirectDisable() {
        return FOR_DIRECT_DISABLE;
    }

    public static OperationSupportImpl forCustomInstall() {
        return FOR_CUSTOM_INSTALL;
    }

    public static OperationSupportImpl forCustomUninstall() {
        return FOR_CUSTOM_UNINSTALL;
    }

    public abstract Boolean doOperation(ProgressHandle var1, OperationContainer<?> var2) throws OperationException;

    public abstract void doCancel() throws OperationException;

    public abstract void doRestart(OperationSupport.Restarter var1, ProgressHandle var2) throws OperationException;

    public abstract void doRestartLater(OperationSupport.Restarter var1);

    private OperationSupportImpl() {
    }

    private static void markForRestart() {
        try {
            LifecycleManager.getDefault().markForRestart();
        }
        catch (UnsupportedOperationException x) {
            LOGGER.log(Level.INFO, null, x);
        }
    }

    private static class ForCustomUninstall
    extends OperationSupportImpl {
        private Collection<UpdateElement> affectedModules = null;

        private ForCustomUninstall() {
            super();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public synchronized Boolean doOperation(ProgressHandle progress, OperationContainer<?> container) throws OperationException {
            boolean success;
            success = false;
            boolean started = false;
            try {
                List infos = container.listAll();
                ArrayList<NativeComponentUpdateElementImpl> customElements = new ArrayList<NativeComponentUpdateElementImpl>();
                for (OperationContainer.OperationInfo operationInfo : infos) {
                    UpdateElementImpl impl = Trampoline.API.impl(operationInfo.getUpdateElement());
                    assert (impl instanceof NativeComponentUpdateElementImpl);
                    customElements.add((NativeComponentUpdateElementImpl)impl);
                }
                assert (customElements != null);
                progress.start(customElements.size());
                started = true;
                int index = 0;
                this.affectedModules = new HashSet<UpdateElement>();
                for (NativeComponentUpdateElementImpl impl : customElements) {
                    progress.progress(NbBundle.getMessage(OperationSupportImpl.class, (String)"OperationSupportImpl_Custom_Uninstall", (Object)impl.getDisplayName()), ++index);
                    CustomUninstaller uninstaller = impl.getNativeItem().getUpdateItemDeploymentImpl().getCustomUninstaller();
                    assert (uninstaller != null);
                    ProgressHandle handle = ProgressHandleFactory.createHandle((String)("Installing " + impl.getDisplayName()));
                    success = uninstaller.uninstall(impl.getCodeName(), impl.getSpecificationVersion() == null ? null : impl.getSpecificationVersion().toString(), handle);
                    handle.finish();
                    if (success) {
                        UpdateUnitImpl unitImpl = Trampoline.API.impl(impl.getUpdateUnit());
                        unitImpl.setAsUninstalled();
                        this.affectedModules.add(impl.getUpdateElement());
                        continue;
                    }
                    throw new OperationException(OperationException.ERROR_TYPE.UNINSTALL, impl.getDisplayName());
                }
            }
            finally {
                if (progress != null && started) {
                    progress.finish();
                }
            }
            return success;
        }

        @Override
        public void doCancel() throws OperationException {
            assert (false);
        }

        @Override
        public void doRestart(OperationSupport.Restarter restarter, ProgressHandle progress) throws OperationException {
            OperationSupportImpl.markForRestart();
            LifecycleManager.getDefault().exit();
            this.doRestartLater(restarter);
        }

        @Override
        public void doRestartLater(OperationSupport.Restarter restarter) {
            OperationSupportImpl.markForRestart();
            if (this.affectedModules != null) {
                for (UpdateElement el : this.affectedModules) {
                    UpdateUnitFactory.getDefault().scheduleForRestart(el);
                }
            }
        }
    }

    private static class ForCustomInstall
    extends OperationSupportImpl {
        private Collection<UpdateElement> affectedModules = null;

        private ForCustomInstall() {
            super();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public synchronized Boolean doOperation(ProgressHandle progress, OperationContainer<?> container) throws OperationException {
            boolean success;
            success = false;
            boolean started = false;
            try {
                List infos = container.listAll();
                ArrayList<NativeComponentUpdateElementImpl> customElements = new ArrayList<NativeComponentUpdateElementImpl>();
                for (OperationContainer.OperationInfo operationInfo : infos) {
                    UpdateElementImpl impl = Trampoline.API.impl(operationInfo.getUpdateElement());
                    assert (impl instanceof NativeComponentUpdateElementImpl);
                    customElements.add((NativeComponentUpdateElementImpl)impl);
                }
                assert (customElements != null);
                if (progress != null) {
                    progress.start(customElements.size());
                }
                started = true;
                int index = 0;
                this.affectedModules = new HashSet<UpdateElement>();
                for (NativeComponentUpdateElementImpl impl : customElements) {
                    if (progress != null) {
                        progress.progress(NbBundle.getMessage(OperationSupportImpl.class, (String)"OperationSupportImpl_Custom_Install", (Object)impl.getDisplayName()), ++index);
                    }
                    CustomInstaller installer = impl.getInstallInfo().getCustomInstaller();
                    assert (installer != null);
                    ProgressHandle handle = ProgressHandleFactory.createHandle((String)("Installing " + impl.getDisplayName()));
                    success = installer.install(impl.getCodeName(), impl.getSpecificationVersion() == null ? null : impl.getSpecificationVersion().toString(), handle);
                    try {
                        handle.finish();
                    }
                    catch (IllegalStateException e) {
                        LOGGER.log(Level.FINE, "Can`t stop progress handle, likely was not started ", e);
                    }
                    if (success) {
                        UpdateUnitImpl unitImpl = Trampoline.API.impl(impl.getUpdateUnit());
                        unitImpl.setInstalled(impl.getUpdateElement());
                        this.affectedModules.add(impl.getUpdateElement());
                        continue;
                    }
                    throw new OperationException(OperationException.ERROR_TYPE.INSTALL, impl.getDisplayName());
                }
            }
            finally {
                if (progress != null && started) {
                    progress.finish();
                }
            }
            return success;
        }

        @Override
        public void doCancel() throws OperationException {
            assert (false);
        }

        @Override
        public void doRestart(OperationSupport.Restarter restarter, ProgressHandle progress) throws OperationException {
            OperationSupportImpl.markForRestart();
            LifecycleManager.getDefault().exit();
            this.doRestartLater(restarter);
        }

        @Override
        public void doRestartLater(OperationSupport.Restarter restarter) {
            OperationSupportImpl.markForRestart();
            if (this.affectedModules != null) {
                for (UpdateElement el : this.affectedModules) {
                    UpdateUnitFactory.getDefault().scheduleForRestart(el);
                }
            }
        }
    }

    private static class ForInstall
    extends OperationSupportImpl {
        private ForInstall() {
            super();
        }

        public synchronized Boolean doOperation(ProgressHandle progress, OperationContainer container) throws OperationException {
            OperationContainer<InstallSupport> containerForUpdate = OperationContainer.createForUpdate();
            List infos = container.listAll();
            for (OperationContainer.OperationInfo info : infos) {
                containerForUpdate.add(info.getUpdateUnit(), info.getUpdateElement());
            }
            assert (containerForUpdate.listInvalid().isEmpty());
            InstallSupport.Validator v = containerForUpdate.getSupport().doDownload(ProgressHandleFactory.createHandle((String)OperationSupportImpl.class.getName()), null, false);
            InstallSupport.Installer i = containerForUpdate.getSupport().doValidate(v, ProgressHandleFactory.createHandle((String)OperationSupportImpl.class.getName()));
            InstallSupportImpl installSupportImpl = Trampoline.API.impl(containerForUpdate.getSupport());
            Boolean needRestart = installSupportImpl.doInstall(i, ProgressHandleFactory.createHandle((String)OperationSupportImpl.class.getName()), true);
            return needRestart;
        }

        @Override
        public void doCancel() throws OperationException {
            assert (false);
        }

        @Override
        public void doRestart(OperationSupport.Restarter restarter, ProgressHandle progress) throws OperationException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void doRestartLater(OperationSupport.Restarter restarter) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    private static class ForDirectUninstall
    extends OperationSupportImpl {
        private ForDirectUninstall() {
            super();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public synchronized Boolean doOperation(ProgressHandle progress, OperationContainer<?> container) throws OperationException {
            try {
                UpdateUnitImpl impl;
                if (progress != null) {
                    progress.start();
                }
                ModuleDeleterImpl deleter = new ModuleDeleterImpl();
                List infos = container.listAll();
                HashSet<ModuleInfo> moduleInfos = new HashSet<ModuleInfo>();
                HashSet<UpdateUnit> affectedModules = new HashSet<UpdateUnit>();
                HashSet<UpdateUnit> affectedFeatures = new HashSet<UpdateUnit>();
                block9 : for (OperationContainer.OperationInfo operationInfo : infos) {
                    UpdateElement updateElement = operationInfo.getUpdateElement();
                    UpdateElementImpl updateElementImpl = Trampoline.API.impl(updateElement);
                    switch (updateElementImpl.getType()) {
                        case KIT_MODULE: 
                        case MODULE: {
                            moduleInfos.add(((ModuleUpdateElementImpl)updateElementImpl).getModuleInfo());
                            affectedModules.add(updateElementImpl.getUpdateUnit());
                            continue block9;
                        }
                        case STANDALONE_MODULE: 
                        case FEATURE: {
                            for (ModuleUpdateElementImpl moduleImpl : ((FeatureUpdateElementImpl)updateElementImpl).getContainedModuleElements()) {
                                moduleInfos.add(moduleImpl.getModuleInfo());
                                if (moduleImpl.getUpdateUnit().getInstalled() == null) continue;
                                affectedModules.add(moduleImpl.getUpdateUnit());
                            }
                            affectedFeatures.add(updateElement.getUpdateUnit());
                            continue block9;
                        }
                    }
                    assert (false);
                }
                try {
                    deleter.delete(moduleInfos.toArray((T[])new ModuleInfo[0]), progress);
                }
                catch (IOException iex) {
                    throw new OperationException(OperationException.ERROR_TYPE.UNINSTALL, iex);
                }
                for (UpdateUnit unit2 : affectedModules) {
                    assert (unit2.getInstalled() != null);
                    LOGGER.log(Level.FINE, "Module was uninstalled " + unit2.getCodeName());
                    impl = Trampoline.API.impl(unit2);
                    impl.setAsUninstalled();
                }
                for (UpdateUnit unit2 : affectedFeatures) {
                    assert (unit2.getInstalled() != null);
                    LOGGER.log(Level.FINE, "Feature was uninstalled " + unit2.getCodeName());
                    impl = Trampoline.API.impl(unit2);
                    impl.setAsUninstalled();
                }
            }
            finally {
                if (progress != null) {
                    progress.finish();
                }
            }
            return false;
        }

        @Override
        public void doCancel() throws OperationException {
            assert (false);
        }

        @Override
        public void doRestart(OperationSupport.Restarter restarter, ProgressHandle progress) throws OperationException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void doRestartLater(OperationSupport.Restarter restarter) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    private static class ForUninstall
    extends OperationSupportImpl {
        private Collection<File> files4remove = null;
        private Collection<UpdateElement> affectedModules = null;

        private ForUninstall() {
            super();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public synchronized Boolean doOperation(ProgressHandle progress, OperationContainer<?> container) throws OperationException {
            try {
                if (progress != null) {
                    progress.start();
                }
                ModuleDeleterImpl deleter = new ModuleDeleterImpl();
                List infos = container.listAll();
                HashSet<ModuleInfo> moduleInfos = new HashSet<ModuleInfo>();
                this.affectedModules = new HashSet<UpdateElement>();
                block10 : for (OperationContainer.OperationInfo operationInfo : infos) {
                    UpdateElement updateElement = operationInfo.getUpdateElement();
                    UpdateElementImpl updateElementImpl = Trampoline.API.impl(updateElement);
                    switch (updateElementImpl.getType()) {
                        case KIT_MODULE: 
                        case MODULE: {
                            moduleInfos.add(((ModuleUpdateElementImpl)updateElementImpl).getModuleInfo());
                            this.affectedModules.add(updateElementImpl.getUpdateElement());
                            break;
                        }
                        case STANDALONE_MODULE: 
                        case FEATURE: {
                            for (ModuleUpdateElementImpl moduleImpl : ((FeatureUpdateElementImpl)updateElementImpl).getContainedModuleElements()) {
                                moduleInfos.add(moduleImpl.getModuleInfo());
                                if (moduleImpl.getUpdateUnit().getInstalled() == null) continue;
                                this.affectedModules.add(moduleImpl.getUpdateElement());
                            }
                            continue block10;
                        }
                        case CUSTOM_HANDLED_COMPONENT: {
                            break;
                        }
                        default: {
                            assert (false);
                            continue block10;
                        }
                    }
                }
                try {
                    this.files4remove = deleter.markForDelete(moduleInfos, progress);
                }
                catch (IOException iex) {
                    throw new OperationException(OperationException.ERROR_TYPE.UNINSTALL, iex);
                }
            }
            finally {
                if (progress != null) {
                    progress.finish();
                }
            }
            return true;
        }

        @Override
        public void doCancel() throws OperationException {
            if (this.files4remove != null) {
                this.files4remove = null;
            }
            if (this.affectedModules != null) {
                this.affectedModules = null;
            }
        }

        @Override
        public void doRestart(OperationSupport.Restarter restarter, ProgressHandle progress) throws OperationException {
            Utilities.writeFileMarkedForDelete(this.files4remove);
            Utilities.deleteAllDoLater();
            LifecycleManager.getDefault().exit();
            this.doRestartLater(restarter);
        }

        @Override
        public void doRestartLater(OperationSupport.Restarter restarter) {
            Utilities.writeFileMarkedForDelete(this.files4remove);
            for (UpdateElement el : this.affectedModules) {
                UpdateUnitFactory.getDefault().scheduleForRestart(el);
            }
            Utilities.writeDeactivateLater(this.files4remove);
        }
    }

    private static class ForDirectDisable
    extends OperationSupportImpl {
        private ForDirectDisable() {
            super();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public synchronized Boolean doOperation(ProgressHandle progress, OperationContainer<?> container) throws OperationException {
            try {
                if (progress != null) {
                    progress.start();
                }
                ModuleManager mm = null;
                List elements = container.listAll();
                HashSet<ModuleInfo> moduleInfos = new HashSet<ModuleInfo>();
                for (OperationContainer.OperationInfo operationInfo : elements) {
                    UpdateElementImpl impl = Trampoline.API.impl(operationInfo.getUpdateElement());
                    moduleInfos.addAll(impl.getModuleInfos());
                }
                final HashSet<Module> modules = new HashSet<Module>();
                for (ModuleInfo info : moduleInfos) {
                    Module m = Utilities.toModule(info);
                    if (Utilities.canDisable(m)) {
                        modules.add(m);
                        LOGGER.log(Level.FINE, "Module will be disabled " + m.getCodeNameBase());
                    }
                    if (mm != null) continue;
                    mm = m.getManager();
                }
                assert (mm != null);
                final ModuleManager fmm = mm;
                try {
                    fmm.mutex().writeAccess((Mutex.ExceptionAction)new Mutex.ExceptionAction<Boolean>(){

                        public Boolean run() throws Exception {
                            return ForDirectDisable.disable(fmm, modules);
                        }
                    });
                }
                catch (MutexException ex) {
                    Exception x = ex.getException();
                    assert (x instanceof OperationException);
                    if (x instanceof OperationException) {
                        throw (OperationException)x;
                    }
                }
            }
            finally {
                if (progress != null) {
                    progress.finish();
                }
            }
            return false;
        }

        private static boolean disable(ModuleManager mm, Set<Module> toRun) throws OperationException {
            boolean retval = false;
            try {
                mm.disable(toRun);
                retval = true;
            }
            catch (IllegalArgumentException ilae) {
                throw new OperationException(OperationException.ERROR_TYPE.ENABLE, ilae);
            }
            return retval;
        }

        @Override
        public void doCancel() throws OperationException {
            assert (false);
        }

        @Override
        public void doRestart(OperationSupport.Restarter restarter, ProgressHandle progress) throws OperationException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void doRestartLater(OperationSupport.Restarter restarter) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

    }

    private static class ForDisable
    extends OperationSupportImpl {
        private Collection<File> controlFileForDisable = null;
        private Collection<UpdateElement> affectedModules = null;

        private ForDisable() {
            super();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public synchronized Boolean doOperation(ProgressHandle progress, OperationContainer<?> container) throws OperationException {
            try {
                if (progress != null) {
                    progress.start();
                }
                ModuleManager mm = null;
                List elements = container.listAll();
                this.affectedModules = new HashSet<UpdateElement>();
                HashSet<ModuleInfo> moduleInfos = new HashSet<ModuleInfo>();
                for (OperationContainer.OperationInfo operationInfo : elements) {
                    UpdateElementImpl impl = Trampoline.API.impl(operationInfo.getUpdateElement());
                    this.affectedModules.add(operationInfo.getUpdateElement());
                    moduleInfos.addAll(impl.getModuleInfos());
                }
                HashSet<ModuleInfo> modules = new HashSet<ModuleInfo>();
                for (ModuleInfo info : moduleInfos) {
                    Module m = Utilities.toModule(info);
                    if (Utilities.canDisable(m)) {
                        modules.add((ModuleInfo)m);
                        LOGGER.log(Level.FINE, "Mark module " + m.getCodeNameBase() + " for disable.");
                    }
                    if (mm != null) continue;
                    mm = m.getManager();
                }
                assert (mm != null);
                ModuleDeleterImpl deleter = new ModuleDeleterImpl();
                this.controlFileForDisable = deleter.markForDisable(modules, progress);
            }
            finally {
                if (progress != null) {
                    progress.finish();
                }
            }
            return true;
        }

        @Override
        public void doCancel() throws OperationException {
            if (this.controlFileForDisable != null) {
                this.controlFileForDisable = null;
            }
            if (this.affectedModules != null) {
                this.affectedModules = null;
            }
        }

        @Override
        public void doRestart(OperationSupport.Restarter restarter, ProgressHandle progress) throws OperationException {
            Utilities.writeFileMarkedForDisable(this.controlFileForDisable);
            Utilities.deleteAllDoLater();
            LifecycleManager.getDefault().exit();
            this.doRestartLater(restarter);
        }

        @Override
        public void doRestartLater(OperationSupport.Restarter restarter) {
            Utilities.writeFileMarkedForDisable(this.controlFileForDisable);
            for (UpdateElement el : this.affectedModules) {
                UpdateUnitFactory.getDefault().scheduleForRestart(el);
            }
            Utilities.writeDeactivateLater(this.controlFileForDisable);
        }
    }

    private static class ForEnable
    extends OperationSupportImpl {
        private ForEnable() {
            super();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public synchronized Boolean doOperation(ProgressHandle progress, OperationContainer<?> container) throws OperationException {
            try {
                if (progress != null) {
                    progress.start();
                }
                ModuleManager mm = null;
                List elements = container.listAll();
                HashSet<ModuleInfo> moduleInfos = new HashSet<ModuleInfo>();
                for (OperationContainer.OperationInfo operationInfo : elements) {
                    UpdateElementImpl impl = Trampoline.API.impl(operationInfo.getUpdateElement());
                    moduleInfos.addAll(impl.getModuleInfos(true));
                }
                final HashSet<Module> modules = new HashSet<Module>();
                for (ModuleInfo info : moduleInfos) {
                    Module m = Utilities.toModule(info);
                    if (Utilities.canEnable(m)) {
                        modules.add(m);
                        LOGGER.log(Level.FINE, "Module will be enabled " + m.getCodeNameBase());
                    }
                    if (mm != null) continue;
                    mm = m.getManager();
                }
                assert (mm != null);
                final ModuleManager fmm = mm;
                try {
                    fmm.mutex().writeAccess((Mutex.ExceptionAction)new Mutex.ExceptionAction<Boolean>(){

                        public Boolean run() throws Exception {
                            return ForEnable.enable(fmm, modules);
                        }
                    });
                }
                catch (MutexException ex) {
                    Exception x = ex.getException();
                    assert (x instanceof OperationException);
                    if (x instanceof OperationException) {
                        throw (OperationException)x;
                    }
                }
            }
            finally {
                if (progress != null) {
                    progress.finish();
                }
            }
            return false;
        }

        @Override
        public void doCancel() throws OperationException {
            assert (false);
        }

        private static boolean enable(ModuleManager mm, Set<Module> toRun) throws OperationException {
            boolean retval = false;
            try {
                mm.enable(toRun);
                retval = true;
            }
            catch (IllegalArgumentException ilae) {
                throw new OperationException(OperationException.ERROR_TYPE.ENABLE, ilae);
            }
            catch (InvalidException ie) {
                throw new OperationException(OperationException.ERROR_TYPE.ENABLE, (Exception)ie);
            }
            return retval;
        }

        @Override
        public void doRestart(OperationSupport.Restarter restarter, ProgressHandle progress) throws OperationException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void doRestartLater(OperationSupport.Restarter restarter) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

    }

}

