/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.Module
 *  org.netbeans.ModuleManager
 *  org.netbeans.core.startup.AutomaticDependencies
 *  org.netbeans.core.startup.AutomaticDependencies$Report
 *  org.netbeans.core.startup.Main
 *  org.netbeans.core.startup.TopLogging
 *  org.netbeans.updater.ModuleDeactivator
 *  org.netbeans.updater.UpdateTracking
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.Dependency
 *  org.openide.modules.InstalledFileLocator
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.Places
 *  org.openide.modules.SpecificationVersion
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.Utilities
 *  org.openide.xml.EntityCatalog
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.modules.autoupdate.services;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import java.util.zip.ZipEntry;
import org.netbeans.Module;
import org.netbeans.ModuleManager;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateManager;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.core.startup.AutomaticDependencies;
import org.netbeans.core.startup.Main;
import org.netbeans.core.startup.TopLogging;
import org.netbeans.modules.autoupdate.services.DependencyAggregator;
import org.netbeans.modules.autoupdate.services.DependencyChecker;
import org.netbeans.modules.autoupdate.services.FeatureUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.InstallManager;
import org.netbeans.modules.autoupdate.services.InstallSupportImpl;
import org.netbeans.modules.autoupdate.services.ModuleCache;
import org.netbeans.modules.autoupdate.services.ModuleUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.Trampoline;
import org.netbeans.modules.autoupdate.services.UpdateElementImpl;
import org.netbeans.modules.autoupdate.services.UpdateManagerImpl;
import org.netbeans.modules.autoupdate.updateprovider.InstallInfo;
import org.netbeans.modules.autoupdate.updateprovider.InstalledModuleProvider;
import org.netbeans.modules.autoupdate.updateprovider.UpdateItemImpl;
import org.netbeans.spi.autoupdate.KeyStoreProvider;
import org.netbeans.spi.autoupdate.UpdateItem;
import org.netbeans.updater.ModuleDeactivator;
import org.netbeans.updater.UpdateTracking;
import org.openide.filesystems.FileUtil;
import org.openide.modules.Dependency;
import org.openide.modules.InstalledFileLocator;
import org.openide.modules.ModuleInfo;
import org.openide.modules.Places;
import org.openide.modules.SpecificationVersion;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.xml.EntityCatalog;
import org.openide.xml.XMLUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class Utilities {
    public static final String UNSIGNED = "UNSIGNED";
    public static final String N_A = "N/A";
    public static final String TRUSTED = "TRUSTED";
    public static final String UNTRUSTED = "UNTRUSTED";
    public static final String UPDATE_DIR = "update";
    public static final String FILE_SEPARATOR = System.getProperty("file.separator");
    public static final String DOWNLOAD_DIR = "update" + FILE_SEPARATOR + "download";
    public static final String RUNNING_DOWNLOAD_DIR = "update" + FILE_SEPARATOR + "download-in-progress";
    public static final String NBM_EXTENTSION = ".nbm";
    public static final String JAR_EXTENSION = ".jar";
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd");
    public static final String ATTR_ESSENTIAL = "AutoUpdate-Essential-Module";
    private static final String PLUGIN_MANAGER_FIRST_CLASS_MODULES = "plugin.manager.first.class.modules";
    private static final String USER_KS_KEY = "userKS";
    private static final String USER_KS_FILE_NAME = "user.ks";
    private static final String KS_USER_PASSWORD = "open4user";
    private static Lookup.Result<KeyStoreProvider> result;
    private static final Logger err;
    private static final String ATTR_NAME = "name";
    private static final String ATTR_SPEC_VERSION = "specification_version";
    private static final String ATTR_SIZE = "size";
    private static final String ATTR_NBM_NAME = "nbm_name";
    private static Reference<Map<ModuleInfo, Set<UpdateElement>>> cachedInfo2RequestedReference;
    private static Reference<Set<ModuleInfo>> cachedInfosReference;
    private static Reference<Set<UpdateElement>> cachedResultReference;
    private static String productVersion;

    private Utilities() {
    }

    public static Collection<KeyStore> getKeyStore() {
        Collection c;
        if (result == null) {
            result = Lookup.getDefault().lookupResult(KeyStoreProvider.class);
            result.addLookupListener((LookupListener)new KeyStoreProviderListener());
        }
        if ((c = result.allInstances()) == null || c.isEmpty()) {
            return Collections.emptyList();
        }
        ArrayList<KeyStore> kss = new ArrayList<KeyStore>();
        for (KeyStoreProvider provider : c) {
            KeyStore ks = provider.getKeyStore();
            if (ks == null) continue;
            kss.add(ks);
        }
        return kss;
    }

    public static String verifyCertificates(Collection<Certificate> archiveCertificates, Collection<Certificate> trustedCertificates) {
        if (archiveCertificates == null) {
            return "N/A";
        }
        if (!archiveCertificates.isEmpty()) {
            HashSet<Certificate> c = new HashSet<Certificate>(trustedCertificates);
            c.retainAll(archiveCertificates);
            if (!c.isEmpty()) {
                return "TRUSTED";
            }
            return "UNTRUSTED";
        }
        return "UNSIGNED";
    }

    public static Collection<Certificate> getCertificates(KeyStore keyStore) throws KeyStoreException {
        HashSet<Certificate> certs = new HashSet<Certificate>();
        for (String alias : Collections.list(keyStore.aliases())) {
            Certificate[] certificateChain = keyStore.getCertificateChain(alias);
            if (certificateChain != null) {
                certs.addAll(Arrays.asList(certificateChain));
            }
            certs.add(keyStore.getCertificate(alias));
        }
        return certs;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static Collection<Certificate> getNbmCertificates(File nbmFile) throws IOException {
        HashSet<Certificate> certs;
        boolean empty;
        certs = new HashSet<Certificate>();
        JarFile jf = new JarFile(nbmFile);
        empty = true;
        try {
            for (JarEntry entry : Collections.list(jf.entries())) {
                Utilities.verifyEntry(jf, entry);
                if (entry.getName().startsWith("META-INF/")) continue;
                empty = false;
                if (entry.getCertificates() == null) continue;
                certs.addAll(Arrays.asList(entry.getCertificates()));
            }
        }
        finally {
            jf.close();
        }
        return empty ? null : certs;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void verifyEntry(JarFile jf, JarEntry je) throws IOException {
        InputStream is = null;
        try {
            is = jf.getInputStream(je);
            byte[] buffer = new byte[8192];
            while (is.read(buffer, 0, buffer.length) != -1) {
            }
        }
        finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private static File getInstallLater(File root) {
        File file = new File(root.getPath() + FILE_SEPARATOR + DOWNLOAD_DIR + FILE_SEPARATOR + "install_later.xml");
        return file;
    }

    public static void deleteAllDoLater() {
        List clusters = UpdateTracking.clusters((boolean)true);
        assert (clusters != null);
        for (File cluster : clusters) {
            for (File doLater : Utilities.findDoLater(cluster)) {
                doLater.delete();
            }
        }
    }

    private static Collection<File> findDoLater(File cluster) {
        if (!cluster.exists()) {
            return Collections.emptySet();
        }
        HashSet<File> res = new HashSet<File>();
        if (Utilities.getInstallLater(cluster).exists()) {
            res.add(Utilities.getInstallLater(cluster));
        }
        if (ModuleDeactivator.getDeactivateLater((File)cluster).exists()) {
            res.add(ModuleDeactivator.getDeactivateLater((File)cluster));
        }
        return res;
    }

    public static void writeInstallLater(Map<UpdateElementImpl, File> updates) {
        List clusters = UpdateTracking.clusters((boolean)true);
        assert (clusters != null);
        for (File cluster : clusters) {
            Utilities.writeInstallLaterToCluster(cluster, updates);
        }
    }

    private static void writeInstallLaterToCluster(File cluster, Map<UpdateElementImpl, File> updates) {
        Document document = XMLUtil.createDocument((String)"installed_modules", (String)null, (String)null, (String)null);
        Element root = document.getDocumentElement();
        if (updates.isEmpty()) {
            return;
        }
        boolean isEmpty = true;
        for (UpdateElementImpl elementImpl : updates.keySet()) {
            File c = updates.get(elementImpl);
            if (!cluster.equals(c)) continue;
            Element module = document.createElement("module");
            module.setAttribute("codename", elementImpl.getCodeName());
            module.setAttribute("name", elementImpl.getDisplayName());
            module.setAttribute("specification_version", elementImpl.getSpecificationVersion().toString());
            module.setAttribute("size", Long.toString(elementImpl.getDownloadSize()));
            module.setAttribute("nbm_name", InstallSupportImpl.getDestination(cluster, elementImpl.getCodeName(), elementImpl.getInstallInfo().getDistribution()).getName());
            root.appendChild(module);
            isEmpty = false;
        }
        if (isEmpty) {
            return;
        }
        Utilities.writeXMLDocumentToFile(document, Utilities.getInstallLater(cluster));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void writeXMLDocumentToFile(Document doc, File dest) {
        doc.getDocumentElement().normalize();
        dest.getParentFile().mkdirs();
        InputStream is = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        OutputStream fos = null;
        try {
            try {
                XMLUtil.write((Document)doc, (OutputStream)bos, (String)"UTF-8");
                bos.close();
                fos = new FileOutputStream(dest);
                is = new ByteArrayInputStream(bos.toByteArray());
                FileUtil.copy((InputStream)is, (OutputStream)fos);
            }
            finally {
                if (is != null) {
                    is.close();
                }
                if (fos != null) {
                    fos.close();
                }
                bos.close();
            }
        }
        catch (FileNotFoundException fnfe) {
            Exceptions.printStackTrace((Throwable)fnfe);
        }
        catch (IOException ioe) {
            Exceptions.printStackTrace((Throwable)ioe);
        }
        finally {
            try {
                bos.close();
            }
            catch (IOException x) {
                Exceptions.printStackTrace((Throwable)x);
            }
        }
    }

    public static void writeDeactivateLater(Collection<File> files) {
        File userdir = InstallManager.getUserDir();
        assert (userdir != null && userdir.exists());
        Utilities.writeMarkedFilesToFile(files, ModuleDeactivator.getDeactivateLater((File)userdir));
    }

    public static void writeFileMarkedForDelete(Collection<File> files) {
        Utilities.writeMarkedFilesToFile(files, ModuleDeactivator.getControlFileForMarkedForDelete((File)InstallManager.getUserDir()));
    }

    public static void writeFileMarkedForDisable(Collection<File> files) {
        Utilities.writeMarkedFilesToFile(files, ModuleDeactivator.getControlFileForMarkedForDisable((File)InstallManager.getUserDir()));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void writeMarkedFilesToFile(Collection<File> files, File dest) {
        StringBuilder content = new StringBuilder();
        if (dest.exists()) {
            content.append(ModuleDeactivator.readStringFromFile((File)dest));
        }
        for (File f : files) {
            content.append(f.getAbsolutePath());
            content.append(UpdateTracking.PATH_SEPARATOR);
        }
        if (content.length() == 0) {
            return;
        }
        dest.getParentFile().mkdirs();
        assert (dest.getParentFile().exists() && dest.getParentFile().isDirectory());
        InputStream is = null;
        FileOutputStream fos = null;
        try {
            try {
                fos = new FileOutputStream(dest);
                is = new ByteArrayInputStream(content.toString().getBytes());
                FileUtil.copy((InputStream)is, (OutputStream)fos);
            }
            finally {
                if (is != null) {
                    is.close();
                }
                if (fos != null) {
                    fos.close();
                }
            }
        }
        catch (FileNotFoundException fnfe) {
            Exceptions.printStackTrace((Throwable)fnfe);
        }
        catch (IOException ioe) {
            Exceptions.printStackTrace((Throwable)ioe);
        }
    }

    public static void writeAdditionalInformation(Map<UpdateElementImpl, File> updates) {
        List clusters = UpdateTracking.clusters((boolean)true);
        assert (clusters != null);
        for (File cluster : clusters) {
            Utilities.writeAdditionalInformationToCluster(cluster, updates);
        }
    }

    public static File locateUpdateTracking(ModuleInfo m) {
        String fileNameToFind = "update_tracking/" + m.getCodeNameBase().replace('.', '-') + ".xml";
        return InstalledFileLocator.getDefault().locate(fileNameToFind, m.getCodeNameBase(), false);
    }

    public static String readSourceFromUpdateTracking(ModuleInfo m) {
        Node n;
        String res = null;
        File ut = Utilities.locateUpdateTracking(m);
        if (ut != null && (n = Utilities.getModuleConfiguration(ut)) != null) {
            Node attrOrigin = n.getAttributes().getNamedItem("origin");
            assert (attrOrigin != null);
            if (!"updater".equals(attrOrigin.getNodeValue()) && !"installer".equals(attrOrigin.getNodeValue())) {
                res = attrOrigin.getNodeValue();
            }
        }
        return res;
    }

    public static Date readInstallTimeFromUpdateTracking(ModuleInfo m) {
        Node n;
        Date res = null;
        String time = null;
        File ut = Utilities.locateUpdateTracking(m);
        if (ut != null && (n = Utilities.getModuleConfiguration(ut)) != null) {
            Node attrInstallTime = n.getAttributes().getNamedItem("install_time");
            assert (attrInstallTime != null);
            time = attrInstallTime.getNodeValue();
        }
        if (time != null) {
            try {
                long lTime = Long.parseLong(time);
                res = new Date(lTime);
            }
            catch (NumberFormatException nfe) {
                Utilities.getLogger().log(Level.INFO, nfe.getMessage(), nfe);
            }
        }
        return res;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void writeUpdateOfUpdaterJar(JarEntry updaterJarEntry, File zipFileWithUpdater, File targetCluster) throws IOException {
        JarFile jf = new JarFile(zipFileWithUpdater);
        String entryPath = updaterJarEntry.getName();
        String entryName = entryPath.contains("/") ? entryPath.substring(entryPath.lastIndexOf("/") + 1) : entryPath;
        File dest = new File(targetCluster, "update" + UpdateTracking.FILE_SEPARATOR + "new_updater" + UpdateTracking.FILE_SEPARATOR + entryName);
        dest.getParentFile().mkdirs();
        assert (dest.getParentFile().exists() && dest.getParentFile().isDirectory());
        InputStream is = null;
        FileOutputStream fos = null;
        try {
            try {
                fos = new FileOutputStream(dest);
                is = jf.getInputStream(updaterJarEntry);
                FileUtil.copy((InputStream)is, (OutputStream)fos);
            }
            finally {
                if (is != null) {
                    is.close();
                }
                if (fos != null) {
                    fos.close();
                }
                jf.close();
            }
        }
        catch (FileNotFoundException fnfe) {
            Utilities.getLogger().log(Level.SEVERE, fnfe.getLocalizedMessage(), fnfe);
        }
        catch (IOException ioe) {
            Utilities.getLogger().log(Level.SEVERE, ioe.getLocalizedMessage(), ioe);
        }
    }

    static void cleanUpdateOfUpdaterJar() {
        List clusters = UpdateTracking.clusters((boolean)true);
        assert (clusters != null);
        for (File cluster : clusters) {
            File updaterDir = new File(cluster, "update" + UpdateTracking.FILE_SEPARATOR + "new_updater");
            if (!updaterDir.exists() || !updaterDir.isDirectory()) continue;
            for (File f : updaterDir.listFiles()) {
                f.delete();
            }
            updaterDir.delete();
        }
    }

    static Module toModule(UpdateUnit uUnit) {
        return Utilities.getModuleInstance(uUnit.getCodeName(), null);
    }

    public static Module toModule(String codeNameBase, SpecificationVersion specificationVersion) {
        return Utilities.getModuleInstance(codeNameBase, specificationVersion);
    }

    public static Module toModule(ModuleInfo info) {
        Module m = Utilities.getModuleInstance(info.getCodeNameBase(), info.getSpecificationVersion());
        if (m == null && info instanceof Module) {
            m = (Module)info;
        }
        return m;
    }

    public static boolean isFixed(ModuleInfo info) {
        Module m = Utilities.toModule(info);
        assert (!info.isEnabled() || m != null);
        return m == null ? false : m.isFixed();
    }

    public static boolean isValid(ModuleInfo info) {
        Module m = Utilities.toModule(info);
        assert (!info.isEnabled() || m != null);
        return m == null ? false : m.isValid();
    }

    static UpdateUnit toUpdateUnit(Module m) {
        return UpdateManagerImpl.getInstance().getUpdateUnit(m.getCodeNameBase());
    }

    static UpdateUnit toUpdateUnit(String codeNameBase) {
        return UpdateManagerImpl.getInstance().getUpdateUnit(codeNameBase);
    }

    public static Set<UpdateElement> findRequiredUpdateElements(UpdateElement element, Collection<ModuleInfo> infos, Set<Dependency> brokenDependencies, boolean topAggressive, Collection<UpdateElement> recommended) {
        HashSet<UpdateElement> retval = new HashSet<UpdateElement>();
        switch (element.getUpdateUnit().getType()) {
            case KIT_MODULE: 
            case MODULE: {
                Set<UpdateElement> more;
                int max_counter;
                boolean avoidRecommended = recommended != null && !recommended.isEmpty();
                ModuleUpdateElementImpl el = (ModuleUpdateElementImpl)Trampoline.API.impl(element);
                Set deps = new HashSet<Dependency>(el.getModuleInfo().getDependencies());
                HashSet<ModuleInfo> availableInfos = new HashSet<ModuleInfo>(infos);
                int counter = max_counter = el.getType().equals((Object)UpdateManager.TYPE.KIT_MODULE) ? 2 : 1;
                boolean aggressive = topAggressive && counter > 0;
                HashSet<Dependency> all = new HashSet<Dependency>();
                do {
                    Set<Dependency> newones = Utilities.processDependencies(deps, retval, availableInfos, brokenDependencies, element, aggressive, recommended, avoidRecommended);
                    newones.removeAll(all);
                    if (newones.isEmpty()) break;
                    all.addAll(newones);
                    deps = newones;
                } while (true);
                HashSet<Dependency> moreBroken = new HashSet<Dependency>();
                HashSet<ModuleInfo> tmp = new HashSet<ModuleInfo>(availableInfos);
                counter = max_counter;
                boolean bl = aggressive = topAggressive && counter > 0;
                while (retval.addAll(more = Utilities.handleBackwardCompatability(tmp, moreBroken, aggressive))) {
                    if (!moreBroken.isEmpty()) {
                        brokenDependencies.addAll(moreBroken);
                        break;
                    }
                    for (UpdateElement e : more) {
                        tmp.add(((ModuleUpdateElementImpl)Trampoline.API.impl(e)).getModuleInfo());
                    }
                    aggressive = aggressive && counter-- > 0;
                }
                if (moreBroken.isEmpty()) break;
                brokenDependencies.addAll(moreBroken);
                break;
            }
            case STANDALONE_MODULE: 
            case FEATURE: {
                FeatureUpdateElementImpl feature = (FeatureUpdateElementImpl)Trampoline.API.impl(element);
                boolean aggressive = topAggressive;
                for (ModuleUpdateElementImpl module : feature.getContainedModuleElements()) {
                    retval.addAll(Utilities.findRequiredUpdateElements(module.getUpdateElement(), infos, brokenDependencies, aggressive, recommended));
                }
                break;
            }
            case CUSTOM_HANDLED_COMPONENT: {
                Utilities.getLogger().log(Level.INFO, "CUSTOM_HANDLED_COMPONENT doesn't care about required elements.");
                break;
            }
            default: {
                assert (false);
                break;
            }
        }
        return retval;
    }

    private static Set<UpdateElement> handleBackwardCompatability4ModuleInfo(ModuleInfo mi, Set<ModuleInfo> forInstall, Set<Dependency> brokenDependencies, boolean aggressive) {
        Set<UpdateElement> requested;
        if (cachedInfo2RequestedReference != null && cachedInfo2RequestedReference.get() != null && (requested = cachedInfo2RequestedReference.get().get((Object)mi)) != null) {
            return requested;
        }
        UpdateUnit u = UpdateManagerImpl.getInstance().getUpdateUnit(mi.getCodeNameBase());
        HashSet<UpdateElement> moreRequested = new HashSet<UpdateElement>();
        if (u == null) {
            return moreRequested;
        }
        UpdateElement i = u.getInstalled();
        if (i == null) {
            return moreRequested;
        }
        HashSet dependencies = new HashSet();
        dependencies.addAll(Dependency.create((int)1, (String)mi.getCodeName()));
        TreeSet<String> newTokens = new TreeSet<String>(Arrays.asList(mi.getProvides()));
        TreeSet<String> oldTokens = new TreeSet<String>(Arrays.asList(((ModuleUpdateElementImpl)Trampoline.API.impl(i)).getModuleInfo().getProvides()));
        oldTokens.removeAll(newTokens);
        for (String tok : oldTokens) {
            if (tok.startsWith("org.openide.modules.os")) continue;
            dependencies.addAll(Dependency.create((int)5, (String)tok));
            dependencies.addAll(Dependency.create((int)6, (String)tok));
        }
        for (Dependency d : dependencies) {
            DependencyAggregator deco = DependencyAggregator.getAggregator(d);
            int type = d.getType();
            String name = d.getName();
            for (ModuleInfo depMI : deco.getDependening()) {
                Module depM = Utilities.getModuleInstance(depMI.getCodeNameBase(), depMI.getSpecificationVersion());
                if (depM == null || !depM.getProblems().isEmpty()) continue;
                for (Dependency toTry : depM.getDependencies()) {
                    UpdateUnit tryUU;
                    Set<Dependency> newones;
                    if (type != toTry.getType() || !name.equals(toTry.getName()) || DependencyChecker.checkDependencyModule(toTry, mi) || (tryUU = UpdateManagerImpl.getInstance().getUpdateUnit(depM.getCodeNameBase())).getAvailableUpdates().isEmpty()) continue;
                    UpdateElement tryUE = tryUU.getAvailableUpdates().get(0);
                    ModuleInfo tryUpdated = ((ModuleUpdateElementImpl)Trampoline.API.impl(tryUE)).getModuleInfo();
                    Set deps = new HashSet<Dependency>(tryUpdated.getDependencies());
                    HashSet<ModuleInfo> availableInfos = new HashSet<ModuleInfo>(forInstall);
                    while (!(newones = Utilities.processDependencies(deps, moreRequested, availableInfos, brokenDependencies, tryUE, aggressive, null, false)).isEmpty()) {
                        deps = newones;
                    }
                    moreRequested.add(tryUE);
                }
            }
        }
        if (cachedInfo2RequestedReference == null || cachedInfo2RequestedReference.get() == null) {
            cachedInfo2RequestedReference = new WeakReference(new HashMap());
        }
        cachedInfo2RequestedReference.get().put(mi, moreRequested);
        return moreRequested;
    }

    private static Set<UpdateElement> handleBackwardCompatability(Set<ModuleInfo> forInstall, Set<Dependency> brokenDependencies, boolean aggressive) {
        if (cachedInfosReference != null && cachedInfosReference.get() != null && cachedInfosReference.get().equals(forInstall) && cachedResultReference != null && cachedResultReference.get() != null) {
            return cachedResultReference.get();
        }
        cachedInfosReference = new WeakReference<Set<ModuleInfo>>(forInstall);
        err.finest("calling handleBackwardCompatability(size: " + forInstall.size() + ")");
        HashSet<UpdateElement> moreRequested = new HashSet<UpdateElement>();
        for (ModuleInfo mi : forInstall) {
            moreRequested.addAll(Utilities.handleBackwardCompatability4ModuleInfo(mi, forInstall, brokenDependencies, aggressive));
        }
        cachedResultReference = new WeakReference(moreRequested);
        return moreRequested;
    }

    private static Set<Dependency> processDependencies(Set<Dependency> original, Set<UpdateElement> retval, Set<ModuleInfo> availableInfos, Set<Dependency> brokenDependencies, UpdateElement el, boolean agressive, Collection<UpdateElement> recommended, boolean avoidRecommended) {
        HashSet<Dependency> res = new HashSet<Dependency>();
        AutomaticDependencies.Report rep = AutomaticDependencies.getDefault().refineDependenciesAndReport(el.getCodeName(), original);
        if (rep.isModified()) {
            err.fine(rep.toString());
        }
        for (Dependency dep : original) {
            Collection<UpdateElement> requestedElements;
            if (7 == dep.getType() && avoidRecommended || (requestedElements = Utilities.handleDependency(el, dep, availableInfos, brokenDependencies, agressive)) == null) continue;
            if (7 == dep.getType() && recommended != null) {
                recommended.addAll(requestedElements);
            }
            for (UpdateElement req : requestedElements) {
                ModuleUpdateElementImpl reqM = (ModuleUpdateElementImpl)Trampoline.API.impl(req);
                availableInfos.add(reqM.getModuleInfo());
                retval.add(req);
                res.addAll(reqM.getModuleInfo().getDependencies());
            }
        }
        res.removeAll(original);
        return res;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    public static Collection<UpdateElement> handleDependency(UpdateElement el, Dependency dep, Collection<ModuleInfo> availableInfos, Set<Dependency> brokenDependencies, boolean beAggressive) {
        requested = new HashSet<UpdateElement>();
        switch (dep.getType()) {
            case 3: {
                if (DependencyChecker.matchDependencyJava(dep) != false) return requested;
                brokenDependencies.add(dep);
                return requested;
            }
            case 2: {
                if (DependencyChecker.matchPackageDependency(dep) != false) return requested;
                brokenDependencies.add(dep);
                return requested;
            }
            case 1: {
                reqUnits = DependencyAggregator.getRequested(dep);
                if (!Utilities.$assertionsDisabled && reqUnits != null && !reqUnits.isEmpty() && reqUnits.size() != 1) {
                    throw new AssertionError((Object)((Object)dep + " returns null, empty or only once module, but returns " + reqUnits));
                }
                matched = false;
                v0 = u = reqUnits == null || reqUnits.isEmpty() != false ? null : reqUnits.iterator().next();
                if (u == null) ** GOTO lbl35
                aggressive = beAggressive;
                if (aggressive && (Utilities.isFirstClassModule(el) || u.getType() == UpdateManager.TYPE.KIT_MODULE)) {
                    aggressive = false;
                }
                if (u.getInstalled() != null) {
                    reqElImpl = Trampoline.API.impl(u.getInstalled());
                    matched = DependencyChecker.checkDependencyModule(dep, ((ModuleUpdateElementImpl)reqElImpl).getModuleInfo());
                }
                if (!matched) {
                    for (ModuleInfo m : availableInfos) {
                        if (!DependencyChecker.checkDependencyModule(dep, m)) continue;
                        matched = true;
                        break;
                    }
                }
                if (!aggressive && matched) ** GOTO lbl39
                v1 = reqEl = u.getAvailableUpdates().isEmpty() != false ? null : u.getAvailableUpdates().get(0);
                if (reqEl != null && DependencyChecker.checkDependencyModule(dep, info = (reqModuleImpl = (ModuleUpdateElementImpl)(reqElImpl = Trampoline.API.impl(reqEl))).getModuleInfo()) && !availableInfos.contains((Object)info)) {
                    requested.add(reqEl);
                    matched = true;
                }
                ** GOTO lbl39
lbl35: // 2 sources:
                for (ModuleInfo m : availableInfos) {
                    if (!DependencyChecker.checkDependencyModule(dep, m)) continue;
                    matched = true;
                    break;
                }
lbl39: // 4 sources:
                if (matched != false) return requested;
                brokenDependencies.add(dep);
                return requested;
            }
            case 5: 
            case 6: 
            case 7: {
                if ("org.openide.modules.ModuleFormat1".equals(dep.getName()) != false) return requested;
                if ("org.openide.modules.ModuleFormat2".equals(dep.getName())) {
                    return requested;
                }
                requestedUnits = DependencyAggregator.getRequested(dep);
                passed = false;
                if (requestedUnits == null || requestedUnits.isEmpty()) {
                    for (ModuleInfo m : availableInfos) {
                        if (!Arrays.asList(m.getProvides()).contains(dep.getName())) continue;
                        return requested;
                    }
                } else {
                    passed = true;
                    for (UpdateUnit uu : requestedUnits) {
                        if (uu.getAvailableUpdates().isEmpty()) continue;
                        requested.add(uu.getAvailableUpdates().get(0));
                    }
                }
                if (passed != false) return requested;
                if (7 == dep.getType()) return requested;
                brokenDependencies.add(dep);
            }
        }
        return requested;
    }

    static Set<String> getBrokenDependencies(UpdateElement element, List<ModuleInfo> infos) {
        assert (element != null);
        HashSet<Dependency> brokenDependencies = new HashSet<Dependency>();
        Utilities.findRequiredUpdateElements(element, infos, brokenDependencies, false, new HashSet<UpdateElement>());
        for (ModuleInfo mi : infos) {
            UpdateElement i;
            UpdateElement ue;
            ModuleInfo newerMI;
            UpdateUnit u = UpdateManagerImpl.getInstance().getUpdateUnit(mi.getCodeNameBase());
            if (u == null || (i = u.getInstalled()) == null || !u.getAvailableUpdates().isEmpty() && infos.contains((Object)(newerMI = ((ModuleUpdateElementImpl)Trampoline.API.impl(ue = u.getAvailableUpdates().get(0))).getModuleInfo()))) continue;
            for (Dependency d : Dependency.create((int)1, (String)mi.getCodeName())) {
                DependencyAggregator deco = DependencyAggregator.getAggregator(d);
                for (ModuleInfo depMI : deco.getDependening()) {
                    Module depM = Utilities.getModuleInstance(depMI.getCodeNameBase(), depMI.getSpecificationVersion());
                    if (depM == null || !depM.getProblems().isEmpty()) continue;
                    for (Dependency toTry : depM.getDependencies()) {
                        if (!deco.equals(DependencyAggregator.getAggregator(toTry)) || DependencyChecker.checkDependencyModule(toTry, mi)) continue;
                        brokenDependencies.add(toTry);
                    }
                }
            }
            TreeSet<String> newTokens = new TreeSet<String>(Arrays.asList(mi.getProvides()));
            TreeSet<String> oldTokens = new TreeSet<String>(Arrays.asList(((ModuleUpdateElementImpl)Trampoline.API.impl(i)).getModuleInfo().getProvides()));
            oldTokens.removeAll(newTokens);
            for (String tok : oldTokens) {
                HashSet deps = new HashSet(Dependency.create((int)5, (String)tok));
                deps.addAll(Dependency.create((int)6, (String)tok));
                for (Dependency d2 : deps) {
                    DependencyAggregator deco = DependencyAggregator.getAggregator(d2);
                    for (ModuleInfo depMI : deco.getDependening()) {
                        Module depM = Utilities.getModuleInstance(depMI.getCodeNameBase(), depMI.getSpecificationVersion());
                        if (depM == null || !depM.getProblems().isEmpty()) continue;
                        for (Dependency toTry : depM.getDependencies()) {
                            if (!deco.equals(DependencyAggregator.getAggregator(toTry))) continue;
                            brokenDependencies.add(toTry);
                        }
                    }
                }
            }
        }
        HashSet<String> retval = new HashSet<String>(brokenDependencies.size());
        for (Dependency dep : brokenDependencies) {
            retval.add(dep.toString());
        }
        return retval;
    }

    static Set<String> getBrokenDependenciesInInstalledModules(UpdateElement element) {
        assert (element != null);
        HashSet<Dependency> deps = new HashSet<Dependency>();
        for (ModuleInfo m : Utilities.getModuleInfos(Collections.singleton(element))) {
            deps.addAll(DependencyChecker.findBrokenDependenciesTransitive(m, InstalledModuleProvider.getInstalledModules().values(), new HashSet<ModuleInfo>()));
        }
        HashSet<String> retval = new HashSet<String>();
        for (Dependency dep : deps) {
            retval.add(dep.toString());
        }
        return retval;
    }

    private static List<ModuleInfo> getModuleInfos(Collection<UpdateElement> elements) {
        ArrayList<ModuleInfo> infos = new ArrayList<ModuleInfo>(elements.size());
        for (UpdateElement el : elements) {
            if (el.getUpdateUnit() != null && el.getUpdateUnit().isPending()) continue;
            UpdateElementImpl impl = Trampoline.API.impl(el);
            infos.addAll(impl.getModuleInfos());
        }
        return infos;
    }

    private static Module getModuleInstance(String codeNameBase, SpecificationVersion specificationVersion) {
        ModuleInfo mi = ModuleCache.getInstance().find(codeNameBase);
        if (mi instanceof Module) {
            Module m = (Module)mi;
            if (specificationVersion == null) {
                err.log(Level.FINE, "no module {0} for null version", (Object)m);
                return m;
            }
            SpecificationVersion version = m.getSpecificationVersion();
            if (version == null) {
                err.log(Level.FINER, "No version for {0}", (Object)m);
                return null;
            }
            int res = version.compareTo((Object)specificationVersion);
            err.log(Level.FINER, "Comparing versions: {0}.compareTo({1}) = {2}", new Object[]{version, specificationVersion, res});
            return res >= 0 ? m : null;
        }
        return null;
    }

    public static boolean isAutomaticallyEnabled(String codeNameBase) {
        Module m = Utilities.getModuleInstance(codeNameBase, null);
        return m != null ? m.isAutoload() || m.isEager() || m.isFixed() : false;
    }

    public static ModuleInfo takeModuleInfo(UpdateElement el) {
        UpdateElementImpl impl = Trampoline.API.impl(el);
        assert (impl instanceof ModuleUpdateElementImpl);
        return ((ModuleUpdateElementImpl)impl).getModuleInfo();
    }

    public static String getProductVersion() {
        if (productVersion == null) {
            String buildNumber = System.getProperty("netbeans.buildnumber");
            productVersion = NbBundle.getMessage(TopLogging.class, (String)"currentVersion", (Object)buildNumber);
        }
        return productVersion;
    }

    private static Node getModuleConfiguration(File moduleUpdateTracking) {
        Document document;
        try {
            BufferedInputStream is = new BufferedInputStream(new FileInputStream(moduleUpdateTracking));
            InputSource xmlInputSource = new InputSource(is);
            document = XMLUtil.parse((InputSource)xmlInputSource, (boolean)false, (boolean)false, (ErrorHandler)null, (EntityResolver)EntityCatalog.getDefault());
            is.close();
        }
        catch (SAXException saxe) {
            Utilities.getLogger().log(Level.INFO, "SAXException when reading " + moduleUpdateTracking, saxe);
            return null;
        }
        catch (IOException ioe) {
            Utilities.getLogger().log(Level.INFO, "IOException when reading " + moduleUpdateTracking, ioe);
            return null;
        }
        assert (document.getDocumentElement() != null);
        if (document.getDocumentElement() == null) {
            return null;
        }
        return Utilities.getModuleElement(document.getDocumentElement());
    }

    private static Node getModuleElement(Element element) {
        Node lastElement = null;
        assert ("module".equals(element.getTagName()));
        NodeList listModuleVersions = element.getElementsByTagName("module_version");
        for (int i = 0; i < listModuleVersions.getLength() && (lastElement = Utilities.getModuleLastVersion(listModuleVersions.item(i))) == null; ++i) {
        }
        return lastElement;
    }

    private static Node getModuleLastVersion(Node version) {
        Node attrLast = version.getAttributes().getNamedItem("last");
        assert (attrLast != null);
        if (Boolean.valueOf(attrLast.getNodeValue()).booleanValue()) {
            return version;
        }
        return null;
    }

    private static File getAdditionalInformation(File root) {
        File file = new File(root.getPath() + FILE_SEPARATOR + DOWNLOAD_DIR + FILE_SEPARATOR + "additional_information.xml");
        return file;
    }

    private static void writeAdditionalInformationToCluster(File cluster, Map<UpdateElementImpl, File> updates) {
        if (updates.isEmpty()) {
            return;
        }
        Document document = XMLUtil.createDocument((String)"module_additional", (String)null, (String)null, (String)null);
        Element root = document.getDocumentElement();
        boolean isEmpty = true;
        for (UpdateElementImpl impl : updates.keySet()) {
            File c = updates.get(impl);
            if (!cluster.equals(c)) continue;
            Element module = document.createElement("module");
            module.setAttribute("nbm_name", InstallSupportImpl.getDestination(cluster, impl.getCodeName(), impl.getInstallInfo().getDistribution()).getName());
            module.setAttribute("source-display-name", impl.getSource());
            root.appendChild(module);
            isEmpty = false;
        }
        if (isEmpty) {
            return;
        }
        Utilities.writeXMLDocumentToFile(document, Utilities.getAdditionalInformation(cluster));
    }

    public static UpdateItem createUpdateItem(UpdateItemImpl impl) {
        assert (Trampoline.SPI != null);
        return Trampoline.SPI.createUpdateItem(impl);
    }

    public static UpdateItemImpl getUpdateItemImpl(UpdateItem item) {
        assert (Trampoline.SPI != null);
        return Trampoline.SPI.impl(item);
    }

    public static boolean canDisable(Module m) {
        return m != null && m.isEnabled() && !Utilities.isEssentialModule((ModuleInfo)m) && !m.isAutoload() && !m.isEager();
    }

    public static boolean canEnable(Module m) {
        return m != null && !m.isEnabled() && !m.isAutoload() && !m.isEager();
    }

    public static boolean isElementInstalled(UpdateElement el) {
        assert (el != null);
        if (el == null) {
            return false;
        }
        return el.equals(el.getUpdateUnit().getInstalled());
    }

    public static boolean isKitModule(ModuleInfo mi) {
        return Main.getModuleSystem().isShowInAutoUpdateClient(mi);
    }

    public static boolean isEssentialModule(ModuleInfo mi) {
        Object o = mi.getAttribute("AutoUpdate-Essential-Module");
        return Utilities.isFixed(mi) || o != null && Boolean.parseBoolean(o.toString());
    }

    public static boolean isFirstClassModule(UpdateElement ue) {
        String codeName = ue.getCodeName();
        String names = System.getProperty("plugin.manager.first.class.modules");
        if (names == null || names.length() == 0) {
            UpdateElementImpl ueImpl = Trampoline.API.impl(ue);
            return ueImpl.isPreferredUpdate();
        }
        StringTokenizer en = new StringTokenizer(names, ",");
        while (en.hasMoreTokens()) {
            if (!en.nextToken().trim().equals(codeName)) continue;
            return true;
        }
        return false;
    }

    private static Logger getLogger() {
        return err;
    }

    public static Set<Module> findRequiredModules(Module m, ModuleManager mm, Map<Module, Set<Module>> m2reqs) {
        Set res;
        if (m2reqs != null) {
            res = m2reqs.get((Object)m);
            if (res == null) {
                res = mm.getModuleInterdependencies(m, false, false, true);
                m2reqs.put(m, res);
            }
        } else {
            res = mm.getModuleInterdependencies(m, false, false, true);
        }
        return res;
    }

    public static Set<Module> findDependingModules(Module m, ModuleManager mm, Map<Module, Set<Module>> m2deps) {
        Set<Module> res;
        if (m2deps != null) {
            res = m2deps.get((Object)m);
            if (res == null) {
                res = Utilities.filterDependingOnOtherProvider(m, mm.getModuleInterdependencies(m, true, false, true));
                m2deps.put(m, res);
            }
        } else {
            res = Utilities.filterDependingOnOtherProvider(m, mm.getModuleInterdependencies(m, true, false, true));
        }
        return res;
    }

    private static Set<Module> filterDependingOnOtherProvider(Module m, Set<Module> modules) {
        HashSet<Module> alive = new HashSet<Module>();
        for (String token : m.getProvides()) {
            for (Module depM : modules) {
                for (Dependency dep : depM.getDependencies()) {
                    if (dep.getType() != 5 && dep.getType() != 6 || !token.equals(dep.getName())) continue;
                    assert (UpdateManagerImpl.getInstance().getInstalledProviders(token).contains((Object)m));
                    if (UpdateManagerImpl.getInstance().getInstalledProviders(token).size() <= 1) continue;
                    alive.add(depM);
                }
            }
        }
        modules.removeAll(alive);
        return modules;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static String formatDate(Date date) {
        SimpleDateFormat simpleDateFormat = DATE_FORMAT;
        synchronized (simpleDateFormat) {
            return DATE_FORMAT.format(date);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static Date parseDate(String date) throws ParseException {
        SimpleDateFormat simpleDateFormat = DATE_FORMAT;
        synchronized (simpleDateFormat) {
            return DATE_FORMAT.parse(date);
        }
    }

    public static boolean canWriteInCluster(File cluster) {
        assert (cluster != null);
        if (cluster == null) {
            return false;
        }
        if (cluster.exists() && cluster.isDirectory()) {
            File update = new File(cluster, "update");
            File download = new File(cluster, DOWNLOAD_DIR);
            File dir4test = download.exists() ? download : (update.exists() ? update : cluster);
            if (dir4test.canWrite() && dir4test.canRead()) {
                boolean canWrite = Utilities.canWrite(dir4test);
                Utilities.getLogger().log(Level.FINE, "Can write into {0}? {1}", new Object[]{dir4test, canWrite});
                return canWrite;
            }
            Utilities.getLogger().log(Level.FINE, "Can write into {0}? {1}", new Object[]{dir4test, dir4test.canWrite()});
            return dir4test.canWrite();
        }
        cluster.mkdirs();
        Utilities.getLogger().log(Level.FINE, "Can write into new cluster {0}? {1}", new Object[]{cluster, cluster.canWrite()});
        return cluster.canWrite();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static boolean canWrite(File f) {
        if (org.openide.util.Utilities.isWindows()) {
            if (f.isFile()) {
                FileWriter fw = null;
                try {
                    fw = new FileWriter(f, true);
                    Utilities.getLogger().log(Level.FINE, "{0} has write permission", f);
                }
                catch (IOException ioe) {
                    Utilities.getLogger().log(Level.FINE, f + " has no write permission", ioe);
                    boolean bl = false;
                    return bl;
                }
                finally {
                    try {
                        if (fw != null) {
                            fw.close();
                        }
                    }
                    catch (IOException ex) {
                        Utilities.getLogger().log(Level.INFO, ex.getLocalizedMessage(), ex);
                    }
                }
                return true;
            }
            try {
                File dummy = File.createTempFile("dummy", null, f);
                dummy.delete();
                Utilities.getLogger().log(Level.FINE, "{0} has write permission", f);
            }
            catch (IOException ioe) {
                Utilities.getLogger().log(Level.FINE, f + " has no write permission", ioe);
                return false;
            }
            return true;
        }
        return f.canWrite();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static KeyStore loadKeyStore() {
        String fileName = Utilities.getPreferences().get("userKS", null);
        if (fileName == null) {
            return null;
        }
        InputStream is = null;
        KeyStore ks = null;
        try {
            File f = new File(Utilities.getCacheDirectory(), fileName);
            if (!f.exists()) {
                KeyStore keyStore = null;
                return keyStore;
            }
            is = new BufferedInputStream(new FileInputStream(f));
            ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(is, "open4user".toCharArray());
        }
        catch (IOException ex) {
            Utilities.getLogger().log(Level.INFO, ex.getLocalizedMessage(), ex);
        }
        catch (NoSuchAlgorithmException ex) {
            Utilities.getLogger().log(Level.INFO, ex.getLocalizedMessage(), ex);
        }
        catch (CertificateException ex) {
            Utilities.getLogger().log(Level.INFO, ex.getLocalizedMessage(), ex);
        }
        catch (KeyStoreException ex) {
            Utilities.getLogger().log(Level.INFO, ex.getLocalizedMessage(), ex);
        }
        finally {
            try {
                if (is != null) {
                    is.close();
                }
            }
            catch (IOException ex) {
                Utilities.getLogger().log(Level.INFO, ex.getLocalizedMessage(), ex);
            }
        }
        return ks;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void storeKeyStore(KeyStore ks) {
        OutputStream os = null;
        try {
            File f = new File(Utilities.getCacheDirectory(), "user.ks");
            os = new BufferedOutputStream(new FileOutputStream(f));
            ks.store(os, "open4user".toCharArray());
            Utilities.getPreferences().put("userKS", "user.ks");
        }
        catch (KeyStoreException ex) {
            Utilities.getLogger().log(Level.INFO, ex.getLocalizedMessage(), ex);
        }
        catch (IOException ex) {
            Utilities.getLogger().log(Level.INFO, ex.getLocalizedMessage(), ex);
        }
        catch (NoSuchAlgorithmException ex) {
            Utilities.getLogger().log(Level.INFO, ex.getLocalizedMessage(), ex);
        }
        catch (CertificateException ex) {
            Utilities.getLogger().log(Level.INFO, ex.getLocalizedMessage(), ex);
        }
        finally {
            try {
                if (os != null) {
                    os.close();
                }
            }
            catch (IOException ex) {
                Utilities.getLogger().log(Level.INFO, ex.getLocalizedMessage(), ex);
            }
        }
    }

    public static void addCertificates(Collection<Certificate> certs) {
        KeyStore ks = Utilities.loadKeyStore();
        if (ks == null) {
            try {
                ks = KeyStore.getInstance(KeyStore.getDefaultType());
                ks.load(null, "open4user".toCharArray());
            }
            catch (IOException ex) {
                Utilities.getLogger().log(Level.INFO, ex.getLocalizedMessage(), ex);
                return;
            }
            catch (NoSuchAlgorithmException ex) {
                Utilities.getLogger().log(Level.INFO, ex.getLocalizedMessage(), ex);
                return;
            }
            catch (CertificateException ex) {
                Utilities.getLogger().log(Level.INFO, ex.getLocalizedMessage(), ex);
                return;
            }
            catch (KeyStoreException ex) {
                Utilities.getLogger().log(Level.INFO, ex.getLocalizedMessage(), ex);
                return;
            }
        }
        for (Certificate c : certs) {
            try {
                if (ks.getCertificateAlias(c) != null) continue;
                String alias = null;
                for (int i = 0; i < 9999 && ks.containsAlias(alias = "genAlias" + i); ++i) {
                }
                if (alias == null) {
                    Utilities.getLogger().log(Level.INFO, "Too many certificates with {0}", c);
                }
                ks.setCertificateEntry(alias, c);
            }
            catch (KeyStoreException ex) {
                Utilities.getLogger().log(Level.INFO, ex.getLocalizedMessage(), ex);
            }
        }
        Utilities.storeKeyStore(ks);
    }

    public static void writeFirstClassModule(String moduleCodeName) {
        if (moduleCodeName == null) {
            Utilities.getPreferences().put("plugin.manager.first.class.modules", "");
            return;
        }
        String names = Utilities.getPreferences().get("plugin.manager.first.class.modules", "");
        names = names.isEmpty() ? moduleCodeName : names + "," + moduleCodeName;
        Utilities.getPreferences().put("plugin.manager.first.class.modules", names);
    }

    private static File getCacheDirectory() {
        return Places.getCacheSubdirectory((String)"catalogcache");
    }

    private static Preferences getPreferences() {
        return NbPreferences.root().node("/org/netbeans/modules/autoupdate");
    }

    static {
        err = Logger.getLogger(Utilities.class.getName());
        cachedInfo2RequestedReference = null;
        cachedInfosReference = null;
        cachedResultReference = null;
        productVersion = null;
    }

    private static class KeyStoreProviderListener
    implements LookupListener {
        private KeyStoreProviderListener() {
        }

        public void resultChanged(LookupEvent ev) {
            result = null;
        }
    }

}

