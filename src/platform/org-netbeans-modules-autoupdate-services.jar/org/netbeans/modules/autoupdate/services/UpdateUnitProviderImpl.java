/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 */
package org.netbeans.modules.autoupdate.services;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import org.netbeans.api.autoupdate.UpdateManager;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.api.autoupdate.UpdateUnitProvider;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.modules.autoupdate.services.AutoupdateSettings;
import org.netbeans.modules.autoupdate.services.Trampoline;
import org.netbeans.modules.autoupdate.services.UpdateManagerImpl;
import org.netbeans.modules.autoupdate.updateprovider.AutoupdateCatalogFactory;
import org.netbeans.modules.autoupdate.updateprovider.AutoupdateCatalogProvider;
import org.netbeans.modules.autoupdate.updateprovider.LocalNBMsProvider;
import org.netbeans.modules.autoupdate.updateprovider.ProviderCategory;
import org.netbeans.spi.autoupdate.UpdateProvider;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

public final class UpdateUnitProviderImpl {
    private UpdateProvider provider;
    private static final Logger err = Logger.getLogger("org.netbeans.modules.autoupdate.services.UpdateUnitProviderImpl");
    private static final String REMOVED_MASK = "_removed";
    private static final String URL = "url";
    private static final String DISPLAY_NAME = "displayName";
    private static final String ENABLED = "enabled";
    private static final String CATEGORY_NAME = "categoryName";
    private static final LookupListenerImpl UPDATE_PROVIDERS = new LookupListenerImpl();

    public UpdateUnitProviderImpl(UpdateProvider provider) {
        this.provider = provider;
    }

    public String getName() {
        return this.getUpdateProvider().getName();
    }

    public String getDescription() {
        return this.getUpdateProvider().getDescription();
    }

    public UpdateUnitProvider.CATEGORY getCategory() {
        return this.getUpdateProvider().getCategory();
    }

    public Image getSourceIcon() {
        UpdateProvider up = this.getUpdateProvider();
        if (up instanceof AutoupdateCatalogProvider) {
            return ((AutoupdateCatalogProvider)up).getProviderCategory().getIcon();
        }
        return ProviderCategory.forValue(UpdateUnitProvider.CATEGORY.COMMUNITY).getIcon();
    }

    public String getSourceDescription() {
        UpdateProvider up = this.getUpdateProvider();
        if (up instanceof AutoupdateCatalogProvider) {
            return ((AutoupdateCatalogProvider)up).getProviderCategory().getDisplayName();
        }
        return ProviderCategory.forValue(UpdateUnitProvider.CATEGORY.COMMUNITY).getDisplayName();
    }

    public String getContentDescription() {
        UpdateProvider up = this.getUpdateProvider();
        if (up instanceof AutoupdateCatalogProvider) {
            return ((AutoupdateCatalogProvider)up).getContentDescription();
        }
        return null;
    }

    public String getDisplayName() {
        return UpdateUnitProviderImpl.loadDisplayName(this.getUpdateProvider());
    }

    public void setDisplayName(String name) {
        UpdateUnitProviderImpl.storeDisplayName(this.getUpdateProvider(), name);
    }

    public URL getProviderURL() {
        return UpdateUnitProviderImpl.loadUrl(this.getUpdateProvider());
    }

    public void setProviderURL(URL url) {
        UpdateUnitProviderImpl.storeUrl(this.getUpdateProvider(), url);
    }

    public /* varargs */ List<UpdateUnit> getUpdateUnits(UpdateManager.TYPE ... types) {
        return UpdateManagerImpl.getUpdateUnits(this.getUpdateProvider(), types);
    }

    public boolean refresh(ProgressHandle handle, boolean force) throws IOException {
        boolean res = false;
        if (handle != null) {
            handle.progress(NbBundle.getMessage(UpdateUnitProviderImpl.class, (String)"UpdateUnitProviderImpl_FormatCheckingForUpdates", (Object)this.getDisplayName()));
        }
        UpdateProvider updateProvider = this.getUpdateProvider();
        updateProvider.refresh(force);
        if (force) {
            AutoupdateSettings.setLastCheck(new Date());
            AutoupdateSettings.setLastCheck(updateProvider.getName(), new Date());
        }
        UpdateManagerImpl.getInstance().clearCache();
        return res;
    }

    public void setEnable(boolean state) {
        UpdateManagerImpl.getInstance().clearCache();
        UpdateUnitProviderImpl.storeState(this.getUpdateProvider(), state);
    }

    public boolean isEnabled() {
        return UpdateUnitProviderImpl.loadState(this.getUpdateProvider().getName());
    }

    public UpdateProvider getUpdateProvider() {
        assert (this.provider != null);
        return this.provider;
    }

    public static void remove(UpdateUnitProvider unitProvider) {
        UpdateUnitProviderImpl impl = Trampoline.API.impl(unitProvider);
        impl.remove();
    }

    private void remove() {
        try {
            if (UpdateUnitProviderImpl.getPreferences().nodeExists(this.getName())) {
                UpdateUnitProviderImpl.getPreferences().node(this.getName()).removeNode();
                UpdateUnitProviderImpl.getPreferences().node(this.getName() + "_removed").putBoolean("_removed", true);
            } else {
                UpdateUnitProviderImpl.getPreferences().node(this.getName() + "_removed").putBoolean("_removed", true);
            }
        }
        catch (BackingStoreException bsx) {
            Exceptions.printStackTrace((Throwable)bsx);
        }
        UpdateManagerImpl.getInstance().clearCache();
    }

    public static UpdateUnitProvider createUpdateUnitProvider(String codeName, String displayName, URL url) {
        return UpdateUnitProviderImpl.createUpdateUnitProvider(codeName, displayName, url, ProviderCategory.forValue(UpdateUnitProvider.CATEGORY.COMMUNITY));
    }

    public static UpdateUnitProvider createUpdateUnitProvider(String codeName, String displayName, URL url, ProviderCategory category) {
        codeName = UpdateUnitProviderImpl.normalizeCodeName(codeName);
        UpdateUnitProviderImpl.storeProvider(codeName, displayName, url, category);
        AutoupdateCatalogProvider catalog = new AutoupdateCatalogProvider(codeName, displayName, url, category);
        return Trampoline.API.createUpdateUnitProvider(new UpdateUnitProviderImpl(catalog));
    }

    public static /* varargs */ UpdateUnitProvider createUpdateUnitProvider(String name, File ... files) {
        name = UpdateUnitProviderImpl.normalizeCodeName(name);
        LocalNBMsProvider provider = new LocalNBMsProvider(name, files);
        return Trampoline.API.createUpdateUnitProvider(new UpdateUnitProviderImpl(provider));
    }

    public static List<UpdateUnitProvider> getUpdateUnitProviders(boolean onlyEnabled) {
        FileObject auTypeFolder = FileUtil.getConfigFile((String)"Services/AutoupdateType");
        if (auTypeFolder != null) {
            for (FileObject auType : auTypeFolder.getChildren()) {
                try {
                    if (UpdateUnitProviderImpl.getPreferences().nodeExists(auType.getName()) || UpdateUnitProviderImpl.getPreferences().nodeExists(auType.getName() + "_removed")) continue;
                    try {
                        UpdateProvider p = AutoupdateCatalogFactory.createUpdateProvider(auType);
                        if (p != null) {
                            UpdateUnitProviderImpl.getPreferences().node(auType.getName()).putBoolean("loaded", true);
                            err.log(Level.FINEST, "{0} loaded", (Object)auType);
                            continue;
                        }
                        err.log(Level.INFO, "{0} cannot be loaded (missing url or url_key)", (Object)auType);
                        UpdateUnitProviderImpl.getPreferences().node(auType.getName()).putBoolean("loaded", false);
                    }
                    catch (Exception x) {
                        Exceptions.printStackTrace((Throwable)x);
                    }
                    continue;
                }
                catch (BackingStoreException bse) {
                    err.log(Level.INFO, bse.getMessage() + " while loading " + (Object)auType, bse);
                }
            }
        }
        Collection<? extends UpdateProvider> col = UPDATE_PROVIDERS.allInstances();
        HashMap<String, UpdateProvider> providerMap = new HashMap<String, UpdateProvider>();
        for (UpdateProvider provider : col) {
            try {
                if (UpdateUnitProviderImpl.getPreferences().nodeExists(provider.getName() + "_removed")) {
                    continue;
                }
            }
            catch (BackingStoreException bsx) {
                Exceptions.printStackTrace((Throwable)bsx);
            }
            providerMap.put(provider.getName(), provider);
        }
        try {
            Preferences p = UpdateUnitProviderImpl.getPreferences();
            String[] children = p.childrenNames();
            if (children != null) {
                for (int i = 0; i < children.length; ++i) {
                    UpdateProvider provider2 = UpdateUnitProviderImpl.loadProvider(children[i]);
                    if (err.isLoggable(Level.FINE) && provider2 != null && providerMap.containsKey(provider2.getName())) {
                        err.log(Level.FINE, "Customized Provider " + provider2.getName());
                    }
                    if (provider2 != null) {
                        providerMap.put(provider2.getName(), provider2);
                    }
                    boolean enabled = UpdateUnitProviderImpl.loadState(children[i]);
                    if (!onlyEnabled || enabled) continue;
                    providerMap.remove(children[i]);
                }
            }
        }
        catch (BackingStoreException bse) {
            err.log(Level.INFO, bse.getMessage(), bse);
        }
        ArrayList<UpdateUnitProvider> unitProviders = new ArrayList<UpdateUnitProvider>(providerMap.values().size());
        for (UpdateProvider p : providerMap.values()) {
            UpdateUnitProviderImpl impl = new UpdateUnitProviderImpl(p);
            unitProviders.add(Trampoline.API.createUpdateUnitProvider(impl));
        }
        return unitProviders;
    }

    public static void refreshProviders(ProgressHandle handle, boolean force) throws IOException {
        List<UpdateUnitProvider> providers = UpdateUnitProviderImpl.getUpdateUnitProviders(true);
        for (UpdateUnitProvider p : providers) {
            p.refresh(handle, force);
            AutoupdateSettings.setLastCheck(p.getName(), new Date());
        }
        if (force) {
            AutoupdateSettings.setLastCheck(new Date());
        }
        UpdateManagerImpl.getInstance().clearCache();
    }

    private static void storeProvider(String codeName, String displayName, URL url, ProviderCategory c) {
        if (codeName.contains("/")) {
            codeName = codeName.replaceAll("/", "_");
        }
        Preferences providerPreferences = UpdateUnitProviderImpl.getPreferences().node(codeName);
        assert (providerPreferences != null);
        providerPreferences.put("url", url.toString());
        providerPreferences.put("displayName", displayName);
        providerPreferences.put("categoryName", c.getDisplayName());
        providerPreferences.put("originalCategoryIconBase", c.getIconBase());
    }

    private static Preferences getPreferences() {
        return NbPreferences.root().node("/org/netbeans/modules/autoupdate");
    }

    static UpdateProvider loadProvider(String codeName) {
        String displayName;
        ProviderCategory pc;
        URL url;
        block9 : {
            UpdateUnitProvider.CATEGORY c;
            try {
                if (UpdateUnitProviderImpl.getPreferences().nodeExists(codeName + "_removed")) {
                    return null;
                }
            }
            catch (BackingStoreException e) {
                err.log(Level.INFO, e.getMessage(), e);
            }
            Preferences providerPreferences = UpdateUnitProviderImpl.getPreferences().node(codeName);
            assert (providerPreferences != null);
            String toUrl = providerPreferences.get("url", providerPreferences.get("originalUrl", null));
            displayName = providerPreferences.get("displayName", providerPreferences.get("originalDisplayName", codeName));
            String categoryName = providerPreferences.get("categoryName", providerPreferences.get("originalCategoryName", UpdateUnitProvider.CATEGORY.COMMUNITY.name()));
            try {
                c = UpdateUnitProvider.CATEGORY.valueOf(categoryName);
            }
            catch (IllegalArgumentException ex) {
                c = null;
            }
            String categoryIconBase = providerPreferences.get("originalCategoryIconBase", null);
            pc = c != null ? ProviderCategory.forValue(c) : ProviderCategory.create(categoryIconBase, categoryName);
            if (toUrl == null) {
                return null;
            }
            url = null;
            try {
                url = new URL(toUrl);
            }
            catch (MalformedURLException mue) {
                if ($assertionsDisabled) break block9;
                throw new AssertionError(mue);
            }
        }
        return new AutoupdateCatalogProvider(codeName, displayName, url, pc);
    }

    private static boolean loadState(String codename) {
        Preferences providerPreferences = UpdateUnitProviderImpl.getPreferences().node(codename);
        assert (providerPreferences != null);
        String enabled = providerPreferences.get("enabled", providerPreferences.get("originalEnabled", null));
        return !Boolean.FALSE.toString().equals(enabled);
    }

    private static void storeState(UpdateProvider p, boolean isEnabled) {
        Preferences providerPreferences = UpdateUnitProviderImpl.getPreferences().node(p.getName());
        assert (providerPreferences != null);
        providerPreferences.put("enabled", Boolean.valueOf(isEnabled).toString());
        try {
            if (UpdateUnitProviderImpl.getPreferences().nodeExists(p.getName() + "_removed")) {
                UpdateUnitProviderImpl.getPreferences().node(p.getName() + "_removed").removeNode();
            }
        }
        catch (BackingStoreException x) {
            err.log(Level.INFO, x.getMessage(), x);
        }
    }

    private static String loadDisplayName(UpdateProvider p) {
        Preferences providerPreferences = UpdateUnitProviderImpl.getPreferences().node(p.getName());
        assert (providerPreferences != null);
        return providerPreferences.get("displayName", p.getDisplayName());
    }

    private static void storeDisplayName(UpdateProvider p, String displayName) {
        Preferences providerPreferences = UpdateUnitProviderImpl.getPreferences().node(p.getName());
        assert (providerPreferences != null);
        if (displayName == null) {
            providerPreferences.remove("displayName");
        } else if (!displayName.equals(p.getDisplayName())) {
            providerPreferences.put("displayName", displayName);
        }
    }

    private static URL loadUrl(UpdateProvider p) {
        Preferences providerPreferences = UpdateUnitProviderImpl.getPreferences().node(p.getName());
        assert (providerPreferences != null);
        String urlSpec = null;
        if (p instanceof AutoupdateCatalogProvider) {
            urlSpec = ((AutoupdateCatalogProvider)p).getUpdateCenterURL().toExternalForm();
        }
        if ((urlSpec = providerPreferences.get("url", urlSpec)) == null || urlSpec.length() == 0) {
            return null;
        }
        int idx = urlSpec.indexOf("?unique");
        if (idx != -1) {
            urlSpec = urlSpec.substring(0, idx);
        }
        err.log(Level.FINE, "Use urlSpec " + urlSpec + " for Provider " + p);
        URL url = null;
        try {
            url = new URL(urlSpec);
        }
        catch (MalformedURLException x) {
            err.log(Level.INFO, x.getMessage(), x);
        }
        return url;
    }

    private static void storeUrl(UpdateProvider p, URL url) {
        Preferences providerPreferences = UpdateUnitProviderImpl.getPreferences().node(p.getName());
        assert (providerPreferences != null);
        if (url == null) {
            providerPreferences.remove("url");
        } else {
            URL orig = null;
            if (p instanceof AutoupdateCatalogProvider) {
                orig = ((AutoupdateCatalogProvider)p).getUpdateCenterURL();
            }
            if (!url.toExternalForm().equals(orig.toExternalForm())) {
                providerPreferences.put("url", url.toExternalForm());
                if (p instanceof AutoupdateCatalogProvider) {
                    ((AutoupdateCatalogProvider)p).setUpdateCenterURL(url);
                }
            }
        }
    }

    private static String normalizeCodeName(String codeName) {
        List<Character> illegalChars = Arrays.asList(Character.valueOf('\"'), Character.valueOf('*'), Character.valueOf('/'), Character.valueOf(':'), Character.valueOf('<'), Character.valueOf('>'), Character.valueOf('?'), Character.valueOf('\\'), Character.valueOf('|'));
        StringBuilder buf = new StringBuilder();
        for (char ch : codeName.toCharArray()) {
            if (illegalChars.contains(Character.valueOf(ch))) {
                ch = '_';
            }
            buf.append(ch);
        }
        return buf.toString();
    }

    public String toString() {
        return this.provider.toString();
    }

    private static class LookupListenerImpl
    implements LookupListener {
        final Lookup.Result<UpdateProvider> result = Lookup.getDefault().lookupResult(UpdateProvider.class);

        public LookupListenerImpl() {
            this.result.addLookupListener((LookupListener)this);
        }

        public void resultChanged(LookupEvent ev) {
            err.log(Level.FINE, "Lookup.Result changed " + (Object)ev);
            try {
                UpdateUnitProviderImpl.refreshProviders(null, false);
            }
            catch (IOException ioe) {
                err.log(Level.INFO, ioe.getMessage(), ioe);
            }
        }

        public Collection<? extends UpdateProvider> allInstances() {
            Collection res = this.result.allInstances();
            return res;
        }
    }

}

