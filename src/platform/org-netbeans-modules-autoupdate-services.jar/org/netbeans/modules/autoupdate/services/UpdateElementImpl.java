/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.SpecificationVersion
 */
package org.netbeans.modules.autoupdate.services;

import java.util.List;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateManager;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.modules.autoupdate.updateprovider.InstallInfo;
import org.netbeans.modules.autoupdate.updateprovider.UpdateItemImpl;
import org.openide.modules.ModuleInfo;
import org.openide.modules.SpecificationVersion;

public abstract class UpdateElementImpl {
    private UpdateUnit unit;
    private UpdateElement element;

    public UpdateElementImpl(UpdateItemImpl item, String providerName) {
    }

    public UpdateUnit getUpdateUnit() {
        return this.unit;
    }

    public void setUpdateUnit(UpdateUnit unit) {
        assert (unit != null);
        this.unit = unit;
    }

    public UpdateElement getUpdateElement() {
        return this.element;
    }

    public void setUpdateElement(UpdateElement element) {
        assert (element != null);
        this.element = element;
    }

    public abstract String getCodeName();

    public abstract String getDisplayName();

    public abstract SpecificationVersion getSpecificationVersion();

    public abstract String getDescription();

    public abstract String getNotification();

    public abstract String getAuthor();

    public abstract String getHomepage();

    public abstract int getDownloadSize();

    public abstract String getSource();

    public abstract String getDate();

    public abstract String getCategory();

    public abstract boolean isEnabled();

    public abstract String getLicence();

    public abstract String getLicenseId();

    public abstract UpdateManager.TYPE getType();

    public abstract boolean isAutoload();

    public abstract boolean isEager();

    public abstract boolean isFixed();

    public abstract boolean isPreferredUpdate();

    public abstract List<ModuleInfo> getModuleInfos();

    public List<ModuleInfo> getModuleInfos(boolean recursive) {
        return this.getModuleInfos();
    }

    public abstract InstallInfo getInstallInfo();
}

