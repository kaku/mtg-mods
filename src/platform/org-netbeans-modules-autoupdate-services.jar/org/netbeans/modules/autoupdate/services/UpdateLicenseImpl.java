/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.autoupdate.services;

import java.net.URL;
import org.netbeans.modules.autoupdate.updateprovider.AutoupdateCatalogCache;

public final class UpdateLicenseImpl {
    private String name;
    private URL url;

    public UpdateLicenseImpl(String licenseName, String agreement) {
        this.name = licenseName;
        this.setAgreement(agreement);
    }

    public UpdateLicenseImpl(String licenseName, String agreement, URL url) {
        this.name = licenseName;
        this.url = url;
        this.setAgreement(agreement);
    }

    public String getName() {
        return this.name;
    }

    public URL getURL() {
        return this.url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public String getAgreement() {
        return this.name == null ? null : AutoupdateCatalogCache.getDefault().getLicense(this.name, this.url);
    }

    public void setAgreement(String content) {
        if (content != null && this.name != null) {
            AutoupdateCatalogCache.getDefault().storeLicense(this.name, content);
        }
    }
}

