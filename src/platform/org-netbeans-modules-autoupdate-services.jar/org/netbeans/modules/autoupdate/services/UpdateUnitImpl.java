/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.SpecificationVersion
 */
package org.netbeans.modules.autoupdate.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateManager;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.openide.modules.SpecificationVersion;

public abstract class UpdateUnitImpl {
    private String codeName;
    private UpdateElement installed;
    private List<UpdateElement> updates;
    private UpdateElement installedLocalization;
    private List<UpdateElement> localizationUpdates;
    private UpdateElement backup;
    private UpdateUnit updateUnit;
    private static final Logger err = Logger.getLogger(UpdateUnitImpl.class.getName());

    public UpdateUnitImpl(String codename) {
        this.codeName = codename;
    }

    public void setUpdateUnit(UpdateUnit unit) {
        assert (unit != null);
        this.updateUnit = unit;
    }

    public UpdateUnit getUpdateUnit() {
        return this.updateUnit;
    }

    public String getCodeName() {
        return this.codeName;
    }

    public UpdateElement getInstalled() {
        return this.installed;
    }

    public List<UpdateElement> getAvailableUpdates() {
        return this.identifyUpdates(this.getInstalled(), this.updates);
    }

    public UpdateElement getInstalledLocalization() {
        return this.installedLocalization;
    }

    public List<UpdateElement> getAvailableLocalizations() {
        return this.identifyLocalizationUpdates(this.installedLocalization, this.localizationUpdates);
    }

    public UpdateElement getBackup() {
        return this.backup;
    }

    public void addUpdate(UpdateElement update) {
        int idx;
        if (this.updates == null) {
            this.updates = new ArrayList<UpdateElement>();
        }
        if ((idx = this.updates.indexOf(update)) != -1) {
            this.updates.remove(update);
        }
        this.updates.add(update);
    }

    public void setInstalled(UpdateElement installed) {
        assert (this.installed == null);
        assert (installed != null);
        this.installed = installed;
    }

    public void setAsUninstalled() {
        assert (this.installed != null);
        this.installed = null;
    }

    public void updateInstalled(UpdateElement installed) {
        this.installed = null;
        this.setInstalled(installed);
    }

    public void addLocalizationUpdate(UpdateElement update) {
        if (this.localizationUpdates == null) {
            this.localizationUpdates = new ArrayList<UpdateElement>();
        }
        assert (!this.localizationUpdates.contains(update));
        this.localizationUpdates.add(update);
    }

    public void setInstalledLocalization(UpdateElement installed) {
        assert (this.installedLocalization == null);
        this.installedLocalization = installed;
    }

    public void setBackup(UpdateElement backup) {
        assert (this.backup == null);
        this.backup = backup;
    }

    public boolean isPending() {
        return false;
    }

    public abstract UpdateManager.TYPE getType();

    protected List<UpdateElement> getUpdates() {
        return this.updates;
    }

    public UpdateElement findUpdateSameAsInstalled() {
        if (this.updates != null && !this.updates.isEmpty()) {
            if (this.installed == null) {
                return null;
            }
            for (UpdateElement update : this.updates) {
                String uspec = update.getSpecificationVersion();
                String ispec = this.installed.getSpecificationVersion();
                if (uspec == null && ispec == null) {
                    err.log(Level.FINE, "Installed UpdateElement w/o version " + this.installed.getCodeName() + "[" + ispec + "] has update w/o version " + update.getCodeName() + "[" + uspec + "] too");
                    return update;
                }
                if (uspec == null || ispec == null || new SpecificationVersion(uspec).compareTo((Object)new SpecificationVersion(ispec)) != 0) continue;
                err.log(Level.FINE, "Installed UpdateElement " + this.installed.getCodeName() + "[" + ispec + "] has update with the same version " + update.getCodeName() + "[" + uspec + "]");
                return update;
            }
            return null;
        }
        return null;
    }

    public abstract UpdateUnit getVisibleAncestor();

    private List<UpdateElement> identifyUpdates(UpdateElement installed, List<UpdateElement> updates) {
        List<UpdateElement> res = null;
        if (updates != null && !updates.isEmpty()) {
            if (installed == null) {
                if (updates != null) {
                    res = updates;
                }
            } else {
                String moduleId = installed.getCodeName();
                ArrayList<UpdateElement> realUpdates = new ArrayList<UpdateElement>();
                for (UpdateElement update : updates) {
                    String uspec = update.getSpecificationVersion();
                    String ispec = installed.getSpecificationVersion();
                    if (uspec != null && ispec == null) {
                        err.log(Level.FINE, "UpdateElement " + moduleId + "[" + ispec + "] has update " + moduleId + "[" + uspec + "]");
                        realUpdates.add(update);
                        continue;
                    }
                    if (uspec == null || ispec == null || new SpecificationVersion(uspec).compareTo((Object)new SpecificationVersion(ispec)) <= 0) continue;
                    err.log(Level.FINE, "UpdateElement " + moduleId + "[" + ispec + "] has update " + moduleId + "[" + uspec + "]");
                    realUpdates.add(update);
                }
                if (!realUpdates.isEmpty()) {
                    res = realUpdates;
                }
            }
        }
        if (res == null) {
            res = Collections.emptyList();
        } else if (res.size() > 1) {
            Collections.sort(res, new Comparator<UpdateElement>(){

                @Override
                public int compare(UpdateElement o1, UpdateElement o2) {
                    String sv1 = o1.getSpecificationVersion();
                    String sv2 = o2.getSpecificationVersion();
                    if (sv1 == null) {
                        if (sv2 == null) {
                            return 0;
                        }
                        return -1;
                    }
                    if (sv2 == null) {
                        return 1;
                    }
                    return new SpecificationVersion(sv2).compareTo((Object)new SpecificationVersion(sv1));
                }
            });
        }
        return res;
    }

    private List<UpdateElement> identifyLocalizationUpdates(UpdateElement installed, List<UpdateElement> updates) {
        ArrayList<UpdateElement> res = null;
        if (updates != null && !updates.isEmpty()) {
            if (installed == null) {
                return updates;
            }
            String moduleId = installed.getCodeName();
            ArrayList<UpdateElement> realUpdates = new ArrayList<UpdateElement>();
            for (UpdateElement update : updates) {
                if (update.getSpecificationVersion().compareTo(installed.getSpecificationVersion()) <= 0) continue;
                err.log(Level.FINE, "Module " + moduleId + "[" + installed.getSpecificationVersion() + "] has update " + moduleId + "[" + update.getSpecificationVersion() + "]");
                realUpdates.add(update);
            }
            if (!realUpdates.isEmpty()) {
                res = realUpdates;
            }
        }
        return res;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        UpdateUnitImpl other = (UpdateUnitImpl)obj;
        if (!(this.codeName == other.codeName || this.codeName != null && this.codeName.equals(other.codeName))) {
            return false;
        }
        if (!(this.installed == other.installed || this.installed != null && this.installed.equals(other.installed))) {
            return false;
        }
        if (!(this.updates == other.updates || this.updates != null && this.updates.equals(other.updates))) {
            return false;
        }
        if (!(this.installedLocalization == other.installedLocalization || this.installedLocalization != null && this.installedLocalization.equals(other.installedLocalization))) {
            return false;
        }
        if (!(this.localizationUpdates == other.localizationUpdates || this.localizationUpdates != null && this.localizationUpdates.equals(other.localizationUpdates))) {
            return false;
        }
        if (!(this.backup == other.backup || this.backup != null && this.backup.equals(other.backup))) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (this.codeName != null ? this.codeName.hashCode() : 0);
        hash = 53 * hash + (this.installed != null ? this.installed.hashCode() : 0);
        hash = 53 * hash + (this.updates != null ? this.updates.hashCode() : 0);
        hash = 53 * hash + (this.installedLocalization != null ? this.installedLocalization.hashCode() : 0);
        hash = 53 * hash + (this.localizationUpdates != null ? this.localizationUpdates.hashCode() : 0);
        hash = 53 * hash + (this.backup != null ? this.backup.hashCode() : 0);
        return hash;
    }

}

