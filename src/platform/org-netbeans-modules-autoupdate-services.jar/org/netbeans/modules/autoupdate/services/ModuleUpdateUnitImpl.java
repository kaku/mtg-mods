/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.Module
 *  org.netbeans.ModuleManager
 *  org.openide.modules.ModuleInfo
 */
package org.netbeans.modules.autoupdate.services;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.netbeans.Module;
import org.netbeans.ModuleManager;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateManager;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.modules.autoupdate.services.Bundle;
import org.netbeans.modules.autoupdate.services.ModuleUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.Trampoline;
import org.netbeans.modules.autoupdate.services.UpdateElementImpl;
import org.netbeans.modules.autoupdate.services.UpdateManagerImpl;
import org.netbeans.modules.autoupdate.services.UpdateUnitFactory;
import org.netbeans.modules.autoupdate.services.UpdateUnitImpl;
import org.netbeans.modules.autoupdate.services.Utilities;
import org.openide.modules.ModuleInfo;

public class ModuleUpdateUnitImpl
extends UpdateUnitImpl {
    private UpdateUnit visibleAncestor;
    private static String BROAD_CATEGORY = Bundle.broad_category();

    public ModuleUpdateUnitImpl(String codename) {
        super(codename);
    }

    @Override
    public UpdateManager.TYPE getType() {
        return UpdateManager.TYPE.MODULE;
    }

    @Override
    public boolean isPending() {
        return UpdateUnitFactory.getDefault().isScheduledForRestart(this.getUpdateUnit());
    }

    @Override
    public UpdateUnit getVisibleAncestor() {
        if (this.visibleAncestor == null) {
            assert (this.getInstalled() != null);
            ModuleUpdateElementImpl installedImpl = (ModuleUpdateElementImpl)Trampoline.API.impl(this.getInstalled());
            TreeSet<Module> visible = new TreeSet<Module>(new Comparator<Module>(){

                @Override
                public int compare(Module o1, Module o2) {
                    return o1.getCodeNameBase().compareTo(o2.getCodeNameBase());
                }
            });
            HashSet<Module> seen = new HashSet<Module>();
            for (ModuleInfo mi : installedImpl.getModuleInfos()) {
                visible.addAll(ModuleUpdateUnitImpl.findVisibleAncestor(Utilities.toModule(mi), seen));
            }
            String cat = installedImpl.getCategory();
            String installationCluster = installedImpl.getInstallationCluster();
            if (BROAD_CATEGORY.contains(cat)) {
                cat = null;
            }
            UpdateUnit shot = null;
            UpdateUnit spare = null;
            UpdateUnit strike = null;
            for (Module visMod : visible) {
                this.visibleAncestor = Utilities.toUpdateUnit(visMod);
                UpdateElementImpl visibleImpl = Trampoline.API.impl(this.visibleAncestor.getInstalled());
                String visTargetCluster = null;
                String visCat = null;
                if (visibleImpl != null && visibleImpl instanceof ModuleUpdateElementImpl) {
                    visTargetCluster = ((ModuleUpdateElementImpl)visibleImpl).getInstallationCluster();
                    visCat = visibleImpl.getCategory();
                }
                if (installationCluster != null && installationCluster.equals(visTargetCluster)) {
                    spare = this.visibleAncestor;
                    continue;
                }
                if (visCat != null && visCat.equals(cat)) {
                    strike = this.visibleAncestor;
                    break;
                }
                if (shot != null) continue;
                shot = this.visibleAncestor;
            }
            UpdateUnit updateUnit = strike != null ? strike : (this.visibleAncestor = spare != null ? spare : shot);
            if (this.visibleAncestor == null && installationCluster != null) {
                for (UpdateElement visEl : UpdateManagerImpl.getInstance().getInstalledKits(installationCluster)) {
                    this.visibleAncestor = visEl.getUpdateUnit();
                    if (!installedImpl.getRawCategory().equals(visEl.getCategory())) continue;
                    this.visibleAncestor = visEl.getUpdateUnit();
                    break;
                }
            }
        }
        return this.visibleAncestor;
    }

    private static Set<Module> findVisibleAncestor(Module module, Set<Module> seen) {
        if (!seen.add(module)) {
            return Collections.EMPTY_SET;
        }
        HashSet<Module> visible = new HashSet<Module>();
        ModuleManager manager = module.getManager();
        Set moduleInterdependencies = manager.getModuleInterdependencies(module, !module.isEager(), false, true);
        for (Module m2 : moduleInterdependencies) {
            if (!m2.isEnabled() || !Utilities.isKitModule((ModuleInfo)m2)) continue;
            visible.add(m2);
        }
        if (visible.isEmpty()) {
            for (Module m2 : moduleInterdependencies) {
                if (!m2.isEnabled()) continue;
                assert (!module.equals((Object)m2));
                if (module.equals((Object)m2)) continue;
                visible.addAll(ModuleUpdateUnitImpl.findVisibleAncestor(m2, seen));
                if (visible.isEmpty()) continue;
                break;
            }
        }
        TreeSet<Module> res = new TreeSet<Module>(new Comparator<Module>(){

            @Override
            public int compare(Module o1, Module o2) {
                return o1.getCodeNameBase().compareTo(o2.getCodeNameBase());
            }
        });
        res.addAll(visible);
        return res;
    }

}

