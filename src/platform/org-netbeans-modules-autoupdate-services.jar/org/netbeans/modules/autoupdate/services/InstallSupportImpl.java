/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.Module
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.updater.UpdateTracking
 *  org.netbeans.updater.UpdaterInternal
 *  org.openide.LifecycleManager
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.SpecificationVersion
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.NbCollections
 */
package org.netbeans.modules.autoupdate.services;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import org.netbeans.Module;
import org.netbeans.api.autoupdate.InstallSupport;
import org.netbeans.api.autoupdate.OperationContainer;
import org.netbeans.api.autoupdate.OperationException;
import org.netbeans.api.autoupdate.OperationSupport;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateManager;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.modules.autoupdate.services.AutoupdateSettings;
import org.netbeans.modules.autoupdate.services.FeatureUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.InstallManager;
import org.netbeans.modules.autoupdate.services.ModuleUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.Trampoline;
import org.netbeans.modules.autoupdate.services.UpdateElementImpl;
import org.netbeans.modules.autoupdate.services.UpdateUnitFactory;
import org.netbeans.modules.autoupdate.services.UpdateUnitImpl;
import org.netbeans.modules.autoupdate.services.Utilities;
import org.netbeans.modules.autoupdate.updateprovider.InstallInfo;
import org.netbeans.modules.autoupdate.updateprovider.NetworkAccess;
import org.netbeans.spi.autoupdate.CustomInstaller;
import org.netbeans.updater.UpdateTracking;
import org.netbeans.updater.UpdaterInternal;
import org.openide.LifecycleManager;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.modules.SpecificationVersion;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.NbCollections;

public class InstallSupportImpl {
    private final InstallSupport support;
    private boolean progressRunning = false;
    private static final Logger LOG = Logger.getLogger(InstallSupportImpl.class.getName());
    private Map<UpdateElementImpl, File> element2Clusters = null;
    private final Set<File> downloadedFiles = new HashSet<File>();
    private Boolean isGlobal;
    private boolean useUserdirAsFallback;
    private int wasDownloaded = 0;
    private Future<Boolean> runningTask;
    private final Object LOCK = new Object();
    private STEP currentStep = STEP.NOTSTARTED;
    private final Collection<UpdateElementImpl> trusted = new ArrayList<UpdateElementImpl>();
    private final Collection<UpdateElementImpl> signed = new ArrayList<UpdateElementImpl>();
    private final Map<UpdateElement, Collection<Certificate>> certs = new HashMap<UpdateElement, Collection<Certificate>>();
    private List<? extends OperationContainer.OperationInfo> infos = null;
    private ExecutorService es = null;
    private Set<ModuleUpdateElementImpl> affectedModuleImpls = null;
    private Set<FeatureUpdateElementImpl> affectedFeatureImpls = null;

    public InstallSupportImpl(InstallSupport installSupport) {
        this.support = installSupport;
    }

    public boolean doDownload(final ProgressHandle progress, final Boolean isGlobal, final boolean useUserdirAsFallback) throws OperationException {
        this.isGlobal = isGlobal;
        this.useUserdirAsFallback = useUserdirAsFallback;
        if (progress != null) {
            progress.start();
            progress.progress(NbBundle.getMessage(InstallSupportImpl.class, (String)"InstallSupportImpl_Download_Estabilish"));
            this.progressRunning = false;
        }
        Callable<Boolean> downloadCallable = new Callable<Boolean>(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public Boolean call() throws Exception {
                OperationContainer<InstallSupport> container = InstallSupportImpl.this.support.getContainer();
                assert (container.listInvalid().isEmpty());
                Object object = InstallSupportImpl.this.LOCK;
                synchronized (object) {
                    InstallSupportImpl.this.currentStep = STEP.DOWNLOAD;
                }
                InstallSupportImpl.this.infos = container.listAll();
                int size = 0;
                for (OperationContainer.OperationInfo info : InstallSupportImpl.this.infos) {
                    UpdateElement ue = info.getUpdateElement();
                    InstallManager.findTargetDirectory(ue.getUpdateUnit().getInstalled(), Trampoline.API.impl(ue), isGlobal, useUserdirAsFallback);
                    size += ue.getDownloadSize();
                }
                int aggregateDownload = 0;
                try {
                    long id = System.currentTimeMillis();
                    for (OperationContainer.OperationInfo info2 : InstallSupportImpl.this.infos) {
                        if (InstallSupportImpl.this.cancelled()) {
                            LOG.log(Level.INFO, "InstallSupport.doDownload was canceled");
                            Boolean bl = false;
                            return bl;
                        }
                        int increment = InstallSupportImpl.this.doDownload(info2, progress, aggregateDownload, size, id);
                        if (increment == -1) {
                            Boolean bl = false;
                            return bl;
                        }
                        aggregateDownload += increment;
                    }
                    InstallSupportImpl.this.postDownload(id);
                }
                finally {
                    if (progress != null) {
                        progress.progress("");
                        progress.finish();
                    }
                }
                assert (size == aggregateDownload);
                InstallSupportImpl.this.wasDownloaded = aggregateDownload;
                return true;
            }
        };
        boolean retval = false;
        try {
            this.runningTask = this.getExecutionService().submit(downloadCallable);
            retval = this.runningTask.get();
        }
        catch (CancellationException ex) {
            LOG.log(Level.FINE, "InstallSupport.doDownload was cancelled", ex);
            return false;
        }
        catch (InterruptedException iex) {
            LOG.log(Level.FINE, iex.getLocalizedMessage(), iex);
            return false;
        }
        catch (ExecutionException iex) {
            if (!(iex.getCause() instanceof OperationException)) {
                Exceptions.printStackTrace((Throwable)iex);
            }
            throw (OperationException)iex.getCause();
        }
        return retval;
    }

    private void postDownload(long id) {
        for (File cluster : UpdateTracking.clusters((boolean)true)) {
            File runningDownloadDir = new File(cluster, Utilities.RUNNING_DOWNLOAD_DIR + '_' + id);
            if (!runningDownloadDir.isDirectory()) continue;
            File downloadDir = new File(cluster, Utilities.DOWNLOAD_DIR);
            downloadDir.mkdirs();
            for (String nbmName : runningDownloadDir.list()) {
                boolean res;
                File nbmSource = new File(runningDownloadDir, nbmName);
                File nbmTarget = new File(downloadDir, nbmName);
                if (nbmTarget.exists()) {
                    nbmTarget.delete();
                }
                if (res = nbmSource.renameTo(nbmTarget)) continue;
                LOG.log(Level.WARNING, "{0} didn''t move to {1}", new Object[]{nbmSource, nbmTarget});
            }
            runningDownloadDir.delete();
        }
    }

    public boolean doValidate(InstallSupport.Validator validator, final ProgressHandle progress) throws OperationException {
        assert (validator != null);
        Callable<Boolean> validationCallable = new Callable<Boolean>(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public Boolean call() throws Exception {
                Object object = InstallSupportImpl.this.LOCK;
                synchronized (object) {
                    assert (InstallSupportImpl.this.currentStep != STEP.FINISHED);
                    if (InstallSupportImpl.this.currentStep == STEP.CANCEL) {
                        return false;
                    }
                    InstallSupportImpl.this.currentStep = STEP.VALIDATION;
                }
                OperationContainer<InstallSupport> container = InstallSupportImpl.this.support.getContainer();
                assert (container.listInvalid().isEmpty());
                if (progress != null) {
                    progress.start(InstallSupportImpl.this.wasDownloaded);
                }
                int aggregateVerified = 0;
                try {
                    for (OperationContainer.OperationInfo info : InstallSupportImpl.this.infos) {
                        boolean hasCustom;
                        if (InstallSupportImpl.this.cancelled()) {
                            Boolean bl = false;
                            return bl;
                        }
                        UpdateElementImpl toUpdateImpl = Trampoline.API.impl(info.getUpdateElement());
                        boolean bl = hasCustom = toUpdateImpl.getInstallInfo().getCustomInstaller() != null;
                        if (hasCustom) {
                            assert (false);
                            continue;
                        }
                        aggregateVerified += InstallSupportImpl.this.doValidate(info, progress, aggregateVerified);
                    }
                }
                finally {
                    if (progress != null) {
                        progress.progress("");
                        progress.finish();
                    }
                }
                return true;
            }
        };
        boolean retval = false;
        try {
            this.runningTask = this.getExecutionService().submit(validationCallable);
            retval = this.runningTask.get();
        }
        catch (CancellationException ex) {
            LOG.log(Level.FINE, "InstallSupport.doValidate was cancelled", ex);
            return false;
        }
        catch (InterruptedException iex) {
            if (iex.getCause() instanceof OperationException) {
                throw (OperationException)iex.getCause();
            }
            LOG.log(Level.FINE, iex.getLocalizedMessage(), iex);
            return false;
        }
        catch (ExecutionException iex) {
            if (iex.getCause() instanceof OperationException) {
                throw (OperationException)iex.getCause();
            }
            Exceptions.printStackTrace((Throwable)iex);
        }
        return retval;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Boolean doInstall(InstallSupport.Installer installer, final ProgressHandle progress, final boolean forceInstall) throws OperationException {
        assert (installer != null);
        Callable<Boolean> installCallable = new Callable<Boolean>(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             * Enabled aggressive block sorting
             * Enabled unnecessary exception pruning
             * Enabled aggressive exception aggregation
             * Converted monitor instructions to comments
             * Lifted jumps to return sites
             */
            @Override
            public Boolean call() throws Exception {
                boolean needsRestart;
                Boolean bl;
                block49 : {
                    Object jf;
                    Object object = InstallSupportImpl.this.LOCK;
                    // MONITORENTER : object
                    assert (InstallSupportImpl.this.currentStep != STEP.FINISHED);
                    if (InstallSupportImpl.this.currentStep == STEP.CANCEL) {
                        // MONITOREXIT : object
                        return false;
                    }
                    InstallSupportImpl.this.currentStep = STEP.INSTALLATION;
                    // MONITOREXIT : object
                    assert (InstallSupportImpl.this.support.getContainer().listInvalid().isEmpty());
                    InstallSupportImpl.this.addTrustedCertificates();
                    InstallSupportImpl.this.affectedModuleImpls = new HashSet();
                    InstallSupportImpl.this.affectedFeatureImpls = new HashSet();
                    if (progress != null) {
                        progress.start();
                    }
                    block28 : for (OperationContainer.OperationInfo info : InstallSupportImpl.this.infos) {
                        UpdateElementImpl toUpdateImpl = Trampoline.API.impl(info.getUpdateElement());
                        switch (toUpdateImpl.getType()) {
                            case KIT_MODULE: 
                            case MODULE: {
                                InstallSupportImpl.this.affectedModuleImpls.add((ModuleUpdateElementImpl)toUpdateImpl);
                                continue block28;
                            }
                            case STANDALONE_MODULE: 
                            case FEATURE: {
                                InstallSupportImpl.this.affectedFeatureImpls.add((FeatureUpdateElementImpl)toUpdateImpl);
                                InstallSupportImpl.this.affectedModuleImpls.addAll(((FeatureUpdateElementImpl)toUpdateImpl).getContainedModuleElements());
                                continue block28;
                            }
                        }
                        assert (false);
                    }
                    needsRestart = false;
                    ArrayList<UpdaterInfo> updaterFiles = new ArrayList<UpdaterInfo>();
                    for (ModuleUpdateElementImpl moduleImpl : InstallSupportImpl.this.affectedModuleImpls) {
                        File dest;
                        UpdateElement installed;
                        Object object2 = InstallSupportImpl.this.LOCK;
                        // MONITORENTER : object2
                        if (InstallSupportImpl.this.currentStep == STEP.CANCEL) {
                            if (progress != null) {
                                progress.finish();
                            }
                            // MONITOREXIT : object2
                            return false;
                        }
                        // MONITOREXIT : object2
                        if (Utilities.isElementInstalled(moduleImpl.getUpdateElement())) continue;
                        installed = moduleImpl.getUpdateUnit().getInstalled();
                        File targetCluster = InstallSupportImpl.this.getTargetCluster(installed, moduleImpl, InstallSupportImpl.this.isGlobal, InstallSupportImpl.this.useUserdirAsFallback);
                        URL source = moduleImpl.getInstallInfo().getDistribution();
                        LOG.log(Level.FINE, "Source URL for " + moduleImpl.getCodeName() + " is " + source);
                        dest = InstallSupportImpl.getDestination(targetCluster, moduleImpl.getCodeName(), source, 0);
                        assert (dest != null);
                        jf = new JarFile(dest);
                        try {
                            for (JarEntry entry : Collections.list(jf.entries())) {
                                if (!"netbeans/modules/ext/updater.jar".equals(entry.toString()) && !entry.toString().matches("netbeans/modules/ext/locale/updater(_[a-zA-Z0-9]+)+")) continue;
                                LOG.log(Level.INFO, entry.toString() + " is being installed from " + moduleImpl.getCodeName());
                                updaterFiles.add(new UpdaterInfo(entry, dest, targetCluster));
                                needsRestart = true;
                            }
                        }
                        finally {
                            jf.close();
                        }
                        needsRestart |= InstallSupportImpl.this.needsRestart(installed != null, moduleImpl, dest);
                    }
                    try {
                        Object info2;
                        Utilities.writeAdditionalInformation(InstallSupportImpl.this.getElement2Clusters());
                        for (int i2 = 0; i2 < updaterFiles.size(); ++i2) {
                            info2 = (UpdaterInfo)updaterFiles.get(i2);
                            try {
                                Utilities.writeUpdateOfUpdaterJar(info2.getUpdaterJarEntry(), info2.getZipFileWithUpdater(), info2.getUpdaterTargetCluster());
                                continue;
                            }
                            catch (IOException ex) {
                                LOG.log(Level.INFO, "Cannot open or close jar file {0}", info2.getZipFileWithUpdater());
                            }
                        }
                        if (needsRestart && !forceInstall) break block49;
                        Object i2 = InstallSupportImpl.this.LOCK;
                        // MONITORENTER : i2
                        if (InstallSupportImpl.this.currentStep == STEP.CANCEL) {
                            if (progress != null) {
                                progress.finish();
                            }
                            info2 = false;
                            // MONITOREXIT : i2
                            return info2;
                        }
                        // MONITOREXIT : i2
                        if (progress != null) {
                            progress.switchToDeterminate(InstallSupportImpl.this.affectedModuleImpls.size());
                        }
                        info2 = InstallSupportImpl.this.downloadedFiles;
                        // MONITORENTER : info2
                        final HashSet files = new HashSet(InstallSupportImpl.this.downloadedFiles);
                        // MONITOREXIT : info2
                        if (!files.isEmpty()) {
                            try {
                                FileUtil.runAtomicAction((Runnable)new Runnable(){

                                    @Override
                                    public void run() {
                                        try {
                                            UpdaterInternal.update((Collection)files, (PropertyChangeListener)new RefreshModulesListener(progress), (String)NbBundle.getBranding());
                                        }
                                        catch (InterruptedException ie) {
                                            LOG.log(Level.INFO, ie.getMessage(), ie);
                                        }
                                    }
                                });
                                for (ModuleUpdateElementImpl impl : InstallSupportImpl.this.affectedModuleImpls) {
                                    int rerunWaitCount;
                                    Module module = Utilities.toModule(impl.getCodeName(), impl.getSpecificationVersion());
                                    for (rerunWaitCount = 0; rerunWaitCount < 100 && module == null; ++rerunWaitCount) {
                                        LOG.log(Level.FINE, "Waiting for {0}@{1} #{2}", new Object[]{impl.getCodeName(), impl.getSpecificationVersion(), rerunWaitCount});
                                        Thread.sleep(100);
                                        module = Utilities.toModule(impl.getCodeName(), impl.getSpecificationVersion());
                                    }
                                    if (rerunWaitCount != 100) continue;
                                    LOG.log(Level.INFO, "Timeout waiting for loading module {0}@{1}", new Object[]{impl.getCodeName(), impl.getSpecificationVersion()});
                                    InstallSupportImpl.this.afterInstall();
                                    jf = InstallSupportImpl.this.downloadedFiles;
                                    // MONITORENTER : jf
                                    InstallSupportImpl.this.downloadedFiles.clear();
                                    // MONITOREXIT : jf
                                    throw new OperationException(OperationException.ERROR_TYPE.INSTALL, NbBundle.getMessage(InstallSupportImpl.class, (String)"InstallSupportImpl_TurnOnTimeout", (Object)impl.getUpdateElement()));
                                }
                            }
                            catch (InterruptedException ie) {
                                LOG.log(Level.INFO, ie.getMessage(), ie);
                            }
                        }
                        InstallSupportImpl.this.afterInstall();
                        Set ie = InstallSupportImpl.this.downloadedFiles;
                        // MONITORENTER : ie
                        InstallSupportImpl.this.downloadedFiles.clear();
                        // MONITOREXIT : ie
                    }
                    finally {
                        if (progress != null) {
                            progress.progress("");
                            progress.finish();
                        }
                    }
                }
                if (needsRestart && !forceInstall) {
                    bl = Boolean.TRUE;
                    return bl;
                }
                bl = Boolean.FALSE;
                return bl;
            }

        };
        boolean retval = false;
        try {
            this.runningTask = this.getExecutionService().submit(installCallable);
            retval = this.runningTask.get();
        }
        catch (CancellationException ex) {
            LOG.log(Level.FINE, "InstallSupport.doInstall was cancelled", ex);
            Boolean bl = false;
            return bl;
        }
        catch (InterruptedException iex) {
            LOG.log(Level.INFO, iex.getLocalizedMessage(), iex);
        }
        catch (ExecutionException iex) {
            if (iex.getCause() instanceof OperationException) {
                throw (OperationException)iex.getCause();
            }
            LOG.log(Level.INFO, iex.getLocalizedMessage(), iex);
        }
        finally {
            if (!retval) {
                this.getElement2Clusters().clear();
            }
        }
        return retval;
    }

    private void afterInstall() {
        UpdateElement el;
        UpdateUnit u;
        if (this.affectedModuleImpls != null) {
            for (ModuleUpdateElementImpl impl : this.affectedModuleImpls) {
                u = impl.getUpdateUnit();
                el = impl.getUpdateElement();
                Trampoline.API.impl(u).updateInstalled(el);
            }
            this.affectedModuleImpls = null;
        }
        if (this.affectedFeatureImpls != null) {
            for (FeatureUpdateElementImpl impl : this.affectedFeatureImpls) {
                u = impl.getUpdateUnit();
                el = impl.getUpdateElement();
                Trampoline.API.impl(u).updateInstalled(el);
            }
            this.affectedFeatureImpls = null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void doRestart(OperationSupport.Restarter restarter, ProgressHandle progress) throws OperationException {
        Object object = this.LOCK;
        synchronized (object) {
            assert (this.currentStep != STEP.FINISHED);
            this.currentStep = STEP.RESTART;
        }
        Utilities.deleteAllDoLater();
        this.getElement2Clusters().clear();
        LifecycleManager.getDefault().exit();
        this.doRestartLater(restarter);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void doRestartLater(OperationSupport.Restarter restarter) {
        if (this.affectedModuleImpls != null) {
            for (ModuleUpdateElementImpl impl : this.affectedModuleImpls) {
                UpdateUnitFactory.getDefault().scheduleForRestart(impl.getUpdateElement());
            }
        }
        Utilities.writeInstallLater(new HashMap<UpdateElementImpl, File>(this.getElement2Clusters()));
        this.getElement2Clusters().clear();
        Set<File> i$ = this.downloadedFiles;
        synchronized (i$) {
            this.downloadedFiles.clear();
        }
    }

    public String getCertificate(InstallSupport.Installer validator, UpdateElement uElement) {
        Collection<Certificate> certificates = this.certs.get(uElement);
        if (certificates != null) {
            String res = "";
            for (Certificate c : certificates) {
                res = res + c;
            }
            return res;
        }
        return null;
    }

    public Collection<Certificate> getCertificates(UpdateElement uElement) {
        return this.certs.get(uElement);
    }

    public boolean isTrusted(InstallSupport.Installer validator, UpdateElement uElement) {
        UpdateElementImpl impl = Trampoline.API.impl(uElement);
        boolean res = false;
        switch (impl.getType()) {
            case KIT_MODULE: 
            case MODULE: {
                res = this.trusted.contains(impl);
                break;
            }
            case STANDALONE_MODULE: 
            case FEATURE: {
                FeatureUpdateElementImpl toUpdateFeatureImpl = (FeatureUpdateElementImpl)impl;
                Set<ModuleUpdateElementImpl> moduleImpls = toUpdateFeatureImpl.getContainedModuleElements();
                res = !moduleImpls.isEmpty();
                for (ModuleUpdateElementImpl moduleImpl : moduleImpls) {
                    if (Utilities.isElementInstalled(moduleImpl.getUpdateElement())) continue;
                    res &= this.trusted.contains(moduleImpl);
                }
                break;
            }
            default: {
                assert (false);
                break;
            }
        }
        return res;
    }

    public boolean isSigned(InstallSupport.Installer validator, UpdateElement uElement) {
        UpdateElementImpl impl = Trampoline.API.impl(uElement);
        boolean res = false;
        switch (impl.getType()) {
            case KIT_MODULE: 
            case MODULE: {
                res = this.signed.contains(impl);
                break;
            }
            case STANDALONE_MODULE: 
            case FEATURE: {
                FeatureUpdateElementImpl toUpdateFeatureImpl = (FeatureUpdateElementImpl)impl;
                Set<ModuleUpdateElementImpl> moduleImpls = toUpdateFeatureImpl.getContainedModuleElements();
                res = !moduleImpls.isEmpty();
                for (ModuleUpdateElementImpl moduleImpl : moduleImpls) {
                    if (Utilities.isElementInstalled(moduleImpl.getUpdateElement())) continue;
                    res &= this.signed.contains(moduleImpl);
                }
                break;
            }
            default: {
                assert (false);
                break;
            }
        }
        return res;
    }

    private void addTrustedCertificates() {
        HashSet<UpdateElementImpl> untrusted = new HashSet<UpdateElementImpl>(this.signed);
        untrusted.removeAll(this.trusted);
        if (untrusted.isEmpty()) {
            return;
        }
        HashSet<Certificate> untrustedCertificates = new HashSet<Certificate>();
        for (UpdateElementImpl i : untrusted) {
            untrustedCertificates.addAll(this.certs.get(i.getUpdateElement()));
        }
        if (!untrustedCertificates.isEmpty()) {
            Utilities.addCertificates(untrustedCertificates);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void doCancel() throws OperationException {
        Object object = this.LOCK;
        synchronized (object) {
            this.currentStep = STEP.CANCEL;
        }
        if (this.runningTask != null && !this.runningTask.isDone() && !this.runningTask.isCancelled()) {
            boolean cancelled = this.runningTask.cancel(true);
            assert (cancelled);
        }
        Set<File> cancelled = this.downloadedFiles;
        synchronized (cancelled) {
            for (File f : this.downloadedFiles) {
                if (f == null || !f.exists()) continue;
                f.delete();
                if (!f.getParentFile().isDirectory() || f.getParentFile().list() == null || f.getParentFile().list().length != 0) continue;
                f.getParentFile().delete();
            }
            this.downloadedFiles.clear();
        }
        Utilities.cleanUpdateOfUpdaterJar();
        if (this.affectedFeatureImpls != null) {
            this.affectedFeatureImpls = null;
        }
        if (this.affectedModuleImpls != null) {
            this.affectedModuleImpls = null;
        }
        this.getElement2Clusters().clear();
    }

    private int doDownload(OperationContainer.OperationInfo info, ProgressHandle progress, int aggregateDownload, int totalSize, long id) throws OperationException {
        UpdateElement toUpdateElement = info.getUpdateElement();
        UpdateElementImpl toUpdateImpl = Trampoline.API.impl(toUpdateElement);
        int res = 0;
        switch (toUpdateImpl.getType()) {
            case KIT_MODULE: 
            case MODULE: {
                res += this.doDownload(toUpdateImpl, progress, aggregateDownload, totalSize, id);
                break;
            }
            case STANDALONE_MODULE: 
            case FEATURE: {
                FeatureUpdateElementImpl toUpdateFeatureImpl = (FeatureUpdateElementImpl)toUpdateImpl;
                Set<ModuleUpdateElementImpl> moduleImpls = toUpdateFeatureImpl.getContainedModuleElements();
                int nestedAggregateDownload = aggregateDownload;
                for (ModuleUpdateElementImpl moduleImpl : moduleImpls) {
                    if (Utilities.isElementInstalled(moduleImpl.getUpdateElement())) continue;
                    int increment = this.doDownload(moduleImpl, progress, nestedAggregateDownload, totalSize, id);
                    if (increment == -1) {
                        return -1;
                    }
                    nestedAggregateDownload += increment;
                    res += increment;
                }
                break;
            }
            default: {
                assert (false);
                break;
            }
        }
        return res;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private int doDownload(UpdateElementImpl toUpdateImpl, ProgressHandle progress, int aggregateDownload, int totalSize, long id) throws OperationException {
        if (this.cancelled()) {
            LOG.log(Level.INFO, "InstallSupport.doDownload was canceled, returns -1");
            return -1;
        }
        UpdateElement installed = toUpdateImpl.getUpdateUnit().getInstalled();
        File targetCluster = this.getTargetCluster(installed, toUpdateImpl, this.isGlobal, this.useUserdirAsFallback);
        assert (targetCluster != null);
        if (targetCluster == null) {
            targetCluster = InstallManager.getUserDir();
        }
        URL source = toUpdateImpl.getInstallInfo().getDistribution();
        LOG.log(Level.FINE, "Source URL for " + toUpdateImpl.getCodeName() + " is " + source);
        if (source == null) {
            String errorString = NbBundle.getMessage(InstallSupportImpl.class, (String)"InstallSupportImpl_NullSource", (Object)toUpdateImpl.getCodeName());
            LOG.log(Level.INFO, errorString);
            throw new OperationException(OperationException.ERROR_TYPE.INSTALL, errorString);
        }
        File dest = InstallSupportImpl.getDestination(targetCluster, toUpdateImpl.getCodeName(), source, id);
        if (dest.exists()) {
            LOG.log(Level.FINE, "Target NBM file " + dest + " of " + toUpdateImpl.getUpdateElement() + " already downloaded.");
            return toUpdateImpl.getDownloadSize();
        }
        try {
            String label = toUpdateImpl.getDisplayName();
            File normalized = FileUtil.normalizeFile((File)InstallSupportImpl.getDestination(targetCluster, toUpdateImpl.getCodeName(), source, 0));
            File normalizedInProgress = FileUtil.normalizeFile((File)InstallSupportImpl.getDestination(targetCluster, toUpdateImpl.getCodeName(), source, id));
            Set<File> set = this.downloadedFiles;
            synchronized (set) {
                this.downloadedFiles.add(normalized);
                this.downloadedFiles.add(normalizedInProgress);
            }
            int c = this.copy(source, dest, progress, toUpdateImpl.getDownloadSize(), aggregateDownload, totalSize, label);
            boolean wasException = false;
            JarFile nbm = new JarFile(dest);
            try {
                Enumeration<JarEntry> en = nbm.entries();
                while (en.hasMoreElements()) {
                    JarEntry jarEntry = en.nextElement();
                    if (!jarEntry.getName().endsWith(".external")) continue;
                    InputStream is = nbm.getInputStream(jarEntry);
                    try {
                        CRC32 check;
                        AtomicLong crc;
                        File external;
                        InputStream real;
                        crc = new AtomicLong();
                        real = InstallSupportImpl.externalDownload(is, crc, jarEntry.getName());
                        if (crc.get() == -1) {
                            throw new IOException(jarEntry.getName() + " does not contain CRC: line!");
                        }
                        byte[] arr = new byte[4096];
                        check = new CRC32();
                        external = new File(dest.getPath() + "." + Long.toHexString(crc.get()));
                        FileOutputStream fos = new FileOutputStream(external);
                        try {
                            int len;
                            while ((len = real.read(arr)) != -1) {
                                check.update(arr, 0, len);
                                fos.write(arr, 0, len);
                                if (!this.progressRunning || (c += len) > toUpdateImpl.getDownloadSize()) continue;
                                progress.progress(aggregateDownload + c);
                            }
                        }
                        finally {
                            fos.close();
                        }
                        real.close();
                        if (check.getValue() == crc.get()) continue;
                        LOG.log(Level.INFO, "Deleting file with uncomplete external content(cause: wrong CRC) " + normalized);
                        dest.delete();
                        Set<File> len = this.downloadedFiles;
                        synchronized (len) {
                            this.downloadedFiles.remove(normalized);
                        }
                        external.delete();
                        throw new IOException("Wrong CRC for " + jarEntry.getName());
                    }
                    finally {
                        is.close();
                        continue;
                    }
                }
            }
            catch (FileNotFoundException x) {
                LOG.log(Level.INFO, x.getMessage(), x);
                wasException = true;
                throw new OperationException(OperationException.ERROR_TYPE.INSTALL, x.getLocalizedMessage());
            }
            catch (IOException x) {
                LOG.log(Level.INFO, x.getMessage(), x);
                wasException = true;
                throw new OperationException(OperationException.ERROR_TYPE.PROXY, x.getLocalizedMessage());
            }
            finally {
                nbm.close();
                if (wasException) {
                    dest.delete();
                }
            }
        }
        catch (UnknownHostException x) {
            LOG.log(Level.INFO, x.getMessage(), x);
            throw new OperationException(OperationException.ERROR_TYPE.PROXY, source.toString());
        }
        catch (FileNotFoundException x) {
            LOG.log(Level.INFO, x.getMessage(), x);
            throw new OperationException(OperationException.ERROR_TYPE.INSTALL, x.getLocalizedMessage());
        }
        catch (IOException x) {
            LOG.log(Level.INFO, x.getMessage(), x);
            throw new OperationException(OperationException.ERROR_TYPE.PROXY, source.toString());
        }
        return toUpdateImpl.getDownloadSize();
    }

    private int doValidate(OperationContainer.OperationInfo info, ProgressHandle progress, int verified) throws OperationException {
        UpdateElement toUpdateElement = info.getUpdateElement();
        UpdateElementImpl toUpdateImpl = Trampoline.API.impl(toUpdateElement);
        int increment = 0;
        switch (toUpdateImpl.getType()) {
            case KIT_MODULE: 
            case MODULE: {
                increment = this.doValidate(toUpdateImpl, progress, verified);
                break;
            }
            case STANDALONE_MODULE: 
            case FEATURE: {
                FeatureUpdateElementImpl toUpdateFeatureImpl = (FeatureUpdateElementImpl)toUpdateImpl;
                Set<ModuleUpdateElementImpl> moduleImpls = toUpdateFeatureImpl.getContainedModuleElements();
                int nestedVerified = verified;
                for (ModuleUpdateElementImpl moduleImpl : moduleImpls) {
                    if (Utilities.isElementInstalled(moduleImpl.getUpdateElement())) continue;
                    int singleIncrement = this.doValidate(moduleImpl, progress, nestedVerified);
                    nestedVerified += singleIncrement;
                    increment += singleIncrement;
                }
                break;
            }
            default: {
                assert (false);
                break;
            }
        }
        return increment;
    }

    private int doValidate(UpdateElementImpl toUpdateImpl, ProgressHandle progress, int verified) throws OperationException {
        UpdateElement installed = toUpdateImpl.getUpdateUnit().getInstalled();
        File targetCluster = this.getTargetCluster(installed, toUpdateImpl, this.isGlobal, this.useUserdirAsFallback);
        URL source = toUpdateImpl.getInstallInfo().getDistribution();
        File dest = InstallSupportImpl.getDestination(targetCluster, toUpdateImpl.getCodeName(), source, 0);
        if (!dest.exists()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Cannot find ").append(dest).append("\n");
            sb.append("Parent directory contains:").append(Arrays.toString(dest.getParentFile().list())).append("\n");
            for (File f : UpdateTracking.clusters((boolean)true)) {
                sb.append("Trying to find result in ").append(f).append(" = ");
                File alt = InstallSupportImpl.getDestination(targetCluster, toUpdateImpl.getCodeName(), source, 0);
                sb.append(alt).append(" exists ").append(alt.exists()).append("\n");
            }
            throw new OperationException(OperationException.ERROR_TYPE.INSTALL, sb.toString());
        }
        int wasVerified = this.verifyNbm(toUpdateImpl.getUpdateElement(), dest, progress, verified);
        return wasVerified;
    }

    public static File getDestination(File targetCluster, String codeName, URL source) {
        return InstallSupportImpl.getDestination(targetCluster, codeName, source, 0);
    }

    private static File getDestination(File targetCluster, String codeName, URL source, long id) {
        LOG.log(Level.FINE, "Target cluster for " + codeName + " is " + targetCluster);
        File destDir = new File(targetCluster, 0 == id ? Utilities.DOWNLOAD_DIR : Utilities.RUNNING_DOWNLOAD_DIR + '_' + id);
        if (!destDir.exists()) {
            destDir.mkdirs();
        }
        String fileName = codeName.replace('.', '-');
        String filePath = source.getFile().toLowerCase(Locale.US);
        String ext = filePath.endsWith(".nbm".toLowerCase(Locale.US)) ? ".nbm" : (filePath.endsWith(".jar".toLowerCase(Locale.US)) ? ".jar" : "");
        File destFile = new File(destDir, fileName + ext);
        LOG.log(Level.FINE, "Destination file for " + codeName + " is " + destFile);
        return destFile;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean cancelled() {
        InstallSupportImpl installSupportImpl = this;
        synchronized (installSupportImpl) {
            return STEP.CANCEL == this.currentStep;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private int copy(URL source, File dest, ProgressHandle progress, int estimatedSize, int aggregateDownload, int totalSize, String label) throws MalformedURLException, IOException {
        int increment;
        BufferedOutputStream bdest;
        boolean canceled;
        int contentLength;
        OpenConnectionListener listener = new OpenConnectionListener(source);
        final NetworkAccess.Task task = NetworkAccess.createNetworkAcessTask(source, AutoupdateSettings.getOpenConnectionTimeout(), listener);
        new Thread(new Runnable(){

            @Override
            public void run() {
                while (!task.isFinished()) {
                    if (InstallSupportImpl.this.cancelled()) {
                        task.cancel();
                        break;
                    }
                    try {
                        Thread.sleep(50);
                    }
                    catch (InterruptedException e) {}
                }
            }
        }).start();
        task.waitFinished();
        try {
            if (listener.getException() != null) {
                throw listener.getException();
            }
        }
        catch (FileNotFoundException x) {
            LOG.log(Level.INFO, x.getMessage(), x);
            throw new IOException(NbBundle.getMessage(InstallSupportImpl.class, (String)"InstallSupportImpl_Download_Unavailable", (Object)source));
        }
        catch (IOException x) {
            LOG.log(Level.INFO, x.getMessage(), x);
            throw new IOException(NbBundle.getMessage(InstallSupportImpl.class, (String)"InstallSupportImpl_Download_Unavailable", (Object)source));
        }
        catch (Exception x) {
            LOG.log(Level.INFO, x.getMessage(), x);
            throw new IOException(NbBundle.getMessage(InstallSupportImpl.class, (String)"InstallSupportImpl_Download_Unavailable", (Object)source));
        }
        if (this.cancelled()) {
            LOG.log(Level.FINE, "Download of " + source + " was cancelled");
            throw new IOException("Download of " + source + " was cancelled");
        }
        InputStream is = listener.getInputStream();
        contentLength = listener.getContentLength();
        BufferedInputStream bsrc = new BufferedInputStream(is);
        bdest = null;
        LOG.log(Level.FINEST, "Copy " + source + " to " + dest + "[" + estimatedSize + "]");
        canceled = false;
        increment = 0;
        try {
            int size;
            byte[] bytes = new byte[1024];
            int c = 0;
            while (!(canceled = this.cancelled()) && (size = bsrc.read(bytes)) != -1) {
                if (bdest == null) {
                    bdest = new BufferedOutputStream(new FileOutputStream(dest));
                }
                bdest.write(bytes, 0, size);
                increment += size;
                c += size;
                if (!this.progressRunning && progress != null) {
                    progress.switchToDeterminate(totalSize);
                    progress.progress(label);
                    this.progressRunning = true;
                }
                if (c <= 1024) continue;
                if (progress != null) {
                    assert (this.progressRunning);
                    progress.switchToDeterminate(totalSize);
                    int i = aggregateDownload + (increment < estimatedSize ? increment : estimatedSize);
                    progress.progress(label, i < totalSize ? i : totalSize);
                }
                c = 0;
            }
            if (estimatedSize != increment) {
                LOG.log(Level.FINEST, "Increment (" + increment + ") of is not equal to estimatedSize (" + estimatedSize + ").");
            }
        }
        catch (IOException ioe) {
            LOG.log(Level.INFO, "Writing content of URL " + source + " failed.", ioe);
        }
        finally {
            try {
                bsrc.close();
                if (bdest != null) {
                    bdest.flush();
                }
                if (bdest != null) {
                    bdest.close();
                }
            }
            catch (IOException ioe) {
                LOG.log(Level.INFO, ioe.getMessage(), ioe);
            }
        }
        if (contentLength != -1 && increment != contentLength) {
            if (canceled) {
                LOG.log(Level.INFO, "Download of " + source + " was cancelled");
            } else {
                LOG.log(Level.INFO, "Content length was reported as " + contentLength + " byte(s) but read " + increment + " byte(s)");
            }
            if (bdest != null && dest.exists()) {
                LOG.log(Level.INFO, "Deleting not fully downloaded file " + dest);
                dest.delete();
                File normalized = FileUtil.normalizeFile((File)dest);
                Set<File> size = this.downloadedFiles;
                synchronized (size) {
                    this.downloadedFiles.remove(normalized);
                }
            }
            if (canceled) {
                throw new IOException("Download of " + source + " was cancelled");
            }
            throw new IOException("Server closed connection unexpectedly");
        }
        LOG.log(Level.FINE, "Destination " + dest + " is successfully wrote. Size " + dest.length());
        return increment;
    }

    private int verifyNbm(UpdateElement el, File nbmFile, ProgressHandle progress, int verified) throws OperationException {
        String res2;
        try {
            Collection<Certificate> nbmCerts;
            ArrayList<Certificate> trustedCerts = new ArrayList<Certificate>();
            for (KeyStore ks : Utilities.getKeyStore()) {
                trustedCerts.addAll(Utilities.getCertificates(ks));
            }
            KeyStore ks2 = Utilities.loadKeyStore();
            if (ks2 != null) {
                trustedCerts.addAll(Utilities.getCertificates(ks2));
            }
            verified += el.getDownloadSize();
            if (progress != null) {
                progress.progress(el.getDisplayName(), verified < this.wasDownloaded ? verified : this.wasDownloaded);
            }
            if ((nbmCerts = Utilities.getNbmCertificates(nbmFile)) != null && nbmCerts.size() > 0) {
                this.certs.put(el, nbmCerts);
            }
            res2 = Utilities.verifyCertificates(nbmCerts, trustedCerts);
            UpdateElementImpl impl = Trampoline.API.impl(el);
            if ("TRUSTED".equals(res2) || "N/A".equals(res2)) {
                this.trusted.add(impl);
                this.signed.add(impl);
            } else if ("UNTRUSTED".equals(res2)) {
                this.signed.add(impl);
            }
        }
        catch (IOException ioe) {
            LOG.log(Level.INFO, ioe.getMessage(), ioe);
            String res2 = "BAD_DOWNLOAD";
            throw new OperationException(OperationException.ERROR_TYPE.INSTALL, NbBundle.getMessage(InstallSupportImpl.class, (String)"InstallSupportImpl_Validate_CorruptedNBM", (Object)nbmFile));
        }
        catch (KeyStoreException kse) {
            LOG.log(Level.INFO, kse.getMessage(), kse);
            String res2 = "CORRUPTED";
            throw new OperationException(OperationException.ERROR_TYPE.INSTALL, NbBundle.getMessage(InstallSupportImpl.class, (String)"InstallSupportImpl_Validate_CorruptedNBM", (Object)nbmFile));
        }
        LOG.log(Level.FINE, "NBM " + nbmFile + " was verified as " + res2);
        return el.getDownloadSize();
    }

    private boolean needsRestart(boolean isUpdate, UpdateElementImpl toUpdateImpl, File dest) {
        return InstallManager.needsRestart(isUpdate, toUpdateImpl, dest);
    }

    private static String relativePath(File f, StringBuilder sb) {
        if (f == null) {
            return null;
        }
        if (f.getName().equals("config")) {
            return sb.toString();
        }
        if (sb.length() > 0) {
            sb.insert(0, '/');
        }
        sb.insert(0, f.getName());
        return InstallSupportImpl.relativePath(f.getParentFile(), sb);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Converted monitor instructions to comments
     * Lifted jumps to return sites
     */
    private static void touch(File f, long minTime) {
        long time;
        int cnt = 0;
        while ((time = f.lastModified()) <= minTime) {
            if (!f.exists()) {
                LOG.log(Level.FINE, "File {0} does not exist anymore", f);
                break;
            }
            LOG.log(Level.FINE, "Need to change time for {0} with delta {1}", new Object[]{f, minTime - f.lastModified()});
            try {
                Class<InstallSupportImpl> class_ = InstallSupportImpl.class;
                // MONITORENTER : org.netbeans.modules.autoupdate.services.InstallSupportImpl.class
                InstallSupportImpl.class.wait(30);
                // MONITOREXIT : class_
            }
            catch (InterruptedException ex) {
                // empty catch block
            }
            f.setLastModified(System.currentTimeMillis() - 1000);
            ++cnt;
        }
        LOG.log(Level.FINE, "Time stamp changed succcessfully {0}", f);
    }

    private File getTargetCluster(UpdateElement installed, UpdateElementImpl update, Boolean isGlobal, boolean useUserdirAsFallback) throws OperationException {
        File cluster = this.getElement2Clusters().get(update);
        if (cluster == null && (cluster = InstallManager.findTargetDirectory(installed, update, isGlobal, useUserdirAsFallback)) != null) {
            this.getElement2Clusters().put(update, cluster);
        }
        return cluster;
    }

    private Map<UpdateElementImpl, File> getElement2Clusters() {
        if (this.element2Clusters == null) {
            this.element2Clusters = new HashMap<UpdateElementImpl, File>();
        }
        return this.element2Clusters;
    }

    private ExecutorService getExecutionService() {
        if (this.es == null || this.es.isShutdown()) {
            this.es = Executors.newSingleThreadExecutor();
        }
        return this.es;
    }

    private static InputStream externalDownload(InputStream is, AtomicLong crc, String pathTo) throws IOException {
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        crc.set(-1);
        String url = null;
        String externalUrl = null;
        IOException ioe = null;
        while ((line = br.readLine()) != null) {
            int index;
            if (line.startsWith("CRC:")) {
                crc.set(Long.parseLong(line.substring(4).trim()));
            }
            if (!line.startsWith("URL:")) continue;
            url = line.substring(4).trim();
            while ((index = url.indexOf("${")) != -1) {
                int end = url.indexOf("}", index);
                String propName = url.substring(index + 2, end);
                String propVal = System.getProperty(propName);
                if (propVal == null) {
                    throw new IOException("Can't find property " + propName);
                }
                url = url.substring(0, index) + propVal + url.substring(end + 1);
            }
            LOG.log(Level.INFO, "Trying external URL: {0}", url);
            try {
                URLConnection conn = new URL(url).openConnection();
                conn.setConnectTimeout(AutoupdateSettings.getOpenConnectionTimeout());
                conn.setReadTimeout(AutoupdateSettings.getOpenConnectionTimeout());
                return conn.getInputStream();
            }
            catch (IOException ex) {
                LOG.log(Level.WARNING, "Cannot connect to {0}", url);
                LOG.log(Level.INFO, "Details", ex);
                if (!(ex instanceof UnknownHostException) && !(ex instanceof ConnectException) && !(ex instanceof SocketTimeoutException)) continue;
                ioe = ex;
                externalUrl = url;
                continue;
            }
        }
        if (ioe == null) {
            throw new FileNotFoundException("Cannot resolve external reference to " + (url == null ? pathTo : url));
        }
        throw new IOException("resolving external reference to " + (externalUrl == null ? pathTo : externalUrl));
    }

    private static class UpdaterInfo {
        private final JarEntry updaterJarEntry;
        private final File zipFileWithUpdater;
        private final File updaterTargetCluster;

        public UpdaterInfo(JarEntry updaterJarEntry, File updaterJarFile, File updaterTargetCluster) {
            this.updaterJarEntry = updaterJarEntry;
            this.zipFileWithUpdater = updaterJarFile;
            this.updaterTargetCluster = updaterTargetCluster;
        }

        public JarEntry getUpdaterJarEntry() {
            return this.updaterJarEntry;
        }

        public File getZipFileWithUpdater() {
            return this.zipFileWithUpdater;
        }

        public File getUpdaterTargetCluster() {
            return this.updaterTargetCluster;
        }
    }

    private static final class RefreshModulesListener
    implements PropertyChangeListener,
    Runnable {
        private final ProgressHandle handle;
        private int i;
        private PropertyChangeEvent ev;

        public RefreshModulesListener(ProgressHandle handle) {
            this.handle = handle;
            this.i = 0;
        }

        @Override
        public void propertyChange(PropertyChangeEvent ev) {
            if ("RUNNING".equals(ev.getPropertyName())) {
                if (this.handle != null) {
                    this.handle.progress(ev.getNewValue() == null ? "" : ev.getNewValue().toString(), this.i++);
                }
            } else if ("FINISHED".equals(ev.getPropertyName())) {
                this.ev = ev;
                FileUtil.runAtomicAction((Runnable)this);
            } else assert (false);
        }

        @Override
        public void run() {
            for (int loop = 0; loop < 10; ++loop) {
                Map modifiedFiles = NbCollections.checkedMapByFilter((Map)((Map)this.ev.getNewValue()), File.class, Long.class, (boolean)true);
                long now = System.currentTimeMillis();
                for (Map.Entry e : modifiedFiles.entrySet()) {
                    InstallSupportImpl.touch((File)e.getKey(), Math.max((Long)e.getValue(), now));
                }
                FileObject modulesRoot = FileUtil.getConfigFile((String)"Modules");
                if (modulesRoot != null) {
                    LOG.fine("Refreshing whole MFS");
                    modulesRoot.refresh();
                    try {
                        FileUtil.getConfigRoot().getFileSystem().refresh(true);
                    }
                    catch (FileStateInvalidException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                    LOG.fine("Done refreshing MFS");
                }
                boolean ok = true;
                for (File file : modifiedFiles.keySet()) {
                    String rel = InstallSupportImpl.relativePath(file, new StringBuilder());
                    if (rel == null) continue;
                    FileObject fo = FileUtil.getConfigFile((String)rel);
                    if (fo == null) {
                        LOG.log(loop < 5 ? Level.FINE : Level.WARNING, "Cannot find " + rel);
                        ok = false;
                        continue;
                    }
                    LOG.fine("Refreshing " + (Object)fo);
                    fo.refresh();
                }
                if (!ok) continue;
                LOG.log(loop < 5 ? Level.FINE : Level.INFO, "All was OK on " + loop + " th iteration");
                break;
            }
        }
    }

    private class OpenConnectionListener
    implements NetworkAccess.NetworkListener {
        private InputStream stream;
        int contentLength;
        private URL source;
        private Exception ex;

        public OpenConnectionListener(URL source) {
            this.stream = null;
            this.contentLength = -1;
            this.source = null;
            this.ex = null;
            this.source = source;
        }

        public InputStream getInputStream() {
            return this.stream;
        }

        public int getContentLength() {
            return this.contentLength;
        }

        @Override
        public void streamOpened(InputStream stream, int contentLength) {
            LOG.log(Level.FINEST, "Opened connection for " + this.source);
            this.stream = stream;
            this.contentLength = contentLength;
        }

        @Override
        public void accessCanceled() {
            LOG.log(Level.INFO, "Opening connection for " + this.source + "was cancelled");
        }

        @Override
        public void accessTimeOut() {
            LOG.log(Level.INFO, "Opening connection for " + this.source + "was finised due to timeout");
        }

        @Override
        public void notifyException(Exception x) {
            this.ex = x;
        }

        public Exception getException() {
            return this.ex;
        }
    }

    private static enum STEP {
        NOTSTARTED,
        DOWNLOAD,
        VALIDATION,
        INSTALLATION,
        RESTART,
        FINISHED,
        CANCEL;
        

        private STEP() {
        }
    }

}

