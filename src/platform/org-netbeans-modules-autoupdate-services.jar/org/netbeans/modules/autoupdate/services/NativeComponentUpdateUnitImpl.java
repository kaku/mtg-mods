/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.autoupdate.services;

import java.util.logging.Logger;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateManager;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.modules.autoupdate.services.NativeComponentUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.Trampoline;
import org.netbeans.modules.autoupdate.services.UpdateElementImpl;
import org.netbeans.modules.autoupdate.services.UpdateUnitImpl;
import org.netbeans.modules.autoupdate.updateprovider.NativeComponentItem;

public class NativeComponentUpdateUnitImpl
extends UpdateUnitImpl {
    private Logger err;

    public NativeComponentUpdateUnitImpl(String codename) {
        super(codename);
        this.err = Logger.getLogger(this.getClass().getName());
    }

    @Override
    public UpdateManager.TYPE getType() {
        return UpdateManager.TYPE.CUSTOM_HANDLED_COMPONENT;
    }

    @Override
    public void setInstalled(UpdateElement newInstalled) {
        UpdateElement oldInstalled = this.getInstalled();
        if (oldInstalled == null) {
            super.setInstalled(newInstalled);
        } else if (oldInstalled.getSpecificationVersion() != null && !oldInstalled.getSpecificationVersion().equals(newInstalled.getSpecificationVersion())) {
            this.setAsUninstalled();
            super.setInstalled(newInstalled);
        }
    }

    @Override
    public void addUpdate(UpdateElement update) {
        UpdateElementImpl impl = Trampoline.API.impl(update);
        assert (impl instanceof NativeComponentUpdateElementImpl);
        NativeComponentUpdateElementImpl nativeImpl = (NativeComponentUpdateElementImpl)impl;
        if (nativeImpl.getNativeItem().isInstalled()) {
            this.setInstalled(update);
        } else {
            super.addUpdate(update);
        }
    }

    @Override
    public UpdateUnit getVisibleAncestor() {
        return this.getUpdateUnit();
    }
}

