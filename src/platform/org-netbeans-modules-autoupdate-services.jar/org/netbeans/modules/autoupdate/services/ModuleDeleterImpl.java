/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.updater.UpdateTracking
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.InstalledFileLocator
 *  org.openide.modules.ModuleInfo
 *  org.openide.xml.EntityCatalog
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.modules.autoupdate.services;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.modules.autoupdate.services.Utilities;
import org.netbeans.modules.autoupdate.updateprovider.InstalledModuleProvider;
import org.netbeans.updater.UpdateTracking;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.modules.InstalledFileLocator;
import org.openide.modules.ModuleInfo;
import org.openide.xml.EntityCatalog;
import org.openide.xml.XMLUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public final class ModuleDeleterImpl {
    private static final ModuleDeleterImpl INSTANCE = new ModuleDeleterImpl();
    private static final String ELEMENT_MODULE = "module";
    private static final String ELEMENT_VERSION = "module_version";
    private static final String ATTR_LAST = "last";
    private static final String ATTR_FILE_NAME = "name";
    private static final Logger err = Logger.getLogger(ModuleDeleterImpl.class.getName());
    private Set<File> storageFilesForDelete = null;

    public static ModuleDeleterImpl getInstance() {
        return INSTANCE;
    }

    public boolean canDelete(ModuleInfo moduleInfo) {
        if (moduleInfo == null) {
            return false;
        }
        if (Utilities.isEssentialModule(moduleInfo)) {
            err.log(Level.FINE, "Cannot delete module because module " + moduleInfo.getCodeName() + " isEssentialModule.");
            return false;
        }
        return this.foundUpdateTracking(moduleInfo);
    }

    public Collection<File> markForDisable(Collection<ModuleInfo> modules, ProgressHandle handle) {
        if (modules == null) {
            throw new IllegalArgumentException("ModuleInfo argument cannot be null.");
        }
        if (handle != null) {
            handle.switchToDeterminate(modules.size() + 1);
        }
        HashSet<File> configs = new HashSet<File>();
        int i = 0;
        for (ModuleInfo moduleInfo : modules) {
            File config = this.locateConfigFile(moduleInfo);
            assert (config != null);
            assert (config.exists());
            err.log(Level.FINE, "Locate config file of " + moduleInfo.getCodeNameBase() + ": " + config);
            if (config != null) {
                configs.add(config);
            }
            if (handle == null) continue;
            handle.progress(++i);
        }
        return configs;
    }

    public Collection<File> markForDelete(Collection<ModuleInfo> modules, ProgressHandle handle) throws IOException {
        this.storageFilesForDelete = null;
        if (modules == null) {
            throw new IllegalArgumentException("ModuleInfo argument cannot be null.");
        }
        if (handle != null) {
            handle.switchToDeterminate(modules.size() * 2 + 1);
        }
        HashSet<File> configFiles = new HashSet<File>();
        int i = 0;
        for (ModuleInfo moduleInfo2 : modules) {
            Collection<File> configs = this.locateAllConfigFiles(moduleInfo2);
            assert (configs != null);
            assert (!configs.isEmpty());
            configFiles.addAll(configs);
            err.log(Level.FINE, "Locate config files of " + moduleInfo2.getCodeNameBase() + ": " + configs);
            if (handle == null) continue;
            handle.progress(++i);
        }
        this.getStorageFilesForDelete().addAll(configFiles);
        for (ModuleInfo moduleInfo2 : modules) {
            this.removeModuleFiles(moduleInfo2, true);
            if (handle == null) continue;
            handle.progress(++i);
        }
        return this.getStorageFilesForDelete();
    }

    public void delete(ModuleInfo[] modules, ProgressHandle handle) throws IOException {
        this.storageFilesForDelete = null;
        if (modules == null) {
            throw new IllegalArgumentException("ModuleInfo argument cannot be null.");
        }
        if (handle != null) {
            handle.switchToDeterminate(modules.length + 1);
        }
        int i = 0;
        for (ModuleInfo moduleInfo : modules) {
            err.log(Level.FINE, "Locate and remove config file of " + moduleInfo.getCodeNameBase());
            this.removeControlModuleFile(moduleInfo, false);
        }
        if (handle != null) {
            handle.progress(++i);
        }
        this.refreshModuleList();
        int rerunWaitCount = 0;
        for (ModuleInfo moduleInfo2 : modules) {
            err.log(Level.FINE, "Locate and remove config file of " + moduleInfo2.getCodeNameBase());
            if (handle != null) {
                handle.progress(moduleInfo2.getDisplayName(), ++i);
            }
            while (rerunWaitCount < 100 && !this.isModuleUninstalled(moduleInfo2)) {
                try {
                    Thread.sleep(100);
                }
                catch (InterruptedException ex) {
                    err.log(Level.INFO, "Overflow checks of uninstalled module " + moduleInfo2.getCodeName());
                    Thread.currentThread().interrupt();
                }
                ++rerunWaitCount;
            }
            this.removeModuleFiles(moduleInfo2, false);
        }
    }

    private boolean isModuleUninstalled(ModuleInfo moduleInfo) {
        return InstalledModuleProvider.getInstalledModules().get(moduleInfo.getCodeNameBase()) == null;
    }

    private File locateConfigFile(ModuleInfo m) {
        String configFile = "config/Modules/" + m.getCodeNameBase().replace('.', '-') + ".xml";
        return InstalledFileLocator.getDefault().locate(configFile, m.getCodeNameBase(), false);
    }

    private Collection<File> locateAllConfigFiles(ModuleInfo m) {
        HashSet<File> configFiles = new HashSet<File>();
        String configFileName = m.getCodeNameBase().replace('.', '-') + ".xml";
        for (File cluster : UpdateTracking.clusters((boolean)true)) {
            File configFile = new File(new File(new File(cluster, "config"), "Modules"), configFileName);
            if (!configFile.exists()) continue;
            configFiles.add(configFile);
        }
        return configFiles;
    }

    private void removeControlModuleFile(ModuleInfo m, boolean markForDelete) throws IOException {
        File configFile;
        while ((configFile = this.locateConfigFile(m)) != null && !this.getStorageFilesForDelete().contains(configFile)) {
            if (configFile != null && configFile.exists()) {
                if (markForDelete) {
                    err.log(Level.FINE, "Control file " + configFile + " is marked for delete.");
                    this.getStorageFilesForDelete().add(configFile);
                    continue;
                }
                err.log(Level.FINE, "Try delete the config File " + configFile);
                configFile.delete();
                err.log(Level.FINE, "Control file " + configFile + " is deleted.");
                continue;
            }
            err.log(Level.FINE, "Warning: Config File " + configFile + " doesn't exist!");
        }
    }

    private boolean foundUpdateTracking(ModuleInfo moduleInfo) {
        File updateTracking = Utilities.locateUpdateTracking(moduleInfo);
        if (updateTracking != null && updateTracking.exists()) {
            if (!Utilities.canWrite(updateTracking)) {
                err.log(Level.FINE, "Cannot delete module " + moduleInfo.getCodeName() + " because is forbidden to write in directory " + updateTracking.getParentFile().getParent());
                return false;
            }
            return true;
        }
        err.log(Level.FINE, "Cannot delete module " + moduleInfo.getCodeName() + " because no update_tracking file found.");
        return false;
    }

    private void removeModuleFiles(ModuleInfo m, boolean markForDelete) throws IOException {
        File updateTracking;
        err.log(Level.FINE, "Entry removing files of module " + (Object)m);
        while ((updateTracking = Utilities.locateUpdateTracking(m)) != null && !this.getStorageFilesForDelete().contains(updateTracking)) {
            this.removeModuleFilesInCluster(m, updateTracking, markForDelete);
        }
        err.log(Level.FINE, "Exit removing files of module " + (Object)m);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void removeModuleFilesInCluster(ModuleInfo moduleInfo, File updateTracking, boolean markForDelete) throws IOException {
        block19 : {
            File file;
            err.log(Level.FINE, "Read update_tracking " + updateTracking + " file.");
            Set<String> moduleFiles = this.readModuleFiles(this.getModuleConfiguration(updateTracking));
            String configFile = "config/Modules/" + moduleInfo.getCodeNameBase().replace('.', '-') + ".xml";
            if (moduleFiles.contains(configFile) && (file = InstalledFileLocator.getDefault().locate(configFile, moduleInfo.getCodeNameBase(), false)) != null && file.exists() && !this.getStorageFilesForDelete().contains(file)) {
                err.log(Level.WARNING, "Config file " + configFile + " must be already removed or marked for remove but still exist as file " + file + " and not found in StorageFilesForDelete : " + this.getStorageFilesForDelete());
            }
            for (String fileName : moduleFiles) {
                if (fileName.equals(configFile)) continue;
                Set files = InstalledFileLocator.getDefault().locateAll(fileName, moduleInfo.getCodeNameBase(), false);
                File file2 = null;
                if (files.size() > 0) {
                    file2 = (File)files.iterator().next();
                    if (files.size() > 1) {
                        boolean found = false;
                        for (File f : files) {
                            if (!f.getPath().startsWith(updateTracking.getParentFile().getParentFile().getPath())) continue;
                            file2 = f;
                            found = true;
                            break;
                        }
                        if (!found) {
                            err.log(Level.WARNING, "InstalledFileLocator doesn't choose the right file with file name " + fileName + " for module " + moduleInfo.getCodeNameBase() + " since a few files were returned : " + Arrays.toString(files.toArray()) + ", and since none are in the same cluster as update tracking file " + updateTracking + " will use " + file2);
                        }
                    }
                }
                if (file2 == null) {
                    err.log(Level.WARNING, "InstalledFileLocator doesn't locate file " + fileName + " for module " + moduleInfo.getCodeNameBase());
                    continue;
                }
                if (file2.equals(updateTracking)) continue;
                assert (file2.exists());
                if (!file2.exists()) continue;
                if (markForDelete) {
                    err.log(Level.FINE, "File " + file2 + " is marked for delete.");
                    this.getStorageFilesForDelete().add(file2);
                    continue;
                }
                try {
                    FileObject fo = FileUtil.toFileObject((File)file2);
                    if (fo != null) {
                        fo.lock().releaseLock();
                    }
                    File f = file2;
                    while (f.delete()) {
                        f = f.getParentFile();
                    }
                }
                catch (IOException ioe) {
                    assert (false);
                    err.log(Level.FINE, "Waring: IOException " + ioe.getMessage() + " was caught. Propably file lock on the file.");
                    err.log(Level.FINE, "Try call File.deleteOnExit() on " + file2);
                    file2.deleteOnExit();
                }
                err.log(Level.FINE, "File " + file2 + " is deleted.");
            }
            FileObject trackingFo = FileUtil.toFileObject((File)updateTracking);
            FileLock lock = null;
            try {
                FileLock fileLock = lock = trackingFo != null ? trackingFo.lock() : null;
                if (markForDelete) {
                    err.log(Level.FINE, "Tracking file " + updateTracking + " is marked for delete.");
                    this.getStorageFilesForDelete().add(updateTracking);
                    break block19;
                }
                updateTracking.delete();
                err.log(Level.FINE, "Tracking file " + updateTracking + " is deleted.");
            }
            finally {
                if (lock != null) {
                    lock.releaseLock();
                }
            }
        }
        err.log(Level.FINE, "File " + updateTracking + " is deleted.");
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Loose catch block
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Lifted jumps to return sites
     */
    private Node getModuleConfiguration(File moduleUpdateTracking) {
        Document document = null;
        try {
            BufferedInputStream is = new BufferedInputStream(new FileInputStream(moduleUpdateTracking));
            InputSource xmlInputSource = new InputSource(is);
            document = XMLUtil.parse((InputSource)xmlInputSource, (boolean)false, (boolean)false, (ErrorHandler)null, (EntityResolver)EntityCatalog.getDefault());
            if (is != null) {
                is.close();
            }
        }
        catch (SAXException saxe) {
            err.log(Level.WARNING, "SAXException when reading " + moduleUpdateTracking, saxe);
            FileReader reader = null;
            reader = new FileReader(moduleUpdateTracking);
            char[] text = new char[1024];
            String fileContent = "";
            while (reader.read(text) > 0) {
                fileContent = fileContent + String.copyValueOf(text);
            }
            err.log(Level.WARNING, "SAXException in file:\n------FILE START------\n " + fileContent + "\n------FILE END-----\n");
            if (reader == null) return null;
            try {
                reader.close();
                return null;
            }
            catch (IOException ex) {
                return null;
            }
            catch (Exception ex) {
                if (reader == null) return null;
                try {
                    reader.close();
                    return null;
                }
                catch (IOException ex) {
                    return null;
                }
                catch (Throwable throwable) {
                    if (reader == null) throw throwable;
                    try {
                        reader.close();
                        throw throwable;
                    }
                    catch (IOException ex) {
                        // empty catch block
                    }
                    throw throwable;
                }
            }
        }
        catch (IOException ioe) {
            err.log(Level.WARNING, "IOException when reading " + moduleUpdateTracking, ioe);
        }
        if ($assertionsDisabled) return this.getModuleElement(document.getDocumentElement());
        if (document.getDocumentElement() != null) return this.getModuleElement(document.getDocumentElement());
        throw new AssertionError((Object)("File " + moduleUpdateTracking + " must contain <module> element."));
    }

    private Node getModuleElement(Element element) {
        Node lastElement = null;
        assert ("module".equals(element.getTagName()));
        NodeList listModuleVersions = element.getElementsByTagName("module_version");
        for (int i = 0; i < listModuleVersions.getLength() && (lastElement = this.getModuleLastVersion(listModuleVersions.item(i))) == null; ++i) {
        }
        return lastElement;
    }

    private Node getModuleLastVersion(Node version) {
        Node attrLast = version.getAttributes().getNamedItem("last");
        assert (attrLast != null);
        if (Boolean.valueOf(attrLast.getNodeValue()).booleanValue()) {
            return version;
        }
        return null;
    }

    private Set<String> readModuleFiles(Node version) {
        if (version == null) {
            return Collections.emptySet();
        }
        NodeList fileNodes = version.getChildNodes();
        if (fileNodes == null) {
            return Collections.emptySet();
        }
        HashSet<String> files = new HashSet<String>();
        for (int i = 0; i < fileNodes.getLength(); ++i) {
            if (!fileNodes.item(i).hasAttributes()) continue;
            NamedNodeMap map = fileNodes.item(i).getAttributes();
            files.add(map.getNamedItem("name").getNodeValue());
            err.log(Level.FINE, "Mark to delete: " + map.getNamedItem("name").getNodeValue());
        }
        return files;
    }

    private void refreshModuleList() {
        FileObject modulesRoot = FileUtil.getConfigFile((String)"Modules");
        err.log(Level.FINE, "Call refresh on " + (Object)modulesRoot + " file object.");
        if (modulesRoot != null) {
            modulesRoot.refresh();
        }
    }

    private Set<File> getStorageFilesForDelete() {
        if (this.storageFilesForDelete == null) {
            this.storageFilesForDelete = new HashSet<File>();
        }
        return this.storageFilesForDelete;
    }
}

