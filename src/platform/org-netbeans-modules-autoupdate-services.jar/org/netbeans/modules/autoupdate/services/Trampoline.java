/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.autoupdate.services;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.autoupdate.InstallSupport;
import org.netbeans.api.autoupdate.OperationContainer;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.api.autoupdate.UpdateUnitProvider;
import org.netbeans.modules.autoupdate.services.InstallSupportImpl;
import org.netbeans.modules.autoupdate.services.OperationContainerImpl;
import org.netbeans.modules.autoupdate.services.UpdateElementImpl;
import org.netbeans.modules.autoupdate.services.UpdateUnitImpl;
import org.netbeans.modules.autoupdate.services.UpdateUnitProviderImpl;
import org.netbeans.modules.autoupdate.updateprovider.UpdateItemImpl;
import org.netbeans.spi.autoupdate.AutoupdateClusterCreator;
import org.netbeans.spi.autoupdate.UpdateItem;

public abstract class Trampoline<Support> {
    public static Trampoline API;
    public static Trampoline SPI;

    protected abstract UpdateUnit createUpdateUnit(UpdateUnitImpl var1);

    protected abstract UpdateUnitImpl impl(UpdateUnit var1);

    protected abstract UpdateElement createUpdateElement(UpdateElementImpl var1);

    protected abstract UpdateElementImpl impl(UpdateElement var1);

    protected abstract OperationContainerImpl impl(OperationContainer var1);

    protected abstract OperationContainerImpl.OperationInfoImpl impl(OperationContainer.OperationInfo var1);

    protected abstract OperationContainer.OperationInfo<Support> createOperationInfo(OperationContainerImpl.OperationInfoImpl var1);

    protected abstract UpdateUnitProvider createUpdateUnitProvider(UpdateUnitProviderImpl var1);

    public abstract UpdateUnitProviderImpl impl(UpdateUnitProvider var1);

    public abstract InstallSupportImpl impl(InstallSupport var1);

    public abstract UpdateItemImpl impl(UpdateItem var1);

    protected abstract UpdateItem createUpdateItem(UpdateItemImpl var1);

    protected abstract File findCluster(String var1, AutoupdateClusterCreator var2);

    protected abstract File[] registerCluster(String var1, File var2, AutoupdateClusterCreator var3) throws IOException;

    static {
        try {
            Class.forName(UpdateUnit.class.getName(), true, Trampoline.class.getClassLoader());
            Class.forName(UpdateItem.class.getName(), true, Trampoline.class.getClassLoader());
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger("org.netbeans.modules.autoupdate.services.Trampoline").log(Level.SEVERE, ex.getMessage(), ex);
        }
    }
}

