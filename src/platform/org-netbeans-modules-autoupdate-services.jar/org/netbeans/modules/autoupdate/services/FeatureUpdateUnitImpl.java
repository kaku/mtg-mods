/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.autoupdate.services;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateManager;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.modules.autoupdate.services.FeatureUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.ModuleUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.Trampoline;
import org.netbeans.modules.autoupdate.services.UpdateElementImpl;
import org.netbeans.modules.autoupdate.services.UpdateUnitFactory;
import org.netbeans.modules.autoupdate.services.UpdateUnitImpl;
import org.netbeans.modules.autoupdate.updateprovider.ArtificialFeaturesProvider;
import org.netbeans.modules.autoupdate.updateprovider.FeatureItem;
import org.openide.util.NbBundle;

public class FeatureUpdateUnitImpl
extends UpdateUnitImpl {
    private static final Logger LOG = Logger.getLogger(FeatureUpdateUnitImpl.class.getName());
    private UpdateElement installedElement = null;
    private UpdateElement updateElement = null;
    private boolean initialized = false;
    private UpdateManager.TYPE type;

    public FeatureUpdateUnitImpl(String codename, UpdateManager.TYPE type) {
        super(codename);
        this.type = type;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public UpdateElement getInstalled() {
        FeatureUpdateUnitImpl featureUpdateUnitImpl = this;
        synchronized (featureUpdateUnitImpl) {
            this.initializeFeature();
        }
        return this.installedElement;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public List<UpdateElement> getAvailableUpdates() {
        FeatureUpdateUnitImpl featureUpdateUnitImpl = this;
        synchronized (featureUpdateUnitImpl) {
            this.initializeFeature();
        }
        if (this.updateElement == null) {
            return Collections.emptyList();
        }
        String id = this.updateElement.getCodeName();
        LOG.log(Level.FINE, "UpdateElement " + id + "[" + (this.installedElement == null ? "<not installed>" : this.installedElement.getSpecificationVersion()) + "] has update " + id + "[" + this.updateElement.getSpecificationVersion() + "]");
        return Collections.singletonList(this.updateElement);
    }

    @Override
    public UpdateManager.TYPE getType() {
        return this.type;
    }

    private void initializeFeature() {
        FeatureItem item;
        boolean isStandalone;
        if (this.initialized) {
            return;
        }
        List<UpdateElement> featureElements = this.getUpdates();
        this.installedElement = null;
        this.updateElement = null;
        UpdateElement installedFeatureElement = null;
        FeatureUpdateElementImpl featureImpl = null;
        HashSet<ModuleUpdateElementImpl> installedModules = new HashSet<ModuleUpdateElementImpl>();
        HashSet<FeatureUpdateElementImpl> installedFeatures = new HashSet<FeatureUpdateElementImpl>();
        HashSet<ModuleUpdateElementImpl> availableModules = new HashSet<ModuleUpdateElementImpl>();
        HashSet<FeatureUpdateElementImpl> availableFeatures = new HashSet<FeatureUpdateElementImpl>();
        HashSet<ModuleUpdateElementImpl> missingModules = new HashSet<ModuleUpdateElementImpl>();
        HashSet<FeatureUpdateElementImpl> missingFeatures = new HashSet<FeatureUpdateElementImpl>();
        assert (featureElements != null);
        for (UpdateElement el : featureElements) {
            UpdateElementImpl iuei;
            UpdateElement iue;
            UpdateElementImpl auei;
            UpdateElement aue;
            featureImpl = (FeatureUpdateElementImpl)Trampoline.API.impl(el);
            boolean installed = false;
            for (ModuleUpdateElementImpl moduleImpl : featureImpl.getContainedModuleElements()) {
                installed |= moduleImpl.getUpdateUnit().getInstalled() != null;
                iue = moduleImpl.getUpdateUnit().getInstalled();
                UpdateElementImpl updateElementImpl = iuei = iue == null ? null : Trampoline.API.impl(iue);
                assert (iuei == null || iuei instanceof ModuleUpdateElementImpl);
                if (iue != null) {
                    installedModules.add((ModuleUpdateElementImpl)iuei);
                } else {
                    LOG.log(Level.FINER, this.getCodeName() + " misses required module " + moduleImpl.getUpdateElement());
                    missingModules.add(moduleImpl);
                }
                if (moduleImpl.getUpdateUnit().getAvailableUpdates().isEmpty()) continue;
                aue = moduleImpl.getUpdateUnit().getAvailableUpdates().get(0);
                auei = Trampoline.API.impl(aue);
                assert (auei instanceof ModuleUpdateElementImpl);
                availableModules.add((ModuleUpdateElementImpl)auei);
                LOG.log(Level.FINER, this + " has a update of module " + moduleImpl.getUpdateElement() + " to " + auei.getUpdateElement());
            }
            for (FeatureUpdateElementImpl dependingFeatureImpl : featureImpl.getDependingFeatures()) {
                installed |= dependingFeatureImpl.getUpdateUnit().getInstalled() != null;
                iue = dependingFeatureImpl.getUpdateUnit().getInstalled();
                UpdateElementImpl updateElementImpl = iuei = iue == null ? null : Trampoline.API.impl(iue);
                assert (iuei == null || iuei instanceof FeatureUpdateElementImpl);
                if (iuei != null) {
                    installedFeatures.add((FeatureUpdateElementImpl)iuei);
                } else {
                    LOG.log(Level.FINER, this.getCodeName() + " misses required module " + featureImpl.getUpdateElement());
                    missingFeatures.add(featureImpl);
                }
                if (dependingFeatureImpl.getUpdateUnit().getAvailableUpdates().isEmpty()) continue;
                aue = dependingFeatureImpl.getUpdateUnit().getAvailableUpdates().get(0);
                auei = Trampoline.API.impl(aue);
                assert (auei instanceof FeatureUpdateElementImpl);
                availableFeatures.add((FeatureUpdateElementImpl)auei);
                LOG.log(Level.FINER, this + " has a update of feature " + dependingFeatureImpl.getUpdateElement() + " to " + auei.getUpdateElement());
            }
            if (!installed) continue;
            installedFeatureElement = el;
        }
        boolean bl = isStandalone = UpdateManager.TYPE.STANDALONE_MODULE == this.getType();
        if (installedFeatureElement != null) {
            item = ArtificialFeaturesProvider.createFeatureItem(this.getCodeName(), installedModules, installedFeatures, featureImpl, isStandalone ? null : FeatureUpdateUnitImpl.presentAddionallyDescription(installedModules, FeatureUpdateUnitImpl.presentMissingModules(missingModules)));
            FeatureUpdateElementImpl featureElementImpl = new FeatureUpdateElementImpl(item, installedFeatureElement.getSource(), installedModules, installedFeatures, featureImpl.getType());
            this.installedElement = Trampoline.API.createUpdateElement(featureElementImpl);
            featureElementImpl.setUpdateUnit(installedFeatureElement.getUpdateUnit());
        }
        if (!(featureElements.isEmpty() || availableModules.isEmpty() && availableFeatures.isEmpty())) {
            missingModules.addAll(availableModules);
            item = ArtificialFeaturesProvider.createFeatureItem(this.getCodeName(), availableModules, availableFeatures, featureImpl, isStandalone ? null : FeatureUpdateUnitImpl.presentAddionallyDescription(FeatureUpdateUnitImpl.presentUpdatableModules(missingModules), installedModules));
            FeatureUpdateElementImpl featureElementImpl = new FeatureUpdateElementImpl(item, featureElements.get(0).getSource(), availableModules, availableFeatures, featureImpl.getType());
            this.updateElement = Trampoline.API.createUpdateElement(featureElementImpl);
            featureElementImpl.setUpdateUnit(featureElements.get(0).getUpdateUnit());
            this.addUpdate(this.updateElement);
        }
        this.initialized = true;
    }

    @Override
    public void setInstalled(UpdateElement installed) {
        assert (false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setAsUninstalled() {
        FeatureUpdateUnitImpl featureUpdateUnitImpl = this;
        synchronized (featureUpdateUnitImpl) {
            this.initialized = false;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void updateInstalled(UpdateElement installed) {
        FeatureUpdateUnitImpl featureUpdateUnitImpl = this;
        synchronized (featureUpdateUnitImpl) {
            this.initialized = false;
        }
    }

    private static String getDisplayNames(Set<ModuleUpdateElementImpl> moduleImpls) {
        assert (moduleImpls != null && !moduleImpls.isEmpty());
        String res = "";
        for (ModuleUpdateElementImpl moduleImpl : moduleImpls) {
            res = res + (res.length() == 0 ? "" : ", ") + moduleImpl.getDisplayName();
        }
        return res;
    }

    private static String presentMissingModules(Set<ModuleUpdateElementImpl> missingModuleImpls) {
        if (missingModuleImpls.isEmpty()) {
            return "";
        }
        boolean once = missingModuleImpls.size() == 1;
        String res = once ? NbBundle.getMessage(FeatureUpdateUnitImpl.class, (String)"FeatureUpdateUnitImpl_MissingModule", (Object)FeatureUpdateUnitImpl.getDisplayNames(missingModuleImpls)) : NbBundle.getMessage(FeatureUpdateUnitImpl.class, (String)"FeatureUpdateUnitImpl_MissingModules", (Object)FeatureUpdateUnitImpl.getDisplayNames(missingModuleImpls));
        return res;
    }

    private static String presentUpdatableModules(Set<ModuleUpdateElementImpl> updatebleModuleImpls) {
        if (updatebleModuleImpls.isEmpty()) {
            return "";
        }
        boolean once = updatebleModuleImpls.size() == 1;
        String res = once ? NbBundle.getMessage(FeatureUpdateUnitImpl.class, (String)"FeatureUpdateUnitImpl_UpdatableModule", (Object)FeatureUpdateUnitImpl.getDisplayNames(updatebleModuleImpls)) : NbBundle.getMessage(FeatureUpdateUnitImpl.class, (String)"FeatureUpdateUnitImpl_UpdatableModules", (Object)FeatureUpdateUnitImpl.getDisplayNames(updatebleModuleImpls));
        return res;
    }

    private static String presentIncludedModules(Set<ModuleUpdateElementImpl> includedModuleImpls) {
        if (includedModuleImpls.isEmpty()) {
            return "";
        }
        boolean once = includedModuleImpls.size() == 1;
        String res = once ? NbBundle.getMessage(FeatureUpdateUnitImpl.class, (String)"FeatureUpdateUnitImpl_ContainedModule", (Object)FeatureUpdateUnitImpl.getDisplayNames(includedModuleImpls)) : NbBundle.getMessage(FeatureUpdateUnitImpl.class, (String)"FeatureUpdateUnitImpl_ContainedModules", (Object)FeatureUpdateUnitImpl.getDisplayNames(includedModuleImpls));
        return res;
    }

    private static String presentAddionallyDescription(Set<ModuleUpdateElementImpl> included, String more) {
        String add = FeatureUpdateUnitImpl.presentIncludedModules(included) + more;
        return add.length() > 0 ? add : null;
    }

    private static String presentAddionallyDescription(String more, Set<ModuleUpdateElementImpl> included) {
        String add = more + FeatureUpdateUnitImpl.presentIncludedModules(included);
        return add.length() > 0 ? add : null;
    }

    @Override
    public boolean isPending() {
        return UpdateUnitFactory.getDefault().isScheduledForRestart(this.getUpdateUnit());
    }

    @Override
    public UpdateUnit getVisibleAncestor() {
        return this.getUpdateUnit();
    }
}

