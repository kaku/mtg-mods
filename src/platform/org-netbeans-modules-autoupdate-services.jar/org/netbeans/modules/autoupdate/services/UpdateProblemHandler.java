/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.Dependency
 */
package org.netbeans.modules.autoupdate.services;

import java.util.Set;
import org.netbeans.api.autoupdate.UpdateElement;
import org.openide.modules.Dependency;

public abstract class UpdateProblemHandler {
    public boolean ignoreBrokenDependency(Dependency dependency) {
        return true;
    }

    public boolean addRequiredElements(Set<UpdateElement> elements) {
        return true;
    }

    public abstract boolean allowUntrustedUpdateElement(String var1, UpdateElement var2);

    public abstract boolean approveLicenseAgreement(String var1);

    public boolean restartNow() {
        return true;
    }
}

