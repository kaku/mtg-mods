/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.autoupdate.services;

import org.netbeans.api.autoupdate.UpdateManager;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.modules.autoupdate.services.ModuleUpdateElementImpl;
import org.netbeans.modules.autoupdate.updateprovider.ModuleItem;

public class KitModuleUpdateElementImpl
extends ModuleUpdateElementImpl {
    public KitModuleUpdateElementImpl(ModuleItem item, String providerName) {
        super(item, providerName);
    }

    @Override
    public UpdateManager.TYPE getType() {
        if (this.getUpdateUnit() == null) {
            return UpdateManager.TYPE.KIT_MODULE;
        }
        return this.getUpdateUnit().getType();
    }
}

