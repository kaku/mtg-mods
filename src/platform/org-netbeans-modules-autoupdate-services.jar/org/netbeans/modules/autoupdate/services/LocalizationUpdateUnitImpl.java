/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.autoupdate.services;

import java.util.logging.Logger;
import org.netbeans.api.autoupdate.UpdateManager;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.modules.autoupdate.services.UpdateUnitImpl;

public class LocalizationUpdateUnitImpl
extends UpdateUnitImpl {
    private Logger err;

    public LocalizationUpdateUnitImpl(String codename) {
        super(codename);
        this.err = Logger.getLogger(this.getClass().getName());
    }

    @Override
    public UpdateManager.TYPE getType() {
        return UpdateManager.TYPE.LOCALIZATION;
    }

    @Override
    public UpdateUnit getVisibleAncestor() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}

