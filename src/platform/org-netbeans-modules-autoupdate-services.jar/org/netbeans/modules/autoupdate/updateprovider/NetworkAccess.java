/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Cancellable
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.autoupdate.updateprovider;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.openide.util.Cancellable;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

public class NetworkAccess {
    private static final Logger err = Logger.getLogger(NetworkAccess.class.getName());
    private static final RequestProcessor NETWORK_ACCESS = new RequestProcessor("autoupdate-network-access", 10, false);

    private NetworkAccess() {
    }

    public static Task createNetworkAcessTask(URL url, int timeout, NetworkListener networkAcesssListener) {
        return new Task(url, timeout, networkAcesssListener);
    }

    public static void initSSL(HttpURLConnection httpCon) throws IOException {
        if (httpCon instanceof HttpsURLConnection) {
            HttpsURLConnection https = (HttpsURLConnection)httpCon;
            try {
                TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager(){

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }

                    @Override
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }
                }};
                SSLContext sslContext = SSLContext.getInstance("SSL");
                sslContext.init(null, trustAllCerts, new SecureRandom());
                https.setHostnameVerifier(new HostnameVerifier(){

                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                });
                https.setSSLSocketFactory(sslContext.getSocketFactory());
            }
            catch (KeyManagementException ex) {
                throw new IOException(ex);
            }
            catch (NoSuchAlgorithmException ex) {
                throw new IOException(ex);
            }
        }
    }

    public static interface NetworkListener {
        public void streamOpened(InputStream var1, int var2);

        public void accessCanceled();

        public void accessTimeOut();

        public void notifyException(Exception var1);
    }

    private static interface SizedConnection
    extends Callable<InputStream> {
        public int getContentLength();
    }

    public static class Task
    implements Cancellable {
        private URL url;
        private int timeout;
        private NetworkListener listener;
        private final ExecutorService es = Executors.newSingleThreadExecutor();
        private Future<InputStream> connect = null;
        private RequestProcessor.Task rpTask = null;

        private Task(URL url, int timeout, NetworkListener listener) {
            if (url == null) {
                throw new IllegalArgumentException("URL cannot be null.");
            }
            if (listener == null) {
                throw new IllegalArgumentException("NetworkListener cannot be null.");
            }
            this.url = url;
            this.timeout = timeout;
            this.listener = listener;
            this.postTask();
        }

        private void postTask() {
            final SizedConnection connectTask = this.createCallableNetwork(this.url, this.timeout);
            this.rpTask = NETWORK_ACCESS.post(new Runnable(){

                @Override
                public void run() {
                    Task.this.connect = Task.this.es.submit(connectTask);
                    try {
                        InputStream is = (InputStream)Task.this.connect.get(Task.this.timeout, TimeUnit.MILLISECONDS);
                        if (Task.this.connect.isDone()) {
                            Task.this.listener.streamOpened(is, connectTask.getContentLength());
                        } else if (Task.this.connect.isCancelled()) {
                            Task.this.listener.accessCanceled();
                        } else {
                            Task.this.listener.accessTimeOut();
                        }
                    }
                    catch (InterruptedException ix) {
                        Task.this.listener.notifyException(ix);
                    }
                    catch (ExecutionException ex) {
                        Throwable t = ex.getCause();
                        if (t != null && t instanceof Exception) {
                            Task.this.listener.notifyException((Exception)t);
                        } else {
                            Task.this.listener.notifyException(ex);
                        }
                    }
                    catch (CancellationException ex) {
                        Task.this.listener.accessCanceled();
                    }
                    catch (TimeoutException tx) {
                        IOException io = new IOException(NbBundle.getMessage(NetworkAccess.class, (String)"NetworkAccess_Timeout", (Object)Task.this.url));
                        io.initCause(tx);
                        Task.this.listener.notifyException(io);
                    }
                }
            });
        }

        public void waitFinished() {
            assert (this.rpTask != null);
            this.rpTask.waitFinished();
        }

        public boolean isFinished() {
            assert (this.rpTask != null);
            return this.rpTask.isFinished();
        }

        private SizedConnection createCallableNetwork(final URL url, final int timeout) {
            return new SizedConnection(){
                private int contentLength;

                @Override
                public int getContentLength() {
                    return this.contentLength;
                }

                @Override
                public InputStream call() throws Exception {
                    URLConnection conn = url.openConnection();
                    conn.setConnectTimeout(timeout);
                    conn.setReadTimeout(timeout);
                    if (conn instanceof HttpsURLConnection) {
                        NetworkAccess.initSSL((HttpsURLConnection)conn);
                    }
                    if (conn instanceof HttpURLConnection) {
                        String redirUrl;
                        conn.connect();
                        if (((HttpURLConnection)conn).getResponseCode() == 302 && null != (redirUrl = conn.getHeaderField("Location")) && !redirUrl.isEmpty()) {
                            URL redirectedUrl = new URL(redirUrl);
                            URLConnection connRedir = redirectedUrl.openConnection();
                            connRedir.setRequestProperty("User-Agent", "NetBeans");
                            connRedir.setConnectTimeout(timeout);
                            conn = (HttpURLConnection)connRedir;
                        }
                    }
                    InputStream is = conn.getInputStream();
                    this.contentLength = conn.getContentLength();
                    Map<String, List<String>> map = conn.getHeaderFields();
                    StringBuilder sb = new StringBuilder("Connection opened for:\n");
                    sb.append("    Url: ").append(conn.getURL()).append("\n");
                    for (String field : map.keySet()) {
                        sb.append("    ").append(field == null ? "Status" : field).append(": ").append(map.get(field)).append("\n");
                    }
                    sb.append("\n");
                    err.log(Level.FINE, sb.toString());
                    return new BufferedInputStream(is);
                }
            };
        }

        public boolean cancel() {
            return this.connect.cancel(true);
        }

    }

}

