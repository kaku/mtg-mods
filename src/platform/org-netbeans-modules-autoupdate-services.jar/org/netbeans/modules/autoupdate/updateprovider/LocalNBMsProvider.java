/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.autoupdate.updateprovider;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.autoupdate.UpdateUnitProvider;
import org.netbeans.modules.autoupdate.updateprovider.AutoupdateInfoParser;
import org.netbeans.spi.autoupdate.UpdateItem;
import org.netbeans.spi.autoupdate.UpdateProvider;
import org.xml.sax.SAXException;

public class LocalNBMsProvider
implements UpdateProvider {
    private String name;
    private File[] nbms;
    private static final Logger err = Logger.getLogger(LocalNBMsProvider.class.getName());

    public /* varargs */ LocalNBMsProvider(String name, File ... files) {
        this.nbms = files;
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getDisplayName() {
        return this.getName();
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public Map<String, UpdateItem> getUpdateItems() {
        HashMap<String, UpdateItem> res = new HashMap<String, UpdateItem>();
        for (int i = 0; i < this.nbms.length; ++i) {
            Map<String, UpdateItem> items = null;
            try {
                items = AutoupdateInfoParser.getUpdateItems(this.nbms[i]);
            }
            catch (IOException ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }
            catch (SAXException ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }
            assert (items != null);
            if (items.size() != 1) {
                err.log(Level.INFO, "File " + this.nbms[i] + " contains not single items: " + items);
            }
            for (String id : items.keySet()) {
                res.put(id, items.get(id));
            }
        }
        return res;
    }

    @Override
    public boolean refresh(boolean force) {
        assert (false);
        return false;
    }

    @Override
    public UpdateUnitProvider.CATEGORY getCategory() {
        return UpdateUnitProvider.CATEGORY.COMMUNITY;
    }
}

