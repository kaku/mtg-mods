/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$Status
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 */
package org.netbeans.modules.autoupdate.updateprovider;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import org.netbeans.api.autoupdate.UpdateUnitProvider;
import org.netbeans.modules.autoupdate.updateprovider.AutoupdateCatalogProvider;
import org.netbeans.modules.autoupdate.updateprovider.ProviderCategory;
import org.netbeans.spi.autoupdate.UpdateProvider;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

public class AutoupdateCatalogFactory {
    private static final Logger err = Logger.getLogger("org.netbeans.modules.autoupdate.updateproviders.AutoupdateCatalogFactory");
    private static final String UPDATE_VERSION_PROP = "netbeans.autoupdate.version";
    private static final String UPDATE_VERSION = "1.23";
    private static final String IDE_HASH_CODE = "netbeans.hash.code";
    private static final String SYSPROP_COUNTRY = "netbeans.autoupdate.country";
    private static final String SYSPROP_LANGUAGE = "netbeans.autoupdate.language";
    private static final String SYSPROP_VARIANT = "netbeans.autoupdate.variant";
    private static final String PROP_QUALIFIED_IDENTITY = "qualifiedId";
    public static final String ORIGINAL_URL = "originalUrl";
    public static final String ORIGINAL_DISPLAY_NAME = "originalDisplayName";
    public static final String ORIGINAL_ENABLED = "originalEnabled";
    public static final String ORIGINAL_CATEGORY_NAME = "originalCategoryName";
    public static final String ORIGINAL_CATEGORY_ICON_BASE = "originalCategoryIconBase";

    private AutoupdateCatalogFactory() {
    }

    public static UpdateProvider createUpdateProvider(FileObject fo) {
        UpdateUnitProvider.CATEGORY category;
        ProviderCategory pc;
        String name;
        URL url;
        String sKey = (String)fo.getAttribute("url_key");
        if (sKey != null) {
            err.log(Level.WARNING, "{0}: url_key attribute deprecated in favor of url", fo.getPath());
            String remoteBundleName = (String)fo.getAttribute("SystemFileSystem.localizingBundle");
            assert (remoteBundleName != null);
            String localizedValue = null;
            ResourceBundle bundle = null;
            try {
                if (remoteBundleName == null) {
                    err.log(Level.WARNING, "Cannot find Bundle to read 'url_key' attribute {0}, trying use the 'url_key' itself...", sKey);
                    localizedValue = sKey;
                } else {
                    bundle = NbBundle.getBundle((String)remoteBundleName);
                    localizedValue = bundle.getString(sKey);
                }
                url = new URL(localizedValue);
            }
            catch (MissingResourceException mre) {
                assert (false);
                return null;
            }
            catch (MalformedURLException urlex) {
                assert (false);
                return null;
            }
            name = sKey;
        } else {
            Object o = fo.getAttribute("url");
            try {
                url = o instanceof String ? new URL((String)o) : (URL)o;
            }
            catch (MalformedURLException urlex) {
                err.log(Level.INFO, urlex.getMessage(), urlex);
                return null;
            }
            name = fo.getName();
        }
        if (url == null) {
            return null;
        }
        url = AutoupdateCatalogFactory.modifyURL(url);
        String categoryName = (String)fo.getAttribute("category");
        try {
            category = categoryName == null ? UpdateUnitProvider.CATEGORY.COMMUNITY : UpdateUnitProvider.CATEGORY.valueOf(categoryName);
        }
        catch (IllegalArgumentException ex) {
            category = null;
        }
        String categoryIconBase = (String)fo.getAttribute("iconBase");
        Preferences providerPreferences = AutoupdateCatalogFactory.getPreferences().node(name);
        if (category == null) {
            if (categoryName == null || categoryIconBase == null) {
                throw new IllegalStateException("Provide category and iconBase for " + (Object)fo);
            }
            pc = ProviderCategory.create(categoryIconBase, categoryName);
            providerPreferences.put("originalCategoryIconBase", categoryIconBase);
        } else {
            pc = ProviderCategory.forValue(category);
        }
        AutoupdateCatalogProvider au_catalog = new AutoupdateCatalogProvider(name, AutoupdateCatalogFactory.displayName(fo), url, pc);
        providerPreferences.put("originalUrl", url.toExternalForm());
        providerPreferences.put("originalDisplayName", au_catalog.getDisplayName());
        providerPreferences.put("originalCategoryName", au_catalog.getProviderCategory().getName());
        providerPreferences.put("originalCategoryIconBase", au_catalog.getProviderCategory().getIconBase());
        Boolean en = (Boolean)fo.getAttribute("enabled");
        if (en != null) {
            providerPreferences.putBoolean("originalEnabled", en);
        }
        return au_catalog;
    }

    @Deprecated
    public static Object createXMLAutoupdateType(FileObject fo) throws IOException {
        return AutoupdateCatalogFactory.createUpdateProvider(fo);
    }

    private static String displayName(FileObject fo) {
        String displayName = null;
        if (fo != null) {
            try {
                FileSystem fs = fo.getFileSystem();
                FileSystem.Status s = fs.getStatus();
                String x = s.annotateName("", Collections.singleton(fo));
                if (!x.isEmpty()) {
                    displayName = x;
                }
            }
            catch (FileStateInvalidException e) {
                // empty catch block
            }
        }
        if (displayName == null) {
            displayName = NbBundle.getMessage(AutoupdateCatalogFactory.class, (String)"CTL_CatalogUpdatesProviderFactory_DefaultName");
        }
        return displayName;
    }

    private static Preferences getPreferences() {
        return NbPreferences.root().node("/org/netbeans/modules/autoupdate");
    }

    private static URL modifyURL(URL original) {
        URL updateURL = null;
        if (System.getProperty("netbeans.autoupdate.version") == null) {
            System.setProperty("netbeans.autoupdate.version", "1.23");
        }
        if (System.getProperty("netbeans.hash.code") == null) {
            String id = AutoupdateCatalogFactory.getPreferences().get("qualifiedId", null);
            if (id == null) {
                Logger.getLogger(AutoupdateCatalogFactory.class.getName()).fine("Property PROP_IDE_IDENTITY hasn't been initialized yet.");
                id = "";
            }
            String prefix = NbBundle.getMessage(AutoupdateCatalogFactory.class, (String)"URL_Prefix_Hash_Code");
            System.setProperty("netbeans.hash.code", "".equals(id) ? prefix + "0" : prefix + id);
            assert (!id.startsWith("-n+"));
        }
        try {
            updateURL = new URL(AutoupdateCatalogFactory.encode(AutoupdateCatalogFactory.replace(original.toString())));
        }
        catch (MalformedURLException urlex) {
            err.log(Level.INFO, urlex.getMessage(), urlex);
        }
        return updateURL;
    }

    private static String encode(String stringURL) {
        String rval = stringURL;
        int q = stringURL.indexOf(63);
        if (q > 0) {
            StringBuilder buf = new StringBuilder(stringURL.substring(0, q + 1));
            StringTokenizer st = new StringTokenizer(stringURL.substring(q + 1), "&");
            while (st.hasMoreTokens()) {
                String a = st.nextToken();
                try {
                    int ei = a.indexOf(61);
                    if (ei < 0) {
                        buf.append(URLEncoder.encode(a, "UTF-8"));
                    } else {
                        buf.append(URLEncoder.encode(a.substring(0, ei), "UTF-8"));
                        buf.append('=');
                        String tna = a.substring(ei + 1);
                        int tni = tna.indexOf(37);
                        if (tni < 0) {
                            buf.append(URLEncoder.encode(tna, "UTF-8"));
                        } else {
                            buf.append(URLEncoder.encode(tna.substring(0, tni), "UTF-8"));
                            buf.append('%');
                            buf.append(URLEncoder.encode(tna.substring(tni + 1), "UTF-8"));
                        }
                    }
                }
                catch (UnsupportedEncodingException ex) {
                    Logger.getLogger(AutoupdateCatalogFactory.class.getName()).log(Level.INFO, ex.getMessage(), ex);
                }
                if (!st.hasMoreTokens()) continue;
                buf.append('&');
            }
            rval = buf.toString();
        }
        return rval;
    }

    private static String replace(String string) {
        AutoupdateCatalogFactory.setSystemProperties();
        if (string == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int prevIndex = 0;
        int index = 0;
        while ((index = string.indexOf(123, index)) != -1 && index < string.length() - 1) {
            if (string.charAt(index + 1) == '{' || string.charAt(index + 1) != '$') {
                ++index;
                continue;
            }
            sb.append(string.substring(prevIndex, index));
            int endBracketIndex = string.indexOf(125, index);
            if (endBracketIndex != -1) {
                String whatToReplace = string.substring(index + 2, endBracketIndex);
                sb.append(AutoupdateCatalogFactory.getReplacement(whatToReplace));
            }
            prevIndex = endBracketIndex == -1 ? index + 2 : endBracketIndex + 1;
            ++index;
        }
        if (prevIndex < string.length() - 1) {
            sb.append(string.substring(prevIndex));
        }
        return sb.toString();
    }

    private static void setSystemProperties() {
        if (System.getProperty("netbeans.autoupdate.country", null) == null) {
            System.setProperty("netbeans.autoupdate.country", Locale.getDefault().getCountry());
        }
        if (System.getProperty("netbeans.autoupdate.language", null) == null) {
            System.setProperty("netbeans.autoupdate.language", Locale.getDefault().getLanguage());
        }
        if (System.getProperty("netbeans.autoupdate.variant", null) == null) {
            System.setProperty("netbeans.autoupdate.variant", Locale.getDefault().getVariant());
        }
    }

    private static String getReplacement(String whatToReplace) {
        return System.getProperty(whatToReplace, "");
    }
}

