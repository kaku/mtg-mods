/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.updater.XMLUtil
 */
package org.netbeans.modules.autoupdate.updateprovider;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.netbeans.modules.autoupdate.services.Trampoline;
import org.netbeans.modules.autoupdate.services.UpdateLicenseImpl;
import org.netbeans.modules.autoupdate.services.Utilities;
import org.netbeans.modules.autoupdate.updateprovider.AutoupdateCatalogProvider;
import org.netbeans.modules.autoupdate.updateprovider.ModuleItem;
import org.netbeans.modules.autoupdate.updateprovider.UpdateItemImpl;
import org.netbeans.spi.autoupdate.UpdateItem;
import org.netbeans.spi.autoupdate.UpdateLicense;
import org.netbeans.updater.XMLUtil;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

public class AutoupdateCatalogParser
extends DefaultHandler {
    private final Map<String, UpdateItem> items;
    private final AutoupdateCatalogProvider provider;
    private final EntityResolver entityResolver;
    private final URI baseUri;
    private static final Logger ERR = Logger.getLogger(AutoupdateCatalogParser.class.getName());
    private static final String MODULE_UPDATES_ATTR_TIMESTAMP = "timestamp";
    private static final String MODULE_GROUP_ATTR_NAME = "name";
    private static final String NOTIFICATION_ATTR_URL = "url";
    private static final String CONTENT_DESCRIPTION_ATTR_URL = "url";
    private static final String LICENSE_ATTR_NAME = "name";
    private static final String MODULE_ATTR_CODE_NAME_BASE = "codenamebase";
    private static final String MODULE_ATTR_HOMEPAGE = "homepage";
    private static final String MODULE_ATTR_DISTRIBUTION = "distribution";
    private static final String MODULE_ATTR_DOWNLOAD_SIZE = "downloadsize";
    private static final String MODULE_ATTR_NEEDS_RESTART = "needsrestart";
    private static final String MODULE_ATTR_MODULE_AUTHOR = "moduleauthor";
    private static final String MODULE_ATTR_RELEASE_DATE = "releasedate";
    private static final String MODULE_ATTR_IS_GLOBAL = "global";
    private static final String MODULE_ATTR_IS_PREFERRED_UPDATE = "preferredupdate";
    private static final String MODULE_ATTR_TARGET_CLUSTER = "targetcluster";
    private static final String MODULE_ATTR_EAGER = "eager";
    private static final String MODULE_ATTR_AUTOLOAD = "autoload";
    private static final String MODULE_ATTR_LICENSE = "license";
    private static final String LICENSE_ATTR_URL = "url";
    private static final String MANIFEST_ATTR_SPECIFICATION_VERSION = "OpenIDE-Module-Specification-Version";
    private static final String TIME_STAMP_FORMAT = "ss/mm/hh/dd/MM/yyyy";
    private static final String L10N_ATTR_LOCALE = "langcode";
    private static final String L10N_ATTR_BRANDING = "brandingcode";
    private static final String L10N_ATTR_MODULE_SPECIFICATION = "module_spec_version";
    private static final String L10N_ATTR_MODULE_MAJOR_VERSION = "module_major_version";
    private static final String L10N_ATTR_LOCALIZED_MODULE_NAME = "OpenIDE-Module-Name";
    private static final String L10N_ATTR_LOCALIZED_MODULE_DESCRIPTION = "OpenIDE-Module-Long-Description";
    private static String GZIP_EXTENSION = ".gz";
    private static String XML_EXTENSION = ".xml";
    private static String GZIP_MIME_TYPE = "application/x-gzip";
    private Stack<String> currentGroup = new Stack();
    private String catalogDate;
    private Stack<ModuleDescriptor> currentModule = new Stack();
    private Stack<Map<String, String>> currentLicense = new Stack();
    private Stack<String> currentNotificationUrl = new Stack();
    private Stack<String> currentContentDescriptionUrl = new Stack();
    private Map<String, UpdateLicenseImpl> name2license = new HashMap<String, UpdateLicenseImpl>();
    private List<String> lines = new ArrayList<String>();
    private int bufferInitSize = 0;

    private AutoupdateCatalogParser(Map<String, UpdateItem> items, AutoupdateCatalogProvider provider, URI base) {
        this.items = items;
        this.provider = provider;
        this.entityResolver = this.newEntityResolver();
        this.baseUri = base;
    }

    private EntityResolver newEntityResolver() {
        try {
            Class.forName("org.netbeans.updater.XMLUtil");
        }
        catch (ClassNotFoundException e) {
            File netbeansHomeFile = new File(System.getProperty("netbeans.home"));
            File userdir = new File(System.getProperty("netbeans.user"));
            String updaterPath = "modules/ext/updater.jar";
            String newUpdaterPath = "update/new_updater/updater.jar";
            File updaterPlatform = new File(netbeansHomeFile, "modules/ext/updater.jar");
            File updaterUserdir = new File(userdir, "modules/ext/updater.jar");
            File newUpdaterPlatform = new File(netbeansHomeFile, "update/new_updater/updater.jar");
            File newUpdaterUserdir = new File(userdir, "update/new_updater/updater.jar");
            String message = "    org.netbeans.updater.XMLUtil is not accessible\n    platform dir = " + netbeansHomeFile.getAbsolutePath() + "\n" + "    userdir  dir = " + userdir.getAbsolutePath() + "\n" + "    updater in platform exist = " + this.updaterInfo(updaterPlatform) + "\n" + "    updater in userdir  exist = " + this.updaterInfo(updaterUserdir) + "\n" + "    new updater in platform exist = " + this.updaterInfo(newUpdaterPlatform) + "\n" + "    new updater in userdir  exist = " + this.updaterInfo(newUpdaterUserdir) + "\n";
            ERR.log(Level.WARNING, message);
        }
        return XMLUtil.createAUResolver();
    }

    private String updaterInfo(File updaterLocation) {
        StringBuilder sb = new StringBuilder();
        sb.append(updaterLocation.exists());
        if (updaterLocation.exists()) {
            try {
                sb.append(", length = ").append(updaterLocation.length()).append(" bytes");
                URLClassLoader url = new URLClassLoader(new URL[]{updaterLocation.toURI().toURL()});
                sb.append(", loading resource: ").append(url.getResource("org/netbeans/updater/XMLUtil.class"));
                sb.append(", loading class: ").append(url.loadClass("org.netbeans.updater.XMLUtil"));
            }
            catch (Throwable ex) {
                sb.append(", exception: ").append(ex.getMessage());
            }
        }
        return sb.toString();
    }

    public static synchronized Map<String, UpdateItem> getUpdateItems(URL url, AutoupdateCatalogProvider provider) throws IOException {
        HashMap<String, UpdateItem> items = new HashMap<String, UpdateItem>();
        try {
            URI base = provider != null ? provider.getUpdateCenterURL().toURI() : url.toURI();
            InputSource is = null;
            try {
                SAXParserFactory factory = SAXParserFactory.newInstance();
                factory.setValidating(true);
                SAXParser saxParser = factory.newSAXParser();
                is = AutoupdateCatalogParser.getInputSource(url, provider, base);
                saxParser.parse(is, (DefaultHandler)new AutoupdateCatalogParser(items, provider, base));
            }
            catch (Exception ex) {
                throw new IOException("Failed to parse " + base, ex);
            }
            finally {
                if (is != null && is.getByteStream() != null) {
                    try {
                        is.getByteStream().close();
                    }
                    catch (IOException e) {}
                }
            }
        }
        catch (URISyntaxException ex) {
            ERR.log(Level.INFO, null, ex);
        }
        return items;
    }

    private static boolean isGzip(AutoupdateCatalogProvider p) {
        boolean res = false;
        if (p != null) {
            URL url = p.getUpdateCenterURL();
            if (url != null) {
                boolean isXML;
                String path = url.getPath().toLowerCase();
                res = path.endsWith(GZIP_EXTENSION);
                if (!res && !(isXML = path.endsWith(XML_EXTENSION))) {
                    try {
                        URLConnection conn = url.openConnection();
                        String contentType = conn.getContentType();
                        res = GZIP_MIME_TYPE.equals(contentType);
                    }
                    catch (IOException ex) {
                        ERR.log(Level.INFO, "Cannot read Content-Type HTTP header, using file extension, cause: ", ex);
                    }
                }
                ERR.log(Level.FINER, "Is GZIP " + url + " ? " + res);
            } else {
                ERR.log(Level.WARNING, "AutoupdateCatalogProvider has not URL.");
            }
        }
        return res;
    }

    private static InputSource getInputSource(URL toParse, AutoupdateCatalogProvider p, URI base) {
        InputStream is = null;
        try {
            is = toParse.openStream();
            if (AutoupdateCatalogParser.isGzip(p)) {
                try {
                    is = new GZIPInputStream(is);
                }
                catch (IOException e) {
                    ERR.log(Level.INFO, "The file at " + toParse + ", corresponding to the catalog at " + p.getUpdateCenterURL() + ", does not look like the gzip file, trying to parse it as the pure xml", e);
                    is.close();
                    is = toParse.openStream();
                }
            }
            InputSource src = new InputSource(new BufferedInputStream(is));
            src.setSystemId(base.toString());
            return src;
        }
        catch (IOException ex) {
            if (is != null) {
                try {
                    is.close();
                }
                catch (IOException e) {
                    // empty catch block
                }
            }
            ERR.log(Level.SEVERE, "Cannot estabilish input stream for {0}", toParse);
            ERR.log(Level.INFO, "Parsing exception", ex);
            return new InputSource();
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        this.lines.add(new String(ch, start, length));
        this.bufferInitSize += length;
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (ELEMENTS.valueOf(qName)) {
            case module_updates: {
                break;
            }
            case module_group: {
                assert (!this.currentGroup.empty());
                this.currentGroup.pop();
                break;
            }
            case module: {
                assert (!this.currentModule.empty());
                this.currentModule.pop();
                break;
            }
            case l10n: {
                break;
            }
            case manifest: {
                break;
            }
            case description: {
                ERR.info("Not supported yet.");
                break;
            }
            case notification: {
                if (this.provider != null && !this.lines.isEmpty()) {
                    StringBuilder sb = new StringBuilder(this.bufferInitSize);
                    for (String line : this.lines) {
                        sb.append(line);
                    }
                    String notification = sb.toString();
                    String notificationUrl = this.currentNotificationUrl.peek();
                    notification = notificationUrl != null && notificationUrl.length() > 0 ? notification + (notification.length() > 0 ? "<br>" : "") + "<a name=\"autoupdate_catalog_parser\" href=\"" + notificationUrl + "\">" + notificationUrl + "</a>" : notification + (notification.length() > 0 ? "<br>" : "") + "<a name=\"autoupdate_catalog_parser\"/>";
                    this.provider.setNotification(notification);
                }
                this.currentNotificationUrl.pop();
                break;
            }
            case content_description: {
                if (this.provider != null && !this.lines.isEmpty()) {
                    StringBuilder sb = new StringBuilder(this.bufferInitSize);
                    for (String line : this.lines) {
                        sb.append(line);
                    }
                    String contentDescription = sb.toString();
                    String contentDescriptionUrl = this.currentContentDescriptionUrl.peek();
                    if (contentDescriptionUrl != null && contentDescriptionUrl.length() > 0) {
                        contentDescription = "<a name=\"update_center_content_description\" href=\"" + contentDescriptionUrl + "\">" + contentDescription + "</a>";
                    }
                    this.provider.setContentDescription(contentDescription);
                }
                this.currentContentDescriptionUrl.pop();
                break;
            }
            case module_notification: {
                if (this.lines.isEmpty()) break;
                ModuleDescriptor md = this.currentModule.peek();
                assert (md != null);
                StringBuilder buf = new StringBuilder(this.bufferInitSize);
                for (String line : this.lines) {
                    buf.append(line);
                }
                md.appendNotification(buf.toString());
                break;
            }
            case external_package: {
                ERR.info("Not supported yet.");
                break;
            }
            case license: {
                assert (!this.currentLicense.empty());
                Map<String, String> curLic = this.currentLicense.peek();
                String licenseName = curLic.keySet().iterator().next();
                Collection<String> values = curLic.values();
                String licenseUrl = values.size() > 0 ? values.iterator().next() : null;
                UpdateLicenseImpl updateLicenseImpl = this.name2license.get(licenseName);
                if (updateLicenseImpl == null) {
                    ERR.info("Unpaired license " + licenseName + " without any module.");
                } else if (!this.lines.isEmpty()) {
                    StringBuilder sb = new StringBuilder(this.bufferInitSize);
                    for (String line : this.lines) {
                        sb.append(line);
                    }
                    updateLicenseImpl.setAgreement(sb.toString());
                } else if (licenseUrl != null) {
                    updateLicenseImpl.setUrl(AutoupdateCatalogParser.getDistribution(licenseUrl, this.baseUri));
                }
                this.currentLicense.pop();
                break;
            }
            default: {
                ERR.warning("Unknown element " + qName);
            }
        }
    }

    @Override
    public void endDocument() throws SAXException {
        ERR.fine("End parsing " + (this.provider == null ? "" : this.provider.getUpdateCenterURL()) + " at " + System.currentTimeMillis());
    }

    @Override
    public void startDocument() throws SAXException {
        ERR.fine("Start parsing " + (this.provider == null ? "" : this.provider.getUpdateCenterURL()) + " at " + System.currentTimeMillis());
    }

    @Override
    public void startElement(String uri, String localName, String qName, org.xml.sax.Attributes attributes) throws SAXException {
        ELEMENTS elem;
        this.lines.clear();
        this.bufferInitSize = 0;
        try {
            elem = ELEMENTS.valueOf(qName);
        }
        catch (IllegalArgumentException ex) {
            throw new SAXException("Wrong element " + qName);
        }
        switch (elem) {
            case module_updates: {
                try {
                    this.catalogDate = "";
                    SimpleDateFormat format = new SimpleDateFormat("ss/mm/hh/dd/MM/yyyy");
                    String timeStamp = attributes.getValue("timestamp");
                    if (timeStamp == null) {
                        ERR.info("No timestamp is presented in " + (this.provider == null ? "" : this.provider.getUpdateCenterURL()));
                        break;
                    }
                    this.catalogDate = Utilities.formatDate(format.parse(timeStamp));
                    ERR.finer("Successfully read time " + timeStamp);
                }
                catch (ParseException pe) {
                    ERR.log(Level.INFO, null, pe);
                }
                break;
            }
            case module_group: {
                this.currentGroup.push(attributes.getValue("name"));
                break;
            }
            case module: {
                ModuleDescriptor md = ModuleDescriptor.getModuleDescriptor(this.currentGroup.size() > 0 ? this.currentGroup.peek() : null, this.baseUri, this.catalogDate);
                md.appendModuleAttributes(attributes);
                this.currentModule.push(md);
                break;
            }
            case l10n: {
                break;
            }
            case manifest: {
                ModuleDescriptor desc = this.currentModule.peek();
                desc.appendManifest(attributes);
                UpdateItem m = desc.createUpdateItem();
                UpdateItemImpl impl = Trampoline.SPI.impl(m);
                String licName = impl.getUpdateLicenseImpl().getName();
                if (this.name2license.keySet().contains(licName)) {
                    impl.setUpdateLicenseImpl(this.name2license.get(licName));
                } else {
                    this.name2license.put(impl.getUpdateLicenseImpl().getName(), impl.getUpdateLicenseImpl());
                }
                this.items.put(desc.getId(), m);
                break;
            }
            case description: {
                ERR.info("Not supported yet.");
                break;
            }
            case module_notification: {
                break;
            }
            case notification: {
                this.currentNotificationUrl.push(attributes.getValue("url"));
                break;
            }
            case content_description: {
                this.currentContentDescriptionUrl.push(attributes.getValue("url"));
                break;
            }
            case external_package: {
                ERR.info("Not supported yet.");
                break;
            }
            case license: {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(attributes.getValue("name"), attributes.getValue("url"));
                this.currentLicense.push(map);
                break;
            }
            default: {
                ERR.warning("Unknown element " + qName);
            }
        }
    }

    @Override
    public void warning(SAXParseException e) throws SAXException {
        this.parseError(e);
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        this.parseError(e);
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        this.parseError(e);
    }

    private void parseError(SAXParseException e) {
        ERR.warning(e.getSystemId() + ":" + e.getLineNumber() + ":" + e.getColumnNumber() + ": " + e.getLocalizedMessage());
    }

    @Override
    public InputSource resolveEntity(String publicId, String systemId) throws IOException, SAXException {
        return this.entityResolver.resolveEntity(publicId, systemId);
    }

    private static URL getDistribution(String distribution, URI base) {
        URL retval = null;
        if (distribution != null && distribution.length() > 0) {
            try {
                URI distributionURI = new URI(distribution);
                if (!distributionURI.isAbsolute() && base != null) {
                    distributionURI = base.resolve(distributionURI);
                }
                retval = distributionURI.toURL();
            }
            catch (MalformedURLException ex) {
                ERR.log(Level.INFO, null, ex);
            }
            catch (URISyntaxException ex) {
                ERR.log(Level.INFO, null, ex);
            }
        }
        return retval;
    }

    private static Manifest getManifest(org.xml.sax.Attributes attrList) {
        Manifest mf = new Manifest();
        Attributes mfAttrs = mf.getMainAttributes();
        for (int i = 0; i < attrList.getLength(); ++i) {
            mfAttrs.put(new Attributes.Name(attrList.getQName(i)), attrList.getValue(i));
        }
        return mf;
    }

    private static class ModuleDescriptor {
        private String moduleCodeName;
        private URL distributionURL;
        private String targetcluster;
        private String homepage;
        private String downloadSize;
        private String author;
        private String publishDate;
        private String notification;
        private Boolean needsRestart;
        private Boolean isGlobal;
        private Boolean isEager;
        private Boolean isAutoload;
        private Boolean isPreferredUpdate;
        private String specVersion;
        private Manifest mf;
        private String id;
        private UpdateLicense lic;
        private String group;
        private URI base;
        private String catalogDate;
        private static ModuleDescriptor md = null;

        private ModuleDescriptor() {
        }

        public static ModuleDescriptor getModuleDescriptor(String group, URI base, String catalogDate) {
            if (md == null) {
                md = new ModuleDescriptor();
            }
            ModuleDescriptor.md.group = group;
            ModuleDescriptor.md.base = base;
            ModuleDescriptor.md.catalogDate = catalogDate;
            return md;
        }

        public void appendModuleAttributes(org.xml.sax.Attributes module) {
            this.moduleCodeName = module.getValue("codenamebase");
            this.distributionURL = AutoupdateCatalogParser.getDistribution(module.getValue("distribution"), this.base);
            this.targetcluster = module.getValue("targetcluster");
            this.homepage = module.getValue("homepage");
            this.downloadSize = module.getValue("downloadsize");
            this.author = module.getValue("moduleauthor");
            this.publishDate = module.getValue("releasedate");
            if (this.publishDate == null || this.publishDate.length() == 0) {
                this.publishDate = this.catalogDate;
            }
            String needsrestart = module.getValue("needsrestart");
            String global = module.getValue("global");
            String eager = module.getValue("eager");
            String autoload = module.getValue("autoload");
            String preferred = module.getValue("preferredupdate");
            this.needsRestart = needsrestart == null || needsrestart.trim().length() == 0 ? null : Boolean.valueOf(needsrestart);
            this.isGlobal = global == null || global.trim().length() == 0 ? null : Boolean.valueOf(global);
            this.isEager = Boolean.parseBoolean(eager);
            this.isAutoload = Boolean.parseBoolean(autoload);
            this.isPreferredUpdate = Boolean.parseBoolean(preferred);
            if (this.isPreferredUpdate.booleanValue()) {
                Utilities.writeFirstClassModule(this.moduleCodeName);
            }
            String licName = module.getValue("license");
            this.lic = UpdateLicense.createUpdateLicense(licName, null);
        }

        public void appendManifest(org.xml.sax.Attributes manifest) {
            this.specVersion = manifest.getValue("OpenIDE-Module-Specification-Version");
            this.mf = AutoupdateCatalogParser.getManifest(manifest);
            this.id = this.moduleCodeName + '_' + this.specVersion;
        }

        public void appendNotification(String notification) {
            this.notification = notification;
        }

        public String getId() {
            return this.id;
        }

        public UpdateItem createUpdateItem() {
            UpdateItem res = UpdateItem.createModule(this.moduleCodeName, this.specVersion, this.distributionURL, this.author, this.downloadSize, this.homepage, this.publishDate, this.group, this.mf, this.isEager, this.isAutoload, this.needsRestart, this.isGlobal, this.isPreferredUpdate, this.targetcluster, this.lic);
            UpdateItemImpl impl = Trampoline.SPI.impl(res);
            ((ModuleItem)impl).setModuleNotification(this.notification);
            this.cleanUp();
            return res;
        }

        private void cleanUp() {
            this.specVersion = null;
            this.mf = null;
            this.notification = null;
        }
    }

    private static enum ELEMENTS {
        module_updates,
        module_group,
        notification,
        content_description,
        module,
        description,
        module_notification,
        external_package,
        manifest,
        l10n,
        license;
        

        private ELEMENTS() {
        }
    }

}

