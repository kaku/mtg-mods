/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.autoupdate.updateprovider;

import org.netbeans.modules.autoupdate.services.UpdateLicenseImpl;
import org.netbeans.spi.autoupdate.UpdateItem;

public abstract class UpdateItemImpl {
    private UpdateItem originalUpdateItem;

    UpdateItemImpl() {
    }

    public void setUpdateItem(UpdateItem item) {
        this.originalUpdateItem = item;
    }

    public UpdateItem getUpdateItem() {
        return this.originalUpdateItem;
    }

    public abstract String getCodeName();

    public abstract UpdateLicenseImpl getUpdateLicenseImpl();

    public abstract void setUpdateLicenseImpl(UpdateLicenseImpl var1);

    public abstract String getCategory();
}

