/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.Module
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.SpecificationVersion
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.autoupdate.updateprovider;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.Module;
import org.netbeans.api.autoupdate.UpdateUnitProvider;
import org.netbeans.modules.autoupdate.services.FeatureUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.ModuleUpdateElementImpl;
import org.netbeans.modules.autoupdate.services.UpdateElementImpl;
import org.netbeans.modules.autoupdate.services.Utilities;
import org.netbeans.modules.autoupdate.updateprovider.FeatureItem;
import org.netbeans.modules.autoupdate.updateprovider.InstalledModuleItem;
import org.netbeans.modules.autoupdate.updateprovider.ModuleItem;
import org.netbeans.modules.autoupdate.updateprovider.UpdateItemImpl;
import org.netbeans.spi.autoupdate.UpdateItem;
import org.netbeans.spi.autoupdate.UpdateProvider;
import org.openide.modules.ModuleInfo;
import org.openide.modules.SpecificationVersion;
import org.openide.util.NbBundle;

public class ArtificialFeaturesProvider
implements UpdateProvider {
    private static final String UNSORTED_CATEGORY = NbBundle.getMessage(ArtificialFeaturesProvider.class, (String)"ArtificialFeaturesProvider_Unsorted_Category");
    private static final String LIBRARIES_CATEGORY = NbBundle.getMessage(ArtificialFeaturesProvider.class, (String)"ArtificialFeaturesProvider_Libraries_Category");
    private static final String BRIDGES_CATEGORY = NbBundle.getMessage(ArtificialFeaturesProvider.class, (String)"ArtificialFeaturesProvider_Bridges_Category");
    private static final String FEATURES_CATEGORY = NbBundle.getMessage(ArtificialFeaturesProvider.class, (String)"ArtificialFeaturesProvider_Features_Category");
    private final Collection<UpdateItem> originalItems;
    private static final Logger log = Logger.getLogger(ArtificialFeaturesProvider.class.getName());
    private static ArtificialFeaturesProvider DUMMY;

    public static ArtificialFeaturesProvider getDummy() {
        if (DUMMY == null) {
            DUMMY = new ArtificialFeaturesProvider(null);
        }
        return DUMMY;
    }

    public ArtificialFeaturesProvider(Collection<UpdateItem> items) {
        this.originalItems = items;
    }

    @Override
    public String getName() {
        return "artificial-module-provider";
    }

    @Override
    public String getDisplayName() {
        return this.getName();
    }

    @Override
    public String getDescription() {
        return null;
    }

    private static boolean generateArtificialFeatures() {
        String tmp = System.getProperty("autoupdate.services.generate.features");
        return tmp != null && Boolean.valueOf(tmp) != false;
    }

    @Override
    public Map<String, UpdateItem> getUpdateItems() throws IOException {
        if (!ArtificialFeaturesProvider.generateArtificialFeatures()) {
            return Collections.emptyMap();
        }
        HashMap<String, UpdateItem> res = new HashMap<String, UpdateItem>();
        HashMap categoryToModules = new HashMap();
        for (UpdateItem item : this.originalItems) {
            String category;
            ModuleItem updateModule;
            UpdateItemImpl impl = Utilities.getUpdateItemImpl(item);
            if (impl instanceof InstalledModuleItem) {
                InstalledModuleItem installedModule = (InstalledModuleItem)impl;
                category = (String)installedModule.getModuleInfo().getLocalizedAttribute("OpenIDE-Module-Display-Category");
                Module module = Utilities.toModule(installedModule.getModuleInfo().getCodeNameBase(), installedModule.getModuleInfo().getSpecificationVersion());
                assert (module != null);
                if (module.isAutoload() || module.isFixed() || module.isEager()) continue;
                if (category == null || category.length() == 0) {
                    category = UNSORTED_CATEGORY;
                }
                if (!categoryToModules.containsKey(category)) {
                    categoryToModules.put(category, new HashSet());
                }
                ((Set)categoryToModules.get(category)).add(installedModule.getModuleInfo());
                continue;
            }
            if (!(impl instanceof ModuleItem) || LIBRARIES_CATEGORY.equals(category = (String)(updateModule = (ModuleItem)impl).getModuleInfo().getLocalizedAttribute("OpenIDE-Module-Display-Category")) || BRIDGES_CATEGORY.equals(category) || FEATURES_CATEGORY.equals(category)) continue;
            if (category == null || category.length() == 0) {
                String dn = (String)updateModule.getModuleInfo().getLocalizedAttribute("OpenIDE-Module-Display-Category");
                category = dn == null || dn.length() == 0 ? UNSORTED_CATEGORY : dn;
            }
            if (!categoryToModules.containsKey(category)) {
                categoryToModules.put(category, new HashSet());
            }
            ((Set)categoryToModules.get(category)).add(updateModule.getModuleInfo());
        }
        Iterator i$ = categoryToModules.keySet().iterator();
        if (i$.hasNext()) {
            String category = (String)i$.next();
            throw new UnsupportedOperationException("Not supported yet.");
        }
        return res;
    }

    @Override
    public boolean refresh(boolean force) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public static FeatureItem createFeatureItem(String codeName, Set<ModuleUpdateElementImpl> modules, Set<FeatureUpdateElementImpl> features, UpdateElementImpl original, String additionalDescription) {
        HashSet<String> containsModulesOrFeatures = new HashSet<String>();
        String versionN = "";
        for (ModuleUpdateElementImpl impl2 : modules) {
            ModuleInfo info = impl2.getModuleInfo();
            containsModulesOrFeatures.add(info.getCodeName() + " > " + (Object)info.getSpecificationVersion());
            SpecificationVersion spec = info.getSpecificationVersion();
            versionN = ArtificialFeaturesProvider.addVersion(versionN, spec);
        }
        for (FeatureUpdateElementImpl impl : features) {
            containsModulesOrFeatures.add(impl.getCodeName() + " > " + (Object)impl.getSpecificationVersion());
            SpecificationVersion spec = impl.getSpecificationVersion();
            versionN = ArtificialFeaturesProvider.addVersion(versionN, spec);
        }
        String description = original == null || original.getDescription() == null || original.getDescription().length() == 0 ? "" : original.getDescription();
        description = additionalDescription == null || additionalDescription.length() == 0 ? description : description + additionalDescription;
        String displayName = original == null || original.getDisplayName() == null || original.getDisplayName().length() == 0 ? codeName : original.getDisplayName();
        String version = original == null || original.getSpecificationVersion() == null ? versionN : original.getSpecificationVersion().toString();
        return new FeatureItem(codeName, version, containsModulesOrFeatures, displayName, description, null);
    }

    public static String createVersion(Collection<ModuleInfo> modules) {
        String version = "";
        for (ModuleInfo info : modules) {
            SpecificationVersion spec = info.getSpecificationVersion();
            version = ArtificialFeaturesProvider.addVersion(version, spec);
        }
        return version;
    }

    private static String addVersion(String version, SpecificationVersion spec) {
        int[] addend1 = ArtificialFeaturesProvider.getDigitsInVersion(version);
        int[] addend2 = ArtificialFeaturesProvider.getDigitsInVersion(spec.toString());
        int length = Math.max(addend1.length, addend2.length);
        int[] result = new int[length];
        for (int i = 0; i < result.length; ++i) {
            assert (i < addend1.length || i < addend2.length);
            int digit = 0;
            if (i < addend1.length) {
                digit += addend1[i];
            }
            if (i < addend2.length) {
                digit += addend2[i];
            }
            result[i] = digit;
        }
        StringBuilder buf = new StringBuilder(result.length * 3 + 1);
        for (int i2 = 0; i2 < result.length; ++i2) {
            if (i2 > 0) {
                buf.append('.');
            }
            buf.append(result[i2]);
        }
        return buf.toString();
    }

    private static int[] getDigitsInVersion(String version) {
        if (version.length() == 0) {
            return new int[0];
        }
        StringTokenizer tok = new StringTokenizer(version, ".", true);
        int len = tok.countTokens();
        assert (len % 2 != 0);
        int[] digits = new int[len / 2 + 1];
        int i = 0;
        boolean expectingNumber = true;
        while (tok.hasMoreTokens()) {
            String toParse = tok.nextToken();
            if (expectingNumber) {
                expectingNumber = false;
                try {
                    int piece = Integer.parseInt(toParse);
                    assert (piece >= 0);
                    digits[i++] = piece;
                }
                catch (NumberFormatException nfe) {
                    log.log(Level.INFO, "NumberFormatException while parsing " + version, nfe);
                }
                continue;
            }
            assert (".".equals(toParse));
            expectingNumber = true;
        }
        return digits;
    }

    @Override
    public UpdateUnitProvider.CATEGORY getCategory() {
        return UpdateUnitProvider.CATEGORY.COMMUNITY;
    }
}

