/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.Module
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.SpecificationVersion
 */
package org.netbeans.modules.autoupdate.updateprovider;

import java.net.URL;
import java.text.ParseException;
import java.util.Date;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.Module;
import org.netbeans.modules.autoupdate.services.UpdateItemDeploymentImpl;
import org.netbeans.modules.autoupdate.services.UpdateLicenseImpl;
import org.netbeans.modules.autoupdate.services.Utilities;
import org.netbeans.modules.autoupdate.updateprovider.DummyModuleInfo;
import org.netbeans.modules.autoupdate.updateprovider.UpdateItemImpl;
import org.netbeans.spi.autoupdate.CustomInstaller;
import org.netbeans.spi.autoupdate.CustomUninstaller;
import org.openide.modules.ModuleInfo;
import org.openide.modules.SpecificationVersion;

public class ModuleItem
extends UpdateItemImpl {
    private String codeName;
    private String specificationVersion;
    private ModuleInfo info;
    private String author;
    private String downloadSize;
    private String homepage;
    private String category;
    private Date publishDate;
    private boolean isEager;
    private boolean isAutoload;
    private boolean isPreferedUpdate;
    private String moduleNotification = null;
    private URL distribution;
    private Manifest manifest;
    private UpdateItemDeploymentImpl deployImpl;
    private UpdateLicenseImpl licenseImpl;

    protected ModuleItem() {
    }

    public ModuleItem(String codeName, String specificationVersion, URL distribution, String author, String publishDateString, String downloadSize, String homepage, String category, Manifest manifest, Boolean isEager, Boolean isAutoload, Boolean needsRestart, Boolean isGlobal, Boolean isPrefered, String targetCluster, UpdateLicenseImpl licenseImpl) {
        this.codeName = codeName;
        this.specificationVersion = specificationVersion;
        this.distribution = distribution;
        this.manifest = manifest;
        this.deployImpl = new UpdateItemDeploymentImpl(needsRestart, isGlobal, targetCluster, null, null);
        if (publishDateString != null && publishDateString.length() > 0) {
            try {
                this.publishDate = Utilities.parseDate(publishDateString);
            }
            catch (ParseException pe) {
                Logger.getLogger(ModuleItem.class.getName()).log(Level.INFO, "Parsing \"" + publishDateString + "\" of " + codeName + " throws " + pe.getMessage(), pe);
            }
            catch (RuntimeException re) {
                Logger.getLogger(ModuleItem.class.getName()).log(Level.INFO, "Parsing \"" + publishDateString + "\" of " + codeName + " throws " + re.getMessage(), re);
            }
        }
        this.licenseImpl = licenseImpl;
        this.author = author;
        this.downloadSize = downloadSize;
        this.homepage = homepage;
        this.category = category;
        this.isEager = isEager;
        this.isAutoload = isAutoload;
        this.isPreferedUpdate = isPrefered;
    }

    @Override
    public String getCodeName() {
        return this.codeName;
    }

    public String getSpecificationVersion() {
        return this.specificationVersion;
    }

    public URL getDistribution() {
        return this.distribution;
    }

    public String getAuthor() {
        return this.author;
    }

    public String getHomepage() {
        return this.homepage;
    }

    public int getDownloadSize() {
        int parseInt = 0;
        if (this.downloadSize == null || this.downloadSize.length() == 0) {
            return parseInt;
        }
        try {
            parseInt = Integer.parseInt(this.downloadSize);
        }
        catch (NumberFormatException ex) {
            Logger.getLogger(ModuleItem.class.getName()).log(Level.WARNING, "Module {0} has invalid value of downloadSize: {1}", new Object[]{this.codeName, this.downloadSize});
        }
        return parseInt;
    }

    public UpdateItemDeploymentImpl getUpdateItemDeploymentImpl() {
        return this.deployImpl;
    }

    @Override
    public UpdateLicenseImpl getUpdateLicenseImpl() {
        return this.licenseImpl;
    }

    public ModuleInfo getModuleInfo() {
        if (this.info == null) {
            Module m = Utilities.toModule(this.codeName, this.specificationVersion == null ? null : new SpecificationVersion(this.specificationVersion));
            this.info = m != null ? m : new DummyModuleInfo(this.manifest.getMainAttributes());
        }
        return this.info;
    }

    public String getAgreement() {
        return this.getUpdateLicenseImpl().getAgreement();
    }

    @Override
    public String getCategory() {
        return this.category;
    }

    public String getDate() {
        return this.publishDate == null ? null : Utilities.formatDate(this.publishDate);
    }

    public boolean isAutoload() {
        return this.isAutoload;
    }

    public boolean isEager() {
        return this.isEager;
    }

    public boolean isPreferredUpdate() {
        return this.isPreferedUpdate;
    }

    public String getModuleNotification() {
        return this.moduleNotification;
    }

    void setModuleNotification(String notification) {
        this.moduleNotification = notification;
    }

    @Override
    public void setUpdateLicenseImpl(UpdateLicenseImpl licenseImpl) {
        this.licenseImpl = licenseImpl;
    }
}

