/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.ModuleInfo
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 */
package org.netbeans.modules.autoupdate.updateprovider;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.modules.autoupdate.updateprovider.InstalledUpdateProvider;
import org.openide.modules.ModuleInfo;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;

public final class InstalledModuleProvider
extends InstalledUpdateProvider {
    private LookupListener lkpListener;
    private Lookup.Result<ModuleInfo> result = Lookup.getDefault().lookup(new Lookup.Template(ModuleInfo.class));
    private Map<String, ModuleInfo> moduleInfos;

    @Override
    protected synchronized Map<String, ModuleInfo> getModuleInfos(boolean force) {
        if (this.moduleInfos == null || force) {
            Collection<ModuleInfo> infos = Collections.unmodifiableCollection(this.result.allInstances());
            this.moduleInfos = new HashMap<String, ModuleInfo>();
            for (ModuleInfo info : infos) {
                this.moduleInfos.put(info.getCodeNameBase(), info);
            }
        }
        assert (this.moduleInfos != null);
        return new HashMap<String, ModuleInfo>(this.moduleInfos);
    }

    public InstalledModuleProvider() {
        this.lkpListener = new LookupListener(){

            public void resultChanged(LookupEvent ev) {
                InstalledModuleProvider.this.clearModuleInfos();
            }
        };
        this.result.addLookupListener(this.lkpListener);
    }

    private synchronized void clearModuleInfos() {
        this.moduleInfos = null;
    }

}

