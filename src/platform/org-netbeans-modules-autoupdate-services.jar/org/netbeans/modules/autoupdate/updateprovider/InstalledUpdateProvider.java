/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.SpecificationVersion
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.autoupdate.updateprovider;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.autoupdate.UpdateUnitProvider;
import org.netbeans.modules.autoupdate.services.Utilities;
import org.netbeans.modules.autoupdate.updateprovider.InstalledModuleItem;
import org.netbeans.spi.autoupdate.UpdateItem;
import org.netbeans.spi.autoupdate.UpdateProvider;
import org.openide.modules.ModuleInfo;
import org.openide.modules.SpecificationVersion;
import org.openide.util.Lookup;

public abstract class InstalledUpdateProvider {
    protected InstalledUpdateProvider() {
    }

    protected abstract Map<String, ModuleInfo> getModuleInfos(boolean var1);

    public static Map<String, ModuleInfo> getInstalledModules() {
        return InstalledUpdateProvider.defaultProvider().getModuleInfos(false);
    }

    private static InstalledUpdateProvider defaultProvider() {
        InstalledUpdateProvider iup = (InstalledUpdateProvider)Lookup.getDefault().lookup(InstalledUpdateProvider.class);
        assert (iup != null);
        return iup;
    }

    public static UpdateProvider getDefault() {
        return UP.DEFAULT;
    }

    private static final class UP
    implements UpdateProvider {
        static final UP DEFAULT = new UP();

        private UP() {
        }

        @Override
        public String getName() {
            return "installed-module-provider";
        }

        @Override
        public String getDisplayName() {
            return this.getName();
        }

        @Override
        public String getDescription() {
            return null;
        }

        @Override
        public UpdateUnitProvider.CATEGORY getCategory() {
            return UpdateUnitProvider.CATEGORY.COMMUNITY;
        }

        @Override
        public Map<String, UpdateItem> getUpdateItems() throws IOException {
            HashMap<String, UpdateItem> res = new HashMap<String, UpdateItem>();
            for (ModuleInfo info : InstalledUpdateProvider.defaultProvider().getModuleInfos(true).values()) {
                Date time = null;
                String installTime = null;
                if (time != null) {
                    installTime = Utilities.formatDate(time);
                }
                InstalledModuleItem impl = new InstalledModuleItem(info.getCodeNameBase(), info.getSpecificationVersion() == null ? null : info.getSpecificationVersion().toString(), info, null, null, installTime);
                UpdateItem updateItem = Utilities.createUpdateItem(impl);
                res.put(info.getCodeName() + '_' + (Object)info.getSpecificationVersion(), updateItem);
            }
            return res;
        }

        @Override
        public boolean refresh(boolean force) throws IOException {
            InstalledUpdateProvider.defaultProvider().getModuleInfos(false);
            return true;
        }
    }

}

