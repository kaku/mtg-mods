/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.autoupdate.updateprovider;

import java.util.Set;
import org.netbeans.modules.autoupdate.services.UpdateItemDeploymentImpl;
import org.netbeans.modules.autoupdate.services.UpdateLicenseImpl;
import org.netbeans.modules.autoupdate.updateprovider.UpdateItemImpl;
import org.netbeans.spi.autoupdate.CustomInstaller;
import org.netbeans.spi.autoupdate.CustomUninstaller;

public class NativeComponentItem
extends UpdateItemImpl {
    private boolean isInstalled;
    private String codeName;
    private String specificationVersion;
    private Set<String> dependencies;
    private String displayName;
    private String description;
    private String downloadSize;
    private UpdateItemDeploymentImpl deployImpl;
    private UpdateLicenseImpl licenseImpl;

    public NativeComponentItem(boolean isInstalled, String codeName, String specificationVersion, String downloadSize, Set<String> dependencies, String displayName, String description, Boolean needsRestart, Boolean isGlobal, String targetCluster, CustomInstaller installer, CustomUninstaller uninstaller, UpdateLicenseImpl license) {
        this.isInstalled = isInstalled;
        this.codeName = codeName;
        this.specificationVersion = specificationVersion;
        this.dependencies = dependencies;
        this.displayName = displayName;
        this.description = description;
        this.licenseImpl = license;
        this.downloadSize = downloadSize;
        this.deployImpl = new UpdateItemDeploymentImpl(needsRestart, isGlobal, targetCluster, installer, uninstaller);
    }

    @Override
    public String getCodeName() {
        return this.codeName;
    }

    public String getSpecificationVersion() {
        return this.specificationVersion;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public String getDescription() {
        return this.description;
    }

    public Set<String> getDependenciesToModules() {
        return this.dependencies;
    }

    public int getDownloadSize() {
        return this.isInstalled ? 0 : Integer.parseInt(this.downloadSize);
    }

    public UpdateItemDeploymentImpl getUpdateItemDeploymentImpl() {
        return this.deployImpl;
    }

    @Override
    public UpdateLicenseImpl getUpdateLicenseImpl() {
        assert (false);
        return this.licenseImpl;
    }

    public String getAgreement() {
        return null;
    }

    @Override
    public String getCategory() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setUpdateLicenseImpl(UpdateLicenseImpl licenseImpl) {
        this.licenseImpl = licenseImpl;
    }

    public boolean isInstalled() {
        return this.isInstalled;
    }
}

