/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.autoupdate.updateprovider;

import java.awt.Image;
import org.netbeans.api.autoupdate.UpdateUnitProvider;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public final class ProviderCategory {
    private final String displayName;
    private final String iconBase;
    private final UpdateUnitProvider.CATEGORY category;

    private ProviderCategory(String displayName, String iconBase, UpdateUnitProvider.CATEGORY category) {
        assert (category != null != (displayName != null && iconBase != null));
        this.displayName = displayName;
        this.iconBase = iconBase;
        this.category = category;
    }

    public static ProviderCategory create(String iconBase, String categoryDisplayName) {
        return new ProviderCategory(categoryDisplayName, iconBase, null);
    }

    public String getDisplayName() {
        String name;
        String string = name = this.category != null ? ProviderCategory.getCategoryName(this.category) : this.displayName;
        if (name == null) {
            name = ProviderCategory.forValue(UpdateUnitProvider.CATEGORY.COMMUNITY).getDisplayName();
            assert (name != null);
        }
        return name;
    }

    public String getName() {
        if (this.category != null) {
            return this.category.name();
        }
        assert (this.displayName != null);
        return this.displayName;
    }

    public Image getIcon() {
        Image img = ImageUtilities.loadImage((String)this.getIconBase(), (boolean)true);
        if (img == null) {
            img = ProviderCategory.forValue(UpdateUnitProvider.CATEGORY.COMMUNITY).getIcon();
            assert (img != null);
        }
        return img;
    }

    public static ProviderCategory forValue(UpdateUnitProvider.CATEGORY c) {
        return new ProviderCategory(null, null, c);
    }

    UpdateUnitProvider.CATEGORY toEnum() {
        return this.category == null ? UpdateUnitProvider.CATEGORY.COMMUNITY : this.category;
    }

    static String getCategoryName(UpdateUnitProvider.CATEGORY category) {
        String key = null;
        switch (category) {
            case STANDARD: {
                key = "AvailableTab_SourceCategory_Tooltip_STANDARD";
                break;
            }
            case BETA: {
                key = "AvailableTab_SourceCategory_Tooltip_BETA";
                break;
            }
            case COMMUNITY: {
                key = "AvailableTab_SourceCategory_Tooltip_COMMUNITY";
            }
        }
        return key != null ? NbBundle.getMessage(ProviderCategory.class, (String)key) : null;
    }

    public final String getIconBase() {
        if (this.iconBase != null) {
            return this.iconBase;
        }
        switch (this.category) {
            case BETA: {
                return "org/netbeans/modules/autoupdate/services/resources/icon-beta.png";
            }
            case STANDARD: {
                return "org/netbeans/modules/autoupdate/services/resources/icon-standard.png";
            }
        }
        return "org/netbeans/modules/autoupdate/services/resources/icon-community.png";
    }

}

