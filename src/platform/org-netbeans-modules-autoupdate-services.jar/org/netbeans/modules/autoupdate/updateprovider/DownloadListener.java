/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.autoupdate.updateprovider;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.autoupdate.updateprovider.NetworkAccess;

public class DownloadListener
implements NetworkAccess.NetworkListener {
    private Exception storedException;
    private File dest;
    private URL sourceUrl;
    private boolean allowZeroLength;
    private Logger err;

    public DownloadListener(URL sourceUrl, File dest, boolean allowZeroLength) {
        this.err = Logger.getLogger(this.getClass().getName());
        this.sourceUrl = sourceUrl;
        this.dest = dest;
        this.allowZeroLength = allowZeroLength;
    }

    @Override
    public void streamOpened(InputStream stream, int contentLength) {
        this.err.log(Level.FINE, "Successfully started reading URI " + this.sourceUrl);
        try {
            this.doCopy(this.sourceUrl, stream, this.dest, contentLength);
        }
        catch (IOException ex) {
            this.storeException(ex);
        }
    }

    @Override
    public void accessCanceled() {
        this.err.log(Level.FINE, "Processing " + this.sourceUrl + " was cancelled.");
        this.storeException(new IOException("Processing " + this.sourceUrl + " was cancelled."));
    }

    @Override
    public void accessTimeOut() {
        this.err.log(Level.FINE, "Timeout when processing " + this.sourceUrl);
        this.storeException(new IOException("Timeout when processing " + this.sourceUrl));
    }

    @Override
    public void notifyException(Exception x) {
        this.err.log(Level.INFO, "Reading URL " + this.sourceUrl + " failed (" + x + ")");
        this.storeException(x);
    }

    public void notifyException() throws IOException {
        if (this.isExceptionStored()) {
            throw new IOException(this.getStoredException().getLocalizedMessage(), this.getStoredException());
        }
    }

    private boolean isExceptionStored() {
        return this.storedException != null;
    }

    private void storeException(Exception x) {
        this.storedException = x;
    }

    private Exception getStoredException() {
        return this.storedException;
    }

    private void doCopy(URL sourceUrl, InputStream is, File temp, int contentLength) throws IOException {
        int totalRead;
        BufferedOutputStream os = null;
        int read = 0;
        totalRead = 0;
        try {
            os = new BufferedOutputStream(new FileOutputStream(temp));
            byte[] bytes = new byte[1024];
            while ((read = is.read(bytes)) != -1) {
                os.write(bytes, 0, read);
                totalRead += read;
            }
        }
        catch (IOException ex) {
            this.err.log(Level.INFO, "Writing content of URL " + sourceUrl + " failed.", ex);
            throw ex;
        }
        finally {
            try {
                is.close();
                if (os != null) {
                    os.flush();
                    os.close();
                }
            }
            catch (IOException ioe) {
                this.err.log(Level.INFO, "Closing streams failed.", ioe);
            }
        }
        if (contentLength != -1 && contentLength != totalRead) {
            this.err.log(Level.INFO, "Content length was reported as " + contentLength + " bytes, but read " + totalRead + " bytes from " + sourceUrl);
            throw new IOException("Unexpected closed connection to " + sourceUrl);
        }
        if (totalRead == 0 && !this.allowZeroLength) {
            this.err.log(Level.INFO, "Connection content length was " + contentLength + " bytes (read " + totalRead + "bytes), expected file size can`t be that size - likely server with file at " + sourceUrl + " is temporary down");
            throw new IOException("Zero sized file reported at " + sourceUrl);
        }
        this.err.log(Level.FINE, "Read " + totalRead + " bytes from file at " + sourceUrl);
    }
}

