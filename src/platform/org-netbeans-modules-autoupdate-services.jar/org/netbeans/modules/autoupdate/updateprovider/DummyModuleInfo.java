/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.Dependency
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.SpecificationVersion
 */
package org.netbeans.modules.autoupdate.updateprovider;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.jar.Attributes;
import org.openide.modules.Dependency;
import org.openide.modules.ModuleInfo;
import org.openide.modules.SpecificationVersion;

public final class DummyModuleInfo
extends ModuleInfo {
    public static final String TOKEN_MODULE_FORMAT1 = "org.openide.modules.ModuleFormat1";
    public static final String TOKEN_MODULE_FORMAT2 = "org.openide.modules.ModuleFormat2";
    private final Attributes attr;
    private final Set<Dependency> deps;
    private final String[] provides;
    private SpecificationVersion specVersion = null;
    private String codeName = null;
    private String codeNameBase = null;
    private Integer codeNameRelease = null;

    public DummyModuleInfo(Attributes attr) throws IllegalArgumentException {
        this.attr = attr;
        if (attr == null) {
            throw new IllegalArgumentException("The parameter attr cannot be null.");
        }
        if (this.getCodeName() == null) {
            throw new IllegalArgumentException("No code name in module descriptor " + attr.entrySet());
        }
        String cnb = this.getCodeNameBase();
        try {
            this.getSpecificationVersion();
        }
        catch (NumberFormatException nfe) {
            throw new IllegalArgumentException(nfe.toString() + " from " + cnb);
        }
        this.deps = DummyModuleInfo.parseDeps(attr, cnb);
        String providesS = attr.getValue("OpenIDE-Module-Provides");
        if (cnb.equals("org.openide.modules")) {
            providesS = providesS == null ? "org.openide.modules.ModuleFormat1" : providesS + ", " + "org.openide.modules.ModuleFormat1";
            String string = providesS = providesS == null ? "org.openide.modules.ModuleFormat2" : providesS + ", " + "org.openide.modules.ModuleFormat2";
        }
        if (providesS == null) {
            this.provides = new String[0];
        } else {
            StringTokenizer tok = new StringTokenizer(providesS, ", ");
            this.provides = new String[tok.countTokens()];
            for (int i = 0; i < this.provides.length; ++i) {
                this.provides[i] = tok.nextToken();
            }
        }
    }

    public boolean isEnabled() {
        return false;
    }

    public SpecificationVersion getSpecificationVersion() {
        if (this.specVersion == null) {
            String sv = this.attr.getValue("OpenIDE-Module-Specification-Version");
            this.specVersion = sv == null ? null : new SpecificationVersion(sv);
        }
        return this.specVersion;
    }

    public String getCodeName() {
        if (this.codeName == null) {
            this.codeName = this.attr.getValue("OpenIDE-Module");
        }
        return this.codeName;
    }

    public int getCodeNameRelease() {
        if (this.codeNameRelease == null) {
            String s = this.getCodeName();
            int idx = s.lastIndexOf(47);
            this.codeNameRelease = idx == -1 ? Integer.valueOf(-1) : Integer.valueOf(Integer.parseInt(s.substring(idx + 1)));
        }
        return this.codeNameRelease;
    }

    public String getCodeNameBase() {
        if (this.codeNameBase == null) {
            String s = this.getCodeName();
            int idx = s.lastIndexOf(47);
            this.codeNameBase = idx == -1 ? s : s.substring(0, idx);
        }
        return this.codeNameBase;
    }

    public Object getLocalizedAttribute(String a) {
        return this.attr.getValue(a);
    }

    public Object getAttribute(String a) {
        return this.attr.getValue(a);
    }

    public Set<Dependency> getDependencies() {
        return this.deps;
    }

    private static final Set<Dependency> parseDeps(Attributes attr, String cnb) throws IllegalArgumentException {
        HashSet<Dependency> s = new HashSet<Dependency>();
        s.addAll(Dependency.create((int)1, (String)attr.getValue("OpenIDE-Module-Module-Dependencies")));
        s.addAll(Dependency.create((int)2, (String)attr.getValue("OpenIDE-Module-Package-Dependencies")));
        s.addAll(Dependency.create((int)3, (String)attr.getValue("OpenIDE-Module-Java-Dependencies")));
        s.addAll(Dependency.create((int)5, (String)attr.getValue("OpenIDE-Module-Requires")));
        s.addAll(Dependency.create((int)6, (String)attr.getValue("OpenIDE-Module-Needs")));
        s.addAll(Dependency.create((int)7, (String)attr.getValue("OpenIDE-Module-Recommends")));
        Object api = null;
        String impl = null;
        String major = null;
        if (api != null) {
            s.addAll(Dependency.create((int)1, (String)("org.openide" + major + " > " + api)));
        }
        if (impl != null) {
            s.addAll(Dependency.create((int)1, (String)("org.openide" + major + " = " + impl)));
        }
        if (api != null || impl == null) {
            // empty if block
        }
        return s;
    }

    public boolean owns(Class clazz) {
        return false;
    }

    public String[] getProvides() {
        return this.provides;
    }
}

