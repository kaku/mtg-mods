/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.Places
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.autoupdate.updateprovider;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.autoupdate.services.AutoupdateSettings;
import org.netbeans.modules.autoupdate.updateprovider.DownloadListener;
import org.netbeans.modules.autoupdate.updateprovider.NetworkAccess;
import org.openide.filesystems.FileUtil;
import org.openide.modules.Places;
import org.openide.util.Utilities;

public final class AutoupdateCatalogCache {
    private final File cacheDir = Places.getCacheSubdirectory((String)"catalogcache");
    private static AutoupdateCatalogCache INSTANCE;
    private static final Logger err;

    private AutoupdateCatalogCache() {
        assert (this.cacheDir != null && this.cacheDir.exists());
        this.getLicenseDir().mkdirs();
        err.log(Level.FINE, "getCacheDirectory: {0}", this.cacheDir.getPath());
    }

    public static synchronized AutoupdateCatalogCache getDefault() {
        if (INSTANCE == null) {
            INSTANCE = new AutoupdateCatalogCache();
        }
        return INSTANCE;
    }

    private synchronized File getCatalogCache() {
        assert (this.cacheDir != null && this.cacheDir.exists());
        return this.cacheDir;
    }

    public URL writeCatalogToCache(String codeName, URL original) throws IOException {
        URL url;
        block3 : {
            url = null;
            File dir = this.getCatalogCache();
            assert (dir != null && dir.exists());
            File cache = new File(dir, codeName);
            this.copy(original, cache, false);
            try {
                url = Utilities.toURI((File)cache).toURL();
            }
            catch (MalformedURLException ex) {
                if ($assertionsDisabled) break block3;
                throw new AssertionError(ex);
            }
        }
        return url;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public URL getCatalogURL(String codeName) {
        File dir = this.getCatalogCache();
        File cache = new File(dir, codeName);
        String string = this.getLock(cache);
        synchronized (string) {
            if (cache.exists()) {
                URL url;
                block7 : {
                    if (cache.length() == 0) {
                        err.log(Level.INFO, "Cache file {0} exists and of zero size", cache);
                        return null;
                    }
                    url = null;
                    try {
                        url = Utilities.toURI((File)cache).toURL();
                    }
                    catch (MalformedURLException ex) {
                        if ($assertionsDisabled) break block7;
                        throw new AssertionError(ex);
                    }
                }
                return url;
            }
            return null;
        }
    }

    private File getLicenseDir() {
        return new File(this.getCatalogCache(), "licenses");
    }

    private File getLicenseFile(String name) {
        return new File(this.getLicenseDir(), name);
    }

    public String getLicense(String name) {
        return this.getLicense(name, null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String getLicense(String name, URL url) {
        File file = this.getLicenseFile(name);
        String string = name.intern();
        synchronized (string) {
            if (!file.exists()) {
                if (url == null) {
                    return null;
                }
                try {
                    this.copy(url, file, true);
                }
                catch (IOException e) {
                    err.log(Level.INFO, "Can`t store license from " + url + " to " + file, e);
                    try {
                        if (file.exists()) {
                            file.delete();
                        }
                        file.createNewFile();
                        file.deleteOnExit();
                    }
                    catch (IOException ex) {
                        err.log(Level.INFO, "Can`t create empty license file", ex);
                    }
                }
            }
            return this.readLicenseFile(name);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void storeLicense(String name, String content) {
        File file = this.getLicenseFile(name);
        String string = name.intern();
        synchronized (string) {
            if (file.exists() || content == null) {
                return;
            }
            this.writeToFile(content, file);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    private String readLicenseFile(String name) {
        File file = this.getLicenseFile(name);
        FileInputStream fr = null;
        String string = name.intern();
        synchronized (string) {
            try {
                int n2;
                fr = new FileInputStream(file);
                byte[] buffer = new byte[8192];
                StringBuilder sb = new StringBuilder();
                while ((n2 = fr.read(buffer)) != -1) {
                    sb.append(new String(buffer, 0, n2, "utf-8"));
                }
                String string2 = sb.toString();
                return string2;
            }
            catch (IOException e) {
                err.log(Level.INFO, "Can`t read license from file " + file, e);
                String n2 = null;
                return n2;
            }
            finally {
                if (fr != null) {
                    try {
                        fr.close();
                    }
                    catch (IOException e) {
                        err.log(Level.INFO, "Can`t read close input stream for " + file, e);
                    }
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void writeToFile(String content, File file) {
        FileOutputStream fw = null;
        try {
            fw = new FileOutputStream(file);
            fw.write(content.getBytes("utf-8"));
        }
        catch (IOException e) {
            err.log(Level.INFO, "Can`t write to " + file, e);
        }
        finally {
            if (fw != null) {
                try {
                    fw.flush();
                    fw.close();
                }
                catch (IOException e) {
                    err.log(Level.INFO, "Can`t output stream for " + file, e);
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void copy(URL sourceUrl, File cache, boolean allowZeroSize) throws IOException {
        err.log(Level.FINE, "Processing URL: {0}", sourceUrl);
        String prefix = "";
        while (prefix.length() < 3) {
            prefix = prefix + cache.getName();
        }
        File temp = File.createTempFile(prefix, null, cache.getParentFile());
        temp.deleteOnExit();
        DownloadListener nwl = new DownloadListener(sourceUrl, temp, allowZeroSize);
        NetworkAccess.Task task = NetworkAccess.createNetworkAcessTask(sourceUrl, AutoupdateSettings.getOpenConnectionTimeout(), nwl);
        task.waitFinished();
        nwl.notifyException();
        String string = this.getLock(cache);
        synchronized (string) {
            this.updateCachedFile(cache, temp);
            assert (cache.exists());
            err.log(Level.FINER, "Cache file {0} was wrote from original URL {1}", new Object[]{cache, sourceUrl});
            if (cache.exists() && cache.length() == 0) {
                err.log(Level.INFO, "Written cache size is zero bytes");
            }
        }
    }

    public String getLock(File cache) {
        return cache.getAbsolutePath().intern();
    }

    public String getLock(URL cache) throws IOException {
        try {
            return this.getLock(Utilities.toFile((URI)cache.toURI()));
        }
        catch (URISyntaxException ex) {
            throw new IOException(ex);
        }
    }

    private void updateCachedFile(File cache, File temp) {
        if (cache.exists() && !cache.delete()) {
            block8 : {
                err.log(Level.INFO, "Cannot delete cache {0}", cache);
                try {
                    Thread.sleep(200);
                }
                catch (InterruptedException ie) {
                    if ($assertionsDisabled) break block8;
                    throw new AssertionError(ie);
                }
            }
            cache.delete();
        }
        if (temp.length() == 0) {
            err.log(Level.INFO, "Temp cache size is zero bytes");
        }
        if (!temp.renameTo(cache)) {
            err.log(Level.INFO, "Cannot rename temp {0} to cache {1}", new Object[]{temp, cache});
            err.log(Level.INFO, "Trying to copy {0} to cache {1}", new Object[]{temp, cache});
            try {
                FileOutputStream os = new FileOutputStream(cache);
                FileInputStream is = new FileInputStream(temp);
                FileUtil.copy((InputStream)is, (OutputStream)os);
                os.close();
                is.close();
                temp.delete();
            }
            catch (IOException ex) {
                err.log(Level.INFO, "Cannot even copy: {0}", ex.getMessage());
                err.log(Level.FINE, null, ex);
            }
        }
        if (cache.exists() && cache.length() == 0) {
            err.log(Level.INFO, "Final cache size is zero bytes");
        }
    }

    static {
        err = Logger.getLogger(AutoupdateCatalogCache.class.getName());
    }
}

