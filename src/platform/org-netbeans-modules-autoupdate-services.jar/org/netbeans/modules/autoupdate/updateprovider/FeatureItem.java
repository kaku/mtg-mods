/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.Dependency
 */
package org.netbeans.modules.autoupdate.updateprovider;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.netbeans.modules.autoupdate.services.UpdateLicenseImpl;
import org.netbeans.modules.autoupdate.updateprovider.UpdateItemImpl;
import org.openide.modules.Dependency;

public class FeatureItem
extends UpdateItemImpl {
    private String codeName;
    private String specificationVersion;
    private Set<String> dependenciesToModules;
    private Set<String> moduleCodeNames;
    private String displayName;
    private String description;
    private String category;

    public FeatureItem(String codeName, String specificationVersion, Set<String> dependencies, String displayName, String description, String category) {
        if (dependencies == null) {
            throw new IllegalArgumentException("Cannot create FeatureItem " + codeName + " with null modules.");
        }
        this.codeName = codeName;
        this.specificationVersion = specificationVersion;
        this.dependenciesToModules = dependencies;
        this.displayName = displayName;
        this.description = description;
        this.category = category;
    }

    @Override
    public String getCodeName() {
        return this.codeName;
    }

    public String getSpecificationVersion() {
        return this.specificationVersion;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public String getDescription() {
        return this.description;
    }

    public Set<String> getDependenciesToModules() {
        return this.dependenciesToModules;
    }

    public Set<String> getModuleCodeNames() {
        if (this.moduleCodeNames == null) {
            this.moduleCodeNames = new HashSet<String>();
            for (String depSpec : this.dependenciesToModules) {
                Set deps = Dependency.create((int)1, (String)depSpec);
                assert (deps.size() == 1);
                Dependency dep = (Dependency)deps.iterator().next();
                assert (1 == dep.getType());
                String name = dep.getName();
                if (name.indexOf(47) != -1) {
                    int to = name.indexOf(47);
                    name = name.substring(0, to);
                }
                this.moduleCodeNames.add(name);
            }
        }
        return this.moduleCodeNames;
    }

    @Override
    public UpdateLicenseImpl getUpdateLicenseImpl() {
        assert (false);
        return null;
    }

    @Override
    public String getCategory() {
        return this.category;
    }

    @Override
    public void setUpdateLicenseImpl(UpdateLicenseImpl licenseImpl) {
        assert (false);
    }

    public String toString() {
        return "FeatureItem[" + this.getCodeName() + "/" + this.getSpecificationVersion() + "]";
    }
}

