/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.autoupdate.updateprovider;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.autoupdate.UpdateUnitProvider;
import org.netbeans.modules.autoupdate.updateprovider.AutoupdateCatalogCache;
import org.netbeans.modules.autoupdate.updateprovider.AutoupdateCatalogParser;
import org.netbeans.modules.autoupdate.updateprovider.ProviderCategory;
import org.netbeans.spi.autoupdate.UpdateItem;
import org.netbeans.spi.autoupdate.UpdateProvider;
import org.openide.util.Parameters;

public class AutoupdateCatalogProvider
implements UpdateProvider {
    private URL updateCenter;
    private final String codeName;
    private String displayName;
    private AutoupdateCatalogCache cache = AutoupdateCatalogCache.getDefault();
    private static final Logger LOG = Logger.getLogger("org.netbeans.modules.autoupdate.updateprovider.AutoupdateCatalog");
    private String description;
    private boolean descriptionInitialized;
    private ProviderCategory category;
    private String contentDescription;
    private boolean contentDescriptionInitialized;

    public AutoupdateCatalogProvider(String name, String displayName, URL updateCenter) {
        this(name, displayName, updateCenter, ProviderCategory.forValue(UpdateUnitProvider.CATEGORY.COMMUNITY));
    }

    public AutoupdateCatalogProvider(String name, String displayName, URL updateCenter, ProviderCategory category) {
        Parameters.notNull((CharSequence)"name", (Object)name);
        this.codeName = name;
        this.displayName = displayName;
        this.updateCenter = updateCenter;
        this.category = category;
    }

    public AutoupdateCatalogProvider(String name, String displayName, URL updateCenter, UpdateUnitProvider.CATEGORY category) {
        this(name, displayName, updateCenter, ProviderCategory.forValue(category));
    }

    @Override
    public String getName() {
        return this.codeName;
    }

    @Override
    public String getDisplayName() {
        return this.displayName == null ? this.codeName : this.displayName;
    }

    @Override
    public String getDescription() {
        if (this.description == null && !this.descriptionInitialized) {
            try {
                this.getUpdateItems();
            }
            catch (IOException e) {
                // empty catch block
            }
        }
        return this.description;
    }

    public void setNotification(String notification) {
        this.description = notification;
        this.descriptionInitialized = true;
    }

    public String getContentDescription() {
        if (this.contentDescription == null && !this.contentDescriptionInitialized) {
            try {
                this.getUpdateItems();
            }
            catch (IOException e) {
                // empty catch block
            }
        }
        return this.contentDescription;
    }

    public void setContentDescription(String description) {
        this.contentDescription = description;
        this.contentDescriptionInitialized = true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Map<String, UpdateItem> getUpdateItems() throws IOException {
        Map<String, UpdateItem> map;
        URL toParse = this.cache.getCatalogURL(this.codeName);
        if (toParse == null) {
            LOG.log(Level.FINE, "No content in cache for {0} provider. Returns EMPTY_MAP", this.codeName);
            return Collections.emptyMap();
        }
        String string = this.cache.getLock(toParse);
        synchronized (string) {
            map = AutoupdateCatalogParser.getUpdateItems(toParse, this);
        }
        this.descriptionInitialized = true;
        return map;
    }

    @Override
    public boolean refresh(boolean force) throws IOException {
        boolean res;
        LOG.log(Level.FINER, "Try write(force? {0}) to cache Update Provider {1} from {2}", new Object[]{force, this.codeName, this.getUpdateCenterURL()});
        if (force) {
            res = this.cache.writeCatalogToCache(this.codeName, this.getUpdateCenterURL()) != null;
            this.description = null;
            this.descriptionInitialized = false;
        } else {
            res = true;
        }
        return res;
    }

    public URL getUpdateCenterURL() {
        assert (this.updateCenter != null);
        return this.updateCenter;
    }

    public void setUpdateCenterURL(URL newUpdateCenter) {
        assert (newUpdateCenter != null);
        this.updateCenter = newUpdateCenter;
    }

    public String toString() {
        return this.displayName + "[" + this.codeName + "] to " + this.updateCenter;
    }

    @Override
    public UpdateUnitProvider.CATEGORY getCategory() {
        return this.category.toEnum();
    }

    public ProviderCategory getProviderCategory() {
        return this.category;
    }
}

