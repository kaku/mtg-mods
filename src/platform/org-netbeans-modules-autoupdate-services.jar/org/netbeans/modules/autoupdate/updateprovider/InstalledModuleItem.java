/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.Module
 *  org.openide.modules.ModuleInfo
 */
package org.netbeans.modules.autoupdate.updateprovider;

import org.netbeans.Module;
import org.netbeans.modules.autoupdate.services.UpdateItemDeploymentImpl;
import org.netbeans.modules.autoupdate.services.Utilities;
import org.netbeans.modules.autoupdate.updateprovider.ModuleItem;
import org.openide.modules.ModuleInfo;

public class InstalledModuleItem
extends ModuleItem {
    private String codeName;
    private String specificationVersion;
    private ModuleInfo info;
    private String author;
    private String source;
    private String installCluster;
    private String installDate;

    public InstalledModuleItem(String codeName, String specificationVersion, ModuleInfo info, String author, String installCluster, String installTime) {
        this.codeName = codeName;
        this.specificationVersion = specificationVersion;
        this.info = info;
        this.author = author;
        this.installCluster = installCluster;
        this.installDate = installTime;
    }

    @Override
    public final String getCodeName() {
        return this.codeName;
    }

    @Override
    public final String getSpecificationVersion() {
        return this.specificationVersion;
    }

    public String getSource() {
        if (this.source == null) {
            this.source = Utilities.readSourceFromUpdateTracking(this.info);
        }
        if (this.source == null) {
            this.source = Utilities.getProductVersion();
        }
        return this.source;
    }

    @Override
    public String getAuthor() {
        return this.author;
    }

    @Override
    public ModuleInfo getModuleInfo() {
        return this.info;
    }

    @Override
    public String getAgreement() {
        assert (false);
        return null;
    }

    @Override
    public int getDownloadSize() {
        return 0;
    }

    @Override
    public String getDate() {
        return this.installDate;
    }

    @Override
    public UpdateItemDeploymentImpl getUpdateItemDeploymentImpl() {
        assert (false);
        return null;
    }

    @Override
    public boolean isAutoload() {
        return this.getModule() != null && this.getModule().isAutoload();
    }

    @Override
    public boolean isEager() {
        return this.getModule() != null && this.getModule().isEager();
    }

    private Module getModule() {
        if (this.info instanceof Module) {
            return (Module)this.info;
        }
        return null;
    }
}

