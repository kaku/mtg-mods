/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.autoupdate.updateprovider;

import java.net.URL;
import org.netbeans.modules.autoupdate.services.UpdateItemDeploymentImpl;
import org.netbeans.modules.autoupdate.updateprovider.FeatureItem;
import org.netbeans.modules.autoupdate.updateprovider.LocalizationItem;
import org.netbeans.modules.autoupdate.updateprovider.ModuleItem;
import org.netbeans.modules.autoupdate.updateprovider.NativeComponentItem;
import org.netbeans.modules.autoupdate.updateprovider.UpdateItemImpl;
import org.netbeans.spi.autoupdate.CustomInstaller;

public final class InstallInfo {
    private UpdateItemImpl item;

    public InstallInfo(UpdateItemImpl item) {
        this.item = item;
    }

    public String getTargetCluster() {
        String res = null;
        if (this.item instanceof ModuleItem) {
            res = ((ModuleItem)this.item).getUpdateItemDeploymentImpl().getTargetCluster();
        } else if (this.item instanceof LocalizationItem) {
            res = ((LocalizationItem)this.item).getUpdateItemDeploymentImpl().getTargetCluster();
        } else if (this.item instanceof FeatureItem) {
            assert (false);
        } else assert (false);
        return res;
    }

    public Boolean needsRestart() {
        Boolean res = null;
        if (this.item instanceof ModuleItem) {
            res = ((ModuleItem)this.item).getUpdateItemDeploymentImpl().needsRestart();
        } else if (this.item instanceof LocalizationItem) {
            res = ((LocalizationItem)this.item).getUpdateItemDeploymentImpl().needsRestart();
        } else if (this.item instanceof FeatureItem) {
            assert (false);
        } else assert (false);
        return res;
    }

    public Boolean isGlobal() {
        Boolean res = null;
        if (this.item instanceof ModuleItem) {
            res = ((ModuleItem)this.item).getUpdateItemDeploymentImpl().isGlobal();
        } else if (this.item instanceof LocalizationItem) {
            res = ((LocalizationItem)this.item).getUpdateItemDeploymentImpl().isGlobal();
        } else if (this.item instanceof FeatureItem) {
            assert (false);
        } else assert (false);
        return res;
    }

    public URL getDistribution() {
        URL res = null;
        if (this.item instanceof ModuleItem) {
            res = ((ModuleItem)this.item).getDistribution();
        } else if (this.item instanceof LocalizationItem) {
            res = ((LocalizationItem)this.item).getDistribution();
        } else if (this.item instanceof FeatureItem) {
            assert (false);
        } else assert (false);
        return res;
    }

    public CustomInstaller getCustomInstaller() {
        CustomInstaller res = null;
        if (this.item instanceof NativeComponentItem) {
            res = ((NativeComponentItem)this.item).getUpdateItemDeploymentImpl().getCustomInstaller();
        }
        return res;
    }

    public UpdateItemImpl getUpdateItemImpl() {
        return this.item;
    }
}

