/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.autoupdate.updateprovider;

import java.net.URL;
import java.util.Locale;
import org.netbeans.modules.autoupdate.services.UpdateItemDeploymentImpl;
import org.netbeans.modules.autoupdate.services.UpdateLicenseImpl;
import org.netbeans.modules.autoupdate.updateprovider.UpdateItemImpl;
import org.netbeans.spi.autoupdate.CustomInstaller;
import org.netbeans.spi.autoupdate.CustomUninstaller;

public class LocalizationItem
extends UpdateItemImpl {
    private String codeName;
    private String specificationVersion;
    private Locale locale;
    private String branding;
    private String moduleSpecificationVersion;
    private String localizedName;
    private String localizedDescription;
    private URL distribution;
    private String category;
    private UpdateItemDeploymentImpl deployImpl;
    private UpdateLicenseImpl licenseImpl;

    public LocalizationItem(String codeName, String specificationVersion, URL distribution, Locale locale, String branding, String moduleSpecificationVersion, String localizedName, String localizedDescription, String category, Boolean needsRestart, Boolean isGlobal, String targetCluster, UpdateLicenseImpl licenseImpl) {
        this.codeName = codeName;
        this.specificationVersion = specificationVersion;
        this.distribution = distribution;
        this.locale = locale;
        this.branding = branding;
        this.moduleSpecificationVersion = moduleSpecificationVersion;
        this.localizedName = localizedName;
        this.localizedDescription = localizedDescription;
        this.deployImpl = new UpdateItemDeploymentImpl(needsRestart, isGlobal, targetCluster, null, null);
        this.licenseImpl = licenseImpl;
        this.category = category;
    }

    @Override
    public String getCodeName() {
        return this.codeName;
    }

    public String getSpecificationVersion() {
        return this.specificationVersion;
    }

    public URL getDistribution() {
        return this.distribution;
    }

    public Locale getLocale() {
        return this.locale;
    }

    public String getBranding() {
        return this.branding;
    }

    public String getMasterModuleSpecificationVersion() {
        return this.moduleSpecificationVersion;
    }

    public String getLocalizedModuleName() {
        return this.localizedName;
    }

    public String getLocalizedModuleDescription() {
        return this.localizedDescription;
    }

    public UpdateItemDeploymentImpl getUpdateItemDeploymentImpl() {
        return this.deployImpl;
    }

    @Override
    public UpdateLicenseImpl getUpdateLicenseImpl() {
        return this.licenseImpl;
    }

    public String getAgreement() {
        return this.getUpdateLicenseImpl().getAgreement();
    }

    @Override
    public String getCategory() {
        return this.category;
    }

    @Override
    public void setUpdateLicenseImpl(UpdateLicenseImpl licenseImpl) {
        this.licenseImpl = licenseImpl;
    }
}

