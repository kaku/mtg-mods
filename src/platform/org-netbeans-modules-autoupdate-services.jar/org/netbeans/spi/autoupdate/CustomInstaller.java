/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 */
package org.netbeans.spi.autoupdate;

import org.netbeans.api.autoupdate.OperationException;
import org.netbeans.api.progress.ProgressHandle;

public interface CustomInstaller {
    public boolean install(String var1, String var2, ProgressHandle var3) throws OperationException;
}

