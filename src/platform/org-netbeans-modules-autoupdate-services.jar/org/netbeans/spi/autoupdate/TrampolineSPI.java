/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.autoupdate;

import java.io.File;
import java.io.IOException;
import org.netbeans.api.autoupdate.InstallSupport;
import org.netbeans.api.autoupdate.OperationContainer;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.api.autoupdate.UpdateUnitProvider;
import org.netbeans.modules.autoupdate.services.InstallSupportImpl;
import org.netbeans.modules.autoupdate.services.OperationContainerImpl;
import org.netbeans.modules.autoupdate.services.Trampoline;
import org.netbeans.modules.autoupdate.services.UpdateElementImpl;
import org.netbeans.modules.autoupdate.services.UpdateUnitImpl;
import org.netbeans.modules.autoupdate.services.UpdateUnitProviderImpl;
import org.netbeans.modules.autoupdate.updateprovider.UpdateItemImpl;
import org.netbeans.spi.autoupdate.AutoupdateClusterCreator;
import org.netbeans.spi.autoupdate.UpdateItem;
import org.netbeans.spi.autoupdate.UpdateProvider;

final class TrampolineSPI
extends Trampoline {
    TrampolineSPI() {
    }

    @Override
    protected UpdateUnit createUpdateUnit(UpdateUnitImpl impl) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected UpdateUnitImpl impl(UpdateUnit unit) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected UpdateElement createUpdateElement(UpdateElementImpl impl) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected UpdateElementImpl impl(UpdateElement element) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public UpdateItemImpl impl(UpdateItem item) {
        return item.impl;
    }

    @Override
    protected UpdateItem createUpdateItem(UpdateItemImpl impl) {
        return new UpdateItem(impl);
    }

    @Override
    protected OperationContainerImpl impl(OperationContainer container) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    protected UpdateUnitProvider createUpdateUnitProvider(UpdateProvider provider) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected UpdateUnitProvider createUpdateUnitProvider(UpdateUnitProviderImpl impl) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public UpdateUnitProviderImpl impl(UpdateUnitProvider provider) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected OperationContainerImpl.OperationInfoImpl impl(OperationContainer.OperationInfo info) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    protected OperationContainer.OperationInfo createOperationInfo(OperationContainerImpl.OperationInfoImpl impl) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected File findCluster(String clusterName, AutoupdateClusterCreator creator) {
        return creator.findCluster(clusterName);
    }

    @Override
    protected File[] registerCluster(String clusterName, File cluster, AutoupdateClusterCreator creator) throws IOException {
        return creator.registerCluster(clusterName, cluster);
    }

    @Override
    public InstallSupportImpl impl(InstallSupport support) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}

