/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.autoupdate;

import java.io.File;
import java.io.IOException;

public abstract class AutoupdateClusterCreator {
    protected abstract File findCluster(String var1);

    protected abstract File[] registerCluster(String var1, File var2) throws IOException;
}

