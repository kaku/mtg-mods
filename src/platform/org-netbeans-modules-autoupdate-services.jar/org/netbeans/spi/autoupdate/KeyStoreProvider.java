/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.autoupdate;

import java.security.KeyStore;

public interface KeyStoreProvider {
    public KeyStore getKeyStore();
}

