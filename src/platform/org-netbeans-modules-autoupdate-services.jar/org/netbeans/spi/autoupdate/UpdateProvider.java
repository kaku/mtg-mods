/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.autoupdate;

import java.io.IOException;
import java.util.Map;
import org.netbeans.api.autoupdate.UpdateUnitProvider;
import org.netbeans.spi.autoupdate.UpdateItem;

public interface UpdateProvider {
    public String getName();

    public String getDisplayName();

    public String getDescription();

    public UpdateUnitProvider.CATEGORY getCategory();

    public Map<String, UpdateItem> getUpdateItems() throws IOException;

    public boolean refresh(boolean var1) throws IOException;
}

