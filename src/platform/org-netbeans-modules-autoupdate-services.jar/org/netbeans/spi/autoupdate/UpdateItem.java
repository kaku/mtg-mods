/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.autoupdate;

import java.net.URL;
import java.util.Locale;
import java.util.Set;
import java.util.jar.Manifest;
import org.netbeans.modules.autoupdate.services.Trampoline;
import org.netbeans.modules.autoupdate.services.UpdateLicenseImpl;
import org.netbeans.modules.autoupdate.updateprovider.FeatureItem;
import org.netbeans.modules.autoupdate.updateprovider.LocalizationItem;
import org.netbeans.modules.autoupdate.updateprovider.ModuleItem;
import org.netbeans.modules.autoupdate.updateprovider.NativeComponentItem;
import org.netbeans.modules.autoupdate.updateprovider.UpdateItemImpl;
import org.netbeans.spi.autoupdate.CustomInstaller;
import org.netbeans.spi.autoupdate.CustomUninstaller;
import org.netbeans.spi.autoupdate.TrampolineSPI;
import org.netbeans.spi.autoupdate.UpdateLicense;

public final class UpdateItem {
    UpdateItemImpl impl;
    UpdateItem original;

    UpdateItem(UpdateItemImpl item) {
        this.impl = item;
        item.setUpdateItem(this);
    }

    public static UpdateItem createModule(String codeName, String specificationVersion, URL distribution, String author, String downloadSize, String homepage, String publishDate, String category, Manifest manifest, Boolean isEager, Boolean isAutoload, Boolean needsRestart, Boolean isGlobal, String targetCluster, UpdateLicense license) {
        ModuleItem item = new ModuleItem(codeName, specificationVersion, distribution, author, publishDate, downloadSize, homepage, category, manifest, isEager, isAutoload, needsRestart, isGlobal, false, targetCluster, license.impl);
        return new UpdateItem(item);
    }

    public static UpdateItem createModule(String codeName, String specificationVersion, URL distribution, String author, String downloadSize, String homepage, String publishDate, String category, Manifest manifest, Boolean isEager, Boolean isAutoload, Boolean needsRestart, Boolean isGlobal, Boolean isPreferedUpdate, String targetCluster, UpdateLicense license) {
        ModuleItem item = new ModuleItem(codeName, specificationVersion, distribution, author, publishDate, downloadSize, homepage, category, manifest, isEager, isAutoload, needsRestart, isGlobal, isPreferedUpdate, targetCluster, license.impl);
        return new UpdateItem(item);
    }

    public static UpdateItem createFeature(String codeName, String specificationVersion, Set<String> dependencies, String displayName, String description, String category) {
        FeatureItem item = new FeatureItem(codeName, specificationVersion, dependencies, displayName, description, category);
        return new UpdateItem(item);
    }

    public static UpdateItem createNativeComponent(String codeName, String specificationVersion, String downloadSize, Set<String> dependencies, String displayName, String description, Boolean needsRestart, Boolean isGlobal, String targetCluster, CustomInstaller installer, UpdateLicense license) {
        NativeComponentItem item = new NativeComponentItem(false, codeName, specificationVersion, downloadSize, dependencies, displayName, description, needsRestart, isGlobal, targetCluster, installer, null, null);
        return new UpdateItem(item);
    }

    public static UpdateItem createInstalledNativeComponent(String codeName, String specificationVersion, Set<String> dependencies, String displayName, String description, CustomUninstaller uninstaller) {
        NativeComponentItem item = new NativeComponentItem(true, codeName, specificationVersion, null, dependencies, displayName, description, null, null, null, null, uninstaller, null);
        return new UpdateItem(item);
    }

    public static UpdateItem createLocalization(String codeName, String specificationVersion, String moduleSpecificationVersion, Locale locale, String branding, String localizedName, String localizedDescription, String category, URL distribution, Boolean needsRestart, Boolean isGlobal, String targetCluster, UpdateLicense license) {
        LocalizationItem item = new LocalizationItem(codeName, specificationVersion, distribution, locale, branding, moduleSpecificationVersion, localizedName, localizedDescription, category, needsRestart, isGlobal, targetCluster, license.impl);
        return new UpdateItem(item);
    }

    static {
        Trampoline.SPI = new TrampolineSPI();
    }
}

