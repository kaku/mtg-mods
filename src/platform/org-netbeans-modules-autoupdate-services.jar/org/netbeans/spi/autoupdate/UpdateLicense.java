/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.autoupdate;

import org.netbeans.modules.autoupdate.services.UpdateLicenseImpl;

public final class UpdateLicense {
    UpdateLicenseImpl impl;

    private UpdateLicense(UpdateLicenseImpl impl) {
        this.impl = impl;
    }

    public static final UpdateLicense createUpdateLicense(String licenseName, String agreement) {
        return new UpdateLicense(new UpdateLicenseImpl(licenseName, agreement));
    }
}

