/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.autoupdate;

import java.util.List;
import org.netbeans.api.autoupdate.TrampolineAPI;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateManager;
import org.netbeans.modules.autoupdate.services.Trampoline;
import org.netbeans.modules.autoupdate.services.UpdateUnitImpl;

public final class UpdateUnit {
    final UpdateUnitImpl impl;

    UpdateUnit(UpdateUnitImpl i) {
        this.impl = i;
    }

    public String getCodeName() {
        return this.impl.getCodeName();
    }

    public UpdateElement getInstalled() {
        return this.impl.getInstalled();
    }

    public List<UpdateElement> getAvailableUpdates() {
        return this.impl.getAvailableUpdates();
    }

    public UpdateElement getBackup() {
        return this.impl.getBackup();
    }

    public UpdateElement getInstalledLocalization() {
        return this.impl.getInstalledLocalization();
    }

    public List<UpdateElement> getAvailableLocalizations() {
        return this.impl.getAvailableLocalizations();
    }

    public UpdateManager.TYPE getType() {
        return this.impl.getType();
    }

    public boolean isPending() {
        return this.impl.isPending();
    }

    public UpdateUnit getVisibleAncestor() {
        if (this.getInstalled() != null) {
            return this.impl.getVisibleAncestor();
        }
        return null;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        UpdateUnit other = (UpdateUnit)obj;
        if (!(this.impl == other.impl || this.impl != null && this.impl.equals(other.impl))) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + (this.impl != null ? this.impl.hashCode() : 0);
        return hash;
    }

    public String toString() {
        return this.impl.getCodeName();
    }

    static {
        Trampoline.API = new TrampolineAPI();
    }
}

