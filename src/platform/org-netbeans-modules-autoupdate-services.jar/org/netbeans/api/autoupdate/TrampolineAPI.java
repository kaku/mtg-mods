/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.autoupdate;

import java.io.File;
import java.io.IOException;
import org.netbeans.api.autoupdate.InstallSupport;
import org.netbeans.api.autoupdate.OperationContainer;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.api.autoupdate.UpdateUnitProvider;
import org.netbeans.modules.autoupdate.services.InstallSupportImpl;
import org.netbeans.modules.autoupdate.services.OperationContainerImpl;
import org.netbeans.modules.autoupdate.services.Trampoline;
import org.netbeans.modules.autoupdate.services.UpdateElementImpl;
import org.netbeans.modules.autoupdate.services.UpdateUnitImpl;
import org.netbeans.modules.autoupdate.services.UpdateUnitProviderImpl;
import org.netbeans.modules.autoupdate.updateprovider.UpdateItemImpl;
import org.netbeans.spi.autoupdate.AutoupdateClusterCreator;
import org.netbeans.spi.autoupdate.UpdateItem;

final class TrampolineAPI
extends Trampoline {
    TrampolineAPI() {
    }

    @Override
    protected UpdateUnit createUpdateUnit(UpdateUnitImpl impl) {
        UpdateUnit unit = new UpdateUnit(impl);
        impl.setUpdateUnit(unit);
        return unit;
    }

    @Override
    protected UpdateUnitImpl impl(UpdateUnit unit) {
        return unit.impl;
    }

    @Override
    protected UpdateElement createUpdateElement(UpdateElementImpl impl) {
        UpdateElement element = new UpdateElement(impl);
        impl.setUpdateElement(element);
        return element;
    }

    @Override
    protected UpdateElementImpl impl(UpdateElement element) {
        return element.impl;
    }

    @Override
    public UpdateItemImpl impl(UpdateItem item) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected UpdateItem createUpdateItem(UpdateItemImpl impl) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected OperationContainerImpl impl(OperationContainer container) {
        return container.impl;
    }

    @Override
    protected UpdateUnitProvider createUpdateUnitProvider(UpdateUnitProviderImpl impl) {
        return new UpdateUnitProvider(impl);
    }

    @Override
    public UpdateUnitProviderImpl impl(UpdateUnitProvider provider) {
        return provider.impl;
    }

    @Override
    protected OperationContainerImpl.OperationInfoImpl impl(OperationContainer.OperationInfo info) {
        return info.impl;
    }

    protected OperationContainer.OperationInfo createOperationInfo(OperationContainerImpl.OperationInfoImpl impl) {
        return new OperationContainer.OperationInfo(impl);
    }

    @Override
    protected File findCluster(String clusterName, AutoupdateClusterCreator creator) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected File[] registerCluster(String clusterName, File cluster, AutoupdateClusterCreator creator) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public InstallSupportImpl impl(InstallSupport support) {
        return support.impl;
    }
}

