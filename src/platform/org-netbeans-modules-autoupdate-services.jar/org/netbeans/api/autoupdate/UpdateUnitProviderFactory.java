/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 */
package org.netbeans.api.autoupdate;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import org.netbeans.api.autoupdate.UpdateUnitProvider;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.modules.autoupdate.services.UpdateUnitProviderImpl;
import org.netbeans.modules.autoupdate.updateprovider.ProviderCategory;

public final class UpdateUnitProviderFactory {
    private static final UpdateUnitProviderFactory INSTANCE = new UpdateUnitProviderFactory();

    private UpdateUnitProviderFactory() {
    }

    public static UpdateUnitProviderFactory getDefault() {
        return INSTANCE;
    }

    public List<UpdateUnitProvider> getUpdateUnitProviders(boolean onlyEnabled) {
        return UpdateUnitProviderImpl.getUpdateUnitProviders(onlyEnabled);
    }

    public UpdateUnitProvider create(String name, String displayName, URL url, UpdateUnitProvider.CATEGORY category) {
        return UpdateUnitProviderImpl.createUpdateUnitProvider(name, displayName, url, ProviderCategory.forValue(category));
    }

    public UpdateUnitProvider create(String name, String displayName, URL url, String categoryIconBase, String categoryDisplayName) {
        return UpdateUnitProviderImpl.createUpdateUnitProvider(name, displayName, url, ProviderCategory.create(categoryIconBase, categoryDisplayName));
    }

    public UpdateUnitProvider create(String name, String displayName, URL url) {
        return UpdateUnitProviderImpl.createUpdateUnitProvider(name, displayName, url, ProviderCategory.forValue(UpdateUnitProvider.CATEGORY.COMMUNITY));
    }

    public /* varargs */ UpdateUnitProvider create(String name, File ... files) {
        return UpdateUnitProviderImpl.createUpdateUnitProvider(name, files);
    }

    public void remove(UpdateUnitProvider unitProvider) {
        UpdateUnitProviderImpl.remove(unitProvider);
    }

    public void refreshProviders(ProgressHandle handle, boolean force) throws IOException {
        UpdateUnitProviderImpl.refreshProviders(handle, force);
    }
}

