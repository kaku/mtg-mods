/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 */
package org.netbeans.api.autoupdate;

import java.security.cert.Certificate;
import java.util.Collection;
import org.netbeans.api.autoupdate.OperationContainer;
import org.netbeans.api.autoupdate.OperationException;
import org.netbeans.api.autoupdate.OperationSupport;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.modules.autoupdate.services.InstallSupportImpl;

public final class InstallSupport {
    private OperationContainer<InstallSupport> container;
    InstallSupportImpl impl;

    InstallSupport() {
        this.impl = new InstallSupportImpl(this);
    }

    public Validator doDownload(ProgressHandle progress, boolean isGlobal) throws OperationException {
        if (this.impl.doDownload(progress, isGlobal ? Boolean.TRUE : null, false)) {
            return new Validator();
        }
        return null;
    }

    public Validator doDownload(ProgressHandle progress, Boolean isGlobal, boolean useUserdirAsFallback) throws OperationException {
        if (this.impl.doDownload(progress, isGlobal, useUserdirAsFallback)) {
            return new Validator();
        }
        return null;
    }

    public Installer doValidate(Validator validator, ProgressHandle progress) throws OperationException {
        if (this.impl.doValidate(validator, progress)) {
            return new Installer();
        }
        return null;
    }

    public OperationSupport.Restarter doInstall(Installer installer, ProgressHandle progress) throws OperationException {
        Boolean restart = this.impl.doInstall(installer, progress, false);
        if (restart == null || !restart.booleanValue()) {
            return null;
        }
        return new OperationSupport.Restarter();
    }

    public void doCancel() throws OperationException {
        this.impl.doCancel();
    }

    public void doRestart(OperationSupport.Restarter restarter, ProgressHandle progress) throws OperationException {
        this.impl.doRestart(restarter, progress);
    }

    public void doRestartLater(OperationSupport.Restarter restarter) {
        this.impl.doRestartLater(restarter);
    }

    public String getCertificate(Installer validator, UpdateElement uElement) {
        return this.impl.getCertificate(validator, uElement);
    }

    public Collection<Certificate> getCertificates(UpdateElement uElement) {
        return this.impl.getCertificates(uElement);
    }

    public boolean isTrusted(Installer validator, UpdateElement uElement) {
        return this.impl.isTrusted(validator, uElement);
    }

    public boolean isSigned(Installer validator, UpdateElement uElement) {
        return this.impl.isSigned(validator, uElement);
    }

    public OperationContainer<InstallSupport> getContainer() {
        return this.container;
    }

    void setContainer(OperationContainer<InstallSupport> c) {
        this.container = c;
    }

    public static final class Installer {
        private Installer() {
        }
    }

    public static final class Validator {
        private Validator() {
        }
    }

}

