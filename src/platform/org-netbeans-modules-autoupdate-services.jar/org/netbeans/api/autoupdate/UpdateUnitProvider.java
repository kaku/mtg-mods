/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 */
package org.netbeans.api.autoupdate;

import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import org.netbeans.api.autoupdate.UpdateManager;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.modules.autoupdate.services.UpdateUnitProviderImpl;

public final class UpdateUnitProvider {
    UpdateUnitProviderImpl impl;

    UpdateUnitProvider(UpdateUnitProviderImpl impl) {
        this.impl = impl;
    }

    public String getName() {
        return this.impl.getName();
    }

    public String getDisplayName() {
        return this.impl.getDisplayName();
    }

    public void setDisplayName(String name) {
        this.impl.setDisplayName(name);
    }

    public String getDescription() {
        return this.impl.getDescription();
    }

    @Deprecated
    public CATEGORY getCategory() {
        return this.impl.getCategory();
    }

    public Image getSourceIcon() {
        return this.impl.getSourceIcon();
    }

    public String getSourceDescription() {
        return this.impl.getSourceDescription();
    }

    public String getContentDescription() {
        return this.impl.getContentDescription();
    }

    public URL getProviderURL() {
        return this.impl.getProviderURL();
    }

    public void setProviderURL(URL url) {
        this.impl.setProviderURL(url);
    }

    public List<UpdateUnit> getUpdateUnits() {
        return this.impl.getUpdateUnits(new UpdateManager.TYPE[0]);
    }

    public /* varargs */ List<UpdateUnit> getUpdateUnits(UpdateManager.TYPE ... types) {
        return this.impl.getUpdateUnits(types);
    }

    public boolean refresh(ProgressHandle handle, boolean force) throws IOException {
        return this.impl.refresh(handle, force);
    }

    public boolean isEnabled() {
        return this.impl.isEnabled();
    }

    public void setEnable(boolean state) {
        this.impl.setEnable(state);
    }

    public String toString() {
        return super.toString() + "[" + this.impl + "]";
    }

    public static enum CATEGORY {
        STANDARD,
        COMMUNITY,
        BETA;
        

        private CATEGORY() {
        }
    }

}

