/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 */
package org.netbeans.api.autoupdate;

import org.netbeans.api.autoupdate.OperationContainer;
import org.netbeans.api.autoupdate.OperationException;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.modules.autoupdate.services.OperationContainerImpl;
import org.netbeans.modules.autoupdate.services.OperationSupportImpl;

public final class OperationSupport {
    private OperationContainer<OperationSupport> container;

    OperationSupport() {
    }

    public Restarter doOperation(ProgressHandle progress) throws OperationException {
        Boolean res = OperationSupport.getImpl(this.container.impl.getType()).doOperation(progress, this.container);
        if (res == null || !res.booleanValue()) {
            return null;
        }
        return new Restarter();
    }

    public void doCancel() throws OperationException {
        OperationSupport.getImpl(this.container.impl.getType()).doCancel();
    }

    public void doRestart(Restarter restarter, ProgressHandle progress) throws OperationException {
        OperationSupport.getImpl(this.container.impl.getType()).doRestart(restarter, progress);
    }

    public void doRestartLater(Restarter restarter) {
        OperationSupport.getImpl(this.container.impl.getType()).doRestartLater(restarter);
    }

    void setContainer(OperationContainer<OperationSupport> c) {
        this.container = c;
    }

    private static OperationSupportImpl getImpl(OperationContainerImpl.OperationType type) {
        assert (type != null);
        OperationSupportImpl impl = null;
        switch (type) {
            case INSTALL: {
                impl = OperationSupportImpl.forInstall();
                break;
            }
            case UNINSTALL: {
                impl = OperationSupportImpl.forUninstall();
                break;
            }
            case DIRECT_UNINSTALL: {
                impl = OperationSupportImpl.forDirectUninstall();
                break;
            }
            case UPDATE: {
                impl = OperationSupportImpl.forInstall();
                break;
            }
            case ENABLE: {
                impl = OperationSupportImpl.forEnable();
                break;
            }
            case DISABLE: {
                impl = OperationSupportImpl.forDisable();
                break;
            }
            case DIRECT_DISABLE: {
                impl = OperationSupportImpl.forDirectDisable();
                break;
            }
            case CUSTOM_INSTALL: {
                impl = OperationSupportImpl.forCustomInstall();
                break;
            }
            case CUSTOM_UNINSTALL: {
                impl = OperationSupportImpl.forCustomUninstall();
                break;
            }
            case INTERNAL_UPDATE: {
                impl = OperationSupportImpl.forInstall();
                break;
            }
            default: {
                assert (false);
                break;
            }
        }
        assert (impl != null);
        return impl;
    }

    public static final class Restarter {
        Restarter() {
        }
    }

}

