/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.autoupdate;

public final class OperationException
extends Exception {
    private ERROR_TYPE error;
    private String msg;

    public OperationException(ERROR_TYPE error) {
        this.error = error;
        this.msg = error.toString();
    }

    public OperationException(ERROR_TYPE error, Exception x) {
        super(x);
        this.error = error;
        this.msg = x.getLocalizedMessage();
    }

    public OperationException(ERROR_TYPE error, String message) {
        super(message);
        this.error = error;
        this.msg = message;
    }

    @Override
    public String getLocalizedMessage() {
        return this.msg;
    }

    public ERROR_TYPE getErrorType() {
        return this.error;
    }

    @Override
    public String toString() {
        String s = this.getClass().getName();
        return this.msg != null ? s + "[" + (Object)((Object)this.error) + "]: " + this.msg : s;
    }

    public static enum ERROR_TYPE {
        PROXY,
        INSTALLER,
        INSTALL,
        ENABLE,
        UNINSTALL,
        WRITE_PERMISSION;
        

        private ERROR_TYPE() {
        }
    }

}

