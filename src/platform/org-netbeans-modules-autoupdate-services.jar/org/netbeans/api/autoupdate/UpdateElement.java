/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.SpecificationVersion
 */
package org.netbeans.api.autoupdate;

import java.awt.Image;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.api.autoupdate.UpdateUnitProvider;
import org.netbeans.modules.autoupdate.services.UpdateElementImpl;
import org.netbeans.modules.autoupdate.services.UpdateManagerImpl;
import org.netbeans.modules.autoupdate.updateprovider.ProviderCategory;
import org.openide.modules.SpecificationVersion;

public final class UpdateElement {
    final UpdateElementImpl impl;

    UpdateElement(UpdateElementImpl elementImpl) {
        if (elementImpl == null) {
            throw new IllegalArgumentException("UpdateElementImpl cannot be null while creating UpdateElement.");
        }
        this.impl = elementImpl;
    }

    public UpdateUnit getUpdateUnit() {
        assert (this.impl.getUpdateUnit() != null);
        return this.impl.getUpdateUnit();
    }

    public String getCodeName() {
        return this.impl.getCodeName();
    }

    public String getDisplayName() {
        return this.impl.getDisplayName();
    }

    public String getSpecificationVersion() {
        return this.impl.getSpecificationVersion() == null ? null : this.impl.getSpecificationVersion().toString();
    }

    public boolean isEnabled() {
        return this.impl.isEnabled();
    }

    public String getDescription() {
        return this.impl.getDescription();
    }

    public String getNotification() {
        return this.impl.getNotification();
    }

    public String getSource() {
        return this.impl.getSource();
    }

    @Deprecated
    public UpdateUnitProvider.CATEGORY getSourceCategory() {
        UpdateUnitProvider provider = this.getUpdateUnitProvider();
        return provider != null ? provider.getCategory() : UpdateUnitProvider.CATEGORY.COMMUNITY;
    }

    public Image getSourceIcon() {
        UpdateUnitProvider provider = this.getUpdateUnitProvider();
        return provider != null ? provider.getSourceIcon() : ProviderCategory.forValue(UpdateUnitProvider.CATEGORY.COMMUNITY).getIcon();
    }

    public String getSourceDescription() {
        UpdateUnitProvider provider = this.getUpdateUnitProvider();
        return provider != null ? provider.getSourceDescription() : ProviderCategory.forValue(UpdateUnitProvider.CATEGORY.COMMUNITY).getDisplayName();
    }

    private UpdateUnitProvider getUpdateUnitProvider() {
        return UpdateManagerImpl.getInstance().getUpdateUnitProvider(this.getSource());
    }

    public String getAuthor() {
        return this.impl.getAuthor();
    }

    public String getHomepage() {
        return this.impl.getHomepage();
    }

    public int getDownloadSize() {
        return this.impl.getDownloadSize();
    }

    public String getCategory() {
        return this.impl.getCategory();
    }

    public String getDate() {
        return this.impl.getDate();
    }

    public String getLicenseId() {
        return this.impl.getLicenseId();
    }

    public String getLicence() {
        return this.impl.getLicence();
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        UpdateElement other = (UpdateElement)obj;
        if (!(this.impl == other.impl || this.impl != null && this.impl.equals(other.impl))) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.impl != null ? this.impl.hashCode() : 0);
        return hash;
    }

    public String toString() {
        return this.impl.getDisplayName() + "[" + this.impl.getCodeName() + "/" + (Object)this.impl.getSpecificationVersion() + "]";
    }
}

