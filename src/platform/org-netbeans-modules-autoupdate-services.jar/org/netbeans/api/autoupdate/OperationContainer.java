/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.autoupdate;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.autoupdate.InstallSupport;
import org.netbeans.api.autoupdate.OperationSupport;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.modules.autoupdate.services.OperationContainerImpl;

public final class OperationContainer<Support> {
    OperationContainerImpl<Support> impl;
    private final Support support;
    private Boolean upToDate = null;

    public static OperationContainer<InstallSupport> createForInstall() {
        OperationContainer<InstallSupport> retval = new OperationContainer<InstallSupport>(OperationContainerImpl.createForInstall(), new InstallSupport());
        retval.getSupportInner().setContainer(retval);
        return retval;
    }

    public static OperationContainer<InstallSupport> createForInternalUpdate() {
        OperationContainer<InstallSupport> retval = new OperationContainer<InstallSupport>(OperationContainerImpl.createForInternalUpdate(), new InstallSupport());
        retval.getSupportInner().setContainer(retval);
        return retval;
    }

    public static OperationContainer<OperationSupport> createForDirectInstall() {
        OperationContainer<OperationSupport> retval = new OperationContainer<OperationSupport>(OperationContainerImpl.createForDirectInstall(), new OperationSupport());
        retval.getSupportInner().setContainer(retval);
        return retval;
    }

    public static OperationContainer<InstallSupport> createForUpdate() {
        OperationContainer<InstallSupport> retval = new OperationContainer<InstallSupport>(OperationContainerImpl.createForUpdate(), new InstallSupport());
        retval.getSupportInner().setContainer(retval);
        return retval;
    }

    public static OperationContainer<OperationSupport> createForDirectUpdate() {
        OperationContainerImpl<OperationSupport> implContainerForDirectUpdate = OperationContainerImpl.createForDirectUpdate();
        OperationContainer<OperationSupport> retval = new OperationContainer<OperationSupport>(implContainerForDirectUpdate, new OperationSupport());
        retval.getSupportInner().setContainer(retval);
        return retval;
    }

    public static OperationContainer<OperationSupport> createForUninstall() {
        OperationContainer<OperationSupport> retval = new OperationContainer<OperationSupport>(OperationContainerImpl.createForUninstall(), new OperationSupport());
        retval.getSupportInner().setContainer(retval);
        return retval;
    }

    public static OperationContainer<OperationSupport> createForDirectUninstall() {
        OperationContainer<OperationSupport> retval = new OperationContainer<OperationSupport>(OperationContainerImpl.createForDirectUninstall(), new OperationSupport());
        retval.getSupportInner().setContainer(retval);
        return retval;
    }

    public static OperationContainer<OperationSupport> createForEnable() {
        OperationContainer<OperationSupport> retval = new OperationContainer<OperationSupport>(OperationContainerImpl.createForEnable(), new OperationSupport());
        retval.getSupportInner().setContainer(retval);
        return retval;
    }

    public static OperationContainer<OperationSupport> createForDisable() {
        OperationContainer<OperationSupport> retval = new OperationContainer<OperationSupport>(OperationContainerImpl.createForDisable(), new OperationSupport());
        retval.getSupportInner().setContainer(retval);
        return retval;
    }

    public static OperationContainer<OperationSupport> createForDirectDisable() {
        OperationContainer<OperationSupport> retval = new OperationContainer<OperationSupport>(OperationContainerImpl.createForDirectDisable(), new OperationSupport());
        retval.getSupportInner().setContainer(retval);
        return retval;
    }

    public static OperationContainer<OperationSupport> createForCustomInstallComponent() {
        OperationContainer<OperationSupport> retval = new OperationContainer<OperationSupport>(OperationContainerImpl.createForInstallNativeComponent(), new OperationSupport());
        retval.getSupportInner().setContainer(retval);
        return retval;
    }

    public static OperationContainer<OperationSupport> createForCustomUninstallComponent() {
        OperationContainer<OperationSupport> retval = new OperationContainer<OperationSupport>(OperationContainerImpl.createForUninstallNativeComponent(), new OperationSupport());
        retval.getSupportInner().setContainer(retval);
        return retval;
    }

    public Support getSupport() {
        if (this.upToDate != null && this.upToDate.booleanValue()) {
            return this.support;
        }
        if (this.listAll().size() > 0 && this.listInvalid().isEmpty()) {
            this.upToDate = true;
            return this.support;
        }
        return null;
    }

    Support getSupportInner() {
        return this.support;
    }

    public boolean canBeAdded(UpdateUnit updateUnit, UpdateElement updateElement) {
        return this.impl.isValid(updateUnit, updateElement);
    }

    public void add(Collection<UpdateElement> elems) {
        if (elems == null) {
            throw new IllegalArgumentException("Cannot add null value.");
        }
        for (UpdateElement el : elems) {
            this.add(el);
        }
    }

    public void add(Map<UpdateUnit, UpdateElement> elems) {
        if (elems == null) {
            throw new IllegalArgumentException("Cannot add null value.");
        }
        for (Map.Entry<UpdateUnit, UpdateElement> entry : elems.entrySet()) {
            this.add(entry.getKey(), entry.getValue());
        }
    }

    public OperationInfo<Support> add(UpdateUnit updateUnit, UpdateElement updateElement) {
        this.upToDate = false;
        return this.impl.add(updateUnit, updateElement);
    }

    public OperationInfo<Support> add(UpdateElement updateElement) {
        this.upToDate = false;
        UpdateUnit updateUnit = updateElement.getUpdateUnit();
        return this.add(updateUnit, updateElement);
    }

    public void remove(Collection<UpdateElement> elems) {
        if (elems == null) {
            throw new IllegalArgumentException("Cannot add null value.");
        }
        for (UpdateElement el : elems) {
            this.remove(el);
        }
    }

    public boolean remove(UpdateElement updateElement) {
        if (this.upToDate != null) {
            this.upToDate = false;
        }
        return this.impl.remove(updateElement);
    }

    public boolean contains(UpdateElement updateElement) {
        return this.impl.contains(updateElement);
    }

    public List<OperationInfo<Support>> listAll() {
        return this.impl.listAllWithPossibleEager();
    }

    public List<OperationInfo<Support>> listInvalid() {
        return this.impl.listInvalid();
    }

    public void remove(OperationInfo<Support> op) {
        if (this.upToDate != null) {
            this.upToDate = false;
        }
        this.impl.remove(op);
    }

    public void removeAll() {
        if (this.upToDate != null) {
            this.upToDate = false;
        }
        this.impl.removeAll();
    }

    private OperationContainer(OperationContainerImpl<Support> impl, Support t) {
        this.impl = impl;
        this.support = t;
    }

    public String toString() {
        return super.toString() + "+" + this.impl;
    }

    public static final class OperationInfo<Support> {
        OperationContainerImpl<Support> impl;

        OperationInfo(OperationContainerImpl<Support> impl) {
            this.impl = impl;
        }

        public UpdateElement getUpdateElement() {
            return this.impl.getUpdateElement();
        }

        public UpdateUnit getUpdateUnit() {
            return this.impl.getUpdateUnit();
        }

        public Set<UpdateElement> getRequiredElements() {
            return new LinkedHashSet<UpdateElement>(this.impl.getRequiredElements());
        }

        public Set<String> getBrokenDependencies() {
            return this.impl.getBrokenDependencies();
        }

        public String toString() {
            return "OperationInfo: " + this.impl.getUpdateElement().toString();
        }
    }

}

