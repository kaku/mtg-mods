/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.autoupdate;

import java.util.List;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.modules.autoupdate.services.UpdateManagerImpl;

public final class UpdateManager {
    private static UpdateManager mgr = null;

    private UpdateManager() {
    }

    public static final UpdateManager getDefault() {
        if (mgr == null) {
            mgr = new UpdateManager();
        }
        return mgr;
    }

    public List<UpdateUnit> getUpdateUnits() {
        return UpdateManager.getImpl().getUpdateUnits();
    }

    public /* varargs */ List<UpdateUnit> getUpdateUnits(TYPE ... types) {
        return UpdateManager.getImpl().getUpdateUnits(types);
    }

    private static UpdateManagerImpl getImpl() {
        return UpdateManagerImpl.getInstance();
    }

    public static enum TYPE {
        MODULE,
        FEATURE,
        STANDALONE_MODULE,
        KIT_MODULE,
        CUSTOM_HANDLED_COMPONENT,
        LOCALIZATION;
        

        private TYPE() {
        }
    }

}

