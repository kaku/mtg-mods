/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.mimelookup;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.netbeans.api.editor.mimelookup.MimeRegistration;

@Target(value={ElementType.TYPE, ElementType.METHOD})
@Retention(value=RetentionPolicy.SOURCE)
public @interface MimeRegistrations {
    public MimeRegistration[] value();
}

