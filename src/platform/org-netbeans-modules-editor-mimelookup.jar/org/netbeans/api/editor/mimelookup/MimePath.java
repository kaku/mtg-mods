/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.mimelookup;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.modules.editor.mimelookup.MimePathLookup;

public final class MimePath {
    public static final MimePath EMPTY = new MimePath();
    private static final Object LOCK = new Object();
    private static final ArrayList<MimePath> LRU = new ArrayList();
    static final int MAX_LRU_SIZE = 3;
    private static final Pattern REG_NAME_PATTERN = Pattern.compile("^[[\\p{Alnum}][!#$&.+\\-^_]]{1,127}$");
    private static final Set<String> WELL_KNOWN_TYPES = new HashSet<String>(Arrays.asList("application", "audio", "content", "image", "message", "model", "multipart", "text", "video"));
    private static final Map<String, Reference<MimePath>> string2mimePath = new HashMap<String, Reference<MimePath>>();
    private final MimePath[] mimePaths;
    private final String path;
    private final String mimeType;
    private Map<String, SoftReference<MimePath>> mimeType2mimePathRef;
    private MimePathLookup lookup;
    private final String LOOKUP_LOCK = new String("MimePath.LOOKUP_LOCK");

    public static MimePath get(String mimeType) {
        if (mimeType == null || mimeType.length() == 0) {
            return EMPTY;
        }
        return MimePath.get(EMPTY, mimeType);
    }

    public static MimePath get(MimePath prefix, String mimeType) {
        if (!MimePath.validate(mimeType)) {
            throw new IllegalArgumentException("Invalid mimeType=\"" + mimeType + "\"");
        }
        return prefix.getEmbedded(mimeType);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static MimePath parse(String path) {
        Object mpRef;
        MimePath mimePath;
        assert (path != null);
        Map<String, Reference<MimePath>> map = string2mimePath;
        synchronized (map) {
            mpRef = string2mimePath.get(path);
            MimePath mimePath2 = mimePath = mpRef != null ? mpRef.get() : null;
            if (mimePath != null) {
                return mimePath;
            }
        }
        Object o = MimePath.parseImpl(path, false);
        if (!(o instanceof MimePath)) {
            throw new IllegalArgumentException((String)o);
        }
        mpRef = string2mimePath;
        synchronized (mpRef) {
            mimePath = (MimePath)o;
            string2mimePath.put(path.intern(), new WeakReference<MimePath>(mimePath));
            return mimePath;
        }
    }

    public static boolean validate(CharSequence type, CharSequence subtype) {
        if (type != null) {
            if (MimePath.startsWith(type, "test")) {
                for (int i = 4; i < type.length(); ++i) {
                    if (type.charAt(i) != '_') continue;
                    type = type.subSequence(i + 1, type.length());
                    break;
                }
            }
            if (!WELL_KNOWN_TYPES.contains(type.toString())) {
                return false;
            }
        }
        if (subtype != null && !REG_NAME_PATTERN.matcher(subtype).matches()) {
            return false;
        }
        return true;
    }

    public static boolean validate(CharSequence path) {
        return !(MimePath.parseImpl(path, true) instanceof String);
    }

    private MimePath(MimePath prefix, String mimeType) {
        int prefixSize = prefix.size();
        this.mimePaths = new MimePath[prefixSize + 1];
        System.arraycopy(prefix.mimePaths, 0, this.mimePaths, 0, prefixSize);
        this.mimePaths[prefixSize] = this;
        String prefixPath = prefix.path;
        this.path = prefixPath != null && prefixPath.length() > 0 ? (prefixPath + '/' + mimeType).intern() : mimeType.intern();
        this.mimeType = mimeType;
    }

    private MimePath() {
        this.mimePaths = new MimePath[0];
        this.path = "";
        this.mimeType = "";
    }

    public String getPath() {
        return this.path;
    }

    public int size() {
        return this.mimePaths.length;
    }

    public String getMimeType(int index) {
        return this.mimePaths[index].mimeType;
    }

    public MimePath getPrefix(int size) {
        return size == 0 ? EMPTY : this.mimePaths[size - 1];
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private MimePath getEmbedded(String mimeType) {
        Object object = LOCK;
        synchronized (object) {
            MimePath mimePath;
            Reference mpRef;
            if (this.mimeType2mimePathRef == null) {
                this.mimeType2mimePathRef = new HashMap<String, SoftReference<MimePath>>();
            }
            if ((mpRef = (Reference)this.mimeType2mimePathRef.get(mimeType)) == null || (mimePath = (MimePath)mpRef.get()) == null) {
                mimePath = new MimePath(this, mimeType);
                this.mimeType2mimePathRef.put(mimeType, new SoftReference<MimePath>(mimePath));
                LRU.add(0, mimePath);
                if (LRU.size() > 3) {
                    LRU.remove(LRU.size() - 1);
                }
            }
            return mimePath;
        }
    }

    private static Object parseImpl(CharSequence path, boolean validateOnly) {
        MimePath mimePath = EMPTY;
        int pathLen = path.length();
        int startIndex = 0;
        do {
            int index;
            int slashIndex = -1;
            for (index = startIndex; index < pathLen; ++index) {
                if (path.charAt(index) != '/') continue;
                slashIndex = index;
                break;
            }
            if (slashIndex == -1) {
                if (index == startIndex) break;
                return "mimeType '" + path.subSequence(startIndex, path.length()) + "' does not contain '/'.";
            }
            ++index;
            while (index < pathLen) {
                if (path.charAt(index) == '/') {
                    if (index != slashIndex + 1) break;
                    return "Two successive slashes in '" + path.subSequence(startIndex, path.length()) + "'";
                }
                ++index;
            }
            if (index == slashIndex + 1) {
                return "Empty string after '/' in '" + path.subSequence(startIndex, path.length()) + "'";
            }
            if (!MimePath.validate(path.subSequence(startIndex, slashIndex), path.subSequence(slashIndex + 1, index))) {
                return "Invalid mimeType=\"" + path.subSequence(startIndex, index) + "\"";
            }
            if (!validateOnly) {
                String mimeType = path.subSequence(startIndex, index).toString();
                mimePath = mimePath.getEmbedded(mimeType);
            }
            startIndex = index + 1;
        } while (true);
        return mimePath;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    MimePathLookup getLookup() {
        String string = this.LOOKUP_LOCK;
        synchronized (string) {
            if (this.lookup == null) {
                this.lookup = new MimePathLookup(this);
            }
            return this.lookup;
        }
    }

    public String toString() {
        return "MimePath[" + this.path + "]";
    }

    private static boolean startsWith(CharSequence sequence, CharSequence subSequence) {
        if (sequence.length() < subSequence.length()) {
            return false;
        }
        for (int i = 0; i < subSequence.length(); ++i) {
            if (sequence.charAt(i) == subSequence.charAt(i)) continue;
            return false;
        }
        return true;
    }

    public String getInheritedType() {
        if ("".equals(this.mimeType)) {
            return null;
        }
        MimePath lastType = this.size() == 1 ? this : MimePath.parse(this.mimeType);
        List<String> inheritedPaths = lastType.getInheritedPaths(null, null);
        if (inheritedPaths.size() > 1) {
            return inheritedPaths.get(1);
        }
        return null;
    }

    public List<MimePath> getIncludedPaths() {
        List<String> paths = this.getInheritedPaths(null, null);
        ArrayList<MimePath> mpaths = new ArrayList<MimePath>(paths.size());
        for (String p : paths) {
            mpaths.add(MimePath.parse(p));
        }
        return mpaths;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    List<String> getInheritedPaths(String prefixPath, String suffixPath) {
        Object object = LOCK;
        synchronized (object) {
            ArrayList<String[]> arrays = new ArrayList<String[]>(this.size());
            String[] mimePathArray = MimePath.split(this);
            for (int i = 0; i <= mimePathArray.length; ++i) {
                String genericMimeType;
                String[] arr = new String[mimePathArray.length - i];
                for (int j = 0; j < arr.length; ++j) {
                    arr[j] = mimePathArray[i + j];
                }
                arrays.add(arr);
                if (arr.length <= 0 || (genericMimeType = MimePath.getGenericPartOfCompoundMimeType(arr[0])) == null) continue;
                String[] arr2 = new String[arr.length];
                System.arraycopy(arr, 0, arr2, 0, arr.length);
                arr2[0] = genericMimeType;
                arrays.add(arr2);
            }
            ArrayList<String> paths = new ArrayList<String>(arrays.size());
            for (String[] p : arrays) {
                StringBuilder sb = new StringBuilder(10 * p.length + 20);
                if (prefixPath != null && prefixPath.length() > 0) {
                    sb.append(prefixPath);
                }
                for (int ii = 0; ii < p.length; ++ii) {
                    if (p[ii].length() <= 0) continue;
                    if (sb.length() > 0) {
                        sb.append('/');
                    }
                    sb.append(p[ii]);
                }
                if (suffixPath != null && suffixPath.length() > 0) {
                    if (sb.length() > 0) {
                        sb.append('/');
                    }
                    sb.append(suffixPath);
                }
                paths.add(sb.toString());
            }
            return paths;
        }
    }

    static String getGenericPartOfCompoundMimeType(String mimeType) {
        int plusIdx = mimeType.lastIndexOf(43);
        if (plusIdx != -1 && plusIdx < mimeType.length() - 1) {
            int slashIdx = mimeType.indexOf(47);
            String prefix = mimeType.substring(0, slashIdx + 1);
            String suffix = mimeType.substring(plusIdx + 1);
            if (suffix.equals("xml")) {
                prefix = "text/";
            }
            return prefix + suffix;
        }
        return null;
    }

    private static String[] split(MimePath mimePath) {
        String[] array = new String[mimePath.size()];
        for (int i = 0; i < mimePath.size(); ++i) {
            array[i] = mimePath.getMimeType(i);
        }
        return array;
    }
}

