/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 */
package org.netbeans.api.editor.mimelookup;

import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.mimelookup.MimePathLookup;
import org.openide.util.Lookup;

public final class MimeLookup
extends Lookup {
    private MimePathLookup mimePathLookup;

    public static Lookup getLookup(MimePath mimePath) {
        if (mimePath == null) {
            throw new NullPointerException("The mimePath parameter must not be null.");
        }
        return mimePath.getLookup();
    }

    public static Lookup getLookup(String mimePath) {
        return MimeLookup.getLookup(MimePath.parse(mimePath));
    }

    public static MimeLookup getMimeLookup(String mimeType) {
        if (mimeType == null) {
            throw new NullPointerException("The mimeType parameter must not be null.");
        }
        return new MimeLookup(MimePath.get(mimeType).getLookup());
    }

    private MimeLookup(MimePathLookup lookup) {
        this.mimePathLookup = lookup;
    }

    public MimeLookup childLookup(String mimeType) {
        if (mimeType == null) {
            throw new NullPointerException("The mimeType parameter must not be null.");
        }
        MimePath mimePath = MimePath.get(this.mimePathLookup.getMimePath(), mimeType);
        return new MimeLookup(mimePath.getLookup());
    }

    public <T> T lookup(Class<T> clazz) {
        return (T)this.mimePathLookup.lookup(clazz);
    }

    public <T> Lookup.Result<T> lookup(Lookup.Template<T> template) {
        return this.mimePathLookup.lookup(template);
    }
}

