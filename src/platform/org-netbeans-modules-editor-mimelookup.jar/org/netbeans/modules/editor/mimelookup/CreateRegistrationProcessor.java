/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbCollections
 */
package org.netbeans.modules.editor.mimelookup;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.processing.Completion;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ExecutableType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbCollections;

@SupportedAnnotationTypes(value={"org.netbeans.api.editor.mimelookup.MimeRegistration", "org.netbeans.api.editor.mimelookup.MimeRegistrations", "org.netbeans.spi.editor.mimelookup.MimeLocation"})
@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
public class CreateRegistrationProcessor
extends LayerGeneratingProcessor {
    private static final String[] DEFAULT_COMPLETIONS = new String[]{"text/plain", "text/xml", "text/x-java"};
    private Processor COMPLETIONS;

    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        TypeElement mimeRegistration = this.processingEnv.getElementUtils().getTypeElement("org.netbeans.api.editor.mimelookup.MimeRegistration");
        for (Element el : roundEnv.getElementsAnnotatedWith(mimeRegistration)) {
            for (AnnotationMirror am : el.getAnnotationMirrors()) {
                if (!mimeRegistration.equals(am.getAnnotationType().asElement())) continue;
                this.process(el, am);
            }
        }
        TypeElement mimeRegistrations = this.processingEnv.getElementUtils().getTypeElement("org.netbeans.api.editor.mimelookup.MimeRegistrations");
        for (Element el2 : roundEnv.getElementsAnnotatedWith(mimeRegistrations)) {
            for (AnnotationMirror am : el2.getAnnotationMirrors()) {
                if (!mimeRegistrations.equals(am.getAnnotationType().asElement())) continue;
                for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> e : am.getElementValues().entrySet()) {
                    if (!e.getKey().getSimpleName().contentEquals("value")) continue;
                    for (AnnotationMirror r : NbCollections.iterable((Iterator)NbCollections.checkedIteratorByFilter(((Iterable)e.getValue().getValue()).iterator(), AnnotationMirror.class, (boolean)true))) {
                        this.process(el2, r);
                    }
                }
            }
        }
        TypeElement mimeLocation = this.processingEnv.getElementUtils().getTypeElement("org.netbeans.spi.editor.mimelookup.MimeLocation");
        for (TypeElement el3 : ElementFilter.typesIn(roundEnv.getElementsAnnotatedWith(mimeLocation))) {
            for (AnnotationMirror am : el3.getAnnotationMirrors()) {
                if (!mimeLocation.equals(am.getAnnotationType().asElement())) continue;
                this.checkMimeLocation(el3, am);
            }
        }
        return true;
    }

    private void process(Element toRegister, AnnotationMirror mimeRegistration) throws LayerGenerationException {
        TypeMirror service = null;
        String mimeType = null;
        int position = Integer.MAX_VALUE;
        for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> e : mimeRegistration.getElementValues().entrySet()) {
            Name simpleName = e.getKey().getSimpleName();
            if (simpleName.contentEquals("service")) {
                service = (TypeMirror)e.getValue().getValue();
                continue;
            }
            if (simpleName.contentEquals("mimeType")) {
                mimeType = (String)e.getValue().getValue();
                continue;
            }
            if (!simpleName.contentEquals("position")) continue;
            position = (Integer)e.getValue().getValue();
        }
        if (mimeType != null) {
            if (mimeType.length() != 0) {
                mimeType = "/" + mimeType;
            }
            String folder = "";
            TypeElement apiTE = (TypeElement)this.processingEnv.getTypeUtils().asElement(service);
            TypeElement location = this.processingEnv.getElementUtils().getTypeElement("org.netbeans.spi.editor.mimelookup.MimeLocation");
            block1 : for (AnnotationMirror am : apiTE.getAnnotationMirrors()) {
                if (!location.equals(am.getAnnotationType().asElement())) continue;
                for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> e2 : am.getElementValues().entrySet()) {
                    if (!e2.getKey().getSimpleName().contentEquals("subfolderName")) continue;
                    folder = "/" + (String)e2.getValue().getValue();
                    break block1;
                }
            }
            this.instantiableClassOrMethod(toRegister, apiTE);
            this.layer(new Element[]{toRegister}).instanceFile("Editors" + mimeType + folder, null, null).position(position).stringvalue("instanceOf", this.processingEnv.getElementUtils().getBinaryName(apiTE).toString()).write();
        }
    }

    private void instantiableClassOrMethod(Element anntated, TypeElement apiClass) throws IllegalArgumentException, LayerGenerationException {
        DeclaredType typeMirror = this.processingEnv.getTypeUtils().getDeclaredType(apiClass, new TypeMirror[0]);
        switch (anntated.getKind()) {
            case CLASS: {
                String clazz = this.processingEnv.getElementUtils().getBinaryName((TypeElement)anntated).toString();
                if (anntated.getModifiers().contains((Object)Modifier.ABSTRACT)) {
                    throw new LayerGenerationException(clazz + " must not be abstract", anntated);
                }
                boolean hasDefaultCtor = false;
                for (ExecutableElement constructor : ElementFilter.constructorsIn(anntated.getEnclosedElements())) {
                    if (!constructor.getParameters().isEmpty()) continue;
                    hasDefaultCtor = true;
                    break;
                }
                if (!hasDefaultCtor) {
                    throw new LayerGenerationException(clazz + " must have a no-argument constructor", anntated);
                }
                if (typeMirror != null && !this.processingEnv.getTypeUtils().isAssignable(anntated.asType(), typeMirror)) {
                    throw new LayerGenerationException(clazz + " is not assignable to " + typeMirror, anntated);
                }
                if (!anntated.getModifiers().contains((Object)Modifier.PUBLIC)) {
                    throw new LayerGenerationException(clazz + " is not public", anntated);
                }
                return;
            }
            case METHOD: {
                String clazz = this.processingEnv.getElementUtils().getBinaryName((TypeElement)anntated.getEnclosingElement()).toString();
                String method = anntated.getSimpleName().toString();
                if (!anntated.getModifiers().contains((Object)Modifier.STATIC)) {
                    throw new LayerGenerationException(clazz + "." + method + " must be static", anntated);
                }
                if (!((ExecutableElement)anntated).getParameters().isEmpty()) {
                    throw new LayerGenerationException(clazz + "." + method + " must not take arguments", anntated);
                }
                if (typeMirror != null && !this.processingEnv.getTypeUtils().isAssignable(((ExecutableElement)anntated).getReturnType(), typeMirror)) {
                    throw new LayerGenerationException(clazz + "." + method + " is not assignable to " + typeMirror, anntated);
                }
                return;
            }
        }
        throw new IllegalArgumentException("Annotated element is not loadable as an instance: " + anntated);
    }

    public Iterable<? extends Completion> getCompletions(Element annotated, AnnotationMirror annotation, ExecutableElement attr, String userText) {
        if (this.processingEnv == null || annotated == null || !annotated.getKind().isClass()) {
            return Collections.emptyList();
        }
        if (annotation == null || !"org.netbeans.api.editor.mimelookup.MimeRegistration".contentEquals(((TypeElement)annotation.getAnnotationType().asElement()).getQualifiedName())) {
            return Collections.emptyList();
        }
        if ("mimeType".contentEquals(attr.getSimpleName())) {
            return this.completeMimePath(annotated, annotation, attr, userText);
        }
        if (!"service".contentEquals(attr.getSimpleName())) {
            return Collections.emptyList();
        }
        TypeElement jlObject = this.processingEnv.getElementUtils().getTypeElement("java.lang.Object");
        if (jlObject == null) {
            return Collections.emptyList();
        }
        LinkedList<TypeCompletion> result = new LinkedList<TypeCompletion>();
        LinkedList<TypeElement> toProcess = new LinkedList<TypeElement>();
        toProcess.add((TypeElement)annotated);
        while (!toProcess.isEmpty()) {
            TypeElement c = (TypeElement)toProcess.remove(0);
            result.add(new TypeCompletion(c.getQualifiedName().toString() + ".class"));
            LinkedList<? extends TypeMirror> parents = new LinkedList<TypeMirror>();
            parents.add(c.getSuperclass());
            parents.addAll(c.getInterfaces());
            for (TypeMirror tm : parents) {
                TypeElement type;
                if (tm == null || tm.getKind() != TypeKind.DECLARED || jlObject.equals(type = (TypeElement)this.processingEnv.getTypeUtils().asElement(tm))) continue;
                toProcess.add(type);
            }
        }
        return result;
    }

    private Iterable<? extends Completion> completeMimePath(Element element, AnnotationMirror annotation, ExecutableElement attr, String userText) {
        if (userText == null) {
            userText = "";
        }
        if (userText.startsWith("\"")) {
            userText = userText.substring(1);
        }
        HashSet<TypeCompletion> res = new HashSet<TypeCompletion>();
        if (this.COMPLETIONS == null) {
            String pathCompletions = System.getProperty("org.openide.awt.ActionReference.completion");
            if (pathCompletions != null) {
                ClassLoader l = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
                if (l == null) {
                    l = Thread.currentThread().getContextClassLoader();
                }
                if (l == null) {
                    l = CreateRegistrationProcessor.class.getClassLoader();
                }
                try {
                    this.COMPLETIONS = (Processor)Class.forName(pathCompletions, true, (ClassLoader)l).newInstance();
                }
                catch (Exception ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                    this.COMPLETIONS = this;
                }
            } else {
                return res;
            }
        }
        if (this.COMPLETIONS != null && this.COMPLETIONS != this) {
            this.COMPLETIONS.init(this.processingEnv);
            for (Completion completion : this.COMPLETIONS.getCompletions(element, annotation, attr, "Editors/" + userText)) {
                String[] arr;
                String v = completion.getValue();
                if (v == null || (arr = v.split("/")).length > 3 || arr.length < 2 || !arr[0].equals("\"Editors") || arr[1].length() == 0 || Character.isUpperCase(arr[1].charAt(0))) continue;
                if (arr.length > 2) {
                    res.add(new TypeCompletion("" + '\"' + arr[1] + '/' + arr[2]));
                    continue;
                }
                res.add(new TypeCompletion("" + '\"' + arr[1] + '/'));
            }
        }
        if (res.isEmpty()) {
            for (String c : DEFAULT_COMPLETIONS) {
                if (!c.startsWith(userText)) continue;
                res.add(new TypeCompletion("\"" + c));
            }
        }
        return res;
    }

    private void checkMimeLocation(TypeElement clazz, AnnotationMirror am) {
        for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> e : am.getElementValues().entrySet()) {
            TypeElement tipc;
            TypeMirror ipc;
            if (!e.getKey().getSimpleName().contentEquals("instanceProviderClass") || (ipc = (TypeMirror)e.getValue().getValue()) == null || ipc.getKind() != TypeKind.DECLARED) continue;
            TypeElement instanceProvider = this.processingEnv.getElementUtils().getTypeElement("org.netbeans.spi.editor.mimelookup.InstanceProvider");
            if (instanceProvider == null) {
                return;
            }
            ExecutableElement createInstance = null;
            for (ExecutableElement ee : ElementFilter.methodsIn(instanceProvider.getEnclosedElements())) {
                if (!ee.getSimpleName().contentEquals("createInstance")) continue;
                createInstance = ee;
                break;
            }
            if (createInstance == null) {
                throw new IllegalStateException("No instanceCreate in InstanceProvider!");
            }
            DeclaredType dipc = (DeclaredType)ipc;
            Types tu = this.processingEnv.getTypeUtils();
            ExecutableType member = (ExecutableType)tu.asMemberOf(dipc, createInstance);
            TypeMirror result = member.getReturnType();
            TypeMirror jlObject = this.processingEnv.getElementUtils().getTypeElement("java.lang.Object").asType();
            if (!tu.isSameType(tu.erasure(result), jlObject) && !tu.isSubtype(tu.erasure(result), tu.erasure(clazz.asType()))) {
                this.processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "The InstanceProvider does not create instances of type " + clazz.getQualifiedName(), clazz, am, e.getValue());
            }
            if (!(tipc = (TypeElement)dipc.asElement()).getModifiers().contains((Object)Modifier.PUBLIC)) {
                this.processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "The InstanceProvider implementation is not public.", clazz, am, e.getValue());
            }
            for (ExecutableElement c : ElementFilter.constructorsIn(tipc.getEnclosedElements())) {
                if (!c.getParameters().isEmpty() || !c.getModifiers().contains((Object)Modifier.PUBLIC)) continue;
                return;
            }
            this.processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "The InstanceProvider implementation does not provide a public no-arg constructor.", clazz, am, e.getValue());
        }
    }

    private static final class TypeCompletion
    implements Completion {
        private final String type;

        public TypeCompletion(String type) {
            this.type = type;
        }

        @Override
        public String getValue() {
            return this.type;
        }

        @Override
        public String getMessage() {
            return null;
        }
    }

}

