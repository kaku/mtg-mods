/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.RequestProcessor
 *  org.openide.util.WeakListeners
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.modules.editor.mimelookup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EventListener;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.spi.editor.mimelookup.MimeDataProvider;
import org.netbeans.spi.editor.mimelookup.MimeLookupInitializer;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;
import org.openide.util.lookup.ProxyLookup;

public final class MimePathLookup
extends ProxyLookup
implements LookupListener {
    private static final Logger LOG = Logger.getLogger(MimePathLookup.class.getName());
    private static final RequestProcessor WORKER = new RequestProcessor("MimePathLookupFiring", 1);
    private final MimePath mimePath;
    private final boolean mimePathBanned;
    private final Lookup.Result<MimeDataProvider> dataProviders;
    private final Lookup.Result<MimeLookupInitializer> mimeInitializers;
    private boolean initialized = false;

    public MimePathLookup(MimePath mimePath) {
        if (mimePath == null) {
            throw new NullPointerException("Mime path can't be null.");
        }
        this.mimePath = mimePath;
        this.mimePathBanned = mimePath.size() > 0 && mimePath.getMimeType(0).contains("text/base");
        this.dataProviders = Lookup.getDefault().lookup(new Lookup.Template(MimeDataProvider.class));
        this.dataProviders.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)((Object)this), this.dataProviders));
        this.mimeInitializers = Lookup.getDefault().lookup(new Lookup.Template(MimeLookupInitializer.class));
        this.mimeInitializers.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)((Object)this), this.mimeInitializers));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void beforeLookup(Lookup.Template<?> template) {
        MimePathLookup mimePathLookup = this;
        synchronized (mimePathLookup) {
            if (!this.initialized) {
                this.initialized = true;
                this.rebuild();
            }
        }
    }

    public MimePath getMimePath() {
        return this.mimePath;
    }

    private void rebuild() {
        ArrayList<Lookup> lookups = new ArrayList<Lookup>();
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Rebuilding MimeLookup for '" + this.mimePath.getPath() + "'...");
        }
        for (MimeDataProvider provider : this.dataProviders.allInstances()) {
            Lookup mimePathLookup;
            if (this.mimePathBanned && !this.isDefaultProvider(provider)) continue;
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("- Querying MimeDataProvider(" + this.mimePath.getPath() + "): " + provider);
            }
            if ((mimePathLookup = provider.getLookup(this.mimePath)) == null) continue;
            lookups.add(mimePathLookup);
        }
        List<MimePath> paths = this.mimePath.getIncludedPaths();
        for (MimePath mp : paths) {
            ArrayList initializers = this.mimeInitializers.allInstances();
            for (int i = 0; i < mp.size(); ++i) {
                ArrayList children = new ArrayList(initializers.size());
                for (MimeLookupInitializer mli : initializers) {
                    children.addAll(mli.child(mp.getMimeType(i)).allInstances());
                }
                initializers = children;
            }
            for (MimeLookupInitializer mli : initializers) {
                Lookup mimePathLookup;
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("- Querying MimeLookupInitializer(" + mp.getPath() + "): " + mli);
                }
                if ((mimePathLookup = mli.lookup()) == null) continue;
                lookups.add(mimePathLookup);
            }
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("MimeLookup for '" + this.mimePath.getPath() + "' rebuilt.");
        }
        this.setLookups((Executor)WORKER, lookups.toArray((T[])new Lookup[lookups.size()]));
    }

    private boolean isDefaultProvider(MimeDataProvider provider) {
        return provider.getClass().getName().equals("org.netbeans.modules.editor.mimelookup.impl.DefaultMimeDataProvider");
    }

    public synchronized void resultChanged(LookupEvent ev) {
        this.rebuild();
    }
}

