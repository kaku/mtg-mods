/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.editor.mimelookup;

import java.util.List;
import org.openide.filesystems.FileObject;

public interface InstanceProvider<T> {
    public T createInstance(List<FileObject> var1);
}

