/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.mimelookup;

import org.netbeans.spi.editor.mimelookup.InstanceProvider;

@Deprecated
public interface Class2LayerFolder<T> {
    public Class<T> getClazz();

    public String getLayerFolderName();

    public InstanceProvider<T> getInstanceProvider();
}

