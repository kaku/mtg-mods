/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.mimelookup;

import java.lang.annotation.Annotation;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.netbeans.spi.editor.mimelookup.InstanceProvider;

@Documented
@Retention(value=RetentionPolicy.RUNTIME)
@Target(value={ElementType.TYPE})
public @interface MimeLocation {
    public String subfolderName();

    public Class<? extends InstanceProvider> instanceProviderClass() default InstanceProvider.class;
}

