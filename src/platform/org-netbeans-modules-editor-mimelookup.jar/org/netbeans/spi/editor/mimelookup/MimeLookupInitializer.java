/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 */
package org.netbeans.spi.editor.mimelookup;

import org.openide.util.Lookup;

@Deprecated
public interface MimeLookupInitializer {
    public Lookup lookup();

    public Lookup.Result<MimeLookupInitializer> child(String var1);
}

