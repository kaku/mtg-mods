/*
 * Decompiled with CFR 0_118.
 */
package org.openide;

import javax.swing.JComponent;

public final class WizardValidationException
extends Exception {
    private String localizedMessage;
    private JComponent source;

    private WizardValidationException() {
    }

    public WizardValidationException(JComponent source, String message, String localizedMessage) {
        super(message);
        this.source = source;
        this.localizedMessage = localizedMessage;
    }

    public JComponent getSource() {
        return this.source;
    }

    @Override
    public String getLocalizedMessage() {
        return this.localizedMessage != null ? this.localizedMessage : this.getMessage();
    }
}

