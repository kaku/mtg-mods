/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.openide;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.text.BreakIterator;
import javax.accessibility.AccessibleContext;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.text.Keymap;
import org.openide.NotificationLineSupport;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class NotifyDescriptor {
    public static final String PROP_MESSAGE = "message";
    public static final String PROP_MESSAGE_TYPE = "messageType";
    public static final String PROP_OPTION_TYPE = "optionType";
    public static final String PROP_OPTIONS = "options";
    public static final String PROP_VALUE = "value";
    public static final String PROP_TITLE = "title";
    public static final String PROP_DETAIL = "detail";
    public static final String PROP_VALID = "valid";
    public static final String PROP_NO_DEFAULT_CLOSE = "noDefaultClose";
    public static final String PROP_ERROR_NOTIFICATION = "errorNotification";
    public static final String PROP_WARNING_NOTIFICATION = "warningNotification";
    public static final String PROP_INFO_NOTIFICATION = "infoNotification";
    public static final Object YES_OPTION = new Integer(0);
    public static final Object NO_OPTION = new Integer(1);
    public static final Object CANCEL_OPTION = new Integer(2);
    public static final Object OK_OPTION = new Integer(0);
    public static final Object CLOSED_OPTION = new Integer(-1);
    public static final int DEFAULT_OPTION = -1;
    public static final int YES_NO_OPTION = 0;
    public static final int YES_NO_CANCEL_OPTION = 1;
    public static final int OK_CANCEL_OPTION = 2;
    public static final int ERROR_MESSAGE = 0;
    public static final int INFORMATION_MESSAGE = 1;
    public static final int WARNING_MESSAGE = 2;
    public static final int QUESTION_MESSAGE = 3;
    public static final int PLAIN_MESSAGE = -1;
    private static final int MAXIMUM_TEXT_WIDTH = 100;
    private static final int SIZE_PREFERRED_WIDTH = 300;
    private static final int SIZE_PREFERRED_HEIGHT = 100;
    private Object message;
    private int messageType = -1;
    private int optionType;
    private Object[] options;
    private Object[] adOptions;
    private Object value;
    private Object defaultValue;
    private String title;
    private boolean valid = true;
    private NotificationLineSupport notificationLineSupport = null;
    private String infoMsg;
    private String warnMsg;
    private String errMsg;
    private PropertyChangeSupport changeSupport;
    private boolean noDefaultClose = false;
    private Color bg = UIManager.getLookAndFeelDefaults().getColor("3-main-dark-color");

    public NotifyDescriptor(Object message, String title, int optionType, int messageType, Object[] options, Object initialValue) {
        this.message = message;
        this.messageType = messageType;
        this.options = options;
        this.optionType = optionType;
        this.title = title;
        this.value = initialValue;
        this.defaultValue = initialValue;
    }

    protected void initialize() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void getterCalled() {
        boolean init = false;
        NotifyDescriptor notifyDescriptor = this;
        synchronized (notifyDescriptor) {
            if (this.changeSupport == null) {
                this.changeSupport = new PropertyChangeSupport(this);
                init = true;
            }
        }
        if (init) {
            this.initialize();
        }
    }

    public final boolean isValid() {
        this.getterCalled();
        return this.valid;
    }

    public final void setValid(boolean newValid) {
        boolean oldValid = this.valid;
        this.valid = newValid;
        this.firePropertyChange("valid", oldValid ? Boolean.TRUE : Boolean.FALSE, newValid ? Boolean.TRUE : Boolean.FALSE);
    }

    public void setMessage(Object newMessage) {
        Object oldMessage = this.message;
        if (newMessage instanceof String) {
            JTextArea area = new JTextArea((String)newMessage);
            area.setPreferredSize(new Dimension(300, 100));
            area.setBackground(UIManager.getColor("Label.background"));
            area.setBorder(BorderFactory.createEmptyBorder());
            area.setLineWrap(true);
            area.setWrapStyleWord(true);
            area.setEditable(false);
            area.setFocusable(true);
            area.getAccessibleContext().setAccessibleName(NbBundle.getMessage(NotifyDescriptor.class, (String)"ACN_NotifyDescriptor_MessageJTextArea"));
            area.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(NotifyDescriptor.class, (String)"ACD_NotifyDescriptor_MessageJTextArea"));
            newMessage = area;
        }
        this.message = newMessage;
        this.firePropertyChange("message", oldMessage, newMessage);
    }

    public Object getMessage() {
        this.getterCalled();
        return this.message;
    }

    public void setMessageType(int newType) {
        if (newType != 0 && newType != 1 && newType != 2 && newType != 3 && newType != -1) {
            throw new IllegalArgumentException("Message type must be one of the following: ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE, QUESTION_MESSAGE or PLAIN_MESSAGE.");
        }
        int oldType = this.messageType;
        this.messageType = newType;
        this.firePropertyChange("messageType", new Integer(oldType), new Integer(this.messageType));
    }

    public int getMessageType() {
        this.getterCalled();
        return this.messageType;
    }

    public void setOptionType(int newType) {
        if (newType != -1 && newType != 0 && newType != 1 && newType != 2) {
            throw new IllegalArgumentException("Option type must be one of the following: DEFAULT_OPTION, YES_NO_OPTION, YES_NO_CANCEL_OPTION or OK_CANCEL_OPTION.");
        }
        int oldType = this.optionType;
        this.optionType = newType;
        this.firePropertyChange("optionType", new Integer(oldType), new Integer(this.optionType));
    }

    public void setBackground(Color color) {
        this.bg = color;
    }

    public Color getBackground() {
        return this.bg;
    }

    public int getOptionType() {
        this.getterCalled();
        return this.optionType;
    }

    public void setOptions(Object[] newOptions) {
        Object[] oldOptions = this.options;
        this.options = newOptions;
        this.firePropertyChange("options", oldOptions, newOptions);
    }

    public Object[] getOptions() {
        this.getterCalled();
        if (this.options != null) {
            return (Object[])this.options.clone();
        }
        return this.options;
    }

    public void setAdditionalOptions(Object[] newOptions) {
        Object[] oldOptions = this.adOptions;
        this.adOptions = newOptions;
        this.firePropertyChange("options", oldOptions, newOptions);
    }

    public Object[] getAdditionalOptions() {
        this.getterCalled();
        if (this.adOptions != null) {
            return (Object[])this.adOptions.clone();
        }
        return null;
    }

    void setValueWithoutPCH(Object newValue) {
        this.value = newValue;
    }

    public void setValue(Object newValue) {
        Object oldValue = this.value;
        this.setValueWithoutPCH(newValue);
        this.firePropertyChange("value", oldValue, newValue);
    }

    public Object getValue() {
        this.getterCalled();
        return this.value;
    }

    public Object getDefaultValue() {
        return this.defaultValue;
    }

    public void setTitle(String newTitle) {
        String oldTitle = this.title;
        this.title = newTitle;
        this.firePropertyChange("title", oldTitle, newTitle);
    }

    public String getTitle() {
        this.getterCalled();
        return this.title;
    }

    public final NotificationLineSupport createNotificationLineSupport() {
        this.notificationLineSupport = new NotificationLineSupport(this);
        return this.notificationLineSupport;
    }

    public final NotificationLineSupport getNotificationLineSupport() {
        return this.notificationLineSupport;
    }

    void setInformationMessage(String msg) {
        if (this.notificationLineSupport == null) {
            throw new IllegalStateException("NotificationLineSupport wasn't created yet.");
        }
        this.infoMsg = msg;
        this.warnMsg = null;
        this.errMsg = null;
        this.firePropertyChange("infoNotification", null, msg);
    }

    void setWarningMessage(String msg) {
        if (this.notificationLineSupport == null) {
            throw new IllegalStateException("NotificationLineSupport wasn't created yet.");
        }
        this.infoMsg = null;
        this.warnMsg = msg;
        this.errMsg = null;
        this.firePropertyChange("warningNotification", null, msg);
    }

    void setErrorMessage(String msg) {
        if (this.notificationLineSupport == null) {
            throw new IllegalStateException("NotificationLineSupport wasn't created yet.");
        }
        this.infoMsg = null;
        this.warnMsg = null;
        this.errMsg = msg;
        this.firePropertyChange("errorNotification", null, msg);
    }

    String getInformationMessage() {
        if (this.notificationLineSupport == null) {
            throw new IllegalStateException("NotificationLineSupport wasn't created yet.");
        }
        return this.infoMsg;
    }

    String getWarningMessage() {
        if (this.notificationLineSupport == null) {
            throw new IllegalStateException("NotificationLineSupport wasn't created yet.");
        }
        return this.warnMsg;
    }

    String getErrorMessage() {
        if (this.notificationLineSupport == null) {
            throw new IllegalStateException("NotificationLineSupport wasn't created yet.");
        }
        return this.errMsg;
    }

    void clearMessages() {
        if (this.notificationLineSupport == null) {
            throw new IllegalStateException("NotificationLineSupport wasn't created yet.");
        }
        this.infoMsg = null;
        this.warnMsg = null;
        this.errMsg = null;
        this.firePropertyChange("infoNotification", null, null);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.getterCalled();
        this.changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        if (this.changeSupport != null) {
            this.changeSupport.removePropertyChangeListener(listener);
        }
    }

    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if (this.changeSupport != null) {
            this.changeSupport.firePropertyChange(propertyName, oldValue, newValue);
        }
    }

    public void setNoDefaultClose(boolean noDefaultClose) {
        boolean oldValue = this.noDefaultClose;
        this.noDefaultClose = noDefaultClose;
        this.firePropertyChange("noDefaultClose", oldValue, noDefaultClose);
    }

    public boolean isNoDefaultClose() {
        return this.noDefaultClose;
    }

    protected static String getTitleForType(int messageType) {
        switch (messageType) {
            case 0: {
                return NbBundle.getMessage(NotifyDescriptor.class, (String)"NTF_ErrorTitle");
            }
            case 2: {
                return NbBundle.getMessage(NotifyDescriptor.class, (String)"NTF_WarningTitle");
            }
            case 3: {
                return NbBundle.getMessage(NotifyDescriptor.class, (String)"NTF_QuestionTitle");
            }
            case 1: {
                return NbBundle.getMessage(NotifyDescriptor.class, (String)"NTF_InformationTitle");
            }
            case -1: {
                return NbBundle.getMessage(NotifyDescriptor.class, (String)"NTF_PlainTitle");
            }
        }
        return "";
    }

    public static class InputLine
    extends NotifyDescriptor {
        protected JTextField textField;

        public InputLine(String text, String title) {
            this(text, title, 2, -1);
        }

        public InputLine(String text, String title, int optionType, int messageType) {
            super(null, title, optionType, messageType, null, null);
            super.setMessage(this.createDesign(text));
        }

        public String getInputText() {
            return this.textField.getText();
        }

        public void setInputText(String text) {
            this.textField.setText(text);
            this.textField.selectAll();
        }

        protected Component createDesign(String text) {
            JPanel panel = new JPanel();
            panel.setOpaque(false);
            JLabel textLabel = new JLabel();
            Mnemonics.setLocalizedText((JLabel)textLabel, (String)text);
            boolean longText = text.length() > 80;
            this.textField = new JTextField(25);
            textLabel.setLabelFor(this.textField);
            this.textField.requestFocus();
            GroupLayout layout = new GroupLayout(panel);
            panel.setLayout(layout);
            if (longText) {
                layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(textLabel, -1, -1, 32767).addGap(32, 32, 32)).addComponent(this.textField)).addContainerGap()));
            } else {
                layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(textLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.textField, -1, 207, 32767).addContainerGap()));
            }
            if (longText) {
                layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(textLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.textField, -2, -1, -2).addContainerGap(-1, 32767)));
            } else {
                layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(textLabel).addComponent(this.textField, -2, -1, -2)).addContainerGap(-1, 32767)));
            }
            KeyStroke enter = KeyStroke.getKeyStroke(10, 0);
            Keymap map = this.textField.getKeymap();
            map.removeKeyStrokeBinding(enter);
            panel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(NotifyDescriptor.class, (String)"ACSD_InputPanel"));
            this.textField.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(NotifyDescriptor.class, (String)"ACSD_InputField"));
            return panel;
        }
    }

    @Deprecated
    public static final class Exception
    extends Confirmation {
        static final long serialVersionUID = -3387516993124229948L;

        public Exception(Throwable detail) {
            Object obj;
            this(detail, detail.getMessage());
            if (detail instanceof InvocationTargetException) {
                Throwable target = ((InvocationTargetException)detail).getTargetException();
                this.setMessage(target);
                Object msgObj = this.getMessage();
                if (msgObj == null || "".equals(msgObj)) {
                    String msg = target.getMessage();
                    msg = Utilities.wrapString((String)msg, (int)100, (BreakIterator)BreakIterator.getCharacterInstance(), (boolean)false);
                    this.setMessage(msg);
                }
            }
            if ((obj = this.getMessage()) == null || "".equals(obj)) {
                this.setMessage(NbBundle.getMessage(NotifyDescriptor.class, (String)"NTF_ExceptionalException", (Object)detail.getClass().getName(), (Object)(System.getProperty("netbeans.user") + File.separator + "system")));
                this.setTitle(NbBundle.getMessage(NotifyDescriptor.class, (String)"NTF_ExceptionalExceptionTitle"));
            }
        }

        public Exception(Throwable detail, Object message) {
            super(message, -1, 0);
            this.setTitle(NbBundle.getMessage(NotifyDescriptor.class, (String)"NTF_ExceptionTitle"));
        }
    }

    public static class Confirmation
    extends NotifyDescriptor {
        public Confirmation(Object message) {
            this(message, 1);
        }

        public Confirmation(Object message, String title) {
            this(message, title, 1);
        }

        public Confirmation(Object message, int optionType) {
            this(message, optionType, 3);
        }

        public Confirmation(Object message, String title, int optionType) {
            this(message, title, optionType, 3);
        }

        public Confirmation(Object message, int optionType, int messageType) {
            Object[] arrobject;
            if (optionType == -1) {
                Object[] arrobject2 = new Object[1];
                arrobject = arrobject2;
                arrobject2[0] = OK_OPTION;
            } else {
                arrobject = null;
            }
            super(message, NotifyDescriptor.getTitleForType(messageType), optionType, messageType, arrobject, OK_OPTION);
        }

        public Confirmation(Object message, String title, int optionType, int messageType) {
            Object[] arrobject;
            if (optionType == -1) {
                Object[] arrobject2 = new Object[1];
                arrobject = arrobject2;
                arrobject2[0] = OK_OPTION;
            } else {
                arrobject = null;
            }
            super(message, title, optionType, messageType, arrobject, OK_OPTION);
        }
    }

    public static class Message
    extends NotifyDescriptor {
        public Message(Object message) {
            this(message, 1);
        }

        public Message(Object message, int messageType) {
            super(message, NotifyDescriptor.getTitleForType(messageType), -1, messageType, new Object[]{OK_OPTION}, OK_OPTION);
        }
    }

}

