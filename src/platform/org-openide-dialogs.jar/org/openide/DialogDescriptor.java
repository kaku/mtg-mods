/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.HelpCtx$Provider
 *  org.openide.util.NbBundle
 */
package org.openide;

import java.awt.event.ActionListener;
import org.openide.NotifyDescriptor;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

public class DialogDescriptor
extends NotifyDescriptor
implements HelpCtx.Provider {
    public static final String PROP_OPTIONS_ALIGN = "optionsAlign";
    public static final String PROP_MODAL = "modal";
    public static final String PROP_LEAF = "leaf";
    public static final String PROP_HELP_CTX = "helpCtx";
    public static final String PROP_BUTTON_LISTENER = "buttonListener";
    public static final String PROP_CLOSING_OPTIONS = "closingOptions";
    public static final int BOTTOM_ALIGN = 0;
    public static final int RIGHT_ALIGN = 1;
    public static final int DEFAULT_ALIGN = 0;
    private static final Object[] DEFAULT_CLOSING_OPTIONS = new Object[]{YES_OPTION, NO_OPTION, CANCEL_OPTION, OK_OPTION};
    private boolean leaf = false;
    private boolean modal;
    private int optionsAlign;
    private HelpCtx helpCtx;
    private ActionListener buttonListener;
    private Object[] closingOptions = DEFAULT_CLOSING_OPTIONS;

    public DialogDescriptor(Object innerPane, String title) {
        this(innerPane, title, true, 2, OK_OPTION, 0, null, null);
    }

    public DialogDescriptor(Object innerPane, String title, boolean isModal, ActionListener bl) {
        this(innerPane, title, isModal, 2, OK_OPTION, 0, null, bl);
    }

    public DialogDescriptor(Object innerPane, String title, boolean isModal, int optionType, Object initialValue, ActionListener bl) {
        this(innerPane, title, isModal, optionType, initialValue, 0, null, bl);
    }

    public DialogDescriptor(Object innerPane, String title, boolean modal, Object[] options, Object initialValue, int optionsAlign, HelpCtx helpCtx, ActionListener bl) {
        super(innerPane, title, -1, -1, options, initialValue);
        this.modal = modal;
        this.optionsAlign = optionsAlign;
        this.helpCtx = helpCtx == null ? HelpCtx.DEFAULT_HELP : helpCtx;
        this.buttonListener = bl;
        if (bl == null) {
            this.setClosingOptions(options);
        }
    }

    public DialogDescriptor(Object innerPane, String title, boolean modal, Object[] options, Object initialValue, int optionsAlign, HelpCtx helpCtx, ActionListener bl, boolean leaf) {
        super(innerPane, title, -1, -1, options, initialValue);
        this.modal = modal;
        this.optionsAlign = optionsAlign;
        this.helpCtx = helpCtx == null ? HelpCtx.DEFAULT_HELP : helpCtx;
        this.buttonListener = bl;
        this.leaf = leaf;
        if (bl == null) {
            this.setClosingOptions(options);
        }
    }

    public DialogDescriptor(Object innerPane, String title, boolean isModal, int optionType, Object initialValue, int optionsAlign, HelpCtx helpCtx, ActionListener bl) {
        super(innerPane, title, optionType, -1, null, initialValue);
        this.modal = isModal;
        this.optionsAlign = optionsAlign;
        this.helpCtx = helpCtx == null ? HelpCtx.DEFAULT_HELP : helpCtx;
        this.buttonListener = bl;
        if (bl == null) {
            this.setClosingOptions(null);
        }
    }

    public int getOptionsAlign() {
        this.getterCalled();
        return this.optionsAlign;
    }

    public void setOptionsAlign(int optionsAlign) {
        if (optionsAlign != 0 && optionsAlign != 1) {
            throw new IllegalArgumentException(NbBundle.getBundle(DialogDescriptor.class).getString("EXC_OptionsAlign"));
        }
        if (this.optionsAlign == optionsAlign) {
            return;
        }
        int oldValue = this.optionsAlign;
        this.optionsAlign = optionsAlign;
        this.firePropertyChange("optionsAlign", new Integer(oldValue), new Integer(optionsAlign));
    }

    public boolean isModal() {
        this.getterCalled();
        return this.modal;
    }

    public void setModal(boolean modal) {
        if (this.modal == modal) {
            return;
        }
        boolean oldModal = this.modal;
        this.modal = modal;
        this.firePropertyChange("modal", oldModal, modal);
    }

    public boolean isLeaf() {
        this.getterCalled();
        return this.leaf;
    }

    public void setLeaf(boolean leaf) {
        if (this.leaf == leaf) {
            return;
        }
        boolean oldLeaf = this.leaf;
        this.leaf = leaf;
        this.firePropertyChange("leaf", oldLeaf, leaf);
    }

    public void setClosingOptions(Object[] arr) {
        Object[] old = this.closingOptions;
        this.closingOptions = arr;
        this.firePropertyChange("closingOptions", old, arr);
    }

    public Object[] getClosingOptions() {
        this.getterCalled();
        return this.closingOptions;
    }

    public HelpCtx getHelpCtx() {
        this.getterCalled();
        return this.helpCtx;
    }

    public void setHelpCtx(HelpCtx helpCtx) {
        if (this.helpCtx != null && this.helpCtx.equals((Object)helpCtx)) {
            return;
        }
        HelpCtx oldHelpCtx = this.helpCtx;
        this.helpCtx = helpCtx;
        this.firePropertyChange("helpCtx", (Object)oldHelpCtx, (Object)helpCtx);
    }

    public ActionListener getButtonListener() {
        this.getterCalled();
        return this.buttonListener;
    }

    public void setButtonListener(ActionListener l) {
        if (this.buttonListener == l) {
            return;
        }
        ActionListener oldButtonListener = this.buttonListener;
        this.buttonListener = l;
        this.firePropertyChange("buttonListener", oldButtonListener, l);
    }
}

