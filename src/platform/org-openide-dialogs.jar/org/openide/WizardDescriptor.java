/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.awt.Mnemonics
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 */
package org.openide;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FocusTraversalPolicy;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.LayoutManager;
import java.awt.MediaTracker;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.ImageObserver;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.EditorKit;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotificationLineSupport;
import org.openide.NotifyDescriptor;
import org.openide.WizardValidationException;
import org.openide.awt.HtmlBrowser;
import org.openide.awt.Mnemonics;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;

public class WizardDescriptor
extends DialogDescriptor {
    public static final Object NEXT_OPTION = new String("NEXT_OPTION");
    public static final Object FINISH_OPTION = OK_OPTION;
    public static final Object PREVIOUS_OPTION = new String("PREVIOUS_OPTION");
    private static final ActionListener CLOSE_PREVENTER = new ActionListener(){

        @Override
        public void actionPerformed(ActionEvent evt) {
        }

        public String toString() {
            return "CLOSE_PREVENTER";
        }
    };
    public static final String PROP_AUTO_WIZARD_STYLE = "WizardPanel_autoWizardStyle";
    public static final String PROP_HELP_DISPLAYED = "WizardPanel_helpDisplayed";
    public static final String PROP_CONTENT_DISPLAYED = "WizardPanel_contentDisplayed";
    public static final String PROP_CONTENT_NUMBERED = "WizardPanel_contentNumbered";
    public static final String PROP_CONTENT_SELECTED_INDEX = "WizardPanel_contentSelectedIndex";
    public static final String PROP_CONTENT_DATA = "WizardPanel_contentData";
    public static final String CLIENT_PROP_DESCRIPTION = "WizardPanel_contentDescription";
    public static final String CLIENT_PROP_IMAGE = "WizardPanel_icon";
    public static final String CLIENT_PROP_BG_COLOR = "WizardPanel_bgColor";
    public static final String PROP_CONTENT_BACK_COLOR = "WizardPanel_contentBackColor";
    public static final String PROP_CONTENT_FOREGROUND_COLOR = "WizardPanel_contentForegroundColor";
    public static final String PROP_IMAGE = "WizardPanel_image";
    public static final String PROP_IMAGE_ALIGNMENT = "WizardPanel_imageAlignment";
    public static final String PROP_LEFT_DIMENSION = "WizardPanel_leftDimension";
    public static final String PROP_HELP_URL = "WizardPanel_helpURL";
    public static final String PROP_ERROR_MESSAGE = "WizardPanel_errorMessage";
    public static final String PROP_WARNING_MESSAGE = "WizardPanel_warningMessage";
    public static final String PROP_INFO_MESSAGE = "WizardPanel_infoMessage";
    private static Logger err = Logger.getLogger(WizardDescriptor.class.getName());
    private final JButton nextButton = new JButton();
    private final JButton finishButton = new JButton();
    private final JButton cancelButton = new JButton();
    private final JButton previousButton = new JButton();
    private FinishAction finishOption;
    private Set newObjects = Collections.EMPTY_SET;
    private Component waitingComponent;
    private boolean changeStateInProgress = false;
    private boolean addedWindowListener;
    private boolean currentPanelWasChangedWhileStoreSettings = false;
    private final AtomicBoolean initialized = new AtomicBoolean(true);
    private boolean autoWizardStyle = false;
    private boolean init = false;
    private WizardPanel wizardPanel;
    private static boolean showWizardPanel = false;
    private Image image;
    private String[] contentData = new String[0];
    private String[] contentDescription = new String[0];
    private Image icon;
    private int contentSelectedIndex = -1;
    private Color contentBackColor;
    private Color contentForegroundColor;
    private URL helpURL;
    private PropL propListener;
    private String imageAlignment = "North";
    private SettingsAndIterator<?> data;
    private ChangeListener weakChangeListener;
    private PropertyChangeListener weakPropertyChangeListener;
    private ActionListener weakNextButtonListener;
    private ActionListener weakPreviousButtonListener;
    private ActionListener weakFinishButtonListener;
    private ActionListener weakCancelButtonListener;
    private Listener baseListener;
    private MessageFormat titleFormat;
    private Map<String, Object> properties;
    ResourceBundle bundle = NbBundle.getBundle(WizardDescriptor.class);
    static final RequestProcessor ASYNCHRONOUS_JOBS_RP = new RequestProcessor("wizard-descriptor-asynchronous-jobs", 1, true);
    private RequestProcessor.Task backgroundValidationTask;
    private boolean validationRuns;
    private ProgressHandle handle;
    private static final String PROGRESS_BAR_DISPLAY_NAME = NbBundle.getMessage(WizardDescriptor.class, (String)"CTL_InstantiateProgress_Title");
    private ActionListener escapeActionListener;
    private boolean isWizardWideHelpSet = false;
    private static final Set<String> logged = new HashSet<String>();

    public <Data> WizardDescriptor(Panel<Data>[] wizardPanels, Data settings) {
        this(new SettingsAndIterator<Data>(new ArrayIterator<Data>(wizardPanels), settings));
    }

    public WizardDescriptor(Panel<WizardDescriptor>[] wizardPanels) {
        this(SettingsAndIterator.create(new ArrayIterator<WizardDescriptor>(wizardPanels)));
    }

    public <Data> WizardDescriptor(Iterator<Data> panels, Data settings) {
        this(new SettingsAndIterator<Data>(panels, settings));
    }

    protected WizardDescriptor() {
        this(SettingsAndIterator.empty());
    }

    private <Data> WizardDescriptor(SettingsAndIterator<Data> data) {
        super("", "", true, -1, null, CLOSE_PREVENTER);
        ResourceBundle b = NbBundle.getBundle((String)"org.openide.Bundle");
        Mnemonics.setLocalizedText((AbstractButton)this.nextButton, (String)b.getString("CTL_NEXT"));
        Mnemonics.setLocalizedText((AbstractButton)this.previousButton, (String)b.getString("CTL_PREVIOUS"));
        Mnemonics.setLocalizedText((AbstractButton)this.finishButton, (String)b.getString("CTL_FINISH"));
        this.finishButton.getAccessibleContext().setAccessibleDescription(b.getString("ACSD_FINISH"));
        Mnemonics.setLocalizedText((AbstractButton)this.cancelButton, (String)b.getString("CTL_CANCEL"));
        this.cancelButton.getAccessibleContext().setAccessibleDescription(b.getString("ACSD_CANCEL"));
        this.finishButton.setDefaultCapable(true);
        this.nextButton.setDefaultCapable(true);
        this.previousButton.setDefaultCapable(true);
        this.previousButton.putClientProperty("defaultButton", Boolean.FALSE);
        this.cancelButton.setDefaultCapable(true);
        this.cancelButton.putClientProperty("defaultButton", Boolean.FALSE);
        this.data = data;
        this.baseListener = new Listener();
        this.weakNextButtonListener = (ActionListener)WeakListeners.create(ActionListener.class, (EventListener)this.baseListener, (Object)this.nextButton);
        this.weakPreviousButtonListener = (ActionListener)WeakListeners.create(ActionListener.class, (EventListener)this.baseListener, (Object)this.previousButton);
        this.weakFinishButtonListener = (ActionListener)WeakListeners.create(ActionListener.class, (EventListener)this.baseListener, (Object)this.finishButton);
        this.weakCancelButtonListener = (ActionListener)WeakListeners.create(ActionListener.class, (EventListener)this.baseListener, (Object)this.cancelButton);
        this.nextButton.addActionListener(this.weakNextButtonListener);
        this.previousButton.addActionListener(this.weakPreviousButtonListener);
        this.finishButton.addActionListener(this.weakFinishButtonListener);
        this.cancelButton.addActionListener(this.weakCancelButtonListener);
        this.finishOption = new FinishAction();
        super.setOptions(new Object[]{this.previousButton, this.nextButton, this.finishButton, this.cancelButton});
        super.setClosingOptions(new Object[]{this.finishOption, this.cancelButton});
        this.createNotificationLineSupport();
        this.weakChangeListener = WeakListeners.change((ChangeListener)this.baseListener, data.getIterator(this));
        data.getIterator(this).addChangeListener(this.weakChangeListener);
        this.callInitialize();
    }

    public WizardDescriptor(Iterator<WizardDescriptor> panels) {
        this(SettingsAndIterator.create(panels));
    }

    @Override
    protected void initialize() {
        super.initialize();
        this._updateState();
        this.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if ("valid".equals(evt.getPropertyName())) {
                    if (!WizardDescriptor.this.isValid()) {
                        WizardDescriptor.this.nextButton.setEnabled(false);
                        WizardDescriptor.this.finishButton.setEnabled(false);
                    } else {
                        WizardDescriptor.this._updateState();
                    }
                }
            }
        });
    }

    @Deprecated
    public final synchronized void setPanels(Iterator panels) {
        if (this.data.getIterator(this) != null) {
            this.data.getIterator(this).removeChangeListener(this.weakChangeListener);
        }
        this.data = this.data.clone(panels);
        this.weakChangeListener = WeakListeners.change((ChangeListener)this.baseListener, this.data.getIterator(this));
        this.data.getIterator(this).addChangeListener(this.weakChangeListener);
        this.init = false;
        this.initialized.set(true);
        this._updateState();
    }

    public final synchronized <Data> void setPanelsAndSettings(Iterator<Data> panels, Data settings) {
        if (this.data.getIterator(this) != null) {
            this.data.getIterator(this).removeChangeListener(this.weakChangeListener);
        }
        this.data = new SettingsAndIterator<Data>(panels, settings);
        this.weakChangeListener = WeakListeners.change((ChangeListener)this.baseListener, this.data.getIterator(this));
        this.data.getIterator(this).addChangeListener(this.weakChangeListener);
        this.init = false;
        this.initialized.set(true);
        this._updateState();
    }

    public void setShowTitlePanel(boolean b) {
        showWizardPanel = b;
    }

    @Override
    public void setOptions(Object[] options) {
        super.setOptions(this.convertOptions(options));
    }

    @Override
    public void setAdditionalOptions(Object[] options) {
        super.setAdditionalOptions(this.convertOptions(options));
    }

    @Override
    public void setClosingOptions(Object[] options) {
        super.setClosingOptions(this.convertOptions(options));
    }

    private Object[] convertOptions(Object[] options) {
        Object[] clonedOptions = (Object[])options.clone();
        for (int i = clonedOptions.length - 1; i >= 0; --i) {
            if (clonedOptions[i] == NEXT_OPTION) {
                clonedOptions[i] = this.nextButton;
            }
            if (clonedOptions[i] == PREVIOUS_OPTION) {
                clonedOptions[i] = this.previousButton;
            }
            if (clonedOptions[i] == FINISH_OPTION) {
                clonedOptions[i] = this.finishButton;
            }
            if (clonedOptions[i] != CANCEL_OPTION) continue;
            clonedOptions[i] = this.cancelButton;
        }
        return clonedOptions;
    }

    @Override
    public Object getValue() {
        return this.backConvertOption(super.getValue());
    }

    private Object backConvertOption(Object op) {
        if (op == this.nextButton) {
            return NEXT_OPTION;
        }
        if (op == this.previousButton) {
            return PREVIOUS_OPTION;
        }
        if (op == this.finishButton) {
            return FINISH_OPTION;
        }
        if (op == this.cancelButton) {
            return CANCEL_OPTION;
        }
        return op;
    }

    public void setTitleFormat(MessageFormat format) {
        this.titleFormat = format;
        if (this.init) {
            this._updateState();
        }
    }

    public synchronized MessageFormat getTitleFormat() {
        if (this.titleFormat == null) {
            this.titleFormat = new MessageFormat(NbBundle.getMessage(WizardDescriptor.class, (String)"CTL_WizardName"));
        }
        return this.titleFormat;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void putProperty(final String name, final Object value) {
        Object oldValue;
        WizardDescriptor wizardDescriptor = this;
        synchronized (wizardDescriptor) {
            if (this.properties == null) {
                this.properties = new HashMap<String, Object>(7);
            }
            oldValue = this.properties.get(name);
            this.properties.put(name, value);
        }
        this.firePropertyChange(name, oldValue, value);
        if (this.propListener != null) {
            Mutex.EVENT.readAccess(new Runnable(){

                @Override
                public void run() {
                    WizardDescriptor.this.propListener.propertyChange(new PropertyChangeEvent(this, name, null, null));
                }
            });
        }
        if ("WizardPanel_errorMessage".equals(name)) {
            if (this.init && OK_OPTION.equals(this.getValue())) {
                return;
            }
            if (this.wizardPanel != null) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        if (WizardDescriptor.this.nextButton.isEnabled() || WizardDescriptor.this.finishButton.isEnabled()) {
                            WizardDescriptor.this.wizardPanel.setMessage(2, value == null ? "" : value);
                        } else {
                            WizardDescriptor.this.wizardPanel.setMessage(1, value == null ? "" : value);
                        }
                    }
                });
            }
        }
        if (("WizardPanel_warningMessage".equals(name) || "WizardPanel_infoMessage".equals(name)) && this.wizardPanel != null) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    if ("WizardPanel_warningMessage".equals(name)) {
                        WizardDescriptor.this.wizardPanel.setMessage(2, value == null ? "" : value);
                    } else {
                        WizardDescriptor.this.wizardPanel.setMessage(3, value == null ? "" : value);
                    }
                }
            });
        }
    }

    public synchronized Object getProperty(String name) {
        return this.properties == null ? null : this.properties.get(name);
    }

    public synchronized Map<String, Object> getProperties() {
        return this.properties == null ? Collections.emptyMap() : new HashMap<String, Object>(this.properties);
    }

    @Override
    public void setHelpCtx(HelpCtx helpCtx) {
        this.isWizardWideHelpSet = null != helpCtx && !HelpCtx.DEFAULT_HELP.equals((Object)helpCtx);
        this.doSetHelpCtx(helpCtx);
    }

    private void doSetHelpCtx(HelpCtx helpCtx) {
        if (this.wizardPanel != null && helpCtx != null) {
            HelpCtx.setHelpIDString((JComponent)this.wizardPanel, (String)helpCtx.getHelpID());
        }
        super.setHelpCtx(helpCtx);
    }

    public Set getInstantiatedObjects() {
        if (!FINISH_OPTION.equals(this.getValue())) {
            throw new IllegalStateException();
        }
        return this.newObjects;
    }

    @Override
    void clearMessages() {
        this.putProperty("WizardPanel_errorMessage", null);
    }

    @Override
    void setErrorMessage(String msg) {
        this.putProperty("WizardPanel_errorMessage", msg);
    }

    @Override
    void setInformationMessage(String msg) {
        this.putProperty("WizardPanel_infoMessage", msg);
    }

    @Override
    void setWarningMessage(String msg) {
        this.putProperty("WizardPanel_warningMessage", msg);
    }

    private synchronized void _updateState() {
        if (SwingUtilities.isEventDispatchThread()) {
            this.updateState();
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    WizardDescriptor.this.updateState();
                }
            });
        }
    }

    protected synchronized void updateState() {
        assert (SwingUtilities.isEventDispatchThread());
        this.updateStateOpen(this.data);
    }

    private static void checkComponent(Panel<?> pnl) {
        String name = pnl.getClass().getName();
        if (pnl instanceof Component && logged.add(name)) {
            Logger.getLogger(WizardDescriptor.class.getName()).warning(name + " is both a " + "WizardDescriptor.Panel and a Component.  This is illegal " + "because Component.isValid() conflicts with " + "Panel.isValid().  See umbrella issue 154624 and " + "issues 150223, 134601, 99680 and " + "many others for why this is a Bad Thing.");
        }
    }

    private <A> void updateStateOpen(SettingsAndIterator<A> data) {
        Component fo;
        String panelName;
        if (!this.initialized.get()) {
            return;
        }
        Panel<A> p = data.getIterator(this).current();
        WizardDescriptor.checkComponent(p);
        if (data.current != p) {
            if (data.current != null) {
                data.current.removeChangeListener(this.weakChangeListener);
                data.current.storeSettings(data.getSettings(this));
            }
            p = data.getIterator(this).current();
            WizardDescriptor.checkComponent(p);
            data.getIterator(this).removeChangeListener(this.weakChangeListener);
            this.weakChangeListener = WeakListeners.change((ChangeListener)this.baseListener, p);
            data.getIterator(this).addChangeListener(this.weakChangeListener);
            p.addChangeListener(this.weakChangeListener);
            data.current = p;
            p.readSettings(data.getSettings(this));
        }
        boolean next = data.getIterator(this).hasNext();
        boolean prev = data.getIterator(this).hasPrevious();
        boolean valid = p.isValid() && !this.validationRuns;
        this.nextButton.setEnabled(next && valid);
        this.previousButton.setEnabled(prev);
        this.cancelButton.setEnabled(true);
        if (data.current instanceof FinishablePanel) {
            if (((FinishablePanel)data.current).isFinishPanel()) {
                this.finishButton.setEnabled(valid);
            } else {
                this.finishButton.setEnabled(valid && !next);
            }
        } else {
            this.finishButton.setEnabled(valid && (!next || data.current instanceof FinishPanel));
        }
        if (!this.isWizardWideHelpSet) {
            this.doSetHelpCtx(p.getHelp());
        }
        assert (SwingUtilities.isEventDispatchThread());
        Component c = p.getComponent();
        if (c == null || c instanceof Window) {
            throw new IllegalStateException("Wizard panel " + p + " gave a strange component " + c);
        }
        if (!this.init) {
            if (c instanceof JComponent) {
                this.autoWizardStyle = this.getBooleanProperty((JComponent)c, "WizardPanel_autoWizardStyle");
                if (this.autoWizardStyle) {
                    this.wizardPanel = new WizardPanel(this.getBooleanProperty((JComponent)c, "WizardPanel_contentDisplayed"), this.getBooleanProperty((JComponent)c, "WizardPanel_helpDisplayed"), this.getBooleanProperty((JComponent)c, "WizardPanel_contentNumbered"), this.getLeftDimension((JComponent)c));
                    this.initBundleProperties();
                }
            }
            if (this.propListener == null) {
                this.propListener = new PropL();
            }
            this.init = true;
        }
        if (this.wizardPanel != null) {
            Component oldComp = this.wizardPanel.getRightComponent();
            if (oldComp != null) {
                oldComp.removePropertyChangeListener(this.weakPropertyChangeListener);
            }
            if (c instanceof JComponent) {
                this.setPanelProperties((JComponent)c);
                this.wizardPanel.setContent(this.contentData);
                this.wizardPanel.setSelectedIndex(this.contentSelectedIndex);
                this.wizardPanel.setContentBackColor(this.contentBackColor);
                this.wizardPanel.setContentForegroundColor(this.contentForegroundColor);
                this.wizardPanel.setImage(this.image);
                this.wizardPanel.setImageAlignment(this.imageAlignment);
                this.wizardPanel.setHelpURL(this.helpURL);
                this.updateButtonAccessibleDescription();
                this.weakPropertyChangeListener = WeakListeners.propertyChange((PropertyChangeListener)this.propListener, (Object)c);
                c.addPropertyChangeListener(this.weakPropertyChangeListener);
            }
            if (this.wizardPanel.getRightComponent() != c) {
                this.wizardPanel.setRightComponent(c);
                if (this.wizardPanel != this.getMessage()) {
                    this.setMessage(this.wizardPanel);
                } else {
                    this.firePropertyChange("message", null, this.wizardPanel);
                }
            }
        } else if (c != this.getMessage()) {
            this.setMessage(c);
        }
        if (!this.addedWindowListener && this.getMessage() instanceof Component) {
            final Component comp = (Component)this.getMessage();
            comp.addHierarchyListener(new HierarchyListener(){

                @Override
                public void hierarchyChanged(HierarchyEvent e) {
                    if ((e.getChangeFlags() & 2) != 0) {
                        this.check();
                    }
                }

                private void check() {
                    final Window w = SwingUtilities.getWindowAncestor(comp);
                    if (!WizardDescriptor.this.addedWindowListener && w != null) {
                        WizardDescriptor.this.addedWindowListener = true;
                        w.addWindowListener(new WindowAdapter(){

                            @Override
                            public void windowClosing(WindowEvent e) {
                                if (!WizardDescriptor.this.changeStateInProgress) {
                                    if (WizardDescriptor.this.getValue() == null || WizardDescriptor.NEXT_OPTION.equals(WizardDescriptor.this.getValue())) {
                                        WizardDescriptor.this.setValue(NotifyDescriptor.CLOSED_OPTION);
                                    }
                                    w.setVisible(false);
                                    w.dispose();
                                }
                            }
                        });
                    }
                }

            });
        }
        if ((panelName = c.getName()) == null) {
            panelName = "";
        }
        Object[] args = new Object[]{panelName, data.getIterator(this).name()};
        MessageFormat mf = this.getTitleFormat();
        if (this.autoWizardStyle) {
            this.wizardPanel.setPanelName(mf.format(args));
        } else {
            this.setTitle(mf.format(args));
        }
        if (this.autoWizardStyle && c instanceof JComponent) {
            Object property = ((JComponent)c).getClientProperty("WizardPanel_contentDescription");
            String headingText = "";
            if (property != null) {
                headingText = (String)property;
            }
            headingText = panelName.toUpperCase() + ": " + headingText;
            this.wizardPanel.setHeadingText(headingText);
            property = ((JComponent)c).getClientProperty("WizardPanel_icon");
            if (property != null) {
                this.wizardPanel.addHeadingIcon((Image)property);
            }
            if ((property = ((JComponent)c).getClientProperty("WizardPanel_bgColor")) != null) {
                this.wizardPanel.setPanelBackground((Color)property);
            }
        }
        if ((fo = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner()) != null && !fo.isEnabled() && this.wizardPanel != null) {
            this.wizardPanel.requestFocus();
        }
    }

    boolean isForwardEnabled() {
        return this.data.getIterator(this).current().isValid() && !this.validationRuns;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void updateStateWithFeedback() {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    WizardDescriptor.this.updateStateWithFeedback();
                }
            });
            return;
        }
        try {
            this.showWaitCursor();
            this.updateState();
        }
        finally {
            this.showNormalCursor();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void goToNextStep(Dimension previousSize) {
        try {
            Window parentWindow;
            this.showWaitCursor();
            boolean alreadyUpdated = false;
            Font controlFont = (Font)UIManager.getDefaults().get("controlFont");
            Integer defaultSize = (Integer)UIManager.get("nbDefaultFontSize");
            if (defaultSize == null) {
                defaultSize = new Integer(11);
            }
            if (controlFont != null && controlFont.getSize() > defaultSize && (parentWindow = SwingUtilities.getWindowAncestor((Component)this.getMessage())) != null) {
                this._updateState();
                alreadyUpdated = true;
                this.resizeWizard(parentWindow, previousSize);
            }
            if (!alreadyUpdated) {
                this._updateState();
            }
            if (this.wizardPanel != null) {
                this.wizardPanel.requestFocus();
            }
        }
        finally {
            this.showNormalCursor();
        }
    }

    private void resizeWizard(Window parentWindow, Dimension prevSize) {
        assert (SwingUtilities.isEventDispatchThread());
        Dimension curSize = this.data.getIterator(this).current().getComponent().getPreferredSize();
        if (curSize.width > prevSize.width || curSize.height > prevSize.height) {
            Rectangle newBounds;
            Rectangle origBounds = parentWindow.getBounds();
            int newWidth = origBounds.width;
            int newHeight = origBounds.height;
            Rectangle screenBounds = Utilities.getUsableScreenBounds();
            if (origBounds.x + newWidth > screenBounds.width || origBounds.y + newHeight > screenBounds.height) {
                newWidth = Math.min(screenBounds.width, newWidth);
                newHeight = Math.min(screenBounds.height, newHeight);
                newBounds = Utilities.findCenterBounds((Dimension)new Dimension(newWidth, newHeight));
            } else {
                newBounds = new Rectangle(origBounds.x, origBounds.y, newWidth, newHeight);
            }
            parentWindow.setBounds(newBounds);
            parentWindow.invalidate();
            parentWindow.validate();
            parentWindow.repaint();
        }
    }

    private void showWaitCursor() {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    WizardDescriptor.this.showWaitCursor();
                }
            });
            return;
        }
        if (this.wizardPanel == null || this.wizardPanel.getRootPane() == null) {
            return;
        }
        if (this.wizardPanel != null) {
            KeyStroke ks = KeyStroke.getKeyStroke(27, 0);
            this.escapeActionListener = this.wizardPanel.getRootPane().getActionForKeyStroke(ks);
            this.wizardPanel.getRootPane().unregisterKeyboardAction(ks);
        }
        this.waitingComponent = this.wizardPanel.getRootPane().getContentPane();
        this.waitingComponent.setCursor(Cursor.getPredefinedCursor(3));
        this.changeStateInProgress = true;
    }

    private void showNormalCursor() {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    WizardDescriptor.this.showNormalCursor();
                }
            });
            return;
        }
        if (this.waitingComponent == null) {
            return;
        }
        Window parentWindow = SwingUtilities.getWindowAncestor((Component)this.getMessage());
        if (parentWindow != null) {
            parentWindow.setEnabled(true);
        }
        if (this.wizardPanel != null) {
            if (this.escapeActionListener != null && this.wizardPanel.getRootPane() != null) {
                this.wizardPanel.getRootPane().registerKeyboardAction(this.escapeActionListener, "Escape", KeyStroke.getKeyStroke(27, 0), 1);
            }
            this.wizardPanel.setProgressComponent(null, null);
        }
        this.waitingComponent.setCursor(null);
        this.waitingComponent = null;
        this.changeStateInProgress = false;
    }

    private boolean getBooleanProperty(JComponent c, String s) {
        Object property = this.getProperty(s);
        if (property instanceof Boolean) {
            return (Boolean)property;
        }
        property = c.getClientProperty(s);
        if (property instanceof Boolean) {
            return (Boolean)property;
        }
        return false;
    }

    private Dimension getLeftDimension(JComponent c) {
        Object property = c.getClientProperty("WizardPanel_leftDimension");
        Dimension leftDimension = property instanceof Dimension ? (Dimension)property : new Dimension(198, 233);
        return leftDimension;
    }

    private void setPanelProperties(JComponent c) {
        Object property = this.getProperty("WizardPanel_contentSelectedIndex");
        if (property instanceof Integer) {
            this.contentSelectedIndex = (Integer)property;
        } else {
            property = c.getClientProperty("WizardPanel_contentSelectedIndex");
            if (property instanceof Integer) {
                this.contentSelectedIndex = (Integer)property;
            }
        }
        property = this.getProperty("WizardPanel_contentData");
        if (property instanceof String[]) {
            this.contentData = (String[])property;
        } else {
            property = c.getClientProperty("WizardPanel_contentData");
            if (property instanceof String[]) {
                this.contentData = (String[])property;
            }
        }
        property = this.getProperty("WizardPanel_image");
        if (property instanceof Image) {
            this.image = (Image)property;
        } else if ((this.properties == null || !this.properties.containsKey("WizardPanel_image")) && (property = c.getClientProperty("WizardPanel_image")) instanceof Image) {
            this.image = (Image)property;
        }
        property = this.getProperty("WizardPanel_imageAlignment");
        if (property instanceof String) {
            this.imageAlignment = (String)property;
        } else {
            property = c.getClientProperty("WizardPanel_imageAlignment");
            if (property instanceof String) {
                this.imageAlignment = (String)property;
            }
        }
        property = this.getProperty("WizardPanel_contentBackColor");
        if (property instanceof Color) {
            this.contentBackColor = (Color)property;
        } else {
            property = c.getClientProperty("WizardPanel_contentBackColor");
            if (property instanceof Color) {
                this.contentBackColor = (Color)property;
            }
        }
        property = this.getProperty("WizardPanel_contentForegroundColor");
        if (property instanceof Color) {
            this.contentForegroundColor = (Color)property;
        } else {
            property = c.getClientProperty("WizardPanel_contentForegroundColor");
            if (property instanceof Color) {
                this.contentForegroundColor = (Color)property;
            }
        }
        property = c.getClientProperty("WizardPanel_helpURL");
        if (property instanceof URL) {
            this.helpURL = (URL)property;
        } else if (property == null) {
            this.helpURL = null;
        }
    }

    private void initBundleProperties() {
        Color c;
        this.contentBackColor = new Color(this.getIntFromBundle("INT_WizardBackRed"), this.getIntFromBundle("INT_WizardBackGreen"), this.getIntFromBundle("INT_WizardBackBlue"));
        if (Color.white.equals(this.contentBackColor) && null != (c = UIManager.getColor("Tree.background"))) {
            this.contentBackColor = new Color(c.getRGB());
        }
        this.contentForegroundColor = new Color(this.getIntFromBundle("INT_WizardForegroundRed"), this.getIntFromBundle("INT_WizardForegroundGreen"), this.getIntFromBundle("INT_WizardForegroundBlue"));
        if (Color.black.equals(this.contentForegroundColor) && null != (c = UIManager.getColor("Tree.foreground"))) {
            this.contentForegroundColor = new Color(c.getRGB());
        }
        this.imageAlignment = this.bundle.getString("STRING_WizardImageAlignment");
    }

    @Override
    public void setValue(Object value) {
        this.setValueOpen(value, this.data);
    }

    private <A> void setValueOpen(Object value, SettingsAndIterator<A> data) {
        Object convertedValue = this.backConvertOption(value);
        Object oldValue = this.getValue();
        this.setValueWithoutPCH(convertedValue);
        if (CLOSED_OPTION.equals(convertedValue)) {
            try {
                this.resetWizard();
            }
            catch (RuntimeException x) {
                err.log(Level.INFO, null, x);
            }
        } else if (FINISH_OPTION.equals(convertedValue) || NEXT_OPTION.equals(convertedValue)) {
            return;
        }
        this.firePropertyChange("value", oldValue, convertedValue);
    }

    private void resetWizard() {
        this.resetWizardOpen(this.data);
    }

    private <A> void storeSettingsAndNotify(SettingsAndIterator<A> data) {
        if (data.current != null) {
            data.current.storeSettings(data.getSettings(this));
        }
        this.firePropertyChange("value", null, NEXT_OPTION);
    }

    private <A> void resetWizardOpen(SettingsAndIterator<A> data) {
        if (data.current != null) {
            data.current.storeSettings(data.getSettings(this));
            data.current.removeChangeListener(this.weakChangeListener);
            data.current = null;
            if (this.wizardPanel != null) {
                this.wizardPanel.resetPreferredSize();
            }
        }
        this.callUninitialize();
        data.getIterator(this).removeChangeListener(this.weakChangeListener);
    }

    private int getIntFromBundle(String key) {
        return Integer.parseInt(this.bundle.getString(key));
    }

    private static Image getDefaultImage() {
        return ImageUtilities.loadImage((String)"org/netbeans/modules/dialogs/defaultWizard.gif", (boolean)true);
    }

    private void updateButtonAccessibleDescription() {
        assert (SwingUtilities.isEventDispatchThread());
        String stepName = this.contentData != null && this.contentSelectedIndex > 0 && this.contentSelectedIndex - 1 < this.contentData.length ? this.contentData[this.contentSelectedIndex - 1] : "";
        try {
            this.previousButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(WizardDescriptor.class, (String)"ACSD_PREVIOUS", (Object)new Integer(this.contentSelectedIndex), (Object)stepName));
        }
        catch (IllegalArgumentException iae) {
            err.log(Level.INFO, iae.getLocalizedMessage() + " while setting ACSD_PREVIOUS with params " + stepName + ", " + this.contentSelectedIndex, iae);
        }
        stepName = this.contentData != null && this.contentSelectedIndex < this.contentData.length - 1 && this.contentSelectedIndex + 1 >= 0 ? this.contentData[this.contentSelectedIndex + 1] : "";
        try {
            this.nextButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(WizardDescriptor.class, (String)"ACSD_NEXT", (Object)new Integer(this.contentSelectedIndex + 2), (Object)stepName));
        }
        catch (IllegalArgumentException iae) {
            err.log(Level.INFO, iae.getLocalizedMessage() + " while setting ACSD_NEXT with params " + stepName + ", " + (this.contentSelectedIndex + 2), iae);
        }
    }

    private void lazyValidate(final Panel panel, final Runnable onValidPerformer) {
        Runnable validationPeformer = new Runnable(){

            @Override
            public void run() {
                err.log(Level.FINE, "validationPeformer entry.");
                ValidatingPanel v = (ValidatingPanel)panel;
                try {
                    if (WizardDescriptor.this.currentPanelWasChangedWhileStoreSettings) {
                        err.log(Level.FINE, "validationPeformer interupt because currentPanelWasChangedWhileStoreSettings");
                        WizardDescriptor.this.currentPanelWasChangedWhileStoreSettings = false;
                    } else {
                        v.validate();
                        err.log(Level.FINE, "validation passed successfully.");
                    }
                    WizardDescriptor.this.validationRuns = false;
                    if (SwingUtilities.isEventDispatchThread()) {
                        err.log(Level.FINE, "Runs onValidPerformer directly in EDT.");
                        onValidPerformer.run();
                    } else {
                        err.log(Level.FINE, "invokeLater onValidPerformer.");
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                if (WizardDescriptor.this.initialized.get()) {
                                    err.log(Level.FINE, "Runs onValidPerformer from invokeLater.");
                                    if (panel instanceof ExtendedAsynchronousValidatingPanel) {
                                        ((ExtendedAsynchronousValidatingPanel)panel).finishValidation();
                                    }
                                    onValidPerformer.run();
                                }
                            }
                        });
                    }
                }
                catch (WizardValidationException wve) {
                    WizardDescriptor.this.validationRuns = false;
                    err.log(Level.FINE, "validation failed", wve);
                    if (WizardDescriptor.FINISH_OPTION.equals(WizardDescriptor.this.getValue())) {
                        WizardDescriptor.this.setValue(WizardDescriptor.this.getDefaultValue());
                    }
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            if (panel instanceof ExtendedAsynchronousValidatingPanel) {
                                ((ExtendedAsynchronousValidatingPanel)panel).finishValidation();
                            }
                            WizardDescriptor.this.onValidationFailed(wve);
                        }
                    });
                }
            }

        };
        if (panel instanceof AsynchronousValidatingPanel) {
            AsynchronousValidatingPanel p = (AsynchronousValidatingPanel)panel;
            this.validationRuns = true;
            p.prepareValidation();
            err.log(Level.FINE, "Do ASYNCHRONOUS_JOBS_RP.post(validationPeformer).");
            this.updateStateWithFeedback();
            this.backgroundValidationTask = ASYNCHRONOUS_JOBS_RP.post(validationPeformer);
        } else if (panel instanceof ValidatingPanel) {
            this.validationRuns = true;
            err.log(Level.FINE, "Runs validationPeformer.");
            validationPeformer.run();
        } else {
            err.log(Level.FINE, "Runs onValidPerformer.");
            onValidPerformer.run();
        }
    }

    private void onValidationFailed(final WizardValidationException wve) {
        assert (SwingUtilities.isEventDispatchThread());
        this._updateState();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                if (WizardDescriptor.this.wizardPanel != null) {
                    WizardDescriptor.this.wizardPanel.setMessage(1, wve.getLocalizedMessage());
                }
            }
        });
        JComponent srcComp = wve.getSource();
        if (srcComp != null && srcComp.isFocusable()) {
            srcComp.requestFocus();
        }
    }

    private void callInitialize() {
        assert (this.data.getIterator(this) != null);
        this.initialized.set(true);
        if (this.data.getIterator(this) instanceof InstantiatingIterator) {
            ((InstantiatingIterator)this.data.getIterator(this)).initialize(this);
        }
        this.newObjects = Collections.EMPTY_SET;
    }

    private void callUninitialize() {
        assert (this.data.getIterator(this) != null);
        this.initialized.set(false);
        if (this.data.getIterator(this) instanceof InstantiatingIterator) {
            ((InstantiatingIterator)this.data.getIterator(this)).uninitialize(this);
        }
    }

    private void callInstantiate() throws IOException {
        this.callInstantiateOpen(this.data);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private <A> void callInstantiateOpen(SettingsAndIterator<A> data) throws IOException {
        block13 : {
            Iterator<A> panels = data.getIterator(this);
            assert (panels != null);
            if (panels instanceof BackgroundInstantiatingIterator) {
                err.fine("is BackgroundInstantiatingIterator");
            } else if (panels instanceof ProgressInstantiatingIterator) {
                err.fine("is ProgressInstantiatingIterator");
                this.handle = ProgressHandleFactory.createHandle((String)PROGRESS_BAR_DISPLAY_NAME);
                final JComponent progressComp = ProgressHandleFactory.createProgressComponent((ProgressHandle)this.handle);
                final JLabel detailComp = ProgressHandleFactory.createDetailLabelComponent((ProgressHandle)this.handle);
                Mutex.EVENT.readAccess(new Runnable(){

                    @Override
                    public void run() {
                        if (WizardDescriptor.this.wizardPanel != null) {
                            WizardDescriptor.this.wizardPanel.setProgressComponent(progressComp, detailComp);
                        }
                    }
                });
                err.log(Level.FINE, "Show progressPanel controlled by iterator later.");
            } else if (panels instanceof AsynchronousInstantiatingIterator) {
                err.fine("is AsynchronousInstantiatingIterator");
                this.handle = ProgressHandleFactory.createHandle((String)PROGRESS_BAR_DISPLAY_NAME);
                final JComponent progressComp = ProgressHandleFactory.createProgressComponent((ProgressHandle)this.handle);
                final JLabel mainLabelComp = ProgressHandleFactory.createMainLabelComponent((ProgressHandle)this.handle);
                Mutex.EVENT.readAccess(new Runnable(){

                    @Override
                    public void run() {
                        if (WizardDescriptor.this.wizardPanel != null) {
                            WizardDescriptor.this.wizardPanel.setProgressComponent(progressComp, mainLabelComp);
                        }
                    }
                });
                this.handle.start();
                err.log(Level.FINE, "Show progressPanel later.");
            }
            panels.current().storeSettings(data.getSettings(this));
            if (panels instanceof InstantiatingIterator) {
                this.showWaitCursor();
                try {
                    assert (!(panels instanceof AsynchronousInstantiatingIterator) || !SwingUtilities.isEventDispatchThread());
                    if (panels instanceof ProgressInstantiatingIterator) {
                        assert (this.handle != null);
                        err.log(Level.FINE, "Calls instantiate(ProgressHandle) on iterator: " + panels.getClass().getName());
                        this.newObjects = ((ProgressInstantiatingIterator)panels).instantiate(this.handle);
                        break block13;
                    }
                    err.log(Level.FINE, "Calls instantiate() on iterator: " + panels.getClass().getName());
                    this.newObjects = ((InstantiatingIterator)panels).instantiate();
                }
                finally {
                    this.showNormalCursor();
                }
            }
        }
    }

    private static Font doDeriveFont(Font original, int style) {
        if (Utilities.isMac()) {
            return new Font(original.getName(), style, original.getSize());
        }
        return original.deriveFont(style);
    }

    public final void doNextClick() {
        assert (SwingUtilities.isEventDispatchThread());
        if (this.nextButton.isEnabled()) {
            this.nextButton.doClick();
        }
    }

    public final void doPreviousClick() {
        assert (SwingUtilities.isEventDispatchThread());
        if (this.previousButton.isEnabled()) {
            this.previousButton.doClick();
        }
    }

    public final void doFinishClick() {
        assert (SwingUtilities.isEventDispatchThread());
        if (this.finishButton.isEnabled()) {
            this.finishButton.doClick();
        }
    }

    public final void doCancelClick() {
        assert (SwingUtilities.isEventDispatchThread());
        if (this.cancelButton.isEnabled()) {
            this.cancelButton.doClick();
        }
    }

    final boolean isNextEnabled() {
        return this.nextButton.isEnabled();
    }

    final boolean isFinishEnabled() {
        return this.finishButton.isEnabled();
    }

    private static final class EmptyPanel
    implements Panel<Void>,
    Iterator<Void> {
        private EmptyPanel() {
        }

        @Override
        public Component getComponent() {
            return new JPanel();
        }

        @Override
        public HelpCtx getHelp() {
            return HelpCtx.DEFAULT_HELP;
        }

        @Override
        public void readSettings(Void settings) {
        }

        @Override
        public void storeSettings(Void settings) {
        }

        @Override
        public boolean isValid() {
            return true;
        }

        @Override
        public void addChangeListener(ChangeListener l) {
        }

        @Override
        public void removeChangeListener(ChangeListener l) {
        }

        @Override
        public Panel<Void> current() {
            return this;
        }

        @Override
        public String name() {
            return "";
        }

        @Override
        public boolean hasNext() {
            return false;
        }

        @Override
        public boolean hasPrevious() {
            return false;
        }

        @Override
        public void nextPanel() {
        }

        @Override
        public void previousPanel() {
        }
    }

    private static final class SettingsAndIterator<Data> {
        private final Iterator<Data> panels;
        private final Data settings;
        private final boolean useThis;
        private Panel<Data> current;

        public SettingsAndIterator(Iterator<Data> iterator, Data settings) {
            this(iterator, settings, false);
        }

        public SettingsAndIterator(Iterator<Data> iterator, Data settings, boolean useThis) {
            this.panels = iterator;
            this.settings = settings;
            this.useThis = useThis;
        }

        public static SettingsAndIterator<WizardDescriptor> create(Iterator<WizardDescriptor> iterator) {
            return new SettingsAndIterator<Object>(iterator, null, true);
        }

        public static SettingsAndIterator<Void> empty() {
            return new SettingsAndIterator<Void>(new EmptyPanel(), null);
        }

        public Iterator<Data> getIterator(WizardDescriptor caller) {
            return this.panels;
        }

        public Data getSettings(WizardDescriptor caller) {
            return (Data)(this.useThis ? caller : this.settings);
        }

        public SettingsAndIterator<Data> clone(Iterator<Data> it) {
            SettingsAndIterator<Data> s = new SettingsAndIterator<Data>(it, this.settings, this.useThis);
            return s;
        }
    }

    private static final class FixedHeightPane
    extends JTextPane {
        private static final int ESTIMATED_HEIGHT = 16;

        public FixedHeightPane() {
            this.setEditable(false);
            this.putClientProperty("JEditorPane.honorDisplayProperties", Boolean.TRUE);
            HTMLEditorKit htmlkit = new HTMLEditorKit();
            StyleSheet css = htmlkit.getStyleSheet();
            if (css.getStyleSheets() == null) {
                StyleSheet css2 = new StyleSheet();
                Font f = new JList().getFont();
                int size = f.getSize();
                try {
                    css2.addRule(new StringBuffer("body { font-size: ").append(size).append("; font-family: ").append(f.getName()).append("; }").toString());
                    css2.addStyleSheet(css);
                    htmlkit.setStyleSheet(css2);
                }
                catch (RuntimeException ex) {
                    Logger.getLogger(WizardDescriptor.class.getName()).log(Level.INFO, "Error while setting up text pane.", ex);
                }
            } else {
                this.setFont(new JLabel().getFont());
            }
            this.setEditorKit(htmlkit);
            this.setOpaque(false);
            if ("Nimbus".equals(UIManager.getLookAndFeel().getID())) {
                this.setBackground(new Color(0, 0, 0, 0));
            }
            this.addHyperlinkListener(new HyperlinkListener(){

                @Override
                public void hyperlinkUpdate(HyperlinkEvent hlevt) {
                    if (HyperlinkEvent.EventType.ACTIVATED == hlevt.getEventType() && hlevt.getURL() != null) {
                        HtmlBrowser.URLDisplayer.getDefault().showURLExternal(hlevt.getURL());
                    }
                }
            });
            this.addMouseListener(new MouseAdapter(){

                @Override
                public void mousePressed(MouseEvent e) {
                    this.showCopyToClipboardPopupMenu(e);
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    this.showCopyToClipboardPopupMenu(e);
                }

                private void showCopyToClipboardPopupMenu(MouseEvent e) {
                    if (e.isPopupTrigger() && null != FixedHeightPane.this.getToolTipText() && !FixedHeightPane.this.getToolTipText().isEmpty()) {
                        JPopupMenu pm = new JPopupMenu();
                        pm.add(new AbstractAction(NbBundle.getMessage(WizardDescriptor.class, (String)"Lbl_CopyToClipboard")){

                            @Override
                            public void actionPerformed(ActionEvent e) {
                                Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
                                c.setContents(new StringSelection(FixedHeightPane.this.getToolTipText()), null);
                            }
                        });
                        pm.show(e.getComponent(), e.getX(), e.getY());
                    }
                }

            });
        }

        @Override
        public Dimension getPreferredSize() {
            Dimension preferredSize = super.getPreferredSize();
            assert (16 == ImageUtilities.loadImage((String)"org/netbeans/modules/dialogs/warning.gif").getHeight(null));
            preferredSize.height = Math.max(16, preferredSize.height);
            return preferredSize;
        }

    }

    private static final class FixedHeightLabel
    extends JLabel {
        private static final int ESTIMATED_HEIGHT = 16;

        @Override
        public Dimension getPreferredSize() {
            Dimension preferredSize = super.getPreferredSize();
            assert (16 == ImageUtilities.loadImage((String)"org/netbeans/modules/dialogs/warning.gif").getHeight(null));
            preferredSize.height = Math.max(16, preferredSize.height);
            return preferredSize;
        }
    }

    static class FinishAction {
        ActionListener listner;

        FinishAction() {
        }

        public void addActionListener(ActionListener ac) {
            this.listner = ac;
        }

        public void removeActionListener(ActionListener ac) {
            this.listner = null;
        }

        public void fireActionPerformed() {
            if (this.listner != null) {
                this.listner.actionPerformed(new ActionEvent(this, 0, ""));
            }
        }
    }

    private static class BoundedHtmlBrowser
    extends HtmlBrowser {
        Dimension dim;

        public BoundedHtmlBrowser(Dimension d) {
            super(false, false);
            this.dim = d;
        }

        public Dimension getPreferredSize() {
            return this.dim;
        }
    }

    private static class HeadingTextAndIcon
    extends JPanel {
        private final JTextArea _headingTextArea = new JTextArea();
        private final JPanel _iconPanel = new JPanel();
        private final JLabel _iconLabel = new JLabel();
        GridBagConstraints gridBagConstraints;

        public HeadingTextAndIcon() {
            this.setLayout(new GridBagLayout());
            Color textareaBackground = UIManager.getLookAndFeelDefaults().getColor("wizard-banner-textarea-bg");
            Color textareaForeground = UIManager.getLookAndFeelDefaults().getColor("wizard-banner-textarea-fg");
            Color bg = UIManager.getLookAndFeelDefaults().getColor("3-main-dark-color");
            this.setBackground(bg);
            this._headingTextArea.setEditable(false);
            this._headingTextArea.setBackground(textareaBackground);
            this._headingTextArea.setColumns(20);
            this._headingTextArea.setFont(new JLabel().getFont().deriveFont((float)new JLabel().getFont().getSize() + 2.0f));
            this._headingTextArea.setForeground(textareaForeground);
            this._headingTextArea.setLineWrap(true);
            this._headingTextArea.setWrapStyleWord(true);
            this._headingTextArea.setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 0));
            this._headingTextArea.setFocusable(false);
            this._headingTextArea.setMinimumSize(new Dimension(70, 63));
            this.gridBagConstraints = new GridBagConstraints();
            this.gridBagConstraints.gridx = 0;
            this.gridBagConstraints.gridy = 0;
            this.gridBagConstraints.gridwidth = 3;
            this.gridBagConstraints.fill = 1;
            this.gridBagConstraints.weightx = 1.0;
            this.gridBagConstraints.insets = new Insets(0, 0, 12, 0);
            this.add((Component)this._headingTextArea, this.gridBagConstraints);
            this._iconPanel.setBackground(textareaBackground);
            this._iconPanel.setMaximumSize(new Dimension(63, 63));
            this._iconPanel.setMinimumSize(new Dimension(63, 63));
            this._iconPanel.setPreferredSize(new Dimension(63, 63));
            this._iconPanel.setLayout(new BorderLayout());
            this.gridBagConstraints = new GridBagConstraints();
            this.gridBagConstraints.gridx = 3;
            this.gridBagConstraints.gridy = 0;
            this.gridBagConstraints.fill = 3;
            this.gridBagConstraints.insets = new Insets(0, 0, 12, 0);
            this.add((Component)this._iconPanel, this.gridBagConstraints);
            this._iconLabel.setBackground(textareaBackground);
            this._iconLabel.setHorizontalAlignment(0);
            this._iconLabel.setVerticalAlignment(0);
            this._iconPanel.add(this._iconLabel);
        }

        public void setText(String text) {
            this._headingTextArea.setText(text);
        }

        public void addIcon(Object object) {
            if (object instanceof Component) {
                this._iconPanel.add((Component)object);
            } else if (object instanceof Image) {
                this._iconLabel.setIcon(new ImageIcon((Image)object));
            }
        }
    }

    private static class WizardPanel
    extends JPanel {
        private JPanel rightPanel = new JPanel(new BorderLayout());
        private JLabel panelName = new JLabel("Step");
        HeadingTextAndIcon heading;
        JPanel fullRightPanel = new JPanel(new BorderLayout());
        private JList contentList;
        private Component rightComponent;
        private ImagedPanel contentPanel;
        private JPanel contentLabelPanel;
        private WrappedCellRenderer cellRenderer;
        private JTabbedPane tabbedPane;
        private HtmlBrowser htmlBrowser;
        private Dimension cachedDimension;
        private JLabel label;
        private JPanel progressBarPanel;
        private int selectedIndex;
        private JTextPane messagePane;
        private JLabel iconLabel;
        private Color nbErrorForeground;
        private Color nbWarningForeground;
        private Color nbInfoForeground;
        private static final int MSG_TYPE_ERROR = 1;
        private static final int MSG_TYPE_WARNING = 2;
        private static final int MSG_TYPE_INFO = 3;

        public WizardPanel(boolean contentDisplayed, boolean helpDispalyed, boolean contentNumbered, Dimension leftDimension) {
            super(new BorderLayout());
            this.initComponents(contentDisplayed, helpDispalyed, contentNumbered, leftDimension);
            this.setOpaque(false);
            this.resetPreferredSize();
        }

        private void initComponents(boolean contentDisplayed, boolean helpDisplayed, boolean contentNumbered, Dimension leftDimension) {
            Color bg = UIManager.getLookAndFeelDefaults().getColor("3-main-dark-color");
            this.setBackground(bg);
            if (contentDisplayed) {
                this.createContentPanel(contentNumbered, leftDimension);
                if (!helpDisplayed) {
                    this.contentPanel.setBackground(bg);
                    this.contentPanel.setBorder(BorderFactory.createEmptyBorder(12, 12, 0, 0));
                    this.add((Component)this.contentPanel, "West");
                }
            }
            if (helpDisplayed) {
                this.htmlBrowser = new BoundedHtmlBrowser(leftDimension);
                this.htmlBrowser.setPreferredSize(leftDimension);
                if (!contentDisplayed) {
                    this.add((Component)this.htmlBrowser, "West");
                }
            }
            if (helpDisplayed && contentDisplayed) {
                this.tabbedPane = new JTabbedPane(3);
                this.tabbedPane.addTab(NbBundle.getMessage(WizardDescriptor.class, (String)"CTL_ContentName"), this.contentPanel);
                this.tabbedPane.addTab(NbBundle.getMessage(WizardDescriptor.class, (String)"CTL_HelpName"), (Component)this.htmlBrowser);
                this.tabbedPane.setEnabledAt(1, false);
                this.tabbedPane.setOpaque(false);
                this.add((Component)this.tabbedPane, "West");
            }
            this.panelName.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, this.panelName.getForeground()));
            this.panelName.setFont(WizardDescriptor.doDeriveFont(this.panelName.getFont(), 1));
            JPanel labelPanel = new JPanel(new BorderLayout());
            labelPanel.add((Component)this.panelName, "North");
            labelPanel.setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 11));
            this.rightPanel.setBorder(BorderFactory.createEmptyBorder(0, 12, 11, 11));
            this.panelName.setLabelFor(labelPanel);
            this.nbErrorForeground = UIManager.getColor("nb.errorForeground");
            if (this.nbErrorForeground == null) {
                this.nbErrorForeground = new Color(255, 0, 0);
            }
            this.nbWarningForeground = UIManager.getColor("nb.warningForeground");
            if (this.nbWarningForeground == null) {
                this.nbWarningForeground = new Color(51, 51, 51);
            }
            this.nbInfoForeground = UIManager.getColor("nb.warningForeground");
            if (this.nbInfoForeground == null) {
                this.nbInfoForeground = UIManager.getColor("Label.foreground");
            }
            JPanel errorPanel = new JPanel(new BorderLayout());
            errorPanel.setBorder(BorderFactory.createEmptyBorder(0, 12, 12, 11));
            this.messagePane = new FixedHeightPane();
            this.messagePane.setForeground(this.nbErrorForeground);
            this.iconLabel = new FixedHeightLabel();
            errorPanel.add((Component)this.iconLabel, "Before");
            errorPanel.add((Component)this.messagePane, "Center");
            this.progressBarPanel = new JPanel(new BorderLayout());
            this.progressBarPanel.setVisible(false);
            if (contentDisplayed) {
                this.progressBarPanel.setOpaque(true);
                this.progressBarPanel.setBackground(UIManager.getLookAndFeelDefaults().getColor("wizard-banner-textarea-bg"));
                this.progressBarPanel.setOpaque(false);
                this.progressBarPanel.setBorder(BorderFactory.createEmptyBorder(0, 4, 7, 4));
                this.contentPanel.add((Component)this.progressBarPanel, "South");
            } else {
                this.progressBarPanel.add((Component)new JLabel(), "North");
                JProgressBar pb = new JProgressBar();
                pb.setOrientation(0);
                pb.setAlignmentX(0.5f);
                pb.setAlignmentY(0.5f);
                pb.setString("0");
                this.progressBarPanel.add((Component)pb, "Center");
                this.progressBarPanel.setBorder(BorderFactory.createEmptyBorder(4, 0, 0, 0));
                errorPanel.add((Component)this.progressBarPanel, "South");
            }
            JPanel fullRightPanelContainer = new JPanel(new BorderLayout());
            fullRightPanelContainer.setBorder(BorderFactory.createEmptyBorder(12, 12, 0, 12));
            fullRightPanelContainer.setBackground(bg);
            if (showWizardPanel) {
                this.fullRightPanel.add((Component)labelPanel, "North");
            } else {
                this.rightPanel.setBorder(BorderFactory.createEmptyBorder(11, 12, 11, 11));
            }
            this.fullRightPanel.add((Component)this.rightPanel, "Center");
            this.fullRightPanel.add((Component)errorPanel, "South");
            this.heading = new HeadingTextAndIcon();
            fullRightPanelContainer.add((Component)this.heading, "North");
            fullRightPanelContainer.add((Component)this.fullRightPanel, "Center");
            this.add((Component)fullRightPanelContainer, "Center");
        }

        public void setMessage(int msgType, String msg) {
            if (msg != null && msg.trim().length() > 0) {
                switch (msgType) {
                    case 1: {
                        this.prepareMessage(msg, ImageUtilities.loadImageIcon((String)"org/netbeans/modules/dialogs/error.gif", (boolean)true), this.nbErrorForeground);
                        break;
                    }
                    case 2: {
                        this.prepareMessage(msg, ImageUtilities.loadImageIcon((String)"org/netbeans/modules/dialogs/warning.gif", (boolean)true), this.nbWarningForeground);
                        break;
                    }
                    case 3: {
                        this.prepareMessage(msg, ImageUtilities.loadImageIcon((String)"org/netbeans/modules/dialogs/info.png", (boolean)true), this.nbInfoForeground);
                        break;
                    }
                    default: {
                        break;
                    }
                }
            } else {
                this.prepareMessage(null, null, null);
            }
        }

        private void prepareMessage(final String msg, final ImageIcon icon, final Color fgColor) {
            if (!SwingUtilities.isEventDispatchThread()) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        WizardPanel.this.prepareMessage(msg, icon, fgColor);
                    }
                });
                return;
            }
            String message = msg;
            this.messagePane.setToolTipText(message);
            if (message != null && !(message = message.replaceAll("\\s", "&nbsp;")).toUpperCase().startsWith("<HTML>")) {
                message = "<HTML>" + message;
            }
            this.iconLabel.setIcon(icon);
            this.iconLabel.setForeground(fgColor);
            this.messagePane.setForeground(fgColor);
            this.messagePane.setText(message);
            this.messagePane.setFocusable(message != null && !message.isEmpty());
        }

        private void setProgressComponent(JComponent progressComp, final JLabel progressLabel) {
            assert (SwingUtilities.isEventDispatchThread());
            if (progressComp == null) {
                this.progressBarPanel.removeAll();
                this.progressBarPanel.setVisible(false);
            } else {
                if (progressLabel != null) {
                    progressLabel.setText(PROGRESS_BAR_DISPLAY_NAME);
                    progressLabel.addPropertyChangeListener("text", new PropertyChangeListener(){

                        @Override
                        public void propertyChange(PropertyChangeEvent evt) {
                            progressLabel.putClientProperty("ToolTipText", evt.getNewValue().toString());
                        }
                    });
                    progressLabel.setToolTipText(PROGRESS_BAR_DISPLAY_NAME);
                    this.progressBarPanel.add((Component)progressLabel, "North");
                }
                this.progressBarPanel.add((Component)progressComp, "Center");
                this.progressBarPanel.setVisible(true);
            }
        }

        private void createContentPanel(boolean contentNumbered, Dimension leftDimension) {
            this.contentList = new JList();
            this.cellRenderer = new WrappedCellRenderer(contentNumbered, leftDimension.width);
            this.cellRenderer.setOpaque(false);
            this.contentList.setCellRenderer(this.cellRenderer);
            this.contentList.setBackground(UIManager.getLookAndFeelDefaults().getColor("wizard-banner-textarea-bg"));
            this.contentList.setOpaque(true);
            this.contentList.setEnabled(false);
            this.contentList.getAccessibleContext().setAccessibleDescription("");
            JScrollPane scroll = new JScrollPane(this.contentList);
            scroll.setHorizontalScrollBarPolicy(31);
            scroll.getViewport().setOpaque(false);
            scroll.setBorder(BorderFactory.createEmptyBorder());
            scroll.setOpaque(false);
            scroll.setViewportBorder(BorderFactory.createEmptyBorder());
            this.label = new JLabel(NbBundle.getMessage(WizardDescriptor.class, (String)"CTL_ContentName").toUpperCase());
            this.label.setForeground(UIManager.getLookAndFeelDefaults().getColor("wizard-banner-textarea-fg"));
            this.label.setFont(WizardDescriptor.doDeriveFont(this.label.getFont().deriveFont((float)this.label.getFont().getSize() + 2.0f), 1));
            this.contentLabelPanel = new JPanel(new BorderLayout());
            this.contentLabelPanel.setBorder(BorderFactory.createEmptyBorder(12, 12, 11, 11));
            this.contentLabelPanel.setOpaque(true);
            this.contentLabelPanel.setBackground(UIManager.getLookAndFeelDefaults().getColor("3-maltego-black"));
            this.contentLabelPanel.add((Component)this.label, "North");
            this.contentPanel = new ImagedPanel(null);
            this.contentPanel.add((Component)this.contentLabelPanel, "North");
            this.contentPanel.add((Component)scroll, "Center");
            this.contentPanel.setPreferredSize(leftDimension);
            this.label.setLabelFor(this.contentList);
        }

        public void setContent(final String[] content) {
            final JList list = this.contentList;
            if (list == null) {
                return;
            }
            Mutex.EVENT.writeAccess(new Runnable(){

                @Override
                public void run() {
                    list.setListData(content);
                    list.revalidate();
                    list.repaint();
                    WizardPanel.this.contentLabelPanel.setVisible(content.length > 0);
                }
            });
        }

        public void setSelectedIndex(final int index) {
            this.selectedIndex = index;
            if (this.cellRenderer != null) {
                this.cellRenderer.setSelectedIndex(index);
                final JList list = this.contentList;
                if (list == null) {
                    return;
                }
                Mutex.EVENT.readAccess(new Runnable(){

                    @Override
                    public void run() {
                        list.ensureIndexIsVisible(index);
                        list.setFixedCellWidth(0);
                        list.setFixedCellWidth(-1);
                    }
                });
            }
        }

        public void setContentBackColor(Color color) {
            if (this.contentPanel != null) {
                this.contentPanel.setBackground(color);
            }
        }

        public void setContentForegroundColor(Color color) {
            if (this.cellRenderer == null) {
                return;
            }
            this.cellRenderer.setForegroundColor(color);
        }

        public void setImage(Image image) {
            if (this.contentPanel != null) {
                this.contentPanel.setImage(image);
            }
        }

        public void setImageAlignment(String align) {
            if (this.contentPanel != null) {
                this.contentPanel.setImageAlignment(align);
            }
        }

        public void setRightComponent(Component c) {
            if (this.rightComponent != null) {
                this.rightPanel.remove(this.rightComponent);
            }
            this.rightComponent = c;
            this.rightPanel.add(this.rightComponent, "Center");
        }

        public Component getRightComponent() {
            return this.rightComponent;
        }

        public void setPanelName(String name) {
            this.panelName.setText(name);
        }

        public void setHeadingText(String text) {
            this.heading.setText(text);
        }

        public void addHeadingIcon(Object object) {
            this.heading.addIcon(object);
        }

        public void setPanelBackground(Color color) {
            this.setPanelColor(this.fullRightPanel, color);
        }

        private void setPanelColor(Container parent, Color color) {
            for (Component c : parent.getComponents()) {
                if (!(c instanceof Container) || c instanceof JTabbedPane) continue;
                if (c instanceof JPanel) {
                    c.setBackground(color);
                }
                this.setPanelColor((Container)c, color);
            }
        }

        public void setHelpURL(URL helpURL) {
            if (this.htmlBrowser == null) {
                return;
            }
            if (helpURL != null) {
                if (!helpURL.equals(this.htmlBrowser.getDocumentURL())) {
                    this.htmlBrowser.setURL(helpURL);
                }
                if (this.tabbedPane != null) {
                    this.tabbedPane.setEnabledAt(this.tabbedPane.indexOfComponent((Component)this.htmlBrowser), true);
                }
            } else if (this.tabbedPane != null) {
                this.tabbedPane.setSelectedComponent(this.contentPanel);
                this.tabbedPane.setEnabledAt(this.tabbedPane.indexOfComponent((Component)this.htmlBrowser), false);
            }
        }

        public void resetPreferredSize() {
            this.cachedDimension = new Dimension(600, 365);
        }

        @Override
        public Dimension getPreferredSize() {
            Dimension dim = super.getPreferredSize();
            if (dim.height > this.cachedDimension.height) {
                this.cachedDimension.height = dim.height;
            }
            if (dim.width > this.cachedDimension.width) {
                this.cachedDimension.width = dim.width;
            }
            return this.cachedDimension;
        }

        @Override
        public void requestFocus() {
            if (this.rightComponent != null && this.rightComponent.isDisplayable()) {
                JComponent comp = (JComponent)this.rightComponent;
                Container rootAnc = comp.getFocusCycleRootAncestor();
                FocusTraversalPolicy policy = rootAnc.getFocusTraversalPolicy();
                Component focus = policy.getComponentAfter(rootAnc, comp);
                if (focus != null) {
                    focus.requestFocus();
                } else {
                    comp.requestFocus();
                }
            } else {
                super.requestFocus();
            }
        }

        @Deprecated
        @Override
        public boolean requestDefaultFocus() {
            if (this.rightComponent instanceof JComponent) {
                return ((JComponent)this.rightComponent).requestDefaultFocus();
            }
            return super.requestDefaultFocus();
        }

        @Override
        public AccessibleContext getAccessibleContext() {
            if (this.accessibleContext == null) {
                this.accessibleContext = new AccessibleWizardPanel();
            }
            return this.accessibleContext;
        }

        private class AccessibleWizardPanel
        extends JPanel.AccessibleJPanel {
            AccessibleWizardPanel() {
                super(WizardPanel.this);
            }

            @Override
            public String getAccessibleDescription() {
                if (this.accessibleDescription != null) {
                    return this.accessibleDescription;
                }
                if (WizardPanel.this.rightComponent instanceof Accessible) {
                    if (WizardPanel.this.rightComponent.getAccessibleContext().getAccessibleDescription() == null) {
                        return null;
                    }
                    return NbBundle.getMessage(WizardDescriptor.class, (String)"ACSD_WizardPanel", (Object)new Integer(WizardPanel.this.selectedIndex + 1), (Object)WizardPanel.this.panelName.getText(), (Object)WizardPanel.this.rightComponent.getAccessibleContext().getAccessibleDescription());
                }
                return super.getAccessibleDescription();
            }
        }

    }

    private static class WrappedCellRenderer
    extends JPanel
    implements ListCellRenderer {
        JTextArea ta = new JTextArea();
        JLabel numberLabel;
        int selected = -1;
        boolean contentNumbered;
        int taWidth;

        private WrappedCellRenderer(boolean contentNumbered, int wrappingWidth) {
            super(new BorderLayout());
            this.contentNumbered = contentNumbered;
            this.ta.setOpaque(false);
            this.ta.setEditable(false);
            this.ta.setLineWrap(true);
            this.ta.setWrapStyleWord(true);
            this.ta.setFont(UIManager.getFont("Label.font"));
            this.ta.getAccessibleContext().setAccessibleDescription("");
            this.ta.setBorder(BorderFactory.createEmptyBorder());
            this.taWidth = wrappingWidth - 12 - 12;
            this.numberLabel = new JLabel(){

                @Override
                protected void paintComponent(Graphics g) {
                    super.paintComponent(g);
                    if (!WrappedCellRenderer.this.contentNumbered) {
                        Rectangle rect = g.getClipBounds();
                        g.fillOval(rect.x, rect.y, 7, 7);
                    }
                }
            };
            this.numberLabel.setLabelFor(this.ta);
            this.numberLabel.setHorizontalAlignment(2);
            this.numberLabel.setVerticalAlignment(1);
            this.numberLabel.setFont(this.ta.getFont());
            this.numberLabel.setOpaque(false);
            this.numberLabel.setPreferredSize(new Dimension(25, 0));
            this.add((Component)this.numberLabel, "West");
            this.taWidth -= 25;
            Insets taInsets = this.ta.getInsets();
            this.ta.setSize(this.taWidth, taInsets.top + taInsets.bottom + 1);
            this.add((Component)this.ta, "Center");
            this.setOpaque(false);
            this.setBorder(BorderFactory.createEmptyBorder(12, 12, 0, 12));
        }

        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            if (index == this.selected) {
                this.numberLabel.setForeground(UIManager.getLookAndFeelDefaults().getColor("wizard-banner-textarea-fg"));
                this.ta.setForeground(UIManager.getLookAndFeelDefaults().getColor("wizard-banner-textarea-fg"));
                this.numberLabel.setFont(WizardDescriptor.doDeriveFont(this.numberLabel.getFont(), 1));
                this.ta.setFont(WizardDescriptor.doDeriveFont(this.ta.getFont(), 1));
            } else {
                this.numberLabel.setForeground(UIManager.getLookAndFeelDefaults().getColor("7-white"));
                this.ta.setForeground(UIManager.getLookAndFeelDefaults().getColor("7-white"));
                this.numberLabel.setFont(WizardDescriptor.doDeriveFont(this.numberLabel.getFont(), 0));
                this.ta.setFont(WizardDescriptor.doDeriveFont(this.ta.getFont(), 0));
            }
            if (this.contentNumbered) {
                this.numberLabel.setText(Integer.toString(index + 1) + ".");
            }
            Insets taInsets = this.ta.getInsets();
            this.ta.setSize(this.taWidth, taInsets.top + taInsets.bottom + 1);
            this.ta.setText((String)value);
            return this;
        }

        private void setSelectedIndex(int i) {
            this.selected = i;
        }

        private void setForegroundColor(Color color) {
            if (this.numberLabel != null) {
                this.numberLabel.setForeground(color);
                this.numberLabel.setBackground(color);
            }
            this.ta.setForeground(color);
        }

    }

    private static class ImagedPanel
    extends JComponent
    implements Accessible,
    Runnable {
        Image image;
        Image tempImage;
        Image image2Load;
        boolean isDefault = false;
        boolean loadPending = false;
        boolean north = true;
        private final Object IMAGE_LOCK = new Object();

        public ImagedPanel(Image im) {
            this.setImage(im);
            this.setLayout(new BorderLayout());
            this.setOpaque(true);
        }

        @Override
        protected void paintComponent(Graphics graphics) {
            graphics.setColor(this.getBackground());
            graphics.fillRect(0, 0, this.getWidth(), this.getHeight());
            if (this.image != null) {
                graphics.drawImage(this.image, 0, this.north ? 0 : this.getHeight() - this.image.getHeight(null), this);
            } else if (this.image2Load != null) {
                this.loadImageInBackground(this.image2Load);
                this.image2Load = null;
            }
        }

        public void setImageAlignment(String align) {
            this.north = "North".equals(align);
        }

        public void setImage(Image im) {
            if (im != null) {
                this.loadImage(im);
                this.isDefault = false;
                return;
            }
            if (!this.isDefault) {
                if (!UIManager.getBoolean("nb.wizard.hideimage")) {
                    this.loadImage(WizardDescriptor.getDefaultImage());
                }
                this.isDefault = true;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void loadImage(Image im) {
            MediaTracker mt = new MediaTracker(this);
            mt.addImage(im, 0);
            if (mt.checkID(0)) {
                this.image = im;
                if (this.isShowing()) {
                    this.repaint();
                }
                return;
            }
            if (this.isShowing()) {
                this.loadImageInBackground(im);
            } else {
                Object object = this.IMAGE_LOCK;
                synchronized (object) {
                    this.image = null;
                }
                this.image2Load = im;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void loadImageInBackground(Image image) {
            Object object = this.IMAGE_LOCK;
            synchronized (object) {
                this.tempImage = image;
                if (this.loadPending) {
                    return;
                }
                this.loadPending = true;
            }
            RequestProcessor.getDefault().post((Runnable)this, 30);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            Image localImage;
            Object object = this.IMAGE_LOCK;
            synchronized (object) {
                localImage = this.tempImage;
                this.tempImage = null;
                this.loadPending = false;
            }
            ImageIcon localImageIcon = new ImageIcon(localImage);
            boolean shouldRepaint = false;
            Object object2 = this.IMAGE_LOCK;
            synchronized (object2) {
                if (!this.loadPending) {
                    this.image = localImageIcon.getImage();
                    shouldRepaint = true;
                }
            }
            if (shouldRepaint) {
                this.repaint();
            }
        }
    }

    private class PropL
    implements PropertyChangeListener {
        PropL() {
        }

        @Override
        public void propertyChange(final PropertyChangeEvent e) {
            if (WizardDescriptor.this.wizardPanel == null) {
                return;
            }
            if (!SwingUtilities.isEventDispatchThread()) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        PropL.this.propertyChange(e);
                    }
                });
                return;
            }
            String propName = e.getPropertyName();
            WizardDescriptor.this.setPanelProperties((JComponent)WizardDescriptor.this.wizardPanel.getRightComponent());
            if ("WizardPanel_contentData".equals(propName)) {
                WizardDescriptor.this.wizardPanel.setContent(WizardDescriptor.this.contentData);
                WizardDescriptor.this.updateButtonAccessibleDescription();
            } else if ("WizardPanel_contentSelectedIndex".equals(propName)) {
                WizardDescriptor.this.wizardPanel.setSelectedIndex(WizardDescriptor.this.contentSelectedIndex);
                WizardDescriptor.this.updateButtonAccessibleDescription();
            } else if ("WizardPanel_contentBackColor".equals(propName)) {
                WizardDescriptor.this.wizardPanel.setContentBackColor(WizardDescriptor.this.contentBackColor);
            } else if ("WizardPanel_contentForegroundColor".equals(propName)) {
                WizardDescriptor.this.wizardPanel.setContentForegroundColor(WizardDescriptor.this.contentForegroundColor);
            } else if ("WizardPanel_image".equals(propName)) {
                WizardDescriptor.this.wizardPanel.setImage(WizardDescriptor.this.image);
            } else if ("WizardPanel_imageAlignment".equals(propName)) {
                WizardDescriptor.this.wizardPanel.setImageAlignment(WizardDescriptor.this.imageAlignment);
            } else if ("WizardPanel_helpURL".equals(propName)) {
                WizardDescriptor.this.wizardPanel.setHelpURL(WizardDescriptor.this.helpURL);
            }
        }

    }

    private final class Listener
    implements ChangeListener,
    ActionListener {
        Listener() {
        }

        @Override
        public void stateChanged(ChangeEvent ev) {
            WizardDescriptor.this._updateState();
        }

        @Override
        public void actionPerformed(ActionEvent ev) {
            final Iterator panels = WizardDescriptor.this.data.getIterator(WizardDescriptor.this);
            if (WizardDescriptor.this.wizardPanel != null) {
                WizardDescriptor.this.wizardPanel.setMessage(1, "");
            }
            Object src = ev.getSource();
            err.log(Level.FINE, "actionPerformed entry. Source: " + src);
            if (src == WizardDescriptor.this.nextButton) {
                assert (SwingUtilities.isEventDispatchThread());
                final Dimension previousSize = panels.current().getComponent().getSize();
                Runnable onValidPerformer = new Runnable(){

                    @Override
                    public void run() {
                        err.log(Level.FINE, "onValidPerformer on next button entry.");
                        WizardDescriptor.this.storeSettingsAndNotify(WizardDescriptor.this.data);
                        panels.nextPanel();
                        try {
                            WizardDescriptor.this.goToNextStep(previousSize);
                        }
                        catch (IllegalStateException ise) {
                            panels.previousPanel();
                            String msg = ise.getMessage();
                            if (msg != null) {
                                DialogDisplayer.getDefault().notify(new NotifyDescriptor.Message(msg));
                            } else {
                                Exceptions.printStackTrace((Throwable)ise);
                            }
                            WizardDescriptor.this._updateState();
                        }
                        err.log(Level.FINE, "onValidPerformer on next button exit.");
                    }
                };
                WizardDescriptor.this.lazyValidate(panels.current(), onValidPerformer);
            }
            if (ev.getSource() == WizardDescriptor.this.previousButton) {
                panels.previousPanel();
                WizardDescriptor.this.updateStateWithFeedback();
            }
            if (ev.getSource() == WizardDescriptor.this.finishButton) {
                Runnable onValidPerformer = new Runnable(){

                    @Override
                    public void run() {
                        err.log(Level.FINE, "onValidPerformer on finish button entry.");
                        WizardDescriptor.this.previousButton.setEnabled(false);
                        WizardDescriptor.this.nextButton.setEnabled(false);
                        WizardDescriptor.this.finishButton.setEnabled(false);
                        WizardDescriptor.this.cancelButton.setEnabled(false);
                        Runnable performFinish = new Runnable(){

                            @Override
                            public void run() {
                                err.log(Level.FINE, "performFinish entry.");
                                Object oldValue = WizardDescriptor.this.getValue();
                                try {
                                    WizardDescriptor.this.callInstantiate();
                                    WizardDescriptor.this.setValueWithoutPCH(NotifyDescriptor.OK_OPTION);
                                    WizardDescriptor.this.resetWizard();
                                }
                                catch (IOException ioe) {
                                    err.log(Level.INFO, null, ioe);
                                    WizardDescriptor.this.setValueWithoutPCH(WizardDescriptor.NEXT_OPTION);
                                    WizardDescriptor.this.updateStateWithFeedback();
                                    WizardDescriptor.this.putProperty("WizardPanel_errorMessage", ioe.getLocalizedMessage());
                                    return;
                                }
                                catch (RuntimeException x) {
                                    err.log(Level.WARNING, null, x);
                                    WizardDescriptor.this.setValueWithoutPCH(WizardDescriptor.NEXT_OPTION);
                                    WizardDescriptor.this.updateStateWithFeedback();
                                    WizardDescriptor.this.putProperty("WizardPanel_errorMessage", x.getLocalizedMessage());
                                    return;
                                }
                                WizardDescriptor.this.firePropertyChange("value", oldValue, NotifyDescriptor.OK_OPTION);
                                SwingUtilities.invokeLater(new Runnable(){

                                    @Override
                                    public void run() {
                                        err.log(Level.FINE, "WD.finishOption.fireActionPerformed()");
                                        WizardDescriptor.this.finishOption.fireActionPerformed();
                                        err.log(Level.FINE, "Set value to OK_OPTION.");
                                        WizardDescriptor.this.setValue(NotifyDescriptor.OK_OPTION);
                                    }
                                });
                                err.log(Level.FINE, "performFinish exit.");
                            }

                        };
                        if (panels instanceof AsynchronousInstantiatingIterator) {
                            err.log(Level.FINE, "Do ASYNCHRONOUS_JOBS_RP.post(performFinish).");
                            WizardDescriptor.ASYNCHRONOUS_JOBS_RP.post(performFinish);
                            if (panels instanceof BackgroundInstantiatingIterator) {
                                Window parentWindow = SwingUtilities.getWindowAncestor((Component)WizardDescriptor.this.getMessage());
                                if (parentWindow != null) {
                                    parentWindow.setVisible(false);
                                } else {
                                    err.log(Level.WARNING, "could not find parent window of {0}", WizardDescriptor.this.getMessage());
                                }
                            }
                        } else {
                            err.log(Level.FINE, "Run performFinish.");
                            performFinish.run();
                        }
                        err.log(Level.FINE, "onValidPerformer on finish button exit on {0}", panels);
                    }

                };
                WizardDescriptor.this.lazyValidate(panels.current(), onValidPerformer);
            }
            if (ev.getSource() == WizardDescriptor.this.cancelButton) {
                if (WizardDescriptor.this.backgroundValidationTask != null) {
                    WizardDescriptor.this.backgroundValidationTask.cancel();
                }
                Object oldValue = WizardDescriptor.this.getValue();
                WizardDescriptor.this.setValueWithoutPCH(NotifyDescriptor.CANCEL_OPTION);
                if (Arrays.asList(WizardDescriptor.this.getClosingOptions()).contains(WizardDescriptor.this.cancelButton)) {
                    try {
                        WizardDescriptor.this.resetWizard();
                    }
                    catch (RuntimeException x) {
                        err.log(Level.INFO, null, x);
                    }
                }
                WizardDescriptor.this.firePropertyChange("value", oldValue, NotifyDescriptor.CANCEL_OPTION);
            }
        }

    }

    public static class ArrayIterator<Data>
    implements Iterator<Data> {
        private Panel<Data>[] panels;
        private int index;

        public ArrayIterator() {
            this.panels = this.initializePanels();
            this.index = 0;
        }

        public ArrayIterator(Panel<Data>[] array) {
            this.panels = array;
            this.index = 0;
        }

        public ArrayIterator(List<Panel<Data>> panels) {
            this.panels = panels.toArray(new Panel[panels.size()]);
            this.index = 0;
        }

        protected Panel<Data>[] initializePanels() {
            return new Panel[0];
        }

        @Override
        public Panel<Data> current() {
            return this.panels[this.index];
        }

        @Override
        public String name() {
            return NbBundle.getMessage(WizardDescriptor.class, (String)"CTL_ArrayIteratorName", (Object)(this.index + 1), (Object)this.panels.length);
        }

        @Override
        public boolean hasNext() {
            return this.index < this.panels.length - 1;
        }

        @Override
        public boolean hasPrevious() {
            return this.index > 0;
        }

        @Override
        public synchronized void nextPanel() {
            if (this.index + 1 == this.panels.length) {
                throw new NoSuchElementException();
            }
            ++this.index;
        }

        @Override
        public synchronized void previousPanel() {
            if (this.index == 0) {
                throw new NoSuchElementException();
            }
            --this.index;
        }

        @Override
        public void addChangeListener(ChangeListener l) {
        }

        @Override
        public void removeChangeListener(ChangeListener l) {
        }

        protected void reset() {
            this.index = 0;
        }
    }

    public static interface ProgressInstantiatingIterator<Data>
    extends AsynchronousInstantiatingIterator<Data> {
        public Set instantiate(ProgressHandle var1) throws IOException;
    }

    public static interface BackgroundInstantiatingIterator<Data>
    extends AsynchronousInstantiatingIterator<Data> {
        @Override
        public Set instantiate() throws IOException;
    }

    public static interface AsynchronousInstantiatingIterator<Data>
    extends InstantiatingIterator<Data> {
        @Override
        public Set instantiate() throws IOException;
    }

    public static interface InstantiatingIterator<Data>
    extends Iterator<Data> {
        public Set instantiate() throws IOException;

        public void initialize(WizardDescriptor var1);

        public void uninitialize(WizardDescriptor var1);
    }

    public static interface FinishablePanel<Data>
    extends Panel<Data> {
        public boolean isFinishPanel();
    }

    public static interface ExtendedAsynchronousValidatingPanel<Data>
    extends AsynchronousValidatingPanel<Data> {
        @Override
        public void prepareValidation();

        @Override
        public void validate() throws WizardValidationException;

        public void finishValidation();
    }

    public static interface AsynchronousValidatingPanel<Data>
    extends ValidatingPanel<Data> {
        public void prepareValidation();

        @Override
        public void validate() throws WizardValidationException;
    }

    public static interface ValidatingPanel<Data>
    extends Panel<Data> {
        public void validate() throws WizardValidationException;
    }

    @Deprecated
    public static interface FinishPanel<Data>
    extends Panel<Data> {
    }

    public static interface Panel<Data> {
        public Component getComponent();

        public HelpCtx getHelp();

        public void readSettings(Data var1);

        public void storeSettings(Data var1);

        public boolean isValid();

        public void addChangeListener(ChangeListener var1);

        public void removeChangeListener(ChangeListener var1);
    }

    public static interface Iterator<Data> {
        public Panel<Data> current();

        public String name();

        public boolean hasNext();

        public boolean hasPrevious();

        public void nextPanel();

        public void previousPanel();

        public void addChangeListener(ChangeListener var1);

        public void removeChangeListener(ChangeListener var1);
    }

}

