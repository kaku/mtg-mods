/*
 * Decompiled with CFR 0_118.
 */
package org.openide;

import org.openide.NotifyDescriptor;

public final class NotificationLineSupport {
    private NotifyDescriptor nd;

    NotificationLineSupport(NotifyDescriptor descriptor) {
        this.nd = descriptor;
    }

    public final void setInformationMessage(String msg) {
        this.nd.setInformationMessage(msg);
    }

    public final String getInformationMessage() {
        return this.nd.getInformationMessage();
    }

    public final void setWarningMessage(String msg) {
        this.nd.setWarningMessage(msg);
    }

    public final String getWarningMessage() {
        return this.nd.getWarningMessage();
    }

    public final void setErrorMessage(String msg) {
        this.nd.setErrorMessage(msg);
    }

    public final String getErrorMessage() {
        return this.nd.getErrorMessage();
    }

    public final void clearMessages() {
        this.nd.clearMessages();
    }
}

