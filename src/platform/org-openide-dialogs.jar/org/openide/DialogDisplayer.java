/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.openide;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.ImageObserver;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.openide.DialogDescriptor;
import org.openide.NotificationLineSupport;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public abstract class DialogDisplayer {
    protected DialogDisplayer() {
    }

    public static DialogDisplayer getDefault() {
        DialogDisplayer dd = (DialogDisplayer)Lookup.getDefault().lookup(DialogDisplayer.class);
        if (dd == null) {
            dd = new Trivial();
        }
        return dd;
    }

    public abstract Object notify(NotifyDescriptor var1);

    public void notifyLater(final NotifyDescriptor descriptor) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                DialogDisplayer.this.notify(descriptor);
            }
        });
    }

    public abstract Dialog createDialog(DialogDescriptor var1);

    public Dialog createDialog(DialogDescriptor descriptor, Frame parent) {
        return this.createDialog(descriptor);
    }

    private static final class Trivial
    extends DialogDisplayer {
        private Trivial() {
        }

        @Override
        public Object notify(NotifyDescriptor nd) {
            StandardDialog dialog = new StandardDialog(nd.getTitle(), true, nd, null, null);
            dialog.setVisible(true);
            return nd.getValue() != null ? nd.getValue() : NotifyDescriptor.CLOSED_OPTION;
        }

        @Override
        public Dialog createDialog(DialogDescriptor dd) {
            StandardDialog dialog = new StandardDialog(dd.getTitle(), dd.isModal(), dd, dd.getClosingOptions(), dd.getButtonListener());
            dd.addPropertyChangeListener(new DialogUpdater(dialog, dd));
            return dialog;
        }

        private static Component message2Component(Object message) {
            if (message instanceof Component) {
                return (Component)message;
            }
            if (message instanceof Object[]) {
                Object[] sub = (Object[])message;
                JPanel panel = new JPanel();
                panel.setLayout(new FlowLayout());
                for (int i = 0; i < sub.length; ++i) {
                    panel.add(Trivial.message2Component(sub[i]));
                }
                return panel;
            }
            if (message instanceof Icon) {
                return new JLabel((Icon)message);
            }
            String text = message.toString();
            JTextArea area = new JTextArea(text);
            Color c = UIManager.getColor("Label.background");
            if (c != null) {
                area.setBackground(c);
            }
            area.setLineWrap(true);
            area.setWrapStyleWord(true);
            area.setEditable(false);
            area.setTabSize(4);
            area.setColumns(40);
            if (text.indexOf(10) != -1) {
                return new JScrollPane(area);
            }
            return area;
        }

        private static Component option2Button(Object option, NotifyDescriptor nd, ActionListener l, JRootPane rp) {
            String text;
            boolean defcap;
            if (option instanceof AbstractButton) {
                AbstractButton b = (AbstractButton)option;
                Trivial.removeOldListeners(b);
                b.addActionListener(l);
                return b;
            }
            if (option instanceof Component) {
                return (Component)option;
            }
            if (option instanceof Icon) {
                return new JLabel((Icon)option);
            }
            if (option == NotifyDescriptor.OK_OPTION) {
                text = NbBundle.getMessage(DialogDisplayer.class, (String)"CTL_OK");
                defcap = true;
            } else if (option == NotifyDescriptor.CANCEL_OPTION) {
                text = NbBundle.getMessage(DialogDisplayer.class, (String)"CTL_CANCEL");
                defcap = false;
            } else if (option == NotifyDescriptor.YES_OPTION) {
                text = NbBundle.getMessage(DialogDisplayer.class, (String)"CTL_YES");
                defcap = true;
            } else if (option == NotifyDescriptor.NO_OPTION) {
                text = NbBundle.getMessage(DialogDisplayer.class, (String)"CTL_NO");
                defcap = false;
            } else {
                if (option == NotifyDescriptor.CLOSED_OPTION) {
                    throw new IllegalArgumentException();
                }
                text = option.toString();
                defcap = false;
            }
            JButton b = new JButton(text);
            if (defcap && rp.getDefaultButton() == null) {
                rp.setDefaultButton(b);
            }
            b.getAccessibleContext().setAccessibleName(text);
            b.addActionListener(l);
            return b;
        }

        private static void removeOldListeners(AbstractButton button) {
            ArrayList<ActionListener> toRem = new ArrayList<ActionListener>();
            for (ActionListener al : button.getActionListeners()) {
                if (!(al instanceof StandardDialog.ButtonListener)) continue;
                toRem.add(al);
            }
            for (ActionListener al2 : toRem) {
                button.removeActionListener(al2);
            }
        }

        private static void updateNotificationLine(StandardDialog dialog, int msgType, Object o) {
            String msg;
            String string = msg = o == null ? null : o.toString();
            if (msg != null && msg.trim().length() > 0) {
                switch (msgType) {
                    case 1: {
                        Trivial.prepareMessage(dialog.notificationLine, ImageUtilities.loadImageIcon((String)"org/netbeans/modules/dialogs/error.gif", (boolean)true), dialog.nbErrorForeground);
                        break;
                    }
                    case 2: {
                        Trivial.prepareMessage(dialog.notificationLine, ImageUtilities.loadImageIcon((String)"org/netbeans/modules/dialogs/warning.gif", (boolean)true), dialog.nbWarningForeground);
                        break;
                    }
                    case 3: {
                        Trivial.prepareMessage(dialog.notificationLine, ImageUtilities.loadImageIcon((String)"org/netbeans/modules/dialogs/info.png", (boolean)true), dialog.nbInfoForeground);
                        break;
                    }
                }
                dialog.notificationLine.setToolTipText(msg);
            } else {
                Trivial.prepareMessage(dialog.notificationLine, null, null);
                dialog.notificationLine.setToolTipText(null);
            }
            dialog.notificationLine.setText(msg);
        }

        private static void prepareMessage(JLabel label, ImageIcon icon, Color fgColor) {
            label.setIcon(icon);
            label.setForeground(fgColor);
        }

        private static final class FixedHeightLabel
        extends JLabel {
            private static final int ESTIMATED_HEIGHT = 16;

            @Override
            public Dimension getPreferredSize() {
                Dimension preferredSize = super.getPreferredSize();
                assert (16 == ImageUtilities.loadImage((String)"org/netbeans/modules/dialogs/warning.gif").getHeight(null));
                preferredSize.height = Math.max(16, preferredSize.height);
                return preferredSize;
            }
        }

        private static class DialogUpdater
        implements PropertyChangeListener {
            private StandardDialog dialog;
            private DialogDescriptor dd;

            public DialogUpdater(StandardDialog dialog, DialogDescriptor dd) {
                this.dialog = dialog;
                this.dd = dd;
            }

            @Override
            public void propertyChange(final PropertyChangeEvent ev) {
                if (!SwingUtilities.isEventDispatchThread()) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            DialogUpdater.this.propertyChange(ev);
                        }
                    });
                    return;
                }
                String pname = ev.getPropertyName();
                if ("title".equals(pname)) {
                    this.dialog.setTitle(this.dd.getTitle());
                } else if ("noDefaultClose".equals(pname)) {
                    this.dialog.setDefaultCloseOperation(this.dd.isNoDefaultClose() ? 0 : 2);
                } else if ("message".equals(pname)) {
                    this.dialog.updateMessage();
                    this.dialog.validate();
                    this.dialog.repaint();
                } else if ("options".equals(pname) || "optionType".equals(pname)) {
                    this.dialog.updateOptions();
                    this.dialog.validate();
                    this.dialog.repaint();
                } else if ("infoNotification".equals(ev.getPropertyName())) {
                    Trivial.updateNotificationLine(this.dialog, 3, ev.getNewValue());
                } else if ("warningNotification".equals(ev.getPropertyName())) {
                    Trivial.updateNotificationLine(this.dialog, 2, ev.getNewValue());
                } else if ("errorNotification".equals(ev.getPropertyName())) {
                    Trivial.updateNotificationLine(this.dialog, 1, ev.getNewValue());
                }
            }

        }

        private static final class StandardDialog
        extends JDialog {
            final NotifyDescriptor nd;
            private Component messageComponent;
            private final JPanel buttonPanel;
            private final Object[] closingOptions;
            private final ActionListener buttonListener;
            private boolean haveFinalValue = false;
            private Color nbErrorForeground;
            private Color nbWarningForeground;
            private Color nbInfoForeground;
            private JLabel notificationLine;
            private static final int MSG_TYPE_ERROR = 1;
            private static final int MSG_TYPE_WARNING = 2;
            private static final int MSG_TYPE_INFO = 3;

            public StandardDialog(String title, boolean modal, NotifyDescriptor nd, Object[] closingOptions, ActionListener buttonListener) {
                super((Frame)null, title, modal);
                this.nd = nd;
                this.closingOptions = closingOptions;
                this.buttonListener = buttonListener;
                this.getContentPane().setLayout(new BorderLayout());
                this.setDefaultCloseOperation(nd.isNoDefaultClose() ? 0 : 2);
                this.updateMessage();
                this.buttonPanel = new JPanel();
                this.buttonPanel.setLayout(new FlowLayout(2));
                this.updateOptions();
                this.getContentPane().add(this.buttonPanel, "South", 1);
                KeyStroke k = KeyStroke.getKeyStroke(27, 0);
                String actionKey = "cancel";
                this.getRootPane().getInputMap(1).put(k, actionKey);
                AbstractAction cancelAction = new AbstractAction(){

                    @Override
                    public void actionPerformed(ActionEvent ev) {
                        if (!StandardDialog.this.nd.isNoDefaultClose()) {
                            StandardDialog.this.cancel();
                        }
                    }
                };
                this.getRootPane().getActionMap().put(actionKey, cancelAction);
                this.addWindowListener(new WindowAdapter(){

                    @Override
                    public void windowClosing(WindowEvent ev) {
                        if (!StandardDialog.this.haveFinalValue) {
                            StandardDialog.this.nd.setValue(NotifyDescriptor.CLOSED_OPTION);
                        }
                    }
                });
                this.pack();
                Rectangle r = Utilities.getUsableScreenBounds();
                int maxW = r.width * 9 / 10;
                int maxH = r.height * 9 / 10;
                Dimension d = this.getPreferredSize();
                d.width = Math.min(d.width, maxW);
                d.height = Math.min(d.height, maxH);
                this.setBounds(Utilities.findCenterBounds((Dimension)d));
            }

            private void cancel() {
                this.nd.setValue(NotifyDescriptor.CANCEL_OPTION);
                this.haveFinalValue = true;
                this.dispose();
            }

            public void updateMessage() {
                if (this.messageComponent != null) {
                    this.getContentPane().remove(this.messageComponent);
                }
                this.messageComponent = Trivial.message2Component(this.nd.getMessage());
                if (!(this.nd instanceof WizardDescriptor) && this.nd.getNotificationLineSupport() != null) {
                    JPanel toAdd = new JPanel(new BorderLayout());
                    toAdd.add(this.messageComponent, "Center");
                    this.nbErrorForeground = UIManager.getColor("nb.errorForeground");
                    if (this.nbErrorForeground == null) {
                        this.nbErrorForeground = new Color(255, 0, 0);
                    }
                    this.nbWarningForeground = UIManager.getColor("nb.warningForeground");
                    if (this.nbWarningForeground == null) {
                        this.nbWarningForeground = new Color(51, 51, 51);
                    }
                    this.nbInfoForeground = UIManager.getColor("nb.warningForeground");
                    if (this.nbInfoForeground == null) {
                        this.nbInfoForeground = UIManager.getColor("Label.foreground");
                    }
                    this.notificationLine = new FixedHeightLabel();
                    NotificationLineSupport nls = this.nd.getNotificationLineSupport();
                    if (nls.getInformationMessage() != null) {
                        Trivial.updateNotificationLine(this, 3, nls.getInformationMessage());
                    } else if (nls.getWarningMessage() != null) {
                        Trivial.updateNotificationLine(this, 2, nls.getWarningMessage());
                    } else if (nls.getErrorMessage() != null) {
                        Trivial.updateNotificationLine(this, 1, nls.getErrorMessage());
                    }
                    toAdd.add((Component)this.notificationLine, "South");
                    this.messageComponent = toAdd;
                }
                this.getContentPane().add(this.messageComponent, "Center");
            }

            public void updateOptions() {
                int i;
                HashSet<Object> addedOptions = new HashSet<Object>(5);
                Object[] options = this.nd.getOptions();
                if (options == null) {
                    switch (this.nd.getOptionType()) {
                        case -1: 
                        case 2: {
                            if (!Utilities.isMac()) {
                                options = new Object[]{NotifyDescriptor.OK_OPTION, NotifyDescriptor.CANCEL_OPTION};
                                break;
                            }
                            options = new Object[]{NotifyDescriptor.CANCEL_OPTION, NotifyDescriptor.OK_OPTION};
                            break;
                        }
                        case 0: {
                            if (!Utilities.isMac()) {
                                options = new Object[]{NotifyDescriptor.YES_OPTION, NotifyDescriptor.NO_OPTION};
                                break;
                            }
                            options = new Object[]{NotifyDescriptor.NO_OPTION, NotifyDescriptor.YES_OPTION};
                            break;
                        }
                        case 1: {
                            if (!Utilities.isMac()) {
                                options = new Object[]{NotifyDescriptor.YES_OPTION, NotifyDescriptor.NO_OPTION, NotifyDescriptor.CANCEL_OPTION};
                                break;
                            }
                            options = new Object[]{NotifyDescriptor.NO_OPTION, NotifyDescriptor.CANCEL_OPTION, NotifyDescriptor.YES_OPTION};
                            break;
                        }
                        default: {
                            throw new IllegalArgumentException();
                        }
                    }
                }
                this.buttonPanel.removeAll();
                JRootPane rp = this.getRootPane();
                for (i = 0; i < options.length; ++i) {
                    addedOptions.add(options[i]);
                    this.buttonPanel.add(Trivial.option2Button(options[i], this.nd, this.makeListener(options[i]), rp));
                }
                options = this.nd.getAdditionalOptions();
                if (options != null) {
                    for (i = 0; i < options.length; ++i) {
                        addedOptions.add(options[i]);
                        this.buttonPanel.add(Trivial.option2Button(options[i], this.nd, this.makeListener(options[i]), rp));
                    }
                }
                if (this.closingOptions != null) {
                    for (i = 0; i < this.closingOptions.length; ++i) {
                        if (!addedOptions.add(this.closingOptions[i])) continue;
                        ActionListener l = this.makeListener(this.closingOptions[i]);
                        this.attachActionListener(this.closingOptions[i], l);
                    }
                }
            }

            private void attachActionListener(Object comp, ActionListener l) {
                Method m;
                if (comp instanceof JButton) {
                    JButton b = (JButton)comp;
                    b.addActionListener(l);
                    return;
                }
                try {
                    m = comp.getClass().getMethod("addActionListener", ActionListener.class);
                    try {
                        m.setAccessible(true);
                    }
                    catch (SecurityException se) {
                        m = null;
                    }
                }
                catch (NoSuchMethodException e) {
                    m = null;
                }
                catch (SecurityException e2) {
                    m = null;
                }
                if (m != null) {
                    try {
                        m.invoke(comp, l);
                    }
                    catch (Exception e) {
                        // empty catch block
                    }
                }
            }

            private ActionListener makeListener(Object option) {
                return new ButtonListener(option);
            }

            private class ButtonListener
            implements ActionListener {
                private final Object option;

                public ButtonListener(Object option) {
                    this.option = option;
                }

                @Override
                public void actionPerformed(ActionEvent e) {
                    StandardDialog.this.nd.setValue(this.option);
                    if (StandardDialog.this.buttonListener != null) {
                        ActionEvent e2 = new ActionEvent(this.option, e.getID(), e.getActionCommand(), e.getWhen(), e.getModifiers());
                        StandardDialog.this.buttonListener.actionPerformed(e2);
                    }
                    if (StandardDialog.this.closingOptions == null || Arrays.asList(StandardDialog.this.closingOptions).contains(this.option)) {
                        StandardDialog.this.haveFinalValue = true;
                        StandardDialog.this.setVisible(false);
                    }
                }
            }

        }

    }

}

