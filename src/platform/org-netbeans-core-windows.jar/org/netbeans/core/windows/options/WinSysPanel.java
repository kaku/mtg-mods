/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.Utilities
 */
package org.netbeans.core.windows.options;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.prefs.Preferences;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.border.Border;
import org.netbeans.core.windows.FloatingWindowTransparencyManager;
import org.netbeans.core.windows.nativeaccess.NativeWindowSystem;
import org.netbeans.core.windows.options.WinSysOptionsPanelController;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.Utilities;

public class WinSysPanel
extends JPanel {
    protected final WinSysOptionsPanelController controller;
    private final Preferences prefs = NbPreferences.forModule(WinSysPanel.class);
    private ButtonGroup buttonGroup1;
    private JCheckBox isAlphaFloating;
    private JCheckBox isDragImage;
    private JCheckBox isDragImageAlpha;
    private JCheckBox isSnapScreenEdges;
    private JCheckBox isSnapping;

    protected WinSysPanel(WinSysOptionsPanelController controller) {
        this.controller = controller;
        this.initComponents();
        boolean isMacJDK17 = WinSysPanel.isMacJDK7();
        this.isAlphaFloating.setEnabled(!isMacJDK17);
    }

    private void initComponents() {
        this.buttonGroup1 = new ButtonGroup();
        this.isDragImage = new JCheckBox();
        this.isAlphaFloating = new JCheckBox();
        this.isSnapping = new JCheckBox();
        this.isDragImageAlpha = new JCheckBox();
        this.isSnapScreenEdges = new JCheckBox();
        this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((AbstractButton)this.isDragImage, (String)NbBundle.getMessage(WinSysPanel.class, (String)"LBL_DragWindowImage"));
        this.isDragImage.setToolTipText(NbBundle.getMessage(WinSysPanel.class, (String)"IsDragWindowTooltip"));
        this.isDragImage.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                WinSysPanel.this.isDragImageActionPerformed(evt);
            }
        });
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 17;
        this.add((Component)this.isDragImage, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.isAlphaFloating, (String)NbBundle.getMessage(WinSysPanel.class, (String)"LBL_TransparentFloatingWindows"));
        this.isAlphaFloating.setToolTipText(NbBundle.getMessage(WinSysPanel.class, (String)"IsAlphaFloatingTooltip"));
        this.isAlphaFloating.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                WinSysPanel.this.isAlphaFloatingActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(20, 0, 0, 0);
        this.add((Component)this.isAlphaFloating, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.isSnapping, (String)NbBundle.getMessage(WinSysPanel.class, (String)"LBL_SnapFloatingWindows"));
        this.isSnapping.setToolTipText(NbBundle.getMessage(WinSysPanel.class, (String)"IsSnappingTooltip"));
        this.isSnapping.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                WinSysPanel.this.isSnappingActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 25, 0, 0);
        this.add((Component)this.isSnapping, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.isDragImageAlpha, (String)NbBundle.getMessage(WinSysPanel.class, (String)"LBL_TransparentDragWindow"));
        this.isDragImageAlpha.setToolTipText(NbBundle.getMessage(WinSysPanel.class, (String)"IsAlphaDragTooltip"));
        this.isDragImageAlpha.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                WinSysPanel.this.isDragImageAlphaActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 17;
        this.add((Component)this.isDragImageAlpha, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.isSnapScreenEdges, (String)NbBundle.getMessage(WinSysPanel.class, (String)"LBL_SnapToScreenEdges"));
        this.isSnapScreenEdges.setToolTipText(NbBundle.getMessage(WinSysPanel.class, (String)"IsSnapScreenEdgesTooltip"));
        this.isSnapScreenEdges.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                WinSysPanel.this.isSnapScreenEdgesActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 25, 0, 0);
        this.add((Component)this.isSnapScreenEdges, gridBagConstraints);
    }

    private void isDragImageActionPerformed(ActionEvent evt) {
        this.updateDragSection();
        this.fireChanged();
    }

    private void isAlphaFloatingActionPerformed(ActionEvent evt) {
        this.fireChanged();
    }

    private void isSnappingActionPerformed(ActionEvent evt) {
        this.updateSnapSection();
        this.fireChanged();
    }

    private void isDragImageAlphaActionPerformed(ActionEvent evt) {
        this.fireChanged();
    }

    private void isSnapScreenEdgesActionPerformed(ActionEvent evt) {
        this.fireChanged();
    }

    private void fireChanged() {
        boolean isChanged = false;
        boolean isNotSolaris = Utilities.getOperatingSystem() != 8;
        boolean isMacJDK17 = WinSysPanel.isMacJDK7();
        if (this.isDragImage.isSelected() != this.prefs.getBoolean("dnd.dragimage", isNotSolaris && !isMacJDK17) || this.isDragImageAlpha.isSelected() != this.prefs.getBoolean("transparency.dragimage", isNotSolaris && !isMacJDK17) || this.isAlphaFloating.isSelected() != this.prefs.getBoolean("transparency.floating", false) || this.isSnapping.isSelected() != this.prefs.getBoolean("snapping", true) || this.isSnapScreenEdges.isSelected() != this.prefs.getBoolean("snapping.screenedges", true)) {
            isChanged = true;
        }
        this.controller.changed(isChanged);
    }

    protected void load() {
        boolean isNotSolaris = Utilities.getOperatingSystem() != 8;
        boolean isMacJDK17 = WinSysPanel.isMacJDK7();
        this.isDragImage.setSelected(this.prefs.getBoolean("dnd.dragimage", isNotSolaris && !isMacJDK17));
        this.isDragImageAlpha.setSelected(this.prefs.getBoolean("transparency.dragimage", isNotSolaris && !isMacJDK17));
        this.isAlphaFloating.setSelected(this.prefs.getBoolean("transparency.floating", false));
        this.isSnapping.setSelected(this.prefs.getBoolean("snapping", true));
        this.isSnapScreenEdges.setSelected(this.prefs.getBoolean("snapping.screenedges", true));
    }

    protected boolean store() {
        this.prefs.putBoolean("dnd.dragimage", this.isDragImage.isSelected());
        this.prefs.putBoolean("transparency.dragimage", this.isDragImageAlpha.isSelected());
        this.prefs.putBoolean("transparency.floating", this.isAlphaFloating.isSelected());
        FloatingWindowTransparencyManager.getDefault().update();
        this.prefs.putBoolean("snapping", this.isSnapping.isSelected());
        this.prefs.putBoolean("snapping.screenedges", this.isSnapScreenEdges.isSelected());
        return false;
    }

    boolean valid() {
        return true;
    }

    protected void initTabsPanel(JPanel panel) {
    }

    private void updateDragSection() {
        boolean isAlpha = NativeWindowSystem.getDefault().isUndecoratedWindowAlphaSupported();
        boolean isDrag = this.isDragImage.isSelected();
        this.isDragImageAlpha.setEnabled(isAlpha && isDrag);
        if (isAlpha) {
            this.isDragImageAlpha.setToolTipText(NbBundle.getMessage(WinSysPanel.class, (String)"IsAlphaDragTooltip"));
        } else {
            this.isDragImageAlpha.setToolTipText(NbBundle.getMessage(WinSysPanel.class, (String)"NoAlphaSupport"));
        }
    }

    private void updateSnapSection() {
        this.isSnapScreenEdges.setEnabled(this.isSnapping.isSelected());
    }

    private void updateFloatingSection() {
        boolean isAlpha = NativeWindowSystem.getDefault().isWindowAlphaSupported();
        this.isAlphaFloating.setEnabled(isAlpha);
        if (isAlpha) {
            this.isAlphaFloating.setToolTipText(NbBundle.getMessage(WinSysPanel.class, (String)"IsAlphaFloatingTooltip"));
        } else {
            this.isAlphaFloating.setToolTipText(NbBundle.getMessage(WinSysPanel.class, (String)"NoAlphaSupport"));
        }
    }

    private static boolean isMacJDK7() {
        String version;
        if (Utilities.isMac() && null != (version = System.getProperty("java.version")) && version.startsWith("1.7")) {
            return true;
        }
        return false;
    }

}

