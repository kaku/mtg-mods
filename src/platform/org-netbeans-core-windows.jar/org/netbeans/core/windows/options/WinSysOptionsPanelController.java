/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.WindowSystem
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package org.netbeans.core.windows.options;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.netbeans.core.WindowSystem;
import org.netbeans.core.windows.options.WinSysPanel;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public class WinSysOptionsPanelController
extends OptionsPanelController {
    private WinSysPanel panel;
    private final PropertyChangeSupport pcs;
    private boolean changed;

    public WinSysOptionsPanelController() {
        this.pcs = new PropertyChangeSupport((Object)this);
    }

    public void update() {
        this.getPanel().load();
        this.changed = false;
    }

    public void applyChanges() {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                boolean refreshWinsys = WinSysOptionsPanelController.this.getPanel().store();
                WinSysOptionsPanelController.this.changed = false;
                if (refreshWinsys) {
                    WindowSystem ws = (WindowSystem)Lookup.getDefault().lookup(WindowSystem.class);
                    ws.hide();
                    ws.show();
                }
            }
        });
    }

    public void cancel() {
    }

    public boolean isValid() {
        return this.getPanel().valid();
    }

    public boolean isChanged() {
        return this.changed;
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx("org.netbeans.core.windows.options.WinSysOptionsPanelController");
    }

    public JComponent getComponent(Lookup masterLookup) {
        return this.getPanel();
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    protected WinSysPanel getPanel() {
        if (this.panel == null) {
            this.panel = new WinSysPanel(this);
        }
        return this.panel;
    }

    protected void changed(boolean isChanged) {
        if (!this.changed) {
            this.pcs.firePropertyChange("changed", false, true);
        }
        this.changed = isChanged;
        this.pcs.firePropertyChange("valid", null, null);
    }

}

