/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.windows.options;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String KW_LafOptions() {
        return NbBundle.getMessage(Bundle.class, (String)"KW_LafOptions");
    }

    static String KW_TabsOptions() {
        return NbBundle.getMessage(Bundle.class, (String)"KW_TabsOptions");
    }

    static String Laf_DisplayName() {
        return NbBundle.getMessage(Bundle.class, (String)"Laf_DisplayName");
    }

    static String OptionsCategory_Keywords_Appearance() {
        return NbBundle.getMessage(Bundle.class, (String)"OptionsCategory_Keywords_Appearance");
    }

    static String OptionsCategory_Name_Appearance() {
        return NbBundle.getMessage(Bundle.class, (String)"OptionsCategory_Name_Appearance");
    }

    static String Tabs_DisplayName() {
        return NbBundle.getMessage(Bundle.class, (String)"Tabs_DisplayName");
    }

    private void Bundle() {
    }
}

