/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package org.netbeans.core.windows.options;

import java.util.prefs.Preferences;
import org.openide.util.NbPreferences;

public interface WinSysPrefs {
    public static final Preferences HANDLER = NbPreferences.forModule(WinSysPrefs.class);
    public static final String DND_SMALLWINDOWS = "dnd.smallwindows";
    public static final String DND_SMALLWINDOWS_WIDTH = "dnd.smallwindows.width";
    public static final String DND_SMALLWINDOWS_HEIGHT = "dnd.smallwindows.height";
    public static final String DND_DRAGIMAGE = "dnd.dragimage";
    public static final String TRANSPARENCY_DRAGIMAGE = "transparency.dragimage";
    public static final String TRANSPARENCY_DRAGIMAGE_ALPHA = "transparency.dragimage.alpha";
    public static final String TRANSPARENCY_FLOATING = "transparency.floating";
    public static final String TRANSPARENCY_FLOATING_TIMEOUT = "transparency.floating.timeout";
    public static final String TRANSPARENCY_FLOATING_ALPHA = "transparency.floating.alpha";
    public static final String SNAPPING = "snapping";
    public static final String SNAPPING_SCREENEDGES = "snapping.screenedges";
    public static final String SNAPPING_ACTIVE_SIZE = "snapping.active.size";
    public static final String EDITOR_CLOSE_ACTIVATES_RECENT = "editor.closing.activates.recent";
    public static final String OPEN_DOCUMENTS_NEXT_TO_ACTIVE_TAB = "editor.open.next.to.active";
    public static final String DOCUMENT_TABS_PLACEMENT = "document.tabs.placement";
    public static final String DOCUMENT_TABS_MULTIROW = "document.tabs.multirow";
    public static final String MAXIMIZE_NATIVE_LAF = "laf.maximize.native";
}

