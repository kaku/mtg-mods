/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.LifecycleManager
 *  org.openide.awt.Mnemonics
 *  org.openide.awt.Notification
 *  org.openide.awt.NotificationDisplayer
 *  org.openide.awt.NotificationDisplayer$Category
 *  org.openide.awt.NotificationDisplayer$Priority
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 */
package org.netbeans.core.windows.options;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.core.windows.options.LafOptionsPanelController;
import org.openide.LifecycleManager;
import org.openide.awt.Mnemonics;
import org.openide.awt.Notification;
import org.openide.awt.NotificationDisplayer;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

public class LafPanel
extends JPanel {
    protected final LafOptionsPanelController controller;
    private final Preferences prefs = NbPreferences.forModule(LafPanel.class);
    private final boolean isAquaLaF = "Aqua".equals(UIManager.getLookAndFeel().getID());
    private int defaultLookAndFeelIndex;
    private final ArrayList<UIManager.LookAndFeelInfo> lafs = new ArrayList(10);
    private ButtonGroup buttonGroup1;
    private JCheckBox checkMaximizeNativeLaF;
    private JComboBox comboLaf;
    private JLabel lblLaf;
    private JLabel lblRestart;
    private JPanel panelLaF;
    private JPanel panelLaFCombo;
    private static Notification restartNotification;
    private static final String COLOR_MODEL_CLASS_NAME = "org.netbeans.modules.options.colors.ColorModel";
    private static final String DARK_COLOR_THEME_NAME = "Norway Today";

    protected LafPanel(LafOptionsPanelController controller) {
        this.controller = controller;
        this.initComponents();
        this.checkMaximizeNativeLaF.addItemListener(new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent e) {
                LafPanel.this.fireChanged();
            }
        });
        this.initLookAndFeel();
        DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
        for (UIManager.LookAndFeelInfo li : this.lafs) {
            model.addElement(li.getName());
        }
        this.comboLaf.setModel(model);
        this.comboLaf.addItemListener(new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent e) {
                LafPanel.this.fireChanged();
            }
        });
    }

    private void fireChanged() {
        boolean isChanged = false;
        if (this.checkMaximizeNativeLaF.isSelected() != this.prefs.getBoolean("laf.maximize.native", false) || this.comboLaf.getSelectedIndex() != this.lafs.indexOf(this.isForcedLaF() ? this.getCurrentLaF() : this.getPreferredLaF())) {
            isChanged = true;
        }
        this.controller.changed(isChanged);
    }

    private void initComponents() {
        this.buttonGroup1 = new ButtonGroup();
        this.panelLaF = new JPanel();
        this.checkMaximizeNativeLaF = new JCheckBox();
        this.panelLaFCombo = new JPanel();
        this.comboLaf = new JComboBox();
        this.lblLaf = new JLabel();
        this.lblRestart = new JLabel();
        this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.setLayout(new GridBagLayout());
        this.panelLaF.setLayout(new BorderLayout());
        Mnemonics.setLocalizedText((AbstractButton)this.checkMaximizeNativeLaF, (String)NbBundle.getMessage(LafPanel.class, (String)"LafPanel.checkMaximizeNativeLaF.text"));
        this.checkMaximizeNativeLaF.setToolTipText(NbBundle.getMessage(LafPanel.class, (String)"LafPanel.checkMaximizeNativeLaF.toolTipText"));
        this.panelLaF.add((Component)this.checkMaximizeNativeLaF, "West");
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(5, 0, 0, 0);
        this.add((Component)this.panelLaF, gridBagConstraints);
        this.panelLaFCombo.setLayout(new BorderLayout(3, 0));
        this.panelLaFCombo.add((Component)this.comboLaf, "Center");
        this.lblLaf.setLabelFor(this.comboLaf);
        Mnemonics.setLocalizedText((JLabel)this.lblLaf, (String)NbBundle.getMessage(LafPanel.class, (String)"LafPanel.lblLaf.text"));
        this.panelLaFCombo.add((Component)this.lblLaf, "West");
        Mnemonics.setLocalizedText((JLabel)this.lblRestart, (String)NbBundle.getMessage(LafPanel.class, (String)"LafPanel.lblRestart.text"));
        this.panelLaFCombo.add((Component)this.lblRestart, "After");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 17;
        this.add((Component)this.panelLaFCombo, gridBagConstraints);
    }

    protected void load() {
        this.checkMaximizeNativeLaF.setSelected(this.prefs.getBoolean("laf.maximize.native", false));
        boolean isForcedLaF = this.isForcedLaF();
        this.defaultLookAndFeelIndex = this.lafs.indexOf(isForcedLaF ? this.getCurrentLaF() : this.getPreferredLaF());
        this.comboLaf.setSelectedIndex(this.defaultLookAndFeelIndex);
        this.comboLaf.setEnabled(!isForcedLaF);
    }

    protected boolean store() {
        this.prefs.putBoolean("laf.maximize.native", this.checkMaximizeNativeLaF.isSelected());
        System.setProperty("nb.native.filechooser", this.checkMaximizeNativeLaF.isSelected() ? "true" : "false");
        int selLaFIndex = this.comboLaf.getSelectedIndex();
        if (selLaFIndex != this.defaultLookAndFeelIndex && !this.isForcedLaF()) {
            UIManager.LookAndFeelInfo li = this.lafs.get(this.comboLaf.getSelectedIndex());
            NbPreferences.root().node("laf").put("laf", li.getClassName());
            this.askForRestart();
        }
        return false;
    }

    boolean valid() {
        return true;
    }

    private void initLookAndFeel() {
        this.lafs.clear();
        for (UIManager.LookAndFeelInfo i : UIManager.getInstalledLookAndFeels()) {
            this.lafs.add(i);
        }
    }

    private boolean isForcedLaF() {
        return null != System.getProperty("nb.laf.forced");
    }

    private UIManager.LookAndFeelInfo getCurrentLaF() {
        UIManager.LookAndFeelInfo currentLaf = null;
        String currentLAFClassName = UIManager.getLookAndFeel().getClass().getName();
        boolean isAqua = "Aqua".equals(UIManager.getLookAndFeel().getID());
        for (UIManager.LookAndFeelInfo li : this.lafs) {
            if (!currentLAFClassName.equals(li.getClassName()) && (!isAqua || !li.getClassName().contains("apple.laf.AquaLookAndFeel"))) continue;
            currentLaf = li;
            break;
        }
        return currentLaf;
    }

    private UIManager.LookAndFeelInfo getPreferredLaF() {
        String lafClassName = NbPreferences.root().node("laf").get("laf", null);
        if (null == lafClassName) {
            return this.getCurrentLaF();
        }
        UIManager.LookAndFeelInfo currentLaf = null;
        boolean isAqua = "Aqua".equals(UIManager.getLookAndFeel().getID());
        for (UIManager.LookAndFeelInfo li : this.lafs) {
            if (!lafClassName.equals(li.getClassName()) && (!isAqua || !li.getClassName().contains("apple.laf.AquaLookAndFeel"))) continue;
            currentLaf = li;
            break;
        }
        return currentLaf;
    }

    private void askForRestart() {
        if (null != restartNotification) {
            restartNotification.clear();
        }
        restartNotification = NotificationDisplayer.getDefault().notify(NbBundle.getMessage(LafPanel.class, (String)"Hint_RESTART_IDE"), (Icon)ImageUtilities.loadImageIcon((String)"org/netbeans/core/windows/resources/restart.png", (boolean)true), this.createRestartNotificationDetails(), this.createRestartNotificationDetails(), NotificationDisplayer.Priority.HIGH, NotificationDisplayer.Category.INFO);
    }

    void selectDarkLookAndFeel() {
        this.comboLaf.requestFocusInWindow();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                LafPanel.this.comboLaf.setPopupVisible(true);
            }
        });
    }

    private boolean isChangeEditorColorsPossible() {
        if (!this.isDarkLookAndFeel()) {
            return false;
        }
        ClassLoader cl = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        if (null == cl) {
            cl = LafPanel.class.getClassLoader();
        }
        try {
            Class klz = cl.loadClass("org.netbeans.modules.options.colors.ColorModel");
            Object colorModel = klz.newInstance();
            Method m = klz.getDeclaredMethod("getCurrentProfile", new Class[0]);
            Object res = m.invoke(colorModel, new Object[0]);
            return res != null && !"Norway Today".equals(res);
        }
        catch (Exception ex) {
            return false;
        }
    }

    private void switchEditorColorsProfile() {
        if (!this.isChangeEditorColorsPossible()) {
            return;
        }
        ClassLoader cl = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        if (null == cl) {
            cl = LafPanel.class.getClassLoader();
        }
        try {
            Class klz = cl.loadClass("org.netbeans.modules.options.colors.ColorModel");
            Object colorModel = klz.newInstance();
            Method m = klz.getDeclaredMethod("setCurrentProfile", String.class);
            m.invoke(colorModel, "Norway Today");
        }
        catch (Exception ex) {
            Logger.getLogger(LafPanel.class.getName()).log(Level.INFO, "Cannot change editor colors profile.", ex);
        }
    }

    private JComponent createRestartNotificationDetails() {
        JPanel res = new JPanel(new BorderLayout(10, 10));
        res.setOpaque(false);
        JLabel lbl = new JLabel(NbBundle.getMessage(LafPanel.class, (String)"Descr_Restart"));
        lbl.setCursor(Cursor.getPredefinedCursor(12));
        res.add((Component)lbl, "Center");
        final JCheckBox checkEditorColors = new JCheckBox(NbBundle.getMessage(LafPanel.class, (String)"Hint_ChangeEditorColors"));
        if (this.isChangeEditorColorsPossible()) {
            checkEditorColors.setSelected(true);
            checkEditorColors.setOpaque(false);
            res.add((Component)checkEditorColors, "South");
        }
        lbl.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent e) {
                if (null != restartNotification) {
                    restartNotification.clear();
                    restartNotification = null;
                }
                if (checkEditorColors.isSelected()) {
                    LafPanel.this.switchEditorColorsProfile();
                }
                LifecycleManager.getDefault().markForRestart();
                LifecycleManager.getDefault().exit();
            }
        });
        return res;
    }

    private boolean isDarkLookAndFeel() {
        String className = NbPreferences.root().node("laf").get("laf", null);
        if (null == className) {
            return false;
        }
        ClassLoader loader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        if (null == loader) {
            loader = ClassLoader.getSystemClassLoader();
        }
        try {
            Class klazz = loader.loadClass(className);
            LookAndFeel laf = (LookAndFeel)klazz.newInstance();
            return laf.getDefaults().getBoolean("nb.dark.theme");
        }
        catch (Exception e) {
            return false;
        }
    }

}

