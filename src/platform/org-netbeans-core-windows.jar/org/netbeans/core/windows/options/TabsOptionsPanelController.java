/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.WindowSystem
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package org.netbeans.core.windows.options;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.netbeans.core.WindowSystem;
import org.netbeans.core.windows.options.TabsPanel;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public class TabsOptionsPanelController
extends OptionsPanelController {
    private TabsPanel panel;
    private final PropertyChangeSupport pcs;
    private boolean changed;
    private boolean changedInnerTabsPanel;

    public TabsOptionsPanelController() {
        this.pcs = new PropertyChangeSupport((Object)this);
    }

    public void update() {
        this.getPanel().load();
        this.changed = false;
        this.changedInnerTabsPanel = false;
    }

    public void applyChanges() {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                boolean refreshWinsys = TabsOptionsPanelController.this.getPanel().store();
                TabsOptionsPanelController.this.changed = false;
                TabsOptionsPanelController.this.changedInnerTabsPanel = false;
                if (refreshWinsys) {
                    WindowSystem ws = (WindowSystem)Lookup.getDefault().lookup(WindowSystem.class);
                    ws.hide();
                    ws.show();
                }
            }
        });
    }

    public void cancel() {
    }

    public boolean isValid() {
        return this.getPanel().valid();
    }

    public boolean isChanged() {
        return this.changed || this.changedInnerTabsPanel;
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx("org.netbeans.core.windows.options.TabsOptionsPanelController");
    }

    public JComponent getComponent(Lookup masterLookup) {
        return this.getPanel();
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    protected TabsPanel getPanel() {
        if (this.panel == null) {
            this.panel = new TabsPanel(this);
        }
        return this.panel;
    }

    protected void changed(Object isChanged, Object isChangedInnerTabsPanel) {
        if (!this.changed) {
            this.pcs.firePropertyChange("changed", false, true);
        }
        if (isChanged != null) {
            this.changed = (Boolean)isChanged;
        }
        if (isChangedInnerTabsPanel != null) {
            this.changedInnerTabsPanel = (Boolean)isChangedInnerTabsPanel;
        }
        this.pcs.firePropertyChange("valid", null, null);
    }

}

