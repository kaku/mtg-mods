/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 */
package org.netbeans.core.windows.options;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.core.windows.options.TabsOptionsPanelController;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

public class TabsPanel
extends JPanel {
    protected final TabsOptionsPanelController controller;
    private final Preferences prefs = NbPreferences.forModule(TabsPanel.class);
    private final boolean isAquaLaF = "Aqua".equals(UIManager.getLookAndFeel().getID());
    private boolean defMultiRow;
    private int defTabPlacement;
    private ButtonGroup buttonGroup1;
    private JCheckBox checkMultiRow;
    private JLabel filler;
    private JCheckBox isCloseActivatesMostRecentDocument;
    private JCheckBox isNewDocumentOpensNextToActiveTab;
    private JLabel jLabel1;
    private JPanel panelDocTabs;
    private JPanel panelTabs;
    private JRadioButton radioBottom;
    private JRadioButton radioLeft;
    private JRadioButton radioRight;
    private JRadioButton radioTop;

    protected TabsPanel(TabsOptionsPanelController controller) {
        this.controller = controller;
        this.initComponents();
        this.initTabsPanel(this.panelTabs);
    }

    private void initComponents() {
        this.buttonGroup1 = new ButtonGroup();
        this.panelDocTabs = new JPanel();
        this.isCloseActivatesMostRecentDocument = new JCheckBox();
        this.isNewDocumentOpensNextToActiveTab = new JCheckBox();
        this.panelTabs = new JPanel();
        this.jLabel1 = new JLabel();
        this.radioTop = new JRadioButton();
        this.radioBottom = new JRadioButton();
        this.radioLeft = new JRadioButton();
        this.radioRight = new JRadioButton();
        this.checkMultiRow = new JCheckBox();
        this.filler = new JLabel();
        this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.setLayout(new GridBagLayout());
        this.panelDocTabs.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((AbstractButton)this.isCloseActivatesMostRecentDocument, (String)NbBundle.getMessage(TabsPanel.class, (String)"LBL_CloseActivatesRecentDocument"));
        this.isCloseActivatesMostRecentDocument.setToolTipText(NbBundle.getMessage(TabsPanel.class, (String)"TIP_CloseActivatesMostRecentDocument"));
        this.isCloseActivatesMostRecentDocument.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                TabsPanel.this.isCloseActivatesMostRecentDocumentActionPerformed(evt);
            }
        });
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 6, 0);
        this.panelDocTabs.add((Component)this.isCloseActivatesMostRecentDocument, gridBagConstraints);
        this.isCloseActivatesMostRecentDocument.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(TabsPanel.class, (String)"TabsPanel.isCloseActivatesMostRecentDocument.AccessibleContext.accessibleDescription"));
        Mnemonics.setLocalizedText((AbstractButton)this.isNewDocumentOpensNextToActiveTab, (String)NbBundle.getMessage(TabsPanel.class, (String)"TabsPanel.isNewDocumentOpensNextToActiveTab.text"));
        this.isNewDocumentOpensNextToActiveTab.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                TabsPanel.this.isNewDocumentOpensNextToActiveTabActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 18;
        this.panelDocTabs.add((Component)this.isNewDocumentOpensNextToActiveTab, gridBagConstraints);
        this.panelTabs.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)NbBundle.getMessage(TabsPanel.class, (String)"TabsPanel.jLabel1.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 13;
        this.panelTabs.add((Component)this.jLabel1, gridBagConstraints);
        this.buttonGroup1.add(this.radioTop);
        this.radioTop.setSelected(true);
        Mnemonics.setLocalizedText((AbstractButton)this.radioTop, (String)NbBundle.getMessage(TabsPanel.class, (String)"TabsPanel.radioTop.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 3, 0, 0);
        this.panelTabs.add((Component)this.radioTop, gridBagConstraints);
        this.buttonGroup1.add(this.radioBottom);
        Mnemonics.setLocalizedText((AbstractButton)this.radioBottom, (String)NbBundle.getMessage(TabsPanel.class, (String)"TabsPanel.radioBottom.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 3, 0, 0);
        this.panelTabs.add((Component)this.radioBottom, gridBagConstraints);
        this.buttonGroup1.add(this.radioLeft);
        Mnemonics.setLocalizedText((AbstractButton)this.radioLeft, (String)NbBundle.getMessage(TabsPanel.class, (String)"TabsPanel.radioLeft.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 3, 0, 0);
        this.panelTabs.add((Component)this.radioLeft, gridBagConstraints);
        this.buttonGroup1.add(this.radioRight);
        Mnemonics.setLocalizedText((AbstractButton)this.radioRight, (String)NbBundle.getMessage(TabsPanel.class, (String)"TabsPanel.radioRight.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 3, 0, 0);
        this.panelTabs.add((Component)this.radioRight, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.checkMultiRow, (String)NbBundle.getMessage(TabsPanel.class, (String)"TabsPanel.checkMultiRow.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(5, 0, 0, 0);
        this.panelTabs.add((Component)this.checkMultiRow, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 18;
        this.panelDocTabs.add((Component)this.panelTabs, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = 23;
        this.add((Component)this.panelDocTabs, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.filler, gridBagConstraints);
    }

    private void isNewDocumentOpensNextToActiveTabActionPerformed(ActionEvent evt) {
        this.fireChanged();
    }

    private void isCloseActivatesMostRecentDocumentActionPerformed(ActionEvent evt) {
        this.fireChanged();
    }

    private void fireChanged() {
        boolean isChanged = false;
        if (this.isCloseActivatesMostRecentDocument.isSelected() != this.prefs.getBoolean("editor.closing.activates.recent", true) || this.isNewDocumentOpensNextToActiveTab.isSelected() != this.prefs.getBoolean("editor.open.next.to.active", false)) {
            isChanged = true;
        }
        this.controller.changed(isChanged, null);
    }

    protected void load() {
        this.isCloseActivatesMostRecentDocument.setSelected(this.prefs.getBoolean("editor.closing.activates.recent", true));
        this.isNewDocumentOpensNextToActiveTab.setSelected(this.prefs.getBoolean("editor.open.next.to.active", false));
        this.defMultiRow = this.prefs.getBoolean("document.tabs.multirow", false);
        this.checkMultiRow.setSelected(this.defMultiRow);
        this.defTabPlacement = this.prefs.getInt("document.tabs.placement", 1);
        switch (this.defTabPlacement) {
            case 3: {
                this.radioBottom.setSelected(true);
                break;
            }
            case 2: {
                this.radioLeft.setSelected(true);
                break;
            }
            case 4: {
                this.radioRight.setSelected(true);
                break;
            }
            default: {
                this.radioTop.setSelected(true);
            }
        }
        if (this.isAquaLaF) {
            this.checkMultiRow.setSelected(false);
            this.checkMultiRow.setEnabled(false);
            this.radioLeft.setEnabled(false);
            this.radioRight.setEnabled(false);
            if (this.radioLeft.isSelected() || this.radioRight.isSelected()) {
                this.radioTop.setSelected(true);
            }
        }
    }

    protected boolean store() {
        this.prefs.putBoolean("editor.closing.activates.recent", this.isCloseActivatesMostRecentDocument.isSelected());
        this.prefs.putBoolean("editor.open.next.to.active", this.isNewDocumentOpensNextToActiveTab.isSelected());
        boolean needsWinsysRefresh = false;
        needsWinsysRefresh = this.checkMultiRow.isSelected() != this.defMultiRow;
        this.prefs.putBoolean("document.tabs.multirow", this.checkMultiRow.isSelected());
        int tabPlacement = 1;
        if (this.radioBottom.isSelected()) {
            tabPlacement = 3;
        } else if (this.radioLeft.isSelected()) {
            tabPlacement = 2;
        } else if (this.radioRight.isSelected()) {
            tabPlacement = 4;
        }
        this.prefs.putInt("document.tabs.placement", tabPlacement);
        return needsWinsysRefresh |= tabPlacement != this.defTabPlacement;
    }

    boolean valid() {
        return true;
    }

    protected void initTabsPanel(JPanel panel) {
    }

}

