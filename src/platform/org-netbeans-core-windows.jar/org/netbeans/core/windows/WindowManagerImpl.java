/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.plaf.BusyTabsSupport
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 *  org.openide.windows.TopComponentGroup
 *  org.openide.windows.WindowManager
 *  org.openide.windows.WindowManager$Component
 *  org.openide.windows.WindowSystemEvent
 *  org.openide.windows.WindowSystemListener
 *  org.openide.windows.Workspace
 */
package org.netbeans.core.windows;

import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JRootPane;
import javax.swing.RootPaneContainer;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import org.netbeans.core.windows.Central;
import org.netbeans.core.windows.FloatingWindowTransparencyManager;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.PersistenceHandler;
import org.netbeans.core.windows.RecentViewList;
import org.netbeans.core.windows.RegistryImpl;
import org.netbeans.core.windows.SplitConstraint;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.TopComponentGroupImpl;
import org.netbeans.core.windows.TopComponentTracker;
import org.netbeans.core.windows.WindowSystemEventType;
import org.netbeans.core.windows.WindowSystemImpl;
import org.netbeans.core.windows.actions.ActionUtils;
import org.netbeans.core.windows.options.WinSysPrefs;
import org.netbeans.core.windows.persistence.PersistenceManager;
import org.netbeans.core.windows.persistence.PersistenceObserver;
import org.netbeans.core.windows.view.dnd.TopComponentDraggable;
import org.netbeans.core.windows.view.ui.MainWindow;
import org.netbeans.swing.tabcontrol.plaf.BusyTabsSupport;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.TopComponentGroup;
import org.openide.windows.WindowManager;
import org.openide.windows.WindowSystemEvent;
import org.openide.windows.WindowSystemListener;
import org.openide.windows.Workspace;

public final class WindowManagerImpl
extends WindowManager
implements Workspace {
    public static final String PROP_ACTIVE_MODE = "activeMode";
    public static final String PROP_MAXIMIZED_MODE = "maximizedMode";
    public static final String PROP_EDITOR_AREA_STATE = "editorAreaState";
    private static final Object LOCK_INIT = new Object();
    private static WindowManagerImpl defaultInstance;
    static boolean assertsEnabled;
    private final Central central = new Central();
    private final PropertyChangeSupport changeSupport;
    private final RecentViewList recentViewList;
    private TopComponent persistenceShowingTC;
    private Exclusive exclusive;
    private boolean exclusivesCompleted;
    private Throwable createdBy;
    private final Collection<WindowSystemListener> listeners;
    private static final String ASSERTION_ERROR_MESSAGE = "Window System API is required to be called from AWT thread only, see http://core.netbeans.org/proposals/threading/";
    private String currentRole;
    private static final Object BUSY_FLAG;
    private static final String BUSY_PROP_NAME = "nbwinsys.tc.isbusy";

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public WindowManagerImpl() {
        this.changeSupport = new PropertyChangeSupport((Object)this);
        this.recentViewList = new RecentViewList(this);
        this.exclusivesCompleted = false;
        this.listeners = new ArrayList<WindowSystemListener>(10);
        this.currentRole = null;
        Object object = LOCK_INIT;
        synchronized (object) {
            if (defaultInstance != null) {
                IllegalStateException ex = new IllegalStateException("Instance already exists");
                if (WindowManagerImpl.defaultInstance.createdBy != null) {
                    ex.initCause(WindowManagerImpl.defaultInstance.createdBy);
                }
                throw ex;
            }
            boolean on = false;
            if (!$assertionsDisabled) {
                on = true;
                if (!true) {
                    throw new AssertionError();
                }
            }
            if (on) {
                this.createdBy = new Exception("createdBy");
            }
            defaultInstance = this;
        }
        this.busyIconWarmUp();
    }

    public static WindowManagerImpl getInstance() {
        if (defaultInstance != null) {
            return defaultInstance;
        }
        return (WindowManagerImpl)((Object)Lookup.getDefault().lookup(WindowManager.class));
    }

    public void topComponentRequestAttention(TopComponent tc) {
        if (tc.isOpened()) {
            ModeImpl mode = (ModeImpl)this.findMode(tc);
            this.central.topComponentRequestAttention(mode, tc);
        }
    }

    public void topComponentCancelRequestAttention(TopComponent tc) {
        if (tc.isOpened()) {
            ModeImpl mode = (ModeImpl)this.findMode(tc);
            this.central.topComponentCancelRequestAttention(mode, tc);
        }
    }

    public void topComponentAttentionHighlight(TopComponent tc, boolean highlight) {
        if (tc.isOpened()) {
            ModeImpl mode = (ModeImpl)this.findMode(tc);
            this.central.topComponentAttentionHighlight(mode, tc, highlight);
        }
    }

    public Frame getMainWindow() {
        WindowManagerImpl.warnIfNotInEDT();
        return this.central.getMainWindow();
    }

    public void updateUI() {
        WindowManagerImpl.warnIfNotInEDT();
        this.central.updateUI();
    }

    protected synchronized WindowManager.Component createTopComponentManager(TopComponent c) {
        WindowManagerImpl.warnIfNotInEDT();
        return null;
    }

    public Workspace createWorkspace(String name, String displayName) {
        WindowManagerImpl.warnIfNotInEDT();
        return this;
    }

    public Workspace findWorkspace(String name) {
        WindowManagerImpl.warnIfNotInEDT();
        return this;
    }

    public Workspace[] getWorkspaces() {
        WindowManagerImpl.warnIfNotInEDT();
        return new Workspace[]{this};
    }

    public void setWorkspaces(Workspace[] workspaces) {
        WindowManagerImpl.warnIfNotInEDT();
    }

    public Workspace getCurrentWorkspace() {
        WindowManagerImpl.warnIfNotInEDT();
        return this;
    }

    public TopComponentGroup findTopComponentGroup(String name) {
        WindowManagerImpl.assertEventDispatchThread();
        for (TopComponentGroupImpl group : this.getTopComponentGroups()) {
            if (!group.getName().equals(name)) continue;
            return group;
        }
        return null;
    }

    public TopComponent findTopComponent(String tcID) {
        WindowManagerImpl.warnIfNotInEDT();
        return this.getTopComponentForID(tcID);
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.changeSupport.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.changeSupport.removePropertyChangeListener(l);
    }

    public String getName() {
        return "FakeWorkspace";
    }

    public String getDisplayName() {
        return NbBundle.getMessage(WindowManagerImpl.class, (String)"LBL_FakeWorkspace");
    }

    public Set<? extends ModeImpl> getModes() {
        return this.central.getModes();
    }

    public Rectangle getBounds() {
        if (this.getEditorAreaState() == 0) {
            return this.getMainWindowBoundsJoined();
        }
        return this.getMainWindowBoundsSeparated();
    }

    public void activate() {
    }

    public Mode createMode(String name, String displayName, URL icon) {
        if (this.getEditorAreaState() == 0) {
            return new WrapMode(this.createMode(name, 1, 0, false, null));
        }
        return this.createMode(name, 0, 1, false, new SplitConstraint[]{new SplitConstraint(1, 1, 0.2)});
    }

    public void userUndockedMode(ModeImpl mode) {
        if (mode.getState() != 0) {
            throw new IllegalStateException("Mode is already in floating state: " + mode);
        }
        this.central.userUndockedMode(mode);
    }

    public void userDockedMode(ModeImpl mode) {
        if (mode.getState() != 1) {
            throw new IllegalStateException("Mode is not in floating state: " + mode);
        }
        this.central.userDockedMode(mode);
    }

    public void userMinimizedMode(ModeImpl mode) {
        WindowManagerImpl.assertEventDispatchThread();
        this.getCentral().userMinimizedMode(mode);
    }

    public void userRestoredMode(ModeImpl slidingMode, ModeImpl modeToRestore) {
        WindowManagerImpl.assertEventDispatchThread();
        this.getCentral().userRestoredMode(slidingMode, modeToRestore);
    }

    public void userClosedMode(ModeImpl mode) {
        WindowManagerImpl.assertEventDispatchThread();
        this.getCentral().userClosedMode(mode);
    }

    private Exclusive getExclusive() {
        if (this.exclusive == null) {
            this.exclusive = new Exclusive();
        }
        return this.exclusive;
    }

    private void toggleUseNativeFileChooser() {
        if (null == System.getProperty("nb.native.filechooser")) {
            boolean useNativeFileChooser = WinSysPrefs.HANDLER.getBoolean("laf.maximize.native", false);
            System.setProperty("nb.native.filechooser", useNativeFileChooser ? "true" : "false");
        }
    }

    public Mode findMode(String name) {
        return this.findModeImpl(name);
    }

    public Mode findMode(TopComponent tc) {
        if (tc == null) {
            return null;
        }
        TopComponent multiviewParent = (TopComponent)SwingUtilities.getAncestorOfClass(TopComponent.class, (Component)tc);
        if (null != multiviewParent) {
            tc = multiviewParent;
        }
        for (ModeImpl mode : this.getModes()) {
            if (!mode.containsTopComponent(tc)) continue;
            return mode;
        }
        return null;
    }

    public void remove() {
    }

    public void addTopComponentGroup(TopComponentGroupImpl tcGroup) {
        this.central.addTopComponentGroup(tcGroup);
    }

    public void removeTopComponentGroup(TopComponentGroupImpl tcGroup) {
        this.central.removeTopComponentGroup(tcGroup);
    }

    public Set<TopComponentGroupImpl> getTopComponentGroups() {
        return this.central.getTopComponentGroups();
    }

    public ModeImpl createMode(String name, int kind, int state, boolean permanent, SplitConstraint[] constraints) {
        ModeImpl mode = (ModeImpl)this.findMode(name);
        if (mode != null) {
            return mode;
        }
        if (constraints == null && kind != 2) {
            if (kind == 1) {
                return this.getDefaultEditorMode();
            }
            return this.getDefaultViewMode();
        }
        mode = this.createModeImpl(name, kind, state, permanent);
        this.addMode(mode, constraints);
        return mode;
    }

    public ModeImpl createSlidingMode(String name, boolean permanent, String side, Map<String, Integer> slideInSizes) {
        ModeImpl mode = (ModeImpl)this.findMode(name);
        if (mode != null) {
            return mode;
        }
        mode = this.createModeImpl(name, 2, permanent);
        this.central.addSlidingMode(mode, null, side, slideInSizes);
        return mode;
    }

    ModeImpl createModeImpl(String name, int kind, boolean permanent) {
        int state = this.getEditorAreaState() == 0 ? 0 : 1;
        return this.createModeImpl(name, kind, state, permanent);
    }

    ModeImpl createModeImpl(String name, int kind, int state, boolean permanent) {
        if (name == null) {
            name = ModeImpl.getUnusedModeName();
        }
        ModeImpl toReturn = ModeImpl.createModeImpl(name, state, kind, permanent);
        return toReturn;
    }

    ModeImpl getDefaultEditorMode() {
        ModeImpl mode = this.findModeImpl("editor");
        if (mode == null) {
            Logger.getLogger(WindowManagerImpl.class.getName()).log(Level.FINE, null, new IllegalStateException("Creating default editor mode. It shouldn't happen this way"));
            ModeImpl newMode = this.createModeImpl("editor", 1, true);
            this.addMode(newMode, new SplitConstraint[0]);
            return newMode;
        }
        return mode;
    }

    ModeImpl getDefaultEditorModeForOpen() {
        ModeImpl mode = this.central.getLastActiveEditorMode();
        if (mode == null) {
            return this.getDefaultEditorMode();
        }
        return mode;
    }

    ModeImpl getDefaultViewMode() {
        ModeImpl mode = this.findModeImpl("explorer");
        if (mode == null) {
            Logger.getLogger(WindowManagerImpl.class.getName()).log(Level.INFO, null, new IllegalStateException("Creating default view mode. It shouldn't happen this way"));
            ModeImpl newMode = this.createModeImpl("explorer", 0, true);
            this.addMode(newMode, new SplitConstraint[]{new SplitConstraint(0, 0, 0.7), new SplitConstraint(1, 0, 0.25)});
            return newMode;
        }
        return mode;
    }

    ModeImpl getDefaultSlidingMode() {
        ModeImpl mode = this.findModeImpl("sliding");
        if (mode == null) {
            Logger.getLogger(WindowManagerImpl.class.getName()).log(Level.INFO, null, new IllegalStateException("Creating default sliding mode. It shouldn't happen this way"));
            ModeImpl newMode = this.createModeImpl("sliding", 2, true);
            this.addMode(newMode, new SplitConstraint[]{new SplitConstraint(0, 0, 0.7), new SplitConstraint(1, 0, 0.25)});
            return newMode;
        }
        return mode;
    }

    private ModeImpl findModeImpl(String name) {
        if (name == null) {
            return null;
        }
        for (ModeImpl mode : this.getModes()) {
            if (!name.equals(mode.getName()) && !mode.getOtherNames().contains(name)) continue;
            return mode;
        }
        return null;
    }

    public TopComponent getSelectedTopComponent(Mode mode) {
        return this.central.getModeSelectedTopComponent((ModeImpl)mode);
    }

    public Rectangle getMainWindowBoundsJoined() {
        return this.central.getMainWindowBoundsJoined();
    }

    public void setMainWindowBoundsJoined(Rectangle bounds) {
        this.central.setMainWindowBoundsJoined(bounds);
    }

    public Rectangle getMainWindowBoundsSeparated() {
        return this.central.getMainWindowBoundsSeparated();
    }

    public void setMainWindowBoundsSeparated(Rectangle bounds) {
        this.central.setMainWindowBoundsSeparated(bounds);
    }

    public int getMainWindowFrameStateJoined() {
        return this.central.getMainWindowFrameStateJoined();
    }

    public void setMainWindowFrameStateJoined(int frameState) {
        this.central.setMainWindowFrameStateJoined(frameState);
    }

    public int getMainWindowFrameStateSeparated() {
        return this.central.getMainWindowFrameStateSeparated();
    }

    public void setMainWindowFrameStateSeparated(int frameState) {
        this.central.setMainWindowFrameStateSeparated(frameState);
    }

    public ModeImpl getActiveMode() {
        return this.central.getActiveMode();
    }

    public void setActiveMode(ModeImpl activeMode) {
        this.central.setActiveMode(activeMode);
    }

    public void setEditorAreaBounds(Rectangle editorAreaBounds) {
        this.central.setEditorAreaBounds(editorAreaBounds);
    }

    public Rectangle getEditorAreaBounds() {
        return this.central.getEditorAreaBounds();
    }

    public void setEditorAreaConstraints(SplitConstraint[] editorAreaConstraints) {
        this.central.setEditorAreaConstraints(editorAreaConstraints);
    }

    public Component getEditorAreaComponent() {
        return this.central.getEditorAreaComponent();
    }

    public SplitConstraint[] getEditorAreaConstraints() {
        return this.central.getEditorAreaConstraints();
    }

    public void setEditorAreaState(int editorAreaState) {
        this.setEditorAreaStateImpl(editorAreaState);
    }

    void setEditorAreaStateImpl(int editorAreaState) {
        this.central.setEditorAreaState(editorAreaState);
    }

    public int getEditorAreaState() {
        return this.central.getEditorAreaState();
    }

    public void setEditorAreaFrameState(int editorAreaFrameState) {
        this.central.setEditorAreaFrameState(editorAreaFrameState);
    }

    public int getEditorAreaFrameState() {
        return this.central.getEditorAreaFrameState();
    }

    public void switchMaximizedMode(ModeImpl newMaximizedMode) {
        this.central.switchMaximizedMode(newMaximizedMode);
    }

    public void setEditorMaximizedMode(ModeImpl editorMaximizedMode) {
        this.central.setEditorMaximizedMode(editorMaximizedMode);
    }

    public void setViewMaximizedMode(ModeImpl viewMaximizedMode) {
        this.central.setViewMaximizedMode(viewMaximizedMode);
    }

    public ModeImpl getCurrentMaximizedMode() {
        return this.central.getCurrentMaximizedMode();
    }

    public ModeImpl getEditorMaximizedMode() {
        return this.central.getEditorMaximizedMode();
    }

    public ModeImpl getViewMaximizedMode() {
        return this.central.getViewMaximizedMode();
    }

    public void setModeConstraints(ModeImpl mode, SplitConstraint[] modeConstraints) {
        this.central.setModeConstraints(mode, modeConstraints);
    }

    public SplitConstraint[] getModeConstraints(ModeImpl mode) {
        return this.central.getModeConstraints(mode);
    }

    private void addMode(ModeImpl mode, SplitConstraint[] modeConstraints) {
        if (mode.getKind() == 2) {
            this.central.addSlidingMode(mode, null, "left", null);
        } else {
            this.central.addMode(mode, modeConstraints);
        }
    }

    public void removeMode(ModeImpl mode) {
        if (mode.getKind() != 2) {
            this.central.removeMode(mode);
        }
    }

    public void setToolbarConfigName(String toolbarConfigName) {
        this.central.setToolbarConfigName(toolbarConfigName);
    }

    public String getToolbarConfigName() {
        return this.central.getToolbarConfigName();
    }

    public void setVisible(boolean visible) {
        if (!visible) {
            FloatingWindowTransparencyManager.getDefault().stop();
        }
        this.central.setVisible(visible);
        if (visible) {
            if (!this.exclusivesCompleted) {
                this.getExclusive().restart();
            } else {
                FloatingWindowTransparencyManager.getDefault().start();
            }
            this.toggleUseNativeFileChooser();
        } else {
            this.getExclusive().stop();
            this.exclusivesCompleted = false;
        }
    }

    public ModeImpl attachModeToSide(ModeImpl referenceMode, String side, String modeName, int modeKind, boolean permanent) {
        return this.central.attachModeToSide(referenceMode, side, modeName, modeKind, permanent);
    }

    public boolean isVisible() {
        return this.central.isVisible();
    }

    public TopComponent getTopComponentForID(String tcID) {
        return PersistenceHandler.getDefault().getTopComponentForID(tcID, true);
    }

    public boolean isTopComponentAllowedToMoveAnywhere(TopComponent tc) {
        if (Boolean.TRUE.equals(tc.getClientProperty((Object)"TopComponentAllowDockAnywhere"))) {
            return true;
        }
        return false;
    }

    public ModeImpl findModeForOpenedID(String tcID) {
        if (tcID == null) {
            return null;
        }
        for (ModeImpl mode : this.getModes()) {
            if (!mode.getOpenedTopComponentsIDs().contains(tcID)) continue;
            return mode;
        }
        return null;
    }

    public ModeImpl findModeForClosedID(String tcID) {
        if (tcID == null) {
            return null;
        }
        for (ModeImpl mode : this.getModes()) {
            if (!mode.getClosedTopComponentsIDs().contains(tcID)) continue;
            return mode;
        }
        return null;
    }

    public String getTopComponentDisplayName(TopComponent tc) {
        if (tc == null) {
            return null;
        }
        String displayName = tc.getHtmlDisplayName();
        if (displayName == null) {
            displayName = tc.getDisplayName();
        }
        if (displayName == null) {
            displayName = tc.getName();
        }
        return displayName;
    }

    Central getCentral() {
        return this.central;
    }

    public boolean isDragInProgress() {
        return this.central.isDragInProgress();
    }

    public String guessSlideSide(TopComponent tc) {
        return this.central.guessSlideSide(tc);
    }

    public boolean isDocked(TopComponent comp) {
        return this.central.isDocked(comp);
    }

    public void userUndockedTopComponent(TopComponent tc, ModeImpl mode) {
        if (!this.isDocked(tc)) {
            throw new IllegalStateException("TopComponent is already in floating state: " + (Object)tc);
        }
        this.central.userUndockedTopComponent(tc, mode);
    }

    public void userDockedTopComponent(TopComponent tc, ModeImpl mode) {
        if (this.isDocked(tc)) {
            throw new IllegalStateException("TopComponent is already inside main window: " + (Object)tc);
        }
        this.central.userDockedTopComponent(tc, mode);
    }

    public void setTopComponentMinimized(TopComponent tc, boolean minimized) {
        WindowManagerImpl.assertEventDispatchThread();
        this.central.setTopComponentMinimized(tc, minimized);
    }

    public boolean isTopComponentMinimized(TopComponent tc) {
        WindowManagerImpl.assertEventDispatchThread();
        return this.central.isTopComponentMinimized(tc);
    }

    public void setRecentViewList(String[] tcIDs) {
        this.recentViewList.setTopComponents(tcIDs);
    }

    public TopComponent[] getRecentViewList() {
        return this.recentViewList.getTopComponents();
    }

    public String[] getRecentViewIDList() {
        return this.recentViewList.getTopComponentIDs();
    }

    void doFirePropertyChange(final String propName, final Object oldValue, final Object newValue) {
        if (SwingUtilities.isEventDispatchThread()) {
            this.changeSupport.firePropertyChange(propName, oldValue, newValue);
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    WindowManagerImpl.this.changeSupport.firePropertyChange(propName, oldValue, newValue);
                }
            });
        }
    }

    public PersistenceObserver getPersistenceObserver() {
        return PersistenceHandler.getDefault();
    }

    public void notifyTopComponentOpened(TopComponent tc) {
        this.componentOpenNotify(tc);
        WindowManagerImpl.notifyRegistryTopComponentOpened(tc);
    }

    public void notifyTopComponentClosed(TopComponent tc) {
        this.componentCloseNotify(tc);
        WindowManagerImpl.notifyRegistryTopComponentClosed(tc);
    }

    static void notifyRegistryTopComponentActivated(final TopComponent tc) {
        ((RegistryImpl)WindowManagerImpl.getDefault().getRegistry()).topComponentActivated(tc);
        if (SwingUtilities.isEventDispatchThread()) {
            WindowManagerImpl.getInstance().activateComponent(tc);
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    WindowManagerImpl.getInstance().activateComponent(tc);
                }
            });
        }
    }

    private static void notifyRegistryTopComponentOpened(TopComponent tc) {
        ((RegistryImpl)WindowManagerImpl.getDefault().getRegistry()).topComponentOpened(tc);
    }

    private static void notifyRegistryTopComponentClosed(TopComponent tc) {
        ((RegistryImpl)WindowManagerImpl.getDefault().getRegistry()).topComponentClosed(tc);
    }

    private static void notifyRegistrySelectedNodesChanged(TopComponent tc, Node[] nodes) {
        ((RegistryImpl)WindowManagerImpl.getDefault().getRegistry()).selectedNodesChanged(tc, nodes);
    }

    public void componentShowing(TopComponent tc) {
        if (tc != null && tc != this.persistenceShowingTC) {
            super.componentShowing(tc);
        }
    }

    void specialPersistenceCompShow(TopComponent tc) {
        this.componentShowing(tc);
        this.persistenceShowingTC = tc;
    }

    public void componentHidden(TopComponent tc) {
        if (tc != null) {
            super.componentHidden(tc);
            if (tc == this.persistenceShowingTC) {
                this.persistenceShowingTC = null;
            }
        }
    }

    protected void topComponentOpen(TopComponent tc) {
        this.topComponentOpenAtTabPosition(tc, -1);
    }

    protected void topComponentOpenAtTabPosition(TopComponent tc, int position) {
        String tcID;
        WindowManagerImpl.warnIfNotInEDT();
        if (tc == null) {
            throw new IllegalArgumentException("Cannot open a null TopComponent");
        }
        ModeImpl mode = this.getMode(tc);
        if (mode == null) {
            mode = this.getDefaultEditorModeForOpen();
            assert (this.getModes().contains(mode));
            if (tc.getClientProperty((Object)"TopComponentAllowDockAnywhere") == null) {
                tc.putClientProperty((Object)"TopComponentAllowDockAnywhere", (Object)Boolean.TRUE);
            }
        }
        boolean alreadyOpened = mode.getOpenedTopComponents().contains((Object)tc);
        TopComponentTracker.getDefault().add(tc, mode);
        ModeImpl maximizedMode = this.getCurrentMaximizedMode();
        if (maximizedMode != null && mode != maximizedMode && mode.getKind() != 2 && (this.central.isViewMaximized() || mode.getKind() == 1)) {
            this.switchMaximizedMode(null);
        }
        if (position == -1) {
            mode.addOpenedTopComponent(tc);
        } else {
            mode.addOpenedTopComponent(tc, position);
        }
        if (this.central.isEditorMaximized() && !alreadyOpened && mode.getState() != 1 && !this.isTopComponentDockedInMaximizedMode(tcID = this.findTopComponentID(tc)) && mode.getKind() == 0) {
            this.central.slide(tc, mode, this.central.getSlideSideForMode(mode));
            this.topComponentRequestActive(tc);
            return;
        }
        if (mode.isMinimized() && Switches.isTopComponentAutoSlideInMinimizedModeEnabled()) {
            this.central.slide(tc, mode, this.central.getSlideSideForMode(mode));
            this.topComponentRequestActive(tc);
        }
    }

    protected int topComponentGetTabPosition(TopComponent tc) {
        WindowManagerImpl.warnIfNotInEDT();
        ModeImpl mode = this.getModeForOpenedTopComponent(tc);
        if (mode != null) {
            return mode.getTopComponentTabPosition(tc);
        }
        return -1;
    }

    protected void topComponentClose(TopComponent tc) {
        WindowManagerImpl.warnIfNotInEDT();
        boolean opened = this.topComponentIsOpened(tc);
        boolean inCloseAll = tc.getClientProperty((Object)"inCloseAll") != null;
        tc.putClientProperty((Object)"inCloseAll", (Object)null);
        if (!opened) {
            return;
        }
        ModeImpl mode = this.getModeForOpenedTopComponent(tc);
        if (mode != null) {
            if (mode == this.central.getCurrentMaximizedMode() && this.central.isViewMaximized()) {
                this.central.switchMaximizedMode(null);
                this.topComponentClose(tc);
            } else {
                TopComponent recentTc = null;
                if (mode.getKind() == 1 && !inCloseAll) {
                    recentTc = this.central.getRecentTopComponent(mode, tc);
                }
                mode.close(tc);
                if (!tc.isOpened() && null != recentTc) {
                    mode.setSelectedTopComponent(recentTc);
                }
            }
        }
    }

    protected void topComponentRequestActive(TopComponent tc) {
        WindowManagerImpl.warnIfNotInEDT();
        ModeImpl mode = this.getModeForOpenedTopComponent(tc);
        if (mode != null) {
            this.central.activateModeTopComponent(mode, tc);
        }
    }

    protected void topComponentRequestVisible(TopComponent tc) {
        WindowManagerImpl.warnIfNotInEDT();
        ModeImpl mode = this.getModeForOpenedTopComponent(tc);
        if (mode != null) {
            this.central.setModeSelectedTopComponent(mode, tc);
            if (mode.getState() == 1) {
                tc.toFront();
            }
        }
    }

    protected void topComponentDisplayNameChanged(TopComponent tc, String displayName) {
        WindowManagerImpl.warnIfNotInEDT();
        ModeImpl mode = this.getModeForOpenedTopComponent(tc);
        if (mode != null) {
            this.central.topComponentDisplayNameChanged(mode, tc);
        }
    }

    protected void topComponentHtmlDisplayNameChanged(TopComponent tc, String htmlDisplayName) {
        this.topComponentDisplayNameChanged(tc, null);
    }

    protected void topComponentToolTipChanged(TopComponent tc, String toolTip) {
        WindowManagerImpl.warnIfNotInEDT();
        ModeImpl mode = this.getModeForOpenedTopComponent(tc);
        if (mode != null) {
            this.central.topComponentToolTipChanged(mode, tc);
        }
    }

    protected void topComponentIconChanged(TopComponent tc, Image icon) {
        WindowManagerImpl.warnIfNotInEDT();
        ModeImpl mode = this.getModeForOpenedTopComponent(tc);
        if (mode != null) {
            this.central.topComponentIconChanged(mode, tc);
        }
    }

    protected void topComponentActivatedNodesChanged(TopComponent tc, Node[] activatedNodes) {
        WindowManagerImpl.warnIfNotInEDT();
        WindowManagerImpl.notifyRegistrySelectedNodesChanged(tc, activatedNodes);
    }

    protected boolean topComponentIsOpened(TopComponent tc) {
        WindowManagerImpl.warnIfNotInEDT();
        return this.getModeForOpenedTopComponent(tc) != null;
    }

    protected Action[] topComponentDefaultActions(TopComponent tc) {
        WindowManagerImpl.warnIfNotInEDT();
        return ActionUtils.createDefaultPopupActions(tc);
    }

    protected String topComponentID(TopComponent tc, String preferredID) {
        WindowManagerImpl.warnIfNotInEDT();
        if (preferredID == null) {
            Logger.getLogger(WindowManagerImpl.class.getName()).log(Level.WARNING, null, new IllegalStateException("Assertion failed. " + tc.getClass().getName() + ".preferredID method shouldn't be overriden to return null. " + "Please change your impl to return non-null string."));
        }
        return PersistenceManager.getDefault().getGlobalTopComponentID(tc, preferredID);
    }

    public void invokeWhenUIReady(Runnable run) {
        this.getExclusive().register(run);
    }

    public boolean isEditorTopComponent(TopComponent tc) {
        if (null == tc) {
            return false;
        }
        for (ModeImpl mode : this.getModes()) {
            if (mode.getKind() != 1 || !mode.containsTopComponent(tc)) continue;
            return true;
        }
        return false;
    }

    public boolean isOpenedEditorTopComponent(TopComponent tc) {
        if (null == tc) {
            return false;
        }
        for (ModeImpl mode : this.getModes()) {
            if (mode.getKind() != 1 || !mode.getOpenedTopComponents().contains((Object)tc)) continue;
            return true;
        }
        return false;
    }

    public boolean isEditorMode(Mode mode) {
        if (null == mode) {
            return false;
        }
        ModeImpl modeImpl = this.findModeImpl(mode.getName());
        return null != modeImpl && modeImpl.getKind() == 1;
    }

    public void newTabGroup(TopComponent tc) {
        WindowManagerImpl.assertEventDispatchThread();
        this.central.newTabGroup(tc);
    }

    public void collapseTabGroup(ModeImpl mode) {
        WindowManagerImpl.assertEventDispatchThread();
        this.central.collapseTabGroup(mode);
    }

    public final void mainWindowPainted() {
        if (!this.exclusivesCompleted && WindowManagerImpl.getInstance().isVisible()) {
            this.exclusivesCompleted = true;
            this.getExclusive().stop();
            this.getExclusive().register(new Runnable(){

                @Override
                public void run() {
                    FloatingWindowTransparencyManager.getDefault().start();
                }
            });
            SwingUtilities.invokeLater(this.getExclusive());
        }
    }

    private void busyIconWarmUp() {
        this.invokeWhenUIReady(new Runnable(){

            @Override
            public void run() {
                RequestProcessor.getDefault().post(new Runnable(){

                    @Override
                    public void run() {
                        BusyTabsSupport.getDefault().getBusyIcon(false);
                    }
                });
            }

        });
    }

    public void resetModel() {
        this.central.resetModel();
        RegistryImpl rimpl = (RegistryImpl)this.componentRegistry();
        rimpl.clear();
    }

    private ModeImpl getMode(TopComponent tc) {
        return (ModeImpl)this.findMode(tc);
    }

    private ModeImpl getModeForOpenedTopComponent(TopComponent tc) {
        if (tc == null) {
            return null;
        }
        for (ModeImpl mode : this.getModes()) {
            if (!mode.getOpenedTopComponents().contains((Object)tc)) continue;
            return mode;
        }
        return null;
    }

    public ModeImpl getPreviousModeForTopComponent(String tcID, ModeImpl slidingMode) {
        return this.getCentral().getModeTopComponentPreviousMode(tcID, slidingMode);
    }

    public int getPreviousIndexForTopComponent(String tcID, ModeImpl slidingMode) {
        return this.getCentral().getModeTopComponentPreviousIndex(tcID, slidingMode);
    }

    public void setPreviousModeForTopComponent(String tcID, ModeImpl slidingMode, ModeImpl prevMode, int prevIndex) {
        this.getCentral().setModeTopComponentPreviousMode(tcID, slidingMode, prevMode, prevIndex);
    }

    public void setTopComponentDockedInMaximizedMode(String tcID, boolean docked) {
        this.getCentral().setTopComponentDockedInMaximizedMode(tcID, docked);
    }

    public boolean isTopComponentDockedInMaximizedMode(String tcID) {
        return this.getCentral().isTopComponentDockedInMaximizedMode(tcID);
    }

    public void setTopComponentSlidedInDefaultMode(String tcID, boolean slided) {
        this.getCentral().setTopComponentSlidedInDefaultMode(tcID, slided);
    }

    public boolean isTopComponentSlidedInDefaultMode(String tcID) {
        return this.getCentral().isTopComponentSlidedInDefaultMode(tcID);
    }

    public boolean isTopComponentMaximizedWhenSlidedIn(String tcID) {
        return this.getCentral().isTopComponentMaximizedWhenSlidedIn(tcID);
    }

    public void setTopComponentMaximizedWhenSlidedIn(String tcID, boolean maximized) {
        this.getCentral().setTopComponentMaximizedWhenSlidedIn(tcID, maximized);
    }

    public void userToggledTopComponentSlideInMaximize(String tcID) {
        this.getCentral().userToggledTopComponentSlideInMaximize(tcID);
    }

    public static boolean isSeparateWindow(Window w) {
        if (!(w instanceof RootPaneContainer)) {
            return false;
        }
        JRootPane rp = ((RootPaneContainer)((Object)w)).getRootPane();
        if (rp == null) {
            return false;
        }
        return rp.getClientProperty("SeparateWindow") != null;
    }

    static void assertEventDispatchThread() {
        assert (SwingUtilities.isEventDispatchThread());
    }

    static void warnIfNotInEDT() {
        Level level;
        Level level2 = level = assertsEnabled ? Level.WARNING : Level.FINE;
        if (!SwingUtilities.isEventDispatchThread()) {
            StackTraceElement[] elems;
            boolean isJDKProblem = false;
            for (StackTraceElement elem : elems = Thread.currentThread().getStackTrace()) {
                if (!"java.awt.EventDispatchThread".equals(elem.getClassName())) continue;
                isJDKProblem = true;
                break;
            }
            if (!isJDKProblem) {
                Logger.getLogger(WindowManagerImpl.class.getName()).log(level, null, new IllegalStateException("Problem in some module which uses Window System: Window System API is required to be called from AWT thread only, see http://core.netbeans.org/proposals/threading/"));
            } else {
                Logger.getLogger(WindowManagerImpl.class.getName()).log(level, null, new IllegalStateException("Known problem in JDK occurred. If you are interested, vote and report at:\nhttp://bugs.sun.com/view_bug.do?bug_id=6424157, http://bugs.sun.com/view_bug.do?bug_id=6553239 \nAlso see related discussion at http://www.netbeans.org/issues/show_bug.cgi?id=90590"));
            }
        }
    }

    public TopComponent[] getEditorTopComponents() {
        TopComponentTracker tcTracker = TopComponentTracker.getDefault();
        ArrayList<TopComponent> editors = new ArrayList<TopComponent>();
        for (TopComponent tc : this.getRegistry().getOpened()) {
            if (tcTracker.isViewTopComponent(tc)) continue;
            editors.add(tc);
        }
        return editors.toArray((T[])new TopComponent[editors.size()]);
    }

    public TopComponent getArbitrarySelectedEditorTopComponent() {
        TopComponentTracker tcTracker = TopComponentTracker.getDefault();
        Set<? extends ModeImpl> modes = this.getModes();
        for (Mode mode : modes) {
            TopComponent tc;
            ModeImpl modeImpl = this.findModeImpl(mode.getName());
            if (modeImpl.getKind() != 1 || null == (tc = mode.getSelectedTopComponent()) || !tcTracker.isEditorTopComponent(tc)) continue;
            return tc;
        }
        return null;
    }

    public void deselectEditorTopComponents() {
        for (ModeImpl modeImpl : this.getModes()) {
            if (modeImpl.getKind() != 1) continue;
            DummyTopComponent dummy = new DummyTopComponent();
            modeImpl.addOpenedTopComponent(dummy);
            modeImpl.setSelectedTopComponent(dummy);
        }
    }

    public void closeNonEditorViews() {
        TopComponentTracker tcTracker = TopComponentTracker.getDefault();
        for (ModeImpl modeImpl : this.getModes()) {
            if (null == modeImpl) continue;
            List<TopComponent> tcs = modeImpl.getOpenedTopComponents();
            for (TopComponent tc : tcs) {
                if (!tcTracker.isViewTopComponent(tc)) continue;
                tc.close();
            }
        }
    }

    public TopComponent[] getOpenedTopComponents(Mode mode) {
        if (mode instanceof ModeImpl) {
            List<TopComponent> openedTcs = ((ModeImpl)mode).getOpenedTopComponents();
            return openedTcs.toArray((T[])new TopComponent[openedTcs.size()]);
        }
        return super.getOpenedTopComponents(mode);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addWindowSystemListener(WindowSystemListener listener) {
        Collection<WindowSystemListener> collection = this.listeners;
        synchronized (collection) {
            this.listeners.add(listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeWindowSystemListener(WindowSystemListener listener) {
        Collection<WindowSystemListener> collection = this.listeners;
        synchronized (collection) {
            this.listeners.remove((Object)listener);
        }
    }

    public String getRole() {
        return this.currentRole;
    }

    public void setRole(String roleName) {
        this.setRole(roleName, false);
    }

    public void setRole(String roleName, boolean keepDocumentWindows) {
        if (null != roleName && roleName.isEmpty()) {
            throw new IllegalArgumentException("Role name cannot be empty.");
        }
        if (!PersistenceHandler.getDefault().isLoaded()) {
            this.currentRole = roleName;
            PersistenceManager.getDefault().setRole(this.currentRole);
        } else {
            if (this.currentRole == null ? roleName == null : this.currentRole.equals(roleName)) {
                return;
            }
            this.switchRole(roleName, keepDocumentWindows);
        }
    }

    boolean switchRole(String newRole, boolean keepDocumentWindows) {
        WindowSystemImpl ws = (WindowSystemImpl)Lookup.getDefault().lookup(WindowSystemImpl.class);
        assert (null != ws);
        PersistenceManager pm = PersistenceManager.getDefault();
        PersistenceHandler.getDefault().finishLazyLoading();
        MainWindow.getInstance().setFullScreenMode(false);
        TopComponent[] editors = this.getEditorTopComponents();
        if (!keepDocumentWindows) {
            for (TopComponent tc : editors) {
                if (tc.canClose()) continue;
                return false;
            }
        }
        TopComponent prevActiveEditor = this.getArbitrarySelectedEditorTopComponent();
        HashSet openedBefore = new HashSet(this.getRegistry().getOpened());
        boolean hideAndShowWhileSwitching = Switches.isShowAndHideMainWindowWhileSwitchingRole();
        if (hideAndShowWhileSwitching) {
            ws.hide();
        } else {
            this.getMainWindow().setCursor(Cursor.getPredefinedCursor(3));
        }
        ws.save();
        this.deselectEditorTopComponents();
        this.resetModel();
        pm.reset();
        PersistenceHandler.getDefault().clear();
        this.currentRole = newRole;
        pm.setRole(newRole);
        ws.load();
        if (keepDocumentWindows) {
            ModeImpl editorMode = (ModeImpl)this.findMode("editor");
            for (int i = 0; i < editors.length && null != editorMode; ++i) {
                TopComponent editor = editors[i];
                if (editor.getPersistenceType() == 2) continue;
                ModeImpl mode = (ModeImpl)this.findMode(editor);
                if (null == mode) {
                    mode = editorMode;
                }
                if (null == mode) continue;
                String tcId = this.findTopComponentID(editor);
                if (!(mode.getOpenedTopComponents().contains((Object)editor) || null != tcId && mode.getOpenedTopComponentsIDs().contains(tcId))) {
                    mode.addOpenedTopComponentNoNotify(editor);
                }
                openedBefore.remove((Object)editor);
            }
        }
        Set openedAfter = this.getRegistry().getOpened();
        openedBefore.removeAll(openedAfter);
        for (TopComponent tc : openedBefore) {
            this.componentCloseNotify(tc);
        }
        if (hideAndShowWhileSwitching) {
            ws.show();
        } else {
            this.getMainWindow().setCursor(null);
        }
        TopComponent editorToActivate = prevActiveEditor;
        if (null != editorToActivate && !editorToActivate.isOpened()) {
            editorToActivate = this.getArbitrarySelectedEditorTopComponent();
        }
        if (null != editorToActivate) {
            editorToActivate.requestActive();
        }
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                Frame mainWindow = WindowManagerImpl.this.getMainWindow();
                mainWindow.invalidate();
                mainWindow.repaint();
            }
        });
        return true;
    }

    public void userStartedKeyboardDragAndDrop(TopComponentDraggable draggable) {
        this.central.userStartedKeyboardDragAndDrop(draggable);
    }

    protected void topComponentMakeBusy(TopComponent tc, boolean busy) {
        ModeImpl mode;
        boolean wasBusy = this.isTopComponentBusy(tc);
        tc.putClientProperty((Object)"nbwinsys.tc.isbusy", busy ? BUSY_FLAG : null);
        if (busy != wasBusy && null != (mode = (ModeImpl)this.findMode(tc))) {
            this.central.topComponentMakeBusy(mode, tc, busy);
        }
    }

    public boolean isTopComponentBusy(TopComponent tc) {
        return tc.getClientProperty((Object)"nbwinsys.tc.isbusy") == BUSY_FLAG;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void fireEvent(WindowSystemEventType type) {
        WindowManagerImpl.assertEventDispatchThread();
        ArrayList<WindowSystemListener> copy = null;
        Collection<WindowSystemListener> collection = this.listeners;
        synchronized (collection) {
            copy = new ArrayList<WindowSystemListener>(this.listeners);
        }
        WindowSystemEvent e = new WindowSystemEvent((Object)this);
        for (WindowSystemListener listener : copy) {
            switch (type) {
                case beforeLoad: {
                    listener.beforeLoad(e);
                    break;
                }
                case beforeSave: {
                    listener.beforeSave(e);
                    break;
                }
                case afterLoad: {
                    listener.afterLoad(e);
                    break;
                }
                case afterSave: {
                    listener.afterSave(e);
                }
            }
        }
    }

    public boolean isHeavyWeightShowing() {
        TopComponent.Registry registry = TopComponent.getRegistry();
        HashSet opened = new HashSet(registry.getOpened());
        for (TopComponent tc : opened) {
            if (!tc.isShowing() || !this.isHeavyWeight((Component)tc)) continue;
            return true;
        }
        return false;
    }

    private boolean isHeavyWeight(Component c) {
        if (null != c && !c.isLightweight()) {
            return true;
        }
        if (c instanceof Container) {
            for (Component child : ((Container)c).getComponents()) {
                if (!this.isHeavyWeight(child)) continue;
                return true;
            }
        }
        return false;
    }

    public boolean isTopComponentFloating(TopComponent tc) {
        WindowManagerImpl.assertEventDispatchThread();
        return !this.isDocked(tc);
    }

    public void setTopComponentFloating(TopComponent tc, boolean floating) {
        boolean isFloating;
        WindowManagerImpl.assertEventDispatchThread();
        boolean bl = isFloating = !this.isDocked(tc);
        if (isFloating == floating) {
            return;
        }
        ModeImpl mode = (ModeImpl)this.findMode(tc);
        if (null == mode) {
            throw new IllegalStateException("Cannot find Mode for TopComponent: " + (Object)tc);
        }
        if (floating) {
            this.userUndockedTopComponent(tc, mode);
        } else {
            this.userDockedTopComponent(tc, mode);
        }
    }

    static {
        assertsEnabled = false;
        if (!$assertionsDisabled) {
            assertsEnabled = true;
            if (!true) {
                throw new AssertionError();
            }
        }
        BUSY_FLAG = new Object();
    }

    private static class DummyTopComponent
    extends TopComponent {
        private DummyTopComponent() {
        }

        protected String preferredID() {
            return "temp";
        }

        public int getPersistenceType() {
            return 2;
        }
    }

    private static final class Exclusive
    implements Runnable,
    ActionListener {
        private ArrayList<Runnable> arr = new ArrayList();
        private Timer paintedTimer;

        public Exclusive() {
            this.paintedTimer = new Timer(5000, this);
            this.paintedTimer.setRepeats(false);
        }

        final void stop() {
            this.paintedTimer.stop();
        }

        final void restart() {
            this.paintedTimer.restart();
        }

        public synchronized void register(Runnable r) {
            this.arr.add(r);
            SwingUtilities.invokeLater(this);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            if (!WindowManagerImpl.getInstance().isVisible()) {
                return;
            }
            Exclusive exclusive = this;
            synchronized (exclusive) {
                if (this.arr.isEmpty()) {
                    return;
                }
                final Runnable toRun = this.arr.remove(0);
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        Logger perf = Logger.getLogger("org.netbeans.log.startup");
                        try {
                            perf.log(Level.FINE, "start", "invokeWhenUIReady: " + toRun.getClass().getName());
                            toRun.run();
                            perf.log(Level.FINE, "end", "invokeWhenUIReady: " + toRun.getClass().getName());
                        }
                        catch (RuntimeException ex) {
                            Logger.getLogger(WindowManagerImpl.class.getName()).log(Level.WARNING, null, ex);
                        }
                        SwingUtilities.invokeLater(Exclusive.this);
                    }
                });
            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Logger.getLogger(WindowManagerImpl.class.getName()).log(Level.FINE, "Painted timer action invoked, which probably means that MainWindow.paint was not called!");
            WindowManagerImpl.getInstance().mainWindowPainted();
        }

    }

    private static class WrapMode
    implements Mode {
        private Mode wrap;

        public WrapMode(Mode wrap) {
            this.wrap = wrap;
        }

        public void addPropertyChangeListener(PropertyChangeListener list) {
            this.wrap.addPropertyChangeListener(list);
        }

        public boolean canDock(TopComponent tc) {
            return this.wrap.canDock(tc);
        }

        public boolean dockInto(TopComponent c) {
            if (c.getClientProperty((Object)"TopComponentAllowDockAnywhere") == null) {
                c.putClientProperty((Object)"TopComponentAllowDockAnywhere", (Object)Boolean.TRUE);
            }
            return this.wrap.dockInto(c);
        }

        public Rectangle getBounds() {
            return this.wrap.getBounds();
        }

        public String getDisplayName() {
            return this.wrap.getDisplayName();
        }

        public Image getIcon() {
            return this.wrap.getIcon();
        }

        public String getName() {
            return this.wrap.getName();
        }

        public TopComponent getSelectedTopComponent() {
            return this.wrap.getSelectedTopComponent();
        }

        public TopComponent[] getTopComponents() {
            return this.wrap.getTopComponents();
        }

        public Workspace getWorkspace() {
            return this.wrap.getWorkspace();
        }

        public void removePropertyChangeListener(PropertyChangeListener list) {
            this.wrap.removePropertyChangeListener(list);
        }

        public void setBounds(Rectangle s) {
            this.wrap.setBounds(s);
        }
    }

}

