/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows;

import java.util.MissingResourceException;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

public final class Switches {
    private static final String PROPERTY_PREFIX = "NB.WinSys.";

    public static boolean isTopComponentDragAndDropEnabled() {
        return Switches.getSwitchValue("TopComponent.DragAndDrop.Enabled", true);
    }

    public static boolean isViewModeDragAndDropEnabled() {
        return Switches.getSwitchValue("Mode.View.DragAndDrop.Enabled", true);
    }

    public static boolean isEditorModeDragAndDropEnabled() {
        return Switches.getSwitchValue("Mode.Editor.DragAndDrop.Enabled", true);
    }

    public static boolean isDraggingEnabled(TopComponent tc) {
        return !Boolean.TRUE.equals(tc.getClientProperty((Object)"netbeans.winsys.tc.dragging_disabled"));
    }

    public static boolean isTopComponentUndockingEnabled() {
        return Switches.getSwitchValue("TopComponent.Undocking.Enabled", true);
    }

    public static boolean isViewModeUndockingEnabled() {
        return Switches.getSwitchValue("Mode.View.Undocking.Enabled", true);
    }

    public static boolean isEditorModeUndockingEnabled() {
        return Switches.getSwitchValue("Mode.Editor.Undocking.Enabled", true);
    }

    public static boolean isUndockingEnabled(TopComponent tc) {
        return !Boolean.TRUE.equals(tc.getClientProperty((Object)"netbeans.winsys.tc.undocking_disabled"));
    }

    public static boolean isTopComponentSlidingEnabled() {
        return Switches.getSwitchValue("TopComponent.Sliding.Enabled", true);
    }

    public static boolean isModeSlidingEnabled() {
        return Switches.getSwitchValue("Mode.Sliding.Enabled", true);
    }

    public static boolean isSlidingEnabled(TopComponent tc) {
        return !Boolean.TRUE.equals(tc.getClientProperty((Object)"netbeans.winsys.tc.sliding_disabled"));
    }

    public static boolean isTopComponentResizingEnabled() {
        return Switches.getSwitchValue("TopComponent.Resizing.Enabled", true);
    }

    public static boolean isViewTopComponentClosingEnabled() {
        return Switches.getSwitchValue("View.TopComponent.Closing.Enabled", true);
    }

    public static boolean isEditorTopComponentClosingEnabled() {
        return Switches.getSwitchValue("Editor.TopComponent.Closing.Enabled", true);
    }

    public static boolean isTopComponentAutoSlideInMinimizedModeEnabled() {
        return Switches.getSwitchValue("TopComponent.Auto.Slide.In.Minimized.Mode.Enabled", true);
    }

    public static boolean isModeClosingEnabled() {
        return Switches.getSwitchValue("Mode.Closing.Enabled", true);
    }

    public static boolean isClosingEnabled(TopComponent tc) {
        return !Boolean.TRUE.equals(tc.getClientProperty((Object)"netbeans.winsys.tc.closing_disabled"));
    }

    public static boolean isTopComponentMaximizationEnabled() {
        return Switches.getSwitchValue("TopComponent.Maximization.Enabled", true);
    }

    public static boolean isMaximizationEnabled(TopComponent tc) {
        return !Boolean.TRUE.equals(tc.getClientProperty((Object)"netbeans.winsys.tc.maximization_disabled"));
    }

    public static boolean isSplitterRespectMinimumSizeEnabled() {
        return Switches.getSwitchValue("Splitter.Respect.MinimumSize.Enabled", true);
    }

    public static boolean isMixingOfEditorsAndViewsEnabled() {
        return Switches.getSwitchValue("Mix.Editors.And.Views.Enabled", true);
    }

    public static boolean isShowAndHideMainWindowWhileSwitchingRole() {
        return Switches.getSwitchValue("WinSys.Show.Hide.MainWindow.While.Switching.Role", true);
    }

    public static boolean isOpenNewEditorsDocked() {
        return Switches.getSwitchValue("WinSys.Open.New.Editors.Docked", false);
    }

    public static boolean isDragAndDropSlidingEnabled() {
        return Switches.getSwitchValue("WinSys.DragAndDrop.Sliding.Enabled", false);
    }

    public static boolean isUseSimpleTabs() {
        return Switches.getSwitchValue("WinSys.TabControl.SimpleTabs.Enabled", false);
    }

    public static int getSimpleTabsPlacement() {
        int result = 1;
        try {
            String resValue = NbBundle.getMessage(Switches.class, (String)"WinSys.TabControl.SimpleTabs.Placement");
            if ("bottom".equals(resValue)) {
                result = 3;
            } else if ("right".equals(resValue)) {
                result = 4;
            } else if ("left".equals(resValue)) {
                result = 2;
            }
        }
        catch (MissingResourceException mrE) {
            // empty catch block
        }
        return result;
    }

    public static boolean isSimpleTabsMultiRow() {
        return Switches.getSwitchValue("WinSys.TabControl.SimpleTabs.MultiRow", false);
    }

    public static boolean isCtrlTabWindowSwitchingInJTableEnabled() {
        return Switches.getSwitchValue("WinSys.CtrlTabSwitching.In.JTable.Enabled", true);
    }

    private static boolean getSwitchValue(String switchName, boolean defaultValue) {
        String propValue = System.getProperty("NB.WinSys." + switchName);
        if (null != propValue) {
            return "true".equals(propValue);
        }
        boolean result = defaultValue;
        try {
            String resValue = NbBundle.getMessage(Switches.class, (String)switchName);
            result = "true".equals(resValue.toLowerCase());
        }
        catch (MissingResourceException mrE) {
            // empty catch block
        }
        return result;
    }

    private Switches() {
    }
}

