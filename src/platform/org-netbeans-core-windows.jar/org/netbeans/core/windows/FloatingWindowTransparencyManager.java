/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows;

import java.awt.Component;
import java.awt.Window;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.prefs.Preferences;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.nativeaccess.NativeWindowSystem;
import org.netbeans.core.windows.options.WinSysPrefs;
import org.openide.util.RequestProcessor;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public class FloatingWindowTransparencyManager {
    private static FloatingWindowTransparencyManager theInstance;
    private static final RequestProcessor RP;
    private PropertyChangeListener topComponentRegistryListener;
    private final AtomicBoolean initialized = new AtomicBoolean(false);
    private final Object LOCK = new Object();

    private FloatingWindowTransparencyManager() {
    }

    public static synchronized FloatingWindowTransparencyManager getDefault() {
        if (null == theInstance) {
            theInstance = new FloatingWindowTransparencyManager();
        }
        return theInstance;
    }

    public void start() {
        new Thread(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                Object object = FloatingWindowTransparencyManager.this.LOCK;
                synchronized (object) {
                    FloatingWindowTransparencyManager.this.initialized.set(true);
                    if (!NativeWindowSystem.getDefault().isWindowAlphaSupported()) {
                        return;
                    }
                    if (null == FloatingWindowTransparencyManager.this.topComponentRegistryListener) {
                        FloatingWindowTransparencyManager.this.topComponentRegistryListener = new PropertyChangeListener(){

                            @Override
                            public void propertyChange(PropertyChangeEvent evt) {
                                SwingUtilities.invokeLater(new Runnable(){

                                    @Override
                                    public void run() {
                                        FloatingWindowTransparencyManager.this.toggleFloatingWindowTransparency();
                                    }
                                });
                            }

                        };
                        TopComponent.getRegistry().addPropertyChangeListener(FloatingWindowTransparencyManager.this.topComponentRegistryListener);
                    }
                }
            }

        }).start();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void stop() {
        Object object = this.LOCK;
        synchronized (object) {
            if (null != this.topComponentRegistryListener) {
                TopComponent.getRegistry().removePropertyChangeListener(this.topComponentRegistryListener);
                this.topComponentRegistryListener = null;
            }
        }
    }

    public void update() {
        this.toggleFloatingWindowTransparency();
    }

    protected void toggleFloatingWindowTransparency() {
        if (!this.initialized.get()) {
            return;
        }
        if (!NativeWindowSystem.getDefault().isWindowAlphaSupported()) {
            return;
        }
        if (WinSysPrefs.HANDLER.getBoolean("transparency.floating", false)) {
            TopComponent currentActive = TopComponent.getRegistry().getActivated();
            if (null != currentActive) {
                Window w;
                final WindowManagerImpl wm = WindowManagerImpl.getInstance();
                ModeImpl currentActiveMode = (ModeImpl)wm.findMode(currentActive);
                if (null != currentActiveMode && currentActiveMode.getState() == 1 && currentActiveMode.getKind() != 1 && null != (w = SwingUtilities.windowForComponent((Component)currentActive))) {
                    NativeWindowSystem.getDefault().setWindowAlpha(w, 1.0f);
                }
                Runnable runnable = new Runnable(){

                    @Override
                    public void run() {
                        if (!SwingUtilities.isEventDispatchThread()) {
                            SwingUtilities.invokeLater(this);
                            return;
                        }
                        TopComponent activeTc = TopComponent.getRegistry().getActivated();
                        if (null == activeTc) {
                            return;
                        }
                        ModeImpl activeMode = (ModeImpl)wm.findMode(activeTc);
                        FloatingWindowTransparencyManager.this.makeFloatingWindowsTransparent(activeMode);
                    }
                };
                RP.post(runnable, WinSysPrefs.HANDLER.getInt("transparency.floating.timeout", 1000));
            }
        } else {
            this.turnTransparencyOff();
        }
    }

    private void turnTransparencyOff() {
        NativeWindowSystem nws = NativeWindowSystem.getDefault();
        for (ModeImpl m : WindowManagerImpl.getInstance().getModes()) {
            Window w;
            TopComponent tc;
            if (m.getState() != 1 || m.getKind() == 1 || null == (tc = m.getSelectedTopComponent()) || null == (w = SwingUtilities.windowForComponent((Component)tc))) continue;
            nws.setWindowAlpha(w, 1.0f);
        }
    }

    private void makeFloatingWindowsTransparent(ModeImpl activeMode) {
        float alpha = WinSysPrefs.HANDLER.getFloat("transparency.floating.alpha", 0.5f);
        NativeWindowSystem nws = NativeWindowSystem.getDefault();
        for (ModeImpl m : WindowManagerImpl.getInstance().getModes()) {
            Window w;
            TopComponent tc;
            if (m.getState() != 1 || m.equals(activeMode) || m.getKind() == 1 || null == (tc = m.getSelectedTopComponent()) || null == (w = SwingUtilities.windowForComponent((Component)tc))) continue;
            nws.setWindowAlpha(w, alpha);
        }
    }

    static {
        RP = new RequestProcessor("FloatingWindowTransparencyManager");
    }

}

