/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.FloatingWindowTransparencyManager;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.PersistenceHandler;
import org.netbeans.core.windows.SplitConstraint;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.TopComponentGroupImpl;
import org.netbeans.core.windows.TopComponentTracker;
import org.netbeans.core.windows.ViewRequest;
import org.netbeans.core.windows.ViewRequestor;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.WindowSystemSnapshot;
import org.netbeans.core.windows.model.DockingStatus;
import org.netbeans.core.windows.model.Model;
import org.netbeans.core.windows.model.ModelElement;
import org.netbeans.core.windows.model.ModelFactory;
import org.netbeans.core.windows.options.WinSysPrefs;
import org.netbeans.core.windows.view.ControllerHandler;
import org.netbeans.core.windows.view.dnd.TopComponentDraggable;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

final class Central
implements ControllerHandler {
    private final Model model = ModelFactory.createWindowSystemModel();
    private final ViewRequestor viewRequestor;
    private ModeImpl modeBeingMaximized;
    private static final Object GROUP_SELECTED = new Object();

    public Central() {
        this.viewRequestor = new ViewRequestor(this);
        this.modeBeingMaximized = null;
    }

    public void topComponentRequestAttention(ModeImpl mode, TopComponent tc) {
        String modeName = this.getModeName(mode);
        this.viewRequestor.scheduleRequest(new ViewRequest(modeName, 63, (Object)tc, (Object)tc));
    }

    public void topComponentCancelRequestAttention(ModeImpl mode, TopComponent tc) {
        String modeName = this.getModeName(mode);
        this.viewRequestor.scheduleRequest(new ViewRequest(modeName, 64, (Object)tc, (Object)tc));
    }

    public void topComponentAttentionHighlight(ModeImpl mode, TopComponent tc, boolean highlight) {
        String modeName = this.getModeName(mode);
        this.viewRequestor.scheduleRequest(new ViewRequest(modeName, highlight ? 65 : 66, (Object)tc, (Object)tc));
    }

    public void setVisible(boolean visible) {
        if (this.isVisible() == visible) {
            return;
        }
        this.model.setVisible(visible);
        this.viewRequestor.scheduleRequest(new ViewRequest(null, 0, null, visible));
    }

    public void setMainWindowBoundsJoined(Rectangle mainWindowBoundsJoined) {
        if (mainWindowBoundsJoined == null) {
            return;
        }
        Rectangle old = this.getMainWindowBoundsJoined();
        if (old.equals(mainWindowBoundsJoined)) {
            return;
        }
        this.model.setMainWindowBoundsJoined(mainWindowBoundsJoined);
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(null, 1, old, mainWindowBoundsJoined));
        }
    }

    public void setMainWindowBoundsSeparated(Rectangle mainWindowBoundsSeparated) {
        if (mainWindowBoundsSeparated == null) {
            return;
        }
        Rectangle old = this.getMainWindowBoundsSeparated();
        if (old.equals(mainWindowBoundsSeparated)) {
            return;
        }
        this.model.setMainWindowBoundsSeparated(mainWindowBoundsSeparated);
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(null, 2, old, mainWindowBoundsSeparated));
        }
    }

    public void setMainWindowFrameStateJoined(int frameState) {
        int old = this.getMainWindowFrameStateJoined();
        if (old == frameState) {
            return;
        }
        this.model.setMainWindowFrameStateJoined(frameState);
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(null, 3, old, frameState));
        }
    }

    public void setMainWindowFrameStateSeparated(int frameState) {
        int old = this.getMainWindowFrameStateSeparated();
        if (old == frameState) {
            return;
        }
        this.model.setMainWindowFrameStateSeparated(frameState);
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(null, 4, old, frameState));
        }
    }

    public void setActiveMode(ModeImpl activeMode) {
        List<TopComponent> l;
        if (activeMode != null && (l = activeMode.getOpenedTopComponents()).isEmpty()) {
            return;
        }
        ModeImpl old = this.getActiveMode();
        if (activeMode == old) {
            boolean top;
            ModeImpl impl = this.model.getSlidingMode("bottom");
            boolean bottom = impl == null || impl.getSelectedTopComponent() == null;
            impl = this.model.getSlidingMode("left");
            boolean left = impl == null || impl.getSelectedTopComponent() == null;
            impl = this.model.getSlidingMode("right");
            boolean right = impl == null || impl.getSelectedTopComponent() == null;
            impl = this.model.getSlidingMode("top");
            boolean bl = top = impl == null || impl.getSelectedTopComponent() == null;
            if (bottom && left && right && top) {
                return;
            }
        }
        this.model.setActiveMode(activeMode);
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(null, 9, old, activeMode));
        }
        WindowManagerImpl.getInstance().doFirePropertyChange("activeMode", old, activeMode);
        if (activeMode != null) {
            WindowManagerImpl.notifyRegistryTopComponentActivated(activeMode.getSelectedTopComponent());
        } else {
            WindowManagerImpl.notifyRegistryTopComponentActivated(null);
        }
    }

    public void setEditorAreaBounds(Rectangle editorAreaBounds) {
        if (editorAreaBounds == null) {
            return;
        }
        Rectangle old = this.getEditorAreaBounds();
        if (old.equals(editorAreaBounds)) {
            return;
        }
        this.model.setEditorAreaBounds(editorAreaBounds);
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(null, 7, old, editorAreaBounds));
        }
    }

    public void setEditorAreaConstraints(SplitConstraint[] editorAreaConstraints) {
        Object[] old = this.getEditorAreaConstraints();
        if (Arrays.equals(old, editorAreaConstraints)) {
            return;
        }
        this.model.setEditorAreaConstraints(editorAreaConstraints);
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(null, 8, old, editorAreaConstraints));
        }
    }

    public void setEditorAreaState(int editorAreaState) {
        int old = this.getEditorAreaState();
        if (editorAreaState == old) {
            return;
        }
        int requiredState = editorAreaState == 0 ? 0 : 1;
        for (ModeImpl mode : this.getModes()) {
            if (mode.getKind() == 0 && mode.getState() != requiredState) {
                Rectangle bounds;
                this.model.setModeState(mode, requiredState);
                if (editorAreaState == 1 && (bounds = this.model.getModeBounds(mode)).isEmpty()) {
                    this.model.setModeBounds(mode, this.model.getModeBoundsSeparatedHelp(mode));
                }
            }
            if (mode.getKind() != 2 || editorAreaState != 1) continue;
            TopComponent[] tcs = mode.getTopComponents();
            for (int i = 0; i < tcs.length; ++i) {
                String tcID = WindowManagerImpl.getInstance().findTopComponentID(tcs[i]);
                ModeImpl targetMode = this.model.getModeTopComponentPreviousMode(mode, tcID);
                if (targetMode == null || !this.model.getModes().contains(targetMode)) {
                    SplitConstraint[] constraints = this.model.getModeTopComponentPreviousConstraints(mode, tcID);
                    constraints = constraints == null ? new SplitConstraint[]{} : constraints;
                    targetMode = WindowManagerImpl.getInstance().createModeImpl(ModeImpl.getUnusedModeName(), 0, false);
                    this.model.setModeState(targetMode, requiredState);
                    this.model.addMode(targetMode, constraints);
                }
                this.moveTopComponentIntoMode(targetMode, tcs[i]);
            }
        }
        if (editorAreaState == 1) {
            Rectangle mainWindowBoundsSeparated;
            Rectangle editorAreaBounds = this.model.getEditorAreaBounds();
            if (editorAreaBounds.isEmpty()) {
                this.model.setEditorAreaBounds(this.model.getEditorAreaBoundsHelp());
            }
            if ((mainWindowBoundsSeparated = this.model.getMainWindowBoundsSeparated()).isEmpty()) {
                this.model.setMainWindowBoundsSeparated(this.model.getMainWindowBoundsSeparatedHelp());
            }
        }
        this.model.setEditorAreaState(editorAreaState);
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(null, 5, old, editorAreaState));
        }
        WindowManagerImpl.getInstance().doFirePropertyChange("editorAreaState", old, editorAreaState);
    }

    public void setEditorAreaFrameState(int frameState) {
        int old = this.getEditorAreaFrameState();
        if (old == frameState) {
            return;
        }
        this.model.setEditorAreaFrameState(frameState);
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(null, 6, old, frameState));
        }
    }

    void switchMaximizedMode(ModeImpl newMaximizedMode) {
        ModeImpl old = this.getCurrentMaximizedMode();
        if (newMaximizedMode == old) {
            return;
        }
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        if (null == newMaximizedMode) {
            if (this.isViewMaximized()) {
                if (this.isEditorMaximized()) {
                    this.restoreViews(this.model.getMaximizedDockingStatus());
                } else {
                    this.restoreViews(this.model.getDefaultDockingStatus());
                }
                ModeImpl currentMaximizedMode = this.getViewMaximizedMode();
                if (currentMaximizedMode.getTopComponents().length == 1) {
                    TopComponent maximizedTC = currentMaximizedMode.getTopComponents()[0];
                    String tcID = wm.findTopComponentID(maximizedTC);
                    ModeImpl prevMode = this.getModeTopComponentPreviousMode(tcID, currentMaximizedMode);
                    int prevIndex = this.model.getModeTopComponentPreviousIndex(currentMaximizedMode, tcID);
                    if (null == prevMode) {
                        SplitConstraint[] constraints;
                        if (!(prevMode != null && this.model.getModes().contains(prevMode) || (constraints = this.model.getModeTopComponentPreviousConstraints(currentMaximizedMode, tcID)) == null)) {
                            prevMode = WindowManagerImpl.getInstance().createModeImpl(ModeImpl.getUnusedModeName(), 0, false);
                            this.model.addMode(prevMode, constraints);
                        }
                        if (prevMode == null) {
                            prevMode = WindowManagerImpl.getInstance().getDefaultViewMode();
                        }
                    }
                    prevMode.addOpenedTopComponent(maximizedTC, prevIndex);
                    prevMode.setSelectedTopComponent(maximizedTC);
                    this.setActiveMode(prevMode);
                    this.model.removeMode(currentMaximizedMode);
                } else {
                    Logger.getLogger(Central.class.getName()).log(Level.WARNING, "A 'view' mode is maximized but it has wrong number of TopComponents, Mode=[" + currentMaximizedMode.getName() + "], TC count=" + currentMaximizedMode.getTopComponents().length);
                }
                this.setViewMaximizedMode(null);
            } else if (this.isEditorMaximized()) {
                this.model.getMaximizedDockingStatus().mark();
                ModeImpl prevActiveMode = this.getActiveMode();
                this.restoreViews(this.model.getDefaultDockingStatus());
                this.setEditorMaximizedMode(null);
                this.setActiveMode(prevActiveMode);
            }
        } else {
            assert (!this.isViewMaximized());
            if (newMaximizedMode.getKind() == 1) {
                this.model.getDefaultDockingStatus().mark();
                this.restoreViews(this.model.getMaximizedDockingStatus());
                this.setEditorMaximizedMode(newMaximizedMode);
            } else if (newMaximizedMode.getKind() == 0) {
                TopComponent tcToMaximize = newMaximizedMode.getSelectedTopComponent();
                if (null == tcToMaximize) {
                    if (newMaximizedMode.getOpenedTopComponents().isEmpty()) {
                        return;
                    }
                    tcToMaximize = newMaximizedMode.getOpenedTopComponents().get(0);
                }
                if (this.isEditorMaximized()) {
                    this.model.getMaximizedDockingStatus().mark();
                } else {
                    this.model.getDefaultDockingStatus().mark();
                }
                this.modeBeingMaximized = newMaximizedMode;
                String tcID = wm.findTopComponentID(tcToMaximize);
                int prevIndex = newMaximizedMode.getOpenedTopComponents().indexOf((Object)tcToMaximize);
                ModeImpl mode = WindowManagerImpl.getInstance().createModeImpl(ModeImpl.getUnusedModeName(), 0, true);
                this.model.addMode(mode, new SplitConstraint[0]);
                mode.addOpenedTopComponent(tcToMaximize);
                mode.setSelectedTopComponent(tcToMaximize);
                this.setModeTopComponentPreviousMode(tcID, mode, newMaximizedMode, prevIndex);
                this.setViewMaximizedMode(mode);
                this.slideAllViews();
                this.setActiveMode(mode);
                this.modeBeingMaximized = null;
            } else {
                throw new IllegalArgumentException("Cannot maximize a sliding view");
            }
        }
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(null, 11, old, this.getCurrentMaximizedMode()));
        }
        WindowManagerImpl.getInstance().doFirePropertyChange("maximizedMode", old, this.getCurrentMaximizedMode());
    }

    void setEditorMaximizedMode(ModeImpl editorMaximizedMode) {
        this.model.setEditorMaximizedMode(editorMaximizedMode);
    }

    void setViewMaximizedMode(ModeImpl viewMaximizedMode) {
        this.model.setViewMaximizedMode(viewMaximizedMode);
    }

    public void setModeConstraints(ModeImpl mode, SplitConstraint[] modeConstraints) {
        Object[] old = this.getModeConstraints(mode);
        if (Arrays.equals(modeConstraints, old)) {
            return;
        }
        this.model.setModeConstraints(mode, modeConstraints);
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(null, 14, old, modeConstraints));
        }
    }

    public void addMode(ModeImpl mode, SplitConstraint[] modeConstraints) {
        SplitConstraint[] old = this.getModeConstraints(mode);
        if (modeConstraints == old) {
            return;
        }
        this.model.addMode(mode, modeConstraints);
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(null, 12, null, mode));
        }
        WindowManagerImpl.getInstance().doFirePropertyChange("modes", null, null);
    }

    final void setModeName(ModeImpl mode, String text) {
        this.model.setModeName(mode, text);
    }

    public void removeMode(ModeImpl mode) {
        if (!this.getModes().contains(mode)) {
            return;
        }
        this.model.removeMode(mode);
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(null, 13, null, mode));
        }
        WindowManagerImpl.getInstance().doFirePropertyChange("modes", null, null);
    }

    public void setToolbarConfigName(String toolbarConfigName) {
        String old = this.getToolbarConfigName();
        if (old.equals(toolbarConfigName)) {
            return;
        }
        this.model.setToolbarConfigName(toolbarConfigName);
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(null, 10, old, toolbarConfigName));
        }
    }

    public void updateUI() {
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(null, 61, null, null));
        }
    }

    private void closeMode(ModeImpl mode) {
        if (mode == null) {
            return;
        }
        TopComponent[] tcs = this.getModeOpenedTopComponents(mode).toArray((T[])new TopComponent[0]);
        for (int i = 0; i < tcs.length; ++i) {
            if (PersistenceHandler.isTopComponentPersistentWhenClosed(tcs[i])) {
                this.model.addModeClosedTopComponent(mode, tcs[i]);
                continue;
            }
            if (Boolean.TRUE.equals(tcs[i].getClientProperty((Object)"KeepNonPersistentTCInModelWhenClosed"))) {
                this.model.addModeClosedTopComponent(mode, tcs[i]);
                continue;
            }
            this.model.removeModeTopComponent(mode, tcs[i], null);
        }
        ModeImpl oldActive = this.getActiveMode();
        ModeImpl newActive = mode == oldActive ? this.setSomeModeActive() : oldActive;
        boolean modeRemoved = false;
        if (!mode.isPermanent() && this.model.getModeTopComponents(mode).isEmpty() && this.doCheckSlidingModes(mode)) {
            this.model.removeMode(mode);
            modeRemoved = true;
        }
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(mode, 45, null, null));
        }
        for (int i2 = 0; i2 < tcs.length; ++i2) {
            WindowManagerImpl.getInstance().notifyTopComponentClosed(tcs[i2]);
        }
        if (oldActive != newActive) {
            WindowManagerImpl.getInstance().doFirePropertyChange("activeMode", oldActive, newActive);
        }
        if (modeRemoved) {
            WindowManagerImpl.getInstance().doFirePropertyChange("modes", null, null);
        }
        if (newActive != null) {
            WindowManagerImpl.notifyRegistryTopComponentActivated(newActive.getSelectedTopComponent());
        } else {
            WindowManagerImpl.notifyRegistryTopComponentActivated(null);
        }
    }

    private ModeImpl setSomeModeActive() {
        for (ModeImpl mode : this.getModes()) {
            if (mode.getOpenedTopComponents().isEmpty() || 2 == mode.getKind()) continue;
            this.model.setActiveMode(mode);
            return mode;
        }
        this.model.setActiveMode(null);
        return this.model.getActiveMode();
    }

    public void setModeBounds(ModeImpl mode, Rectangle bounds) {
        if (bounds == null) {
            return;
        }
        Rectangle old = this.getModeBounds(mode);
        if (old.equals(bounds)) {
            return;
        }
        this.model.setModeBounds(mode, bounds);
        if (this.isVisible() && this.getEditorAreaState() == 1) {
            this.viewRequestor.scheduleRequest(new ViewRequest(mode, 20, old, bounds));
        }
        mode.doFirePropertyChange("bounds", old, bounds);
    }

    public void setModeFrameState(ModeImpl mode, int frameState) {
        int old = this.getModeFrameState(mode);
        if (frameState == old) {
            return;
        }
        this.model.setModeFrameState(mode, frameState);
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(mode, 21, old, frameState));
        }
    }

    public void setModeSelectedTopComponent(ModeImpl mode, TopComponent selected) {
        if (!(mode.getKind() == 2 && selected == null || this.getModeOpenedTopComponents(mode).contains((Object)selected))) {
            return;
        }
        TopComponent old = this.getModeSelectedTopComponent(mode);
        if (selected == old) {
            return;
        }
        this.model.setModeSelectedTopComponent(mode, selected);
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(mode, 22, (Object)old, (Object)selected));
        }
        if (mode == this.getActiveMode()) {
            WindowManagerImpl.notifyRegistryTopComponentActivated(selected);
        }
    }

    public void setModePreviousSelectedTopComponentID(ModeImpl mode, String tcId) {
        this.model.setModePreviousSelectedTopComponentID(mode, tcId);
    }

    public void addModeOpenedTopComponent(ModeImpl mode, TopComponent tc) {
        boolean wasOpened = tc.isOpened();
        if (this.getModeOpenedTopComponents(mode).contains((Object)tc)) {
            return;
        }
        this.removeTopComponentFromOtherModes(mode, tc);
        this.model.addModeOpenedTopComponent(mode, tc);
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(mode, 23, null, (Object)tc));
        }
        if (!wasOpened) {
            WindowManagerImpl.getInstance().notifyTopComponentOpened(tc);
        }
    }

    void addModeOpenedTopComponentNoNotify(ModeImpl mode, TopComponent tc) {
        if (this.getModeOpenedTopComponents(mode).contains((Object)tc)) {
            return;
        }
        this.removeTopComponentFromOtherModes(mode, tc);
        this.model.addModeOpenedTopComponent(mode, tc);
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(mode, 23, null, (Object)tc));
        }
    }

    public void insertModeOpenedTopComponent(ModeImpl mode, TopComponent tc, int index) {
        boolean wasOpened = tc.isOpened();
        List<TopComponent> openedTcs = this.getModeOpenedTopComponents(mode);
        if (index >= 0 && !openedTcs.isEmpty() && openedTcs.size() > index && openedTcs.get(index) == tc) {
            return;
        }
        this.removeTopComponentFromOtherModes(mode, tc);
        this.model.insertModeOpenedTopComponent(mode, tc, index);
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(mode, 23, null, (Object)tc));
        }
        if (!wasOpened) {
            WindowManagerImpl.getInstance().notifyTopComponentOpened(tc);
        }
    }

    public boolean addModeClosedTopComponent(ModeImpl mode, TopComponent tc) {
        boolean opened = this.getModeOpenedTopComponents(mode).contains((Object)tc);
        if (opened && !tc.canClose()) {
            return false;
        }
        if (this.containsModeTopComponent(mode, tc) && !opened) {
            return false;
        }
        if (this.isViewMaximized() && mode.getKind() == 2) {
            mode = this.unSlide(tc, mode);
        }
        this.removeTopComponentFromOtherModes(mode, tc);
        this.model.addModeClosedTopComponent(mode, tc);
        ModeImpl oldActive = this.getActiveMode();
        ModeImpl newActive = this.model.getModeOpenedTopComponents(mode).isEmpty() && mode == oldActive ? this.setSomeModeActive() : oldActive;
        if (this.getCurrentMaximizedMode() == mode && this.model.getModeOpenedTopComponents(mode).isEmpty()) {
            this.switchMaximizedMode(null);
        }
        if (this.isVisible() && opened) {
            this.viewRequestor.scheduleRequest(new ViewRequest(mode, 24, null, (Object)tc));
        }
        if (oldActive != newActive) {
            WindowManagerImpl.getInstance().doFirePropertyChange("activeMode", oldActive, newActive);
        }
        if (newActive != null) {
            WindowManagerImpl.notifyRegistryTopComponentActivated(newActive.getSelectedTopComponent());
        } else {
            WindowManagerImpl.notifyRegistryTopComponentActivated(null);
        }
        if (opened) {
            WindowManagerImpl.getInstance().notifyTopComponentClosed(tc);
        }
        return true;
    }

    public void addModeUnloadedTopComponent(ModeImpl mode, String tcID, int index) {
        TopComponentTracker.getDefault().add(tcID, mode);
        this.model.addModeUnloadedTopComponent(mode, tcID, index);
    }

    public void setUnloadedSelectedTopComponent(ModeImpl mode, String tcID) {
        this.model.setModeUnloadedSelectedTopComponent(mode, tcID);
    }

    public void setUnloadedPreviousSelectedTopComponent(ModeImpl mode, String tcID) {
        this.model.setModeUnloadedPreviousSelectedTopComponent(mode, tcID);
    }

    public List<String> getModeOpenedTopComponentsIDs(ModeImpl mode) {
        return this.model.getModeOpenedTopComponentsIDs(mode);
    }

    public List getModeClosedTopComponentsIDs(ModeImpl mode) {
        return this.model.getModeClosedTopComponentsIDs(mode);
    }

    public List<String> getModeTopComponentsIDs(ModeImpl mode) {
        return this.model.getModeTopComponentsIDs(mode);
    }

    private boolean removeTopComponentFromOtherModes(ModeImpl mode, TopComponent tc) {
        boolean tcRemoved = false;
        for (ModeImpl m : this.model.getModes()) {
            if (m == mode || !this.model.containsModeTopComponent(m, tc)) continue;
            tcRemoved = true;
            this.model.removeModeTopComponent(m, tc, null);
            boolean modeRemoved = false;
            if (!m.isPermanent() && m.isEmpty() && this.doCheckSlidingModes(m) && mode.getKind() != 2 && m != this.modeBeingMaximized) {
                this.model.removeMode(m);
                modeRemoved = true;
            }
            if (!modeRemoved) continue;
            WindowManagerImpl.getInstance().doFirePropertyChange("modes", null, null);
        }
        return tcRemoved;
    }

    public boolean removeModeTopComponent(ModeImpl mode, TopComponent tc) {
        if (!this.containsModeTopComponent(mode, tc)) {
            return false;
        }
        boolean viewChange = this.getModeOpenedTopComponents(mode).contains((Object)tc);
        if (viewChange && !tc.canClose()) {
            return false;
        }
        TopComponent recentTc = null;
        if (mode.getKind() == 1) {
            recentTc = this.getRecentTopComponent(mode, tc);
        }
        this.model.removeModeTopComponent(mode, tc, recentTc);
        ModeImpl oldActive = this.getActiveMode();
        ModeImpl newActive = this.model.getModeOpenedTopComponents(mode).isEmpty() && mode == oldActive ? this.setSomeModeActive() : oldActive;
        if (this.getCurrentMaximizedMode() == mode && this.model.getModeOpenedTopComponents(mode).isEmpty()) {
            this.switchMaximizedMode(null);
        }
        boolean modeRemoved = false;
        if (!mode.isPermanent() && this.model.getModeTopComponents(mode).isEmpty() && this.doCheckSlidingModes(mode)) {
            this.model.removeMode(mode);
            modeRemoved = true;
        }
        if (viewChange && this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(mode, 24, null, (Object)tc));
        }
        if (viewChange) {
            WindowManagerImpl.getInstance().notifyTopComponentClosed(tc);
        }
        if (oldActive != newActive) {
            WindowManagerImpl.getInstance().doFirePropertyChange("activeMode", oldActive, newActive);
        }
        if (modeRemoved) {
            WindowManagerImpl.getInstance().doFirePropertyChange("modes", null, null);
        }
        if (newActive != null) {
            WindowManagerImpl.notifyRegistryTopComponentActivated(newActive.getSelectedTopComponent());
        } else {
            WindowManagerImpl.notifyRegistryTopComponentActivated(null);
        }
        return true;
    }

    TopComponent getRecentTopComponent(ModeImpl editorMode, TopComponent closedTc) {
        String[] ids;
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        if (!WinSysPrefs.HANDLER.getBoolean("editor.closing.activates.recent", true)) {
            List<TopComponent> open = editorMode.getOpenedTopComponents();
            int pos = open.indexOf((Object)closedTc);
            if (open.size() > 1 && pos >= 0) {
                if (pos > 0) {
                    return open.get(pos - 1);
                }
                return open.get(1);
            }
        }
        for (String tcId : ids = wm.getRecentViewIDList()) {
            TopComponent tc;
            ModeImpl mode = this.findMode(tcId);
            if (mode == null || mode != editorMode || (tc = wm.findTopComponent(tcId)) == closedTc) continue;
            return tc;
        }
        return null;
    }

    private ModeImpl findMode(String tcId) {
        for (ModeImpl mode : this.getModes()) {
            if (!mode.getTopComponentsIDs().contains(tcId)) continue;
            return mode;
        }
        return null;
    }

    boolean doCheckSlidingModes(ModeImpl mode) {
        String tcID;
        int i;
        ModeImpl impl;
        TopComponent[] tcs;
        ModeImpl slid = this.model.getSlidingMode("bottom");
        if (slid != null) {
            tcs = slid.getTopComponents();
            for (i = 0; i < tcs.length; ++i) {
                tcID = WindowManagerImpl.getInstance().findTopComponentID(tcs[i]);
                impl = this.model.getModeTopComponentPreviousMode(slid, tcID);
                if (impl != mode) continue;
                return false;
            }
        }
        if ((slid = this.model.getSlidingMode("left")) != null) {
            tcs = slid.getTopComponents();
            for (i = 0; i < tcs.length; ++i) {
                tcID = WindowManagerImpl.getInstance().findTopComponentID(tcs[i]);
                impl = this.model.getModeTopComponentPreviousMode(slid, tcID);
                if (impl != mode) continue;
                return false;
            }
        }
        if ((slid = this.model.getSlidingMode("right")) != null) {
            tcs = slid.getTopComponents();
            for (i = 0; i < tcs.length; ++i) {
                tcID = WindowManagerImpl.getInstance().findTopComponentID(tcs[i]);
                impl = this.model.getModeTopComponentPreviousMode(slid, tcID);
                if (impl != mode) continue;
                return false;
            }
        }
        if ((slid = this.model.getSlidingMode("top")) != null) {
            tcs = slid.getTopComponents();
            for (i = 0; i < tcs.length; ++i) {
                tcID = WindowManagerImpl.getInstance().findTopComponentID(tcs[i]);
                impl = this.model.getModeTopComponentPreviousMode(slid, tcID);
                if (impl != mode) continue;
                return false;
            }
        }
        return true;
    }

    public void removeModeClosedTopComponentID(ModeImpl mode, String tcID) {
        this.model.removeModeClosedTopComponentID(mode, tcID);
    }

    public boolean isGroupOpened(TopComponentGroupImpl tcGroup) {
        return this.model.isGroupOpened(tcGroup);
    }

    public void openGroup(TopComponentGroupImpl tcGroup) {
        if (this.isGroupOpened(tcGroup)) {
            return;
        }
        if (this.isEditorMaximized() && this.isViewMaximized()) {
            this.switchMaximizedMode(null);
        }
        HashSet<TopComponent> openedBeforeTopComponents = new HashSet<TopComponent>();
        Set<TopComponent> tcs = tcGroup.getTopComponents();
        for (TopComponent tc : tcs) {
            if (!tc.isOpened()) continue;
            openedBeforeTopComponents.add(tc);
        }
        tcs = tcGroup.getOpeningSet();
        HashSet<ModeImpl> openedModes = new HashSet<ModeImpl>(tcs.size());
        ArrayList<TopComponent> openedTcs = new ArrayList<TopComponent>();
        for (TopComponent tc22 : tcs) {
            String tcID;
            if (tc22.isOpened()) continue;
            WindowManagerImpl wm = WindowManagerImpl.getInstance();
            ModeImpl mode = (ModeImpl)wm.findMode(tc22);
            if (mode == null) {
                mode = wm.getDefaultViewMode();
            } else if (mode.getOpenedTopComponentsIDs().isEmpty()) {
                openedModes.add(mode);
            }
            this.model.addModeOpenedTopComponent(mode, tc22);
            if (tc22.getClientProperty(GROUP_SELECTED) != null) {
                tc22.requestVisible();
            }
            if (this.isEditorMaximized() && mode.getState() != 1 && !this.isTopComponentDockedInMaximizedMode(tcID = wm.findTopComponentID(tc22)) && mode.getKind() != 2) {
                this.slide(tc22, mode, this.getSlideSideForMode(mode));
            }
            openedTcs.add(tc22);
        }
        this.model.openGroup(tcGroup, new HashSet<TopComponent>(openedTcs), openedBeforeTopComponents);
        for (ModeImpl mode : openedModes) {
            TopComponent prevSelTC = mode.getPreviousSelectedTopComponent();
            if (null == prevSelTC) continue;
            mode.setSelectedTopComponent(prevSelTC);
        }
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(tcGroup, 42, null, openedTcs.toArray((T[])new TopComponent[0])));
        }
        for (TopComponent tc22 : openedTcs) {
            WindowManagerImpl.getInstance().notifyTopComponentOpened(tc22);
        }
    }

    public void closeGroup(TopComponentGroupImpl tcGroup) {
        if (!this.isGroupOpened(tcGroup)) {
            return;
        }
        if (this.isViewMaximized()) {
            this.switchMaximizedMode(null);
        }
        Set tcs = tcGroup.getClosingSet();
        ArrayList<TopComponent> closedTcs = new ArrayList<TopComponent>();
        Set<TopComponent> openedTcsByGroup = this.model.getGroupOpenedTopComponents(tcGroup);
        Set<TopComponent> openedTcsBefore = this.model.getGroupOpenedBeforeTopComponents(tcGroup);
        for (TopComponent tc : this.model.getGroupTopComponents(tcGroup)) {
            boolean wasOpenedBefore = openedTcsBefore.contains((Object)tc);
            boolean openedByGroup = openedTcsByGroup.contains((Object)tc);
            if (tc.isOpened()) {
                if (wasOpenedBefore || openedByGroup) continue;
                this.model.addGroupOpeningTopComponent(tcGroup, tc);
                continue;
            }
            if (!wasOpenedBefore && !openedByGroup) continue;
            this.model.removeGroupOpeningTopComponent(tcGroup, tc);
        }
        ArrayList<ModeImpl> groupModes = new ArrayList<ModeImpl>(tcs.size());
        for (TopComponent tc2 : tcs) {
            ModeImpl mode;
            if (!tc2.isOpened() || openedTcsBefore.contains((Object)tc2) || null == (mode = (ModeImpl)WindowManagerImpl.getInstance().findMode(tc2))) continue;
            groupModes.add(mode);
        }
        for (ModeImpl mode : groupModes) {
            String tcId;
            TopComponent selTC = mode.getSelectedTopComponent();
            if (null == selTC || null == (tcId = WindowManagerImpl.getInstance().findTopComponentID(selTC))) continue;
            this.setModePreviousSelectedTopComponentID(mode, tcId);
        }
        WindowManagerImpl wmi = WindowManagerImpl.getInstance();
        for (TopComponent tc3 : tcs) {
            ModeImpl mode2;
            if (!tc3.isOpened() || openedTcsBefore.contains((Object)tc3) || wmi.isEditorTopComponent(tc3)) continue;
            boolean ignore = false;
            for (TopComponentGroupImpl group : this.model.getTopComponentGroups()) {
                if (group == tcGroup || !group.isOpened() || !group.getOpeningSet().contains((Object)tc3)) continue;
                ignore = true;
                break;
            }
            if (ignore || (mode2 = (ModeImpl)WindowManagerImpl.getInstance().findMode(tc3)) == null) continue;
            if (mode2.getSelectedTopComponent() == tc3) {
                tc3.putClientProperty(GROUP_SELECTED, (Object)Boolean.TRUE);
            } else {
                tc3.putClientProperty(GROUP_SELECTED, (Object)null);
            }
            if (PersistenceHandler.isTopComponentPersistentWhenClosed(tc3)) {
                this.model.addModeClosedTopComponent(mode2, tc3);
            } else if (Boolean.TRUE.equals(tc3.getClientProperty((Object)"KeepNonPersistentTCInModelWhenClosed"))) {
                this.model.addModeClosedTopComponent(mode2, tc3);
            } else {
                this.model.removeModeTopComponent(mode2, tc3, null);
            }
            closedTcs.add(tc3);
        }
        this.model.closeGroup(tcGroup);
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(tcGroup, 43, null, closedTcs.toArray((T[])new TopComponent[0])));
        }
        for (TopComponent tc4 : closedTcs) {
            WindowManagerImpl.getInstance().notifyTopComponentClosed(tc4);
        }
    }

    public void addTopComponentGroup(TopComponentGroupImpl tcGroup) {
        this.model.addTopComponentGroup(tcGroup);
    }

    public void removeTopComponentGroup(TopComponentGroupImpl tcGroup) {
        this.model.removeTopComponentGroup(tcGroup);
    }

    public boolean addGroupUnloadedTopComponent(TopComponentGroupImpl tcGroup, String tcID) {
        return this.model.addGroupUnloadedTopComponent(tcGroup, tcID);
    }

    public boolean removeGroupUnloadedTopComponent(TopComponentGroupImpl tcGroup, String tcID) {
        return this.model.removeGroupUnloadedTopComponent(tcGroup, tcID);
    }

    public boolean addGroupUnloadedOpeningTopComponent(TopComponentGroupImpl tcGroup, String tcID) {
        return this.model.addGroupUnloadedOpeningTopComponent(tcGroup, tcID);
    }

    public boolean removeGroupUnloadedOpeningTopComponent(TopComponentGroupImpl tcGroup, String tcID) {
        return this.model.removeGroupUnloadedOpeningTopComponent(tcGroup, tcID);
    }

    public boolean addGroupUnloadedClosingTopComponent(TopComponentGroupImpl tcGroup, String tcID) {
        return this.model.addGroupUnloadedClosingTopComponent(tcGroup, tcID);
    }

    public boolean removeGroupUnloadedClosingTopComponent(TopComponentGroupImpl tcGroup, String tcID) {
        return this.model.removeGroupUnloadedClosingTopComponent(tcGroup, tcID);
    }

    public boolean addGroupUnloadedOpenedTopComponent(TopComponentGroupImpl tcGroup, String tcID) {
        if (!this.isGroupOpened(tcGroup)) {
            return false;
        }
        return this.model.addGroupUnloadedOpenedTopComponent(tcGroup, tcID);
    }

    public Set getGroupOpenedTopComponents(TopComponentGroupImpl tcGroup) {
        return this.model.getGroupOpenedTopComponents(tcGroup);
    }

    public Set<String> getGroupTopComponentsIDs(TopComponentGroupImpl tcGroup) {
        return this.model.getGroupTopComponentsIDs(tcGroup);
    }

    public Set<String> getGroupOpeningSetIDs(TopComponentGroupImpl tcGroup) {
        return this.model.getGroupOpeningSetIDs(tcGroup);
    }

    public Set<String> getGroupClosingSetIDs(TopComponentGroupImpl tcGroup) {
        return this.model.getGroupClosingSetIDs(tcGroup);
    }

    public Set<String> getGroupOpenedTopComponentsIDs(TopComponentGroupImpl tcGroup) {
        return this.model.getGroupOpenedTopComponentsIDs(tcGroup);
    }

    public boolean isVisible() {
        return this.model.isVisible();
    }

    public Set<? extends ModeImpl> getModes() {
        return this.model.getModes();
    }

    public Rectangle getMainWindowBoundsJoined() {
        return this.model.getMainWindowBoundsJoined();
    }

    public Rectangle getMainWindowBoundsSeparated() {
        return this.model.getMainWindowBoundsSeparated();
    }

    public int getMainWindowFrameStateJoined() {
        return this.model.getMainWindowFrameStateJoined();
    }

    public int getMainWindowFrameStateSeparated() {
        return this.model.getMainWindowFrameStateSeparated();
    }

    public ModeImpl getActiveMode() {
        return this.model.getActiveMode();
    }

    public ModeImpl getLastActiveEditorMode() {
        return this.model.getLastActiveEditorMode();
    }

    public Rectangle getEditorAreaBounds() {
        return this.model.getEditorAreaBounds();
    }

    public SplitConstraint[] getEditorAreaConstraints() {
        return this.model.getEditorAreaConstraints();
    }

    public int getEditorAreaState() {
        return this.model.getEditorAreaState();
    }

    public int getEditorAreaFrameState() {
        return this.model.getEditorAreaFrameState();
    }

    public Component getEditorAreaComponent() {
        return this.viewRequestor.getEditorAreaComponent();
    }

    ModeImpl getCurrentMaximizedMode() {
        if (this.isViewMaximized()) {
            return this.model.getViewMaximizedMode();
        }
        if (this.isEditorMaximized()) {
            return this.model.getEditorMaximizedMode();
        }
        return null;
    }

    ModeImpl getEditorMaximizedMode() {
        return this.model.getEditorMaximizedMode();
    }

    ModeImpl getViewMaximizedMode() {
        return this.model.getViewMaximizedMode();
    }

    public SplitConstraint[] getModeConstraints(ModeImpl mode) {
        return this.model.getModeConstraints(mode);
    }

    public String getToolbarConfigName() {
        return this.model.getToolbarConfigName();
    }

    public String getModeName(ModeImpl mode) {
        return this.model.getModeName(mode);
    }

    public Rectangle getModeBounds(ModeImpl mode) {
        return this.model.getModeBounds(mode);
    }

    public int getModeState(ModeImpl mode) {
        return this.model.getModeState(mode);
    }

    public int getModeKind(ModeImpl mode) {
        return this.model.getModeKind(mode);
    }

    public String getModeSide(ModeImpl mode) {
        return this.model.getModeSide(mode);
    }

    public int getModeFrameState(ModeImpl mode) {
        return this.model.getModeFrameState(mode);
    }

    public boolean isModePermanent(ModeImpl mode) {
        return this.model.isModePermanent(mode);
    }

    public boolean isModeEmpty(ModeImpl mode) {
        return this.model.isModeEmpty(mode);
    }

    public boolean containsModeTopComponent(ModeImpl mode, TopComponent tc) {
        return this.model.containsModeTopComponent(mode, tc);
    }

    public TopComponent getModeSelectedTopComponent(ModeImpl mode) {
        return this.model.getModeSelectedTopComponent(mode);
    }

    public String getModePreviousSelectedTopComponentID(ModeImpl mode) {
        return this.model.getModePreviousSelectedTopComponentID(mode);
    }

    public List<TopComponent> getModeTopComponents(ModeImpl mode) {
        return this.model.getModeTopComponents(mode);
    }

    public List<TopComponent> getModeOpenedTopComponents(ModeImpl mode) {
        return this.model.getModeOpenedTopComponents(mode);
    }

    public int getModeTopComponentTabPosition(ModeImpl mode, TopComponent tc) {
        return this.model.getModeOpenedTopComponentTabPosition(mode, tc);
    }

    boolean isModeMinimized(ModeImpl mode) {
        return this.model.isModeMinimized(mode);
    }

    void setModeMinimized(ModeImpl mode, boolean minimized) {
        this.model.setModeMinimized(mode, minimized);
    }

    Collection<String> getModeOtherNames(ModeImpl mode) {
        return this.model.getModeOtherNames(mode);
    }

    void addModeOtherName(ModeImpl mode, String modeOtherName) {
        this.model.addModeOtherName(mode, modeOtherName);
    }

    public Set<TopComponentGroupImpl> getTopComponentGroups() {
        return this.model.getTopComponentGroups();
    }

    public String getGroupName(TopComponentGroupImpl tcGroup) {
        return this.model.getGroupName(tcGroup);
    }

    public Set<TopComponent> getGroupTopComponents(TopComponentGroupImpl tcGroup) {
        return this.model.getGroupTopComponents(tcGroup);
    }

    public Set<TopComponent> getGroupOpeningTopComponents(TopComponentGroupImpl tcGroup) {
        return this.model.getGroupOpeningTopComponents(tcGroup);
    }

    public Set getGroupClosingTopComponents(TopComponentGroupImpl tcGroup) {
        return this.model.getGroupClosingTopComponents(tcGroup);
    }

    public void topComponentDisplayNameChanged(ModeImpl mode, TopComponent tc) {
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(mode, 31, null, (Object)tc));
        }
    }

    public void topComponentDisplayNameAnnotation(ModeImpl mode, TopComponent tc) {
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(mode, 32, null, (Object)tc));
        }
    }

    public void topComponentToolTipChanged(ModeImpl mode, TopComponent tc) {
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(mode, 33, null, (Object)tc));
        }
    }

    public void topComponentIconChanged(ModeImpl mode, TopComponent tc) {
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(mode, 34, null, (Object)tc));
        }
    }

    public void topComponentMakeBusy(ModeImpl mode, TopComponent tc, boolean busy) {
        String modeName = this.getModeName(mode);
        this.viewRequestor.scheduleRequest(new ViewRequest(modeName, busy ? 70 : 71, (Object)tc, (Object)tc));
    }

    public void resetModel() {
        this.model.reset();
    }

    ModeImpl attachModeToSide(ModeImpl referenceMode, String side, String modeName, int modeKind, boolean permanent) {
        ModeImpl newMode = WindowManagerImpl.getInstance().createModeImpl(modeName, modeKind, permanent);
        this.model.addModeToSide(newMode, referenceMode, side);
        return newMode;
    }

    private ModeImpl attachModeToSide(ModeImpl attachMode, String side, int modeKind) {
        return this.attachModeToSide(attachMode, side, ModeImpl.getUnusedModeName(), modeKind, false);
    }

    private ModeImpl attachModeAroundDesktop(String side) {
        ModeImpl newMode = WindowManagerImpl.getInstance().createModeImpl(ModeImpl.getUnusedModeName(), 0, false);
        this.model.addModeAround(newMode, side);
        return newMode;
    }

    private ModeImpl attachModeAroundEditor(String side, int modeKind) {
        ModeImpl newMode = WindowManagerImpl.getInstance().createModeImpl(ModeImpl.getUnusedModeName(), modeKind, false);
        this.model.addModeAroundEditor(newMode, side);
        return newMode;
    }

    private ModeImpl createFloatingMode(Rectangle bounds, int modeKind) {
        WindowManagerImpl wmi = WindowManagerImpl.getInstance();
        ModeImpl newMode = wmi.createModeImpl(ModeImpl.getUnusedModeName(), modeKind, 1, false);
        newMode.setBounds(bounds);
        this.model.addMode(newMode, new SplitConstraint[]{new SplitConstraint(1, 100, 0.5)});
        return newMode;
    }

    public void activateModeTopComponent(ModeImpl mode, TopComponent tc) {
        Component fOwn;
        if (!this.getModeOpenedTopComponents(mode).contains((Object)tc)) {
            return;
        }
        ModeImpl oldActiveMode = this.getActiveMode();
        if (oldActiveMode != null && oldActiveMode.equals(mode) && tc != null && tc.equals((Object)this.model.getModeSelectedTopComponent(mode)) && (fOwn = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner()) != null && SwingUtilities.isDescendingFrom(fOwn, (Component)tc)) {
            this.slideOutSlidingWindows(mode);
            return;
        }
        this.model.setActiveMode(mode);
        this.model.setModeSelectedTopComponent(mode, tc);
        if (this.isVisible()) {
            Frame frame;
            this.viewRequestor.scheduleRequest(new ViewRequest(mode, 44, null, (Object)tc));
            if (mode.getState() == 1 && null != (frame = (Frame)SwingUtilities.getAncestorOfClass(Frame.class, (Component)tc)) && frame != WindowManagerImpl.getInstance().getMainWindow() && (frame.getExtendedState() & 1) > 0) {
                frame.setExtendedState(frame.getExtendedState() - 1);
            }
        }
        WindowManagerImpl.notifyRegistryTopComponentActivated(tc);
        if (oldActiveMode != mode) {
            WindowManagerImpl.getInstance().doFirePropertyChange("activeMode", oldActiveMode, mode);
        }
    }

    protected void slideOutSlidingWindows(ModeImpl newActiveMode) {
        for (ModeImpl mode : this.getModes()) {
            if (newActiveMode.equals(mode) || mode.getKind() != 2 || null == mode.getSelectedTopComponent()) continue;
            this.setModeSelectedTopComponent(mode, null);
        }
    }

    public boolean isDragInProgress() {
        return this.viewRequestor.isDragInProgress();
    }

    public Frame getMainWindow() {
        return this.viewRequestor.getMainWindow();
    }

    public String guessSlideSide(TopComponent tc) {
        return this.viewRequestor.guessSlideSide(tc);
    }

    String getSlideSideForMode(ModeImpl mode) {
        return this.model.getSlideSideForMode(mode);
    }

    public boolean isDocked(TopComponent comp) {
        ModeImpl mode = (ModeImpl)WindowManagerImpl.getInstance().findMode(comp);
        return mode != null && mode.getState() == 0;
    }

    public void createModeModel(ModeImpl mode, String name, int state, int kind, boolean permanent) {
        this.model.createModeModel(mode, name, state, kind, permanent);
    }

    public void createGroupModel(TopComponentGroupImpl tcGroup, String name, boolean opened) {
        this.model.createGroupModel(tcGroup, name, opened);
    }

    public WindowSystemSnapshot createWindowSystemSnapshot() {
        return this.model.createWindowSystemSnapshot();
    }

    @Override
    public void userActivatedMode(ModeImpl mode) {
        if (mode != null) {
            this.setActiveMode(mode);
        }
    }

    @Override
    public void userActivatedModeWindow(ModeImpl mode) {
        if (mode != null) {
            this.setActiveMode(mode);
        }
    }

    @Override
    public void userActivatedEditorWindow() {
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        TopComponent[] tcs = wm.getRecentViewList();
        for (int i = 0; i < tcs.length; ++i) {
            TopComponent tc = tcs[i];
            ModeImpl mode = (ModeImpl)wm.findMode(tc);
            if (mode == null || mode.getKind() != 1 || mode.getOpenedTopComponents().isEmpty()) continue;
            this.setActiveMode(mode);
            return;
        }
        ModeImpl mode = wm.getDefaultEditorMode();
        if (mode != null && !mode.getOpenedTopComponents().isEmpty()) {
            this.setActiveMode(mode);
        } else {
            this.setActiveMode(null);
        }
    }

    @Override
    public void userActivatedTopComponent(ModeImpl mode, TopComponent selected) {
        if (mode != null) {
            this.setModeSelectedTopComponent(mode, selected);
        }
    }

    @Override
    public void userResizedMainWindow(Rectangle bounds) {
        if (this.getEditorAreaState() == 0) {
            this.model.setMainWindowBoundsJoined(bounds);
        } else {
            this.model.setMainWindowBoundsSeparated(bounds);
        }
    }

    @Override
    public void userResizedMainWindowBoundsSeparatedHelp(Rectangle bounds) {
        if (this.getEditorAreaState() == 0 && this.getMainWindowBoundsSeparated().isEmpty()) {
            this.model.setMainWindowBoundsUserSeparatedHelp(bounds);
        }
    }

    @Override
    public void userResizedEditorArea(Rectangle bounds) {
        this.model.setEditorAreaBounds(bounds);
    }

    @Override
    public void userResizedEditorAreaBoundsHelp(Rectangle bounds) {
        if (this.getEditorAreaState() == 0 && this.getEditorAreaBounds().isEmpty()) {
            this.model.setEditorAreaBoundsUserHelp(bounds);
        }
    }

    @Override
    public void userResizedModeBounds(ModeImpl mode, Rectangle bounds) {
        Rectangle old = this.model.getModeBounds(mode);
        this.model.setModeBounds(mode, bounds);
        mode.doFirePropertyChange("bounds", old, bounds);
    }

    @Override
    public void userResizedModeBoundsSeparatedHelp(ModeImpl mode, Rectangle bounds) {
        this.model.setModeBoundsSeparatedHelp(mode, bounds);
    }

    @Override
    public void userChangedFrameStateMainWindow(int frameState) {
        if (this.getEditorAreaState() == 0) {
            this.model.setMainWindowFrameStateJoined(frameState);
        } else {
            this.model.setMainWindowFrameStateSeparated(frameState);
        }
    }

    @Override
    public void userChangedFrameStateEditorArea(int frameState) {
        this.model.setEditorAreaFrameState(frameState);
    }

    @Override
    public void userChangedFrameStateMode(ModeImpl mode, int frameState) {
        this.model.setModeFrameState(mode, frameState);
    }

    @Override
    public void userChangedSplit(ModelElement[] snapshots, double[] splitWeights) {
        this.model.setSplitWeights(snapshots, splitWeights);
    }

    @Override
    public void userClosedTopComponent(ModeImpl mode, TopComponent tc) {
        ModeImpl otherEditorMode;
        if (mode == this.getCurrentMaximizedMode() && this.isViewMaximized()) {
            this.switchMaximizedMode(null);
            for (ModeImpl newMode : this.getModes()) {
                if (!newMode.containsTopComponent(tc)) continue;
                this.userClosedTopComponent(newMode, tc);
                return;
            }
        }
        TopComponent recentTc = null;
        if (mode.getKind() == 1) {
            recentTc = this.getRecentTopComponent(mode, tc);
        }
        boolean wasTcClosed = false;
        if (PersistenceHandler.isTopComponentPersistentWhenClosed(tc)) {
            wasTcClosed = this.addModeClosedTopComponent(mode, tc);
        } else if (Boolean.TRUE.equals(tc.getClientProperty((Object)"KeepNonPersistentTCInModelWhenClosed"))) {
            wasTcClosed = this.addModeClosedTopComponent(mode, tc);
        } else {
            wasTcClosed = this.removeModeTopComponent(mode, tc);
        }
        if (wasTcClosed != false && mode.getKind() == 1 && "editor".equals(mode.getName()) && mode.getOpenedTopComponentsIDs().isEmpty() && null != (otherEditorMode = this.findSomeOtherEditorModeImpl())) {
            for (String closedTcId : otherEditorMode.getClosedTopComponentsIDs()) {
                mode.addUnloadedTopComponent(closedTcId);
            }
            List<TopComponent> tcs = otherEditorMode.getOpenedTopComponents();
            for (TopComponent t : tcs) {
                int index = otherEditorMode.getTopComponentTabPosition(t);
                mode.addOpenedTopComponent(t, index);
            }
            this.removeMode(otherEditorMode);
        }
        if (recentTc != null && wasTcClosed != false) {
            recentTc.requestActive();
        }
        if (TopComponent.getRegistry().getOpened().isEmpty()) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    Frame f = Central.this.getMainWindow();
                    if (null != f && f.isVisible()) {
                        f.invalidate();
                        f.repaint();
                    }
                }
            });
        }
    }

    private ModeImpl findSomeOtherEditorModeImpl() {
        for (ModeImpl m : this.getModes()) {
            if (m.getKind() != 1 || "editor".equals(m.getName()) || m.getOpenedTopComponentsIDs().isEmpty() || m.isPermanent() || m.getState() == 1) continue;
            return m;
        }
        return null;
    }

    @Override
    public void userClosedMode(ModeImpl mode) {
        if (mode != null) {
            boolean allAreClosable = true;
            for (TopComponent tc : mode.getOpenedTopComponents()) {
                if (Switches.isClosingEnabled(tc)) continue;
                allAreClosable = false;
                break;
            }
            if (allAreClosable) {
                this.closeMode(mode);
            } else {
                ArrayList<TopComponent> tcs = new ArrayList<TopComponent>(mode.getOpenedTopComponents());
                for (TopComponent tc2 : tcs) {
                    if (!Switches.isClosingEnabled(tc2)) continue;
                    this.userClosedTopComponent(mode, tc2);
                }
            }
            if (mode.getOpenedTopComponents().isEmpty() && mode == this.getCurrentMaximizedMode()) {
                this.switchMaximizedMode(null);
            }
        }
    }

    @Override
    public void userDroppedTopComponents(ModeImpl mode, TopComponentDraggable draggable) {
        boolean unmaximize = false;
        if (draggable.isTopComponentTransfer()) {
            unmaximize = this.moveTopComponentIntoMode(mode, draggable.getTopComponent());
        } else {
            TopComponent selTC = draggable.getMode().getSelectedTopComponent();
            this.mergeModes(draggable.getMode(), mode, -1);
            if (null != selTC) {
                mode.setSelectedTopComponent(selTC);
            }
        }
        this.updateViewAfterDnD(unmaximize);
    }

    @Override
    public void userDroppedTopComponents(ModeImpl mode, TopComponentDraggable draggable, int index) {
        boolean unmaximize = false;
        if (draggable.isTopComponentTransfer()) {
            unmaximize = this.moveTopComponentIntoMode(mode, draggable.getTopComponent(), index);
        } else {
            TopComponent selTC = draggable.getMode().getSelectedTopComponent();
            this.mergeModes(draggable.getMode(), mode, index);
            if (null != selTC) {
                mode.setSelectedTopComponent(selTC);
            }
        }
        this.updateViewAfterDnD(unmaximize);
    }

    @Override
    public void userDroppedTopComponents(ModeImpl mode, TopComponentDraggable draggable, String side) {
        ModeImpl newMode = this.attachModeToSide(mode, side, mode.getKind());
        if (draggable.isTopComponentTransfer()) {
            this.moveTopComponentIntoMode(newMode, draggable.getTopComponent());
        } else if (newMode.getKind() != draggable.getKind()) {
            this.mergeModes(draggable.getMode(), newMode, -1);
        } else {
            this.dockMode(newMode, draggable.getMode());
        }
        this.updateViewAfterDnD(true);
    }

    @Override
    public void userDroppedTopComponentsIntoEmptyEditor(TopComponentDraggable draggable) {
        ModeImpl mode = (ModeImpl)WindowManagerImpl.getInstance().findMode("editor");
        if (null == mode || mode.getState() == 1) {
            for (ModeImpl m : this.getModes()) {
                if (m.getKind() != 1 || m.getState() != 0) continue;
                mode = m;
                break;
            }
        }
        if (null == mode || mode == draggable.getMode()) {
            if (draggable.isModeTransfer() && draggable.getMode().getKind() == 1) {
                this.userDockedMode(draggable.getMode());
            }
            return;
        }
        if (draggable.isTopComponentTransfer()) {
            this.moveTopComponentIntoMode(mode, draggable.getTopComponent());
        } else if (mode.getKind() != draggable.getKind()) {
            this.mergeModes(draggable.getMode(), mode, 0);
        } else {
            this.dockMode(mode, draggable.getMode());
        }
        this.updateViewAfterDnD(true);
    }

    @Override
    public void userDroppedTopComponentsAround(TopComponentDraggable draggable, String side) {
        ModeImpl newMode = this.attachModeAroundDesktop(side);
        if (draggable.isTopComponentTransfer()) {
            this.moveTopComponentIntoMode(newMode, draggable.getTopComponent());
        } else {
            this.dockMode(newMode, draggable.getMode());
        }
        this.updateViewAfterDnD(true);
    }

    @Override
    public void userDroppedTopComponentsAroundEditor(TopComponentDraggable draggable, String side) {
        ModeImpl newMode = this.attachModeAroundEditor(side, draggable.getKind());
        if (draggable.isTopComponentTransfer()) {
            this.moveTopComponentIntoMode(newMode, draggable.getTopComponent());
        } else {
            this.dockMode(newMode, draggable.getMode());
        }
        this.updateViewAfterDnD(true);
    }

    @Override
    public void userDroppedTopComponentsIntoFreeArea(TopComponentDraggable draggable, Rectangle bounds) {
        if (draggable.isTopComponentTransfer()) {
            ModeImpl newMode = this.createFloatingMode(bounds, draggable.getKind());
            this.moveTopComponentIntoMode(newMode, draggable.getTopComponent());
            newMode.setSelectedTopComponent(draggable.getTopComponent());
        } else {
            this.userUndockedMode(draggable.getMode(), bounds);
        }
        this.updateViewAfterDnD(false);
    }

    public void userUndockedMode(ModeImpl mode) {
        if (this.getCurrentMaximizedMode() == mode) {
            this.switchMaximizedMode(null);
        }
        Rectangle modeBounds = null;
        TopComponent tc = mode.getSelectedTopComponent();
        if (null != tc) {
            Point tcLoc = tc.getLocation();
            Dimension tcSize = tc.getSize();
            SwingUtilities.convertPointToScreen(tcLoc, (Component)tc);
            modeBounds = new Rectangle(tcLoc, tcSize);
        }
        this.userUndockedMode(mode, modeBounds);
    }

    private void userUndockedMode(ModeImpl mode, Rectangle modeBounds) {
        int modeKind = mode.getKind();
        if (modeKind == 2) {
            modeKind = 0;
        }
        if (this.getCurrentMaximizedMode() == mode) {
            this.switchMaximizedMode(null);
        }
        if (null != modeBounds) {
            this.model.setModeBounds(mode, modeBounds);
        }
        SplitConstraint[] constraints = mode.getConstraints();
        this.model.setModeState(mode, 1);
        ModeImpl previousMode = WindowManagerImpl.getInstance().createMode(null, mode.getKind(), 0, true, constraints);
        constraints = previousMode.getConstraints();
        List<String> openedIDs = mode.getOpenedTopComponentsIDs();
        for (String tcID : this.getModeTopComponentsIDs(mode)) {
            this.model.setModeTopComponentPreviousMode(mode, tcID, previousMode, openedIDs.indexOf(tcID));
            this.model.setModeTopComponentPreviousConstraints(mode, tcID, constraints);
        }
        this.model.setModeConstraints(mode, new SplitConstraint[0]);
        this.updateViewAfterDnD(false);
        WindowManagerImpl.getInstance().doFirePropertyChange("modes", null, null);
    }

    public void userDockedMode(ModeImpl mode) {
        int modeKind = mode.getKind();
        if (modeKind == 2) {
            modeKind = 0;
        }
        this.switchMaximizedMode(null);
        TopComponent selectedTC = mode.getSelectedTopComponent();
        if (!mode.isPermanent()) {
            for (TopComponent tc : mode.getOpenedTopComponents()) {
                this.userDockedTopComponent(tc, mode);
            }
        } else {
            List<String> ids = mode.getTopComponentsIDs();
            if (!ids.isEmpty()) {
                SplitConstraint[] constraints;
                String tcID = ids.get(0);
                ModeImpl previousMode = this.model.getModeTopComponentPreviousMode(mode, tcID);
                if (!(null != previousMode && this.model.getModes().contains(previousMode) || null == (constraints = this.model.getModeTopComponentPreviousConstraints(mode, tcID)))) {
                    previousMode = this.findJoinedMode(modeKind, constraints);
                }
                if (null == previousMode) {
                    constraints = this.model.getModeTopComponentPreviousConstraints(mode, tcID);
                    if (null != constraints) {
                        this.model.setModeConstraints(mode, constraints);
                    }
                    this.model.setModeState(mode, 0);
                } else if (previousMode.isPermanent() && !previousMode.getTopComponentsIDs().isEmpty()) {
                    List<String> opened = mode.getOpenedTopComponentsIDs();
                    for (String id : opened) {
                        int prevIndex = this.model.getModeTopComponentPreviousIndex(mode, id);
                        TopComponent tc = WindowManagerImpl.getInstance().findTopComponent(id);
                        previousMode.addOpenedTopComponent(tc, prevIndex);
                    }
                    this.mergeModes(mode, previousMode, -1);
                    mode = null;
                } else {
                    this.dockMode(previousMode, mode);
                }
            }
            if (null != mode) {
                SplitConstraint[] constraints = mode.getConstraints();
                if (null == constraints || constraints.length == 0) {
                    this.model.setModeConstraints(mode, new SplitConstraint[]{new SplitConstraint(1, 0, 0.2)});
                }
                this.model.setModeState(mode, 0);
            }
        }
        this.updateViewAfterDnD(false);
        if (null != selectedTC) {
            selectedTC.requestActive();
        }
        WindowManagerImpl.getInstance().doFirePropertyChange("modes", null, null);
    }

    private void mergeModes(ModeImpl source, ModeImpl target, int index) {
        ModeImpl prevMode = null;
        SplitConstraint[] prevConstraints = null;
        if (source.isPermanent() && !target.isPermanent()) {
            prevMode = this.getPreviousMode(source);
            prevConstraints = this.getPreviousConstraints(source);
        } else {
            prevMode = this.getPreviousMode(target);
            prevConstraints = this.getPreviousConstraints(target);
        }
        int tmpIndex = index;
        for (String tcID : source.getTopComponentsIDs()) {
            target.addUnloadedTopComponent(tcID, tmpIndex);
            if (tmpIndex < 0) continue;
            ++tmpIndex;
        }
        tmpIndex = index;
        List<TopComponent> opened = source.getOpenedTopComponents();
        for (TopComponent tc : opened) {
            target.addOpenedTopComponent(tc, tmpIndex);
            if (tmpIndex >= 0) {
                ++tmpIndex;
            }
            TopComponentTracker.getDefault().add(tc, target);
        }
        if (source.isPermanent()) {
            target.addOtherName(source.getName());
            for (String otherName : source.getOtherNames()) {
                target.addOtherName(otherName);
            }
        }
        if (source.isPermanent()) {
            this.model.makeModePermanent(target);
        }
        if (target.getState() == 1) {
            this.setPreviousMode(target, prevMode);
            this.setPreviousConstraints(target, prevConstraints);
        }
        this.model.removeMode(source);
    }

    private ModeImpl getPreviousMode(ModeImpl mode) {
        ModeImpl prevMode = null;
        if (mode.getState() == 0) {
            prevMode = mode;
        } else {
            List<String> ids = mode.getTopComponentsIDs();
            if (!ids.isEmpty()) {
                prevMode = this.model.getModeTopComponentPreviousMode(mode, ids.get(0));
            }
        }
        if (prevMode != null && !this.getModes().contains(prevMode)) {
            prevMode = null;
        }
        return prevMode;
    }

    private SplitConstraint[] getPreviousConstraints(ModeImpl mode) {
        ModeImpl prevMode = this.getPreviousMode(mode);
        if (null != prevMode) {
            return mode.getConstraints();
        }
        List<String> ids = mode.getTopComponentsIDs();
        if (!ids.isEmpty()) {
            return this.model.getModeTopComponentPreviousConstraints(mode, ids.get(0));
        }
        return null;
    }

    private void setPreviousMode(ModeImpl mode, ModeImpl prevMode) {
        for (String tcId : mode.getTopComponentsIDs()) {
            int prevIndex = this.model.getModeTopComponentPreviousIndex(mode, tcId);
            this.model.setModeTopComponentPreviousMode(mode, tcId, prevMode, prevIndex);
        }
    }

    private void setPreviousConstraints(ModeImpl mode, SplitConstraint[] prevConstraints) {
        for (String tcId : mode.getTopComponentsIDs()) {
            this.model.setModeTopComponentPreviousConstraints(mode, tcId, prevConstraints);
        }
    }

    private void dockMode(ModeImpl prevMode, ModeImpl floatingMode) {
        ModeImpl floatingPrevMode = this.getPreviousMode(floatingMode);
        List<TopComponent> opened = prevMode.getOpenedTopComponents();
        for (TopComponent tc : opened) {
            floatingMode.addOpenedTopComponent(tc);
        }
        for (String tcID : prevMode.getClosedTopComponentsIDs()) {
            floatingMode.addUnloadedTopComponent(tcID);
        }
        this.model.dockMode(prevMode, floatingMode);
        this.setPreviousMode(floatingMode, null);
        this.setPreviousConstraints(floatingMode, null);
        if (null != floatingPrevMode && floatingPrevMode.getTopComponentsIDs().isEmpty()) {
            this.model.removeMode(floatingPrevMode);
        }
    }

    void userMinimizedMode(ModeImpl mode) {
        List<TopComponent> opened = mode.getOpenedTopComponents();
        TopComponent selTc = mode.getSelectedTopComponent();
        String side = this.getSlideSideForMode(mode);
        for (TopComponent tc : opened) {
            this.slide(tc, mode, side);
        }
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        int index = 0;
        for (TopComponent tc2 : opened) {
            ModeImpl newMode = (ModeImpl)wm.findMode(tc2);
            if (null == newMode) continue;
            String tcId = wm.findTopComponentID(tc2);
            this.model.setModeTopComponentPreviousMode(newMode, tcId, mode, index++);
        }
        this.setModeMinimized(mode, true);
        if (null != selTc) {
            mode.setPreviousSelectedTopComponentID(wm.findTopComponentID(selTc));
        }
    }

    void userRestoredMode(ModeImpl slidingMode, ModeImpl modeToRestore) {
        TopComponent tcToSelect = modeToRestore.getPreviousSelectedTopComponent();
        this.setModeMinimized(modeToRestore, false);
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        for (TopComponent tc : slidingMode.getOpenedTopComponents()) {
            String id = wm.findTopComponentID(tc);
            ModeImpl prevMode = this.model.getModeTopComponentPreviousMode(slidingMode, id);
            if (null == prevMode || !prevMode.equals(modeToRestore)) continue;
            int prevIndex = this.model.getModeTopComponentPreviousIndex(slidingMode, id);
            this.moveTopComponentIntoMode(prevMode, tc, prevIndex);
        }
        if (null != tcToSelect) {
            modeToRestore.setSelectedTopComponent(tcToSelect);
        }
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(null, 48, null, null));
        }
        this.setActiveMode(modeToRestore);
    }

    @Override
    public void userUndockedTopComponent(TopComponent tc, ModeImpl mode) {
        Point tcLoc = tc.getLocation();
        Dimension tcSize = tc.getSize();
        SwingUtilities.convertPointToScreen(tcLoc, (Component)tc);
        Rectangle bounds = new Rectangle(tcLoc, tcSize);
        int modeKind = mode.getKind();
        if (modeKind == 2) {
            modeKind = 0;
        }
        if (this.getCurrentMaximizedMode() == mode && mode.getOpenedTopComponents().size() == 1 && mode.getOpenedTopComponents().get(0) == tc) {
            this.switchMaximizedMode(null);
        }
        ModeImpl newMode = this.createFloatingMode(bounds, modeKind);
        this.moveTopComponentIntoMode(newMode, tc);
        this.updateViewAfterDnD(false);
        WindowManagerImpl.getInstance().doFirePropertyChange("modes", null, null);
    }

    @Override
    public void userDockedTopComponent(TopComponent tc, ModeImpl mode) {
        SplitConstraint[] constraints;
        ModeImpl dockTo = null;
        String tcID = WindowManagerImpl.getInstance().findTopComponentID(tc);
        ModeImpl source = (ModeImpl)WindowManagerImpl.getInstance().findMode(tc);
        dockTo = this.model.getModeTopComponentPreviousMode(source, tcID);
        int dockIndex = this.model.getModeTopComponentPreviousIndex(source, tcID);
        int modeKind = mode.getKind();
        if (!(dockTo != null && this.model.getModes().contains(dockTo) && dockTo.getState() != 1 || (constraints = this.model.getModeTopComponentPreviousConstraints(source, tcID)) == null || null != (dockTo = this.findJoinedMode(modeKind, constraints)))) {
            dockTo = WindowManagerImpl.getInstance().createModeImpl(ModeImpl.getUnusedModeName(), modeKind, false);
            this.model.addMode(dockTo, constraints);
        }
        if (dockTo == null) {
            dockTo = modeKind == 1 ? WindowManagerImpl.getInstance().getDefaultEditorMode() : WindowManagerImpl.getInstance().getDefaultViewMode();
        }
        this.moveTopComponentIntoMode(dockTo, tc, dockIndex);
        this.updateViewAfterDnD(false);
        WindowManagerImpl.getInstance().doFirePropertyChange("modes", null, null);
    }

    void userStartedKeyboardDragAndDrop(TopComponentDraggable draggable) {
        this.viewRequestor.userStartedKeyboardDragAndDrop(draggable);
    }

    private ModeImpl findJoinedMode(int modeKind, SplitConstraint[] constraints) {
        for (ModeImpl m : this.getModes()) {
            SplitConstraint[] modeConstraints;
            if (m.getKind() != modeKind || m.getState() != 0 || (modeConstraints = m.getConstraints()).length != constraints.length) continue;
            boolean match = true;
            for (int i = 0; i < constraints.length; ++i) {
                if (constraints[i].orientation == modeConstraints[i].orientation && constraints[i].index == modeConstraints[i].index) continue;
                match = false;
                break;
            }
            if (!match) continue;
            return m;
        }
        return null;
    }

    private boolean moveTopComponentIntoMode(ModeImpl mode, TopComponent tc) {
        return this.moveTopComponentIntoMode(mode, tc, -1);
    }

    private boolean moveTopComponentIntoMode(ModeImpl mode, TopComponent tc, int index) {
        int prevIndex;
        boolean moved = false;
        boolean intoSliding = mode.getKind() == 2;
        boolean intoSeparate = mode.getState() == 1;
        ModeImpl prevMode = null;
        String tcID = WindowManagerImpl.getInstance().findTopComponentID(tc);
        if (!mode.canContain(tc)) {
            return false;
        }
        TopComponentTracker.getDefault().add(tc, mode);
        for (ModeImpl m : this.model.getModes()) {
            if (!this.model.containsModeTopComponent(m, tc)) continue;
            if (m.getKind() == 2 || m.getState() == 1) {
                prevMode = this.model.getModeTopComponentPreviousMode(m, tcID);
                break;
            }
            prevMode = m;
            break;
        }
        if (!tc.isOpened()) {
            tc.open();
        }
        int n = prevIndex = prevMode != null && (intoSliding || intoSeparate) ? prevMode.getOpenedTopComponentsIDs().indexOf(tcID) : -1;
        if (this.removeTopComponentFromOtherModes(mode, tc)) {
            moved = true;
        }
        if (index > -1) {
            this.model.insertModeOpenedTopComponent(mode, tc, index);
        } else {
            this.model.addModeOpenedTopComponent(mode, tc);
        }
        if (prevMode != null && (intoSliding || intoSeparate)) {
            ModeImpl groupPrevMode;
            List<String> ids;
            if (intoSeparate && mode.isPermanent() && !(ids = mode.getTopComponentsIDs()).isEmpty() && null != (groupPrevMode = this.model.getModeTopComponentPreviousMode(mode, ids.get(0)))) {
                prevMode = groupPrevMode;
            }
            this.model.setModeTopComponentPreviousMode(mode, tcID, prevMode, prevIndex);
            this.model.setModeTopComponentPreviousConstraints(mode, tcID, this.model.getModeConstraints(prevMode));
        }
        if (!intoSliding) {
            this.model.setActiveMode(mode);
            this.model.setModeSelectedTopComponent(mode, tc);
        } else {
            this.sortSlidedOutTopComponentsByPrevModes(mode);
            if (prevMode != null && prevMode == this.getActiveMode() && prevMode.getOpenedTopComponents().isEmpty()) {
                this.setSomeModeActive();
            }
            if (mode.getBounds().width == 0 && mode.getBounds().height == 0) {
                mode.setBounds(tc.getBounds());
            }
        }
        return moved;
    }

    private void sortSlidedOutTopComponentsByPrevModes(ModeImpl slidingMode) {
        if (!Switches.isModeSlidingEnabled()) {
            return;
        }
        List<String> opened = slidingMode.getOpenedTopComponentsIDs();
    }

    private void updateViewAfterDnD(boolean unmaximize) {
        if (unmaximize) {
            this.switchMaximizedMode(null);
        }
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(null, 46, null, null));
            FloatingWindowTransparencyManager.getDefault().update();
        }
    }

    public void addSlidingMode(ModeImpl mode, ModeImpl original, String side, Map<String, Integer> slideInSizes) {
        ModeImpl targetMode = this.model.getSlidingMode(side);
        if (targetMode != null) {
            return;
        }
        targetMode = WindowManagerImpl.getInstance().createModeImpl(ModeImpl.getUnusedModeName(), 2, false);
        this.model.addSlidingMode(mode, side, slideInSizes);
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(null, 12, null, mode));
        }
        WindowManagerImpl.getInstance().doFirePropertyChange("modes", null, null);
    }

    @Override
    public void userEnabledAutoHide(TopComponent tc, ModeImpl source, String targetSide) {
        if (this.isViewMaximized()) {
            this.switchMaximizedMode(null);
        }
        String tcID = WindowManagerImpl.getInstance().findTopComponentID(tc);
        if (this.isEditorMaximized()) {
            this.setTopComponentDockedInMaximizedMode(tcID, false);
        }
        this.slide(tc, source, targetSide);
    }

    void slide(TopComponent tc, ModeImpl source, String targetSide) {
        ModeImpl targetMode = this.model.getSlidingMode(targetSide);
        if (targetMode == null) {
            targetMode = WindowManagerImpl.getInstance().createModeImpl(ModeImpl.getUnusedModeName(), 2, false);
            this.model.addSlidingMode(targetMode, targetSide, null);
            this.model.setModeBounds(targetMode, new Rectangle(tc.getBounds()));
        }
        ModeImpl oldActive = this.getActiveMode();
        this.moveTopComponentIntoMode(targetMode, tc);
        ModeImpl newActive = this.getActiveMode();
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(null, 47, null, null));
        }
        if (oldActive != newActive) {
            WindowManagerImpl.getInstance().doFirePropertyChange("activeMode", oldActive, newActive);
        }
        if (newActive != null) {
            WindowManagerImpl.notifyRegistryTopComponentActivated(newActive.getSelectedTopComponent());
        } else {
            WindowManagerImpl.notifyRegistryTopComponentActivated(null);
        }
    }

    @Override
    public void userResizedSlidingMode(ModeImpl mode, Rectangle rect) {
        this.model.setModeBounds(mode, new Rectangle(rect));
        String side = this.model.getSlidingModeConstraints(mode);
        this.model.setSlideInSize(side, mode.getSelectedTopComponent(), "bottom".equals(side) || "top".equals(side) ? rect.height : rect.width);
        if (null != mode.getSelectedTopComponent()) {
            String tcID = WindowManagerImpl.getInstance().findTopComponentID(mode.getSelectedTopComponent());
            this.model.setTopComponentMaximizedWhenSlidedIn(tcID, false);
        }
    }

    @Override
    public void userDisabledAutoHide(TopComponent tc, ModeImpl source) {
        if (this.isViewMaximized()) {
            this.switchMaximizedMode(null);
        }
        String tcID = WindowManagerImpl.getInstance().findTopComponentID(tc);
        if (this.isEditorMaximized()) {
            this.setTopComponentDockedInMaximizedMode(tcID, true);
        }
        this.unSlide(tc, source);
    }

    private ModeImpl unSlide(TopComponent tc, ModeImpl source) {
        SplitConstraint[] constraints;
        String tcID = WindowManagerImpl.getInstance().findTopComponentID(tc);
        ModeImpl targetMode = this.model.getModeTopComponentPreviousMode(source, tcID);
        int targetIndex = this.model.getModeTopComponentPreviousIndex(source, tcID);
        if (!(targetMode != null && this.model.getModes().contains(targetMode) || (constraints = this.model.getModeTopComponentPreviousConstraints(source, tcID)) == null)) {
            targetMode = WindowManagerImpl.getInstance().createModeImpl(ModeImpl.getUnusedModeName(), source.getKind(), false);
            this.model.addMode(targetMode, constraints);
        }
        if (targetMode == null) {
            targetMode = source.getKind() == 1 ? WindowManagerImpl.getInstance().getDefaultEditorMode() : WindowManagerImpl.getInstance().getDefaultViewMode();
        }
        this.moveTopComponentIntoMode(targetMode, tc, targetIndex);
        targetMode.setMinimized(false);
        if (source.isEmpty()) {
            this.model.removeMode(source);
        }
        if (this.isVisible()) {
            this.viewRequestor.scheduleRequest(new ViewRequest(null, 48, null, null));
        }
        WindowManagerImpl.getInstance().doFirePropertyChange("activeMode", null, this.getActiveMode());
        return targetMode;
    }

    void setTopComponentMinimized(TopComponent tc, boolean minimized) {
        if (!tc.isOpened()) {
            return;
        }
        if (this.isTopComponentMinimized(tc) == minimized) {
            return;
        }
        ModeImpl mode = (ModeImpl)WindowManagerImpl.getInstance().findMode(tc);
        if (null == mode || mode.getState() != 0) {
            return;
        }
        if (minimized) {
            this.slide(tc, mode, this.guessSlideSide(tc));
        } else {
            this.unSlide(tc, mode);
        }
        WindowManagerImpl.getInstance().doFirePropertyChange("modes", null, null);
    }

    boolean isTopComponentMinimized(TopComponent tc) {
        if (!tc.isOpened()) {
            return false;
        }
        ModeImpl mode = (ModeImpl)WindowManagerImpl.getInstance().findMode(tc);
        return null != mode && mode.getKind() == 2;
    }

    public ModeImpl getModeTopComponentPreviousMode(String tcID, ModeImpl currentSlidingMode) {
        return this.model.getModeTopComponentPreviousMode(currentSlidingMode, tcID);
    }

    public int getModeTopComponentPreviousIndex(String tcID, ModeImpl currentSlidingMode) {
        return this.model.getModeTopComponentPreviousIndex(currentSlidingMode, tcID);
    }

    public void setModeTopComponentPreviousMode(String tcID, ModeImpl currentSlidingMode, ModeImpl prevMode, int prevIndex) {
        this.model.setModeTopComponentPreviousMode(currentSlidingMode, tcID, prevMode, prevIndex);
    }

    Map<String, Integer> getSlideInSizes(String side) {
        return this.model.getSlideInSizes(side);
    }

    void setTopComponentDockedInMaximizedMode(String tcID, boolean docked) {
        if (docked) {
            this.model.getMaximizedDockingStatus().addDocked(tcID);
        } else {
            this.model.getMaximizedDockingStatus().addSlided(tcID);
        }
    }

    boolean isTopComponentDockedInMaximizedMode(String tcID) {
        return this.model.getMaximizedDockingStatus().isDocked(tcID);
    }

    void setTopComponentSlidedInDefaultMode(String tcID, boolean slided) {
        if (slided) {
            this.model.getDefaultDockingStatus().addSlided(tcID);
        } else {
            this.model.getDefaultDockingStatus().addDocked(tcID);
        }
    }

    boolean isTopComponentMaximizedWhenSlidedIn(String tcID) {
        return this.model.isTopComponentMaximizedWhenSlidedIn(tcID);
    }

    void setTopComponentMaximizedWhenSlidedIn(String tcID, boolean maximized) {
        this.model.setTopComponentMaximizedWhenSlidedIn(tcID, maximized);
    }

    void userToggledTopComponentSlideInMaximize(String tcID) {
        this.setTopComponentMaximizedWhenSlidedIn(tcID, !this.isTopComponentMaximizedWhenSlidedIn(tcID));
        if (this.isVisible()) {
            TopComponent tc = WindowManagerImpl.getInstance().findTopComponent(tcID);
            ModeImpl mode = WindowManagerImpl.getInstance().findModeForOpenedID(tcID);
            if (null != tc && null != mode && null != mode.getSide()) {
                this.viewRequestor.scheduleRequest(new ViewRequest((Object)tc, 67, null, mode.getSide()));
            }
        }
    }

    boolean isTopComponentSlidedInDefaultMode(String tcID) {
        return this.model.getDefaultDockingStatus().isSlided(tcID);
    }

    boolean isEditorMaximized() {
        return null != this.model.getEditorMaximizedMode();
    }

    boolean isViewMaximized() {
        return null != this.model.getViewMaximizedMode();
    }

    private void restoreViews(DockingStatus viewStatus) {
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        Set<? extends ModeImpl> modes = this.getModes();
        for (ModeImpl modeImpl2 : modes) {
            String tcID;
            List<TopComponent> views;
            if (modeImpl2.getState() == 1) continue;
            if (modeImpl2.getKind() == 0) {
                views = this.getModeOpenedTopComponents(modeImpl2);
                Collections.reverse(views);
                for (TopComponent tc : views) {
                    tcID = wm.findTopComponentID(tc);
                    if (!viewStatus.shouldSlide(tcID)) continue;
                    this.slide(tc, modeImpl2, this.guessSlideSide(tc));
                }
                continue;
            }
            if (modeImpl2.getKind() != 2) continue;
            views = this.getModeOpenedTopComponents(modeImpl2);
            Collections.reverse(views);
            for (TopComponent tc : views) {
                tcID = wm.findTopComponentID(tc);
                if (!viewStatus.shouldDock(tcID)) continue;
                this.unSlide(tc, modeImpl2);
            }
        }
        for (ModeImpl modeImpl : modes) {
            TopComponent prevActiveTc;
            if (modeImpl.getState() == 1 || modeImpl.getKind() != 0 || null == (prevActiveTc = modeImpl.getPreviousSelectedTopComponent())) continue;
            this.setModeSelectedTopComponent(modeImpl, prevActiveTc);
        }
    }

    private void slideAllViews() {
        List<TopComponent> views;
        HashMap<TopComponent, String> tc2slideSide = new HashMap<TopComponent, String>(30);
        Set<? extends ModeImpl> modes = this.getModes();
        for (ModeImpl modeImpl2 : modes) {
            if (modeImpl2.getKind() != 0 || modeImpl2 == this.getViewMaximizedMode() || modeImpl2.getState() == 1) continue;
            views = this.getModeOpenedTopComponents(modeImpl2);
            for (TopComponent tc : views) {
                tc2slideSide.put(tc, this.guessSlideSide(tc));
            }
        }
        for (ModeImpl modeImpl : modes) {
            if (modeImpl.getKind() != 0 || modeImpl == this.getViewMaximizedMode() || modeImpl.getState() == 1) continue;
            views = this.getModeOpenedTopComponents(modeImpl);
            Collections.reverse(views);
            for (TopComponent tc : views) {
                this.slide(tc, modeImpl, (String)tc2slideSide.get((Object)tc));
            }
        }
    }

    void newTabGroup(TopComponent tc) {
        ModeImpl currentMode = (ModeImpl)WindowManagerImpl.getInstance().findMode(tc);
        if (null == currentMode) {
            return;
        }
        ModeImpl newMode = this.attachModeToSide(currentMode, null, currentMode.getKind());
        this.moveTopComponentIntoMode(newMode, tc);
        this.updateViewAfterDnD(true);
        tc.requestActive();
        WindowManagerImpl.getInstance().doFirePropertyChange("modes", null, null);
    }

    void collapseTabGroup(ModeImpl mode) {
        ModeImpl neighbor = this.findClosestNeighbor(mode);
        if (null == neighbor) {
            return;
        }
        TopComponent selTC = mode.getSelectedTopComponent();
        this.mergeModes(mode, neighbor, -1);
        if (null != selTC) {
            selTC.requestActive();
        }
        this.updateViewAfterDnD(true);
        WindowManagerImpl.getInstance().doFirePropertyChange("modes", null, null);
    }

    private ModeImpl findClosestNeighbor(ModeImpl mode) {
        ArrayList<ModeImpl> modes = new ArrayList<ModeImpl>(this.model.getModes().size());
        ModeImpl inSplitLeftNeighbor = null;
        ModeImpl inSplitRightNeighbor = null;
        SplitConstraint[] sc = mode.getConstraints();
        int index = sc[sc.length - 1].index;
        for (ModeImpl m : this.model.getModes()) {
            if (mode == m || m.getKind() != mode.getKind() || m.getState() != mode.getState()) continue;
            SplitConstraint[] otherSc = m.getConstraints();
            if (this.sameSplit(sc, otherSc)) {
                int otherIndex = otherSc[sc.length - 1].index;
                if (index < otherIndex) {
                    if (null == inSplitLeftNeighbor || otherIndex > inSplitLeftNeighbor.getConstraints()[sc.length - 1].index) {
                        inSplitLeftNeighbor = m;
                    }
                } else if (null == inSplitRightNeighbor || otherIndex < inSplitRightNeighbor.getConstraints()[sc.length - 1].index) {
                    inSplitRightNeighbor = m;
                }
            }
            modes.add(m);
        }
        if (modes.isEmpty()) {
            return null;
        }
        if (null != inSplitLeftNeighbor) {
            return inSplitLeftNeighbor;
        }
        if (null != inSplitRightNeighbor) {
            return inSplitRightNeighbor;
        }
        Collections.sort(modes, new Comparator<ModeImpl>(){

            @Override
            public int compare(ModeImpl o1, ModeImpl o2) {
                SplitConstraint[] sc1 = o1.getConstraints();
                SplitConstraint[] sc2 = o2.getConstraints();
                return sc1.length - sc2.length;
            }
        });
        return (ModeImpl)modes.get(0);
    }

    private boolean sameSplit(SplitConstraint[] sc1, SplitConstraint[] sc2) {
        if (sc1.length != sc2.length) {
            return false;
        }
        for (int i = 0; i < sc1.length - 1; ++i) {
            if (sc1[i].orientation == sc2[i].orientation && sc1[i].index == sc2[i].index) continue;
            return false;
        }
        return sc1[sc1.length - 1].orientation == sc2[sc2.length - 1].orientation;
    }

}

