/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.util.WeakSet
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 */
package org.netbeans.core.windows;

import java.awt.Component;
import java.awt.Window;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.MenuElement;
import javax.swing.MenuSelectionManager;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.Debug;
import org.openide.nodes.Node;
import org.openide.util.WeakSet;
import org.openide.windows.TopComponent;

public final class RegistryImpl
implements TopComponent.Registry {
    private WeakReference<TopComponent> activatedTopComponent = new WeakReference<Object>(null);
    private WeakReference<TopComponent> previousActivated;
    private final Set<TopComponent> openSet = new WeakSet(30);
    private Node[] currentNodes;
    private Node[] activatedNodes;
    private final PropertyChangeSupport support;
    private static final boolean DEBUG = Debug.isLoggable(RegistryImpl.class);

    public RegistryImpl() {
        this.support = new PropertyChangeSupport(this);
    }

    public synchronized Set<TopComponent> getOpened() {
        return new SyncSet();
    }

    public TopComponent getActivated() {
        return this.activatedTopComponent.get();
    }

    public Node[] getCurrentNodes() {
        return this.currentNodes;
    }

    public Node[] getActivatedNodes() {
        return this.activatedNodes == null ? new Node[]{} : this.activatedNodes;
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.support.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.support.removePropertyChangeListener(l);
    }

    void topComponentActivated(TopComponent tc) {
        if (this.activatedTopComponent.get() == tc && this.activatedNodes != null) {
            return;
        }
        final TopComponent old = this.activatedTopComponent.get();
        if (old != null && old.getActivatedNodes() != null) {
            this.previousActivated = new WeakReference<TopComponent>(old);
        }
        this.activatedTopComponent = new WeakReference<TopComponent>(tc);
        final TopComponent tmp = this.activatedTopComponent.get();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                RegistryImpl.this.doFirePropertyChange("activated", (Object)old, (Object)tmp);
            }
        });
        this.selectedNodesChanged(tmp, tmp == null ? null : tmp.getActivatedNodes());
    }

    public synchronized void topComponentOpened(TopComponent tc) {
        assert (null != tc);
        if (this.openSet.contains((Object)tc)) {
            return;
        }
        HashSet<TopComponent> old = new HashSet<TopComponent>(this.openSet);
        this.openSet.add(tc);
        this.doFirePropertyChange("tcOpened", null, (Object)tc);
        this.doFirePropertyChange("opened", old, new HashSet<TopComponent>(this.openSet));
    }

    public synchronized void topComponentClosed(TopComponent tc) {
        Object[] closedNodes;
        if (!this.openSet.contains((Object)tc)) {
            return;
        }
        HashSet<TopComponent> old = new HashSet<TopComponent>(this.openSet);
        this.openSet.remove((Object)tc);
        this.doFirePropertyChange("tcClosed", null, (Object)tc);
        this.doFirePropertyChange("opened", old, new HashSet<TopComponent>(this.openSet));
        if (this.activatedNodes != null && (closedNodes = tc.getActivatedNodes()) != null && Arrays.equals(closedNodes, this.activatedNodes)) {
            this.activatedNodes = null;
            this.doFirePropertyChange("activatedNodes", closedNodes, null);
        }
    }

    public synchronized void addTopComponent(TopComponent tc) {
        assert (null != tc);
        this.openSet.add(tc);
    }

    public void selectedNodesChanged(TopComponent tc, Node[] newNodes) {
        Object[] oldNodes = this.currentNodes;
        if (tc != this.activatedTopComponent.get() && this.activatedNodes != null && !this.isProperPrevious(tc, newNodes)) {
            return;
        }
        if (Arrays.equals(oldNodes, (Object[])newNodes) && this.activatedNodes != null) {
            return;
        }
        this.currentNodes = newNodes == null ? null : (Node[])newNodes.clone();
        this.tryFireChanges((Node[])oldNodes, this.currentNodes);
    }

    private boolean isProperPrevious(TopComponent tc, Node[] newNodes) {
        if (this.previousActivated == null || newNodes == null) {
            return false;
        }
        TopComponent previousTC = this.previousActivated.get();
        if (previousTC == null || !previousTC.equals((Object)tc)) {
            return false;
        }
        TopComponent tmp = this.activatedTopComponent.get();
        return tmp != null && tmp.getActivatedNodes() == null;
    }

    public static void cancelMenu(Window window) {
        MenuSelectionManager msm = MenuSelectionManager.defaultManager();
        MenuElement[] path = msm.getSelectedPath();
        for (int i = 0; i < path.length; ++i) {
            Window w = SwingUtilities.windowForComponent(path[i].getComponent());
            if (w == null || w != window && w.getOwner() != window) continue;
            return;
        }
        if (path.length > 0) {
            msm.clearSelectedPath();
        }
    }

    private void tryFireChanges(Node[] oldNodes, Node[] newNodes) {
        this.doFirePropertyChange("currentNodes", oldNodes, newNodes);
        if (newNodes == null && this.activatedNodes == null) {
            newNodes = new Node[]{};
        }
        if (newNodes != null) {
            oldNodes = this.activatedNodes;
            this.activatedNodes = newNodes;
            this.support.firePropertyChange("activatedNodes", oldNodes, this.activatedNodes);
        }
    }

    private void doFirePropertyChange(final String propName, final Object oldValue, final Object newValue) {
        if (DEBUG) {
            RegistryImpl.debugLog("");
            RegistryImpl.debugLog("Scheduling event firing: propName=" + propName);
            RegistryImpl.debugLog("\toldValue=" + (oldValue instanceof Object[] ? Arrays.asList((Object[])oldValue) : oldValue));
            RegistryImpl.debugLog("\tnewValue=" + (newValue instanceof Object[] ? Arrays.asList((Object[])newValue) : newValue));
        }
        if (SwingUtilities.isEventDispatchThread()) {
            this.support.firePropertyChange(propName, oldValue, newValue);
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    RegistryImpl.this.support.firePropertyChange(propName, oldValue, newValue);
                }
            });
        }
    }

    void clear() {
        this.activatedTopComponent.clear();
        this.openSet.clear();
        this.currentNodes = null;
        this.activatedNodes = null;
    }

    private static void debugLog(String message) {
        Debug.log(RegistryImpl.class, message);
    }

    private final class SyncSet
    implements Set<TopComponent> {
        private SyncSet() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public int size() {
            RegistryImpl registryImpl = RegistryImpl.this;
            synchronized (registryImpl) {
                return RegistryImpl.this.openSet.size();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public boolean isEmpty() {
            RegistryImpl registryImpl = RegistryImpl.this;
            synchronized (registryImpl) {
                return RegistryImpl.this.openSet.isEmpty();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public boolean contains(Object o) {
            RegistryImpl registryImpl = RegistryImpl.this;
            synchronized (registryImpl) {
                return RegistryImpl.this.openSet.contains(o);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public Iterator<TopComponent> iterator() {
            RegistryImpl registryImpl = RegistryImpl.this;
            synchronized (registryImpl) {
                return new HashSet(RegistryImpl.this.openSet).iterator();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public Object[] toArray() {
            RegistryImpl registryImpl = RegistryImpl.this;
            synchronized (registryImpl) {
                return RegistryImpl.this.openSet.toArray();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public <T> T[] toArray(T[] a) {
            RegistryImpl registryImpl = RegistryImpl.this;
            synchronized (registryImpl) {
                return RegistryImpl.this.openSet.toArray(a);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public boolean containsAll(Collection<?> c) {
            RegistryImpl registryImpl = RegistryImpl.this;
            synchronized (registryImpl) {
                return RegistryImpl.this.openSet.containsAll(c);
            }
        }

        @Override
        public boolean add(TopComponent e) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean remove(Object o) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean addAll(Collection<? extends TopComponent> c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void clear() {
            throw new UnsupportedOperationException();
        }
    }

}

