/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.services;

import javax.swing.JDialog;

public abstract class PresenterDecorator {
    public abstract void customizePresenter(JDialog var1);
}

