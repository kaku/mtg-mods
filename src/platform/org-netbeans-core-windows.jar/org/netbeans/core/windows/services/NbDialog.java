/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.NotifyDescriptor
 *  org.openide.util.HelpCtx
 */
package org.netbeans.core.windows.services;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.ActionListener;
import org.netbeans.core.windows.services.NbPresenter;
import org.openide.DialogDescriptor;
import org.openide.NotifyDescriptor;
import org.openide.util.HelpCtx;

final class NbDialog
extends NbPresenter {
    static final long serialVersionUID = -4508637164126678997L;

    public NbDialog(DialogDescriptor d, Frame owner) {
        super((NotifyDescriptor)d, owner, d.isModal());
    }

    public NbDialog(DialogDescriptor d, Dialog owner) {
        super((NotifyDescriptor)d, owner, d.isModal());
    }

    @Override
    protected HelpCtx getHelpCtx() {
        return ((DialogDescriptor)this.descriptor).getHelpCtx();
    }

    @Override
    protected int getOptionsAlign() {
        return ((DialogDescriptor)this.descriptor).getOptionsAlign();
    }

    @Override
    protected ActionListener getButtonListener() {
        return ((DialogDescriptor)this.descriptor).getButtonListener();
    }

    @Override
    protected Object[] getClosingOptions() {
        return ((DialogDescriptor)this.descriptor).getClosingOptions();
    }
}

