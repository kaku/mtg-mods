/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.ExplorerUtils
 *  org.openide.explorer.view.BeanTreeView
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeAcceptor
 *  org.openide.nodes.NodeAdapter
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeOperation
 *  org.openide.util.HelpCtx
 *  org.openide.util.HelpCtx$Provider
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 *  org.openide.util.NbBundle
 *  org.openide.util.UserCancelException
 *  org.openide.util.WeakSet
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.windows.services;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.FocusTraversalPolicy;
import java.awt.LayoutManager;
import java.awt.Window;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashSet;
import java.util.Set;
import java.util.WeakHashMap;
import javax.accessibility.AccessibleContext;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import org.netbeans.core.windows.services.FileSelector;
import org.netbeans.core.windows.services.NbPresenter;
import org.netbeans.core.windows.view.ui.NbSheet;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.BeanTreeView;
import org.openide.nodes.Node;
import org.openide.nodes.NodeAcceptor;
import org.openide.nodes.NodeAdapter;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeOperation;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.UserCancelException;
import org.openide.util.WeakSet;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public final class NodeOperationImpl
extends NodeOperation {
    private static WeakSet<Node[]> nodeCache = new WeakSet();
    private static WeakHashMap<Node[], Dialog> dialogCache = new WeakHashMap();

    public void explore(final Node n) {
        Mutex.EVENT.readAccess(new Runnable(){

            @Override
            public void run() {
                ExplorerPanel et = new ExplorerPanel(n);
                Mode target = WindowManager.getDefault().findMode("explorer");
                if (target != null) {
                    target.dockInto((TopComponent)et);
                }
                et.open();
                et.requestActive();
            }
        });
    }

    public boolean customize(Node n) {
        final Component customizer = n.getCustomizer();
        if (customizer == null) {
            return false;
        }
        return (Boolean)Mutex.EVENT.readAccess((Mutex.Action)new Mutex.Action<Boolean>(){

            public Boolean run() {
                if (customizer instanceof NbPresenter) {
                    ((NbPresenter)customizer).pack();
                    ((NbPresenter)customizer).show();
                    return Boolean.TRUE;
                }
                if (customizer instanceof Window) {
                    ((Window)customizer).pack();
                    customizer.setVisible(true);
                    return Boolean.TRUE;
                }
                HelpFwdPanel p = null;
                p = customizer instanceof ExplorerManager.Provider ? new ExplorerProviderFwd(customizer, (ExplorerManager.Provider)customizer) : new HelpFwdPanel(customizer);
                p.setLayout(new BorderLayout());
                p.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(NodeOperationImpl.class, (String)"CTL_Customizer_dialog_title"));
                if (customizer.getClass().getName().startsWith("org.netbeans.modules.xml.catalog")) {
                    p.setBorder(BorderFactory.createEmptyBorder(12, 12, 0, 11));
                }
                p.add(customizer, "Center");
                DialogDescriptor dd = new DialogDescriptor((Object)p, NbBundle.getMessage(NodeOperationImpl.class, (String)"CTL_Customizer_dialog_title"));
                dd.setOptions(new Object[]{DialogDescriptor.CLOSED_OPTION});
                Dialog dialog = DialogDisplayer.getDefault().createDialog(dd);
                dialog.pack();
                dialog.setVisible(true);
                return Boolean.TRUE;
            }
        });
    }

    public void showProperties(Node n) {
        Dialog d = NodeOperationImpl.findCachedPropertiesDialog(n);
        if (null == d) {
            Node[] nds = new Node[]{n};
            NodeOperationImpl.openProperties(new NbSheet(), nds);
        } else {
            d.setVisible(true);
            NbSheet sheet = this.findCachedSheet(d);
            if (null != sheet) {
                sheet.setNodes(new Node[]{n});
            }
            d.toFront();
            FocusTraversalPolicy ftp = d.getFocusTraversalPolicy();
            if (null != ftp && null != ftp.getDefaultComponent(d)) {
                ftp.getDefaultComponent(d).requestFocusInWindow();
            } else {
                d.requestFocusInWindow();
            }
        }
    }

    public void showProperties(Node[] nodes) {
        Dialog d = NodeOperationImpl.findCachedPropertiesDialog(nodes);
        if (null == d) {
            NodeOperationImpl.openProperties(new NbSheet(), nodes);
        } else {
            d.setVisible(true);
            NbSheet sheet = this.findCachedSheet(d);
            if (null != sheet) {
                sheet.setNodes(nodes);
            }
            d.toFront();
            FocusTraversalPolicy ftp = d.getFocusTraversalPolicy();
            if (null != ftp && null != ftp.getDefaultComponent(d)) {
                ftp.getDefaultComponent(d).requestFocusInWindow();
            } else {
                d.requestFocusInWindow();
            }
        }
    }

    private NbSheet findCachedSheet(Container c) {
        NbSheet res = null;
        int childrenCount = c.getComponentCount();
        for (int i = 0; i < childrenCount && res == null; ++i) {
            Component child = c.getComponent(i);
            if (child instanceof NbSheet) {
                res = (NbSheet)((Object)child);
                continue;
            }
            if (!(child instanceof Container)) continue;
            res = this.findCachedSheet((Container)child);
        }
        return res;
    }

    private static Dialog findCachedPropertiesDialog(Node n) {
        return NodeOperationImpl.findCachedPropertiesDialog(new Node[]{n});
    }

    private static Dialog findCachedPropertiesDialog(Node[] nodes) {
        for (Node[] cached : nodeCache) {
            if (cached.length != nodes.length) continue;
            boolean match = true;
            for (int i = 0; i < cached.length; ++i) {
                if (cached[i].equals((Object)nodes[i])) continue;
                match = false;
                break;
            }
            if (!match) continue;
            return dialogCache.get(cached);
        }
        return null;
    }

    public Node[] select(String title, String rootTitle, Node root, NodeAcceptor acceptor, Component top) throws UserCancelException {
        FileSelector selector = new FileSelector(rootTitle, root, acceptor, top);
        selector.setBorder(new EmptyBorder(12, 12, 0, 12));
        DialogDescriptor dd = new DialogDescriptor((Object)selector, title, true, selector.getOptions(), selector.getSelectOption(), 0, HelpCtx.DEFAULT_HELP, null);
        Object ret = DialogDisplayer.getDefault().notify((NotifyDescriptor)dd);
        if (ret != selector.getSelectOption()) {
            throw new UserCancelException();
        }
        return selector.getNodes();
    }

    private static void openProperties(final NbSheet sheet, final Node[] nds) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                boolean modal = NbPresenter.currentModalDialog != null;
                Dialog dlg = DialogDisplayer.getDefault().createDialog(new DialogDescriptor((Object)sheet, sheet.getName(), modal, new Object[]{DialogDescriptor.CLOSED_OPTION}, DialogDescriptor.CLOSED_OPTION, 0, new HelpCtx("org.netbeans.core.windows.view.ui.NbSheet"), null));
                sheet.setNodes(nds);
                SheetNodesListener listener = new SheetNodesListener(dlg, sheet);
                listener.attach(nds);
                nodeCache.add((Object)nds);
                dialogCache.put(nds, dlg);
                dlg.setVisible(true);
            }
        });
    }

    private static class SheetNodesListener
    extends NodeAdapter
    implements PropertyChangeListener {
        private Dialog dialog;
        private Set<Node> listenerSet;
        private TopComponent tc;

        SheetNodesListener(Dialog dialog, TopComponent tc) {
            this.dialog = dialog;
            this.tc = tc;
            tc.addPropertyChangeListener((PropertyChangeListener)this);
        }

        @Override
        public void propertyChange(PropertyChangeEvent pce) {
            if ("name".equals(pce.getPropertyName())) {
                this.dialog.setTitle((String)pce.getNewValue());
            }
        }

        public void attach(Node[] nodes) {
            this.listenerSet = new HashSet<Node>(nodes.length * 2);
            for (int i = 0; i < nodes.length; ++i) {
                this.listenerSet.add(nodes[i]);
                nodes[i].addNodeListener((NodeListener)this);
            }
        }

        public void nodeDestroyed(NodeEvent ev) {
            Node destroyedNode = ev.getNode();
            destroyedNode.removeNodeListener((NodeListener)this);
            this.listenerSet.remove((Object)destroyedNode);
            if (this.listenerSet.isEmpty()) {
                this.tc.removePropertyChangeListener((PropertyChangeListener)this);
                Mutex.EVENT.readAccess(new Runnable(){

                    @Override
                    public void run() {
                        if (SheetNodesListener.this.dialog != null) {
                            SheetNodesListener.this.dialog.setVisible(false);
                            SheetNodesListener.this.dialog.dispose();
                            SheetNodesListener.this.dialog = null;
                        }
                    }
                });
            }
        }

    }

    private static final class ExplorerProviderFwd
    extends HelpFwdPanel
    implements ExplorerManager.Provider {
        private ExplorerManager.Provider explProvider;

        private ExplorerProviderFwd(Component innerComp, ExplorerManager.Provider explProvider) {
            super(innerComp);
            this.explProvider = explProvider;
        }

        public ExplorerManager getExplorerManager() {
            return this.explProvider.getExplorerManager();
        }
    }

    private static class HelpFwdPanel
    extends JPanel
    implements HelpCtx.Provider {
        private Component innerComp;
        private boolean active = false;

        private HelpFwdPanel(Component innerComp) {
            this.innerComp = innerComp;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public HelpCtx getHelpCtx() {
            try {
                if (this.active) {
                    HelpCtx helpCtx = null;
                    return helpCtx;
                }
                this.active = true;
                HelpCtx helpCtx = HelpCtx.findHelp((Component)this.innerComp);
                return helpCtx;
            }
            finally {
                this.active = false;
            }
        }
    }

    private static class ExplorerPanel
    extends TopComponent
    implements ExplorerManager.Provider {
        private ExplorerManager manager = new ExplorerManager();

        public ExplorerPanel(Node n) {
            this.manager.setRootContext(n);
            ActionMap map = this.getActionMap();
            map.put("copy-to-clipboard", ExplorerUtils.actionCopy((ExplorerManager)this.manager));
            map.put("cut-to-clipboard", ExplorerUtils.actionCut((ExplorerManager)this.manager));
            map.put("paste-from-clipboard", ExplorerUtils.actionPaste((ExplorerManager)this.manager));
            map.put("delete", ExplorerUtils.actionDelete((ExplorerManager)this.manager, (boolean)true));
            this.associateLookup(ExplorerUtils.createLookup((ExplorerManager)this.manager, (ActionMap)map));
            this.setLayout((LayoutManager)new BorderLayout());
            this.add((Component)new BeanTreeView());
            this.setName(n.getDisplayName());
        }

        public ExplorerManager getExplorerManager() {
            return this.manager;
        }

        public void addNotify() {
            super.addNotify();
            ExplorerUtils.activateActions((ExplorerManager)this.manager, (boolean)true);
        }

        public void removeNotify() {
            ExplorerUtils.activateActions((ExplorerManager)this.manager, (boolean)false);
            super.removeNotify();
        }

        public int getPersistenceType() {
            return 2;
        }
    }

}

