/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.InstanceCookie$Of
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataShadow
 *  org.openide.loaders.LoaderTransfer
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.datatransfer.PasteType
 */
package org.netbeans.core.windows.services;

import java.awt.datatransfer.Transferable;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import javax.swing.Action;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataShadow;
import org.openide.loaders.LoaderTransfer;
import org.openide.nodes.Node;
import org.openide.util.datatransfer.PasteType;

final class ActionPasteType {
    ActionPasteType() {
    }

    static PasteType getPasteType(DataFolder targetFolder, Transferable transfer) {
        PasteTypeImpl retVal;
        block3 : {
            FileObject folder = targetFolder.getPrimaryFile();
            retVal = null;
            try {
                if (!folder.getFileSystem().isDefault()) break block3;
                int[] pasteOperations = new int[]{1, 4};
                for (int i = 0; i < pasteOperations.length; ++i) {
                    DataObject[] dataObjects = LoaderTransfer.getDataObjects((Transferable)transfer, (int)pasteOperations[i]);
                    if (dataObjects == null || !ActionPasteType.canBePasted(dataObjects, targetFolder, pasteOperations[i])) continue;
                    retVal = new PasteTypeImpl(Arrays.asList(dataObjects), targetFolder, pasteOperations[i]);
                    break;
                }
            }
            catch (FileStateInvalidException e) {
                // empty catch block
            }
        }
        return retVal;
    }

    private static boolean canBePasted(DataObject[] dataObjects, DataFolder targetFolder, int operation) throws FileStateInvalidException {
        HashSet<DataObject> pasteableDataObjects = new HashSet<DataObject>();
        FileObject folder = targetFolder.getPrimaryFile();
        DataObject[] folderChildren = targetFolder.getChildren();
        for (int j = 0; j < dataObjects.length; ++j) {
            boolean isCutPaste;
            DataObject dataObject = dataObjects[j];
            FileObject fo = dataObject.getPrimaryFile();
            if (!ActionPasteType.isAction(dataObject) || !fo.getFileSystem().isDefault()) break;
            boolean isCopyPaste = operation == 1 && dataObject.isCopyAllowed();
            boolean bl = isCutPaste = operation == 4 && dataObject.isMoveAllowed() && fo.getParent() != folder;
            if (!isCopyPaste && !isCutPaste) continue;
            boolean isDuplicate = false;
            for (int i = 0; i < folderChildren.length; ++i) {
                if (0 != folderChildren[i].getName().compareTo(dataObject.getName())) continue;
                isDuplicate = true;
                break;
            }
            if (isDuplicate) continue;
            pasteableDataObjects.add(dataObject);
        }
        return pasteableDataObjects.size() == dataObjects.length;
    }

    private static boolean isAction(DataObject dataObject) {
        boolean retVal = false;
        InstanceCookie.Of ic = (InstanceCookie.Of)dataObject.getCookie(InstanceCookie.Of.class);
        if (ic != null && ic.instanceOf(Action.class)) {
            retVal = true;
        }
        return retVal;
    }

    private static final class PasteTypeImpl
    extends PasteType {
        private final DataFolder targetFolder;
        private final Collection sourceDataObjects;
        private final int pasteOperation;

        private PasteTypeImpl(Collection sourceDataObjects, DataFolder targetFolder, int pasteOperation) {
            this.targetFolder = targetFolder;
            this.sourceDataObjects = sourceDataObjects;
            this.pasteOperation = pasteOperation;
        }

        public Transferable paste() throws IOException {
            if (this.targetFolder != null) {
                for (DataObject dataObject : this.sourceDataObjects) {
                    boolean isValid;
                    boolean bl = isValid = dataObject != null && dataObject.isValid();
                    if (isValid && this.pasteOperation == 1) {
                        dataObject.createShadow(this.targetFolder);
                    }
                    if (!isValid || this.pasteOperation != 4) continue;
                    dataObject.move(this.targetFolder);
                }
            }
            return null;
        }
    }

}

