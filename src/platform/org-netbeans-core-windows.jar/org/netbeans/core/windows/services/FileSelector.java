/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.view.BeanTreeView
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeAcceptor
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.core.windows.services;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.util.ResourceBundle;
import javax.accessibility.AccessibleContext;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.JViewport;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.tree.TreeCellEditor;
import org.openide.awt.Mnemonics;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.BeanTreeView;
import org.openide.nodes.Node;
import org.openide.nodes.NodeAcceptor;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

final class FileSelector
extends JPanel
implements PropertyChangeListener,
ExplorerManager.Provider {
    static final long serialVersionUID = 6524404012203099065L;
    private final ExplorerManager manager = new ExplorerManager();
    private BeanTreeView tree;
    private JButton okButton;
    private JButton cancelButton;
    private JButton[] buttons;
    private NodeAcceptor acceptor;

    public FileSelector(String rootLabel, Node root, NodeAcceptor acceptor, Component top) {
        this.acceptor = acceptor;
        ResourceBundle bundle = NbBundle.getBundle(FileSelector.class);
        this.okButton = new JButton(bundle.getString("CTL_FileSelectorOkButton"));
        this.cancelButton = new JButton(bundle.getString("CTL_FileSelectorCancelButton"));
        this.okButton.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_FileSelectorOkButton"));
        this.cancelButton.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_FileSelectorCancelButton"));
        this.buttons = new JButton[]{this.okButton, this.cancelButton};
        this.manager.setRootContext(root);
        this.tree = new BeanTreeView(){};
        this.tree.setPopupAllowed(false);
        this.tree.setDefaultActionAllowed(false);
        this.tree.setBorder((Border)UIManager.get("Nb.ScrollPane.border"));
        this.tree.getAccessibleContext().setAccessibleName(NbBundle.getBundle(FileSelector.class).getString("ACSN_FileSelectorTreeView"));
        this.tree.getAccessibleContext().setAccessibleDescription(NbBundle.getBundle(FileSelector.class).getString("ACSD_FileSelectorTreeView"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getBundle(FileSelector.class).getString("ACSD_FileSelectorDialog"));
        this.setLayout(new BorderLayout());
        this.add((Component)this.tree, "Center");
        try {
            this.manager.setSelectedNodes(new Node[]{root});
            JLabel label = new JLabel();
            Mnemonics.setLocalizedText((JLabel)label, (String)rootLabel);
            label.setLabelFor(this.tree.getViewport().getView());
            this.add((Component)label, "North");
        }
        catch (PropertyVetoException pve) {
            throw new IllegalStateException(pve.getMessage());
        }
        if (top != null) {
            this.add(top, "South");
        }
        this.manager.addPropertyChangeListener((PropertyChangeListener)this);
        if (acceptor.acceptNodes(this.manager.getSelectedNodes())) {
            this.enableButton();
        } else {
            this.disableButton();
        }
    }

    Object[] getOptions() {
        return this.buttons;
    }

    Object getSelectOption() {
        return this.okButton;
    }

    @Override
    public void propertyChange(PropertyChangeEvent ev) {
        if (ev.getPropertyName().equals("selectedNodes")) {
            if (this.acceptor.acceptNodes(this.manager.getSelectedNodes())) {
                this.enableButton();
            } else {
                this.disableButton();
            }
        }
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension dim = super.getPreferredSize();
        dim.height = Math.max(dim.height, Utilities.getUsableScreenBounds().height / 2);
        return dim;
    }

    public Node[] getNodes() {
        return this.manager.getSelectedNodes();
    }

    void enableButton() {
        this.okButton.setEnabled(true);
    }

    void disableButton() {
        this.okButton.setEnabled(false);
    }

    public ExplorerManager getExplorerManager() {
        return this.manager;
    }

}

