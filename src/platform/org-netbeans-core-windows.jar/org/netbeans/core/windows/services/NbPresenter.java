/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.NotificationLineSupport
 *  org.openide.NotifyDescriptor
 *  org.openide.WizardDescriptor
 *  org.openide.awt.EqualFlowLayout
 *  org.openide.awt.Mnemonics
 *  org.openide.util.ChangeSupport
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 */
package org.netbeans.core.windows.services;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.DefaultKeyboardFocusManager;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.ImageObserver;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.MenuElement;
import javax.swing.MenuSelectionManager;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.OptionPaneUI;
import javax.swing.plaf.basic.BasicLookAndFeel;
import javax.swing.plaf.basic.BasicOptionPaneUI;
import javax.swing.plaf.metal.MetalLookAndFeel;
import org.netbeans.core.windows.Constants;
import org.openide.DialogDescriptor;
import org.openide.NotificationLineSupport;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.awt.EqualFlowLayout;
import org.openide.awt.Mnemonics;
import org.openide.util.ChangeSupport;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;

class NbPresenter
extends JDialog
implements PropertyChangeListener,
WindowListener,
Mutex.Action<Void>,
Comparator<Object> {
    public static NbPresenter currentModalDialog;
    private static final ChangeSupport cs;
    private static Boolean isJava17;
    protected NotifyDescriptor descriptor;
    private final JButton stdYesButton = new JButton(NbBundle.getBundle(NbPresenter.class).getString("YES_OPTION_CAPTION"));
    private final JButton stdNoButton = new JButton(NbBundle.getBundle(NbPresenter.class).getString("NO_OPTION_CAPTION"));
    private final JButton stdOKButton = new JButton(NbBundle.getBundle(NbPresenter.class).getString("OK_OPTION_CAPTION"));
    private final JButton stdCancelButton = new JButton(NbBundle.getBundle(NbPresenter.class).getString("CANCEL_OPTION_CAPTION"));
    private final JButton stdClosedButton = new JButton(NbBundle.getBundle(NbPresenter.class).getString("CLOSED_OPTION_CAPTION"));
    private final JButton stdHelpButton = new JButton();
    private final JButton stdDetailButton = new JButton(NbBundle.getBundle(NbPresenter.class).getString("HELP_OPTION_CAPTION"));
    private static final String ESCAPE_COMMAND = "Escape";
    private Component currentMessage;
    private JScrollPane currentScrollPane;
    private boolean leaf;
    private JPanel currentButtonsPanel;
    private JLabel notificationLine;
    private Component[] currentPrimaryButtons;
    private Component[] currentSecondaryButtons;
    private static final int MSG_TYPE_ERROR = 1;
    private static final int MSG_TYPE_WARNING = 2;
    private static final int MSG_TYPE_INFO = 3;
    private Color nbErrorForeground;
    private Color nbWarningForeground;
    private Color nbInfoForeground;
    private int currentAlign;
    private ButtonListener buttonListener;
    private transient HelpCtx currentHelp;
    private transient boolean haveCalledInitializeButtons;
    static final Logger LOG;
    static final long serialVersionUID = -4508637164126678997L;
    private JButton initialDefaultButton;
    private final HackTypeAhead hack;
    static Field markers;
    static Method dequeue;

    public NbPresenter(NotifyDescriptor d, Frame owner, boolean modal) {
        super(owner, d.getTitle(), modal);
        this.stdYesButton.setDefaultCapable(true);
        this.stdOKButton.setDefaultCapable(true);
        this.stdNoButton.setDefaultCapable(true);
        this.stdNoButton.putClientProperty("defaultButton", Boolean.FALSE);
        this.stdCancelButton.setDefaultCapable(true);
        this.stdCancelButton.putClientProperty("defaultButton", Boolean.FALSE);
        this.stdCancelButton.setVerifyInputWhenFocusTarget(false);
        this.stdClosedButton.setDefaultCapable(true);
        this.stdClosedButton.putClientProperty("defaultButton", Boolean.FALSE);
        this.stdHelpButton.setDefaultCapable(true);
        this.stdHelpButton.putClientProperty("defaultButton", Boolean.FALSE);
        this.stdDetailButton.setDefaultCapable(true);
        this.stdDetailButton.putClientProperty("defaultButton", Boolean.FALSE);
        Mnemonics.setLocalizedText((AbstractButton)this.stdHelpButton, (String)NbBundle.getBundle(NbPresenter.class).getString("HELP_OPTION_CAPTION"));
        this.initAccessibility();
        this.leaf = false;
        this.currentHelp = null;
        this.haveCalledInitializeButtons = false;
        this.hack = new HackTypeAhead();
        this.initialize(d);
    }

    public NbPresenter(NotifyDescriptor d, Dialog owner, boolean modal) {
        super(owner, d.getTitle(), modal);
        this.stdYesButton.setDefaultCapable(true);
        this.stdOKButton.setDefaultCapable(true);
        this.stdNoButton.setDefaultCapable(true);
        this.stdNoButton.putClientProperty("defaultButton", Boolean.FALSE);
        this.stdCancelButton.setDefaultCapable(true);
        this.stdCancelButton.putClientProperty("defaultButton", Boolean.FALSE);
        this.stdCancelButton.setVerifyInputWhenFocusTarget(false);
        this.stdClosedButton.setDefaultCapable(true);
        this.stdClosedButton.putClientProperty("defaultButton", Boolean.FALSE);
        this.stdHelpButton.setDefaultCapable(true);
        this.stdHelpButton.putClientProperty("defaultButton", Boolean.FALSE);
        this.stdDetailButton.setDefaultCapable(true);
        this.stdDetailButton.putClientProperty("defaultButton", Boolean.FALSE);
        Mnemonics.setLocalizedText((AbstractButton)this.stdHelpButton, (String)NbBundle.getBundle(NbPresenter.class).getString("HELP_OPTION_CAPTION"));
        this.initAccessibility();
        this.leaf = false;
        this.currentHelp = null;
        this.haveCalledInitializeButtons = false;
        this.hack = new HackTypeAhead();
        this.initialize(d);
    }

    boolean isLeaf() {
        return this.leaf;
    }

    private void initAccessibility() {
        ResourceBundle bundle = NbBundle.getBundle(NbPresenter.class);
        this.stdYesButton.getAccessibleContext().setAccessibleName(bundle.getString("ACS_YES_OPTION_NAME"));
        this.stdYesButton.getAccessibleContext().setAccessibleDescription(bundle.getString("ACS_YES_OPTION_DESC"));
        this.stdNoButton.getAccessibleContext().setAccessibleName(bundle.getString("ACS_NO_OPTION_NAME"));
        this.stdNoButton.getAccessibleContext().setAccessibleDescription(bundle.getString("ACS_NO_OPTION_DESC"));
        this.stdOKButton.getAccessibleContext().setAccessibleName(bundle.getString("ACS_OK_OPTION_NAME"));
        this.stdOKButton.getAccessibleContext().setAccessibleDescription(bundle.getString("ACS_OK_OPTION_DESC"));
        this.stdCancelButton.getAccessibleContext().setAccessibleName(bundle.getString("ACS_CANCEL_OPTION_NAME"));
        this.stdCancelButton.getAccessibleContext().setAccessibleDescription(bundle.getString("ACS_CANCEL_OPTION_DESC"));
        this.stdClosedButton.getAccessibleContext().setAccessibleName(bundle.getString("ACS_CLOSED_OPTION_NAME"));
        this.stdClosedButton.getAccessibleContext().setAccessibleDescription(bundle.getString("ACS_CLOSED_OPTION_DESC"));
        this.stdHelpButton.getAccessibleContext().setAccessibleName(bundle.getString("ACS_HELP_OPTION_NAME"));
        this.stdHelpButton.getAccessibleContext().setAccessibleDescription(bundle.getString("ACS_HELP_OPTION_DESC"));
        this.stdDetailButton.getAccessibleContext().setAccessibleName(bundle.getString("ACS_HELP_OPTION_NAME"));
        this.stdDetailButton.getAccessibleContext().setAccessibleDescription(bundle.getString("ACS_HELP_OPTION_DESC"));
    }

    private void initialize(NotifyDescriptor d) {
        this.getRootPane().setOpaque(true);
        if (d instanceof WizardDescriptor || d.isNoDefaultClose()) {
            this.setDefaultCloseOperation(0);
        } else {
            this.setDefaultCloseOperation(2);
        }
        this.descriptor = d;
        this.buttonListener = new ButtonListener();
        this.leaf = d instanceof DialogDescriptor ? ((DialogDescriptor)d).isLeaf() : true;
        this.getRootPane().getInputMap(1).put(KeyStroke.getKeyStroke(27, 0), "Escape");
        this.getRootPane().getActionMap().put("Escape", new EscapeAction());
        this.initializePresenter();
        this.pack();
        this.initBounds();
    }

    private void requestFocusForMessage() {
        Component comp = this.currentMessage;
        if (comp == null) {
            return;
        }
        if (!(comp instanceof JComponent) || !((JComponent)comp).requestDefaultFocus()) {
            comp.requestFocus();
        }
    }

    private void initializeMessage() {
        Object newMessage = this.descriptor.getMessage();
        boolean isDefaultOptionPane = false;
        if (this.currentMessage == null || !this.currentMessage.equals(newMessage)) {
            this.uninitializeMessage();
            Component toAdd = null;
            if (this.descriptor.getMessageType() == -1 && newMessage instanceof Component) {
                this.currentMessage = (Component)newMessage;
            } else {
                this.currentMessage = this.createOptionPane();
                isDefaultOptionPane = true;
            }
            Dimension prefSize = this.currentMessage.getPreferredSize();
            final Rectangle screenBounds = Utilities.getUsableScreenBounds();
            if (prefSize.width > screenBounds.width - 100 || prefSize.height > screenBounds.height - 100) {
                this.currentScrollPane = new JScrollPane(){

                    @Override
                    public Dimension getPreferredSize() {
                        Dimension sz = new Dimension(super.getPreferredSize());
                        if (sz.width > screenBounds.width - 100) {
                            sz.width = screenBounds.width * 3 / 4;
                        }
                        if (sz.height > screenBounds.height - 100) {
                            sz.height = screenBounds.height * 3 / 4;
                        }
                        return sz;
                    }
                };
                this.currentScrollPane.setViewportView(this.currentMessage);
                toAdd = this.currentScrollPane;
            } else {
                toAdd = this.currentMessage;
            }
            if (!(this.descriptor instanceof WizardDescriptor) && this.descriptor.getNotificationLineSupport() != null) {
                JPanel enlargedToAdd = new JPanel(new BorderLayout());
                enlargedToAdd.add(toAdd, "Center");
                this.nbErrorForeground = UIManager.getColor("nb.errorForeground");
                if (this.nbErrorForeground == null) {
                    this.nbErrorForeground = new Color(255, 0, 0);
                }
                this.nbWarningForeground = UIManager.getColor("nb.warningForeground");
                if (this.nbWarningForeground == null) {
                    this.nbWarningForeground = new Color(51, 51, 51);
                }
                this.nbInfoForeground = UIManager.getColor("nb.warningForeground");
                if (this.nbInfoForeground == null) {
                    this.nbInfoForeground = UIManager.getColor("Label.foreground");
                }
                this.notificationLine = new FixedHeightLabel();
                NotificationLineSupport nls = this.descriptor.getNotificationLineSupport();
                if (nls.getInformationMessage() != null) {
                    this.updateNotificationLine(3, nls.getInformationMessage());
                } else if (nls.getWarningMessage() != null) {
                    this.updateNotificationLine(2, nls.getWarningMessage());
                } else if (nls.getErrorMessage() != null) {
                    this.updateNotificationLine(1, nls.getErrorMessage());
                }
                JPanel notificationPanel = new JPanel();
                GroupLayout layout = new GroupLayout(notificationPanel);
                notificationPanel.setLayout(layout);
                layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.notificationLine).addContainerGap(-1, 32767)));
                layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.notificationLine, 16, 16, 32767).addContainerGap(-1, 32767)));
                enlargedToAdd.add((Component)notificationPanel, "South");
                toAdd = enlargedToAdd;
            }
            this.getRootPane().putClientProperty("nb.default.option.pane", isDefaultOptionPane);
            this.getContentPane().add(toAdd, "Center");
        }
    }

    private void uninitializeMessage() {
        if (this.currentMessage != null) {
            if (this.currentScrollPane != null) {
                this.getContentPane().remove(this.currentScrollPane);
                this.currentScrollPane = null;
            } else {
                this.getContentPane().remove(this.currentMessage);
            }
            this.currentMessage = null;
        }
    }

    private void initializePresenter() {
        if (this.currentMessage != null) {
            return;
        }
        this.initialDefaultButton = this.getRootPane().getDefaultButton();
        this.initializeMessage();
        this.updateHelp();
        this.initializeButtons();
        this.haveCalledInitializeButtons = true;
        this.descriptor.addPropertyChangeListener((PropertyChangeListener)this);
        this.addWindowListener(this);
        this.initializeClosingOptions();
    }

    private void uninitializePresenter() {
        this.descriptor.removePropertyChangeListener((PropertyChangeListener)this);
        this.uninitializeMessage();
        this.uninitializeButtons();
        this.uninitializeClosingOptions();
        this.initialDefaultButton = null;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.initializePresenter();
        this.hack.activate();
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.uninitializePresenter();
    }

    private JOptionPane createOptionPane() {
        Object msg = this.descriptor.getMessage();
        boolean override = true;
        String strMsg = null;
        if (msg instanceof String) {
            msg = ((String)msg).replace("\t", "    ");
            strMsg = (String)(msg = ((String)msg).replace("\r", ""));
            String strMsgLower = strMsg.toLowerCase();
            boolean bl = override = !strMsgLower.startsWith("<html>");
        }
        if (msg instanceof Accessible) {
            strMsg = ((Accessible)msg).getAccessibleContext().getAccessibleDescription();
        }
        JOptionPane optionPane = override ? new JOptionPane(msg, this.descriptor.getMessageType(), 0, null, new Object[0], null){

            @Override
            public int getMaxCharactersPerLineCount() {
                return 100;
            }
        } : new JOptionPane(msg, this.descriptor.getMessageType(), 0, null, new Object[0], null);
        if (UIManager.getLookAndFeel().getClass() == MetalLookAndFeel.class || UIManager.getLookAndFeel().getClass() == BasicLookAndFeel.class) {
            optionPane.setUI(new BasicOptionPaneUI(){

                @Override
                public Dimension getMinimumOptionPaneSize() {
                    if (this.minimumSize == null) {
                        return new Dimension(262, 50);
                    }
                    return new Dimension(this.minimumSize.width, 50);
                }
            });
        }
        optionPane.setWantsInput(false);
        optionPane.getAccessibleContext().setAccessibleDescription(strMsg);
        if (null != strMsg) {
            final String clipboardText = strMsg;
            optionPane.addMouseListener(new MouseAdapter(){

                @Override
                public void mousePressed(MouseEvent e) {
                    this.showCopyToClipboardPopupMenu(e);
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    this.showCopyToClipboardPopupMenu(e);
                }

                private void showCopyToClipboardPopupMenu(MouseEvent e) {
                    if (e.isPopupTrigger()) {
                        JPopupMenu pm = new JPopupMenu();
                        pm.add(new AbstractAction(NbBundle.getMessage(NbPresenter.class, (String)"Lbl_CopyToClipboard")){

                            @Override
                            public void actionPerformed(ActionEvent e) {
                                Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
                                c.setContents(new StringSelection(clipboardText), null);
                            }
                        });
                        pm.show(e.getComponent(), e.getX(), e.getY());
                    }
                }

            });
        }
        return optionPane;
    }

    private void uninitializeButtons() {
        if (this.currentButtonsPanel != null) {
            int i;
            if (this.currentPrimaryButtons != null) {
                for (i = 0; i < this.currentPrimaryButtons.length; ++i) {
                    this.modifyListener(this.currentPrimaryButtons[i], this.buttonListener, false);
                }
            }
            if (this.currentSecondaryButtons != null) {
                for (i = 0; i < this.currentSecondaryButtons.length; ++i) {
                    this.modifyListener(this.currentSecondaryButtons[i], this.buttonListener, false);
                }
            }
            this.getContentPane().remove(this.currentButtonsPanel);
            this.currentButtonsPanel = null;
        }
    }

    private void initializeClosingOptions(boolean init) {
        Object[] options = this.getClosingOptions();
        if (options == null) {
            return;
        }
        for (int i = 0; i < options.length; ++i) {
            this.modifyListener(options[i], this.buttonListener, init);
        }
    }

    private void initializeClosingOptions() {
        this.initializeClosingOptions(true);
    }

    private void uninitializeClosingOptions() {
        this.initializeClosingOptions(false);
    }

    @Override
    public int compare(Object a, Object b) {
        boolean isDefaultButton = a.equals(this.descriptor.getDefaultValue());
        int result = a.equals(NotifyDescriptor.OK_OPTION) || a.equals(NotifyDescriptor.YES_OPTION) ? 1 : 0;
        if (isDefaultButton) {
            ++result;
        } else if (b.equals(this.descriptor.getDefaultValue())) {
            --result;
        }
        return result;
    }

    protected final void initializeButtons() {
        boolean isAqua;
        JButton button;
        int i;
        Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        boolean dontShowHelp = Constants.DO_NOT_SHOW_HELP_IN_DIALOGS || this.descriptor instanceof WizardDescriptor && Boolean.FALSE.equals(((WizardDescriptor)this.descriptor).getProperty("WizardPanel_helpDisplayed"));
        boolean helpButtonShown = this.stdHelpButton.isShowing() || this.descriptor instanceof WizardDescriptor && !dontShowHelp;
        this.uninitializeButtons();
        Object[] primaryOptions = this.descriptor.getOptions();
        Object[] secondaryOptions = this.descriptor.getAdditionalOptions();
        this.currentAlign = this.getOptionsAlign();
        this.currentPrimaryButtons = null;
        this.currentSecondaryButtons = null;
        boolean bl = isAqua = "Aqua".equals(UIManager.getLookAndFeel().getID()) || "true".equalsIgnoreCase(System.getProperty("xtest.looks_as_mac"));
        if (isAqua) {
            this.currentAlign = 0;
        }
        if (primaryOptions != null) {
            if (isAqua) {
                Arrays.sort(primaryOptions, this);
            }
            this.currentPrimaryButtons = new Component[primaryOptions.length];
            for (i = 0; i < primaryOptions.length; ++i) {
                if (primaryOptions[i] == NotifyDescriptor.YES_OPTION) {
                    this.currentPrimaryButtons[i] = this.stdYesButton;
                    continue;
                }
                if (primaryOptions[i] == NotifyDescriptor.NO_OPTION) {
                    this.currentPrimaryButtons[i] = this.stdNoButton;
                    continue;
                }
                if (primaryOptions[i] == NotifyDescriptor.OK_OPTION) {
                    this.currentPrimaryButtons[i] = this.stdOKButton;
                    this.stdOKButton.setEnabled(this.descriptor.isValid());
                    continue;
                }
                if (primaryOptions[i] == NotifyDescriptor.CANCEL_OPTION) {
                    this.currentPrimaryButtons[i] = this.stdCancelButton;
                    continue;
                }
                if (primaryOptions[i] == NotifyDescriptor.CLOSED_OPTION) {
                    this.currentPrimaryButtons[i] = this.stdClosedButton;
                    continue;
                }
                if (primaryOptions[i] instanceof Component) {
                    this.currentPrimaryButtons[i] = (Component)primaryOptions[i];
                    continue;
                }
                if (primaryOptions[i] instanceof Icon) {
                    button = new JButton((Icon)primaryOptions[i]);
                    button.putClientProperty("defaultButton", Boolean.FALSE);
                    this.currentPrimaryButtons[i] = button;
                    continue;
                }
                button = new JButton();
                Mnemonics.setLocalizedText((AbstractButton)button, (String)primaryOptions[i].toString());
                button.putClientProperty("defaultButton", primaryOptions[i].equals(this.descriptor.getDefaultValue()));
                this.currentPrimaryButtons[i] = button;
            }
        } else {
            switch (this.descriptor.getOptionType()) {
                case 0: {
                    if (isAqua) {
                        this.currentPrimaryButtons = new Component[2];
                        this.currentPrimaryButtons[0] = this.stdNoButton;
                        this.currentPrimaryButtons[1] = this.stdYesButton;
                        break;
                    }
                    this.currentPrimaryButtons = new Component[2];
                    this.currentPrimaryButtons[0] = this.stdYesButton;
                    this.currentPrimaryButtons[1] = this.stdNoButton;
                    break;
                }
                case 1: {
                    this.currentPrimaryButtons = new Component[3];
                    if (isAqua) {
                        this.currentPrimaryButtons[0] = this.stdCancelButton;
                        this.currentPrimaryButtons[1] = this.stdNoButton;
                        this.currentPrimaryButtons[2] = this.stdYesButton;
                        break;
                    }
                    this.currentPrimaryButtons[0] = this.stdYesButton;
                    this.currentPrimaryButtons[1] = this.stdNoButton;
                    this.currentPrimaryButtons[2] = this.stdCancelButton;
                    break;
                }
                default: {
                    if (isAqua) {
                        this.currentPrimaryButtons = new Component[2];
                        this.currentPrimaryButtons[0] = this.stdCancelButton;
                        this.currentPrimaryButtons[1] = this.stdOKButton;
                    } else {
                        this.currentPrimaryButtons = new Component[2];
                        this.currentPrimaryButtons[0] = this.stdOKButton;
                        this.currentPrimaryButtons[1] = this.stdCancelButton;
                    }
                    this.stdOKButton.setEnabled(this.descriptor.isValid());
                }
            }
        }
        if (secondaryOptions != null && secondaryOptions.length != 0) {
            this.currentSecondaryButtons = new Component[secondaryOptions.length];
            Arrays.sort(secondaryOptions, this);
            for (i = 0; i < secondaryOptions.length; ++i) {
                if (secondaryOptions[i] == NotifyDescriptor.YES_OPTION) {
                    this.currentSecondaryButtons[i] = this.stdYesButton;
                    continue;
                }
                if (secondaryOptions[i] == NotifyDescriptor.NO_OPTION) {
                    this.currentSecondaryButtons[i] = this.stdNoButton;
                    continue;
                }
                if (secondaryOptions[i] == NotifyDescriptor.OK_OPTION) {
                    this.currentSecondaryButtons[i] = this.stdOKButton;
                    this.stdOKButton.setEnabled(this.descriptor.isValid());
                    continue;
                }
                if (secondaryOptions[i] == NotifyDescriptor.CANCEL_OPTION) {
                    this.currentSecondaryButtons[i] = this.stdCancelButton;
                    continue;
                }
                if (secondaryOptions[i] == NotifyDescriptor.CLOSED_OPTION) {
                    this.currentSecondaryButtons[i] = this.stdClosedButton;
                    continue;
                }
                if (secondaryOptions[i] instanceof Component) {
                    this.currentSecondaryButtons[i] = (Component)secondaryOptions[i];
                    continue;
                }
                if (secondaryOptions[i] instanceof Icon) {
                    button = new JButton((Icon)secondaryOptions[i]);
                    this.currentSecondaryButtons[i] = button;
                    continue;
                }
                button = new JButton();
                Mnemonics.setLocalizedText((AbstractButton)button, (String)secondaryOptions[i].toString());
                this.currentSecondaryButtons[i] = button;
            }
        }
        if (!dontShowHelp && (this.currentHelp != null || helpButtonShown)) {
            if (this.helpButtonLeft()) {
                if (this.currentSecondaryButtons == null) {
                    this.currentSecondaryButtons = new Component[0];
                }
                Component[] cSB2 = new Component[this.currentSecondaryButtons.length + 1];
                System.arraycopy(this.currentSecondaryButtons, 0, cSB2, 1, this.currentSecondaryButtons.length);
                cSB2[0] = this.stdHelpButton;
                this.currentSecondaryButtons = cSB2;
            } else {
                if (this.currentPrimaryButtons == null) {
                    this.currentPrimaryButtons = new Component[0];
                }
                Component[] cPB2 = new Component[this.currentPrimaryButtons.length + 1];
                if (isAqua) {
                    System.arraycopy(this.currentPrimaryButtons, 0, cPB2, 1, this.currentPrimaryButtons.length);
                    cPB2[0] = this.stdHelpButton;
                } else {
                    System.arraycopy(this.currentPrimaryButtons, 0, cPB2, 0, this.currentPrimaryButtons.length);
                    cPB2[this.currentPrimaryButtons.length] = this.stdHelpButton;
                }
                this.currentPrimaryButtons = cPB2;
            }
            this.stdHelpButton.setEnabled(this.currentHelp != null);
        }
        if (this.currentAlign == 0 || this.currentAlign == -1) {
            int i2;
            JPanel panelForPrimary = null;
            JPanel panelForSecondary = null;
            if (this.currentPrimaryButtons != null) {
                panelForPrimary = new JPanel();
                panelForPrimary.setBackground(this.descriptor.getBackground());
                if (this.currentAlign == -1) {
                    panelForPrimary.setLayout((LayoutManager)new EqualFlowLayout());
                } else {
                    panelForPrimary.setLayout((LayoutManager)new EqualFlowLayout(2));
                }
                for (i2 = 0; i2 < this.currentPrimaryButtons.length; ++i2) {
                    this.modifyListener(this.currentPrimaryButtons[i2], this.buttonListener, true);
                    panelForPrimary.add(this.currentPrimaryButtons[i2]);
                }
            }
            if (this.currentSecondaryButtons != null) {
                panelForSecondary = new JPanel();
                panelForSecondary.setBackground(this.descriptor.getBackground());
                panelForSecondary.setLayout((LayoutManager)new EqualFlowLayout(0));
                for (i2 = 0; i2 < this.currentSecondaryButtons.length; ++i2) {
                    this.modifyListener(this.currentSecondaryButtons[i2], this.buttonListener, true);
                    panelForSecondary.add(this.currentSecondaryButtons[i2]);
                }
            }
            if (panelForPrimary != null && panelForSecondary != null) {
                this.currentButtonsPanel = new JPanel();
                this.currentButtonsPanel.setLayout(new BorderLayout());
                this.currentButtonsPanel.setBackground(this.descriptor.getBackground());
                this.currentButtonsPanel.add((Component)panelForPrimary, "East");
                this.currentButtonsPanel.add((Component)panelForSecondary, "West");
            } else {
                this.currentButtonsPanel = panelForPrimary != null ? panelForPrimary : panelForSecondary;
            }
            if (this.currentButtonsPanel != null && this.currentButtonsPanel.getComponentCount() != 0) {
                if (this.currentButtonsPanel.getBorder() == null) {
                    this.currentButtonsPanel.setBorder(new EmptyBorder(new Insets(11, 6, 5, 5)));
                }
                this.getContentPane().add((Component)this.currentButtonsPanel, "South");
            }
        } else if (this.currentAlign == 1) {
            this.currentButtonsPanel = new JPanel();
            this.currentButtonsPanel.setLayout(new GridBagLayout());
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridwidth = 0;
            gbc.weightx = 1.0;
            gbc.insets = new Insets(5, 4, 2, 5);
            gbc.fill = 2;
            if (this.currentPrimaryButtons != null) {
                for (int i3 = 0; i3 < this.currentPrimaryButtons.length; ++i3) {
                    this.modifyListener(this.currentPrimaryButtons[i3], this.buttonListener, true);
                    this.currentButtonsPanel.add(this.currentPrimaryButtons[i3], gbc);
                }
            }
            GridBagConstraints padding = new GridBagConstraints();
            padding.gridwidth = 0;
            padding.weightx = 1.0;
            padding.weighty = 1.0;
            padding.fill = 1;
            this.currentButtonsPanel.add((Component)new JPanel(), padding);
            gbc.insets = new Insets(2, 4, 5, 5);
            if (this.currentSecondaryButtons != null) {
                for (int i4 = 0; i4 < this.currentSecondaryButtons.length; ++i4) {
                    this.modifyListener(this.currentSecondaryButtons[i4], this.buttonListener, true);
                    this.currentButtonsPanel.add(this.currentSecondaryButtons[i4], gbc);
                }
            }
            if (this.currentButtonsPanel != null) {
                if (this.currentButtonsPanel.getBorder() == null) {
                    this.currentButtonsPanel.setBorder(new EmptyBorder(new Insets(6, 7, 5, 5)));
                }
                this.getContentPane().add((Component)this.currentButtonsPanel, "East");
            }
        }
        this.updateDefaultButton();
        Component fo = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (fo != focusOwner && focusOwner != null) {
            focusOwner.requestFocus();
        }
    }

    private boolean helpButtonLeft() {
        boolean result = false;
        try {
            String resValue = NbBundle.getMessage(NbPresenter.class, (String)"HelpButtonAtTheLeftSide");
            result = "true".equals(resValue.toLowerCase());
        }
        catch (MissingResourceException e) {
            // empty catch block
        }
        return result;
    }

    private void updateDefaultButton() {
        if (this.descriptor.getDefaultValue() != null) {
            JButton b;
            if (this.descriptor.getDefaultValue() instanceof JButton) {
                b = (JButton)this.descriptor.getDefaultValue();
                if (b.isVisible() && b.isEnabled() && b.isDefaultCapable() && !Boolean.FALSE.equals(b.getClientProperty("defaultButton"))) {
                    this.getRootPane().setDefaultButton(b);
                    return;
                }
            } else {
                b = null;
                HashSet<Component> currentActive = new HashSet<Component>();
                if (this.currentPrimaryButtons != null) {
                    currentActive.addAll(Arrays.asList(this.currentPrimaryButtons));
                }
                if (this.currentSecondaryButtons != null) {
                    currentActive.addAll(Arrays.asList(this.currentSecondaryButtons));
                }
                Arrays.asList(this.currentPrimaryButtons);
                if (this.descriptor.getDefaultValue().equals(NotifyDescriptor.OK_OPTION) && currentActive.contains(this.stdOKButton)) {
                    b = this.stdOKButton;
                } else if (this.descriptor.getDefaultValue().equals(NotifyDescriptor.YES_OPTION) && currentActive.contains(this.stdYesButton)) {
                    b = this.stdYesButton;
                } else if (this.descriptor.getDefaultValue().equals(NotifyDescriptor.NO_OPTION)) {
                    b = this.stdNoButton;
                } else if (this.descriptor.getDefaultValue().equals(NotifyDescriptor.CANCEL_OPTION)) {
                    b = this.stdCancelButton;
                } else if (this.descriptor.getDefaultValue().equals(NotifyDescriptor.CLOSED_OPTION)) {
                    b = this.stdClosedButton;
                }
                if (b != null && b.isVisible() && b.isEnabled()) {
                    this.getRootPane().setDefaultButton(b);
                    return;
                }
            }
        }
        if (this.currentPrimaryButtons != null) {
            for (int i = 0; i < this.currentPrimaryButtons.length; ++i) {
                JButton b;
                if (!(this.currentPrimaryButtons[i] instanceof JButton) || !(b = (JButton)this.currentPrimaryButtons[i]).isVisible() || !b.isEnabled() || !b.isDefaultCapable() || Boolean.FALSE.equals(b.getClientProperty("defaultButton"))) continue;
                this.getRootPane().setDefaultButton(b);
                return;
            }
        }
        if (null != this.initialDefaultButton && this.initialDefaultButton.isEnabled() && this.initialDefaultButton.isDefaultCapable()) {
            this.getRootPane().setDefaultButton(this.initialDefaultButton);
        } else {
            this.getRootPane().setDefaultButton(null);
        }
    }

    private void updateNotificationLine(int msgType, Object o) {
        String msg;
        String string = msg = o == null ? null : o.toString();
        if (msg != null && msg.trim().length() > 0) {
            switch (msgType) {
                case 1: {
                    this.prepareMessage(this.notificationLine, ImageUtilities.loadImageIcon((String)"org/netbeans/core/windows/resources/error.png", (boolean)true), this.nbErrorForeground);
                    break;
                }
                case 2: {
                    this.prepareMessage(this.notificationLine, ImageUtilities.loadImageIcon((String)"org/netbeans/core/windows/resources/warning.png", (boolean)true), this.nbWarningForeground);
                    break;
                }
                case 3: {
                    this.prepareMessage(this.notificationLine, ImageUtilities.loadImageIcon((String)"org/netbeans/core/windows/resources/info.png", (boolean)true), this.nbInfoForeground);
                    break;
                }
            }
            this.notificationLine.setToolTipText(msg);
        } else {
            this.prepareMessage(this.notificationLine, null, null);
            this.notificationLine.setToolTipText(null);
        }
        this.notificationLine.setText(msg);
    }

    private void prepareMessage(JLabel label, ImageIcon icon, Color fgColor) {
        label.setIcon(icon);
        label.setForeground(fgColor);
    }

    private void updateOKButton(boolean valid) {
        int i;
        JButton b;
        if (this.currentPrimaryButtons != null) {
            for (i = 0; i < this.currentPrimaryButtons.length; ++i) {
                if (!(this.currentPrimaryButtons[i] instanceof JButton) || (b = (JButton)this.currentPrimaryButtons[i]) != this.stdOKButton || !b.isVisible()) continue;
                b.setEnabled(valid);
            }
        }
        if (this.currentSecondaryButtons != null) {
            for (i = 0; i < this.currentSecondaryButtons.length; ++i) {
                if (!(this.currentSecondaryButtons[i] instanceof JButton) || (b = (JButton)this.currentSecondaryButtons[i]) != this.stdOKButton || !b.isVisible()) continue;
                b.setEnabled(valid);
            }
        }
    }

    private void modifyListener(Object comp, ButtonListener l, boolean add) {
        if (comp instanceof JButton) {
            JButton b = (JButton)comp;
            if (add) {
                List listeners = Arrays.asList(b.getActionListeners());
                if (!listeners.contains(l)) {
                    b.addActionListener(l);
                }
                if (!(listeners = Arrays.asList(b.getComponentListeners())).contains(l)) {
                    b.addComponentListener(l);
                }
                if (!(listeners = Arrays.asList(b.getPropertyChangeListeners())).contains(l)) {
                    b.addPropertyChangeListener(l);
                }
            } else {
                b.removeActionListener(l);
                b.removeComponentListener(l);
                b.removePropertyChangeListener(l);
            }
            return;
        }
        Method m = null;
        try {
            m = comp.getClass().getMethod(add ? "addActionListener" : "removeActionListener", ActionListener.class);
            try {
                m.setAccessible(true);
            }
            catch (SecurityException se) {
                m = null;
            }
        }
        catch (NoSuchMethodException e) {
            m = null;
        }
        catch (SecurityException e2) {
            m = null;
        }
        if (m != null) {
            try {
                m.invoke(comp, l);
            }
            catch (Exception e) {
                // empty catch block
            }
        }
    }

    private void superShow() {
        assert (SwingUtilities.isEventDispatchThread());
        super.show();
    }

    @Deprecated
    @Override
    public void show() {
        if (this.isModal()) {
            Mutex.EVENT.readAccess((Mutex.Action)this);
        } else if (SwingUtilities.isEventDispatchThread()) {
            this.doShow();
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    NbPresenter.this.doShow();
                }
            });
        }
    }

    public Void run() {
        this.doShow();
        return null;
    }

    private void doShow() {
        GraphicsDevice gd;
        Window fullScreenWindow = null;
        if (Utilities.isUnix() && (gd = this.getGraphicsConfiguration().getDevice()).isFullScreenSupported() && null != (fullScreenWindow = gd.getFullScreenWindow())) {
            gd.setFullScreenWindow(null);
        }
        NbPresenter prev = null;
        try {
            MenuSelectionManager.defaultManager().clearSelectedPath();
        }
        catch (NullPointerException npE) {
            LOG.log(Level.FINE, null, npE);
        }
        if (this.isModal()) {
            prev = currentModalDialog;
            currentModalDialog = this;
            NbPresenter.fireChangeEvent();
        }
        this.superShow();
        if (null != fullScreenWindow) {
            this.getGraphicsConfiguration().getDevice().setFullScreenWindow(fullScreenWindow);
        }
        if (currentModalDialog != prev) {
            currentModalDialog = prev;
            NbPresenter.fireChangeEvent();
        }
    }

    @Override
    public void propertyChange(final PropertyChangeEvent evt) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    NbPresenter.this.propertyChange(evt);
                }
            });
            return;
        }
        boolean update = false;
        if ("options".equals(evt.getPropertyName())) {
            this.initializeButtons();
            update = true;
        } else if ("optionType".equals(evt.getPropertyName())) {
            this.initializeButtons();
            update = true;
        } else if ("optionsAlign".equals(evt.getPropertyName())) {
            this.initializeButtons();
            update = true;
        } else if ("message".equals(evt.getPropertyName())) {
            this.initializeMessage();
            this.requestFocusForMessage();
            this.updateHelp();
            update = true;
        } else if ("messageType".equals(evt.getPropertyName())) {
            this.initializeMessage();
            this.requestFocusForMessage();
            update = true;
        } else if ("title".equals(evt.getPropertyName())) {
            this.setTitle(this.descriptor.getTitle());
        } else if ("noDefaultClose".equals(evt.getPropertyName())) {
            this.setDefaultCloseOperation(this.descriptor instanceof WizardDescriptor || this.descriptor.isNoDefaultClose() ? 0 : 2);
        } else if ("helpCtx".equals(evt.getPropertyName())) {
            Component fo = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            this.updateHelp();
            if (this.currentButtonsPanel != null) {
                this.currentButtonsPanel.revalidate();
            }
            if (this.currentButtonsPanel != null) {
                this.currentButtonsPanel.repaint();
            }
            if (fo != null) {
                fo.requestFocus();
            }
        } else if ("valid".equals(evt.getPropertyName())) {
            this.updateOKButton((Boolean)evt.getNewValue());
        } else if ("infoNotification".equals(evt.getPropertyName())) {
            this.updateNotificationLine(3, evt.getNewValue());
        } else if ("warningNotification".equals(evt.getPropertyName())) {
            this.updateNotificationLine(2, evt.getNewValue());
        } else if ("errorNotification".equals(evt.getPropertyName())) {
            this.updateNotificationLine(1, evt.getNewValue());
        }
        if (update) {
            Dimension sz = this.getSize();
            Dimension prefSize = this.getPreferredSize();
            if (prefSize.width > sz.width || prefSize.height > sz.height) {
                this.setSize(Math.max(prefSize.width, sz.width), Math.max(prefSize.height, sz.height));
            }
            this.invalidate();
            this.validate();
            this.repaint();
        }
    }

    private void updateHelp() {
        HelpCtx help = this.getHelpCtx();
        if (HelpCtx.DEFAULT_HELP.equals((Object)help)) {
            Object msg = this.descriptor.getMessage();
            if (msg instanceof Component) {
                help = HelpCtx.findHelp((Component)((Component)msg));
            }
            if (HelpCtx.DEFAULT_HELP.equals((Object)help)) {
                help = null;
            }
        }
        if (!Utilities.compareObjects((Object)this.currentHelp, (Object)help)) {
            this.currentHelp = help;
            if (help != null && help.getHelpID() != null) {
                HelpCtx.setHelpIDString((JComponent)this.getRootPane(), (String)help.getHelpID());
            }
            if (this.haveCalledInitializeButtons) {
                this.initializeButtons();
            }
        }
    }

    protected int getOptionsAlign() {
        return 0;
    }

    protected ActionListener getButtonListener() {
        return null;
    }

    protected Object[] getClosingOptions() {
        return null;
    }

    protected HelpCtx getHelpCtx() {
        return null;
    }

    @Override
    public void windowDeactivated(WindowEvent p1) {
    }

    @Override
    public void windowClosed(WindowEvent p1) {
    }

    @Override
    public void windowDeiconified(WindowEvent p1) {
    }

    @Override
    public void windowOpened(WindowEvent p1) {
    }

    @Override
    public void windowIconified(WindowEvent p1) {
    }

    @Override
    public void windowClosing(WindowEvent p1) {
        if (!(this.descriptor instanceof WizardDescriptor)) {
            this.descriptor.setValue(NotifyDescriptor.CLOSED_OPTION);
        }
    }

    @Override
    public void windowActivated(WindowEvent p1) {
    }

    public static void addChangeListener(ChangeListener l) {
        cs.addChangeListener(l);
    }

    public static void removeChangeListener(ChangeListener l) {
        cs.removeChangeListener(l);
    }

    private static void fireChangeEvent() {
        EventQueue.invokeLater(new Runnable(){

            @Override
            public void run() {
                cs.fireChange();
            }
        });
    }

    @Override
    public AccessibleContext getAccessibleContext() {
        if (this.accessibleContext == null) {
            this.accessibleContext = new AccessibleNbPresenter();
        }
        return this.accessibleContext;
    }

    private static String getMessageTypeDescription(int messageType) {
        switch (messageType) {
            case 0: {
                return NbBundle.getBundle(NbPresenter.class).getString("ACSD_ErrorMessage");
            }
            case 2: {
                return NbBundle.getBundle(NbPresenter.class).getString("ACSD_WarningMessage");
            }
            case 3: {
                return NbBundle.getBundle(NbPresenter.class).getString("ACSD_QuestionMessage");
            }
            case 1: {
                return NbBundle.getBundle(NbPresenter.class).getString("ACSD_InformationMessage");
            }
            case -1: {
                return NbBundle.getBundle(NbPresenter.class).getString("ACSD_PlainMessage");
            }
        }
        return "";
    }

    private static void showHelp(HelpCtx helpCtx) {
        if (!helpCtx.display()) {
            Utilities.disabledActionBeep();
        }
    }

    private void initBounds() {
        Window w = this.findFocusedWindow();
        if (null != w) {
            this.setLocationRelativeTo(w);
            Rectangle screen = Utilities.getUsableScreenBounds((GraphicsConfiguration)w.getGraphicsConfiguration());
            Rectangle bounds = this.getBounds();
            int dx = bounds.x;
            int dy = bounds.y;
            if (dy + bounds.height > screen.y + screen.height) {
                dy = screen.y + screen.height - bounds.height;
            }
            if (dy < screen.y) {
                dy = screen.y;
            }
            if (dx + bounds.width > screen.x + screen.width) {
                dx = screen.x + screen.width - bounds.width;
            }
            if (dx < screen.x) {
                dx = screen.x;
            }
            this.setLocation(dx, dy);
        } else {
            Rectangle centerBounds;
            Dimension size = this.getSize();
            if (size.equals((centerBounds = Utilities.findCenterBounds((Dimension)size)).getSize())) {
                this.setLocation(centerBounds.x, centerBounds.y);
            } else {
                this.setBounds(centerBounds);
            }
        }
    }

    private Window findFocusedWindow() {
        Window w;
        for (w = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusedWindow(); null != w && !w.isShowing(); w = w.getOwner()) {
        }
        return w;
    }

    static {
        cs = new ChangeSupport(NbPresenter.class);
        isJava17 = null;
        LOG = Logger.getLogger(NbPresenter.class.getName());
        if (Boolean.getBoolean("netbeans.hack.50423")) {
            try {
                markers = DefaultKeyboardFocusManager.class.getDeclaredField("typeAheadMarkers");
                markers.setAccessible(true);
                dequeue = DefaultKeyboardFocusManager.class.getDeclaredMethod("dequeueKeyEvents", Long.TYPE, Component.class);
                dequeue.setAccessible(true);
            }
            catch (Throwable ex) {
                LOG.log(Level.WARNING, "Not activating workaround for #50423", ex);
            }
        }
    }

    private final class HackTypeAhead
    implements Runnable {
        private RequestProcessor.Task task;

        public HackTypeAhead() {
            this.task = RequestProcessor.getDefault().create((Runnable)this);
        }

        public void activate() {
            if (NbPresenter.markers != null) {
                this.task.schedule(1000);
            }
        }

        @Override
        public void run() {
            if (!SwingUtilities.isEventDispatchThread()) {
                SwingUtilities.invokeLater(this);
                return;
            }
            KeyboardFocusManager fm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
            Collection result = null;
            try {
                result = (Collection)NbPresenter.markers.get(fm);
            }
            catch (Exception ex) {
                Logger.getLogger(NbPresenter.class.getName()).log(Level.WARNING, null, ex);
            }
            if (result == null || result.isEmpty()) {
                return;
            }
            NbPresenter.LOG.warning("Symptoms of #50423: There is something in type ahead: " + result + " requesting focus change");
            try {
                NbPresenter.dequeue.invoke(fm, -1, NbPresenter.this);
            }
            catch (Exception ex) {
                Logger.getLogger(NbPresenter.class.getName()).log(Level.WARNING, null, ex);
            }
        }
    }

    private class AccessibleNbPresenter
    extends JDialog.AccessibleJDialog {
        AccessibleNbPresenter() {
            super(NbPresenter.this);
        }

        @Override
        public String getAccessibleName() {
            if (this.accessibleName != null) {
                return this.accessibleName;
            }
            if (NbPresenter.this.currentMessage instanceof Accessible && NbPresenter.this.currentMessage.getAccessibleContext().getAccessibleName() != null) {
                return NbPresenter.this.currentMessage.getAccessibleContext().getAccessibleName();
            }
            return super.getAccessibleName();
        }

        @Override
        public String getAccessibleDescription() {
            if (this.accessibleDescription != null) {
                return this.accessibleDescription;
            }
            if (NbPresenter.this.currentMessage instanceof Accessible && NbPresenter.this.currentMessage.getAccessibleContext().getAccessibleDescription() != null) {
                return MessageFormat.format(NbPresenter.getMessageTypeDescription(NbPresenter.this.descriptor.getMessageType()), NbPresenter.this.currentMessage.getAccessibleContext().getAccessibleDescription());
            }
            return super.getAccessibleDescription();
        }
    }

    private class ButtonListener
    implements ActionListener,
    ComponentListener,
    PropertyChangeListener {
        ButtonListener() {
        }

        /*
         * Enabled aggressive block sorting
         */
        @Override
        public void actionPerformed(ActionEvent evt) {
            Object[] arr;
            boolean isAqua = "Aqua".equals(UIManager.getLookAndFeel().getID()) || "true".equalsIgnoreCase(System.getProperty("xtest.looks_as_mac"));
            Object pressedOption = evt.getSource();
            if ("Escape".equals(evt.getActionCommand())) {
                MenuElement[] selPath = MenuSelectionManager.defaultManager().getSelectedPath();
                if (selPath != null && selPath.length != 0) {
                    MenuSelectionManager.defaultManager().clearSelectedPath();
                    return;
                }
                pressedOption = NotifyDescriptor.CLOSED_OPTION;
            } else {
                if (evt.getSource() == NbPresenter.this.stdHelpButton) {
                    NbPresenter.showHelp(NbPresenter.this.currentHelp);
                    return;
                }
                Object[] options = NbPresenter.this.descriptor.getOptions();
                if (isAqua && options != null) {
                    Arrays.sort(options, NbPresenter.this);
                }
                if (options != null && NbPresenter.this.currentPrimaryButtons != null && options.length == NbPresenter.this.currentPrimaryButtons.length - (NbPresenter.this.currentHelp != null ? 1 : 0)) {
                    int offset = NbPresenter.this.currentHelp != null && isAqua ? -1 : 0;
                    for (int i = 0; i < NbPresenter.this.currentPrimaryButtons.length; ++i) {
                        if (evt.getSource() != NbPresenter.this.currentPrimaryButtons[i]) continue;
                        pressedOption = options[i + offset];
                    }
                }
                options = NbPresenter.this.descriptor.getAdditionalOptions();
                if (isAqua && options != null) {
                    Arrays.sort(options, NbPresenter.this);
                }
                if (options != null && NbPresenter.this.currentSecondaryButtons != null && options.length == NbPresenter.this.currentSecondaryButtons.length) {
                    for (int i = 0; i < NbPresenter.this.currentSecondaryButtons.length; ++i) {
                        if (evt.getSource() != NbPresenter.this.currentSecondaryButtons[i]) continue;
                        pressedOption = options[i];
                    }
                }
                if (evt.getSource() == NbPresenter.this.stdYesButton) {
                    pressedOption = NotifyDescriptor.YES_OPTION;
                } else if (evt.getSource() == NbPresenter.this.stdNoButton) {
                    pressedOption = NotifyDescriptor.NO_OPTION;
                } else if (evt.getSource() == NbPresenter.this.stdCancelButton) {
                    pressedOption = NotifyDescriptor.CANCEL_OPTION;
                } else if (evt.getSource() == NbPresenter.this.stdClosedButton) {
                    pressedOption = NotifyDescriptor.CLOSED_OPTION;
                } else if (evt.getSource() == NbPresenter.this.stdOKButton) {
                    pressedOption = NotifyDescriptor.OK_OPTION;
                }
            }
            NbPresenter.this.descriptor.setValue(pressedOption);
            ActionListener al = NbPresenter.this.getButtonListener();
            if (al != null) {
                if (pressedOption == evt.getSource()) {
                    al.actionPerformed(evt);
                } else {
                    al.actionPerformed(new ActionEvent(pressedOption, evt.getID(), evt.getActionCommand(), evt.getModifiers()));
                }
            }
            if ((arr = NbPresenter.this.getClosingOptions()) != null && pressedOption != NotifyDescriptor.CLOSED_OPTION) {
                List<Object> l = Arrays.asList(arr);
                if (!l.contains(pressedOption)) return;
                NbPresenter.this.dispose();
                return;
            }
            NbPresenter.this.dispose();
        }

        @Override
        public void componentShown(ComponentEvent p1) {
            NbPresenter.this.updateDefaultButton();
        }

        @Override
        public void componentResized(ComponentEvent p1) {
        }

        @Override
        public void componentHidden(ComponentEvent p1) {
            NbPresenter.this.updateDefaultButton();
        }

        @Override
        public void componentMoved(ComponentEvent p1) {
        }

        @Override
        public void propertyChange(PropertyChangeEvent p1) {
            if ("enabled".equals(p1.getPropertyName())) {
                NbPresenter.this.updateDefaultButton();
            }
        }
    }

    private final class EscapeAction
    extends AbstractAction {
        public EscapeAction() {
            this.putValue("ActionCommandKey", "Escape");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (!NbPresenter.this.descriptor.isNoDefaultClose()) {
                NbPresenter.this.buttonListener.actionPerformed(e);
            }
        }
    }

    private static final class FixedHeightLabel
    extends JLabel {
        private static final int ESTIMATED_HEIGHT = 16;

        @Override
        public Dimension getPreferredSize() {
            Dimension preferredSize = super.getPreferredSize();
            assert (16 == ImageUtilities.loadImage((String)"org/netbeans/core/windows/resources/warning.png").getHeight(null));
            preferredSize.height = Math.max(16, preferredSize.height);
            return preferredSize;
        }
    }

}

