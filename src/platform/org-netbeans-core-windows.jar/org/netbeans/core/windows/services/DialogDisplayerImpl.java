/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.windows.services;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.KeyboardFocusManager;
import java.awt.Window;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.services.NbDialog;
import org.netbeans.core.windows.services.NbPresenter;
import org.netbeans.core.windows.services.PresenterDecorator;
import org.netbeans.core.windows.view.ui.DefaultSeparateContainer;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class DialogDisplayerImpl
extends DialogDisplayer {
    private static List<Runnable> run = Collections.synchronizedList(new ArrayList());
    private Object testResult;

    public DialogDisplayerImpl() {
        this(null);
    }

    DialogDisplayerImpl(Object testResult) {
        this.testResult = testResult;
    }

    public static void runDelayed() {
        NbPresenter.LOG.fine("runDelayed");
        List<Runnable> local = run;
        run = null;
        if (local == null) {
            NbPresenter.LOG.fine("runDelayed, nothing");
            return;
        }
        assert (EventQueue.isDispatchThread());
        for (Runnable r : local) {
            NbPresenter.LOG.fine("runDelayed, run = " + r);
            r.run();
        }
        NbPresenter.LOG.fine("runDelayed, done");
    }

    public Dialog createDialog(DialogDescriptor d) {
        return this.createDialog(d, null);
    }

    public Dialog createDialog(final DialogDescriptor d, final Frame preferredParent) {
        if (GraphicsEnvironment.isHeadless()) {
            throw new HeadlessException();
        }
        return (Dialog)Mutex.EVENT.readAccess((Mutex.Action)new Mutex.Action<Dialog>(){

            public Dialog run() {
                NbDialog dlg;
                if (NbPresenter.currentModalDialog != null) {
                    NbDialog dlg2 = NbPresenter.currentModalDialog.isLeaf() ? new NbDialog(d, WindowManager.getDefault().getMainWindow()) : new NbDialog(d, NbPresenter.currentModalDialog);
                    DialogDisplayerImpl.customizeDlg(dlg2);
                    return dlg2;
                }
                Component w = preferredParent;
                if (null == w) {
                    w = KeyboardFocusManager.getCurrentKeyboardFocusManager().getActiveWindow();
                    if (!(w instanceof NbPresenter) || !w.isVisible()) {
                        if (!(w instanceof DefaultSeparateContainer.ModeUIBase)) {
                            Container cont = SwingUtilities.getAncestorOfClass(Window.class, w);
                            w = cont != null && cont instanceof DefaultSeparateContainer.ModeUIBase ? (Window)cont : WindowManager.getDefault().getMainWindow();
                        }
                    } else if (w instanceof NbPresenter && ((NbPresenter)w).isLeaf()) {
                        w = WindowManager.getDefault().getMainWindow();
                    }
                }
                if (w instanceof Dialog) {
                    dlg = new NbDialog(d, (Dialog)w);
                } else {
                    Frame f = w instanceof Frame ? (Frame)w : WindowManager.getDefault().getMainWindow();
                    dlg = new NbDialog(d, f);
                }
                DialogDisplayerImpl.customizeDlg(dlg);
                dlg.requestFocusInWindow();
                return dlg;
            }
        });
    }

    public Object notify(NotifyDescriptor descriptor) {
        return this.notify(descriptor, false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Object notify(NotifyDescriptor descriptor, boolean noParent) {
        class AWTQuery
        implements Runnable {
            public Object result;
            public boolean running;
            final /* synthetic */ NotifyDescriptor val$descriptor;
            final /* synthetic */ boolean val$noParent;

            AWTQuery() {
                this.val$descriptor = var2_2;
                this.val$noParent = n;
            }

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                AWTQuery aWTQuery = this;
                synchronized (aWTQuery) {
                    this.notify();
                    this.running = true;
                }
                this.showDialog();
                aWTQuery = this;
                synchronized (aWTQuery) {
                    this.result = this.val$descriptor.getValue();
                    this.notifyAll();
                }
            }

            public void showDialog() {
                Object win;
                if (this$0.testResult != null) {
                    this.val$descriptor.setValue(this$0.testResult);
                    return;
                }
                Component focusOwner = null;
                TopComponent comp = TopComponent.getRegistry().getActivated();
                for (win = comp; win != null && !(win instanceof Window); win = win.getParent()) {
                }
                if (win != null) {
                    focusOwner = ((Window)win).getFocusOwner();
                }
                JDialog presenter = null;
                if (this.val$descriptor instanceof DialogDescriptor) {
                    if (NbPresenter.currentModalDialog != null) {
                        presenter = NbPresenter.currentModalDialog.isLeaf() ? new NbDialog((DialogDescriptor)this.val$descriptor, WindowManager.getDefault().getMainWindow()) : new NbDialog((DialogDescriptor)this.val$descriptor, NbPresenter.currentModalDialog);
                    } else {
                        Frame f;
                        Window w = KeyboardFocusManager.getCurrentKeyboardFocusManager().getActiveWindow();
                        if (w instanceof NbPresenter && ((NbPresenter)w).isLeaf()) {
                            w = WindowManager.getDefault().getMainWindow();
                        }
                        Frame frame = f = w instanceof Frame ? (Frame)w : WindowManager.getDefault().getMainWindow();
                        if (this.val$noParent) {
                            f = null;
                        }
                        presenter = new NbDialog((DialogDescriptor)this.val$descriptor, f);
                    }
                } else if (NbPresenter.currentModalDialog != null) {
                    presenter = NbPresenter.currentModalDialog.isLeaf() ? new NbPresenter(this.val$descriptor, WindowManager.getDefault().getMainWindow(), true) : new NbPresenter(this.val$descriptor, NbPresenter.currentModalDialog, true);
                } else {
                    Frame f;
                    Frame frame = f = KeyboardFocusManager.getCurrentKeyboardFocusManager().getActiveWindow() instanceof Frame ? (Frame)KeyboardFocusManager.getCurrentKeyboardFocusManager().getActiveWindow() : WindowManager.getDefault().getMainWindow();
                    if (this.val$noParent) {
                        f = null;
                    }
                    presenter = new NbPresenter(this.val$descriptor, f, true);
                }
                if ("true".equals(System.getProperty("javahelp.ignore.modality"))) {
                    presenter.getRootPane().putClientProperty("javahelp.ignore.modality", "true");
                    System.setProperty("javahelp.ignore.modality", "false");
                }
                DialogDisplayerImpl.customizeDlg((NbPresenter)presenter);
                presenter.getRootPane().requestDefaultFocus();
                presenter.setVisible(true);
                if (focusOwner != null) {
                    win.requestFocusInWindow();
                    comp.requestFocusInWindow();
                    if (!(focusOwner instanceof JRootPane)) {
                        focusOwner.requestFocusInWindow();
                    }
                }
            }
        }
        AWTQuery query = new AWTQuery(this, descriptor, noParent);
        if (SwingUtilities.isEventDispatchThread()) {
            query.showDialog();
            return descriptor.getValue();
        }
        AWTQuery aWTQuery = query;
        synchronized (aWTQuery) {
            SwingUtilities.invokeLater(query);
            try {
                query.wait(10000);
            }
            catch (InterruptedException ex) {
                // empty catch block
            }
            if (query.running) {
                while (query.result == null) {
                    try {
                        query.wait(3000);
                    }
                    catch (InterruptedException ex) {}
                }
                return query.result;
            }
            return NotifyDescriptor.CLOSED_OPTION;
        }
    }

    public void notifyLater(NotifyDescriptor descriptor) {
        class R
        implements Runnable {
            public boolean noParent;
            final /* synthetic */ NotifyDescriptor val$descriptor;

            R() {
                this.val$descriptor = var2_2;
            }

            @Override
            public void run() {
                this$0.notify(this.val$descriptor, this.noParent);
            }
        }
        R r = new R(this, descriptor);
        List<Runnable> local = run;
        if (local != null) {
            r.noParent = true;
            local.add(r);
        } else {
            EventQueue.invokeLater(r);
        }
    }

    private static void customizeDlg(NbPresenter presenter) {
        for (PresenterDecorator p : Lookup.getDefault().lookupAll(PresenterDecorator.class)) {
            p.customizePresenter(presenter);
        }
    }

}

