/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowSystemEvent
 *  org.openide.windows.WindowSystemListener
 */
package org.netbeans.core.windows;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.PersistenceHandler;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.persistence.TCRefConfig;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowSystemEvent;
import org.openide.windows.WindowSystemListener;

final class LazyLoader {
    private static final boolean NO_LAZY_LOADING = Boolean.getBoolean("nb.core.windows.no.lazy.loading");
    private boolean isActive = false;
    private boolean isLoading = false;
    private final Map<ModeImpl, LazyMode> lazyModes = new HashMap<ModeImpl, LazyMode>(15);

    public LazyLoader() {
        WindowManagerImpl.getInstance().addWindowSystemListener(new WindowSystemListener(){

            public void beforeLoad(WindowSystemEvent event) {
                LazyLoader.this.isLoading = false;
            }

            public void afterLoad(WindowSystemEvent event) {
                LazyLoader.this.isActive = true;
                WindowManagerImpl.getInstance().invokeWhenUIReady(new Runnable(){

                    @Override
                    public void run() {
                        LazyLoader.this.startLoading();
                    }
                });
            }

            public void beforeSave(WindowSystemEvent event) {
                LazyLoader.this.isActive = false;
                LazyLoader.this.loadAllNow();
            }

            public void afterSave(WindowSystemEvent event) {
            }

        });
    }

    void loadAllNow() {
        this.isActive = false;
        this.isLoading = true;
        PersistenceHandler persistenceHandler = PersistenceHandler.getDefault();
        for (LazyMode lazyMode : this.lazyModes.values()) {
            for (String tcId : lazyMode.getTopComponents()) {
                TopComponent tc = persistenceHandler.getTopComponentForID(tcId, true);
                if (null == tc || tc.isOpened()) continue;
                lazyMode.mode.addOpenedTopComponent(tc, lazyMode.getPosition(tcId));
            }
        }
        this.lazyModes.clear();
    }

    private void startLoading() {
        this.isLoading = true;
        ArrayList<LazyMode> sortedLazyModes = new ArrayList<LazyMode>(this.lazyModes.values());
        Collections.sort(sortedLazyModes);
        for (LazyMode lazyMode : sortedLazyModes) {
            Iterator<String> i$ = lazyMode.getTopComponents().iterator();
            while (i$.hasNext()) {
                String tcId;
                final String tcId2Load = tcId = i$.next();
                final ModeImpl targetMode = lazyMode.mode;
                final int position = lazyMode.getPosition(tcId);
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        LazyLoader.this.loadNow(targetMode, tcId2Load, position);
                    }
                });
            }
        }
    }

    void lazyLoad(ModeImpl mode, String selectedTCid, TCRefConfig tcRefConfig, int index) {
        if (NO_LAZY_LOADING || tcRefConfig.tc_id.equals(selectedTCid) || this.isLoading) {
            TopComponent tc = PersistenceHandler.getDefault().getTopComponentForID(tcRefConfig.tc_id, true);
            if (tc != null) {
                mode.addOpenedTopComponent(tc);
            }
            if (!NO_LAZY_LOADING) {
                LazyMode lazyMode = this.getLazyMode(mode);
                lazyMode.selectedTCposition = index;
            }
        } else {
            mode.addUnloadedTopComponent(tcRefConfig.tc_id);
            LazyMode lazyMode = this.getLazyMode(mode);
            lazyMode.add(tcRefConfig.tc_id, index);
        }
    }

    private void loadNow(ModeImpl mode, String tcId, int position) {
        if (!this.isActive) {
            return;
        }
        TopComponent tc = PersistenceHandler.getDefault().getTopComponentForID(tcId, true);
        if (null != tc && !tc.isOpened()) {
            if (position < 0) {
                position = mode.getOpenedTopComponentsIDs().size();
            }
            mode.addOpenedTopComponent(tc, position);
        }
        this.remove(mode, tcId);
    }

    private void remove(ModeImpl mode, String tcId) {
        LazyMode lazyMode = this.getLazyMode(mode);
        lazyMode.id2position.remove(tcId);
    }

    private LazyMode getLazyMode(ModeImpl mode) {
        LazyMode res = this.lazyModes.get(mode);
        if (null == res) {
            res = new LazyMode(mode);
            this.lazyModes.put(mode, res);
        }
        return res;
    }

    private static class LazyMode
    implements Comparable<LazyMode> {
        private int selectedTCposition;
        private final ModeImpl mode;
        private final Map<String, Integer> id2position = new HashMap<String, Integer>(30);

        public LazyMode(ModeImpl mode) {
            this.mode = mode;
        }

        private void add(String tc_id, int index) {
            this.id2position.put(tc_id, index);
        }

        @Override
        public int compareTo(LazyMode o) {
            if (this.mode.getKind() != o.mode.getKind()) {
                if (this.mode.getKind() == 1) {
                    return 1;
                }
                if (o.mode.getKind() == 1) {
                    return -1;
                }
            }
            return 0;
        }

        Collection<String> getTopComponents() {
            ArrayList<String> res = new ArrayList<String>(this.id2position.keySet());
            Collections.sort(res, new Comparator<String>(){

                @Override
                public int compare(String o1, String o2) {
                    int position1 = (Integer)LazyMode.this.id2position.get(o1);
                    int position2 = (Integer)LazyMode.this.id2position.get(o2);
                    int res = position1 - position2;
                    if (position1 < LazyMode.this.selectedTCposition && position2 < LazyMode.this.selectedTCposition) {
                        res *= -1;
                    }
                    return res;
                }
            });
            return res;
        }

        int getPosition(String tcId) {
            if (!this.id2position.containsKey(tcId)) {
                return -1;
            }
            int position = this.id2position.get(tcId);
            if (position <= this.selectedTCposition) {
                return 0;
            }
            return -1;
        }

    }

}

