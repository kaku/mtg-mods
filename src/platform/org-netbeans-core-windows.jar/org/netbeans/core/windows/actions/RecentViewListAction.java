/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.actions;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.TopComponentTracker;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.view.ui.popupswitcher.KeyboardPopupSwitcher;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public final class RecentViewListAction
extends AbstractAction
implements PropertyChangeListener {
    private final boolean documentsOnly;

    public RecentViewListAction() {
        this(false);
    }

    public static Action createDocumentsOnlyInstance() {
        return new RecentViewListAction(true);
    }

    private RecentViewListAction(boolean documentsOnly) {
        this.documentsOnly = documentsOnly;
        this.putValue("Name", NbBundle.getMessage(RecentViewListAction.class, (String)(documentsOnly ? "CTL_RecentDocumentListAction" : "CTL_RecentViewListAction")));
        TopComponent.getRegistry().addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)TopComponent.getRegistry()));
        this.updateEnabled();
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        TopComponent[] documents;
        boolean views;
        KeyStroke keyStroke;
        TopComponent activeTc;
        boolean editors = true;
        boolean bl = views = !this.documentsOnly;
        if ("immediately".equals(evt.getActionCommand()) && null != (activeTc = TopComponent.getRegistry().getActivated())) {
            if (TopComponentTracker.getDefault().isEditorTopComponent(activeTc)) {
                views = false;
            } else {
                editors = false;
                views = true;
            }
        }
        if ((documents = RecentViewListAction.getRecentWindows(editors, views)).length < 2) {
            return;
        }
        if (!"immediately".equals(evt.getActionCommand()) && !(evt.getSource() instanceof JMenuItem) && (keyStroke = Utilities.stringToKey((String)evt.getActionCommand())) != null) {
            int triggerKey = keyStroke.getKeyCode();
            int reverseKey = 16;
            int releaseKey = 0;
            int modifiers = keyStroke.getModifiers();
            if ((2 & modifiers) != 0) {
                releaseKey = 17;
            } else if ((8 & modifiers) != 0) {
                releaseKey = 18;
            } else if ((4 & modifiers) != 0) {
                releaseKey = 157;
            }
            if (releaseKey != 0) {
                if (!KeyboardPopupSwitcher.isShown()) {
                    KeyboardPopupSwitcher.showPopup(this.documentsOnly, releaseKey, triggerKey, (evt.getModifiers() & 1) == 0);
                }
                return;
            }
        }
        int documentIndex = (evt.getModifiers() & 1) == 0 ? 1 : documents.length - 1;
        TopComponent tc = documents[documentIndex];
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        ModeImpl mode = (ModeImpl)wm.findMode(tc);
        if (mode != null && mode != wm.getCurrentMaximizedMode()) {
            wm.switchMaximizedMode(null);
        }
        tc.requestActive();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("opened".equals(evt.getPropertyName())) {
            this.updateEnabled();
        }
    }

    public static String getStringRep4Unixes() {
        if (Utilities.isUnix() && !Utilities.isMac()) {
            return "Actions/Window/org-netbeans-core-windows-actions-RecentViewListAction.instance";
        }
        return null;
    }

    private void updateEnabled() {
        this.setEnabled(this.isMoreThanOneViewOpened());
    }

    private boolean isMoreThanOneViewOpened() {
        if (!this.documentsOnly) {
            return TopComponent.getRegistry().getOpened().size() > 1;
        }
        for (ModeImpl mode : WindowManagerImpl.getInstance().getModes()) {
            if (mode.getKind() != 1) continue;
            return mode.getOpenedTopComponents().size() > 1;
        }
        return false;
    }

    private static TopComponent[] getRecentWindows(boolean editors, boolean views) {
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        TopComponent[] documents = wm.getRecentViewList();
        TopComponentTracker tcTracker = TopComponentTracker.getDefault();
        ArrayList<TopComponent> docsList = new ArrayList<TopComponent>();
        for (int i = 0; i < documents.length; ++i) {
            ModeImpl mode;
            TopComponent tc = documents[i];
            if (tc == null || (mode = (ModeImpl)wm.findMode(tc)) == null || (!editors || !tcTracker.isEditorTopComponent(tc)) && (!views || !tcTracker.isViewTopComponent(tc))) continue;
            docsList.add(tc);
        }
        return docsList.toArray((T[])new TopComponent[0]);
    }
}

