/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Timer;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.actions.ActionUtils;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public class CloseAllButThisAction
extends AbstractAction
implements PropertyChangeListener,
Runnable {
    private TopComponent tc;
    private boolean isContext;
    private Timer updateTimer;
    private final Object LOCK = new Object();

    public CloseAllButThisAction() {
        this.isContext = false;
        this.putValue("Name", NbBundle.getMessage(CloseAllButThisAction.class, (String)"CTL_CloseAllButThisAction_MainMenu"));
        this.updateTimer = new Timer(300, new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                CloseAllButThisAction.this.updateEnabled();
            }
        });
        this.updateTimer.setRepeats(false);
        TopComponent.getRegistry().addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)TopComponent.getRegistry()));
        this.updateEnabled();
    }

    public CloseAllButThisAction(TopComponent topComp, boolean isContext) {
        this.tc = topComp;
        this.isContext = isContext;
        this.putValue("Name", NbBundle.getMessage(CloseAllButThisAction.class, (String)"CTL_CloseAllButThisAction"));
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        TopComponent topC = this.obtainTC();
        if (topC != null) {
            ActionUtils.closeAllExcept(topC, this.isContext);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String propName = evt.getPropertyName();
        if ("activated".equals(propName) || "opened".equals(propName)) {
            this.scheduleUpdate();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void scheduleUpdate() {
        Object object = this.LOCK;
        synchronized (object) {
            if (this.updateTimer.isRunning()) {
                this.updateTimer.restart();
            } else {
                this.updateTimer.start();
            }
        }
    }

    private void updateEnabled() {
        Mutex.EVENT.readAccess((Runnable)this);
    }

    @Override
    public void run() {
        TopComponent tc = this.obtainTC();
        WindowManagerImpl wmi = WindowManagerImpl.getInstance();
        ModeImpl mode = (ModeImpl)wmi.findMode(tc);
        boolean areOtherDocs = this.isContext ? mode.getOpenedTopComponents().size() > 1 : wmi.getEditorTopComponents().length > 1;
        this.setEnabled(mode != null && mode.getKind() == 1 && areOtherDocs && Switches.isEditorTopComponentClosingEnabled());
    }

    private TopComponent obtainTC() {
        TopComponent res = this.tc;
        if (null == res) {
            String[] ids;
            WindowManagerImpl wmi = WindowManagerImpl.getInstance();
            for (String tcId : ids = wmi.getRecentViewIDList()) {
                ModeImpl mode = wmi.findModeForOpenedID(tcId);
                if (mode == null || mode.getKind() != 1) continue;
                res = wmi.findTopComponent(tcId);
                break;
            }
        }
        if (null == res) {
            res = TopComponent.getRegistry().getActivated();
        }
        return res;
    }

    @Override
    public void putValue(String key, Object newValue) {
        if ("AcceleratorKey".equals(key)) {
            ActionUtils.putSharedAccelerator("CloseAllButThis", newValue);
        } else {
            super.putValue(key, newValue);
        }
    }

    @Override
    public Object getValue(String key) {
        if ("AcceleratorKey".equals(key)) {
            return ActionUtils.getSharedAccelerator("CloseAllButThis");
        }
        return super.getValue(key);
    }

}

