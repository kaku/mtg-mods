/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.actions;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.actions.ActionUtils;
import org.netbeans.core.windows.view.dnd.TopComponentDraggable;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public final class MoveModeAction
extends AbstractAction
implements PropertyChangeListener {
    private final ModeImpl mode;

    public MoveModeAction() {
        this(null);
        TopComponent.getRegistry().addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)TopComponent.getRegistry()));
    }

    public MoveModeAction(ModeImpl mode) {
        this.putValue("Name", NbBundle.getMessage(ActionUtils.class, (String)"CTL_MoveModeAction"));
        this.mode = mode;
        if (SwingUtilities.isEventDispatchThread()) {
            this.updateEnabled();
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    MoveModeAction.this.updateEnabled();
                }
            });
        }
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        this.updateEnabled();
        if (!this.isEnabled()) {
            return;
        }
        ModeImpl contextMode = this.getModeToWorkWith();
        if (null == contextMode) {
            return;
        }
        WindowManagerImpl.getInstance().userStartedKeyboardDragAndDrop(new TopComponentDraggable(contextMode));
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("activated".equals(evt.getPropertyName())) {
            this.updateEnabled();
        }
    }

    private void updateEnabled() {
        ModeImpl contextMode = this.getModeToWorkWith();
        if (null == contextMode || contextMode.getState() == 1 || null == contextMode.getSelectedTopComponent() || WindowManagerImpl.getInstance().getCurrentMaximizedMode() != null) {
            this.setEnabled(false);
            return;
        }
        TopComponent tc = contextMode.getSelectedTopComponent();
        if (null == tc) {
            this.setEnabled(false);
            return;
        }
        this.setEnabled(true);
    }

    private ModeImpl getModeToWorkWith() {
        if (null != this.mode) {
            return this.mode;
        }
        TopComponent activeTc = TopComponent.getRegistry().getActivated();
        if (null == activeTc) {
            return null;
        }
        return (ModeImpl)WindowManagerImpl.getInstance().findMode(activeTc);
    }

    @Override
    public void putValue(String key, Object newValue) {
        if ("AcceleratorKey".equals(key)) {
            ActionUtils.putSharedAccelerator("MoveMode", newValue);
        } else {
            super.putValue(key, newValue);
        }
    }

    @Override
    public Object getValue(String key) {
        if ("AcceleratorKey".equals(key)) {
            return ActionUtils.getSharedAccelerator("MoveMode");
        }
        return super.getValue(key);
    }

}

