/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.actions;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Set;
import javax.swing.AbstractAction;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.WindowManagerImpl;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public class SwitchToRecentDocumentAction
extends AbstractAction
implements PropertyChangeListener {
    public SwitchToRecentDocumentAction() {
        this.putValue("Name", NbBundle.getMessage(SwitchToRecentDocumentAction.class, (String)"CTL_SwitchToRecentDocumentAction"));
        TopComponent.getRegistry().addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)TopComponent.getRegistry()));
        this.updateEnabled();
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        TopComponent[] tcs = wm.getRecentViewList();
        if (tcs.length == 0) {
            return;
        }
        for (int i = 0; i < tcs.length; ++i) {
            TopComponent tc = tcs[i];
            ModeImpl mode = (ModeImpl)wm.findMode(tc);
            if (mode == null || mode.getKind() != 1) continue;
            if (mode != wm.getCurrentMaximizedMode()) {
                wm.switchMaximizedMode(null);
            }
            tc.requestActive();
            break;
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("opened".equals(evt.getPropertyName())) {
            this.updateEnabled();
        }
    }

    private void updateEnabled() {
        for (ModeImpl mode : WindowManagerImpl.getInstance().getModes()) {
            if (mode.getKind() != 1 || mode.getOpenedTopComponents().isEmpty()) continue;
            this.setEnabled(true);
            return;
        }
        this.setEnabled(false);
    }
}

