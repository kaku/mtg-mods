/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.actions;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.actions.ActionUtils;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.windows.TopComponent;

public class CloseWindowAction
extends AbstractAction
implements PropertyChangeListener {
    private TopComponent tc;

    public CloseWindowAction() {
        this.putValue("Name", NbBundle.getMessage(CloseWindowAction.class, (String)"CTL_CloseWindowAction_MainMenu"));
        TopComponent.getRegistry().addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)TopComponent.getRegistry()));
        if (SwingUtilities.isEventDispatchThread()) {
            this.updateEnabled();
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    CloseWindowAction.this.updateEnabled();
                }
            });
        }
    }

    public CloseWindowAction(TopComponent topcomp) {
        this.tc = topcomp;
        this.putValue("Name", NbBundle.getMessage(CloseWindowAction.class, (String)"CTL_CloseWindowAction"));
        if (WindowManagerImpl.getInstance().isEditorTopComponent(this.tc)) {
            this.setEnabled(Switches.isEditorTopComponentClosingEnabled() && Switches.isClosingEnabled(this.tc));
        } else {
            this.setEnabled(Switches.isViewTopComponentClosingEnabled() && Switches.isClosingEnabled(this.tc));
        }
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        TopComponent topC = this.tc;
        if (topC == null) {
            topC = TopComponent.getRegistry().getActivated();
        }
        if (topC != null) {
            topC.requestFocusInWindow();
            final TopComponent toClose = topC;
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    ActionUtils.closeWindow(toClose);
                }
            });
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("activated".equals(evt.getPropertyName())) {
            this.updateEnabled();
        }
    }

    private void updateEnabled() {
        TopComponent activeTc = TopComponent.getRegistry().getActivated();
        if (null == activeTc) {
            this.setEnabled(false);
            return;
        }
        if (WindowManagerImpl.getInstance().isEditorTopComponent(activeTc)) {
            this.setEnabled(Switches.isEditorTopComponentClosingEnabled() && Switches.isClosingEnabled(activeTc));
        } else {
            this.setEnabled(Switches.isViewTopComponentClosingEnabled() && Switches.isClosingEnabled(activeTc));
        }
    }

    @Override
    public void putValue(String key, Object newValue) {
        if ("AcceleratorKey".equals(key)) {
            ActionUtils.putSharedAccelerator("CloseWindow", newValue);
        } else {
            super.putValue(key, newValue);
        }
    }

    @Override
    public Object getValue(String key) {
        if ("AcceleratorKey".equals(key)) {
            return ActionUtils.getSharedAccelerator("CloseWindow");
        }
        return super.getValue(key);
    }

}

