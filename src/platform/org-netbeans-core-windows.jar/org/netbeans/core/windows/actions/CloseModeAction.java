/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.actions;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.actions.ActionUtils;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public final class CloseModeAction
extends AbstractAction
implements PropertyChangeListener {
    private ModeImpl mode;

    public CloseModeAction() {
        this.putValue("Name", NbBundle.getMessage(CloseModeAction.class, (String)"CTL_CloseModeAction"));
        TopComponent.getRegistry().addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)TopComponent.getRegistry()));
        if (SwingUtilities.isEventDispatchThread()) {
            this.updateEnabled();
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    CloseModeAction.this.updateEnabled();
                }
            });
        }
    }

    public CloseModeAction(ModeImpl mode) {
        this.putValue("Name", NbBundle.getMessage(ActionUtils.class, (String)"CTL_CloseModeAction"));
        this.mode = mode;
        if (mode.getKind() == 1) {
            this.setEnabled(false);
        } else {
            this.setEnabled(Switches.isModeClosingEnabled());
        }
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        TopComponent tc;
        ModeImpl contextMode = this.mode;
        if (contextMode == null && null != (tc = TopComponent.getRegistry().getActivated())) {
            contextMode = (ModeImpl)WindowManagerImpl.getInstance().findMode(tc);
        }
        if (contextMode != null) {
            WindowManagerImpl.getInstance().userClosedMode(contextMode);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("activated".equals(evt.getPropertyName())) {
            this.updateEnabled();
        }
    }

    private void updateEnabled() {
        TopComponent activeTc = TopComponent.getRegistry().getActivated();
        if (null == activeTc) {
            this.setEnabled(false);
            return;
        }
        if (WindowManagerImpl.getInstance().isEditorTopComponent(activeTc)) {
            this.setEnabled(false);
        } else {
            this.setEnabled(Switches.isModeClosingEnabled());
        }
    }

    @Override
    public void putValue(String key, Object newValue) {
        if ("AcceleratorKey".equals(key)) {
            ActionUtils.putSharedAccelerator("CloseMode", newValue);
        } else {
            super.putValue(key, newValue);
        }
    }

    @Override
    public Object getValue(String key) {
        if ("AcceleratorKey".equals(key)) {
            return ActionUtils.getSharedAccelerator("CloseMode");
        }
        return super.getValue(key);
    }

}

