/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.actions;

import javax.swing.Action;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public abstract class ActionsFactory {
    public abstract Action[] createPopupActions(TopComponent var1, Action[] var2);

    public abstract Action[] createPopupActions(Mode var1, Action[] var2);
}

