/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.windows.actions;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.actions.ActionUtils;
import org.netbeans.core.windows.actions.CloseModeAction;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public final class CollapseTabGroupAction
extends AbstractAction
implements PropertyChangeListener {
    private ModeImpl mode;

    public CollapseTabGroupAction() {
        this.putValue("Name", NbBundle.getMessage(CloseModeAction.class, (String)"CTL_CollapseTabGroupAction"));
        TopComponent.getRegistry().addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)TopComponent.getRegistry()));
        WindowManager.getDefault().addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)WindowManager.getDefault()));
        if (SwingUtilities.isEventDispatchThread()) {
            this.updateEnabled();
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    CollapseTabGroupAction.this.updateEnabled();
                }
            });
        }
    }

    public CollapseTabGroupAction(ModeImpl mode) {
        this.putValue("Name", NbBundle.getMessage(ActionUtils.class, (String)"CTL_CollapseTabGroupAction"));
        this.mode = mode;
        if (SwingUtilities.isEventDispatchThread()) {
            this.updateEnabled();
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    CollapseTabGroupAction.this.updateEnabled();
                }
            });
        }
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        TopComponent tc;
        ModeImpl contextMode = this.mode;
        if (contextMode == null && null != (tc = TopComponent.getRegistry().getActivated())) {
            contextMode = (ModeImpl)WindowManagerImpl.getInstance().findMode(tc);
        }
        if (contextMode != null) {
            WindowManagerImpl.getInstance().collapseTabGroup(contextMode);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("activated".equals(evt.getPropertyName()) || "modes".equals(evt.getPropertyName())) {
            this.updateEnabled();
        }
    }

    private void updateEnabled() {
        TopComponent tc;
        ModeImpl contextMode = this.mode;
        if (contextMode == null && null != (tc = TopComponent.getRegistry().getActivated())) {
            contextMode = (ModeImpl)WindowManagerImpl.getInstance().findMode(tc);
        }
        if (null == contextMode) {
            this.setEnabled(false);
            return;
        }
        boolean enable = contextMode.getState() == 0;
        enable &= contextMode.getKind() == 1;
        boolean hasOtherEditorMode = false;
        for (ModeImpl m : WindowManagerImpl.getInstance().getModes()) {
            if (m.getKind() != 1 || m.getState() != 0 || m == contextMode) continue;
            hasOtherEditorMode = true;
            break;
        }
        this.setEnabled(enable &= hasOtherEditorMode);
    }

    @Override
    public void putValue(String key, Object newValue) {
        if ("AcceleratorKey".equals(key)) {
            ActionUtils.putSharedAccelerator("CollapseTabGroup", newValue);
        } else {
            super.putValue(key, newValue);
        }
    }

    @Override
    public Object getValue(String key) {
        if ("AcceleratorKey".equals(key)) {
            return ActionUtils.getSharedAccelerator("CollapseTabGroup");
        }
        return super.getValue(key);
    }

}

