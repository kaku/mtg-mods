/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.windows.actions;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.actions.ActionUtils;
import org.netbeans.core.windows.actions.DockWindowAction;
import org.openide.util.NbBundle;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public final class UndockWindowAction
extends AbstractAction {
    private final TopComponent tc;

    public UndockWindowAction() {
        this.tc = null;
        this.putValue("Name", NbBundle.getMessage(DockWindowAction.class, (String)"CTL_UndockWindowAction"));
    }

    public UndockWindowAction(TopComponent tc) {
        this.tc = tc;
        this.putValue("Name", NbBundle.getMessage(DockWindowAction.class, (String)"CTL_UndockWindowAction"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        WindowManagerImpl wmi = WindowManagerImpl.getInstance();
        TopComponent contextTC = this.getTC2WorkWith();
        if (null == contextTC) {
            return;
        }
        boolean isDocked = wmi.isDocked(contextTC);
        ModeImpl mode = (ModeImpl)wmi.findMode(contextTC);
        if (isDocked) {
            wmi.userUndockedTopComponent(contextTC, mode);
        }
    }

    @Override
    public void putValue(String key, Object newValue) {
        if ("AcceleratorKey".equals(key)) {
            ActionUtils.putSharedAccelerator("UndockWindowAction", newValue);
        } else {
            super.putValue(key, newValue);
        }
    }

    @Override
    public Object getValue(String key) {
        if ("AcceleratorKey".equals(key)) {
            return ActionUtils.getSharedAccelerator("UndockWindowAction");
        }
        return super.getValue(key);
    }

    @Override
    public boolean isEnabled() {
        boolean res;
        TopComponent context = this.getTC2WorkWith();
        boolean bl = res = null != context;
        if (res && (res &= Switches.isTopComponentUndockingEnabled() && Switches.isUndockingEnabled(context))) {
            res &= WindowManagerImpl.getInstance().isDocked(context);
        }
        return res;
    }

    private TopComponent getTC2WorkWith() {
        if (this.tc != null) {
            return this.tc;
        }
        return WindowManager.getDefault().getRegistry().getActivated();
    }
}

