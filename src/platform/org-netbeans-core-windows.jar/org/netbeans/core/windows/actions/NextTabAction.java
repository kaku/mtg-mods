/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.actions;

import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.AbstractAction;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.WindowManagerImpl;
import org.openide.util.NbBundle;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public class NextTabAction
extends AbstractAction {
    public NextTabAction() {
        this.putValue("Name", NbBundle.getMessage(NextTabAction.class, (String)"CTL_NextTabAction"));
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        TopComponent select;
        TopComponent tc = TopComponent.getRegistry().getActivated();
        if (tc == null) {
            return;
        }
        ModeImpl mode = (ModeImpl)WindowManagerImpl.getInstance().findMode(tc);
        List<TopComponent> openedTcs = mode.getOpenedTopComponents();
        int index = openedTcs.indexOf((Object)tc);
        if (index == -1) {
            return;
        }
        if (++index >= openedTcs.size()) {
            index = 0;
        }
        if ((select = openedTcs.get(index)) == null) {
            return;
        }
        mode.setSelectedTopComponent(select);
        select.requestActive();
    }
}

