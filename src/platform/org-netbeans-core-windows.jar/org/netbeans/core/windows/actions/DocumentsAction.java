/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 */
package org.netbeans.core.windows.actions;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.view.ui.DocumentsDlg;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.windows.TopComponent;

public class DocumentsAction
extends AbstractAction
implements Runnable {
    private final PropertyChangeListener propListener;

    public DocumentsAction() {
        this.putValue("Name", NbBundle.getMessage(DocumentsAction.class, (String)"CTL_DocumentsAction"));
        this.propListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if ("opened".equals(evt.getPropertyName())) {
                    DocumentsAction.this.updateState();
                }
            }
        };
        TopComponent.Registry registry = TopComponent.getRegistry();
        registry.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.propListener, (Object)registry));
        if (SwingUtilities.isEventDispatchThread()) {
            this.updateState();
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    DocumentsAction.this.updateState();
                }
            });
        }
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        if (SwingUtilities.isEventDispatchThread()) {
            this.run();
        } else {
            SwingUtilities.invokeLater(this);
        }
    }

    @Override
    public void run() {
        DocumentsDlg.showDocumentsDialog();
    }

    private void updateState() {
        this.setEnabled(!DocumentsDlg.isEmpty());
    }

}

