/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallableSystemAction
 */
package org.netbeans.core.windows.actions;

import org.netbeans.core.windows.view.ui.NbSheet;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallableSystemAction;

public final class GlobalPropertiesAction
extends CallableSystemAction {
    public void performAction() {
        NbSheet c = NbSheet.findDefault();
        c.open();
        c.requestActive();
    }

    protected boolean asynchronous() {
        return false;
    }

    public String getName() {
        return NbBundle.getBundle(GlobalPropertiesAction.class).getString("GlobalProperties");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(GlobalPropertiesAction.class);
    }

    protected String iconResource() {
        return "org/netbeans/core/windows/resources/properties.gif";
    }
}

