/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 */
package org.netbeans.core.windows.actions;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.actions.ActionUtils;
import org.openide.awt.Mnemonics;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.util.actions.Presenter;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public final class MaximizeWindowAction
extends AbstractAction
implements Presenter.Popup,
Presenter.Menu {
    private final PropertyChangeListener propListener;
    private Reference<TopComponent> topComponent;
    private JCheckBoxMenuItem menuItem;
    private boolean state = true;

    public MaximizeWindowAction() {
        String label = NbBundle.getMessage(MaximizeWindowAction.class, (String)"CTL_MaximizeWindowAction");
        this.putValue("Name", label);
        this.propListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                String propName = evt.getPropertyName();
                if ("maximizedMode".equals(propName) || "editorAreaState".equals(evt.getPropertyName()) || "modes".equals(evt.getPropertyName()) || "activeMode".equals(evt.getPropertyName())) {
                    MaximizeWindowAction.this.updateState();
                }
                if ("activated".equals(propName)) {
                    MaximizeWindowAction.this.updateState();
                }
            }
        };
        TopComponent.Registry registry = TopComponent.getRegistry();
        registry.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.propListener, (Object)registry));
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        wm.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.propListener, (Object)((Object)wm)));
        this.updateState();
    }

    public MaximizeWindowAction(TopComponent tc) {
        String label = NbBundle.getMessage(MaximizeWindowAction.class, (String)"CTL_MaximizeWindowAction");
        this.putValue("Name", label);
        this.topComponent = new WeakReference<TopComponent>(tc);
        this.propListener = null;
        this.updateState();
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        this.state = !this.state;
        this.getMenuItem().setSelected(this.state);
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        TopComponent curTC = this.getTCToWorkWith();
        if (wm.isDocked(curTC)) {
            ModeImpl mode = (ModeImpl)wm.findMode(curTC);
            String tcID = wm.findTopComponentID(curTC);
            if (mode.getKind() == 2) {
                wm.userToggledTopComponentSlideInMaximize(tcID);
            } else if (null != mode) {
                ModeImpl previousMax = wm.getCurrentMaximizedMode();
                if (null != previousMax) {
                    if (previousMax.getKind() == 1 && mode.getKind() == 0) {
                        wm.switchMaximizedMode(mode);
                    } else {
                        wm.switchMaximizedMode(null);
                    }
                } else {
                    wm.switchMaximizedMode(mode);
                }
            } else {
                wm.switchMaximizedMode(null);
            }
        } else {
            ModeImpl curMax = (ModeImpl)wm.findMode(curTC);
            if (curMax != null) {
                if (curMax.getFrameState() == 0) {
                    curMax.setFrameState(6);
                } else {
                    curMax.setFrameState(0);
                }
            }
        }
        this.updateState();
    }

    private void updateState() {
        Mutex.EVENT.readAccess(new Runnable(){

            @Override
            public void run() {
                MaximizeWindowAction.this.doUpdateState();
            }
        });
    }

    private void doUpdateState() {
        boolean maximize;
        TopComponent active;
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        ModeImpl activeMode = (ModeImpl)wm.findMode(active = this.getTCToWorkWith());
        if (activeMode == null || !Switches.isTopComponentMaximizationEnabled() || !Switches.isMaximizationEnabled(active) || !wm.isDocked(active) && !wm.isEditorMode(activeMode)) {
            this.getMenuPresenter().setSelected(false);
            this.getPopupPresenter().setSelected(false);
            this.setEnabled(false);
            return;
        }
        if (wm.isDocked(active)) {
            maximize = wm.getCurrentMaximizedMode() != activeMode;
        } else {
            boolean bl = maximize = activeMode.getFrameState() == 0;
        }
        if (activeMode != null && activeMode.getKind() == 2) {
            maximize = null != active && !wm.isTopComponentMaximizedWhenSlidedIn(wm.findTopComponentID(active));
        }
        this.state = !maximize;
        this.getMenuPresenter().setSelected(this.state);
        this.getPopupPresenter().setSelected(this.state);
        this.setEnabled(activeMode != null);
    }

    private TopComponent getTCToWorkWith() {
        TopComponent tc;
        if (this.topComponent != null && (tc = this.topComponent.get()) != null) {
            return tc;
        }
        return TopComponent.getRegistry().getActivated();
    }

    @Override
    public void putValue(String key, Object newValue) {
        if ("AcceleratorKey".equals(key)) {
            ActionUtils.putSharedAccelerator("MaximizeWindow", newValue);
        } else {
            super.putValue(key, newValue);
        }
    }

    @Override
    public Object getValue(String key) {
        if ("AcceleratorKey".equals(key)) {
            return ActionUtils.getSharedAccelerator("MaximizeWindow");
        }
        return super.getValue(key);
    }

    public JMenuItem getPopupPresenter() {
        return this.getMenuItem();
    }

    public JMenuItem getMenuPresenter() {
        return this.getMenuItem();
    }

    private JMenuItem getMenuItem() {
        if (this.menuItem == null) {
            this.menuItem = new JCheckBoxMenuItem(this);
            Mnemonics.setLocalizedText((AbstractButton)this.menuItem, (String)((String)this.getValue("Name")));
            this.menuItem.setState(this.state);
        }
        return this.menuItem;
    }

}

