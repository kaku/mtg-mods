/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.WindowSystem
 *  org.openide.util.Lookup
 */
package org.netbeans.core.windows.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.netbeans.core.WindowSystem;
import org.openide.util.Lookup;

public final class SaveWindowsAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        ((WindowSystem)Lookup.getDefault().lookup(WindowSystem.class)).save();
    }
}

