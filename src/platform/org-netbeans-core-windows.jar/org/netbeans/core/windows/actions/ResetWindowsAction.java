/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.WindowSystem
 *  org.openide.ErrorManager
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponentGroup
 */
package org.netbeans.core.windows.actions;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.netbeans.core.WindowSystem;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.PersistenceHandler;
import org.netbeans.core.windows.RegistryImpl;
import org.netbeans.core.windows.TopComponentGroupImpl;
import org.netbeans.core.windows.TopComponentTracker;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.persistence.PersistenceManager;
import org.netbeans.core.windows.view.ui.MainWindow;
import org.openide.ErrorManager;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.TopComponentGroup;

public class ResetWindowsAction
implements ActionListener {
    private final boolean reset;

    public static ActionListener reset() {
        return new ResetWindowsAction(true);
    }

    public static ActionListener reload() {
        return new ResetWindowsAction(false);
    }

    public ResetWindowsAction(boolean reset) {
        this.reset = reset;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        final WindowSystem ws = (WindowSystem)Lookup.getDefault().lookup(WindowSystem.class);
        if (null == ws) {
            Logger.getLogger(ResetWindowsAction.class.getName()).log(Level.INFO, "Reset Windows action does not support custom WindowSystem implementations.");
            return;
        }
        final WindowManagerImpl wm = WindowManagerImpl.getInstance();
        MainWindow.getInstance().setFullScreenMode(false);
        wm.getMainWindow().setExtendedState(0);
        TopComponentGroupImpl projectTCGroup = (TopComponentGroupImpl)wm.findTopComponentGroup("OpenedProjects");
        final boolean isProjectsTCGroupOpened = null != projectTCGroup && projectTCGroup.isOpened();
        final TopComponent[] editors = this.collectEditors();
        wm.closeNonEditorViews();
        wm.getMainWindow().setVisible(false);
        final TopComponent activeEditor = wm.getArbitrarySelectedEditorTopComponent();
        wm.deselectEditorTopComponents();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                TopComponentGroup tcGroup;
                try {
                    FileObject rootFolder = PersistenceManager.getDefault().getRootLocalFolder();
                    if (ResetWindowsAction.this.reset && null != rootFolder) {
                        for (FileObject fo : rootFolder.getChildren()) {
                            if ("Components".equals(fo.getName())) continue;
                            fo.delete();
                        }
                    }
                }
                catch (IOException ioE) {
                    ErrorManager.getDefault().notify(1, (Throwable)ioE);
                }
                ws.hide();
                WindowManagerImpl.getInstance().resetModel();
                PersistenceManager.getDefault().reset();
                PersistenceHandler.getDefault().clear();
                ws.load();
                ws.show();
                if (isProjectsTCGroupOpened && null != (tcGroup = wm.findTopComponentGroup("OpenedProjects"))) {
                    tcGroup.open();
                }
                ModeImpl editorMode = (ModeImpl)wm.findMode("editor");
                RegistryImpl registry = (RegistryImpl)TopComponent.getRegistry();
                for (int i = 0; i < editors.length && null != editorMode; ++i) {
                    ModeImpl mode = (ModeImpl)wm.findMode(editors[i]);
                    if (null == mode) {
                        mode = editorMode;
                    }
                    if (null != mode) {
                        mode.addOpenedTopComponentNoNotify(editors[i]);
                    }
                    registry.addTopComponent(editors[i]);
                }
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        Frame mainWindow = wm.getMainWindow();
                        mainWindow.invalidate();
                        mainWindow.repaint();
                    }
                });
                if (null != activeEditor) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            activeEditor.requestActive();
                        }
                    });
                }
            }

        });
    }

    private TopComponent[] collectEditors() {
        TopComponentTracker tcTracker = TopComponentTracker.getDefault();
        ArrayList<TopComponent> editors = new ArrayList<TopComponent>(TopComponent.getRegistry().getOpened().size());
        ModeImpl editorMode = (ModeImpl)WindowManagerImpl.getInstance().findMode("editor");
        if (null != editorMode) {
            for (TopComponent tc : editorMode.getOpenedTopComponents()) {
                if (tcTracker.isViewTopComponent(tc)) continue;
                editors.add(tc);
            }
        }
        for (ModeImpl m : WindowManagerImpl.getInstance().getModes()) {
            if ("editor".equals(m.getName())) continue;
            for (TopComponent tc : m.getOpenedTopComponents()) {
                if (tcTracker.isViewTopComponent(tc)) continue;
                editors.add(tc);
            }
        }
        return editors.toArray((T[])new TopComponent[editors.size()]);
    }

}

