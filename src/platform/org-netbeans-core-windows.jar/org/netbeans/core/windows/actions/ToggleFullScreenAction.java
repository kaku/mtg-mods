/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.DynamicMenuContent
 *  org.openide.awt.Mnemonics
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.windows.actions;

import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.view.ui.MainWindow;
import org.openide.awt.DynamicMenuContent;
import org.openide.awt.Mnemonics;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.util.actions.SystemAction;
import org.openide.windows.WindowManager;

public class ToggleFullScreenAction
extends SystemAction
implements DynamicMenuContent,
Runnable {
    private JCheckBoxMenuItem[] menuItems;

    public ToggleFullScreenAction() {
        this.addPropertyChangeListener(new PropertyChangeListener(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if ("AcceleratorKey".equals(evt.getPropertyName())) {
                    ToggleFullScreenAction toggleFullScreenAction = ToggleFullScreenAction.this;
                    synchronized (toggleFullScreenAction) {
                        ToggleFullScreenAction.this.menuItems = null;
                        ToggleFullScreenAction.this.createItems();
                    }
                }
            }
        });
    }

    public JComponent[] getMenuPresenters() {
        this.createItems();
        this.updateState();
        return this.menuItems;
    }

    public JComponent[] synchMenuPresenters(JComponent[] items) {
        this.updateState();
        return this.menuItems;
    }

    private void updateState() {
        if (EventQueue.isDispatchThread()) {
            this.run();
        } else {
            SwingUtilities.invokeLater(this);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void run() {
        Frame frame = WindowManager.getDefault().getMainWindow();
        ToggleFullScreenAction toggleFullScreenAction = this;
        synchronized (toggleFullScreenAction) {
            this.createItems();
            this.menuItems[0].setSelected(null != frame && MainWindow.getInstance().isFullScreenMode());
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void createItems() {
        ToggleFullScreenAction toggleFullScreenAction = this;
        synchronized (toggleFullScreenAction) {
            if (this.menuItems == null) {
                this.menuItems = new JCheckBoxMenuItem[1];
                this.menuItems[0] = new JCheckBoxMenuItem((Action)((Object)this));
                this.menuItems[0].setIcon(null);
                Mnemonics.setLocalizedText((AbstractButton)this.menuItems[0], (String)NbBundle.getMessage(ToggleFullScreenAction.class, (String)"CTL_ToggleFullScreenAction"));
            }
        }
    }

    public void actionPerformed(ActionEvent ev) {
        MainWindow mainWindow;
        mainWindow.setFullScreenMode(!(mainWindow = MainWindow.getInstance()).isFullScreenMode());
    }

    public String getName() {
        return NbBundle.getMessage(ToggleFullScreenAction.class, (String)"CTL_ToggleFullScreenAction");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(ToggleFullScreenAction.class);
    }

    public boolean isEnabled() {
        return WindowManager.getDefault().getMainWindow() == MainWindow.getInstance().getFrame() && !Utilities.isMac();
    }

}

