/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 */
package org.netbeans.core.windows.actions;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.actions.ActionUtils;
import org.netbeans.core.windows.actions.DockModeAction;
import org.openide.util.NbBundle;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public final class UndockModeAction
extends AbstractAction {
    private final ModeImpl mode;

    public UndockModeAction() {
        this.mode = null;
        this.putValue("Name", NbBundle.getMessage(DockModeAction.class, (String)"CTL_UndockModeAction"));
    }

    public UndockModeAction(ModeImpl mode) {
        this.mode = mode;
        this.putValue("Name", NbBundle.getMessage(DockModeAction.class, (String)"CTL_UndockModeAction"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        boolean isDocked;
        WindowManagerImpl wmi = WindowManagerImpl.getInstance();
        ModeImpl contextMode = this.getMode2WorkWith();
        boolean bl = isDocked = contextMode.getState() == 0;
        if (isDocked) {
            wmi.userUndockedMode(contextMode);
        } else {
            wmi.userDockedMode(contextMode);
        }
    }

    @Override
    public void putValue(String key, Object newValue) {
        if ("AcceleratorKey".equals(key)) {
            ActionUtils.putSharedAccelerator("UndockModeAction", newValue);
        } else {
            super.putValue(key, newValue);
        }
    }

    @Override
    public Object getValue(String key) {
        if ("AcceleratorKey".equals(key)) {
            return ActionUtils.getSharedAccelerator("UndockModeAction");
        }
        return super.getValue(key);
    }

    @Override
    public boolean isEnabled() {
        boolean docked;
        ModeImpl contextMode = this.getMode2WorkWith();
        if (null == contextMode) {
            return false;
        }
        boolean bl = docked = contextMode.getState() == 0;
        if (!docked) {
            return false;
        }
        if (contextMode.getKind() == 1) {
            return Switches.isEditorModeUndockingEnabled();
        }
        return contextMode.getKind() == 0 && Switches.isViewModeUndockingEnabled();
    }

    private ModeImpl getMode2WorkWith() {
        if (this.mode != null) {
            return this.mode;
        }
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        return (ModeImpl)wm.findMode(wm.getRegistry().getActivated());
    }
}

