/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Cloneable
 */
package org.netbeans.core.windows.actions;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.actions.ActionUtils;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public class CloneDocumentAction
extends AbstractAction
implements PropertyChangeListener,
Runnable {
    public CloneDocumentAction() {
        this.putValue("Name", NbBundle.getMessage(CloneDocumentAction.class, (String)"CTL_CloneDocumentAction"));
        TopComponent.getRegistry().addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)TopComponent.getRegistry()));
        if (EventQueue.isDispatchThread()) {
            this.updateEnabled();
        } else {
            SwingUtilities.invokeLater(this);
        }
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        TopComponent tc = TopComponent.getRegistry().getActivated();
        if (tc == null || !(tc instanceof TopComponent.Cloneable)) {
            return;
        }
        ModeImpl mode = (ModeImpl)WindowManagerImpl.getInstance().findMode(tc);
        if (mode == null) {
            return;
        }
        if (mode.getKind() == 1) {
            ActionUtils.cloneWindow(tc);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("activated".equals(evt.getPropertyName())) {
            this.updateEnabled();
        }
    }

    private void updateEnabled() {
        TopComponent tc = TopComponent.getRegistry().getActivated();
        if (tc != null) {
            ModeImpl mode = (ModeImpl)WindowManagerImpl.getInstance().findMode(tc);
            this.setEnabled(tc instanceof TopComponent.Cloneable && mode != null && mode.getKind() == 1);
        }
    }

    @Override
    public void run() {
        this.updateEnabled();
    }
}

