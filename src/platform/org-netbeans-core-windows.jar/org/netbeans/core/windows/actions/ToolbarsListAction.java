/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 */
package org.netbeans.core.windows.actions;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.view.ui.toolbars.ToolbarConfiguration;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;
import org.openide.util.actions.Presenter;

public class ToolbarsListAction
extends AbstractAction
implements Presenter.Menu {
    public ToolbarsListAction() {
        this.putValue("Name", NbBundle.getMessage(ToolbarsListAction.class, (String)"CTL_ToolbarsListAction"));
        this.putValue("noIconInMenu", Boolean.TRUE);
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
    }

    public JMenuItem getMenuPresenter() {
        String label = NbBundle.getMessage(ToolbarsListAction.class, (String)"CTL_ToolbarsListAction");
        final JMenu menu = new JMenu(label);
        Mnemonics.setLocalizedText((AbstractButton)menu, (String)label);
        if (EventQueue.isDispatchThread()) {
            return ToolbarConfiguration.getToolbarsMenu(menu);
        }
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                ToolbarConfiguration.getToolbarsMenu(menu);
            }
        });
        return menu;
    }

}

