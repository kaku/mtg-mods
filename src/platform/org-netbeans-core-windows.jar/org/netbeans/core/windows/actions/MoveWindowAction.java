/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.actions;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.actions.ActionUtils;
import org.netbeans.core.windows.view.dnd.TopComponentDraggable;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public final class MoveWindowAction
extends AbstractAction
implements PropertyChangeListener {
    private final TopComponent tc;

    public MoveWindowAction() {
        this(null);
        TopComponent.getRegistry().addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)TopComponent.getRegistry()));
    }

    public MoveWindowAction(TopComponent tc) {
        this.putValue("Name", NbBundle.getMessage(ActionUtils.class, (String)"CTL_MoveWindowAction"));
        this.tc = tc;
        if (SwingUtilities.isEventDispatchThread()) {
            this.updateEnabled();
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    MoveWindowAction.this.updateEnabled();
                }
            });
        }
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        this.updateEnabled();
        if (!this.isEnabled()) {
            return;
        }
        TopComponent contextTc = this.getTCToWorkWith();
        if (null == contextTc) {
            return;
        }
        WindowManagerImpl.getInstance().userStartedKeyboardDragAndDrop(new TopComponentDraggable(contextTc));
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("activated".equals(evt.getPropertyName())) {
            this.updateEnabled();
        }
    }

    private void updateEnabled() {
        TopComponent contextTc = this.getTCToWorkWith();
        if (null == contextTc) {
            this.setEnabled(false);
            return;
        }
        ModeImpl mode = (ModeImpl)WindowManagerImpl.getInstance().findMode(contextTc);
        if (null == mode || WindowManagerImpl.getInstance().getCurrentMaximizedMode() != null) {
            this.setEnabled(false);
            return;
        }
        this.setEnabled(true);
    }

    private TopComponent getTCToWorkWith() {
        if (null != this.tc) {
            return this.tc;
        }
        return TopComponent.getRegistry().getActivated();
    }

    @Override
    public void putValue(String key, Object newValue) {
        if ("AcceleratorKey".equals(key)) {
            ActionUtils.putSharedAccelerator("MoveWindow", newValue);
        } else {
            super.putValue(key, newValue);
        }
    }

    @Override
    public Object getValue(String key) {
        if ("AcceleratorKey".equals(key)) {
            return ActionUtils.getSharedAccelerator("MoveWindow");
        }
        return super.getValue(key);
    }

}

