/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.actions;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.EditorOnlyDisplayer;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.actions.ActionUtils;
import org.openide.util.NbBundle;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public final class MoveWindowWithinModeAction
extends AbstractAction
implements PropertyChangeListener {
    private final boolean moveLeft;
    private final TopComponent tc;

    private MoveWindowWithinModeAction(boolean moveLeft) {
        this(null, moveLeft);
    }

    private MoveWindowWithinModeAction(TopComponent tc, boolean moveLeft) {
        this.moveLeft = moveLeft;
        this.tc = tc;
        if (null != tc) {
            this.putValue("Name", NbBundle.getMessage(MoveWindowWithinModeAction.class, (String)(moveLeft ? "CTL_MoveWindowLeftContextAction" : "CTL_MoveWindowRightContextAction")));
            if (SwingUtilities.isEventDispatchThread()) {
                this.updateEnabled();
            } else {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        MoveWindowWithinModeAction.this.updateEnabled();
                    }
                });
            }
        } else {
            this.putValue("Name", NbBundle.getMessage(MoveWindowWithinModeAction.class, (String)(moveLeft ? "CTL_MoveWindowLeftAction" : "CTL_MoveWindowRightAction")));
        }
    }

    public static Action createMoveLeft() {
        return new MoveWindowWithinModeAction(null, true);
    }

    public static Action createMoveRight() {
        return new MoveWindowWithinModeAction(null, false);
    }

    static Action createMoveLeft(TopComponent tc) {
        return new MoveWindowWithinModeAction(tc, true);
    }

    static Action createMoveRight(TopComponent tc) {
        return new MoveWindowWithinModeAction(tc, false);
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        TopComponent contextTc;
        if (EditorOnlyDisplayer.getInstance().isActive()) {
            return;
        }
        TopComponent topComponent = contextTc = null == this.tc ? TopComponent.getRegistry().getActivated() : this.tc;
        if (null == contextTc) {
            return;
        }
        ModeImpl mode = (ModeImpl)WindowManagerImpl.getInstance().findMode(contextTc);
        if (null == mode) {
            return;
        }
        int position = mode.getTopComponentTabPosition(contextTc);
        position = this.moveLeft ? --position : ++position;
        if (position >= 0 && position < mode.getOpenedTopComponents().size()) {
            mode.addOpenedTopComponent(contextTc, position);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("activated".equals(evt.getPropertyName())) {
            this.updateEnabled();
        }
    }

    private void updateEnabled() {
        if (null == this.tc) {
            return;
        }
        ModeImpl mode = (ModeImpl)WindowManagerImpl.getInstance().findMode(this.tc);
        if (null == mode) {
            return;
        }
        int position = mode.getTopComponentTabPosition(this.tc);
        if (0 == position && this.moveLeft) {
            this.setEnabled(false);
            return;
        }
        if (position == mode.getOpenedTopComponents().size() - 1 && !this.moveLeft) {
            this.setEnabled(false);
            return;
        }
        if (EditorOnlyDisplayer.getInstance().isActive()) {
            this.setEnabled(false);
            return;
        }
        this.setEnabled(true);
    }

    @Override
    public void putValue(String key, Object newValue) {
        if ("AcceleratorKey".equals(key)) {
            ActionUtils.putSharedAccelerator(this.moveLeft ? "MoveWindowLeft" : "MoveWindowRight", newValue);
        } else {
            super.putValue(key, newValue);
        }
    }

    @Override
    public Object getValue(String key) {
        if ("AcceleratorKey".equals(key)) {
            return ActionUtils.getSharedAccelerator(this.moveLeft ? "MoveWindowLeft" : "MoveWindowRight");
        }
        return super.getValue(key);
    }

}

