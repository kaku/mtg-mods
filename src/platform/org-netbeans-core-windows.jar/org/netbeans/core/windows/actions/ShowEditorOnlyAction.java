/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.DynamicMenuContent
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.actions;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.EditorOnlyDisplayer;
import org.netbeans.core.windows.TopComponentTracker;
import org.openide.awt.DynamicMenuContent;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.windows.TopComponent;

public class ShowEditorOnlyAction
extends AbstractAction
implements PropertyChangeListener,
Runnable,
DynamicMenuContent {
    private JCheckBoxMenuItem[] menuItems;

    private ShowEditorOnlyAction() {
        super(NbBundle.getMessage(ShowEditorOnlyAction.class, (String)"CTL_ShowOnlyEditor"));
        this.addPropertyChangeListener(new PropertyChangeListener(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if ("AcceleratorKey".equals(evt.getPropertyName())) {
                    ShowEditorOnlyAction showEditorOnlyAction = ShowEditorOnlyAction.this;
                    synchronized (showEditorOnlyAction) {
                        ShowEditorOnlyAction.this.menuItems = null;
                        ShowEditorOnlyAction.this.createItems();
                    }
                }
            }
        });
        TopComponent.getRegistry().addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)TopComponent.getRegistry()));
        if (EventQueue.isDispatchThread()) {
            this.updateState();
        } else {
            SwingUtilities.invokeLater(this);
        }
    }

    public static Action create() {
        return new ShowEditorOnlyAction();
    }

    public JComponent[] getMenuPresenters() {
        this.createItems();
        this.updateState();
        return this.menuItems;
    }

    public JComponent[] synchMenuPresenters(JComponent[] items) {
        this.updateState();
        return this.menuItems;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        EditorOnlyDisplayer eod;
        eod.setActive(!(eod = EditorOnlyDisplayer.getInstance()).isActive());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void createItems() {
        ShowEditorOnlyAction showEditorOnlyAction = this;
        synchronized (showEditorOnlyAction) {
            if (this.menuItems == null) {
                this.menuItems = new JCheckBoxMenuItem[1];
                this.menuItems[0] = new JCheckBoxMenuItem(this);
                this.menuItems[0].setIcon(null);
                Mnemonics.setLocalizedText((AbstractButton)this.menuItems[0], (String)NbBundle.getMessage(ShowEditorOnlyAction.class, (String)"CTL_ShowOnlyEditor"));
            }
        }
    }

    private void updateState() {
        if (EventQueue.isDispatchThread()) {
            this.run();
        } else {
            SwingUtilities.invokeLater(this);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void run() {
        boolean isDocumentActive = false;
        TopComponent tc = TopComponent.getRegistry().getActivated();
        if (tc != null) {
            isDocumentActive = TopComponentTracker.getDefault().isEditorTopComponent(tc);
        }
        ShowEditorOnlyAction showEditorOnlyAction = this;
        synchronized (showEditorOnlyAction) {
            this.createItems();
            this.menuItems[0].setSelected(EditorOnlyDisplayer.getInstance().isActive());
            this.menuItems[0].setEnabled(isDocumentActive);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("activated".equals(evt.getPropertyName())) {
            this.updateState();
        }
    }

}

