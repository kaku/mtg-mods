/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.windows.actions;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_SaveWindowsAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_SaveWindowsAction");
    }

    static String CTL_SwitchRoleKeepDocumentsAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_SwitchRoleKeepDocumentsAction");
    }

    private void Bundle() {
    }
}

