/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.netbeans.core.windows.WindowManagerImpl;

public final class SwitchRoleKeepDocumentsAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        String role = e.getActionCommand();
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        wm.setRole(role, true);
    }
}

