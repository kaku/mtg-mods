/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.actions;

import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.AbstractAction;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.actions.ActionUtils;
import org.openide.util.NbBundle;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public class CloseAllDocumentsAction
extends AbstractAction {
    private boolean isContext;

    public CloseAllDocumentsAction() {
        this(false);
    }

    public CloseAllDocumentsAction(boolean isContext) {
        this.isContext = isContext;
        String key = isContext ? "LBL_CloseAllDocumentsAction" : "CTL_CloseAllDocumentsAction";
        this.putValue("Name", NbBundle.getMessage(CloseAllDocumentsAction.class, (String)key));
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        ActionUtils.closeAllDocuments(this.isContext);
    }

    @Override
    public void putValue(String key, Object newValue) {
        if ("AcceleratorKey".equals(key)) {
            ActionUtils.putSharedAccelerator("CloseAllDocuments", newValue);
        } else {
            super.putValue(key, newValue);
        }
    }

    @Override
    public Object getValue(String key) {
        if ("AcceleratorKey".equals(key)) {
            return ActionUtils.getSharedAccelerator("CloseAllDocuments");
        }
        return super.getValue(key);
    }

    @Override
    public boolean isEnabled() {
        if (!Switches.isEditorTopComponentClosingEnabled()) {
            return false;
        }
        WindowManagerImpl wmi = WindowManagerImpl.getInstance();
        if (this.isContext) {
            TopComponent activeTC = TopComponent.getRegistry().getActivated();
            ModeImpl mode = (ModeImpl)wmi.findMode(activeTC);
            return mode != null && mode.getKind() == 1 && !mode.getOpenedTopComponents().isEmpty();
        }
        return wmi.getEditorTopComponents().length > 0;
    }
}

