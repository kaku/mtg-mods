/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.Mnemonics
 *  org.openide.cookies.SaveCookie
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Cloneable
 */
package org.netbeans.core.windows.actions;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.TopComponentTracker;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.actions.ActionsFactory;
import org.netbeans.core.windows.actions.CloseAllButThisAction;
import org.netbeans.core.windows.actions.CloseAllDocumentsAction;
import org.netbeans.core.windows.actions.CloseModeAction;
import org.netbeans.core.windows.actions.CloseWindowAction;
import org.netbeans.core.windows.actions.CollapseTabGroupAction;
import org.netbeans.core.windows.actions.DockModeAction;
import org.netbeans.core.windows.actions.DockWindowAction;
import org.netbeans.core.windows.actions.MaximizeWindowAction;
import org.netbeans.core.windows.actions.MinimizeModeAction;
import org.netbeans.core.windows.actions.MoveModeAction;
import org.netbeans.core.windows.actions.MoveWindowAction;
import org.netbeans.core.windows.actions.MoveWindowWithinModeAction;
import org.netbeans.core.windows.actions.NewTabGroupAction;
import org.netbeans.core.windows.actions.ResizeModeAction;
import org.netbeans.core.windows.actions.UndockModeAction;
import org.netbeans.core.windows.actions.UndockWindowAction;
import org.netbeans.core.windows.view.ui.slides.SlideController;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.cookies.SaveCookie;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.Presenter;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public abstract class ActionUtils {
    private static HashMap<Object, Object> sharedAccelerators = new HashMap();

    private ActionUtils() {
    }

    public static Action[] createDefaultPopupActions(TopComponent tc) {
        ModeImpl mode = ActionUtils.findMode(tc);
        int kind = mode != null ? mode.getKind() : 1;
        TopComponentTracker tcTracker = TopComponentTracker.getDefault();
        boolean isEditor = tcTracker.isEditorTopComponent(tc);
        ArrayList<Action> actions = new ArrayList<Action>();
        if (kind == 1) {
            if (Switches.isClosingEnabled(tc) && (isEditor && Switches.isEditorTopComponentClosingEnabled() || !isEditor && Switches.isViewTopComponentClosingEnabled())) {
                actions.add(new CloseWindowAction(tc));
            }
            if (Switches.isEditorTopComponentClosingEnabled()) {
                actions.add(new CloseAllDocumentsAction(true));
                CloseAllButThisAction allBut = new CloseAllButThisAction(tc, true);
                if (mode != null && mode.getOpenedTopComponents().size() == 1) {
                    allBut.setEnabled(false);
                }
                actions.add(allBut);
            }
            actions.add(null);
            if (Switches.isTopComponentMaximizationEnabled() && Switches.isMaximizationEnabled(tc)) {
                actions.add(new MaximizeWindowAction(tc));
            }
            if (Switches.isTopComponentUndockingEnabled() && Switches.isUndockingEnabled(tc)) {
                actions.add(new UndockWindowAction(tc));
            }
            if (Switches.isEditorModeUndockingEnabled() && isEditor) {
                actions.add(new UndockModeAction(mode));
            }
            if (Switches.isTopComponentUndockingEnabled() && Switches.isUndockingEnabled(tc)) {
                actions.add(new DockWindowAction(tc));
            }
            if (Switches.isEditorModeUndockingEnabled() && isEditor) {
                actions.add(new DockModeAction(mode, null));
            }
            actions.add(MoveWindowWithinModeAction.createMoveLeft(tc));
            actions.add(MoveWindowWithinModeAction.createMoveRight(tc));
            if (isEditor) {
                actions.add(null);
                actions.add(new NewTabGroupAction(tc));
                actions.add(new CollapseTabGroupAction(mode));
            }
        } else if (kind == 0) {
            if (Switches.isClosingEnabled(tc) && (isEditor && Switches.isEditorTopComponentClosingEnabled() || !isEditor && Switches.isViewTopComponentClosingEnabled())) {
                actions.add(new CloseWindowAction(tc));
            }
            if (Switches.isModeClosingEnabled()) {
                actions.add(new CloseModeAction(mode));
            }
            actions.add(null);
            if (Switches.isTopComponentMaximizationEnabled() && Switches.isMaximizationEnabled(tc)) {
                actions.add(new MaximizeWindowAction(tc));
            }
            if (Switches.isTopComponentSlidingEnabled() && Switches.isSlidingEnabled(tc)) {
                actions.add(ActionUtils.createMinimizeWindowAction(tc));
            }
            if (Switches.isModeSlidingEnabled()) {
                actions.add(new MinimizeModeAction(mode));
            }
            if (Switches.isTopComponentUndockingEnabled() && Switches.isUndockingEnabled(tc)) {
                actions.add(new UndockWindowAction(tc));
            }
            if (Switches.isViewModeUndockingEnabled()) {
                actions.add(new UndockModeAction(mode));
            }
            if (Switches.isTopComponentUndockingEnabled() && Switches.isUndockingEnabled(tc)) {
                actions.add(new DockWindowAction(tc));
            }
            if (Switches.isViewModeUndockingEnabled()) {
                actions.add(new DockModeAction(mode, null));
            }
            actions.add(null);
            actions.add(new MoveWindowAction(tc));
            actions.add(MoveWindowWithinModeAction.createMoveLeft(tc));
            actions.add(MoveWindowWithinModeAction.createMoveRight(tc));
            actions.add(new MoveModeAction(mode));
            actions.add(new ResizeModeAction(mode));
        } else if (kind == 2) {
            if (Switches.isClosingEnabled(tc) && (isEditor && Switches.isEditorTopComponentClosingEnabled() || !isEditor && Switches.isViewTopComponentClosingEnabled())) {
                actions.add(new CloseWindowAction(tc));
            }
            if (Switches.isModeClosingEnabled()) {
                actions.add(new CloseModeAction(mode));
            }
            actions.add(null);
            if (Switches.isTopComponentMaximizationEnabled() && Switches.isMaximizationEnabled(tc)) {
                actions.add(new MaximizeWindowAction(tc));
            }
            if (Switches.isTopComponentSlidingEnabled() && Switches.isSlidingEnabled(tc)) {
                actions.add(ActionUtils.createDisabledAction("CTL_MinimizeWindowAction"));
            }
            if (Switches.isModeSlidingEnabled()) {
                actions.add(ActionUtils.createDisabledAction("CTL_MinimizeModeAction"));
            }
            if (Switches.isTopComponentUndockingEnabled() && Switches.isUndockingEnabled(tc)) {
                actions.add(new UndockWindowAction(tc));
            }
            if (Switches.isViewModeUndockingEnabled()) {
                actions.add(ActionUtils.createDisabledAction("CTL_UndockModeAction"));
            }
            if (Switches.isTopComponentUndockingEnabled() && Switches.isUndockingEnabled(tc)) {
                actions.add(new DockWindowAction(tc));
            }
            if (Switches.isViewModeUndockingEnabled() || Switches.isModeSlidingEnabled()) {
                actions.add(new DockModeAction(ActionUtils.findPreviousMode(tc, mode), mode));
            }
            actions.add(null);
            actions.add(ActionUtils.createDisabledAction("CTL_MoveWindowAction"));
            actions.add(ActionUtils.createDisabledAction("CTL_MoveModeAction"));
            actions.add(ActionUtils.createDisabledAction("CTL_ResizeModeAction"));
        }
        Action[] res = actions.toArray(new Action[actions.size()]);
        for (ActionsFactory factory : Lookup.getDefault().lookupAll(ActionsFactory.class)) {
            res = factory.createPopupActions(tc, res);
        }
        return res;
    }

    public static Action[] createDefaultPopupActions(ModeImpl mode) {
        int kind = mode != null ? mode.getKind() : 1;
        ArrayList<Action> actions = new ArrayList<Action>();
        if (kind == 1) {
            if (Switches.isEditorTopComponentClosingEnabled()) {
                actions.add(ActionUtils.createDisabledAction("CTL_CloseWindowAction"));
                actions.add(new CloseAllDocumentsAction(true));
                actions.add(ActionUtils.createDisabledAction("CTL_CloseAllButThisAction"));
                actions.add(null);
            }
            actions.add(null);
            if (Switches.isTopComponentMaximizationEnabled()) {
                actions.add(ActionUtils.createDisabledAction("CTL_MaximizeWindowAction"));
            }
            if (Switches.isTopComponentUndockingEnabled()) {
                actions.add(ActionUtils.createDisabledAction("CTL_UndockWindowAction"));
            }
            if (Switches.isEditorModeUndockingEnabled()) {
                actions.add(new UndockModeAction(mode));
            }
            if (Switches.isTopComponentUndockingEnabled()) {
                actions.add(ActionUtils.createDisabledAction("CTL_UndockWindowAction_Dock"));
            }
            if (Switches.isEditorModeUndockingEnabled()) {
                actions.add(new DockModeAction(mode, null));
            }
            actions.add(null);
            actions.add(ActionUtils.createDisabledAction("CTL_NewTabGroupAction"));
            actions.add(new CollapseTabGroupAction(mode));
        } else if (kind == 0) {
            if (Switches.isViewTopComponentClosingEnabled()) {
                actions.add(ActionUtils.createDisabledAction("CTL_CloseWindowAction"));
            }
            if (Switches.isModeClosingEnabled()) {
                actions.add(new CloseModeAction(mode));
            }
            actions.add(null);
            if (Switches.isTopComponentMaximizationEnabled()) {
                actions.add(ActionUtils.createDisabledAction("CTL_MaximizeWindowAction"));
            }
            if (Switches.isTopComponentSlidingEnabled()) {
                actions.add(ActionUtils.createDisabledAction("LBL_AutoHideWindowAction"));
            }
            if (Switches.isModeSlidingEnabled()) {
                actions.add(new MinimizeModeAction(mode));
            }
            if (Switches.isTopComponentUndockingEnabled()) {
                actions.add(ActionUtils.createDisabledAction("CTL_UndockWindowAction"));
            }
            if (Switches.isViewModeUndockingEnabled()) {
                actions.add(new UndockModeAction(mode));
            }
            if (Switches.isTopComponentUndockingEnabled()) {
                actions.add(ActionUtils.createDisabledAction("CTL_UndockWindowAction_Dock"));
            }
            if (Switches.isViewModeUndockingEnabled()) {
                actions.add(new DockModeAction(mode, null));
            }
            actions.add(null);
            actions.add(ActionUtils.createDisabledAction("CTL_MoveWindowAction"));
            actions.add(new MoveModeAction(mode));
            actions.add(new ResizeModeAction(mode));
        } else if (kind == 2) {
            if (Switches.isViewTopComponentClosingEnabled()) {
                actions.add(ActionUtils.createDisabledAction("CTL_CloseWindowAction"));
                actions.add(new CloseModeAction(mode));
            }
            if (mode.getState() == 0 && Switches.isTopComponentMaximizationEnabled()) {
                actions.add(ActionUtils.createDisabledAction("CTL_MaximizeWindowAction"));
            }
            if (Switches.isTopComponentUndockingEnabled()) {
                actions.add(ActionUtils.createDisabledAction("CTL_UndockWindowAction"));
            }
        }
        Action[] res = actions.toArray(new Action[actions.size()]);
        for (ActionsFactory factory : Lookup.getDefault().lookupAll(ActionsFactory.class)) {
            res = factory.createPopupActions(mode, res);
        }
        return res;
    }

    static Action createMinimizeWindowAction(TopComponent tc) {
        SlideController slideController = (SlideController)((Object)SwingUtilities.getAncestorOfClass(SlideController.class, (Component)tc));
        ModeImpl mode = ActionUtils.findMode(tc);
        int tabIndex = null == mode ? -1 : mode.getOpenedTopComponents().indexOf((Object)tc);
        boolean initialState = WindowManagerImpl.getInstance().isTopComponentMinimized(tc);
        AutoHideWindowAction res = new AutoHideWindowAction(slideController, tabIndex, initialState);
        res.setEnabled(null != mode && mode.getState() == 0);
        return res;
    }

    public static void closeAllDocuments(boolean isContext) {
        if (isContext) {
            TopComponent activeTC = TopComponent.getRegistry().getActivated();
            List<TopComponent> tcs = ActionUtils.getOpened(activeTC);
            ActionUtils.closeAll(tcs.toArray((T[])new TopComponent[tcs.size()]));
        } else {
            TopComponent[] tcs = WindowManagerImpl.getInstance().getEditorTopComponents();
            ActionUtils.closeAll(tcs);
        }
    }

    private static void closeAll(TopComponent[] tcs) {
        for (TopComponent tc : tcs) {
            if (!Switches.isClosingEnabled(tc)) continue;
            final TopComponent toBeClosed = tc;
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    toBeClosed.putClientProperty((Object)"inCloseAll", (Object)Boolean.TRUE);
                    toBeClosed.close();
                }
            });
        }
    }

    public static void closeAllExcept(TopComponent tc, boolean isContext) {
        if (isContext) {
            List<TopComponent> tcs = ActionUtils.getOpened(tc);
            for (TopComponent curTC : tcs) {
                if (!Switches.isClosingEnabled(curTC) || curTC == tc) continue;
                curTC.putClientProperty((Object)"inCloseAll", (Object)Boolean.TRUE);
                curTC.close();
            }
        } else {
            TopComponent[] tcs;
            for (TopComponent curTC : tcs = WindowManagerImpl.getInstance().getEditorTopComponents()) {
                if (!Switches.isClosingEnabled(curTC) || curTC == tc) continue;
                curTC.putClientProperty((Object)"inCloseAll", (Object)Boolean.TRUE);
                curTC.close();
            }
        }
    }

    private static List<TopComponent> getOpened(TopComponent tc) {
        ModeImpl mode = ActionUtils.findMode(tc);
        ArrayList<TopComponent> tcs = new ArrayList<TopComponent>();
        if (mode != null) {
            tcs.addAll(mode.getOpenedTopComponents());
        }
        return tcs;
    }

    static void closeWindow(TopComponent tc) {
        tc.close();
    }

    private static void saveDocument(TopComponent tc) {
        SaveCookie sc = ActionUtils.getSaveCookie(tc);
        if (sc != null) {
            try {
                sc.save();
            }
            catch (IOException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
            }
        }
    }

    private static SaveCookie getSaveCookie(TopComponent tc) {
        Lookup lookup = tc.getLookup();
        Object obj = lookup.lookup(SaveCookie.class);
        if (obj instanceof SaveCookie) {
            return (SaveCookie)obj;
        }
        return null;
    }

    static void cloneWindow(TopComponent tc) {
        if (tc instanceof TopComponent.Cloneable) {
            TopComponent clone = ((TopComponent.Cloneable)tc).cloneComponent();
            int openIndex = -1;
            ModeImpl m = ActionUtils.findMode(tc);
            if (null != m) {
                TopComponent[] tcs = m.getTopComponents();
                for (int i = 0; i < tcs.length; ++i) {
                    if (tcs[i] != tc) continue;
                    openIndex = i + 1;
                    break;
                }
                if (openIndex >= tcs.length) {
                    openIndex = -1;
                }
            }
            if (openIndex >= 0) {
                clone.openAtTabPosition(openIndex);
            } else {
                clone.open();
            }
            clone.requestActive();
        }
    }

    static void putSharedAccelerator(Object key, Object value) {
        sharedAccelerators.put(key, value);
    }

    static Object getSharedAccelerator(Object key) {
        return sharedAccelerators.get(key);
    }

    private static Action createDisabledAction(String bundleKey) {
        return new DisabledAction(NbBundle.getMessage(ActionUtils.class, (String)bundleKey));
    }

    static ModeImpl findMode(TopComponent tc) {
        TopComponent multiviewParent;
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        ModeImpl mode = (ModeImpl)wm.findMode(tc);
        if (null == mode && null != (multiviewParent = (TopComponent)SwingUtilities.getAncestorOfClass(TopComponent.class, (Component)tc))) {
            mode = (ModeImpl)wm.findMode(multiviewParent);
        }
        return mode;
    }

    private static ModeImpl findPreviousMode(TopComponent tc, ModeImpl slidingMode) {
        ModeImpl res = null;
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        String tcId = wm.findTopComponentID(tc);
        if (null != tcId) {
            res = wm.getPreviousModeForTopComponent(tcId, slidingMode);
        }
        return res;
    }

    private static class DisabledAction
    extends AbstractAction {
        private DisabledAction(String name) {
            super(name);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
        }

        @Override
        public boolean isEnabled() {
            return false;
        }
    }

    private static class CloneDocumentAction
    extends AbstractAction {
        private final TopComponent tc;

        public CloneDocumentAction(TopComponent tc) {
            this.tc = tc;
            this.putValue("Name", NbBundle.getMessage(ActionUtils.class, (String)"LBL_CloneDocumentAction"));
            this.putValue("_nb_action_id_", "clone");
            this.setEnabled(tc instanceof TopComponent.Cloneable);
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            ActionUtils.cloneWindow(this.tc);
        }
    }

    public static final class ToggleWindowTransparencyAction
    extends AbstractAction {
        public ToggleWindowTransparencyAction(SlideController slideController, int tabIndex, boolean initialState) {
            this.putValue("Name", NbBundle.getMessage(ActionUtils.class, (String)"LBL_ToggleWindowTransparencyAction"));
        }

        public HelpCtx getHelpCtx() {
            return null;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)new NotifyDescriptor.Message((Object)NbBundle.getMessage(ActionUtils.class, (String)"LBL_WindowTransparencyHint"), 1));
        }
    }

    public static final class AutoHideWindowAction
    extends AbstractAction
    implements Presenter.Popup {
        private final SlideController slideController;
        private final int tabIndex;
        private boolean state;
        private JCheckBoxMenuItem menuItem;

        public AutoHideWindowAction(SlideController slideController, int tabIndex, boolean initialState) {
            this.slideController = slideController;
            this.tabIndex = tabIndex;
            this.state = initialState;
            this.putValue("Name", NbBundle.getMessage(ActionUtils.class, (String)"LBL_AutoHideWindowAction"));
        }

        public HelpCtx getHelpCtx() {
            return null;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.state = !this.state;
            this.getMenuItem().setSelected(this.state);
            this.slideController.userToggledAutoHide(this.tabIndex, this.state);
        }

        public JMenuItem getPopupPresenter() {
            return this.getMenuItem();
        }

        private JCheckBoxMenuItem getMenuItem() {
            if (this.menuItem == null) {
                this.menuItem = new JCheckBoxMenuItem("", this.state);
                Mnemonics.setLocalizedText((AbstractButton)this.menuItem, (String)((String)this.getValue("Name")));
                this.menuItem.setAccelerator(KeyStroke.getKeyStroke(8, 128));
                this.menuItem.addActionListener(this);
                this.menuItem.setEnabled(this.isEnabled());
            }
            return this.menuItem;
        }
    }

}

