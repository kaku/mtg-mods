/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.windows.actions;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.actions.ActionUtils;
import org.netbeans.core.windows.actions.CloseModeAction;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public final class MinimizeModeAction
extends AbstractAction
implements PropertyChangeListener {
    private ModeImpl mode;

    public MinimizeModeAction() {
        this.putValue("Name", NbBundle.getMessage(CloseModeAction.class, (String)"CTL_MinimizeModeAction"));
        TopComponent.getRegistry().addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)TopComponent.getRegistry()));
        WindowManager.getDefault().addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)WindowManager.getDefault()));
        if (SwingUtilities.isEventDispatchThread()) {
            this.updateEnabled();
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    MinimizeModeAction.this.updateEnabled();
                }
            });
        }
    }

    public MinimizeModeAction(ModeImpl mode) {
        this.putValue("Name", NbBundle.getMessage(ActionUtils.class, (String)"CTL_MinimizeModeAction"));
        this.mode = mode;
        this.setEnabled(Switches.isModeSlidingEnabled() && mode.getKind() == 0 && mode.getState() == 0);
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        TopComponent tc;
        ModeImpl contextMode = this.mode;
        if (contextMode == null && null != (tc = TopComponent.getRegistry().getActivated())) {
            contextMode = (ModeImpl)WindowManagerImpl.getInstance().findMode(tc);
        }
        if (contextMode != null) {
            WindowManagerImpl.getInstance().userMinimizedMode(contextMode);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("activated".equals(evt.getPropertyName()) || "modes".equals(evt.getPropertyName()) || "activeMode".equals(evt.getPropertyName())) {
            this.updateEnabled();
        }
    }

    private void updateEnabled() {
        TopComponent tc;
        ModeImpl contextMode = this.mode;
        if (contextMode == null && null != (tc = TopComponent.getRegistry().getActivated())) {
            contextMode = (ModeImpl)WindowManagerImpl.getInstance().findMode(tc);
        }
        if (null == contextMode) {
            this.setEnabled(false);
            return;
        }
        this.setEnabled(Switches.isModeSlidingEnabled() && contextMode.getKind() == 0 && contextMode.getState() == 0);
    }

    @Override
    public void putValue(String key, Object newValue) {
        if ("AcceleratorKey".equals(key)) {
            ActionUtils.putSharedAccelerator("MinimizeMode", newValue);
        } else {
            super.putValue(key, newValue);
        }
    }

    @Override
    public Object getValue(String key) {
        if ("AcceleratorKey".equals(key)) {
            return ActionUtils.getSharedAccelerator("MinimizeMode");
        }
        return super.getValue(key);
    }

}

