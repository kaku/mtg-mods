/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.windows.actions;

import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.AbstractAction;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.actions.ActionUtils;
import org.openide.util.NbBundle;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public final class NewTabGroupAction
extends AbstractAction {
    private final TopComponent tc;

    public NewTabGroupAction() {
        this(null);
    }

    public NewTabGroupAction(TopComponent tc) {
        this.tc = tc;
        this.putValue("Name", NbBundle.getMessage(NewTabGroupAction.class, (String)"CTL_NewTabGroupAction"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        WindowManagerImpl wmi = WindowManagerImpl.getInstance();
        TopComponent contextTC = this.getTC2WorkWith();
        if (null == contextTC) {
            return;
        }
        ModeImpl currentMode = (ModeImpl)wmi.findMode(contextTC);
        if (null == currentMode || currentMode.getKind() != 1 || !wmi.isDocked(contextTC)) {
            return;
        }
        wmi.newTabGroup(contextTC);
    }

    @Override
    public void putValue(String key, Object newValue) {
        if ("AcceleratorKey".equals(key)) {
            ActionUtils.putSharedAccelerator("NewTabGroupAction", newValue);
        } else {
            super.putValue(key, newValue);
        }
    }

    @Override
    public Object getValue(String key) {
        if ("AcceleratorKey".equals(key)) {
            return ActionUtils.getSharedAccelerator("NewTabGroupAction");
        }
        return super.getValue(key);
    }

    @Override
    public boolean isEnabled() {
        ModeImpl mode;
        WindowManagerImpl wm;
        boolean res;
        TopComponent context = this.getTC2WorkWith();
        boolean bl = res = null != context;
        if (res && (res &= null != (mode = (ModeImpl)(wm = WindowManagerImpl.getInstance()).findMode(context)))) {
            res &= mode.getKind() == 1;
            res &= mode.getOpenedTopComponents().size() > 1;
            res &= wm.isDocked(context);
        }
        return res;
    }

    private TopComponent getTC2WorkWith() {
        if (this.tc != null) {
            return this.tc;
        }
        return WindowManager.getDefault().getRegistry().getActivated();
    }
}

