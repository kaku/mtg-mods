/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.windows.actions;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.actions.ActionUtils;
import org.netbeans.core.windows.actions.CloseModeAction;
import org.netbeans.core.windows.view.ui.slides.SlideController;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public final class MinimizeWindowAction
extends AbstractAction
implements PropertyChangeListener {
    public MinimizeWindowAction() {
        this.putValue("Name", NbBundle.getMessage(CloseModeAction.class, (String)"CTL_MinimizeWindowAction"));
        TopComponent.getRegistry().addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)TopComponent.getRegistry()));
        WindowManager.getDefault().addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)WindowManager.getDefault()));
        if (SwingUtilities.isEventDispatchThread()) {
            this.setEnabled(this.checkEnabled());
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    MinimizeWindowAction.this.setEnabled(MinimizeWindowAction.this.checkEnabled());
                }
            });
        }
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        TopComponent context = TopComponent.getRegistry().getActivated();
        if (null == context) {
            return;
        }
        Action a = ActionUtils.createMinimizeWindowAction(context);
        if (a.isEnabled()) {
            a.actionPerformed(ev);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("activated".equals(evt.getPropertyName()) || "modes".equals(evt.getPropertyName()) || "activeMode".equals(evt.getPropertyName())) {
            this.setEnabled(this.checkEnabled());
        }
    }

    private boolean checkEnabled() {
        TopComponent context = TopComponent.getRegistry().getActivated();
        if (null == context) {
            return false;
        }
        SlideController slideController = (SlideController)((Object)SwingUtilities.getAncestorOfClass(SlideController.class, (Component)context));
        if (null == slideController) {
            return false;
        }
        ModeImpl mode = (ModeImpl)WindowManagerImpl.getInstance().findMode(context);
        if (null == mode) {
            return false;
        }
        if (WindowManagerImpl.getInstance().isTopComponentMinimized(context)) {
            return false;
        }
        if (mode.getState() != 0) {
            return false;
        }
        if (mode.getKind() != 0) {
            return false;
        }
        return Switches.isTopComponentSlidingEnabled() && Switches.isSlidingEnabled(context);
    }

    @Override
    public void putValue(String key, Object newValue) {
        if ("AcceleratorKey".equals(key)) {
            ActionUtils.putSharedAccelerator("MinimizeWindow", newValue);
        } else {
            super.putValue(key, newValue);
        }
    }

    @Override
    public Object getValue(String key) {
        if ("AcceleratorKey".equals(key)) {
            return ActionUtils.getSharedAccelerator("MinimizeWindow");
        }
        return super.getValue(key);
    }

}

