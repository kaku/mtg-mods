/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 */
package org.netbeans.core.windows.actions;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.actions.ActionUtils;
import org.openide.util.NbBundle;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public final class DockModeAction
extends AbstractAction {
    private final ModeImpl mode;
    private ModeImpl slidingMode;

    public DockModeAction() {
        this.mode = null;
        this.putValue("Name", NbBundle.getMessage(DockModeAction.class, (String)"CTL_UndockModeAction_Dock"));
    }

    public DockModeAction(ModeImpl modeToRestore, ModeImpl slidingMode) {
        this.mode = modeToRestore;
        this.slidingMode = slidingMode;
        this.putValue("Name", NbBundle.getMessage(DockModeAction.class, (String)"CTL_UndockModeAction_Dock"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        WindowManagerImpl wmi = WindowManagerImpl.getInstance();
        ModeImpl contextMode = this.getMode2WorkWith();
        if (null == contextMode) {
            return;
        }
        if (null != this.slidingMode) {
            WindowManagerImpl.getInstance().userRestoredMode(this.slidingMode, contextMode);
        } else {
            boolean isDocked;
            boolean bl = isDocked = contextMode.getState() == 0;
            if (!isDocked) {
                wmi.userDockedMode(contextMode);
            }
        }
    }

    @Override
    public void putValue(String key, Object newValue) {
        if ("AcceleratorKey".equals(key)) {
            ActionUtils.putSharedAccelerator("DockModeAction", newValue);
        } else {
            super.putValue(key, newValue);
        }
    }

    @Override
    public Object getValue(String key) {
        if ("AcceleratorKey".equals(key)) {
            return ActionUtils.getSharedAccelerator("DockModeAction");
        }
        return super.getValue(key);
    }

    @Override
    public boolean isEnabled() {
        boolean docked;
        ModeImpl contextMode = this.getMode2WorkWith();
        if (null == contextMode) {
            return false;
        }
        if (null != this.slidingMode) {
            return true;
        }
        boolean bl = docked = contextMode.getState() == 0;
        if (docked) {
            return false;
        }
        if (contextMode.getKind() == 1) {
            return Switches.isEditorModeUndockingEnabled();
        }
        return contextMode.getKind() == 0 && Switches.isViewModeUndockingEnabled();
    }

    private ModeImpl getMode2WorkWith() {
        if (this.mode != null) {
            return this.mode;
        }
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        return (ModeImpl)wm.findMode(wm.getRegistry().getActivated());
    }
}

