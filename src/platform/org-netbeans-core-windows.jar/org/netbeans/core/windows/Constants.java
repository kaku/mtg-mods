/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows;

import java.awt.Dimension;

public abstract class Constants {
    public static final int EDITOR_AREA_JOINED = 0;
    public static final int EDITOR_AREA_SEPARATED = 1;
    public static final int MODE_STATE_JOINED = 0;
    public static final int MODE_STATE_SEPARATED = 1;
    public static final int MODE_KIND_EDITOR = 1;
    public static final int MODE_KIND_VIEW = 0;
    public static final int MODE_KIND_SLIDING = 2;
    public static final int VERTICAL = 0;
    public static final int HORIZONTAL = 1;
    public static final String TOP = "top";
    public static final String BOTTOM = "bottom";
    public static final String LEFT = "left";
    public static final String RIGHT = "right";
    public static final int DIVIDER_SIZE_VERTICAL = 4;
    public static final int DIVIDER_SIZE_HORIZONTAL = 4;
    public static final int DROP_AREA_SIZE = 20;
    public static final int DRAG_GESTURE_START_DISTANCE = 10;
    public static final int DRAG_GESTURE_START_TIME = 200;
    public static final double DROP_TO_SIDE_RATIO = 0.5;
    public static final double DROP_AROUND_EDITOR_RATIO = 0.25;
    public static final double DROP_BETWEEN_RATIO = 0.3333333333333333;
    public static final double DROP_AROUND_RATIO = 0.25;
    public static final Dimension DROP_NEW_MODE_SIZE = new Dimension(300, 200);
    public static final String TOPCOMPONENT_ALLOW_DOCK_ANYWHERE = "TopComponentAllowDockAnywhere";
    public static final String KEEP_NON_PERSISTENT_TC_IN_MODEL_WHEN_CLOSED = "KeepNonPersistentTCInModelWhenClosed";
    public static final String KEEP_PREFERRED_SIZE_WHEN_SLIDED_IN = "netbeans.winsys.tc.keep_preferred_size_when_slided_in";
    public static final String ACTIVATE_AT_STARTUP = "netbeans.winsys.tc.activate_at_startup";
    public static final String SEPARATE_WINDOW_PROPERTY = "SeparateWindow";
    public static final boolean SWITCH_MODE_ADD_NO_RESTRICT = Boolean.getBoolean("netbeans.winsys.allow.dock.anywhere");
    public static final boolean SWITCH_DND_DISABLE = Boolean.getBoolean("netbeans.winsys.disable_dnd");
    public static final boolean SWITCH_DROP_INDICATION_FADE = Boolean.getBoolean("netbeans.winsys.dndfade.on");
    public static final boolean SWITCH_STATUSLINE_IN_MENUBAR = Boolean.getBoolean("netbeans.winsys.statusLine.in.menuBar");
    public static final String SWITCH_IMAGE_SOURCE = System.getProperty("netbeans.winsys.imageSource");
    public static final boolean SWITCH_HIDE_EMPTY_DOCUMENT_AREA = Boolean.getBoolean("netbeans.winsys.hideEmptyDocArea");
    public static final boolean NO_TOOLBARS = Boolean.getBoolean("netbeans.winsys.no_toolbars");
    public static final String CUSTOM_MENU_BAR_PATH = System.getProperty("netbeans.winsys.menu_bar.path");
    public static final String CUSTOM_STATUS_LINE_PATH = System.getProperty("netbeans.winsys.status_line.path");
    public static final boolean DO_NOT_SHOW_HELP_IN_DIALOGS = Boolean.getBoolean("netbeans.winsys.no_help_in_dialogs");
    public static final boolean AUTO_ICONIFY = Boolean.getBoolean("netbeans.winsys.auto_iconify");

    private Constants() {
    }
}

