/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 *  org.openide.util.WeakSet
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.WindowManagerImpl;
import org.openide.util.NbPreferences;
import org.openide.util.WeakSet;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public final class TopComponentTracker {
    private final Set<String> viewIds = new HashSet<String>(30);
    private final Set<String> editorIds = new HashSet<String>(30);
    private final Set<TopComponent> editors = new WeakSet(100);
    private static TopComponentTracker theInstance;

    private TopComponentTracker() {
    }

    public static synchronized TopComponentTracker getDefault() {
        if (null == theInstance) {
            theInstance = new TopComponentTracker();
        }
        return theInstance;
    }

    void clear() {
        this.viewIds.clear();
        this.editorIds.clear();
        this.editors.clear();
    }

    void load() {
        Preferences prefs = this.getPreferences();
        try {
            for (String key : prefs.keys()) {
                boolean view = prefs.getBoolean(key, false);
                if (view) {
                    this.viewIds.add(key);
                    continue;
                }
                this.editorIds.add(key);
            }
        }
        catch (BackingStoreException ex) {
            Logger.getLogger(TopComponentTracker.class.getName()).log(Level.INFO, null, ex);
        }
    }

    void save() {
        Preferences prefs = this.getPreferences();
        try {
            prefs.clear();
        }
        catch (BackingStoreException ex) {
            Logger.getLogger(TopComponentTracker.class.getName()).log(Level.INFO, null, ex);
        }
        for (String id2 : this.viewIds) {
            prefs.putBoolean(id2, true);
        }
        for (String id2 : this.editorIds) {
            prefs.putBoolean(id2, false);
        }
    }

    void add(TopComponent tc, ModeImpl mode) {
        if (tc.getPersistenceType() == 2) {
            return;
        }
        String tcId = WindowManagerImpl.getInstance().findTopComponentID(tc);
        if (null == tcId) {
            return;
        }
        if (this.viewIds.contains(tcId) || this.editorIds.contains(tcId)) {
            return;
        }
        if (mode.getKind() != 1) {
            if (this.editors.contains((Object)tc)) {
                this.editorIds.add(tcId);
            } else {
                this.viewIds.add(tcId);
            }
        } else {
            this.editors.add(tc);
        }
    }

    void add(String tcId, ModeImpl mode) {
        if (this.viewIds.contains(tcId) || this.editorIds.contains(tcId)) {
            return;
        }
        if (mode.getKind() != 1) {
            this.viewIds.add(tcId);
        }
    }

    public boolean isEditorTopComponent(TopComponent tc) {
        return !this.isViewTopComponent(tc);
    }

    public boolean isViewTopComponent(TopComponent tc) {
        if (tc.getPersistenceType() == 2) {
            ModeImpl mode = (ModeImpl)WindowManagerImpl.getInstance().findMode(tc);
            return null != mode && mode.getKind() != 1;
        }
        String id = WindowManagerImpl.getInstance().findTopComponentID(tc);
        return id != null && this.viewIds.contains(id);
    }

    private Preferences getPreferences() {
        Preferences prefs = NbPreferences.forModule(TopComponentTracker.class).node("tctracker");
        String role = WindowManagerImpl.getInstance().getRole();
        if (null != role) {
            prefs = prefs.node(role);
        }
        return prefs;
    }
}

