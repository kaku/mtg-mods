/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.SpecificationVersion
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.windows.persistence;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.windows.Debug;
import org.netbeans.core.windows.persistence.InternalConfig;
import org.netbeans.core.windows.persistence.PersistenceManager;
import org.netbeans.core.windows.persistence.TCGroupConfig;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.modules.SpecificationVersion;
import org.openide.util.NbBundle;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

class TCGroupParser {
    public static final String INSTANCE_DTD_ID_2_0 = "-//NetBeans//DTD Top Component in Group Properties 2.0//EN";
    private static final boolean DEBUG = Debug.isLoggable(TCGroupParser.class);
    private String tc_id;
    private FileObject moduleParentFolder;
    private FileObject localParentFolder;
    private InternalConfig internalConfig;
    private boolean inModuleFolder;
    private boolean inLocalFolder;

    public TCGroupParser(String tc_id) {
        this.tc_id = tc_id;
    }

    TCGroupConfig load() throws IOException {
        if (DEBUG) {
            Debug.log(TCGroupParser.class, "load ENTER tcGrp:" + this.tc_id);
        }
        TCGroupConfig tcGroupCfg = new TCGroupConfig();
        PropertyHandler propertyHandler = new PropertyHandler();
        InternalConfig internalCfg = this.getInternalConfig();
        internalCfg.clear();
        propertyHandler.readData(tcGroupCfg, internalCfg);
        if (DEBUG) {
            Debug.log(TCGroupParser.class, "load LEAVE tcGrp:" + this.tc_id);
        }
        return tcGroupCfg;
    }

    void save(TCGroupConfig tcGroupCfg) throws IOException {
        if (DEBUG) {
            Debug.log(TCGroupParser.class, "save ENTER tcGrp:" + this.tc_id);
        }
        PropertyHandler propertyHandler = new PropertyHandler();
        InternalConfig internalCfg = this.getInternalConfig();
        propertyHandler.writeData(tcGroupCfg, internalCfg);
        if (DEBUG) {
            Debug.log(TCGroupParser.class, "save LEAVE tcGrp:" + this.tc_id);
        }
    }

    String getName() {
        return this.tc_id;
    }

    InternalConfig getInternalConfig() {
        if (this.internalConfig == null) {
            this.internalConfig = new InternalConfig();
        }
        return this.internalConfig;
    }

    boolean isInModuleFolder() {
        return this.inModuleFolder;
    }

    void setInModuleFolder(boolean inModuleFolder) {
        this.inModuleFolder = inModuleFolder;
    }

    boolean isInLocalFolder() {
        return this.inLocalFolder;
    }

    void setInLocalFolder(boolean inLocalFolder) {
        this.inLocalFolder = inLocalFolder;
    }

    void setModuleParentFolder(FileObject moduleParentFolder) {
        this.moduleParentFolder = moduleParentFolder;
    }

    void setLocalParentFolder(FileObject localParentFolder) {
        this.localParentFolder = localParentFolder;
    }

    void log(String s) {
        Debug.log(TCGroupParser.class, s);
    }

    private final class PropertyHandler
    extends DefaultHandler {
        private TCGroupConfig tcGroupConfig;
        private InternalConfig internalConfig;
        private final Object RW_LOCK;

        public PropertyHandler() {
            this.tcGroupConfig = null;
            this.internalConfig = null;
            this.RW_LOCK = new Object();
        }

        private FileObject getConfigFOInput() {
            FileObject tcGroupConfigFO = TCGroupParser.this.isInLocalFolder() ? TCGroupParser.this.localParentFolder.getFileObject(TCGroupParser.this.getName(), "wstcgrp") : (TCGroupParser.this.isInModuleFolder() ? TCGroupParser.this.moduleParentFolder.getFileObject(TCGroupParser.this.getName(), "wstcgrp") : null);
            return tcGroupConfigFO;
        }

        private FileObject getConfigFOOutput() throws IOException {
            FileObject tcGroupConfigFO = TCGroupParser.this.localParentFolder.getFileObject(TCGroupParser.this.getName(), "wstcgrp");
            if (tcGroupConfigFO != null) {
                return tcGroupConfigFO;
            }
            StringBuffer buffer = new StringBuffer();
            buffer.append(TCGroupParser.this.getName());
            buffer.append('.');
            buffer.append("wstcgrp");
            tcGroupConfigFO = FileUtil.createData((FileObject)TCGroupParser.this.localParentFolder, (String)buffer.toString());
            return tcGroupConfigFO;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void readData(TCGroupConfig tcGroupCfg, InternalConfig internalCfg) throws IOException {
            this.tcGroupConfig = tcGroupCfg;
            this.internalConfig = internalCfg;
            FileObject cfgFOInput = this.getConfigFOInput();
            if (cfgFOInput == null) {
                throw new FileNotFoundException("[WinSys] Missing TCGroup configuration file:" + TCGroupParser.this.getName());
            }
            InputStream is = null;
            try {
                Object object = this.RW_LOCK;
                synchronized (object) {
                    is = cfgFOInput.getInputStream();
                    PersistenceManager.getDefault().getXMLParser(this).parse(new InputSource(is));
                }
            }
            catch (SAXException exc) {
                String msg = NbBundle.getMessage(TCGroupParser.class, (String)"EXC_TCGroupParse", (Object)cfgFOInput);
                throw (IOException)new IOException(msg).initCause(exc);
            }
            finally {
                try {
                    if (is != null) {
                        is.close();
                    }
                }
                catch (IOException exc) {
                    Logger.getLogger(TCGroupParser.class.getName()).log(Level.WARNING, null, exc);
                }
            }
            tcGroupCfg = this.tcGroupConfig;
            internalCfg = this.internalConfig;
            this.tcGroupConfig = null;
            this.internalConfig = null;
        }

        @Override
        public void startElement(String nameSpace, String name, String qname, Attributes attrs) throws SAXException {
            if ("tc-group".equals(qname)) {
                this.handleTCGroup(attrs);
            } else if (this.internalConfig.specVersion != null && this.internalConfig.specVersion.compareTo((Object)new SpecificationVersion("2.0")) == 0) {
                if ("module".equals(qname)) {
                    this.handleModule(attrs);
                } else if ("tc-id".equals(qname)) {
                    this.handleTcId(attrs);
                } else if ("open-close-behavior".equals(qname)) {
                    this.handleOpenCloseBehavior(attrs);
                }
            } else {
                TCGroupParser.this.log("-- TCGroupParser.startElement PARSING OLD");
            }
        }

        @Override
        public void error(SAXParseException ex) throws SAXException {
            throw ex;
        }

        private void handleTCGroup(Attributes attrs) {
            String version = attrs.getValue("version");
            if (version != null) {
                this.internalConfig.specVersion = new SpecificationVersion(version);
            } else {
                PersistenceManager.LOG.log(Level.WARNING, "[WinSys.TCGroupParser.handleTCGroup] Warning: Missing attribute \"version\" of element \"tc-group\".");
                this.internalConfig.specVersion = new SpecificationVersion("2.0");
            }
        }

        private void handleModule(Attributes attrs) {
            String moduleCodeName = attrs.getValue("name");
            this.internalConfig.moduleCodeNameBase = null;
            this.internalConfig.moduleCodeNameRelease = null;
            this.internalConfig.moduleSpecificationVersion = null;
            if (moduleCodeName != null) {
                int i = moduleCodeName.indexOf(47);
                if (i != -1) {
                    this.internalConfig.moduleCodeNameBase = moduleCodeName.substring(0, i);
                    this.internalConfig.moduleCodeNameRelease = moduleCodeName.substring(i + 1);
                    this.checkReleaseCode(this.internalConfig);
                } else {
                    this.internalConfig.moduleCodeNameBase = moduleCodeName;
                }
                this.internalConfig.moduleSpecificationVersion = attrs.getValue("spec");
            }
        }

        private void checkReleaseCode(InternalConfig internalConfig) {
            if ("null".equals(internalConfig.moduleCodeNameRelease)) {
                Logger.getLogger(TCGroupParser.class.getName()).log(Level.WARNING, null, new IllegalStateException("Module release code was saved as null string for module " + internalConfig.moduleCodeNameBase + "! Repairing."));
                internalConfig.moduleCodeNameRelease = null;
            }
        }

        private void handleTcId(Attributes attrs) throws SAXException {
            String tc_id = attrs.getValue("id");
            if (tc_id != null) {
                this.tcGroupConfig.tc_id = tc_id;
                if (!tc_id.equals(TCGroupParser.this.getName())) {
                    PersistenceManager.LOG.log(Level.WARNING, "[WinSys.TCGroupParser.handleTcId] Error: Value of attribute \"id\" of element \"tc-id\" and configuration file name must be the same.");
                    throw new SAXException("Invalid attribute value");
                }
            } else {
                PersistenceManager.LOG.log(Level.WARNING, "[WinSys.TCGroupParser.handleTcId] Error: Missing required attribute \"id\" of element \"tc-id\".");
                throw new SAXException("Missing required attribute");
            }
        }

        private void handleOpenCloseBehavior(Attributes attrs) throws SAXException {
            String open = attrs.getValue("open");
            if (open != null) {
                if ("true".equals(open)) {
                    this.tcGroupConfig.open = true;
                } else if ("false".equals(open)) {
                    this.tcGroupConfig.open = false;
                } else {
                    PersistenceManager.LOG.log(Level.WARNING, "[WinSys.TCGroupParser.handleOpenCloseBehavior] Warning: Invalid value of attribute \"open\" of element \"open-close-behavior\".");
                    this.tcGroupConfig.open = false;
                }
            } else {
                PersistenceManager.LOG.log(Level.WARNING, "[WinSys.TCGroupParser.handleOpenCloseBehavior] Warning: Missing required attribute \"open\" of element \"open-close-behavior\".");
                this.tcGroupConfig.open = false;
            }
            String close = attrs.getValue("close");
            if (close != null) {
                if ("true".equals(close)) {
                    this.tcGroupConfig.close = true;
                } else if ("false".equals(close)) {
                    this.tcGroupConfig.close = false;
                } else {
                    PersistenceManager.LOG.log(Level.WARNING, "[WinSys.TCGroupParser.handleOpenCloseBehavior] Warning: Invalid value of attribute \"close\" of element \"open-close-behavior\".");
                    this.tcGroupConfig.close = false;
                }
            } else {
                PersistenceManager.LOG.log(Level.WARNING, "[WinSys.TCGroupParser.handleOpenCloseBehavior] Warning: Missing required attribute \"close\" of element \"open-close-behavior\".");
                this.tcGroupConfig.close = false;
            }
            String wasOpened = attrs.getValue("was-opened");
            if (wasOpened != null) {
                if ("true".equals(wasOpened)) {
                    this.tcGroupConfig.wasOpened = true;
                } else if ("false".equals(wasOpened)) {
                    this.tcGroupConfig.wasOpened = false;
                } else {
                    PersistenceManager.LOG.log(Level.WARNING, "[WinSys.TCGroupParser.handleOpenCloseBehavior] Warning: Invalid value of attribute \"was-opened\" of element \"open-close-behavior\".");
                    this.tcGroupConfig.wasOpened = false;
                }
            } else {
                this.tcGroupConfig.wasOpened = false;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void writeData(TCGroupConfig tcGroupCfg, InternalConfig ic) throws IOException {
            StringBuffer buff = this.fillBuffer(tcGroupCfg, ic);
            Object object = this.RW_LOCK;
            synchronized (object) {
                FileObject cfgFOOutput = this.getConfigFOOutput();
                FileLock lock = null;
                OutputStream os = null;
                OutputStreamWriter osw = null;
                try {
                    lock = cfgFOOutput.lock();
                    os = cfgFOOutput.getOutputStream(lock);
                    osw = new OutputStreamWriter(os, "UTF-8");
                    osw.write(buff.toString());
                }
                finally {
                    try {
                        if (osw != null) {
                            osw.close();
                        }
                    }
                    catch (IOException exc) {
                        Logger.getLogger(TCGroupParser.class.getName()).log(Level.WARNING, null, exc);
                    }
                    if (lock != null) {
                        lock.releaseLock();
                    }
                }
            }
        }

        private StringBuffer fillBuffer(TCGroupConfig tcGroupCfg, InternalConfig ic) throws IOException {
            StringBuffer buff = new StringBuffer(800);
            Object curValue = null;
            buff.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n").append("<tc-group version=\"2.0\">\n");
            this.appendModule(ic, buff);
            this.appendTcId(tcGroupCfg, buff);
            this.appendOpenCloseBehavior(tcGroupCfg, buff);
            buff.append("</tc-group>\n");
            return buff;
        }

        private void appendModule(InternalConfig ic, StringBuffer buff) {
            if (ic == null) {
                return;
            }
            if (ic.moduleCodeNameBase != null) {
                buff.append(" <module name=\"");
                buff.append(ic.moduleCodeNameBase);
                if (ic.moduleCodeNameRelease != null) {
                    buff.append("/").append(ic.moduleCodeNameRelease);
                }
                if (ic.moduleSpecificationVersion != null) {
                    buff.append("\" spec=\"");
                    buff.append(ic.moduleSpecificationVersion);
                }
                buff.append("\" />\n");
            }
        }

        private void appendTcId(TCGroupConfig tcGroupCfg, StringBuffer buff) {
            buff.append(" <tc-id id=\"").append(PersistenceManager.escapeTcId4XmlContent(tcGroupCfg.tc_id)).append("\"/>\n");
        }

        private void appendOpenCloseBehavior(TCGroupConfig tcGroupCfg, StringBuffer buff) {
            buff.append(" <open-close-behavior open=\"").append(tcGroupCfg.open).append("\" close=\"").append(tcGroupCfg.close).append("\" was-opened=\"").append(tcGroupCfg.wasOpened).append("\"/>\n");
        }
    }

}

