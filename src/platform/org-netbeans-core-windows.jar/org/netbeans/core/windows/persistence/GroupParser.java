/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.SpecificationVersion
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.windows.persistence;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.windows.Debug;
import org.netbeans.core.windows.persistence.GroupConfig;
import org.netbeans.core.windows.persistence.InternalConfig;
import org.netbeans.core.windows.persistence.PersistenceManager;
import org.netbeans.core.windows.persistence.TCGroupConfig;
import org.netbeans.core.windows.persistence.TCGroupParser;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.modules.ModuleInfo;
import org.openide.modules.SpecificationVersion;
import org.openide.util.NbBundle;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

class GroupParser {
    public static final String INSTANCE_DTD_ID_2_0 = "-//NetBeans//DTD Group Properties 2.0//EN";
    private static final boolean DEBUG = Debug.isLoggable(GroupParser.class);
    private FileObject moduleParentFolder;
    private FileObject localParentFolder;
    private InternalConfig internalConfig;
    private Map<String, TCGroupParser> tcGroupParserMap = new HashMap<String, TCGroupParser>(19);
    private String groupName;
    private boolean inModuleFolder;
    private boolean inLocalFolder;

    public GroupParser(String name) {
        this.groupName = name;
    }

    GroupConfig load() throws IOException {
        GroupConfig sc = new GroupConfig();
        this.readProperties(sc);
        this.readTCGroups(sc);
        return sc;
    }

    void save(GroupConfig sc) throws IOException {
        this.writeProperties(sc);
        this.writeTCGroups(sc);
    }

    private void readProperties(GroupConfig sc) throws IOException {
        if (DEBUG) {
            Debug.log(GroupParser.class, "readProperties ENTER group:" + this.getName());
        }
        PropertyHandler propertyHandler = new PropertyHandler();
        InternalConfig internalCfg = this.getInternalConfig();
        internalCfg.clear();
        propertyHandler.readData(sc, internalCfg);
        if (DEBUG) {
            Debug.log(GroupParser.class, "readProperties LEAVE group:" + this.getName());
        }
    }

    private void readTCGroups(GroupConfig sc) throws IOException {
        int i;
        FileObject localGroupFolder;
        TCGroupParser tcGroupParser;
        FileObject moduleGroupFolder;
        FileObject[] files;
        if (DEBUG) {
            Debug.log(GroupParser.class, "readTCGroups ENTER group:" + this.getName());
        }
        Iterator<String> it = this.tcGroupParserMap.keySet().iterator();
        while (it.hasNext()) {
            TCGroupParser tcGroupParser2 = this.tcGroupParserMap.get(it.next());
            tcGroupParser2.setInModuleFolder(false);
            tcGroupParser2.setInLocalFolder(false);
        }
        if (this.isInModuleFolder() && (moduleGroupFolder = this.moduleParentFolder.getFileObject(this.groupName)) != null) {
            files = moduleGroupFolder.getChildren();
            for (i = 0; i < files.length; ++i) {
                if (files[i].isFolder() || !"wstcgrp".equals(files[i].getExt())) continue;
                tcGroupParser = this.tcGroupParserMap.get(files[i].getName());
                if (tcGroupParser == null) {
                    tcGroupParser = new TCGroupParser(files[i].getName());
                    this.tcGroupParserMap.put(files[i].getName(), tcGroupParser);
                }
                tcGroupParser.setInModuleFolder(true);
                tcGroupParser.setModuleParentFolder(moduleGroupFolder);
            }
        }
        if (this.isInLocalFolder() && (localGroupFolder = this.localParentFolder.getFileObject(this.groupName)) != null) {
            files = localGroupFolder.getChildren();
            for (i = 0; i < files.length; ++i) {
                if (files[i].isFolder() || !"wstcgrp".equals(files[i].getExt())) continue;
                if (this.tcGroupParserMap.containsKey(files[i].getName())) {
                    tcGroupParser = this.tcGroupParserMap.get(files[i].getName());
                } else {
                    tcGroupParser = new TCGroupParser(files[i].getName());
                    this.tcGroupParserMap.put(files[i].getName(), tcGroupParser);
                }
                tcGroupParser.setInLocalFolder(true);
                tcGroupParser.setLocalParentFolder(localGroupFolder);
            }
        }
        ArrayList<TCGroupConfig> tcGroupCfgList = new ArrayList<TCGroupConfig>(this.tcGroupParserMap.size());
        ArrayList<TCGroupParser> toRemove = new ArrayList<TCGroupParser>(this.tcGroupParserMap.size());
        Iterator<String> it2 = this.tcGroupParserMap.keySet().iterator();
        while (it2.hasNext()) {
            TCGroupConfig tcGroupCfg;
            tcGroupParser = this.tcGroupParserMap.get(it2.next());
            try {
                tcGroupCfg = tcGroupParser.load();
            }
            catch (IOException exc) {
                toRemove.add(tcGroupParser);
                this.deleteLocalTCGroup(tcGroupParser.getName());
                Logger.getLogger(GroupParser.class.getName()).log(Level.INFO, null, exc);
                continue;
            }
            boolean tcGroupAccepted = this.acceptTCGroup(tcGroupParser, tcGroupCfg);
            if (tcGroupAccepted) {
                tcGroupCfgList.add(tcGroupCfg);
                continue;
            }
            toRemove.add(tcGroupParser);
            this.deleteLocalTCGroup(tcGroupParser.getName());
        }
        for (int i2 = 0; i2 < toRemove.size(); ++i2) {
            tcGroupParser = (TCGroupParser)toRemove.get(i2);
            this.tcGroupParserMap.remove(tcGroupParser.getName());
        }
        sc.tcGroupConfigs = tcGroupCfgList.toArray(new TCGroupConfig[tcGroupCfgList.size()]);
        PersistenceManager pm = PersistenceManager.getDefault();
        for (int i3 = 0; i3 < sc.tcGroupConfigs.length; ++i3) {
            pm.addUsedTCId(sc.tcGroupConfigs[i3].tc_id);
        }
        if (DEBUG) {
            Debug.log(GroupParser.class, "readTCGroups LEAVE group:" + this.getName());
        }
    }

    private boolean acceptTCGroup(TCGroupParser tcGroupParser, TCGroupConfig config) {
        InternalConfig cfg = tcGroupParser.getInternalConfig();
        if (cfg.moduleCodeNameBase != null) {
            ModuleInfo curModuleInfo = PersistenceManager.findModule(cfg.moduleCodeNameBase, cfg.moduleCodeNameRelease, cfg.moduleSpecificationVersion);
            if (curModuleInfo == null) {
                PersistenceManager.LOG.fine("Cannot find module '" + cfg.moduleCodeNameBase + " " + cfg.moduleCodeNameRelease + " " + cfg.moduleSpecificationVersion + "' for tcgrp with name '" + config.tc_id + "'");
            }
            if (curModuleInfo != null && curModuleInfo.isEnabled()) {
                return true;
            }
            return false;
        }
        return true;
    }

    private void writeProperties(GroupConfig sc) throws IOException {
        if (DEBUG) {
            Debug.log(GroupParser.class, "writeProperties ENTER group:" + this.getName());
        }
        PropertyHandler propertyHandler = new PropertyHandler();
        InternalConfig internalCfg = this.getInternalConfig();
        propertyHandler.writeData(sc, internalCfg);
        if (DEBUG) {
            Debug.log(GroupParser.class, "writeProperties LEAVE group:" + this.getName());
        }
    }

    private void writeTCGroups(GroupConfig sc) throws IOException {
        int i;
        TCGroupParser tcGroupParser;
        if (DEBUG) {
            Debug.log(GroupParser.class, "writeTCGroups ENTER group:" + this.getName());
        }
        HashMap<String, TCGroupConfig> tcGroupConfigMap = new HashMap<String, TCGroupConfig>(19);
        for (int i2 = 0; i2 < sc.tcGroupConfigs.length; ++i2) {
            tcGroupConfigMap.put(sc.tcGroupConfigs[i2].tc_id, sc.tcGroupConfigs[i2]);
        }
        ArrayList<String> toDelete = new ArrayList<String>(10);
        for (String s : this.tcGroupParserMap.keySet()) {
            tcGroupParser = this.tcGroupParserMap.get(s);
            if (tcGroupConfigMap.containsKey(tcGroupParser.getName())) continue;
            toDelete.add(tcGroupParser.getName());
        }
        for (i = 0; i < toDelete.size(); ++i) {
            this.tcGroupParserMap.remove(toDelete.get(i));
            this.deleteLocalTCGroup((String)toDelete.get(i));
        }
        for (i = 0; i < sc.tcGroupConfigs.length; ++i) {
            if (this.tcGroupParserMap.containsKey(sc.tcGroupConfigs[i].tc_id)) continue;
            TCGroupParser tcGroupParser2 = new TCGroupParser(sc.tcGroupConfigs[i].tc_id);
            this.tcGroupParserMap.put(sc.tcGroupConfigs[i].tc_id, tcGroupParser2);
        }
        FileObject localFolder = this.localParentFolder.getFileObject(this.getName());
        if (localFolder == null && this.tcGroupParserMap.size() > 0) {
            localFolder = FileUtil.createFolder((FileObject)this.localParentFolder, (String)this.getName());
        }
        Iterator<String> it = this.tcGroupParserMap.keySet().iterator();
        while (it.hasNext()) {
            tcGroupParser = this.tcGroupParserMap.get(it.next());
            tcGroupParser.setLocalParentFolder(localFolder);
            tcGroupParser.setInLocalFolder(true);
            tcGroupParser.save((TCGroupConfig)tcGroupConfigMap.get(tcGroupParser.getName()));
        }
        if (DEBUG) {
            Debug.log(GroupParser.class, "writeTCGroups LEAVE group:" + this.getName());
        }
    }

    private void deleteLocalTCGroup(String tcGroupName) {
        if (DEBUG) {
            Debug.log(GroupParser.class, "deleteLocalTCGroup group:" + tcGroupName);
        }
        if (this.localParentFolder == null) {
            return;
        }
        FileObject localGroupFolder = this.localParentFolder.getFileObject(this.groupName);
        if (localGroupFolder == null) {
            return;
        }
        FileObject tcGroupFO = localGroupFolder.getFileObject(tcGroupName, "wstcgrp");
        if (tcGroupFO != null) {
            PersistenceManager.deleteOneFO(tcGroupFO);
        }
    }

    void removeTCGroup(String tcGroupName) {
        this.tcGroupParserMap.remove(tcGroupName);
        this.deleteLocalTCGroup(tcGroupName);
    }

    TCGroupConfig addTCGroup(String tcGroupName) {
        TCGroupParser tcGroupParser = this.tcGroupParserMap.get(tcGroupName);
        if (tcGroupParser != null) {
            PersistenceManager.LOG.log(Level.INFO, "[WinSys.GroupParser.addTCGroup] Warning: GroupParser " + this.getName() + ". TCGroupParser " + tcGroupName + " exists but it should not.");
            this.tcGroupParserMap.remove(tcGroupName);
        }
        tcGroupParser = new TCGroupParser(tcGroupName);
        FileObject moduleFolder = this.moduleParentFolder.getFileObject(this.groupName);
        tcGroupParser.setModuleParentFolder(moduleFolder);
        tcGroupParser.setInModuleFolder(true);
        this.tcGroupParserMap.put(tcGroupName, tcGroupParser);
        TCGroupConfig tcGroupConfig = null;
        try {
            tcGroupConfig = tcGroupParser.load();
        }
        catch (IOException exc) {
            PersistenceManager.LOG.log(Level.INFO, "[WinSys.GroupParser.addTCGroup] Warning: GroupParser " + this.getName() + ". Cannot load tcGroup " + tcGroupName, exc);
        }
        return tcGroupConfig;
    }

    InternalConfig getInternalConfig() {
        if (this.internalConfig == null) {
            this.internalConfig = new InternalConfig();
        }
        return this.internalConfig;
    }

    void setModuleParentFolder(FileObject moduleParentFolder) {
        this.moduleParentFolder = moduleParentFolder;
    }

    void setLocalParentFolder(FileObject localParentFolder) {
        this.localParentFolder = localParentFolder;
    }

    String getName() {
        return this.groupName;
    }

    boolean isInModuleFolder() {
        return this.inModuleFolder;
    }

    void setInModuleFolder(boolean inModuleFolder) {
        this.inModuleFolder = inModuleFolder;
    }

    boolean isInLocalFolder() {
        return this.inLocalFolder;
    }

    void setInLocalFolder(boolean inLocalFolder) {
        this.inLocalFolder = inLocalFolder;
    }

    private final class PropertyHandler
    extends DefaultHandler {
        private GroupConfig groupConfig;
        private InternalConfig internalConfig;
        private final Object RW_LOCK;

        public PropertyHandler() {
            this.groupConfig = null;
            this.internalConfig = null;
            this.RW_LOCK = new Object();
        }

        private FileObject getConfigFOInput() {
            FileObject groupConfigFO = GroupParser.this.isInLocalFolder() ? GroupParser.this.localParentFolder.getFileObject(GroupParser.this.getName(), "wsgrp") : (GroupParser.this.isInModuleFolder() ? GroupParser.this.moduleParentFolder.getFileObject(GroupParser.this.getName(), "wsgrp") : null);
            return groupConfigFO;
        }

        private FileObject getConfigFOOutput() throws IOException {
            FileObject groupConfigFO = GroupParser.this.localParentFolder.getFileObject(GroupParser.this.getName(), "wsgrp");
            if (groupConfigFO != null) {
                return groupConfigFO;
            }
            StringBuffer buffer = new StringBuffer();
            buffer.append(GroupParser.this.getName());
            buffer.append('.');
            buffer.append("wsgrp");
            groupConfigFO = FileUtil.createData((FileObject)GroupParser.this.localParentFolder, (String)buffer.toString());
            return groupConfigFO;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void readData(GroupConfig groupCfg, InternalConfig internalCfg) throws IOException {
            this.groupConfig = groupCfg;
            this.internalConfig = internalCfg;
            FileObject cfgFOInput = this.getConfigFOInput();
            if (cfgFOInput == null) {
                throw new FileNotFoundException("[WinSys] Missing Group configuration file:" + GroupParser.this.getName());
            }
            InputStream is = null;
            try {
                Object object = this.RW_LOCK;
                synchronized (object) {
                    is = cfgFOInput.getInputStream();
                    PersistenceManager.getDefault().getXMLParser(this).parse(new InputSource(is));
                }
            }
            catch (SAXException exc) {
                String msg = NbBundle.getMessage(GroupParser.class, (String)"EXC_GroupParse", (Object)cfgFOInput);
                throw (IOException)new IOException(msg).initCause(exc);
            }
            finally {
                try {
                    if (is != null) {
                        is.close();
                    }
                }
                catch (IOException exc) {
                    Logger.getLogger(GroupParser.class.getName()).log(Level.INFO, null, exc);
                }
            }
            groupCfg = this.groupConfig;
            internalCfg = this.internalConfig;
            this.groupConfig = null;
            this.internalConfig = null;
        }

        @Override
        public void startElement(String nameSpace, String name, String qname, Attributes attrs) throws SAXException {
            if ("group".equals(qname)) {
                this.handleGroup(attrs);
            } else if (this.internalConfig.specVersion.compareTo((Object)new SpecificationVersion("2.0")) == 0) {
                if ("module".equals(qname)) {
                    this.handleModule(attrs);
                } else if ("name".equals(qname)) {
                    this.handleName(attrs);
                } else if ("state".equals(qname)) {
                    this.handleState(attrs);
                }
            } else if (DEBUG) {
                Debug.log(GroupParser.class, "-- GroupParser.startElement PARSING OLD");
            }
        }

        @Override
        public void error(SAXParseException ex) throws SAXException {
            throw ex;
        }

        private void handleGroup(Attributes attrs) {
            String version = attrs.getValue("version");
            if (version != null) {
                this.internalConfig.specVersion = new SpecificationVersion(version);
            } else {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.GroupParser.handleGroup] Warning: Missing attribute \"version\" of element \"group\".");
                this.internalConfig.specVersion = new SpecificationVersion("2.0");
            }
        }

        private void handleModule(Attributes attrs) {
            String moduleCodeName = attrs.getValue("name");
            this.internalConfig.moduleCodeNameBase = null;
            this.internalConfig.moduleCodeNameRelease = null;
            this.internalConfig.moduleSpecificationVersion = null;
            if (moduleCodeName != null) {
                int i = moduleCodeName.indexOf(47);
                if (i != -1) {
                    this.internalConfig.moduleCodeNameBase = moduleCodeName.substring(0, i);
                    this.internalConfig.moduleCodeNameRelease = moduleCodeName.substring(i + 1);
                    this.checkReleaseCode(this.internalConfig);
                } else {
                    this.internalConfig.moduleCodeNameBase = moduleCodeName;
                }
                this.internalConfig.moduleSpecificationVersion = attrs.getValue("spec");
            }
        }

        private void checkReleaseCode(InternalConfig internalConfig) {
            if ("null".equals(internalConfig.moduleCodeNameRelease)) {
                Logger.getLogger(GroupParser.class.getName()).log(Level.INFO, null, new IllegalStateException("Module release code was saved as null string for module " + internalConfig.moduleCodeNameBase + "! Repairing."));
                internalConfig.moduleCodeNameRelease = null;
            }
        }

        private void handleName(Attributes attrs) throws SAXException {
            String name = attrs.getValue("unique");
            if (name != null) {
                this.groupConfig.name = name;
                if (!name.equals(GroupParser.this.getName())) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.GroupParser.handleName] Error: Value of attribute \"unique\" of element \"name\" and configuration file name must be the same.");
                    throw new SAXException("Invalid attribute value");
                }
            } else {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.GroupParser.handleName] Error: Missing required attribute \"unique\" of element \"name\".");
                throw new SAXException("Missing required attribute");
            }
        }

        private void handleState(Attributes attrs) throws SAXException {
            String opened = attrs.getValue("opened");
            if (opened != null) {
                if ("true".equals(opened)) {
                    this.groupConfig.opened = true;
                } else if ("false".equals(opened)) {
                    this.groupConfig.opened = false;
                } else {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.GroupParser.handleState] Warning: Invalid value of attribute \"opened\" of element \"state\".");
                    this.groupConfig.opened = false;
                }
            } else {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.GroupParser.handleState] Error: Missing required attribute \"opened\" of element \"state\".");
                this.groupConfig.opened = false;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void writeData(GroupConfig sc, InternalConfig ic) throws IOException {
            StringBuffer buff = this.fillBuffer(sc, ic);
            Object object = this.RW_LOCK;
            synchronized (object) {
                FileObject cfgFOOutput = this.getConfigFOOutput();
                FileLock lock = null;
                OutputStream os = null;
                OutputStreamWriter osw = null;
                try {
                    lock = cfgFOOutput.lock();
                    os = cfgFOOutput.getOutputStream(lock);
                    osw = new OutputStreamWriter(os, "UTF-8");
                    osw.write(buff.toString());
                }
                finally {
                    try {
                        if (osw != null) {
                            osw.close();
                        }
                    }
                    catch (IOException exc) {
                        Logger.getLogger(GroupParser.class.getName()).log(Level.INFO, null, exc);
                    }
                    if (lock != null) {
                        lock.releaseLock();
                    }
                }
            }
        }

        private StringBuffer fillBuffer(GroupConfig gc, InternalConfig ic) throws IOException {
            StringBuffer buff = new StringBuffer(800);
            buff.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n");
            buff.append("<group version=\"2.0\">\n");
            this.appendModule(ic, buff);
            this.appendName(gc, buff);
            this.appendState(gc, buff);
            buff.append("</group>\n");
            return buff;
        }

        private void appendModule(InternalConfig ic, StringBuffer buff) {
            if (ic == null) {
                return;
            }
            if (ic.moduleCodeNameBase != null) {
                buff.append("    <module");
                buff.append(" name=\"");
                buff.append(ic.moduleCodeNameBase);
                if (ic.moduleCodeNameRelease != null) {
                    buff.append("/" + ic.moduleCodeNameRelease);
                }
                if (ic.moduleSpecificationVersion != null) {
                    buff.append("\" spec=\"");
                    buff.append(ic.moduleSpecificationVersion);
                }
                buff.append("\" />\n");
            }
        }

        private void appendName(GroupConfig gc, StringBuffer buff) {
            buff.append("    <name");
            buff.append(" unique=\"");
            buff.append(gc.name);
            buff.append("\"");
            buff.append(" />\n");
        }

        private void appendState(GroupConfig gc, StringBuffer buff) {
            buff.append("    <state");
            buff.append(" opened=\"");
            if (gc.opened) {
                buff.append("true");
            } else {
                buff.append("false");
            }
            buff.append("\"");
            buff.append(" />\n");
        }

        @Override
        public InputSource resolveEntity(String publicId, String systemId) throws SAXException {
            if ("-//NetBeans//DTD Group Properties 2.0//EN".equals(publicId)) {
                ByteArrayInputStream is = new ByteArrayInputStream(new byte[0]);
                return new InputSource(is);
            }
            return null;
        }
    }

}

