/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.persistence;

import java.awt.Dimension;
import java.awt.Rectangle;
import org.netbeans.core.windows.SplitConstraint;
import org.netbeans.core.windows.persistence.GroupConfig;
import org.netbeans.core.windows.persistence.ModeConfig;

public class WindowManagerConfig {
    public boolean centeredHorizontallySeparated;
    public boolean centeredVerticallySeparated;
    public int xSeparated;
    public int ySeparated;
    public int widthSeparated;
    public int heightSeparated;
    public float relativeXSeparated;
    public float relativeYSeparated;
    public float relativeWidthSeparated;
    public float relativeHeightSeparated;
    public boolean centeredHorizontallyJoined;
    public boolean centeredVerticallyJoined;
    public int xJoined;
    public int yJoined;
    public int widthJoined;
    public int heightJoined;
    public float relativeXJoined;
    public float relativeYJoined;
    public float relativeWidthJoined;
    public float relativeHeightJoined;
    public int maximizeIfWidthBelowJoined;
    public int maximizeIfHeightBelowJoined;
    public int mainWindowFrameStateJoined;
    public int mainWindowFrameStateSeparated;
    public int editorAreaState;
    public SplitConstraint[] editorAreaConstraints = new SplitConstraint[0];
    public Rectangle editorAreaBounds;
    public Rectangle editorAreaRelativeBounds;
    public int editorAreaFrameState;
    public Dimension screenSize;
    public String activeModeName = "";
    public String editorMaximizedModeName = "";
    public String viewMaximizedModeName = "";
    public String toolbarConfiguration = "";
    public int preferredToolbarIconSize = 24;
    public ModeConfig[] modes = new ModeConfig[0];
    public GroupConfig[] groups = new GroupConfig[0];
    public String[] tcIdViewList = new String[0];

    public boolean equals(Object obj) {
        int j;
        int i;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WindowManagerConfig)) {
            return false;
        }
        WindowManagerConfig wmCfg = (WindowManagerConfig)obj;
        if (this.centeredHorizontallySeparated != wmCfg.centeredHorizontallySeparated || this.centeredVerticallySeparated != wmCfg.centeredVerticallySeparated) {
            return false;
        }
        if (this.xSeparated != wmCfg.xSeparated || this.ySeparated != wmCfg.ySeparated || this.widthSeparated != wmCfg.widthSeparated || this.heightSeparated != wmCfg.heightSeparated) {
            return false;
        }
        if (this.relativeXSeparated != wmCfg.relativeXSeparated || this.relativeYSeparated != wmCfg.relativeYSeparated) {
            return false;
        }
        if (this.relativeWidthSeparated != wmCfg.relativeWidthSeparated || this.relativeHeightSeparated != wmCfg.relativeHeightSeparated) {
            return false;
        }
        if (this.centeredHorizontallyJoined != wmCfg.centeredHorizontallyJoined || this.centeredVerticallyJoined != wmCfg.centeredVerticallyJoined) {
            return false;
        }
        if (this.xJoined != wmCfg.xJoined || this.yJoined != wmCfg.yJoined || this.widthJoined != wmCfg.widthJoined || this.heightJoined != wmCfg.heightJoined) {
            return false;
        }
        if (this.relativeXJoined != wmCfg.relativeXJoined || this.relativeYJoined != wmCfg.relativeYJoined) {
            return false;
        }
        if (this.relativeWidthJoined != wmCfg.relativeWidthJoined || this.relativeHeightJoined != wmCfg.relativeHeightJoined) {
            return false;
        }
        if (this.maximizeIfWidthBelowJoined != wmCfg.maximizeIfWidthBelowJoined || this.maximizeIfHeightBelowJoined != wmCfg.maximizeIfHeightBelowJoined) {
            return false;
        }
        if (this.mainWindowFrameStateJoined != wmCfg.mainWindowFrameStateJoined) {
            return false;
        }
        if (this.mainWindowFrameStateSeparated != wmCfg.mainWindowFrameStateSeparated) {
            return false;
        }
        if (this.editorAreaState != wmCfg.editorAreaState) {
            return false;
        }
        if (this.editorAreaConstraints.length != wmCfg.editorAreaConstraints.length) {
            return false;
        }
        for (i = 0; i < this.editorAreaConstraints.length; ++i) {
            if (this.editorAreaConstraints[i].equals(wmCfg.editorAreaConstraints[i])) continue;
            return false;
        }
        if (this.editorAreaBounds != null && wmCfg.editorAreaBounds != null ? !this.editorAreaBounds.equals(wmCfg.editorAreaBounds) : this.editorAreaBounds != null || wmCfg.editorAreaBounds != null) {
            return false;
        }
        if (this.editorAreaRelativeBounds != null && wmCfg.editorAreaRelativeBounds != null ? !this.editorAreaRelativeBounds.equals(wmCfg.editorAreaRelativeBounds) : this.editorAreaRelativeBounds != null || wmCfg.editorAreaRelativeBounds != null) {
            return false;
        }
        if (this.editorAreaFrameState != wmCfg.editorAreaFrameState) {
            return false;
        }
        if (this.screenSize != null && wmCfg.screenSize != null ? !this.screenSize.equals(wmCfg.screenSize) : this.screenSize != null || wmCfg.screenSize != null) {
            return false;
        }
        if (!this.activeModeName.equals(wmCfg.activeModeName)) {
            return false;
        }
        if (!this.editorMaximizedModeName.equals(wmCfg.editorMaximizedModeName)) {
            return false;
        }
        if (!this.viewMaximizedModeName.equals(wmCfg.viewMaximizedModeName)) {
            return false;
        }
        if (!this.toolbarConfiguration.equals(wmCfg.toolbarConfiguration)) {
            return false;
        }
        if (this.preferredToolbarIconSize != wmCfg.preferredToolbarIconSize) {
            return false;
        }
        if (this.modes.length != wmCfg.modes.length) {
            return false;
        }
        for (i = 0; i < this.modes.length; ++i) {
            ModeConfig modeCfg = null;
            for (j = 0; j < wmCfg.modes.length; ++j) {
                if (!this.modes[i].name.equals(wmCfg.modes[j].name)) continue;
                modeCfg = wmCfg.modes[j];
                break;
            }
            if (modeCfg == null) {
                return false;
            }
            if (this.modes[i].equals(modeCfg)) continue;
            return false;
        }
        if (this.groups.length != wmCfg.groups.length) {
            return false;
        }
        for (i = 0; i < this.groups.length; ++i) {
            GroupConfig groupCfg = null;
            for (j = 0; j < wmCfg.groups.length; ++j) {
                if (!this.groups[i].name.equals(wmCfg.groups[j].name)) continue;
                groupCfg = wmCfg.groups[j];
                break;
            }
            if (groupCfg == null) {
                return false;
            }
            if (this.groups[i].equals(groupCfg)) continue;
            return false;
        }
        if (this.tcIdViewList.length != wmCfg.tcIdViewList.length) {
            return false;
        }
        for (i = 0; i < this.tcIdViewList.length; ++i) {
            if (this.tcIdViewList[i].equals(wmCfg.tcIdViewList[i])) continue;
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int hash = 17;
        hash = 37 * hash + (this.centeredHorizontallySeparated ? 0 : 1);
        hash = 37 * hash + (this.centeredVerticallySeparated ? 0 : 1);
        hash = 37 * hash + this.xSeparated;
        hash = 37 * hash + this.ySeparated;
        hash = 37 * hash + this.widthSeparated;
        hash = 37 * hash + this.heightSeparated;
        hash = 37 * hash + Float.floatToIntBits(this.relativeXSeparated);
        hash = 37 * hash + Float.floatToIntBits(this.relativeYSeparated);
        hash = 37 * hash + Float.floatToIntBits(this.relativeWidthSeparated);
        hash = 37 * hash + Float.floatToIntBits(this.relativeHeightSeparated);
        hash = 37 * hash + (this.centeredHorizontallyJoined ? 0 : 1);
        hash = 37 * hash + (this.centeredVerticallyJoined ? 0 : 1);
        hash = 37 * hash + this.xJoined;
        hash = 37 * hash + this.yJoined;
        hash = 37 * hash + this.widthJoined;
        hash = 37 * hash + this.heightJoined;
        hash = 37 * hash + Float.floatToIntBits(this.relativeXJoined);
        hash = 37 * hash + Float.floatToIntBits(this.relativeYJoined);
        hash = 37 * hash + Float.floatToIntBits(this.relativeWidthJoined);
        hash = 37 * hash + Float.floatToIntBits(this.relativeHeightJoined);
        hash = 37 * hash + this.maximizeIfWidthBelowJoined;
        hash = 37 * hash + this.maximizeIfHeightBelowJoined;
        hash = 37 * hash + this.mainWindowFrameStateJoined;
        hash = 37 * hash + this.mainWindowFrameStateSeparated;
        hash = 37 * hash + this.editorAreaState;
        for (i = 0; i < this.editorAreaConstraints.length; ++i) {
            hash = 37 * hash + this.editorAreaConstraints[i].hashCode();
        }
        if (this.editorAreaBounds != null) {
            hash = 37 * hash + this.editorAreaBounds.hashCode();
        }
        if (this.editorAreaRelativeBounds != null) {
            hash = 37 * hash + this.editorAreaRelativeBounds.hashCode();
        }
        hash = 37 * hash + this.editorAreaFrameState;
        if (this.screenSize != null) {
            hash = 37 * hash + this.screenSize.hashCode();
        }
        hash = 37 * hash + this.activeModeName.hashCode();
        hash = 37 * hash + this.editorMaximizedModeName.hashCode();
        hash = 37 * hash + this.viewMaximizedModeName.hashCode();
        hash = 37 * hash + this.toolbarConfiguration.hashCode();
        hash = 37 * hash + this.preferredToolbarIconSize;
        for (i = 0; i < this.modes.length; ++i) {
            hash = 37 * hash + this.modes[i].hashCode();
        }
        for (i = 0; i < this.groups.length; ++i) {
            hash = 37 * hash + this.groups[i].hashCode();
        }
        for (i = 0; i < this.tcIdViewList.length; ++i) {
            hash = 37 * hash + this.tcIdViewList[i].hashCode();
        }
        return hash;
    }
}

