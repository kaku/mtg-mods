/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.SpecificationVersion
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.windows.persistence;

import java.awt.Rectangle;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.windows.Debug;
import org.netbeans.core.windows.SplitConstraint;
import org.netbeans.core.windows.persistence.InternalConfig;
import org.netbeans.core.windows.persistence.ModeConfig;
import org.netbeans.core.windows.persistence.PersistenceManager;
import org.netbeans.core.windows.persistence.TCRefConfig;
import org.netbeans.core.windows.persistence.TCRefParser;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.modules.ModuleInfo;
import org.openide.modules.SpecificationVersion;
import org.openide.util.NbBundle;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

class ModeParser {
    public static final String INSTANCE_DTD_ID_1_0 = "-//NetBeans//DTD Mode Properties 1.0//EN";
    public static final String INSTANCE_DTD_ID_1_1 = "-//NetBeans//DTD Mode Properties 1.1//EN";
    public static final String INSTANCE_DTD_ID_1_2 = "-//NetBeans//DTD Mode Properties 1.2//EN";
    public static final String INSTANCE_DTD_ID_2_0 = "-//NetBeans//DTD Mode Properties 2.0//EN";
    public static final String INSTANCE_DTD_ID_2_1 = "-//NetBeans//DTD Mode Properties 2.1//EN";
    public static final String INSTANCE_DTD_ID_2_2 = "-//NetBeans//DTD Mode Properties 2.2//EN";
    public static final String INSTANCE_DTD_ID_2_3 = "-//NetBeans//DTD Mode Properties 2.3//EN";
    public static final String INSTANCE_DTD_ID_2_4 = "-//NetBeans//DTD Mode Properties 2.4//EN";
    private static final String EA_ORDER = "WinSys-TCRef-Order";
    private static final boolean DEBUG = Debug.isLoggable(ModeParser.class);
    private FileObject moduleParentFolder;
    private FileObject localParentFolder;
    private InternalConfig internalConfig;
    private Map<String, TCRefParser> tcRefParserMap = new HashMap<String, TCRefParser>(19);
    private Map<String, Integer> tcRefOrder;
    private String modeName;
    private boolean inModuleFolder;
    private boolean inLocalFolder;
    private Set maskSet;

    public ModeParser(String name, Set maskSet) {
        this.modeName = name;
        this.maskSet = maskSet;
    }

    ModeConfig load() throws IOException {
        ModeConfig mc = new ModeConfig();
        this.readProperties(mc);
        if (mc.kind == 2 && mc.side != null && !mc.permanent) {
            mc.permanent = true;
        }
        this.readTCRefs(mc);
        return mc;
    }

    void save(ModeConfig mc) throws IOException {
        this.writeProperties(mc);
        this.writeTCRefs(mc);
    }

    private void readProperties(ModeConfig mc) throws IOException {
        if (DEBUG) {
            Debug.log(ModeParser.class, "readProperties ENTER mo:" + this.getName());
        }
        PropertyHandler propertyHandler = new PropertyHandler();
        InternalConfig internalCfg = this.getInternalConfig();
        internalCfg.clear();
        propertyHandler.readData(mc, internalCfg);
        if (DEBUG) {
            Debug.log(ModeParser.class, "readProperties LEAVE mo:" + this.getName());
        }
    }

    private void readTCRefs(ModeConfig mc) throws IOException {
        FileObject moduleModeFolder;
        int i;
        FileObject localModeFolder;
        int i2;
        TCRefParser tcRefParser;
        TCRefParser tcRefParser2;
        FileObject[] files;
        if (DEBUG) {
            Debug.log(ModeParser.class, "readTCRefs ENTER mo:" + this.getName());
        }
        Iterator<String> it = this.tcRefParserMap.keySet().iterator();
        while (it.hasNext()) {
            TCRefParser tcRefParser3 = this.tcRefParserMap.get(it.next());
            tcRefParser3.setInModuleFolder(false);
            tcRefParser3.setInLocalFolder(false);
        }
        if (this.isInModuleFolder() && (moduleModeFolder = this.moduleParentFolder.getFileObject(this.modeName)) != null) {
            files = moduleModeFolder.getChildren();
            for (i = 0; i < files.length; ++i) {
                if (files[i].isFolder() || !"wstcref".equals(files[i].getExt())) continue;
                if (this.tcRefParserMap.containsKey(files[i].getName())) {
                    tcRefParser2 = this.tcRefParserMap.get(files[i].getName());
                } else {
                    tcRefParser2 = new TCRefParser(files[i].getName());
                    this.tcRefParserMap.put(files[i].getName(), tcRefParser2);
                }
                tcRefParser2.setInModuleFolder(true);
                tcRefParser2.setModuleParentFolder(moduleModeFolder);
            }
        }
        if (this.isInLocalFolder() && (localModeFolder = this.localParentFolder.getFileObject(this.modeName)) != null) {
            files = localModeFolder.getChildren();
            for (i = 0; i < files.length; ++i) {
                if (files[i].isFolder() || !"wstcref".equals(files[i].getExt())) continue;
                tcRefParser2 = this.tcRefParserMap.get(files[i].getName());
                if (tcRefParser2 == null) {
                    tcRefParser2 = new TCRefParser(files[i].getName());
                    this.tcRefParserMap.put(files[i].getName(), tcRefParser2);
                }
                tcRefParser2.setInLocalFolder(true);
                tcRefParser2.setLocalParentFolder(localModeFolder);
            }
        }
        this.readOrder();
        List localList = new ArrayList<TCRefParser>(10);
        HashMap<String, TCRefParser> localMap = new HashMap<String, TCRefParser>(this.tcRefParserMap);
        if (this.tcRefOrder != null) {
            TCRefParser[] tcRefParserArray = new TCRefParser[this.tcRefOrder.size()];
            for (Map.Entry<String, Integer> en : this.tcRefOrder.entrySet()) {
                TCRefParser tcRefParser4;
                String tcRefName = en.getKey();
                int index = en.getValue();
                tcRefParserArray[index] = tcRefParser4 = localMap.remove(tcRefName);
            }
            for (int i3 = 0; i3 < tcRefParserArray.length; ++i3) {
                if (tcRefParserArray[i3] == null) continue;
                localList.add(tcRefParserArray[i3]);
            }
            for (String s : localMap.keySet()) {
                tcRefParser = localMap.get(s);
                localList.add(tcRefParser);
            }
        } else {
            for (String s : localMap.keySet()) {
                TCRefParser tcRefParser5 = localMap.get(s);
                localList.add(tcRefParser5);
            }
            localList = this.carefullySort(localList);
            if (this.tcRefOrder == null) {
                this.tcRefOrder = new HashMap<String, Integer>(19);
            }
            this.tcRefOrder.clear();
            for (int i4 = 0; i4 < localList.size(); ++i4) {
                tcRefParser2 = (TCRefParser)localList.get(i4);
                this.tcRefOrder.put(tcRefParser2.getName(), i4);
            }
            this.writeOrder();
        }
        ArrayList<TCRefConfig> tcRefCfgList = new ArrayList<TCRefConfig>(localList.size());
        ArrayList<TCRefParser> toRemove = new ArrayList<TCRefParser>(localList.size());
        for (i2 = 0; i2 < localList.size(); ++i2) {
            TCRefConfig tcRefCfg;
            tcRefParser = (TCRefParser)localList.get(i2);
            if (this.maskSet.contains(tcRefParser.getName()) && tcRefParser.isInModuleFolder() && !tcRefParser.isInLocalFolder()) {
                toRemove.add(tcRefParser);
                continue;
            }
            try {
                tcRefCfg = tcRefParser.load();
            }
            catch (IOException exc) {
                Logger.getLogger(ModeParser.class.getName()).log(Level.INFO, null, exc);
                continue;
            }
            boolean tcRefAccepted = this.acceptTCRef(tcRefParser, tcRefCfg);
            if (tcRefAccepted) {
                tcRefCfgList.add(tcRefCfg);
                continue;
            }
            toRemove.add(tcRefParser);
            this.deleteLocalTCRef(tcRefParser.getName());
        }
        for (i2 = 0; i2 < toRemove.size(); ++i2) {
            tcRefParser = (TCRefParser)toRemove.get(i2);
            localList.remove(tcRefParser);
            this.tcRefParserMap.remove(tcRefParser.getName());
        }
        if (toRemove.size() > 0) {
            if (this.tcRefOrder == null) {
                this.tcRefOrder = new HashMap<String, Integer>(19);
            }
            this.tcRefOrder.clear();
            for (i2 = 0; i2 < localList.size(); ++i2) {
                tcRefParser = (TCRefParser)localList.get(i2);
                this.tcRefOrder.put(tcRefParser.getName(), i2);
            }
            this.writeOrder();
        }
        mc.tcRefConfigs = tcRefCfgList.toArray(new TCRefConfig[tcRefCfgList.size()]);
        PersistenceManager pm = PersistenceManager.getDefault();
        for (int i5 = 0; i5 < mc.tcRefConfigs.length; ++i5) {
            pm.addUsedTCId(mc.tcRefConfigs[i5].tc_id);
        }
        if (DEBUG) {
            Debug.log(ModeParser.class, "readTCRefs LEAVE mo:" + this.getName());
        }
    }

    private boolean acceptTCRef(TCRefParser tcRefParser, TCRefConfig config) {
        InternalConfig cfg = tcRefParser.getInternalConfig();
        if (cfg.moduleCodeNameBase != null) {
            ModuleInfo curModuleInfo = PersistenceManager.findModule(cfg.moduleCodeNameBase, cfg.moduleCodeNameRelease, cfg.moduleSpecificationVersion);
            if (curModuleInfo == null) {
                PersistenceManager.LOG.fine("Cannot find module '" + cfg.moduleCodeNameBase + " " + cfg.moduleCodeNameRelease + " " + cfg.moduleSpecificationVersion + "' for tcref with id '" + config.tc_id + "'");
            }
            return curModuleInfo != null && curModuleInfo.isEnabled();
        }
        return true;
    }

    private void writeProperties(ModeConfig mc) throws IOException {
        if (DEBUG) {
            Debug.log(ModeParser.class, "writeProperties ENTER mo:" + this.getName());
        }
        PropertyHandler propertyHandler = new PropertyHandler();
        InternalConfig internalCfg = this.getInternalConfig();
        propertyHandler.writeData(mc, internalCfg);
        if (DEBUG) {
            Debug.log(ModeParser.class, "writeProperties LEAVE mo:" + this.getName());
        }
    }

    private void writeTCRefs(ModeConfig mc) throws IOException {
        int i;
        if (DEBUG) {
            Debug.log(ModeParser.class, "writeTCRefs ENTER mo:" + this.getName());
        }
        if (mc.tcRefConfigs.length > 0) {
            if (this.tcRefOrder == null) {
                this.tcRefOrder = new HashMap<String, Integer>(19);
            }
            this.tcRefOrder.clear();
            for (int i2 = 0; i2 < mc.tcRefConfigs.length; ++i2) {
                this.tcRefOrder.put(mc.tcRefConfigs[i2].tc_id, i2);
            }
        } else {
            this.tcRefOrder = null;
        }
        this.writeOrder();
        HashMap<String, TCRefConfig> tcRefConfigMap = new HashMap<String, TCRefConfig>(19);
        for (int i3 = 0; i3 < mc.tcRefConfigs.length; ++i3) {
            tcRefConfigMap.put(mc.tcRefConfigs[i3].tc_id, mc.tcRefConfigs[i3]);
        }
        ArrayList<String> toDelete = new ArrayList<String>(10);
        for (String s : this.tcRefParserMap.keySet()) {
            TCRefParser tcRefParser = this.tcRefParserMap.get(s);
            if (tcRefConfigMap.containsKey(tcRefParser.getName())) continue;
            toDelete.add(tcRefParser.getName());
        }
        for (i = 0; i < toDelete.size(); ++i) {
            this.tcRefParserMap.remove(toDelete.get(i));
            this.deleteLocalTCRef((String)toDelete.get(i));
        }
        for (i = 0; i < mc.tcRefConfigs.length; ++i) {
            if (this.tcRefParserMap.containsKey(mc.tcRefConfigs[i].tc_id)) continue;
            TCRefParser tcRefParser = new TCRefParser(mc.tcRefConfigs[i].tc_id);
            this.tcRefParserMap.put(mc.tcRefConfigs[i].tc_id, tcRefParser);
        }
        FileObject localFolder = this.localParentFolder.getFileObject(this.getName());
        if (localFolder == null && this.tcRefParserMap.size() > 0) {
            localFolder = FileUtil.createFolder((FileObject)this.localParentFolder, (String)this.getName());
        }
        Iterator<String> it = this.tcRefParserMap.keySet().iterator();
        while (it.hasNext()) {
            TCRefParser tcRefParser = this.tcRefParserMap.get(it.next());
            tcRefParser.setLocalParentFolder(localFolder);
            tcRefParser.setInLocalFolder(true);
            tcRefParser.save((TCRefConfig)tcRefConfigMap.get(tcRefParser.getName()));
        }
        if (DEBUG) {
            Debug.log(ModeParser.class, "writeTCRefs LEAVE mo:" + this.getName());
        }
    }

    private void deleteLocalTCRef(String tcRefName) {
        if (DEBUG) {
            Debug.log(ModeParser.class, "deleteLocalTCRef tcRefName:" + tcRefName);
        }
        if (this.localParentFolder == null) {
            return;
        }
        FileObject localModeFolder = this.localParentFolder.getFileObject(this.modeName);
        if (localModeFolder == null) {
            return;
        }
        FileObject tcRefFO = localModeFolder.getFileObject(tcRefName, "wstcref");
        if (tcRefFO != null) {
            PersistenceManager.deleteOneFO(tcRefFO);
        }
    }

    private void readOrder() {
        FileObject localModeFolder;
        if (this.localParentFolder == null) {
            try {
                this.localParentFolder = PersistenceManager.getDefault().getModesLocalFolder();
            }
            catch (IOException ex) {
                Logger.getLogger(ModeParser.class.getName()).log(Level.INFO, "Cannot get access to lcoal modes folder", ex);
                return;
            }
        }
        if ((localModeFolder = this.localParentFolder.getFileObject(this.modeName)) == null) {
            this.tcRefOrder = null;
            return;
        }
        Object o = localModeFolder.getAttribute("WinSys-TCRef-Order");
        if (o == null) {
            this.tcRefOrder = null;
            return;
        }
        if (o instanceof String) {
            String sepNames = (String)o;
            HashMap<String, Integer> map = new HashMap<String, Integer>(19);
            StringTokenizer tok = new StringTokenizer(sepNames, "/");
            int i = 0;
            while (tok.hasMoreTokens()) {
                String tcRefName = tok.nextToken();
                map.put(tcRefName, i);
                ++i;
            }
            this.tcRefOrder = map;
            return;
        }
        this.tcRefOrder = null;
    }

    private void writeOrder() throws IOException {
        FileObject localModeFolder;
        if (this.localParentFolder == null) {
            this.localParentFolder = PersistenceManager.getDefault().getModesLocalFolder();
        }
        if ((localModeFolder = this.localParentFolder.getFileObject(this.modeName)) == null) {
            localModeFolder = FileUtil.createFolder((FileObject)this.localParentFolder, (String)this.modeName);
        }
        if (this.tcRefOrder == null) {
            localModeFolder.setAttribute("WinSys-TCRef-Order", (Object)null);
        } else {
            String[] tcRefNames = new String[this.tcRefOrder.size()];
            for (Map.Entry<String, Integer> en : this.tcRefOrder.entrySet()) {
                String tcRefName = en.getKey();
                int index = en.getValue();
                tcRefNames[index] = tcRefName;
            }
            StringBuilder buf = new StringBuilder(255);
            for (int i = 0; i < tcRefNames.length; ++i) {
                if (i > 0) {
                    buf.append('/');
                }
                buf.append(tcRefNames[i]);
            }
            localModeFolder.setAttribute("WinSys-TCRef-Order", (Object)buf.toString());
        }
    }

    private List<TCRefParser> carefullySort(List<TCRefParser> l) {
        if (this.tcRefOrder != null && !this.tcRefOrder.isEmpty()) {
            return l;
        }
        if (this.moduleParentFolder == null) {
            return l;
        }
        FileObject moduleModeFolder = this.moduleParentFolder.getFileObject(this.modeName);
        if (moduleModeFolder == null) {
            return l;
        }
        LinkedHashMap<FileObject, TCRefParser> m = new LinkedHashMap<FileObject, TCRefParser>();
        for (TCRefParser p : l) {
            FileObject f = moduleModeFolder.getFileObject(p.getName() + '.' + "wstcref");
            if (f == null) {
                return l;
            }
            m.put(f, p);
        }
        List files = FileUtil.getOrder(m.keySet(), (boolean)true);
        ArrayList<TCRefParser> tcs = new ArrayList<TCRefParser>(m.size());
        for (FileObject f : files) {
            tcs.add((TCRefParser)m.get((Object)f));
        }
        return tcs;
    }

    void removeTCRef(String tcRefName) {
        int i;
        TCRefParser tcRefParser;
        if (DEBUG) {
            Debug.log(ModeParser.class, "removeTCRef ENTER tcRef:" + tcRefName);
        }
        ArrayList<TCRefParser> localList = new ArrayList<TCRefParser>(10);
        Map localMap = (Map)((HashMap)this.tcRefParserMap).clone();
        this.tcRefParserMap.remove(tcRefName);
        TCRefParser[] tcRefParserArray = new TCRefParser[this.tcRefOrder.size()];
        for (Map.Entry<String, Integer> en : this.tcRefOrder.entrySet()) {
            TCRefParser tcRefParser2;
            String name = en.getKey();
            int index = en.getValue();
            tcRefParserArray[index] = tcRefParser2 = (TCRefParser)localMap.remove(name);
        }
        for (int i2 = 0; i2 < tcRefParserArray.length; ++i2) {
            localList.add(tcRefParserArray[i2]);
        }
        Iterator it = localMap.keySet().iterator();
        while (it.hasNext()) {
            tcRefParser = (TCRefParser)localMap.get(it.next());
            localList.add(tcRefParser);
        }
        for (i = 0; i < localList.size(); ++i) {
            tcRefParser = (TCRefParser)localList.get(i);
            if (!tcRefName.equals(tcRefParser.getName())) continue;
            localList.remove(i);
            break;
        }
        if (null == this.tcRefOrder) {
            this.tcRefOrder = new HashMap<String, Integer>(19);
        }
        this.tcRefOrder.clear();
        for (i = 0; i < localList.size(); ++i) {
            tcRefParser = (TCRefParser)localList.get(i);
            this.tcRefOrder.put(tcRefParser.getName(), i);
        }
        try {
            this.writeOrder();
        }
        catch (IOException exc) {
            PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.removeTCRef] Warning: Cannot write order of mode: " + this.getName(), exc);
        }
        this.deleteLocalTCRef(tcRefName);
        if (DEBUG) {
            Debug.log(ModeParser.class, "removeTCRef LEAVE tcRef:" + tcRefName);
        }
    }

    TCRefConfig addTCRef(String tcRefName, List<String> tcRefNameList) {
        TCRefParser tcRefParser;
        if (DEBUG) {
            Debug.log(ModeParser.class, "addTCRef ENTER mo:" + this.getName() + " tcRef:" + tcRefName);
        }
        if ((tcRefParser = this.tcRefParserMap.get(tcRefName)) != null) {
            PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.addTCRef] Warning: ModeParser " + this.getName() + ". TCRefParser " + tcRefName + " exists but it should not.");
            this.tcRefParserMap.remove(tcRefName);
        }
        tcRefParser = new TCRefParser(tcRefName);
        FileObject moduleFolder = this.moduleParentFolder.getFileObject(this.modeName);
        tcRefParser.setModuleParentFolder(moduleFolder);
        tcRefParser.setInModuleFolder(true);
        this.tcRefParserMap.put(tcRefName, tcRefParser);
        TCRefConfig tcRefConfig = null;
        try {
            tcRefConfig = tcRefParser.load();
        }
        catch (IOException exc) {
            PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.addTCRef] Warning: ModeParser " + this.getName() + ". Cannot load tcRef " + tcRefName, exc);
        }
        List localList = new ArrayList<TCRefParser>(10);
        Map localMap = (Map)((HashMap)this.tcRefParserMap).clone();
        if (null == this.tcRefOrder) {
            PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.addTCRef] Warning: ModeParser " + this.getName() + ". TCRefParser " + tcRefName + " is missing TC order.");
            this.tcRefParserMap.remove(tcRefName);
            this.readOrder();
        }
        TCRefParser[] tcRefParserArray = new TCRefParser[this.tcRefOrder.size()];
        for (Map.Entry<String, Integer> en : this.tcRefOrder.entrySet()) {
            String name = en.getKey();
            int index = en.getValue();
            tcRefParserArray[index] = tcRefParser = (TCRefParser)localMap.remove(name);
        }
        for (int i = 0; i < tcRefParserArray.length; ++i) {
            if (null == tcRefParserArray[i]) continue;
            localList.add((TCRefParser)tcRefParserArray[i]);
        }
        for (String key : localMap.keySet()) {
            tcRefParser = (TCRefParser)localMap.get(key);
            assert (tcRefParser != null);
            localList.add(tcRefParser);
        }
        localList = this.carefullySort(localList);
        if (null == this.tcRefOrder) {
            this.tcRefOrder = new HashMap<String, Integer>(19);
        }
        this.tcRefOrder.clear();
        for (int i2 = 0; i2 < localList.size(); ++i2) {
            tcRefParser = (TCRefParser)localList.get(i2);
            this.tcRefOrder.put(tcRefParser.getName(), i2);
        }
        try {
            this.writeOrder();
        }
        catch (IOException exc) {
            PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.addTCRef] Warning: Cannot write order of mode: " + this.getName(), exc);
        }
        tcRefNameList.clear();
        for (int i3 = 0; i3 < localList.size(); ++i3) {
            tcRefParser = (TCRefParser)localList.get(i3);
            tcRefNameList.add(tcRefParser.getName());
        }
        if (DEBUG) {
            Debug.log(ModeParser.class, "addTCRef LEAVE mo:" + this.getName() + " tcRef:" + tcRefName);
        }
        return tcRefConfig;
    }

    void addTCRefImport(String tcRefName, InternalConfig internalCfg) {
        TCRefParser tcRefParser;
        if (DEBUG) {
            Debug.log(ModeParser.class, "addTCRefImport ENTER mo:" + this.getName() + " tcRef:" + tcRefName);
        }
        if ((tcRefParser = this.tcRefParserMap.get(tcRefName)) != null) {
            PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.addTCRef] Warning: ModeParser " + this.getName() + ". TCRefParser " + tcRefName + " exists but it should not.");
            this.tcRefParserMap.remove(tcRefName);
        }
        tcRefParser = new TCRefParser(tcRefName);
        FileObject localFolder = this.localParentFolder.getFileObject(this.modeName);
        tcRefParser.setLocalParentFolder(localFolder);
        tcRefParser.setInternalConfig(internalCfg);
        this.tcRefParserMap.put(tcRefName, tcRefParser);
        if (DEBUG) {
            Debug.log(ModeParser.class, "addTCRefImport LEAVE mo:" + this.getName() + " tcRef:" + tcRefName);
        }
    }

    TCRefParser findTCRefParser(String tcRefName) {
        return this.tcRefParserMap.get(tcRefName);
    }

    InternalConfig getInternalConfig() {
        if (this.internalConfig == null) {
            this.internalConfig = new InternalConfig();
        }
        return this.internalConfig;
    }

    void setModuleParentFolder(FileObject moduleParentFolder) {
        this.moduleParentFolder = moduleParentFolder;
    }

    void setLocalParentFolder(FileObject localParentFolder) {
        this.localParentFolder = localParentFolder;
    }

    String getName() {
        return this.modeName;
    }

    boolean isInModuleFolder() {
        return this.inModuleFolder;
    }

    void setInModuleFolder(boolean inModuleFolder) {
        this.inModuleFolder = inModuleFolder;
    }

    boolean isInLocalFolder() {
        return this.inLocalFolder;
    }

    void setInLocalFolder(boolean inLocalFolder) {
        this.inLocalFolder = inLocalFolder;
    }

    private final class PropertyHandler
    extends DefaultHandler {
        private ModeConfig modeConfig;
        private InternalConfig internalConfig;
        private List<SplitConstraint> itemList;
        private final Object RW_LOCK;

        public PropertyHandler() {
            this.modeConfig = null;
            this.internalConfig = null;
            this.itemList = new ArrayList<SplitConstraint>(10);
            this.RW_LOCK = new Object();
        }

        private FileObject getConfigFOInput() {
            FileObject modeConfigFO = ModeParser.this.isInLocalFolder() ? ModeParser.this.localParentFolder.getFileObject(ModeParser.this.getName(), "wsmode") : (ModeParser.this.isInModuleFolder() ? ModeParser.this.moduleParentFolder.getFileObject(ModeParser.this.getName(), "wsmode") : null);
            return modeConfigFO;
        }

        private FileObject getConfigFOOutput() throws IOException {
            FileObject modeConfigFO = ModeParser.this.localParentFolder.getFileObject(ModeParser.this.getName(), "wsmode");
            if (modeConfigFO != null) {
                return modeConfigFO;
            }
            StringBuffer buffer = new StringBuffer();
            buffer.append(ModeParser.this.getName());
            buffer.append('.');
            buffer.append("wsmode");
            modeConfigFO = FileUtil.createData((FileObject)ModeParser.this.localParentFolder, (String)buffer.toString());
            return modeConfigFO;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void readData(ModeConfig modeCfg, InternalConfig internalCfg) throws IOException {
            this.modeConfig = modeCfg;
            this.internalConfig = internalCfg;
            this.itemList.clear();
            FileObject cfgFOInput = this.getConfigFOInput();
            if (cfgFOInput == null) {
                throw new FileNotFoundException("[WinSys] Missing Mode configuration file:" + ModeParser.this.getName());
            }
            InputStream is = null;
            try {
                Object object = this.RW_LOCK;
                synchronized (object) {
                    is = cfgFOInput.getInputStream();
                    PersistenceManager.getDefault().getXMLParser(this).parse(new InputSource(is));
                }
            }
            catch (SAXException exc) {
                String msg = NbBundle.getMessage(ModeParser.class, (String)"EXC_ModeParse", (Object)cfgFOInput);
                throw (IOException)new IOException(msg).initCause(exc);
            }
            finally {
                try {
                    if (is != null) {
                        is.close();
                    }
                }
                catch (IOException exc) {
                    Logger.getLogger(ModeParser.class.getName()).log(Level.INFO, null, exc);
                }
            }
            this.modeConfig.constraints = this.itemList.toArray(new SplitConstraint[this.itemList.size()]);
            modeCfg = this.modeConfig;
            internalCfg = this.internalConfig;
            this.modeConfig = null;
            this.internalConfig = null;
        }

        @Override
        public void startElement(String nameSpace, String name, String qname, Attributes attrs) throws SAXException {
            if ("mode".equals(qname)) {
                this.handleMode(attrs);
            } else if (this.internalConfig.specVersion != null && this.internalConfig.specVersion.compareTo((Object)new SpecificationVersion("2.0")) >= 0) {
                if ("module".equals(qname)) {
                    this.handleModule(attrs);
                } else if ("name".equals(qname)) {
                    this.handleName(attrs);
                } else if ("kind".equals(qname)) {
                    this.handleKind(attrs);
                } else if ("slidingSide".equals(qname)) {
                    this.handleSlidingSide(attrs);
                } else if ("slide-in-size".equals(qname)) {
                    this.handleSlideInSize(attrs);
                } else if ("state".equals(qname)) {
                    this.handleState(attrs);
                } else if ("constraints".equals(qname)) {
                    this.handleConstraints(attrs);
                } else if ("path".equals(qname)) {
                    this.handlePath(attrs);
                } else if ("bounds".equals(qname)) {
                    this.handleBounds(attrs);
                } else if ("relative-bounds".equals(qname)) {
                    this.handleRelativeBounds(attrs);
                } else if ("frame".equals(qname)) {
                    this.handleFrame(attrs);
                } else if ("active-tc".equals(qname)) {
                    this.handleActiveTC(attrs);
                } else if ("empty-behavior".equals(qname)) {
                    this.handleEmptyBehavior(attrs);
                }
            } else if (DEBUG) {
                Debug.log(ModeParser.class, "-- ModeParser.startElement PARSING OLD");
            }
        }

        @Override
        public void error(SAXParseException ex) throws SAXException {
            throw ex;
        }

        private void handleMode(Attributes attrs) {
            String version = attrs.getValue("version");
            if (version != null) {
                this.internalConfig.specVersion = new SpecificationVersion(version);
            } else {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleMode] Warning: Missing attribute \"version\" of element \"mode\".");
                this.internalConfig.specVersion = new SpecificationVersion("2.0");
            }
        }

        private void handleModule(Attributes attrs) {
            String moduleCodeName = attrs.getValue("name");
            this.internalConfig.moduleCodeNameBase = null;
            this.internalConfig.moduleCodeNameRelease = null;
            this.internalConfig.moduleSpecificationVersion = null;
            if (moduleCodeName != null) {
                int i = moduleCodeName.indexOf(47);
                if (i != -1) {
                    this.internalConfig.moduleCodeNameBase = moduleCodeName.substring(0, i);
                    this.internalConfig.moduleCodeNameRelease = moduleCodeName.substring(i + 1);
                    this.checkReleaseCode(this.internalConfig);
                } else {
                    this.internalConfig.moduleCodeNameBase = moduleCodeName;
                }
                this.internalConfig.moduleSpecificationVersion = attrs.getValue("spec");
            }
        }

        private void checkReleaseCode(InternalConfig internalConfig) {
            if ("null".equals(internalConfig.moduleCodeNameRelease)) {
                Logger.getLogger(ModeParser.class.getName()).log(Level.INFO, null, new IllegalStateException("Module release code was saved as null string for module " + internalConfig.moduleCodeNameBase + "! Repairing."));
                internalConfig.moduleCodeNameRelease = null;
            }
        }

        private void handleName(Attributes attrs) throws SAXException {
            String name = attrs.getValue("unique");
            if (name != null) {
                this.modeConfig.name = name;
                if (!name.equals(ModeParser.this.getName())) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleName] Error: Value of attribute \"unique\" of element \"name\" and configuration file name must be the same.");
                    throw new SAXException("Invalid attribute value");
                }
            } else {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleName] Error: Missing required attribute \"unique\" of element \"name\".");
                throw new SAXException("Missing required attribute");
            }
            String includes = attrs.getValue("includes");
            if (includes != null) {
                String[] split = includes.split(",");
                HashSet<String> otherNames = new HashSet<String>(split.length);
                for (String s : split) {
                    if ((s = s.trim()).isEmpty()) continue;
                    otherNames.add(s);
                }
                if (otherNames.isEmpty()) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleName] Error: Attribute \"includes\" of element \"name\" is present but does not contain any valid mode names.");
                    throw new SAXException("Invalid attribute value");
                }
                this.modeConfig.otherNames = otherNames;
            }
        }

        private void handleKind(Attributes attrs) throws SAXException {
            String type = attrs.getValue("type");
            if (type != null) {
                if ("editor".equals(type)) {
                    this.modeConfig.kind = 1;
                } else if ("view".equals(type)) {
                    this.modeConfig.kind = 0;
                } else if ("sliding".equals(type)) {
                    this.modeConfig.kind = 2;
                    if (null != this.modeConfig.otherNames && !this.modeConfig.otherNames.isEmpty()) {
                        PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleName] Error: Sliding modes are not allowed to have additional names.");
                        throw new SAXException("Invalid attribute value");
                    }
                } else {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleKind] Warning: Invalid value of attribute \"type\".");
                    this.modeConfig.kind = 0;
                }
            } else {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleKind] Error: Missing required attribute \"type\" of element \"kind\".");
                this.modeConfig.kind = 0;
            }
        }

        private void handleSlidingSide(Attributes attrs) {
            String side = attrs.getValue("side");
            if (side != null) {
                if ("left".equals(side) || "right".equals(side) || "top".equals(side) || "bottom".equals(side)) {
                    this.modeConfig.side = side;
                } else {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleSlidingSide] Warning: Wrong value \"" + side + "\" of attribute \"side\" for sliding mode");
                    this.modeConfig.side = "left";
                }
            } else {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleSlidingSide] Warning: Missing value of attribute \"side\" for sliding mode.");
                this.modeConfig.side = "left";
            }
        }

        private void handleSlideInSize(Attributes attrs) {
            String tcId = attrs.getValue("tc-id");
            String size = attrs.getValue("size");
            if (tcId != null && size != null) {
                try {
                    Integer intSize = Integer.valueOf(size);
                    if (null == this.modeConfig.slideInSizes) {
                        this.modeConfig.slideInSizes = new HashMap<String, Integer>(5);
                    }
                    this.modeConfig.slideInSizes.put(tcId, intSize);
                    return;
                }
                catch (NumberFormatException nfE) {
                    // empty catch block
                }
            }
            PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleSlideInSize] Warning: Invalid attributes for preferred slide-in size.");
        }

        private void handleState(Attributes attrs) throws SAXException {
            String type = attrs.getValue("type");
            if (type != null) {
                if ("joined".equals(type)) {
                    this.modeConfig.state = 0;
                } else if ("separated".equals(type)) {
                    this.modeConfig.state = 1;
                } else {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleState] Warning: Invalid value of attribute \"type\" of element \"state\".");
                    this.modeConfig.state = 0;
                }
            } else {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleState] Error: Missing required attribute \"type\" of element \"state\".");
                this.modeConfig.state = 0;
            }
            String minimized = attrs.getValue("minimized");
            if (minimized != null) {
                if ("true".equals(minimized)) {
                    if (this.modeConfig.kind == 2) {
                        PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleState] Error: Sliding mode cannot be minimized.");
                        throw new SAXException("Invalid attribute value");
                    }
                    this.modeConfig.minimized = true;
                } else if ("false".equals(minimized)) {
                    this.modeConfig.minimized = false;
                } else {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleState] Warning: Invalid value of attribute \"minimized\" of element \"state\".");
                    this.modeConfig.minimized = false;
                }
            }
        }

        private void handleConstraints(Attributes attrs) {
        }

        private void handlePath(Attributes attrs) {
            int number;
            int orientation;
            double weight;
            String s = attrs.getValue("orientation");
            if ("horizontal".equals(s)) {
                orientation = 1;
            } else if ("vertical".equals(s)) {
                orientation = 0;
            } else {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handlePath] Warning: Invalid or missing value of attribute \"orientation\".");
                orientation = 0;
            }
            try {
                s = attrs.getValue("number");
                if (s != null) {
                    number = Integer.parseInt(s);
                } else {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handlePath] Warning: Missing value of attribute \"number\".");
                    number = 0;
                }
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handlePath] Warning: Cannot read element \"path\", attribute \"number\"", exc);
                number = 0;
            }
            try {
                s = attrs.getValue("weight");
                weight = s != null ? Double.parseDouble(s) : 0.5;
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handlePath] Warning: Cannot read element \"path\", attribute \"weight\".", exc);
                weight = 0.5;
            }
            SplitConstraint item = new SplitConstraint(orientation, number, weight);
            this.itemList.add(item);
        }

        private void handleBounds(Attributes attrs) {
            try {
                this.modeConfig.bounds = null;
                String s = attrs.getValue("x");
                if (s == null) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleBounds] Warning: Missing attribute \"x\" of element \"bounds\".");
                    return;
                }
                int x = Integer.parseInt(s);
                s = attrs.getValue("y");
                if (s == null) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleBounds] Warning: Missing attribute \"y\" of element \"bounds\".");
                    return;
                }
                int y = Integer.parseInt(s);
                s = attrs.getValue("width");
                if (s == null) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleBounds] Warning: Missing attribute \"width\" of element \"bounds\".");
                    return;
                }
                int width = Integer.parseInt(s);
                s = attrs.getValue("height");
                if (s == null) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleBounds] Warning: Missing attribute \"height\" of element \"bounds\".");
                    return;
                }
                int height = Integer.parseInt(s);
                this.modeConfig.bounds = new Rectangle(x, y, width, height);
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleBounds] Warning: Cannot read element \"bounds\".", exc);
            }
        }

        private void handleRelativeBounds(Attributes attrs) {
            try {
                this.modeConfig.relativeBounds = null;
                String s = attrs.getValue("x");
                if (s == null) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleRelativeBounds] Warning: Missing attribute \"x\" of element \"relative-bounds\".");
                    return;
                }
                int x = Integer.parseInt(s);
                s = attrs.getValue("y");
                if (s == null) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleRelativeBounds] Warning: Missing attribute \"y\" of element \"relative-bounds\".");
                    return;
                }
                int y = Integer.parseInt(s);
                s = attrs.getValue("width");
                if (s == null) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleRelativeBounds] Warning: Missing attribute \"width\" of element \"relative-bounds\".");
                    return;
                }
                int width = Integer.parseInt(s);
                s = attrs.getValue("height");
                if (s == null) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleRelativeBounds] Warning: Missing attribute \"height\" of element \"relative-bounds\".");
                    return;
                }
                int height = Integer.parseInt(s);
                this.modeConfig.relativeBounds = new Rectangle(x, y, width, height);
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleRelativeBounds] Warning: Cannot read element \"relative-bounds\".", exc);
            }
        }

        private void handleFrame(Attributes attrs) {
            String frameState = attrs.getValue("state");
            if (frameState != null) {
                try {
                    this.modeConfig.frameState = Integer.parseInt(frameState);
                }
                catch (NumberFormatException exc) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleFrame] Warning: Cannot read attribute \"state\" of element \"frame\".", exc);
                    this.modeConfig.frameState = 0;
                }
            } else {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleFrame] Warning: Missing value of attribute \"state\" of element \"frame\".");
                this.modeConfig.frameState = 0;
            }
        }

        private void handleActiveTC(Attributes attrs) {
            String id = attrs.getValue("id");
            this.modeConfig.selectedTopComponentID = id != null ? id : "";
            String prevId = attrs.getValue("prev-id");
            this.modeConfig.previousSelectedTopComponentID = prevId != null ? prevId : "";
        }

        private void handleEmptyBehavior(Attributes attrs) {
            String value = attrs.getValue("permanent");
            if ("true".equals(value)) {
                this.modeConfig.permanent = true;
            } else if ("false".equals(value)) {
                this.modeConfig.permanent = false;
            } else {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.ModeParser.handleEmptyBehavior] Warning: Invalid value of attribute \"permanent\".");
                this.modeConfig.permanent = false;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void writeData(ModeConfig mc, InternalConfig ic) throws IOException {
            StringBuffer buff = this.fillBuffer(mc, ic);
            Object object = this.RW_LOCK;
            synchronized (object) {
                FileObject cfgFOOutput = this.getConfigFOOutput();
                FileLock lock = null;
                OutputStream os = null;
                OutputStreamWriter osw = null;
                try {
                    lock = cfgFOOutput.lock();
                    os = cfgFOOutput.getOutputStream(lock);
                    osw = new OutputStreamWriter(os, "UTF-8");
                    osw.write(buff.toString());
                }
                finally {
                    try {
                        if (osw != null) {
                            osw.close();
                        }
                    }
                    catch (IOException exc) {
                        Logger.getLogger(ModeParser.class.getName()).log(Level.INFO, null, exc);
                    }
                    if (lock != null) {
                        lock.releaseLock();
                    }
                }
            }
        }

        private StringBuffer fillBuffer(ModeConfig mc, InternalConfig ic) throws IOException {
            StringBuffer buff = new StringBuffer(800);
            buff.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n").append("<mode version=\"2.4\">\n");
            this.appendModule(ic, buff);
            this.appendName(mc, buff);
            this.appendKind(mc, buff);
            if (mc.kind == 2) {
                this.appendSlidingSide(mc, buff);
                if (null != mc.slideInSizes) {
                    this.appendSlideInSize(mc, buff);
                }
            }
            this.appendState(mc, buff);
            this.appendConstraints(mc, buff);
            if (mc.bounds != null) {
                this.appendBounds(mc, buff);
            } else if (mc.relativeBounds != null) {
                this.appendRelativeBounds(mc, buff);
            }
            this.appendFrame(mc, buff);
            this.appendActiveTC(mc, buff);
            this.appendEmptyBehavior(mc, buff);
            buff.append("</mode>\n");
            return buff;
        }

        private void appendModule(InternalConfig ic, StringBuffer buff) {
            if (ic == null) {
                return;
            }
            if (ic.moduleCodeNameBase != null) {
                buff.append("    <module name=\"");
                buff.append(ic.moduleCodeNameBase);
                if (ic.moduleCodeNameRelease != null) {
                    buff.append("/").append(ic.moduleCodeNameRelease);
                }
                if (ic.moduleSpecificationVersion != null) {
                    buff.append("\" spec=\"");
                    buff.append(ic.moduleSpecificationVersion);
                }
                buff.append("\" />\n");
            }
        }

        private void appendName(ModeConfig mc, StringBuffer buff) {
            buff.append("    <name unique=\"");
            buff.append(mc.name);
            buff.append("\" ");
            if (null != mc.otherNames && !mc.otherNames.isEmpty()) {
                buff.append(" includes=\"");
                boolean comma = false;
                for (String s : mc.otherNames) {
                    if (comma) {
                        buff.append(',');
                    }
                    buff.append(s);
                    comma = true;
                }
                buff.append("\" ");
            }
            buff.append(" />\n");
        }

        private void appendKind(ModeConfig mc, StringBuffer buff) {
            buff.append("  <kind type=\"");
            if (mc.kind == 1) {
                buff.append("editor");
            } else if (mc.kind == 0) {
                buff.append("view");
            } else if (mc.kind == 2) {
                buff.append("sliding");
            }
            buff.append("\" />\n");
        }

        private void appendSlidingSide(ModeConfig mc, StringBuffer buff) {
            buff.append("  <slidingSide side=\"");
            buff.append(mc.side);
            buff.append("\" ");
            buff.append("/>\n");
        }

        private void appendSlideInSize(ModeConfig mc, StringBuffer buff) {
            if (null != mc.slideInSizes) {
                for (String tcId : mc.slideInSizes.keySet()) {
                    Integer size = mc.slideInSizes.get(tcId);
                    buff.append("  <slide-in-size tc-id=\"");
                    buff.append(tcId);
                    buff.append("\" size=\"");
                    buff.append(size);
                    buff.append("\" />\n");
                }
            }
        }

        private void appendState(ModeConfig mc, StringBuffer buff) {
            buff.append("  <state type=\"");
            if (mc.state == 0) {
                buff.append("joined");
            } else if (mc.state == 1) {
                buff.append("separated");
            }
            buff.append("\" ");
            if (mc.minimized) {
                buff.append(" minimized=\"true\" ");
            }
            buff.append(" />\n");
        }

        private void appendConstraints(ModeConfig mc, StringBuffer buff) {
            if (mc.constraints.length == 0) {
                return;
            }
            buff.append("  <constraints>\n");
            for (int i = 0; i < mc.constraints.length; ++i) {
                SplitConstraint item = mc.constraints[i];
                buff.append("    <path orientation=\"");
                if (item.orientation == 1) {
                    buff.append("horizontal");
                } else {
                    buff.append("vertical");
                }
                buff.append("\" number=\"").append(item.index).append("\" weight=\"").append(item.splitWeight).append("\"/>\n");
            }
            buff.append("  </constraints>\n");
        }

        private void appendBounds(ModeConfig mc, StringBuffer buff) {
            if (mc.bounds == null) {
                return;
            }
            buff.append("  <bounds x=\"").append(mc.bounds.x).append("\" y=\"").append(mc.bounds.y).append("\" width=\"").append(mc.bounds.width).append("\" height=\"").append(mc.bounds.height).append("\" />\n");
        }

        private void appendRelativeBounds(ModeConfig mc, StringBuffer buff) {
            if (mc.relativeBounds == null) {
                return;
            }
            buff.append("  <relative-bounds x=\"").append(mc.relativeBounds.x).append("\" y=\"").append(mc.relativeBounds.y).append("\" width=\"").append(mc.relativeBounds.width).append("\" height=\"").append(mc.relativeBounds.height).append("\" />\n");
        }

        private void appendFrame(ModeConfig mc, StringBuffer buff) {
            buff.append("  <frame state=\"").append(mc.frameState).append("\"/>\n");
        }

        private void appendActiveTC(ModeConfig mc, StringBuffer buff) {
            if (mc.selectedTopComponentID != null && !"".equals(mc.selectedTopComponentID) || mc.previousSelectedTopComponentID != null && !"".equals(mc.previousSelectedTopComponentID)) {
                String tcName;
                buff.append("    <active-tc ");
                if (mc.selectedTopComponentID != null && !"".equals(mc.selectedTopComponentID)) {
                    tcName = PersistenceManager.escapeTcId4XmlContent(mc.selectedTopComponentID);
                    buff.append(" id=\"").append(tcName).append("\" ");
                }
                if (mc.previousSelectedTopComponentID != null && !"".equals(mc.previousSelectedTopComponentID)) {
                    tcName = PersistenceManager.escapeTcId4XmlContent(mc.previousSelectedTopComponentID);
                    buff.append(" prev-id=\"").append(tcName).append("\" ");
                }
                buff.append("/>\n");
            }
        }

        private void appendEmptyBehavior(ModeConfig mc, StringBuffer buff) {
            buff.append("    <empty-behavior permanent=\"").append(mc.permanent).append("\"/>\n");
        }
    }

}

