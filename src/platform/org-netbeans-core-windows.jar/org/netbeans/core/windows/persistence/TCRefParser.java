/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.SpecificationVersion
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.windows.persistence;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.windows.Debug;
import org.netbeans.core.windows.persistence.InternalConfig;
import org.netbeans.core.windows.persistence.PersistenceManager;
import org.netbeans.core.windows.persistence.TCRefConfig;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.modules.SpecificationVersion;
import org.openide.util.NbBundle;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

class TCRefParser {
    public static final String INSTANCE_DTD_ID_1_0 = "-//NetBeans//DTD Top Component in Mode Properties 1.0//EN";
    public static final String INSTANCE_DTD_ID_2_0 = "-//NetBeans//DTD Top Component in Mode Properties 2.0//EN";
    public static final String INSTANCE_DTD_ID_2_1 = "-//NetBeans//DTD Top Component in Mode Properties 2.1//EN";
    public static final String INSTANCE_DTD_ID_2_2 = "-//NetBeans//DTD Top Component in Mode Properties 2.2//EN";
    private static final boolean DEBUG = Debug.isLoggable(TCRefParser.class);
    private String tc_id;
    private FileObject moduleParentFolder;
    private FileObject localParentFolder;
    private InternalConfig internalConfig;
    private boolean inModuleFolder;
    private boolean inLocalFolder;

    public TCRefParser(String tc_id) {
        this.tc_id = tc_id;
    }

    TCRefConfig load() throws IOException {
        if (DEBUG) {
            Debug.log(TCRefParser.class, "load ENTER tcRef:" + this.tc_id);
        }
        TCRefConfig tcRefCfg = new TCRefConfig();
        PropertyHandler propertyHandler = new PropertyHandler();
        InternalConfig internalCfg = this.getInternalConfig();
        internalCfg.clear();
        propertyHandler.readData(tcRefCfg, internalCfg);
        if (DEBUG) {
            Debug.log(TCRefParser.class, "load LEAVE tcRef:" + this.tc_id);
        }
        return tcRefCfg;
    }

    void save(TCRefConfig tcRefCfg) throws IOException {
        if (DEBUG) {
            Debug.log(TCRefParser.class, "save ENTER tcRef:" + this.tc_id);
        }
        PropertyHandler propertyHandler = new PropertyHandler();
        InternalConfig internalCfg = this.getInternalConfig();
        propertyHandler.writeData(tcRefCfg, internalCfg);
        if (DEBUG) {
            Debug.log(TCRefParser.class, "save LEAVE tcRef:" + this.tc_id);
        }
    }

    String getName() {
        return this.tc_id;
    }

    InternalConfig getInternalConfig() {
        if (this.internalConfig == null) {
            this.internalConfig = new InternalConfig();
        }
        return this.internalConfig;
    }

    void setInternalConfig(InternalConfig internalCfg) {
        this.internalConfig = internalCfg;
    }

    boolean isInModuleFolder() {
        return this.inModuleFolder;
    }

    void setInModuleFolder(boolean inModuleFolder) {
        this.inModuleFolder = inModuleFolder;
    }

    boolean isInLocalFolder() {
        return this.inLocalFolder;
    }

    void setInLocalFolder(boolean inLocalFolder) {
        this.inLocalFolder = inLocalFolder;
    }

    void setModuleParentFolder(FileObject moduleParentFolder) {
        this.moduleParentFolder = moduleParentFolder;
    }

    void setLocalParentFolder(FileObject localParentFolder) {
        this.localParentFolder = localParentFolder;
    }

    void log(String s) {
        Debug.log(TCRefParser.class, s);
    }

    private final class PropertyHandler
    extends DefaultHandler {
        private TCRefConfig tcRefConfig;
        private InternalConfig internalConfig;
        private final Object RW_LOCK;

        public PropertyHandler() {
            this.tcRefConfig = null;
            this.internalConfig = null;
            this.RW_LOCK = new Object();
        }

        private FileObject getConfigFOInput() {
            FileObject tcRefConfigFO = TCRefParser.this.isInLocalFolder() ? TCRefParser.this.localParentFolder.getFileObject(TCRefParser.this.getName(), "wstcref") : (TCRefParser.this.isInModuleFolder() ? TCRefParser.this.moduleParentFolder.getFileObject(TCRefParser.this.getName(), "wstcref") : null);
            return tcRefConfigFO;
        }

        private FileObject getConfigFOOutput() throws IOException {
            FileObject tcRefConfigFO = TCRefParser.this.localParentFolder.getFileObject(TCRefParser.this.getName(), "wstcref");
            if (tcRefConfigFO != null) {
                return tcRefConfigFO;
            }
            StringBuffer buffer = new StringBuffer();
            buffer.append(TCRefParser.this.getName());
            buffer.append('.');
            buffer.append("wstcref");
            tcRefConfigFO = FileUtil.createData((FileObject)TCRefParser.this.localParentFolder, (String)buffer.toString());
            return tcRefConfigFO;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void readData(TCRefConfig tcRefCfg, InternalConfig internalCfg) throws IOException {
            this.tcRefConfig = tcRefCfg;
            this.internalConfig = internalCfg;
            FileObject cfgFOInput = this.getConfigFOInput();
            if (cfgFOInput == null) {
                throw new FileNotFoundException("[WinSys] Missing TCRef configuration file:" + TCRefParser.this.getName());
            }
            InputStream is = null;
            try {
                Object object = this.RW_LOCK;
                synchronized (object) {
                    is = cfgFOInput.getInputStream();
                    PersistenceManager.getDefault().getXMLParser(this).parse(new InputSource(is));
                }
            }
            catch (SAXException exc) {
                String msg = NbBundle.getMessage(TCRefParser.class, (String)"EXC_TCRefParse", (Object)cfgFOInput);
                throw (IOException)new IOException(msg).initCause(exc);
            }
            finally {
                try {
                    if (is != null) {
                        is.close();
                    }
                }
                catch (IOException exc) {
                    Logger.getLogger(TCRefParser.class.getName()).log(Level.WARNING, null, exc);
                }
            }
            tcRefCfg = this.tcRefConfig;
            internalCfg = this.internalConfig;
            this.tcRefConfig = null;
            this.internalConfig = null;
        }

        @Override
        public void startElement(String nameSpace, String name, String qname, Attributes attrs) throws SAXException {
            if ("tc-ref".equals(qname)) {
                this.handleTCRef(attrs);
            } else if (this.internalConfig.specVersion.compareTo((Object)new SpecificationVersion("2.0")) >= 0) {
                if ("module".equals(qname)) {
                    this.handleModule(attrs);
                } else if ("tc-id".equals(qname)) {
                    this.handleTcId(attrs);
                } else if ("state".equals(qname)) {
                    this.handleState(attrs);
                } else if ("previousMode".equals(qname)) {
                    this.handlePreviousMode(attrs);
                } else if ("docking-status".equals(qname)) {
                    this.handleDockingStatus(attrs);
                } else if ("slide-in-status".equals(qname)) {
                    this.handleSlideInStatus(attrs);
                }
            } else {
                TCRefParser.this.log("-- TCRefParser.startElement PARSING OLD");
            }
        }

        @Override
        public void error(SAXParseException ex) throws SAXException {
            throw ex;
        }

        private void handleTCRef(Attributes attrs) {
            String tc_id;
            String version = attrs.getValue("version");
            if (version != null) {
                this.internalConfig.specVersion = new SpecificationVersion(version);
            } else {
                PersistenceManager.LOG.log(Level.WARNING, "[WinSys.TCRefParser.handleTCRef] Warning: Missing attribute \"version\" of element \"tc-ref\".");
                this.internalConfig.specVersion = new SpecificationVersion("2.0");
            }
            if (this.internalConfig.specVersion.compareTo((Object)new SpecificationVersion("2.0")) < 0 && (tc_id = attrs.getValue("id")) == null) {
                PersistenceManager.LOG.log(Level.WARNING, "[WinSys.TCRefParser.handleTCRef] Warning: Missing attribute \"id\" of element \"tc-ref\".");
            }
        }

        private void handleModule(Attributes attrs) {
            String moduleCodeName = attrs.getValue("name");
            this.internalConfig.moduleCodeNameBase = null;
            this.internalConfig.moduleCodeNameRelease = null;
            this.internalConfig.moduleSpecificationVersion = null;
            if (moduleCodeName != null) {
                int i = moduleCodeName.indexOf(47);
                if (i != -1) {
                    this.internalConfig.moduleCodeNameBase = moduleCodeName.substring(0, i);
                    this.internalConfig.moduleCodeNameRelease = moduleCodeName.substring(i + 1);
                    this.checkReleaseCode(this.internalConfig);
                } else {
                    this.internalConfig.moduleCodeNameBase = moduleCodeName;
                }
                this.internalConfig.moduleSpecificationVersion = attrs.getValue("spec");
            }
        }

        private void checkReleaseCode(InternalConfig internalConfig) {
            if ("null".equals(internalConfig.moduleCodeNameRelease)) {
                Logger.getLogger(TCRefParser.class.getName()).log(Level.WARNING, null, new IllegalStateException("Module release code was saved as null string for module " + internalConfig.moduleCodeNameBase + "! Repairing."));
                internalConfig.moduleCodeNameRelease = null;
            }
        }

        private void handleTcId(Attributes attrs) throws SAXException {
            String tc_id = attrs.getValue("id");
            if (tc_id != null) {
                this.tcRefConfig.tc_id = tc_id;
                if (!tc_id.equals(TCRefParser.this.getName())) {
                    PersistenceManager.LOG.log(Level.WARNING, "[WinSys.TCRefParser.handleTcId] Error: Value of attribute \"id\" of element \"tc-id\" and configuration file name must be the same: " + tc_id + " x " + TCRefParser.this.getName());
                    throw new SAXException("Invalid attribute value");
                }
            } else {
                PersistenceManager.LOG.log(Level.WARNING, "[WinSys.TCRefParser.handleTcId] Error: Missing required attribute \"id\" of element \"tc-id\".");
                throw new SAXException("Missing required attribute");
            }
        }

        private void handleState(Attributes attrs) throws SAXException {
            String opened = attrs.getValue("opened");
            if (opened != null) {
                if ("true".equals(opened)) {
                    this.tcRefConfig.opened = true;
                } else if ("false".equals(opened)) {
                    this.tcRefConfig.opened = false;
                } else {
                    PersistenceManager.LOG.log(Level.WARNING, "[WinSys.TCRefParser.handleState] Warning: Invalid value of attribute \"opened\" of element \"state\".");
                    this.tcRefConfig.opened = false;
                }
            } else {
                PersistenceManager.LOG.log(Level.WARNING, "[WinSys.TCRefParser.handleState] Warning: Missing required attribute \"opened\" of element \"state\".");
                this.tcRefConfig.opened = false;
            }
        }

        private void handlePreviousMode(Attributes attrs) throws SAXException {
            String name = attrs.getValue("name");
            if (name != null) {
                this.tcRefConfig.previousMode = name;
            } else {
                PersistenceManager.LOG.log(Level.WARNING, "[WinSys.TCRefParser.handlePreviousMode] Warning: Missing required attribute \"name\" of element \"previousMode\".");
                this.tcRefConfig.previousMode = null;
            }
            String index = attrs.getValue("index");
            if (index != null) {
                try {
                    this.tcRefConfig.previousIndex = Integer.parseInt(index);
                }
                catch (NumberFormatException nfE) {
                    PersistenceManager.LOG.log(Level.WARNING, "[WinSys.TCRefParser.handlePreviousMode] Warning: Invalid value of attribute \"index\" of element \"previousMode\".");
                    this.tcRefConfig.previousIndex = -1;
                }
            }
        }

        private void handleDockingStatus(Attributes attrs) throws SAXException {
            String status = attrs.getValue("maximized-mode");
            if (status != null) {
                if ("docked".equals(status)) {
                    this.tcRefConfig.dockedInMaximizedMode = true;
                } else if ("slided".equals(status)) {
                    this.tcRefConfig.dockedInMaximizedMode = false;
                } else {
                    PersistenceManager.LOG.log(Level.WARNING, "[WinSys.TCRefParser.handleDockingStatus] Warning: Invalid value of attribute \"maximized-mode\" of element \"docking-status\".");
                    this.tcRefConfig.dockedInMaximizedMode = false;
                }
            }
            if ((status = attrs.getValue("default-mode")) != null) {
                if ("docked".equals(status)) {
                    this.tcRefConfig.dockedInDefaultMode = true;
                } else if ("slided".equals(status)) {
                    this.tcRefConfig.dockedInDefaultMode = false;
                } else {
                    PersistenceManager.LOG.log(Level.WARNING, "[WinSys.TCRefParser.handleDockingStatus] Warning: Invalid value of attribute \"default-mode\" of element \"docking-status\".");
                    this.tcRefConfig.dockedInDefaultMode = true;
                }
            }
        }

        private void handleSlideInStatus(Attributes attrs) throws SAXException {
            String status = attrs.getValue("maximized");
            if (status != null) {
                if ("true".equals(status)) {
                    this.tcRefConfig.slidedInMaximized = true;
                } else if ("false".equals(status)) {
                    this.tcRefConfig.slidedInMaximized = false;
                } else {
                    PersistenceManager.LOG.log(Level.WARNING, "[WinSys.TCRefParser.handleSlideInStatus] Warning: Invalid value of attribute \"maximized\" of element \"slide-in-status\".");
                    this.tcRefConfig.slidedInMaximized = false;
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void writeData(TCRefConfig tcRefCfg, InternalConfig ic) throws IOException {
            StringBuffer buff = this.fillBuffer(tcRefCfg, ic);
            Object object = this.RW_LOCK;
            synchronized (object) {
                FileObject cfgFOOutput = this.getConfigFOOutput();
                FileLock lock = null;
                OutputStream os = null;
                OutputStreamWriter osw = null;
                try {
                    lock = cfgFOOutput.lock();
                    os = cfgFOOutput.getOutputStream(lock);
                    osw = new OutputStreamWriter(os, "UTF-8");
                    osw.write(buff.toString());
                }
                finally {
                    try {
                        if (osw != null) {
                            osw.close();
                        }
                    }
                    catch (IOException exc) {
                        Logger.getLogger(TCRefParser.class.getName()).log(Level.WARNING, null, exc);
                    }
                    if (lock != null) {
                        lock.releaseLock();
                    }
                }
            }
        }

        private StringBuffer fillBuffer(TCRefConfig tcRefCfg, InternalConfig ic) throws IOException {
            StringBuffer buff = new StringBuffer(800);
            Object curValue = null;
            buff.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n");
            buff.append("<tc-ref version=\"2.2\">\n");
            this.appendModule(ic, buff);
            this.appendTcId(tcRefCfg, buff);
            this.appendState(tcRefCfg, buff);
            if (tcRefCfg.previousMode != null) {
                this.appendPreviousMode(tcRefCfg, buff);
            }
            this.appendDockingStatus(tcRefCfg, buff);
            this.appendSlideInStatus(tcRefCfg, buff);
            buff.append("</tc-ref>\n");
            return buff;
        }

        private void appendModule(InternalConfig ic, StringBuffer buff) {
            if (ic == null) {
                return;
            }
            if (ic.moduleCodeNameBase != null) {
                buff.append("    <module");
                buff.append(" name=\"");
                buff.append(ic.moduleCodeNameBase);
                if (ic.moduleCodeNameRelease != null) {
                    buff.append("/" + ic.moduleCodeNameRelease);
                }
                if (ic.moduleSpecificationVersion != null) {
                    buff.append("\" spec=\"");
                    buff.append(ic.moduleSpecificationVersion);
                }
                buff.append("\" />\n");
            }
        }

        private void appendTcId(TCRefConfig tcRefCfg, StringBuffer buff) {
            buff.append("    <tc-id");
            buff.append(" id=\"");
            buff.append(PersistenceManager.escapeTcId4XmlContent(tcRefCfg.tc_id));
            buff.append("\"");
            buff.append(" />\n");
        }

        private void appendState(TCRefConfig tcRefCfg, StringBuffer buff) {
            buff.append("    <state");
            buff.append(" opened=\"");
            if (tcRefCfg.opened) {
                buff.append("true");
            } else {
                buff.append("false");
            }
            buff.append("\"");
            buff.append(" />\n");
        }

        private void appendDockingStatus(TCRefConfig tcRefCfg, StringBuffer buff) {
            if (tcRefCfg.dockedInMaximizedMode || !tcRefCfg.dockedInDefaultMode) {
                buff.append("    <docking-status");
                if (tcRefCfg.dockedInMaximizedMode) {
                    buff.append(" maximized-mode=\"docked\"");
                }
                if (!tcRefCfg.dockedInDefaultMode) {
                    buff.append(" default-mode=\"slided\"");
                }
                buff.append(" />\n");
            }
        }

        private void appendSlideInStatus(TCRefConfig tcRefCfg, StringBuffer buff) {
            if (tcRefCfg.slidedInMaximized) {
                buff.append("    <slide-in-status maximized=\"true\" />\n");
            }
        }

        private void appendPreviousMode(TCRefConfig tcRefCfg, StringBuffer buff) {
            buff.append("    <previousMode name=\"");
            buff.append(tcRefCfg.previousMode).append("\" ");
            if (tcRefCfg.previousIndex >= 0) {
                buff.append(" index=\"").append(tcRefCfg.previousIndex).append("\" ");
            }
            buff.append(" />\n");
        }
    }

}

