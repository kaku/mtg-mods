/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.persistence;

import org.netbeans.core.windows.persistence.TCGroupConfig;

public class GroupConfig {
    public String name = "";
    public boolean opened;
    public TCGroupConfig[] tcGroupConfigs = new TCGroupConfig[0];

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GroupConfig)) {
            return false;
        }
        GroupConfig groupCfg = (GroupConfig)obj;
        if (!this.name.equals(groupCfg.name)) {
            return false;
        }
        if (this.opened != groupCfg.opened) {
            return false;
        }
        if (this.tcGroupConfigs.length != groupCfg.tcGroupConfigs.length) {
            return false;
        }
        for (int i = 0; i < this.tcGroupConfigs.length; ++i) {
            TCGroupConfig tcGroupCfg = null;
            for (int j = 0; j < groupCfg.tcGroupConfigs.length; ++j) {
                if (!this.tcGroupConfigs[i].tc_id.equals(groupCfg.tcGroupConfigs[j].tc_id)) continue;
                tcGroupCfg = groupCfg.tcGroupConfigs[j];
                break;
            }
            if (tcGroupCfg == null) {
                return false;
            }
            if (this.tcGroupConfigs[i].equals(tcGroupCfg)) continue;
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hash = 17;
        hash = 37 * hash + this.name.hashCode();
        hash = 37 * hash + (this.opened ? 0 : 1);
        for (int i = 0; i < this.tcGroupConfigs.length; ++i) {
            hash = 37 * hash + this.tcGroupConfigs[i].hashCode();
        }
        return hash;
    }
}

