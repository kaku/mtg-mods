/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.persistence;

import java.awt.Rectangle;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import org.netbeans.core.windows.SplitConstraint;
import org.netbeans.core.windows.persistence.TCRefConfig;

public class ModeConfig {
    public String name = "";
    public Collection<String> otherNames;
    public int state;
    public int kind;
    public String side;
    public SplitConstraint[] constraints = new SplitConstraint[0];
    public Rectangle bounds;
    public Rectangle relativeBounds;
    public int frameState;
    public String selectedTopComponentID = "";
    public boolean permanent = true;
    public boolean minimized = false;
    public TCRefConfig[] tcRefConfigs = new TCRefConfig[0];
    public Map<String, Integer> slideInSizes;
    public String previousSelectedTopComponentID = "";

    public boolean equals(Object obj) {
        int i;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModeConfig)) {
            return false;
        }
        ModeConfig modeCfg = (ModeConfig)obj;
        if (!this.name.equals(modeCfg.name)) {
            return false;
        }
        if (this.state != modeCfg.state || this.kind != modeCfg.kind) {
            return false;
        }
        if (null != this.side && !this.side.equals(modeCfg.side)) {
            return false;
        }
        if (null == this.side && null != modeCfg.side) {
            return false;
        }
        if (this.constraints.length != modeCfg.constraints.length) {
            return false;
        }
        for (i = 0; i < this.constraints.length; ++i) {
            if (this.constraints[i].equals(modeCfg.constraints[i])) continue;
            return false;
        }
        if (this.bounds != null && modeCfg.bounds != null ? !this.bounds.equals(modeCfg.bounds) : this.bounds != null || modeCfg.bounds != null) {
            return false;
        }
        if (this.relativeBounds != null && modeCfg.relativeBounds != null ? !this.relativeBounds.equals(modeCfg.relativeBounds) : this.relativeBounds != null || modeCfg.relativeBounds != null) {
            return false;
        }
        if (this.frameState != modeCfg.frameState) {
            return false;
        }
        if (!this.selectedTopComponentID.equals(modeCfg.selectedTopComponentID)) {
            return false;
        }
        if (this.permanent != modeCfg.permanent) {
            return false;
        }
        if (this.minimized != modeCfg.minimized) {
            return false;
        }
        if (this.tcRefConfigs.length != modeCfg.tcRefConfigs.length) {
            return false;
        }
        for (i = 0; i < this.tcRefConfigs.length; ++i) {
            if (this.tcRefConfigs[i].equals(modeCfg.tcRefConfigs[i])) continue;
            return false;
        }
        if (null != this.slideInSizes && null != modeCfg.slideInSizes) {
            if (this.slideInSizes.size() != modeCfg.slideInSizes.size()) {
                return false;
            }
            for (String tcId : this.slideInSizes.keySet()) {
                if (this.slideInSizes.get(tcId).equals(modeCfg.slideInSizes.get(tcId))) continue;
                return false;
            }
        } else if (null != this.slideInSizes || null != modeCfg.slideInSizes) {
            return false;
        }
        if (null != this.otherNames && null != modeCfg.otherNames) {
            if (this.otherNames.size() != modeCfg.otherNames.size()) {
                return false;
            }
            if (!this.otherNames.containsAll(modeCfg.otherNames)) {
                return false;
            }
        } else if (null != this.otherNames || null != modeCfg.otherNames) {
            return false;
        }
        if (!this.previousSelectedTopComponentID.equals(modeCfg.previousSelectedTopComponentID)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int hash = 17;
        hash = 37 * hash + this.name.hashCode();
        hash = 37 * hash + this.state;
        hash = 37 * hash + this.kind;
        if (this.side != null) {
            hash = 37 * hash + this.side.hashCode();
        }
        for (i = 0; i < this.constraints.length; ++i) {
            hash = 37 * hash + this.constraints[i].hashCode();
        }
        if (this.bounds != null) {
            hash = 37 * hash + this.bounds.hashCode();
        }
        if (this.relativeBounds != null) {
            hash = 37 * hash + this.relativeBounds.hashCode();
        }
        hash = 37 * hash + this.frameState;
        hash = 37 * hash + this.selectedTopComponentID.hashCode();
        hash = 37 * hash + (this.permanent ? 0 : 1);
        hash = 37 * hash + (this.minimized ? 0 : 1);
        for (i = 0; i < this.tcRefConfigs.length; ++i) {
            hash = 37 * hash + this.tcRefConfigs[i].hashCode();
        }
        if (null != this.slideInSizes) {
            for (String key : this.slideInSizes.keySet()) {
                hash = 37 * hash + key.hashCode();
                hash = 37 * hash + this.slideInSizes.get(key).hashCode();
            }
        }
        if (null != this.otherNames) {
            for (String n : this.otherNames) {
                hash = 37 * hash + n.hashCode();
            }
        }
        hash = 37 * hash + this.previousSelectedTopComponentID.hashCode();
        return hash;
    }
}

