/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.SpecificationVersion
 */
package org.netbeans.core.windows.persistence;

import org.openide.modules.SpecificationVersion;

public class InternalConfig {
    public SpecificationVersion specVersion;
    public String moduleCodeNameBase;
    public String moduleCodeNameRelease;
    public String moduleSpecificationVersion;

    public void clear() {
        this.specVersion = null;
        this.moduleCodeNameBase = null;
        this.moduleCodeNameRelease = null;
        this.moduleSpecificationVersion = null;
    }
}

