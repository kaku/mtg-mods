/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.SaveCookie
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.loaders.InstanceDataObject
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.SpecificationVersion
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.core.windows.persistence;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InvalidObjectException;
import java.io.NotSerializableException;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.windows.Debug;
import org.netbeans.core.windows.persistence.GroupConfig;
import org.netbeans.core.windows.persistence.ModeConfig;
import org.netbeans.core.windows.persistence.ModuleChangeHandler;
import org.netbeans.core.windows.persistence.RoleFileSystem;
import org.netbeans.core.windows.persistence.TCGroupConfig;
import org.netbeans.core.windows.persistence.TCRefConfig;
import org.netbeans.core.windows.persistence.WindowManagerConfig;
import org.netbeans.core.windows.persistence.WindowManagerParser;
import org.openide.cookies.InstanceCookie;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.loaders.InstanceDataObject;
import org.openide.modules.ModuleInfo;
import org.openide.modules.SpecificationVersion;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;
import org.openide.xml.XMLUtil;
import org.xml.sax.ContentHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public final class PersistenceManager
implements PropertyChangeListener {
    static final Logger LOG = Logger.getLogger("org.netbeans.core.windows.persistence");
    private static final String ROOT_MODULE_FOLDER = "Windows2";
    private static final String ROOT_LOCAL_FOLDER = "Windows2Local";
    static final String WINDOWMANAGER_FOLDER = "WindowManager";
    static final String GROUPS_FOLDER = "Groups";
    static final String MODES_FOLDER = "Modes";
    public static final String COMPS_FOLDER = "Components";
    public static final String WINDOWMANAGER_EXT = "wswmgr";
    public static final String WORKSPACE_EXT = "wswksp";
    public static final String MODE_EXT = "wsmode";
    public static final String TCREF_EXT = "wstcref";
    public static final String GROUP_EXT = "wsgrp";
    public static final String TCGROUP_EXT = "wstcgrp";
    public static final String COMPONENT_EXT = "settings";
    private static final String DEFAULT_TC_NAME = "untitled_tc";
    private static final boolean DEBUG = Debug.isLoggable(PersistenceManager.class);
    private FileObject rootModuleFolder;
    private FileObject rootLocalFolder;
    private FileObject compsModuleFolder;
    private FileObject groupsModuleFolder;
    private FileObject groupsLocalFolder;
    private FileObject modesModuleFolder;
    private FileObject modesLocalFolder;
    private WindowManagerParser windowManagerParser;
    private ModuleChangeHandler changeHandler;
    private final Map<TopComponent, String> topComponent2IDMap = new WeakHashMap<TopComponent, String>(30);
    private final Map<TopComponent, String> topComponentNonPersistent2IDMap = new WeakHashMap<TopComponent, String>(30);
    private Set<String> globalIDSet = new HashSet<String>(30);
    private Set<String> topComponentNonPersistentID = new HashSet<String>(30);
    private Set<String> topComponentPersistentOnlyOpenedID = new HashSet<String>(30);
    private final Map<String, Reference<TopComponent>> id2TopComponentMap = Collections.synchronizedMap(new HashMap(30));
    private final Map<String, Reference<TopComponent>> id2TopComponentNonPersistentMap = Collections.synchronizedMap(new HashMap(30));
    private final Map<DataObject, String> dataobjectToTopComponentMap = new WeakHashMap<DataObject, String>(30);
    private final Set<String> usedTcIds = new HashSet<String>(10);
    private final Object LOCK_IDS = new Object();
    private XMLReader parser;
    private static PersistenceManager defaultInstance;
    private String currentRole;
    private final Set<String> warnedIDs = Collections.synchronizedSet(new HashSet());
    private Map<Exception, String> failedCompsMap;

    private PersistenceManager() {
    }

    public static synchronized PersistenceManager getDefault() {
        if (defaultInstance == null) {
            defaultInstance = new PersistenceManager();
        }
        return defaultInstance;
    }

    public void reset() {
        this.rootModuleFolder = null;
        this.rootLocalFolder = null;
        this.compsModuleFolder = null;
        this.groupsModuleFolder = null;
        this.groupsLocalFolder = null;
        this.modesModuleFolder = null;
        this.modesLocalFolder = null;
        this.windowManagerParser = null;
        if (this.changeHandler != null) {
            this.changeHandler.stopHandling();
        }
        this.changeHandler = null;
    }

    public void clear() {
        this.reset();
        this.topComponent2IDMap.clear();
        this.topComponentNonPersistent2IDMap.clear();
        this.globalIDSet = new HashSet<String>(30);
        this.id2TopComponentMap.clear();
        this.id2TopComponentNonPersistentMap.clear();
        this.dataobjectToTopComponentMap.clear();
        this.usedTcIds.clear();
    }

    public void setRole(String newRole) {
        if (newRole == null ? this.currentRole == null : newRole.equals(this.currentRole)) {
            return;
        }
        this.currentRole = newRole;
        this.rootModuleFolder = null;
        this.rootLocalFolder = null;
        this.compsModuleFolder = null;
        this.groupsModuleFolder = null;
        this.groupsLocalFolder = null;
        this.modesModuleFolder = null;
        this.modesLocalFolder = null;
    }

    public String getRole() {
        return this.currentRole;
    }

    FileObject getRootModuleFolder() throws IOException {
        try {
            if (this.rootModuleFolder == null) {
                FileSystem fs = RoleFileSystem.create(this.currentRole);
                this.rootModuleFolder = FileUtil.createFolder((FileObject)fs.getRoot(), (String)"Windows2");
            }
            return this.rootModuleFolder;
        }
        catch (IOException exc) {
            String annotation = NbBundle.getMessage(PersistenceManager.class, (String)"EXC_RootFolder", (Object)"Windows2");
            Exceptions.attachLocalizedMessage((Throwable)exc, (String)annotation);
            throw exc;
        }
    }

    public FileObject getRootLocalFolder() throws IOException {
        try {
            if (this.rootLocalFolder == null) {
                String folderName = "Windows2Local";
                if (null != this.currentRole) {
                    folderName = folderName + "-" + this.currentRole;
                }
                this.rootLocalFolder = FileUtil.createFolder((FileObject)FileUtil.getConfigRoot(), (String)folderName);
            }
            return this.rootLocalFolder;
        }
        catch (IOException exc) {
            String annotation = NbBundle.getMessage(PersistenceManager.class, (String)"EXC_RootFolder", (Object)"Windows2Local");
            Exceptions.attachLocalizedMessage((Throwable)exc, (String)annotation);
            throw exc;
        }
    }

    void setRootModuleFolder(FileObject rootModuleFolder) {
        this.rootModuleFolder = rootModuleFolder;
    }

    void setRootLocalFolder(FileObject rootLocalFolder) {
        this.rootLocalFolder = rootLocalFolder;
    }

    public FileObject getComponentsModuleFolder() throws IOException {
        try {
            if (this.compsModuleFolder == null) {
                this.compsModuleFolder = FileUtil.createFolder((FileObject)this.getRootModuleFolder(), (String)"Components");
            }
            return this.compsModuleFolder;
        }
        catch (IOException exc) {
            String annotation = NbBundle.getMessage(PersistenceManager.class, (String)"EXC_CompsFolder", (Object)"Components");
            Exceptions.attachLocalizedMessage((Throwable)exc, (String)annotation);
            throw exc;
        }
    }

    public FileObject getComponentsLocalFolder() throws IOException {
        try {
            FileObject compsLocalFolder = FileUtil.createFolder((FileObject)this.getRootLocalFolder(), (String)"Components");
            return compsLocalFolder;
        }
        catch (IOException exc) {
            String annotation = NbBundle.getMessage(PersistenceManager.class, (String)"EXC_CompsFolder", (Object)"Components");
            Exceptions.attachLocalizedMessage((Throwable)exc, (String)annotation);
            throw exc;
        }
    }

    public FileObject getGroupsModuleFolder() throws IOException {
        try {
            if (this.groupsModuleFolder == null) {
                this.groupsModuleFolder = FileUtil.createFolder((FileObject)this.getRootModuleFolder(), (String)"Groups");
            }
            return this.groupsModuleFolder;
        }
        catch (IOException exc) {
            String annotation = NbBundle.getMessage(PersistenceManager.class, (String)"EXC_GroupsFolder", (Object)"Groups");
            Exceptions.attachLocalizedMessage((Throwable)exc, (String)annotation);
            throw exc;
        }
    }

    public FileObject getGroupsLocalFolder() throws IOException {
        try {
            if (this.groupsLocalFolder == null) {
                this.groupsLocalFolder = FileUtil.createFolder((FileObject)this.getRootLocalFolder(), (String)"Groups");
            }
            return this.groupsLocalFolder;
        }
        catch (IOException exc) {
            String annotation = NbBundle.getMessage(PersistenceManager.class, (String)"EXC_GroupsFolder", (Object)"Groups");
            Exceptions.attachLocalizedMessage((Throwable)exc, (String)annotation);
            throw exc;
        }
    }

    public FileObject getModesModuleFolder() throws IOException {
        try {
            if (this.modesModuleFolder == null) {
                this.modesModuleFolder = FileUtil.createFolder((FileObject)this.getRootModuleFolder(), (String)"Modes");
            }
            return this.modesModuleFolder;
        }
        catch (IOException exc) {
            String annotation = NbBundle.getMessage(PersistenceManager.class, (String)"EXC_ModesFolder", (Object)"Modes");
            Exceptions.attachLocalizedMessage((Throwable)exc, (String)annotation);
            throw exc;
        }
    }

    public FileObject getModesLocalFolder() throws IOException {
        try {
            if (this.modesLocalFolder == null) {
                this.modesLocalFolder = FileUtil.createFolder((FileObject)this.getRootLocalFolder(), (String)"Modes");
            }
            return this.modesLocalFolder;
        }
        catch (IOException exc) {
            String annotation = NbBundle.getMessage(PersistenceManager.class, (String)"EXC_ModesFolder", (Object)"Modes");
            Exceptions.attachLocalizedMessage((Throwable)exc, (String)annotation);
            throw exc;
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("cookie".equals(evt.getPropertyName())) {
            Object obj = evt.getSource();
            this.removeTopComponentForDataObject((DataObject)obj);
        }
    }

    private static int persistenceType(TopComponent tc) {
        return tc.getPersistenceType();
    }

    public static boolean isTopComponentPersistentWhenClosed(TopComponent tc) {
        int persistenceType = PersistenceManager.persistenceType(tc);
        if (persistenceType == 0) {
            return true;
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void removeTopComponentForDataObject(DataObject dob) {
        InstanceCookie ic = (InstanceCookie)dob.getCookie(InstanceCookie.class);
        if (ic == null) {
            Object object = this.LOCK_IDS;
            synchronized (object) {
                Reference<TopComponent> result;
                TopComponent tc;
                String tc_id = this.dataobjectToTopComponentMap.remove((Object)dob);
                if (tc_id != null && (result = this.id2TopComponentMap.remove(tc_id)) != null && (tc = result.get()) != null) {
                    this.topComponent2IDMap.remove((Object)tc);
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String getGlobalTopComponentID(TopComponent tc, String preferredID) {
        Object object = this.LOCK_IDS;
        synchronized (object) {
            String result = this.topComponent2IDMap.get((Object)tc);
            if (result != null) {
                return result;
            }
            result = this.topComponentNonPersistent2IDMap.get((Object)tc);
            if (result != null) {
                return result;
            }
        }
        if (this.isTopComponentProbablyPersistent(tc)) {
            try {
                return this.createTopComponentPersistentID(tc, preferredID);
            }
            catch (IOException exc) {
                LOG.log(Level.INFO, "[PersistenceManager.getGlobalTopComponentID]: Cannot create TC ID", exc);
                return this.createTopComponentNonPersistentID(tc, preferredID);
            }
        }
        return this.createTopComponentNonPersistentID(tc, preferredID);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void removeGlobalTopComponentID(String id) {
        Object object = this.LOCK_IDS;
        synchronized (object) {
            this.id2TopComponentMap.remove(id);
            this.id2TopComponentNonPersistentMap.remove(id);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private TopComponent getTopComponentPersistentForID(String stringId, boolean deserialize) {
        block23 : {
            Object object = this.LOCK_IDS;
            synchronized (object) {
                Reference<TopComponent> result = this.id2TopComponentMap.get(stringId);
                if (result != null) {
                    TopComponent tc = result.get();
                    if (tc != null) {
                        return tc;
                    }
                    this.id2TopComponentMap.remove(stringId);
                }
            }
            if (!deserialize) {
                return null;
            }
            FileNotFoundException resultExc = null;
            try {
                DataObject dob = PersistenceManager.findTopComponentDataObject(this.getComponentsLocalFolder(), stringId);
                if (dob == null) {
                    dob = PersistenceManager.findTopComponentDataObject(this.getComponentsModuleFolder(), stringId);
                }
                if (dob != null) {
                    InstanceCookie ic = (InstanceCookie)dob.getCookie(InstanceCookie.class);
                    if (ic == null && null != (dob = PersistenceManager.findTopComponentDataObject(this.getComponentsModuleFolder(), stringId)) && (ic = (InstanceCookie)dob.getCookie(InstanceCookie.class)) != null) {
                        LOG.log(this.warningLevelForDeserTC(stringId), "[PersistenceManager.getTopComponentForID] Problem when deserializing TopComponent for tcID:'" + stringId + "'. Reason: Broken .settings file in Windows2Local folder, falling back to module's original file.");
                    }
                    if (ic != null) {
                        TopComponent tc = (TopComponent)ic.instanceCreate();
                        Object object2 = this.LOCK_IDS;
                        synchronized (object2) {
                            this.topComponent2IDMap.put(tc, stringId);
                            this.id2TopComponentMap.put(stringId, new TopComponentReference(tc, stringId));
                            if (PersistenceManager.persistenceType(tc) == 1) {
                                this.topComponentPersistentOnlyOpenedID.add(stringId);
                            } else if (PersistenceManager.persistenceType(tc) == 2) {
                                this.topComponentNonPersistentID.add(stringId);
                            }
                            this.dataobjectToTopComponentMap.put(dob, stringId);
                        }
                        dob.addPropertyChangeListener((PropertyChangeListener)this);
                        return tc;
                    }
                    String excAnnotation = NbBundle.getMessage(PersistenceManager.class, (String)"EXC_BrokenTCSetting", (Object)stringId);
                    LOG.log(this.warningLevelForDeserTC(stringId), "[PersistenceManager.getTopComponentForID] Problem when deserializing TopComponent for tcID:'" + stringId + "'. Reason: " + excAnnotation);
                    break block23;
                }
                String excAnnotation = NbBundle.getMessage(PersistenceManager.class, (String)"EXC_FailedLocateTC", (Object)stringId);
                resultExc = new FileNotFoundException(excAnnotation);
                LOG.log(this.warningLevelForDeserTC(stringId), "[PersistenceManager.getTopComponentForID] Problem when deserializing TopComponent for tcID:'" + stringId + "'. Reason: " + excAnnotation);
            }
            catch (NoClassDefFoundError ndfe) {
                LOG.log(this.warningLevelForDeserTC(stringId), "[PersistenceManager.getTopComponentForID] Problem when deserializing TopComponent for tcID:'" + stringId + "'. Reason: " + ndfe.getMessage(), ndfe);
            }
            catch (InvalidObjectException ioe) {
                LOG.log(this.warningLevelForDeserTC(stringId), "[PersistenceManager.getTopComponentForID] Problem when deserializing TopComponent for tcID:'" + stringId + "'. Reason: " + ioe.getMessage(), ioe);
            }
            catch (DataObjectNotFoundException dnfe) {
                LOG.log(this.warningLevelForDeserTC(stringId), "[PersistenceManager.getTopComponentForID] Problem when deserializing TopComponent for tcID:'" + stringId + "'. Reason: " + " Object not found: " + dnfe.getMessage() + ". It was probably deleted.", (Throwable)dnfe);
            }
            catch (ClassNotFoundException exc) {
                LOG.log(this.warningLevelForDeserTC(stringId), "[PersistenceManager.getTopComponentForID] Problem when deserializing TopComponent for tcID:'" + stringId + "'. Reason: " + exc.getMessage(), exc);
            }
            catch (ClassCastException exc) {
                LOG.log(this.warningLevelForDeserTC(stringId), "[PersistenceManager.getTopComponentForID] Problem when deserializing TopComponent for tcID:'" + stringId + "'. Reason: " + exc.getMessage(), exc);
            }
            catch (IOException ioe) {
                LOG.log(this.warningLevelForDeserTC(stringId), "[PersistenceManager.getTopComponentForID] Problem when deserializing TopComponent for tcID:'" + stringId + "'. Reason: " + ioe.getMessage(), ioe);
            }
        }
        return null;
    }

    private Level warningLevelForDeserTC(String id) {
        return this.warnedIDs.add(id) ? Level.INFO : Level.FINE;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private TopComponent getTopComponentNonPersistentForID(String stringId) {
        Object object = this.LOCK_IDS;
        synchronized (object) {
            WeakReference result = (WeakReference)this.id2TopComponentNonPersistentMap.get(stringId);
            if (result != null) {
                TopComponent tc = (TopComponent)result.get();
                if (tc != null) {
                    return tc;
                }
                this.id2TopComponentNonPersistentMap.remove(stringId);
            }
            return null;
        }
    }

    public TopComponent getTopComponentForID(String stringId, boolean deserialize) {
        TopComponent tc = this.getTopComponentNonPersistentForID(stringId);
        if (tc == null) {
            return this.getTopComponentPersistentForID(stringId, deserialize);
        }
        return tc;
    }

    DataObject findTopComponentDataObject(String name) throws IOException {
        DataObject res = PersistenceManager.findTopComponentDataObject(this.getComponentsLocalFolder(), name);
        if (null == res) {
            res = PersistenceManager.findTopComponentDataObject(this.getComponentsLocalFolder(), name);
        }
        return res;
    }

    private static DataObject findTopComponentDataObject(FileObject folder, String name) throws IOException {
        FileObject fo = folder.getFileObject(name, "settings");
        if (fo == null) {
            fo = folder.getFileObject(name, "ser");
        }
        if (fo == null) {
            fo = folder.getFileObject(name, "xml");
        }
        if (fo != null) {
            return DataObject.find((FileObject)fo);
        }
        Enumeration e = folder.getChildren(false);
        while (e.hasMoreElements()) {
            fo = (FileObject)e.nextElement();
            DataObject dob = DataObject.find((FileObject)fo);
            if (!dob.getName().equals(name)) continue;
            return dob;
        }
        DataFolder dfolder = DataFolder.findFolder((FileObject)folder);
        e = dfolder.children();
        while (e.hasMoreElements()) {
            DataObject dob = (DataObject)e.nextElement();
            if (!dob.getName().equals(name)) continue;
            return dob;
        }
        return null;
    }

    private boolean isTopComponentProbablyPersistent(TopComponent tc) {
        int persistenceType = PersistenceManager.persistenceType(tc);
        if (2 == persistenceType) {
            return false;
        }
        return true;
    }

    public boolean isTopComponentPersistent(TopComponent tc) {
        int persistenceType = PersistenceManager.persistenceType(tc);
        if (2 == persistenceType || 1 == persistenceType && !tc.isOpened()) {
            return false;
        }
        return true;
    }

    public boolean isTopComponentNonPersistentForID(String stringId) {
        if (this.topComponentNonPersistentID.contains(stringId)) {
            return true;
        }
        return false;
    }

    public boolean isTopComponentPersistentOnlyOpenedForID(String stringId) {
        if (this.topComponentPersistentOnlyOpenedID.contains(stringId)) {
            return true;
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void saveTopComponents(WindowManagerConfig wmc) {
        HashMap<String, Reference<TopComponent>> copyIdToTopComponentMap;
        DataFolder compsFolder;
        try {
            compsFolder = DataFolder.findFolder((FileObject)this.getComponentsLocalFolder());
        }
        catch (IOException exc) {
            LOG.log(Level.INFO, "[PersistenceManager.saveTopComponents] Cannot get components folder", exc);
            return;
        }
        Object object = this.LOCK_IDS;
        synchronized (object) {
            copyIdToTopComponentMap = new HashMap<String, Reference<TopComponent>>(this.id2TopComponentMap);
        }
        for (Map.Entry<String, Reference<TopComponent>> curEntry : copyIdToTopComponentMap.entrySet()) {
            String annotation;
            String id;
            String id2;
            TopComponent curTC = curEntry.getValue().get();
            if (curTC == null || !curTC.isOpened() && !PersistenceManager.isTopComponentPersistentWhenClosed(curTC)) continue;
            try {
                FileObject fo = compsFolder.getPrimaryFile().getFileObject(curEntry.getKey(), "settings");
                DataObject ido = null;
                if (fo != null) {
                    ido = DataObject.find((FileObject)fo);
                }
                if (ido == null) {
                    InstanceDataObject.create((DataFolder)compsFolder, (String)PersistenceManager.unescape(curEntry.getKey()), (Object)curTC, (ModuleInfo)null);
                    continue;
                }
                SaveCookie sc = (SaveCookie)ido.getCookie(SaveCookie.class);
                if (sc != null) {
                    sc.save();
                    continue;
                }
                ido.delete();
                InstanceDataObject.create((DataFolder)compsFolder, (String)PersistenceManager.unescape(curEntry.getKey()), (Object)curTC, (ModuleInfo)null);
            }
            catch (NotSerializableException nse) {
                id = this.topComponent2IDMap.get((Object)curTC);
                LOG.log(Level.INFO, "TopComponent " + id + " is not serializable.", nse);
                this.removeTCFromConfig(wmc, id);
            }
            catch (IOException exc) {
                LOG.log(Level.INFO, null, exc);
                id = this.topComponent2IDMap.get((Object)curTC);
                this.removeTCFromConfig(wmc, id);
            }
            catch (RuntimeException exc) {
                annotation = NbBundle.getMessage(PersistenceManager.class, (String)"EXC_CannotSaveTCSettings", (Object)curTC.getName());
                Exceptions.attachLocalizedMessage((Throwable)exc, (String)annotation);
                LOG.log(Level.INFO, null, exc);
                id2 = this.topComponent2IDMap.get((Object)curTC);
                this.removeTCFromConfig(wmc, id2);
            }
            catch (LinkageError le) {
                annotation = NbBundle.getMessage(PersistenceManager.class, (String)"EXC_CannotSaveTCSettings", (Object)curTC.getName());
                Exceptions.attachLocalizedMessage((Throwable)le, (String)annotation);
                LOG.log(Level.INFO, null, le);
                id2 = this.topComponent2IDMap.get((Object)curTC);
                this.removeTCFromConfig(wmc, id2);
            }
        }
    }

    private static FileObject findTopComponentRefFile(FileObject folder, String tcId) {
        FileObject result = folder.getFileObject(tcId, "wstcref");
        if (result != null) {
            return result;
        }
        for (FileObject child : folder.getChildren()) {
            if (!child.isFolder() || (result = PersistenceManager.findTopComponentRefFile(child, tcId)) == null) continue;
            return result;
        }
        return null;
    }

    private static String escape(String name) {
        try {
            Method escape = InstanceDataObject.class.getDeclaredMethod("escapeAndCut", String.class);
            escape.setAccessible(true);
            return (String)escape.invoke(null, name);
        }
        catch (Exception ex) {
            LOG.log(Level.INFO, "Escape support failed", ex);
            return name;
        }
    }

    private static String unescape(String name) {
        try {
            Method unescape = InstanceDataObject.class.getDeclaredMethod("unescape", String.class);
            unescape.setAccessible(true);
            return (String)unescape.invoke(null, name);
        }
        catch (Exception ex) {
            LOG.log(Level.INFO, "Escape support failed", ex);
            return name;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private String createTopComponentNonPersistentID(TopComponent tc, String preferredID) {
        String compName;
        String string = compName = preferredID != null ? preferredID : null;
        if (compName == null || compName.length() == 0) {
            compName = "untitled_tc";
        }
        boolean isUsed = true;
        String srcName = compName = PersistenceManager.escape(compName);
        int i = 1;
        Object object = this.LOCK_IDS;
        synchronized (object) {
            while (isUsed) {
                isUsed = false;
                if (!this.globalIDSet.contains(srcName.toUpperCase(Locale.ENGLISH))) continue;
                isUsed = true;
                srcName = compName + "_" + i;
                ++i;
            }
            this.topComponentNonPersistent2IDMap.put(tc, srcName);
            this.id2TopComponentNonPersistentMap.put(srcName, new WeakReference<TopComponent>(tc));
            this.globalIDSet.add(srcName.toUpperCase(Locale.ENGLISH));
            this.topComponentNonPersistentID.add(srcName);
        }
        return srcName;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private String createTopComponentPersistentID(TopComponent tc, String preferredID) throws IOException {
        String compName;
        String string = compName = preferredID != null ? preferredID : null;
        if (compName == null || compName.length() == 0) {
            compName = "untitled_tc";
        }
        boolean isUsed = true;
        String origName = compName;
        String srcName = compName = PersistenceManager.escape(compName);
        int i = 1;
        Object object = this.LOCK_IDS;
        synchronized (object) {
            while (isUsed) {
                isUsed = false;
                String uniqueName = FileUtil.findFreeFileName((FileObject)this.getComponentsLocalFolder(), (String)srcName, (String)"settings");
                if (srcName.equals(uniqueName) && !this.globalIDSet.contains(uniqueName.toUpperCase(Locale.ENGLISH))) continue;
                isUsed = true;
                srcName = PersistenceManager.escape(origName + "_" + i);
                ++i;
            }
            this.topComponent2IDMap.put(tc, srcName);
            this.id2TopComponentMap.put(srcName, new TopComponentReference(tc, srcName));
            this.globalIDSet.add(srcName.toUpperCase(Locale.ENGLISH));
            if (PersistenceManager.persistenceType(tc) == 1) {
                this.topComponentPersistentOnlyOpenedID.add(srcName);
            }
        }
        return srcName;
    }

    public void annotatePersistenceError(Exception exc, String tcName) {
        if (this.failedCompsMap == null) {
            this.failedCompsMap = new HashMap<Exception, String>();
        }
        this.failedCompsMap.put(exc, tcName);
    }

    public void checkPersistenceErrors(boolean reading) {
        if (this.failedCompsMap == null || this.failedCompsMap.isEmpty()) {
            return;
        }
        for (Exception e : this.failedCompsMap.keySet()) {
            String name = this.failedCompsMap.get(e);
            String message = NbBundle.getMessage(PersistenceManager.class, (String)(reading ? "FMT_TCReadError" : "FMT_TCWriteError"), (Object)name);
            Exceptions.attachLocalizedMessage((Throwable)e, (String)message);
            LOG.log(Level.INFO, null, e);
        }
        this.failedCompsMap = null;
    }

    public WindowManagerParser getWindowManagerParser() {
        if (this.windowManagerParser == null) {
            this.windowManagerParser = new WindowManagerParser(this, "WindowManager");
        }
        return this.windowManagerParser;
    }

    public XMLReader getXMLParser(DefaultHandler h) throws SAXException {
        if (this.parser == null) {
            this.parser = XMLUtil.createXMLReader();
            this.parser.setEntityResolver(new EntityResolver(){

                @Override
                public InputSource resolveEntity(String publicId, String systemId) throws SAXException {
                    if ("-//NetBeans//DTD Mode Properties 1.0//EN".equals(publicId) || "-//NetBeans//DTD Mode Properties 1.1//EN".equals(publicId) || "-//NetBeans//DTD Mode Properties 1.2//EN".equals(publicId) || "-//NetBeans//DTD Mode Properties 2.0//EN".equals(publicId) || "-//NetBeans//DTD Mode Properties 2.1//EN".equals(publicId) || "-//NetBeans//DTD Mode Properties 2.2//EN".equals(publicId) || "-//NetBeans//DTD Mode Properties 2.3//EN".equals(publicId) || "-//NetBeans//DTD Group Properties 2.0//EN".equals(publicId) || "-//NetBeans//DTD Top Component in Group Properties 2.0//EN".equals(publicId) || "-//NetBeans//DTD Top Component in Mode Properties 1.0//EN".equals(publicId) || "-//NetBeans//DTD Top Component in Mode Properties 2.0//EN".equals(publicId) || "-//NetBeans//DTD Top Component in Mode Properties 2.1//EN".equals(publicId) || "-//NetBeans//DTD Top Component in Mode Properties 2.2//EN".equals(publicId) || "-//NetBeans//DTD Window Manager Properties 1.0//EN".equals(publicId) || "-//NetBeans//DTD Window Manager Properties 1.1//EN".equals(publicId) || "-//NetBeans//DTD Window Manager Properties 2.0//EN".equals(publicId) || "-//NetBeans//DTD Window Manager Properties 2.1//EN".equals(publicId)) {
                        ByteArrayInputStream is = new ByteArrayInputStream(new byte[0]);
                        return new InputSource(is);
                    }
                    return null;
                }
            });
        }
        this.parser.setContentHandler(h);
        this.parser.setErrorHandler(h);
        return this.parser;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addUsedTCId(String tc_id) {
        Object object = this.LOCK_IDS;
        synchronized (object) {
            this.usedTcIds.add(tc_id);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeUsedTCId(String tc_id) {
        Object object = this.LOCK_IDS;
        synchronized (object) {
            this.usedTcIds.remove(tc_id);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isUsedTCId(String tc_id) {
        Object object = this.LOCK_IDS;
        synchronized (object) {
            return this.usedTcIds.contains(tc_id);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void checkUsedTCId() throws IOException {
        for (FileObject file : this.getComponentsLocalFolder().getChildren()) {
            if (file.isFolder() || !"settings".equals(file.getExt())) continue;
            String tc_id = file.getName();
            Object object = this.LOCK_IDS;
            synchronized (object) {
                boolean contains = this.usedTcIds.contains(tc_id);
                if (!contains) {
                    PersistenceManager.deleteOneFO(file);
                } else {
                    this.globalIDSet.add(tc_id.toUpperCase(Locale.ENGLISH));
                }
                continue;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public WindowManagerConfig loadWindowSystem() throws IOException {
        Object object = this.LOCK_IDS;
        synchronized (object) {
            this.usedTcIds.clear();
        }
        this.copySettingsFiles();
        WindowManagerParser wmParser = this.getWindowManagerParser();
        WindowManagerConfig wmc = wmParser.load();
        this.checkUsedTCId();
        if (this.changeHandler == null) {
            this.changeHandler = new ModuleChangeHandler();
            this.changeHandler.startHandling();
        }
        this.parser = null;
        return wmc;
    }

    public void saveWindowSystem(WindowManagerConfig wmc) {
        WindowManagerParser wmParser = this.getWindowManagerParser();
        try {
            this.saveTopComponents(wmc);
            wmParser.save(wmc);
        }
        catch (IOException exc) {
            LOG.log(Level.INFO, null, exc);
        }
    }

    private void removeTCFromConfig(WindowManagerConfig wmc, String id) {
        int i;
        int j;
        int j2;
        boolean removeFromRecent = false;
        for (i = 0; i < wmc.tcIdViewList.length; ++i) {
            if (!id.equals(wmc.tcIdViewList[i])) continue;
            removeFromRecent = true;
            break;
        }
        if (removeFromRecent) {
            ArrayList<String> l = new ArrayList<String>(wmc.tcIdViewList.length);
            for (int i2 = 0; i2 < wmc.tcIdViewList.length; ++i2) {
                if (id.equals(wmc.tcIdViewList[i2])) continue;
                l.add(wmc.tcIdViewList[i2]);
            }
            wmc.tcIdViewList = l.toArray(new String[l.size()]);
        }
        for (i = 0; i < wmc.modes.length; ++i) {
            ModeConfig mc = wmc.modes[i];
            if (id.equals(mc.selectedTopComponentID)) {
                mc.selectedTopComponentID = "";
            }
            if (id.equals(mc.previousSelectedTopComponentID)) {
                mc.previousSelectedTopComponentID = "";
            }
            boolean removeFromMode = false;
            for (j = 0; j < mc.tcRefConfigs.length; ++j) {
                if (!id.equals(mc.tcRefConfigs[j].tc_id)) continue;
                removeFromMode = true;
                break;
            }
            if (!removeFromMode) continue;
            ArrayList<TCRefConfig> l = new ArrayList<TCRefConfig>(mc.tcRefConfigs.length);
            for (j2 = 0; j2 < mc.tcRefConfigs.length; ++j2) {
                if (id.equals(mc.tcRefConfigs[j2].tc_id)) continue;
                l.add(mc.tcRefConfigs[j2]);
            }
            mc.tcRefConfigs = l.toArray(new TCRefConfig[l.size()]);
        }
        for (i = 0; i < wmc.groups.length; ++i) {
            GroupConfig gc = wmc.groups[i];
            boolean removeFromGroup = false;
            for (j = 0; j < gc.tcGroupConfigs.length; ++j) {
                if (!id.equals(gc.tcGroupConfigs[j].tc_id)) continue;
                removeFromGroup = true;
                break;
            }
            if (!removeFromGroup) continue;
            ArrayList<TCGroupConfig> l = new ArrayList<TCGroupConfig>(gc.tcGroupConfigs.length);
            for (j2 = 0; j2 < gc.tcGroupConfigs.length; ++j2) {
                if (id.equals(gc.tcGroupConfigs[j2].tc_id)) continue;
                l.add(gc.tcGroupConfigs[j2]);
            }
            gc.tcGroupConfigs = l.toArray(new TCGroupConfig[l.size()]);
        }
    }

    private void copySettingsFiles() throws IOException {
        if (DEBUG) {
            Debug.log(PersistenceManager.class, "copySettingsFiles ENTER");
        }
        HashSet<String> localSet = new HashSet<String>(100);
        FileObject[] filesLocal = this.getComponentsLocalFolder().getChildren();
        for (int i = 0; i < filesLocal.length; ++i) {
            if (filesLocal[i].isFolder() || !"settings".equals(filesLocal[i].getExt())) continue;
            localSet.add(filesLocal[i].getName());
        }
        FileObject[] filesModule = this.getComponentsModuleFolder().getChildren();
        for (int i2 = 0; i2 < filesModule.length; ++i2) {
            if (filesModule[i2].isFolder() || !"settings".equals(filesModule[i2].getExt()) || localSet.contains(filesModule[i2].getName())) continue;
            this.copySettingsFile(filesModule[i2]);
        }
        if (DEBUG) {
            Debug.log(PersistenceManager.class, "copySettingsFiles LEAVE");
        }
    }

    private void copySettingsFile(FileObject fo) throws IOException {
        if (DEBUG) {
            Debug.log(PersistenceManager.class, "copySettingsFile fo:" + (Object)fo);
        }
        FileObject destFolder = this.getComponentsLocalFolder();
        try {
            fo.copy(destFolder, fo.getName(), fo.getExt());
        }
        catch (IOException exc) {
            String annotation = NbBundle.getMessage(PersistenceManager.class, (String)"EXC_CopyFails", (Object)destFolder);
            Exceptions.attachLocalizedMessage((Throwable)exc, (String)annotation);
            LOG.log(Level.INFO, null, exc);
        }
    }

    void copySettingsFileIfNeeded(FileObject fo) throws IOException {
        FileObject localSettingsFO = this.getComponentsLocalFolder().getFileObject(fo.getNameExt());
        if (localSettingsFO == null) {
            this.copySettingsFile(fo);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void deleteOneFO(FileObject fo) {
        FileLock lock = null;
        if (fo.isValid()) {
            try {
                lock = fo.lock();
                fo.delete(lock);
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            finally {
                if (lock != null) {
                    lock.releaseLock();
                }
            }
        }
    }

    @Deprecated
    static final ModuleInfo findModule(String codeNameBase, String strRelease, String strSpec) {
        SpecificationVersion spec = null;
        int release = -1;
        if (strRelease != null) {
            try {
                release = Integer.parseInt(strRelease);
            }
            catch (NumberFormatException nfe) {
                LOG.log(Level.INFO, null, nfe);
            }
        }
        if (strSpec != null) {
            spec = new SpecificationVersion(strSpec);
        }
        Lookup.Result modulesResult = Lookup.getDefault().lookup(new Lookup.Template(ModuleInfo.class));
        for (ModuleInfo curInfo : modulesResult.allInstances()) {
            if (!curInfo.getCodeNameBase().equals(codeNameBase)) continue;
            if (release < 0 && spec == null || curInfo.getCodeNameRelease() >= release) {
                return curInfo;
            }
            if (release >= 0 && curInfo.getCodeNameRelease() != release) continue;
            if (spec == null) {
                return curInfo;
            }
            if (curInfo.getSpecificationVersion() == null || curInfo.getSpecificationVersion().compareTo((Object)spec) < 0) continue;
            return curInfo;
        }
        return null;
    }

    public static String escapeTcId4XmlContent(String tcName) {
        if (tcName.indexOf(38) != -1 || tcName.indexOf(39) != -1) {
            tcName = tcName.replaceAll("&", "&amp;");
            tcName = tcName.replaceAll("'", "&apos;");
        }
        return tcName;
    }

    private class TopComponentReference
    extends WeakReference<TopComponent>
    implements Runnable {
        private final String tcID;

        public TopComponentReference(TopComponent ref, String tcID) {
            super(ref, Utilities.activeReferenceQueue());
            this.tcID = tcID;
        }

        @Override
        public void run() {
            PersistenceManager.this.removeGlobalTopComponentID(this.tcID);
        }
    }

}

