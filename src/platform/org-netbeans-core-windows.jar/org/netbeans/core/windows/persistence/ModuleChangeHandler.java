/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.InstanceCookie
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.core.windows.persistence;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.Debug;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.persistence.GroupConfig;
import org.netbeans.core.windows.persistence.ModeConfig;
import org.netbeans.core.windows.persistence.PersistenceManager;
import org.netbeans.core.windows.persistence.PersistenceObserver;
import org.netbeans.core.windows.persistence.TCGroupConfig;
import org.netbeans.core.windows.persistence.TCRefConfig;
import org.netbeans.core.windows.persistence.WindowManagerParser;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.RequestProcessor;

class ModuleChangeHandler
implements FileChangeListener {
    private static final boolean DEBUG = Debug.isLoggable(ModuleChangeHandler.class);
    private boolean started = false;
    private FileSystem fs = null;
    private FileObject modesModuleFolder;
    private FileObject groupsModuleFolder;
    private Set<FileObject> modesModuleChildren = new HashSet<FileObject>();
    private Set<FileObject> groupsModuleChildren = new HashSet<FileObject>();
    private FileObject componentsModuleFolder;
    private List<FileObject> tcRefsWaitingOnSettings;
    private static final RequestProcessor RP = new RequestProcessor("WinSysModuleChangeHandler", 1);

    private void refreshModesFolder() {
        FileObject[] arr = this.modesModuleFolder.getChildren();
        this.modesModuleChildren.clear();
        for (FileObject fo : arr) {
            if (!fo.isFolder()) continue;
            this.modesModuleChildren.add(fo);
            fo.getChildren();
        }
    }

    private void refreshGroupsFolder() {
        FileObject[] arr = this.groupsModuleFolder.getChildren();
        this.groupsModuleChildren.clear();
        for (FileObject fo : arr) {
            if (!fo.isFolder()) continue;
            this.groupsModuleChildren.add(fo);
            fo.getChildren();
        }
    }

    void startHandling() {
        if (this.started) {
            return;
        }
        PersistenceManager pm = PersistenceManager.getDefault();
        try {
            this.modesModuleFolder = pm.getModesModuleFolder();
            this.refreshModesFolder();
            this.groupsModuleFolder = pm.getGroupsModuleFolder();
            this.refreshGroupsFolder();
            this.componentsModuleFolder = pm.getComponentsModuleFolder();
        }
        catch (IOException exc) {
            PersistenceManager.LOG.log(Level.WARNING, "[WinSys.ModuleChangeHandler.startHandling] Cannot get data folders.", exc);
            return;
        }
        try {
            this.fs = this.modesModuleFolder.getFileSystem();
        }
        catch (FileStateInvalidException exc) {
            PersistenceManager.LOG.log(Level.WARNING, "[WinSys.ModuleChangeHandler.startHandling] Cannot get filesystem.", (Throwable)exc);
            return;
        }
        this.fs.addFileChangeListener((FileChangeListener)this);
        this.started = true;
    }

    void stopHandling() {
        if (!this.started) {
            return;
        }
        this.fs.removeFileChangeListener((FileChangeListener)this);
        this.fs = null;
        this.started = false;
    }

    private boolean acceptEvent(FileObject fo) {
        FileObject parent = fo.getParent();
        if (parent == null) {
            return false;
        }
        if (parent.getPath().equals(this.modesModuleFolder.getPath())) {
            if (DEBUG) {
                Debug.log(ModuleChangeHandler.class, "++ MODE ++");
            }
            return true;
        }
        if (parent.getPath().equals(this.groupsModuleFolder.getPath())) {
            if (DEBUG) {
                Debug.log(ModuleChangeHandler.class, "++ GROUP ++");
            }
            return true;
        }
        if (parent.getPath().equals(this.componentsModuleFolder.getPath())) {
            if (DEBUG) {
                Debug.log(ModuleChangeHandler.class, "++ COMPONENT ++");
            }
            return true;
        }
        if ((parent = parent.getParent()) == null) {
            return false;
        }
        if (parent.getPath().equals(this.modesModuleFolder.getPath())) {
            if (DEBUG) {
                Debug.log(ModuleChangeHandler.class, "++ tcRef ++");
            }
            return true;
        }
        if (parent.getPath().equals(this.groupsModuleFolder.getPath())) {
            if (DEBUG) {
                Debug.log(ModuleChangeHandler.class, "++ tcGroup ++");
            }
            return true;
        }
        return false;
    }

    private boolean isInModesFolder(FileObject fo) {
        FileObject parent = fo.getParent();
        if (parent == null) {
            return false;
        }
        if (parent.getPath().equals(this.modesModuleFolder.getPath())) {
            if (DEBUG) {
                Debug.log(ModuleChangeHandler.class, "++ MODE ++");
            }
            return true;
        }
        return false;
    }

    private boolean isInGroupsFolder(FileObject fo) {
        FileObject parent = fo.getParent();
        if (parent == null) {
            return false;
        }
        if (parent.getPath().equals(this.groupsModuleFolder.getPath())) {
            if (DEBUG) {
                Debug.log(ModuleChangeHandler.class, "++ GROUP ++");
            }
            return true;
        }
        return false;
    }

    public void fileAttributeChanged(FileAttributeEvent fe) {
    }

    public void fileChanged(FileEvent fe) {
    }

    public void fileDataCreated(final FileEvent fe) {
        RP.post(new Runnable(){

            @Override
            public void run() {
                FileObject fo = fe.getFile();
                boolean accepted = ModuleChangeHandler.this.acceptEvent(fo);
                if (!accepted) {
                    return;
                }
                if (DEBUG) {
                    Debug.log(ModuleChangeHandler.class, "-- fileDataCreated fo: " + (Object)fo + " isFolder:" + fo.isFolder() + " ACCEPTED" + " th:" + Thread.currentThread().getName());
                    if (accepted && fo.isFolder()) {
                        FileObject[] files = fo.getChildren();
                        for (int i = 0; i < files.length; ++i) {
                            Debug.log(ModuleChangeHandler.class, "fo[" + i + "]: " + (Object)files[i]);
                        }
                    }
                }
                ModuleChangeHandler.this.processDataOrFolderCreated(fo);
            }
        });
    }

    public void fileFolderCreated(final FileEvent fe) {
        RP.post(new Runnable(){

            @Override
            public void run() {
                FileObject fo = fe.getFile();
                boolean accepted = ModuleChangeHandler.this.acceptEvent(fo);
                if (!accepted) {
                    return;
                }
                if (ModuleChangeHandler.this.isInModesFolder(fo)) {
                    ModuleChangeHandler.this.refreshModesFolder();
                }
                if (ModuleChangeHandler.this.isInGroupsFolder(fo)) {
                    ModuleChangeHandler.this.refreshGroupsFolder();
                }
                if (DEBUG) {
                    Debug.log(ModuleChangeHandler.class, "-- fileFolderCreated fo: " + (Object)fo + " isFolder:" + fo.isFolder() + " ACCEPTED" + " th:" + Thread.currentThread().getName());
                    if (accepted && fo.isFolder()) {
                        FileObject[] files = fo.getChildren();
                        for (int i = 0; i < files.length; ++i) {
                            Debug.log(ModuleChangeHandler.class, "fo[" + i + "]: " + (Object)files[i]);
                        }
                    }
                }
                ModuleChangeHandler.this.processDataOrFolderCreated(fo);
            }
        });
    }

    private void processDataOrFolderCreated(FileObject fo) {
        FileObject parent2;
        FileObject parent1 = fo.getParent();
        if (parent1.getPath().equals(this.modesModuleFolder.getPath())) {
            if (!fo.isFolder() && "wsmode".equals(fo.getExt())) {
                if (DEBUG) {
                    Debug.log(ModuleChangeHandler.class, "++ process MODE ADD ++");
                }
                this.addMode(fo.getName());
            }
        } else if (parent1.getPath().equals(this.groupsModuleFolder.getPath())) {
            if (!fo.isFolder() && "wsgrp".equals(fo.getExt())) {
                if (DEBUG) {
                    Debug.log(ModuleChangeHandler.class, "++ process GROUP ADD ++");
                }
                this.addGroup(fo.getName());
            }
        } else if (parent1.getPath().equals(this.componentsModuleFolder.getPath()) && !fo.isFolder() && "settings".equals(fo.getExt())) {
            if (DEBUG) {
                Debug.log(ModuleChangeHandler.class, "++ process COMPONENT ADD ++");
            }
            this.addComponent(fo);
        }
        if ((parent2 = parent1.getParent()).getPath().equals(this.modesModuleFolder.getPath())) {
            if (!fo.isFolder() && "wstcref".equals(fo.getExt())) {
                if (DEBUG) {
                    Debug.log(ModuleChangeHandler.class, "++ process tcRef ADD ++");
                }
                this.processTCRef(parent1.getName(), fo);
            }
        } else if (parent2.getPath().equals(this.groupsModuleFolder.getPath()) && !fo.isFolder() && "wstcgrp".equals(fo.getExt())) {
            if (DEBUG) {
                Debug.log(ModuleChangeHandler.class, "++ process tcGroup ADD ++");
            }
            this.addTCGroup(parent1.getName(), fo.getName());
        }
    }

    public void fileDeleted(final FileEvent fe) {
        RP.post(new Runnable(){

            @Override
            public void run() {
                FileObject parent2;
                FileObject parent1;
                FileObject fo = fe.getFile();
                boolean accepted = ModuleChangeHandler.this.acceptEvent(fo);
                if (!accepted) {
                    return;
                }
                if (ModuleChangeHandler.this.isInModesFolder(fo)) {
                    ModuleChangeHandler.this.refreshModesFolder();
                }
                if (ModuleChangeHandler.this.isInGroupsFolder(fo)) {
                    ModuleChangeHandler.this.refreshGroupsFolder();
                }
                if (DEBUG) {
                    Debug.log(ModuleChangeHandler.class, "-- fileDeleted fo: " + (Object)fo + " isFolder:" + fo.isFolder() + " isValid:" + fo.isValid() + " ACCEPTED" + " th:" + Thread.currentThread().getName());
                }
                if ((parent1 = fo.getParent()).getPath().equals(ModuleChangeHandler.this.modesModuleFolder.getPath())) {
                    if (!fo.isFolder() && "wsmode".equals(fo.getExt())) {
                        if (DEBUG) {
                            Debug.log(ModuleChangeHandler.class, "++ process MODE REMOVE ++");
                        }
                        ModuleChangeHandler.this.removeMode(fo.getName());
                    }
                } else if (parent1.getPath().equals(ModuleChangeHandler.this.groupsModuleFolder.getPath()) && !fo.isFolder() && "wsgrp".equals(fo.getExt())) {
                    if (DEBUG) {
                        Debug.log(ModuleChangeHandler.class, "++ process GROUP REMOVE ++");
                    }
                    ModuleChangeHandler.this.removeGroup(fo.getName());
                }
                if ((parent2 = parent1.getParent()).getPath().equals(ModuleChangeHandler.this.modesModuleFolder.getPath())) {
                    if (!fo.isFolder() && "wstcref".equals(fo.getExt())) {
                        if (DEBUG) {
                            Debug.log(ModuleChangeHandler.class, "++ process tcRef REMOVE ++");
                        }
                        ModuleChangeHandler.this.removeTCRef(fo.getName());
                    }
                } else if (parent2.getPath().equals(ModuleChangeHandler.this.groupsModuleFolder.getPath()) && !fo.isFolder() && "wstcgrp".equals(fo.getExt())) {
                    if (DEBUG) {
                        Debug.log(ModuleChangeHandler.class, "++ process tcGroup REMOVE ++");
                    }
                    ModuleChangeHandler.this.removeTCGroup(parent1.getName(), fo.getName());
                }
            }
        });
    }

    public void fileRenamed(FileRenameEvent fe) {
    }

    private void addMode(String modeName) {
        ModeConfig modeConfig;
        WindowManagerParser wmParser;
        if (DEBUG) {
            Debug.log(ModuleChangeHandler.class, "addMode mo:" + modeName);
        }
        if ((modeConfig = (wmParser = PersistenceManager.getDefault().getWindowManagerParser()).addMode(modeName)) != null) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    WindowManagerImpl.getInstance().getPersistenceObserver().modeConfigAdded(modeConfig);
                }
            });
        }
    }

    private void addGroup(String groupName) {
        WindowManagerParser wmParser;
        GroupConfig groupConfig;
        if (DEBUG) {
            Debug.log(ModuleChangeHandler.class, "addGroup group:" + groupName);
        }
        if ((groupConfig = (wmParser = PersistenceManager.getDefault().getWindowManagerParser()).addGroup(groupName)) != null) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    WindowManagerImpl.getInstance().getPersistenceObserver().groupConfigAdded(groupConfig);
                }
            });
        }
    }

    private void processTCRef(String modeName, FileObject tcRefFO) {
        FileObject compsFO = null;
        try {
            compsFO = PersistenceManager.getDefault().getComponentsLocalFolder();
        }
        catch (IOException exc) {
            PersistenceManager.LOG.log(Level.WARNING, "[WinSys.ModuleChangeHandler.processTCRef] Cannot get components folder.", exc);
            return;
        }
        FileObject localSettings = compsFO.getFileObject(tcRefFO.getName(), "settings");
        if (localSettings != null) {
            this.addTCRef(modeName, tcRefFO.getName());
        } else {
            if (this.tcRefsWaitingOnSettings == null) {
                this.tcRefsWaitingOnSettings = new ArrayList<FileObject>(5);
            }
            this.tcRefsWaitingOnSettings.add(tcRefFO);
        }
    }

    private void addTCRef(final String modeName, String tcRefName) {
        if (DEBUG) {
            Debug.log(ModuleChangeHandler.class, "addTCRef modeName:" + modeName + " tcRefName:" + tcRefName);
        }
        WindowManagerParser wmParser = PersistenceManager.getDefault().getWindowManagerParser();
        ArrayList<String> tcRefNameList = new ArrayList<String>(10);
        final TCRefConfig tcRefConfig = wmParser.addTCRef(modeName, tcRefName, tcRefNameList);
        try {
            DataObject dob;
            if (null != tcRefConfig && null != (dob = PersistenceManager.getDefault().findTopComponentDataObject(tcRefConfig.tc_id))) {
                dob.getCookie(InstanceCookie.class);
            }
        }
        catch (IOException ioE) {
            Logger.getLogger(ModuleChangeHandler.class.getName()).log(Level.FINER, null, ioE);
        }
        if (tcRefConfig != null) {
            final String[] tcRefNameArray = tcRefNameList.toArray(new String[tcRefNameList.size()]);
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    WindowManagerImpl.getInstance().getPersistenceObserver().topComponentRefConfigAdded(modeName, tcRefConfig, tcRefNameArray);
                }
            });
        }
    }

    private void addTCGroup(final String groupName, String tcGroupName) {
        WindowManagerParser wmParser;
        TCGroupConfig tcGroupConfig;
        if (DEBUG) {
            Debug.log(ModuleChangeHandler.class, "addTCGroup groupName:" + groupName + " tcGroupName:" + tcGroupName);
        }
        if ((tcGroupConfig = (wmParser = PersistenceManager.getDefault().getWindowManagerParser()).addTCGroup(groupName, tcGroupName)) != null) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    WindowManagerImpl.getInstance().getPersistenceObserver().topComponentGroupConfigAdded(groupName, tcGroupConfig);
                }
            });
        }
    }

    private void addComponent(FileObject fo) {
        if (DEBUG) {
            Debug.log(ModuleChangeHandler.class, "addComponent settingsName:" + fo.getNameExt());
        }
        try {
            PersistenceManager.getDefault().copySettingsFileIfNeeded(fo);
        }
        catch (IOException exc) {
            PersistenceManager.LOG.log(Level.WARNING, "[WinSys.ModuleChangeHandler.addComponent] Cannot copy settings files.", exc);
            return;
        }
        FileObject waitingTcRef = this.findWaitingTcRef(fo);
        if (waitingTcRef != null) {
            this.tcRefsWaitingOnSettings.remove((Object)waitingTcRef);
            this.addTCRef(waitingTcRef.getParent().getName(), waitingTcRef.getName());
        }
    }

    private FileObject findWaitingTcRef(FileObject settingsFo) {
        if (this.tcRefsWaitingOnSettings == null) {
            return null;
        }
        String settingsName = settingsFo.getName();
        for (FileObject curTcRef : this.tcRefsWaitingOnSettings) {
            if (!settingsName.equals(curTcRef.getName())) continue;
            return curTcRef;
        }
        return null;
    }

    private void removeMode(String modeName) {
        if (DEBUG) {
            Debug.log(ModuleChangeHandler.class, "removeMode mo:" + modeName);
        }
        WindowManagerParser wmParser = PersistenceManager.getDefault().getWindowManagerParser();
        wmParser.removeMode(modeName);
    }

    private void removeGroup(final String groupName) {
        if (DEBUG) {
            Debug.log(ModuleChangeHandler.class, "removeGroup group:" + groupName);
        }
        WindowManagerParser wmParser = PersistenceManager.getDefault().getWindowManagerParser();
        wmParser.removeGroup(groupName);
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                WindowManagerImpl.getInstance().getPersistenceObserver().groupConfigRemoved(groupName);
            }
        });
    }

    private void removeTCRef(final String tcRefName) {
        WindowManagerParser wmParser;
        if (DEBUG) {
            Debug.log(ModuleChangeHandler.class, "removeTCRef tcRefName:" + tcRefName);
        }
        if ((wmParser = PersistenceManager.getDefault().getWindowManagerParser()).removeTCRef(tcRefName)) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    WindowManagerImpl.getInstance().getPersistenceObserver().topComponentRefConfigRemoved(tcRefName);
                }
            });
        }
    }

    private void removeTCGroup(final String groupName, final String tcGroupName) {
        WindowManagerParser wmParser;
        if (DEBUG) {
            Debug.log(ModuleChangeHandler.class, "removeTCGroup groupName:" + groupName + " tcGroupName:" + tcGroupName);
        }
        if ((wmParser = PersistenceManager.getDefault().getWindowManagerParser()).removeTCGroup(groupName, tcGroupName)) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    WindowManagerImpl.getInstance().getPersistenceObserver().topComponentGroupConfigRemoved(groupName, tcGroupName);
                }
            });
        }
    }

    private void log(String s) {
        Debug.log(ModuleChangeHandler.class, s);
    }

}

