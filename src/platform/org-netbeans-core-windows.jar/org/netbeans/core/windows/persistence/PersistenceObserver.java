/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.persistence;

import org.netbeans.core.windows.persistence.GroupConfig;
import org.netbeans.core.windows.persistence.ModeConfig;
import org.netbeans.core.windows.persistence.TCGroupConfig;
import org.netbeans.core.windows.persistence.TCRefConfig;

public interface PersistenceObserver {
    public void modeConfigAdded(ModeConfig var1);

    public void modeConfigRemoved(String var1);

    public void topComponentRefConfigAdded(String var1, TCRefConfig var2, String[] var3);

    public void topComponentRefConfigRemoved(String var1);

    public void groupConfigAdded(GroupConfig var1);

    public void groupConfigRemoved(String var1);

    public void topComponentGroupConfigAdded(String var1, TCGroupConfig var2);

    public void topComponentGroupConfigRemoved(String var1, String var2);
}

