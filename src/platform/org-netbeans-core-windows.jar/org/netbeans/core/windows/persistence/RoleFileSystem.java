/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.MultiFileSystem
 */
package org.netbeans.core.windows.persistence;

import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.MultiFileSystem;

public class RoleFileSystem
extends MultiFileSystem {
    private final String roleName;

    public static FileSystem create(String roleName) throws FileStateInvalidException {
        if (null == roleName) {
            return FileUtil.getConfigRoot().getFileSystem();
        }
        RoleFileSystem rfs = new RoleFileSystem(roleName);
        return new MultiFileSystem(new FileSystem[]{rfs, FileUtil.getConfigRoot().getFileSystem()});
    }

    private RoleFileSystem(String roleName) throws FileStateInvalidException {
        super(new FileSystem[]{FileUtil.getConfigRoot().getFileSystem()});
        this.roleName = roleName;
    }

    protected FileObject findResourceOn(FileSystem fs, String res) {
        return super.findResourceOn(fs, this.convert(res));
    }

    String convert(String path) {
        if (path.startsWith("Windows2/")) {
            return "Windows2/Roles/" + this.roleName + path.substring("Windows2".length());
        }
        return path;
    }
}

