/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.persistence;

public class TCGroupConfig {
    public String tc_id = "";
    public boolean open;
    public boolean close;
    public boolean wasOpened;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof TCGroupConfig) {
            TCGroupConfig tcGroupCfg = (TCGroupConfig)obj;
            return this.tc_id.equals(tcGroupCfg.tc_id) && this.open == tcGroupCfg.open && this.close == tcGroupCfg.close && this.wasOpened == tcGroupCfg.wasOpened;
        }
        return false;
    }

    public int hashCode() {
        int hash = 17;
        hash = 37 * hash + this.tc_id.hashCode();
        hash = 37 * hash + (this.open ? 0 : 1);
        hash = 37 * hash + (this.close ? 0 : 1);
        hash = 37 * hash + (this.wasOpened ? 0 : 1);
        return hash;
    }
}

