/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.persistence;

public class TCRefConfig {
    public String tc_id = "";
    public boolean opened;
    public String previousMode;
    public int previousIndex = -1;
    public boolean dockedInMaximizedMode = false;
    public boolean dockedInDefaultMode = true;
    public boolean slidedInMaximized = false;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof TCRefConfig) {
            TCRefConfig tcRefCfg = (TCRefConfig)obj;
            return this.tc_id.equals(tcRefCfg.tc_id) && this.opened == tcRefCfg.opened && this.dockedInMaximizedMode == tcRefCfg.dockedInMaximizedMode && this.dockedInDefaultMode == tcRefCfg.dockedInDefaultMode && this.slidedInMaximized == tcRefCfg.slidedInMaximized && this.previousIndex == tcRefCfg.previousIndex;
        }
        return false;
    }

    public int hashCode() {
        int hash = 17;
        hash = 37 * hash + this.tc_id.hashCode();
        hash = 37 * hash + (this.opened ? 0 : 1);
        hash = 37 * hash + (this.dockedInMaximizedMode ? 0 : 1);
        hash = 37 * hash + (this.dockedInDefaultMode ? 0 : 1);
        hash = 37 * hash + (this.slidedInMaximized ? 0 : 1);
        hash = 37 * hash + this.previousIndex;
        return hash;
    }

    public String toString() {
        return "TCRefConfig: tc_id=" + this.tc_id + ", opened=" + this.opened + ", maximizedMode=" + this.dockedInMaximizedMode + ", defaultMode=" + this.dockedInDefaultMode + ", slidedInMaximized=" + this.slidedInMaximized + ", previousMode=" + this.previousMode;
    }
}

