/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.SpecificationVersion
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.windows.persistence;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.windows.Debug;
import org.netbeans.core.windows.SplitConstraint;
import org.netbeans.core.windows.persistence.GroupConfig;
import org.netbeans.core.windows.persistence.GroupParser;
import org.netbeans.core.windows.persistence.InternalConfig;
import org.netbeans.core.windows.persistence.ModeConfig;
import org.netbeans.core.windows.persistence.ModeParser;
import org.netbeans.core.windows.persistence.PersistenceManager;
import org.netbeans.core.windows.persistence.TCGroupConfig;
import org.netbeans.core.windows.persistence.TCRefConfig;
import org.netbeans.core.windows.persistence.TCRefParser;
import org.netbeans.core.windows.persistence.WindowManagerConfig;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.modules.ModuleInfo;
import org.openide.modules.SpecificationVersion;
import org.openide.util.NbBundle;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class WindowManagerParser {
    public static final String INSTANCE_DTD_ID_1_0 = "-//NetBeans//DTD Window Manager Properties 1.0//EN";
    public static final String INSTANCE_DTD_ID_1_1 = "-//NetBeans//DTD Window Manager Properties 1.1//EN";
    public static final String INSTANCE_DTD_ID_2_0 = "-//NetBeans//DTD Window Manager Properties 2.0//EN";
    public static final String INSTANCE_DTD_ID_2_1 = "-//NetBeans//DTD Window Manager Properties 2.1//EN";
    private static final boolean DEBUG = Debug.isLoggable(WindowManagerParser.class);
    private String wmName;
    private PersistenceManager pm;
    private InternalConfig internalConfig;
    private Map<String, ModeParser> modeParserMap = new HashMap<String, ModeParser>(19);
    private Map<String, GroupParser> groupParserMap = new HashMap<String, GroupParser>(19);
    private Set<String> tcRefNameLocalSet = new HashSet<String>(101);
    private static final Object SAVING_LOCK = new Object();
    private static final String[] floatStrings = new String[]{"0", "0.0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1", "1.0"};
    private static final float[] floatVals = new float[]{0.0f, 0.0f, 0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f, 0.9f, 1.0f, 1.0f};

    public WindowManagerParser(PersistenceManager pm, String wmName) {
        this.pm = pm;
        this.wmName = wmName;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    WindowManagerConfig load() throws IOException {
        Object object = SAVING_LOCK;
        synchronized (object) {
            WindowManagerConfig wmc = new WindowManagerConfig();
            this.readProperties(wmc);
            this.readModes(wmc);
            this.readGroups(wmc);
            return wmc;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void save(WindowManagerConfig wmc) throws IOException {
        Object object = SAVING_LOCK;
        synchronized (object) {
            this.writeProperties(wmc);
            this.writeModes(wmc);
            this.writeGroups(wmc);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void removeMode(String modeName) {
        Object object = SAVING_LOCK;
        synchronized (object) {
            ModeParser modeParser;
            if (DEBUG) {
                Debug.log(WindowManagerParser.class, "removeMode mo:" + modeName);
            }
            if ((modeParser = this.modeParserMap.get(modeName)) != null) {
                modeParser.setInModuleFolder(false);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    ModeConfig addMode(String modeName) {
        Object object = SAVING_LOCK;
        synchronized (object) {
            ModeParser modeParser;
            if (DEBUG) {
                Debug.log(WindowManagerParser.class, "addMode ENTER mo:" + modeName);
            }
            if ((modeParser = this.modeParserMap.get(modeName)) == null) {
                modeParser = new ModeParser(modeName, this.tcRefNameLocalSet);
                this.modeParserMap.put(modeName, modeParser);
            }
            FileObject modesModuleFolder = null;
            try {
                modesModuleFolder = this.pm.getModesModuleFolder();
            }
            catch (IOException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.addMode] Cannot get modes folder", exc);
                return null;
            }
            modeParser.setModuleParentFolder(modesModuleFolder);
            modeParser.setInModuleFolder(true);
            ModeConfig modeConfig = null;
            try {
                modeConfig = modeParser.load();
            }
            catch (IOException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.addMode] Warning: Cannot load mode " + modeName, exc);
            }
            return modeConfig;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void removeGroup(String groupName) {
        Object object = SAVING_LOCK;
        synchronized (object) {
            if (DEBUG) {
                Debug.log(WindowManagerParser.class, "WMParser.removeGroup group:" + groupName);
            }
            this.groupParserMap.remove(groupName);
            this.deleteLocalGroup(groupName);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    GroupConfig addGroup(String groupName) {
        Object object = SAVING_LOCK;
        synchronized (object) {
            GroupParser groupParser;
            if (DEBUG) {
                Debug.log(WindowManagerParser.class, "WMParser.addGroup ENTER group:" + groupName);
            }
            if ((groupParser = this.groupParserMap.get(groupName)) != null) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.addGroup] Warning: GroupParser " + groupName + " exists but it should not.");
                this.groupParserMap.remove(groupName);
            }
            groupParser = new GroupParser(groupName);
            FileObject groupsModuleFolder = null;
            try {
                groupsModuleFolder = this.pm.getGroupsModuleFolder();
            }
            catch (IOException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.addGroup] Cannot get groups folder", exc);
                return null;
            }
            groupParser.setModuleParentFolder(groupsModuleFolder);
            groupParser.setInModuleFolder(true);
            this.groupParserMap.put(groupName, groupParser);
            GroupConfig groupConfig = null;
            try {
                groupConfig = groupParser.load();
            }
            catch (IOException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.addGroup] Warning: Cannot load group " + groupName, exc);
            }
            return groupConfig;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean removeTCRef(String tcRefName) {
        Object object = SAVING_LOCK;
        synchronized (object) {
            ModeParser modeParser;
            if (DEBUG) {
                Debug.log(WindowManagerParser.class, "removeTCRef ENTER tcRef:" + tcRefName);
            }
            if ((modeParser = this.findModeParser(tcRefName)) == null) {
                if (DEBUG) {
                    Debug.log(WindowManagerParser.class, "removeTCRef LEAVE 1 tcRef:" + tcRefName);
                }
                return false;
            }
            if (DEBUG) {
                Debug.log(WindowManagerParser.class, "removeTCRef REMOVING tcRef:" + tcRefName + " FROM mo:" + modeParser.getName());
            }
            modeParser.removeTCRef(tcRefName);
            if (DEBUG) {
                Debug.log(WindowManagerParser.class, "removeTCRef LEAVE 2 tcRef:" + tcRefName);
            }
            return true;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    TCRefConfig addTCRef(String modeName, String tcRefName, List<String> tcRefNameList) {
        Object object = SAVING_LOCK;
        synchronized (object) {
            ModeParser modeParser;
            if (DEBUG) {
                Debug.log(WindowManagerParser.class, "WMParser.addTCRef ENTER mo:" + modeName + " tcRef:" + tcRefName);
            }
            if ((modeParser = this.modeParserMap.get(modeName)) == null) {
                if (DEBUG) {
                    Debug.log(WindowManagerParser.class, "WMParser.addTCRef LEAVE 1 mo:" + modeName + " tcRef:" + tcRefName);
                }
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.addTCRef] Warning: Cannot add tcRef " + tcRefName + ". ModeParser " + modeName + " not found.");
                return null;
            }
            TCRefConfig tcRefConfig = modeParser.addTCRef(tcRefName, tcRefNameList);
            if (DEBUG) {
                Debug.log(WindowManagerParser.class, "WMParser.addTCRef LEAVE 2 mo:" + modeName + " tcRef:" + tcRefName);
            }
            return tcRefConfig;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    boolean removeTCGroup(String groupName, String tcGroupName) {
        Object object = SAVING_LOCK;
        synchronized (object) {
            GroupParser groupParser;
            if (DEBUG) {
                Debug.log(WindowManagerParser.class, "WMParser.removeTCGroup ENTER group:" + groupName + " tcGroup:" + tcGroupName);
            }
            if ((groupParser = this.groupParserMap.get(groupName)) == null) {
                if (DEBUG) {
                    Debug.log(WindowManagerParser.class, "WMParser.removeTCGroup LEAVE 1 group:" + groupName + " tcGroup:" + tcGroupName);
                }
                return false;
            }
            groupParser.removeTCGroup(tcGroupName);
            if (DEBUG) {
                Debug.log(WindowManagerParser.class, "WMParser.removeTCGroup LEAVE 2 group:" + groupName + " tcGroup:" + tcGroupName);
            }
            return true;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    TCGroupConfig addTCGroup(String groupName, String tcGroupName) {
        Object object = SAVING_LOCK;
        synchronized (object) {
            GroupParser groupParser;
            if (DEBUG) {
                Debug.log(WindowManagerParser.class, "WMParser.addTCGroup ENTER group:" + groupName + " tcGroup:" + tcGroupName);
            }
            if ((groupParser = this.groupParserMap.get(groupName)) == null) {
                if (DEBUG) {
                    Debug.log(WindowManagerParser.class, "WMParser.addTCGroup LEAVE 1 group:" + groupName + " tcGroup:" + tcGroupName);
                }
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.addTCGroup] Warning: Cannot add tcGroup " + tcGroupName + ". GroupParser " + groupName + " not found.");
                return null;
            }
            TCGroupConfig tcGroupConfig = groupParser.addTCGroup(tcGroupName);
            if (DEBUG) {
                Debug.log(WindowManagerParser.class, "WMParser.addTCGroup LEAVE 2 group:" + groupName + " tcGroup:" + tcGroupName);
            }
            return tcGroupConfig;
        }
    }

    public void addTCRefImport(String modeName, String tcRefName, InternalConfig internalCfg) {
        ModeParser modeParser;
        if (DEBUG) {
            Debug.log(WindowManagerParser.class, "addTCRefImport ENTER mo:" + modeName + " tcRef:" + tcRefName);
        }
        if ((modeParser = this.modeParserMap.get(modeName)) == null) {
            if (DEBUG) {
                Debug.log(WindowManagerParser.class, "addTCRefImport LEAVE 1 mo:" + modeName + " tcRef:" + tcRefName);
            }
            PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.addTCRef] Warning: Cannot add tcRef " + tcRefName + ". ModeParser " + modeName + " not found.");
            return;
        }
        modeParser.addTCRefImport(tcRefName, internalCfg);
        if (DEBUG) {
            Debug.log(WindowManagerParser.class, "addTCRefImport LEAVE 2 mo:" + modeName + " tcRef:" + tcRefName);
        }
    }

    ModeParser findModeParser(String tcRefName) {
        if (DEBUG) {
            Debug.log(WindowManagerParser.class, "findModeParser ENTER tcRef:" + tcRefName);
        }
        Iterator<String> it = this.modeParserMap.keySet().iterator();
        while (it.hasNext()) {
            ModeParser modeParser = this.modeParserMap.get(it.next());
            TCRefParser tcRefParser = modeParser.findTCRefParser(tcRefName);
            if (tcRefParser == null) continue;
            return modeParser;
        }
        return null;
    }

    private void readProperties(WindowManagerConfig wmc) throws IOException {
        if (DEBUG) {
            Debug.log(WindowManagerParser.class, "readProperties ENTER");
        }
        PropertyHandler propertyHandler = new PropertyHandler();
        this.internalConfig = new InternalConfig();
        propertyHandler.readData(wmc, this.internalConfig);
        if (DEBUG) {
            Debug.log(WindowManagerParser.class, "readProperties LEAVE");
        }
    }

    private void readModes(WindowManagerConfig wmc) throws IOException {
        if (DEBUG) {
            Debug.log(WindowManagerParser.class, "readModes ENTER");
        }
        Iterator<String> it = this.modeParserMap.keySet().iterator();
        while (it.hasNext()) {
            ModeParser modeParser = this.modeParserMap.get(it.next());
            modeParser.setInModuleFolder(false);
            modeParser.setInLocalFolder(false);
        }
        FileObject modesModuleFolder = this.pm.getRootModuleFolder().getFileObject("Modes");
        if (modesModuleFolder != null) {
            FileObject[] files = modesModuleFolder.getChildren();
            for (int i = 0; i < files.length; ++i) {
                if (files[i].isFolder() || !"wsmode".equals(files[i].getExt())) continue;
                ModeParser modeParser = this.modeParserMap.get(files[i].getName());
                if (modeParser == null) {
                    modeParser = new ModeParser(files[i].getName(), this.tcRefNameLocalSet);
                    this.modeParserMap.put(files[i].getName(), modeParser);
                }
                modeParser.setInModuleFolder(true);
                modeParser.setModuleParentFolder(modesModuleFolder);
            }
        }
        FileObject modesLocalFolder = this.pm.getRootLocalFolder().getFileObject("Modes");
        this.tcRefNameLocalSet.clear();
        if (modesLocalFolder != null) {
            FileObject[] files = modesLocalFolder.getChildren();
            for (int i = 0; i < files.length; ++i) {
                if (!files[i].isFolder() && "wsmode".equals(files[i].getExt())) {
                    ModeParser modeParser;
                    if (this.modeParserMap.containsKey(files[i].getName())) {
                        modeParser = this.modeParserMap.get(files[i].getName());
                    } else {
                        modeParser = new ModeParser(files[i].getName(), this.tcRefNameLocalSet);
                        this.modeParserMap.put(files[i].getName(), modeParser);
                    }
                    modeParser.setInLocalFolder(true);
                    modeParser.setLocalParentFolder(modesLocalFolder);
                }
                if (!files[i].isFolder()) continue;
                FileObject[] subFiles = files[i].getChildren();
                for (int j = 0; j < subFiles.length; ++j) {
                    if (subFiles[j].isFolder() || !"wstcref".equals(subFiles[j].getExt())) continue;
                    this.tcRefNameLocalSet.add(subFiles[j].getName());
                }
            }
        }
        ArrayList<ModeConfig> modeCfgList = new ArrayList<ModeConfig>(this.modeParserMap.size());
        ArrayList<ModeParser> toRemove = new ArrayList<ModeParser>(this.modeParserMap.size());
        Iterator<String> it2 = this.modeParserMap.keySet().iterator();
        while (it2.hasNext()) {
            ModeConfig modeCfg;
            ModeParser modeParser = this.modeParserMap.get(it2.next());
            try {
                modeCfg = modeParser.load();
            }
            catch (IOException exc) {
                Logger.getLogger(WindowManagerParser.class.getName()).log(Level.INFO, null, exc);
                continue;
            }
            boolean modeAccepted = this.acceptMode(modeParser, modeCfg);
            if (modeAccepted) {
                modeCfgList.add(modeCfg);
                continue;
            }
            toRemove.add(modeParser);
            this.deleteLocalMode(modeParser.getName());
        }
        for (int i = 0; i < toRemove.size(); ++i) {
            ModeParser modeParser = (ModeParser)toRemove.get(i);
            this.modeParserMap.remove(modeParser.getName());
        }
        this.mergeModes(modeCfgList);
        wmc.modes = modeCfgList.toArray(new ModeConfig[modeCfgList.size()]);
        if (DEBUG) {
            Debug.log(WindowManagerParser.class, "readModes LEAVE");
        }
    }

    private void mergeModes(List<ModeConfig> modeCfgList) {
        HashSet<String> mergedModes = new HashSet<String>(20);
        for (ModeConfig modeConfig : modeCfgList) {
            if (modeConfig.otherNames == null) continue;
            mergedModes.addAll(modeConfig.otherNames);
            ModeParser parser = this.modeParserMap.get(modeConfig.name);
            if (null == parser) continue;
            for (String otherName : modeConfig.otherNames) {
                this.modeParserMap.put(otherName, parser);
            }
        }
        for (String name : mergedModes) {
            ModeConfig merged = null;
            for (ModeConfig mc2 : modeCfgList) {
                if (!name.equals(mc2.name)) continue;
                modeCfgList.remove(mc2);
                merged = mc2;
                break;
            }
            if (null != merged && merged.tcRefConfigs.length > 0) {
                for (ModeConfig mc2 : modeCfgList) {
                    if (null == mc2.otherNames || !mc2.otherNames.contains(merged.name)) continue;
                    ArrayList<TCRefConfig> refs = new ArrayList<TCRefConfig>(Arrays.asList(mc2.tcRefConfigs));
                    for (TCRefConfig tcrf : merged.tcRefConfigs) {
                        if (refs.contains(tcrf)) continue;
                        refs.add(tcrf);
                    }
                    mc2.tcRefConfigs = refs.toArray(new TCRefConfig[refs.size()]);
                    break;
                }
            }
            ModeParser parser = this.modeParserMap.remove(name);
        }
    }

    private boolean acceptMode(ModeParser modeParser, ModeConfig config) {
        InternalConfig cfg = modeParser.getInternalConfig();
        if (cfg.moduleCodeNameBase != null) {
            ModuleInfo curModuleInfo = PersistenceManager.findModule(cfg.moduleCodeNameBase, cfg.moduleCodeNameRelease, cfg.moduleSpecificationVersion);
            if (curModuleInfo == null) {
                PersistenceManager.LOG.info("Cannot find module '" + cfg.moduleCodeNameBase + " " + cfg.moduleCodeNameRelease + " " + cfg.moduleSpecificationVersion + "' for wsmode with name '" + config.name + "'");
            }
            if (curModuleInfo != null && curModuleInfo.isEnabled()) {
                return true;
            }
            return false;
        }
        return true;
    }

    private void readGroups(WindowManagerConfig wmc) throws IOException {
        GroupParser groupParser;
        FileObject groupsLocalFolder;
        if (DEBUG) {
            Debug.log(WindowManagerParser.class, "readGroups ENTER");
        }
        Iterator<String> it = this.groupParserMap.keySet().iterator();
        while (it.hasNext()) {
            GroupParser groupParser2 = this.groupParserMap.get(it.next());
            groupParser2.setInModuleFolder(false);
            groupParser2.setInLocalFolder(false);
        }
        FileObject groupsModuleFolder = this.pm.getRootModuleFolder().getFileObject("Groups");
        if (groupsModuleFolder != null) {
            FileObject[] files = groupsModuleFolder.getChildren();
            for (int i = 0; i < files.length; ++i) {
                GroupParser groupParser3;
                if (files[i].isFolder() || !"wsgrp".equals(files[i].getExt())) continue;
                if (this.groupParserMap.containsKey(files[i].getName())) {
                    groupParser3 = this.groupParserMap.get(files[i].getName());
                } else {
                    groupParser3 = new GroupParser(files[i].getName());
                    this.groupParserMap.put(files[i].getName(), groupParser3);
                }
                groupParser3.setInModuleFolder(true);
                groupParser3.setModuleParentFolder(groupsModuleFolder);
            }
        }
        if ((groupsLocalFolder = this.pm.getRootLocalFolder().getFileObject("Groups")) != null) {
            FileObject[] files = groupsLocalFolder.getChildren();
            for (int i = 0; i < files.length; ++i) {
                GroupParser groupParser4;
                if (files[i].isFolder() || !"wsgrp".equals(files[i].getExt())) continue;
                if (this.groupParserMap.containsKey(files[i].getName())) {
                    groupParser4 = this.groupParserMap.get(files[i].getName());
                } else {
                    groupParser4 = new GroupParser(files[i].getName());
                    this.groupParserMap.put(files[i].getName(), groupParser4);
                }
                groupParser4.setInLocalFolder(true);
                groupParser4.setLocalParentFolder(groupsLocalFolder);
            }
        }
        ArrayList<GroupConfig> groupCfgList = new ArrayList<GroupConfig>(this.groupParserMap.size());
        ArrayList<GroupParser> toRemove = new ArrayList<GroupParser>(this.groupParserMap.size());
        Iterator<String> it2 = this.groupParserMap.keySet().iterator();
        while (it2.hasNext()) {
            GroupConfig groupCfg;
            groupParser = this.groupParserMap.get(it2.next());
            try {
                groupCfg = groupParser.load();
            }
            catch (IOException exc) {
                Logger.getLogger(WindowManagerParser.class.getName()).log(Level.INFO, null, exc);
                continue;
            }
            boolean groupAccepted = this.acceptGroup(groupParser, groupCfg);
            if (groupAccepted) {
                groupCfgList.add(groupCfg);
                continue;
            }
            toRemove.add(groupParser);
            this.deleteLocalGroup(groupParser.getName());
        }
        for (int i = 0; i < toRemove.size(); ++i) {
            groupParser = (GroupParser)toRemove.get(i);
            this.groupParserMap.remove(groupParser.getName());
        }
        wmc.groups = groupCfgList.toArray(new GroupConfig[groupCfgList.size()]);
        if (DEBUG) {
            Debug.log(WindowManagerParser.class, "readGroups LEAVE");
        }
    }

    private boolean acceptGroup(GroupParser groupParser, GroupConfig config) {
        InternalConfig cfg = groupParser.getInternalConfig();
        if (cfg.moduleCodeNameBase != null) {
            ModuleInfo curModuleInfo = PersistenceManager.findModule(cfg.moduleCodeNameBase, cfg.moduleCodeNameRelease, cfg.moduleSpecificationVersion);
            if (curModuleInfo == null) {
                PersistenceManager.LOG.log(Level.FINE, "Cannot find module '" + cfg.moduleCodeNameBase + " " + cfg.moduleCodeNameRelease + " " + cfg.moduleSpecificationVersion + "' for group with name '" + config.name + "'");
            }
            if (curModuleInfo != null && curModuleInfo.isEnabled()) {
                return true;
            }
            return false;
        }
        return true;
    }

    private void writeProperties(WindowManagerConfig wmc) throws IOException {
        if (DEBUG) {
            Debug.log(WindowManagerParser.class, "writeProperties ENTER");
        }
        PropertyHandler propertyHandler = new PropertyHandler();
        propertyHandler.writeData(wmc);
        if (DEBUG) {
            Debug.log(WindowManagerParser.class, "writeProperties LEAVE");
        }
    }

    private void writeModes(WindowManagerConfig wmc) throws IOException {
        int i;
        ModeParser modeParser;
        if (DEBUG) {
            Debug.log(WindowManagerParser.class, "writeModes ENTER");
        }
        HashMap<String, ModeConfig> modeConfigMap = new HashMap<String, ModeConfig>();
        for (int i2 = 0; i2 < wmc.modes.length; ++i2) {
            modeConfigMap.put(wmc.modes[i2].name, wmc.modes[i2]);
        }
        ArrayList<String> toDelete = new ArrayList<String>(10);
        for (String s : this.modeParserMap.keySet()) {
            modeParser = this.modeParserMap.get(s);
            if (modeConfigMap.containsKey(modeParser.getName())) continue;
            toDelete.add(modeParser.getName());
        }
        for (i = 0; i < toDelete.size(); ++i) {
            this.modeParserMap.remove(toDelete.get(i));
            this.deleteLocalMode((String)toDelete.get(i));
        }
        for (i = 0; i < wmc.modes.length; ++i) {
            if (this.modeParserMap.containsKey(wmc.modes[i].name)) continue;
            ModeParser modeParser2 = new ModeParser(wmc.modes[i].name, this.tcRefNameLocalSet);
            this.modeParserMap.put(wmc.modes[i].name, modeParser2);
        }
        FileObject modesLocalFolder = this.pm.getRootLocalFolder().getFileObject("Modes");
        if (modesLocalFolder == null && this.modeParserMap.size() > 0) {
            modesLocalFolder = this.pm.getModesLocalFolder();
        }
        Iterator<String> it = this.modeParserMap.keySet().iterator();
        while (it.hasNext()) {
            modeParser = this.modeParserMap.get(it.next());
            modeParser.setLocalParentFolder(modesLocalFolder);
            modeParser.setInLocalFolder(true);
            modeParser.save((ModeConfig)modeConfigMap.get(modeParser.getName()));
        }
        if (DEBUG) {
            Debug.log(WindowManagerParser.class, "writeModes LEAVE");
        }
    }

    private void writeGroups(WindowManagerConfig wmc) throws IOException {
        int i;
        GroupParser groupParser;
        if (DEBUG) {
            Debug.log(WindowManagerParser.class, "writeGroups ENTER");
        }
        HashMap<String, GroupConfig> groupConfigMap = new HashMap<String, GroupConfig>();
        for (int i2 = 0; i2 < wmc.groups.length; ++i2) {
            groupConfigMap.put(wmc.groups[i2].name, wmc.groups[i2]);
        }
        ArrayList<String> toDelete = new ArrayList<String>(10);
        for (String s : this.groupParserMap.keySet()) {
            groupParser = this.groupParserMap.get(s);
            if (groupConfigMap.containsKey(groupParser.getName())) continue;
            toDelete.add(groupParser.getName());
        }
        for (i = 0; i < toDelete.size(); ++i) {
            this.groupParserMap.remove(toDelete.get(i));
            this.deleteLocalGroup((String)toDelete.get(i));
        }
        for (i = 0; i < wmc.groups.length; ++i) {
            if (this.groupParserMap.containsKey(wmc.groups[i].name)) continue;
            GroupParser groupParser2 = new GroupParser(wmc.groups[i].name);
            this.groupParserMap.put(wmc.groups[i].name, groupParser2);
        }
        FileObject groupsLocalFolder = this.pm.getRootLocalFolder().getFileObject("Groups");
        if (groupsLocalFolder == null && this.groupParserMap.size() > 0) {
            groupsLocalFolder = this.pm.getGroupsLocalFolder();
        }
        Iterator<String> it = this.groupParserMap.keySet().iterator();
        while (it.hasNext()) {
            groupParser = this.groupParserMap.get(it.next());
            groupParser.setLocalParentFolder(groupsLocalFolder);
            groupParser.setInLocalFolder(true);
            groupParser.save((GroupConfig)groupConfigMap.get(groupParser.getName()));
        }
        if (DEBUG) {
            Debug.log(WindowManagerParser.class, "writeGroups LEAVE");
        }
    }

    private void deleteLocalMode(String modeName) {
        if (DEBUG) {
            Debug.log(WindowManagerParser.class, "deleteLocalMode mo:" + modeName);
        }
        FileObject rootFO = null;
        try {
            rootFO = this.pm.getRootLocalFolder();
        }
        catch (IOException exc) {
            PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.deleteLocalMode] Cannot get root local folder", exc);
            return;
        }
        FileObject modesLocalFolder = rootFO.getFileObject("Modes");
        if (modesLocalFolder == null) {
            return;
        }
        FileObject modeFO = modesLocalFolder.getFileObject(modeName);
        if (modeFO != null) {
            PersistenceManager.deleteOneFO(modeFO);
        }
        if ((modeFO = modesLocalFolder.getFileObject(modeName, "wsmode")) != null) {
            PersistenceManager.deleteOneFO(modeFO);
        }
    }

    private void deleteLocalGroup(String groupName) {
        if (DEBUG) {
            Debug.log(WindowManagerParser.class, "deleteLocalGroup groupName:" + groupName);
        }
        FileObject rootFO = null;
        try {
            rootFO = this.pm.getRootLocalFolder();
        }
        catch (IOException exc) {
            PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.deleteLocalGroup] Cannot get root local folder", exc);
            return;
        }
        FileObject groupsLocalFolder = rootFO.getFileObject("Groups");
        if (groupsLocalFolder == null) {
            return;
        }
        FileObject groupFO = groupsLocalFolder.getFileObject(groupName);
        if (groupFO != null) {
            PersistenceManager.deleteOneFO(groupFO);
        }
        if ((groupFO = groupsLocalFolder.getFileObject(groupName, "wsgrp")) != null) {
            PersistenceManager.deleteOneFO(groupFO);
        }
    }

    String getName() {
        return this.wmName;
    }

    void log(String s) {
        if (DEBUG) {
            Debug.log(WindowManagerParser.class, s);
        }
    }

    private static final float floatParse(String s) throws NumberFormatException {
        int i = Arrays.binarySearch(floatStrings, s);
        if (i >= 0) {
            return floatVals[i];
        }
        return Float.parseFloat(s);
    }

    public static ModeConfig loadModeConfigFrom(FileObject fo) throws IOException {
        String modeName = fo.getName();
        ModeParser parser = new ModeParser(modeName, new HashSet(1));
        parser.setInLocalFolder(true);
        parser.setLocalParentFolder(fo.getParent());
        return parser.load();
    }

    private final class PropertyHandler
    extends DefaultHandler {
        private WindowManagerConfig winMgrConfig;
        private InternalConfig internalConfig;
        private List<SplitConstraint> itemList;
        private List<String> tcIdList;
        private final Object RW_LOCK;

        public PropertyHandler() {
            this.winMgrConfig = null;
            this.internalConfig = null;
            this.itemList = new ArrayList<SplitConstraint>(10);
            this.tcIdList = new ArrayList<String>(10);
            this.RW_LOCK = new Object();
        }

        private FileObject getConfigFOInput() throws IOException {
            FileObject rootFolder = WindowManagerParser.this.pm.getRootLocalFolder();
            FileObject wmConfigFO = rootFolder.getFileObject(WindowManagerParser.this.getName(), "wswmgr");
            if (wmConfigFO != null) {
                return wmConfigFO;
            }
            rootFolder = WindowManagerParser.this.pm.getRootModuleFolder();
            wmConfigFO = rootFolder.getFileObject(WindowManagerParser.this.getName(), "wswmgr");
            return wmConfigFO;
        }

        private FileObject getConfigFOOutput() throws IOException {
            FileObject rootFolder = WindowManagerParser.this.pm.getRootLocalFolder();
            FileObject wmConfigFO = rootFolder.getFileObject(WindowManagerParser.this.getName(), "wswmgr");
            if (wmConfigFO != null) {
                return wmConfigFO;
            }
            StringBuffer buffer = new StringBuffer();
            buffer.append(WindowManagerParser.this.getName());
            buffer.append('.');
            buffer.append("wswmgr");
            wmConfigFO = FileUtil.createData((FileObject)rootFolder, (String)buffer.toString());
            return wmConfigFO;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void readData(WindowManagerConfig winMgrCfg, InternalConfig internalCfg) throws IOException {
            this.winMgrConfig = winMgrCfg;
            this.internalConfig = internalCfg;
            this.itemList.clear();
            this.tcIdList.clear();
            FileObject cfgFOInput = this.getConfigFOInput();
            if (cfgFOInput == null) {
                throw new FileNotFoundException("[WinSys] Missing Window Manager configuration file");
            }
            InputStream is = null;
            try {
                Object object = this.RW_LOCK;
                synchronized (object) {
                    is = cfgFOInput.getInputStream();
                    PersistenceManager.getDefault().getXMLParser(this).parse(new InputSource(is));
                }
            }
            catch (SAXException exc) {
                String msg = NbBundle.getMessage(WindowManagerParser.class, (String)"EXC_WindowManagerParse", (Object)cfgFOInput);
                throw (IOException)new IOException(msg).initCause(exc);
            }
            finally {
                try {
                    if (is != null) {
                        is.close();
                    }
                }
                catch (IOException exc) {
                    Logger.getLogger(WindowManagerParser.class.getName()).log(Level.INFO, null, exc);
                }
            }
            this.winMgrConfig.editorAreaConstraints = this.itemList.toArray(new SplitConstraint[this.itemList.size()]);
            this.winMgrConfig.tcIdViewList = this.tcIdList.toArray(new String[this.tcIdList.size()]);
            winMgrCfg = this.winMgrConfig;
            internalCfg = this.internalConfig;
            this.winMgrConfig = null;
            this.internalConfig = null;
        }

        @Override
        public void startElement(String nameSpace, String name, String qname, Attributes attrs) throws SAXException {
            if ("windowmanager".equals(qname)) {
                this.handleWindowManager(attrs);
            } else if (this.internalConfig.specVersion != null && this.internalConfig.specVersion.compareTo((Object)new SpecificationVersion("2.0")) >= 0) {
                if ("main-window".equals(qname)) {
                    this.handleMainWindow(attrs);
                } else if ("joined-properties".equals(qname)) {
                    this.handleJoinedProperties(attrs);
                } else if ("separated-properties".equals(qname)) {
                    this.handleSeparatedProperties(attrs);
                } else if ("editor-area".equals(qname)) {
                    this.handleEditorArea(attrs);
                } else if ("constraints".equals(qname)) {
                    this.handleConstraints(attrs);
                } else if ("path".equals(qname)) {
                    this.handlePath(attrs);
                } else if ("bounds".equals(qname)) {
                    this.handleEditorAreaBounds(attrs);
                } else if ("relative-bounds".equals(qname)) {
                    this.handleEditorAreaRelativeBounds(attrs);
                } else if ("screen".equals(qname)) {
                    this.handleScreen(attrs);
                } else if ("active-mode".equals(qname)) {
                    this.handleActiveMode(attrs);
                } else if ("maximized-mode".equals(qname)) {
                    this.handleMaximizedMode(attrs);
                } else if ("toolbar".equals(qname)) {
                    this.handleToolbar(attrs);
                } else if ("tc-id".equals(qname)) {
                    this.handleTcId(attrs);
                } else if ("tcref-item".equals(qname)) {
                    this.handleTCRefItem(attrs);
                }
            } else if (DEBUG) {
                Debug.log(WindowManagerParser.class, "WMP.startElement PARSING OLD");
            }
        }

        @Override
        public void error(SAXParseException ex) throws SAXException {
            throw ex;
        }

        private void handleWindowManager(Attributes attrs) {
            String version = attrs.getValue("version");
            if (version != null) {
                this.internalConfig.specVersion = new SpecificationVersion(version);
            } else {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleWindowManager] Missing attribute \"version\" of element \"windowmanager\".");
                this.internalConfig.specVersion = new SpecificationVersion("2.0");
            }
        }

        private void handleMainWindow(Attributes attrs) {
        }

        private void handleJoinedProperties(Attributes attrs) {
            String s;
            try {
                s = attrs.getValue("x");
                this.winMgrConfig.xJoined = s != null ? Integer.parseInt(s) : -1;
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleJoinedProperties] Warning: Cannot read attribute \"x\" of element \"joined-properties\".", exc);
                this.winMgrConfig.xJoined = -1;
            }
            try {
                s = attrs.getValue("y");
                this.winMgrConfig.yJoined = s != null ? Integer.parseInt(s) : -1;
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleJoinedProperties] Warning: Cannot read attribute \"y\" of element \"joined-properties\".", exc);
                this.winMgrConfig.yJoined = -1;
            }
            try {
                s = attrs.getValue("width");
                this.winMgrConfig.widthJoined = s != null ? Integer.parseInt(s) : -1;
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleJoinedProperties] Warning: Cannot read attribute \"width\" of element \"joined-properties\".", exc);
                this.winMgrConfig.widthJoined = -1;
            }
            try {
                s = attrs.getValue("height");
                this.winMgrConfig.heightJoined = s != null ? Integer.parseInt(s) : -1;
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleJoinedProperties] Warning: Cannot read attribute \"height\" of element \"joined-properties\".", exc);
                this.winMgrConfig.heightJoined = -1;
            }
            try {
                s = attrs.getValue("relative-x");
                this.winMgrConfig.relativeXJoined = s != null ? WindowManagerParser.floatParse(s) : -1.0f;
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleJoinedProperties] Warning: Cannot read attribute \"relative-x\" of element \"joined-properties\".", exc);
                this.winMgrConfig.relativeXJoined = -1.0f;
            }
            try {
                s = attrs.getValue("relative-y");
                this.winMgrConfig.relativeYJoined = s != null ? WindowManagerParser.floatParse(s) : -1.0f;
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleJoinedProperties] Warning: Cannot read attribute \"relative-y\" of element \"joined-properties\".", exc);
                this.winMgrConfig.relativeYJoined = -1.0f;
            }
            try {
                s = attrs.getValue("relative-width");
                this.winMgrConfig.relativeWidthJoined = s != null ? WindowManagerParser.floatParse(s) : -1.0f;
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleJoinedProperties] Warning: Cannot read attribute \"relative-width\" of element \"joined-properties\".", exc);
                this.winMgrConfig.relativeWidthJoined = -1.0f;
            }
            try {
                s = attrs.getValue("relative-height");
                this.winMgrConfig.relativeHeightJoined = s != null ? WindowManagerParser.floatParse(s) : -1.0f;
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleJoinedProperties] Warning: Cannot read attribute \"relative-height\" of element \"joined-properties\".", exc);
                this.winMgrConfig.relativeHeightJoined = -1.0f;
            }
            s = attrs.getValue("centered-horizontally");
            if (s != null) {
                if ("true".equals(s)) {
                    this.winMgrConfig.centeredHorizontallyJoined = true;
                } else if ("false".equals(s)) {
                    this.winMgrConfig.centeredHorizontallyJoined = false;
                } else {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleJoinedProperties] Warning: Invalid value of attribute \"centered-horizontally\" of element \"joined-properties\".");
                    this.winMgrConfig.centeredHorizontallyJoined = false;
                }
            } else {
                this.winMgrConfig.centeredHorizontallyJoined = false;
            }
            s = attrs.getValue("centered-vertically");
            if (s != null) {
                if ("true".equals(s)) {
                    this.winMgrConfig.centeredVerticallyJoined = true;
                } else if ("false".equals(s)) {
                    this.winMgrConfig.centeredVerticallyJoined = false;
                } else {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleJoinedProperties] Warning: Invalid value of attribute \"centered-vertically\" of element \"joined-properties\".");
                    this.winMgrConfig.centeredVerticallyJoined = false;
                }
            } else {
                this.winMgrConfig.centeredVerticallyJoined = false;
            }
            try {
                s = attrs.getValue("maximize-if-width-below");
                this.winMgrConfig.maximizeIfWidthBelowJoined = s != null ? Integer.parseInt(s) : -1;
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleJoinedProperties] Warning: Cannot read attribute \"maximize-if-width-below\" of element \"joined-properties\".", exc);
                this.winMgrConfig.maximizeIfWidthBelowJoined = -1;
            }
            try {
                s = attrs.getValue("maximize-if-height-below");
                this.winMgrConfig.maximizeIfHeightBelowJoined = s != null ? Integer.parseInt(s) : -1;
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleJoinedProperties] Warning: Cannot read attribute \"maximize-if-height-below\" of element \"joined-properties\".", exc);
                this.winMgrConfig.maximizeIfHeightBelowJoined = -1;
            }
            String frameState = attrs.getValue("frame-state");
            if (frameState != null) {
                try {
                    this.winMgrConfig.mainWindowFrameStateJoined = Integer.parseInt(frameState);
                }
                catch (NumberFormatException exc) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleJoinedProperties] Warning: Cannot read attribute \"frame-state\" of element \"joined-properties\".", exc);
                    this.winMgrConfig.mainWindowFrameStateJoined = 0;
                }
            } else {
                this.winMgrConfig.mainWindowFrameStateJoined = 0;
            }
        }

        private void handleSeparatedProperties(Attributes attrs) {
            String s;
            try {
                s = attrs.getValue("x");
                this.winMgrConfig.xSeparated = s != null ? Integer.parseInt(s) : -1;
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleSeparatedProperties] Warning: Cannot read attribute \"x\" of element \"separated-properties\".", exc);
                this.winMgrConfig.xSeparated = -1;
            }
            try {
                s = attrs.getValue("y");
                this.winMgrConfig.ySeparated = s != null ? Integer.parseInt(s) : -1;
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleSeparatedProperties] Warning: Cannot read attribute \"y\" of element \"separated-properties\".", exc);
                this.winMgrConfig.ySeparated = -1;
            }
            try {
                s = attrs.getValue("width");
                this.winMgrConfig.widthSeparated = s != null ? Integer.parseInt(s) : -1;
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleSeparatedProperties] Warning: Cannot read attribute \"width\" of element \"separated-properties\".", exc);
                this.winMgrConfig.widthSeparated = -1;
            }
            try {
                s = attrs.getValue("height");
                this.winMgrConfig.heightSeparated = s != null ? Integer.parseInt(s) : -1;
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleSeparatedProperties] Warning: Cannot read attribute \"height\" of element \"separated-properties\".", exc);
                this.winMgrConfig.heightSeparated = -1;
            }
            try {
                s = attrs.getValue("relative-x");
                this.winMgrConfig.relativeXSeparated = s != null ? WindowManagerParser.floatParse(s) : -1.0f;
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleSeparatedProperties] Warning: Cannot read attribute \"relative-x\" of element \"separated-properties\".", exc);
                this.winMgrConfig.relativeXSeparated = -1.0f;
            }
            try {
                s = attrs.getValue("relative-y");
                this.winMgrConfig.relativeYSeparated = s != null ? WindowManagerParser.floatParse(s) : -1.0f;
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleSeparatedProperties] Warning: Cannot read attribute \"relative-y\" of element \"separated-properties\".", exc);
                this.winMgrConfig.relativeYSeparated = -1.0f;
            }
            try {
                s = attrs.getValue("relative-width");
                this.winMgrConfig.relativeWidthSeparated = s != null ? WindowManagerParser.floatParse(s) : -1.0f;
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleSeparatedProperties] Warning: Cannot read attribute \"relative-width\" of element \"separated-properties\".", exc);
                this.winMgrConfig.relativeWidthSeparated = -1.0f;
            }
            try {
                s = attrs.getValue("relative-height");
                this.winMgrConfig.relativeHeightSeparated = s != null ? WindowManagerParser.floatParse(s) : -1.0f;
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleSeparatedProperties] Warning: Cannot read attribute \"relative-height\" of element \"separated-properties\".", exc);
                this.winMgrConfig.relativeHeightSeparated = -1.0f;
            }
            s = attrs.getValue("centered-horizontally");
            if (s != null) {
                if ("true".equals(s)) {
                    this.winMgrConfig.centeredHorizontallySeparated = true;
                } else if ("false".equals(s)) {
                    this.winMgrConfig.centeredHorizontallySeparated = false;
                } else {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleSeparatedProperties] Warning: Invalid value of attribute \"centered-horizontally\" of element \"separated-properties\".");
                    this.winMgrConfig.centeredHorizontallySeparated = false;
                }
            } else {
                this.winMgrConfig.centeredHorizontallySeparated = false;
            }
            s = attrs.getValue("centered-vertically");
            if (s != null) {
                if ("true".equals(s)) {
                    this.winMgrConfig.centeredVerticallySeparated = true;
                } else if ("false".equals(s)) {
                    this.winMgrConfig.centeredVerticallySeparated = false;
                } else {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleSeparatedProperties] Warning: Invalid value of attribute \"centered-vertically\" of element \"separated-properties\".");
                    this.winMgrConfig.centeredVerticallySeparated = false;
                }
            } else {
                this.winMgrConfig.centeredVerticallySeparated = false;
            }
            String frameState = attrs.getValue("frame-state");
            if (frameState != null) {
                try {
                    this.winMgrConfig.mainWindowFrameStateSeparated = Integer.parseInt(frameState);
                }
                catch (NumberFormatException exc) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleSeparatedProperties] Warning: Cannot read attribute \"frame-state\" of element \"separated-properties\".", exc);
                    this.winMgrConfig.mainWindowFrameStateSeparated = 0;
                }
            } else {
                this.winMgrConfig.mainWindowFrameStateSeparated = 0;
            }
        }

        private void handleEditorArea(Attributes attrs) {
            String state = attrs.getValue("state");
            if (state != null) {
                if ("joined".equals(state)) {
                    this.winMgrConfig.editorAreaState = 0;
                } else if ("separated".equals(state)) {
                    this.winMgrConfig.editorAreaState = 1;
                } else {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleEditorArea] Warning: Invalid value of attribute \"state\" of element \"editor-area\".");
                    this.winMgrConfig.editorAreaState = 0;
                }
            } else {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleEditorArea] Warning: Missing value of attribute \"state\" of element \"editor-area\".");
                this.winMgrConfig.editorAreaState = 0;
            }
            String frameState = attrs.getValue("frame-state");
            if (frameState != null) {
                try {
                    this.winMgrConfig.editorAreaFrameState = Integer.parseInt(frameState);
                }
                catch (NumberFormatException exc) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleEditorArea] Warning: Cannot read attribute \"frame-state\" of element \"editor-area\".", exc);
                    this.winMgrConfig.editorAreaFrameState = 0;
                }
            } else {
                this.winMgrConfig.editorAreaFrameState = 0;
            }
        }

        private void handleConstraints(Attributes attrs) {
        }

        private void handlePath(Attributes attrs) {
            int number;
            int orientation;
            double weight;
            String s = attrs.getValue("orientation");
            if ("horizontal".equals(s)) {
                orientation = 1;
            } else if ("vertical".equals(s)) {
                orientation = 0;
            } else {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handlePath] Invalid or missing value of attribute \"orientation\".");
                orientation = 0;
            }
            try {
                s = attrs.getValue("number");
                if (s != null) {
                    number = Integer.parseInt(s);
                } else {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handlePath] Missing value of attribute \"number\".");
                    number = 0;
                }
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handlePath] Cannot read element \"path\", attribute \"number\"", exc);
                number = 0;
            }
            try {
                s = attrs.getValue("weight");
                weight = s != null ? Double.parseDouble(s) : 0.5;
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handlePath] Warning: Cannot read element \"path\", attribute \"weight\".", exc);
                weight = 0.5;
            }
            SplitConstraint item = new SplitConstraint(orientation, number, weight);
            this.itemList.add(item);
        }

        private void handleScreen(Attributes attrs) {
            try {
                this.winMgrConfig.screenSize = null;
                String s = attrs.getValue("width");
                if (s == null) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleScreen] Warning: Missing attribute \"width\" of element \"screen\".");
                    return;
                }
                int width = Integer.parseInt(s);
                s = attrs.getValue("height");
                if (s == null) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleScreen] Warning: Missing attribute \"height\" of element \"screen\".");
                    return;
                }
                int height = Integer.parseInt(s);
                this.winMgrConfig.screenSize = new Dimension(width, height);
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleScreen] Warning: Cannot read element \"screen\".", exc);
            }
        }

        private void handleEditorAreaBounds(Attributes attrs) {
            try {
                this.winMgrConfig.editorAreaBounds = null;
                String s = attrs.getValue("x");
                if (s == null) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleEditorAreaBounds] Warning: Missing attribute \"x\" of element \"bounds\".");
                    return;
                }
                int x = Integer.parseInt(s);
                s = attrs.getValue("y");
                if (s == null) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleEditorAreaBounds] Warning: Missing attribute \"y\" of element \"bounds\".");
                    return;
                }
                int y = Integer.parseInt(s);
                s = attrs.getValue("width");
                if (s == null) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleEditorAreaBounds] Warning: Missing attribute \"width\" of element \"bounds\".");
                    return;
                }
                int width = Integer.parseInt(s);
                s = attrs.getValue("height");
                if (s == null) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleEditorAreaBounds] Warning: Missing attribute \"height\" of element \"bounds\".");
                    return;
                }
                int height = Integer.parseInt(s);
                this.winMgrConfig.editorAreaBounds = new Rectangle(x, y, width, height);
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleEditorAreaBounds] Warning: Cannot read element \"bounds\".", exc);
            }
        }

        private void handleEditorAreaRelativeBounds(Attributes attrs) {
            try {
                this.winMgrConfig.editorAreaRelativeBounds = null;
                String s = attrs.getValue("x");
                if (s == null) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleEditorAreaRelativeBounds] Warning: Missing attribute \"x\" of element \"relative-bounds\".");
                    return;
                }
                int x = Integer.parseInt(s);
                s = attrs.getValue("y");
                if (s == null) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleEditorAreaRelativeBounds] Warning: Missing attribute \"y\" of element \"relative-bounds\".");
                    return;
                }
                int y = Integer.parseInt(s);
                s = attrs.getValue("width");
                if (s == null) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleEditorAreaRelativeBounds] Warning: Missing attribute \"width\" of element \"relative-bounds\".");
                    return;
                }
                int width = Integer.parseInt(s);
                s = attrs.getValue("height");
                if (s == null) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleEditorAreaRelativeBounds] Warning: Missing attribute \"height\" of element \"relative-bounds\".");
                    return;
                }
                int height = Integer.parseInt(s);
                this.winMgrConfig.editorAreaRelativeBounds = new Rectangle(x, y, width, height);
            }
            catch (NumberFormatException exc) {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleEditorAreaRelativeBounds] Warning: Cannot read element \"relative-bounds\".", exc);
            }
        }

        private void handleActiveMode(Attributes attrs) {
            String name = attrs.getValue("name");
            this.winMgrConfig.activeModeName = name != null ? name : "";
        }

        private void handleMaximizedMode(Attributes attrs) {
            String name = attrs.getValue("editor");
            this.winMgrConfig.editorMaximizedModeName = name != null ? name : "";
            name = attrs.getValue("view");
            this.winMgrConfig.viewMaximizedModeName = name != null ? name : "";
        }

        private void handleToolbar(Attributes attrs) {
            String configuration = attrs.getValue("configuration");
            this.winMgrConfig.toolbarConfiguration = configuration != null ? configuration : "";
            String prefIconSize = attrs.getValue("preferred-icon-size");
            if (prefIconSize != null) {
                try {
                    this.winMgrConfig.preferredToolbarIconSize = Integer.parseInt(prefIconSize);
                    if (this.winMgrConfig.preferredToolbarIconSize != 16 && this.winMgrConfig.preferredToolbarIconSize != 24) {
                        PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleToolbar] Warning: Invalid value of attribute \"preferred-icon-size\" of element \"toolbar\": " + this.winMgrConfig.preferredToolbarIconSize + ". Fixed to default value 24.");
                        this.winMgrConfig.preferredToolbarIconSize = 24;
                    }
                }
                catch (NumberFormatException exc) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleToolbar] Warning: Cannot read attribute \"preferred-icon-size\" of element \"toolbar\". Fixed to default value 24.", exc);
                    this.winMgrConfig.preferredToolbarIconSize = 24;
                }
            } else {
                this.winMgrConfig.preferredToolbarIconSize = 24;
            }
        }

        private void handleTcId(Attributes attrs) {
            String id = attrs.getValue("id");
            if (id != null) {
                if (!"".equals(id)) {
                    this.tcIdList.add(id);
                } else {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleTcId] Warning: Empty required attribute \"id\" of element \"tc-id\".");
                }
            } else {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleTcId] Warning: Missing required attribute \"id\" of element \"tc-id\".");
            }
        }

        private void handleTCRefItem(Attributes attrs) {
            String workspaceName = attrs.getValue("workspace");
            String modeName = attrs.getValue("mode");
            String tc_id = attrs.getValue("id");
            if (workspaceName != null) {
                if ("".equals(workspaceName)) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleTCRefItem] Warning: Empty required attribute \"workspace\" of element \"tcref-item\".");
                    return;
                }
            } else {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleTCRefItem] Warning: Missing required attribute \"workspace\" of element \"tcref-item\".");
                return;
            }
            if (modeName != null) {
                if ("".equals(modeName)) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleTCRefItem] Warning: Empty required attribute \"mode\" of element \"tcref-item\".");
                    return;
                }
            } else {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleTCRefItem] Warning: Missing required attribute \"mode\" of element \"tcref-item\".");
                return;
            }
            if (tc_id != null) {
                if ("".equals(tc_id)) {
                    PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleTCRefItem] Warning: Empty required attribute \"id\" of element \"tcref-item\".");
                    return;
                }
            } else {
                PersistenceManager.LOG.log(Level.INFO, "[WinSys.WindowManagerParser.handleTCRefItem] Warning: Missing required attribute \"id\" of element \"tcref-item\".");
                return;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void writeData(WindowManagerConfig wmc) throws IOException {
            StringBuffer buff = this.fillBuffer(wmc);
            Object object = this.RW_LOCK;
            synchronized (object) {
                FileObject cfgFOOutput = this.getConfigFOOutput();
                FileLock lock = null;
                OutputStream os = null;
                OutputStreamWriter osw = null;
                try {
                    lock = cfgFOOutput.lock();
                    os = cfgFOOutput.getOutputStream(lock);
                    osw = new OutputStreamWriter(os, "UTF-8");
                    osw.write(buff.toString());
                }
                finally {
                    try {
                        if (osw != null) {
                            osw.close();
                        }
                    }
                    catch (IOException exc) {
                        Logger.getLogger(WindowManagerParser.class.getName()).log(Level.INFO, null, exc);
                    }
                    if (lock != null) {
                        lock.releaseLock();
                    }
                }
            }
        }

        private StringBuffer fillBuffer(WindowManagerConfig wmc) throws IOException {
            StringBuffer buff = new StringBuffer(800);
            Object curValue = null;
            buff.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n").append("<windowmanager version=\"2.1\">\n");
            this.appendMainWindow(wmc, buff);
            this.appendEditorArea(wmc, buff);
            this.appendScreen(wmc, buff);
            this.appendActiveMode(wmc, buff);
            this.appendMaximizedMode(wmc, buff);
            this.appendToolbar(wmc, buff);
            this.appendRecentViewList(wmc, buff);
            buff.append("</windowmanager>\n");
            return buff;
        }

        private void appendMainWindow(WindowManagerConfig wmc, StringBuffer buff) {
            buff.append("    <main-window>\n  <joined-properties\n").append("   x=\"").append(wmc.xJoined).append("\"\n").append("   y=\"").append(wmc.yJoined).append("\"\n").append("   width=\"").append(wmc.widthJoined).append("\"\n").append("   height=\"").append(wmc.heightJoined).append("\"\n").append("   relative-x=\"").append(wmc.relativeXJoined).append("\"\n").append("   relative-y=\"").append(wmc.relativeYJoined).append("\"\n").append("   relative-width=\"").append(wmc.relativeWidthJoined).append("\"\n").append("   relative-height=\"").append(wmc.relativeHeightJoined).append("\"\n").append("   centered-horizontally=\"").append(wmc.centeredHorizontallyJoined).append("\"\n").append("   centered-vertically=\"").append(wmc.centeredVerticallyJoined).append("\"\n").append("   maximize-if-width-below=\"").append(wmc.maximizeIfWidthBelowJoined).append("\"\n").append("   maximize-if-height-below=\"").append(wmc.maximizeIfHeightBelowJoined).append("\"\n").append("   frame-state=\"").append(wmc.mainWindowFrameStateJoined).append("\"\n/>\n").append("  <separated-properties\n").append("   x=\"").append(wmc.xSeparated).append("\"\n").append("   y=\"").append(wmc.ySeparated).append("\"\n").append("   width=\"").append(wmc.widthSeparated).append("\"\n").append("   height=\"").append(wmc.heightSeparated).append("\"\n").append("   relative-x=\"").append(wmc.relativeXSeparated).append("\"\n").append("   relative-y=\"").append(wmc.relativeYSeparated).append("\"\n").append("   relative-width=\"").append(wmc.relativeWidthSeparated).append("\"\n").append("   relative-height=\"").append(wmc.relativeHeightSeparated).append("\"\n").append("   centered-horizontally=\"").append(wmc.centeredHorizontallySeparated).append("\"\n").append("   centered-vertically=\"").append(wmc.centeredVerticallySeparated).append("\"\n").append("   frame-state=\"").append(wmc.mainWindowFrameStateSeparated).append("\"\n").append("/>\n  </main-window>\n");
        }

        private void appendEditorArea(WindowManagerConfig wmc, StringBuffer buff) {
            buff.append("    <editor-area state=\"");
            if (wmc.editorAreaState == 0) {
                buff.append("joined");
            } else {
                buff.append("separated");
            }
            buff.append("\" frame-state=\"").append(wmc.editorAreaFrameState).append("\">\n");
            buff.append("  <constraints>\n");
            for (int i = 0; i < wmc.editorAreaConstraints.length; ++i) {
                SplitConstraint item = wmc.editorAreaConstraints[i];
                buff.append("  <path orientation=\"");
                if (item.orientation == 1) {
                    buff.append("horizontal");
                } else {
                    buff.append("vertical");
                }
                buff.append("\" number=\"").append(item.index).append("\" weight=\"").append(item.splitWeight).append("\" />\n");
            }
            buff.append("  </constraints>\n");
            if (wmc.editorAreaBounds != null) {
                buff.append("  <bounds x=\"").append(wmc.editorAreaBounds.x).append("\" y=\"").append(wmc.editorAreaBounds.y).append("\" width=\"").append(wmc.editorAreaBounds.width).append("\" height=\"");
                buff.append(wmc.editorAreaBounds.height).append("\" />\n");
            } else if (wmc.editorAreaRelativeBounds != null) {
                buff.append("  <relative-bounds x=\"").append(wmc.editorAreaRelativeBounds.x).append("\" y=\"").append(wmc.editorAreaRelativeBounds.y).append("\" width=\"").append(wmc.editorAreaRelativeBounds.width).append("\" height=\"").append(wmc.editorAreaRelativeBounds.height).append("\"/>\n");
            }
            buff.append("    </editor-area>\n");
        }

        private void appendScreen(WindowManagerConfig wmc, StringBuffer buff) {
            buff.append("    <screen width=\"").append(wmc.screenSize.width).append("\" height=\"").append(wmc.screenSize.height).append("\"/>\n");
        }

        private void appendActiveMode(WindowManagerConfig wmc, StringBuffer buff) {
            if (wmc.activeModeName != null && !"".equals(wmc.activeModeName)) {
                buff.append("    <active-mode name=\"").append(wmc.activeModeName).append("\"/>\n");
            }
        }

        private void appendMaximizedMode(WindowManagerConfig wmc, StringBuffer buff) {
            if (wmc.editorMaximizedModeName != null && !"".equals(wmc.editorMaximizedModeName) || wmc.viewMaximizedModeName != null && !"".equals(wmc.viewMaximizedModeName)) {
                buff.append("    <maximized-mode");
                if (wmc.editorMaximizedModeName != null && !"".equals(wmc.editorMaximizedModeName)) {
                    buff.append(" editor=\"").append(wmc.editorMaximizedModeName).append("\"");
                }
                if (wmc.viewMaximizedModeName != null && !"".equals(wmc.viewMaximizedModeName)) {
                    buff.append(" view=\"").append(wmc.viewMaximizedModeName).append("\"");
                }
                buff.append("/>\n");
            }
        }

        private void appendToolbar(WindowManagerConfig wmc, StringBuffer buff) {
            buff.append("    <toolbar");
            if (wmc.toolbarConfiguration != null && !"".equals(wmc.toolbarConfiguration)) {
                buff.append(" configuration=\"").append(wmc.toolbarConfiguration).append("\"");
            }
            buff.append(" preferred-icon-size=\"").append(wmc.preferredToolbarIconSize).append("\"/>\n");
        }

        private void appendRecentViewList(WindowManagerConfig wmc, StringBuffer buff) {
            if (wmc.tcIdViewList.length == 0) {
                return;
            }
            buff.append("    <tc-list>\n");
            for (int i = 0; i < wmc.tcIdViewList.length; ++i) {
                buff.append("  <tc-id id=\"").append(wmc.tcIdViewList[i]).append("\"/>\n");
            }
            buff.append("    </tc-list>\n");
        }
    }

}

