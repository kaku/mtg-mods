/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows;

public class SplitConstraint {
    public final int orientation;
    public final int index;
    public final double splitWeight;

    public SplitConstraint(int orientation, int index, double splitWeight) {
        this.orientation = orientation;
        this.index = index;
        this.splitWeight = splitWeight;
    }

    public String toString() {
        String o = this.orientation == 0 ? "V" : (this.orientation == 1 ? "H" : String.valueOf(this.orientation));
        return "[" + o + ", " + this.index + ", " + this.splitWeight + "]";
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof SplitConstraint) {
            SplitConstraint item = (SplitConstraint)obj;
            if (this.orientation == item.orientation && this.index == item.index && this.splitWeight == item.splitWeight) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        int hash = 17;
        hash = 37 * hash + this.orientation;
        hash = 37 * hash + this.index;
        long l = Double.doubleToLongBits(this.splitWeight);
        hash = 37 * hash + (int)(l ^ l >>> 32);
        return hash;
    }
}

