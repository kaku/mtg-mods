/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.core.windows.Constants;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.SplitConstraint;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.model.ModelElement;
import org.openide.windows.TopComponent;

public class ModeStructureSnapshot {
    private final ElementSnapshot splitRootSnapshot;
    private final Set<ModeSnapshot> separateModeSnapshots;
    private final Set<SlidingModeSnapshot> slidingModeSnapshots;

    public ModeStructureSnapshot(ElementSnapshot splitRootSnapshot, Set<ModeSnapshot> separateModeSnapshots, Set<SlidingModeSnapshot> slidingModeSnapshots) {
        this.splitRootSnapshot = splitRootSnapshot;
        this.separateModeSnapshots = separateModeSnapshots;
        this.slidingModeSnapshots = slidingModeSnapshots;
    }

    public ElementSnapshot getSplitRootSnapshot() {
        return this.splitRootSnapshot;
    }

    public ModeSnapshot[] getSeparateModeSnapshots() {
        return this.separateModeSnapshots.toArray(new ModeSnapshot[0]);
    }

    public SlidingModeSnapshot[] getSlidingModeSnapshots() {
        return this.slidingModeSnapshots.toArray(new SlidingModeSnapshot[0]);
    }

    public ModeSnapshot findModeSnapshot(String name) {
        ModeSnapshot ma2222 = ModeStructureSnapshot.findModeSnapshotOfName(this.splitRootSnapshot, name);
        if (ma2222 != null) {
            return ma2222;
        }
        for (ModeSnapshot ma2222 : this.separateModeSnapshots) {
            if (!name.equals(ma2222.getName())) continue;
            return ma2222;
        }
        for (SlidingModeSnapshot ma2222 : this.slidingModeSnapshots) {
            if (!name.equals(ma2222.getName())) continue;
            return ma2222;
        }
        return null;
    }

    private static ModeSnapshot findModeSnapshotOfName(ElementSnapshot snapshot, String name) {
        ModeSnapshot ma;
        EditorSnapshot editorSnapshot;
        if (snapshot instanceof ModeSnapshot) {
            ModeSnapshot ma2 = (ModeSnapshot)snapshot;
            if (name.equals(ma2.getName())) {
                return ma2;
            }
        } else if (snapshot instanceof SplitSnapshot) {
            SplitSnapshot split = (SplitSnapshot)snapshot;
            for (ElementSnapshot child : split.getChildSnapshots()) {
                ModeSnapshot ma3 = ModeStructureSnapshot.findModeSnapshotOfName(child, name);
                if (ma3 == null) continue;
                return ma3;
            }
        } else if (snapshot instanceof EditorSnapshot && (ma = ModeStructureSnapshot.findModeSnapshotOfName((editorSnapshot = (EditorSnapshot)snapshot).getEditorAreaSnapshot(), name)) != null) {
            return ma;
        }
        return null;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("\nModesSnapshot hashCode=" + Integer.toHexString(this.hashCode()));
        sb.append("\nSplit modes:\n");
        sb.append(ModeStructureSnapshot.dumpSnapshot(this.splitRootSnapshot, 0));
        sb.append("\nSeparate Modes:");
        sb.append(ModeStructureSnapshot.dumpSet(this.separateModeSnapshots));
        return sb.toString();
    }

    private static String dumpSnapshot(ElementSnapshot snapshot, int indent) {
        StringBuffer sb = new StringBuffer();
        String indentString = ModeStructureSnapshot.createIndentString(indent);
        if (snapshot instanceof SplitSnapshot) {
            SplitSnapshot splitSnapshot = (SplitSnapshot)snapshot;
            sb.append(indentString + "split=" + splitSnapshot);
            ++indent;
            for (ElementSnapshot child : splitSnapshot.getChildSnapshots()) {
                sb.append("\n" + ModeStructureSnapshot.dumpSnapshot(child, indent));
            }
        } else if (snapshot instanceof ModeSnapshot) {
            sb.append(indentString + "mode=" + snapshot);
        } else if (snapshot instanceof EditorSnapshot) {
            sb.append(indentString + "editor=" + snapshot);
            sb.append(ModeStructureSnapshot.dumpSnapshot(((EditorSnapshot)snapshot).getEditorAreaSnapshot(), ++indent));
        }
        return sb.toString();
    }

    private static String createIndentString(int indent) {
        StringBuffer sb = new StringBuffer(indent);
        for (int i = 0; i < indent; ++i) {
            sb.append(' ');
        }
        return sb.toString();
    }

    private static String dumpSet(Set separateModes) {
        StringBuffer sb = new StringBuffer();
        Iterator it = separateModes.iterator();
        while (it.hasNext()) {
            sb.append("\nmode=" + it.next());
        }
        return sb.toString();
    }

    public static class EditorSnapshot
    extends ElementSnapshot {
        private final ElementSnapshot editorAreaSnapshot;
        private final double resizeWeight;

        public EditorSnapshot(ModelElement originator, SplitSnapshot parent, ElementSnapshot editorAreaSnapshot, double resizeWeight) {
            super(originator, parent);
            this.editorAreaSnapshot = editorAreaSnapshot;
            this.resizeWeight = resizeWeight;
        }

        @Override
        public double getResizeWeight() {
            return this.resizeWeight;
        }

        public ElementSnapshot getEditorAreaSnapshot() {
            return this.editorAreaSnapshot;
        }

        @Override
        public boolean isVisibleInSplit() {
            if (Constants.SWITCH_HIDE_EMPTY_DOCUMENT_AREA) {
                return null != this.editorAreaSnapshot && this.editorAreaSnapshot.isVisibleInSplit();
            }
            return true;
        }

        @Override
        public boolean hasVisibleDescendant() {
            return this.isVisibleInSplit();
        }

        @Override
        public String toString() {
            return super.toString() + "\n" + this.editorAreaSnapshot;
        }
    }

    public static class SlidingModeSnapshot
    extends ModeSnapshot {
        private final String side;
        private final Map<TopComponent, Integer> slideInSizes;

        public SlidingModeSnapshot(ModeImpl mode, String side, Map<TopComponent, Integer> slideInSizes) {
            super(null, null, mode, 0.0);
            this.side = side;
            this.slideInSizes = slideInSizes;
        }

        public String getSide() {
            return this.side;
        }

        public Map<TopComponent, Integer> getSlideInSizes() {
            return this.slideInSizes;
        }
    }

    public static class ModeSnapshot
    extends ElementSnapshot {
        private final ModeImpl mode;
        private final String name;
        private final int state;
        private final int kind;
        private final Rectangle bounds;
        private final int frameState;
        private final TopComponent selectedTopComponent;
        private final TopComponent[] openedTopComponents;
        private final double resizeWeight;

        public ModeSnapshot(ModelElement originator, SplitSnapshot parent, ModeImpl mode, double resizeWeight) {
            super(originator, parent);
            this.mode = mode;
            this.name = mode.getName();
            this.state = mode.getState();
            this.kind = mode.getKind();
            this.bounds = mode.getBounds();
            this.frameState = mode.getFrameState();
            this.selectedTopComponent = mode.getSelectedTopComponent();
            this.openedTopComponents = mode.getOpenedTopComponents().toArray((T[])new TopComponent[0]);
            this.resizeWeight = resizeWeight;
        }

        @Override
        public boolean originatorEquals(ElementSnapshot o) {
            if (!super.originatorEquals(o)) {
                return false;
            }
            ModeSnapshot me = (ModeSnapshot)o;
            return this.getState() == me.getState();
        }

        public ModeImpl getMode() {
            return this.mode;
        }

        public String getName() {
            return this.name;
        }

        public int getState() {
            return this.state;
        }

        public int getKind() {
            return this.kind;
        }

        public Rectangle getBounds() {
            return this.bounds;
        }

        public int getFrameState() {
            return this.frameState;
        }

        public TopComponent getSelectedTopComponent() {
            return this.selectedTopComponent;
        }

        public TopComponent[] getOpenedTopComponents() {
            return this.openedTopComponents;
        }

        @Override
        public double getResizeWeight() {
            return this.resizeWeight;
        }

        @Override
        public boolean isVisibleInSplit() {
            WindowManagerImpl wm;
            if (this.getOpenedTopComponents().length == 0) {
                return false;
            }
            if (this.getState() == 1) {
                return false;
            }
            if (this.mode.getKind() == 1 && null != (wm = WindowManagerImpl.getInstance()).getEditorMaximizedMode() && wm.getEditorMaximizedMode() != this.mode) {
                return false;
            }
            return true;
        }

        public boolean isVisibleSeparate() {
            if (this.getOpenedTopComponents().length == 0) {
                return false;
            }
            if (this.getState() == 0) {
                return false;
            }
            return true;
        }

        @Override
        public boolean hasVisibleDescendant() {
            return this.isVisibleInSplit();
        }

        @Override
        public String toString() {
            return super.toString() + "[name=" + this.mode.getName() + ", permanent=" + this.mode.isPermanent() + ", constraints=" + Arrays.asList(this.mode.getConstraints()) + "]";
        }
    }

    public static class SplitSnapshot
    extends ElementSnapshot {
        private final int orientation;
        private final List<ElementSnapshot> childSnapshots = new ArrayList<ElementSnapshot>();
        private final Map<ElementSnapshot, Double> childSnapshot2splitWeight = new HashMap<ElementSnapshot, Double>();
        private final double resizeWeight;

        public SplitSnapshot(ModelElement originator, SplitSnapshot parent, int orientation, List<ElementSnapshot> childSnapshots, Map<ElementSnapshot, Double> childSnapshot2splitWeight, double resizeWeight) {
            super(originator, parent);
            this.orientation = orientation;
            this.childSnapshots.addAll(childSnapshots);
            this.childSnapshot2splitWeight.putAll(childSnapshot2splitWeight);
            this.resizeWeight = resizeWeight;
        }

        public int getOrientation() {
            return this.orientation;
        }

        public List getVisibleChildSnapshots() {
            List<ElementSnapshot> l = this.getChildSnapshots();
            Iterator<ElementSnapshot> it = l.iterator();
            while (it.hasNext()) {
                ElementSnapshot child = it.next();
                if (child.hasVisibleDescendant()) continue;
                it.remove();
            }
            return l;
        }

        public List<ElementSnapshot> getChildSnapshots() {
            return new ArrayList<ElementSnapshot>(this.childSnapshots);
        }

        public double getChildSnapshotSplitWeight(ElementSnapshot childSnapshot) {
            Double d = this.childSnapshot2splitWeight.get(childSnapshot);
            return d == null ? -1.0 : d;
        }

        @Override
        public double getResizeWeight() {
            return this.resizeWeight;
        }

        @Override
        public boolean isVisibleInSplit() {
            int count = 0;
            for (ElementSnapshot child : this.getChildSnapshots()) {
                if (!child.hasVisibleDescendant() || ++count < 2 && (!Constants.SWITCH_HIDE_EMPTY_DOCUMENT_AREA || this.getParent() != null || count != 1)) continue;
                return true;
            }
            return false;
        }

        @Override
        public boolean hasVisibleDescendant() {
            for (ElementSnapshot child : this.getChildSnapshots()) {
                if (!child.hasVisibleDescendant()) continue;
                return true;
            }
            return false;
        }

        @Override
        public String toString() {
            return super.toString() + "[orientation=" + (this.orientation == 1 ? "horizontal" : "vertical") + "]";
        }
    }

    public static abstract class ElementSnapshot {
        private final ModelElement originator;
        private SplitSnapshot parent;

        public ElementSnapshot(ModelElement originator, SplitSnapshot parent) {
            this.originator = originator;
            this.setParent(parent);
        }

        public ModelElement getOriginator() {
            return this.originator;
        }

        public void setParent(SplitSnapshot parent) {
            if (this.parent != null) {
                throw new IllegalStateException("Parent can be set only once, this.parent=" + this.parent + ", parent=" + parent);
            }
            this.parent = parent;
        }

        public SplitSnapshot getParent() {
            return this.parent;
        }

        public boolean originatorEquals(ElementSnapshot o) {
            return this.getClass().equals(o.getClass()) && o.originator == this.originator;
        }

        public abstract double getResizeWeight();

        public abstract boolean isVisibleInSplit();

        public abstract boolean hasVisibleDescendant();

        public String toString() {
            return "Snapshot[originatorHash=" + (this.originator != null ? Integer.toHexString(this.originator.hashCode()) : "null") + "]";
        }
    }

}

