/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.WindowSystem
 */
package org.netbeans.core.windows;

import java.awt.EventQueue;
import org.netbeans.core.WindowSystem;
import org.netbeans.core.windows.PersistenceHandler;
import org.netbeans.core.windows.ShortcutAndMenuKeyEventProcessor;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.WindowSystemEventType;
import org.netbeans.core.windows.design.DesignView;
import org.netbeans.core.windows.services.DialogDisplayerImpl;
import org.netbeans.core.windows.view.ui.MainWindow;

public class WindowSystemImpl
implements WindowSystem {
    public void init() {
        assert (!EventQueue.isDispatchThread());
        if (Boolean.getBoolean("org.netbeans.core.WindowSystem.designMode")) {
            DesignView.initialize();
        }
        MainWindow.init();
    }

    public void load() {
        WindowManagerImpl.assertEventDispatchThread();
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        wm.fireEvent(WindowSystemEventType.beforeLoad);
        PersistenceHandler.getDefault().load();
        wm.fireEvent(WindowSystemEventType.afterLoad);
    }

    public void save() {
        WindowManagerImpl.assertEventDispatchThread();
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        wm.fireEvent(WindowSystemEventType.beforeSave);
        PersistenceHandler.getDefault().save();
        wm.fireEvent(WindowSystemEventType.afterSave);
    }

    public void show() {
        WindowManagerImpl.assertEventDispatchThread();
        DialogDisplayerImpl.runDelayed();
        ShortcutAndMenuKeyEventProcessor.install();
        WindowManagerImpl.getInstance().setVisible(true);
    }

    public void hide() {
        WindowManagerImpl.assertEventDispatchThread();
        WindowManagerImpl.getInstance().setVisible(false);
        ShortcutAndMenuKeyEventProcessor.uninstall();
    }
}

