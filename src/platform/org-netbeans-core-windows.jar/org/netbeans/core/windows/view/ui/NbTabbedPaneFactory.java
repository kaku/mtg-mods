/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.TabbedPaneFactory
 */
package org.netbeans.core.windows.view.ui;

import javax.swing.JTabbedPane;
import org.netbeans.core.windows.view.ui.CloseButtonTabbedPane;
import org.openide.awt.TabbedPaneFactory;

public class NbTabbedPaneFactory
extends TabbedPaneFactory {
    public JTabbedPane createTabbedPane() {
        return new CloseButtonTabbedPane();
    }
}

