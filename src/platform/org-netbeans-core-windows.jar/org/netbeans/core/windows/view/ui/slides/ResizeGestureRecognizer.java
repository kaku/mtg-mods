/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.windows.view.ui.slides;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.view.ui.slides.CommandManager;
import org.openide.windows.WindowManager;

public class ResizeGestureRecognizer
implements AWTEventListener {
    static final int RESIZE_BUFFER = 8;
    private boolean isResizing = false;
    private Component comp;
    private String side;
    private CommandManager mgr;
    private GlassPane glass;
    private Component oldGlass;
    private int state;
    private Point startPoint;
    private static final int STATE_NOOP = 0;
    private static final int STATE_START = 1;
    private static final int STATE_DRAGGING = 2;

    void attachResizeRecognizer(String side, Component component) {
        this.update(side, component);
        Toolkit.getDefaultToolkit().addAWTEventListener(this, 48);
    }

    void detachResizeRecognizer(String side, Component component) {
        Toolkit.getDefaultToolkit().removeAWTEventListener(this);
        this.update(null, null);
    }

    public ResizeGestureRecognizer(CommandManager mgr) {
        this.mgr = mgr;
        this.glass = new GlassPane();
    }

    public void update(String side, Component component) {
        this.side = side;
        this.comp = component;
        this.state = 0;
        this.resetState();
    }

    private boolean isInResizeArea(MouseEvent event) {
        int right;
        if (this.comp == null || this.side == null || SwingUtilities.getRoot(this.comp) == null && SwingUtilities.getWindowAncestor(this.comp) == null) {
            return false;
        }
        Point leftTop = new Point(0, 0);
        leftTop = SwingUtilities.convertPoint(this.comp, leftTop, SwingUtilities.getRoot(this.comp));
        Component comp2 = event.getComponent();
        if (!comp2.isDisplayable()) {
            return false;
        }
        Point evtPoint = SwingUtilities.convertPoint(comp2, event.getPoint(), SwingUtilities.getRoot(comp2));
        if ("bottom".equals(this.side) && evtPoint.x > leftTop.x && evtPoint.x < leftTop.x + this.comp.getBounds().width && Math.abs(evtPoint.y - leftTop.y) < 8) {
            return true;
        }
        if ("top".equals(this.side) && evtPoint.x > leftTop.x && evtPoint.x < leftTop.x + this.comp.getBounds().width && Math.abs(evtPoint.y - (leftTop.y + this.comp.getBounds().height)) < 8) {
            return true;
        }
        if ("left".equals(this.side) && evtPoint.y > leftTop.y && evtPoint.y < leftTop.y + this.comp.getBounds().height && Math.abs(evtPoint.x - (right = this.comp.getBounds().width + leftTop.x)) < 8) {
            return true;
        }
        if ("right".equals(this.side) && evtPoint.y > leftTop.y && evtPoint.y < leftTop.y + this.comp.getBounds().height && Math.abs(evtPoint.x - leftTop.x) < 8) {
            return true;
        }
        return false;
    }

    private int resize(MouseEvent event, Point dragPoint) {
        if (this.comp == null || this.side == null) {
            return 0;
        }
        Point leftTop = SwingUtilities.convertPoint(this.comp, new Point(0, 0), SwingUtilities.getRoot(this.comp));
        Point evtPoint = SwingUtilities.convertPoint(event.getComponent(), event.getPoint(), SwingUtilities.getRoot(event.getComponent()));
        if (("bottom".equals(this.side) || "top".equals(this.side)) && evtPoint.x > leftTop.x && evtPoint.x < leftTop.x + this.comp.getBounds().width) {
            return evtPoint.y - dragPoint.y;
        }
        if (("left".equals(this.side) || "right".equals(this.side)) && evtPoint.y > leftTop.y && evtPoint.y < leftTop.y + this.comp.getBounds().height) {
            return evtPoint.x - dragPoint.x;
        }
        return 0;
    }

    @Override
    public void eventDispatched(AWTEvent aWTEvent) {
        if (!(aWTEvent.getSource() instanceof Component)) {
            return;
        }
        if (this.comp == null || this.side == null) {
            this.state = 0;
            this.resetState();
            return;
        }
        MouseEvent evt = (MouseEvent)aWTEvent;
        if (evt.getSource() instanceof JPopupMenu || evt.getSource() instanceof JMenuItem) {
            return;
        }
        Frame mainWindow = WindowManager.getDefault().getMainWindow();
        if (!mainWindow.equals(evt.getComponent()) && !mainWindow.equals(SwingUtilities.getWindowAncestor(evt.getComponent()))) {
            return;
        }
        if (evt.getID() == 503) {
            boolean noModif;
            boolean bl = noModif = evt.getModifiersEx() == 0;
            if (noModif && this.isInResizeArea(evt)) {
                if (this.state == 0) {
                    this.state = 1;
                    JRootPane pane = SwingUtilities.getRootPane(this.comp);
                    this.oldGlass = pane.getGlassPane();
                    this.glass.setCursor(this.side);
                    this.comp.setCursor("bottom".equals(this.side) || "top".equals(this.side) ? Cursor.getPredefinedCursor(8) : Cursor.getPredefinedCursor(11));
                    pane.setGlassPane(this.glass);
                    this.glass.setVisible(true);
                }
                return;
            }
            if (this.state != 0) {
                this.resetState();
            }
            return;
        }
        if (evt.getID() == 501 && this.state == 1) {
            boolean button1;
            boolean bl = button1 = (evt.getModifiersEx() & 1024) == 1024;
            if (button1 && this.isInResizeArea(evt)) {
                this.state = 2;
                this.startPoint = SwingUtilities.convertPoint(evt.getComponent(), evt.getPoint(), SwingUtilities.getRoot(evt.getComponent()));
                evt.consume();
                return;
            }
            this.resetState();
            return;
        }
        if (evt.getID() == 506 && this.state == 2) {
            boolean button1;
            int delta;
            boolean bl = button1 = (evt.getModifiersEx() & 1024) == 1024;
            if (button1 && this.startPoint != null && Math.abs(delta = this.resize(evt, this.startPoint)) > 3) {
                this.startPoint = SwingUtilities.convertPoint(evt.getComponent(), evt.getPoint(), SwingUtilities.getRoot(evt.getComponent()));
                this.mgr.slideResize(delta);
            }
            return;
        }
    }

    public boolean isDragging() {
        return this.state == 2;
    }

    private void resetState() {
        JComponent current;
        this.state = 0;
        JRootPane pane = SwingUtilities.getRootPane(this.comp);
        this.glass.setVisible(false);
        if (pane != null && this.oldGlass != null && (current = (JComponent)pane.getGlassPane()) instanceof GlassPane) {
            pane.setGlassPane(this.oldGlass);
        }
        if (null != this.comp) {
            this.comp.setCursor(null);
        }
        this.oldGlass = null;
        this.startPoint = null;
    }

    private class GlassPane
    extends JPanel {
        private MouseListener list;

        public GlassPane() {
            this.list = new MouseAdapter(){};
            this.setOpaque(false);
            this.putClientProperty("dontActivate", Boolean.TRUE);
            this.addMouseListener(this.list);
        }

        public void setCursor(String side) {
            this.setCursor("bottom".equals(side) || "top".equals(side) ? Cursor.getPredefinedCursor(8) : Cursor.getPredefinedCursor(11));
        }

    }

}

