/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view.ui.slides;

import java.awt.AlphaComposite;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.ImageObserver;
import javax.swing.JComponent;
import javax.swing.JLayeredPane;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.core.windows.view.ui.slides.SlideOperation;
import org.netbeans.core.windows.view.ui.slides.SlidingFx;

final class ScaleFx
implements SlidingFx,
ActionListener {
    private float initialAlpha = 0.1f;
    private float finishAlpha = 0.9f;
    private int iterCount = 9;
    private int curIter = 0;
    private static final float DIVIDING_FACTOR = 2.0f;
    private static final int FRAME_DELAY = 20;
    private Rectangle middle = new Rectangle();
    private Rectangle current = new Rectangle();
    private Timer timer = null;
    private StretchedImageComp stretchedImage;
    private Rectangle[] path;
    private JLayeredPane pane;
    private SlideOperation operation;
    private Image preparedImage;
    private ChangeListener finishL;
    private final boolean shouldOperationWait;

    public ScaleFx(float initialAlpha, float finishAlpha, boolean shouldOperationWait) {
        this.stretchedImage = new StretchedImageComp();
        this.setTransparency(initialAlpha, finishAlpha);
        this.shouldOperationWait = shouldOperationWait;
    }

    @Override
    public void prepareEffect(SlideOperation operation) {
        Component comp = operation.getComponent();
        this.preparedImage = this.createCompImage(operation.getComponent(), operation.getComponent().getSize());
    }

    @Override
    public void showEffect(JLayeredPane pane, Integer layer, SlideOperation operation) {
        this.pane = pane;
        this.operation = operation;
        Component comp = operation.getComponent();
        Graphics2D gr2d = (Graphics2D)pane.getGraphics();
        Rectangle start = operation.getStartBounds();
        Rectangle finish = operation.getFinishBounds();
        Dimension finishSize = finish.getSize();
        Dimension startSize = start.getSize();
        Rectangle current = start;
        Image compImage = this.preparedImage;
        pane.add((Component)this.stretchedImage, layer);
        this.path = this.computePath(start, finish);
        this.curIter = 1;
        if (compImage != null) {
            this.stretchedImage.setOrigImage(compImage);
        } else if (finishSize.width * finishSize.height > startSize.width * startSize.height) {
            this.stretchedImage.setComp(comp, finishSize);
        } else {
            this.stretchedImage.setComp(comp, startSize);
        }
        this.stretchedImage.setBoundsAndAlpha(start, this.initialAlpha);
        this.getTimer().start();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        float coef = (float)this.curIter / (float)(this.iterCount - 1);
        float curAlpha = (1.0f - coef) * this.initialAlpha + coef * this.finishAlpha;
        this.stretchedImage.setBoundsAndAlpha(this.path[this.curIter], curAlpha);
        ++this.curIter;
        if (this.curIter >= this.iterCount) {
            this.getTimer().stop();
            this.finish();
        }
    }

    private void finish() {
        this.pane.remove(this.stretchedImage);
        this.stretchedImage.cleanup();
        if (this.finishL != null) {
            this.finishL.stateChanged(null);
        }
    }

    public void setTransparency(float initialAlpha, float finishAlpha) {
        this.initialAlpha = initialAlpha;
        this.finishAlpha = finishAlpha;
    }

    private void setSuggestedIterations(int count) {
        if (count < 3) {
            count = 3;
        }
        this.iterCount = count % 2 == 0 ? count + 1 : count;
    }

    private Rectangle[] computePath(Rectangle start, Rectangle finish) {
        int i;
        Rectangle[] path = new Rectangle[this.iterCount];
        this.middle.x = Math.abs((finish.x + start.x) / 2);
        this.middle.y = Math.abs((finish.y + start.y) / 2);
        this.middle.width = Math.abs((finish.width + start.width) / 2);
        this.middle.height = Math.abs((finish.height + start.height) / 2);
        this.current = new Rectangle(this.middle);
        for (i = this.iterCount / 2 - 1; i >= 0; --i) {
            this.current.x = (int)Math.abs((float)(this.current.x + start.x) / 2.0f);
            this.current.y = (int)Math.abs((float)(this.current.y + start.y) / 2.0f);
            this.current.width = (int)Math.abs((float)(this.current.width + start.width) / 2.0f);
            this.current.height = (int)Math.abs((float)(this.current.height + start.height) / 2.0f);
            path[i] = new Rectangle(this.current);
        }
        path[this.iterCount / 2] = new Rectangle(this.middle);
        this.current = this.middle;
        for (i = this.iterCount / 2 + 1; i < this.iterCount; ++i) {
            this.current.x = (int)Math.abs((float)(this.current.x + finish.x) / 2.0f);
            this.current.y = (int)Math.abs((float)(this.current.y + finish.y) / 2.0f);
            this.current.width = (int)Math.abs((float)(this.current.width + finish.width) / 2.0f);
            this.current.height = (int)Math.abs((float)(this.current.height + finish.height) / 2.0f);
            path[i] = new Rectangle(this.current);
        }
        return path;
    }

    private Image createCompImage(Component comp, Dimension targetSize) {
        if (!comp.isShowing()) {
            return null;
        }
        Image image = comp.createImage(comp.getWidth(), comp.getHeight());
        Graphics2D gr2d = (Graphics2D)image.getGraphics();
        comp.paint(gr2d);
        gr2d.dispose();
        return image;
    }

    private Timer getTimer() {
        if (this.timer == null) {
            this.timer = new Timer(20, this);
            this.timer.setRepeats(true);
        }
        return this.timer;
    }

    @Override
    public void setFinishListener(ChangeListener finishL) {
        this.finishL = finishL;
    }

    @Override
    public boolean shouldOperationWait() {
        return this.shouldOperationWait;
    }

    private class StretchedImageComp
    extends JComponent {
        private Image origImage;
        private float alpha;
        private Component comp;
        private Dimension scaleSource;
        private Dimension targetSize;

        private StretchedImageComp() {
            this.alpha = 1.0f;
        }

        public void setComp(Component comp, Dimension targetSize) {
            this.comp = comp;
            this.targetSize = targetSize;
        }

        public void setOrigImage(Image origImage) {
            this.origImage = origImage;
        }

        public void setScaleSource(Dimension scaleSource) {
            this.scaleSource = scaleSource;
        }

        public void setBoundsAndAlpha(Rectangle bounds, float alpha) {
            this.alpha = alpha;
            this.setBounds(bounds);
            if (this.origImage == null) {
                this.origImage = this.tryCreateImage();
            }
        }

        private Image tryCreateImage() {
            Image result = null;
            if (this.comp != null && this.isDisplayable()) {
                this.comp.setSize(this.targetSize);
                this.add(this.comp);
                result = ScaleFx.this.createCompImage(this.comp, this.targetSize);
                this.remove(this.comp);
            }
            return result;
        }

        public void cleanup() {
            this.comp = null;
            this.origImage = null;
            this.scaleSource = null;
            this.targetSize = null;
        }

        @Override
        public void paint(Graphics g) {
            Rectangle bounds = this.getBounds();
            if (this.origImage == null) {
                if (this.comp == null) {
                    return;
                }
                this.origImage = this.tryCreateImage();
                if (this.origImage == null) {
                    return;
                }
            }
            Image img = this.origImage;
            Graphics2D g2d = (Graphics2D)g;
            Composite origComposite = g2d.getComposite();
            g2d.setComposite(AlphaComposite.getInstance(3, this.alpha));
            g2d.drawImage(img, 0, 0, bounds.width, bounds.height, null);
            if (origComposite != null) {
                g2d.setComposite(origComposite);
            }
        }
    }

}

