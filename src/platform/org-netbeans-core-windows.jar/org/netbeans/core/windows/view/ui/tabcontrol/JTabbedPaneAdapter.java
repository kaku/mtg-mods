/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.ComponentConverter
 *  org.netbeans.swing.tabcontrol.TabDataModel
 *  org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer
 *  org.netbeans.swing.tabcontrol.customtabs.Tabbed
 *  org.netbeans.swing.tabcontrol.customtabs.Tabbed$Accessor
 *  org.netbeans.swing.tabcontrol.customtabs.TabbedType
 *  org.netbeans.swing.tabcontrol.event.TabActionEvent
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view.ui.tabcontrol;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionListener;
import java.awt.geom.Area;
import javax.swing.Icon;
import javax.swing.SingleSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.core.windows.view.ui.slides.SlideController;
import org.netbeans.core.windows.view.ui.tabcontrol.AbstractTabbedImpl;
import org.netbeans.core.windows.view.ui.tabcontrol.tabbedpane.CloseableTabComponent;
import org.netbeans.core.windows.view.ui.tabcontrol.tabbedpane.NBTabbedPane;
import org.netbeans.core.windows.view.ui.tabcontrol.tabbedpane.NBTabbedPaneController;
import org.netbeans.swing.tabcontrol.ComponentConverter;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.customtabs.Tabbed;
import org.netbeans.swing.tabcontrol.customtabs.TabbedType;
import org.netbeans.swing.tabcontrol.event.TabActionEvent;
import org.openide.windows.TopComponent;

public class JTabbedPaneAdapter
extends NBTabbedPane
implements Tabbed.Accessor,
SlideController {
    private NBTabbedPaneController controller;
    private final AbstractTabbedImpl tabbedImpl;

    public JTabbedPaneAdapter(TabbedType type, WinsysInfoForTabbedContainer info) {
        super(null, type, info);
        this.tabbedImpl = new AbstractTabbedImpl(){

            public int getTabCount() {
                return JTabbedPaneAdapter.this.getTabCount();
            }

            public int indexOf(Component tc) {
                return JTabbedPaneAdapter.this.indexOf(tc);
            }

            public void setTitleAt(int index, String title) {
                CloseableTabComponent ctc = (CloseableTabComponent)JTabbedPaneAdapter.this.getTabComponentAt(index);
                ctc.setTitle(title);
                this.getTabModel().setText(index, title);
            }

            public void setIconAt(int index, Icon icon) {
                CloseableTabComponent ctc = (CloseableTabComponent)JTabbedPaneAdapter.this.getTabComponentAt(index);
                ctc.setIcon(icon);
            }

            public void setToolTipTextAt(int index, String toolTip) {
                CloseableTabComponent ctc = (CloseableTabComponent)JTabbedPaneAdapter.this.getTabComponentAt(index);
                ctc.setTooltip(toolTip);
            }

            public void addActionListener(ActionListener al) {
                JTabbedPaneAdapter.this.addActionListener(al);
            }

            public void removeActionListener(ActionListener al) {
                JTabbedPaneAdapter.this.removeActionListener(al);
            }

            public void setActive(boolean active) {
                JTabbedPaneAdapter.this.setActive(active);
            }

            public int tabForCoordinate(Point p) {
                return JTabbedPaneAdapter.this.tabForCoordinate(p);
            }

            public Image createImageOfTab(int tabIndex) {
                return JTabbedPaneAdapter.this.createImageOfTab(tabIndex);
            }

            public Component getComponent() {
                return JTabbedPaneAdapter.this;
            }

            public Rectangle getTabBounds(int tabIndex) {
                return JTabbedPaneAdapter.this.getBoundsAt(tabIndex);
            }

            public Rectangle getTabsArea() {
                return JTabbedPaneAdapter.this.getBounds();
            }

            public boolean isTransparent() {
                return false;
            }

            public void setTransparent(boolean transparent) {
            }

            @Override
            protected TabDataModel getTabModel() {
                return JTabbedPaneAdapter.this.getDataModel();
            }

            @Override
            protected SingleSelectionModel getSelectionModel() {
                return JTabbedPaneAdapter.this.getModel();
            }

            @Override
            protected void requestAttention(int tabIndex) {
                JTabbedPaneAdapter.this.requestAttention(tabIndex);
            }

            @Override
            protected void cancelRequestAttention(int tabIndex) {
                JTabbedPaneAdapter.this.cancelRequestAttention(tabIndex);
            }

            @Override
            protected void setAttentionHighlight(int tabIndex, boolean highlight) {
                JTabbedPaneAdapter.this.setAttentionHighlight(tabIndex, highlight);
            }

            @Override
            protected int dropIndexOfPoint(Point location) {
                return JTabbedPaneAdapter.this.dropIndexOfPoint(location);
            }

            @Override
            protected ComponentConverter getComponentConverter() {
                return JTabbedPaneAdapter.this.getComponentConverter();
            }

            @Override
            protected Shape getDropIndication(TopComponent draggedTC, Point location) {
                return JTabbedPaneAdapter.this.getDropIndication((Object)draggedTC, location);
            }
        };
        this.controller = new NBTabbedPaneController(this);
        this.controller.attachModelAndSelectionListeners();
        this.getModel().addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent ce) {
                int idx = JTabbedPaneAdapter.this.getModel().getSelectedIndex();
                if (idx != -1) {
                    JTabbedPaneAdapter.this.tabbedImpl.fireStateChanged();
                }
            }
        });
    }

    public Shape getDropIndication(Object dragged, Point location) {
        int over = this.dropIndexOfPoint(location);
        Rectangle component = this.getSelectedComponent().getBounds();
        Area selectedComponent = new Area(component);
        Rectangle firstTab = null;
        Rectangle secondTab = null;
        if (over > 0 && over < this.getTabCount()) {
            firstTab = this.getBoundsAt(over - 1);
        }
        if (over < this.getTabCount()) {
            secondTab = this.getBoundsAt(over);
        }
        if (over >= this.getTabCount()) {
            firstTab = this.getBoundsAt(this.getTabCount() - 1);
            secondTab = null;
        }
        Rectangle joined = this.joinTabAreas(firstTab, secondTab);
        Area t = new Area(joined);
        selectedComponent.add(t);
        return selectedComponent;
    }

    private Rectangle joinTabAreas(Rectangle firstTab, Rectangle secondTab) {
        assert (null != firstTab || null != secondTab);
        Rectangle res = new Rectangle();
        switch (this.getTabPlacement()) {
            case 1: 
            case 3: {
                if (null != firstTab && null != secondTab && firstTab.y != secondTab.y) {
                    firstTab = null;
                }
                if (null == firstTab) {
                    res.height = secondTab.height;
                    res.y = secondTab.y;
                    res.x = secondTab.x;
                    res.width = secondTab.width / 2;
                    break;
                }
                if (null == secondTab) {
                    res.height = firstTab.height;
                    res.y = firstTab.y;
                    res.x = firstTab.x + firstTab.width / 2;
                    res.width = firstTab.width / 2;
                    break;
                }
                res.height = firstTab.height;
                res.y = firstTab.y;
                res.x = firstTab.x + firstTab.width / 2;
                res.width = firstTab.width / 2 + secondTab.width / 2;
                break;
            }
            case 2: 
            case 4: {
                if (null != firstTab && null != secondTab && firstTab.x != secondTab.x) {
                    firstTab = null;
                }
                if (null == firstTab) {
                    res.width = secondTab.width;
                    res.y = secondTab.y;
                    res.x = secondTab.x;
                    res.height = secondTab.height / 2;
                    break;
                }
                if (null == secondTab) {
                    res.width = firstTab.width;
                    res.x = firstTab.x;
                    res.y = firstTab.y + firstTab.height / 2;
                    res.height = firstTab.height / 2;
                    break;
                }
                res.width = firstTab.width;
                res.x = firstTab.x;
                res.y = firstTab.y + firstTab.height / 2;
                res.height = firstTab.height / 2 + secondTab.height / 2;
            }
        }
        return res;
    }

    public Tabbed getTabbed() {
        return this.tabbedImpl;
    }

    @Override
    public void userToggledAutoHide(int tabIndex, boolean enabled) {
        this.postActionEvent(new TabActionEvent((Object)this, "enableAutoHide", tabIndex));
    }

    @Override
    public void userToggledTransparency(int tabIndex) {
        this.postActionEvent(new TabActionEvent((Object)this, "toggleTransparency", tabIndex));
    }

    @Override
    public Dimension getMinimumSize() {
        return new Dimension(10, 10);
    }

}

