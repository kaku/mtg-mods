/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view;

import java.awt.Component;
import java.awt.Dimension;
import org.netbeans.core.windows.view.Controller;

public abstract class ViewElement {
    private final Controller controller;
    private final double resizeWeight;

    public ViewElement(Controller controller, double resizeWeight) {
        this.controller = controller;
        this.resizeWeight = resizeWeight;
    }

    public final Controller getController() {
        return this.controller;
    }

    public abstract Component getComponent();

    public final double getResizeWeight() {
        return this.resizeWeight;
    }

    public abstract boolean updateAWTHierarchy(Dimension var1);
}

