/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.ToolbarPool
 *  org.openide.util.WeakSet
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.Debug;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.WindowSystemSnapshot;
import org.netbeans.core.windows.view.Controller;
import org.netbeans.core.windows.view.ControllerHandler;
import org.netbeans.core.windows.view.EditorView;
import org.netbeans.core.windows.view.ElementAccessor;
import org.netbeans.core.windows.view.ModeAccessor;
import org.netbeans.core.windows.view.ModeStructureAccessor;
import org.netbeans.core.windows.view.ModeView;
import org.netbeans.core.windows.view.SlidingView;
import org.netbeans.core.windows.view.SplitAccessor;
import org.netbeans.core.windows.view.SplitView;
import org.netbeans.core.windows.view.View;
import org.netbeans.core.windows.view.ViewElement;
import org.netbeans.core.windows.view.ViewEvent;
import org.netbeans.core.windows.view.ViewHelper;
import org.netbeans.core.windows.view.ViewHierarchy;
import org.netbeans.core.windows.view.WindowSystemAccessor;
import org.netbeans.core.windows.view.dnd.TopComponentDraggable;
import org.netbeans.core.windows.view.dnd.WindowDnDManager;
import org.netbeans.core.windows.view.ui.MainWindow;
import org.netbeans.core.windows.view.ui.slides.SlideOperation;
import org.openide.awt.ToolbarPool;
import org.openide.util.WeakSet;
import org.openide.windows.TopComponent;

class DefaultView
implements View,
Controller,
WindowDnDManager.ViewAccessor {
    private final ViewHierarchy hierarchy;
    private final ControllerHandler controllerHandler;
    private final Set<TopComponent> showingTopComponents;
    private static final boolean DEBUG = Debug.isLoggable(DefaultView.class);

    public DefaultView(ControllerHandler controllerHandler) {
        this.hierarchy = new ViewHierarchy(this, new WindowDnDManager(this));
        this.showingTopComponents = new WeakSet(10);
        this.controllerHandler = controllerHandler;
    }

    @Override
    public boolean isDragInProgress() {
        return this.hierarchy.isDragInProgress();
    }

    @Override
    public Frame getMainWindow() {
        return this.hierarchy.getMainWindow().getFrame();
    }

    @Override
    public Component getEditorAreaComponent() {
        return this.hierarchy.getEditorAreaComponent();
    }

    @Override
    public String guessSlideSide(TopComponent comp) {
        String toReturn = "left";
        if (this.hierarchy.getMaximizedModeView() != null) {
            toReturn = (String)comp.getClientProperty((Object)"lastSlideSide");
            if (toReturn == null) {
                toReturn = "left";
            }
        } else {
            Rectangle editorb = this.hierarchy.getPureEditorAreaBounds();
            Point leftTop = new Point(0, 0);
            SwingUtilities.convertPointToScreen(leftTop, (Component)comp);
            if (editorb.x > leftTop.x) {
                toReturn = "left";
                comp.putClientProperty((Object)"lastSlideSide", (Object)toReturn);
            }
            if (editorb.x + editorb.width < leftTop.x) {
                toReturn = "right";
                comp.putClientProperty((Object)"lastSlideSide", (Object)toReturn);
            }
            if (editorb.y + editorb.height < leftTop.y) {
                toReturn = "bottom";
                comp.putClientProperty((Object)"lastSlideSide", (Object)toReturn);
            }
        }
        return toReturn;
    }

    @Override
    public void changeGUI(ViewEvent[] viewEvents, WindowSystemSnapshot snapshot) {
        int i;
        ViewEvent viewEvent;
        int changeType;
        WindowSystemAccessor wsa = ViewHelper.createWindowSystemAccessor(snapshot);
        if (DEBUG) {
            DefaultView.debugLog("CHANGEGUI()");
            DefaultView.debugLog("Structure=" + wsa);
            DefaultView.debugLog("");
        }
        if (wsa != null) {
            this.hierarchy.updateViewHierarchy(wsa.getModeStructureAccessor());
        }
        HashSet<TopComponent> oldShowing = new HashSet<TopComponent>(this.showingTopComponents);
        Set<TopComponent> newShowing = this.hierarchy.getShowingTopComponents();
        this.showingTopComponents.clear();
        this.showingTopComponents.addAll(newShowing);
        HashSet<TopComponent> toShow = new HashSet<TopComponent>(newShowing);
        toShow.removeAll(oldShowing);
        for (TopComponent tc : toShow) {
            WindowManagerImpl.getInstance().componentShowing(tc);
        }
        if (DEBUG) {
            DefaultView.debugLog("ChangeGUI: Checking view events...");
        }
        for (i = 0; i < viewEvents.length; ++i) {
            viewEvent = viewEvents[i];
            changeType = viewEvent.getType();
            if (DEBUG) {
                DefaultView.debugLog("ViewEvent=" + viewEvent);
            }
            if (changeType != 0) continue;
            if (DEBUG) {
                DefaultView.debugLog("Winsys visibility changed, visible=" + viewEvent.getNewValue());
            }
            this.windowSystemVisibilityChanged((Boolean)viewEvent.getNewValue(), wsa);
            return;
        }
        for (i = 0; i < viewEvents.length; ++i) {
            ModeView modeView;
            Rectangle bounds;
            TopComponent tc2;
            viewEvent = viewEvents[i];
            changeType = viewEvent.getType();
            if (changeType == 1) {
                if (DEBUG) {
                    DefaultView.debugLog("Main window bounds joined changed");
                }
                if (wsa.getEditorAreaState() != 0 || (bounds = (Rectangle)viewEvent.getNewValue()) == null) continue;
                this.hierarchy.getMainWindow().setBounds(bounds);
                continue;
            }
            if (changeType == 2) {
                if (DEBUG) {
                    DefaultView.debugLog("Main window bounds separated changed");
                }
                if (wsa.getEditorAreaState() != 1 || (bounds = (Rectangle)viewEvent.getNewValue()) == null) continue;
                this.hierarchy.getMainWindow().setBounds(bounds);
                continue;
            }
            if (changeType == 3) {
                if (DEBUG) {
                    DefaultView.debugLog("Main window frame state joined changed");
                }
                if (wsa.getEditorAreaState() != 0) continue;
                this.hierarchy.getMainWindow().setExtendedState(wsa.getMainWindowFrameStateJoined());
                continue;
            }
            if (changeType == 4) {
                if (DEBUG) {
                    DefaultView.debugLog("Main window frame state separated changed");
                }
                if (wsa.getEditorAreaState() != 1) continue;
                this.hierarchy.getMainWindow().setExtendedState(wsa.getMainWindowFrameStateSeparated());
                continue;
            }
            if (changeType == 5) {
                if (DEBUG) {
                    DefaultView.debugLog("Editor area state changed");
                }
                this.hierarchy.updateMainWindowBounds(wsa);
                this.hierarchy.updateDesktop(wsa);
                this.hierarchy.setSeparateModesVisible(true);
                continue;
            }
            if (changeType == 6) {
                if (DEBUG) {
                    DefaultView.debugLog("Editor area frame state changed");
                }
                this.hierarchy.updateEditorAreaFrameState(wsa.getEditorAreaFrameState());
                continue;
            }
            if (changeType == 7) {
                if (DEBUG) {
                    DefaultView.debugLog("Editor area bounds changed");
                }
                this.hierarchy.updateEditorAreaBounds((Rectangle)viewEvent.getNewValue());
                continue;
            }
            if (changeType == 8) {
                if (DEBUG) {
                    DefaultView.debugLog("Editor area constraints changed");
                }
                this.hierarchy.updateDesktop(wsa);
                continue;
            }
            if (changeType == 9) {
                if (DEBUG) {
                    DefaultView.debugLog("Active mode changed, mode=" + viewEvent.getNewValue());
                }
                this.hierarchy.updateDesktop(wsa);
                this.hierarchy.activateMode(wsa.getActiveModeAccessor());
                continue;
            }
            if (changeType == 10) {
                if (DEBUG) {
                    DefaultView.debugLog("Toolbar config name changed");
                }
                ToolbarPool.getDefault().setConfiguration(wsa.getToolbarConfigurationName());
                continue;
            }
            if (changeType == 11) {
                if (DEBUG) {
                    DefaultView.debugLog("Maximized mode changed");
                }
                this.hierarchy.setMaximizedModeView(this.hierarchy.getModeViewForAccessor(wsa.getMaximizedModeAccessor()));
                this.hierarchy.updateDesktop(wsa);
                this.hierarchy.activateMode(wsa.getActiveModeAccessor());
                continue;
            }
            if (changeType == 12) {
                if (DEBUG) {
                    DefaultView.debugLog("Mode added");
                }
                this.hierarchy.updateDesktop(wsa);
                continue;
            }
            if (changeType == 13) {
                if (DEBUG) {
                    DefaultView.debugLog("Mode removed");
                }
                this.hierarchy.updateDesktop(wsa);
                this.hierarchy.activateMode(wsa.getActiveModeAccessor());
                continue;
            }
            if (changeType == 14) {
                if (DEBUG) {
                    DefaultView.debugLog("Mode constraints changed");
                }
                this.hierarchy.updateDesktop(wsa);
                continue;
            }
            if (changeType == 20) {
                if (DEBUG) {
                    DefaultView.debugLog("Mode bounds changed");
                }
                if ((modeView = this.hierarchy.getModeViewForAccessor(wsa.findModeAccessor((String)viewEvent.getSource()))) == null) continue;
                modeView.getComponent().setBounds((Rectangle)viewEvent.getNewValue());
                continue;
            }
            if (changeType == 21) {
                if (DEBUG) {
                    DefaultView.debugLog("Mode state changed");
                }
                if ((modeView = this.hierarchy.getModeViewForAccessor(wsa.findModeAccessor((String)viewEvent.getSource()))) == null) continue;
                modeView.setFrameState((Integer)viewEvent.getNewValue());
                modeView.updateFrameState();
                continue;
            }
            if (changeType == 22) {
                if (DEBUG) {
                    DefaultView.debugLog("Selected topcomponent changed, tc=" + viewEvent.getNewValue());
                }
                this.hierarchy.updateDesktop(wsa);
                this.hierarchy.activateMode(wsa.getActiveModeAccessor());
                continue;
            }
            if (changeType == 23) {
                if (DEBUG) {
                    DefaultView.debugLog("TopComponent added");
                }
                this.hierarchy.updateDesktop(wsa);
                this.hierarchy.setSeparateModesVisible(true);
                modeView = this.hierarchy.getModeViewForAccessor(wsa.findModeAccessor((String)viewEvent.getSource()));
                if (modeView == null) continue;
                modeView.updateFrameState();
                continue;
            }
            if (changeType == 24) {
                if (DEBUG) {
                    DefaultView.debugLog("TopComponent removed");
                }
                this.hierarchy.setMaximizedModeView(this.hierarchy.getModeViewForAccessor(wsa.getMaximizedModeAccessor()));
                this.hierarchy.updateDesktop(wsa);
                modeView = this.hierarchy.getModeViewForAccessor(wsa.findModeAccessor((String)viewEvent.getSource()));
                if (modeView != null) {
                    modeView.removeTopComponent((TopComponent)viewEvent.getNewValue());
                }
                this.hierarchy.activateMode(wsa.getActiveModeAccessor());
                continue;
            }
            if (changeType == 31) {
                if (DEBUG) {
                    DefaultView.debugLog("TopComponent display name changed, tc=" + viewEvent.getNewValue());
                }
                if ((modeView = this.hierarchy.getModeViewForAccessor(wsa.findModeAccessor((String)viewEvent.getSource()))) == null) continue;
                modeView.updateName((TopComponent)viewEvent.getNewValue());
                continue;
            }
            if (changeType == 32) {
                if (DEBUG) {
                    DefaultView.debugLog("TopComponent display name annotation changed, tc=" + viewEvent.getNewValue());
                }
                if ((modeView = this.hierarchy.getModeViewForAccessor(wsa.findModeAccessor((String)viewEvent.getSource()))) == null) continue;
                modeView.updateName((TopComponent)viewEvent.getNewValue());
                continue;
            }
            if (changeType == 33) {
                if (DEBUG) {
                    DefaultView.debugLog("TopComponent tooltip changed, tc=" + viewEvent.getNewValue());
                }
                if ((modeView = this.hierarchy.getModeViewForAccessor(wsa.findModeAccessor((String)viewEvent.getSource()))) == null) continue;
                modeView.updateToolTip((TopComponent)viewEvent.getNewValue());
                continue;
            }
            if (changeType == 34) {
                if (DEBUG) {
                    DefaultView.debugLog("TopComponent icon changed");
                }
                if ((modeView = this.hierarchy.getModeViewForAccessor(wsa.findModeAccessor((String)viewEvent.getSource()))) == null) continue;
                modeView.updateIcon((TopComponent)viewEvent.getNewValue());
                continue;
            }
            if (changeType == 41) {
                if (DEBUG) {
                    DefaultView.debugLog("TopComponent attached");
                }
                this.hierarchy.updateDesktop(wsa);
                this.hierarchy.activateMode(wsa.getActiveModeAccessor());
                continue;
            }
            if (changeType == 42) {
                if (DEBUG) {
                    DefaultView.debugLog("TopComponent array added:" + Arrays.asList((TopComponent[])viewEvent.getNewValue()));
                }
                this.hierarchy.updateDesktop(wsa);
                continue;
            }
            if (changeType == 43) {
                if (DEBUG) {
                    DefaultView.debugLog("TopComponent array removed:" + Arrays.asList((TopComponent[])viewEvent.getNewValue()));
                }
                this.hierarchy.updateDesktop(wsa);
                this.hierarchy.activateMode(wsa.getActiveModeAccessor());
                continue;
            }
            if (changeType == 44) {
                if (DEBUG) {
                    DefaultView.debugLog("TopComponent activated, tc=" + viewEvent.getNewValue());
                }
                this.hierarchy.updateDesktop(wsa);
                this.hierarchy.activateMode(wsa.getActiveModeAccessor());
                continue;
            }
            if (changeType == 45) {
                if (DEBUG) {
                    DefaultView.debugLog("Mode closed, mode=" + viewEvent.getSource());
                }
                this.hierarchy.updateDesktop();
                continue;
            }
            if (changeType == 46) {
                if (DEBUG) {
                    DefaultView.debugLog("DnD performed");
                }
                this.hierarchy.setMaximizedModeView(this.hierarchy.getModeViewForAccessor(wsa.getMaximizedModeAccessor()));
                this.hierarchy.updateDesktop();
                this.hierarchy.activateMode(wsa.getActiveModeAccessor());
                continue;
            }
            if (changeType == 61) {
                if (DEBUG) {
                    DefaultView.debugLog("UI update");
                }
                this.hierarchy.updateUI();
                continue;
            }
            if (changeType == 47 || changeType == 48) {
                if (DEBUG) {
                    DefaultView.debugLog("Top Component Auto Hide changed");
                }
                this.hierarchy.setMaximizedModeView(this.hierarchy.getModeViewForAccessor(wsa.getMaximizedModeAccessor()));
                this.hierarchy.updateDesktop(wsa);
                this.hierarchy.activateMode(wsa.getActiveModeAccessor());
                continue;
            }
            if (changeType == 63) {
                if (DEBUG) {
                    DefaultView.debugLog("Top component request attention");
                }
                if ((modeView = this.hierarchy.getModeViewForAccessor(wsa.findModeAccessor((String)viewEvent.getSource()))) != null) {
                    tc2 = (TopComponent)viewEvent.getNewValue();
                    if (tc2 == null) {
                        throw new NullPointerException("Top component is null for attention request");
                    }
                    modeView.requestAttention(tc2);
                    continue;
                }
                Logger.getLogger(DefaultView.class.getName()).fine("Could not find mode " + viewEvent.getSource());
                continue;
            }
            if (changeType == 64) {
                if (DEBUG) {
                    DefaultView.debugLog("Top component cancel request attention");
                }
                if ((modeView = this.hierarchy.getModeViewForAccessor(wsa.findModeAccessor((String)viewEvent.getSource()))) != null) {
                    tc2 = (TopComponent)viewEvent.getNewValue();
                    if (tc2 == null) {
                        throw new NullPointerException("Top component is null for attention cancellation request");
                    }
                    if (!modeView.getTopComponents().contains((Object)tc2)) continue;
                    modeView.cancelRequestAttention(tc2);
                    continue;
                }
                Logger.getLogger(DefaultView.class.getName()).fine("Could not find mode " + viewEvent.getSource());
                continue;
            }
            if (changeType == 65 || changeType == 66) {
                if (DEBUG) {
                    DefaultView.debugLog("Top component attention highlight");
                }
                if ((modeView = this.hierarchy.getModeViewForAccessor(wsa.findModeAccessor((String)viewEvent.getSource()))) != null) {
                    tc2 = (TopComponent)viewEvent.getNewValue();
                    if (tc2 == null) {
                        throw new NullPointerException("Top component is null for attention cancellation request");
                    }
                    if (!modeView.getTopComponents().contains((Object)tc2)) continue;
                    modeView.setAttentionHighlight(tc2, changeType == 65);
                    continue;
                }
                Logger.getLogger(DefaultView.class.getName()).fine("Could not find mode " + viewEvent.getSource());
                continue;
            }
            if (changeType == 70 || changeType == 71) {
                if (DEBUG) {
                    DefaultView.debugLog("Top component show/hide busy");
                }
                if ((modeView = this.hierarchy.getModeViewForAccessor(wsa.findModeAccessor((String)viewEvent.getSource()))) != null) {
                    tc2 = (TopComponent)viewEvent.getNewValue();
                    if (tc2 == null) {
                        throw new NullPointerException("Top component is null for make busy request");
                    }
                    if (!modeView.getTopComponents().contains((Object)tc2)) continue;
                    modeView.makeBusy(tc2, changeType == 70);
                    continue;
                }
                Logger.getLogger(DefaultView.class.getName()).fine("Could not find mode " + viewEvent.getSource());
                continue;
            }
            if (changeType != 67) continue;
            if (DEBUG) {
                DefaultView.debugLog("Slided-in top component toggle maximize");
            }
            TopComponent tc3 = (TopComponent)viewEvent.getSource();
            String side = (String)viewEvent.getNewValue();
            this.hierarchy.performSlideToggleMaximize(tc3, side);
        }
        HashSet<TopComponent> toHide = new HashSet<TopComponent>(oldShowing);
        toHide.removeAll(newShowing);
        for (TopComponent tc4 : toHide) {
            WindowManagerImpl.getInstance().componentHidden(tc4);
        }
    }

    private void windowSystemVisibilityChanged(boolean visible, WindowSystemAccessor wsa) {
        if (visible) {
            this.showWindowSystem(wsa);
        } else {
            this.hideWindowSystem();
        }
    }

    private void showWindowSystem(WindowSystemAccessor wsa) {
        long start = System.currentTimeMillis();
        if (DEBUG) {
            DefaultView.debugLog("ShowWindowSystem--");
        }
        this.hierarchy.getMainWindow().initializeComponents();
        JFrame frame = this.hierarchy.getMainWindow().getFrame();
        ToolbarPool.getDefault().setConfiguration(wsa.getToolbarConfigurationName());
        if (DEBUG) {
            DefaultView.debugLog(wsa.getModeStructureAccessor().toString());
        }
        this.hierarchy.getMainWindow().prepareWindow();
        if (DEBUG) {
            DefaultView.debugLog("Init view 4=" + (System.currentTimeMillis() - start) + " ms");
        }
        if (DEBUG) {
            DefaultView.debugLog("Init view 2=" + (System.currentTimeMillis() - start) + " ms");
        }
        this.hierarchy.setSplitModesVisible(true);
        if (DEBUG) {
            DefaultView.debugLog("Init view 3=" + (System.currentTimeMillis() - start) + " ms");
        }
        if (wsa.getEditorAreaState() == 0) {
            this.hierarchy.getMainWindow().setExtendedState(wsa.getMainWindowFrameStateJoined());
        } else {
            this.hierarchy.getMainWindow().setExtendedState(wsa.getMainWindowFrameStateSeparated());
        }
        this.hierarchy.getMainWindow().setVisible(true);
        this.hierarchy.setMaximizedModeView(this.hierarchy.getModeViewForAccessor(wsa.getMaximizedModeAccessor()));
        this.hierarchy.updateDesktop(wsa);
        this.hierarchy.setSeparateModesVisible(true);
        this.hierarchy.updateEditorAreaFrameState(wsa.getEditorAreaFrameState());
        this.hierarchy.updateFrameStates();
        if (wsa.getEditorAreaState() == 0 && frame.getExtendedState() != 6) {
            if (DEBUG) {
                DefaultView.debugLog("do updateMainWindowBoundsSeparatedHelp");
            }
            this.updateMainWindowBoundsSeparatedHelp();
            this.updateEditorAreaBoundsHelp();
        }
        this.hierarchy.activateMode(wsa.getActiveModeAccessor());
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                if (DEBUG) {
                    DefaultView.debugLog("Installing main window listeners.");
                }
                DefaultView.this.hierarchy.installMainWindowListeners();
            }
        });
        if (DEBUG) {
            DefaultView.debugLog("Init view 5=" + (System.currentTimeMillis() - start) + " ms");
        }
    }

    private void hideWindowSystem() {
        this.hierarchy.uninstallMainWindowListeners();
        this.hierarchy.setSeparateModesVisible(false);
        this.hierarchy.getMainWindow().setVisible(false);
        this.hierarchy.releaseAll();
    }

    @Override
    public void userActivatedModeView(ModeView modeView) {
        if (DEBUG) {
            DefaultView.debugLog("User activated mode view, mode=" + modeView);
        }
        ModeAccessor modeAccessor = (ModeAccessor)this.hierarchy.getAccessorForView(modeView);
        ModeImpl mode = DefaultView.getModeForModeAccessor(modeAccessor);
        this.controllerHandler.userActivatedMode(mode);
    }

    @Override
    public void userActivatedModeWindow(ModeView modeView) {
        if (DEBUG) {
            DefaultView.debugLog("User activated mode window, mode=" + modeView);
        }
        ModeAccessor modeAccessor = (ModeAccessor)this.hierarchy.getAccessorForView(modeView);
        ModeImpl mode = DefaultView.getModeForModeAccessor(modeAccessor);
        this.controllerHandler.userActivatedModeWindow(mode);
    }

    @Override
    public void userActivatedEditorWindow() {
        if (DEBUG) {
            DefaultView.debugLog("User activated editor window");
        }
        this.controllerHandler.userActivatedEditorWindow();
    }

    @Override
    public void userSelectedTab(ModeView modeView, TopComponent selected) {
        if (DEBUG) {
            DefaultView.debugLog("User selected tab, tc=" + WindowManagerImpl.getInstance().getTopComponentDisplayName(selected));
        }
        ModeAccessor modeAccessor = (ModeAccessor)this.hierarchy.getAccessorForView(modeView);
        ModeImpl mode = DefaultView.getModeForModeAccessor(modeAccessor);
        this.controllerHandler.userActivatedTopComponent(mode, selected);
    }

    @Override
    public void userClosingMode(ModeView modeView) {
        if (DEBUG) {
            DefaultView.debugLog("User closing mode=" + modeView);
        }
        ModeAccessor modeAccessor = (ModeAccessor)this.hierarchy.getAccessorForView(modeView);
        ModeImpl mode = DefaultView.getModeForModeAccessor(modeAccessor);
        this.controllerHandler.userClosedMode(mode);
    }

    @Override
    public void userResizedMainWindow(Rectangle bounds) {
        if (DEBUG) {
            DefaultView.debugLog("User resized main window");
        }
        if (this.hierarchy.getMainWindow().getExtendedState() != 6) {
            this.controllerHandler.userResizedMainWindow(bounds);
        }
        if (this.hierarchy.getMainWindow().getExtendedState() != 6) {
            this.updateMainWindowBoundsSeparatedHelp();
            this.updateEditorAreaBoundsHelp();
            this.updateSeparateBoundsForView(this.hierarchy.getSplitRootElement());
        }
    }

    @Override
    public void userMovedMainWindow(Rectangle bounds) {
        if (DEBUG) {
            DefaultView.debugLog("User moved main window");
        }
        if (this.hierarchy.getMainWindow().getExtendedState() != 6) {
            this.controllerHandler.userResizedMainWindow(bounds);
        }
    }

    @Override
    public void userResizedEditorArea(Rectangle bounds) {
        if (DEBUG) {
            DefaultView.debugLog("User resized editor area");
        }
        this.controllerHandler.userResizedEditorArea(bounds);
    }

    @Override
    public void userResizedModeBounds(ModeView modeView, Rectangle bounds) {
        ModeAccessor modeAccessor;
        if (DEBUG) {
            DefaultView.debugLog("User resized mode");
        }
        if ((modeAccessor = (ModeAccessor)this.hierarchy.getAccessorForView(modeView)) != null) {
            ModeImpl mode = DefaultView.getModeForModeAccessor(modeAccessor);
            this.controllerHandler.userResizedModeBounds(mode, bounds);
        }
    }

    @Override
    public void userMovedSplit(SplitView splitView, ViewElement[] childrenViews, double[] splitWeights) {
        if (DEBUG) {
            DefaultView.debugLog("User moved split");
        }
        SplitAccessor splitAccessor = (SplitAccessor)this.hierarchy.getAccessorForView(splitView);
        ElementAccessor[] childrenAccessors = new ElementAccessor[childrenViews.length];
        for (int i = 0; i < childrenViews.length; ++i) {
            childrenAccessors[i] = this.hierarchy.getAccessorForView(childrenViews[i]);
        }
        ViewHelper.setSplitWeights(splitAccessor, childrenAccessors, splitWeights, this.controllerHandler);
    }

    @Override
    public void userClosedTopComponent(ModeView modeView, TopComponent tc) {
        if (DEBUG) {
            DefaultView.debugLog("User closed topComponent=" + (Object)tc);
        }
        ModeAccessor modeAccessor = (ModeAccessor)this.hierarchy.getAccessorForView(modeView);
        ModeImpl mode = DefaultView.getModeForModeAccessor(modeAccessor);
        this.controllerHandler.userClosedTopComponent(mode, tc);
    }

    @Override
    public void userChangedFrameStateMainWindow(int frameState) {
        if (DEBUG) {
            DefaultView.debugLog("User changed frame state main window");
        }
        this.controllerHandler.userChangedFrameStateMainWindow(frameState);
    }

    @Override
    public void userChangedFrameStateEditorArea(int frameState) {
        if (DEBUG) {
            DefaultView.debugLog("User changed frame state editor area");
        }
        this.controllerHandler.userChangedFrameStateEditorArea(frameState);
    }

    @Override
    public void userChangedFrameStateMode(ModeView modeView, int frameState) {
        if (DEBUG) {
            DefaultView.debugLog("User changed frame state mode");
        }
        ModeAccessor modeAccessor = (ModeAccessor)this.hierarchy.getAccessorForView(modeView);
        ModeImpl mode = DefaultView.getModeForModeAccessor(modeAccessor);
        this.controllerHandler.userChangedFrameStateMode(mode, frameState);
    }

    @Override
    public void userDroppedTopComponents(ModeView modeView, TopComponentDraggable draggable) {
        if (DEBUG) {
            DefaultView.debugLog("User dropped TopComponent's");
        }
        ModeAccessor modeAccessor = (ModeAccessor)this.hierarchy.getAccessorForView(modeView);
        ModeImpl mode = DefaultView.getModeForModeAccessor(modeAccessor);
        this.controllerHandler.userDroppedTopComponents(mode, draggable);
    }

    @Override
    public void userDroppedTopComponents(ModeView modeView, TopComponentDraggable draggable, int index) {
        int position;
        if (DEBUG) {
            DefaultView.debugLog("User dropped TopComponent's to index=" + index);
        }
        ModeAccessor modeAccessor = (ModeAccessor)this.hierarchy.getAccessorForView(modeView);
        ModeImpl mode = DefaultView.getModeForModeAccessor(modeAccessor);
        if (draggable.isTopComponentTransfer() && (position = Arrays.asList(modeAccessor.getOpenedTopComponents()).indexOf((Object)draggable.getTopComponent())) > -1 && position <= index) {
            --index;
        }
        this.controllerHandler.userDroppedTopComponents(mode, draggable, index);
    }

    @Override
    public void userDroppedTopComponents(ModeView modeView, TopComponentDraggable draggable, String side) {
        if (DEBUG) {
            DefaultView.debugLog("User dropped TopComponent's to side=" + side);
        }
        ModeAccessor modeAccessor = (ModeAccessor)this.hierarchy.getAccessorForView(modeView);
        ModeImpl mode = DefaultView.getModeForModeAccessor(modeAccessor);
        this.controllerHandler.userDroppedTopComponents(mode, draggable, side);
    }

    @Override
    public void userDroppedTopComponentsIntoEmptyEditor(TopComponentDraggable draggable) {
        if (DEBUG) {
            DefaultView.debugLog("User dropped TopComponent's into empty editor");
        }
        this.controllerHandler.userDroppedTopComponentsIntoEmptyEditor(draggable);
    }

    @Override
    public void userDroppedTopComponentsAround(TopComponentDraggable draggable, String side) {
        if (DEBUG) {
            DefaultView.debugLog("User dropped TopComponent's around, side=" + side);
        }
        this.controllerHandler.userDroppedTopComponentsAround(draggable, side);
    }

    @Override
    public void userDroppedTopComponentsAroundEditor(TopComponentDraggable draggable, String side) {
        if (DEBUG) {
            DefaultView.debugLog("User dropped TopComponent's around editor, side=" + side);
        }
        this.controllerHandler.userDroppedTopComponentsAroundEditor(draggable, side);
    }

    @Override
    public void userDroppedTopComponentsIntoFreeArea(TopComponentDraggable draggable, Rectangle bounds) {
        if (DEBUG) {
            DefaultView.debugLog("User dropped TopComponent's into free area, bounds=" + bounds);
        }
        this.controllerHandler.userDroppedTopComponentsIntoFreeArea(draggable, bounds);
    }

    @Override
    public void userDisabledAutoHide(ModeView modeView, TopComponent tc) {
        ModeAccessor modeAccessor = (ModeAccessor)this.hierarchy.getAccessorForView(modeView);
        ModeImpl mode = DefaultView.getModeForModeAccessor(modeAccessor);
        this.controllerHandler.userDisabledAutoHide(tc, mode);
    }

    @Override
    public void userEnabledAutoHide(ModeView modeView, TopComponent tc) {
        ModeAccessor modeAccessor = (ModeAccessor)this.hierarchy.getAccessorForView(modeView);
        ModeImpl mode = DefaultView.getModeForModeAccessor(modeAccessor);
        String side = this.guessSlideSide(tc);
        this.controllerHandler.userEnabledAutoHide(tc, mode, side);
    }

    @Override
    public void userTriggeredSlideIn(ModeView modeView, SlideOperation operation) {
        this.hierarchy.performSlideIn(operation);
    }

    @Override
    public void userTriggeredSlideOut(ModeView modeView, SlideOperation operation) {
        this.hierarchy.performSlideOut(operation);
        if (operation.requestsActivation()) {
            ModeView lastNonSlidingActive = this.hierarchy.getLastNonSlidingActiveModeView();
            ModeImpl mode = null;
            if (lastNonSlidingActive != null) {
                mode = DefaultView.getModeForModeAccessor((ModeAccessor)this.hierarchy.getAccessorForView(lastNonSlidingActive));
            }
            if (mode != null) {
                this.controllerHandler.userActivatedMode(mode);
            } else {
                this.controllerHandler.userActivatedEditorWindow();
            }
        }
    }

    @Override
    public void userTriggeredSlideIntoDesktop(ModeView modeView, SlideOperation operation) {
        this.hierarchy.performSlideIntoDesktop(operation);
    }

    @Override
    public void userTriggeredSlideIntoEdge(ModeView modeView, SlideOperation operation) {
        this.hierarchy.performSlideIntoEdge(operation);
    }

    @Override
    public void userResizedSlidingWindow(ModeView modeView, SlideOperation operation) {
        ((SlidingView)modeView).setSlideBounds(modeView.getSelectedTopComponent().getBounds());
        this.hierarchy.performSlideResize(operation);
        ModeAccessor modeAccessor = (ModeAccessor)this.hierarchy.getAccessorForView(modeView);
        ModeImpl mode = DefaultView.getModeForModeAccessor(modeAccessor);
        this.controllerHandler.userResizedSlidingMode(mode, operation.getFinishBounds());
    }

    private static ModeImpl getModeForModeAccessor(ModeAccessor accessor) {
        return accessor == null ? null : accessor.getMode();
    }

    private void updateMainWindowBoundsSeparatedHelp() {
        this.controllerHandler.userResizedMainWindowBoundsSeparatedHelp(this.hierarchy.getMainWindow().getPureMainWindowBounds());
    }

    private void updateEditorAreaBoundsHelp() {
        Rectangle bounds = this.hierarchy.getPureEditorAreaBounds();
        this.controllerHandler.userResizedEditorAreaBoundsHelp(bounds);
    }

    void updateSeparateBoundsForView(ViewElement view) {
        if (view.getComponent() instanceof JComponent) {
            JComponent comp = (JComponent)view.getComponent();
            Dimension dim = new Dimension(comp.getSize());
            comp.setPreferredSize(dim);
            comp.putClientProperty("lastAvailableSpace", dim);
        }
        if (view instanceof ModeView) {
            ModeView mv = (ModeView)view;
            ModeAccessor ma = (ModeAccessor)this.hierarchy.getAccessorForView(mv);
            if (ma != null) {
                Component comp = mv.getComponent();
                Rectangle bounds = comp.getBounds();
                Point point = new Point(0, 0);
                SwingUtilities.convertPointToScreen(point, comp);
                bounds.setLocation(point);
                ModeImpl mode = DefaultView.getModeForModeAccessor(ma);
                this.controllerHandler.userResizedModeBoundsSeparatedHelp(mode, bounds);
            }
        } else if (view instanceof SplitView) {
            SplitView sv = (SplitView)view;
            List<ViewElement> children = sv.getChildren();
            for (ViewElement child : children) {
                this.updateSeparateBoundsForView(child);
            }
        } else if (view instanceof EditorView) {
            this.updateEditorAreaBoundsHelp();
        }
    }

    @Override
    public Set<Component> getModeComponents() {
        return this.hierarchy.getModeComponents();
    }

    @Override
    public Set<Component> getSeparateModeFrames() {
        return this.hierarchy.getSeparateModeFrames();
    }

    @Override
    public Controller getController() {
        return this;
    }

    @Override
    public Component getSlidingModeComponent(String side) {
        return this.hierarchy.getSlidingModeComponent(side);
    }

    private static void debugLog(String message) {
        Debug.log(DefaultView.class, message);
    }

    @Override
    public void userStartedKeyboardDragAndDrop(TopComponentDraggable draggable) {
        this.hierarchy.userStartedKeyboardDragAndDrop(draggable);
    }

}

