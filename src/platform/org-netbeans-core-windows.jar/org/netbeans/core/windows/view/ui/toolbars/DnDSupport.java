/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Toolbar
 *  org.openide.awt.ToolbarPool
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataShadow
 *  org.openide.nodes.Node
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 *  org.openide.util.datatransfer.ExTransferable
 *  org.openide.util.datatransfer.ExTransferable$Single
 */
package org.netbeans.core.windows.view.ui.toolbars;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragGestureRecognizer;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceContext;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.dnd.DragSourceMotionListener;
import java.awt.dnd.DropTargetContext;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.dnd.InvalidDnDOperationException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import org.netbeans.core.windows.nativeaccess.NativeWindowSystem;
import org.netbeans.core.windows.view.ui.toolbars.ToolbarConfiguration;
import org.netbeans.core.windows.view.ui.toolbars.ToolbarContainer;
import org.netbeans.core.windows.view.ui.toolbars.ToolbarRow;
import org.openide.awt.Toolbar;
import org.openide.awt.ToolbarPool;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataShadow;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.util.datatransfer.ExTransferable;

final class DnDSupport
implements DragSourceListener,
DragGestureListener,
DropTargetListener,
DragSourceMotionListener {
    private final ToolbarConfiguration config;
    private static final DataFlavor buttonDataFlavor = new DataFlavor(DataObject.class, "Toolbar Item");
    private static final DataFlavor actionDataFlavor = new DataFlavor(Node.class, "Action Node");
    private static final DataFlavor toolbarDataFlavor = new DataFlavor(ToolbarContainer.class, "Toolbar Container");
    private final DragSource dragSource = DragSource.getDefaultDragSource();
    private static final Cursor dragMoveCursor = DragSource.DefaultMoveDrop;
    private static final Cursor dragNoDropCursor = DragSource.DefaultMoveNoDrop;
    private Cursor dragRemoveCursor;
    private final Map<Component, DragGestureRecognizer> recognizers = new HashMap<Component, DragGestureRecognizer>();
    private boolean buttonDndAllowed = false;
    private Toolbar currentToolbar;
    private Toolbar sourceToolbar;
    private int dropTargetButtonIndex = -1;
    private int dragSourceButtonIndex = -1;
    private boolean insertBefore = true;
    private ToolbarContainer sourceContainer;
    private ToolbarRow currentRow;
    private ToolbarRow sourceRow;
    private Point startingPoint;
    private Window dragWindow;
    private Image dragImage;
    private boolean isToolbarDrag;
    private boolean isButtonDrag;
    private final Logger log = Logger.getLogger(DnDSupport.class.getName());
    private static final int SLIDE_INTERVAL = 33;

    public DnDSupport(ToolbarConfiguration config) {
        this.config = config;
        this.dragSource.addDragSourceMotionListener(this);
        this.dragRemoveCursor = Utilities.createCustomCursor((Component)ToolbarPool.getDefault(), (Image)ImageUtilities.loadImage((String)"org/netbeans/core/windows/resources/delete.gif"), (String)"NO_ACTION_MOVE");
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void register(Component c) {
        Map<Component, DragGestureRecognizer> map = this.recognizers;
        synchronized (map) {
            DragGestureRecognizer dgr = this.recognizers.get(c);
            if (null == dgr) {
                dgr = this.dragSource.createDefaultDragGestureRecognizer(c, 2, this);
                this.recognizers.put(c, dgr);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void unregister(Component c) {
        Map<Component, DragGestureRecognizer> map = this.recognizers;
        synchronized (map) {
            DragGestureRecognizer dgr = this.recognizers.get(c);
            if (null != dgr) {
                dgr.setComponent(null);
            }
            this.recognizers.remove(c);
        }
    }

    @Override
    public void dragEnter(DragSourceDragEvent e) {
    }

    @Override
    public void dragOver(DragSourceDragEvent e) {
    }

    @Override
    public void dragExit(DragSourceEvent e) {
        if (this.isButtonDrag) {
            this.resetDropGesture();
        }
    }

    @Override
    public void dragDropEnd(DragSourceDropEvent e) {
        if (this.isButtonDrag) {
            Component sourceComponent = e.getDragSourceContext().getComponent();
            if (sourceComponent instanceof JButton) {
                ((JButton)sourceComponent).getModel().setRollover(false);
            }
            sourceComponent.repaint();
            this.resetDropGesture();
            if (!e.getDropSuccess() && !this.isInToolbarPanel(e.getLocation())) {
                this.removeButton(e.getDragSourceContext().getTransferable());
            }
        } else if (this.isToolbarDrag) {
            Point newLocationOnScreen = null;
            boolean save = false;
            if (null != this.currentRow) {
                newLocationOnScreen = this.currentRow.drop();
                if (this.sourceRow != this.currentRow) {
                    this.sourceRow.dragSuccess();
                    this.config.removeEmptyRows();
                }
                save = true;
            } else if (null != this.sourceRow) {
                newLocationOnScreen = this.sourceRow.dragAbort();
                save = true;
            }
            if (null != this.dragWindow) {
                if (null != newLocationOnScreen) {
                    this.animateDragWindow(newLocationOnScreen);
                } else {
                    this.dragWindow.dispose();
                }
                this.dragWindow = null;
            }
            this.config.maybeRemoveLastRow();
            if (save) {
                this.config.refresh();
                this.config.save();
            }
        }
        this.isButtonDrag = false;
        this.isToolbarDrag = false;
    }

    @Override
    public void dragGestureRecognized(DragGestureEvent e) {
        Component c = e.getComponent();
        if (!(c instanceof JComponent)) {
            return;
        }
        ExTransferable.Single t = null;
        try {
            ToolbarContainer container;
            final DataObject dob = (DataObject)((JComponent)c).getClientProperty("file");
            if (dob != null && c.getParent() instanceof Toolbar && this.buttonDndAllowed) {
                this.sourceToolbar = (Toolbar)c.getParent();
                t = new ExTransferable.Single(buttonDataFlavor){

                    public Object getData() {
                        return dob;
                    }
                };
                this.isToolbarDrag = false;
                this.isButtonDrag = true;
                this.dragSourceButtonIndex = this.sourceToolbar.getComponentIndex(c);
            } else if (Boolean.TRUE.equals(((JComponent)c).getClientProperty("_toolbar_dragger_")) && (container = (ToolbarContainer)c.getParent().getParent()).isShowing()) {
                this.sourceContainer = container;
                this.sourceRow = (ToolbarRow)container.getParent();
                t = new ExTransferable.Single(toolbarDataFlavor){

                    public Object getData() {
                        return container;
                    }
                };
                this.isToolbarDrag = true;
                this.isButtonDrag = false;
                this.startingPoint = new Point(e.getDragOrigin());
                Rectangle bounds = new Rectangle(this.sourceContainer.getPreferredSize());
                bounds.setLocation(this.sourceContainer.getLocationOnScreen());
                this.dragImage = this.createContentImage(this.sourceContainer, bounds.getSize());
                this.sourceRow.dragStarted(this.sourceContainer);
                this.dragWindow = this.createDragWindow(this.dragImage, bounds);
            }
            if (c instanceof JButton) {
                ((JButton)c).getModel().setArmed(false);
                ((JButton)c).getModel().setPressed(false);
                ((JButton)c).getModel().setRollover(true);
            }
            if (t != null) {
                e.startDrag(dragMoveCursor, (Transferable)t, this);
            }
        }
        catch (InvalidDnDOperationException idoE) {
            this.log.log(Level.INFO, null, idoE);
        }
    }

    @Override
    public void dropActionChanged(DragSourceDragEvent e) {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void drop(DropTargetDropEvent dtde) {
        boolean res = false;
        try {
            if (this.isButtonDrag) {
                if (this.validateDropPosition()) {
                    res = this.handleDrop(dtde.getTransferable());
                }
            } else if (this.isToolbarDrag) {
                res = true;
            }
        }
        finally {
            dtde.dropComplete(res);
        }
        this.resetDropGesture();
    }

    @Override
    public void dragExit(DropTargetEvent dte) {
        this.resetDropGesture();
    }

    @Override
    public void dropActionChanged(DropTargetDragEvent dtde) {
    }

    @Override
    public void dragEnter(DropTargetDragEvent e) {
        if (e.isDataFlavorSupported(buttonDataFlavor) || e.isDataFlavorSupported(actionDataFlavor)) {
            e.acceptDrag(3);
            this.isButtonDrag = true;
        } else if (e.isDataFlavorSupported(toolbarDataFlavor)) {
            e.acceptDrag(3);
        } else {
            e.rejectDrag();
        }
    }

    @Override
    public void dragOver(DropTargetDragEvent e) {
        if (e.isDataFlavorSupported(buttonDataFlavor) || e.isDataFlavorSupported(actionDataFlavor)) {
            this.updateDropGesture(e);
            if (!this.validateDropPosition()) {
                e.rejectDrag();
            } else {
                e.acceptDrag(3);
            }
        } else if (e.isDataFlavorSupported(toolbarDataFlavor)) {
            e.acceptDrag(3);
        } else {
            e.rejectDrag();
        }
    }

    @Override
    public void dragMouseMoved(DragSourceDragEvent e) {
        DragSourceContext context = e.getDragSourceContext();
        if (this.isButtonDrag) {
            int action = e.getDropAction();
            if ((action & 2) != 0) {
                context.setCursor(dragMoveCursor);
            } else if (this.isInToolbarPanel(e.getLocation())) {
                context.setCursor(dragNoDropCursor);
            } else {
                context.setCursor(this.dragRemoveCursor);
            }
        } else if (this.isToolbarDrag && null != this.dragWindow) {
            Point p = new Point(e.getLocation());
            p.x -= this.startingPoint.x;
            p.y -= this.startingPoint.y;
            this.dragWindow.setLocation(p);
            context.setCursor(Cursor.getPredefinedCursor(13));
            ToolbarRow row = this.config.getToolbarRowAt(e.getLocation());
            if (!(null != row || this.sourceRow.countVisibleToolbars() <= 1 && this.config.isLastRow(this.sourceRow))) {
                row = this.config.maybeAddEmptyRow(e.getLocation());
            }
            ToolbarRow oldRow = this.currentRow;
            this.currentRow = row;
            if (null != oldRow && oldRow != this.currentRow) {
                oldRow.hideDropFeedback();
                this.config.repaint();
            }
            if (null != this.currentRow) {
                this.currentRow.showDropFeedback(this.sourceContainer, e.getLocation(), this.dragImage);
            }
            if (!this.config.isLastRow(this.currentRow)) {
                this.config.maybeRemoveLastRow();
            }
        }
    }

    void setButtonDragAndDropAllowed(boolean buttonDndAllowed) {
        this.buttonDndAllowed = buttonDndAllowed;
    }

    boolean isButtonDragAndDropAllowed() {
        return this.buttonDndAllowed;
    }

    private Window createDragWindow(Image dragImage, Rectangle bounds) {
        Window w = new Window(SwingUtilities.windowForComponent(this.sourceRow));
        w.add(new JLabel(new ImageIcon(dragImage)));
        w.setBounds(bounds);
        w.setVisible(true);
        NativeWindowSystem nws = NativeWindowSystem.getDefault();
        if (nws.isUndecoratedWindowAlphaSupported()) {
            nws.setWindowAlpha(w, 0.7f);
        }
        return w;
    }

    private BufferedImage createContentImage(JComponent c, Dimension contentSize) {
        GraphicsConfiguration cfg = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
        boolean opaque = c.isOpaque();
        c.setOpaque(true);
        BufferedImage res = cfg.createCompatibleImage(contentSize.width, contentSize.height);
        Graphics2D g = res.createGraphics();
        g.setColor(c.getBackground());
        g.fillRect(0, 0, contentSize.width, contentSize.height);
        g.setComposite(AlphaComposite.getInstance(3, 0.5f));
        c.paint(g);
        c.setOpaque(opaque);
        return res;
    }

    private void animateDragWindow(final Point newLocationOnScreen) {
        final Timer timer = new Timer(33, null);
        final Window returningWindow = this.dragWindow;
        timer.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Point location = returningWindow.getLocationOnScreen();
                Point dst = new Point(newLocationOnScreen);
                int dx = (dst.x - location.x) / 2;
                int dy = (dst.y - location.y) / 2;
                if (dx != 0 || dy != 0) {
                    location.translate(dx, dy);
                    returningWindow.setLocation(location);
                } else {
                    timer.stop();
                    returningWindow.dispose();
                }
            }
        });
        timer.setInitialDelay(0);
        timer.start();
    }

    private boolean isInToolbarPanel(Point p) {
        ToolbarPool c = ToolbarPool.getDefault();
        SwingUtilities.convertPointFromScreen(p, (Component)c);
        return c.contains(p);
    }

    private DataFolder getBackingFolder(Toolbar bar) {
        Object res = bar.getClientProperty((Object)"folder");
        if (res instanceof DataFolder) {
            return (DataFolder)res;
        }
        return null;
    }

    private boolean addButton(DataObject dobj, int dropIndex, boolean dropBefore) throws IOException {
        if (null == dobj) {
            return false;
        }
        String objName = dobj.getName();
        DataFolder backingFolder = this.getBackingFolder(this.currentToolbar);
        DataObject[] children = backingFolder.getChildren();
        for (int i = 0; i < children.length; ++i) {
            if (!objName.equals(children[i].getName())) continue;
            return this.moveButton(children[i], dropIndex, dropBefore);
        }
        DataObject objUnderCursor = this.getDataObjectUnderDropCursor(dropIndex - 1, dropBefore);
        DataShadow shadow = DataShadow.create((DataFolder)backingFolder, (DataObject)dobj);
        shadow.getPrimaryFile().setAttribute("position", (Object)100001);
        DataObject newObj = null;
        children = backingFolder.getChildren();
        for (int i2 = 0; i2 < children.length; ++i2) {
            if (!objName.equals(children[i2].getName())) continue;
            newObj = children[i2];
            break;
        }
        if (null != newObj) {
            this.reorderButtons(newObj, objUnderCursor);
        }
        return true;
    }

    private boolean moveButton(DataObject ob, int dropIndex, boolean dropBefore) throws IOException {
        DataObject objUnderCursor = this.getDataObjectUnderDropCursor(dropIndex - 1, dropBefore);
        if (this.sourceToolbar != this.currentToolbar) {
            ob.move(this.getBackingFolder(this.currentToolbar));
        }
        this.reorderButtons(ob, objUnderCursor);
        return true;
    }

    private void reorderButtons(DataObject objToMove, DataObject objUnderCursor) throws IOException {
        DataFolder backingFolder = this.getBackingFolder(this.currentToolbar);
        ArrayList<DataObject> children = new ArrayList<DataObject>(Arrays.asList(backingFolder.getChildren()));
        if (null == objUnderCursor) {
            children.remove((Object)objToMove);
            children.add(objToMove);
        } else {
            int targetIndex = children.indexOf((Object)objUnderCursor);
            int currentIndex = children.indexOf((Object)objToMove);
            if (currentIndex < targetIndex) {
                --targetIndex;
            }
            targetIndex = Math.max(0, targetIndex);
            targetIndex = Math.min(children.size(), targetIndex);
            children.remove((Object)objToMove);
            children.add(targetIndex, objToMove);
        }
        backingFolder.setOrder(children.toArray((T[])new DataObject[children.size()]));
    }

    private DataObject getDataObjectUnderDropCursor(int dropIndex, boolean dropBefore) {
        DataObject[] buttons = this.getBackingFolder(this.currentToolbar).getChildren();
        DataObject objUnderCursor = null;
        if (buttons.length > 0) {
            if (!dropBefore) {
                ++dropIndex;
            }
            if (dropIndex < buttons.length && dropIndex >= 0) {
                objUnderCursor = buttons[dropIndex];
            }
        }
        return objUnderCursor;
    }

    private boolean validateDropPosition() {
        return this.dropTargetButtonIndex >= 0;
    }

    private void updateDropGesture(DropTargetDragEvent e) {
        Point p = e.getLocation();
        Component c = e.getDropTargetContext().getComponent();
        if (c instanceof Toolbar) {
            ToolbarContainer container;
            boolean b;
            Toolbar bar = (Toolbar)c;
            Component button = bar.getComponentAt(p);
            int index = bar.getComponentIndex(button);
            boolean bl = b = p.x <= button.getLocation().x + button.getWidth() / 2;
            if (index != this.dropTargetButtonIndex || b != this.insertBefore) {
                this.dropTargetButtonIndex = index;
                this.insertBefore = b;
            }
            if (null != this.currentToolbar) {
                container = this.getContainer(this.currentToolbar);
                container.setDropGesture(-1, false);
            }
            this.currentToolbar = bar;
            container = this.getContainer(this.currentToolbar);
            container.setDropGesture(this.dropTargetButtonIndex, this.insertBefore);
        } else {
            this.resetDropGesture();
            this.currentToolbar = null;
        }
    }

    private void resetDropGesture() {
        this.dropTargetButtonIndex = -1;
        if (null != this.currentToolbar) {
            ToolbarContainer container = this.getContainer(this.currentToolbar);
            container.setDropGesture(-1, false);
        }
    }

    private ToolbarContainer getContainer(Toolbar bar) {
        return (ToolbarContainer)bar.getParent();
    }

    private void removeButton(Transferable t) {
        try {
            Object o = null;
            if (t.isDataFlavorSupported(buttonDataFlavor)) {
                o = t.getTransferData(buttonDataFlavor);
            }
            if (null != o && o instanceof DataObject) {
                ((DataObject)o).delete();
                this.sourceToolbar.repaint();
            }
        }
        catch (UnsupportedFlavorException e) {
            this.log.log(Level.INFO, null, e);
        }
        catch (IOException ioE) {
            this.log.log(Level.INFO, null, ioE);
        }
    }

    private boolean handleDrop(final Transferable t) {
        final boolean[] res = new boolean[]{false};
        FileUtil.runAtomicAction((Runnable)new Runnable(){

            @Override
            public void run() {
                res[0] = DnDSupport.this.handleDropImpl(t);
            }
        });
        return res[0];
    }

    private boolean handleDropImpl(Transferable t) {
        try {
            if (t.isDataFlavorSupported(actionDataFlavor)) {
                Object o = t.getTransferData(actionDataFlavor);
                if (o instanceof Node) {
                    DataObject dobj = (DataObject)((Node)o).getLookup().lookup(DataObject.class);
                    return this.addButton(dobj, this.dropTargetButtonIndex, this.insertBefore);
                }
            } else {
                Object o = t.getTransferData(buttonDataFlavor);
                if (o instanceof DataObject) {
                    return this.moveButton((DataObject)o, this.dropTargetButtonIndex, this.insertBefore);
                }
            }
        }
        catch (UnsupportedFlavorException e) {
            this.log.log(Level.INFO, null, e);
        }
        catch (IOException ioE) {
            this.log.log(Level.INFO, null, ioE);
        }
        return false;
    }

}

