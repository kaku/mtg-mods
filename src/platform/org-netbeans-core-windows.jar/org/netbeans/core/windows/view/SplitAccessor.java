/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view;

import org.netbeans.core.windows.view.ElementAccessor;

public interface SplitAccessor
extends ElementAccessor {
    public int getOrientation();

    public double[] getSplitWeights();

    public ElementAccessor[] getChildren();

    public double getResizeWeight();
}

