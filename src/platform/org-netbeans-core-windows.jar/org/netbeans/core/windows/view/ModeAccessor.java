/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view;

import java.awt.Rectangle;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.view.ElementAccessor;
import org.openide.windows.TopComponent;

interface ModeAccessor
extends ElementAccessor {
    public String getName();

    public int getState();

    public int getKind();

    public Rectangle getBounds();

    public int getFrameState();

    public TopComponent getSelectedTopComponent();

    public TopComponent[] getOpenedTopComponents();

    public double getResizeWeight();

    public ModeImpl getMode();
}

