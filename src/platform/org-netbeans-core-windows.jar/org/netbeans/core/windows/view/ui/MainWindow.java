/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.LifecycleManager
 *  org.openide.awt.MenuBar
 *  org.openide.awt.StatusLineElementProvider
 *  org.openide.awt.ToolbarPool
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.SaveCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.windows.view.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JSeparator;
import javax.swing.LookAndFeel;
import javax.swing.MenuElement;
import javax.swing.MenuSelectionManager;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.core.windows.Constants;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.view.ui.AutoHideStatusText;
import org.netbeans.core.windows.view.ui.StatusLine;
import org.netbeans.core.windows.view.ui.toolbars.ToolbarConfiguration;
import org.openide.LifecycleManager;
import org.openide.awt.MenuBar;
import org.openide.awt.StatusLineElementProvider;
import org.openide.awt.ToolbarPool;
import org.openide.cookies.InstanceCookie;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public final class MainWindow {
    static final long serialVersionUID = -1160791973145645501L;
    private final JFrame frame;
    private static JMenuBar mainMenuBar;
    private Component desktop;
    private JPanel desktopPanel;
    private static JPanel innerIconsPanel;
    private boolean inited;
    private Lookup.Result<SaveCookie> saveResult;
    private Lookup.Result<DataObject> dobResult;
    private LookupListener saveListener;
    private static MainWindow theInstance;
    private JPanel statusBarContainer = null;
    private JComponent statusBar;
    private static final Logger LOGGER;
    private final RequestProcessor RP = new RequestProcessor("MainWndMac", 1);
    private static Lookup.Result<StatusLineElementProvider> result;
    private static final String ICON_16 = "org/netbeans/core/startup/frame.gif";
    private static final String ICON_32 = "org/netbeans/core/startup/frame32.gif";
    private static final String ICON_48 = "org/netbeans/core/startup/frame48.gif";
    private Rectangle forcedBounds = null;
    private boolean isFullScreenMode = false;
    private Rectangle restoreBounds;
    private int restoreExtendedState = 0;
    private boolean isSwitchingFullScreenMode = false;
    private boolean isUndecorated = true;
    private int windowDecorationStyle = 1;
    private static boolean lafLogged;

    private MainWindow(JFrame frame) {
        this.frame = frame;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static MainWindow install(JFrame frame) {
        Class<MainWindow> class_ = MainWindow.class;
        synchronized (MainWindow.class) {
            if (null != theInstance) {
                LOGGER.log(Level.INFO, "Installing MainWindow again, existing frame is: " + MainWindow.theInstance.frame);
            }
            theInstance = new MainWindow(frame);
            // ** MonitorExit[var1_1] (shouldn't be in output)
            return theInstance;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static MainWindow getInstance() {
        Class<MainWindow> class_ = MainWindow.class;
        synchronized (MainWindow.class) {
            if (null == theInstance) {
                LOGGER.log(Level.INFO, "Accessing uninitialized MainWindow, using dummy JFrame instead.");
                theInstance = new MainWindow(new JFrame());
            }
            // ** MonitorExit[var0] (shouldn't be in output)
            return theInstance;
        }
    }

    public static void init() {
        if (mainMenuBar == null) {
            String session;
            mainMenuBar = MainWindow.createMenuBar();
            ToolbarPool.getDefault().waitFinished();
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Class xtoolkit = toolkit.getClass();
            if (xtoolkit.getName().equals("sun.awt.X11.XToolkit")) {
                try {
                    Field awtAppClassName = xtoolkit.getDeclaredField("awtAppClassName");
                    awtAppClassName.setAccessible(true);
                    awtAppClassName.set(null, NbBundle.getMessage(MainWindow.class, (String)"CTL_MainWindow_Title_No_Project", (Object)"").trim());
                }
                catch (Exception x) {
                    LOGGER.log(Level.FINE, null, x);
                }
            }
            if ("gnome-shell".equals(session = System.getenv("DESKTOP_SESSION")) || "gnome".equals(session) || "mate".equals(session)) {
                try {
                    Class xwm = Class.forName("sun.awt.X11.XWM");
                    Field awt_wmgr = xwm.getDeclaredField("awt_wmgr");
                    awt_wmgr.setAccessible(true);
                    Field other_wm = xwm.getDeclaredField("OTHER_WM");
                    other_wm.setAccessible(true);
                    if (awt_wmgr.get(null).equals(other_wm.get(null))) {
                        Field metacity_wm = xwm.getDeclaredField("METACITY_WM");
                        metacity_wm.setAccessible(true);
                        awt_wmgr.set(null, metacity_wm.get(null));
                        LOGGER.info("installed #198639 workaround");
                    }
                }
                catch (Exception x) {
                    LOGGER.log(Level.FINE, null, x);
                }
            }
        }
        MainWindow.logLookAndFeelUsage();
    }

    public void initializeComponents() {
        if (this.inited) {
            return;
        }
        this.inited = true;
        JPanel contentPane = new JPanel(new BorderLayout()){

            @Override
            public void paint(Graphics g) {
                super.paint(g);
                LOGGER.log(Level.FINE, "Paint method of main window invoked normally.");
                WindowManagerImpl.getInstance().mainWindowPainted();
            }
        };
        if (MainWindow.isShowCustomBackground()) {
            contentPane.setOpaque(false);
        }
        this.frame.setContentPane(contentPane);
        MainWindow.init();
        this.initRootPane();
        MainWindow.initFrameIcons(this.frame);
        this.initListeners();
        this.frame.setDefaultCloseOperation(0);
        this.frame.getAccessibleContext().setAccessibleDescription(NbBundle.getBundle(MainWindow.class).getString("ACSD_MainWindow"));
        this.frame.setJMenuBar(mainMenuBar);
        if (!Constants.NO_TOOLBARS) {
            JComponent tb = MainWindow.getToolbarComponent();
            this.frame.getContentPane().add((Component)tb, "North");
        }
        if (!Constants.SWITCH_STATUSLINE_IN_MENUBAR) {
            if (Constants.CUSTOM_STATUS_LINE_PATH == null) {
                boolean separateStatusLine = null == this.statusBarContainer;
                final JPanel statusLinePanel = new JPanel(new BorderLayout());
                if (MainWindow.isShowCustomBackground()) {
                    statusLinePanel.setOpaque(false);
                }
                int magicConstant = 0;
                if (Utilities.isMac()) {
                    magicConstant = 12;
                    if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
                        if (separateStatusLine) {
                            statusLinePanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, UIManager.getColor("NbBrushedMetal.darkShadow")), BorderFactory.createMatteBorder(1, 0, 0, 0, UIManager.getColor("NbBrushedMetal.lightShadow"))));
                        } else {
                            statusLinePanel.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, UIManager.getColor("NbBrushedMetal.darkShadow")));
                        }
                    }
                }
                statusLinePanel.setBorder(BorderFactory.createCompoundBorder(statusLinePanel.getBorder(), BorderFactory.createEmptyBorder(0, 0, 0, magicConstant)));
                if (!"Aqua".equals(UIManager.getLookAndFeel().getID()) && !UIManager.getBoolean("NbMainWindow.StatusBar.HideSeparator") && separateStatusLine) {
                    statusLinePanel.add((Component)new JSeparator(), "North");
                }
                if (separateStatusLine) {
                    StatusLine status = new StatusLine();
                    status.setText(" ");
                    status.setPreferredSize(new Dimension(0, status.getPreferredSize().height));
                    status.setBorder(BorderFactory.createEmptyBorder(0, 15, 0, 0));
                    statusLinePanel.add((Component)status, "Center");
                }
                WindowManager.getDefault().invokeWhenUIReady(new Runnable(){

                    @Override
                    public void run() {
                        MainWindow.decoratePanel(statusLinePanel, false);
                    }
                });
                statusLinePanel.setName("statusLine");
                this.statusBar = statusLinePanel;
                if (separateStatusLine) {
                    this.frame.getContentPane().add((Component)statusLinePanel, "South");
                } else {
                    this.statusBarContainer.add((Component)statusLinePanel, "Center");
                    AutoHideStatusText.install(this.frame, this.statusBarContainer);
                }
            } else {
                JComponent status = MainWindow.getCustomStatusLine();
                if (status != null) {
                    this.frame.getContentPane().add((Component)status, "South");
                }
            }
        }
        this.frame.getContentPane().add((Component)this.getDesktopPanel(), "Center");
        MenuSelectionManager.defaultManager().addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                MenuElement[] elems = MenuSelectionManager.defaultManager().getSelectedPath();
                if (elems != null && elems.length > 0 && elems[0] == MainWindow.this.frame.getJMenuBar() && !MainWindow.this.frame.isActive()) {
                    MainWindow.this.frame.toFront();
                }
            }
        });
        String title = NbBundle.getMessage(MainWindow.class, (String)"CTL_MainWindow_Title_No_Project", (Object)System.getProperty("netbeans.buildnumber"));
        if (!title.isEmpty()) {
            this.frame.setTitle(title);
        }
        if (Utilities.getOperatingSystem() == 4096) {
            this.saveResult = Utilities.actionsGlobalContext().lookupResult(SaveCookie.class);
            this.dobResult = Utilities.actionsGlobalContext().lookupResult(DataObject.class);
            if (null != this.saveResult && null != this.dobResult) {
                this.saveListener = new LookupListener(){

                    public void resultChanged(final LookupEvent ev) {
                        MainWindow.this.RP.post(new Runnable(){

                            @Override
                            public void run() {
                                MainWindow.this.updateMacDocumentProperties(ev);
                            }
                        });
                    }

                };
                this.saveResult.addLookupListener(this.saveListener);
                this.dobResult.addLookupListener(this.saveListener);
            }
            this.dobResult.allItems();
        }
    }

    private void updateMacDocumentProperties(LookupEvent ev) {
        if (ev.getSource() == this.saveResult) {
            final boolean modified = this.saveResult.allItems().size() > 0;
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    MainWindow.this.frame.getRootPane().putClientProperty("Window.documentModified", modified ? Boolean.TRUE : Boolean.FALSE);
                }
            });
        } else if (ev.getSource() == this.dobResult) {
            DataObject dob;
            final File[] documentFile = new File[1];
            Collection allItems = this.dobResult.allItems();
            if (1 == allItems.size() && null != (dob = (DataObject)((Lookup.Item)allItems.iterator().next()).getInstance())) {
                FileObject file = dob.getPrimaryFile();
                documentFile[0] = FileUtil.toFile((FileObject)file);
            }
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    MainWindow.this.frame.getRootPane().putClientProperty("Window.documentFile", documentFile[0]);
                }
            });
        }
    }

    private static void decoratePanel(JPanel panel, boolean safeAccess) {
        assert (safeAccess || SwingUtilities.isEventDispatchThread());
        if (innerIconsPanel != null) {
            panel.remove(innerIconsPanel);
        }
        if ((MainWindow.innerIconsPanel = MainWindow.getStatusLineElements(panel)) != null) {
            panel.add((Component)innerIconsPanel, "East");
        }
        if (MainWindow.isShowCustomBackground()) {
            panel.setOpaque(false);
        }
    }

    static JPanel getStatusLineElements(JPanel panel) {
        Collection c;
        if (result == null) {
            result = Lookup.getDefault().lookup(new Lookup.Template(StatusLineElementProvider.class));
            result.addLookupListener((LookupListener)new StatusLineElementsListener(panel));
        }
        if ((c = result.allInstances()) == null || c.isEmpty()) {
            return null;
        }
        final Iterator it = c.iterator();
        final JPanel icons = new JPanel(new FlowLayout(2, 0, 0));
        if (MainWindow.isShowCustomBackground()) {
            icons.setOpaque(false);
        }
        icons.setBorder(BorderFactory.createEmptyBorder(1, 0, 0, 2));
        final boolean[] some = new boolean[]{false};
        Runnable r = new Runnable(){

            @Override
            public void run() {
                while (it.hasNext()) {
                    StatusLineElementProvider o = (StatusLineElementProvider)it.next();
                    Component comp = o.getStatusLineElement();
                    if (comp == null) continue;
                    some[0] = true;
                    icons.add(comp);
                }
            }
        };
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(r);
            return icons;
        }
        r.run();
        return some[0] ? icons : null;
    }

    protected void initRootPane() {
        JRootPane root = this.frame.getRootPane();
        if (null == root) {
            return;
        }
        HelpCtx.setHelpIDString((JComponent)root, (String)new HelpCtx(MainWindow.class).getHelpID());
        if (Utilities.isWindows()) {
            JPanel c = new JPanel(){

                @Override
                public void setVisible(boolean flag) {
                    if (flag != this.isVisible()) {
                        super.setVisible(flag);
                    }
                }
            };
            c.setName(root.getName() + ".nbGlassPane");
            c.setVisible(false);
            ((JPanel)c).setOpaque(false);
            root.setGlassPane(c);
        }
    }

    public void setBounds(Rectangle bounds) {
        this.frame.setBounds(bounds);
    }

    public void setExtendedState(int extendedState) {
        this.frame.setExtendedState(extendedState);
    }

    public void setVisible(boolean visible) {
        if ("false".equals(System.getProperty("org.netbeans.core.WindowSystem.show"))) {
            return;
        }
        this.frame.setVisible(visible);
    }

    public int getExtendedState() {
        return this.frame.getExtendedState();
    }

    public JMenuBar getJMenuBar() {
        return this.frame.getJMenuBar();
    }

    void setStatusBarContainer(JPanel panel) {
        assert (null != panel);
        assert (panel.getLayout() instanceof BorderLayout);
        this.statusBarContainer = panel;
        if (null != this.statusBar) {
            this.statusBarContainer.add((Component)this.statusBar, "Center");
        }
    }

    private static Border getDesktopBorder() {
        Border b = (Border)UIManager.get("nb.desktop.splitpane.border");
        if (b != null) {
            return b;
        }
        return new EmptyBorder(1, 1, 1, 1);
    }

    static void initFrameIcons(Frame f) {
        List<Image> currentIcons = f.getIconImages();
        if (!currentIcons.isEmpty()) {
            return;
        }
        f.setIconImages(Arrays.asList(ImageUtilities.loadImage((String)"org/netbeans/core/startup/frame.gif", (boolean)true), ImageUtilities.loadImage((String)"org/netbeans/core/startup/frame32.gif", (boolean)true), ImageUtilities.loadImage((String)"org/netbeans/core/startup/frame48.gif", (boolean)true)));
    }

    private void initListeners() {
        this.frame.addWindowListener(new WindowAdapter(){

            @Override
            public void windowClosing(WindowEvent evt) {
                LifecycleManager.getDefault().exit();
            }
        });
    }

    static void preInitMenuAndToolbar() {
        MainWindow.createMenuBar();
        ToolbarPool.getDefault();
    }

    private static JMenuBar createMenuBar() {
        JMenuBar menu = MainWindow.getCustomMenuBar();
        if (menu == null) {
            menu = new MenuBar(null);
        }
        menu.setBorderPainted(false);
        if (menu instanceof MenuBar) {
            ((MenuBar)menu).waitFinished();
        }
        if (Constants.SWITCH_STATUSLINE_IN_MENUBAR) {
            if (Constants.CUSTOM_STATUS_LINE_PATH == null) {
                StatusLine status = new StatusLine();
                JSeparator sep = new JSeparator(1);
                Dimension d = sep.getPreferredSize();
                d.width += 6;
                sep.setPreferredSize(d);
                JPanel statusLinePanel = new JPanel(new BorderLayout());
                statusLinePanel.add((Component)sep, "West");
                statusLinePanel.add((Component)status, "Center");
                MainWindow.decoratePanel(statusLinePanel, true);
                statusLinePanel.setName("statusLine");
                menu.add(statusLinePanel);
            } else {
                JComponent status = MainWindow.getCustomStatusLine();
                if (status != null) {
                    menu.add(status);
                }
            }
        }
        return menu;
    }

    private static JMenuBar getCustomMenuBar() {
        try {
            InstanceCookie ic;
            DataObject dobj;
            String fileName = Constants.CUSTOM_MENU_BAR_PATH;
            if (fileName == null) {
                return null;
            }
            FileObject fo = FileUtil.getConfigFile((String)fileName);
            if (fo != null && (ic = (InstanceCookie)(dobj = DataObject.find((FileObject)fo)).getCookie(InstanceCookie.class)) != null) {
                return (JMenuBar)ic.instanceCreate();
            }
        }
        catch (Exception e) {
            Exceptions.printStackTrace((Throwable)e);
        }
        return null;
    }

    private static JComponent getCustomStatusLine() {
        try {
            InstanceCookie ic;
            DataObject dobj;
            String fileName = Constants.CUSTOM_STATUS_LINE_PATH;
            if (fileName == null) {
                return null;
            }
            FileObject fo = FileUtil.getConfigFile((String)fileName);
            if (fo != null && (ic = (InstanceCookie)(dobj = DataObject.find((FileObject)fo)).getCookie(InstanceCookie.class)) != null) {
                return (JComponent)ic.instanceCreate();
            }
        }
        catch (Exception e) {
            Exceptions.printStackTrace((Throwable)e);
        }
        return null;
    }

    private static JComponent getToolbarComponent() {
        ToolbarPool tp = ToolbarPool.getDefault();
        tp.waitFinished();
        return tp;
    }

    private void initializeBounds() {
        Rectangle bounds = WindowManagerImpl.getInstance().getEditorAreaState() == 0 ? WindowManagerImpl.getInstance().getMainWindowBoundsJoined() : WindowManagerImpl.getInstance().getMainWindowBoundsSeparated();
        if (null != this.forcedBounds) {
            bounds = new Rectangle(this.forcedBounds);
            this.frame.setPreferredSize(bounds.getSize());
            this.forcedBounds = null;
        }
        if (!bounds.isEmpty()) {
            this.frame.setBounds(bounds);
        }
    }

    public void prepareWindow() {
        this.initializeBounds();
    }

    public void setDesktop(Component comp) {
        if (this.desktop == comp) {
            if (this.desktop != null && !Arrays.asList(this.getDesktopPanel().getComponents()).contains(this.desktop)) {
                this.getDesktopPanel().add(this.desktop, "Center");
            }
            return;
        }
        if (this.desktop != null) {
            this.getDesktopPanel().remove(this.desktop);
        }
        this.desktop = comp;
        if (this.desktop != null) {
            this.getDesktopPanel().add(this.desktop, "Center");
        }
        this.frame.invalidate();
        this.frame.validate();
        this.frame.repaint();
    }

    public Component getDesktop() {
        return this.desktop;
    }

    public boolean hasDesktop() {
        return this.desktop != null;
    }

    private JPanel getDesktopPanel() {
        if (this.desktopPanel == null) {
            this.desktopPanel = new JPanel();
            this.desktopPanel.setBorder(MainWindow.getDesktopBorder());
            this.desktopPanel.setLayout(new BorderLayout());
            if (MainWindow.isShowCustomBackground()) {
                this.desktopPanel.setOpaque(false);
            }
        }
        return this.desktopPanel;
    }

    public Rectangle getPureMainWindowBounds() {
        Rectangle bounds = this.frame.getBounds();
        if (this.desktop != null) {
            Dimension desktopSize = this.desktop.getSize();
            bounds.height -= desktopSize.height;
        }
        return bounds;
    }

    public void setFullScreenMode(boolean fullScreenMode) {
        ToolbarConfiguration tc;
        boolean updateBounds;
        if (this.isFullScreenMode == fullScreenMode || this.isSwitchingFullScreenMode || Utilities.isMac()) {
            return;
        }
        this.isSwitchingFullScreenMode = true;
        if (!this.isFullScreenMode) {
            this.restoreExtendedState = this.frame.getExtendedState();
            this.restoreBounds = this.frame.getBounds();
            this.isUndecorated = this.frame.isUndecorated();
            this.windowDecorationStyle = this.frame.getRootPane().getWindowDecorationStyle();
        }
        final TopComponent activeTc = TopComponent.getRegistry().getActivated();
        GraphicsDevice device = null;
        GraphicsConfiguration conf = this.frame.getGraphicsConfiguration();
        if (null != conf) {
            device = conf.getDevice();
            if (this.isFullScreenMode && device.isFullScreenSupported() && !Utilities.isMac() && !Utilities.isWindows()) {
                try {
                    device.setFullScreenWindow(null);
                }
                catch (IllegalArgumentException iaE) {
                    LOGGER.log(Level.FINE, null, iaE);
                }
            }
        }
        this.isFullScreenMode = fullScreenMode;
        if (Utilities.isWindows()) {
            this.frame.setVisible(false);
        } else {
            WindowManagerImpl.getInstance().setVisible(false);
        }
        this.frame.dispose();
        this.frame.setUndecorated(this.isFullScreenMode || this.isUndecorated);
        this.frame.getRootPane().setWindowDecorationStyle(this.isFullScreenMode ? 0 : this.windowDecorationStyle);
        final String toolbarConfigName = ToolbarPool.getDefault().getConfiguration();
        if (null != toolbarConfigName && null != (tc = ToolbarConfiguration.findConfiguration(toolbarConfigName))) {
            ToolbarConfiguration.rebuildMenu();
        }
        MainWindow.getToolbarComponent().setVisible(!this.isFullScreenMode);
        boolean bl = updateBounds = !this.isFullScreenMode;
        if (updateBounds || this.isFullScreenMode() && !Utilities.isWindows()) {
            if (updateBounds) {
                this.forcedBounds = this.restoreBounds;
            } else if (null != conf) {
                this.forcedBounds = conf.getBounds();
            } else {
                GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
                this.forcedBounds = ge.getMaximumWindowBounds();
            }
        }
        if (null != device && device.isFullScreenSupported() && !Utilities.isWindows()) {
            device.setFullScreenWindow(this.isFullScreenMode ? this.frame : null);
        } else {
            this.frame.setExtendedState(this.isFullScreenMode ? 6 : this.restoreExtendedState);
        }
        if (Utilities.isWindows()) {
            this.frame.setVisible(true);
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    MainWindow.this.frame.invalidate();
                    MainWindow.this.frame.validate();
                    MainWindow.this.frame.repaint();
                    if (updateBounds) {
                        MainWindow.this.frame.setPreferredSize(MainWindow.this.restoreBounds.getSize());
                        MainWindow.this.frame.setBounds(MainWindow.this.restoreBounds);
                    }
                    ToolbarPool.getDefault().setConfiguration(toolbarConfigName);
                    MainWindow.this.isSwitchingFullScreenMode = false;
                    if (null != activeTc) {
                        activeTc.requestFocusInWindow();
                    }
                }
            });
        } else {
            WindowManagerImpl.getInstance().setVisible(true);
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    MainWindow.this.frame.invalidate();
                    MainWindow.this.frame.validate();
                    MainWindow.this.frame.repaint();
                    ToolbarPool.getDefault().setConfiguration(toolbarConfigName);
                    MainWindow.this.isSwitchingFullScreenMode = false;
                    if (null != activeTc) {
                        activeTc.requestFocusInWindow();
                    }
                }
            });
        }
    }

    public boolean isFullScreenMode() {
        return this.isFullScreenMode;
    }

    public JFrame getFrame() {
        return this.frame;
    }

    private static boolean isShowCustomBackground() {
        return UIManager.getBoolean("NbMainWindow.showCustomBackground");
    }

    private static void logLookAndFeelUsage() {
        if (lafLogged) {
            return;
        }
        lafLogged = true;
        LookAndFeel laf = UIManager.getLookAndFeel();
        Logger logger = Logger.getLogger("org.netbeans.ui.metrics.laf");
        LogRecord rec = new LogRecord(Level.INFO, "USG_LOOK_AND_FEEL");
        String lafId = laf.getID();
        if (laf.getDefaults().getBoolean("nb.dark.theme")) {
            lafId = "DARK " + lafId;
        }
        rec.setParameters(new Object[]{lafId, laf.getName()});
        rec.setLoggerName(logger.getName());
        logger.log(rec);
    }

    static {
        LOGGER = Logger.getLogger(MainWindow.class.getName());
        lafLogged = false;
    }

    private static class HeavyWeightPopup
    extends Popup {
        public HeavyWeightPopup(Component owner, Component contents, int x, int y) {
            super(owner, contents, x, y);
        }
    }

    private static class HeavyWeightPopupFactory
    extends PopupFactory {
        private HeavyWeightPopupFactory() {
        }

        @Override
        public Popup getPopup(Component owner, Component contents, int x, int y) throws IllegalArgumentException {
            return new HeavyWeightPopup(owner, contents, x, y);
        }
    }

    private static class StatusLineElementsListener
    implements LookupListener {
        private JPanel decoratingPanel;

        StatusLineElementsListener(JPanel decoratingPanel) {
            this.decoratingPanel = decoratingPanel;
        }

        public void resultChanged(LookupEvent ev) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    MainWindow.decoratePanel(StatusLineElementsListener.this.decoratingPanel, false);
                }
            });
        }

    }

}

