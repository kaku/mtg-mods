/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Actions
 *  org.openide.awt.MouseUtils
 *  org.openide.awt.MouseUtils$PopupMouseAdapter
 *  org.openide.awt.Toolbar
 *  org.openide.awt.ToolbarPool
 *  org.openide.loaders.DataObject
 *  org.openide.util.Lookup
 */
package org.netbeans.core.windows.view.ui.toolbars;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.metal.MetalBorders;
import javax.swing.plaf.synth.Region;
import javax.swing.plaf.synth.SynthContext;
import javax.swing.plaf.synth.SynthLookAndFeel;
import javax.swing.plaf.synth.SynthStyle;
import javax.swing.plaf.synth.SynthStyleFactory;
import org.netbeans.core.windows.view.ui.toolbars.DnDSupport;
import org.netbeans.core.windows.view.ui.toolbars.ToolbarConfiguration;
import org.openide.awt.Actions;
import org.openide.awt.MouseUtils;
import org.openide.awt.Toolbar;
import org.openide.awt.ToolbarPool;
import org.openide.loaders.DataObject;
import org.openide.util.Lookup;

final class ToolbarContainer
extends JPanel {
    static final String PROP_DRAGGER = "_toolbar_dragger_";
    private static final Logger LOG = Logger.getLogger(Toolbar.class.getName());
    private final Toolbar toolbar;
    private JComponent dragger;
    private final DnDSupport dnd;
    private final boolean draggable;
    private DropTarget dropTarget;
    private int dropIndex = -1;
    private boolean dropBefore;
    private static final int TOP = 2;
    private static final int LEFT = 3;
    private static final int BOTTOM = 2;
    private static final int RIGHT = 3;
    private static Class synthIconClass = null;
    private static boolean testExecuted = false;
    private static Boolean isXP = null;
    private static Map<RenderingHints.Key, Object> hintsMap = null;

    public ToolbarContainer(Toolbar toolbar, final DnDSupport dnd, boolean draggable) {
        super(new BorderLayout());
        this.setOpaque(false);
        this.toolbar = toolbar;
        this.dnd = dnd;
        this.draggable = draggable;
        this.add((Component)toolbar, "Center");
        toolbar.addContainerListener(new ContainerListener(){

            @Override
            public void componentAdded(ContainerEvent e) {
                dnd.register(e.getChild());
            }

            @Override
            public void componentRemoved(ContainerEvent e) {
                dnd.unregister(e.getChild());
            }
        });
        String lAndF = UIManager.getLookAndFeel().getID();
        if (lAndF.equals("Windows")) {
            toolbar.setBorder(Boolean.getBoolean("netbeans.small.main.window") ? BorderFactory.createEmptyBorder(1, 1, 1, 1) : BorderFactory.createEmptyBorder());
        } else if (!"Aqua".equals(lAndF) && !"GTK".equals(lAndF)) {
            Border b = UIManager.getBorder("Nb.ToolBar.border");
            if (null == b) {
                b = UIManager.getBorder("ToolBar.border");
            }
            if (b == null || b instanceof MetalBorders.ToolBarBorder) {
                b = BorderFactory.createEtchedBorder(1);
            }
            toolbar.setBorder((Border)new CompoundBorder(b, new EmptyBorder(2, 3, 2, 3)));
        } else if ("Aqua".equals(lAndF)) {
            toolbar.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
            toolbar.setOpaque(false);
        }
        if (!"Aqua".equals(lAndF)) {
            toolbar.putClientProperty((Object)"JToolBar.isRollover", (Object)Boolean.TRUE);
        }
    }

    @Override
    public void addNotify() {
        super.addNotify();
        if (null == this.dragger && this.isDraggable()) {
            this.dragger = this.createDragger();
            this.dragger.setToolTipText(Actions.cutAmpersand((String)this.toolbar.getDisplayName()));
            this.dragger.addMouseListener((MouseListener)new MouseUtils.PopupMouseAdapter(){

                protected void showPopup(MouseEvent evt) {
                    ToolbarConfiguration config = ToolbarConfiguration.findConfiguration(ToolbarPool.getDefault().getConfiguration());
                    if (null != config) {
                        config.getContextMenu().show(ToolbarContainer.this.dragger, evt.getX(), evt.getY());
                    }
                }
            });
            this.addToolbarDragger();
        }
        this.registerDnd();
        if (null == this.dropTarget) {
            this.dropTarget = new DropTarget((Component)this.toolbar, this.dnd);
        }
    }

    @Override
    public Dimension getMinimumSize() {
        Dimension d = new Dimension(0, 0);
        d.height = this.toolbar.getMinimumSize().height;
        if (this.toolbar.getComponentCount() <= 1) {
            d.width += ToolbarPool.getDefault().getPreferredIconSize();
        } else {
            d.width += this.toolbar.getComponent((int)0).getMinimumSize().width;
            if (this.toolbar.getComponentCount() > 1) {
                d.width += this.toolbar.getComponent((int)1).getMinimumSize().width;
            }
        }
        Insets insets = this.toolbar.getInsets();
        if (null != insets) {
            d.width += insets.left + insets.right;
        }
        return d;
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.unregisterDnd();
        if (null != this.dropTarget) {
            this.dropTarget.removeDropTargetListener(this.dnd);
            this.dropTarget = null;
        }
    }

    Toolbar getToolbar() {
        return this.toolbar;
    }

    @Override
    public String getName() {
        return null == this.toolbar ? super.getName() : this.toolbar.getName();
    }

    private void addToolbarDragger() {
        Component oldDragger = null;
        for (Component c : this.toolbar.getComponents()) {
            JComponent jc;
            if (!(c instanceof JComponent) || !Boolean.TRUE.equals((jc = (JComponent)c).getClientProperty("_toolbar_dragger_"))) continue;
            oldDragger = c;
            break;
        }
        if (null != oldDragger) {
            this.dragger = (JComponent)oldDragger;
        } else {
            this.toolbar.add((Component)this.dragger, 0);
        }
    }

    private void registerDnd() {
        for (Component c : this.toolbar.getComponents()) {
            JComponent jc;
            Object o;
            if (!(c instanceof JComponent) || !((o = (jc = (JComponent)c).getClientProperty("file")) instanceof DataObject)) continue;
            this.dnd.register(c);
        }
        if (this.isDraggable() && null != this.dragger) {
            this.dnd.register(this.dragger);
        }
    }

    private void unregisterDnd() {
        for (Component c : this.toolbar.getComponents()) {
            this.dnd.unregister(c);
        }
        if (null != this.dragger) {
            this.dnd.unregister(this.dragger);
        }
    }

    private boolean isDraggable() {
        return this.draggable;
    }

    private JComponent createDragger() {
        String className = UIManager.getString("Nb.MainWindow.Toolbar.Dragger");
        if (null != className) {
            try {
                Class klzz = ((ClassLoader)Lookup.getDefault().lookup(ClassLoader.class)).loadClass(className);
                Object inst = klzz.newInstance();
                if (inst instanceof JComponent) {
                    JComponent dragarea = (JComponent)inst;
                    dragarea.setCursor(Cursor.getPredefinedCursor(13));
                    dragarea.putClientProperty("_toolbar_dragger_", Boolean.TRUE);
                    return dragarea;
                }
            }
            catch (Exception e) {
                Logger.getLogger(ToolbarContainer.class.getName()).log(Level.INFO, null, e);
            }
        }
        String lfID = UIManager.getLookAndFeel().getID();
        JPanel dragarea = null;
        dragarea = lfID.endsWith("Windows") ? (ToolbarContainer.isXPTheme() ? new ToolbarXP() : new ToolbarGrip()) : (lfID.equals("Aqua") ? new ToolbarAqua() : (lfID.equals("GTK") ? new ToolbarGtk() : new ToolbarBump()));
        dragarea.setCursor(Cursor.getPredefinedCursor(13));
        dragarea.putClientProperty("_toolbar_dragger_", Boolean.TRUE);
        return dragarea;
    }

    void setDropGesture(int dropIndex, boolean dropBefore) {
        this.dropIndex = dropIndex;
        this.dropBefore = dropBefore;
        this.repaint();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (this.dropIndex >= 0) {
            this.paintDropGesture(g);
        }
    }

    private void paintDropGesture(Graphics g) {
        Component c = this.toolbar.getComponentAtIndex(this.dropIndex);
        if (null == c) {
            return;
        }
        Point location = c.getLocation();
        int cursorLocation = location.x;
        if (!this.dropBefore) {
            cursorLocation += c.getWidth();
            if (this.dropIndex == this.toolbar.getComponentCount() - 1) {
                cursorLocation -= 3;
            }
        }
        this.drawDropLine(g, cursorLocation);
    }

    private void drawDropLine(Graphics g, int x) {
        Color oldColor = g.getColor();
        g.setColor(Color.black);
        int height = this.getHeight();
        g.drawLine(x, 3, x, height - 4);
        g.drawLine(x - 1, 3, x - 1, height - 4);
        g.drawLine(x + 1, 2, x + 1 + 2, 2);
        g.drawLine(x + 1, height - 3, x + 1 + 2, height - 3);
        g.drawLine(x - 2, 2, x - 2 - 2, 2);
        g.drawLine(x - 2, height - 3, x - 2 - 2, height - 3);
        g.setColor(oldColor);
    }

    private static boolean useSynthIcon() {
        if (!testExecuted) {
            testExecuted = true;
            try {
                synthIconClass = ClassLoader.getSystemClassLoader().loadClass("sun.swing.plaf.synth.SynthIcon");
            }
            catch (ClassNotFoundException exc) {
                LOG.log(Level.INFO, null, exc);
            }
        }
        return synthIconClass != null;
    }

    private static boolean isXPTheme() {
        if (isXP == null) {
            Boolean xp = (Boolean)Toolkit.getDefaultToolkit().getDesktopProperty("win.xpstyle.themeActive");
            isXP = Boolean.TRUE.equals(xp) ? Boolean.TRUE : Boolean.FALSE;
        }
        return isXP;
    }

    static final Map getHints() {
        if (hintsMap == null && (ToolbarContainer.hintsMap = (Map)Toolkit.getDefaultToolkit().getDesktopProperty("awt.font.desktophints")) == null) {
            hintsMap = new HashMap<RenderingHints.Key, Object>();
            hintsMap.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        }
        return hintsMap;
    }

    private final class ToolbarGrip
    extends JPanel {
        static final int HGAP = 1;
        static final int VGAP = 2;
        static final int STEP = 1;
        private static final int GRIP_WIDTH = 2;
        int columns;
        Dimension dim;
        Dimension max;

        public ToolbarGrip() {
            this(1);
        }

        public ToolbarGrip(int col) {
            this.columns = col;
            int width = (col - 1) * 1 + col * 2 + 2;
            this.dim = new Dimension(width, width);
            this.max = new Dimension(width, Integer.MAX_VALUE);
            this.setBorder(new EmptyBorder(2, 1, 2, 1));
        }

        @Override
        public void paint(Graphics g) {
            Dimension size = this.getSize();
            int top = 2;
            int bottom = size.height - 1 - 2;
            int height = bottom - top;
            g.setColor(this.getBackground());
            int i = 0;
            int x = 1;
            while (i < this.columns) {
                g.draw3DRect(x, top, 2, height, true);
                ++i;
                x += 3;
            }
        }

        @Override
        public Dimension getMinimumSize() {
            return this.dim;
        }

        @Override
        public Dimension getPreferredSize() {
            return this.getMinimumSize();
        }

        @Override
        public Dimension getMaximumSize() {
            return this.max;
        }
    }

    private final class ToolbarXP
    extends JPanel {
        private static final int GRIP_WIDTH = 7;
        Dimension dim;
        Dimension max;

        public ToolbarXP() {
            this.dim = new Dimension(7, 7);
            this.max = new Dimension(7, Integer.MAX_VALUE);
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            int x = 3;
            for (int i = 4; i < this.getHeight() - 4; i += 4) {
                g.setColor(UIManager.getColor("controlLtHighlight"));
                g.fillRect(x + 1, i + 1, 2, 2);
                Color col = UIManager.getColor("controlShadow");
                g.setColor(col);
                g.drawLine(x + 1, i + 1, x + 1, i + 1);
                int red = col.getRed();
                int green = col.getGreen();
                int blue = col.getBlue();
                Color back = this.getBackground();
                int rb = back.getRed();
                int gb = back.getGreen();
                int bb = back.getBlue();
                int incr = (rb - red) / 5;
                int incg = (gb - green) / 5;
                int incb = (bb - blue) / 5;
                col = new Color(red += incr, green += incg, blue += incb);
                g.setColor(col);
                g.drawLine(x + 1, i, x + 1, i);
                col = new Color(red += incr, green += incg, blue += incb);
                g.setColor(col);
                g.drawLine(x, i + 1, x, i + 1);
                col = new Color(red += incr, green += incg, blue += incb);
                g.setColor(col);
                g.drawLine(x, i, x, i);
            }
        }

        @Override
        public Dimension getMinimumSize() {
            return this.dim;
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(7, ToolbarContainer.this.toolbar.getHeight() - 4);
        }

        @Override
        public Dimension getMaximumSize() {
            return this.max;
        }
    }

    private final class ToolbarAqua
    extends JPanel {
        private static final int GRIP_WIDTH = 8;
        Dimension dim;
        Dimension max;

        public ToolbarAqua() {
            super(new BorderLayout());
            JToolBar.Separator sep = new JToolBar.Separator();
            sep.setOrientation(1);
            sep.setForeground(UIManager.getColor("NbSplitPane.background"));
            this.add((Component)sep, "Center");
            this.dim = new Dimension(8, 8);
            this.max = new Dimension(8, Integer.MAX_VALUE);
            this.setBorder(BorderFactory.createEmptyBorder(4, 0, 2, 0));
        }

        @Override
        public Dimension getMinimumSize() {
            return this.dim;
        }

        @Override
        public Dimension getPreferredSize() {
            return this.getMinimumSize();
        }

        @Override
        public Dimension getMaximumSize() {
            return this.max;
        }
    }

    private final class ToolbarGtk
    extends JPanel {
        int TOPGAP;
        int BOTGAP;
        private static final int GRIP_WIDTH = 6;
        Dimension dim;
        Dimension max;

        public ToolbarGtk() {
            int width = 6;
            if (ToolbarContainer.useSynthIcon()) {
                this.TOPGAP = 0;
                this.BOTGAP = 0;
            } else {
                this.TOPGAP = 2;
                this.BOTGAP = 2;
            }
            this.dim = new Dimension(width, width);
            this.max = new Dimension(width, Integer.MAX_VALUE);
        }

        @Override
        public void paint(Graphics g) {
            if (ToolbarContainer.useSynthIcon()) {
                int height = ToolbarContainer.this.toolbar.getHeight() - this.BOTGAP;
                Icon icon = UIManager.getIcon("ToolBar.handleIcon");
                Region region = Region.TOOL_BAR;
                SynthStyleFactory sf = SynthLookAndFeel.getStyleFactory();
                SynthStyle style = sf.getStyle((JComponent)ToolbarContainer.this.toolbar, region);
                SynthContext context = new SynthContext((JComponent)ToolbarContainer.this.toolbar, region, style, 1024);
                Method m = null;
                try {
                    m = synthIconClass.getMethod("getIconWidth", Icon.class, SynthContext.class);
                }
                catch (NoSuchMethodException exc) {
                    LOG.log(Level.WARNING, null, exc);
                }
                int width = 0;
                try {
                    width = (Integer)m.invoke(null, icon, context);
                }
                catch (IllegalAccessException exc) {
                    LOG.log(Level.WARNING, null, exc);
                }
                catch (InvocationTargetException exc) {
                    LOG.log(Level.WARNING, null, exc);
                }
                try {
                    m = synthIconClass.getMethod("paintIcon", Icon.class, SynthContext.class, Graphics.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE);
                }
                catch (NoSuchMethodException exc) {
                    LOG.log(Level.WARNING, null, exc);
                }
                try {
                    m.invoke(null, icon, context, g, new Integer(0), new Integer(-1), new Integer(width), new Integer(height));
                }
                catch (IllegalAccessException exc) {
                    LOG.log(Level.WARNING, null, exc);
                }
                catch (InvocationTargetException exc) {
                    LOG.log(Level.WARNING, null, exc);
                }
            } else {
                Dimension size = this.getSize();
                int height = size.height - this.BOTGAP;
                g.setColor(this.getBackground());
                int x = 0;
                while (x + 1 < size.width) {
                    int y = this.TOPGAP;
                    while (y + 1 < height) {
                        g.setColor(this.getBackground().brighter());
                        g.drawLine(x, y, x, y);
                        if (x + 5 < size.width && y + 5 < height) {
                            g.drawLine(x + 2, y + 2, x + 2, y + 2);
                        }
                        g.setColor(this.getBackground().darker().darker());
                        g.drawLine(x + 1, y + 1, x + 1, y + 1);
                        if (x + 5 < size.width && y + 5 < height) {
                            g.drawLine(x + 3, y + 3, x + 3, y + 3);
                        }
                        y += 4;
                    }
                    x += 4;
                }
            }
        }

        @Override
        public Dimension getMinimumSize() {
            return this.dim;
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(6, ToolbarPool.getDefault().getPreferredIconSize());
        }

        @Override
        public Dimension getMaximumSize() {
            return this.max;
        }
    }

    private final class ToolbarBump
    extends JPanel {
        static final int TOPGAP = 2;
        static final int BOTGAP = 2;
        private static final int GRIP_WIDTH = 6;
        Dimension dim;
        Dimension max;

        public ToolbarBump() {
            int width = 6;
            this.dim = new Dimension(width, width);
            this.max = new Dimension(width, Integer.MAX_VALUE);
        }

        @Override
        public void paint(Graphics g) {
            Dimension size = this.getSize();
            int height = size.height - 2;
            g.setColor(this.getBackground());
            int x = 0;
            while (x + 1 < size.width) {
                int y = 2;
                while (y + 1 < height) {
                    g.setColor(this.getBackground().brighter());
                    g.drawLine(x, y, x, y);
                    if (x + 5 < size.width && y + 5 < height) {
                        g.drawLine(x + 2, y + 2, x + 2, y + 2);
                    }
                    g.setColor(this.getBackground().darker().darker());
                    g.drawLine(x + 1, y + 1, x + 1, y + 1);
                    if (x + 5 < size.width && y + 5 < height) {
                        g.drawLine(x + 3, y + 3, x + 3, y + 3);
                    }
                    y += 4;
                }
                x += 4;
            }
        }

        @Override
        public Dimension getMinimumSize() {
            return this.dim;
        }

        @Override
        public Dimension getPreferredSize() {
            return this.getMinimumSize();
        }

        @Override
        public Dimension getMaximumSize() {
            return this.max;
        }
    }

}

