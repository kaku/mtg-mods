/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleRole;
import javax.accessibility.AccessibleState;
import javax.accessibility.AccessibleStateSet;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.view.ViewElement;
import org.netbeans.core.windows.view.ui.DefaultSplitContainer;
import org.netbeans.core.windows.view.ui.ModeResizer;
import org.netbeans.core.windows.view.ui.MultiSplitCell;
import org.netbeans.core.windows.view.ui.MultiSplitDivider;

public class MultiSplitPane
extends JPanel
implements MouseMotionListener,
MouseListener {
    private MultiSplitDivider draggingDivider;
    private ArrayList<MultiSplitDivider> dividers = new ArrayList();
    private boolean dirty = true;
    private ArrayList<MultiSplitCell> cells = new ArrayList();
    private int orientation;
    private int dividerSize;
    private boolean userMovedSplit = false;
    private PropertyChangeListener _lookAndFeelListener;

    public MultiSplitPane() {
        this.setLayout(new MultiSplitLayout());
        this.addMouseMotionListener(this);
        this.addMouseListener(this);
        this.updateLAF();
    }

    private void updateLAF() {
        Color bkColor = UIManager.getColor("NbSplitPane.background");
        if (null != bkColor) {
            this.setBackground(bkColor);
            this.setOpaque(true);
        }
        if (UIManager.getBoolean("NbMainWindow.showCustomBackground")) {
            this.setOpaque(false);
        }
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._lookAndFeelListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                MultiSplitPane.this.updateLAF();
            }
        };
        UIManager.addPropertyChangeListener(this._lookAndFeelListener);
    }

    @Override
    public void removeNotify() {
        UIManager.removePropertyChangeListener(this._lookAndFeelListener);
        this._lookAndFeelListener = null;
        super.removeNotify();
    }

    public void setChildren(int orientation, ViewElement[] childrenViews, double[] splitWeights) {
        assert (childrenViews.length == splitWeights.length);
        this.orientation = orientation;
        List<Component> currentComponents = this.collectComponents();
        this.cells.clear();
        for (int i = 0; i < childrenViews.length; ++i) {
            this.cells.add(new MultiSplitCell(childrenViews[i], splitWeights[i], this.isHorizontalSplit()));
        }
        List<Component> updatedComponents = this.collectComponents();
        ArrayList<Component> removed = new ArrayList<Component>(currentComponents);
        removed.removeAll(updatedComponents);
        ArrayList<Component> added = new ArrayList<Component>(updatedComponents);
        added.removeAll(currentComponents);
        for (Component c2 : removed) {
            this.remove(c2);
        }
        for (Component c2 : added) {
            this.add(c2);
        }
        this.dirty = true;
        ModeResizer.stop();
    }

    int getCellCount() {
        return this.cells.size();
    }

    MultiSplitCell cellAt(int index) {
        assert (index >= 0);
        assert (index < this.cells.size());
        return this.cells.get(index);
    }

    public void removeViewElementAt(int index) {
        if (index < 0 || index >= this.cells.size()) {
            return;
        }
        MultiSplitCell cellToRemove = this.cells.remove(index);
        this.remove(cellToRemove.getComponent());
        this.dirty = true;
    }

    public int getOrientation() {
        return this.orientation;
    }

    public boolean isVerticalSplit() {
        return this.orientation == 0;
    }

    public boolean isHorizontalSplit() {
        return this.orientation == 1;
    }

    private List<Component> collectComponents() {
        ArrayList<Component> res = new ArrayList<Component>(this.getCellCount());
        for (int i = 0; i < this.getCellCount(); ++i) {
            MultiSplitCell cell = this.cellAt(i);
            Component c = cell.getComponent();
            assert (null != c);
            res.add(c);
        }
        return res;
    }

    public void calculateSplitWeights(List<ViewElement> visibleViews, List<Double> splitWeights) {
        double size;
        double d = size = this.isHorizontalSplit() ? (double)this.getSize().width : (double)this.getSize().height;
        if (size <= 0.0) {
            return;
        }
        for (int i = 0; i < this.getCellCount(); ++i) {
            MultiSplitCell cell = this.cellAt(i);
            double weight = (double)cell.getSize() / size;
            splitWeights.add(weight);
            visibleViews.add(cell.getViewElement());
        }
    }

    public int getDividerSize() {
        return this.dividerSize;
    }

    public void setDividerSize(int newDividerSize) {
        this.dirty |= newDividerSize != this.dividerSize;
        this.dividerSize = newDividerSize;
    }

    @Override
    public Dimension getMinimumSize() {
        Dimension d = new Dimension();
        for (int i = 0; i < this.getCellCount(); ++i) {
            MultiSplitCell cell = this.cellAt(i);
            int size = cell.getMinimumSize();
            Dimension minDim = cell.getComponent().getMinimumSize();
            if (this.isHorizontalSplit()) {
                d.width += size;
                if (minDim.height <= d.height) continue;
                d.height = minDim.height;
                continue;
            }
            d.height += size;
            if (minDim.width <= d.width) continue;
            d.width = minDim.width;
        }
        if (this.isHorizontalSplit()) {
            d.width += (this.getCellCount() - 1) * this.getDividerSize();
        } else {
            d.height += (this.getCellCount() - 1) * this.getDividerSize();
        }
        return d;
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        this.switchCursor(e);
        e.consume();
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (null == this.draggingDivider) {
            return;
        }
        this.draggingDivider.dragTo(e.getPoint());
        e.consume();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (null == this.draggingDivider) {
            return;
        }
        Point p = new Point(e.getPoint());
        this.draggingDivider.finishDraggingTo(p);
        this.draggingDivider = null;
        this.setCursor(Cursor.getDefaultCursor());
        e.consume();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (!Switches.isTopComponentResizingEnabled()) {
            return;
        }
        MultiSplitDivider divider = this.dividerAtPoint(e.getPoint());
        if (null == divider) {
            return;
        }
        this.draggingDivider = divider;
        divider.startDragging(e.getPoint());
        e.consume();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (null == this.draggingDivider) {
            this.setCursor(Cursor.getDefaultCursor());
        }
        e.consume();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    private void switchCursor(MouseEvent e) {
        if (!Switches.isTopComponentResizingEnabled()) {
            return;
        }
        MultiSplitDivider divider = this.dividerAtPoint(e.getPoint());
        if (null == divider) {
            this.setCursor(Cursor.getDefaultCursor());
        } else if (divider.isHorizontal()) {
            this.setCursor(Cursor.getPredefinedCursor(11));
        } else {
            this.setCursor(Cursor.getPredefinedCursor(8));
        }
    }

    private MultiSplitDivider dividerAtPoint(Point p) {
        for (MultiSplitDivider d : this.dividers) {
            if (!d.containsPoint(p)) continue;
            return d;
        }
        return null;
    }

    private void resize(int newSize) {
        int currentSize = 0;
        for (int i = 0; i < this.getCellCount(); ++i) {
            currentSize += this.cellAt(i).getRequiredSize();
        }
        int totalDividerSize = this.getDividerSize() * (this.getCellCount() - 1);
        int newNetSize = newSize - totalDividerSize;
        int delta = newNetSize - currentSize;
        if (delta > 0) {
            this.grow(delta);
        } else if (delta < 0 && (delta = this.shrink(delta)) > 0) {
            newNetSize -= delta;
        }
        int totalSize = 0;
        for (int i2 = 0; i2 < this.getCellCount(); ++i2) {
            MultiSplitCell cell = this.cellAt(i2);
            totalSize += cell.getRequiredSize();
        }
        if (totalSize < newNetSize) {
            MultiSplitCell lastCell = this.cellAt(this.getCellCount() - 1);
            lastCell.setRequiredSize(lastCell.getRequiredSize() + (newNetSize - totalSize));
        }
    }

    private void grow(int delta) {
        List<MultiSplitCell> hungryCells = this.getResizeHungryCells();
        if (!hungryCells.isEmpty()) {
            this.normalizeResizeWeights(hungryCells);
            this.distributeDelta(delta, hungryCells);
        } else {
            ArrayList<MultiSplitCell> resizeableCells = new ArrayList<MultiSplitCell>(this.cells);
            this.normalizeResizeWeights(resizeableCells);
            this.distributeDelta(delta, resizeableCells);
        }
    }

    private int shrink(int negativeDelta) {
        int delta = - negativeDelta;
        List<MultiSplitCell> hungryCells = this.getResizeHungryCells();
        int resizeArea = this.calculateShrinkableArea(hungryCells);
        if (resizeArea >= delta) {
            resizeArea = delta;
            delta = 0;
        } else {
            delta -= resizeArea;
        }
        if (resizeArea > 0) {
            this.distributeDelta(- resizeArea, hungryCells);
        }
        if (delta > 0) {
            ArrayList<MultiSplitCell> resizeableCells = new ArrayList<MultiSplitCell>(this.cells);
            resizeArea = this.calculateShrinkableArea(resizeableCells);
            if (resizeArea >= delta) {
                resizeArea = delta;
                delta = 0;
            } else {
                delta -= resizeArea;
            }
            if (resizeArea > 0) {
                this.distributeDelta(- resizeArea, resizeableCells);
            }
        }
        return delta;
    }

    private int calculateShrinkableArea(List<MultiSplitCell> cells) {
        int currentSize;
        int minSize;
        int res = 0;
        ArrayList<MultiSplitCell> nonShrinkable = new ArrayList<MultiSplitCell>(cells.size());
        for (int i = 0; i < cells.size(); ++i) {
            MultiSplitCell c = cells.get(i);
            currentSize = c.getRequiredSize();
            if (currentSize - (minSize = c.getMinimumSize()) > 0) {
                res += currentSize - minSize;
                continue;
            }
            nonShrinkable.add(c);
        }
        cells.removeAll(nonShrinkable);
        for (MultiSplitCell c : cells) {
            currentSize = c.getRequiredSize();
            minSize = c.getMinimumSize();
            c.setNormalizedResizeWeight(1.0 * (double)(currentSize - minSize) / (double)res);
        }
        return res;
    }

    private void distributeDelta(int delta, List<MultiSplitCell> cells) {
        int totalDistributed = 0;
        for (int i = 0; i < cells.size(); ++i) {
            MultiSplitCell cell = cells.get(i);
            int cellDelta = (int)(cell.getNormalizedResizeWeight() * (double)delta);
            totalDistributed += cellDelta;
            if (i == cells.size() - 1) {
                cellDelta += delta - totalDistributed;
            }
            cell.setRequiredSize(cell.getRequiredSize() + cellDelta);
        }
    }

    private void normalizeResizeWeights(List cells) {
        if (cells.isEmpty()) {
            return;
        }
        double totalWeight = 0.0;
        for (MultiSplitCell c : cells) {
            totalWeight += c.getResizeWeight();
        }
        double deltaWeight = (1.0 - totalWeight) / (double)cells.size();
        for (MultiSplitCell c2 : cells) {
            c2.setNormalizedResizeWeight(c2.getResizeWeight() + deltaWeight);
        }
    }

    List<MultiSplitCell> getResizeHungryCells() {
        ArrayList<MultiSplitCell> res = new ArrayList<MultiSplitCell>(this.cells.size());
        for (int i = 0; i < this.getCellCount(); ++i) {
            MultiSplitCell cell = this.cellAt(i);
            if (cell.getResizeWeight() <= 0.0) continue;
            res.add(cell);
        }
        return res;
    }

    void createDividers() {
        this.dividers.clear();
        for (int i = 0; i < this.getCellCount() - 1; ++i) {
            MultiSplitCell first = this.cellAt(i);
            MultiSplitCell second = this.cellAt(i + 1);
            MultiSplitDivider divider = new MultiSplitDivider(this, first, second);
            this.dividers.add(divider);
        }
    }

    void splitterMoved() {
        this.userMovedSplit = true;
        this.validate();
    }

    @Override
    public AccessibleContext getAccessibleContext() {
        if (this.accessibleContext == null) {
            this.accessibleContext = new AccessibleMultiSplitPane();
        }
        return this.accessibleContext;
    }

    int getDividerAccessibleIndex(MultiSplitDivider divider) {
        int res = this.dividers.indexOf(divider);
        return res += this.getAccessibleContext().getAccessibleChildrenCount() - this.dividers.size();
    }

    public void startResizing(Component child) {
        MultiSplitDivider divider = this.findSplitDividerFor(child);
        if (null == divider) {
            return;
        }
        MultiSplitCell resizingCell = this.findSplitCellFor(child);
        MultiSplitDivider parentDivider = null;
        MultiSplitPane parentSplit = (MultiSplitPane)SwingUtilities.getAncestorOfClass(MultiSplitPane.class, this);
        if (null != parentSplit) {
            parentDivider = parentSplit.findSplitDividerFor(this);
        }
        ModeResizer.start(resizingCell.getComponent(), divider, parentDivider);
    }

    private MultiSplitDivider findSplitDividerFor(Component c) {
        MultiSplitCell cell = this.findSplitCellFor(c);
        if (null == cell) {
            return null;
        }
        int index = this.cells.indexOf(cell);
        if (index >= this.dividers.size()) {
            --index;
        }
        return this.dividers.get(index);
    }

    private MultiSplitCell findSplitCellFor(Component c) {
        Container container = SwingUtilities.getAncestorOfClass(DefaultSplitContainer.ModePanel.class, c);
        if (null == container) {
            if (c instanceof MultiSplitPane) {
                container = (Container)c;
            } else {
                return null;
            }
        }
        for (MultiSplitCell cell : this.cells) {
            if (!cell.getComponent().equals(container)) continue;
            return cell;
        }
        return null;
    }

    protected class MultiSplitLayout
    implements LayoutManager {
        protected MultiSplitLayout() {
        }

        @Override
        public void layoutContainer(Container c) {
            if (c != MultiSplitPane.this) {
                return;
            }
            int newSize = MultiSplitPane.this.isHorizontalSplit() ? MultiSplitPane.this.getSize().width : MultiSplitPane.this.getSize().height;
            for (int i = 0; i < MultiSplitPane.this.getCellCount(); ++i) {
                MultiSplitCell cell = MultiSplitPane.this.cellAt(i);
                cell.maybeResetToInitialSize(newSize);
            }
            MultiSplitPane.this.resize(newSize);
            this.layoutCells();
            if (MultiSplitPane.this.userMovedSplit) {
                MultiSplitPane.this.userMovedSplit = false;
                MultiSplitPane.this.firePropertyChange("splitPositions", null, this);
            }
            MultiSplitPane.this.createDividers();
        }

        private void layoutCells() {
            int x = 0;
            int y = 0;
            int width = MultiSplitPane.this.getWidth();
            int height = MultiSplitPane.this.getHeight();
            for (int i = 0; i < MultiSplitPane.this.getCellCount(); ++i) {
                MultiSplitCell cell = MultiSplitPane.this.cellAt(i);
                if (cell.getComponent().getParent() != MultiSplitPane.this) {
                    MultiSplitPane.this.add(cell.getComponent());
                }
                if (MultiSplitPane.this.isHorizontalSplit()) {
                    width = cell.getRequiredSize();
                } else {
                    height = cell.getRequiredSize();
                }
                cell.layout(x, y, width, height);
                if (MultiSplitPane.this.isHorizontalSplit()) {
                    x += width;
                    if (i >= MultiSplitPane.this.getCellCount()) continue;
                    x += MultiSplitPane.this.getDividerSize();
                    continue;
                }
                y += height;
                if (i >= MultiSplitPane.this.getCellCount() - 1) continue;
                y += MultiSplitPane.this.getDividerSize();
            }
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            return container.getSize();
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            return container.getSize();
        }

        @Override
        public void removeLayoutComponent(Component c) {
        }

        @Override
        public void addLayoutComponent(String string, Component c) {
        }
    }

    protected class AccessibleMultiSplitPane
    extends JComponent.AccessibleJComponent {
        protected AccessibleMultiSplitPane() {
            super(MultiSplitPane.this);
        }

        @Override
        public AccessibleStateSet getAccessibleStateSet() {
            AccessibleStateSet states = super.getAccessibleStateSet();
            if (MultiSplitPane.this.isHorizontalSplit()) {
                states.add(AccessibleState.HORIZONTAL);
            } else {
                states.add(AccessibleState.VERTICAL);
            }
            return states;
        }

        @Override
        public AccessibleRole getAccessibleRole() {
            return AccessibleRole.SPLIT_PANE;
        }

        @Override
        public Accessible getAccessibleAt(Point p) {
            MultiSplitDivider divider = MultiSplitPane.this.dividerAtPoint(p);
            if (null != divider) {
                return divider;
            }
            return super.getAccessibleAt(p);
        }

        @Override
        public Accessible getAccessibleChild(int i) {
            int childrenCount = super.getAccessibleChildrenCount();
            if (i < childrenCount) {
                return super.getAccessibleChild(i);
            }
            if (i - childrenCount >= MultiSplitPane.this.dividers.size()) {
                return null;
            }
            MultiSplitDivider divider = (MultiSplitDivider)MultiSplitPane.this.dividers.get(i - childrenCount);
            return divider;
        }

        @Override
        public int getAccessibleChildrenCount() {
            return super.getAccessibleChildrenCount() + MultiSplitPane.this.dividers.size();
        }
    }

}

