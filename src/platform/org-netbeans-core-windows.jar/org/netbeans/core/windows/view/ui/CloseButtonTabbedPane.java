/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.CloseButtonFactory
 *  org.openide.util.Exceptions
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.FocusTraversalPolicy;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.plaf.TabbedPaneUI;
import javax.swing.plaf.UIResource;
import org.netbeans.core.windows.actions.MaximizeWindowAction;
import org.openide.awt.CloseButtonFactory;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;

final class CloseButtonTabbedPane
extends JTabbedPane
implements PropertyChangeListener {
    private Action scrollLeftAction;
    private Action scrollRightAction;
    private static final boolean IS_AQUA_LAF = "Aqua".equals(UIManager.getLookAndFeel().getID());
    private int pressedCloseButtonIndex = -1;
    private int mouseOverCloseButtonIndex = -1;
    private static final boolean HTML_TABS_BROKEN = CloseButtonTabbedPane.htmlTabsBroken();
    private final Pattern removeHtmlTags = HTML_TABS_BROKEN ? Pattern.compile("\\<.*?\\>") : null;

    CloseButtonTabbedPane() {
        this.addMouseListener(new MouseAdapter(){
            int lastIdx;

            @Override
            public void mousePressed(MouseEvent e) {
                if (SwingUtilities.isMiddleMouseButton(e)) {
                    this.lastIdx = CloseButtonTabbedPane.this.getUI().tabForCoordinate(CloseButtonTabbedPane.this, e.getX(), e.getY());
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (SwingUtilities.isMiddleMouseButton(e)) {
                    int idx = CloseButtonTabbedPane.this.getUI().tabForCoordinate(CloseButtonTabbedPane.this, e.getX(), e.getY());
                    if (idx >= 0) {
                        Component comp = CloseButtonTabbedPane.this.getComponentAt(idx);
                        if (idx == this.lastIdx && comp != null && !CloseButtonTabbedPane.this.hideCloseButton(comp)) {
                            CloseButtonTabbedPane.this.fireCloseRequest(comp);
                        }
                    }
                    this.lastIdx = -1;
                }
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                MaximizeWindowAction mwa;
                TopComponent tc;
                if (e.getClickCount() == 2 && SwingUtilities.isLeftMouseButton(e) && null != (tc = (TopComponent)SwingUtilities.getAncestorOfClass(TopComponent.class, CloseButtonTabbedPane.this)) && (mwa = new MaximizeWindowAction(tc)).isEnabled()) {
                    mwa.actionPerformed(null);
                }
            }
        });
        this.addMouseWheelListener(new MouseWheelListener(){

            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                if (e.getScrollType() == 0) {
                    if (e.getWheelRotation() < 0) {
                        CloseButtonTabbedPane.this.scrollTabsLeft();
                    } else {
                        CloseButtonTabbedPane.this.scrollTabsRight();
                    }
                }
            }
        });
        this.setFocusable(false);
        this.setFocusCycleRoot(true);
        this.setFocusTraversalPolicy(new CBTPPolicy());
        this.setTabLayoutPolicy(1);
    }

    private Component sel() {
        Component c = this.getSelectedComponent();
        return c == null ? this : c;
    }

    @Override
    public void insertTab(String title, Icon icon, Component component, String tip, int index) {
        super.insertTab(title, icon, component, tip, index);
        component.addPropertyChangeListener("noCloseButton", this);
        if (!this.hideCloseButton(component)) {
            this.setTabComponentAt(index, new ButtonTab());
        }
        if (title != null) {
            this.setTitleAt(index, title);
        }
        this.validate();
    }

    @Override
    public void updateUI() {
        super.updateUI();
        ActionMap am = this.getActionMap();
        Action a = am.get("navigatePageUp");
        if (null != a && !(a instanceof MyNavigateAction)) {
            am.put("navigatePageUp", new MyNavigateAction(a));
        }
        if (null != (a = am.get("navigatePageDown")) && !(a instanceof MyNavigateAction)) {
            am.put("navigatePageDown", new MyNavigateAction(a));
        }
        this.scrollRightAction = am.get("scrollTabsForwardAction");
        this.scrollLeftAction = am.get("scrollTabsBackwardAction");
    }

    private void scrollTabsLeft() {
        if (IS_AQUA_LAF) {
            int selIndex = this.getSelectedIndex();
            if (selIndex > 0) {
                this.setSelectedIndex(selIndex - 1);
            }
        } else if (null != this.scrollLeftAction && this.scrollLeftAction.isEnabled()) {
            this.scrollLeftAction.actionPerformed(new ActionEvent(this, 0, ""));
        }
    }

    private void scrollTabsRight() {
        if (IS_AQUA_LAF) {
            int selIndex = this.getSelectedIndex();
            if (selIndex < this.getTabCount() - 1) {
                this.setSelectedIndex(selIndex + 1);
            }
        } else if (null != this.scrollRightAction && this.scrollRightAction.isEnabled()) {
            this.scrollRightAction.actionPerformed(new ActionEvent(this, 0, ""));
        }
    }

    @Override
    public void removeTabAt(int index) {
        Component c = this.getComponentAt(index);
        c.removePropertyChangeListener("noCloseButton", this);
        super.removeTabAt(index);
    }

    private static boolean htmlTabsBroken() {
        String version = System.getProperty("java.version");
        for (int i = 14; i < 18; ++i) {
            if (!version.startsWith("1.6.0_" + i)) continue;
            return true;
        }
        if (version.startsWith("1.6.0") && IS_AQUA_LAF) {
            return true;
        }
        return false;
    }

    @Override
    public void setTitleAt(int idx, String title) {
        if (title == null) {
            super.setTitleAt(idx, null);
            return;
        }
        if (this.removeHtmlTags != null && title.startsWith("<html>")) {
            title = this.removeHtmlTags.matcher(title).replaceAll("");
            title = title.replace("&nbsp;", "");
        }
        super.setTitleAt(idx, title);
    }

    private Component findTabAt(int index) {
        int componentIndex = -1;
        for (Component c : this.getComponents()) {
            if (c instanceof UIResource || ++componentIndex != index) continue;
            return c;
        }
        return null;
    }

    private boolean hideCloseButton(Component c) {
        Object prop;
        if (c != null && c instanceof JComponent && (prop = ((JComponent)c).getClientProperty("noCloseButton")) != null && prop instanceof Boolean && ((Boolean)prop).booleanValue()) {
            return true;
        }
        return false;
    }

    private Rectangle getCloseButtonBoundsAt(int i) {
        Component c = this.findTabAt(i);
        if (this.hideCloseButton(c)) {
            return null;
        }
        Rectangle b = this.getBoundsAt(i);
        if (b == null) {
            return null;
        }
        b = new Rectangle(b);
        CloseButtonTabbedPane.fixGetBoundsAt(b);
        Dimension tabsz = this.getSize();
        if (b.x + b.width >= tabsz.width || b.y + b.height >= tabsz.height) {
            return null;
        }
        if (b.width == 0 || b.height == 0) {
            return null;
        }
        if ((this.isWindowsVistaLaF() || this.isWindowsXPLaF() || this.isWindowsLaF()) && i == this.getSelectedIndex()) {
            b.x -= 3;
            b.y -= 2;
        } else if (this.isWindowsXPLaF() || this.isWindowsLaF() || IS_AQUA_LAF) {
            b.x -= 2;
        } else if (this.isGTKLaF() && i == this.getSelectedIndex()) {
            --b.x;
            b.y -= 2;
        }
        if (i == this.getTabCount() - 1) {
            if (this.isMetalLaF()) {
                --b.x;
            } else if (IS_AQUA_LAF) {
                b.x -= 3;
            }
        }
        return new Rectangle(b.x + b.width - 13, b.y + b.height / 2 - 5, 12, 12);
    }

    private boolean isWindowsVistaLaF() {
        String osName = System.getProperty("os.name");
        return osName.indexOf("Vista") >= 0 || osName.equals("Windows NT (unknown)") && "6.0".equals(System.getProperty("os.version"));
    }

    private boolean isWindowsXPLaF() {
        Boolean isXP = (Boolean)Toolkit.getDefaultToolkit().getDesktopProperty("win.xpstyle.themeActive");
        return this.isWindowsLaF() && isXP != null && isXP != false;
    }

    private boolean isWindowsLaF() {
        String lfID = UIManager.getLookAndFeel().getID();
        return lfID.endsWith("Windows");
    }

    private boolean isMetalLaF() {
        String lfID = UIManager.getLookAndFeel().getID();
        return "Metal".equals(lfID);
    }

    private boolean isGTKLaF() {
        return "GTK".equals(UIManager.getLookAndFeel().getID());
    }

    private void setPressedCloseButtonIndex(int index) {
        Rectangle r;
        if (this.pressedCloseButtonIndex == index) {
            return;
        }
        if (this.pressedCloseButtonIndex >= 0 && this.pressedCloseButtonIndex < this.getTabCount()) {
            JComponent c;
            r = this.getCloseButtonBoundsAt(this.pressedCloseButtonIndex);
            if (r != null) {
                this.repaint(r.x, r.y, r.width + 2, r.height + 2);
            }
            if ((c = this._getJComponentAt(this.pressedCloseButtonIndex)) != null) {
                this.setToolTipTextAt(this.pressedCloseButtonIndex, c.getToolTipText());
            }
        }
        this.pressedCloseButtonIndex = index;
        if (this.pressedCloseButtonIndex >= 0 && this.pressedCloseButtonIndex < this.getTabCount()) {
            r = this.getCloseButtonBoundsAt(this.pressedCloseButtonIndex);
            if (r != null) {
                this.repaint(r.x, r.y, r.width + 2, r.height + 2);
            }
            this.setMouseOverCloseButtonIndex(-1);
            this.setToolTipTextAt(this.pressedCloseButtonIndex, null);
        }
    }

    private void setMouseOverCloseButtonIndex(int index) {
        Rectangle r;
        if (this.mouseOverCloseButtonIndex == index) {
            return;
        }
        if (this.mouseOverCloseButtonIndex >= 0 && this.mouseOverCloseButtonIndex < this.getTabCount()) {
            JComponent c;
            r = this.getCloseButtonBoundsAt(this.mouseOverCloseButtonIndex);
            if (r != null) {
                this.repaint(r.x, r.y, r.width + 2, r.height + 2);
            }
            if ((c = this._getJComponentAt(this.mouseOverCloseButtonIndex)) != null) {
                this.setToolTipTextAt(this.mouseOverCloseButtonIndex, c.getToolTipText());
            }
        }
        this.mouseOverCloseButtonIndex = index;
        if (this.mouseOverCloseButtonIndex >= 0 && this.mouseOverCloseButtonIndex < this.getTabCount()) {
            r = this.getCloseButtonBoundsAt(this.mouseOverCloseButtonIndex);
            if (r != null) {
                this.repaint(r.x, r.y, r.width + 2, r.height + 2);
            }
            this.setPressedCloseButtonIndex(-1);
            this.setToolTipTextAt(this.mouseOverCloseButtonIndex, null);
        }
    }

    private JComponent _getJComponentAt(int tabIndex) {
        Component c = this.getComponentAt(tabIndex);
        return c instanceof JComponent ? (JComponent)c : null;
    }

    private void fireCloseRequest(Component c) {
        int idx;
        this.firePropertyChange("close", null, c);
        if (this.getTabLayoutPolicy() == 1 && (idx = this.getSelectedIndex()) > 0) {
            this.setSelectedIndex(0);
            this.setSelectedIndex(idx);
        }
    }

    static void fixGetBoundsAt(Rectangle b) {
        if (b.y < 0) {
            b.y = - b.y;
        }
        if (b.x < 0) {
            b.x = - b.x;
        }
    }

    static int findTabForCoordinate(JTabbedPane tab, int x, int y) {
        for (int i = 0; i < tab.getTabCount(); ++i) {
            Rectangle b = tab.getBoundsAt(i);
            if (b == null) continue;
            b = new Rectangle(b);
            CloseButtonTabbedPane.fixGetBoundsAt(b);
            if (!b.contains(x, y)) continue;
            return i;
        }
        return -1;
    }

    @Override
    protected void processMouseEvent(MouseEvent me) {
        try {
            super.processMouseEvent(me);
        }
        catch (ArrayIndexOutOfBoundsException aioobe) {
            Exceptions.attachLocalizedMessage((Throwable)aioobe, (String)"Suppressed AIOOBE bug in BasicTabbedPaneUI");
            Logger.getAnonymousLogger().log(Level.WARNING, null, aioobe);
        }
    }

    @Override
    protected void fireStateChanged() {
        block2 : {
            try {
                super.fireStateChanged();
            }
            catch (ArrayIndexOutOfBoundsException e) {
                if (Utilities.isMac()) break block2;
                throw e;
            }
        }
    }

    @Override
    public Color getBackgroundAt(int index) {
        if (this.isWindowsLaF() && !this.isWindowsXPLaF()) {
            Color unselected;
            Color selected = UIManager.getColor("controlHighlight");
            if (selected.equals(unselected = UIManager.getColor("control"))) {
                unselected = new Color(Math.max(selected.getRed() - 12, 0), Math.max(selected.getGreen() - 12, 0), Math.max(selected.getBlue() - 12, 0));
            }
            return index == this.getSelectedIndex() ? selected : unselected;
        }
        return super.getBackgroundAt(index);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getSource() instanceof Component) {
            assert (evt.getPropertyName().equals("noCloseButton"));
            Component c = (Component)evt.getSource();
            int idx = this.indexOfComponent(c);
            boolean noCloseButton = (Boolean)evt.getNewValue();
            this.setTabComponentAt(idx, noCloseButton ? null : new ButtonTab());
        }
    }

    private class MyNavigateAction
    extends AbstractAction {
        private final Action orig;

        public MyNavigateAction(Action orig) {
            this.orig = orig;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.orig.actionPerformed(e);
        }

        @Override
        public boolean isEnabled() {
            return CloseButtonTabbedPane.this.getTabCount() > 1;
        }
    }

    class ButtonTab
    extends JPanel {
        JLabel label;

        public ButtonTab() {
            super(new FlowLayout(0, 0, 0));
            this.setOpaque(false);
            this.label = new JLabel("", CloseButtonTabbedPane.this){
                private String lastText;
                final /* synthetic */ CloseButtonTabbedPane val$this$0;

                @Override
                public String getText() {
                    String currentText = "";
                    int i = CloseButtonTabbedPane.this.indexOfTabComponent(ButtonTab.this);
                    if (i >= 0) {
                        currentText = CloseButtonTabbedPane.this.getTitleAt(i);
                    }
                    if (null != this.lastText && this.lastText.equals(currentText)) {
                        return this.lastText;
                    }
                    this.lastText = currentText;
                    if (!super.getText().equals(currentText)) {
                        this.setText(currentText);
                    }
                    return currentText;
                }

                @Override
                public Icon getIcon() {
                    Icon icon;
                    int i = CloseButtonTabbedPane.this.indexOfTabComponent(ButtonTab.this);
                    Icon icon2 = icon = i >= 0 ? CloseButtonTabbedPane.this.getIconAt(i) : null;
                    if (super.getIcon() != icon) {
                        this.setIcon(icon);
                    }
                    return icon;
                }
            };
            this.add(this.label);
            JButton tabCloseButton = CloseButtonFactory.createCloseButton();
            tabCloseButton.addActionListener(new ActionListener(CloseButtonTabbedPane.this){
                final /* synthetic */ CloseButtonTabbedPane val$this$0;

                @Override
                public void actionPerformed(ActionEvent e) {
                    int i = CloseButtonTabbedPane.this.indexOfTabComponent(ButtonTab.this);
                    if (i != -1) {
                        CloseButtonTabbedPane.this.fireCloseRequest(CloseButtonTabbedPane.this.getComponentAt(i));
                    }
                }
            });
            this.add(tabCloseButton);
        }

    }

    private class CBTPPolicy
    extends FocusTraversalPolicy {
        private CBTPPolicy() {
        }

        @Override
        public Component getComponentAfter(Container aContainer, Component aComponent) {
            return CloseButtonTabbedPane.this.sel();
        }

        @Override
        public Component getComponentBefore(Container aContainer, Component aComponent) {
            return CloseButtonTabbedPane.this.sel();
        }

        @Override
        public Component getFirstComponent(Container aContainer) {
            return CloseButtonTabbedPane.this.sel();
        }

        @Override
        public Component getLastComponent(Container aContainer) {
            return CloseButtonTabbedPane.this.sel();
        }

        @Override
        public Component getDefaultComponent(Container aContainer) {
            return CloseButtonTabbedPane.this.sel();
        }
    }

}

