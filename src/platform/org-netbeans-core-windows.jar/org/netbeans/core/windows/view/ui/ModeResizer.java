/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view.ui;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.AWTEventListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import org.netbeans.core.windows.view.ui.MultiSplitDivider;
import org.openide.windows.TopComponent;

final class ModeResizer
implements AWTEventListener,
PropertyChangeListener {
    private static final int STEP_SMALL = 1;
    private static final int STEP_NORMAL = 10;
    private static final int STEP_LARGE = 50;
    private static ModeResizer currentResizer;
    private final Component resizingComponent;
    private final MultiSplitDivider splitter;
    private final MultiSplitDivider parentSplitter;
    private final Point originalLocation;
    private final Point originalParentLocation;
    private Component oldGlass = null;
    private GlassPane glass = null;
    private JFrame frame = null;

    private ModeResizer(Component c, MultiSplitDivider splitter, MultiSplitDivider parentSplitter) {
        this.resizingComponent = c;
        this.splitter = splitter;
        this.parentSplitter = parentSplitter;
        this.originalLocation = splitter.initDragMinMax();
        this.originalParentLocation = null == parentSplitter ? null : parentSplitter.initDragMinMax();
    }

    static void start(Component c, MultiSplitDivider splitter, MultiSplitDivider parentSplitter) {
        if (!SwingUtilities.isEventDispatchThread()) {
            throw new IllegalStateException("This method must be called from EDT.");
        }
        if (null != currentResizer) {
            currentResizer.stop(false);
            currentResizer = null;
        }
        currentResizer = new ModeResizer(c, splitter, parentSplitter);
        currentResizer.start();
    }

    private void start() {
        Toolkit.getDefaultToolkit().addAWTEventListener(this, 8);
        TopComponent.getRegistry().addPropertyChangeListener((PropertyChangeListener)this);
        Window w = SwingUtilities.getWindowAncestor(this.resizingComponent);
        if (w instanceof JFrame) {
            this.frame = (JFrame)w;
            this.oldGlass = this.frame.getGlassPane();
            this.glass = new GlassPane(this.resizingComponent);
            this.frame.setGlassPane(this.glass);
            this.glass.setVisible(true);
            this.glass.invalidate();
            this.glass.revalidate();
            this.glass.repaint();
            this.glass.refresh();
        }
    }

    static void stop() {
        if (null != currentResizer) {
            currentResizer.stop(true);
        }
    }

    @Override
    public void eventDispatched(AWTEvent e) {
        if (!(e instanceof KeyEvent)) {
            return;
        }
        KeyEvent ke = (KeyEvent)e;
        ke.consume();
        if (e.getID() == 401) {
            int nStep = 0;
            switch (ke.getModifiers()) {
                case 1: {
                    nStep = 1;
                    break;
                }
                case 0: {
                    nStep = 10;
                    break;
                }
                case 2: {
                    nStep = 50;
                }
            }
            if (nStep != 0) {
                switch (ke.getKeyCode()) {
                    case 37: {
                        this.moveBy(- nStep, 0);
                        break;
                    }
                    case 39: {
                        this.moveBy(nStep, 0);
                        break;
                    }
                    case 38: {
                        this.moveBy(0, - nStep);
                        break;
                    }
                    case 40: {
                        this.moveBy(0, nStep);
                        break;
                    }
                    case 10: {
                        this.stop(true);
                        break;
                    }
                    case 27: {
                        this.stop(true);
                    }
                }
            }
        }
    }

    private void moveBy(int deltaX, int deltaY) {
        if (deltaX != 0) {
            if (this.splitter.isHorizontal()) {
                this.splitter.resize(deltaX);
            } else if (null != this.parentSplitter && this.parentSplitter.isHorizontal()) {
                this.parentSplitter.resize(deltaX);
            }
        }
        if (deltaY != 0) {
            if (this.splitter.isVertical()) {
                this.splitter.resize(deltaY);
            } else if (null != this.parentSplitter && this.parentSplitter.isVertical()) {
                this.parentSplitter.resize(deltaY);
            }
        }
        if (null != this.glass) {
            this.glass.refresh();
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("activated".equals(evt.getPropertyName()) || "opened".equals(evt.getPropertyName())) {
            this.stop(true);
        }
    }

    private void stop(boolean commitChanges) {
        Toolkit.getDefaultToolkit().removeAWTEventListener(this);
        TopComponent.getRegistry().removePropertyChangeListener((PropertyChangeListener)this);
        if (null != this.frame) {
            boolean glassVisible = this.oldGlass.isVisible();
            this.frame.setGlassPane(this.oldGlass);
            this.oldGlass.setVisible(glassVisible);
        }
        if (!commitChanges) {
            if (null != this.parentSplitter) {
                this.parentSplitter.finishDraggingTo(this.originalParentLocation);
            }
            this.splitter.finishDraggingTo(this.originalLocation);
        }
        if (currentResizer == this) {
            currentResizer = null;
        }
    }

    private static class GlassPane
    extends JPanel {
        private final Component resizingComponent;
        private final JLabel lbl;
        private final JPanel panel;

        public GlassPane(Component resizingComponent) {
            super(null);
            this.setOpaque(false);
            this.resizingComponent = resizingComponent;
            this.lbl = new JLabel();
            this.panel = new JPanel(new BorderLayout());
            this.panel.setBorder(BorderFactory.createEtchedBorder());
            this.lbl.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
            this.panel.add(this.lbl);
            this.add(this.panel);
        }

        public void refresh() {
            Point p = this.resizingComponent.getLocationOnScreen();
            int width = this.resizingComponent.getWidth();
            int height = this.resizingComponent.getHeight();
            p.x += width / 2;
            p.y += height / 2;
            this.lbl.setText("" + width + " x " + height);
            SwingUtilities.convertPointFromScreen(p, this);
            Dimension size = this.panel.getPreferredSize();
            p.x -= size.width / 2;
            p.y -= size.height / 2;
            this.panel.setLocation(p);
            this.panel.setSize(size);
        }
    }

}

