/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view;

import java.awt.Component;
import java.awt.Frame;
import org.netbeans.core.windows.WindowSystemSnapshot;
import org.netbeans.core.windows.view.ViewEvent;
import org.netbeans.core.windows.view.dnd.TopComponentDraggable;
import org.openide.windows.TopComponent;

public interface View {
    public static final int CHANGE_VISIBILITY_CHANGED = 0;
    public static final int CHANGE_MAIN_WINDOW_BOUNDS_JOINED_CHANGED = 1;
    public static final int CHANGE_MAIN_WINDOW_BOUNDS_SEPARATED_CHANGED = 2;
    public static final int CHANGE_MAIN_WINDOW_FRAME_STATE_JOINED_CHANGED = 3;
    public static final int CHANGE_MAIN_WINDOW_FRAME_STATE_SEPARATED_CHANGED = 4;
    public static final int CHANGE_EDITOR_AREA_STATE_CHANGED = 5;
    public static final int CHANGE_EDITOR_AREA_FRAME_STATE_CHANGED = 6;
    public static final int CHANGE_EDITOR_AREA_BOUNDS_CHANGED = 7;
    public static final int CHANGE_EDITOR_AREA_CONSTRAINTS_CHANGED = 8;
    public static final int CHANGE_ACTIVE_MODE_CHANGED = 9;
    public static final int CHANGE_TOOLBAR_CONFIGURATION_CHANGED = 10;
    public static final int CHANGE_MAXIMIZED_MODE_CHANGED = 11;
    public static final int CHANGE_MODE_ADDED = 12;
    public static final int CHANGE_MODE_REMOVED = 13;
    public static final int CHANGE_MODE_CONSTRAINTS_CHANGED = 14;
    public static final int CHANGE_MODE_BOUNDS_CHANGED = 20;
    public static final int CHANGE_MODE_FRAME_STATE_CHANGED = 21;
    public static final int CHANGE_MODE_SELECTED_TOPCOMPONENT_CHANGED = 22;
    public static final int CHANGE_MODE_TOPCOMPONENT_ADDED = 23;
    public static final int CHANGE_MODE_TOPCOMPONENT_REMOVED = 24;
    public static final int CHANGE_TOPCOMPONENT_DISPLAY_NAME_CHANGED = 31;
    public static final int CHANGE_TOPCOMPONENT_DISPLAY_NAME_ANNOTATION_CHANGED = 32;
    public static final int CHANGE_TOPCOMPONENT_TOOLTIP_CHANGED = 33;
    public static final int CHANGE_TOPCOMPONENT_ICON_CHANGED = 34;
    public static final int CHANGE_TOPCOMPONENT_ATTACHED = 41;
    public static final int CHANGE_TOPCOMPONENT_ARRAY_ADDED = 42;
    public static final int CHANGE_TOPCOMPONENT_ARRAY_REMOVED = 43;
    public static final int CHANGE_TOPCOMPONENT_ACTIVATED = 44;
    public static final int CHANGE_MODE_CLOSED = 45;
    public static final int CHANGE_DND_PERFORMED = 46;
    public static final int CHANGE_TOPCOMPONENT_AUTO_HIDE_ENABLED = 47;
    public static final int CHANGE_TOPCOMPONENT_AUTO_HIDE_DISABLED = 48;
    public static final int CHANGE_UI_UPDATE = 61;
    public static final int TOPCOMPONENT_REQUEST_ATTENTION = 63;
    public static final int TOPCOMPONENT_CANCEL_REQUEST_ATTENTION = 64;
    public static final int TOPCOMPONENT_ATTENTION_HIGHLIGHT_ON = 65;
    public static final int TOPCOMPONENT_ATTENTION_HIGHLIGHT_OFF = 66;
    public static final int CHANGE_MAXIMIZE_TOPCOMPONENT_SLIDE_IN = 67;
    public static final int TOPCOMPONENT_SHOW_BUSY = 70;
    public static final int TOPCOMPONENT_HIDE_BUSY = 71;

    public void changeGUI(ViewEvent[] var1, WindowSystemSnapshot var2);

    public boolean isDragInProgress();

    public Frame getMainWindow();

    public Component getEditorAreaComponent();

    public String guessSlideSide(TopComponent var1);

    public void userStartedKeyboardDragAndDrop(TopComponentDraggable var1);
}

