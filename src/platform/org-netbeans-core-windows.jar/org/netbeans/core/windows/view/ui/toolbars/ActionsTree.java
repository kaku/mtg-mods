/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.view.BeanTreeView
 *  org.openide.explorer.view.NodeRenderer
 *  org.openide.explorer.view.Visualizer
 *  org.openide.nodes.Node
 */
package org.netbeans.core.windows.view.ui.toolbars;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragGestureRecognizer;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceContext;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.dnd.InvalidDnDOperationException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTree;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import org.openide.explorer.view.BeanTreeView;
import org.openide.explorer.view.NodeRenderer;
import org.openide.explorer.view.Visualizer;
import org.openide.nodes.Node;

public class ActionsTree
extends BeanTreeView
implements DragGestureListener,
DragSourceListener {
    private Cursor dragMoveCursor = DragSource.DefaultMoveDrop;
    private Cursor dragNoDropCursor = DragSource.DefaultMoveNoDrop;

    public ActionsTree() {
        this.setRootVisible(false);
        this.tree.setCellRenderer((TreeCellRenderer)new NodeRenderer());
        this.tree.setShowsRootHandles(true);
        this.setDragSource(false);
        this.setDropTarget(false);
        DragSource.getDefaultDragSource().createDefaultDragGestureRecognizer(this.tree, 2, this);
        this.setQuickSearchAllowed(true);
        this.setSelectionMode(1);
    }

    @Override
    public void dragGestureRecognized(DragGestureEvent dge) {
        TreePath path = this.tree.getPathForLocation(dge.getDragOrigin().x, dge.getDragOrigin().y);
        if (null != path) {
            Object obj = path.getLastPathComponent();
            if (this.tree.getModel().isLeaf(obj)) {
                try {
                    Node node = Visualizer.findNode((Object)obj);
                    Transferable t = node.drag();
                    dge.getDragSource().addDragSourceListener(this);
                    dge.startDrag(this.dragNoDropCursor, t);
                }
                catch (InvalidDnDOperationException e) {
                    Logger.getLogger(ActionsTree.class.getName()).log(Level.INFO, e.getMessage(), e);
                }
                catch (IOException e) {
                    Logger.getLogger(ActionsTree.class.getName()).log(Level.WARNING, null, e);
                }
            }
        }
    }

    @Override
    public void dragExit(DragSourceEvent dse) {
        dse.getDragSourceContext().setCursor(this.dragNoDropCursor);
    }

    @Override
    public void dropActionChanged(DragSourceDragEvent dsde) {
    }

    @Override
    public void dragOver(DragSourceDragEvent e) {
        DragSourceContext context = e.getDragSourceContext();
        int action = e.getDropAction();
        if ((action & 2) != 0) {
            context.setCursor(this.dragMoveCursor);
        } else {
            context.setCursor(this.dragNoDropCursor);
        }
    }

    @Override
    public void dragEnter(DragSourceDragEvent dsde) {
        this.dragOver(dsde);
    }

    @Override
    public void dragDropEnd(DragSourceDropEvent dsde) {
    }
}

