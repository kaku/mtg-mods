/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.WeakSet
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Cloneable
 */
package org.netbeans.core.windows.view.dnd;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceMotionListener;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetListener;
import java.awt.event.AWTEventListener;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.RootPaneContainer;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.Constants;
import org.netbeans.core.windows.Debug;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.view.Controller;
import org.netbeans.core.windows.view.EditorView;
import org.netbeans.core.windows.view.ModeView;
import org.netbeans.core.windows.view.ViewElement;
import org.netbeans.core.windows.view.dnd.DropTargetGlassPane;
import org.netbeans.core.windows.view.dnd.EnhancedDragPainter;
import org.netbeans.core.windows.view.dnd.KeyboardDnd;
import org.netbeans.core.windows.view.dnd.TopComponentDragSupport;
import org.netbeans.core.windows.view.dnd.TopComponentDraggable;
import org.netbeans.core.windows.view.dnd.TopComponentDroppable;
import org.netbeans.core.windows.view.dnd.ZOrderManager;
import org.netbeans.core.windows.view.ui.MainWindow;
import org.netbeans.core.windows.view.ui.ModeComponent;
import org.netbeans.core.windows.view.ui.MultiSplitPane;
import org.openide.util.Lookup;
import org.openide.util.WeakSet;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public final class WindowDnDManager
implements DropTargetGlassPane.Observer,
DropTargetGlassPane.Informer {
    private final TopComponentDragSupport topComponentDragSupport;
    private DragSource windowDragSource;
    private boolean dragging;
    private boolean dropSuccess;
    private final Map<JRootPane, Component> root2glass;
    private final Set<Component> floatingFrames;
    private Reference<DropTargetGlassPane> lastTargetWRef;
    private final ViewAccessor viewAccessor;
    private TopComponentDroppable startingDroppable;
    private Point startingPoint;
    private TopComponentDraggable startingTransfer;
    private MotionListener motionListener;
    private static Reference<CenterPanelDroppable> centerDropWRef = new WeakReference<Object>(null);
    private static Reference<EditorAreaDroppable> editorDropWRef = new WeakReference<Object>(null);
    private static final boolean DEBUG = Debug.isLoggable(WindowDnDManager.class);
    private CenterSlidingDroppable lastSlideDroppable;

    public WindowDnDManager(ViewAccessor viewAccessor) {
        this.topComponentDragSupport = new TopComponentDragSupport(this);
        this.root2glass = new HashMap<JRootPane, Component>();
        this.floatingFrames = new WeakSet(4);
        this.lastTargetWRef = new WeakReference<Object>(null);
        this.viewAccessor = viewAccessor;
        Toolkit.getDefaultToolkit().addAWTEventListener(this.topComponentDragSupport, 48);
    }

    public static boolean isDnDEnabled() {
        return !Constants.SWITCH_DND_DISABLE && (Switches.isTopComponentDragAndDropEnabled() || Switches.isEditorModeDragAndDropEnabled() || Switches.isViewModeDragAndDropEnabled());
    }

    public synchronized DragSource getWindowDragSource() {
        if (this.windowDragSource == null) {
            this.windowDragSource = new DragSource();
            this.windowDragSource.addDragSourceMotionListener(this.getMotionListener());
        }
        return this.windowDragSource;
    }

    MotionListener getMotionListener() {
        if (this.motionListener == null) {
            this.motionListener = new MotionListener(this, this.topComponentDragSupport);
        }
        return this.motionListener;
    }

    public boolean isDragging() {
        return this.dragging;
    }

    @Override
    public void setDropSuccess(boolean dropSuccess) {
        this.dropSuccess = dropSuccess;
    }

    public boolean isDropSuccess() {
        return this.dropSuccess;
    }

    @Override
    public void setLastDropTarget(DropTargetGlassPane target) {
        if (target != this.lastTargetWRef.get()) {
            this.lastTargetWRef = new WeakReference<DropTargetGlassPane>(target);
        }
    }

    public void resetDragSource() {
        this.dragFinished();
    }

    public TopComponentDroppable getStartingDroppable() {
        return this.startingDroppable;
    }

    public Point getStartingPoint() {
        return this.startingPoint;
    }

    public TopComponentDraggable getStartingTransfer() {
        return this.startingTransfer;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void dragStarting(TopComponentDroppable startingDroppable, Point startingPoint, TopComponentDraggable startingTransfer) {
        Set<Component> i$;
        if (DEBUG) {
            WindowDnDManager.debugLog("");
            WindowDnDManager.debugLog("dragStarting");
        }
        this.startingDroppable = startingDroppable;
        this.startingPoint = startingPoint;
        this.startingTransfer = startingTransfer;
        HashMap<JRootPane, Component> addedRoots = new HashMap<JRootPane, Component>();
        HashSet<Component> addedFrames = new HashSet<Component>();
        for (Component comp : this.viewAccessor.getModeComponents()) {
            Component originalGlass;
            if (!(comp instanceof TopComponentDroppable)) continue;
            JRootPane root = null;
            if (comp instanceof RootPaneContainer) {
                root = ((RootPaneContainer)((Object)comp)).getRootPane();
            } else {
                RootPaneContainer rootContainer = (RootPaneContainer)((Object)SwingUtilities.getAncestorOfClass(RootPaneContainer.class, comp));
                if (rootContainer != null) {
                    root = rootContainer.getRootPane();
                }
            }
            if (root == null || (originalGlass = WindowDnDManager.setDropTargetGlassPane(root, this)) == null) continue;
            addedRoots.put(root, originalGlass);
        }
        for (Component w : this.viewAccessor.getSeparateModeFrames()) {
            if (w == null) continue;
            addedFrames.add(w);
        }
        if (!addedRoots.isEmpty()) {
            i$ = this.root2glass;
            synchronized (i$) {
                this.root2glass.putAll(addedRoots);
            }
        }
        if (!addedFrames.isEmpty()) {
            i$ = this.floatingFrames;
            synchronized (i$) {
                this.floatingFrames.addAll(addedFrames);
            }
        }
        this.dragging = true;
        this.dropSuccess = false;
    }

    private static Component setDropTargetGlassPane(JRootPane rootPane, WindowDnDManager windowDnDManager) {
        Component glassPane = rootPane.getGlassPane();
        if (glassPane instanceof DropTargetGlassPane) {
            return null;
        }
        DropTargetGlassPane dropGlass = new DropTargetGlassPane(windowDnDManager);
        new DropTarget(dropGlass, 3, dropGlass);
        rootPane.setGlassPane(dropGlass);
        dropGlass.initialize();
        return glassPane;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void dragFinished() {
        HashMap<JRootPane, Component> removedRoots;
        if (DEBUG) {
            WindowDnDManager.debugLog("");
            WindowDnDManager.debugLog("dragFinished");
        }
        this.getMotionListener().dragFinished();
        this.startingDroppable = null;
        this.startingPoint = null;
        this.startingTransfer = null;
        this.topComponentDragSupport.dragFinished();
        this.dragging = false;
        Map<JRootPane, Component> map = this.root2glass;
        synchronized (map) {
            removedRoots = new HashMap<JRootPane, Component>(this.root2glass);
            this.root2glass.clear();
        }
        for (JRootPane root : removedRoots.keySet()) {
            WindowDnDManager.setOriginalGlassPane(root, removedRoots.get(root));
        }
    }

    private static void setOriginalGlassPane(JRootPane rootPane, Component originalGlass) {
        JInternalFrame internalFrame;
        Component glass = rootPane.getGlassPane();
        if (glass instanceof DropTargetGlassPane) {
            DropTargetGlassPane dropGlass = (DropTargetGlassPane)glass;
            dropGlass.setDropTarget(null);
            dropGlass.uninitialize();
        }
        if (originalGlass != null) {
            rootPane.setGlassPane(originalGlass);
        }
        if ((internalFrame = (JInternalFrame)SwingUtilities.getAncestorOfClass(JInternalFrame.class, originalGlass)) != null && !internalFrame.isSelected() && !originalGlass.isVisible()) {
            originalGlass.setVisible(true);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void dragFinishedEx() {
        Set<Component> set = this.floatingFrames;
        synchronized (set) {
            this.floatingFrames.clear();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Set<Component> getFloatingFrames() {
        Set<Component> set = this.floatingFrames;
        synchronized (set) {
            return new HashSet<Component>(this.floatingFrames);
        }
    }

    public boolean isInFloatingFrame(Point location) {
        for (Component w : this.getFloatingFrames()) {
            if (!w.getBounds().contains(location)) continue;
            return true;
        }
        return false;
    }

    @Override
    public boolean isCopyOperationPossible() {
        return this.topComponentDragSupport.isCopyOperationPossible();
    }

    @Override
    public Controller getController() {
        return this.viewAccessor.getController();
    }

    private static void debugLog(String message) {
        Debug.log(WindowDnDManager.class, message);
    }

    static boolean isInMainWindow(Point location) {
        return WindowManagerImpl.getInstance().getMainWindow().getBounds().contains(location);
    }

    private boolean isInMainWindowDroppable(Point location, TopComponentDraggable transfer) {
        return this.findMainWindowDroppable(location, transfer) != null;
    }

    private boolean isInFloatingFrameDroppable(Set<Component> floatingFrames, Point location, TopComponentDraggable transfer) {
        return this.findFloatingFrameDroppable(floatingFrames, location, transfer) != null;
    }

    private static boolean isInFreeArea(Point location, Window exclude) {
        Frame mainWindow = WindowManagerImpl.getInstance().getMainWindow();
        Window[] owned = mainWindow.getOwnedWindows();
        Frame[] frames = Frame.getFrames();
        Window[] windows = new Window[owned.length + frames.length];
        System.arraycopy(frames, 0, windows, 0, frames.length);
        System.arraycopy(owned, 0, windows, frames.length, owned.length);
        for (int i = 0; i < windows.length; ++i) {
            if (windows[i] == exclude || !windows[i].isVisible() || !windows[i].getBounds().contains(location.x, location.y)) continue;
            return false;
        }
        return true;
    }

    private TopComponentDroppable findDroppableFromScreen(Set<Component> floatingFrames, Point location, TopComponentDraggable transfer) {
        TopComponentDroppable droppable = this.findMainWindowDroppable(location, transfer);
        if (droppable != null) {
            return droppable;
        }
        if (transfer.isUndockingEnabled()) {
            droppable = this.findFloatingFrameDroppable(floatingFrames, location, transfer);
            if (droppable != null) {
                return droppable;
            }
            if (WindowDnDManager.isInFreeArea(location, this.motionListener.fakeWindow)) {
                return WindowDnDManager.getFreeAreaDroppable(location);
            }
        }
        return null;
    }

    private TopComponentDroppable findMainWindowDroppable(Point location, TopComponentDraggable transfer) {
        JFrame mainWindow = (JFrame)WindowManagerImpl.getInstance().getMainWindow();
        if (!ZOrderManager.getInstance().isOnTop(mainWindow, location)) {
            return null;
        }
        Point p = new Point(location);
        SwingUtilities.convertPointFromScreen(p, mainWindow.getContentPane());
        if (transfer.isSlidingEnabled()) {
            CenterSlidingDroppable drop;
            if (this.lastSlideDroppable != null && this.lastSlideDroppable.isWithinSlide(p)) {
                return this.lastSlideDroppable;
            }
            TopComponentDroppable droppable = WindowDnDManager.findSlideDroppable(this.viewAccessor.getSlidingModeComponent("left"));
            if (droppable != null && (drop = new CenterSlidingDroppable(this.viewAccessor, droppable, "left")).isWithinSlide(p)) {
                if (!drop.supportsKind(transfer)) {
                    this.lastSlideDroppable = null;
                    return null;
                }
                this.lastSlideDroppable = drop;
                return drop;
            }
            droppable = WindowDnDManager.findSlideDroppable(this.viewAccessor.getSlidingModeComponent("right"));
            if (droppable != null && (drop = new CenterSlidingDroppable(this.viewAccessor, droppable, "right")).isWithinSlide(p)) {
                if (!drop.supportsKind(transfer)) {
                    this.lastSlideDroppable = null;
                    return null;
                }
                this.lastSlideDroppable = drop;
                return drop;
            }
            droppable = WindowDnDManager.findSlideDroppable(this.viewAccessor.getSlidingModeComponent("bottom"));
            if (droppable != null && (drop = new CenterSlidingDroppable(this.viewAccessor, droppable, "bottom")).isWithinSlide(p)) {
                if (!drop.supportsKind(transfer)) {
                    this.lastSlideDroppable = null;
                    return null;
                }
                this.lastSlideDroppable = drop;
                return drop;
            }
            droppable = WindowDnDManager.findSlideDroppable(this.viewAccessor.getSlidingModeComponent("top"));
            if (droppable != null && (drop = new CenterSlidingDroppable(this.viewAccessor, droppable, "top")).isWithinSlide(p)) {
                if (!drop.supportsKind(transfer)) {
                    this.lastSlideDroppable = null;
                    return null;
                }
                this.lastSlideDroppable = drop;
                return drop;
            }
        }
        this.lastSlideDroppable = null;
        if (WindowDnDManager.isNearEditorEdge(location, this.viewAccessor, transfer.getKind())) {
            return this.getEditorAreaDroppable();
        }
        if (WindowDnDManager.isNearEdge(location, this.viewAccessor)) {
            return this.getCenterPanelDroppable();
        }
        Point mainP = new Point(location);
        SwingUtilities.convertPointFromScreen(mainP, mainWindow);
        return this.findDroppable(mainWindow, mainP, transfer);
    }

    private static TopComponentDroppable findSlideDroppable(Component comp) {
        if (!Switches.isDragAndDropSlidingEnabled()) {
            return null;
        }
        TopComponentDroppable droppable = null;
        droppable = comp instanceof TopComponentDroppable ? (TopComponentDroppable)((Object)comp) : (TopComponentDroppable)((Object)SwingUtilities.getAncestorOfClass(TopComponentDroppable.class, comp));
        return droppable;
    }

    private TopComponentDroppable findFloatingFrameDroppable(Set<Component> floatingFrames, Point location, TopComponentDraggable transfer) {
        for (Component comp : floatingFrames) {
            TopComponentDroppable droppable;
            Rectangle bounds = comp.getBounds();
            if (!bounds.contains(location) || !ZOrderManager.getInstance().isOnTop((RootPaneContainer)((Object)comp), location) || (droppable = this.findDroppable(comp, new Point(location.x - bounds.x, location.y - bounds.y), transfer)) == null) continue;
            return droppable;
        }
        return null;
    }

    private TopComponentDroppable findDroppable(Component comp, Point location, TopComponentDraggable transfer) {
        RootPaneContainer rpc;
        TopComponentDroppable droppable;
        TopComponentDroppable editor;
        if (comp instanceof RootPaneContainer) {
            rpc = (RootPaneContainer)((Object)comp);
        } else {
            Window w = SwingUtilities.getWindowAncestor(comp);
            if (w instanceof RootPaneContainer) {
                rpc = (RootPaneContainer)((Object)w);
            } else {
                return null;
            }
        }
        Point screenLocation = new Point(location);
        SwingUtilities.convertPointToScreen(screenLocation, comp);
        Container contentPane = rpc.getContentPane();
        location = SwingUtilities.convertPoint(comp, location, contentPane);
        Component deepest = SwingUtilities.getDeepestComponentAt(contentPane, location.x, location.y);
        if (deepest instanceof MultiSplitPane) {
            MultiSplitPane splitPane = (MultiSplitPane)deepest;
            int dx = 0;
            int dy = 0;
            if (splitPane.isHorizontalSplit()) {
                dx = splitPane.getDividerSize() + 1;
            } else {
                dy = splitPane.getDividerSize() + 1;
            }
            Point pt = SwingUtilities.convertPoint(contentPane, location, deepest);
            deepest = SwingUtilities.getDeepestComponentAt(deepest, pt.x + dx, pt.y + dy);
        }
        if (deepest instanceof TopComponentDroppable && (droppable = (TopComponentDroppable)((Object)deepest)).supportsKind(transfer)) {
            return droppable;
        }
        TopComponentDroppable res = null;
        while (deepest != null) {
            TopComponentDroppable nextDroppable = (TopComponentDroppable)((Object)SwingUtilities.getAncestorOfClass(TopComponentDroppable.class, deepest));
            if (nextDroppable != null && nextDroppable.supportsKind(transfer)) {
                res = nextDroppable;
                break;
            }
            deepest = (Component)((Object)nextDroppable);
        }
        if (res instanceof ModeComponent && transfer.getKind() != 1 && ((ModeComponent)((Object)res)).getKind() == 1 && (editor = this.getEditorAreaDroppable()).supportsKind(transfer)) {
            Point p = new Point(screenLocation);
            SwingUtilities.convertPointFromScreen(p, res.getDropComponent());
            Object side = res.getConstraintForLocation(p);
            p = new Point(screenLocation);
            SwingUtilities.convertPointFromScreen(p, editor.getDropComponent());
            if (null != side && side.equals(editor.getConstraintForLocation(p))) {
                res = editor;
            }
        }
        return res;
    }

    static boolean isAroundCenterPanel(Point location) {
        Component desktop = MainWindow.getInstance().getDesktop();
        if (desktop == null) {
            return false;
        }
        Point p = new Point(location);
        SwingUtilities.convertPointFromScreen(p, desktop.getParent());
        Rectangle centerBounds = desktop.getBounds();
        if (!centerBounds.contains(p)) {
            centerBounds.grow(20, 20);
            if (centerBounds.contains(p)) {
                return true;
            }
        }
        return false;
    }

    static boolean isMixedTCDragDrop(TopComponentDraggable transfer, TopComponentDroppable droppable) {
        if (transfer != null && droppable != null && (droppable.getKind() == 1 && transfer.getKind() != 1 || droppable.getKind() != 1 && transfer.getKind() == 1)) {
            return true;
        }
        return false;
    }

    static boolean isNearEditorEdge(Point location, ViewAccessor viewAccessor, int kind) {
        Component editor = WindowManagerImpl.getInstance().getEditorAreaComponent();
        if (editor == null || editor.getParent() == null) {
            return false;
        }
        Point p = new Point(location);
        SwingUtilities.convertPointFromScreen(p, editor.getParent());
        Rectangle editorBounds = editor.getBounds();
        editorBounds.y -= 10;
        editorBounds.height += 10;
        Rectangle shrinked = editor.getBounds();
        shrinked.grow(-10, 0);
        shrinked.height -= 10;
        Component dr = viewAccessor.getSlidingModeComponent("right");
        if (dr != null) {
            shrinked.width -= dr.getBounds().width;
        }
        if ((dr = viewAccessor.getSlidingModeComponent("bottom")) != null) {
            shrinked.height -= dr.getBounds().height;
        }
        return editorBounds.contains(p) && !shrinked.contains(p) && kind == 1;
    }

    static boolean isNearEdge(Point location, ViewAccessor viewAccessor) {
        Component desktop = MainWindow.getInstance().getDesktop();
        if (desktop == null) {
            return false;
        }
        Point p = new Point(location);
        SwingUtilities.convertPointFromScreen(p, desktop);
        Rectangle centerBounds = desktop.getBounds();
        centerBounds.y -= 20;
        centerBounds.height += 20;
        Rectangle shrinked = desktop.getBounds();
        shrinked.grow(-10, 0);
        shrinked.height -= 10;
        Component dr = viewAccessor.getSlidingModeComponent("left");
        if (dr != null) {
            shrinked.x += dr.getBounds().width;
            shrinked.width -= dr.getBounds().width;
        }
        if ((dr = viewAccessor.getSlidingModeComponent("right")) != null) {
            shrinked.width -= dr.getBounds().width;
        }
        if ((dr = viewAccessor.getSlidingModeComponent("bottom")) != null) {
            shrinked.height -= dr.getBounds().height;
        }
        if ((dr = viewAccessor.getSlidingModeComponent("top")) != null) {
            shrinked.y += dr.getBounds().height;
        }
        boolean cont = centerBounds.contains(p) && !shrinked.contains(p);
        return cont;
    }

    TopComponentDroppable getCenterPanelDroppable() {
        CenterPanelDroppable droppable = centerDropWRef.get();
        if (droppable == null) {
            droppable = new CenterPanelDroppable();
            centerDropWRef = new WeakReference<CenterPanelDroppable>(droppable);
        }
        return droppable;
    }

    private static TopComponentDroppable getFreeAreaDroppable(Point location) {
        return new FreeAreaDroppable(location);
    }

    private TopComponentDroppable getEditorAreaDroppable() {
        EditorAreaDroppable droppable = editorDropWRef.get();
        if (droppable == null) {
            droppable = new EditorAreaDroppable();
            editorDropWRef = new WeakReference<EditorAreaDroppable>(droppable);
        }
        return droppable;
    }

    boolean tryPerformDrop(Controller controller, Set<Component> floatingFrames, Point location, int dropAction, Transferable transferable) {
        TopComponentDraggable draggable = WindowDnDManager.extractTopComponentDraggable(dropAction == 1, transferable);
        if (draggable == null) {
            return false;
        }
        TopComponentDroppable droppable = this.findDroppableFromScreen(floatingFrames, location, draggable);
        if (droppable == null) {
            return false;
        }
        Component dropComponent = droppable.getDropComponent();
        if (dropComponent != null) {
            SwingUtilities.convertPointFromScreen(location, dropComponent);
        }
        return WindowDnDManager.performDrop(controller, droppable, draggable, location);
    }

    static TopComponentDraggable extractTopComponentDraggable(boolean clone, Transferable tr) {
        DataFlavor df = WindowDnDManager.getDataFlavorForDropAction(clone);
        if (df == null) {
            return null;
        }
        if (tr.isDataFlavorSupported(df)) {
            try {
                ModeImpl mode;
                TopComponent tc;
                if (clone) {
                    TopComponent.Cloneable ctc = (TopComponent.Cloneable)tr.getTransferData(df);
                    tc = ctc.cloneComponent();
                } else {
                    tc = (TopComponent)tr.getTransferData(df);
                }
                if (null != tc && null != (mode = (ModeImpl)WindowManagerImpl.getInstance().findMode((TopComponent)tr.getTransferData(df)))) {
                    return new TopComponentDraggable(tc, mode);
                }
            }
            catch (UnsupportedFlavorException ufe) {
                Logger.getLogger(WindowDnDManager.class.getName()).log(Level.WARNING, null, ufe);
            }
            catch (IOException ioe) {
                Logger.getLogger(WindowDnDManager.class.getName()).log(Level.WARNING, null, ioe);
            }
        }
        if (tr.isDataFlavorSupported(df = new DataFlavor("application/x-java-jvm-local-objectref; class=org.netbeans.core.windows.ModeImpl", null))) {
            try {
                ModeImpl mode = (ModeImpl)tr.getTransferData(df);
                if (null != mode) {
                    return new TopComponentDraggable(mode);
                }
            }
            catch (UnsupportedFlavorException ufe) {
                Logger.getLogger(WindowDnDManager.class.getName()).log(Level.WARNING, null, ufe);
            }
            catch (IOException ioe) {
                Logger.getLogger(WindowDnDManager.class.getName()).log(Level.WARNING, null, ioe);
            }
        }
        return null;
    }

    private static DataFlavor getDataFlavorForDropAction(boolean clone) {
        DataFlavor df = null;
        ClassLoader cl = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        try {
            df = clone ? new DataFlavor("application/x-java-jvm-local-objectref; class=org.openide.windows.TopComponent$Cloneable", null, cl) : new DataFlavor("application/x-java-jvm-local-objectref; class=org.openide.windows.TopComponent", null, cl);
        }
        catch (ClassNotFoundException cnfE) {
            Logger.getLogger(WindowDnDManager.class.getName()).log(Level.INFO, null, cnfE);
        }
        return df;
    }

    static boolean performDrop(Controller controller, TopComponentDroppable droppable, TopComponentDraggable draggable, Point location) {
        if (DEBUG) {
            WindowDnDManager.debugLog("");
            WindowDnDManager.debugLog("performDrop");
            WindowDnDManager.debugLog("droppable=" + droppable);
        }
        if (draggable == null) {
            return true;
        }
        if (!droppable.canDrop(draggable, location)) {
            return true;
        }
        ViewElement viewElement = droppable.getDropViewElement();
        Object constr = droppable.getConstraintForLocation(location);
        if (viewElement instanceof EditorView) {
            int kind = draggable.getKind();
            if (kind == 1) {
                controller.userDroppedTopComponentsIntoEmptyEditor(draggable);
            } else if (constr == "top" || constr == "left" || constr == "right" || constr == "bottom") {
                controller.userDroppedTopComponentsAroundEditor(draggable, (String)constr);
            } else if (draggable.isAllowedToMoveAnywhere()) {
                controller.userDroppedTopComponentsIntoEmptyEditor(draggable);
            }
        } else if (viewElement instanceof ModeView) {
            ModeView modeView = (ModeView)viewElement;
            if (constr == "top" || constr == "left" || constr == "right" || constr == "bottom") {
                controller.userDroppedTopComponents(modeView, draggable, (String)constr);
            } else if (constr instanceof Integer) {
                controller.userDroppedTopComponents(modeView, draggable, (Integer)constr);
            } else {
                controller.userDroppedTopComponents(modeView, draggable);
            }
        } else if (viewElement == null) {
            if (constr == "top" || constr == "left" || constr == "right" || constr == "bottom") {
                if (droppable instanceof EditorAreaDroppable) {
                    controller.userDroppedTopComponentsAroundEditor(draggable, (String)constr);
                } else {
                    controller.userDroppedTopComponentsAround(draggable, (String)constr);
                }
            } else if (constr instanceof Rectangle) {
                Rectangle bounds = (Rectangle)constr;
                Rectangle modeBounds = draggable.getBounds();
                if (null != modeBounds) {
                    bounds.setSize(modeBounds.width, modeBounds.height);
                }
                controller.userDroppedTopComponentsIntoFreeArea(draggable, bounds);
            }
        }
        return true;
    }

    public void startKeyboardDragAndDrop(TopComponentDraggable draggable) {
        KeyboardDnd.start(this, draggable, this.viewAccessor);
    }

    private static class CenterSlidingDroppable
    implements TopComponentDroppable,
    EnhancedDragPainter {
        private ViewAccessor accesor;
        private TopComponentDroppable original;
        private String side;
        JPanel pan;
        private boolean isShowing;

        public CenterSlidingDroppable(ViewAccessor viewAccesor, TopComponentDroppable slidingBarDelegate, String side) {
            this.original = slidingBarDelegate;
            this.accesor = viewAccesor;
            this.side = side;
            this.pan = new JPanel();
            this.isShowing = false;
        }

        @Override
        public boolean canDrop(TopComponentDraggable transfer, Point location) {
            return this.original.canDrop(transfer, location);
        }

        @Override
        public Object getConstraintForLocation(Point location) {
            return this.original.getConstraintForLocation(location);
        }

        @Override
        public Component getDropComponent() {
            return this.original.getDropComponent();
        }

        @Override
        public ViewElement getDropViewElement() {
            return this.original.getDropViewElement();
        }

        @Override
        public Shape getIndicationForLocation(Point location) {
            Shape toReturn = this.original.getIndicationForLocation(location);
            Rectangle dim = this.original.getDropComponent().getBounds();
            if (dim.width < 10 || dim.height < 10) {
                Rectangle rect = toReturn.getBounds();
                if ("left".equals(this.side)) {
                    toReturn = new Rectangle(0, 0, Math.max(rect.width, 20), Math.max(rect.height, 20));
                } else if ("right".equals(this.side)) {
                    toReturn = new Rectangle(-20, 0, Math.max(rect.width, 20), Math.max(rect.height, 20));
                } else if ("bottom".equals(this.side)) {
                    toReturn = new Rectangle(0, -20, Math.max(rect.width, 20), Math.max(rect.height, 20));
                } else if ("top".equals(this.side)) {
                    toReturn = new Rectangle(0, 0, Math.max(rect.width, 20), Math.max(rect.height, 20));
                }
            }
            return toReturn;
        }

        public boolean isWithinSlide(Point location) {
            JRootPane root = SwingUtilities.getRootPane(this.original.getDropComponent());
            if (null == root || null == SwingUtilities.getWindowAncestor(this.original.getDropComponent())) {
                return false;
            }
            Point barLoc = SwingUtilities.convertPoint(root, location, this.original.getDropComponent());
            if (this.original.getDropComponent().contains(barLoc)) {
                return true;
            }
            Dimension dim = this.original.getDropComponent().getSize();
            if ("left".equals(this.side)) {
                int abs = Math.abs(barLoc.x);
                if (barLoc.y > -20 && barLoc.y < dim.height + 20) {
                    if (this.isShowing && abs < 20) {
                        return true;
                    }
                    if (!this.isShowing && barLoc.x <= 0 && barLoc.x > -20) {
                        return true;
                    }
                }
            } else if ("right".equals(this.side)) {
                if (barLoc.y > -20 && barLoc.y < dim.height + 20) {
                    if (this.isShowing && (barLoc.x < 0 && barLoc.x > -20 || barLoc.x > 0 && barLoc.x - dim.width < 20)) {
                        return true;
                    }
                    if (!this.isShowing && barLoc.x >= 0 && barLoc.x < 20 + dim.width) {
                        return true;
                    }
                }
            } else if ("bottom".equals(this.side)) {
                if (barLoc.x > -20 && barLoc.x < dim.width + 20) {
                    if (this.isShowing && (barLoc.y < 0 && barLoc.y > -20 || barLoc.y > 0 && barLoc.y - dim.height < 20)) {
                        return true;
                    }
                    if (!this.isShowing && barLoc.y >= 0 && barLoc.y < 20 + dim.height) {
                        return true;
                    }
                }
            } else if ("top".equals(this.side) && barLoc.x > -20 && barLoc.x < dim.width + 20) {
                if (this.isShowing && (barLoc.y < 0 && barLoc.y > -20 || barLoc.y > 0 && barLoc.y + dim.height < 20)) {
                    return true;
                }
                if (!this.isShowing && barLoc.y >= 0 && barLoc.y < 20 + dim.height) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public boolean supportsKind(TopComponentDraggable transfer) {
            return this.original.supportsKind(transfer);
        }

        @Override
        public void additionalDragPaint(Graphics2D g) {
            Point firstDivider;
            Point secondDevider;
            Rectangle dim = this.original.getDropComponent().getBounds();
            if (dim.width > 10 && dim.height > 10) {
                return;
            }
            this.isShowing = true;
            Component glassPane = ((JComponent)this.original.getDropComponent()).getRootPane().getGlassPane();
            Point leftTop = SwingUtilities.convertPoint(this.original.getDropComponent(), 0, 0, glassPane);
            if ("right".equals(this.side)) {
                leftTop = new Point(leftTop.x - 24, leftTop.y);
                firstDivider = new Point(leftTop);
                secondDevider = new Point(leftTop.x, leftTop.y + dim.height);
            } else if ("bottom".equals(this.side)) {
                leftTop = new Point(0, leftTop.y - 24);
                firstDivider = new Point(leftTop);
                secondDevider = new Point(leftTop.x + glassPane.getBounds().width, leftTop.y);
            } else if ("top".equals(this.side)) {
                firstDivider = new Point(leftTop.x, leftTop.y + 24);
                secondDevider = new Point(leftTop.x + glassPane.getBounds().width, leftTop.y + 24);
            } else {
                firstDivider = new Point(leftTop.x + 25, leftTop.y);
                secondDevider = new Point(leftTop.x + 25, leftTop.y + dim.height);
            }
            Rectangle rect = new Rectangle(leftTop.x, leftTop.y, Math.max(25, dim.width), Math.max(25, dim.height));
            if ("bottom".equals(this.side) || "top".equals(this.side)) {
                rect.width = glassPane.getBounds().width;
            }
            Color col = g.getColor();
            g.setColor(this.pan.getBackground());
            g.fill(rect);
            g.setColor(this.pan.getBackground().darker());
            g.drawLine(firstDivider.x, firstDivider.y, secondDevider.x, secondDevider.y);
            g.setColor(col);
        }

        @Override
        public Rectangle getPaintArea() {
            Rectangle dim = this.original.getDropComponent().getBounds();
            if (dim.width > 10 && dim.height > 10) {
                return null;
            }
            Component glassPane = ((JComponent)this.original.getDropComponent()).getRootPane().getGlassPane();
            Point leftTop = SwingUtilities.convertPoint(this.original.getDropComponent(), 0, 0, glassPane);
            if ("right".equals(this.side)) {
                leftTop = new Point(leftTop.x - 24, leftTop.y);
            } else if ("bottom".equals(this.side)) {
                leftTop = new Point(0, leftTop.y - 24);
            } else if ("top".equals(this.side)) {
                leftTop = new Point(0, leftTop.y + 24);
            }
            Rectangle rect = new Rectangle(leftTop.x, leftTop.y, Math.max(25, dim.width), Math.max(25, dim.height));
            if ("bottom".equals(this.side) || "top".equals(this.side)) {
                rect.width = glassPane.getBounds().width;
            }
            return rect;
        }

        @Override
        public int getKind() {
            return 2;
        }
    }

    private static class FreeAreaDroppable
    implements TopComponentDroppable {
        private Point location;

        public FreeAreaDroppable(Point location) {
            this.location = location;
        }

        @Override
        public Shape getIndicationForLocation(Point p) {
            return null;
        }

        @Override
        public Object getConstraintForLocation(Point p) {
            return new Rectangle(this.location.x, this.location.y, Constants.DROP_NEW_MODE_SIZE.width, Constants.DROP_NEW_MODE_SIZE.height);
        }

        @Override
        public Component getDropComponent() {
            return null;
        }

        @Override
        public ViewElement getDropViewElement() {
            return null;
        }

        @Override
        public boolean canDrop(TopComponentDraggable transfer, Point location) {
            if (transfer.isAllowedToMoveAnywhere()) {
                return true;
            }
            ModeImpl mode = transfer.getMode();
            if (null != mode && mode.getState() == 1 && mode.getOpenedTopComponents().size() == 1) {
                return false;
            }
            return true;
        }

        @Override
        public boolean supportsKind(TopComponentDraggable transfer) {
            return true;
        }

        @Override
        public int getKind() {
            return 0;
        }
    }

    private class EditorAreaDroppable
    implements TopComponentDroppable {
        private static final int DROP_BORDER_WIDTH = 30;

        private EditorAreaDroppable() {
        }

        @Override
        public Shape getIndicationForLocation(Point p) {
            Rectangle bounds = this.getDropComponent().getBounds();
            Rectangle res = null;
            double ratio = 0.25;
            Object constraint = this.getConstraintForLocation(p);
            if (constraint == "left") {
                res = new Rectangle(0, 0, (int)((double)bounds.width * ratio) - 1, bounds.height - 1);
            } else if (constraint == "top") {
                res = new Rectangle(0, 0, bounds.width - 1, (int)((double)bounds.height * ratio) - 1);
            } else if (constraint == "right") {
                res = new Rectangle(bounds.width - (int)((double)bounds.width * ratio), 0, (int)((double)bounds.width * ratio) - 1, bounds.height - 1);
            } else if (constraint == "bottom") {
                res = new Rectangle(0, bounds.height - (int)((double)bounds.height * ratio), bounds.width - 1, (int)((double)bounds.height * ratio) - 1);
            }
            return res;
        }

        @Override
        public Object getConstraintForLocation(Point p) {
            Rectangle bounds = this.getDropComponent().getBounds();
            Component leftSlide = WindowDnDManager.this.viewAccessor.getSlidingModeComponent("left");
            Component rightSlide = WindowDnDManager.this.viewAccessor.getSlidingModeComponent("right");
            Component bottomSlide = WindowDnDManager.this.viewAccessor.getSlidingModeComponent("bottom");
            Component topSlide = WindowDnDManager.this.viewAccessor.getSlidingModeComponent("top");
            if (null != leftSlide && p.x < leftSlide.getBounds().width + 30) {
                return "left";
            }
            if (p.y < bounds.y) {
                return "top";
            }
            if (null != rightSlide && null != leftSlide && p.x > bounds.width - 30 - rightSlide.getBounds().width - leftSlide.getBounds().width) {
                return "right";
            }
            if (null != bottomSlide && p.y > bounds.height - 30 - bottomSlide.getBounds().height) {
                return "bottom";
            }
            if (null != topSlide && p.y < bounds.y + topSlide.getBounds().height + 30) {
                return "top";
            }
            return null;
        }

        @Override
        public Component getDropComponent() {
            return WindowManagerImpl.getInstance().getEditorAreaComponent();
        }

        @Override
        public ViewElement getDropViewElement() {
            return null;
        }

        @Override
        public boolean canDrop(TopComponentDraggable transfer, Point location) {
            if (transfer.isAllowedToMoveAnywhere()) {
                return true;
            }
            return transfer.getKind() == 1;
        }

        @Override
        public boolean supportsKind(TopComponentDraggable transfer) {
            if (transfer.isAllowedToMoveAnywhere()) {
                return true;
            }
            return transfer.getKind() == 1;
        }

        @Override
        public int getKind() {
            if (null == WindowDnDManager.this.getStartingDroppable()) {
                return 1;
            }
            return WindowDnDManager.this.getStartingDroppable().getKind();
        }
    }

    private class CenterPanelDroppable
    implements TopComponentDroppable {
        private CenterPanelDroppable() {
        }

        @Override
        public Shape getIndicationForLocation(Point p) {
            Rectangle bounds = this.getDropComponent().getBounds();
            Rectangle res = null;
            double ratio = 0.25;
            Object constraint = this.getConstraintForLocation(p);
            if (constraint == "left") {
                res = new Rectangle(0, 0, (int)((double)bounds.width * ratio) - 1, bounds.height - 1);
            } else if (constraint == "top") {
                res = new Rectangle(0, 0, bounds.width - 1, (int)((double)bounds.height * ratio) - 1);
            } else if (constraint == "right") {
                res = new Rectangle(bounds.width - (int)((double)bounds.width * ratio), 0, (int)((double)bounds.width * ratio) - 1, bounds.height - 1);
            } else if (constraint == "bottom") {
                res = new Rectangle(0, bounds.height - (int)((double)bounds.height * ratio), bounds.width - 1, (int)((double)bounds.height * ratio) - 1);
            }
            return res;
        }

        @Override
        public Object getConstraintForLocation(Point p) {
            Rectangle bounds = this.getDropComponent().getBounds();
            Component leftSlide = WindowDnDManager.this.viewAccessor.getSlidingModeComponent("left");
            Component rightSlide = WindowDnDManager.this.viewAccessor.getSlidingModeComponent("right");
            Component bottomSlide = WindowDnDManager.this.viewAccessor.getSlidingModeComponent("bottom");
            Component topSlide = WindowDnDManager.this.viewAccessor.getSlidingModeComponent("top");
            if (null != leftSlide && p.x < leftSlide.getBounds().width + 10) {
                return "left";
            }
            if (p.y < bounds.y) {
                return "top";
            }
            if (null != rightSlide && null != leftSlide && p.x > bounds.width - 10 - rightSlide.getBounds().width - leftSlide.getBounds().width) {
                return "right";
            }
            if (null != bottomSlide && p.y > bounds.height - 10 - bottomSlide.getBounds().height) {
                return "bottom";
            }
            if (null != topSlide && p.y < bounds.y + topSlide.getBounds().height + 10) {
                return "top";
            }
            return null;
        }

        @Override
        public Component getDropComponent() {
            return MainWindow.getInstance().getDesktop();
        }

        @Override
        public ViewElement getDropViewElement() {
            return null;
        }

        @Override
        public boolean canDrop(TopComponentDraggable transfer, Point location) {
            if (transfer.isAllowedToMoveAnywhere()) {
                return true;
            }
            return transfer.getKind() == 0 || transfer.getKind() == 2;
        }

        @Override
        public boolean supportsKind(TopComponentDraggable transfer) {
            if (transfer.isAllowedToMoveAnywhere()) {
                return true;
            }
            return transfer.getKind() == 0 || transfer.getKind() == 2;
        }

        @Override
        public int getKind() {
            return 0;
        }
    }

    public static interface ViewAccessor {
        public Set<Component> getModeComponents();

        public Set<Component> getSeparateModeFrames();

        public Controller getController();

        public Component getSlidingModeComponent(String var1);
    }

    private static class MotionListener
    implements DragSourceMotionListener {
        private final WindowDnDManager windowDnDManager;
        private final TopComponentDragSupport topComponentDragSupport;
        private Point previousDragLoc;
        private Window fakeWindow;
        private boolean isSizeSet;

        private MotionListener(WindowDnDManager windowDnDManager, TopComponentDragSupport topComponentDragSupport) {
            this.windowDnDManager = windowDnDManager;
            this.topComponentDragSupport = topComponentDragSupport;
        }

        @Override
        public void dragMouseMoved(DragSourceDragEvent evt) {
            Point location;
            if (DEBUG) {
                WindowDnDManager.debugLog("dragMouseMoved evt=" + evt);
            }
            if ((location = evt.getLocation()) == null) {
                return;
            }
            if (this.windowDnDManager.startingTransfer == null) {
                return;
            }
            boolean isInMainDroppable = this.windowDnDManager.isInMainWindowDroppable(location, this.windowDnDManager.startingTransfer);
            boolean isInFrameDroppable = this.windowDnDManager.isInFloatingFrameDroppable(this.windowDnDManager.getFloatingFrames(), location, this.windowDnDManager.startingTransfer) && this.windowDnDManager.startingTransfer.isUndockingEnabled();
            boolean isAroundCenterPanel = WindowDnDManager.isAroundCenterPanel(location);
            boolean isMixedTCDragDrop = WindowDnDManager.isMixedTCDragDrop(this.windowDnDManager.startingTransfer, this.windowDnDManager.findDroppableFromScreen(this.windowDnDManager.getFloatingFrames(), location, this.windowDnDManager.startingTransfer));
            if (isInMainDroppable || isInFrameDroppable || isAroundCenterPanel) {
                TopComponentDroppable droppable = this.windowDnDManager.findDroppableFromScreen(this.windowDnDManager.getFloatingFrames(), location, this.windowDnDManager.startingTransfer);
                if (droppable instanceof FreeAreaDroppable) {
                    if (WindowManagerImpl.getInstance().getEditorAreaState() == 1 && droppable.canDrop(this.windowDnDManager.startingTransfer, location)) {
                        this.topComponentDragSupport.setSuccessCursor(true, isMixedTCDragDrop);
                    } else {
                        this.topComponentDragSupport.setUnsuccessCursor(isMixedTCDragDrop);
                    }
                } else if (droppable != null) {
                    JComponent cp = (JComponent)droppable.getDropComponent();
                    Component glass = cp.getRootPane().getGlassPane();
                    if (glass instanceof DropTargetGlassPane) {
                        this.windowDnDManager.setLastDropTarget((DropTargetGlassPane)glass);
                    }
                    Point p = new Point(location);
                    SwingUtilities.convertPointFromScreen(p, droppable.getDropComponent());
                    if (droppable.canDrop(this.windowDnDManager.startingTransfer, p)) {
                        this.topComponentDragSupport.setSuccessCursor(false, isMixedTCDragDrop);
                    } else {
                        this.topComponentDragSupport.setUnsuccessCursor(isMixedTCDragDrop);
                    }
                    this.dragOverDropTarget(location, droppable);
                }
            } else if (!WindowDnDManager.isInMainWindow(location) && this.windowDnDManager.isInFloatingFrame(location)) {
                this.topComponentDragSupport.setSuccessCursor(false, isMixedTCDragDrop);
            } else if (WindowDnDManager.isInFreeArea(location, this.fakeWindow) && WindowDnDManager.getFreeAreaDroppable(location).canDrop(this.windowDnDManager.startingTransfer, location) && this.windowDnDManager.startingTransfer.isUndockingEnabled()) {
                this.topComponentDragSupport.setSuccessCursor(true, isMixedTCDragDrop);
            } else {
                this.topComponentDragSupport.setUnsuccessCursor(isMixedTCDragDrop);
            }
            if (!(isInMainDroppable || isInFrameDroppable || isAroundCenterPanel)) {
                this.clearExitedDropTarget();
            }
        }

        private void dragOverDropTarget(Point location, TopComponentDroppable droppable) {
            DropTargetGlassPane lastTarget = (DropTargetGlassPane)this.windowDnDManager.lastTargetWRef.get();
            if (lastTarget != null) {
                Point p = new Point(location);
                SwingUtilities.convertPointFromScreen(p, lastTarget);
                lastTarget.dragOver(p, droppable);
            }
        }

        private void clearExitedDropTarget() {
            DropTargetGlassPane lastTarget = (DropTargetGlassPane)this.windowDnDManager.lastTargetWRef.get();
            if (lastTarget != null) {
                lastTarget.clearIndications();
                this.windowDnDManager.lastTargetWRef = new WeakReference<Object>(null);
            }
        }

        void dragFinished() {
            this.previousDragLoc = null;
            if (this.fakeWindow != null) {
                this.fakeWindow.dispose();
                this.fakeWindow = null;
            }
        }
    }

}

