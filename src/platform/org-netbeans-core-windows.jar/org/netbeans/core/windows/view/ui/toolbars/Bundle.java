/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.windows.view.ui.toolbars;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String MSG_ToolbarsInitializing() {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_ToolbarsInitializing");
    }

    private void Bundle() {
    }
}

