/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.SlidingButton
 *  org.netbeans.swing.tabcontrol.customtabs.Tabbed
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view.ui.slides;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Window;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.view.ModeView;
import org.netbeans.core.windows.view.SlidingView;
import org.netbeans.core.windows.view.ViewElement;
import org.netbeans.core.windows.view.dnd.TopComponentDraggable;
import org.netbeans.core.windows.view.dnd.TopComponentDroppable;
import org.netbeans.core.windows.view.dnd.WindowDnDManager;
import org.netbeans.core.windows.view.ui.AbstractModeContainer;
import org.netbeans.core.windows.view.ui.ModeComponent;
import org.netbeans.core.windows.view.ui.TabbedHandler;
import org.netbeans.core.windows.view.ui.slides.SlideBar;
import org.netbeans.core.windows.view.ui.slides.TabbedSlideAdapter;
import org.netbeans.swing.tabcontrol.SlidingButton;
import org.netbeans.swing.tabcontrol.customtabs.Tabbed;
import org.openide.windows.TopComponent;

public final class SlideBarContainer
extends AbstractModeContainer {
    VisualPanel panel;
    private static final boolean isAqua = "Aqua".equals(UIManager.getLookAndFeel().getID());
    private static Border bottomBorder;
    private static Border bottomEmptyBorder;
    private static Border leftEmptyBorder;
    private static Border leftBorder;
    private static Border rightEmptyBorder;
    private static Border rightBorder;
    private static Border topEmptyBorder;
    private static Border topBorder;

    public SlideBarContainer(ModeView modeView, WindowDnDManager windowDnDManager) {
        super(modeView, windowDnDManager, 2);
        this.panel = new VisualPanel(this);
        Component slideBar = this.tabbedHandler.getComponent();
        boolean horizontal = true;
        if (slideBar instanceof SlideBar) {
            horizontal = ((SlideBar)slideBar).isHorizontal();
        }
        boolean giveExtraPadding = false;
        if (this.hasMinimizedWindow(slideBar)) {
            giveExtraPadding = true;
        }
        this.panel.setBorder(SlideBarContainer.computeBorder(this.getSlidingView().getSide(), giveExtraPadding));
        this.panel.add(slideBar, horizontal ? "West" : "North");
    }

    private boolean hasMinimizedWindow(Component slideBar) {
        boolean b = false;
        if (slideBar instanceof SlideBar) {
            SlideBar bar = (SlideBar)slideBar;
            for (Component comp : bar.getComponents()) {
                if (!(comp instanceof SlidingButton)) continue;
                return true;
            }
        }
        return b;
    }

    public SlidingView getSlidingView() {
        return (SlidingView)super.getModeView();
    }

    @Override
    public void requestAttention(TopComponent tc) {
        this.tabbedHandler.requestAttention(tc);
    }

    @Override
    public void cancelRequestAttention(TopComponent tc) {
        this.tabbedHandler.cancelRequestAttention(tc);
    }

    @Override
    public void setAttentionHighlight(TopComponent tc, boolean highlight) {
        this.tabbedHandler.setAttentionHighlight(tc, highlight);
    }

    @Override
    public void makeBusy(TopComponent tc, boolean busy) {
        this.tabbedHandler.makeBusy(tc, busy);
    }

    @Override
    public void setTopComponents(TopComponent[] tcs, TopComponent selected) {
        super.setTopComponents(tcs, selected);
    }

    public Rectangle getTabBounds(int tabIndex) {
        return this.tabbedHandler.getTabBounds(tabIndex);
    }

    @Override
    protected Component getModeComponent() {
        return this.panel;
    }

    @Override
    protected Tabbed createTabbed() {
        return new TabbedSlideAdapter(((SlidingView)this.modeView).getSide());
    }

    @Override
    protected boolean isAttachingPossible() {
        return false;
    }

    @Override
    protected TopComponentDroppable getModeDroppable() {
        return this.panel;
    }

    @Override
    protected void updateActive(boolean active) {
        Window window;
        if (active && (window = SwingUtilities.getWindowAncestor(this.panel)) != null && !window.isActive() && WindowManagerImpl.getInstance().getEditorAreaState() == 1) {
            window.toFront();
        }
    }

    @Override
    public boolean isActive() {
        Window window = SwingUtilities.getWindowAncestor(this.panel);
        return window == null ? false : window.isActive();
    }

    @Override
    protected void updateTitle(String title) {
    }

    public static Border computeBorder(String orientation, boolean giveExtraPadding) {
        int bottomOrientationPadding;
        if (isAqua) {
            return BorderFactory.createEmptyBorder();
        }
        int bottom = 0;
        int left = 0;
        int right = 0;
        int top = 0;
        int topOrientationPadding = bottomOrientationPadding = UIManager.getLookAndFeelDefaults().getInt("SplitPane.dividerSize");
        if ((topOrientationPadding -= 5) < 0) {
            topOrientationPadding = 0;
        }
        int extraPadding = 0;
        if (giveExtraPadding) {
            extraPadding = bottomOrientationPadding > 1 ? 2 : 1;
        }
        if (bottomOrientationPadding > 0) {
            --bottomOrientationPadding;
        }
        if ("left".equals(orientation)) {
            top = 1;
            left = 0;
            bottom = 1;
            right = extraPadding;
        }
        if ("bottom".equals(orientation)) {
            top = extraPadding != 0 ? extraPadding : bottomOrientationPadding;
            left = 1;
            bottom = 1;
            right = 1;
        }
        if ("top".equals(orientation)) {
            top = 1;
            left = 1;
            bottom = topOrientationPadding;
            right = 1;
        }
        if ("right".equals(orientation)) {
            top = 1;
            left = extraPadding;
            bottom = 1;
            right = 0;
        }
        return new EmptyBorder(top, left, bottom, right);
    }

    public class VisualPanel
    extends JPanel
    implements ModeComponent,
    TopComponentDroppable {
        private final SlideBarContainer modeContainer;
        private final String side;

        public VisualPanel(SlideBarContainer modeContainer) {
            super(new BorderLayout());
            this.modeContainer = modeContainer;
            this.enableEvents(16);
            this.side = modeContainer.getSlidingView().getSide();
            if (isAqua) {
                this.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
                this.setOpaque(true);
            }
            if (UIManager.getBoolean("NbMainWindow.showCustomBackground")) {
                this.setOpaque(false);
            }
        }

        public SlideBarContainer getOuterClass() {
            return SlideBarContainer.this;
        }

        @Override
        public ModeView getModeView() {
            return this.modeContainer.getModeView();
        }

        @Override
        public int getKind() {
            return this.modeContainer.getKind();
        }

        @Override
        public Shape getIndicationForLocation(Point location) {
            return this.modeContainer.getIndicationForLocation(location);
        }

        @Override
        public Object getConstraintForLocation(Point location) {
            return this.modeContainer.getConstraintForLocation(location);
        }

        @Override
        public Component getDropComponent() {
            return this.modeContainer.getDropComponent();
        }

        @Override
        public ViewElement getDropViewElement() {
            return this.modeContainer.getDropModeView();
        }

        @Override
        public boolean canDrop(TopComponentDraggable transfer, Point location) {
            return this.modeContainer.canDrop(transfer) && !transfer.isModeTransfer();
        }

        @Override
        public boolean supportsKind(TopComponentDraggable transfer) {
            if (transfer.isModeTransfer()) {
                return false;
            }
            if (transfer.isAllowedToMoveAnywhere()) {
                return true;
            }
            boolean isNonEditor = transfer.getKind() == 0 || transfer.getKind() == 2;
            boolean thisIsNonEditor = this.getKind() == 0 || this.getKind() == 2;
            return isNonEditor == thisIsNonEditor;
        }

        @Override
        public Dimension getMinimumSize() {
            if (!this.hasVisibleComponents()) {
                Border b = this.getBorder();
                if (null != b) {
                    Insets insets = b.getBorderInsets(this);
                    return new Dimension(Math.max(1, insets.left + insets.right), Math.max(1, insets.top + insets.bottom));
                }
                return new Dimension(1, 1);
            }
            return super.getMinimumSize();
        }

        @Override
        public Dimension getPreferredSize() {
            if (isAqua && !this.hasVisibleComponents()) {
                return this.getMinimumSize();
            }
            return super.getPreferredSize();
        }

        private boolean hasVisibleComponents() {
            for (Component c : this.getComponents()) {
                if (c instanceof SlideBar || null == c || !c.isVisible()) continue;
                return true;
            }
            return this.modeContainer.getTopComponents().length > 0;
        }

        @Override
        public Border getBorder() {
            if (!isAqua || null == this.modeContainer) {
                return super.getBorder();
            }
            Border result = "bottom".equals(this.side) ? (!this.hasVisibleComponents() ? bottomEmptyBorder : bottomBorder) : ("top".equals(this.side) ? (!this.hasVisibleComponents() ? topEmptyBorder : topBorder) : ("right".equals(this.side) ? (!this.hasVisibleComponents() ? rightEmptyBorder : rightBorder) : ("left".equals(this.side) ? (!this.hasVisibleComponents() ? leftEmptyBorder : leftBorder) : BorderFactory.createEmptyBorder())));
            return result;
        }
    }

}

