/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.ComponentConverter
 *  org.netbeans.swing.tabcontrol.ComponentConverter$Fixed
 *  org.netbeans.swing.tabcontrol.DefaultTabDataModel
 *  org.netbeans.swing.tabcontrol.TabData
 *  org.netbeans.swing.tabcontrol.TabDataModel
 *  org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer
 *  org.netbeans.swing.tabcontrol.customtabs.TabbedType
 *  org.netbeans.swing.tabcontrol.event.TabActionEvent
 */
package org.netbeans.core.windows.view.ui.tabcontrol.tabbedpane;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.Timer;
import org.netbeans.swing.tabcontrol.ComponentConverter;
import org.netbeans.swing.tabcontrol.DefaultTabDataModel;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.customtabs.TabbedType;
import org.netbeans.swing.tabcontrol.event.TabActionEvent;

public class NBTabbedPane
extends JTabbedPane {
    private final WinsysInfoForTabbedContainer winsysInfo;
    private final TabDataModel dataModel;
    protected final TabbedType type;
    private transient List<ActionListener> actionListenerList;
    private boolean active;
    private ComponentConverter converter;
    private int _tabIndex;
    private Color _background;
    private Color _foreground;
    private Color _savedBackground;
    private Color _savedForeground;
    private int count;
    private int blinks = 3;
    private Timer timer;

    public NBTabbedPane(TabDataModel model, TabbedType type, WinsysInfoForTabbedContainer winsysInfo) {
        this.timer = new Timer(1000, new ActionListener(){
            private boolean on;

            @Override
            public void actionPerformed(ActionEvent e) {
                NBTabbedPane.this.count = 0;
                NBTabbedPane.this.blink(this.on);
                this.on = !this.on;
            }
        });
        switch (type) {
            case VIEW: 
            case EDITOR: {
                break;
            }
            default: {
                throw new IllegalArgumentException("Unsupported UI type: " + (Object)type);
            }
        }
        if (model == null) {
            model = new DefaultTabDataModel();
        }
        this.dataModel = model;
        this.type = type;
        this.winsysInfo = winsysInfo;
        this.updateUI();
        this.setFocusable(false);
    }

    public WinsysInfoForTabbedContainer getWinsysInfoForTabbedContainer() {
        return this.winsysInfo;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final void postActionEvent(TabActionEvent event) {
        List<ActionListener> list;
        NBTabbedPane nBTabbedPane = this;
        synchronized (nBTabbedPane) {
            if (this.actionListenerList == null) {
                return;
            }
            list = Collections.unmodifiableList(this.actionListenerList);
        }
        for (ActionListener l : list) {
            l.actionPerformed((ActionEvent)event);
        }
    }

    public final TabbedType getType() {
        return this.type;
    }

    public int indexOf(Component comp) {
        if (null == comp) {
            return -1;
        }
        return this.indexOfComponent(comp);
    }

    public final ComponentConverter getComponentConverter() {
        if (this.converter != null) {
            return this.converter;
        }
        return ComponentConverter.DEFAULT;
    }

    public final void setComponentConverter(ComponentConverter cc) {
        List l;
        ComponentConverter old = this.converter;
        this.converter = cc;
        if (old instanceof ComponentConverter.Fixed && cc instanceof ComponentConverter.Fixed && !(l = this.getDataModel().getTabs()).isEmpty()) {
            TabData[] td = l.toArray((T[])new TabData[0]);
            this.getDataModel().setTabs(new TabData[0]);
            this.getDataModel().setTabs(td);
        }
    }

    public final synchronized void addActionListener(ActionListener listener) {
        if (this.actionListenerList == null) {
            this.actionListenerList = new ArrayList<ActionListener>();
        }
        this.actionListenerList.add(listener);
    }

    public final synchronized void removeActionListener(ActionListener listener) {
        if (this.actionListenerList != null) {
            this.actionListenerList.remove(listener);
            if (this.actionListenerList.isEmpty()) {
                this.actionListenerList = null;
            }
        }
    }

    public TabDataModel getDataModel() {
        return this.dataModel;
    }

    public int dropIndexOfPoint(Point location) {
        int index = this.indexAtLocation(location.x, location.y);
        if (index < 0) {
            index = this.getTabCount();
        } else if (index == this.getTabCount() - 1) {
            Rectangle rect = this.getBoundsAt(index);
            if (this.getTabPlacement() == 1 || this.getTabPlacement() == 3) {
                if (location.x > rect.x + rect.width / 2) {
                    ++index;
                }
            } else if (location.y > rect.y + rect.height / 2) {
                ++index;
            }
        }
        return index;
    }

    public final void requestAttention(int tab) {
        this.startBlinking(tab, Color.RED, Color.BLUE);
    }

    public final void cancelRequestAttention(int tab) {
        this.stopBlinking();
    }

    public final void setAttentionHighlight(int tabIndex, boolean highlight) {
    }

    public final void setActive(boolean active) {
        if (active != this.active) {
            this.active = active;
            this.firePropertyChange("active", !active, active);
        }
    }

    public int tabForCoordinate(Point p) {
        return this.indexAtLocation(p.x, p.y);
    }

    public Image createImageOfTab(int tabIndex) {
        TabData td = this.getDataModel().getTab(tabIndex);
        JLabel lbl = new JLabel(td.getText());
        int width = lbl.getFontMetrics(lbl.getFont()).stringWidth(td.getText());
        int height = lbl.getFontMetrics(lbl.getFont()).getHeight();
        width = width + td.getIcon().getIconWidth() + 6;
        height = Math.max(height, td.getIcon().getIconHeight()) + 5;
        GraphicsConfiguration config = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
        BufferedImage image = config.createCompatibleImage(width, height);
        Graphics2D g = image.createGraphics();
        g.setColor(lbl.getForeground());
        g.setFont(lbl.getFont());
        td.getIcon().paintIcon(lbl, g, 0, 0);
        g.drawString(td.getText(), 18, height / 2);
        return image;
    }

    public void startBlinking(int tabIndex, Color foreground, Color background) {
        this._tabIndex = tabIndex;
        this._savedForeground = this.getForeground();
        this._savedBackground = this.getBackground();
        this._foreground = foreground;
        this._background = background;
        this.timer.start();
    }

    private void blink(boolean on) {
        if (this.count >= this.blinks) {
            this.stopBlinking();
        }
        ++this.count;
        if (on) {
            if (this._foreground != null) {
                this.setForegroundAt(this._tabIndex, this._foreground);
            }
            if (this._background != null) {
                this.setBackgroundAt(this._tabIndex, this._background);
            }
        } else {
            if (this._savedForeground != null) {
                this.setForegroundAt(this._tabIndex, this._savedForeground);
            }
            if (this._savedBackground != null) {
                this.setBackgroundAt(this._tabIndex, this._savedBackground);
            }
        }
        this.repaint();
    }

    public void stopBlinking() {
        this.timer.stop();
        this.setForegroundAt(this._tabIndex, this._savedForeground);
        this.setBackgroundAt(this._tabIndex, this._savedBackground);
    }

}

