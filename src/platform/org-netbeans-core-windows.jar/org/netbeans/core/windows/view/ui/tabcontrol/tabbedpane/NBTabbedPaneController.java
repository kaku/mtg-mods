/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.ComponentConverter
 *  org.netbeans.swing.tabcontrol.TabData
 *  org.netbeans.swing.tabcontrol.TabDataModel
 *  org.netbeans.swing.tabcontrol.customtabs.TabbedType
 *  org.netbeans.swing.tabcontrol.event.ArrayDiff
 *  org.netbeans.swing.tabcontrol.event.ComplexListDataEvent
 *  org.netbeans.swing.tabcontrol.event.ComplexListDataListener
 *  org.netbeans.swing.tabcontrol.event.TabActionEvent
 *  org.netbeans.swing.tabcontrol.event.VeryComplexListDataEvent
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view.ui.tabcontrol.tabbedpane;

import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.SingleSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListDataEvent;
import javax.swing.plaf.TabbedPaneUI;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import org.netbeans.core.windows.view.ui.tabcontrol.tabbedpane.CloseableTabComponent;
import org.netbeans.core.windows.view.ui.tabcontrol.tabbedpane.NBTabbedPane;
import org.netbeans.swing.tabcontrol.ComponentConverter;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.customtabs.TabbedType;
import org.netbeans.swing.tabcontrol.event.ArrayDiff;
import org.netbeans.swing.tabcontrol.event.ComplexListDataEvent;
import org.netbeans.swing.tabcontrol.event.ComplexListDataListener;
import org.netbeans.swing.tabcontrol.event.TabActionEvent;
import org.netbeans.swing.tabcontrol.event.VeryComplexListDataEvent;
import org.openide.windows.TopComponent;

public class NBTabbedPaneController {
    protected ComplexListDataListener modelListener = null;
    private NBTabbedPane container;
    protected final Controller controller;

    public NBTabbedPaneController(final NBTabbedPane container) {
        this.container = container;
        this.modelListener = new ComplexListDataListener(){

            public void indicesAdded(ComplexListDataEvent e) {
                int[] indices = e.getIndices();
                for (int i = 0; i < indices.length; ++i) {
                    NBTabbedPaneController.this.addToContainer(container.getDataModel().getTab(indices[i]), 0);
                }
            }

            public void indicesRemoved(ComplexListDataEvent e) {
                int[] indices = e.getIndices();
                TabData[] removedTabs = e.getAffectedItems();
                for (int i = 0; i < indices.length; ++i) {
                    Component curComp = NBTabbedPaneController.this.toComp(removedTabs[i]);
                    container.remove(curComp);
                }
            }

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            public void indicesChanged(ComplexListDataEvent e) {
                if (e instanceof VeryComplexListDataEvent) {
                    ArrayDiff dif = ((VeryComplexListDataEvent)e).getDiff();
                    Set deleted = dif.getDeletedIndices();
                    Set added = dif.getAddedIndices();
                    TabData[] old = dif.getOldData();
                    TabData[] nue = dif.getNewData();
                    HashSet<Component> components = new HashSet<Component>();
                    for (int i = 0; i < nue.length; ++i) {
                        components.add(NBTabbedPaneController.this.toComp(nue[i]));
                    }
                    boolean changed = false;
                    Object object = container.getTreeLock();
                    synchronized (object) {
                        if (added.isEmpty() && deleted.isEmpty() && !dif.getMovedIndices().isEmpty()) {
                            for (int i2 = 0; i2 < container.getTabCount(); ++i2) {
                                added.add(i2);
                                deleted.add(i2);
                            }
                            components.clear();
                        }
                        if (!deleted.isEmpty()) {
                            for (Integer idx : deleted) {
                                TabData del = old[idx];
                                if (components.contains(NBTabbedPaneController.this.toComp(del))) continue;
                                container.remove(NBTabbedPaneController.this.toComp(del));
                                changed = true;
                            }
                        }
                        if (!added.isEmpty()) {
                            for (Integer idx : added) {
                                TabData add = nue[idx];
                                if (container.isAncestorOf(NBTabbedPaneController.this.toComp(add))) continue;
                                NBTabbedPaneController.this.addToContainer(add, idx);
                                changed = true;
                            }
                        }
                    }
                    if (changed) {
                        container.revalidate();
                        container.repaint();
                    }
                }
            }

            public void intervalAdded(ListDataEvent e) {
                Component curC = null;
                for (int i = e.getIndex0(); i <= e.getIndex1(); ++i) {
                    curC = NBTabbedPaneController.this.toComp(container.getDataModel().getTab(i));
                    NBTabbedPaneController.this.addToContainer(container.getDataModel().getTab(i), 0);
                }
            }

            public void intervalRemoved(ListDataEvent e) {
                ComplexListDataEvent clde = (ComplexListDataEvent)e;
                TabData[] removedTabs = clde.getAffectedItems();
                for (int i = 0; i < removedTabs.length; ++i) {
                    Component curComp = NBTabbedPaneController.this.toComp(removedTabs[i]);
                    container.remove(curComp);
                }
            }

            private void maybeMakeSelectedTabVisible(ComplexListDataEvent clde) {
                if (!container.isShowing() || container.getWidth() < 10) {
                    return;
                }
                if (container.getType() == TabbedType.EDITOR) {
                    int idx = container.getModel().getSelectedIndex();
                    if (clde.getIndex0() == clde.getIndex1() && clde.getIndex0() == idx) {
                        container.setSelectedIndex(idx);
                    }
                }
            }

            public void contentsChanged(ListDataEvent e) {
                if (e instanceof ComplexListDataEvent) {
                    ComplexListDataEvent clde = (ComplexListDataEvent)e;
                    int index = clde.getIndex0();
                    if (clde.isUserObjectChanged() && index != -1) {
                        boolean add;
                        Component comp = container.getComponent(index);
                        container.remove(comp);
                        boolean bl = add = index == container.getModel().getSelectedIndex();
                        if (add) {
                            NBTabbedPaneController.this.addToContainer(container.getDataModel().getTab(index), index);
                        }
                    }
                    if (clde.isTextChanged()) {
                        this.maybeMakeSelectedTabVisible(clde);
                    }
                }
            }
        };
        this.controller = new Controller();
        container.addMouseListener(this.controller);
    }

    private void addToContainer(TabData tabData, int index) {
        if (index > this.container.getTabCount()) {
            index = -1;
        }
        this.container.add(tabData.getComponent(), index);
        index = this.container.indexOfComponent(tabData.getComponent());
        this.container.setTabComponentAt(index, new CloseableTabComponent(tabData.getIcon(), tabData.getText(), true, tabData.getTooltip(), this.container, this.controller));
    }

    protected final Component toComp(TabData data) {
        return this.container.getComponentConverter().getComponent(data);
    }

    public void attachModelAndSelectionListeners() {
        this.container.getDataModel().addComplexListDataListener(this.modelListener);
    }

    protected void detachModelAndSelectionListeners() {
        this.container.getDataModel().removeComplexListDataListener(this.modelListener);
    }

    protected final boolean shouldPerformAction(String command, int tab, MouseEvent event) {
        TabActionEvent evt = new TabActionEvent((Object)this.container, command, tab, event);
        this.container.postActionEvent(evt);
        return !evt.isConsumed();
    }

    private void makeRollover(int tabIndex) {
        if (!(this.container.getUI() instanceof BasicTabbedPaneUI)) {
            return;
        }
        BasicTabbedPaneUI ui = (BasicTabbedPaneUI)this.container.getUI();
        try {
            Method m = this.container.getUI().getClass().getDeclaredMethod("setRolloverTab", Integer.TYPE);
            m.setAccessible(true);
            m.invoke(ui, tabIndex);
        }
        catch (Exception e) {
            // empty catch block
        }
    }

    protected class Controller
    extends MouseAdapter
    implements MouseMotionListener {
        protected Controller() {
        }

        protected boolean shouldReact(MouseEvent e) {
            boolean isLeft = SwingUtilities.isLeftMouseButton(e);
            return isLeft;
        }

        @Override
        public void mousePressed(MouseEvent e) {
            Point p = e.getPoint();
            p = SwingUtilities.convertPoint(e.getComponent(), p, NBTabbedPaneController.this.container);
            int tabIndex = NBTabbedPaneController.this.container.indexAtLocation(p.x, p.y);
            SingleSelectionModel sel = NBTabbedPaneController.this.container.getModel();
            if (tabIndex >= 0 && e.getComponent() != NBTabbedPaneController.this.container) {
                CloseableTabComponent tab = (CloseableTabComponent)NBTabbedPaneController.this.container.getTabComponentAt(tabIndex);
                if (tab.isInCloseButton(e)) {
                    return;
                }
                tabIndex = NBTabbedPaneController.this.container.indexOf(NBTabbedPaneController.this.container.getComponentAt(tabIndex));
                boolean change = NBTabbedPaneController.this.shouldPerformAction("select", tabIndex, e);
                if (change) {
                    sel.setSelectedIndex(tabIndex);
                    Component tc = NBTabbedPaneController.this.container.getDataModel().getTab(tabIndex).getComponent();
                    if (null != tc && tc instanceof TopComponent && !((TopComponent)tc).isAncestorOf(KeyboardFocusManager.getCurrentKeyboardFocusManager().getPermanentFocusOwner())) {
                        ((TopComponent)tc).requestActive();
                    }
                }
            }
            if (e.isPopupTrigger()) {
                NBTabbedPaneController.this.shouldPerformAction("popup", tabIndex, e);
            }
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            int i;
            Point p = e.getPoint();
            p = SwingUtilities.convertPoint(e.getComponent(), p, NBTabbedPaneController.this.container);
            int tabIndex = i = NBTabbedPaneController.this.container.indexAtLocation(p.x, p.y);
            if (i >= 0) {
                tabIndex = NBTabbedPaneController.this.container.indexOf(NBTabbedPaneController.this.container.getComponentAt(i));
            }
            if (e.getClickCount() >= 2 && !e.isPopupTrigger()) {
                boolean change;
                SingleSelectionModel sel = NBTabbedPaneController.this.container.getModel();
                if (i >= 0 && (change = NBTabbedPaneController.this.shouldPerformAction("select", tabIndex, e))) {
                    sel.setSelectedIndex(i);
                }
                if (i != -1 && e.getButton() == 1) {
                    NBTabbedPaneController.this.shouldPerformAction("maximize", tabIndex, e);
                }
            } else if (e.getClickCount() == 1 && e.getButton() == 1 && i >= 0) {
                CloseableTabComponent tab = (CloseableTabComponent)NBTabbedPaneController.this.container.getTabComponentAt(i);
                if (tab.isInCloseButton(e)) {
                    String command = "close";
                    if (NBTabbedPaneController.this.container.getType() == TabbedType.EDITOR) {
                        if ((e.getModifiers() & 1) > 0) {
                            command = "closeAll";
                        } else if ((e.getModifiers() & 8) > 0) {
                            command = "closeAllButThis";
                        }
                    }
                    NBTabbedPaneController.this.shouldPerformAction(command, tabIndex, e);
                }
            } else if (e.getClickCount() == 1 && e.getButton() == 2 && i >= 0) {
                CloseableTabComponent tab = (CloseableTabComponent)NBTabbedPaneController.this.container.getTabComponentAt(i);
                String command = "close";
                NBTabbedPaneController.this.shouldPerformAction(command, tabIndex, e);
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            Point p = e.getPoint();
            p = SwingUtilities.convertPoint(e.getComponent(), p, NBTabbedPaneController.this.container);
            int i = NBTabbedPaneController.this.container.indexAtLocation(p.x, p.y);
            if (e.isPopupTrigger()) {
                if (i >= 0) {
                    i = NBTabbedPaneController.this.container.indexOf(NBTabbedPaneController.this.container.getComponentAt(i));
                }
                NBTabbedPaneController.this.shouldPerformAction("popup", i, e);
            }
        }

        @Override
        public void mouseDragged(MouseEvent e) {
        }

        @Override
        public void mouseMoved(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            Point p = e.getPoint();
            p = SwingUtilities.convertPoint(e.getComponent(), p, NBTabbedPaneController.this.container);
            int i = NBTabbedPaneController.this.container.indexAtLocation(p.x, p.y);
            if (i >= 0) {
                NBTabbedPaneController.this.makeRollover(i);
            }
        }
    }

}

