/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view.dnd;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.TexturePaint;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.Autoscroll;
import java.awt.dnd.DropTargetContext;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Set;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.netbeans.core.windows.Constants;
import org.netbeans.core.windows.Debug;
import org.netbeans.core.windows.view.Controller;
import org.netbeans.core.windows.view.dnd.EnhancedDragPainter;
import org.netbeans.core.windows.view.dnd.TopComponentDraggable;
import org.netbeans.core.windows.view.dnd.TopComponentDroppable;
import org.netbeans.core.windows.view.dnd.WindowDnDManager;

public final class DropTargetGlassPane
extends JPanel
implements DropTargetListener {
    private final Observer observer;
    private final Informer informer;
    private WindowDnDManager windowDragAndDrop;
    private Point location;
    private TopComponentDroppable droppable;
    private Reference<Autoscroll> lastAutoscroll = null;
    private static final boolean DEBUG = Debug.isLoggable(DropTargetGlassPane.class);
    private Point dragLocation = null;
    private Shape currentDropIndication;
    private EnhancedDragPainter currentPainter;
    private Component componentUnderCursor;
    private TexturePaint texturePaint;
    private int modeKind = -1;
    private Stroke stroke;
    private static final Color FILL_COLOR = new Color(200, 200, 200, 120);

    public DropTargetGlassPane(WindowDnDManager wdnd) {
        this.observer = wdnd;
        this.informer = wdnd;
        this.windowDragAndDrop = wdnd;
        this.setOpaque(false);
    }

    public void initialize() {
        if (this.isVisible()) {
            this.revalidate();
        } else {
            this.setVisible(true);
        }
    }

    public void uninitialize() {
        if (this.location != null) {
            this.dragFinished();
        }
        this.setVisible(false);
        this.stopAutoscroll();
    }

    void dragOver(Point location, TopComponentDroppable droppable) {
        this.droppable = droppable;
        this.setDragLocation(location);
        this.autoscroll(droppable, location);
    }

    private void setDragLocation(Point p) {
        Point old = this.dragLocation;
        this.dragLocation = p;
        if (p != null && p.equals(old)) {
            return;
        }
        if (p == null) {
            return;
        }
        this.setVisible(true);
        if (this.droppable != null) {
            Component c;
            Shape s;
            Rectangle repaintRectangle = null;
            if (null != this.currentDropIndication) {
                Rectangle rect;
                repaintRectangle = this.currentDropIndication.getBounds();
                repaintRectangle = SwingUtilities.convertRectangle(this.componentUnderCursor, repaintRectangle, this);
                if (null != this.currentPainter && null != (rect = this.currentPainter.getPaintArea())) {
                    repaintRectangle.add(rect);
                }
            }
            if (null != (s = this.droppable.getIndicationForLocation(SwingUtilities.convertPoint(this, p, c = this.droppable.getDropComponent()))) && s.equals(this.currentDropIndication)) {
                return;
            }
            this.currentPainter = this.droppable instanceof EnhancedDragPainter ? (EnhancedDragPainter)((Object)this.droppable) : null;
            this.currentDropIndication = s;
            this.componentUnderCursor = c;
            if (null != this.currentDropIndication) {
                Rectangle rect = this.currentDropIndication.getBounds();
                rect = SwingUtilities.convertRectangle(c, rect, this);
                if (null != repaintRectangle) {
                    repaintRectangle.add(rect);
                } else {
                    repaintRectangle = rect;
                }
                if (null != this.currentPainter && null != (rect = this.currentPainter.getPaintArea())) {
                    repaintRectangle.add(rect);
                }
            }
            if (null != repaintRectangle) {
                repaintRectangle.grow(2, 2);
                this.repaint(repaintRectangle);
            }
        } else if (null != this.currentDropIndication) {
            Rectangle repaintRect = this.currentDropIndication.getBounds();
            this.currentDropIndication = null;
            if (null != this.currentPainter) {
                Rectangle rect = this.currentPainter.getPaintArea();
                if (null != rect) {
                    repaintRect = repaintRect.union(rect);
                }
                this.currentPainter = null;
            }
            this.repaint(repaintRect);
        }
    }

    private void dragExited() {
        this.clear();
    }

    public void clearIndications() {
        this.currentDropIndication = null;
        this.currentPainter = null;
        this.componentUnderCursor = null;
        this.repaint();
        this.clear();
    }

    private void dragActionChanged(Point location) {
        this.setDragLocation(location);
    }

    private void dragFinished() {
        this.clear();
    }

    private void clear() {
        this.stopAutoscroll();
        this.droppable = null;
        this.setDragLocation(null);
    }

    @Override
    public void paint(Graphics g) {
        if (null != this.currentDropIndication) {
            Color c;
            Graphics2D g2d = (Graphics2D)g.create();
            if (null != this.currentPainter) {
                this.currentPainter.additionalDragPaint(g2d);
            }
            if ((c = UIManager.getColor("Panel.dropTargetGlassPane")) == null) {
                c = Color.red;
            }
            g2d.setColor(c);
            Point p = new Point(0, 0);
            p = SwingUtilities.convertPoint(this.componentUnderCursor, p, this);
            AffineTransform at = AffineTransform.getTranslateInstance(p.x, p.y);
            g2d.transform(at);
            g2d.setStroke(this.getIndicationStroke());
            g2d.setPaint(this.getIndicationPaint());
            Color fillColor = Constants.SWITCH_DROP_INDICATION_FADE ? FILL_COLOR : null;
            g2d.draw(this.currentDropIndication);
            if (null != fillColor) {
                g2d.fill(this.currentDropIndication);
            }
            g2d.dispose();
        }
    }

    private TexturePaint getIndicationPaint() {
        if (this.droppable != null && this.droppable.getKind() != this.modeKind) {
            boolean isModeMixing;
            BufferedImage image = new BufferedImage(2, 2, 2);
            Graphics2D g2 = image.createGraphics();
            Color c = UIManager.getColor("Panel.dropTargetGlassPane");
            boolean bl = isModeMixing = this.droppable.getKind() == 1 && this.windowDragAndDrop.getStartingTransfer().getKind() != 1 || this.droppable.getKind() != 1 && this.windowDragAndDrop.getStartingTransfer().getKind() == 1;
            if (c == null) {
                c = new Color(255, 90, 0);
            }
            if (isModeMixing) {
                g2.setColor(c);
                g2.fillRect(0, 0, 1, 1);
                g2.fillRect(1, 1, 1, 1);
                g2.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), 0));
                g2.setComposite(AlphaComposite.getInstance(3, 0.75f));
                g2.fillRect(1, 0, 1, 1);
                g2.fillRect(0, 1, 1, 1);
            } else {
                g2.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), 200));
                g2.fillRect(0, 0, 2, 2);
            }
            this.texturePaint = new TexturePaint(image, new Rectangle(0, 0, 2, 2));
            this.modeKind = this.droppable.getKind();
        }
        return this.texturePaint;
    }

    private Stroke getIndicationStroke() {
        if (null == this.stroke) {
            this.stroke = new BasicStroke(3.0f);
        }
        return this.stroke;
    }

    @Override
    public void dragEnter(DropTargetDragEvent evt) {
        int dropAction;
        if (DEBUG) {
            DropTargetGlassPane.debugLog("");
            DropTargetGlassPane.debugLog("dragEnter");
        }
        if ((dropAction = evt.getDropAction()) == 0) {
            dropAction = 2;
        }
        if ((dropAction & 3) > 0) {
            evt.acceptDrag(dropAction);
        } else {
            evt.rejectDrag();
        }
    }

    @Override
    public void dragExit(DropTargetEvent evt) {
        Component c;
        if (DEBUG) {
            DropTargetGlassPane.debugLog("");
            DropTargetGlassPane.debugLog("dragExit");
        }
        if ((c = evt.getDropTargetContext().getComponent()) == this) {
            this.dragExited();
            this.stopAutoscroll();
        }
    }

    @Override
    public void dragOver(DropTargetDragEvent evt) {
        if (DEBUG) {
            DropTargetGlassPane.debugLog("");
            DropTargetGlassPane.debugLog("dragOver");
        }
        this.observer.setLastDropTarget(this);
    }

    void autoscroll(TopComponentDroppable droppable, Point location) {
        Autoscroll prev;
        Component c = droppable.getDropComponent();
        location = SwingUtilities.convertPoint(this, location, c);
        Component child = SwingUtilities.getDeepestComponentAt(c, location.x, location.y);
        Autoscroll as = child instanceof Autoscroll ? (Autoscroll)((Object)child) : (Autoscroll)((Object)SwingUtilities.getAncestorOfClass(Autoscroll.class, child));
        Autoscroll autoscroll = prev = null == this.lastAutoscroll ? null : this.lastAutoscroll.get();
        if (null != prev && prev != as) {
            prev.autoscroll(new Point(Integer.MIN_VALUE, Integer.MIN_VALUE));
        }
        if (as != null) {
            as.autoscroll(location);
            this.lastAutoscroll = new WeakReference<Autoscroll>(as);
        } else {
            this.lastAutoscroll = null;
        }
    }

    void stopAutoscroll() {
        Autoscroll as = null == this.lastAutoscroll ? null : this.lastAutoscroll.get();
        this.lastAutoscroll = null;
        if (as != null) {
            as.autoscroll(new Point(Integer.MIN_VALUE, Integer.MIN_VALUE));
        }
    }

    @Override
    public void dropActionChanged(DropTargetDragEvent evt) {
        int dropAction;
        if (DEBUG) {
            DropTargetGlassPane.debugLog("");
            DropTargetGlassPane.debugLog("dropActionChanged");
        }
        boolean acceptDrag = (dropAction = evt.getDropAction()) == 2 || dropAction == 1 && this.informer.isCopyOperationPossible();
        if (acceptDrag) {
            evt.acceptDrag(dropAction);
        } else {
            evt.rejectDrag();
        }
        Component c = evt.getDropTargetContext().getComponent();
        if (c == this) {
            this.dragActionChanged(acceptDrag ? evt.getLocation() : null);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    @Override
    public void drop(DropTargetDropEvent evt) {
        boolean success;
        Point loc;
        int dropAction;
        block6 : {
            Component c;
            if (DEBUG) {
                DropTargetGlassPane.debugLog("");
                DropTargetGlassPane.debugLog("drop");
            }
            if ((c = evt.getDropTargetContext().getComponent()) == this) {
                this.dragFinished();
            }
            if ((dropAction = evt.getDropAction()) != 2 && dropAction != 1) {
                evt.rejectDrop();
                return;
            }
            evt.acceptDrop(dropAction);
            success = false;
            try {
                loc = evt.getLocation();
                SwingUtilities.convertPointToScreen(loc, c);
                if (!WindowDnDManager.isAroundCenterPanel(loc)) break block6;
                this.observer.setDropSuccess(success);
                evt.dropComplete(false);
            }
            catch (Throwable var6_6) {
                this.observer.setDropSuccess(success);
                evt.dropComplete(false);
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        DropTargetGlassPane.this.windowDragAndDrop.dragFinished();
                        DropTargetGlassPane.this.windowDragAndDrop.dragFinishedEx();
                    }
                });
                throw var6_6;
            }
            SwingUtilities.invokeLater(new );
            return;
        }
        success = this.windowDragAndDrop.tryPerformDrop(this.informer.getController(), this.informer.getFloatingFrames(), loc, dropAction, evt.getTransferable());
        this.observer.setDropSuccess(success);
        evt.dropComplete(false);
        SwingUtilities.invokeLater(new );
    }

    private static void debugLog(String message) {
        Debug.log(DropTargetGlassPane.class, message);
    }

    static interface Informer {
        public boolean isCopyOperationPossible();

        public Controller getController();

        public Set<Component> getFloatingFrames();
    }

    static interface Observer {
        public void setDropSuccess(boolean var1);

        public void setLastDropTarget(DropTargetGlassPane var1);
    }

}

