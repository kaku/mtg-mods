/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view.ui.slides;

import java.awt.Component;
import java.awt.Rectangle;
import javax.swing.JLayeredPane;

public interface SlideOperation {
    public static final int SLIDE_IN = 0;
    public static final int SLIDE_OUT = 1;
    public static final int SLIDE_INTO_EDGE = 2;
    public static final int SLIDE_INTO_DESKTOP = 3;
    public static final int SLIDE_RESIZE = 4;

    public Component getComponent();

    public Rectangle getStartBounds();

    public Rectangle getFinishBounds();

    public String getSide();

    public boolean requestsActivation();

    public void run(JLayeredPane var1, Integer var2);

    public void setStartBounds(Rectangle var1);

    public void setFinishBounds(Rectangle var1);

    public int getType();

    public void prepareEffect();
}

