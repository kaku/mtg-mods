/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.customtabs.Tabbed
 */
package org.netbeans.core.windows.view.dnd;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.util.prefs.Preferences;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import org.netbeans.core.windows.options.WinSysPrefs;
import org.netbeans.swing.tabcontrol.customtabs.Tabbed;

class DragWindow
extends JWindow {
    private boolean useFadeEffects = !Boolean.getBoolean("winsys.dnd.nofadeeffects");
    private Tabbed container;
    private Rectangle tabRectangle;
    private BufferedImage tabImage;
    private BufferedImage contentImage;
    private BufferedImage imageBuffer;
    private float contentAlpha = 0.15f;
    private Color contentBackground = Color.white;
    private Timer currentEffect;
    private boolean dropEnabled = true;
    private static final float NO_DROP_ALPHA = 0.5f;
    private static final float ALPHA_INCREMENT = 0.085f;

    public DragWindow(Tabbed container, Rectangle tabRectangle, Dimension contentSize, Component content) {
        this.tabRectangle = tabRectangle;
        this.container = container;
        this.setAlwaysOnTop(true);
        this.tabImage = this.createTabImage();
        this.contentImage = this.createContentImage(content, contentSize);
        if (this.useFadeEffects) {
            this.imageBuffer = this.createImageBuffer(this.contentImage);
            this.currentEffect = this.createInitialEffect();
            this.currentEffect.start();
        } else {
            this.contentAlpha = 1.0f;
        }
    }

    private BufferedImage createTabImage() {
        GraphicsConfiguration config = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
        Window parentWindow = SwingUtilities.getWindowAncestor(this.container.getComponent());
        Rectangle rect = SwingUtilities.convertRectangle(this.container.getComponent(), this.tabRectangle, parentWindow);
        BufferedImage res = config.createCompatibleImage(this.tabRectangle.width, this.tabRectangle.height);
        Graphics2D g = res.createGraphics();
        g.translate(- rect.x, - rect.y);
        g.setClip(rect);
        parentWindow.paint(g);
        return res;
    }

    private BufferedImage createContentImage(Component c, Dimension contentSize) {
        GraphicsConfiguration config = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
        BufferedImage res = config.createCompatibleImage(contentSize.width, contentSize.height);
        Graphics2D g = res.createGraphics();
        g.setColor(Color.white);
        g.fillRect(0, 0, contentSize.width, contentSize.height);
        if (WinSysPrefs.HANDLER.getBoolean("dnd.smallwindows", true) && c.getWidth() > 0 && c.getHeight() > 0) {
            double xScale = contentSize.getWidth() / (double)c.getWidth();
            double yScale = contentSize.getHeight() / (double)c.getHeight();
            g.setTransform(AffineTransform.getScaleInstance(xScale, yScale));
        }
        c.paint(g);
        return res;
    }

    private BufferedImage createImageBuffer(BufferedImage src) {
        GraphicsConfiguration config = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
        BufferedImage res = config.createCompatibleImage(src.getWidth(), src.getHeight());
        Graphics2D g = res.createGraphics();
        g.setColor(this.contentBackground);
        g.fillRect(0, 0, res.getWidth(), res.getHeight());
        g.setComposite(AlphaComposite.getInstance(3, this.contentAlpha));
        g.drawImage(src, 0, 0, null);
        return res;
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D)g.create();
        g2d.setColor(Color.white);
        g2d.fillRect(0, 0, this.getWidth(), this.tabRectangle.height);
        g2d.setColor(Color.gray);
        g2d.drawRect(0, this.tabRectangle.height, this.getWidth() - 1, this.getHeight() - this.tabRectangle.height - 1);
        if (WinSysPrefs.HANDLER.getBoolean("dnd.smallwindows", true)) {
            g2d.drawImage(this.tabImage, 0, 0, null);
        } else {
            g2d.drawImage(this.tabImage, this.tabRectangle.x, this.tabRectangle.y, null);
        }
        if (!this.useFadeEffects || null == this.imageBuffer) {
            g2d.setColor(Color.black);
            g2d.fillRect(1, this.tabRectangle.height + 1, this.getWidth() - 2, this.getHeight() - this.tabRectangle.height - 2);
            g2d.setComposite(AlphaComposite.getInstance(3, this.contentAlpha));
            g2d.drawImage(this.contentImage, 1, this.tabRectangle.height + 1, null);
        } else if (null != this.imageBuffer) {
            g2d.drawImage(this.imageBuffer, 1, this.tabRectangle.height + 1, null);
        }
        g2d.dispose();
    }

    public void setDropFeedback(boolean dropEnabled) {
        boolean prevState = this.dropEnabled;
        this.dropEnabled = dropEnabled;
        if (prevState != this.dropEnabled) {
            if (null != this.currentEffect) {
                this.currentEffect.stop();
            }
            if (this.useFadeEffects) {
                this.contentBackground = Color.black;
                this.currentEffect = dropEnabled ? this.createDropEnabledEffect() : this.createNoDropEffect();
                this.currentEffect.start();
                this.repaint();
            } else {
                this.contentAlpha = dropEnabled ? 1.0f : 0.5f;
                this.repaint();
            }
        }
    }

    private Timer createInitialEffect() {
        final Timer timer = new Timer(100, null);
        timer.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                if (DragWindow.this.contentAlpha < 1.0f) {
                    DragWindow.access$016(DragWindow.this, 0.085f);
                } else {
                    timer.stop();
                }
                if (DragWindow.this.contentAlpha > 1.0f) {
                    DragWindow.this.contentAlpha = 1.0f;
                }
                DragWindow.this.repaintImageBuffer();
                DragWindow.this.repaint();
            }
        });
        timer.setInitialDelay(0);
        return timer;
    }

    private Timer createDropEnabledEffect() {
        return this.createInitialEffect();
    }

    private Timer createNoDropEffect() {
        final Timer timer = new Timer(100, null);
        timer.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                if (DragWindow.this.contentAlpha > 0.5f) {
                    DragWindow.access$024(DragWindow.this, 0.085f);
                } else {
                    timer.stop();
                }
                if (DragWindow.this.contentAlpha < 0.5f) {
                    DragWindow.this.contentAlpha = 0.5f;
                }
                DragWindow.this.repaintImageBuffer();
                DragWindow.this.repaint();
            }
        });
        timer.setInitialDelay(0);
        return timer;
    }

    private void repaintImageBuffer() {
        if (!this.useFadeEffects) {
            return;
        }
        if (null == this.imageBuffer) {
            return;
        }
        Graphics2D g2d = this.imageBuffer.createGraphics();
        g2d.setColor(this.contentBackground);
        g2d.fillRect(0, 0, this.imageBuffer.getWidth(), this.imageBuffer.getHeight());
        g2d.setComposite(AlphaComposite.getInstance(3, this.contentAlpha));
        g2d.drawImage(this.contentImage, 0, 0, null);
        g2d.dispose();
    }

    void abort() {
        if (null != this.currentEffect) {
            this.currentEffect.stop();
            this.currentEffect = null;
        }
        this.dropEnabled = true;
        this.contentAlpha = 1.0f;
        this.repaintImageBuffer();
        this.repaint();
    }

    static /* synthetic */ float access$016(DragWindow x0, float x1) {
        return x0.contentAlpha += x1;
    }

    static /* synthetic */ float access$024(DragWindow x0, float x1) {
        return x0.contentAlpha -= x1;
    }

}

