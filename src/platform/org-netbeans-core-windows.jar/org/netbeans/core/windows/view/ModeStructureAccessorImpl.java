/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view;

import java.awt.Rectangle;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.ModeStructureSnapshot;
import org.netbeans.core.windows.model.ModelElement;
import org.netbeans.core.windows.view.EditorAccessor;
import org.netbeans.core.windows.view.ElementAccessor;
import org.netbeans.core.windows.view.ModeAccessor;
import org.netbeans.core.windows.view.ModeStructureAccessor;
import org.netbeans.core.windows.view.SlidingAccessor;
import org.netbeans.core.windows.view.SplitAccessor;
import org.openide.windows.TopComponent;

final class ModeStructureAccessorImpl
implements ModeStructureAccessor {
    private final ElementAccessor splitRootAccessor;
    private final Set<ModeAccessor> separateModeAccessors;
    private final Set<SlidingAccessor> slidingModeAccessors;

    public ModeStructureAccessorImpl(ElementAccessor splitRootAccessor, Set<ModeAccessor> separateModeAccessors, Set<SlidingAccessor> slidingModeAccessors) {
        this.splitRootAccessor = splitRootAccessor;
        this.separateModeAccessors = separateModeAccessors;
        this.slidingModeAccessors = slidingModeAccessors;
    }

    @Override
    public ElementAccessor getSplitRootAccessor() {
        return this.splitRootAccessor;
    }

    @Override
    public ModeAccessor[] getSeparateModeAccessors() {
        return this.separateModeAccessors.toArray(new ModeAccessor[0]);
    }

    @Override
    public SlidingAccessor[] getSlidingModeAccessors() {
        return this.slidingModeAccessors.toArray(new SlidingAccessor[0]);
    }

    public ModeAccessor findModeAccessor(String name) {
        ModeAccessor ma2222 = ModeStructureAccessorImpl.findModeAccessorOfName(this.splitRootAccessor, name);
        if (ma2222 != null) {
            return ma2222;
        }
        for (ModeAccessor ma2222 : this.separateModeAccessors) {
            if (!name.equals(ma2222.getName())) continue;
            return ma2222;
        }
        for (ModeAccessor ma2222 : this.slidingModeAccessors) {
            if (!name.equals(ma2222.getName())) continue;
            return ma2222;
        }
        return null;
    }

    private static ModeAccessor findModeAccessorOfName(ElementAccessor accessor, String name) {
        ModeAccessor ma;
        EditorAccessor editorAccessor;
        if (accessor instanceof ModeAccessor) {
            ModeAccessor ma2 = (ModeAccessor)accessor;
            if (name.equals(ma2.getName())) {
                return ma2;
            }
        } else if (accessor instanceof SplitAccessor) {
            SplitAccessor split = (SplitAccessor)accessor;
            ElementAccessor[] children = split.getChildren();
            for (int i = 0; i < children.length; ++i) {
                ModeAccessor ma3 = ModeStructureAccessorImpl.findModeAccessorOfName(children[i], name);
                if (ma3 == null) continue;
                return ma3;
            }
        } else if (accessor instanceof EditorAccessor && (ma = ModeStructureAccessorImpl.findModeAccessorOfName((editorAccessor = (EditorAccessor)accessor).getEditorAreaAccessor(), name)) != null) {
            return ma;
        }
        return null;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("\nModesAccessorImpl hashCode=" + this.hashCode());
        sb.append("\nSplit modes:\n");
        sb.append(ModeStructureAccessorImpl.dumpAccessor(this.splitRootAccessor, 0));
        sb.append("\nSeparate Modes:");
        sb.append(ModeStructureAccessorImpl.dumpSet(this.separateModeAccessors));
        return sb.toString();
    }

    private static String dumpAccessor(ElementAccessor accessor, int indent) {
        StringBuffer sb = new StringBuffer();
        String indentString = ModeStructureAccessorImpl.createIndentString(indent);
        if (accessor instanceof SplitAccessor) {
            SplitAccessor splitAccessor = (SplitAccessor)accessor;
            sb.append(indentString + "split=" + splitAccessor);
            ++indent;
            ElementAccessor[] children = splitAccessor.getChildren();
            for (int i = 0; i < children.length; ++i) {
                sb.append("\n" + ModeStructureAccessorImpl.dumpAccessor(children[i], indent));
            }
        } else if (accessor instanceof ModeAccessor) {
            sb.append(indentString + "mode=" + accessor);
        } else if (accessor instanceof EditorAccessor) {
            sb.append(indentString + "editor=" + accessor);
            sb.append(ModeStructureAccessorImpl.dumpAccessor(((EditorAccessor)accessor).getEditorAreaAccessor(), ++indent));
        }
        return sb.toString();
    }

    private static String createIndentString(int indent) {
        StringBuffer sb = new StringBuffer(indent);
        for (int i = 0; i < indent; ++i) {
            sb.append(' ');
        }
        return sb.toString();
    }

    private static String dumpSet(Set separateModes) {
        StringBuffer sb = new StringBuffer();
        Iterator it = separateModes.iterator();
        while (it.hasNext()) {
            sb.append("\nmode=" + it.next());
        }
        return sb.toString();
    }

    static final class EditorAccessorImpl
    extends ElementAccessorImpl
    implements EditorAccessor {
        private final ElementAccessor editorAreaAccessor;
        private final double resizeWeight;

        public EditorAccessorImpl(ModelElement originator, ModeStructureSnapshot.ElementSnapshot snapshot, ElementAccessor editorAreaAccessor, double resizeWeight) {
            super(originator, snapshot);
            this.editorAreaAccessor = editorAreaAccessor;
            this.resizeWeight = resizeWeight;
        }

        @Override
        public double getResizeWeight() {
            return this.resizeWeight;
        }

        @Override
        public ElementAccessor getEditorAreaAccessor() {
            return this.editorAreaAccessor;
        }

        @Override
        public String toString() {
            return super.toString() + "\n" + this.editorAreaAccessor;
        }
    }

    static final class SlidingAccessorImpl
    extends ModeAccessorImpl
    implements SlidingAccessor {
        private final String side;
        private final Map<TopComponent, Integer> slideInSizes;

        public SlidingAccessorImpl(ModelElement originator, ModeStructureSnapshot.ModeSnapshot snapshot, String side, Map<TopComponent, Integer> slideInSizes) {
            super(originator, snapshot);
            this.side = side;
            this.slideInSizes = slideInSizes;
        }

        @Override
        public String getSide() {
            return this.side;
        }

        @Override
        public Map<TopComponent, Integer> getSlideInSizes() {
            return this.slideInSizes;
        }

        @Override
        public boolean originatorEquals(ElementAccessor o) {
            if (!super.originatorEquals(o)) {
                return false;
            }
            SlidingAccessor me = (SlidingAccessor)o;
            return this.getSide() == me.getSide();
        }
    }

    static class ModeAccessorImpl
    extends ElementAccessorImpl
    implements ModeAccessor {
        public ModeAccessorImpl(ModelElement originator, ModeStructureSnapshot.ModeSnapshot snapshot) {
            super(originator, snapshot);
        }

        private ModeStructureSnapshot.ModeSnapshot getModeSnapShot() {
            return (ModeStructureSnapshot.ModeSnapshot)this.getSnapshot();
        }

        @Override
        public boolean originatorEquals(ElementAccessor o) {
            if (!super.originatorEquals(o)) {
                return false;
            }
            ModeAccessor me = (ModeAccessor)o;
            return this.getState() == me.getState();
        }

        @Override
        public ModeImpl getMode() {
            return this.getModeSnapShot().getMode();
        }

        @Override
        public String getName() {
            return this.getModeSnapShot().getName();
        }

        @Override
        public int getState() {
            return this.getModeSnapShot().getState();
        }

        @Override
        public int getKind() {
            return this.getModeSnapShot().getKind();
        }

        @Override
        public Rectangle getBounds() {
            return this.getModeSnapShot().getBounds();
        }

        @Override
        public int getFrameState() {
            return this.getModeSnapShot().getFrameState();
        }

        @Override
        public TopComponent getSelectedTopComponent() {
            return this.getModeSnapShot().getSelectedTopComponent();
        }

        @Override
        public TopComponent[] getOpenedTopComponents() {
            return this.getModeSnapShot().getOpenedTopComponents();
        }

        @Override
        public double getResizeWeight() {
            return this.getModeSnapShot().getResizeWeight();
        }

        @Override
        public String toString() {
            return super.toString() + "[name=" + this.getName() + " ]";
        }
    }

    static final class SplitAccessorImpl
    extends ElementAccessorImpl
    implements SplitAccessor {
        private final int orientation;
        private final double[] splitPositions;
        private final ElementAccessor[] children;
        private final double resizeWeight;

        public SplitAccessorImpl(ModelElement originator, ModeStructureSnapshot.ElementSnapshot snapshot, int orientation, double[] splitPositions, ElementAccessor[] children, double resizeWeight) {
            super(originator, snapshot);
            this.orientation = orientation;
            this.splitPositions = splitPositions;
            this.children = children;
            this.resizeWeight = resizeWeight;
        }

        @Override
        public int getOrientation() {
            return this.orientation;
        }

        @Override
        public double[] getSplitWeights() {
            return this.splitPositions;
        }

        @Override
        public ElementAccessor[] getChildren() {
            return this.children;
        }

        @Override
        public double getResizeWeight() {
            return this.resizeWeight;
        }

        @Override
        public String toString() {
            StringBuffer buffer = new StringBuffer();
            buffer.append(super.toString());
            buffer.append("[orientation=" + this.orientation);
            buffer.append(", splitPosition=");
            for (int i = 0; i < this.splitPositions.length; ++i) {
                buffer.append(this.splitPositions[i]);
                if (i >= this.splitPositions.length - 1) continue;
                buffer.append(" : ");
            }
            buffer.append("]");
            return buffer.toString();
        }
    }

    static abstract class ElementAccessorImpl
    implements ElementAccessor {
        private final ModelElement originator;
        private final ModeStructureSnapshot.ElementSnapshot snapshot;

        public ElementAccessorImpl(ModelElement originator, ModeStructureSnapshot.ElementSnapshot snapshot) {
            this.originator = originator;
            this.snapshot = snapshot;
        }

        @Override
        public final ModelElement getOriginator() {
            return this.originator;
        }

        @Override
        public final ModeStructureSnapshot.ElementSnapshot getSnapshot() {
            return this.snapshot;
        }

        @Override
        public boolean originatorEquals(ElementAccessor o) {
            if (o instanceof ElementAccessorImpl) {
                return this.getClass().equals(o.getClass()) && ((ElementAccessorImpl)o).originator == this.originator;
            }
            return false;
        }

        public String toString() {
            return super.toString() + "[originatorHash=" + (this.originator != null ? Integer.toHexString(this.originator.hashCode()) : "null") + "]";
        }
    }

}

