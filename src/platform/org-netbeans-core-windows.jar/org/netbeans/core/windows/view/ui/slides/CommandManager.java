/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.SlideBarDataModel
 *  org.netbeans.swing.tabcontrol.SlidingButton
 *  org.netbeans.swing.tabcontrol.TabData
 *  org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer
 *  org.netbeans.swing.tabcontrol.customtabs.Tabbed
 *  org.netbeans.swing.tabcontrol.customtabs.TabbedComponentFactory
 *  org.netbeans.swing.tabcontrol.customtabs.TabbedType
 *  org.netbeans.swing.tabcontrol.event.TabActionEvent
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view.ui.slides;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.SingleSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.core.windows.view.ui.slides.ResizeGestureRecognizer;
import org.netbeans.core.windows.view.ui.slides.SlideBar;
import org.netbeans.core.windows.view.ui.slides.SlideBarActionEvent;
import org.netbeans.core.windows.view.ui.slides.SlideOperation;
import org.netbeans.core.windows.view.ui.slides.SlideOperationFactory;
import org.netbeans.core.windows.view.ui.slides.TabbedSlideAdapter;
import org.netbeans.swing.tabcontrol.SlideBarDataModel;
import org.netbeans.swing.tabcontrol.SlidingButton;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.customtabs.Tabbed;
import org.netbeans.swing.tabcontrol.customtabs.TabbedComponentFactory;
import org.netbeans.swing.tabcontrol.customtabs.TabbedType;
import org.netbeans.swing.tabcontrol.event.TabActionEvent;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;

final class CommandManager
implements ActionListener {
    private final SlideBar slideBar;
    private Tabbed slidingTabbed;
    private Component curSlidedComp;
    private SlidingButton curSlideButton;
    private int curSlideOrientation;
    private int curSlidedIndex;
    private ResizeGestureRecognizer recog;
    private static final boolean NO_POPUP_PLACEMENT_HACK = Boolean.getBoolean("netbeans.popup.no_hack");
    private AWTEventListener awtListener = null;
    private final Action escapeAction;

    public CommandManager(SlideBar slideBar) {
        this.escapeAction = new EscapeAction();
        this.slideBar = slideBar;
        this.recog = new ResizeGestureRecognizer(this);
    }

    ResizeGestureRecognizer getResizer() {
        return this.recog;
    }

    public void slideResize(int delta) {
        if (!this.isCompSlided()) {
            return;
        }
        SlideOperation op = SlideOperationFactory.createSlideResize(this.getSlidingTabbed().getComponent(), this.curSlideOrientation);
        Rectangle finish = this.getSlidingTabbed().getComponent().getBounds(null);
        String side = CommandManager.orientation2Side(this.curSlideOrientation);
        if ("bottom".equals(side)) {
            finish.height -= delta;
            finish.y += delta;
        }
        if ("right".equals(side)) {
            finish.width -= delta;
            finish.x += delta;
        }
        if ("left".equals(side)) {
            finish.width += delta;
        }
        if ("top".equals(side)) {
            finish.height += delta;
        }
        op.setFinishBounds(finish);
        this.postEvent(new SlideBarActionEvent(this.slideBar, "slideResize", op));
    }

    public void slideIn(int tabIndex) {
        SlideBarDataModel model = this.slideBar.getModel();
        if (this.isCompSlided() && this.curSlidedComp != model.getTab(tabIndex).getComponent()) {
            this.slideOut(false, false);
        }
        this.curSlidedIndex = tabIndex;
        this.curSlidedComp = model.getTab(tabIndex).getComponent();
        this.curSlideOrientation = model.getOrientation();
        this.curSlideButton = this.slideBar.getButton(tabIndex);
        Tabbed cont = this.updateSlidedTabContainer(tabIndex);
        SlideOperation operation = SlideOperationFactory.createSlideIn(cont.getComponent(), this.curSlideOrientation, true, true);
        boolean alreadyListening = false;
        for (AWTEventListener el : Toolkit.getDefaultToolkit().getAWTEventListeners()) {
            if (el != this.getAWTListener()) continue;
            alreadyListening = false;
            break;
        }
        if (!alreadyListening) {
            Toolkit.getDefaultToolkit().addAWTEventListener(this.getAWTListener(), 16);
        }
        this.curSlideButton.setSelected(true);
        this.postEvent(new SlideBarActionEvent(this.slideBar, "slideIn", operation));
        this.recog.attachResizeRecognizer(CommandManager.orientation2Side(this.curSlideOrientation), cont.getComponent());
    }

    public void slideOut(boolean requestsActivation, boolean useEffect) {
        if (!this.isCompSlided()) {
            return;
        }
        SlideOperation operation = SlideOperationFactory.createSlideOut(this.getSlidingTabbed().getComponent(), this.curSlideOrientation, useEffect, requestsActivation);
        Toolkit.getDefaultToolkit().removeAWTEventListener(this.getAWTListener());
        this.curSlideButton.setSelected(false);
        this.recog.detachResizeRecognizer(CommandManager.orientation2Side(this.curSlideOrientation), this.getSlidingTabbed().getComponent());
        this.curSlidedComp = null;
        this.curSlideButton = null;
        this.curSlideOrientation = -1;
        this.curSlidedIndex = -1;
        this.postEvent(new SlideBarActionEvent(this.slideBar, "slideOut", operation));
    }

    public void slideIntoDesktop(int tabIndex, boolean useEffect) {
        SlideOperation operation = null;
        if (this.isCompSlided()) {
            operation = SlideOperationFactory.createSlideIntoDesktop(this.getSlidingTabbed().getComponent(), this.curSlideOrientation, useEffect);
        }
        this.recog.detachResizeRecognizer(CommandManager.orientation2Side(this.curSlideOrientation), this.getSlidingTabbed().getComponent());
        this.postEvent(new SlideBarActionEvent(this.slideBar, "disableAutoHide", operation, null, tabIndex));
    }

    public void toggleTransparency(int tabIndex) {
    }

    public void showPopup(MouseEvent mouseEvent, int tabIndex) {
        this.postEvent(new SlideBarActionEvent(this.slideBar, "popup", mouseEvent, tabIndex));
    }

    protected static String orientation2Side(int orientation) {
        String side = "left";
        if (orientation == 2) {
            side = "left";
        } else if (orientation == 1) {
            side = "right";
        } else if (orientation == 3) {
            side = "bottom";
        } else if (orientation == 4) {
            side = "top";
        }
        return side;
    }

    public void setActive(boolean active) {
        this.getSlidingTabbed().setActive(active);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if ("popup".equals(e.getActionCommand())) {
            TabActionEvent tae = (TabActionEvent)e;
            if (this.curSlidedComp != null && this.curSlidedComp instanceof TopComponent) {
                TopComponent tc = (TopComponent)this.curSlidedComp;
                Action[] actions = this.slideBar.getTabbed().getPopupActions(tc.getActions(), this.curSlidedIndex);
                if (actions == null) {
                    actions = tc.getActions();
                }
                if (actions == null || actions.length == 0) {
                    return;
                }
                CommandManager.showPopupMenu(Utilities.actionsToPopup((Action[])actions, (Lookup)tc.getLookup()), tae.getMouseEvent().getPoint(), tae.getMouseEvent().getComponent());
            }
        } else if ("disableAutoHide".equals(e.getActionCommand())) {
            if (this.curSlidedIndex >= 0) {
                this.slideIntoDesktop(this.curSlidedIndex, true);
            }
        } else if ("enableAutoHide".equals(e.getActionCommand())) {
            this.slideBar.getSelectionModel().setSelectedIndex(-1);
        } else if ("toggleTransparency".equals(e.getActionCommand())) {
            TabActionEvent tae = (TabActionEvent)e;
            this.toggleTransparency(tae.getTabIndex());
        } else if ("maximize".equals(e.getActionCommand())) {
            this.postEvent(new SlideBarActionEvent(this.slideBar, "slideMaximize", null, null, this.curSlidedIndex));
        } else {
            TabActionEvent tae = (TabActionEvent)e;
            if ("close".equals(tae.getActionCommand()) && this.curSlidedIndex < 0) {
                return;
            }
            TabActionEvent newEvt = new TabActionEvent(tae.getSource(), tae.getActionCommand(), this.curSlidedIndex, tae.getMouseEvent());
            this.postEvent((ActionEvent)newEvt);
        }
    }

    private Rectangle getScreenCompRect(Component comp) {
        Rectangle result = new Rectangle(comp.getLocationOnScreen(), comp.getSize());
        return result;
    }

    private static void showPopupMenu(JPopupMenu popup, Point p, Component comp) {
        if (NO_POPUP_PLACEMENT_HACK) {
            popup.show(comp, p.x, p.y);
            return;
        }
        SwingUtilities.convertPointToScreen(p, comp);
        Dimension popupSize = popup.getPreferredSize();
        Rectangle screenBounds = Utilities.getUsableScreenBounds((GraphicsConfiguration)comp.getGraphicsConfiguration());
        if (p.x + popupSize.width > screenBounds.x + screenBounds.width) {
            p.x = screenBounds.x + screenBounds.width - popupSize.width;
        }
        if (p.y + popupSize.height > screenBounds.y + screenBounds.height) {
            p.y = screenBounds.y + screenBounds.height - popupSize.height;
        }
        SwingUtilities.convertPointFromScreen(p, comp);
        popup.show(comp, p.x, p.y);
    }

    private Tabbed getSlidingTabbed() {
        if (this.slidingTabbed == null) {
            TabbedComponentFactory factory = (TabbedComponentFactory)Lookup.getDefault().lookup(TabbedComponentFactory.class);
            this.slidingTabbed = factory.createTabbedComponent(TabbedType.VIEW, this.slideBar.createWinsysInfo());
            this.slidingTabbed.addActionListener((ActionListener)this);
            Border b = null;
            String side = CommandManager.orientation2Side(this.slideBar.getModel().getOrientation());
            b = UIManager.getBorder("floatingBorder-" + side);
            if (b == null) {
                b = UIManager.getBorder("floatingBorder");
            }
            if (b != null && this.slidingTabbed.getComponent() instanceof JComponent) {
                ((JComponent)this.slidingTabbed.getComponent()).setBorder(b);
            }
            if (this.slidingTabbed.getComponent() instanceof JComponent) {
                this.registerEscHandler((JComponent)this.slidingTabbed.getComponent());
            }
        }
        return this.slidingTabbed;
    }

    private Tabbed updateSlidedTabContainer(int tabIndex) {
        Tabbed container = this.getSlidingTabbed();
        SlideBarDataModel dataModel = this.slideBar.getModel();
        TabData origTab = dataModel.getTab(tabIndex);
        TopComponent tc = (TopComponent)origTab.getComponent();
        container.setTopComponents(new TopComponent[]{tc}, tc);
        return container;
    }

    private void registerEscHandler(JComponent comp) {
        comp.getInputMap(2).put(KeyStroke.getKeyStroke(27, 0), "slideOut");
        comp.getActionMap().put("slideOut", this.escapeAction);
    }

    private AWTEventListener getAWTListener() {
        if (null == this.awtListener) {
            this.awtListener = new AWTEventListener(){

                @Override
                public void eventDispatched(AWTEvent event) {
                    TopComponent tc;
                    if (event.getID() == 500 && event.getSource() instanceof Component && !SwingUtilities.isDescendingFrom((Component)event.getSource(), CommandManager.this.getSlidingTabbed().getComponent()) && !SwingUtilities.isDescendingFrom((Component)event.getSource(), CommandManager.this.slideBar) && null != (tc = CommandManager.this.slideBar.getTabbed().getSelectedTopComponent()) && TopComponent.getRegistry().getActivated() != tc) {
                        CommandManager.this.slideBar.getSelectionModel().setSelectedIndex(-1);
                    }
                }
            };
        }
        return this.awtListener;
    }

    boolean isCompSlided() {
        return this.curSlidedComp != null;
    }

    Component getSlidedComp() {
        if (!this.isCompSlided()) {
            return null;
        }
        return this.slidingTabbed.getComponent();
    }

    void syncWithModel() {
        if (this.curSlidedComp == null) {
            return;
        }
        if (!this.slideBar.containsComp(this.curSlidedComp)) {
            this.slideOut(false, false);
        } else {
            SlideBarDataModel model = this.slideBar.getModel();
            if (this.curSlidedIndex < model.size()) {
                String freshText = model.getTab(this.curSlidedIndex).getText();
                this.getSlidingTabbed().setTitleAt(0, freshText);
                this.slideBar.repaint();
                this.curSlideButton = this.slideBar.getButton(this.curSlidedIndex);
                this.curSlideButton.setSelected(true);
            }
        }
    }

    private void postEvent(ActionEvent evt) {
        ((TabbedSlideAdapter)this.slideBar.getTabbed()).postActionEvent(evt);
    }

    private final class EscapeAction
    extends AbstractAction {
        private EscapeAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            CommandManager.this.slideBar.getSelectionModel().setSelectedIndex(-1);
        }
    }

}

