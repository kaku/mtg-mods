/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.windows.view.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;
import org.netbeans.core.windows.view.Controller;
import org.netbeans.core.windows.view.ui.MainWindow;
import org.openide.util.NbBundle;

public class EditorAreaFrame
extends JFrame {
    private Component desktop;
    private Controller controller;
    private long frametimestamp = 0;
    private long timeStamp = 0;
    private long mainWindowStamp = 0;

    public EditorAreaFrame() {
        super(NbBundle.getMessage(EditorAreaFrame.class, (String)"LBL_EditorAreaFrameTitle"));
        MainWindow.initFrameIcons(this);
    }

    public void setWindowActivationListener(Controller control) {
        this.controller = control;
        this.addWindowListener(new WindowAdapter(){

            @Override
            public void windowActivated(WindowEvent evt) {
                if (EditorAreaFrame.this.frametimestamp != 0 && System.currentTimeMillis() > EditorAreaFrame.this.frametimestamp + 500) {
                    EditorAreaFrame.this.controller.userActivatedEditorWindow();
                }
            }

            @Override
            public void windowOpened(WindowEvent event) {
                EditorAreaFrame.this.frametimestamp = System.currentTimeMillis();
            }
        });
    }

    @Override
    public void toFront() {
        this.frametimestamp = System.currentTimeMillis();
        super.toFront();
    }

    @Override
    public void setVisible(boolean visible) {
        this.frametimestamp = System.currentTimeMillis();
        super.setVisible(visible);
    }

    public void setDesktop(Component component) {
        if (this.desktop == component) {
            return;
        }
        if (this.desktop != null) {
            this.getContentPane().remove(this.desktop);
        }
        this.desktop = component;
        if (component != null) {
            this.getContentPane().add(component);
        }
    }

    public void setUserStamp(long stamp) {
        this.timeStamp = stamp;
    }

    public long getUserStamp() {
        return this.timeStamp;
    }

    public void setMainWindowStamp(long stamp) {
        this.mainWindowStamp = stamp;
    }

    public long getMainWindowStamp() {
        return this.mainWindowStamp;
    }

}

