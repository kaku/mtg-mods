/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view;

import org.netbeans.core.windows.view.ElementAccessor;

public interface EditorAccessor
extends ElementAccessor {
    public ElementAccessor getEditorAreaAccessor();

    public double getResizeWeight();
}

