/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view.ui;

import org.netbeans.core.windows.view.ModeView;

public interface ModeComponent {
    public ModeView getModeView();

    public int getKind();
}

