/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.SlideBarDataModel
 *  org.netbeans.swing.tabcontrol.SlidingButton
 *  org.netbeans.swing.tabcontrol.TabData
 *  org.netbeans.swing.tabcontrol.TabDataModel
 *  org.netbeans.swing.tabcontrol.TabDisplayer
 *  org.netbeans.swing.tabcontrol.TabbedContainer
 *  org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer
 *  org.netbeans.swing.tabcontrol.customtabs.Tabbed
 *  org.netbeans.swing.tabcontrol.customtabs.Tabbed$Accessor
 *  org.netbeans.swing.tabcontrol.event.ComplexListDataEvent
 *  org.netbeans.swing.tabcontrol.event.ComplexListDataListener
 *  org.netbeans.swing.tabcontrol.event.TabActionEvent
 *  org.netbeans.swing.tabcontrol.plaf.BusyTabsSupport
 *  org.netbeans.swing.tabcontrol.plaf.TabControlButton
 *  org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view.ui.slides;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;
import javax.swing.SingleSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListDataEvent;
import org.netbeans.core.windows.EditorOnlyDisplayer;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.view.SlidingView;
import org.netbeans.core.windows.view.ui.slides.CommandManager;
import org.netbeans.core.windows.view.ui.slides.ResizeGestureRecognizer;
import org.netbeans.core.windows.view.ui.slides.SlideBarContainer;
import org.netbeans.core.windows.view.ui.slides.SlideBarController;
import org.netbeans.core.windows.view.ui.slides.SlideGestureRecognizer;
import org.netbeans.core.windows.view.ui.slides.TabbedSlideAdapter;
import org.netbeans.core.windows.view.ui.tabcontrol.TabbedAdapter;
import org.netbeans.swing.tabcontrol.SlideBarDataModel;
import org.netbeans.swing.tabcontrol.SlidingButton;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.TabbedContainer;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.customtabs.Tabbed;
import org.netbeans.swing.tabcontrol.event.ComplexListDataEvent;
import org.netbeans.swing.tabcontrol.event.ComplexListDataListener;
import org.netbeans.swing.tabcontrol.event.TabActionEvent;
import org.netbeans.swing.tabcontrol.plaf.BusyTabsSupport;
import org.netbeans.swing.tabcontrol.plaf.TabControlButton;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public final class SlideBar
extends JPanel
implements ComplexListDataListener,
SlideBarController,
Tabbed.Accessor,
ChangeListener,
ActionListener {
    public static final String COMMAND_SLIDE_IN = "slideIn";
    public static final String COMMAND_SLIDE_OUT = "slideOut";
    public static final String COMMAND_SLIDE_RESIZE = "slideResize";
    public static final String COMMAND_POPUP_REQUEST = "popup";
    public static final String COMMAND_DISABLE_AUTO_HIDE = "disableAutoHide";
    public static final String COMMAND_MAXIMIZE = "slideMaximize";
    private static final boolean isAqua = "Aqua".equals(UIManager.getLookAndFeel().getID());
    private final TabbedSlideAdapter tabbed;
    private final SlideBarDataModel dataModel;
    private final SingleSelectionModel selModel;
    private SlideGestureRecognizer gestureRecognizer;
    private List<SlidingButton> buttons;
    private CommandManager commandMgr;
    private boolean active = false;
    private final TabDisplayer dummyDisplayer = new TabDisplayer();
    private final int separatorOrientation;
    private int row = 0;
    private int col = 0;
    private PropertyChangeListener _lookAndFeelListener;

    public SlideBar(TabbedSlideAdapter tabbed, SlideBarDataModel dataModel, SingleSelectionModel selModel) {
        super(new GridBagLayout());
        this.tabbed = tabbed;
        this.dataModel = dataModel;
        this.selModel = selModel;
        this.commandMgr = new CommandManager(this);
        this.gestureRecognizer = new SlideGestureRecognizer(this, this.commandMgr.getResizer());
        this.buttons = new ArrayList<SlidingButton>(5);
        this.separatorOrientation = tabbed.isHorizontal() ? 1 : 0;
        this.dummyDisplayer.addActionListener((ActionListener)this);
        this.syncWithModel();
        dataModel.addComplexListDataListener((ComplexListDataListener)this);
        selModel.addChangeListener(this);
        this.updateLAF();
    }

    private void updateLAF() {
        if (isAqua) {
            Color bkColor = UIManager.getColor("NbSplitPane.background");
            if (null == bkColor) {
                bkColor = this.getBackground().darker();
            }
            if (this.dataModel.getOrientation() == 3) {
                this.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, bkColor));
            } else if (this.dataModel.getOrientation() == 4) {
                this.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, bkColor));
            } else if (this.dataModel.getOrientation() == 2) {
                this.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, bkColor));
            } else if (this.dataModel.getOrientation() == 1) {
                this.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, bkColor));
            }
        }
        if (UIManager.getBoolean("NbMainWindow.showCustomBackground")) {
            this.setOpaque(false);
        }
    }

    public SlideBarDataModel getModel() {
        return this.dataModel;
    }

    public SingleSelectionModel getSelectionModel() {
        return this.selModel;
    }

    public void intervalAdded(ListDataEvent e) {
        this.syncWithModel();
    }

    public void intervalRemoved(ListDataEvent e) {
        this.syncWithModel();
    }

    public void contentsChanged(ListDataEvent e) {
        this.syncWithModel();
    }

    public void indicesAdded(ComplexListDataEvent e) {
        this.syncWithModel();
    }

    public void indicesChanged(ComplexListDataEvent e) {
        this.syncWithModel();
    }

    public void indicesRemoved(ComplexListDataEvent e) {
        this.syncWithModel();
    }

    public int tabForCoordinate(int x, int y) {
        Rectangle curBounds = new Rectangle();
        int index = 0;
        Iterator<SlidingButton> iter = this.buttons.iterator();
        while (iter.hasNext()) {
            ((Component)iter.next()).getBounds(curBounds);
            if (curBounds.contains(x, y)) {
                return index;
            }
            ++index;
        }
        return -1;
    }

    int nextTabForCoordinate(int x, int y) {
        Rectangle curBounds = new Rectangle();
        int index = 0;
        for (Component comp : this.buttons) {
            comp.getBounds(curBounds);
            if (this.tabbed.isHorizontal()) {
                if (curBounds.x + curBounds.width / 2 < x) {
                    ++index;
                    continue;
                }
            } else if (curBounds.y + curBounds.height / 2 < y) {
                ++index;
                continue;
            }
            return index;
        }
        return index;
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        int selIndex = this.selModel.getSelectedIndex();
        if (selIndex >= 0) {
            EditorOnlyDisplayer.getInstance().cancel(false);
        }
        this.tabbed.postSelectionEvent();
        if (this.isDisplayable() && this.isVisible()) {
            if (selIndex != -1) {
                this.commandMgr.slideIn(selIndex);
            } else {
                this.commandMgr.slideOut(true, true);
            }
        }
    }

    @Override
    public void userToggledAutoHide(int tabIndex, boolean enabled) {
        this.commandMgr.slideIntoDesktop(tabIndex, true);
    }

    @Override
    public void userToggledTransparency(int tabIndex) {
        if (tabIndex != this.getSelectionModel().getSelectedIndex()) {
            this.getSelectionModel().setSelectedIndex(tabIndex);
        }
        this.commandMgr.toggleTransparency(tabIndex);
    }

    @Override
    public void userTriggeredPopup(MouseEvent mouseEvent, Component clickedButton) {
        int index = this.getButtonIndex(clickedButton);
        this.commandMgr.showPopup(mouseEvent, index);
    }

    private SlidingButton buttonFor(TopComponent tc) {
        TabData td;
        int idx = 0;
        Iterator i = this.dataModel.getTabs().iterator();
        while (i.hasNext() && (td = (TabData)i.next()).getComponent() != tc) {
            if (!i.hasNext()) {
                idx = -1;
                continue;
            }
            ++idx;
        }
        if (idx >= 0 && idx < this.dataModel.size()) {
            return this.getButton(idx);
        }
        return null;
    }

    public void setBlinking(TopComponent tc, boolean val) {
        SlidingButton button = this.buttonFor(tc);
        if (button != null) {
            button.setBlinking(val);
        }
    }

    @Override
    public void userClickedSlidingButton(Component clickedButton) {
        int index = this.getButtonIndex(clickedButton);
        SlidingButton button = this.buttons.get(index);
        button.setBlinking(false);
        if (index != this.selModel.getSelectedIndex() || !this.isActive()) {
            TopComponent tc = (TopComponent)this.dataModel.getTab(index).getComponent();
            if (tc != null) {
                tc.requestActive();
            }
            button.setSelected(true);
        } else {
            this.selModel.setSelectedIndex(-1);
        }
    }

    @Override
    public void userMiddleClickedSlidingButton(Component clickedButton) {
        int index = this.getButtonIndex(clickedButton);
        SlidingButton button = this.buttons.get(index);
        button.setBlinking(false);
        if (index >= 0 && index < this.dataModel.size()) {
            TopComponent tc = (TopComponent)this.dataModel.getTab(index).getComponent();
            tc.close();
        }
    }

    @Override
    public boolean userTriggeredAutoSlideIn(Component sourceButton) {
        int index = this.getButtonIndex(sourceButton);
        if (index < 0) {
            return false;
        }
        SlidingButton button = this.buttons.get(index);
        button.setBlinking(false);
        TopComponent tc = (TopComponent)this.dataModel.getTab(index).getComponent();
        if (tc == null) {
            return false;
        }
        tc.requestVisible();
        return true;
    }

    @Override
    public void userTriggeredAutoSlideOut() {
        this.selModel.setSelectedIndex(-1);
    }

    public Rectangle getTabBounds(int tabIndex) {
        SlidingButton button = this.getButton(tabIndex);
        if (button == null) {
            return null;
        }
        Insets insets = this.getInsets();
        Point leftTop = new Point(insets.left, insets.top);
        if (this.tabbed.isHorizontal()) {
            if (tabIndex < this.dataModel.size()) {
                leftTop.x = this.getButton((int)tabIndex).getLocation().x;
            }
        } else if (tabIndex < this.dataModel.size()) {
            leftTop.y = this.getButton((int)tabIndex).getLocation().y;
        }
        return new Rectangle(leftTop, button.getPreferredSize());
    }

    public Tabbed getTabbed() {
        return this.tabbed;
    }

    public WinsysInfoForTabbedContainer createWinsysInfo() {
        return new SlidedWinsysInfoForTabbedContainer();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        TabActionEvent tae;
        if (e instanceof TabActionEvent && "restoreGroup".equals((tae = (TabActionEvent)e).getActionCommand())) {
            String nameOfModeToRestore = tae.getGroupName();
            WindowManagerImpl wm = WindowManagerImpl.getInstance();
            ModeImpl modeToRestore = (ModeImpl)wm.findMode(nameOfModeToRestore);
            if (null != modeToRestore) {
                wm.userRestoredMode(this.tabbed.getSlidingMode(), modeToRestore);
            }
        }
    }

    private GridBagConstraints createConstraints() {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = this.row++;
        c.gridy = this.col++;
        c.fill = 1;
        c.anchor = 10;
        if (!this.tabbed.isHorizontal()) {
            // empty if block
        }
        return c;
    }

    private void addSeparator() {
        int separatorSize = UIManager.getInt("NbSlideBar.GroupSeparator.Size");
        if (separatorSize > 0) {
            this.addStrut(separatorSize);
        } else {
            JSeparator separator = new JSeparator(this.separatorOrientation);
            int gap = UIManager.getInt("NbSlideBar.GroupSeparator.Gap.Before");
            if (gap == 0) {
                gap = 15;
            }
            this.addStrut(gap);
            GridBagConstraints c = this.createConstraints();
            c.insets = new Insets(1, 1, 1, 1);
            c.fill = this.separatorOrientation == 1 ? 3 : 2;
            this.add((Component)separator, c);
            gap = UIManager.getInt("NbSlideBar.GroupSeparator.Gap.After");
            if (gap == 0) {
                gap = 5;
            }
            this.addStrut(gap);
        }
    }

    private void addRestoreButton(String modeName) {
        if (null == modeName) {
            return;
        }
        TabControlButton restoreButton = TabControlButtonFactory.createRestoreGroupButton((TabDisplayer)this.dummyDisplayer, (String)modeName);
        this.add((Component)restoreButton, this.createConstraints());
        restoreButton.putClientProperty((Object)"NbSlideBar.RestoreButton.Orientation", (Object)this.getModel().getOrientation());
        int gap = UIManager.getInt("NbSlideBar.RestoreButton.Gap");
        if (gap == 0) {
            gap = 10;
        }
        this.addStrut(gap);
    }

    private void addButton(SlidingButton sb) {
        AquaButtonPanel btn = isAqua ? new AquaButtonPanel((JToggleButton)sb) : sb;
        this.add((Component)btn, this.createConstraints());
        int gap = UIManager.getInt("NbSlideBar.SlideButton.Gap");
        if (gap > 0) {
            this.addStrut(gap);
        }
    }

    void makeBusy(TopComponent tc, boolean busy) {
        Component slide;
        BusyTabsSupport.getDefault().makeTabBusy((Tabbed)this.tabbed, 0, busy);
        if (!busy && null != (slide = this.getSlidedComp())) {
            slide.repaint();
        }
        this.syncWithModel();
    }

    @Override
    public void addNotify() {
        super.addNotify();
        BusyTabsSupport.getDefault().install(this.getTabbed(), (TabDataModel)this.dataModel);
        this._lookAndFeelListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                SlideBar.this.updateLAF();
            }
        };
        UIManager.addPropertyChangeListener(this._lookAndFeelListener);
    }

    @Override
    public void removeNotify() {
        UIManager.removePropertyChangeListener(this._lookAndFeelListener);
        this._lookAndFeelListener = null;
        super.removeNotify();
        BusyTabsSupport.getDefault().uninstall(this.getTabbed(), (TabDataModel)this.dataModel);
    }

    boolean isHorizontal() {
        return this.tabbed.isHorizontal();
    }

    Component getSlidedComp() {
        return this.commandMgr.getSlidedComp();
    }

    void setActive(boolean active) {
        this.active = active;
        this.commandMgr.setActive(active);
    }

    boolean isActive() {
        return this.active;
    }

    boolean isHoveringAllowed() {
        return !this.isActive() || !this.commandMgr.isCompSlided();
    }

    int getButtonIndex(Component button) {
        return this.buttons.indexOf(button);
    }

    SlidingButton getButton(int index) {
        return this.buttons.get(index);
    }

    boolean containsComp(Component comp) {
        List tabs = this.getModel().getTabs();
        TabData curTab2 = null;
        for (TabData curTab2 : tabs) {
            if (!comp.equals(curTab2.getComponent())) continue;
            return true;
        }
        return false;
    }

    private void addStrut(int size) {
        JLabel lbl = new JLabel();
        Dimension dim = new Dimension(size, size);
        lbl.setMinimumSize(dim);
        lbl.setPreferredSize(dim);
        lbl.setMaximumSize(dim);
        GridBagConstraints c = this.createConstraints();
        c.fill = 0;
        this.add((Component)lbl, c);
    }

    private void syncWithModel() {
        Container container;
        assert (SwingUtilities.isEventDispatchThread());
        HashSet<TabData> blinks = null;
        for (SlidingButton curr : this.buttons) {
            if (curr.isBlinking()) {
                if (blinks == null) {
                    blinks = new HashSet<TabData>();
                }
                blinks.add(curr.getData());
            }
            this.gestureRecognizer.detachButton((AbstractButton)curr);
        }
        this.removeAll();
        this.buttons.clear();
        List dataList = this.dataModel.getTabs();
        SlidingButton curButton = null;
        String currentMode = null;
        boolean first = true;
        this.row = 0;
        this.col = 0;
        for (TabData td : dataList) {
            TopComponent tc;
            curButton = new SlidingButton(td, this.dataModel.getOrientation());
            if (blinks != null && blinks.contains((Object)td)) {
                curButton.setBlinking(true);
            }
            if (this.tabbed.isBusy(tc = (TopComponent)td.getComponent())) {
                curButton.setIcon(BusyTabsSupport.getDefault().getBusyIcon(false));
            }
            String modeName = this.getRestoreModeNameForTab(td);
            this.gestureRecognizer.attachButton((AbstractButton)curButton);
            this.buttons.add(curButton);
            if (Switches.isModeSlidingEnabled()) {
                if (isAqua && first) {
                    this.addStrut(4);
                }
                if (null == currentMode || !currentMode.equals(modeName)) {
                    if (!first) {
                        this.addSeparator();
                    }
                    this.addRestoreButton(modeName);
                    currentMode = modeName;
                    first = false;
                }
                this.addButton(curButton);
                continue;
            }
            this.addButton(curButton);
        }
        boolean giveExtraPadding = false;
        if (!dataList.isEmpty()) {
            giveExtraPadding = true;
        }
        if ((container = this.getParent()) != null && container.getClass().getName().startsWith("org.netbeans.core.windows.view.ui.slides.SlideBarContainer$VisualPanel")) {
            SlideBarContainer.VisualPanel vp = (SlideBarContainer.VisualPanel)container;
            vp.setBorder(SlideBarContainer.computeBorder(vp.getOuterClass().getSlidingView().getSide(), giveExtraPadding));
        }
        GridBagConstraints c = this.createConstraints();
        if (this.tabbed.isHorizontal()) {
            c.weightx = 1.0;
        } else {
            c.weighty = 1.0;
        }
        c.fill = 0;
        this.add((Component)new JLabel(), c);
        this.commandMgr.syncWithModel();
        if (!UIManager.getBoolean("NbMainWindow.showCustomBackground")) {
            this.setOpaque(!this.buttons.isEmpty());
        }
        this.revalidate();
        this.repaint();
    }

    private String getRestoreModeNameForTab(TabData tab) {
        ModeImpl prevMode;
        String tcId;
        WindowManagerImpl wm;
        Component c = tab.getComponent();
        if (c instanceof TopComponent && null != (tcId = (wm = WindowManagerImpl.getInstance()).findTopComponentID((TopComponent)c)) && null != (prevMode = wm.getPreviousModeForTopComponent(tcId, this.tabbed.getSlidingMode()))) {
            return prevMode.getName();
        }
        return null;
    }

    boolean isSlidedTabTransparent() {
        boolean res = false;
        Component c = this.getSlidedComp();
        res = c instanceof TabbedContainer ? ((TabbedContainer)c).isTransparent() : this.tabbed.isTransparent();
        return res;
    }

    private static final class VerticalBorder
    implements Border {
        private VerticalBorder() {
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            g.setColor(UIManager.getColor("NbBrushedMetal.darkShadow"));
            g.drawLine(x, y, x + width - 1, y);
            g.setColor(UIManager.getColor("NbBrushedMetal.lightShadow"));
            g.drawLine(x, y + height - 1, x + width - 1, y + height - 1);
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return new Insets(1, 3, 1, 3);
        }

        @Override
        public boolean isBorderOpaque() {
            return false;
        }
    }

    private static final class BottomBorder
    implements Border {
        private BottomBorder() {
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            g.setColor(UIManager.getColor("NbBrushedMetal.darkShadow"));
            g.drawLine(x, y, x, y + height);
            g.setColor(UIManager.getColor("NbBrushedMetal.lightShadow"));
            g.drawLine(x + width - 1, y, x + width - 1, y + height);
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return new Insets(3, 1, 3, 1);
        }

        @Override
        public boolean isBorderOpaque() {
            return false;
        }
    }

    private class AquaButtonPanel
    extends JPanel {
        private final JToggleButton slidingButton;
        private final Border pressedBorder;

        public AquaButtonPanel(JToggleButton button) {
            super(new GridBagLayout());
            this.slidingButton = button;
            this.pressedBorder = SlideBar.this.tabbed.isHorizontal() ? new BottomBorder() : new VerticalBorder();
            this.add((Component)button, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, 10, 0, new Insets(2, 0, 2, 0), 0, 0));
            button.getModel().addChangeListener(new ChangeListener(SlideBar.this){
                final /* synthetic */ SlideBar val$this$0;

                @Override
                public void stateChanged(ChangeEvent e) {
                    AquaButtonPanel.this.repaint();
                }
            });
        }

        private boolean isPressed() {
            if (null == this.slidingButton) {
                return false;
            }
            ButtonModel model = this.slidingButton.getModel();
            return model.isArmed() || model.isPressed() || model.isSelected();
        }

        private boolean isRollover() {
            if (null == this.slidingButton) {
                return false;
            }
            ButtonModel model = this.slidingButton.getModel();
            return model.isRollover();
        }

        @Override
        public boolean isOpaque() {
            return this.isPressed() || this.isRollover();
        }

        @Override
        public Color getBackground() {
            if (this.isRollover()) {
                return UIManager.getColor("NbSlideBar.rollover");
            }
            if (this.isPressed()) {
                return UIManager.getColor("NbSplitPane.background");
            }
            return super.getBackground();
        }

        @Override
        public Border getBorder() {
            if (this.isPressed()) {
                return this.pressedBorder;
            }
            return BorderFactory.createEmptyBorder();
        }

        @Override
        public Dimension getPreferredSize() {
            return null == this.slidingButton ? super.getPreferredSize() : this.slidingButton.getPreferredSize();
        }

        @Override
        public Dimension getMinimumSize() {
            return null == this.slidingButton ? super.getMinimumSize() : this.slidingButton.getMinimumSize();
        }

        @Override
        public Dimension getMaximumSize() {
            return null == this.slidingButton ? super.getMaximumSize() : this.slidingButton.getMaximumSize();
        }

    }

    private class SlidedWinsysInfoForTabbedContainer
    extends WinsysInfoForTabbedContainer {
        private SlidedWinsysInfoForTabbedContainer() {
        }

        public Object getOrientation(Component comp) {
            if (WindowManagerImpl.getInstance().getEditorAreaState() != 0) {
                return TabDisplayer.ORIENTATION_INVISIBLE;
            }
            return TabDisplayer.ORIENTATION_CENTER;
        }

        public boolean inMaximizedMode(Component comp) {
            return TabbedAdapter.isInMaximizedMode(comp);
        }

        public boolean isTopComponentSlidingEnabled() {
            return Switches.isTopComponentSlidingEnabled();
        }

        public boolean isTopComponentClosingEnabled() {
            return Switches.isViewTopComponentClosingEnabled();
        }

        public boolean isTopComponentMaximizationEnabled() {
            return Switches.isTopComponentMaximizationEnabled();
        }

        public boolean isTopComponentClosingEnabled(TopComponent tc) {
            return !Boolean.TRUE.equals(tc.getClientProperty((Object)"netbeans.winsys.tc.closing_disabled"));
        }

        public boolean isTopComponentMaximizationEnabled(TopComponent tc) {
            return !Boolean.TRUE.equals(tc.getClientProperty((Object)"netbeans.winsys.tc.maximization_disabled"));
        }

        public boolean isTopComponentSlidingEnabled(TopComponent tc) {
            return !Boolean.TRUE.equals(tc.getClientProperty((Object)"netbeans.winsys.tc.sliding_disabled"));
        }

        public boolean isModeSlidingEnabled() {
            return Switches.isModeSlidingEnabled();
        }

        public boolean isSlidedOutContainer() {
            return true;
        }

        public boolean isTopComponentBusy(TopComponent tc) {
            return WindowManagerImpl.getInstance().isTopComponentBusy(tc);
        }
    }

}

