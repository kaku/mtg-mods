/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 *  org.openide.windows.ExternalDropHandler
 */
package org.netbeans.core.windows.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.core.windows.Constants;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.view.Controller;
import org.netbeans.core.windows.view.ViewElement;
import org.netbeans.core.windows.view.dnd.TopComponentDraggable;
import org.netbeans.core.windows.view.dnd.TopComponentDroppable;
import org.netbeans.core.windows.view.dnd.WindowDnDManager;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.windows.ExternalDropHandler;

public class EditorView
extends ViewElement {
    private static final boolean IS_GTK = "GTK".equals(UIManager.getLookAndFeel().getID());
    private ViewElement editorArea;
    private EditorAreaComponent editorAreaComponent;
    private final WindowDnDManager windowDnDManager;
    private static DataFlavor URI_LIST_DATA_FLAVOR;

    public EditorView(Controller controller, WindowDnDManager windowDnDManager, double resizeWeight, ViewElement editorArea) {
        super(controller, resizeWeight);
        this.editorArea = editorArea;
        this.windowDnDManager = windowDnDManager;
    }

    Rectangle getPureBounds() {
        EditorAreaComponent comp = this.getEditorAreaComponent();
        Rectangle bounds = comp.getBounds();
        Point location = new Point(0, 0);
        SwingUtilities.convertPointToScreen(location, comp);
        bounds.setLocation(location);
        return bounds;
    }

    private EditorAreaComponent getEditorAreaComponent() {
        if (this.editorAreaComponent == null) {
            this.editorAreaComponent = new EditorAreaComponent(this, this.windowDnDManager);
        }
        if (IS_GTK && !this.editorAreaComponent.isValid()) {
            this.editorAreaComponent.repaint();
        }
        return this.editorAreaComponent;
    }

    private void manageBorder(JPanel panel) {
        if (this.editorArea != null) {
            panel.setBorder(null);
        } else if (Utilities.isMac()) {
            panel.setBorder(BorderFactory.createEmptyBorder());
        } else {
            Border border = UIManager.getBorder("Nb.EmptyEditorArea.border");
            if (null == border) {
                border = UIManager.getBorder("Nb.ScrollPane.border");
            }
            panel.setBorder(border);
        }
    }

    public ViewElement getEditorArea() {
        return this.editorArea;
    }

    public void setEditorArea(ViewElement editorArea) {
        this.editorArea = editorArea;
    }

    @Override
    public Component getComponent() {
        return this.getEditorAreaComponent();
    }

    @Override
    public boolean updateAWTHierarchy(Dimension availableSpace) {
        boolean result = false;
        EditorAreaComponent comp = this.getEditorAreaComponent();
        Dimension d = (Dimension)comp.getClientProperty("lastAvailableSpace");
        Dimension currDim = comp.getPreferredSize();
        if (!availableSpace.equals(d) || !availableSpace.equals(currDim)) {
            comp.setPreferredSize(availableSpace);
            comp.putClientProperty("lastAvailableSpace", availableSpace);
            result = true;
        }
        this.assureComponentInEditorArea();
        if (this.editorArea != null) {
            result |= this.editorArea.updateAWTHierarchy(new Dimension(availableSpace.width - 5, availableSpace.height - 5));
        }
        return result;
    }

    void assureComponentInEditorArea() {
        EditorAreaComponent eac = this.getEditorAreaComponent();
        if (this.editorArea == null) {
            eac.setAreaComponent(null);
        } else {
            eac.setAreaComponent(this.editorArea.getComponent());
        }
        this.manageBorder(eac);
    }

    static {
        try {
            URI_LIST_DATA_FLAVOR = new DataFlavor("text/uri-list;class=java.lang.String");
        }
        catch (ClassNotFoundException cnfE) {
            cnfE.printStackTrace();
        }
    }

    private static class EditorAreaComponent
    extends JPanel
    implements TopComponentDroppable {
        private final EditorView editorView;
        private final WindowDnDManager windowDnDManager;
        private Component areaComponent;

        public EditorAreaComponent(EditorView editorView, WindowDnDManager windowDnDManager) {
            this.editorView = editorView;
            this.windowDnDManager = windowDnDManager;
            this.init();
        }

        private void init() {
            this.setLayout(new BorderLayout());
            String lfID = UIManager.getLookAndFeel().getID();
            String imageSource = Constants.SWITCH_IMAGE_SOURCE;
            if (imageSource != null) {
                Image image = ImageUtilities.loadImage((String)imageSource);
                if (image != null) {
                    JLabel label = new JLabel(new ImageIcon(image));
                    label.setMinimumSize(new Dimension(0, 0));
                    this.add((Component)label, "Center");
                } else {
                    Logger.getLogger(EditorView.class.getName()).log(Level.WARNING, null, new NullPointerException("Image not found at " + imageSource));
                }
            }
            DropTarget dropTarget = new DropTarget(this, new DropTargetListener(){

                @Override
                public void dragEnter(DropTargetDragEvent dtde) {
                }

                @Override
                public void dragExit(DropTargetEvent dte) {
                }

                @Override
                public void dragOver(DropTargetDragEvent dtde) {
                    for (ExternalDropHandler handler : Lookup.getDefault().lookupAll(ExternalDropHandler.class)) {
                        if (!handler.canDrop(dtde)) continue;
                        dtde.acceptDrag(1);
                        return;
                    }
                    dtde.rejectDrag();
                }

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void drop(DropTargetDropEvent dtde) {
                    boolean dropRes = false;
                    try {
                        for (ExternalDropHandler handler : Lookup.getDefault().lookupAll(ExternalDropHandler.class)) {
                            if (!handler.canDrop(dtde)) continue;
                            dtde.acceptDrop(1);
                            dropRes = handler.handleDrop(dtde);
                            break;
                        }
                    }
                    finally {
                        dtde.dropComplete(dropRes);
                    }
                }

                @Override
                public void dropActionChanged(DropTargetDragEvent dtde) {
                }
            });
            this.setDropTarget(dropTarget);
            if (UIManager.getBoolean("NbMainWindow.showCustomBackground") || "Aqua".equals(UIManager.getLookAndFeel().getID())) {
                this.setOpaque(false);
            }
        }

        public void setAreaComponent(Component areaComponent) {
            if (this.areaComponent == areaComponent) {
                if (areaComponent != null && !Arrays.asList(this.getComponents()).contains(areaComponent)) {
                    this.add(areaComponent, "Center");
                }
                return;
            }
            if (this.areaComponent != null) {
                this.remove(this.areaComponent);
            }
            this.areaComponent = areaComponent;
            if (this.areaComponent != null) {
                this.add(this.areaComponent, "Center");
            }
        }

        @Override
        public Shape getIndicationForLocation(Point location) {
            int kind = this.windowDnDManager.getStartingTransfer().getKind();
            if (kind == 1) {
                Rectangle rect = this.getBounds();
                rect.setLocation(0, 0);
                return rect;
            }
            Rectangle rect = this.getBounds();
            rect.setLocation(0, 0);
            String side = this.getSideForLocation(location);
            double ratio = 0.25;
            if ("top".equals(side)) {
                return new Rectangle(0, 0, rect.width, (int)((double)rect.height * ratio));
            }
            if (side == "left") {
                return new Rectangle(0, 0, (int)((double)rect.width * ratio), rect.height);
            }
            if (side == "right") {
                return new Rectangle(rect.width - (int)((double)rect.width * ratio), 0, (int)((double)rect.width * ratio), rect.height);
            }
            if (side == "bottom") {
                return new Rectangle(0, rect.height - (int)((double)rect.height * ratio), rect.width, (int)((double)rect.height * ratio));
            }
            if (this.windowDnDManager.getStartingTransfer().isAllowedToMoveAnywhere()) {
                return rect;
            }
            return null;
        }

        @Override
        public Object getConstraintForLocation(Point location) {
            int kind = this.windowDnDManager.getStartingTransfer().getKind();
            if (kind == 1) {
                return null;
            }
            return this.getSideForLocation(location);
        }

        private String getSideForLocation(Point location) {
            Rectangle bounds = this.getBounds();
            bounds.setLocation(0, 0);
            int delta = 20;
            Rectangle top = new Rectangle(0, 0, bounds.width, delta);
            if (top.contains(location)) {
                return "top";
            }
            Rectangle left = new Rectangle(0, delta, delta, bounds.height - 2 * delta);
            if (left.contains(location)) {
                return "left";
            }
            Rectangle right = new Rectangle(bounds.width - delta, delta, delta, bounds.height - 2 * delta);
            if (right.contains(location)) {
                return "right";
            }
            Rectangle bottom = new Rectangle(0, bounds.height - delta, bounds.width, delta);
            if (bottom.contains(location)) {
                return "bottom";
            }
            return null;
        }

        @Override
        public Component getDropComponent() {
            return this;
        }

        @Override
        public ViewElement getDropViewElement() {
            return this.editorView;
        }

        @Override
        public boolean canDrop(TopComponentDraggable transfer, Point location) {
            if (transfer.isAllowedToMoveAnywhere()) {
                return true;
            }
            int kind = transfer.getKind();
            if (kind == 1) {
                return true;
            }
            if (WindowManagerImpl.getInstance().getEditorAreaState() == 0 && this.getSideForLocation(location) != null) {
                return true;
            }
            return false;
        }

        @Override
        public boolean supportsKind(TopComponentDraggable transfer) {
            return true;
        }

        @Override
        public int getKind() {
            return 1;
        }

    }

}

