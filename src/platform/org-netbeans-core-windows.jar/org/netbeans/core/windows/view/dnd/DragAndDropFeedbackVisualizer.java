/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.customtabs.Tabbed
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view.dnd;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Window;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Area;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import org.netbeans.core.windows.nativeaccess.NativeWindowSystem;
import org.netbeans.core.windows.options.WinSysPrefs;
import org.netbeans.core.windows.view.dnd.DragWindow;
import org.netbeans.swing.tabcontrol.customtabs.Tabbed;
import org.openide.windows.TopComponent;

public class DragAndDropFeedbackVisualizer {
    private static final Preferences prefs = WinSysPrefs.HANDLER;
    private DragWindow dragWindow = null;
    private Tabbed source;
    private Point originalLocationOnScreen;
    private Point dragOffset;
    private int tabIndex;
    private static final int SLIDE_INTERVAL = 33;

    public DragAndDropFeedbackVisualizer(Tabbed src, int tabIndex) {
        this.source = src;
        this.tabIndex = tabIndex;
    }

    private DragWindow createDragWindow(int idx) {
        Rectangle tabRectangle = this.source.getTabBounds(idx);
        Dimension tabContentSize = this.source.getTopComponentAt(idx).getSize();
        --tabContentSize.width;
        --tabContentSize.height;
        tabContentSize.width = Math.max(tabContentSize.width, 1);
        tabContentSize.height = Math.max(tabContentSize.height, 1);
        Dimension size = new Dimension(tabContentSize);
        if (prefs.getBoolean("dnd.smallwindows", true)) {
            int maxWidth = prefs.getInt("dnd.smallwindows.width", 250);
            int maxHeight = prefs.getInt("dnd.smallwindows.height", 250);
            size.width = Math.min(maxWidth, size.width);
            size.height = Math.min(maxHeight, size.height);
            tabRectangle.width = Math.min(maxHeight, tabRectangle.width);
        }
        DragWindow w = new DragWindow(this.source, tabRectangle, new Dimension(size), (Component)this.source.getTopComponentAt(idx));
        size.width += 2;
        size.height += 2;
        Dimension windowSize = new Dimension(size);
        windowSize.height += tabRectangle.height;
        w.setSize(windowSize);
        NativeWindowSystem nws = NativeWindowSystem.getDefault();
        if (nws.isUndecoratedWindowAlphaSupported() && prefs.getBoolean("transparency.dragimage", true)) {
            Area mask;
            nws.setWindowAlpha(w, prefs.getFloat("transparency.dragimage.alpha", 0.7f));
            if (prefs.getBoolean("dnd.smallwindows", true)) {
                mask = new Area(new Rectangle(0, 0, tabRectangle.width, tabRectangle.height));
                mask.add(new Area(new Rectangle(0, tabRectangle.height, size.width, size.height)));
            } else {
                mask = new Area(tabRectangle);
                mask.add(new Area(new Rectangle(0, tabRectangle.height, size.width, size.height)));
            }
            nws.setWindowMask((Window)w, mask);
        }
        return w;
    }

    public void start(DragGestureEvent e) {
        DragWindow tmp;
        this.originalLocationOnScreen = this.source.getComponent().getLocationOnScreen();
        Rectangle tabRect = this.source.getTabBounds(this.tabIndex);
        if (prefs.getBoolean("dnd.smallwindows", true)) {
            this.originalLocationOnScreen.x += tabRect.x;
        }
        if (null != (tmp = this.createDragWindow(this.tabIndex))) {
            this.dragOffset = new Point(0, 0);
            Point loc = new Point(e.getDragOrigin());
            SwingUtilities.convertPointToScreen(loc, e.getComponent());
            tmp.setLocation(loc.x - this.dragOffset.x, loc.y - this.dragOffset.y);
            try {
                tmp.setVisible(true);
                this.dragWindow = tmp;
            }
            catch (UnsatisfiedLinkError ulE) {
                Logger.getLogger(DragAndDropFeedbackVisualizer.class.getName()).log(Level.INFO, null, ulE);
            }
            catch (Throwable ex) {
                Logger.getLogger(DragAndDropFeedbackVisualizer.class.getName()).log(Level.FINE, null, ex);
            }
        }
    }

    public void update(DragSourceDragEvent e) {
        if (null != this.dragWindow) {
            this.dragWindow.setLocation(e.getLocation().x - this.dragOffset.x, e.getLocation().y - this.dragOffset.y);
        }
    }

    public void dispose(boolean dropSuccessful) {
        if (null == this.dragWindow) {
            return;
        }
        if (!dropSuccessful) {
            this.returnDragWindowToOrigin();
        } else {
            this.dragWindow.dispose();
        }
        this.dragWindow = null;
    }

    public void setDropFeedback(boolean dropEnabled, boolean mixedTCDragDrop) {
        if (null != this.dragWindow) {
            this.dragWindow.setDropFeedback(dropEnabled);
        }
    }

    private void returnDragWindowToOrigin() {
        final Timer timer = new Timer(33, null);
        final DragWindow returningWindow = this.dragWindow;
        this.dragWindow.abort();
        timer.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Point location = returningWindow.getLocationOnScreen();
                Point dst = new Point(DragAndDropFeedbackVisualizer.this.originalLocationOnScreen);
                int dx = (dst.x - location.x) / 2;
                int dy = (dst.y - location.y) / 2;
                if (dx != 0 || dy != 0) {
                    location.translate(dx, dy);
                    returningWindow.setLocation(location);
                } else {
                    timer.stop();
                    returningWindow.dispose();
                }
            }
        });
        timer.setInitialDelay(0);
        timer.start();
    }

}

