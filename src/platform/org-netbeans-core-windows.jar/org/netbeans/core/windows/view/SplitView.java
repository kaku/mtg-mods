/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view;

import java.awt.Component;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.core.windows.Debug;
import org.netbeans.core.windows.view.Controller;
import org.netbeans.core.windows.view.ViewElement;
import org.netbeans.core.windows.view.ui.MultiSplitPane;

public class SplitView
extends ViewElement {
    private int orientation;
    private ArrayList<Double> splitWeights;
    private ArrayList<ViewElement> children;
    private MultiSplitPane splitPane;
    private boolean isDirty = false;

    public SplitView(Controller controller, double resizeWeight, int orientation, List<Double> splitWeights, List<ViewElement> children) {
        super(controller, resizeWeight);
        this.orientation = orientation;
        this.splitWeights = new ArrayList<Double>(splitWeights);
        this.children = new ArrayList<ViewElement>(children);
    }

    public void setOrientation(int newOrientation) {
        this.orientation = newOrientation;
    }

    public void setSplitWeights(List<Double> newSplitWeights) {
        this.splitWeights.clear();
        this.splitWeights.addAll(newSplitWeights);
    }

    public int getOrientation() {
        return this.orientation;
    }

    public List<ViewElement> getChildren() {
        return new ArrayList<ViewElement>(this.children);
    }

    @Override
    public Component getComponent() {
        return this.getSplitPane();
    }

    public void remove(ViewElement view) {
        int index = this.children.indexOf(view);
        if (index >= 0) {
            this.children.remove(index);
            this.splitWeights.remove(index);
            if (null != this.splitPane) {
                this.splitPane.removeViewElementAt(index);
            }
            this.isDirty = true;
        }
    }

    public void setChildren(List<ViewElement> newChildren) {
        this.children.clear();
        this.children.addAll(newChildren);
        assert (this.children.size() == this.splitWeights.size());
        this.isDirty = true;
        if (null != this.splitPane) {
            this.updateSplitPane();
        }
    }

    @Override
    public boolean updateAWTHierarchy(Dimension availableSpace) {
        boolean res = false;
        if (!availableSpace.equals(this.getSplitPane().getSize()) || this.isDirty) {
            this.isDirty = false;
            this.getSplitPane().setSize(availableSpace);
            this.getSplitPane().invalidate();
            res = true;
        }
        for (ViewElement child : this.children) {
            res |= child.updateAWTHierarchy(child.getComponent().getSize());
        }
        return res;
    }

    private MultiSplitPane getSplitPane() {
        if (this.splitPane == null) {
            Integer override;
            int dividerSize;
            this.splitPane = new MultiSplitPane();
            this.updateSplitPane();
            if (this.orientation == 0) {
                dividerSize = UIManager.getInt("Nb.SplitPane.dividerSize.vertical");
                if (dividerSize == 0 && (dividerSize = UIManager.getInt("SplitPane.dividerSize")) == 0) {
                    dividerSize = 4;
                }
            } else {
                dividerSize = UIManager.getInt("Nb.SplitPane.dividerSize.horizontal");
                if (dividerSize == 0 && (dividerSize = UIManager.getInt("SplitPane.dividerSize")) == 0) {
                    dividerSize = 4;
                }
            }
            if (null != (override = Integer.getInteger("Nb.SplitPane.dividerSize"))) {
                dividerSize = override;
            }
            this.splitPane.setDividerSize(dividerSize);
            this.splitPane.setBorder(BorderFactory.createEmptyBorder());
            this.splitPane.addPropertyChangeListener("splitPositions", new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    ArrayList<Double> weights = new ArrayList<Double>(SplitView.this.children.size());
                    ArrayList<ViewElement> views = new ArrayList<ViewElement>(SplitView.this.children.size());
                    SplitView.this.splitPane.calculateSplitWeights(views, weights);
                    ViewElement[] arrViews = new ViewElement[views.size()];
                    double[] arrWeights = new double[views.size()];
                    for (int i = 0; i < views.size(); ++i) {
                        arrViews[i] = views.get(i);
                        arrWeights[i] = weights.get(i);
                    }
                    SplitView.this.getController().userMovedSplit(SplitView.this, arrViews, arrWeights);
                }
            });
        }
        return this.splitPane;
    }

    public int getDividerSize() {
        return this.getSplitPane().getDividerSize();
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(Object.super.toString());
        buffer.append("[");
        for (int i = 0; i < this.children.size(); ++i) {
            ViewElement child = this.children.get(i);
            buffer.append(i + 1);
            buffer.append('=');
            if (child instanceof SplitView) {
                buffer.append(child.getClass());
                buffer.append('@');
                buffer.append(Integer.toHexString(child.hashCode()));
            } else {
                buffer.append(child.toString());
            }
            if (i >= this.children.size() - 1) continue;
            buffer.append(", ");
        }
        buffer.append("]");
        return buffer.toString();
    }

    private void updateSplitPane() {
        ViewElement[] arrViews = new ViewElement[this.children.size()];
        double[] arrSplitWeights = new double[this.children.size()];
        for (int i = 0; i < this.children.size(); ++i) {
            ViewElement view;
            arrViews[i] = view = this.children.get(i);
            arrSplitWeights[i] = this.splitWeights.get(i);
        }
        this.splitPane.setChildren(this.orientation, arrViews, arrSplitWeights);
    }

    private static void debugLog(String message) {
        Debug.log(SplitView.class, message);
    }

}

